package com.faiglelabs.floid.floidserver.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import java.util.*;
import java.util.function.Function;

@Component
public class JwtTokenUtil {
    @Value("${jwt.validity}")
    private long JWT_TOKEN_VALIDITY;
    @Value("${jwt.validity-download}")
    private long JWT_TOKEN_VALIDITY_DOWNLOAD;
    private final static Logger logger = LoggerFactory.getLogger(JwtTokenUtil.class.getName());
    private final static String IS_DOWNLOAD_JWT_CLAIMS_TOKEN = "IsDownload";
    private final static String IS_DOWNLOAD_FILE_NAME_CLAIMS_TOKEN = "DownloadFileName";
    private static SecretKeySpec SECRET_KEY;
    private static final String KEY_HASH_ALG = "HmacSHA256";

    @Value("${jwt.secret}")
    public void setSecretStatic(String secret) {
        JwtTokenUtil.SECRET_KEY = new SecretKeySpec(Base64.getDecoder().decode(secret), KEY_HASH_ALG);
    }

    //retrieve username from jwt token
    public String getUsernameFromToken(String token) {
        try {
            return getClaimFromToken(token, Claims::getSubject);
        } catch (io.jsonwebtoken.ExpiredJwtException expiredJwtException) {
            logger.warn("getUsernameFromToken: Token expired");
        } catch (Exception e) {
            logger.error("getUsernameFromToken exception: " + e.toString());
        }
        return null;
    }

    //retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        if(claims==null) return null;
        return claimsResolver.apply(claims);
    }

    //for retrieving any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token) {
        try {
            return Jwts.parserBuilder().setSigningKey(SECRET_KEY).build().parseClaimsJws(token).getBody();
        } catch (io.jsonwebtoken.ExpiredJwtException expiredJwtException) {
            logger.warn("getAllClaimsFromToken: Token expired");
        } catch (Exception e) {
            logger.error("getAllClaimsFromToken exception: " + e.toString());
        }
        return null;
    }

    //check if the token has expired
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    //generate token for user
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, userDetails.getUsername(), false);
    }

    public String generateDownloadToken(UserDetails userDetails, String downloadFileName) {
        if(userDetails!=null && downloadFileName != null && !downloadFileName.isEmpty()) {
            Map<String, Object> claims = new HashMap<>();
            claims.put(IS_DOWNLOAD_JWT_CLAIMS_TOKEN, true);
            claims.put(IS_DOWNLOAD_FILE_NAME_CLAIMS_TOKEN, downloadFileName);
            return doGenerateToken(claims, userDetails.getUsername(), true);
        }
        return null;
    }

    // 1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
    // 2. Sign the JWT using the HS512 algorithm and secret key.
    // 3. Compact according to JWS Compact Serialization (https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
    private String doGenerateToken(Map<String, Object> claims, String subject, boolean download) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + (download ? JWT_TOKEN_VALIDITY_DOWNLOAD : JWT_TOKEN_VALIDITY) * 1000))
                .signWith(SECRET_KEY).compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        if(username==null) return false;
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    public boolean validateDownloadToken(String token, JwtRequestFilter.DownloadInformation downloadInformation) {
        try {
            if(token!=null && downloadInformation!=null) {
                final String username = getUsernameFromToken(token);
                if(username==null) return false;
                if(!isTokenExpired(token)) {
                    Claims claims = getAllClaimsFromToken(token);
                    if (claims != null) {
                        Boolean isDownloadFile = claims.get(IS_DOWNLOAD_JWT_CLAIMS_TOKEN, Boolean.class);
                        String downloadFileName = claims.get(IS_DOWNLOAD_FILE_NAME_CLAIMS_TOKEN, String.class);
                        if (isDownloadFile && downloadFileName != null) {
                            downloadInformation.fileName = downloadFileName;
                            ArrayList<GrantedAuthority> authorities = new ArrayList<>();
                            authorities.add(new SimpleGrantedAuthority("ROLE_" + WebSecurityConfig.FLOID_ROLE_FLOID_APP_DOWNLOAD));
                            downloadInformation.userDetails = new User(username, "", authorities);
                            logger.info("Successful download token validation");
                            return true;
                        }
                    }
                } else {
                    logger.warn("Download Token expired for " + username);
                }
            }
        } catch (Exception e) {
            logger.error("Exception in validateDownloadToken", e);
         }
        return false;
    }


}
