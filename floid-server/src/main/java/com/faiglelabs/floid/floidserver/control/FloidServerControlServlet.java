/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.floidserver.control;

import com.faiglelabs.floid.floidserver.FloidServer;
import com.faiglelabs.floid.floidserver.FloidServerRunnable;
import com.faiglelabs.floid.floidserver.security.JwtTokenUtil;
import com.faiglelabs.floid.servertypes.commands.FloidServerCommandResult;
import com.faiglelabs.floid.servertypes.floidid.FloidIdAndState;
import com.faiglelabs.floid.servertypes.floidid.FloidUuid;
import com.faiglelabs.floid.servertypes.floidid.FloidUuidAndState;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.annotation.PostConstruct;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * The type Floid server control servlet.
 */
@CrossOrigin
@WebServlet(value = "/fsc")
public class FloidServerControlServlet extends HttpServlet {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    private final static Logger logger = LoggerFactory.getLogger(FloidServerControlServlet.class.getName());

    /**
     * The floidServerControlServletConfig.getLogger().
     */
    private static final FloidServerControlServletConfig floidServerControlServletConfig = new FloidServerControlServletConfig();
    private static FloidServer floidServer = null;
    private static final Object floidServerSynchronizer = new Object();

    /**
     * Start.
     */
    @SuppressWarnings("WeakerAccess")
    public static void start() {
        synchronized (floidServerSynchronizer) {
            if (floidServer == null) {
                if (floidServerControlServletConfig.getPort() == 0) {
                    floidServerControlServletConfig.getLogger().info("FloidServer: BAD PORT: CANNOT START WITH PORT 0");
                    return;
                }
                floidServerControlServletConfig.getLogger().info("FloidServer: Starting");
                floidServer = new FloidServer(floidServerControlServletConfig.getPort(),
                                              floidServerControlServletConfig.isRunThreadDumper(),
                                              floidServerControlServletConfig.getServerKeyStorePath(),
                                              floidServerControlServletConfig.getServerKeyStorePasswordString(),
                                              floidServerControlServletConfig.getServerCertificatePasswordString(),
                                              floidServerControlServletConfig.getMessagingTemplate(),
                                              floidServerControlServletConfig.getJwtTokenUtil());
                if (floidServer.start()) {
                    floidServerControlServletConfig.getLogger().info("FloidServer: Startup Complete");
                } else {
                    floidServer = null;
                    floidServerControlServletConfig.getLogger().error("FloidServer: Startup Failure");
                }
            } else {
                floidServerControlServletConfig.getLogger().info("FloidServer: Already started");
            }
        }
    }

    /**
     * Stop.
     */
    @SuppressWarnings("WeakerAccess")
    static void stop() {
        synchronized (floidServerSynchronizer) {
            if (floidServer != null) {
                floidServerControlServletConfig.getLogger().info("*** FloidServer: Stopping ***");
                floidServer.stop();
                floidServer = null;
                floidServerControlServletConfig.getLogger().info("*** FloidServer: Stopping complete ***");
            } else {
                floidServerControlServletConfig.getLogger().info("*** FloidServer: Already stopped ***");
            }
        }
    }

    /**
     * Restart.
     */
    @SuppressWarnings("WeakerAccess")
    static void restart() {
        stop();
        start();
    }

    /**
     * Control.
     *
     * @param command the command
     */
    public static void control(String command) {
        if (command.compareTo("start") == 0) {
            floidServerControlServletConfig.getLogger().info("*** FloidServer: Received Start ***");
            start();
        } else if (command.compareTo("stop") == 0) {
            floidServerControlServletConfig.getLogger().info("*** FloidServer: Received Stop ***");
            stop();
        } else {
            floidServerControlServletConfig.getLogger().info("*** FloidServer: Received Restart ***");
            if (command.compareTo("restart") == 0) {
                restart();
            } else {
                floidServerControlServletConfig.getLogger().info("*** FloidServer: Unknown Command ***");
                // Unknown
                // Do nothing...
            }
        }
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    @SuppressWarnings("unused")
    public static String getStatus() {
        if (floidServer != null) {
            return floidServer.getStatus();
        } else {
            return "STOPPED";
        }
    }

    /**
     * Send instruction.
     *
     * @param floidId          the floid id
     * @param droidInstruction the droid instruction
     * @param floidServerCommandResult the floid server command result
     */
    public static void sendInstruction(int floidId, DroidInstruction droidInstruction, FloidServerCommandResult floidServerCommandResult) {
        if (floidServer != null) {
            floidServerCommandResult.setServletSuccess(true);
            floidServer.sendInstruction(floidId, droidInstruction, floidServerCommandResult);
        } else {
            floidServerCommandResult.setServletSuccess(false);
            floidServerCommandResult.setResultMessage(FloidServerRunnable.ERROR_MESSAGE_NO_SERVER);
            floidServerCommandResult.setResult(FloidServerRunnable.ERROR_CODE_NO_SERVER);
        }
    }

    public static String createDownloadToken(String userName, int fileId) {
        if (floidServer!=null && userName!=null) {
            logger.info("Creating download token for " + userName + " for file " + fileId);
            return floidServer.createDownloadToken(userName, fileId);
        } else {
            logger.info("Creating download token failed as file or user name was null");
            return "";
        }
    }
    /**
     * Gets floid list.
     *
     * @return the floid list
     */
    public static ArrayList<Integer> getFloidList() {
        if (floidServer != null) {
            return floidServer.getFloidList();
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Gets floid id and state list.
     *
     * @return the floid id and state list
     */
    public static ArrayList<FloidIdAndState> getFloidIdAndStateList() {
        if (floidServer != null) {
            return FloidServer.getFloidIdAndStateList();
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Gets floid uuid list.
     *
     * @param floidId the floid id
     * @return the floid uuid list
     */
    @SuppressWarnings("unused")
    public static ArrayList<FloidUuid> getFloidUuidList(int floidId) {
        if (floidServer != null) {
            return FloidServer.getFloidUuidList(floidId);
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Gets floid uuid and state list.
     *
     * @param floidId the floid id
     * @return the floid uuid and state list
     */
    public static ArrayList<FloidUuidAndState> getFloidUuidAndStateList(int floidId) {
        if (floidServer != null) {
            return FloidServer.getFloidUuidAndStateList(floidId);
        } else {
            return new ArrayList<>();
        }
    }

    @Value("${floid.server_port}")
    private int serverPort;

    @Value("${floid.keystore_password}")
    private String keystorePassword;

    @Value("${floid.certificate_password}")
    private String certificatePassword;

    @Value("${floid.run_floid_server_thread_dumper}")
    private boolean runFloidServerThreadDumper;

    @Value("${floid.run_floid_server_on_startup}")
    private boolean runFloidServerOnStartup;

    @Override
    @PostConstruct
    public void init() {
        try {
            floidServerControlServletConfig.setLogger(LoggerFactory.getLogger(this.getClass().getName()));
            //  Template
            floidServerControlServletConfig.setJwtTokenUtil(jwtTokenUtil);
            // Messaging Template
            floidServerControlServletConfig.setMessagingTemplate(messagingTemplate);
            floidServerControlServletConfig.getLogger().info("Floid Server Messaging Template: " + floidServerControlServletConfig.getMessagingTemplate());
            // Server:
            floidServerControlServletConfig.setPort(serverPort);
            floidServerControlServletConfig.getLogger().info("Floid Server Port: " + floidServerControlServletConfig.getPort());
            // Key & Certificate Stores:
            floidServerControlServletConfig.setServerKeyStorePath("keys/floid_server.jks");
            floidServerControlServletConfig.getLogger().info("Floid Server KeyStore Path: " + floidServerControlServletConfig.getServerKeyStorePath());
            floidServerControlServletConfig.setServerKeyStorePasswordString(keystorePassword);
            floidServerControlServletConfig.getLogger().info("Floid Server Keystore Password: " + "--> HIDDEN <---"); //floidServerControlServletConfig.getServerKeyStorePasswordString());
            floidServerControlServletConfig.setServerCertificatePasswordString(certificatePassword);
            floidServerControlServletConfig.getLogger().info("Floid Server Certificate Password: " + "--> HIDDEN <---"); //floidServerControlServletConfig.getServerCertificatePasswordString());
            // Run the thread dumper?
            floidServerControlServletConfig.setRunThreadDumper(runFloidServerThreadDumper);
            // Run on startup:
            floidServerControlServletConfig.getLogger().info("FloidServer run_floid_server_on_startup = " + (runFloidServerOnStartup ? "true" : "false"));
            if (runFloidServerOnStartup) {
                start();
            }
        } catch (Exception e) {
            floidServerControlServletConfig.getLogger().error("FloidServer: failed to load config: ", e);
        }
    }

    @Override
    public void destroy() {
        stop();
    }

    /**
     * Process request.
     *
     * @param request  the request
     * @param response the response
     */
    @SuppressWarnings("WeakerAccess")
    void processRequest(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        String command = "";
        try {
            command = request.getParameter("request");
        } catch (Exception e) {
            // Do nothing
        }

        if (command == null) {
            floidServerControlServletConfig.getLogger().info("Received null command.");
            command = "";
        }
        if(!("start".equals(command) || "stop".equals(command) || "restart".equals(command))) {
            command = "unknown";
        }
        try (PrintWriter out = response.getWriter()) {

            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FloidServerControlServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Command: " + command + " [Performing]</h1>");
            control(command);
            out.println("<h1>Command: " + command + " [Performed]</h1>");

            out.println("</body>");
            out.println("</html>");
        } catch (Exception e) {
            floidServerControlServletConfig.getLogger().error("Caught error processing servlet request", e);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
