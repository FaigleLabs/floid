package com.faiglelabs.floid.floidserver.rest;

import com.faiglelabs.floid.floidserver.FloidServerRunnable;
import com.faiglelabs.floid.floidserver.control.FloidServerControlServlet;
import com.faiglelabs.floid.servertypes.commands.*;
import com.faiglelabs.floid.servertypes.floidid.FloidIdAndState;
import com.faiglelabs.floid.servertypes.floidid.FloidUuidAndState;
import com.faiglelabs.floid.servertypes.geo.FloidGpsPoint;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import com.faiglelabs.floid.servertypes.mission.DroidMissionList;
import com.faiglelabs.floid.servertypes.photovideo.FloidDroidPhoto;
import com.faiglelabs.floid.servertypes.photovideo.FloidDroidVideoFrame;
import com.faiglelabs.floid.servertypes.pins.FloidPinSet;
import com.faiglelabs.floid.servertypes.pins.FloidPinSetAlt;
import com.faiglelabs.floid.servertypes.pins.FloidPinSetList;
import com.faiglelabs.floid.servertypes.statuses.FloidDroidStatus;
import com.faiglelabs.floid.servertypes.statuses.FloidModelParameters;
import com.faiglelabs.floid.servertypes.statuses.FloidModelStatus;
import com.faiglelabs.floid.servertypes.statuses.FloidStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;

import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("/floidserver")
public class FloidServerREST {
    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    /**
     * The starting log token for the command
     */
    private static final String FLOID_COMMAND_LOG_START = "Sending...";

    /**
     * The ending log token for the command
     */
    private static final String FLOID_COMMAND_LOG_END = "Done";

    @CrossOrigin
    @RequestMapping(value="/user", method=RequestMethod.GET)
    @ResponseBody
    public String user() {
        String userName = "";
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof UserDetails) {
                userName = ((UserDetails) principal).getUsername();
            } else {
                userName = principal.toString();
            }
        } catch (Exception e) {
            logger.error("userInfo caught exception", e);
        }
        return userName;
    }

    @CrossOrigin
    @RequestMapping(value="/floidServerStatus", method=RequestMethod.GET)
    @ResponseBody
    public String floidServerStatus() {
        return FloidServerControlServlet.getStatus();
    }

    @CrossOrigin
    @RequestMapping(value="/createDownloadToken", method=RequestMethod.POST)
    @ResponseBody
    public String createDownloadToken(@RequestParam("fileId") int fileId) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        if(securityContext!=null) {
            Authentication authentication = securityContext.getAuthentication();
            if(authentication!=null) {
                Object principal = authentication.getPrincipal();
                if (principal != null) {
                    String userName;
                    if (principal instanceof UserDetails) {
                        userName = ((UserDetails) principal).getUsername();
                    } else {
                        userName = principal.toString();
                    }
                    return FloidServerControlServlet.createDownloadToken(userName, fileId);
                }
            }
        }
        return "";
    }

    @CrossOrigin
    @RequestMapping(value="/floidList", method=RequestMethod.GET)
    @ResponseBody
    public ArrayList<Integer> floidList() {
        return FloidServerControlServlet.getFloidList();
    }

    @CrossOrigin
    @RequestMapping(value="/floidIdAndStateList", method=RequestMethod.GET)
    @ResponseBody
    public ArrayList<FloidIdAndState> floidIdAndState() {
        return FloidServerControlServlet.getFloidIdAndStateList();
    }

    @CrossOrigin
    @RequestMapping(value="/floidUuidAndStateList", method=RequestMethod.GET)
    @ResponseBody
    public ArrayList<FloidUuidAndState> floidUuidAndStateList(@RequestParam("floidId") int floidId) {
        return FloidServerControlServlet.getFloidUuidAndStateList(floidId);
    }

    @CrossOrigin
    @RequestMapping(value="/missionList", method=RequestMethod.GET)
    @ResponseBody
    public ArrayList<DroidMissionList> missionList() {
        return FloidServerRunnable.getMissionList();
    }

    @CrossOrigin
    @RequestMapping(value="/mission", method=RequestMethod.GET)
    @ResponseBody
    public Document mission(@RequestParam("missionId") int missionId) {
        return FloidServerRunnable.getMission(missionId);
    }

    @CrossOrigin
    @RequestMapping(value="/missionJSON", method=RequestMethod.GET)
    @ResponseBody
    public String missionJSON(@RequestParam("missionId") int missionId) {
        return FloidServerRunnable.getMissionJSON(missionId);
    }

    @CrossOrigin
    @RequestMapping(value="/pinSetList", method=RequestMethod.GET)
    @ResponseBody
    public ArrayList<FloidPinSetList> pinSetList() {
        return FloidServerRunnable.getPinSetList();
    }

    @CrossOrigin
    @RequestMapping(value="/pinSet", method=RequestMethod.GET)
    @ResponseBody
    public FloidPinSetAlt pinSet(@RequestParam("pinSetId") int pinSetId) {
        return FloidServerRunnable.getPinSet(pinSetId);
    }

    @CrossOrigin
    @RequestMapping(value="/gpsPoints", method=RequestMethod.GET)
    @ResponseBody
    public ArrayList<FloidGpsPoint> gpsPoints(@RequestParam("floidId") int floidId, @RequestParam("floidUuid") String floidUuid) {
        return FloidServerRunnable.getGpsPoints(floidId, floidUuid);
    }

    @CrossOrigin
    @RequestMapping(value="/gpsPointsFiltered", method=RequestMethod.GET)
    @ResponseBody
    public ArrayList<FloidGpsPoint> gpsPointsFiltered(@RequestParam("floidId") int floidId,
                                                      @RequestParam("floidUuid") String floidUuid,
                                                      @RequestParam("maxPoints") int maxPoints,
                                                      @RequestParam("minTime") long minTime,
                                                      @RequestParam("minDistance") double minDistance,
                                                      @RequestParam("maxKink") double maxKink,
                                                      @RequestParam("maxKinkAlpha") double maxKinkAlpha) {
        return FloidServerRunnable.getGpsPointsFiltered(floidId, floidUuid,maxPoints, minTime, minDistance, maxKink, maxKinkAlpha);
    }

    @CrossOrigin
    @RequestMapping(value="/droidGpsPoints", method=RequestMethod.GET)
    @ResponseBody
    public ArrayList<FloidGpsPoint> droidGpsPoints(@RequestParam("floidId") int floidId, @RequestParam("floidUuid") String floidUuid) {
        return FloidServerRunnable.getDroidGpsPoints(floidId, floidUuid);
    }

    @CrossOrigin
    @RequestMapping(value="/lastFloidDroidStatus", method=RequestMethod.GET)
    @ResponseBody
    public FloidDroidStatus lastFloidDroidStatus(@RequestParam("floidId") int floidId, @RequestParam("floidUuid") String floidUuid) {
        return FloidServerRunnable.getLastFloidDroidStatus(floidId, floidUuid);
    }

    @CrossOrigin
    @RequestMapping(value="/lastFloidStatus", method=RequestMethod.GET)
    @ResponseBody
    public FloidStatus lastFloidStatus(@RequestParam("floidId") int floidId, @RequestParam("floidUuid") String floidUuid) {
        return FloidServerRunnable.getLastFloidStatus(floidId, floidUuid);
    }

    @CrossOrigin
    @RequestMapping(value="/lastFloidModelStatus", method=RequestMethod.GET)
    @ResponseBody
    public FloidModelStatus lastFloidModelStatus(@RequestParam("floidId") int floidId, @RequestParam("floidUuid") String floidUuid) {
        return FloidServerRunnable.getLastFloidModelStatus(floidId, floidUuid);
    }

    @CrossOrigin
    @RequestMapping(value="/lastFloidModelParameters", method=RequestMethod.GET)
    @ResponseBody
    public FloidModelParameters lastFloidModelParameters(@RequestParam("floidId") int floidId, @RequestParam("floidUuid") String floidUuid) {
        return FloidServerRunnable.getLastFloidModelParameters(floidId, floidUuid);
    }

    @CrossOrigin
    @RequestMapping(value="/droidGpsPointsFiltered", method=RequestMethod.GET)
    @ResponseBody
    public ArrayList<FloidGpsPoint> droidGpsPointsFiltered(@RequestParam("floidId") int floidId,
                                                      @RequestParam("floidUuid") String floidUuid,
                                                      @RequestParam("maxPoints") int maxPoints,
                                                      @RequestParam("minTime") long minTime,
                                                      @RequestParam("minDistance") double minDistance,
                                                      @RequestParam("maxKink") double maxKink,
                                                      @RequestParam("maxKinkAlpha") double maxKinkAlpha) {
        return FloidServerRunnable.getDroidGpsPointsFiltered(floidId, floidUuid,maxPoints, minTime, minDistance, maxKink, maxKinkAlpha);
    }

    @CrossOrigin
    @RequestMapping(value="/lastFloidDroidPhoto", method=RequestMethod.GET)
    @ResponseBody
    public FloidDroidPhoto lastFloidDroidPhoto(@RequestParam("floidId") int floidId, @RequestParam("floidUuid") String floidUuid) {
        return FloidServerRunnable.getLastFloidDroidPhoto(floidId, floidUuid);
    }

    @CrossOrigin
    @RequestMapping(value="/lastFloidDroidVideoFrame", method=RequestMethod.GET)
    @ResponseBody
    public FloidDroidVideoFrame lastFloidDroidVideoFrame(@RequestParam("floidId") int floidId, @RequestParam("floidUuid") String floidUuid) {
        return FloidServerRunnable.getLastFloidDroidVideoFrame(floidId, floidUuid);
    }


    @CrossOrigin
    @RequestMapping(value="/performAcquireHomePositionCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performAcquireHomePositionCommand(@RequestParam("floidId") int floidId,
                                                                      @RequestParam("requiredXYDistance") double requiredXYDistance,
                                                                      @RequestParam("requiredAltitudeDistance") double requiredAltitudeDistance,
                                                                      @RequestParam("requiredHeadingDistance") double requiredHeadingDistance,
                                                                      @RequestParam("requiredGPSGoodCount") int requiredGPSGoodCount,
                                                                      @RequestParam("requiredAltimeterGoodCount") int requiredAltimeterGoodCount) {
        // Create the command:
        AcquireHomePositionCommand acquireHomePositionCommand = new AcquireHomePositionCommand();
        acquireHomePositionCommand.setRequiredXYDistance(requiredXYDistance);
        acquireHomePositionCommand.setRequiredAltitudeDistance(requiredAltitudeDistance);
        acquireHomePositionCommand.setRequiredHeadingDistance(requiredHeadingDistance);
        acquireHomePositionCommand.setRequiredGPSGoodCount(requiredGPSGoodCount);
        acquireHomePositionCommand.setRequiredAltimeterGoodCount(requiredAltimeterGoodCount);
        // Send the command:
        return sendCommand(floidId, acquireHomePositionCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performPayloadCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performPayloadCommand(@RequestParam("floidId") int floidId,
                                                          @RequestParam("bay") int bay) {
        // Create the command:
        PayloadCommand payloadCommand = new PayloadCommand();
        // Set the parameters:
        payloadCommand.setBay(bay);
        // Send the command:
        return sendCommand(floidId, payloadCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performDebugCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performDebugCommand(@RequestParam("floidId") int floidId,
                                                        @RequestParam("deviceIndex") int deviceIndex,
                                                        @RequestParam("deviceOn") boolean deviceOn) {
        // Create the command:
        DebugCommand debugCommand = new DebugCommand();
        debugCommand.setDeviceIndex(deviceIndex);
        debugCommand.setDebugValue(deviceOn ? 1 : 0);
        // Send the command:
        return sendCommand(floidId, debugCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performDelayCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performDelayCommand(@RequestParam("floidId") int floidId,
                                                        @RequestParam("delayTime") int delayTime) {
        // Create the command:
        DelayCommand delayCommand = new DelayCommand();
        // Set the parameters:
        delayCommand.setDelayTime(delayTime);
        // Send the command:
        return sendCommand(floidId, delayCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performDeployParachuteCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performDeployParachuteCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        DeployParachuteCommand deployParachuteCommand = new DeployParachuteCommand();
        // Send the command:
        return sendCommand(floidId, deployParachuteCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performDesignateCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performDesignateCommand(@RequestParam("floidId") int floidId,
                                                            @RequestParam("latitude") double latitude,
                                                            @RequestParam("longitude") double longitude,
                                                            @RequestParam("height") double height) {
        // Create the command:
        DesignateCommand designateCommand = new DesignateCommand();
        designateCommand.setX(longitude);
        designateCommand.setY(latitude);
        designateCommand.setZ(height);
        designateCommand.setUsePin(false);
        designateCommand.setUsePinHeight(false);
        designateCommand.setPinName("");  // WE DO NOT USE PIN NAMES FOR THE DIRECT DESIGNATOR
        // Send the command:
        return sendCommand(floidId, designateCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performFlyToCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performFlyToCommand(@RequestParam("floidId") int floidId,
                                                        @RequestParam("latitude") double latitude,
                                                        @RequestParam("longitude") double longitude,
                                                        @RequestParam("flyToHome") boolean flyToHome) {
        // Create the command:
        FlyToCommand flyToCommand = new FlyToCommand();
        // Set the parameters:
        flyToCommand.setX(longitude);
        flyToCommand.setY(latitude);
        flyToCommand.setFlyToHome(flyToHome);
        // Send the command:
        return sendCommand(floidId, flyToCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performHeightToCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performHeightToCommand(@RequestParam("floidId") int floidId,
                                                           @RequestParam("absolute") boolean absolute,
                                                           @RequestParam("height") double height) {
        // Create the command:
        HeightToCommand heightToCommand = new HeightToCommand();
        heightToCommand.setZ(height);
        heightToCommand.setAbsolute(absolute);
        // Send the command:
        return sendCommand(floidId, heightToCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performHoverCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performHoverCommand(@RequestParam("floidId") int floidId,
                                                        @RequestParam("time") int time) {
        // Create the command:
        HoverCommand hoverCommand = new HoverCommand();
        hoverCommand.setHoverTime(time);
        // Send the command:
        return sendCommand(floidId, hoverCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performLandCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performLandCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        LandCommand landCommand = new LandCommand();
        // Send the command:
        return sendCommand(floidId, landCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performLiftOffCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performLiftOffCommand(@RequestParam("floidId") int floidId,
                                                          @RequestParam("absolute") boolean absolute,
                                                          @RequestParam("z") int z) {
        // Create the command:
        LiftOffCommand liftOffCommand = new LiftOffCommand();
        // Set the parameters:
        liftOffCommand.setZ(z);
        liftOffCommand.setAbsolute(absolute);
        // Send the command:
        return sendCommand(floidId, liftOffCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performNullCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performNullCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        NullCommand nullCommand = new NullCommand();
        // Send the command:
        return sendCommand(floidId, nullCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performPanTiltCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performPanTiltCommand(@RequestParam("floidId") int floidId,
                                                          @RequestParam("angleMode") boolean angleMode,
                                                          @RequestParam("latitude") double latitude,
                                                          @RequestParam("longitude") double longitude,
                                                          @RequestParam("height") double height,
                                                          @RequestParam("pan") double pan,
                                                          @RequestParam("tilt") double tilt) {
        // Create the command:
        PanTiltCommand panTiltCommand = new PanTiltCommand();
        panTiltCommand.setPinName("");  // Do not use pin name here
        panTiltCommand.setUseAngle(angleMode);
        if (angleMode) {
            panTiltCommand.setPan(pan);
            panTiltCommand.setTilt(tilt);
            panTiltCommand.setX(0.0);
            panTiltCommand.setY(0.0);
            panTiltCommand.setZ(0.0);
        } else {
            panTiltCommand.setPan(0);
            panTiltCommand.setTilt(0);
            panTiltCommand.setX(longitude);
            panTiltCommand.setY(latitude);
            panTiltCommand.setZ(height);
        }
        // Send the command:
        return sendCommand(floidId, panTiltCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performRetrieveLogsCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performRetrieveLogsCommand(@RequestParam("floidId") int floidId,
                                                        @RequestParam("logLimit") int logLimit) {
        // Create the command:
        RetrieveLogsCommand retrieveLogsCommand = new RetrieveLogsCommand();
        // Set the parameters:
        retrieveLogsCommand.setLogLimit(logLimit);
        // Send the command:
        return sendCommand(floidId, retrieveLogsCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performSetParametersCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performSetParametersCommand(@RequestParam("floidId") int floidId,
                                                                @RequestParam("droidParameters") String droidParameters,
                                                                @RequestParam("resetDroidParameters") boolean resetDroidParameters) {
        // Create the command:
        SetParametersCommand setParametersCommand = new SetParametersCommand();
        setParametersCommand.setHomePinName("UNDEFINED");
        setParametersCommand.setFirstPinIsHome(true);
        setParametersCommand.setDroidParameters(droidParameters);
        setParametersCommand.setResetDroidParameters(resetDroidParameters);
        // Send the command:
        return sendCommand(floidId, setParametersCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performShutDownHelisCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performShutDownHelisCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        ShutDownHelisCommand shutDownHelisCommand = new ShutDownHelisCommand();
        // Send the command:
        return sendCommand(floidId, shutDownHelisCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performSpeakerCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performSpeakerCommand(@RequestParam("floidId") int floidId,
                                                          @RequestParam("alert") boolean alert,
                                                          @RequestParam("mode") String mode,
                                                          @RequestParam("tts") String tts,
                                                          @RequestParam("pitch") double pitch,
                                                          @RequestParam("rate") double rate,
                                                          @RequestParam("fileName") String fileName,
                                                          @RequestParam("volume") double volume,
                                                          @RequestParam("speakerRepeat") int speakerRepeat,
                                                          @RequestParam("delay") int delay) {
        // Create the command:
        SpeakerCommand speakerCommand = new SpeakerCommand();
        speakerCommand.setAlert(alert);
        speakerCommand.setMode(mode);
        speakerCommand.setTts(tts);
        speakerCommand.setPitch(pitch);
        speakerCommand.setRate(rate);
        speakerCommand.setFileName(fileName);
        speakerCommand.setVolume(volume);
        speakerCommand.setSpeakerRepeat(speakerRepeat);
        speakerCommand.setDelay(delay);
        // Send the command:
        return sendCommand(floidId, speakerCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performSpeakerOnCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performSpeakerOnCommand(@RequestParam("floidId") int floidId,
                                                          @RequestParam("alert") boolean alert,
                                                          @RequestParam("mode") String mode,
                                                          @RequestParam("tts") String tts,
                                                          @RequestParam("pitch") double pitch,
                                                          @RequestParam("rate") double rate,
                                                          @RequestParam("fileName") String fileName,
                                                          @RequestParam("volume") double volume,
                                                          @RequestParam("speakerRepeat") int speakerRepeat,
                                                          @RequestParam("delay") int delay) {
        // Create the command:
        SpeakerOnCommand speakerCommand = new SpeakerOnCommand();
        speakerCommand.setAlert(alert);
        speakerCommand.setMode(mode);
        speakerCommand.setTts(tts);
        speakerCommand.setPitch(pitch);
        speakerCommand.setRate(rate);
        speakerCommand.setFileName(fileName);
        speakerCommand.setVolume(volume);
        speakerCommand.setSpeakerRepeat(speakerRepeat);
        speakerCommand.setDelay(delay);
        // Send the command:
        return sendCommand(floidId, speakerCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performSpeakerOffCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performSpeakerOffCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        SpeakerOffCommand speakerOffCommand = new SpeakerOffCommand();
        // Send the command:
        return sendCommand(floidId, speakerOffCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performStartUpHelisCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performStartUpHelisCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        StartUpHelisCommand startUpHelisCommand = new StartUpHelisCommand();
        // Send the command:
        return sendCommand(floidId, startUpHelisCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performStartVideoCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performStartVideoCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        StartVideoCommand startVideoCommand = new StartVideoCommand();
        // Send the command:
        return sendCommand(floidId, startVideoCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performStopDesignatingCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performStopDesignatingCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        StopDesignatingCommand stopDesignatingCommand = new StopDesignatingCommand();
        // Send the command:
        return sendCommand(floidId, stopDesignatingCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performStopVideoCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performStopVideoCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        StopVideoCommand stopVideoCommand = new StopVideoCommand();
        // Send the command:
        return sendCommand(floidId, stopVideoCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performPhotoStrobeCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performPhotoStrobeCommand(@RequestParam("floidId") int floidId,
                                                              @RequestParam("delay") int delay) {
        // Create the command:
        PhotoStrobeCommand photoStrobeCommand = new PhotoStrobeCommand();
        photoStrobeCommand.setDelay(delay);
        // Send the command:
        return sendCommand(floidId, photoStrobeCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performStopPhotoStrobeCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performStopPhotoStrobeCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        StopPhotoStrobeCommand stopPhotoStrobeCommand = new StopPhotoStrobeCommand();
        // Send the command:
        return sendCommand(floidId, stopPhotoStrobeCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performTakePhotoCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performTakePhotoCommand(@RequestParam("floidId") int floidId) {
        // Create the command:
        TakePhotoCommand takePhotoCommand = new TakePhotoCommand();
        // Send the command:
        return sendCommand(floidId, takePhotoCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/performTurnToCommand", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult performTurnToCommand(@RequestParam("floidId") int floidId,
                                                         @RequestParam("followMode") boolean followMode,
                                                         @RequestParam("heading") double heading) {
        // Create the command:
        TurnToCommand turnToCommand = new TurnToCommand();
        turnToCommand.setHeading(heading);
        turnToCommand.setFollowMode(followMode);
        // Send the command:
        return sendCommand(floidId, turnToCommand);
    }

    @CrossOrigin
    @RequestMapping(value="/createNewMission", method=RequestMethod.POST)
    @ResponseBody
    public Long createNewMission(@RequestParam("newMissionName") String newMissionName) {
        // Create a new mission and return the id:
        return FloidServerRunnable.createNewMission(newMissionName);
    }

    @CrossOrigin
    @RequestMapping(value="/duplicateMission", method=RequestMethod.POST)
    @ResponseBody
    public Long duplicateMission(@RequestParam("originalMissionId") Long originalMissionId,
                                 @RequestParam("newMissionName") String newMissionName) {
        // Duplicate a mission and return the id:
        return FloidServerRunnable.duplicateMission(originalMissionId, newMissionName);
    }
    @CrossOrigin
    @RequestMapping(value="/executeMission", method=RequestMethod.POST)
    @ResponseBody
    public FloidServerCommandResult executeMission(/*HttpServletRequest request */
                                                @RequestParam("floidId") int floidId,
                                                   @RequestParam("missionId") Long missionId,
                                                   @RequestParam("pinSetId") Long pinSetId) {
        try {
            logger.info("[floidId=" + floidId + "] Execute mission: " + missionId + " over pinSet: " + pinSetId);
            return FloidServerRunnable.executeMission(floidId, missionId, pinSetId);
        } catch (Exception e) {
            logger.error("Caught exception executing mission", e);
            FloidServerCommandResult floidServerCommandResult = new FloidServerCommandResult();
            floidServerCommandResult.setResultMessage(e.toString());
            return floidServerCommandResult;
        }
    }

    @CrossOrigin
    @RequestMapping(value="/deleteMission", method=RequestMethod.POST)
    @ResponseBody
    public boolean deleteMission(@RequestParam("missionId") Long missionId) {
        // Delete the mission:
        try {
            return FloidServerRunnable.deleteMission(missionId);
        } catch (Exception e) {
            logger.error("Caught exception deleting mission", e);
        }
        return false;
    }

    @CrossOrigin
    @RequestMapping(value="/createPinSet", method=RequestMethod.POST)
    @ResponseBody
    public Long createPinSet(@RequestParam("newPinSetName") String newPinSetName) {
        // Create a new mission and return the id:
        return FloidServerRunnable.createPinSet(newPinSetName);
    }

    @CrossOrigin
    @RequestMapping(value="/deletePinSet", method=RequestMethod.POST)
    @ResponseBody
    public boolean deletePinSet(@RequestParam("pinSetId") Long pinSetId) {
        try {
            return FloidServerRunnable.deletePinSet(pinSetId);
        } catch (Exception e) {
            logger.error("Caught exception deleting pin set", e);
        }
        return false;
    }

    @CrossOrigin
    @RequestMapping(value="/savePinSet", method=RequestMethod.POST)
    @ResponseBody
    public long savePinSet(@RequestParam("pinSetAlt") String pinSetAltJson) {
        try {
            return FloidServerRunnable.savePinSetJson(pinSetAltJson);
        } catch (Exception e) {
            logger.error("Caught exception saving pin set", e);
        }
        return FloidPinSet.NO_PIN_SET;
    }

    @CrossOrigin
    @RequestMapping(value="/importPinSet", method=RequestMethod.POST)
    @ResponseBody
    public long importPinSet(@RequestParam("pinSetAlt") String pinSetAltJson) {
        try {
            return FloidServerRunnable.importPinSet(pinSetAltJson);
        } catch (Exception e) {
            logger.error("Caught exception importing pin set", e);
        }
        return FloidPinSet.NO_PIN_SET;
    }

    @CrossOrigin
    @RequestMapping(value="/duplicatePinSet", method=RequestMethod.POST)
    @ResponseBody
    public Long duplicatePinSet(@RequestParam("originalPinSetId") Long originalPinSetId,
                                 @RequestParam("newPinSetName") String newPinSetName) {
        // Duplicate a pin set and return the id:
        return FloidServerRunnable.duplicatePinSet(originalPinSetId, newPinSetName);
    }

    @CrossOrigin
    @RequestMapping(value="/saveMission", method=RequestMethod.POST)
    @ResponseBody
    public long saveMission(@RequestParam("missionDocument") Document missionDocument) {
        try {
            return FloidServerRunnable.saveMission(missionDocument);
        } catch (Exception e) {
            logger.info(e.toString());
        }
        return DroidMission.NO_MISSION;
    }

    @CrossOrigin
    @RequestMapping(value="/saveMissionJSON", method=RequestMethod.POST)
    @ResponseBody
    public long saveMissionJSON(@RequestParam("mission") String mission) {
        try {
            return FloidServerRunnable.saveMissionJSON(mission);
        } catch (Exception e) {
            logger.info(e.toString());
        }
        return DroidMission.NO_MISSION;
    }

    @CrossOrigin
    @RequestMapping(value="/importMission", method=RequestMethod.POST)
    @ResponseBody
    public long importMission(@RequestParam("mission") String mission) {
        try {
            return FloidServerRunnable.importMission(mission);
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return DroidMission.NO_MISSION;
    }




    @CrossOrigin
    @RequestMapping(value="/deleteFloidId", method=RequestMethod.POST)
    @ResponseBody
    public boolean deletePinSet(@RequestParam("floidId") Integer floidId) {
        try {
            return FloidServerRunnable.deleteFloidId(floidId);
        } catch (Exception e) {
            logger.error("Caught exception deleting floid id", e);
        }
        return false;
    }


    /**
     * Log a command message for the remoting
     * @param floidId the floid id
     * @param droidInstruction the instruction
     * @param message the log message
     */
    private void logCommand(int floidId, DroidInstruction droidInstruction, String message) {
        logger.info("Floid: " + floidId + " <-- " + droidInstruction.getClass().getSimpleName() + " : " + message);
    }

    /**
     * Send a command to a floid
     * @param floidId the floid id
     * @param droidInstruction the instruction
     * @return the floid server command result
     */
    private FloidServerCommandResult sendCommand(int floidId, DroidInstruction droidInstruction) {
        // Log the start:
        logCommand(floidId, droidInstruction, FLOID_COMMAND_LOG_START);
        // Set up the floid server command result:
        FloidServerCommandResult floidServerCommandResult = new FloidServerCommandResult(floidId, droidInstruction);
        floidServerCommandResult.setRemotingSuccess(true);
        // Send the command:
        FloidServerControlServlet.sendInstruction(floidId, droidInstruction, floidServerCommandResult);
        // Log completion:
        logCommand(floidId, droidInstruction, FLOID_COMMAND_LOG_END);
        return floidServerCommandResult;
    }
}
