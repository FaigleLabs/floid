/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * FloidServer.java
*/
package com.faiglelabs.floid.floidserver;

import com.faiglelabs.floid.floidserver.security.JwtTokenUtil;
import com.faiglelabs.floid.floidserver.security.WebSecurityConfig;
import com.faiglelabs.floid.servertypes.commands.FloidServerCommandResult;
import com.faiglelabs.floid.servertypes.floidid.FloidId;
import com.faiglelabs.floid.servertypes.floidid.FloidIdAndState;
import com.faiglelabs.floid.servertypes.floidid.FloidUuid;
import com.faiglelabs.floid.servertypes.floidid.FloidUuidAndState;
import com.faiglelabs.floid.servertypes.geo.FloidGpsPoint;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The Floid server.
 */
public class FloidServer {
    private JwtTokenUtil jwtTokenUtil;
    public final String FLOID_APP_FILE_NAME = "app-full-release.apk";
    public final int FLOOD_APP_DOWNLOAD_ID = 1;
    private final Logger logger;
    private final int port;
    private final boolean runThreadDumper;
    private final String serverKeyStorePath;
    private final String serverKeyStorePasswordString;
    private final String serverCertificatePasswordString;
    private SSLServerSocket sslServerSocket = null;
    private FloidServerSocketRunnable floidServerSocketRunnable = null;
    private final SimpMessagingTemplate messagingTemplate;
    @SuppressWarnings("FieldCanBeLocal")
    private final long THREAD_DUMPER_EXECUTION_PERIOD = 10;

    /**
     * Creates a new instance of FloidServer
     *
     * @param iPort                            listen on this port
     * @param iRunThreadDumper                 run the thread dumper
     * @param iServerKeyStorePath              server key store path
     * @param iServerKeyStorePasswordString    server key store password
     * @param iServerCertificatePasswordString server certificate password
     */
    public FloidServer(int iPort,
                       boolean iRunThreadDumper,
                       String iServerKeyStorePath,
                       String iServerKeyStorePasswordString,
                       String iServerCertificatePasswordString,
                       SimpMessagingTemplate iMessagingTemplate,
                       JwtTokenUtil iJwtTokenUtil) {
        // Start up the logger:
        logger = LoggerFactory.getLogger(this.getClass().getName());
        port = iPort;
        runThreadDumper = iRunThreadDumper;
        serverKeyStorePath = iServerKeyStorePath;
        serverKeyStorePasswordString = iServerKeyStorePasswordString;
        serverCertificatePasswordString = iServerCertificatePasswordString;
        messagingTemplate = iMessagingTemplate;
        jwtTokenUtil = iJwtTokenUtil;

        // See here: http://www.movable-type.co.uk/scripts/latlong.html
        /*
            var φ2 = Math.asin( Math.sin(φ1)*Math.cos(d/R)+ Math.cos(φ1)*Math.sin(d/R)*Math.cos(bearing) );
            var λ2 = λ1 + Math.atan2(Math.sin(bearing)*Math.sin(d/R)*Math.cos(φ1), Math.cos(d/R)-Math.sin(φ1)*Math.sin(φ2));
         */

        // Note: Filter test code below:
        /*
        ArrayList<FloidGpsPoint> floidGpsPoints = new ArrayList<>();
        Random random = new Random();
        double lat = 34.00;  // Start lat
        double lng = -74.00; // Start Lng
        double bearing = 0.0;  // Compass degrees?
        double distance = 5.0; // Meters
        long time = 1000L; // One second
        double altitude = 0.0; // We do not use
        FloidGpsPoint currentFloidGpsPoint = new FloidGpsPoint(-1,"", time, lat, lng, altitude);
        // Add in first point
        floidGpsPoints.add(currentFloidGpsPoint);
        for(int i=1; i<150; ++i) {
            // Retrieve current lat/lng:
            lat = currentFloidGpsPoint.getLatitude();
            lng = currentFloidGpsPoint.getLongitude();
            // Adjust bearing
            bearing = bearing + (random.nextDouble() * 2.0) - 1.0;
            // Make positive:
            if(bearing < 0) {
                bearing += 360.0;
            }
            // But not too positive:
            bearing = (bearing % 360.0);
            // Adjust distance:
            distance = distance + (random.nextDouble() * 600.0) - 300.0;
            if(distance < 0) {
                distance = 5;  // Reset
            }
            // Adjust time:
            long deltaTime = (long)(random.nextDouble() * 1000.0);
            time = time + deltaTime;
            currentFloidGpsPoint = getPointAtBearingDistance(lat, lng, bearing, distance, time, altitude);
            floidGpsPoints.add(currentFloidGpsPoint);
        }
        FloidServerRunnable.filterGpsPoints(floidGpsPoints, 80, 100L, 200, 20.0, 0.1);
        FloidServerRunnable.filterGpsPoints(floidGpsPoints, 70, 100L, 200, 20.0, 0.1);
        FloidServerRunnable.filterGpsPoints(floidGpsPoints, 60, 100L, 200, 20.0, 0.1);
        */
        try {
            // Open and close a hibernate session to preload it:
            logger.info("Starting hibernate");
            FloidServerHibernateUtil.getSessionFactory().openSession().close();
            logger.info("Hibernate started");
        } catch (Exception e) {
            // Failed to start hibernate
            logger.error("Caught exception starting hibernate", e);
        }
    }

    @SuppressWarnings("unused")
    private static FloidGpsPoint getPointAtBearingDistance(double lat, double lng, double bearing, double distance, long time, double altitude) {
        /*
            // Note: φ = latitude in radians
            // Note: λ = longitude in radians
                var φ2 = Math.asin( Math.sin(φ1)*Math.cos(d/R)+ Math.cos(φ1)*Math.sin(d/R)*Math.cos(bearing) );
                var λ2 = λ1 + Math.atan2(Math.sin(bearing)*Math.sin(d/R)*Math.cos(φ1), Math.cos(d/R)-Math.sin(φ1)*Math.sin(φ2));
        */
        double earthRadius = 6371000; // Earths Radius approx
        double sphericalEarthDistance = distance / earthRadius;
        double latRad1 = Math.toRadians(lat);
        double lngRad1 = Math.toRadians(lng);
        double bearingRad = Math.toRadians(bearing);
        double cosSphericalEarthDistance = Math.cos(sphericalEarthDistance);
        double sinSphericalEarthDistance = Math.sin(sphericalEarthDistance);
        double cosLatRad1 = Math.cos(latRad1);
        double sinLatRad1 = Math.sin(latRad1);
        double cosBearingRad = Math.cos(bearingRad);
        double sinBearingRad = Math.sin(bearingRad);

        double latRad2 = Math.asin(sinLatRad1 * cosSphericalEarthDistance
                                 + cosLatRad1 * sinSphericalEarthDistance * cosBearingRad);
        double sinLatRad2 = Math.sin(latRad2);
        double lngRad2 = lngRad1 + Math.atan2(sinBearingRad * sinSphericalEarthDistance * cosLatRad1,
                                              cosSphericalEarthDistance - (sinLatRad1 * sinLatRad2));

        // Convert to degrees:
        double lat2 = Math.toDegrees(latRad2);
        double lng2 = Math.toDegrees(lngRad2);
        // Make return point:
        return new FloidGpsPoint(-1,"", time, lat2, lng2, altitude);
    }
    /**
     * Get floid id and state list
     *
     * @return the floid id and state list
     */
    public static ArrayList<FloidIdAndState> getFloidIdAndStateList() {
        return FloidIdHelper.getFloidIdAndStateList();
    }

    /**
     * Get the floid uuid list for this floid id
     *
     * @param floidId the floid id
     * @return the floid uuid list
     */
    public static ArrayList<FloidUuid> getFloidUuidList(int floidId) {
        return FloidIdHelper.getFloidUuidList(floidId);
    }

    /**
     * Get the floid uuid and state list for this floid id
     *
     * @param floidId the floid id
     * @return the floid uuid and state list
     */
    public static ArrayList<FloidUuidAndState> getFloidUuidAndStateList(int floidId) {
        return FloidIdHelper.getFloidUuidAndStateList(floidId);
    }

    /**
     * Get the floid server status
     *
     * @return FloidServerSocketRunnable.SERVER_OFF, FloidServerSocketRunnable.SERVER_ON or FloidServerSocketRunnable.SERVER_STOPPING
     */
    public String getStatus() {
        if (floidServerSocketRunnable != null) {
            return floidServerSocketRunnable.getStatus();
        } else {
            return "STOPPED";
        }
    }

    /**
     * Stop the floid server
     */
    public void stop() {
        logger.info("**** Stopping Floid Server *****");

        if (floidServerSocketRunnable != null) {
            floidServerSocketRunnable.setStop();
            floidServerSocketRunnable = null;
        }
        if (sslServerSocket != null) {
            try {
                logger.info("**** Closing Floid Server Socket *****");
                sslServerSocket.close();
            } catch (Exception e) {
                logger.error(e.toString());
            }
            sslServerSocket = null;
        }
    }

    /**
     * Send an instruction to the floid
     *
     * @param floidId          the floid id
     * @param droidInstruction the instruction
     * @param floidServerCommandResult the floid command result
     */
    public void sendInstruction(int floidId, DroidInstruction droidInstruction, FloidServerCommandResult floidServerCommandResult) {
        // Send this to the socket runnable if exists:
        if (floidServerSocketRunnable != null) {
            floidServerCommandResult.setServerSuccess(true);
            floidServerSocketRunnable.sendInstruction(floidId, droidInstruction, floidServerCommandResult);
        } else {
            floidServerCommandResult.setServletSuccess(false);
            floidServerCommandResult.setResultMessage(FloidServerRunnable.ERROR_MESSAGE_NO_SOCKET_RUNNABLE);
            floidServerCommandResult.setResult(FloidServerRunnable.ERROR_CODE_NO_SOCKET_RUNNABLE);
        }
    }

    /**
     * Get the list of floids
     *
     * @return the list of floids
     */
    public ArrayList<Integer> getFloidList() {
        ArrayList<Integer> floidIdList = new ArrayList<>();
        List<FloidId> floidIds = FloidIdHelper.getFloidIds();
        for (FloidId floidIdObject : floidIds) {
            floidIdList.add(floidIdObject.getFloidId());
        }
        return floidIdList;
    }

    /**
     * Start the floid server
     *
     * @return true if started
     */
    public boolean start() {
        logger.info("***** Starting Floid Server *****");
        // Create out objects - thread id, thread lists:
        LinkedList<FloidServerThreadInfo> currentFloidServerThreadInfoList = new LinkedList<>();
        LinkedList<FloidServerThreadInfo> completedFloidServerThreadInfoList = new LinkedList<>();
        if (runThreadDumper) {
            logger.info("Starting Floid Thread Dumper");
            // Create a thread dumper and start it running:
            final FloidServerThreadDumper floidServerThreadDumper = new FloidServerThreadDumper(currentFloidServerThreadInfoList, completedFloidServerThreadInfoList);
            final ScheduledExecutorService threadDumperExecutor = Executors.newSingleThreadScheduledExecutor();
            /* ScheduledFuture<?> threadDumperTask = */ threadDumperExecutor.scheduleAtFixedRate(() -> {
                try {
                    floidServerThreadDumper.run();
                } catch (Exception e) {
                    logger.error("Exception in floidServerThreadDumper: ", e);
                }
            }, 0, THREAD_DUMPER_EXECUTION_PERIOD, TimeUnit.SECONDS);
        }
        try {
            // NOTE: See http://www.herongyang.com/jdk/ssl_client_auth.html
            // NOTE: See http://shib.kuleuven.be/docs/ssl_commands.shtml for good docs on creating JKS keys
            // SSLContext protocols: TLS, SSL, SSLv3
            SSLContext sslContext = SSLContext.getInstance("TLS");
            logger.debug("            SSLContext class: " + sslContext.getClass());
            logger.debug("                    protocol: " + sslContext.getProtocol());
            logger.debug("                    provider: " + sslContext.getProvider());
            // SSLContext algorithms: SunX509
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            logger.debug("     KeyManagerFactory class: " + keyManagerFactory.getClass());
            logger.debug("                   algorithm: " + keyManagerFactory.getAlgorithm());
            logger.debug("                    provider: " + keyManagerFactory.getProvider());
            // KeyStore types: JKS
            char[] keyStorePassword = serverKeyStorePasswordString.toCharArray();
            char[] certificatePassword = serverCertificatePasswordString.toCharArray();
            // Set up necessary system properties:
            File jks;
            try {
                jks = getKeyStoreFile(serverKeyStorePath, "ssl_keystore", ".jks");
            } catch (Exception e) {
                logger.error("Caught Exception loading key store file: " + serverKeyStorePath);
                throw e;
            }
            logger.info("SERVER KEYSTORE PATH:" + serverKeyStorePath);
            logger.info("SERVER NEW FILE PATH:" + jks.getPath());
            System.setProperty("javax.net.ssl.trustStore", jks.getPath());
            System.setProperty("javax.net.ssl.trustStorePassword", serverKeyStorePasswordString);
            KeyStore keyStore = KeyStore.getInstance("JKS");
            // Load the key store:
            InputStream keyStoreInputStream = null;
            try {
                keyStoreInputStream = new FileInputStream(jks); // serverKeyStorePath);
                keyStore.load(keyStoreInputStream, keyStorePassword);
                keyStoreInputStream.close();
                keyStoreInputStream = null;
            } catch (Exception e) {
                logger.error("Exception reading keystore input stream", e);
            }
            if(keyStoreInputStream != null) {
                try {
                    keyStoreInputStream.close();
                } catch (Exception e) {
                    logger.error("Exception closing keystore input stream", e);
                }
            }
            logger.debug("              KeyStore class: " + keyStore.getClass());
            logger.debug("                        type: " + keyStore.getType());
            logger.debug("                    provider: " + keyStore.getProvider());
            logger.debug("                        size: " + keyStore.size());
            // Generating KeyManager list
            keyManagerFactory.init(keyStore, certificatePassword);
            KeyManager[] keyManagerList = keyManagerFactory.getKeyManagers();
            logger.debug("            KeyManager class: " + keyManagerList[0].getClass());
            logger.debug("              # key managers: " + keyManagerList.length);
            // Generating SSLServerSocketFactory
            sslContext.init(keyManagerList, null, null);
            SSLServerSocketFactory sslServerSocketFactory = sslContext.getServerSocketFactory();
            logger.debug("SSLServerSocketFactory class: " + sslServerSocketFactory.getClass());
            // Generating SSLServerSocket
            sslServerSocket = (SSLServerSocket) sslServerSocketFactory.createServerSocket(port);
            logger.debug("       SSLServerSocket class: " + sslServerSocket.getClass());
            logger.debug("                      string: " + sslServerSocket.toString());
            String[] supportedProtocols = sslServerSocket.getSupportedProtocols();
            for (String supportedProtocol : supportedProtocols) {
                logger.debug("          supported protocol: " + supportedProtocol);
            }
            String[] enabledProtocols = sslServerSocket.getEnabledProtocols();
            for (String enabledProtocol : enabledProtocols) {
                logger.debug("            enabled protocol: " + enabledProtocol);
            }
            String[] supportedCipherSuites = sslServerSocket.getSupportedCipherSuites();
            for (String supportedCipher : supportedCipherSuites) {
                logger.debug("          supported cipher suite: " + supportedCipher);
            }
            String[] enabledCipherSuites = sslServerSocket.getEnabledCipherSuites();
            for (String enabledCipherSuite : enabledCipherSuites) {
                logger.debug("            enabled cipher suite: " + enabledCipherSuite);
            }
            // Enable only TLSv1.3:
            sslServerSocket.setEnabledProtocols(new String[]{"TLSv1.3"});
            String[] finalEnabledProtocols = sslServerSocket.getEnabledProtocols();
            for (String finalEnabledProtocol : finalEnabledProtocols) {
                logger.debug("            enabled protocol: " + finalEnabledProtocol);
            }
            // Client must authenticate also:
            sslServerSocket.setNeedClientAuth(true);
            printServerSocketInfo(sslServerSocket);
            logger.debug("           Listening on port: " + port);
            floidServerSocketRunnable = new FloidServerSocketRunnable(currentFloidServerThreadInfoList, completedFloidServerThreadInfoList, sslServerSocket, messagingTemplate);
            Thread floidServerSocketRunnableThread = new Thread(floidServerSocketRunnable);
            floidServerSocketRunnableThread.start();
        } catch (Exception e) {
            logger.debug(e.toString());
            return false;
        }
        return true;
    }
    private static File getKeyStoreFile(String resourcePath, String fileName, String fileSuffix) throws IOException {
        ClassPathResource resource = new ClassPathResource(resourcePath);

        // Tomcat won't allow reading File from classpath so read as InputStream into temp File
        File jks = File.createTempFile(fileName, fileSuffix);
        InputStream inputStream = resource.getInputStream();
        try {
            FileUtils.copyInputStreamToFile(inputStream, jks);
        } finally {
            try {
                inputStream.close();
            } catch (Exception e) {
                // Do nothing.
            }
        }
        return jks;
    }

    public String createDownloadToken(String userName, int fileId) {
        try {
            String fileName;
            switch (fileId) {
                case FLOOD_APP_DOWNLOAD_ID:
                    fileName = FLOID_APP_FILE_NAME;
                    logger.info("creating download token for: " + FLOID_APP_FILE_NAME);
                    break;
                default:
                    logger.error("bad file id creating download token " + fileId);
                    return "";
            }
            ArrayList<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_" + WebSecurityConfig.FLOID_ROLE_FLOID_APP_DOWNLOAD));
            User user = new User(userName, "", authorities);
            return jwtTokenUtil.generateDownloadToken(user, fileName);
        } catch (Exception e) {
            logger.error("Exception creating download token ", e);
        }
        return "";
    }
    private void printServerSocketInfo(SSLServerSocket sslServerSocket) {
        logger.debug("Server socket class: " + sslServerSocket.getClass());
        logger.debug("            address: " + sslServerSocket.getInetAddress().toString());
        logger.debug("               port: " + sslServerSocket.getLocalPort());
        logger.debug("   need client auth: " + sslServerSocket.getNeedClientAuth());
        logger.debug("   want client auth: " + sslServerSocket.getWantClientAuth());
        logger.debug("    use client mode: " + sslServerSocket.getUseClientMode());
    }
}

