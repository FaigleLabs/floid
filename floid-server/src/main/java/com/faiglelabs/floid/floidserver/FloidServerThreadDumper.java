/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * FloidServerThreadDumper.java
*/

package com.faiglelabs.floid.floidserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Floid server thread dumper
 */
@SuppressWarnings("unused, WeakerAccess")
public class FloidServerThreadDumper implements Runnable {
    @SuppressWarnings("FieldCanBeLocal")
    private final List<FloidServerThreadInfo> currentPingServerThreadInfoList;
    @SuppressWarnings("FieldCanBeLocal")
    private final List<FloidServerThreadInfo> completedPingServerThreadInfoList;
    private Logger logger;

    /**
     * Creates a new instance of FloidServerThreadDumper
     *
     * @param iCurrentPingServerThreadInfoList   the list of current ping server thread information
     * @param iCompletedPingServerThreadInfoList the list of completed ping server thread information
     */
    public FloidServerThreadDumper(List<FloidServerThreadInfo> iCurrentPingServerThreadInfoList, List<FloidServerThreadInfo> iCompletedPingServerThreadInfoList) {
        logger = LoggerFactory.getLogger(this.getClass().getName());
        currentPingServerThreadInfoList = iCurrentPingServerThreadInfoList;
        completedPingServerThreadInfoList = iCompletedPingServerThreadInfoList;
    }

    public void run() {
        try {
            synchronized (currentPingServerThreadInfoList) {
                logger.info("====================================");
                logger.info("CURRENT THREADS: " + currentPingServerThreadInfoList.size());
                for (FloidServerThreadInfo pingServerThreadInfo : currentPingServerThreadInfoList) {
                    logger.info(pingServerThreadInfo.toString());
                }
                logger.info("COMPLETED THREADS: " + completedPingServerThreadInfoList.size());
                for (FloidServerThreadInfo pingServerThreadInfo : completedPingServerThreadInfoList) {
                    logger.info(pingServerThreadInfo.toString());
                }
                logger.info("====================================");
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }
}
