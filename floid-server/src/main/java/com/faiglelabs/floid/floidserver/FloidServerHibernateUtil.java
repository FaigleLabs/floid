/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.floidserver;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hibernate Utility class with convenient methods to get Session Factory object and to Hibernate an object
 */
class FloidServerHibernateUtil {
    private static final SessionFactory sessionFactory;
    private static final Logger logger = LoggerFactory.getLogger(Class.class.getName());

    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            Configuration configuration = new Configuration().configure();
            sessionFactory = configuration.configure().buildSessionFactory();
        } catch (Exception e) {
            // Log the exception. 
            logger.error("Initial SessionFactory creation failed.", e);
            throw new ExceptionInInitializerError(e);
        }
        logger.debug("Successful creation of FloidServerHibernateUtil");
    }

    /**
     * Return a hibernate session factory
     *
     * @return the hibernate session factory
     */
    static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Hibernate the object using the factory hibernate session
     *
     * @param object the object to hibernate
     */
    static void hibernateObject(Object object) {
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(object);
            transaction.commit();
            session.close();
        } catch (Exception e) {
            logger.error("hibernate object error: " + e.getMessage() + " " + e.toString());
            throw e;
        }
    }
}
