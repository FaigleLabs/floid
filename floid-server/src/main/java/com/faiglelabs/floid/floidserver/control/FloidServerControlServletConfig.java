package com.faiglelabs.floid.floidserver.control;

import com.faiglelabs.floid.floidserver.security.JwtTokenUtil;
import org.slf4j.Logger;
import org.springframework.messaging.simp.SimpMessagingTemplate;

/**
 * Configuration for Single FloidServerControlServlet
 */
@SuppressWarnings("WeakerAccess")
public class FloidServerControlServletConfig {
    private Logger logger = null;
    private int port = 0;
    private boolean runThreadDumper = false; // Note: Set from context
    private String serverKeyStorePath = null;
    private String serverKeyStorePasswordString = null;
    private String serverCertificatePasswordString = null;
    private SimpMessagingTemplate messagingTemplate = null;

    public JwtTokenUtil getJwtTokenUtil() {
        return jwtTokenUtil;
    }

    public void setJwtTokenUtil(JwtTokenUtil jwtTokenUtil) {
        this.jwtTokenUtil = jwtTokenUtil;
    }

    private JwtTokenUtil jwtTokenUtil = null;

    /**
     * Gets logger.
     *
     * @return the logger
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * Sets logger.
     *
     * @param logger the logger
     */
    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    /**
     * Gets port.
     *
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * Sets port.
     *
     * @param port the port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Is run thread dumper boolean.
     *
     * @return the boolean
     */
    public boolean isRunThreadDumper() {
        return runThreadDumper;
    }

    /**
     * Sets run thread dumper.
     *
     * @param runThreadDumper the run thread dumper
     */
    public void setRunThreadDumper(boolean runThreadDumper) {
        this.runThreadDumper = runThreadDumper;
    }

    /**
     * Gets server key store path.
     *
     * @return the server key store path
     */
    public String getServerKeyStorePath() {
        return serverKeyStorePath;
    }

    /**
     * Sets server key store path.
     *
     * @param serverKeyStorePath the server key store path
     */
    public void setServerKeyStorePath(String serverKeyStorePath) {
        this.serverKeyStorePath = serverKeyStorePath;
    }

    /**
     * Gets server key store password string.
     *
     * @return the server key store password string
     */
    public String getServerKeyStorePasswordString() {
        return serverKeyStorePasswordString;
    }

    /**
     * Sets server key store password string.
     *
     * @param serverKeyStorePasswordString the server key store password string
     */
    public void setServerKeyStorePasswordString(String serverKeyStorePasswordString) {
        this.serverKeyStorePasswordString = serverKeyStorePasswordString;
    }

    /**
     * Gets server certificate password string.
     *
     * @return the server certificate password string
     */
    public String getServerCertificatePasswordString() {
        return serverCertificatePasswordString;
    }

    /**
     * Sets server certificate password string.
     *
     * @param serverCertificatePasswordString the server certificate password string
     */
    public void setServerCertificatePasswordString(String serverCertificatePasswordString) {
        this.serverCertificatePasswordString = serverCertificatePasswordString;
    }

    public SimpMessagingTemplate getMessagingTemplate() {
        return messagingTemplate;
    }

    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }
}
