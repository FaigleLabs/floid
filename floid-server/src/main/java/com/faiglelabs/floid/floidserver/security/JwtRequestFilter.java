package com.faiglelabs.floid.floidserver.security;

import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;
    private final  Logger logger = LoggerFactory.getLogger(JwtRequestFilter.class.getName());
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    private final static String JWT_URI_TOKEN_PARAMETER = "token";

    public class DownloadInformation {
        UserDetails userDetails;
        String fileName;
    }

    private boolean requestIsValidDownload(HttpServletRequest request, DownloadInformation downloadInformation) {
        try {
            // 1. See if there is a token in the request
            String token = request.getParameter(JWT_URI_TOKEN_PARAMETER);
            if(token!=null) {
                // Valid?
                if(jwtTokenUtil.validateDownloadToken(token, downloadInformation)) {
                    logger.info("Request was valid download " + downloadInformation.fileName + " " + downloadInformation.userDetails.getUsername());
                    logger.info("Authorities:");
                    for(GrantedAuthority grantedAuthority:downloadInformation.userDetails.getAuthorities()) {
                        logger.info("  " + grantedAuthority.getAuthority());
                    }
                    return true;
                }
            }
         } catch (Exception e) {
            logger.error("Exception in requestIsValidDownload", e);
        }
        return false;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull FilterChain chain)
            throws ServletException, IOException {
        StringBuilder logResult = new StringBuilder(request.getRequestURI());
        int logLevel = 0;
        try {
            DownloadInformation downloadInformation = new DownloadInformation();
            if(requestIsValidDownload(request, downloadInformation)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        downloadInformation.userDetails, null, downloadInformation.userDetails.getAuthorities());
                usernamePasswordAuthenticationToken
                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                // After setting the Authentication in the context, we specify
                // that the current user is authenticated. So it passes the
                // Spring Security Configurations successfully.
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            } else {
                final String requestTokenHeader = request.getHeader("Authorization");
                String username = null;
                String jwtToken = null;

                // JWT Token is in the form "Bearer token". Remove Bearer word and get
                // only the Token:
                if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
                    logResult.append(" Bearer: ");
                    jwtToken = requestTokenHeader.substring(7);
                    try {
                        username = jwtTokenUtil.getUsernameFromToken(jwtToken);
                        if(username!=null) {
                            logResult.append("[").append(username).append(']');
                        } else {
                            logLevel = 1;
                            logResult.append("(failed)");
                        }
                    } catch (IllegalArgumentException e) {
                        logLevel = 1;
                        logResult.append("(failed)");
                    } catch (ExpiredJwtException e) {
                        logLevel = 1;
                        logResult.append("(expired)");
                    }
                } else {
                    logResult.append(" (no bearer)");
                }
                // Once we get the token validate it.
                if (username != null) {
                    if (SecurityContextHolder.getContext().getAuthentication() == null) {
                        UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);
                        // if token is valid configure Spring Security to manually set
                        // authentication
                        if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
                            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                                    userDetails, null, userDetails.getAuthorities());
                            usernamePasswordAuthenticationToken
                                    .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                            // After setting the Authentication in the context, we specify
                            // that the current user is authenticated. So it passes the
                            // Spring Security Configurations successfully.
                            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                            logResult.append(" (validated)");
                        } else {
                            logResult.append(" (fail)");
                            logLevel = 1;
                        }
                    } else {
                        logResult.append(" (already validated)");
                    }
                }
            }
        } catch(Exception e) {
            logResult.append(" Exception: ").append(e);
            logLevel=2;
        }
        if (logLevel == 0) {
            logger.info(logResult.toString());
        } else if (logLevel == 1 ){
            logger.warn(logResult.toString());
        } else {
            logger.error(logResult.toString());
        }
        chain.doFilter(request, response);
    }
}
