/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.floidserver.elevation;

import com.faiglelabs.floid.utils.DistanceUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.geotools.coverage.grid.GridCoordinates2D;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridGeometry2D;
import org.geotools.gce.geotiff.GeoTiffFormat;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.geotools.util.factory.Hints;
import org.json.JSONArray;
import org.json.JSONObject;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.awt.image.Raster;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * The Elevation REST servlet.
 */
@CrossOrigin
@RestController
@RequestMapping("/elevation")
public class FloidServerElevationServlet {
    private final static String TILE_INDEX_FILENAME = "index.csv";
    @Value("${elevations.location}")
    private String location;
    private final static Logger logger = LoggerFactory.getLogger(FloidServerElevationServlet.class.getName());
    private static List<TileInfo> tileInfoList;
    private static final double ERROR_ELEVATION = -32000;

    @PostConstruct
    public void init() {
        logger.info("FloidServerElevationServlet - Initializing");
        // Read tile list:
        tileInfoList = readTilesDirectory(location);
        logger.info("FloidServerElevationServlet - complete");
    }

    @CrossOrigin
    @RequestMapping(value = "/lookup", method = RequestMethod.POST)
    @ResponseBody
    public String lookup(@RequestParam("locations") String locations) {
        try {
            // Parse the json object:
            JSONObject locationsJson = new JSONObject(locations);
            JSONArray locationsJsonArray = locationsJson.getJSONArray("locations");
            StringBuilder jsonStringBuilder = new StringBuilder();
            boolean first = true;
            jsonStringBuilder.append("{\"results\":[");
            TileInfo tileInfo = null;
            ElevationTile elevationTile = new ElevationTile();
            for (int i = 0; i < locationsJsonArray.length(); ++i) {
                JSONObject locationJson = (JSONObject) (locationsJsonArray.get(i));
                double latitude = locationJson.getDouble("latitude");
                double longitude = locationJson.getDouble("longitude");
//                logger.info("Location: " + latitude + ", " + longitude);
                tileInfo = findTile(tileInfoList, latitude, longitude, tileInfo, elevationTile);
                double lat2 = latitude;
                double lng2 = longitude;
                if(tileInfo==null) {
                    lat2 = latitude + 0.0007;
                    lng2 = longitude + 0.0007;
                    // Move the point just a little bit and try again if there was a gap in the elevation map:
                    tileInfo = findTile(tileInfoList, lat2, lng2, tileInfo, elevationTile);
                }
                if(tileInfo==null) {
                    lat2 = latitude + 0.0007;
                    lng2 = longitude - 0.0007;
                    // Move the point just a little bit and try again if there was a gap in the elevation map:
                    tileInfo = findTile(tileInfoList, latitude + .0007, longitude - .0007, tileInfo, elevationTile);
                }
                if(tileInfo==null) {
                    lat2 = latitude - 0.0007;
                    lng2 = longitude + 0.0007;
                    // Move the point just a little bit and try again if there was a gap in the elevation map:
                    tileInfo = findTile(tileInfoList, latitude - .0007, longitude + .0007, tileInfo, elevationTile);
                }
                if(tileInfo==null) {
                    lat2 = latitude - 0.0007;
                    lng2 = longitude - 0.0007;
                    // Move the point just a little bit and try again if there was a gap in the elevation map:
                    tileInfo = findTile(tileInfoList, latitude - .0007, longitude - .0007, tileInfo, elevationTile);
                }
                if (tileInfo != null) {
//                    logger.info("Found tile: " + tileInfo.fileName);
                    double elevation = lookupElevation(lat2, lng2, elevationTile);
                    if (!first) jsonStringBuilder.append(", ");
                    jsonStringBuilder.append("{ \"latitude\": ").append(latitude).append(", ");
                    jsonStringBuilder.append("\"longitude\": ").append(longitude).append(", ");
                    jsonStringBuilder.append("\"elevation\": ").append(elevation).append(" }");
                    first = false;
                } else {
                    logger.error("Did not find tile at: " + latitude + ", " + longitude);
                    // findTileDistances(tileInfoList, latitude, longitude);
                    if (!first) jsonStringBuilder.append(", ");
                    jsonStringBuilder.append("{ \"latitude\": ").append(latitude).append(", ");
                    jsonStringBuilder.append("\"longitude\": ").append(longitude).append(", ");
                    jsonStringBuilder.append("\"elevation\": ").append(ERROR_ELEVATION).append(" }");
                    first = false;
                }
            }
            jsonStringBuilder.append(" ] }");
//            logger.info("Elevation Results: " + jsonStringBuilder);
            return jsonStringBuilder.toString();
        } catch (Exception e) {
            logger.error(e.toString() + " " + ExceptionUtils.getStackTrace(e));
            return "";
        }
    }

    static void findTileDistances(List<TileInfo> tileInfoList, double latitude, double longitude) {
        AtomicReference<Double> totalMinDistance = new AtomicReference<>(Double.MAX_VALUE);
        AtomicReference<Double> totalMinDistance2 = new AtomicReference<>(Double.MAX_VALUE);
        AtomicReference<Double> totalMinDistance3 = new AtomicReference<>(Double.MAX_VALUE);
        AtomicReference<Double> totalMinDistance4 = new AtomicReference<>(Double.MAX_VALUE);
        tileInfoList.forEach(tileInfo -> {
            if(withinTile(tileInfo, latitude, longitude)) {
                logger.error(" Found within tile: [" + tileInfo.latMin + ", " + tileInfo.lngMax + "]->[" + tileInfo.latMax + ", " + tileInfo.lngMax + "] " + latitude + ", " + longitude);
                totalMinDistance.set(0.0);
                totalMinDistance2.set(0.0);
                totalMinDistance3.set(0.0);
                totalMinDistance4.set(0.0);
            } else {
                double minDistance = DistanceUtils.calcLatLngDistance(
                        tileInfo.latMin, tileInfo.lngMin,
                        latitude, longitude);
                double testDistance = DistanceUtils.calcLatLngDistance(
                        tileInfo.latMax, tileInfo.lngMin,
                        latitude, longitude);
                if(testDistance < minDistance) {
                    minDistance = testDistance;
                }
                testDistance = DistanceUtils.calcLatLngDistance(
                        tileInfo.latMin, tileInfo.lngMax,
                        latitude, longitude);
                if(testDistance < minDistance) {
                    minDistance = testDistance;
                }
                testDistance = DistanceUtils.calcLatLngDistance(
                        tileInfo.latMax, tileInfo.lngMax,
                        latitude, longitude);
                if(testDistance < minDistance) {
                    minDistance = testDistance;
                }
                if(minDistance < totalMinDistance.get()) {
                    totalMinDistance4.set(totalMinDistance3.get());
                    totalMinDistance3.set(totalMinDistance2.get());
                    totalMinDistance2.set(totalMinDistance.get());
                    totalMinDistance.set(minDistance);
                }
                logger.error("Min Distance to tile: " + minDistance + " [" + tileInfo.latMin + ", " + tileInfo.lngMin + "]->[" + tileInfo.latMax + ", " + tileInfo.lngMax + "] " + latitude + ", " + longitude);
            }
        });
        logger.error("Total Min Distance: " + totalMinDistance.get() + " " + totalMinDistance2.get() + " " + totalMinDistance3.get() + " " + totalMinDistance4.get() + " ");
    }
    static class TileInfo {
        TileInfo(String fileName,
                 double latMin,
                 double lngMin,
                 double latMax,
                 double lngMax
        ) {
            this.fileName = fileName;
            this.latMin = latMin;
            this.lngMin = lngMin;
            this.latMax = latMax;
            this.lngMax = lngMax;
        }

        String fileName;
        double latMin;
        double lngMin;
        double latMax;
        double lngMax;
    }

    static TileInfo findTile(List<TileInfo> tileInfoList, double lat, double lng, TileInfo currentTile, ElevationTile currentElevationTile) {
        // First check current tile if any:
        if (currentTile != null) {
            if (withinTile(currentTile, lat, lng)) {
                // logger.info("Same as previous tile");
                return currentTile;
            }
        }
        for (TileInfo tileInfo : tileInfoList) {
            if (withinTile(tileInfo, lat, lng)) {
                if(!getTile(tileInfo.fileName, currentElevationTile)) return null;
                return tileInfo;
            }
        }
        return null;
    }

    static boolean withinTile(TileInfo tileInfo, double lat, double lng) {
        return (lat >= tileInfo.latMin)
                && (lat <= tileInfo.latMax)
                && (lng >= tileInfo.lngMin)
                && (lng <= tileInfo.lngMax);
    }

    static List<TileInfo> readTilesDirectory(String path) {
        logger.info("readTilesDirectory - path: " + path);
        List<TileInfo> tileInfoList = new ArrayList<>();
        BufferedReader indexReader;
        boolean indexGood = true;
        try {
            indexReader = new BufferedReader(new FileReader(path + "/" + TILE_INDEX_FILENAME));
            String indexLine = indexReader.readLine();
            while (indexLine != null) {
                String[] indexLineSplit = indexLine.split(",");
                if (indexLineSplit.length != 5) {
                    indexGood = false;
                    break;
                }
                TileInfo tileInfo = new TileInfo(indexLineSplit[0],
                        Double.parseDouble(indexLineSplit[1]),
                        Double.parseDouble(indexLineSplit[2]),
                        Double.parseDouble(indexLineSplit[3]),
                        Double.parseDouble(indexLineSplit[4])
                );
                tileInfoList.add(tileInfo);
                indexLine = indexReader.readLine();
            }
            indexReader.close();
        } catch (FileNotFoundException fileNotFoundException) {
            logger.info("No tile index file found - indexing...");
            indexGood = false;
        } catch (Exception e) {
            indexGood = false;
            e.printStackTrace();
            logger.info("Error in tile index file found - re-indexing...");
        }
        if (indexGood) {
            return tileInfoList;
        }
        try {
            // Reset list:
            tileInfoList = new ArrayList<>();
            File tileDirectoryFile = new File(path);
            final File[] tileFiles = tileDirectoryFile.listFiles();
            int fileNumber = 0;
            if (tileFiles != null) {
                for (File tileFile : tileFiles) {
                    try {
                        if (tileFile.getName().endsWith(".tif")) {
                            try {
                                ElevationTile elevationTile = new ElevationTile();
                                if (readElevationTile(tileFile.getCanonicalPath(), elevationTile)) {
                                    double latMin = elevationTile.coverage2D.getEnvelope2D().getMinimum(1);
                                    double latMax = elevationTile.coverage2D.getEnvelope2D().getMaximum(1);
                                    double lngMin = elevationTile.coverage2D.getEnvelope2D().getMinimum(0);
                                    double lngMax = elevationTile.coverage2D.getEnvelope2D().getMaximum(0);
                                    logger.info(++fileNumber +
                                            ". " +
                                            tileFile.getName() +
                                            " " +
                                            latMin +
                                            "," +
                                            lngMin +
                                            " -> " +
                                            latMax +
                                            "," +
                                            lngMax);
                                    tileInfoList.add(new TileInfo(tileFile.getCanonicalPath(), latMin, lngMin, latMax, lngMax));
                                }
                            } catch (Exception e) {
                                logger.error(String.format("GeoTIFF error processing  %s", tileFile.getName()));
                            }
                        }
                    } catch (Exception e) {
                        logger.error(e.toString() + " " + ExceptionUtils.getStackTrace(e));
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.toString() + " " + ExceptionUtils.getStackTrace(e));
        }
        // OK, now write out the tile index:
        BufferedWriter indexWriter;
        try {
            indexWriter = new BufferedWriter(new FileWriter(path + "/" + TILE_INDEX_FILENAME));
            for (TileInfo tileInfo : tileInfoList) {
                indexWriter.append(tileInfo.fileName).append(',')
                        .append(Double.toString(tileInfo.latMin)).append(',')
                        .append(Double.toString(tileInfo.lngMin)).append(',')
                        .append(Double.toString(tileInfo.latMax)).append(',')
                        .append(Double.toString(tileInfo.lngMax)).append('\n');
            }
            indexWriter.flush();
            indexWriter.close();
        } catch (Exception e) {
            logger.error(e.toString() + " " + ExceptionUtils.getStackTrace(e));
        }
        return tileInfoList;
    }

    static class ElevationTile {
        String pathToGeoTiffFile;
        GridCoverage2D coverage2D;
        Raster tiffRaster;
        GridGeometry2D gg;

    }

    static boolean readElevationTile(String pathToGeoTiffFile, ElevationTile elevationTile) {
        try {
            Hints forceLongLat = new Hints(Hints.FORCE_LONGITUDE_FIRST_AXIS_ORDER, Boolean.TRUE);
            GeoTiffFormat format = new GeoTiffFormat();
            GeoTiffReader reader = format.getReader(pathToGeoTiffFile, forceLongLat);
            GridCoverage2D coverage2D = reader.read(null);
            // logger.info("Elevation model CRS is: {}", coverage2D.getCoordinateReferenceSystem2D());
            elevationTile.pathToGeoTiffFile = pathToGeoTiffFile;
            elevationTile.coverage2D = coverage2D;
            elevationTile.tiffRaster = coverage2D.getRenderedImage().getData();
            elevationTile.gg = coverage2D.getGridGeometry();
            return true;
        } catch (Exception e) {
            logger.error("readElevationTile: exception " + e.toString());
            elevationTile.pathToGeoTiffFile = null;
            elevationTile.coverage2D = null;
            elevationTile.tiffRaster = null;
            elevationTile.gg = null;
        }
        return false;
    }

    static double lookupElevation(double lat, double lng, ElevationTile elevationTile) {
        try {
            CoordinateReferenceSystem wgs84 = DefaultGeographicCRS.WGS84;
            DirectPosition2D posWorld = new DirectPosition2D(wgs84, lng, lat); // longitude supplied first
            GridCoordinates2D posGrid = elevationTile.gg.worldToGrid(posWorld);
            // sample tiff data with at pixel coordinate
            double[] rasterData = new double[1];
            elevationTile.tiffRaster.getPixel(posGrid.x, posGrid.y, rasterData);
            double elevation = rasterData[0];
            logger.info(String.format("GeoTIFF data at %f, %f: %f", lat, lng, elevation));
            return elevation;
        } catch (Exception e) {
            logger.error(String.format("GeoTIFF error at %f, %f", lat, lng), e);
        }
        return ERROR_ELEVATION;
    }


    private static final Object cacheSynchronizer = new Object();
    private static final List<ElevationTile> cachedTiles = new ArrayList<>();
    private static final int CACHE_SIZE = 10;

    private static boolean getTile(String tileFilename, ElevationTile elevationTile) {
        if (getTileFromCache(tileFilename, elevationTile)) return true;
        if (readElevationTile(tileFilename, elevationTile)) {
            putTileInCache(elevationTile);
            return true;
        }
        return false;
    }

    private static boolean getTileFromCache(String tileFilename, ElevationTile elevationTile) {
        synchronized (cacheSynchronizer) {
            for (ElevationTile cachedElevationTile : cachedTiles) {
                if (cachedElevationTile.pathToGeoTiffFile.equals(tileFilename)) {
                    // Found the tile:
                    elevationTile.pathToGeoTiffFile = cachedElevationTile.pathToGeoTiffFile;
                    elevationTile.tiffRaster = cachedElevationTile.tiffRaster;
                    elevationTile.coverage2D = cachedElevationTile.coverage2D;
                    elevationTile.gg = cachedElevationTile.gg;
                    return true;
                }
            }
        }
        return false;
    }

    private static void putTileInCache(ElevationTile elevationTile) {
        ElevationTile insertElevationTile = new ElevationTile();
        insertElevationTile.pathToGeoTiffFile = elevationTile.pathToGeoTiffFile;
        insertElevationTile.tiffRaster = elevationTile.tiffRaster;
        insertElevationTile.coverage2D = elevationTile.coverage2D;
        insertElevationTile.gg = elevationTile.gg;
        synchronized (cacheSynchronizer) {
            if (cachedTiles.size() == CACHE_SIZE) {
                // Remove the one at the end:
                cachedTiles.remove(CACHE_SIZE - 1);
            }
            // Add this one at zero:
            cachedTiles.add(0, insertElevationTile);
        }
    }
}
