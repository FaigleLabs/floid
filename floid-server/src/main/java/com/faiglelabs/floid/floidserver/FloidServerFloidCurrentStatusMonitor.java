package com.faiglelabs.floid.floidserver;

import com.faiglelabs.floid.servertypes.floidid.FloidId;
import com.faiglelabs.floid.servertypes.floidid.FloidIdAndState;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class FloidServerFloidCurrentStatusMonitor {
    static class FloidIdListAndMap {
        ArrayList<FloidIdAndState> floidIdAndStateList;
        HashMap<Integer, FloidIdAndState> floidIdAndStateMap;
        FloidIdListAndMap(ArrayList<FloidIdAndState> floidIdAndStateList,
                          HashMap<Integer, FloidIdAndState> floidIdAndStateMap) {
            this.floidIdAndStateList = floidIdAndStateList;
            this.floidIdAndStateMap = floidIdAndStateMap;
        }
    }
    private final static Object statusSync = new Object();
    private static ArrayList<FloidIdAndState> floidIdAndStateList = new ArrayList<>();
    private static HashMap<Integer, FloidIdAndState> floidIdAndStateMap = new HashMap<>();

    private final static Logger logger = LoggerFactory.getLogger(FloidServerFloidCurrentStatusMonitor.class.getName());

    @PostConstruct
    private void init() {
        FloidIdListAndMap floidIdListAndMap = retrieveFloidIdAndStateListAndMap();
        if(floidIdListAndMap!=null) {
                floidIdAndStateList = floidIdListAndMap.floidIdAndStateList;
                floidIdAndStateMap = floidIdListAndMap.floidIdAndStateMap;
        }
    }

    @Scheduled(fixedDelay=10000, initialDelay = 10000)
    public void work() {
        update();
    }
    public static void update() {
        FloidIdListAndMap floidIdListAndMap = retrieveFloidIdAndStateListAndMap();
        if(floidIdListAndMap!=null) {
            synchronized (statusSync) {
                floidIdAndStateList = floidIdListAndMap.floidIdAndStateList;
                floidIdAndStateMap = floidIdListAndMap.floidIdAndStateMap;
            }
        }
    }

    private static FloidIdListAndMap retrieveFloidIdAndStateListAndMap() {
        logger.debug("Floid Cache: Retrieving id and state list");
        // Gets a list of floid id's sorted by lastUpdateTime
        ArrayList<FloidIdAndState> floidIdAndStateList = new ArrayList<>();
        HashMap<Integer, FloidIdAndState> floidIdAndStateMap = new HashMap<>();
        FloidIdListAndMap floidIdAndStateListAndMap = new FloidIdListAndMap(floidIdAndStateList, floidIdAndStateMap);
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.createQuery("select new FloidId(floidId, floidIdCreateTime, floidIdUpdateTime) from FloidId order by floidIdUpdateTime desc");
            @SuppressWarnings("unchecked")
            List<FloidId> floidIdList = (List<FloidId>) query.list();
            for (FloidId floidId : floidIdList) {
                boolean updateFloidId = true;
                // Update or not?
                FloidIdAndState mappedFloidIdAndState = FloidServerFloidCurrentStatusMonitor.floidIdAndStateMap.get(floidId.getFloidId());
                if(mappedFloidIdAndState!=null){
                    if(mappedFloidIdAndState.getFloidIdUpdateTime() == floidId.getFloidIdUpdateTime()) {
                        // Already in the map no need to update:
                        updateFloidId = false;
                        floidIdAndStateList.add(mappedFloidIdAndState);
                        floidIdAndStateMap.put(floidId.getFloidId(), mappedFloidIdAndState);
                    }
                }
                if(updateFloidId) {
                    Query floidStatusQuery = session.createQuery("select floidId from FloidStatus where floidId=" + floidId.getFloidId());
                    floidStatusQuery.setMaxResults(1);
                    if (floidStatusQuery.list().size() > 0) {
                        // Has floid status:
                        FloidIdAndState floidIdAndState = new FloidIdAndState(floidId, true);
                        floidIdAndStateList.add(floidIdAndState);
                        floidIdAndStateMap.put(floidId.getFloidId(), floidIdAndState);
                    } else {
                        // No floid status:
                        FloidIdAndState floidIdAndState = new FloidIdAndState(floidId, false);
                        floidIdAndStateList.add(floidIdAndState);
                        floidIdAndStateMap.put(floidId.getFloidId(), floidIdAndState);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("getFloidIds: " + e.getMessage() + " " + e.toString());
        } finally {
            session.close();
        }
        return floidIdAndStateListAndMap;
    }

    public static ArrayList<FloidIdAndState> getFloidIdAndStateList() {
        synchronized (statusSync) {
            return floidIdAndStateList;
        }
    }
}
