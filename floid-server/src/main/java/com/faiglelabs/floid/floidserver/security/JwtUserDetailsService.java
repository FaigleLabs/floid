package com.faiglelabs.floid.floidserver.security;

import com.faiglelabs.floid.floidserver.FloidUserMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private final static Logger logger = LoggerFactory.getLogger(JwtUserDetailsService.class.getName());

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User floidUser = FloidUserMonitor.getFloidUser(username);
            if(floidUser==null) {
                logger.warn("User Not Found: " + username);
            }
            return floidUser;
        } catch (Exception e) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }
}
