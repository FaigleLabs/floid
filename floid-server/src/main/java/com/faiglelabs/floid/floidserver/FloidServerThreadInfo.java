/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

/*
 * FloidServerThreadInfo.java
*/

package com.faiglelabs.floid.floidserver;

import java.time.Instant;

/**
 * The type Floid server thread info.
 */
class FloidServerThreadInfo {
    /**
     * Status type Initialized
     */
    public static final String STATUS_INITIALIZED = "Initialized";
    /**
     * Status type Running
     */
    public static final String STATUS_RUNNING = "Running";
    /**
     * Status type Stopping
     */
    public static final String STATUS_STOPPING = "Stopping";

    /**
     * Status type Finalized
     */
    public static final String STATUS_FINALIZED = "Finalized";
    /**
     * Ping type unknown
     */
    @SuppressWarnings("WeakerAccess")
    public static final String PING_TYPE_UNKNOWN = "Unknown";
    /**
     * Ping type floid
     */
    @SuppressWarnings("WeakerAccess")
    public static final String PING_TYPE_FLOID = "Floid";
    private final int threadID;
    private String pingType = PING_TYPE_UNKNOWN;
    private final long startTimestamp;
    private long endTimestamp = 0L;
    private String status;
    private int numberOfPings = 0;
    private FloidServerRunnable floidServerRunnable = null;
    private Thread floidServerThread = null;

    /**
     * Creates a new instance of FloidServerThreadInfo
     *
     * @param iThreadID       the thread id
     * @param iStartTimestamp the starting timestamp
     */
    FloidServerThreadInfo(int iThreadID, long iStartTimestamp) {
        threadID = iThreadID;
        startTimestamp = iStartTimestamp;
        status = STATUS_INITIALIZED;
    }

    /**
     * Gets floid server runnable.
     *
     * @return the floid server runnable
     */
    FloidServerRunnable getFloidServerRunnable() {
        return floidServerRunnable;
    }

    /**
     * Sets floid server runnable.
     *
     * @param floidServerRunnable the floid server runnable
     */
    void setFloidServerRunnable(FloidServerRunnable floidServerRunnable) {
        this.floidServerRunnable = floidServerRunnable;
    }

    /**
     * Sets ping type.
     *
     * @param iPingType the ping type
     */
    @SuppressWarnings({"unused", "SameParameterValue"})
    void setPingType(String iPingType) {
        pingType = iPingType;
    }

    /**
     * Sets status.
     *
     * @param iStatus the status
     */
    void setStatus(String iStatus) {
        status = iStatus;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    @SuppressWarnings("unused")
    String getPingType() {
        return pingType;
    }

    /**
     * Add ping.
     */
    @SuppressWarnings("unused")
    void addPing() {
        ++numberOfPings;
    }

    /**
     * Gets start timestamp.
     *
     * @return the start timestamp
     */
    @SuppressWarnings("unused")
    long getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * Gets end timestamp.
     *
     * @return the end timestamp
     */
    @SuppressWarnings("unused")
    long getEndTimestamp() {
        return endTimestamp;
    }

    /**
     * Sets end timestamp.
     *
     * @param iEndTimestamp the end timestamp
     */
    @SuppressWarnings("SameParameterValue")
    void setEndTimestamp(long iEndTimestamp) {
        endTimestamp = iEndTimestamp;
    }

    /**
     * Get the floid server thread
     * @return the floid server thread
     */
    @SuppressWarnings("WeakerAccess")
    public Thread getFloidServerThread() {
        return floidServerThread;
    }

    /**
     * Set the floid server thread
     * @param floidServerThread the new floid server thread
     */
    @SuppressWarnings("WeakerAccess")
    public void setFloidServerThread(Thread floidServerThread) {
        this.floidServerThread = floidServerThread;
    }

    /**
     * Get the thread id
     * @return the thread id
     */
    @SuppressWarnings("WeakerAccess")
    public int getThreadID() {
        return threadID;
    }

    /**
     * Get the status of the thread
     * @return the status of the thread
     */
    @SuppressWarnings("unused")
    public String getStatus() {
        return status;
    }

    /**
     * Get the number of pings this thread has received
     * @return the number of pings this thread has received
     */
    @SuppressWarnings("WeakerAccess")
    public int getNumberOfPings() {
        return numberOfPings;
    }

    /**
     * Set the number of pings this thread has received
     * @param numberOfPings the number of pings
     */
    @SuppressWarnings("unused")
    public void setNumberOfPings(int numberOfPings) {
        this.numberOfPings = numberOfPings;
    }

    /**
     * Increase the number of pings this thread has received by one
     */
    @SuppressWarnings("WeakerAccess")
    public void increaseNumberOfPings() {
        ++this.numberOfPings;
    }

    private String formatTimeStamp(long timestamp) {
        Instant instant = Instant.ofEpochMilli(timestamp);
        return instant.toString();
    }
    public String toString() {
        return "[" + threadID + "] " + pingType + " " + formatTimeStamp(startTimestamp) + " " + formatTimeStamp(endTimestamp) + " " + status + " " + numberOfPings;
    }
}

