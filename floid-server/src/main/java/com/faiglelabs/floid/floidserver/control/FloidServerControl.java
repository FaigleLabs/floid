package com.faiglelabs.floid.floidserver.control;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/floidservercontrol")
public class FloidServerControl {
    @RequestMapping("/echo")
    public String echo() {
        return "This is echo";
    }
}
