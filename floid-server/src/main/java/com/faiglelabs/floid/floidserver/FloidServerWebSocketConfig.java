/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * FloidServerThreadDumper.java
 */
package com.faiglelabs.floid.floidserver;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

// SEE HERE: https://github.com/lahsivjar/spring-websocket-template/tree/master/with-sockjs/src/main/java/lahsivjar/spring/websocket/template
@Configuration
@EnableWebSocketMessageBroker
public class FloidServerWebSocketConfig implements WebSocketMessageBrokerConfigurer {

    // This will be for sending and receiving messages:
    // These are our path prefixes - /topic for messages /app for application level work:
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/queue/", "/topic/");
        registry.setApplicationDestinationPrefixes("/app");
    }

    // This will be for subscribing:
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        // Note: To test: sudo wscat -c  http://localhost:8080/handler/websocket
        registry.addEndpoint("/handler").setAllowedOrigins("*").withSockJS(); // NOTE WE HAVE ALLOWED ALL ORIGINS TO SOCKJS...
    }

}
