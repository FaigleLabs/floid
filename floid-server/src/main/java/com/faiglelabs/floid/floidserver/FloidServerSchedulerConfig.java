/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * FloidServerSchedulerConfig.java
 */

package com.faiglelabs.floid.floidserver;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
public class FloidServerSchedulerConfig {
    // The purpose of this class is solely to enable spring
    // scheduling so that @Schedule annotations are enabled
}
