package com.faiglelabs.floid.floidserver.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;

@Configuration
public class WebSocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {
    // NOTE: See e.g.: sudo wscat -c -s STOMP http://localhost:8080/handler/websocket

    // See: https://docs.spring.io/spring-security/site/docs/4.2.x/reference/html/websocket.html
    @Override
    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
        messages
                .nullDestMatcher().authenticated()
                .simpDestMatchers("/handler/**").hasAnyRole(WebSecurityConfig.FLOID_ROLE_FLOID_ADMIN, WebSecurityConfig.FLOID_ROLE_FLOID_COMMANDER, WebSecurityConfig.FLOID_ROLE_FLOID_USER)
                .simpSubscribeDestMatchers("/topic/**").hasAnyRole(WebSecurityConfig.FLOID_ROLE_FLOID_ADMIN, WebSecurityConfig.FLOID_ROLE_FLOID_COMMANDER, WebSecurityConfig.FLOID_ROLE_FLOID_USER)
                .simpTypeMatchers(SimpMessageType.SUBSCRIBE).hasAnyRole(WebSecurityConfig.FLOID_ROLE_FLOID_ADMIN, WebSecurityConfig.FLOID_ROLE_FLOID_COMMANDER, WebSecurityConfig.FLOID_ROLE_FLOID_USER)
                .simpTypeMatchers(SimpMessageType.MESSAGE).denyAll()
                .anyMessage().denyAll();
    }

    @Override
    protected boolean sameOriginDisabled() {
        return true;
    }
}
