package com.faiglelabs.floid.floidserver;

import com.faiglelabs.floid.servertypes.user.FloidUser;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class FloidUserMonitor {

    private final static Object userSync = new Object();
    private static Map<String, User> floidUserMap= new HashMap<>();
    private final static Logger logger = LoggerFactory.getLogger(FloidUserMonitor.class.getName());

    @PostConstruct
    private void init() {
        floidUserMap = retrieveFloidUserMap();
    }

    @Scheduled(fixedDelay=10000, initialDelay = 10000)
    public void work() {
        Map<String, User> newFloidUserMap = retrieveFloidUserMap();
        if(newFloidUserMap!=null) {
            synchronized (userSync) {
                floidUserMap = newFloidUserMap;
            }
        }
    }

    private static Map<String, User> retrieveFloidUserMap() {
        logger.debug("Floid Cache: Retrieving users");
        // Creates a map of floid users:
        HashMap<String, User>  floidUserMap = new HashMap<>();
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            try {
                Query query = session.createQuery("select new FloidUser(id, userName, password, roles) from FloidUser");
                @SuppressWarnings("unchecked")
                List<FloidUser> floidUserList = (List<FloidUser>) query.list();
                for (FloidUser floidUser : floidUserList) {
                    ArrayList<GrantedAuthority> authorities = new ArrayList<>();
                    for (String role : floidUser.getRoles().split(",")) {
                        authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
                    }
                    floidUserMap.put(floidUser.getUserName(), new User(floidUser.getUserName(), floidUser.getPassword(), authorities));
                }
            } catch (Exception e2) {
                logger.error("getFloidIds: " + e2.getMessage() + " " + e2.toString());
            } finally {
                if(session!=null) {
                    session.close();
                }
            }
        } catch (Exception e) {
            logger.error("getFloidIds: " + e.getMessage() + " " + e.toString());
        }
        return floidUserMap;
    }

    public static Map<String, User> getFloidUserMap() {
        synchronized (userSync) {
            return floidUserMap;
        }
    }
    public static User getFloidUser(String userName) {
        synchronized (userSync) {
            if(floidUserMap!=null) {
                return floidUserMap.get(userName);
            }
            return null;
        }
    }
}
