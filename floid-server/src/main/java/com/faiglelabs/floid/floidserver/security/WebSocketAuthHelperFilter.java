package com.faiglelabs.floid.floidserver.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WebSocketAuthHelperFilter implements Filter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        final String authorization = httpServletRequest.getParameter("authorization");
        // If the authorization parameter is not null and we do not have one in the header then add it:
        if(authorization != null && httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION)==null) {
            MutableHttpServletRequest mutableHttpServletRequest = new MutableHttpServletRequest(httpServletRequest);
            mutableHttpServletRequest.putHeader(HttpHeaders.AUTHORIZATION, authorization);
            httpServletRequest = mutableHttpServletRequest;
        }
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        // logger.info("Request:  {} : {}", httpServletRequest.getMethod(), httpServletRequest.getRequestURI());
        chain.doFilter(httpServletRequest, response);
        // logger.info("WebSocketAuthHelperFilter Response type:{}", httpServletResponse.getContentType());
    }
}
