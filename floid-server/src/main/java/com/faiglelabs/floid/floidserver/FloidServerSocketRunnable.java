/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.floidserver;

import com.faiglelabs.floid.servertypes.commands.FloidServerCommandResult;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import java.io.IOException;
import java.util.*;

/**
 * The type Floid server socket runnable.
 */
@SuppressWarnings("WeakerAccess")
public class FloidServerSocketRunnable implements Runnable {
    /**
     * The Logger.
     */
    protected final Logger logger;
    private boolean stop;
    private final List<FloidServerThreadInfo> currentFloidServerThreadInfoList;               // Set in constructor
    private final List<FloidServerThreadInfo> completedFloidServerThreadInfoList;             // Set in constructor

    private final SSLServerSocket sslServerSocket;
    private int currentThreadID = 0;
    private String status;
    private int floidServerMessageID = 0;
    @SuppressWarnings("FieldCanBeLocal")
    private final int floidServerSocketTimeout = 10000;

    private final SimpMessagingTemplate messagingTemplate;

    /**
     * Server is running
     */
    public final static String STATUS_RUNNING = "RUNNING";
    /**
     * Server is stopping
     */
    public final static String STATUS_STOPPING = "STOPPING";
    /**
     * Server is off
     */
    public final static String STATUS_OFF = "OFF";

    /**
     * Instantiates a new Floid server socket runnable.
     *
     * @param iCurrentFloidServerThreadInfoList   the current floid server thread info list
     * @param iCompletedFloidServerThreadInfoList the completed floid server thread info list
     * @param iSSLServerSocket                    the ssl server socket
     */
    public FloidServerSocketRunnable(List<FloidServerThreadInfo> iCurrentFloidServerThreadInfoList,
                                     List<FloidServerThreadInfo> iCompletedFloidServerThreadInfoList,
                                     SSLServerSocket iSSLServerSocket,
                                     SimpMessagingTemplate iMessagingTemplate) {
        logger = LoggerFactory.getLogger(this.getClass().getName());
        sslServerSocket = iSSLServerSocket;
        currentFloidServerThreadInfoList = iCurrentFloidServerThreadInfoList;
        completedFloidServerThreadInfoList = iCompletedFloidServerThreadInfoList;
        stop = false;
        status = STATUS_OFF;
        messagingTemplate = iMessagingTemplate;
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets stop.
     */
    public void setStop() {
        stop = true;
        status = STATUS_STOPPING;
        sendFloidServerStatusMessage(status);
    }

    /**
     * Send floid server status message.
     *
     * @param status the status
     */
    public void sendFloidServerStatusMessage(String status) {
        boolean success = FloidServerRunnable.setupFloidDroidMessage(
                messagingTemplate,
                FloidServerRunnable.FLOID_SUBTOPIC_SERVER_STATUS,
                FloidServerRunnable.FLOID_MESSAGE_TYPE_FLOID_SERVER_STATUS,
                String.valueOf(floidServerMessageID++),
                -1,
                "",
                FloidServerRunnable.FLOID_SERVER_STATUS_DESTINATION,
                status,
                logger);
        if(success) {
            logger.debug("Sent floid server status message.");
        } else {
            logger.error("Failed to send floid server status message.");
        }
    }

    @Override
    public void run() {
        try {
            stop = false;
            status = STATUS_RUNNING;
            sendFloidServerStatusMessage(status);

            while (true) {
                if (stop) {
                    status = STATUS_OFF;
                    sendFloidServerStatusMessage(status);
                    return; // Done!
                }
                try {
                    // Accept the connection:
                    SSLSocket sslSocket = (SSLSocket) sslServerSocket.accept();
                    sslSocket.setSoTimeout(floidServerSocketTimeout);
                    printSocketInfo(sslSocket);

                    // Make a new instance of the current time:
                    Calendar currentDateTime = Calendar.getInstance();

                    // Make a new thread info:
                    FloidServerThreadInfo floidServerThreadInfo = new FloidServerThreadInfo(++currentThreadID, currentDateTime.getTimeInMillis());

                    // Pass it all off to the new runnable:
                    FloidServerRunnable floidServerRunnable = new FloidServerRunnable(floidServerThreadInfo, currentFloidServerThreadInfoList, completedFloidServerThreadInfoList, sslSocket, messagingTemplate);

                    // Record the thread:
                    floidServerThreadInfo.setFloidServerRunnable(floidServerRunnable);

                    // And create a thread out of it and fire it up:
                    Thread floidServerThread = new Thread(floidServerRunnable);

                    // Give the thread to the thread info:
                    floidServerThreadInfo.setFloidServerThread(floidServerThread);

                    // Add the thread info into the list:
                    synchronized (currentFloidServerThreadInfoList) {
                        currentFloidServerThreadInfoList.add(floidServerThreadInfo);
                    }

                    // And start...
                    floidServerThread.start();
                } catch (IOException e) {
                    logger.error("FloidServerSocketRunnable - I/O exception in main thread", e);
                }
            }
        } catch (Exception e) {
            logger.error("FloidServerSocketRunnable - MAJOR exception in main thread", e);
        }
    }

    /**
     *
     * Send instruction.
     *
     * @param floidId          the floid id
     * @param droidInstruction the droid instruction
     * @param floidServerCommandResult the current floid server command result
     */
    public void sendInstruction(int floidId, DroidInstruction droidInstruction, FloidServerCommandResult floidServerCommandResult) {
        floidServerCommandResult.setThreadSuccess(false);
        logger.debug("Received Instruction - listing available floid server threads:");
        for (FloidServerThreadInfo floidServerThreadInfo : currentFloidServerThreadInfoList) {
            logger.debug("[" + floidServerThreadInfo.getThreadID() + "] -->" + floidServerThreadInfo.getPingType() + " " + floidServerThreadInfo.getFloidServerRunnable().getFloidId() + " " + floidServerThreadInfo.getFloidServerRunnable().getFloidUuid());
        }
        // Logic: Go through all the floid server thread infos and get the last matching floid id:
        FloidServerThreadInfo matchingFloidServerThreadInfo = null;
        for (FloidServerThreadInfo floidServerThreadInfo : currentFloidServerThreadInfoList) {
            if (floidServerThreadInfo.getFloidServerRunnable().getFloidId() == floidId) {
                if(matchingFloidServerThreadInfo != null) {
                    // Logic error - we should only have one thread for this floid id at this point:
                    logger.error("Logic error - should find only one matching thread but also see this [" + matchingFloidServerThreadInfo.getThreadID() + "]");
                }
                matchingFloidServerThreadInfo = floidServerThreadInfo;
            }
        }
        if(matchingFloidServerThreadInfo != null) {
            logger.debug("Sending instruction to thread: [" + matchingFloidServerThreadInfo.getThreadID() + "]");
            floidServerCommandResult.setThreadSuccess(true);
            // Send the instruction:
            matchingFloidServerThreadInfo.getFloidServerRunnable().sendInstruction(droidInstruction, floidServerCommandResult);
            return;
        }
        floidServerCommandResult.setResultMessage(FloidServerRunnable.ERROR_MESSAGE_NO_THREAD);
        floidServerCommandResult.setResult(FloidServerRunnable.ERROR_CODE_NO_THREAD);
    }

    /**
     * Gets floid list.
     *
     * @return the floid list
     */
    @SuppressWarnings("unused")
    public ArrayList<Integer> getFloidList() {
        ArrayList<Integer> floidList = new ArrayList<>();

        // Logic: Go through all the floid server thread infos:
        for (FloidServerThreadInfo floidServerThreadInfo : currentFloidServerThreadInfoList) {
            Integer floidId = floidServerThreadInfo.getFloidServerRunnable().getFloidId();
            if (!floidList.contains(floidId)) {
                floidList.add(floidId);
            }
        }
        return floidList;
    }

    private void printSocketInfo(SSLSocket sslSocket) {
        logger.debug("        Socket class: " + sslSocket.getClass());
        logger.debug("      Remote address: " + sslSocket.getInetAddress().toString());
        logger.debug("         Remote port: " + sslSocket.getPort());
        logger.debug("Local socket address: " + sslSocket.getLocalSocketAddress().toString());
        logger.debug("       Local address: " + sslSocket.getLocalAddress().toString());
        logger.debug("          Local port: " + sslSocket.getLocalPort());
        logger.debug("    Need client auth: " + sslSocket.getNeedClientAuth());
        SSLSession sslSession = sslSocket.getSession();
        try {
            logger.debug("    Session class: " + sslSession.getClass());
            logger.debug("     Cipher suite: " + sslSession.getCipherSuite());
            logger.debug("         Protocol: " + sslSession.getProtocol());
            logger.debug("    PeerPrincipal: " + sslSession.getPeerPrincipal().getName());
            logger.debug("   LocalPrincipal: " + sslSession.getLocalPrincipal().getName());
        } catch (SSLPeerUnverifiedException e) {
            logger.debug(e.toString());
        }
    }

}
