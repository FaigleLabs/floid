/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * StaticResourceConfiguration
 */
package com.faiglelabs.floid.floidserver.staticfiles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class StaticResourceConfiguration implements WebMvcConfigurer {
    private static final Logger staticLogger = LoggerFactory.getLogger(StaticResourceConfiguration.class);

    @Value("${maps.location}")
    private String location;
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if(location !=null) {
            staticLogger.info("Local Maps Location: {}", location);
            registry.addResourceHandler("/maps/**").addResourceLocations(String.format("file:%s", location));
        } else {
            staticLogger.error("Local Maps Location was NULL");
        }
    }
}
