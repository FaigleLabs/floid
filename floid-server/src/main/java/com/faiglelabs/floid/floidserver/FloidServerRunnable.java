/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * FloidServerRunnable
 */
package com.faiglelabs.floid.floidserver;

import com.faiglelabs.floid.database.FloidDatabaseTables;
import com.faiglelabs.floid.floidserver.control.FloidServerControlServlet;
import com.faiglelabs.floid.servertypes.commands.*;
import com.faiglelabs.floid.servertypes.floidid.FloidIdAndState;
import com.faiglelabs.floid.servertypes.floidid.FloidUuidAndState;
import com.faiglelabs.floid.servertypes.geo.FloidGpsPoint;
import com.faiglelabs.floid.servertypes.geo.FloidGpsPointMercator;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import com.faiglelabs.floid.servertypes.mission.DroidMissionList;
import com.faiglelabs.floid.servertypes.photovideo.FloidDroidPhoto;
import com.faiglelabs.floid.servertypes.photovideo.FloidDroidVideoFrame;
import com.faiglelabs.floid.servertypes.photovideo.FloidImage;
import com.faiglelabs.floid.servertypes.pins.*;
import com.faiglelabs.floid.servertypes.statuses.*;
import com.faiglelabs.floid.utils.DistanceUtils;
import com.faiglelabs.floid.utils.FloidServerMessageStatus;
import com.faiglelabs.floid.utils.FloidServerMessageUtils;
import com.faiglelabs.proto.floid.FloidServerMessages.FloidClientMessage;
import com.faiglelabs.proto.floid.FloidServerMessages.FloidServerMessage;
import com.goebl.simplify.PointExtractor;
import com.goebl.simplify.Simplify;
import org.apache.commons.io.FileUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.PrintStream;
import java.math.BigInteger;
import java.net.Socket;
import java.util.*;

/**
 * The floid server runnable
 */
@SuppressWarnings({"JpaQlInspection", "rawtypes"})
public class FloidServerRunnable implements Runnable {
    /**
     * THe floid server status message type
     */
    static final String FLOID_MESSAGE_TYPE_FLOID_SERVER_STATUS = "FloidServerStatus";
    private static final String FLOID_MESSAGE_TYPE_NEW_FLOID_UUID = "NewFloidUuid";
    private static final String FLOID_MESSAGE_TYPE_NEW_FLOID_ID = "NewFloidId";
    private static final String FLOID_MESSAGE_TYPE_FLOID_STATUS = "FloidStatus";
    private static final String FLOID_MESSAGE_TYPE_FLOID_MODEL_STATUS = "FloidModelStatus";
    private static final String FLOID_MESSAGE_TYPE_FLOID_MODEL_PARAMETERS = "FloidModelParameters";
    private static final String FLOID_MESSAGE_TYPE_FLOID_DROID_MESSAGE = "FloidDroidMessage";
    private static final String FLOID_MESSAGE_TYPE_FLOID_DROID_STATUS = "FloidDroidStatus";
    private static final String FLOID_MESSAGE_TYPE_FLOID_DROID_PHOTO = "FloidDroidPhoto";
    private static final String FLOID_MESSAGE_TYPE_FLOID_DROID_VIDEO_FRAME = "FloidDroidVideoFrame";
    private static final String FLOID_MESSAGE_TYPE_FLOID_DROID_MISSION_INSTRUCTION_STATUS_MESSAGE = "FloidDroidMissionInstructionStatusMessage";
    /**
     * THe floid server status destination
     */
    static final String FLOID_SERVER_STATUS_DESTINATION = "floidServerStatusDestination";
    private static final String FLOID_STATUS_DESTINATION = "floidStatusDestination";
    private static final String FLOID_MODEL_STATUS_DESTINATION  = "floidModelStatusDestination";
    private static final String FLOID_MODEL_PARAMETERS_DESTINATION  = "floidModelParametersDestination";
    private static final String FLOID_DROID_MESSAGE_DESTINATION  = "floidDroidMessageDestination";
    private static final String FLOID_DROID_STATUS_DESTINATION  = "floidDroidStatusDestination";
    private static final String FLOID_DROID_PHOTO_DESTINATION  = "floidDroidPhotoDestination";
    private static final String FLOID_DROID_VIDEO_FRAME_DESTINATION  = "floidDroidVideoFrameDestination";
    private static final String FLOID_NEW_FLOID_ID_DESTINATION  = "floidNewFloidIdDestination";
    private static final String FLOID_NEW_FLOID_UUID_DESTINATION  = "floidNewFloidUuidDestination";
    private static final String FLOID_DROID_MISSION_INSTRUCTION_STATUS_MESSAGE_DESTINATION  = "floidDroidMissionInstructionStatusMessageDestination";

    public static final String FLOID_SUBTOPIC_SERVER_STATUS = "com.faiglelabs.floid.serverstatus";
    private static final String FLOID_SUBTOPIC_FLOID_STATUS = "com.faiglelabs.floid.floidstatus.floidId=";
    private static final String FLOID_SUBTOPIC_FLOID_MODEL_STATUS = "com.faiglelabs.floid.floidmodelstatus.floidId=";
    private static final String FLOID_SUBTOPIC_FLOID_MODEL_PARAMETERS = "com.faiglelabs.floid.floidmodelparameters.floidId=";
    private static final String FLOID_SUBTOPIC_FLOID_DROID_MESSAGE = "com.faiglelabs.floid.floiddroidmessage.floidId=";
    private static final String FLOID_SUBTOPIC_FLOID_DROID_STATUS = "com.faiglelabs.floid.floiddroidstatus.floidId=";
    private static final String FLOID_SUBTOPIC_FLOID_DROID_PHOTO = "com.faiglelabs.floid.floiddroidphoto.floidId=";
    private static final String FLOID_SUBTOPIC_FLOID_DROID_VIDEO_FRAME = "com.faiglelabs.floid.floiddroidvideoframe.floidId=";
    private static final String FLOID_SUBTOPIC_FLOID_NEW_FLOID_UUID = "com.faiglelabs.floid.newfloiduuid.floidId=";
    private static final String FLOID_SUBTOPIC_FLOID_NEW_FLOID_ID = "com.faiglelabs.floid.newfloidid";
    private static final String FLOID_SUBTOPIC_FLOID_DROID_MISSION_INSTRUCTION_STATUS = "com.faiglelabs.floid.floiddroidmissioninstructionstatus.floidId=";
    private static final int FLOID_LIFT_OFF_TEST_MIN_ALTITUDE = 10; // Note - this is hard-coded
    private static final String WARNING_LIFT_OFF_CANNOT_CHECK_PIN_HEIGHT = "[WARNING] LiftOff cannot check pin height";
    private static final String WARNING_HEIGHT_TO_CANNOT_CHECK_PIN_HEIGHT = "[WARNING] HeightTo cannot check pin height";
    private static final String WARNING_TAKE_PHOTO_WITH_VIDEO_STARTED_NOT_SUPPORTED_ON_ALL_DEVICES = "[WARNING] Take photo with video on not supported on all devices";
    private static final String ERROR_HOVER_ONLY_AFTER_LIFT_OFF = "[ERROR] Hover only after LiftOff";
    private static final String ERROR_DELAY_ONLY_ON_GROUND = "[ERROR] Delay only on ground";
    private static final String ERROR_PHOTO_STROBE_ALREADY_ON = "[ERROR] Photo strobe already on";
    private static final String ERROR_STOP_PHOTO_STROBE_WHEN_NO_STARED = "[ERROR] Stop photo strobe when not stared";
    private static final String ERROR_PAN_TILT_PIN_NAME = "[ERROR] PanTilt pin name: ";
    private static final String ERROR_LIFT_OFF_RELATIVE_HEIGHT_MUST_BE = "[ERROR] Lift off relative height must be >= ";
    private static final String METERS = "[ERROR]  meters";
    private static final String ERROR_LIFT_OFF_BELOW_PIN_HEIGHT = "[ERROR] Lift off below pin height";
    private static final String ERROR_LIFT_OFF_CANNOT_CHECK_PIN_HEIGHT = "[ERROR] Lift off cannot check pin height";
    private static final String ERROR_LIFT_OFF_WITHOUT_START_HELIS = "[ERROR] Lift off without StartHelis";
    private static final String ERROR_ALREADY_PERFORMED_LIFT_OFF = "[ERROR] Already performed Lift off";
    private static final String ERROR_HEIGHT_TO_BELOW_PIN_HEIGHT = "[ERROR] Height to below pin height";
    private static final String ERROR_HEIGHT_TO_WITHOUT_LIFT_OFF = "[ERROR] Height to without Lift off";
    private static final String ERROR_TURN_TO_WITHOUT_LIFT_OFF = "[ERROR] Turn to without Lift off";
    private static final String ERROR_LAND_WITHOUT_LIFT_OFF = "[ERROR] Land without Lift off";
    private static final String ERROR_HELIS_ALREADY_STARTED = "[ERROR] Helis already started";
    private static final String ERROR_HELIS_SHUT_DOWN_WITHOUT_START_UP = "[ERROR] Helis shut down without start up";
    private static final String ERROR_HELIS_SHUT_DOWN_DURING_FLIGHT = "[ERROR] Helis shut down during flight";
    private static final String ERROR_VIDEO_ALREADY_STARTED = "[ERROR] Video already started";
    private static final String ERROR_VIDEO_NOT_STARTED = "[ERROR] Video not started";
    private static final String ERROR_PAYLOAD_MUST_LIFT_OFF_FIRST = "[ERROR] Must lift off before payload";
    private static final String ERROR_PAYLOAD_BAY_ALREADY_AWAY = "[ERROR] Payload bay already away: ";
    private static final String ERROR_PAYLOAD_INVALID_BAY_NUMBER = "[ERROR] Invalid payload bay number";
    private static final String ERROR_FLIGHT_DOES_NOT_END_ON_GROUND = "[ERROR] Does not end on ground";
    private static final String ERROR_HELIS_ARE_NOT_TURNED_OFF = "[ERROR] Helis are not turned off";
    private static final String ERROR_VIDEO_NOT_STOPPED = "[ERROR] Video is not stopped";
    private static final String ERROR_PHOTO_STROBE_NOT_STOPPED = "[ERROR] Photo strobe is not stopped";
    private static int floidStatusMessageID = 0;
    private static int floidModelStatusMessageID = 0;
    private static int floidModelParametersMessageID = 0;
    private static int floidDroidMessageID = 0;
    private static int floidDroidStatusMessageID = 0;
    private static int floidDroidPhotoMessageID = 0;
    private static int floidDroidVideoFrameMessageID = 0;
    private static int floidNewFloidIdMessageID = 0;
    private static int floidMissionInstructionStatusMessageID = 0;
    private final FloidServerThreadInfo floidServerThreadInfo;                          // Set in constructor
    private final List<FloidServerThreadInfo> currentFloidServerThreadInfoList;         // Set in constructor
    private final List<FloidServerThreadInfo> completedFloidServerThreadInfoList;       // Set in constructor
    private final ArrayList<DroidInstruction> droidInstructions = new ArrayList<>();
    private final Logger logger;
    private final Socket socket;
    private boolean stop = false;
    private int floidId = 0; // We are droid 0 by default
    private String floidUuid = "";
    private static final Logger staticLogger = LoggerFactory.getLogger(FloidServerRunnable.class);
    private static final String SUCCESS_MESSAGE = "Success";
    private static final String ERROR_MESSAGE_FAILED_TO_EXECUTE_MISSION = "Error: Failed to execute mission";
    private static final String ERROR_MESSAGE_FAILED_TO_LOAD_MISSION = "Error: Failed to load mission";
    private static final String ERROR_MESSAGE_FAILED_TO_RETRIEVE_PIN_SET = "Error: Failed to retrieve pin set";
    private static final String ERROR_MESSAGE_DB_INTERACTION_FAILED = "Error: DB Interaction failed (session.close)";
    private static final String ERROR_MESSAGE_FAILED_TO_LOAD_PIN_SET = "Error: Failed to load pin set";
    private static final String ERROR_MESSAGE_MISSION_EXECUTE_FAILED_WITH_ERRORS = "Error: Mission execute failed with errors:";
    private static final String ERROR_MESSAGE_MISSION_EXECUTE_WITH_WARNINGS = "Warn: Mission executed with warnings: ";
    private static final String ERROR_MESSAGE_FAILED_TO_UPDATE_PIN_SET = "Error: Failed updating mission with pin locations";
    private static final String ERROR_MESSAGE_WRONG_MODE_FOR_INSTRUCTION = "Error: Wrong mode for instruction";
    private static final String ERROR_MESSAGE_OFFLINE = "Error: Offline";
    /**
     * No server error message
     */
    public static final String ERROR_MESSAGE_NO_SERVER = "Error: No Server";
    /**
     * No socket runnable error message
     */
    @SuppressWarnings("WeakerAccess")
    public static final String ERROR_MESSAGE_NO_SOCKET_RUNNABLE = "Error: No Socket Runnable";
    /**
     * No thread error message
     */
    @SuppressWarnings("WeakerAccess")
    public static final String ERROR_MESSAGE_NO_THREAD = "Error: No Thread";
    private static final int SUCCESS_CODE = 1;
    private static final int ERROR_CODE_FAILED_TO_EXECUTE_MISSION = -2;
    private static final int ERROR_CODE_FAILED_TO_LOAD_MISSION = -3;
    private static final int ERROR_CODE_FAILED_TO_RETRIEVE_PIN_SET = -4;
    private static final int ERROR_CODE_DB_INTERACTION_FAILED = -5;
    private static final int ERROR_CODE_FAILED_TO_LOAD_PIN_SET = -6;
    private static final int ERROR_CODE_MISSION_EXECUTE_FAILED_WITH_ERRORS = -7;
    @SuppressWarnings("unused")
    private static final int ERROR_CODE_MISSION_EXECUTE_WITH_WARNINGS = -8;
    private static final int ERROR_CODE_FAILED_TO_UPDATE_PIN_SET = -9;
    @SuppressWarnings("unused")
    private static final int ERROR_CODE_WRONG_MODE_FOR_INSTRUCTION = -10;
    private static final int ERROR_CODE_OFFLINE = -11;
    /**
     * No server error code
     */
    public static final int ERROR_CODE_NO_SERVER = -12;
    /**
     * No socket runnable error code
     */
     static final int ERROR_CODE_NO_SOCKET_RUNNABLE = -13;
    /**
     * No thread error code
     */
    static final int ERROR_CODE_NO_THREAD = -14;
    /**
     * Bad instruction error code
     */
    @SuppressWarnings("unused")
    static final int ERROR_CODE_BAD_INSTRUCTION = -15;

    private final SimpMessagingTemplate messagingTemplate;

    /**
     * Creates a new floid server runnable instance - this is the main server thread
     *
     * @param iPingServerThreadInfo              the ping server thread info
     * @param iCurrentPingServerThreadInfoList   the list of current ping server threads
     * @param iCompletedPingServerThreadInfoList the list of completed ping server threads
     * @param iSocket                            the socket
     */
    FloidServerRunnable(final FloidServerThreadInfo iPingServerThreadInfo, List<FloidServerThreadInfo> iCurrentPingServerThreadInfoList, List<FloidServerThreadInfo> iCompletedPingServerThreadInfoList, Socket iSocket, SimpMessagingTemplate iMessagingTemplate) {
        logger = LoggerFactory.getLogger(this.getClass().getName());
        socket = iSocket;
        floidServerThreadInfo = iPingServerThreadInfo;
        currentFloidServerThreadInfoList = iCurrentPingServerThreadInfoList;
        completedFloidServerThreadInfoList = iCompletedPingServerThreadInfoList;
        messagingTemplate = iMessagingTemplate;
    }

    /**
     * Get the mission from the mission id as an xml document
     *
     * @param missionId the mission id
     * @return the xml document
     */
    public static Document getMission(long missionId) {
        // OK...Get the mission and return it - if fail return an empty doc? or return null? Or throw an exception?
        // We need a Document
        Document missionDocument;
        try {
            try {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                missionDocument = documentBuilder.newDocument();
            } catch (Exception e) {
                staticLogger.error("FloidServerRunnable:getMission: ERROR CREATING MISSION DOCUMENT", e);
                return null;
            }
            if (missionDocument == null) {
                staticLogger.error("FloidServerRunnable:getMission: ERROR CREATING MISSION DOCUMENT: null Mission Document");
                return null;
            }
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            try {
                DroidMission loadedMission = session.get(DroidMission.class, missionId);
                session.close();
                if (loadedMission != null) {
                    missionDocument.appendChild(getElementFromMission(loadedMission, missionDocument));
                }
            } catch (Exception e2) {
                try {
                    staticLogger.error("FloidServerRemoting:getMission: Caught Exception", e2);
                    session.close();
                    return null;
                } catch (Exception e3) {
                    staticLogger.error("FloidServerRemoting:getMission: Caught Exception", e3);
                    return null;
                }
            }
        } catch (Exception e) {
            staticLogger.error("FloidServerRemoting:getMission: Caught Exception", e);
            return null;
        }
        return missionDocument;
    }

    private static DroidMission clearMissionReferencesForJSON(DroidMission mission) {
        mission.setDroidMission(null);
        for(DroidInstruction instruction:mission.getDroidInstructions()) {
            instruction.setDroidMission(null);
        }
        return mission;
    }
    /**
     * Get the mission from the mission id as a json string
     *
     * @param missionId the mission id
     * @return the xml document
     */
    public static String getMissionJSON(long missionId) {
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            try {
                DroidMission loadedMission = session.get(DroidMission.class, missionId);
                session.close();
                if(loadedMission != null) {
                    return clearMissionReferencesForJSON(loadedMission).toJSON().toString();
                }
            } catch (Exception e2) {
                try {
                    staticLogger.error("FloidServerRemoting:getMissionJSON: Caught Exception", e2);
                    session.close();
                    return null;
                } catch (Exception e3) {
                    staticLogger.error("FloidServerRemoting:getMissionJSON: Caught Exception", e3);
                    return null;
                }
            }
        } catch (Exception e) {
            staticLogger.error("FloidServerRemoting:getMissionJSON: Caught Exception", e);
            return null;
        }
        return "";
    }

    /**
     * Get the element from the droid instruction in the mission document
     *
     * @param droidInstruction the droid instruction
     * @param missionDocument  the mission document
     * @return the element
     */
    private static Element getElementFromMission(DroidInstruction droidInstruction, Document missionDocument) {
        Element newElement = missionDocument.createElement("node");
        try {
            if (droidInstruction.getId() != null) {
                newElement.setAttribute(FloidCommands.PARAMETER_ID, String.valueOf(droidInstruction.getId()));
            } else {
                newElement.setAttribute(FloidCommands.PARAMETER_ID, "" + DroidInstruction.NO_INSTRUCTION);
            }
            // Get the type:
            if (droidInstruction.getClass() == DroidMission.class) {
                // This is a mission - create the nodes and add the children;
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_MISSION, "missionIcon");
                DroidMission droidMission = (DroidMission) droidInstruction;
                newElement.setAttribute(FloidCommands.PARAMETER_MISSION_NAME, droidMission.getMissionName());
                newElement.setAttribute(FloidCommands.PARAMETER_TOP_LEVEL_MISSION, String.valueOf(droidMission.isTopLevelMission()));
                List<DroidInstruction> missionDroidInstructions = droidMission.getDroidInstructions();
                for (DroidInstruction missionDroidInstruction : missionDroidInstructions) {
                    newElement.appendChild(getElementFromMission(missionDroidInstruction, missionDocument));
                }
            } else if (droidInstruction.getClass() == PayloadCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_PAYLOAD, "payloadIcon");
                PayloadCommand payloadCommand = (PayloadCommand) droidInstruction;
                newElement.setAttribute(FloidCommands.PARAMETER_BAY, String.valueOf(payloadCommand.getBay()));
            } else if (droidInstruction.getClass() == TakePhotoCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_TAKE_PHOTO, "takePhotoIcon");
            } else if (droidInstruction.getClass() == ShutDownHelisCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_SHUT_DOWN_HELIS, "shutDownHelisIcon");
            } else if (droidInstruction.getClass() == HeightToCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_HEIGHT_TO, "heightToIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_Z, String.valueOf(((HeightToCommand) droidInstruction).getZ()));
                if (((HeightToCommand) droidInstruction).isAbsolute()) {
                    newElement.setAttribute(FloidCommands.PARAMETER_ABSOLUTE, FloidCommands.PARAMETER_TRUE);
                } else {
                    newElement.setAttribute(FloidCommands.PARAMETER_ABSOLUTE, FloidCommands.PARAMETER_FALSE);
                }
            } else if (droidInstruction.getClass() == LiftOffCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_LIFT_OFF, "liftOffIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_Z, String.valueOf(((LiftOffCommand) droidInstruction).getZ()));
                if (((LiftOffCommand) droidInstruction).isAbsolute()) {
                    newElement.setAttribute(FloidCommands.PARAMETER_ABSOLUTE, FloidCommands.PARAMETER_TRUE);
                } else {
                    newElement.setAttribute(FloidCommands.PARAMETER_ABSOLUTE, FloidCommands.PARAMETER_FALSE);
                }
            } else if (droidInstruction.getClass() == FlyToCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_FLY_TO, "flyToIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_PIN_NAME, ((FlyToCommand) droidInstruction).getPinName());
                newElement.setAttribute(FloidCommands.PARAMETER_X, String.valueOf(((FlyToCommand) droidInstruction).getX()));
                newElement.setAttribute(FloidCommands.PARAMETER_Y, String.valueOf(((FlyToCommand) droidInstruction).getY()));
                if (((FlyToCommand) droidInstruction).getFlyToHome()) {
                    newElement.setAttribute(FloidCommands.PARAMETER_FLY_TO_HOME, FloidCommands.PARAMETER_TRUE);
                } else {
                    newElement.setAttribute(FloidCommands.PARAMETER_FLY_TO_HOME, FloidCommands.PARAMETER_FALSE);
                }
            } else if (droidInstruction.getClass() == TurnToCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_TURN_TO, "turnToIcon");

                newElement.setAttribute(FloidCommands.PARAMETER_HEADING, String.valueOf(((TurnToCommand) droidInstruction).getHeading()));
                if (((TurnToCommand) droidInstruction).isFollowMode()) {
                    newElement.setAttribute(FloidCommands.PARAMETER_FOLLOW_MODE, FloidCommands.PARAMETER_TRUE);
                } else {
                    newElement.setAttribute(FloidCommands.PARAMETER_FOLLOW_MODE, FloidCommands.PARAMETER_FALSE);
                }
            } else if (droidInstruction.getClass() == StartVideoCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_START_VIDEO, "startVideoIcon");
            } else if (droidInstruction.getClass() == StopVideoCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_STOP_VIDEO, "stopVideoIcon");
            } else if (droidInstruction.getClass() == DeployParachuteCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_DEPLOY_PARACHUTE, "deployParachuteIcon");
            } else if (droidInstruction.getClass() == StartUpHelisCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_START_UP_HELIS, "startUpHelisIcon");
            } else if (droidInstruction.getClass() == AcquireHomePositionCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_ACQUIRE_HOME_POSITION, "acquireHomePositionIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_REQUIRED_XY_DISTANCE, String.valueOf(((AcquireHomePositionCommand) droidInstruction).getRequiredXYDistance()));
                newElement.setAttribute(FloidCommands.PARAMETER_REQUIRED_ALTITUDE_DISTANCE, String.valueOf(((AcquireHomePositionCommand) droidInstruction).getRequiredAltitudeDistance()));
                newElement.setAttribute(FloidCommands.PARAMETER_REQUIRED_HEADING_DISTANCE, String.valueOf(((AcquireHomePositionCommand) droidInstruction).getRequiredHeadingDistance()));
                newElement.setAttribute(FloidCommands.PARAMETER_REQUIRED_GPS_GOOD_COUNT, String.valueOf(((AcquireHomePositionCommand) droidInstruction).getRequiredGPSGoodCount()));
                newElement.setAttribute(FloidCommands.PARAMETER_REQUIRED_ALTIMETER_GOOD_COUNT, String.valueOf(((AcquireHomePositionCommand) droidInstruction).getRequiredAltimeterGoodCount()));
            } else if (droidInstruction.getClass() == DelayCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_DELAY, "delayIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_DELAY_TIME, String.valueOf(((DelayCommand) droidInstruction).getDelayTime()));
            } else if (droidInstruction.getClass() == HoverCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_HOVER, "hoverIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_HOVER_TIME, String.valueOf(((HoverCommand) droidInstruction).getHoverTime()));
            } else if (droidInstruction.getClass() == LandCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_LAND, "landIcon");
            } else if (droidInstruction.getClass() == SetParametersCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_SET_PARAMETERS, "setParametersIcon");

                if (((SetParametersCommand) droidInstruction).isFirstPinIsHome()) {
                    newElement.setAttribute(FloidCommands.PARAMETER_FIRST_PIN_IS_HOME, FloidCommands.PARAMETER_TRUE);
                } else {
                    newElement.setAttribute(FloidCommands.PARAMETER_FIRST_PIN_IS_HOME, FloidCommands.PARAMETER_FALSE);
                }
                newElement.setAttribute(FloidCommands.PARAMETER_HOME_PIN_NAME, ((SetParametersCommand) droidInstruction).getHomePinName());
                if (((SetParametersCommand) droidInstruction).isResetDroidParameters()) {
                    newElement.setAttribute(FloidCommands.PARAMETER_RESET_DROID_PARAMETERS, FloidCommands.PARAMETER_TRUE);
                } else {
                    newElement.setAttribute(FloidCommands.PARAMETER_RESET_DROID_PARAMETERS, FloidCommands.PARAMETER_FALSE);
                }
                newElement.setAttribute(FloidCommands.PARAMETER_DROID_PARAMETERS, ((SetParametersCommand) droidInstruction).getDroidParameters());
            } else if (droidInstruction.getClass() == FlyPatternCommand.class) {
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_FLY_PATTERN, "flyPatternIcon");
                // TODO [FLY PATTERN] UNIMPLEMENTED FOR NOW!
            } else if (droidInstruction.getClass() == PanTiltCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_PAN_TILT, "panTiltIcon");

                newElement.setAttribute(FloidCommands.PARAMETER_PAN, String.valueOf(((PanTiltCommand) droidInstruction).getPan()));
                newElement.setAttribute(FloidCommands.PARAMETER_PAN, String.valueOf(((PanTiltCommand) droidInstruction).getTilt()));
                newElement.setAttribute(FloidCommands.PARAMETER_PIN_NAME, ((PanTiltCommand) droidInstruction).getPinName());
                newElement.setAttribute(FloidCommands.PARAMETER_DEVICE, String.valueOf(((PanTiltCommand) droidInstruction).getDevice()));
                newElement.setAttribute(FloidCommands.PARAMETER_X, String.valueOf(((PanTiltCommand) droidInstruction).getX()));
                newElement.setAttribute(FloidCommands.PARAMETER_Y, String.valueOf(((PanTiltCommand) droidInstruction).getY()));
                newElement.setAttribute(FloidCommands.PARAMETER_Z, String.valueOf(((PanTiltCommand) droidInstruction).getZ()));
                if (((PanTiltCommand) droidInstruction).isUseAngle()) {
                    newElement.setAttribute(FloidCommands.PARAMETER_PAN_TILT, FloidCommands.PARAMETER_ANGLE);
                } else {
                    newElement.setAttribute(FloidCommands.PARAMETER_PAN_TILT, FloidCommands.PARAMETER_PIN);
                }
            } else if (droidInstruction.getClass() == DesignateCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_DESIGNATE, "designateIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_PIN_NAME, ((DesignateCommand) droidInstruction).getPinName());
                newElement.setAttribute(FloidCommands.PARAMETER_X, String.valueOf(((DesignateCommand) droidInstruction).getX()));
                newElement.setAttribute(FloidCommands.PARAMETER_Y, String.valueOf(((DesignateCommand) droidInstruction).getY()));
                newElement.setAttribute(FloidCommands.PARAMETER_Z, String.valueOf(((DesignateCommand) droidInstruction).getZ()));
                newElement.setAttribute(FloidCommands.PARAMETER_USE_PIN, ((DesignateCommand) droidInstruction).isUsePin() ? FloidCommands.PARAMETER_TRUE : FloidCommands.PARAMETER_FALSE);
                newElement.setAttribute(FloidCommands.PARAMETER_USE_PIN_HEIGHT, ((DesignateCommand) droidInstruction).isUsePinHeight() ? FloidCommands.PARAMETER_TRUE : FloidCommands.PARAMETER_FALSE);
            } else if (droidInstruction.getClass() == StopDesignatingCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_STOP_DESIGNATING, "stopDesignatingIcon");
            }
            // Speaker, SpeakerOn, SpeakerOff:
            else if (droidInstruction.getClass() == SpeakerCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_SPEAKER, "speakerIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_SPEAKER_REPEAT, String.valueOf(((SpeakerCommand) droidInstruction).getSpeakerRepeat()));
                newElement.setAttribute(FloidCommands.PARAMETER_RATE, String.valueOf(((SpeakerCommand) droidInstruction).getRate()));
                newElement.setAttribute(FloidCommands.PARAMETER_PITCH, String.valueOf(((SpeakerCommand) droidInstruction).getPitch()));
                newElement.setAttribute(FloidCommands.PARAMETER_DELAY, String.valueOf(((SpeakerCommand) droidInstruction).getDelay()));
                newElement.setAttribute(FloidCommands.PARAMETER_TTS, ((SpeakerCommand) droidInstruction).getTts());
                newElement.setAttribute(FloidCommands.PARAMETER_MODE, ((SpeakerCommand) droidInstruction).getMode());
                newElement.setAttribute(FloidCommands.PARAMETER_FILE_NAME, ((SpeakerCommand) droidInstruction).getFileName());
                newElement.setAttribute(FloidCommands.PARAMETER_VOLUME, String.valueOf(((SpeakerCommand) droidInstruction).getVolume()));
                newElement.setAttribute(FloidCommands.PARAMETER_ALERT, ((SpeakerCommand) droidInstruction).isAlert() ? FloidCommands.PARAMETER_TRUE : FloidCommands.PARAMETER_FALSE);
            } else if (droidInstruction.getClass() == SpeakerOnCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_SPEAKER_ON, "speakerOnIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_SPEAKER_REPEAT, String.valueOf(((SpeakerOnCommand) droidInstruction).getSpeakerRepeat()));
                newElement.setAttribute(FloidCommands.PARAMETER_RATE, String.valueOf(((SpeakerOnCommand) droidInstruction).getRate()));
                newElement.setAttribute(FloidCommands.PARAMETER_PITCH, String.valueOf(((SpeakerOnCommand) droidInstruction).getPitch()));
                newElement.setAttribute(FloidCommands.PARAMETER_DELAY, String.valueOf(((SpeakerOnCommand) droidInstruction).getDelay()));
                newElement.setAttribute(FloidCommands.PARAMETER_TTS, ((SpeakerOnCommand) droidInstruction).getTts());
                newElement.setAttribute(FloidCommands.PARAMETER_MODE, ((SpeakerOnCommand) droidInstruction).getMode());
                newElement.setAttribute(FloidCommands.PARAMETER_FILE_NAME, ((SpeakerOnCommand) droidInstruction).getFileName());
                newElement.setAttribute(FloidCommands.PARAMETER_VOLUME, String.valueOf(((SpeakerOnCommand) droidInstruction).getVolume()));
                newElement.setAttribute(FloidCommands.PARAMETER_ALERT, ((SpeakerOnCommand) droidInstruction).isAlert() ? FloidCommands.PARAMETER_TRUE : FloidCommands.PARAMETER_FALSE);
            } else if (droidInstruction.getClass() == SpeakerOffCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_SPEAKER_OFF, "speakerOffIcon");
            } else if (droidInstruction.getClass() == PhotoStrobeCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_PHOTO_STROBE, "photoStrobeIcon");
                newElement.setAttribute(FloidCommands.PARAMETER_DELAY, String.valueOf(((PhotoStrobeCommand) droidInstruction).getDelay()));
            } else if (droidInstruction.getClass() == StopPhotoStrobeCommand.class) {
                //create the root element and add it to the document
                setStandardElementAttributes(newElement, FloidCommands.COMMAND_STOP_PHOTO_STROBE, "stopPhotoStrobe");
            } else {
                // Error!
                staticLogger.error("FloidServerRunnable:getElementFromMission: ERROR - UNRECOGNIZED CLASS TYPE: " + droidInstruction.getClass().toString());
            }
        }
        catch (Exception e) {
            staticLogger.error("FloidServerRunnable:getElementFromMission: ERROR - UNRECOGNIZED CLASS TYPE", e);
        }
        return newElement;
    }

    /**
     * Set the standard element attributes
     *
     * @param element  the element
     * @param typeName the type name
     * @param iconName the icon name
     */
    private static void setStandardElementAttributes(Element element, String typeName, String iconName) {
        element.setAttribute(FloidCommands.PARAMETER_COMMAND, typeName);
        element.setAttribute(FloidCommands.PARAMETER_LABEL, typeName);
        element.setAttribute(FloidCommands.PARAMETER_ICON, iconName);
    }

    /**
     * Get the list of missions
     *
     * @return the list of missions
     */
    public static ArrayList<DroidMissionList> getMissionList() {
        ArrayList<DroidMissionList> missionList = new ArrayList<>();
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        try {
            // Create a native query - only populate mission list:
            final NativeQuery sqlQuery = session.createSQLQuery("select di.id, di.missionName from " + FloidDatabaseTables.DROID_INSTRUCTION + " di where topLevelMission=true");
            sqlQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            List<Map<String,Object>> aliasToValueMapList=sqlQuery.list();
            aliasToValueMapList.forEach((droidInstructionMap) -> {
                missionList.add(new DroidMissionList(((BigInteger)droidInstructionMap.get("id")).longValue(), (String) droidInstructionMap.get("missionName")));
            });
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getMissionList: Caught Exception: " + e);
                session.close();
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getMissionList: Caught Exception: " + e2);
            }
            throw e;
        } finally {
            if(session!=null) {
                session.close();
            }
        }
        return missionList;
    }

    /**
     * Get the pin set list
     *
     * @return the pin set list
     */
    public static ArrayList<FloidPinSetList> getPinSetList() {
        // Get the list of pin sets:
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();

        ArrayList<FloidPinSetList> pinSetList = new ArrayList<>();

/*
        // TEST CODE BELOW:
        ArrayList<Double> lats = new ArrayList<Double>();
        ArrayList<Double> lngs = new ArrayList<Double>();
        ArrayList<Double> heights = new ArrayList<Double>();
        ArrayList<String> names = new ArrayList<String>();
        lats.add(37.53620705273443);
        lngs.add(-77.42211574020386);
        heights.add(44.21388244628906);
        names.add("Church Hill");
        lats.add(36.53620705273443);
        lngs.add(-77.42211574020386);
        heights.add(44.21388244628906);
        names.add("Somewhere Else");
        for(int j=0;j<lats.size(); ++j)
        {
            double startLat     = lats.get(j);
            double startLng     = lngs.get(j);
            double startHeight  = heights.get(j);
            String name         = names.get(j);
            double offsetLat    = .01;
            double offsetLng    = .005;
            double offsetHeight = 10;

            FloidPinSet    pinSet    = new FloidPinSet();
            pinSet.setPinSetName(name);
            for(int i = 0; i<10; ++i)
            {
                try
                {
                    FloidPin newPin = createPin(name + i, startLat + offsetLat*i, startLng + offsetLng*i, startHeight + offsetHeight*i, true, null, pinSet, session);
                }
                catch(Exception e)
                {
                    staticLogger.error(e.toString());
                    // Do nothing for now - just don't want to throw this exception and fail to close the session...
                }
            }
            try
            {
                session.beginTransaction();
                session.persist(pinSet);
                session.getTransaction().commit();
            }
            catch(Exception e)
            {
                try
                {
                    staticLogger.error("FloidServerRemoting:getPinSetList: Caught Exception: " + e.toString());
                    session.close();
                }
                catch(Exception e2)
                {
                }
                throw e;
            }
        }
*/

        try {
            // Create a native query - only populate mission list:
            final NativeQuery sqlQuery = session.createSQLQuery("select ps.id, ps.pinSetName from " + FloidDatabaseTables.FLOID_PIN_SET + " ps");
            sqlQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            List<Map<String,Object>> aliasToValueMapList=sqlQuery.list();
            aliasToValueMapList.forEach((droidInstructionMap) -> {
                pinSetList.add(new FloidPinSetList(((BigInteger)droidInstructionMap.get("id")).longValue(), (String) droidInstructionMap.get("pinSetName")));
            });
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getPinSetList: Caught Exception: " + e);
                session.close();
                session = null;
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getPinSetList: Caught Exception: " + e2);
            }
            throw e;
        } finally {
            if(session!=null) {
                session.close();
            }
        }
        return pinSetList;
    }

    private static FloidPin createPin(String name, double lat, double lng, double height, boolean heightAuto, Long id, FloidPinSet pinSet) {
        FloidPin newPin = new FloidPin();
        newPin.setId(id);
        newPin.setPinName(name);
        newPin.setPinLat(lat);
        newPin.setPinLng(lng);
        newPin.setPinHeight(height);
        newPin.setPinHeightAuto(heightAuto);
        pinSet.add(newPin);
        return newPin;
    }

    /**
     * Get the pin set for the pin set id
     *
     * @param pinSetId the pin set id
     * @return the pin set
     */
    public static FloidPinSetAlt getPinSet(int pinSetId) {
        // The commented code below allows for transactions out of session,
        // but as long as I make sure the fetch type on these objects is "EAGER"
        // and make sure the session is closed (e.g. all exceptions caught) then I don't need to use it
        // it is just being stored here in case I find out the above is untrue...
        //        SessionFactory sessionFactory = (SessionFactory) WebApplicationContextUtils.context.getBean("sessionFactory");
        //        Session session = SessionFactoryUtils.getSession(sessionFactory, true);
        //        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));

        FloidPinSet pinSet;
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        try {
            pinSet = session.get(FloidPinSet.class, (long) pinSetId); //pins.FloidPinSet, pinSetId);
            session.close();
            session = null;
        } catch (HibernateException e) {
            try {
                if(session!=null) {
                    session.close();
                    session = null;
                }
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getPinSet: Caught Exception", e2);
            }
            throw e;
        } finally {
            if(session!=null) {
                session.close();
                session = null;
            }
        }

        // Code to be used with above transaction code if necessary:
        //        TransactionSynchronizationManager.unbindResource(sessionFactory);
        FloidPinSetAlt pinSetAlt = new FloidPinSetAlt();
        pinSetAlt.setPinSetName(pinSet.getPinSetName());
        pinSetAlt.setId(pinSet.getId());
        List<FloidPinAlt> pinAltList = new ArrayList<>();
        for (FloidPin pin : pinSet.getPinList()) {
            FloidPinAlt pinAlt = new FloidPinAlt();
            pinAlt.setId(pin.getId());
            pinAlt.setPinName(pin.getPinName());
            pinAlt.setPinLat(pin.getPinLat());
            pinAlt.setPinLng(pin.getPinLng());
            pinAlt.setPinHeight(pin.getPinHeight());
            pinAlt.setPinHeightAuto(pin.isPinHeightAuto());
            pinAltList.add(pinAlt);
        }
        pinSetAlt.setPinAltList(pinAltList);
        return pinSetAlt;
    }

    /**
     * Save the pin set from JSON
     *
     * @param pinSetAltJson the json form of the pin set
     * @return id if success, {@link FloidPinSet.NO_PIN_SET} ({@value FloidPinSet.NO_PIN_SET})
     */
    @SuppressWarnings("SameReturnValue")
    public static long savePinSetJson(String pinSetAltJson) {
        try {
            FloidPinSetAlt pinSetAlt = new FloidPinSetAlt().fromJSON(new JSONObject(pinSetAltJson));
            if (pinSetAlt != null) {
                return savePinSet(pinSetAlt);
            } else {
                staticLogger.error("FloidServerRemoting:savePinSetJson: pinSetAlt is null");
                return FloidPinSet.NO_PIN_SET;
            }
        } catch (Exception e) {
            staticLogger.error("FloidServerRemoting:savePinSetJson: Caught Exception", e);
        }
        return FloidPinSet.NO_PIN_SET;
    }

    /**
     * Import a pin set
     *
     * @param pinSetAltJson the json form of the pin set
     * @return id if success, {@link FloidPinSet.NO_PIN_SET} ({@value FloidPinSet.NO_PIN_SET})
     */
    @SuppressWarnings("SameReturnValue")
    public static Long importPinSet(String pinSetAltJson) {
        try {
            FloidPinSetAlt pinSetAlt = new FloidPinSetAlt().fromJSON(new JSONObject(pinSetAltJson));
            if (pinSetAlt != null) {
                savePinSet(pinSetAlt);
                return pinSetAlt.id;
            } else {
                staticLogger.error("FloidServerRemoting:savePinSetJson: pinSetAlt is null");
                return -1L;
            }
        } catch (Exception e) {
            staticLogger.error("FloidServerRemoting:savePinSetJson: Caught Exception", e);
        }
        return -1L;
    }

    /**
     * Save the pin set
     *
     * @param pinSetAlt the alternate form of the pin set
     * @return id if success, {@link FloidPinSet.NO_PIN_SET} ({@value FloidPinSet.NO_PIN_SET})
     */
    @SuppressWarnings("SameReturnValue")
    public static long savePinSet(FloidPinSetAlt pinSetAlt) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        // Make a new pin set...
        FloidPinSet pinSet = new FloidPinSet();
        pinSet.setPinSetName(pinSetAlt.getPinSetName());
        if (pinSetAlt.getId() == -1) {
            pinSetAlt.setId(null);
        } else {
            pinSet.setId(pinSetAlt.getId());
        }
        // Add in the pins and replace any '-1' id's with nulls:
        for (FloidPinAlt pinAlt : pinSetAlt.getPinAltList()) {
            if (pinAlt.id != null) {
                if (pinAlt.id == -1) {
                    pinAlt.id = null;
                }
            }
            @SuppressWarnings("unused")
            FloidPin newPin = createPin(pinAlt.getPinName(), pinAlt.getPinLat(), pinAlt.getPinLng(), pinAlt.getPinHeight(), pinAlt.isPinHeightAuto(), pinAlt.getId(), pinSet);
        }
        // OK, create a pin set and pin item for each:
        try {
            if (pinSetAlt.getId() == null) {
                // This is a new set:
                Transaction transaction = session.beginTransaction();
                session.saveOrUpdate(pinSet);
                transaction.commit();
                session.close();
                session = null;
            } else {
                // Not new: Merge
                Transaction transaction = session.beginTransaction();
                session.merge(pinSet);
                transaction.commit();
                session.close();
                session = null;
            }
            return pinSet.getId();
        } catch (Exception e) {
            staticLogger.error("FloidServerRemoting:savePinSet: Caught Exception", e);
            try {
                session.close();
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:savePinSet: Caught Exception", e2);
            }
        } finally {
            if(session!=null) {
                session.close();
                session = null;
            }
        }
        return FloidPinSet.NO_PIN_SET;
    }

    /**
     * Save the mission from the mission document
     *
     * @param missionDocument the mission document
     * @return the id for the mission once saved
     */
    public static Long saveMission(Document missionDocument) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        try {
            DroidMission droidMission = getDroidMissionFromXML(missionDocument.getDocumentElement(), null, session);
            if (droidMission == null) {
                staticLogger.error("droidMission was null");
                return DroidMission.NO_MISSION;  //-1L
            }
            Transaction transaction = session.beginTransaction();
            if ((droidMission.getId() == null) || (droidMission.getId() == -1)) {
                session.saveOrUpdate(droidMission);
            } else {
                session.merge(droidMission);
            }
            transaction.commit();
            session.close();
            session = null;
            return droidMission.getId();
        } catch (HibernateException e) {
            staticLogger.error("FloidServerRemoting:saveMission: Caught Exception", e);
            try {
                if(session!=null ) {
                    session.close();
                    session = null;
                }
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:saveMission: Caught Exception", e2);
            }
            throw e;
        } finally {
            if(session!=null) {
                session.close();
            }
        }
    }

    /**
     * Save the mission from the mission JSON
     *
     * @param missionJSON the mission JSON
     * @return the id for the mission once saved
     */
    public static Long saveMissionJSON(String missionJSON) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        try {
            DroidMission droidMission = new DroidMission().fromJSON(new JSONObject(missionJSON));
            if (droidMission == null) {
                staticLogger.error("FloidServerRemoting:saveMissionJSON: droidMission was null");
                return DroidMission.NO_MISSION;  //-1L
            }
            // Set the droid mission and make sure the numbering is correct:
            long currentInstructionNumber = 0;
            for(DroidInstruction droidInstruction: droidMission.getDroidInstructions()) {
                droidInstruction.setDroidMission(droidMission);
                droidInstruction.setMissionInstructionNumber(currentInstructionNumber++);
            }
            Transaction transaction = session.beginTransaction();
            if ((droidMission.getId() == null) || (droidMission.getId() == -1)) {
                session.saveOrUpdate(droidMission);
            } else {
                session.merge(droidMission);
            }
            transaction.commit();
            session.close();
            session=null;
            return droidMission.getId();
        } catch (HibernateException e) {
            staticLogger.error("FloidServerRemoting:saveMissionJSON: Caught Exception", e);
            try {
                if(session!=null) {
                    session.close();
                    session = null;
                }
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:saveMissionJSON: Caught Exception", e2);
            }
            throw e;
        } finally {
            if(session!=null) {
                session.close();
            }
        }
    }

    /**
     * Import the mission from the mission JSON
     *
     * @param missionJSON the mission JSON
     * @return the id for the mission once saved
     */
    public static Long importMission(String missionJSON) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        try {
            DroidMission droidMission = new DroidMission().fromJSON(new JSONObject(missionJSON));
            if (droidMission == null) {
                staticLogger.error("FloidServerRemoting:importMission: droidMission was null");
                return DroidMission.NO_MISSION;  //-1L
            }
            // Set the droid mission and make sure the numbering is correct:
            // And we also set all id's to null:
            droidMission.setId(null);
            long currentInstructionNumber = 0;
            for(DroidInstruction droidInstruction: droidMission.getDroidInstructions()) {
                droidInstruction.setId(null);
                droidInstruction.setDroidMission(droidMission);
                droidInstruction.setMissionInstructionNumber(currentInstructionNumber++);
            }
            Transaction transaction = session.beginTransaction();
            if ((droidMission.getId() == null) || (droidMission.getId() == -1)) {
                session.saveOrUpdate(droidMission);
            } else {
                session.merge(droidMission);
            }
            transaction.commit();
            session.close();
            session = null;
            return droidMission.getId();
        } catch (HibernateException e) {
            staticLogger.error("FloidServerRemoting:importMission: Caught Exception", e);
            try {
                if(session!=null) {
                    session.close();
                    session = null;
                }
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:importMission: Caught Exception", e2);
            }
            throw e;
        } finally {
            if(session!=null) {
                session.close();
            }
        }
    }

    private static DroidMission getDroidMissionFromXML(Element element, DroidMission parent, Session session) {
        // OK, make sure this is a mission element:
        DroidMission droidMission = new DroidMission();
        try {
            droidMission.setDroidMission(parent);       // Set the parent so it will hibernate correctly
            NamedNodeMap attributes = element.getAttributes();
            String commandAttributeNodeValue = getItemAsString(attributes,FloidCommands.PARAMETER_COMMAND);
            if (commandAttributeNodeValue.equals(FloidCommands.COMMAND_MISSION)) {
                long idLong = DroidMission.NO_MISSION;  // No id tag: -1L
                // OK - this is a mission like it is supposed to be - get the name:
                String nameString = getItemAsString(attributes, FloidCommands.PARAMETER_MISSION_NAME);
                droidMission.setMissionName(nameString);
                Node topLevelMissionAttributes = attributes.getNamedItem(FloidCommands.PARAMETER_TOP_LEVEL_MISSION);
                String topLevelMissionString = topLevelMissionAttributes.getNodeValue();
                if (topLevelMissionString.equals(FloidCommands.PARAMETER_TRUE)) {
                    droidMission.setTopLevelMission(true);
                }
                Node idAttributeNode = attributes.getNamedItem(FloidCommands.PARAMETER_ID);
                if (idAttributeNode != null) {
                    String idAttributeNodeValue = idAttributeNode.getNodeValue();
                    if (idAttributeNodeValue != null) {
                        idLong = Long.parseLong(idAttributeNodeValue);
                    }
                }
                //noinspection StatementWithEmptyBody
                if (idLong >= 0) {
                    droidMission.setId(idLong);
                } else {
                    // Don't do anything - it will be set automatically
                }
                // OK, get each child from the XML:
                NodeList children = element.getChildNodes();
                for (int i = 0; i < children.getLength(); ++i) {
                    if (children.item(i) instanceof Element) {
                        Element childI = (Element) children.item(i);
                        DroidInstruction newDroidInstruction = getDroidInstructionFromXML(childI, droidMission, session);
                        if(newDroidInstruction!=null) {
                            droidMission.addInstruction(newDroidInstruction);
                        }
                    }
                }
                return droidMission;
            }
        }
        catch(Exception e) {
            staticLogger.error("FloidServerRemoting:getDroidMissionFromXML: Caught Exception", e);
        }
        return null;    // Bad mission!
    }

    private static DroidInstruction getSetParametersCommand(NamedNodeMap attributes) {
        Node pinNameAttributes = attributes.getNamedItem(FloidCommands.PARAMETER_HOME_PIN_NAME);
        String pinNameString = pinNameAttributes.getNodeValue();
        Node firstPinIsHomeAttributes = attributes.getNamedItem(FloidCommands.PARAMETER_FIRST_PIN_IS_HOME);
        String firstPinIsHomeString = firstPinIsHomeAttributes.getNodeValue();
        boolean firstPinIsHomeBoolean = Boolean.parseBoolean(firstPinIsHomeString);
        Node resetDroidParametersAttributes = attributes.getNamedItem(FloidCommands.PARAMETER_RESET_DROID_PARAMETERS);
        String resetDroidParametersString = resetDroidParametersAttributes.getNodeValue();
        boolean resetDroidParametersBoolean = Boolean.parseBoolean(resetDroidParametersString);
        Node parametersAttributes = attributes.getNamedItem(FloidCommands.PARAMETER_DROID_PARAMETERS);
        String parametersString = parametersAttributes.getNodeValue();
        SetParametersCommand setParametersCommand = new SetParametersCommand();
        setParametersCommand.setHomePinName(pinNameString);
        setParametersCommand.setFirstPinIsHome(firstPinIsHomeBoolean);
        setParametersCommand.setResetDroidParameters(resetDroidParametersBoolean);
        setParametersCommand.setDroidParameters(parametersString);
        return setParametersCommand;
    }

    private static DroidInstruction getAcquireHomePositionCommand(NamedNodeMap attributes) {
        double requiredXYDistanceDouble = getItemAsDouble(attributes,FloidCommands.PARAMETER_REQUIRED_XY_DISTANCE);
        double requiredAltitudeDistanceDouble = getItemAsDouble(attributes,FloidCommands.PARAMETER_REQUIRED_ALTITUDE_DISTANCE);
        double requiredHeadingDistanceDouble = getItemAsDouble(attributes,FloidCommands.PARAMETER_REQUIRED_HEADING_DISTANCE);
        int requiredGPSGoodCountInt = getItemAsInt(attributes,FloidCommands.PARAMETER_REQUIRED_GPS_GOOD_COUNT);
        int requiredAltimeterGoodCountInt = getItemAsInt(attributes,FloidCommands.PARAMETER_REQUIRED_ALTIMETER_GOOD_COUNT);
        AcquireHomePositionCommand acquireHomePositionCommand = new AcquireHomePositionCommand();
        acquireHomePositionCommand.setRequiredXYDistance(requiredXYDistanceDouble);
        acquireHomePositionCommand.setRequiredAltitudeDistance(requiredAltitudeDistanceDouble);
        acquireHomePositionCommand.setRequiredHeadingDistance(requiredHeadingDistanceDouble);
        acquireHomePositionCommand.setRequiredGPSGoodCount(requiredGPSGoodCountInt);
        acquireHomePositionCommand.setRequiredAltimeterGoodCount(requiredAltimeterGoodCountInt);
        // 6. Return it:
        return acquireHomePositionCommand;
    }

    private static DroidInstruction getFlyToCommand(NamedNodeMap attributes) {
        String pinNameString = getItemAsString(attributes,FloidCommands.PARAMETER_PIN_NAME);
        boolean flyToHomeBoolean = getItemAsBoolean(attributes,FloidCommands.PARAMETER_FLY_TO_HOME);
        double x = getItemAsDouble(attributes,FloidCommands.PARAMETER_X);
        double y = getItemAsDouble(attributes,FloidCommands.PARAMETER_Y);
        FlyToCommand flyToCommand = new FlyToCommand();
        flyToCommand.setPinName(pinNameString);
        flyToCommand.setFlyToHome(flyToHomeBoolean);
        flyToCommand.setX(x);
        flyToCommand.setY(y);
        return flyToCommand;
    }

    private static DroidInstruction getDesignateCommand(NamedNodeMap attributes) {
        String pinNameString = getItemAsString(attributes,FloidCommands.PARAMETER_PIN_NAME);
        double x = getItemAsDouble(attributes,FloidCommands.PARAMETER_X);
        double y = getItemAsDouble(attributes,FloidCommands.PARAMETER_Y);
        double z = getItemAsDouble(attributes,FloidCommands.PARAMETER_Z);
        boolean usePin = getItemAsBoolean(attributes,FloidCommands.PARAMETER_USE_PIN);
        boolean usePinHeight = getItemAsBoolean(attributes,FloidCommands.PARAMETER_USE_PIN_HEIGHT);
        DesignateCommand designateCommand = new DesignateCommand();
        designateCommand.setPinName(pinNameString);
        designateCommand.setX(x);
        designateCommand.setY(y);
        designateCommand.setZ(z);
        designateCommand.setUsePin(usePin);
        designateCommand.setUsePinHeight(usePinHeight);
        return designateCommand;
    }

    private static double getItemAsDouble(NamedNodeMap attributes, String item){
        try {
            return Double.parseDouble(attributes.getNamedItem(item).getNodeValue());
        } catch(Exception e) {
            staticLogger.error("Exception retrieving " + item + " as double", e);
        }
        return 0.0;
    }
    private static int getItemAsInt(NamedNodeMap attributes, String item){
        try {
            return Integer.parseInt(attributes.getNamedItem(item).getNodeValue());
        } catch(Exception e) {
            staticLogger.error("Exception retrieving " + item + " as double", e);
        }
        return 0;
    }
    private static long getItemAsLong(NamedNodeMap attributes, String item){
        try {
            return Long.parseLong(attributes.getNamedItem(item).getNodeValue());
        } catch(Exception e) {
            staticLogger.error("Exception retrieving " + item + " as double", e);
        }
        return 0L;
    }
    private static String getItemAsString(NamedNodeMap attributes, String item){
        try {
            return attributes.getNamedItem(item).getNodeValue();
        } catch(Exception e) {
            staticLogger.error("Exception retrieving " + item + " as double", e);
        }
        return "";
    }
    private static boolean getItemAsBoolean(NamedNodeMap attributes, String item){
        try {
            return Boolean.parseBoolean(attributes.getNamedItem(item).getNodeValue());
        } catch(Exception e) {
            staticLogger.error("Exception retrieving " + item + " as double", e);
        }
        return false;
    }
    private static DroidInstruction getSpeakerCommand(NamedNodeMap attributes) {
        // Prototype:
        //   addNode(event, <node id="-1" label="Speaker" command="Speaker" delay="0" icon="speakerIcon" speakerRepeat="0" volume="1.0" mode="tts" tts="" pitch="1.0" rate="1.0" fileName="" alert="false" />);
        double pitchDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_PITCH);
        double rateDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_RATE);
        double volumeDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_VOLUME);
        int speakerRepeatInteger = getItemAsInt(attributes, FloidCommands.PARAMETER_SPEAKER_REPEAT);
        int delayInteger = getItemAsInt(attributes, FloidCommands.PARAMETER_DELAY);
        String fileNameString = getItemAsString(attributes, FloidCommands.PARAMETER_FILE_NAME);
        String ttsString = getItemAsString(attributes, FloidCommands.PARAMETER_TTS);
        String modeString= getItemAsString(attributes, FloidCommands.PARAMETER_MODE);
        boolean alertBoolean = getItemAsBoolean(attributes, FloidCommands.PARAMETER_ALERT);
        SpeakerCommand speakerCommand = new SpeakerCommand();
        speakerCommand.setAlert(alertBoolean);
        speakerCommand.setDelay(delayInteger);
        speakerCommand.setFileName(fileNameString);
        speakerCommand.setMode(modeString);
        speakerCommand.setPitch(pitchDouble);
        speakerCommand.setRate(rateDouble);
        speakerCommand.setSpeakerRepeat(speakerRepeatInteger);
        speakerCommand.setTts(ttsString);
        speakerCommand.setVolume(volumeDouble);
        return speakerCommand;
    }

    private static DroidInstruction getPanTiltCommand(NamedNodeMap attributes) {
        String panTiltString = getItemAsString(attributes, FloidCommands.PARAMETER_PAN_TILT);
        double panDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_PAN);
        double tiltDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_TILT);
        String pinNameString = getItemAsString(attributes, FloidCommands.PARAMETER_PIN_NAME);
        double x = getItemAsDouble(attributes, FloidCommands.PARAMETER_X);
        double y = getItemAsDouble(attributes, FloidCommands.PARAMETER_Y);
        double z = getItemAsDouble(attributes, FloidCommands.PARAMETER_Z);
        int device = getItemAsInt(attributes, FloidCommands.PARAMETER_DEVICE);

        PanTiltCommand panTiltCommand = new PanTiltCommand();
        panTiltCommand.setUseAngle(panTiltString.equals(FloidCommands.PARAMETER_ANGLE));

        panTiltCommand.setPan(panDouble);
        panTiltCommand.setTilt(tiltDouble);
        panTiltCommand.setPinName(pinNameString);
        panTiltCommand.setX(x);
        panTiltCommand.setY(y);
        panTiltCommand.setZ(z);
        panTiltCommand.setDevice(device);
        return panTiltCommand;
    }

    private static DroidInstruction getSpeakerOnCommand(NamedNodeMap attributes) {
        // Prototype:
        //   addNode(event, <node id="-1" label="SpeakerOn" command="SpeakerOn" delay="0" icon="speakerOnIcon" speakerRepeat="-1" volume="1.0" mode="tts" tts="" pitch="1.0" rate="1.0" fileName="" alert="false" />);
        double pitchDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_PITCH);
        double rateDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_RATE);
        double volumeDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_VOLUME);
        int speakerRepeatInteger = getItemAsInt(attributes, FloidCommands.PARAMETER_SPEAKER_REPEAT);
        int delayInteger = getItemAsInt(attributes, FloidCommands.PARAMETER_DELAY);
        String fileNameString = getItemAsString(attributes, FloidCommands.PARAMETER_FILE_NAME);
        String ttsString = getItemAsString(attributes, FloidCommands.PARAMETER_TTS);
        String modeString= getItemAsString(attributes, FloidCommands.PARAMETER_MODE);
        boolean alertBoolean = getItemAsBoolean(attributes, FloidCommands.PARAMETER_ALERT);
        SpeakerOnCommand speakerOnCommand = new SpeakerOnCommand();
        speakerOnCommand.setAlert(alertBoolean);
        speakerOnCommand.setDelay(delayInteger);
        speakerOnCommand.setFileName(fileNameString);
        speakerOnCommand.setMode(modeString);
        speakerOnCommand.setPitch(pitchDouble);
        speakerOnCommand.setRate(rateDouble);
        speakerOnCommand.setSpeakerRepeat(speakerRepeatInteger);
        speakerOnCommand.setTts(ttsString);
        speakerOnCommand.setVolume(volumeDouble);
        return speakerOnCommand;
    }

    /**
     * Get the droid instruction from the mission for this element
     *
     * @param element      the element
     * @param droidMission the droid mission
     * @param session      the database session
     * @return the droid instruction
     */
    private static DroidInstruction getDroidInstructionFromXML(Element element, DroidMission droidMission, Session session) {
        DroidInstruction droidInstruction = null;          // Pointer to future DroidInstruction
        NamedNodeMap attributes;
        Node idAttributeNode;
        String idAttributeNodeValue;
        try {
            // Get attribute list, id and command:
            long idLong = DroidInstruction.NO_INSTRUCTION;
            attributes = element.getAttributes();
            idAttributeNode = attributes.getNamedItem(FloidCommands.PARAMETER_ID);
            if (idAttributeNode != null) {
                idAttributeNodeValue = idAttributeNode.getNodeValue();
                if (idAttributeNodeValue != null) {
                    idLong = Long.parseLong(idAttributeNodeValue);
                }
            }
            String commandAttributeNodeValue = getItemAsString(attributes, FloidCommands.PARAMETER_COMMAND);
            if (commandAttributeNodeValue.equals(FloidCommands.COMMAND_MISSION)) {
                return getDroidMissionFromXML(element, droidMission, session);
            }
            switch (commandAttributeNodeValue) {
                case FloidCommands.COMMAND_SET_PARAMETERS: {
                    droidInstruction = getSetParametersCommand(attributes);
                    break;
                }
                case FloidCommands.COMMAND_ACQUIRE_HOME_POSITION:
                    droidInstruction = getAcquireHomePositionCommand(attributes);
                    break;
                case FloidCommands.COMMAND_START_UP_HELIS:
                    // No parameters
                    droidInstruction = new StartUpHelisCommand();
                    break;
                case FloidCommands.COMMAND_LIFT_OFF: {
                    // height is parameter:
                    double heightDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_Z);
                    LiftOffCommand liftOffCommand = new LiftOffCommand();
                    liftOffCommand.setZ(heightDouble);
                    droidInstruction = liftOffCommand;
                    break;
                }
                case FloidCommands.COMMAND_DELAY: {
                    // Parameter is delayTime (in seconds)
                    long timeLong = getItemAsLong(attributes, FloidCommands.PARAMETER_DELAY_TIME);
                    DelayCommand delayCommand = new DelayCommand();
                    delayCommand.setDelayTime(timeLong);
                    droidInstruction = delayCommand;
                    break;
                }
                case FloidCommands.COMMAND_FLY_TO: {
                    droidInstruction = getFlyToCommand(attributes);
                    break;
                }
                case FloidCommands.COMMAND_PAYLOAD:
                    int bayInteger = getItemAsInt(attributes, FloidCommands.PARAMETER_BAY);
                    PayloadCommand payloadCommand = new PayloadCommand();
                    payloadCommand.setBay(bayInteger);
                    droidInstruction = payloadCommand;
                    break;
                case FloidCommands.COMMAND_TURN_TO:
                    // Parameters are: heading and followMode::
                    double headingDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_HEADING);
                    boolean followModeBoolean = getItemAsBoolean(attributes, FloidCommands.PARAMETER_FOLLOW_MODE);
                    TurnToCommand turnToCommand = new TurnToCommand();
                    turnToCommand.setHeading(headingDouble);
                    turnToCommand.setFollowMode(followModeBoolean);
                    droidInstruction = turnToCommand;
                    break;
                case FloidCommands.COMMAND_HEIGHT_TO: {
                    // height is parameter:
                    double heightDouble = getItemAsDouble(attributes, FloidCommands.PARAMETER_Z);
                    HeightToCommand heightToCommand = new HeightToCommand();
                    heightToCommand.setZ(heightDouble);
                    droidInstruction = heightToCommand;
                    break;
                }
                case FloidCommands.COMMAND_START_VIDEO:
                    droidInstruction = new StartVideoCommand();
                    break;
                case FloidCommands.COMMAND_STOP_VIDEO:
                    droidInstruction = new StopVideoCommand();
                    break;
                case FloidCommands.COMMAND_DESIGNATE: {
                    droidInstruction = getDesignateCommand(attributes);
                    break;
                }
                case FloidCommands.COMMAND_STOP_DESIGNATING:
                    droidInstruction = new StopDesignatingCommand();
                    break;
                case FloidCommands.COMMAND_HOVER: {
                    // Parameter is hoverTime (in seconds)
                    long timeLong = getItemAsLong(attributes, FloidCommands.PARAMETER_HOVER_TIME);
                    HoverCommand hoverCommand = new HoverCommand();
                    hoverCommand.setHoverTime(timeLong);
                    droidInstruction = hoverCommand;
                    break;
                }
                case FloidCommands.COMMAND_PAN_TILT: {
                    droidInstruction = getPanTiltCommand(attributes);
                    break;
                }
                case FloidCommands.COMMAND_FLY_PATTERN:
                    droidInstruction = new FlyPatternCommand();
                    break;
                case FloidCommands.COMMAND_TAKE_PHOTO:
                    droidInstruction = new TakePhotoCommand();
                    break;
                case FloidCommands.COMMAND_LAND:
                    droidInstruction = new LandCommand();
                    break;
                case FloidCommands.COMMAND_SHUT_DOWN_HELIS:
                    droidInstruction = new ShutDownHelisCommand();
                    break;
                case FloidCommands.COMMAND_DEPLOY_PARACHUTE:
                    droidInstruction = new DeployParachuteCommand();
                    break;
                case FloidCommands.COMMAND_SPEAKER: {
                    droidInstruction = getSpeakerCommand(attributes);
                    break;
                }
                case FloidCommands.COMMAND_SPEAKER_ON: {
                    droidInstruction = getSpeakerOnCommand(attributes);
                    break;
                }
                case FloidCommands.COMMAND_SPEAKER_OFF:
                    // No parameters:
                    droidInstruction = new SpeakerOffCommand();
                    break;
                case FloidCommands.COMMAND_PHOTO_STROBE:
                    // Delay:
                    int delayInteger = getItemAsInt(attributes, FloidCommands.PARAMETER_DELAY);
                    PhotoStrobeCommand photoStrobeCommand = new PhotoStrobeCommand();
                    photoStrobeCommand.setDelay(delayInteger);
                    droidInstruction = photoStrobeCommand;
                    break;
                case FloidCommands.COMMAND_STOP_PHOTO_STROBE:
                    // No parameters:
                    droidInstruction = new StopPhotoStrobeCommand();
                    break;
                default:
                    staticLogger.error("FloidServerRunnable: getDroidInstructionFromXML: Bad Command: " + commandAttributeNodeValue);
                    break;
            }
            // Add id if it exists:
            if (droidInstruction != null) {
                // If we have an id set it, otherwise it will be set by hibernate
                if (idLong >= 0) {
                    droidInstruction.setId(idLong);
                }
                droidInstruction.setDroidMission(droidMission);

            } else {
                staticLogger.error("FloidServerRunnable: getDroidInstructionFromXMLNode: Returning a null node.");
            }
        } catch (NumberFormatException | DOMException e) {
            staticLogger.error("Caught Exception", e);
            return null;
        }
        return droidInstruction;
    }
    /**
     * Create a new standard mission with the mission name
     *
     * @param newMissionName the mission name
     * @return the new mission id
     */
    public static Long createNewMission(String newMissionName) {
        // Makes the following default mission:
 /*
    - SetParameters
	- AcquireHomePosition
	- StartVideo
	- Delay 10 seconds
	- StartUpHelis
	- LiftOff 500 m
	- Hover 30 seconds
	- Land
	- ShutDownHelis
	- Delay 10 seconds
	- StopVideo
 */

        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        long returnMissionId = DroidMission.NO_MISSION;
        // OK, create an empty pinset with this name and hibernate it, returning the id!
        DroidMission droidMission = new DroidMission();
        droidMission.setTopLevelMission(true);
        droidMission.setMissionName(newMissionName);

        // OK, add the elements below:
        SetParametersCommand setParametersCommand = new SetParametersCommand();
        setParametersCommand.setFirstPinIsHome(true);
        setParametersCommand.setHomePinName("UNDEFINED");
        setParametersCommand.setDroidParameters("");
        setParametersCommand.setResetDroidParameters(true);
        AcquireHomePositionCommand acquireHomePositionCommand = new AcquireHomePositionCommand();
        acquireHomePositionCommand.setRequiredXYDistance(15.0);
        acquireHomePositionCommand.setRequiredAltitudeDistance(FLOID_LIFT_OFF_TEST_MIN_ALTITUDE);
        acquireHomePositionCommand.setRequiredHeadingDistance(3.0);
        acquireHomePositionCommand.setRequiredGPSGoodCount(20);
        acquireHomePositionCommand.setRequiredAltimeterGoodCount(10);
        StartVideoCommand startVideoCommand = new StartVideoCommand();
        DelayCommand delayCommand1 = new DelayCommand();
        delayCommand1.setDelayTime(10);
        StartUpHelisCommand startUpHelisCommand = new StartUpHelisCommand();
        LiftOffCommand liftOffCommand = new LiftOffCommand();
        liftOffCommand.setZ(15);
        liftOffCommand.setAbsolute(false);
        HoverCommand hoverCommand = new HoverCommand();
        hoverCommand.setHoverTime(30);
        LandCommand landCommand = new LandCommand();
        ShutDownHelisCommand shutDownHelisCommand = new ShutDownHelisCommand();
        DelayCommand delayCommand2 = new DelayCommand();
        delayCommand2.setDelayTime(10);
        StopVideoCommand stopVideoCommand = new StopVideoCommand();
        setParametersCommand.setDroidMission(droidMission);
        acquireHomePositionCommand.setDroidMission(droidMission);
        startVideoCommand.setDroidMission(droidMission);
        delayCommand1.setDroidMission(droidMission);
        startUpHelisCommand.setDroidMission(droidMission);
        liftOffCommand.setDroidMission(droidMission);
        hoverCommand.setDroidMission(droidMission);
        landCommand.setDroidMission(droidMission);
        shutDownHelisCommand.setDroidMission(droidMission);
        delayCommand2.setDroidMission(droidMission);
        stopVideoCommand.setDroidMission(droidMission);
        droidMission.addInstruction(setParametersCommand);
        droidMission.addInstruction(acquireHomePositionCommand);
        droidMission.addInstruction(startVideoCommand);
        droidMission.addInstruction(delayCommand1);
        droidMission.addInstruction(startUpHelisCommand);
        droidMission.addInstruction(liftOffCommand);
        droidMission.addInstruction(hoverCommand);
        droidMission.addInstruction(landCommand);
        droidMission.addInstruction(shutDownHelisCommand);
        droidMission.addInstruction(delayCommand2);
        droidMission.addInstruction(stopVideoCommand);
        try {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(droidMission);
            transaction.commit();
            session.close();
            session = null;
            returnMissionId = droidMission.getId();
        } catch (HibernateException e) {
            staticLogger.error("FloidServerRemoting:createNewMission: Caught Exception", e);
            try {
                if(session!=null) {
                    session.close();
                    session = null;
                }
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:createNewMission: Caught Exception", e2);
            }
        } finally {
            if(session!=null) {
                session.close();
            }
        }
        return returnMissionId;
    }

    /**
     * Duplicate a mission
     *
     * @param originalMissionId the original mission id
     * @param newMissionName    the new mission name
     * @return the new mission id
     */
    public static Long duplicateMission(Long originalMissionId, String newMissionName) {
        long returnMissionId = DroidMission.NO_MISSION;
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        try {
            Transaction transaction = session.beginTransaction();
            DroidMission droidMission = session.get(DroidMission.class, originalMissionId);
            DroidMission newDroidMission = new DroidMission(droidMission); // Copy constructor
            newDroidMission.setMissionName(newMissionName);
            session.persist(newDroidMission);
            transaction.commit();
            session.close();
            session=null;
            returnMissionId = newDroidMission.getId();
        } catch (Exception e) {
            staticLogger.error("FloidServerRemoting:duplicateMission: Caught Exception", e);
        } finally {
            if (session!=null) {
                session.close();
            }
        }
        return returnMissionId;
    }

    /**
     * Delete a mission
     *
     * @param deleteMissionId the mission id
     * @return true if success
     */
    public static boolean deleteMission(Long deleteMissionId) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        try {
            Transaction transaction = session.beginTransaction();
            DroidMission droidMission = session.get(DroidMission.class, deleteMissionId);
            session.delete(droidMission);
            transaction.commit();
            session.close();
            return true;    // Successful delete
        } catch (HibernateException e) {
            staticLogger.error("FloidServerRemoting:deleteMission: Caught Exception", e);
            try {
                session.close();
            } catch (HibernateException e2) {
                // Do nothing...
            }
        }
        return false;   // Error - was not deleted...
    }

    /**
     * Create a pin set
     *
     * @param newPinSetName the new pin set name
     * @return the id of the new pin set
     */
    public static Long createPinSet(String newPinSetName) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        long returnPinSetId = FloidPinSet.NO_PIN_SET;
        // OK, create an empty pinset with this name and hibernate it, returning the id!
        FloidPinSet pinSet = new FloidPinSet();
        pinSet.setPinSetName(newPinSetName);

        try {
            Transaction transaction = session.beginTransaction();
            session.persist(pinSet);
            transaction.commit();
            session.close();
            returnPinSetId = pinSet.getId();
        } catch (HibernateException e) {
            staticLogger.error("FloidServerRemoting:createPinSet: Caught Exception", e);
            try {
                session.close();
            } catch (HibernateException e2) {
                // Do nothing...
            }
        }
        return returnPinSetId;
    }

    /**
     * Delete a pin set
     *
     * @param pinSetId the pin set id
     * @return true if success
     */
    public static boolean deletePinSet(Long pinSetId) {
        FloidPinSet pinSet;
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        try {
            Transaction transaction = session.beginTransaction();
            pinSet = session.get(FloidPinSet.class, pinSetId);
            if (pinSet == null) {
                session.close();
                return false;
            }
            session.delete(pinSet);
            session.flush();
            session.clear();
            transaction.commit();
            session.close();
        } catch (HibernateException e) {
            staticLogger.error("FloidServerRemoting:deletePinSet: Caught Exception", e);
            try {
                session.close();
                return false;
            } catch (HibernateException e2) {
                return false;
            }
        }
        return true;
    }


    /**
     * Duplicate a pin set
     *
     * @param originalPinSetId the original pin set id
     * @param newPinSetName    the new pin set name
     * @return the id of the new pin set
     */
    public static Long duplicatePinSet(Long originalPinSetId, String newPinSetName) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        long returnPinSetId = originalPinSetId;
        try {
            session.beginTransaction();
            FloidPinSet floidPinSet = session.get(FloidPinSet.class, originalPinSetId);
            FloidPinSet newFloidPinSet = new FloidPinSet();
            newFloidPinSet.setPinSetName(newPinSetName);
            for (FloidPin floidPin : floidPinSet.getPinList()) {
                FloidPin newFloidPin = new FloidPin();
                newFloidPin.setPinHeight(floidPin.getPinHeight());
                newFloidPin.setPinLat(floidPin.getPinLat());
                newFloidPin.setPinLng(floidPin.getPinLng());
                newFloidPin.setPinName(floidPin.getPinName());
                newFloidPin.setPinSet(newFloidPinSet);
                newFloidPinSet.getPinList().add(newFloidPin);
            }
            session.saveOrUpdate(newFloidPinSet);
            session.getTransaction().commit();
            session.close();
            returnPinSetId = newFloidPinSet.getId();
        } catch (HibernateException e) {
            staticLogger.error("FloidServerRemoting:duplicatePinSet: Caught Exception", e);
            try {
                session.close();
            } catch (HibernateException e2) {
                // Do nothing...
            }
        }
        return returnPinSetId;
    }

    /**
     * Delete a floid id (all records) from the database
     *
     * @param floidId the floid id
     * @return true if any records  deleted
     */
    public static boolean deleteFloidId(Integer floidId) {
        // Delete the ID:
        if(FloidIdHelper.deleteFloidId(floidId)) {
            // And force a floid id status update:
            FloidServerFloidCurrentStatusMonitor.update();
            return true;
        }
        return false;
    }

    /**
     * Execute a mission on a floid over a pin set
     *
     * @param floidId   the floid id
     * @param missionId the mission id
     * @param pinSetId  the pin set id
     * @return the floid server command result
     */
    public static FloidServerCommandResult executeMission(int floidId, Long missionId, Long pinSetId) {
        FloidServerCommandResult floidServerCommandResult = new FloidServerCommandResult();
        floidServerCommandResult.setFloidId(floidId);
        floidServerCommandResult.setRemotingSuccess(true);

        // 1. Get the mission:
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        DroidMission mission;
        try {
            mission = session.get(DroidMission.class, missionId);
        } catch (HibernateException e2) {
            try {
                staticLogger.error("FloidServerRemoting:executeMission: Caught Exception", e2);
                session.close();
            } catch (HibernateException e3) {
                staticLogger.error("FloidServerRemoting:executeMission: Caught Exception", e3);
            }
            floidServerCommandResult.setResultMessage(ERROR_MESSAGE_FAILED_TO_EXECUTE_MISSION);
            floidServerCommandResult.setResult(ERROR_CODE_FAILED_TO_EXECUTE_MISSION);
            return floidServerCommandResult;
        }
        if (mission == null) {
            try {
                staticLogger.error("FloidServerRemoting:executeMission: Mission null");
                session.close();
            } catch (HibernateException e3) {
                staticLogger.error("FloidServerRemoting:executeMission: Caught Exception", e3);
            }
            floidServerCommandResult.setResultMessage(ERROR_MESSAGE_FAILED_TO_LOAD_MISSION);
            floidServerCommandResult.setResult(ERROR_CODE_FAILED_TO_LOAD_MISSION);
            return floidServerCommandResult;
        }
        floidServerCommandResult.setDroidInstruction(mission);
        // 2. Get the pin set
        FloidPinSet floidPinSet;
        try {
            floidPinSet = session.get(FloidPinSet.class, pinSetId); //pins.FloidPinSet, pinSetId);
        } catch (HibernateException e) {
            try {
                session.close();
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:executeMission: Caught Exception", e2);
            }
            floidServerCommandResult.setResultMessage(ERROR_MESSAGE_FAILED_TO_RETRIEVE_PIN_SET);
            floidServerCommandResult.setResult(ERROR_CODE_FAILED_TO_RETRIEVE_PIN_SET);
            return floidServerCommandResult;
        }
        try {
            session.close();
        } catch (HibernateException e) {
            floidServerCommandResult.setResultMessage(ERROR_MESSAGE_DB_INTERACTION_FAILED);
            floidServerCommandResult.setResult(ERROR_CODE_DB_INTERACTION_FAILED);
            return floidServerCommandResult;
        }
        if (floidPinSet == null) {
            floidServerCommandResult.setResultMessage(ERROR_MESSAGE_FAILED_TO_LOAD_PIN_SET);
            floidServerCommandResult.setResult(ERROR_CODE_FAILED_TO_LOAD_PIN_SET);
            return floidServerCommandResult;
        }
        // 3. Check isGoForExecute()
        ArrayList<String> missionErrors = new ArrayList<>();
        ArrayList<String> missionWarnings = new ArrayList<>();
        // This sets up the mission errors:
        boolean goForExecute = isGoForExecute(mission, floidPinSet, missionErrors, missionWarnings);
        // Did we have any mission errors?
        if (!goForExecute || !missionErrors.isEmpty()) {
            // We have some errors - return them in a string:
            floidServerCommandResult.setResultMessage(ERROR_MESSAGE_MISSION_EXECUTE_FAILED_WITH_ERRORS + "\n" + missionErrors + missionWarnings);
            floidServerCommandResult.setResult(ERROR_CODE_MISSION_EXECUTE_FAILED_WITH_ERRORS);
            return floidServerCommandResult;
        }
        // 4. Update the mission with the x, y and z parameters
        if (!updateMissionWithPinLocations(mission, floidPinSet)) {
            floidServerCommandResult.setResultMessage(ERROR_MESSAGE_FAILED_TO_UPDATE_PIN_SET);
            floidServerCommandResult.setResult(ERROR_CODE_FAILED_TO_UPDATE_PIN_SET);
            return floidServerCommandResult;
        }
        // 5. Execute:
        FloidServerControlServlet.sendInstruction(floidId, mission, floidServerCommandResult);
        if (!missionWarnings.isEmpty()) {
            floidServerCommandResult.setResultMessage(floidServerCommandResult.getResultMessage() + "\n" + ERROR_MESSAGE_MISSION_EXECUTE_WITH_WARNINGS + "\n" + missionWarnings);
        }
        return floidServerCommandResult;
    }

    private static boolean updateMissionWithPinLocations(DroidMission mission, FloidPinSet floidPinSet) {
        List<DroidInstruction> droidInstructionList = mission.getDroidInstructions();
        for (DroidInstruction currentInstruction : droidInstructionList) {
            if (currentInstruction.getClass() == DroidMission.class) {
                // Recurse
                DroidMission subMission = (DroidMission) currentInstruction;
                updateMissionWithPinLocations(subMission, floidPinSet);
            }
            if (currentInstruction.getClass() == FlyToCommand.class) {
                FlyToCommand flyToCommand = (FlyToCommand) currentInstruction;
                // If we are not flying to the acquired home, then use the pin - otherwise home is obtained by AcquireHomePosition in the processing...
                if (!flyToCommand.isFlyToHome()) {
                    FloidPin floidPin = getFloidPinFromName(flyToCommand.getPinName(), floidPinSet);
                    if (floidPin == null) {
                        return false;   // Fail
                    }
                    flyToCommand.setX(floidPin.getPinLng());
                    flyToCommand.setY(floidPin.getPinLat());
                }
            }
            if (currentInstruction.getClass() == DesignateCommand.class) {
                DesignateCommand designateCommand = (DesignateCommand) currentInstruction;
                if (designateCommand.isUsePin()) {
                    FloidPin floidPin = getFloidPinFromName(designateCommand.getPinName(), floidPinSet);
                    if (floidPin == null) {
                        return false;   // Fail
                    }
                    designateCommand.setX(floidPin.getPinLng());
                    designateCommand.setY(floidPin.getPinLat());
                    if (designateCommand.isUsePinHeight()) {
                        designateCommand.setZ(floidPin.getPinHeight());
                    }
                }
            }
            if (currentInstruction.getClass() == PanTiltCommand.class) {
                PanTiltCommand panTiltCommand = (PanTiltCommand) currentInstruction;
                if (!panTiltCommand.isUseAngle()) {
                    FloidPin floidPin = getFloidPinFromName(panTiltCommand.getPinName(), floidPinSet);
                    if (floidPin == null) {
                        return false;   // Fail
                    }
                    panTiltCommand.setX(floidPin.getPinLng());
                    panTiltCommand.setY(floidPin.getPinLat());
                    panTiltCommand.setZ(floidPin.getPinHeight());
                    panTiltCommand.setPan(0);
                    panTiltCommand.setTilt(0);
                } else {
                    // All info is in the pan/tilt angle:
                    panTiltCommand.setPinName("");  // Empty these - no point sending..
                    panTiltCommand.setX(0);
                    panTiltCommand.setY(0);
                    panTiltCommand.setZ(0);
                }
            }
        }
        return true;    // No errors;
    }

    private static FloidPin getFloidPinFromName(String pinName, FloidPinSet floidPinSet) {
        List<FloidPin> floidPinList = floidPinSet.getPinList();
        for (FloidPin floidPin : floidPinList) {
            if (floidPin.getPinName().equals(pinName)) {
                return floidPin;
            }
        }
        return null;
    }

    /**
     * Is this mission a go for execute over this pinset
     *
     * @param mission         the mission
     * @param floidPinSet     the pinset
     * @param missionErrors   an accumulating list of mission errors
     * @param missionWarnings an accumulating list of mission warnings
     * @return true if go for execute
     */
    private static boolean isGoForExecute(DroidMission mission, FloidPinSet floidPinSet, ArrayList<String> missionErrors, ArrayList<String> missionWarnings) {
        // The structure:
        //  We build an array of errors - if the array is not empty we return false...
        if (mission == null) {
            missionErrors.add("Empty mission");
        } else {
            if (mission.getDroidInstructions().isEmpty()) {
                missionErrors.add("Empty mission");
            }
        }
        if (floidPinSet.getPinList().isEmpty()) {
            missionWarnings.add("WARNING: Empty pin set");
        }
        return missionErrors.isEmpty() || checkMissionForErrors(mission, floidPinSet, missionErrors, missionWarnings);
    }

    private static List<DroidInstruction> flattenMission(DroidMission mission) {
        ArrayList<DroidInstruction> flatMissionInstructions = new ArrayList<>();
        List<DroidInstruction> missionInstructions = mission.getDroidInstructions();
        for (DroidInstruction droidInstruction : missionInstructions) {
            if (droidInstruction.getClass() == DroidMission.class) {
                DroidMission subMission = (DroidMission) droidInstruction;
                flatMissionInstructions.addAll(flattenMission(subMission));
            } else {
                flatMissionInstructions.add(droidInstruction);
            }
        }
        return flatMissionInstructions;
    }

    private static boolean checkMissionForErrors(DroidMission mission, FloidPinSet floidPinSet, ArrayList<String> missionErrors, ArrayList<String> missionWarnings) {
        // Here we need to check mission only items, in addition to pin-names and heights, such as:
        // Must start with SetParameters, then an optional delay, then acquire home position
        // Must have lift-offs and lands in order
        // Must have a start and stop video in order
        // Must have designate and stop designating in order
        // Must stop designating before landing
        int initialErrorSize = missionErrors.size();    // Store original size - if we added any then there were errors...
        boolean spOk = false;
        boolean ahpOk = false;
        boolean hasSetParameters = false;
        boolean helisOn = false;
        boolean photoStrobeOn = false;
        boolean isLiftedOff = false;
        boolean videoIsOn = false;
        boolean payloadBay0Used = false;
        boolean payloadBay1Used = false;
        boolean payloadBay2Used = false;
        boolean payloadBay3Used = false;
        // Get a flat copy of the mission:
        List<DroidInstruction> flatMission = flattenMission(mission);
        // Check that setParameters is first item:
        if (flatMission.size() > 0) {
            if (flatMission.get(0).getClass() == SetParametersCommand.class) {
                spOk = true;
                SetParametersCommand setParametersCommand = (SetParametersCommand) flatMission.get(0);
                if (setParametersCommand.getDroidParameters().contains("mission.skip_check=true")) {
                    staticLogger.info("FloidServerRemoting:checkMissionForErrors: mission.skip_check=true - skipping checks...");
                    return true; // We are all good and no need to check - log this though.
                }
            }
        }
        if (!spOk) {
            missionErrors.add("REQUIRED: First item must be SetParameters");
        }
        // Check that acquireHomePosition is second item:
        if (flatMission.size() > 1) {
            if (flatMission.get(1).getClass() == AcquireHomePositionCommand.class) {
                ahpOk = true;
            }
        }
        if (!ahpOk) {
            missionErrors.add("REQUIRED: Second item must be AcquireHomePosition");
        }
        // OK, get the home pin and heights, etc.
        // We will need to store the current pin..in order to know original height for lift off
        double currentHeight = 0.0;
        boolean currentHeightValid = false;
        FloidPin homePin = getHomePin(mission, floidPinSet);
        FloidPin currentPin = homePin;
        if (currentPin != null) {
            currentHeight = currentPin.getPinHeight();
            currentHeightValid = true;
        }
        if (homePin == null) {
            missionWarnings.add("WARNING: No home pin");
        }
        double newHeight = 0;    // Placeholder for new height
        // OK, now check pin names (setParameters, flyTo, designate, panTilt, liftOff/land, start/stopVideo and video v photo)
        for (DroidInstruction droidInstruction : flatMission) {
            // OK, check the type of the node:
            if (droidInstruction.getClass() == SetParametersCommand.class) {
                // Do we already have one?
                if (hasSetParameters) {
                    missionErrors.add("ERROR: Duplicate SetParameters");
                } else {
                    hasSetParameters = true;
                    // Home pin was already checked above - do not do anything again...
                }
            } else if (droidInstruction.getClass() == FlyPatternCommand.class) {
                missionErrors.add("NOT IMPLEMENTED: FlyPattern not implemented");
            } else if (droidInstruction.getClass() == FlyToCommand.class) {
                FlyToCommand flyToCommand = (FlyToCommand) droidInstruction;
                boolean flyToGoodPin = false;
                String flyToPinName = flyToCommand.getPinName();
                FloidPin flyToPin = null;
                if (flyToPinName != null) {
                    if (flyToCommand.isFlyToHome()) {
                        flyToPin = getHomePin(mission, floidPinSet);
                    } else {
                        flyToPin = getFloidPinFromName(flyToPinName, floidPinSet);
                    }
                    if (flyToPin != null) {
                        // Pin was good
                        flyToGoodPin = true;
                        // Now check the height:
                        if (currentHeightValid) {
                            if (flyToPin.getPinHeight() >= currentHeight) {
                                missionErrors.add("ERROR: FlyTo - Flight below pin level");
                            }
                        } else {
                            // Bad current height - add warning:
                            missionWarnings.add("WARNING: FlyTo cannot verify height");
                        }
                    }
                }
                currentPin = flyToPin;
                if (!flyToGoodPin) {
                    missionErrors.add("Error: FlyTo pin name: " + flyToPinName);
                }
            } else if (droidInstruction.getClass() == DesignateCommand.class) {
                DesignateCommand designateCommand = (DesignateCommand) droidInstruction;
                String designatePinName = designateCommand.getPinName();
                FloidPin designatePin = getFloidPinFromName(designatePinName, floidPinSet);
                if (designatePin == null) {
                    // Bad designate pin:
                    missionErrors.add("Error: Designate pin name: " + designatePinName);
                }
            } else if (droidInstruction.getClass() == HoverCommand.class) {
                if (!isLiftedOff) {
                    missionErrors.add(ERROR_HOVER_ONLY_AFTER_LIFT_OFF);
                }
            } else if (droidInstruction.getClass() == DelayCommand.class) {
                if (isLiftedOff) {
                    missionErrors.add(ERROR_DELAY_ONLY_ON_GROUND);
                }
            } else if (droidInstruction.getClass() == PhotoStrobeCommand.class) {
                if (photoStrobeOn) {
                    missionErrors.add(ERROR_PHOTO_STROBE_ALREADY_ON);
                } else {
                    photoStrobeOn = true;
                }
            } else if (droidInstruction.getClass() == StopPhotoStrobeCommand.class) {
                if (photoStrobeOn) {
                    photoStrobeOn = false;
                } else {
                    missionErrors.add(ERROR_STOP_PHOTO_STROBE_WHEN_NO_STARED);
                }
            } else if (droidInstruction.getClass() == PanTiltCommand.class) {
                PanTiltCommand panTiltCommand = (PanTiltCommand) droidInstruction;
                // Check either angles or pinName:
                //noinspection StatementWithEmptyBody
                if (panTiltCommand.isUseAngle()) {
                    // TODO [PAN TILT] CHECK THAT THESE ANGLES ARE IN RANGE.....ONCE I KNOW WHAT THE SERVOS WILL DO...THEN REMOVE THE WARNING SUPPRESSION ABOVE
                } else {
                    // Check the pinName
                    String panTiltPinName = panTiltCommand.getPinName();
                    FloidPin panTiltPin = getFloidPinFromName(panTiltPinName, floidPinSet);
                    if (panTiltPin == null) {
                        // Bad pan/tilt pin:
                        missionErrors.add(ERROR_PAN_TILT_PIN_NAME + panTiltPinName);
                    }
                }
            } else if (droidInstruction.getClass() == LiftOffCommand.class) {
                LiftOffCommand liftOffCommand = (LiftOffCommand) droidInstruction;
                newHeight = liftOffCommand.getZ();
                if (liftOffCommand.isAbsolute()) {
                    newHeight = liftOffCommand.getZ();
                    currentHeightValid = true;
                } else {
                    if (currentHeightValid) {
                        newHeight = currentHeight + liftOffCommand.getZ();
                    }
                    if (liftOffCommand.getZ() < FLOID_LIFT_OFF_TEST_MIN_ALTITUDE) {
                        missionErrors.add(ERROR_LIFT_OFF_RELATIVE_HEIGHT_MUST_BE + FLOID_LIFT_OFF_TEST_MIN_ALTITUDE + METERS);
                        currentHeightValid = false;
                    }
                }
                if (currentHeightValid) {
                    if (currentPin != null) {
                        if (currentPin.getPinHeight() >= newHeight) {
                            missionErrors.add(ERROR_LIFT_OFF_BELOW_PIN_HEIGHT);
                        }
                    } else {
                        missionErrors.add(ERROR_LIFT_OFF_CANNOT_CHECK_PIN_HEIGHT);
                    }
                } else {
                    missionWarnings.add(WARNING_LIFT_OFF_CANNOT_CHECK_PIN_HEIGHT);
                }
                currentHeight = newHeight;
                if (!helisOn) {
                    missionErrors.add(ERROR_LIFT_OFF_WITHOUT_START_HELIS);
                }
                if (isLiftedOff) {
                    missionErrors.add(ERROR_ALREADY_PERFORMED_LIFT_OFF);
                }
                isLiftedOff = true;
            } else if (droidInstruction.getClass() == HeightToCommand.class) {
                HeightToCommand heightToCommand = (HeightToCommand) droidInstruction;
                if (heightToCommand.isAbsolute()) {
                    newHeight = heightToCommand.getZ();
                    currentHeightValid = true;
                } else {
                    if (currentHeightValid) {
                        newHeight = currentHeight + heightToCommand.getZ();
                    }
                }
                if (currentHeightValid) {
                    if (currentPin != null) {
                        if (currentPin.getPinHeight() >= newHeight) {
                            missionErrors.add(ERROR_HEIGHT_TO_BELOW_PIN_HEIGHT);
                        }
                    }
                } else {
                    missionWarnings.add(WARNING_HEIGHT_TO_CANNOT_CHECK_PIN_HEIGHT);
                }
                // Check that we are lifted off
                if (!isLiftedOff) {
                    missionErrors.add(ERROR_HEIGHT_TO_WITHOUT_LIFT_OFF);
                }
                currentHeight = newHeight;
                currentHeightValid = true;
            } else if (droidInstruction.getClass() == TurnToCommand.class) {
                // Check that we have lifted off
                if (!isLiftedOff) {
                    missionErrors.add(ERROR_TURN_TO_WITHOUT_LIFT_OFF);
                }
                isLiftedOff = false;
            } else if (droidInstruction.getClass() == LandCommand.class) {
                // Check that we have lifted off
                if (!isLiftedOff) {
                    missionErrors.add(ERROR_LAND_WITHOUT_LIFT_OFF);
                }
                isLiftedOff = false;
            } else if (droidInstruction.getClass() == StartUpHelisCommand.class) {
                // Check that the helis are not yet started
                if (helisOn) {
                    missionErrors.add(ERROR_HELIS_ALREADY_STARTED);
                }
                helisOn = true;
            } else if (droidInstruction.getClass() == ShutDownHelisCommand.class) {
                // Check that the helis are started
                // Check that we are not lifted off
                if (!helisOn) {
                    missionErrors.add(ERROR_HELIS_SHUT_DOWN_WITHOUT_START_UP);
                }
                if (isLiftedOff) {
                    missionErrors.add(ERROR_HELIS_SHUT_DOWN_DURING_FLIGHT);
                }
                helisOn = false;
            } else if (droidInstruction.getClass() == StartVideoCommand.class) {
                // Check that the video has not started
                if (videoIsOn) {
                    missionErrors.add(ERROR_VIDEO_ALREADY_STARTED);
                }
                videoIsOn = true;
            } else if (droidInstruction.getClass() == StopVideoCommand.class) {
                // Check that the video has started
                if (!videoIsOn) {
                    missionErrors.add(ERROR_VIDEO_NOT_STARTED);
                }
                videoIsOn = false;
            } else if (droidInstruction.getClass() == TakePhotoCommand.class) {
                // Check that the video has not started
                if (videoIsOn) {
                    missionWarnings.add(WARNING_TAKE_PHOTO_WITH_VIDEO_STARTED_NOT_SUPPORTED_ON_ALL_DEVICES);
                }
            } else if (droidInstruction.getClass() == PayloadCommand.class) {
                PayloadCommand payloadCommand = (PayloadCommand) droidInstruction;
                // Check that we have lifted off:
                if (!isLiftedOff) {
                    missionErrors.add(ERROR_PAYLOAD_MUST_LIFT_OFF_FIRST);
                }
                // Check the bay:
                int bayNumber = payloadCommand.getBay();
                switch (bayNumber) {
                    case 0:
                        if (payloadBay0Used) {
                            missionErrors.add(ERROR_PAYLOAD_BAY_ALREADY_AWAY);
                        }
                        payloadBay0Used = true;
                        break;
                    case 1:
                        if (payloadBay1Used) {
                            missionErrors.add("PAYLOAD: Bay 1 already away");
                        }
                        payloadBay1Used = true;
                        break;
                    case 2:
                        if (payloadBay2Used) {
                            missionErrors.add("PAYLOAD: Bay 2 already away");
                        }
                        payloadBay2Used = true;
                        break;
                    case 3:
                        if (payloadBay3Used) {
                            missionErrors.add("PAYLOAD: Bay 3 already away");
                        }
                        payloadBay3Used = true;
                        break;
                    default:
                        missionErrors.add(ERROR_PAYLOAD_INVALID_BAY_NUMBER);
                        break;
                }
            }
        }
        // Finally, we need to make sure the heli landed if took off, turned off video if on, stopped helis if started, etc. etc.
        if (isLiftedOff) {
            missionErrors.add(ERROR_FLIGHT_DOES_NOT_END_ON_GROUND);
        }
        if (helisOn) {
            missionErrors.add(ERROR_HELIS_ARE_NOT_TURNED_OFF);
        }
        if (videoIsOn) {
            missionErrors.add(ERROR_VIDEO_NOT_STOPPED);
        }
        if (photoStrobeOn) {
            missionErrors.add(ERROR_PHOTO_STROBE_NOT_STOPPED);
        }
        return missionErrors.size() != initialErrorSize;
    }

    private static FloidPin getHomePin(DroidMission mission, FloidPinSet floidPinSet) {
        if (mission == null) {
            return null;
        }
        if (floidPinSet == null) {
            return null;
        }

        // Find the first "SetParameters", then see if the home pin is named or is the first pin.
        SetParametersCommand firstSetParametersCommand = getFirstSetParametersCommand(mission);
        if (firstSetParametersCommand != null) {
            if (firstSetParametersCommand.isFirstPinIsHome()) {
                return floidPinSet.getPinList().get(0);     // First pin is home...
            } else {
                return getFloidPinFromName(firstSetParametersCommand.getHomePinName(), floidPinSet);
            }
        }
        return null;
    }

    private static SetParametersCommand getFirstSetParametersCommand(DroidMission mission) {
        List<DroidInstruction> missionDroidInstructions = mission.getDroidInstructions();
        for (DroidInstruction droidInstruction : missionDroidInstructions) {
            if (droidInstruction.getClass() == SetParametersCommand.class) {
                return (SetParametersCommand) droidInstruction;
            }
            if (droidInstruction.getClass() == DroidMission.class) {
                SetParametersCommand subMissionSetParameters = getFirstSetParametersCommand((DroidMission) droidInstruction);
                if (subMissionSetParameters != null) {
                    return subMissionSetParameters;
                }
            }
        }
        return null;        // Not found
    }

    /**
     * Get the next floid status message id
     *
     * @return the next floid status message id
     */
    private static synchronized int getFloidStatusMessageID() {
        return floidStatusMessageID++;
    }

    /**
     * Get the next floid mission instruction status message id
     *
     * @return the next floid mission instruction status message id
     */
    private static synchronized int getFloidMissionInstructionStatusMessageID() {
        return floidMissionInstructionStatusMessageID++;
    }

    /**
     * Get the next floid model status message id
     *
     * @return the next floid model status message id
     */
    private static synchronized int getFloidModelStatusMessageID() {
        return floidModelStatusMessageID++;
    }

    /**
     * Get the next floid model parameters message id
     *
     * @return the next floid model parameters message id
     */
    private static synchronized int getFloidModelParametersMessageID() {
        return floidModelParametersMessageID++;
    }

    /**
     * Get the next floid droid status message id
     *
     * @return the next floid droid status message id
     */
    private static synchronized int getFloidDroidStatusMessageID() {
        return floidDroidStatusMessageID++;
    }

    /**
     * Get the next floid droid message id
     *
     * @return the next floid droid message id
     */
    private static synchronized int getFloidDroidMessageID() {
        return floidDroidMessageID++;
    }

    /**
     * Get the next floid droid photo message id
     *
     * @return the next floid droid photo message id
     */
    private static synchronized int getFloidDroidPhotoMessageID() {
        return floidDroidPhotoMessageID++;
    }

    /**
     * Get the next floid droid video preview frame message id
     *
     * @return the next floid droid video preview frame message id
     */
    private static synchronized int getFloidDroidVideoFrameMessageID() {
        return floidDroidVideoFrameMessageID++;
    }

    /**
     * Get the next floid id message id
     *
     * @return the next floid id message id
     */
    private static synchronized int getFloidNewFloidIdMessageID() {
        return floidNewFloidIdMessageID++;
    }


    /**
     * Get the most recent floid droid status for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @return the most recent floid droid status or null
     */
    public static FloidDroidStatus getLastFloidDroidStatus(int floidId, String floidUuid) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        FloidDroidStatus floidDroidStatus = null;
        try {
            Query query = session.createQuery("select new FloidDroidStatus(fds.id, fds.floidId, fds.floidUuid, fds.floidMessageNumber, fds.timestamp, fds.statusTime,  fds.statusNumber,  fds.gpsStatusTime,  fds.gpsHasAccuracy,  fds.gpsAccuracy,  fds.gpsLatitude,  fds.gpsLongitude,  fds.gpsHasAltitude,  fds.gpsAltitude,  fds.gpsHasBearing,  fds.gpsBearing,  fds.gpsSpeed,  fds.gpsProvider,  fds.floidConnected,  fds.floidError,  fds.currentStatus,  fds.currentMode,  fds.missionNumber,  fds.droidStarted,  fds.serverConnected,  fds.serverError,  fds.batteryVoltage,  fds.batteryLevel,  fds.batteryRawLevel,  fds.batteryRawScale,  fds.helisStarted,  fds.liftedOff,  fds.parachuteDeployed,  fds.takingPhoto,  fds.photoNumber,  fds.photoStrobe,  fds.photoStrobeDelay,  fds.takingVideo,  fds.videoNumber,  fds.homePositionAcquired,  fds.homePositionX,  fds.homePositionY,  fds.homePositionZ,  fds.homePositionHeading) from FloidDroidStatus fds where floidId=:floidId and floidUuid=:floidUuid order by statusNumber desc");
            query.setMaxResults(1);
            query.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            query.setParameter(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
            List list = query.list();
            if(list.size() >0) {
                floidDroidStatus = (FloidDroidStatus)list.get(0);
            }
            session.close();
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getLastFloidDroidStatus: Caught Exception: " + e);
                session.close();
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getLastFloidDroidStatus: Caught Exception: " + e2);
            }
            throw e;
        }
        return floidDroidStatus;
    }

    /**
     * Get the most recent floid status for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @return the most recent floid status or null
     */
    public static FloidStatus getLastFloidStatus(int floidId, String floidUuid) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        FloidStatus floidStatus = null;
        try {
            final NativeQuery sqlQuery = session.createSQLQuery("select fs.type, fs.timestamp, fs.floidId, fs.floidUuid, fs.floidMessageNumber, fs.std,  fs.st,  fs.sn,  fs.fm,  fs.ffm,  fs.fmf,  fs.dc,  fs.dg,  fs.df,  fs.dv,  fs.dp,  fs.dd,  fs.da,  fs.dl,  fs.dh,  fs.de,  fs.kd,  fs.kv,  fs.ka,  fs.kw,  fs.kc,  fs.jt,  fs.jx,  fs.jy,  fs.jf,  fs.jm,  fs.jz,  fs.jh,  fs.js,  fs.jxf,  fs.jyf,  fs.jmf,  fs.jg,  fs.jr,  fs.ph,  fs.phs,  fs.phv,  fs.pz,  fs.pzs,  fs.pt,  fs.pp,  fs.pps,  fs.pv,  fs.pr,  fs.prs,  fs.ppv,  fs.prv,  fs.pzv,  fs.pd,  fs.par,  fs.pg,  fs.pe,  fs.bm,  fs.b0,  fs.b1,  fs.b2,  fs.b3,  fs.ba,  fs.c0,  fs.c1,  fs.c2,  fs.c3,  fs.ca,  fs.s0o,  fs.s1o,  fs.s2o,  fs.s3o,  fs.s00,  fs.s01,  fs.s02,  fs.s10,  fs.s11,  fs.s12,  fs.s20,  fs.s21,  fs.s22,  fs.s30,  fs.s31,  fs.s32,  fs.et,  fs.e0o,  fs.e1o,  fs.e2o,  fs.e3o,  fs.e0,  fs.e1,  fs.e2,  fs.e3,  fs.ep0,  fs.ep1,  fs.ep2,  fs.ep3,  fs.ghg,  fs.gid,  fs.gt,  fs.gs,  fs.gx,  fs.gy,  fs.gz,  fs.ga,  fs.gif,  fs.glo,  fs.gld,  fs.t0m,  fs.t1m,  fs.t2m,  fs.t3m,  fs.t0x,  fs.t0y,  fs.t0z,  fs.t1x,  fs.t1y,  fs.t1z,  fs.t2x,  fs.t2y,  fs.t2z,  fs.t3x,  fs.t3y,  fs.t3z,  fs.t0o,  fs.t1o,  fs.t2o,  fs.t3o,  fs.t0p,  fs.t0t,  fs.t1p,  fs.t1t,  fs.t2p,  fs.t2t,  fs.t3p,  fs.t3t,  fs.y0,  fs.y1,  fs.y2,  fs.y3,  fs.yn0,  fs.yn1,  fs.yn2,  fs.yn3,  fs.yg0,  fs.yg1,  fs.yg2,  fs.yg3,  fs.vd,  fs.va,  fs.vx,  fs.vb,  fs.vc,  fs.ve,  fs.vg,  fs.vh,  fs.vm,  fs.vt,  fs.vy,  fs.vp,  fs.vl,  fs.vs,  fs.ai,  fs.av,  fs.aa,  fs.ad,  fs.ac,  fs.lc,  fs.lr,  fs.sr,  fs.mt,  fs.md,  fs.ma,  fs.mo,  fs.mg,  fs.mk,  fs.oop,  fs.ooq,  fs.oor,  fs.oos,  fs.opv,  fs.opw,  fs.orv,  fs.orw,  fs.oav,  fs.oaw,  fs.ohv,  fs.ohw,  fs.ov,  fs.lmc,  fs.lma,  fs.lmt,  fs.hsm,  fs.ias,  fs.dpy,  fs.dba,  fs.rm,  fs.tm from " + FloidDatabaseTables.FLOID_STATUS + " fs where floidId=:floidId and floidUuid=:floidUuid order by std desc");
            sqlQuery.setMaxResults(1);
            sqlQuery.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            sqlQuery.setParameter(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
            sqlQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            List<Map<String,Object>> aliasToValueMapList=sqlQuery.list();
            if(aliasToValueMapList.size()>0) {
                Map<String, Object> floidStatusMap = aliasToValueMapList.get(0);
                floidStatusMap.put("floidMessageNumber", ((BigInteger)floidStatusMap.get("floidMessageNumber")).longValue());
                floidStatusMap.put("std", ((BigInteger)floidStatusMap.get("std")).longValue());
                floidStatus = new FloidStatus().fromMap(floidStatusMap);
            }
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getLastFloidStatus: Caught Exception: " + e);
                session.close();
                session=null;
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getLastFloidStatus: Caught Exception: " + e2);
            }
            throw e;
        } finally {
            if(session!=null) {
                session.close();
            }
        }
        return floidStatus;
    }

    /**
     * Get the most recent floid model status for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @return the most recent floid model status or null
     */
    public static FloidModelStatus getLastFloidModelStatus(int floidId, String floidUuid) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        FloidModelStatus floidModelStatus = null;
        try {
            Query query = session.createQuery("select new FloidModelStatus(fms.id, fms.floidId, fms.floidUuid, fms.floidMessageNumber, fms.timestamp, fms.collectiveDeltaOrientationH0, fms.collectiveDeltaOrientationH1,fms.collectiveDeltaOrientationH2,fms.collectiveDeltaOrientationH3,fms.collectiveDeltaAltitudeH0,fms.collectiveDeltaAltitudeH1,fms.collectiveDeltaAltitudeH2,fms.collectiveDeltaAltitudeH3,fms.collectiveDeltaHeadingH0,fms.collectiveDeltaHeadingH1,fms.collectiveDeltaHeadingH2,fms.collectiveDeltaHeadingH3,fms.collectiveDeltaTotalH0,fms.collectiveDeltaTotalH1,fms.collectiveDeltaTotalH2,fms.collectiveDeltaTotalH3,fms.cyclicValueH0S0,fms.cyclicValueH0S1,fms.cyclicValueH0S2,fms.cyclicValueH1S0,fms.cyclicValueH1S1,fms.cyclicValueH1S2,fms.cyclicValueH2S0,fms.cyclicValueH2S1,fms.cyclicValueH2S2,fms.cyclicValueH3S0,fms.cyclicValueH3S1,fms.cyclicValueH3S2,fms.collectiveValueH0,fms.collectiveValueH1,fms.collectiveValueH2,fms.collectiveValueH3,fms.calculatedCyclicBladePitchH0S0,fms.calculatedCyclicBladePitchH0S1,fms.calculatedCyclicBladePitchH0S2,fms.calculatedCyclicBladePitchH1S0,fms.calculatedCyclicBladePitchH1S1,fms.calculatedCyclicBladePitchH1S2,fms.calculatedCyclicBladePitchH2S0,fms.calculatedCyclicBladePitchH2S1,fms.calculatedCyclicBladePitchH2S2,fms.calculatedCyclicBladePitchH3S0,fms.calculatedCyclicBladePitchH3S1,fms.calculatedCyclicBladePitchH3S2,fms.calculatedCollectiveBladePitchH0,fms.calculatedCollectiveBladePitchH1,fms.calculatedCollectiveBladePitchH2,fms.calculatedCollectiveBladePitchH3,fms.calculatedBladePitchH0S0,fms.calculatedBladePitchH0S1,fms.calculatedBladePitchH0S2,fms.calculatedBladePitchH1S0,fms.calculatedBladePitchH1S1,fms.calculatedBladePitchH1S2,fms.calculatedBladePitchH2S0,fms.calculatedBladePitchH2S1,fms.calculatedBladePitchH2S2,fms.calculatedBladePitchH3S0,fms.calculatedBladePitchH3S1,fms.calculatedBladePitchH3S2,fms.calculatedServoDegreesH0S0,fms.calculatedServoDegreesH0S1,fms.calculatedServoDegreesH0S2,fms.calculatedServoDegreesH1S0,fms.calculatedServoDegreesH1S1,fms.calculatedServoDegreesH1S2,fms.calculatedServoDegreesH2S0,fms.calculatedServoDegreesH2S1,fms.calculatedServoDegreesH2S2,fms.calculatedServoDegreesH3S0,fms.calculatedServoDegreesH3S1,fms.calculatedServoDegreesH3S2,fms.calculatedServoPulseH0S0,fms.calculatedServoPulseH0S1,fms.calculatedServoPulseH0S2,fms.calculatedServoPulseH1S0,fms.calculatedServoPulseH1S1,fms.calculatedServoPulseH1S2,fms.calculatedServoPulseH2S0,fms.calculatedServoPulseH2S1,fms.calculatedServoPulseH2S2,fms.calculatedServoPulseH3S0,fms.calculatedServoPulseH3S1,fms.calculatedServoPulseH3S2,fms.cyclicHeading, fms.velocityCyclicAlpha) from FloidModelStatus fms where floidId=:floidId and floidUuid=:floidUuid order by timestamp desc");
            query.setMaxResults(1);
            query.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            query.setParameter(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
            List list = query.list();
            if(list.size() >0) {
                floidModelStatus = (FloidModelStatus)list.get(0);
            }
            session.close();
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getLastFloidModelStatus: Caught Exception: " + e);
                session.close();
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getLastFloidModelStatus: Caught Exception: " + e2);
            }
            throw e;
        }
        return floidModelStatus;
    }
    /**
     * Get the most recent floid model parameters for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @return the most recent floid model parameters or null
     */
    public static FloidModelParameters getLastFloidModelParameters(int floidId, String floidUuid) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        FloidModelParameters floidModelParameters = null;
        try {
            Query query = session.createQuery("select new FloidModelParameters(fmp.id, fmp.floidId, fmp.floidUuid, fmp.floidMessageNumber, fmp.timestamp, fmp.bladesLow, fmp.bladesZero, fmp.bladesHigh, fmp.h0S0Low, fmp.h0S1Low, fmp.h0S2Low, fmp.h1S0Low, fmp.h1S1Low, fmp.h1S2Low, fmp.h0S0Zero, fmp.h0S1Zero, fmp.h0S2Zero, fmp.h1S0Zero, fmp.h1S1Zero, fmp.h1S2Zero, fmp.h0S0High, fmp.h0S1High, fmp.h0S2High, fmp.h1S0High, fmp.h1S1High, fmp.h1S2High, fmp.h2S0Low, fmp.h2S1Low, fmp.h2S2Low, fmp.h3S0Low, fmp.h3S1Low, fmp.h3S2Low, fmp.h2S0Zero, fmp.h2S1Zero, fmp.h2S2Zero, fmp.h3S0Zero, fmp.h3S1Zero, fmp.h3S2Zero, fmp.h2S0High, fmp.h2S1High, fmp.h2S2High, fmp.h3S0High, fmp.h3S1High, fmp.h3S2High, fmp.collectiveMin, fmp.collectiveMax, fmp.collectiveDefault, fmp.cyclicRange, fmp.cyclicDefault, fmp.escType, fmp.escPulseMin, fmp.escPulseMax, fmp.escLowValue, fmp.escHighValue, fmp.attackAngleMinDistanceValue, fmp.attackAngleMaxDistanceValue, fmp.attackAngleValue, fmp.pitchDeltaMinValue, fmp.pitchDeltaMaxValue, fmp.pitchTargetVelocityMaxValue, fmp.rollDeltaMinValue, fmp.rollDeltaMaxValue, fmp.rollTargetVelocityMaxValue, fmp.altitudeToTargetMinValue, fmp.altitudeToTargetMaxValue, fmp.altitudeTargetVelocityMaxValue, fmp.headingDeltaMinValue, fmp.headingDeltaMaxValue, fmp.headingTargetVelocityMaxValue, fmp.distanceToTargetMinValue, fmp.distanceToTargetMaxValue, fmp.distanceTargetVelocityMaxValue, fmp.orientationMinDistanceValue, fmp.liftOffTargetAltitudeDeltaValue, fmp.servoPulseMin, fmp.servoPulseMax, fmp.servoDegreeMin, fmp.servoDegreeMax, fmp.servoSignLeft, fmp.servoSignRight, fmp.servoSignPitch, fmp.servoOffsetLeft0, fmp.servoOffsetLeft1, fmp.servoOffsetLeft2, fmp.servoOffsetLeft3, fmp.servoOffsetRight0, fmp.servoOffsetRight1, fmp.servoOffsetRight2, fmp.servoOffsetRight3, fmp.servoOffsetPitch0, fmp.servoOffsetPitch1, fmp.servoOffsetPitch2, fmp.servoOffsetPitch3, fmp.targetVelocityKeepStill, fmp.targetVelocityFullSpeed, fmp.targetPitchVelocityAlpha, fmp.targetRollVelocityAlpha, fmp.targetHeadingVelocityAlpha, fmp.targetAltitudeVelocityAlpha, fmp.landModeTimeRequiredAtMinAltitude, fmp.startMode, fmp.rotationMode, fmp.cyclicHeadingAlpha, fmp.heliStartupModeNumberSteps, fmp.heliStartupModeStepTick, fmp.floidModelEscCollectiveCalcMidpoint, fmp.floidModelEscCollectiveLowValue, fmp.floidModelEscCollectiveMidValue, fmp.floidModelEscCollectiveHighValue, fmp.velocityDeltaCyclicAlphaScale, fmp.accelerationMultiplierScale) from FloidModelParameters fmp where floidId=:floidId and floidUuid=:floidUuid order by timestamp desc");
            query.setMaxResults(1);
            query.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            query.setParameter(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
            List list = query.list();
            if(list.size() >0) {
                floidModelParameters = (FloidModelParameters)list.get(0);
            }
            session.close();
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getLastFloidModelStatus: Caught Exception: " + e);
                session.close();
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getLastFloidModelStatus: Caught Exception: " + e2);
            }
            throw e;
        }
        return floidModelParameters;
    }

    /**
     * Get the most recent floid droid photo for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @return the most recent floid droid photo or null
     */
    public static FloidDroidPhoto getLastFloidDroidPhoto(int floidId, String floidUuid) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        FloidDroidPhoto floidDroidPhoto = null;
        try {
            Query query = session.createQuery("select new FloidDroidPhoto(fdp.floidId, fdp.floidUuid, fdp.imageName, fdp.imageType, fdp.imageExtension, fdp.latitude, fdp.longitude, fdp.altitude, fdp.pan, fdp.tilt) from FloidDroidPhoto fdp where floidId=:floidId and floidUuid=:floidUuid order by timestamp desc");
            query.setMaxResults(1);
            query.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            query.setParameter(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
            List list = query.list();
            if(list.size() >0) {
                floidDroidPhoto = (FloidDroidPhoto)list.get(0);
                String photoFileName = FloidServerImageLocation.getImagesLocation() + floidDroidPhoto.getImageName();
                staticLogger.info("Reading photo image file: " + photoFileName);
                try {
                    // Try to read the file from the server:
                    floidDroidPhoto.setImageBytes(FileUtils.readFileToByteArray(new File(photoFileName)));
                } catch (Exception e) {
                    staticLogger.warn("FloidServerRemoting:getLastFloidDroidPhoto: Failed to read image file: " + photoFileName + ": " + e);
                }
            }
            session.close();
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getLastFloidDroidPhoto: Caught Exception: " + e);
                session.close();
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getLastFloidDroidPhoto: Caught Exception: " + e2);
            }
            throw e;
        }
        return floidDroidPhoto;
    }

    /**
     * Get the most recent floid droid video frame for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @return the most recent floid droid video frame or null
     */
    public static FloidDroidVideoFrame getLastFloidDroidVideoFrame(int floidId, String floidUuid) {
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        FloidDroidVideoFrame floidDroidVideoFrame = null;
        try {
            Query query = session.createQuery("select new FloidDroidVideoFrame(fdvf.floidId, fdvf.floidUuid, fdvf.imageName, fdvf.imageType, fdvf.imageExtension, fdvf.latitude, fdvf.longitude, fdvf.altitude, fdvf.pan, fdvf.tilt) from FloidDroidVideoFrame fdvf where floidId=:floidId and floidUuid=:floidUuid order by timestamp desc");
            query.setMaxResults(1);
            query.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            query.setParameter(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
            List list = query.list();
            if(list.size() >0) {
                floidDroidVideoFrame = (FloidDroidVideoFrame)list.get(0);
                String videoFrameFileName = FloidServerImageLocation.getImagesLocation() + floidDroidVideoFrame.getImageName();
                staticLogger.info("Reading video frame image file: " + videoFrameFileName);
                try {
                    // Try to read the file from the server:
                    floidDroidVideoFrame.setImageBytes(FileUtils.readFileToByteArray(new File(videoFrameFileName)));
                } catch (Exception e) {
                    staticLogger.warn("FloidServerRemoting:getLastFloidDroidVideoFrame: Failed to read image file: " + videoFrameFileName + ": "+ e);
                }
            }
            session.close();
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getLastFloidDroidVideoFrame: Caught Exception: " + e);
                session.close();
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getLastFloidDroidVideoFrame: Caught Exception: " + e2);
            }
            throw e;
        }
        return floidDroidVideoFrame;
    }
    /**
     * Get the current floid gps points for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @return the list of floid gps points
     */
    public static ArrayList<FloidGpsPoint> getGpsPoints(int floidId, String floidUuid) {
        // Get the list of gps points for this floidUuid:
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
        ArrayList<FloidGpsPoint> gpsPointList = new ArrayList<>();
        try {
            final NativeQuery sqlQuery = session.createSQLQuery("select fs.floidId, fs.floidUuid, fs.std, fs.jyf, fs.jxf,fs.ka from " + FloidDatabaseTables.FLOID_STATUS + " fs where floidId=:floidId and floidUuid=:floidUuid and jt>0 and dg=1 and jxf!=0 and jyf!=0 and std>0");
            sqlQuery.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            sqlQuery.setParameter(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
            sqlQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            List<Map<String,Object>> floidGpsPointList=sqlQuery.list();
            floidGpsPointList.forEach(floidGpsPointMap -> {
                gpsPointList.add(new FloidGpsPoint(floidId, floidUuid, ((BigInteger)floidGpsPointMap.get("std")).longValue(), (Double)floidGpsPointMap.get("jyf"), (Double)floidGpsPointMap.get("jxf"), (Double)floidGpsPointMap.get("ka")));
            });
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getGpsPoints: Caught Exception: " + e);
                session.close();
                session=null;
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getGpsPoints: Caught Exception: " + e2);
            }
            throw e;
        } finally {
            if(session!=null) {
                session.close();
            }
        }
        return gpsPointList;
    }

    /**
     * Get the current droid gps points for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @return the list of droid gps points
     */
    public static ArrayList<FloidGpsPoint> getDroidGpsPoints(int floidId, String floidUuid) {
        // Get the list of droid gps points for this floidUuid:
        Session session = FloidServerHibernateUtil.getSessionFactory().openSession();

        ArrayList<FloidGpsPoint> gpsPointList = new ArrayList<>();
        try {
            final NativeQuery sqlQuery = session.createSQLQuery("select fds.floidId, fds.floidUuid, fds.statusTime, fds.gpsLatitude, fds.gpsLongitude,fds.gpsAltitude from " + FloidDatabaseTables.FLOID_DROID_STATUS + " fds where floidId=:floidId and floidUuid=:floidUuid");
            sqlQuery.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            sqlQuery.setParameter(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
            sqlQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
            List<Map<String,Object>> droidGpsPointList=sqlQuery.list();
            droidGpsPointList.forEach(droidGpsPointMap -> {
                //statusTime, gpsLatitude, gpsLongitude, gpsAltitude
                gpsPointList.add(new FloidGpsPoint(floidId, floidUuid, ((BigInteger)droidGpsPointMap.get("statusTime")).longValue(), (Double)droidGpsPointMap.get("gpsLatitude"), (Double)droidGpsPointMap.get("gpsLongitude"), (Double)droidGpsPointMap.get("gpsAltitude")));
            });
        } catch (Exception e) {
            try {
                staticLogger.error("FloidServerRemoting:getDroidGpsPoints: Caught Exception: " + e);
                session.close();
                session=null;
            } catch (HibernateException e2) {
                staticLogger.error("FloidServerRemoting:getDroidGpsPoints: Caught Exception: " + e2);
            }
            throw e;
        } finally {
            if(session!=null) {
                session.close();
            }
        }
        return gpsPointList;
    }

    /**
     * Get the current floid gps points for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @param maxPoints the maximum number of points to return - 0 = no max
     * @param minTime the minimum time (milliseconds) between points for filtering - 0 = no filter
     * @param minDistance the minimum distance (meters) between points for filtering
     * @param maxKink the minimum kink distance (meters) from point to line
     * @param maxKinkAlpha the alpha applied to maxKink on successive iterations if maxPoints was not met on first pass
     * @return the filtered list of droid gps points
     */
    public static ArrayList<FloidGpsPoint> getGpsPointsFiltered(int floidId,
                                                                String floidUuid,
                                                                int maxPoints,
                                                                long minTime,
                                                                double minDistance,
                                                                double maxKink,
                                                                double maxKinkAlpha) {
        // Get the list of droid gps points for this floidUuid and filter::
        return filterGpsPoints(getGpsPoints(floidId, floidUuid), maxPoints, minTime, minDistance, maxKink, maxKinkAlpha);
    }
    /**
     * Get the current droid gps points for the floid id and uuid
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @param maxPoints the maximum number of points to return - 0 = no max
     * @param minTime the minimum time (milliseconds) between points for filtering - 0 = no filter
     * @param minDistance the minimum distance (meters) between points for filtering
     * @param maxKink the minimum kink distance (meters) from point to line
     * @param maxKinkAlpha the alpha applied to maxKink on successive iterations if maxPoints was not met on first pass
     * @return the filtered list of droid gps points
     */
    public static ArrayList<FloidGpsPoint> getDroidGpsPointsFiltered(int floidId,
                                                                     String floidUuid,
                                                                     int maxPoints,
                                                                     long minTime,
                                                                     double minDistance,
                                                                     double maxKink,
                                                                     double maxKinkAlpha) {
        // Get the list of droid gps points for this floidUuid and filter::
        return filterGpsPoints(getDroidGpsPoints(floidId, floidUuid), maxPoints, minTime, minDistance, maxKink, maxKinkAlpha);
    }

    /**
     * Filter a list of gps points with the given parameters
     * @param gpsPointList the gps point list to filter
     * @param maxPoints the maximum number of points to return - 0 = no max
     * @param minTime the minimum time (milliseconds) between points for filtering - 0 = no filter
     * @param minDistance the minimum distance (meters) between points for filtering
     * @param maxKink the minimum kink distance (meters) from point to line
     * @param maxKinkAlpha the alpha applied to maxKink on successive iterations if maxPoints was not met on first pass
     * @return the filtered list of droid gps points
     */
    @SuppressWarnings("WeakerAccess")
    public static ArrayList<FloidGpsPoint> filterGpsPoints(ArrayList<FloidGpsPoint> gpsPointList,
                                                           int maxPoints,
                                                           long minTime,
                                                           double minDistance,
                                                           double maxKink,
                                                           double maxKinkAlpha) {
        staticLogger.debug("Filtering GPS points:");
        staticLogger.debug("  maxPoints:    " + maxPoints);
        staticLogger.debug("  minTime:      " + minTime);
        staticLogger.debug("  minDistance:  " + minDistance);
        staticLogger.debug("  maxKink:      " + maxKink);
        staticLogger.debug("  maxKinkAlpha: " + maxKinkAlpha);
        staticLogger.debug("  input size:   " + (gpsPointList!=null?gpsPointList.size():"NULL"));

        // Trivial cases:
        if(gpsPointList == null) {
            staticLogger.debug(" -- List null - returning empty list");
            return new ArrayList<>();
        }
        if(gpsPointList.size() < 3) {
            staticLogger.debug(" -- List less than three points");
            return gpsPointList;
        }
        // Filter time and distance:
        staticLogger.debug(" -- Filtering on time distance");
        ArrayList<FloidGpsPoint> gpsPointListTimeDistanceFiltered = filterGpsPointListTimeDistance(gpsPointList, minTime, minDistance);
        // Unlimited points? or are we short enough?
        if((maxPoints==0) || (gpsPointListTimeDistanceFiltered.size() <= maxPoints)) {
            staticLogger.debug("++ List is small enough - returning");
            return gpsPointListTimeDistanceFiltered;
        }
        // Yes - filter:
        // First, project the points onto mercator grid:
        ArrayList<FloidGpsPointMercator> mercatorGpsPointList = new ArrayList<>();
        for(FloidGpsPoint floidGpsPoint:gpsPointListTimeDistanceFiltered) {
            mercatorGpsPointList.add(new FloidGpsPointMercator(floidGpsPoint));
        }
        ArrayList<FloidGpsPointMercator> filteredMercatorGpsPointList;
        int reductionRound = 0;
        do {
            double kinkMultiplier = Math.pow(1 + maxKinkAlpha, reductionRound);
            double currentMaxKink = maxKink * kinkMultiplier;
            staticLogger.debug(" Simplifying to max kink of: " + currentMaxKink + "m");
            filteredMercatorGpsPointList = filterMercatorGpsPointListKink(mercatorGpsPointList, currentMaxKink);
            staticLogger.debug(" List was simplified to a size of: " + filteredMercatorGpsPointList.size());
//            staticLogger.debug("   Mercator:");
//            for(FloidGpsPointMercator floidGpsPointMercator:filteredMercatorGpsPointList) {
//                staticLogger.debug("      " + floidGpsPointMercator.getLatMercator()+ "," + floidGpsPointMercator.getLngMercator());
//            }
//            staticLogger.debug("   Lat/Lng:");
//            for(FloidGpsPointMercator floidGpsPointMercator:filteredMercatorGpsPointList) {
//                staticLogger.debug("      " + floidGpsPointMercator.getLatitude()+ "," + floidGpsPointMercator.getLongitude());
//            }
            // Increase alpha for next time:
            reductionRound++;
        } while(filteredMercatorGpsPointList.size() > maxPoints);
        // Create the new list:
        ArrayList<FloidGpsPoint> filteredGpsPointList = new ArrayList<>();
        for(FloidGpsPointMercator floidGpsPointMercator:filteredMercatorGpsPointList) {
            filteredGpsPointList.add(floidGpsPointMercator.floidGpsPointFactory());
        }
        return filteredGpsPointList;
    }

    private static ArrayList<FloidGpsPointMercator> filterMercatorGpsPointListKink(ArrayList<FloidGpsPointMercator> mercatorGpsPointList, double currentMaxKink) {
//        staticLogger.debug("Original list:");
//        for(FloidGpsPointMercator floidGpsPointMercator:mercatorGpsPointList) {
//            staticLogger.debug("{},{}", floidGpsPointMercator.getLatitude(), floidGpsPointMercator.getLongitude());
//        }
        // Point extractor for our gps points mapped to mercator:
        PointExtractor<FloidGpsPointMercator> pointExtractor = new PointExtractor<FloidGpsPointMercator>()
        {
            @Override
            public double getX(FloidGpsPointMercator floidGpsPointMercator)
            {
                return floidGpsPointMercator.getLngMercator(); // X is longitude
            }
            @Override
            public double getY(FloidGpsPointMercator floidGpsPointMercator)
            {
                return floidGpsPointMercator.getLatMercator(); // Y is latitude
            }
        };
        // This is required for the Simplify initialization
        // An empty array is explicitly required by the Simplify library
        Simplify<FloidGpsPointMercator> simplify = new Simplify<>(new FloidGpsPointMercator[0], pointExtractor);
        FloidGpsPointMercator[] mercatorGpsPointArray = new FloidGpsPointMercator[mercatorGpsPointList.size()];
        mercatorGpsPointList.toArray(mercatorGpsPointArray);
        FloidGpsPointMercator[] simplifiedArray = simplify.simplify(mercatorGpsPointArray, currentMaxKink, true);
        return new ArrayList<>(Arrays.asList(simplifiedArray));
/*
        ArrayList<FloidGpsPointMercator> simplifiedList = new ArrayList<>(Arrays.asList(simplifiedArray));
        staticLogger.debug("Simplified list:");
        for(FloidGpsPointMercator floidGpsPointMercator:simplifiedList) {
            staticLogger.debug("{},{}", floidGpsPointMercator.getLatitude(), floidGpsPointMercator.getLongitude());
        }
        return simplifiedList;
*/
    }

    /**
     * Filter GPS point list by minimum time and distance
     * @param gpsPointList the gps point list
     * @param minTime the minimum time (milliseconds) between points - 0 = no filter
     * @param minDistance the minimum distance (meters) between points - 0 = no filter
     * @return the filtered list of gps points
     */
    private static ArrayList<FloidGpsPoint> filterGpsPointListTimeDistance(ArrayList<FloidGpsPoint> gpsPointList,
                                                                           long minTime,
                                                                           double minDistance) {
        staticLogger.debug("  Filtering GPS points Time/Distance: " + minTime + " " + minDistance);
        ArrayList<FloidGpsPoint> filteredGpsPointList = new ArrayList<>();
        // Trivial cases:
        if(gpsPointList == null) {
            return filteredGpsPointList;
        }
        if(gpsPointList.size() < 3) {
            return filteredGpsPointList;
        }
        // Get first and last:
        FloidGpsPoint currentPoint = gpsPointList.get(0);
//        staticLogger.debug(" -- First point: " + currentPoint.getLatitude() + ", " + currentPoint.getLongitude());
        FloidGpsPoint lastPoint = gpsPointList.get(gpsPointList.size()-1);
//        staticLogger.debug(" -- Last point: " + lastPoint.getLatitude() + ", " + lastPoint.getLongitude());
        // Add first:
        filteredGpsPointList.add(currentPoint);
        // Loop over the middle ones and try to eliminate:
        for(int i=1; i<gpsPointList.size()-1; ++i) {

            FloidGpsPoint testPoint = gpsPointList.get(i);
//            staticLogger.debug(" -- Testing point [ " + i + "]: " + testPoint.getLatitude() + ", " + testPoint.getLongitude());
            // Use time test?
            if(minTime != 0L) {
                // Too soon?
//                staticLogger.debug("    Time Delta: " + (testPoint.getStatusTime() - currentPoint.getStatusTime()));
                if(Math.abs(testPoint.getStatusTime() - currentPoint.getStatusTime()) < minTime) {
//                    staticLogger.debug("      ...dropping");
                    continue; // Go to next point
                }
            }
            // Use distance test?
            if(minDistance != 0L) {
//                staticLogger.debug("    Distance: " + DistanceUtils.calcLatLngDistance(currentPoint.getLatitude(), currentPoint.getLongitude(), testPoint. getLatitude(), testPoint.getLongitude()));
                // Too close?
                if(!testDistance(currentPoint, testPoint, minDistance)) {
//                    staticLogger.debug("      ...dropping");
                    continue;
                }
            }
//            staticLogger.debug("    ++ Keeping");
            // Test point passed - add to list:
            filteredGpsPointList.add(testPoint);
            // New current point:
            currentPoint = testPoint;
        }
        // Add the last point:
        filteredGpsPointList.add(lastPoint);
        staticLogger.debug(" Time/Distance filtered list size: " + filteredGpsPointList.size());
        // Return the list:
        return filteredGpsPointList;
    }

    /**
     * Test the distance between two lat/lng pts
     * @param p1 first point
     * @param p2 second point
     * @param minDistance test distance
     * @return true if distance <= test distance
     */
    private static boolean testDistance(FloidGpsPoint p1, FloidGpsPoint p2, double minDistance) {
        return(DistanceUtils.calcLatLngDistance(p1.getLatitude(), p1.getLongitude(), p2. getLatitude(), p2.getLongitude()) >=minDistance);
    }

    /**
     * Get the floid id
     *
     * @return the floid id
     */
    int getFloidId() {
        return floidId;
    }

    /**
     * Set the floid id
     *
     * @param floidId the floid id
     */
    @SuppressWarnings("unused")
    void setFloidId(int floidId) {
        this.floidId = floidId;
    }

    /**
     * Get the floid uuid
     *
     * @return the floid uuid
     */
    @SuppressWarnings("unused")
    String getFloidUuid() {
        return floidUuid;
    }

    /**
     * Set the floid uuid
     *
     * @param floidUuid the floid uuid
     */
    @SuppressWarnings("unused")
    void setFloidUuid(String floidUuid) {
        this.floidUuid = floidUuid;
    }

    // TODO [PARAMETERS] Make this timeout configurable:
    private static final int floidOnlineTimeoutInSeconds = 10;
    /**
     * Send the next instruction
     *
     * @param droidInstruction the next instruction
     * @param floidServerCommandResult the floid server command result
     */
    void sendInstruction(DroidInstruction droidInstruction, FloidServerCommandResult floidServerCommandResult) {
        logger.info(("Sending Droid Instruction: " + droidInstruction.getType()));
        boolean isOnline = FloidIdHelper.isOnline(floidId, floidOnlineTimeoutInSeconds);
        if (isOnline) {
            logger.info("Sending Droid Instruction:  ONLINE - SENDING");
            floidServerCommandResult.setOnline(true);
            floidServerCommandResult.setQueueSuccess(true);
            floidServerCommandResult.setQueueLength(droidInstructions.size());
            floidServerCommandResult.setResultMessage(FloidServerRunnable.SUCCESS_MESSAGE);
            floidServerCommandResult.setResult(FloidServerRunnable.SUCCESS_CODE);
            floidServerCommandResult.setSuccess(true);
            synchronized (droidInstructions) {
                droidInstructions.add(droidInstruction);
            }
        } else {
            logger.info("Sending Droid Instruction:  OFFLINE - NOT SENDING");
            floidServerCommandResult.setOnline(false);
            floidServerCommandResult.setQueueSuccess(false);
            floidServerCommandResult.setQueueLength(0);
            floidServerCommandResult.setResultMessage(FloidServerRunnable.ERROR_MESSAGE_OFFLINE);
            floidServerCommandResult.setResult(FloidServerRunnable.ERROR_CODE_OFFLINE);
            floidServerCommandResult.setSuccess(false);
        }
    }

    @Override
    public void run() {
        try {
            // Get the input and output streams:
            PrintStream printStream = new PrintStream(socket.getOutputStream());
            BufferedInputStream bufferedInputStream = new BufferedInputStream(socket.getInputStream());
            floidServerThreadInfo.setStatus(FloidServerThreadInfo.STATUS_RUNNING);
            // Now handle messages:
            while (!stop) {
                try {
                    FloidServerMessageStatus floidServerMessageStatus = new FloidServerMessageStatus();
                    byte[] floidServerMessageBytes = FloidServerMessageUtils.readFloidMessage(bufferedInputStream, floidServerMessageStatus);
                    if(floidServerMessageBytes!=null && floidServerMessageBytes.length > 0) {
                        FloidServerMessage floidServerMessage = FloidServerMessage.parseFrom(floidServerMessageBytes);
                        if (floidServerMessage != null) {
                            floidServerThreadInfo.setEndTimestamp(new Date().getTime());  // Update our thread timestamp info
                            FloidClientMessage floidClientMessage = handleMessage(floidServerMessage, floidServerThreadInfo, currentFloidServerThreadInfoList);
                            if (floidClientMessage != null) {
                                FloidServerMessageUtils.writeFloidMessage(printStream, floidClientMessage.toByteArray(), floidServerMessageStatus);
                            } else {
                                logger.error("Floid Client Message null");
                                logger.error("Logs: " + floidServerMessageStatus.getLogs().toString());
                                logger.info("[" + floidServerThreadInfo.getThreadID() + "] stopping (" + floidId + "-" + floidUuid + ")");
                                floidServerThreadInfo.setStatus(FloidServerThreadInfo.STATUS_STOPPING);
                                stop = true;
                                FloidServerMessageUtils.writeFloidMessage(printStream, sNullCommandClientMessage.toByteArray(), floidServerMessageStatus);
                            }
                        } else {
                            logger.error("FloidServerMessage null");
                            logger.error("Logs: " + floidServerMessageStatus.getLogs().toString());
                            logger.info("[" + floidServerThreadInfo.getThreadID() + "] stopping (" + floidId + "-" + floidUuid + ")");
                            floidServerThreadInfo.setStatus(FloidServerThreadInfo.STATUS_STOPPING);
                            stop = true;
                            FloidServerMessageUtils.writeFloidMessage(printStream, sNullCommandClientMessage.toByteArray(), floidServerMessageStatus);
                        }
                    } else {
                        logger.info("[" + floidServerThreadInfo.getThreadID() + "] stopping (" + floidId + "-" + floidUuid + ")");
                        //logger.debug("Logs: " + floidServerMessageStatus.getLogs().toString());
                        floidServerThreadInfo.setStatus(FloidServerThreadInfo.STATUS_STOPPING);
                        stop = true;
                        FloidServerMessageUtils.writeFloidMessage(printStream, sNullCommandClientMessage.toByteArray(), floidServerMessageStatus);
                    }
                } catch (Exception e) {
                    logger.error("Received Floid Server Runnable received exception: stopping... ", e);
                    logger.info("[" + floidServerThreadInfo.getThreadID() + "] stopping (" + floidId + "-" + floidUuid + ")");
                    floidServerThreadInfo.setStatus(FloidServerThreadInfo.STATUS_STOPPING);
                    stop = true;
                }
            }
            // Close print stream
            try {
                logger.info("Closing print stream for thread: [" + floidServerThreadInfo.getThreadID() + "] [" + floidServerThreadInfo.getFloidServerRunnable() + "] " + floidId + "-" + floidUuid);
                printStream.close();
            } catch (Exception f) {
                logger.error("Exception closing print stream", f);
            }
            // Close buffered input stream:
            try {
                logger.info("Closing buffered input stream for thread: [" + floidServerThreadInfo.getThreadID() + "] [" + floidServerThreadInfo.getFloidServerRunnable() + "] " + floidId + "-" + floidUuid);
                bufferedInputStream.close();
            } catch (Exception f) {
                logger.error("Exception buffered reader", f);
            }
        } catch (Exception e) {
            try {
                logger.info("Caught exception for thread [" + floidServerThreadInfo.getThreadID() + "] [" + floidServerThreadInfo.getFloidServerRunnable() + "] " + floidId + "-" + floidUuid, e);
            } catch (Exception f) {
                logger.error("Exception printing thread info:" , f);
            }
            floidServerThreadInfo.setStatus(e.toString());
        }
        try {
            logger.info("Closing socket for thread: [" + floidServerThreadInfo.getThreadID() + "] [" + floidServerThreadInfo.getFloidServerRunnable() + "] " + floidId + "-" + floidUuid);
            socket.close();
        } catch (Exception f) {
            logger.error("Exception closing socket", f);
        }
        try {
            // Finalize our thread info object and move to the completed list:
            floidServerThreadInfo.setEndTimestamp(new Date().getTime());
            logger.info("Moving to completion state [" + floidServerThreadInfo.getThreadID() + "] [" + floidServerThreadInfo.getFloidServerRunnable() + "] " + floidId + "-" + floidUuid);
            synchronized (currentFloidServerThreadInfoList) {
                currentFloidServerThreadInfoList.remove(floidServerThreadInfo);
                floidServerThreadInfo.setStatus(FloidServerThreadInfo.STATUS_FINALIZED);
                completedFloidServerThreadInfoList.add(floidServerThreadInfo);
            }
        } catch (Exception e) {
            logger.error("Exception finalizing thread",e);
        }
    }

    private void stopAlternateFloidIdThreads(FloidServerThreadInfo floidServerThreadInfo, int floidId, List<FloidServerThreadInfo> currentFloidServerThreadInfoList) {
        try {
/*
            logger.debug("stopAlternateFloidIdThreads: Current threads:");
            for (FloidServerThreadInfo floidServerThreadInfo1 : currentFloidServerThreadInfoList) {
                logger.debug("Thread: " + floidServerThreadInfo1.getThreadID() + " Floid ID: " + floidServerThreadInfo1.getFloidServerRunnable().getFloidId() + "  Pings: " + floidServerThreadInfo1.getNumberOfPings());
            }
            logger.debug("stopAlternateFloidIdThreads: Completed threads:");
            for (FloidServerThreadInfo floidServerThreadInfo1 : completedFloidServerThreadInfoList) {
                logger.debug("Thread: " + floidServerThreadInfo1.getThreadID() + " Floid ID: " + floidServerThreadInfo1.getFloidServerRunnable().getFloidId() + "  Pings: " + floidServerThreadInfo1.getNumberOfPings());
            }
            logger.info("stopAlternateFloidIdThreads: Completing any needed threads:");
*/
            List<FloidServerThreadInfo> threadsToStop = new ArrayList<>();
            for (FloidServerThreadInfo floidServerThreadInfo1 : currentFloidServerThreadInfoList) {
//                logger.info("Thread: " + floidServerThreadInfo1.getThreadID() + " Floid ID: " + floidServerThreadInfo1.getFloidServerRunnable().getFloidId() + "  Pings: " + floidServerThreadInfo1.getNumberOfPings());
                // Get list of threads to stop:
                if ((floidServerThreadInfo1.getFloidServerRunnable().getFloidId() == floidId) && (floidServerThreadInfo.getThreadID() > floidServerThreadInfo1.getThreadID())) {
                    threadsToStop.add(floidServerThreadInfo1);
                }
            }
            for(FloidServerThreadInfo floidServerThreadInfo1:threadsToStop) {
                // Add this thread to close list:
                try {
                    logger.info("Stopping thread: " + floidServerThreadInfo1.getThreadID() + " Floid ID: " + floidServerThreadInfo1.getFloidServerRunnable().getFloidId() + "  Pings: " + floidServerThreadInfo1.getNumberOfPings());
                    //                    logger.info("Interrupting thread for floidId: " + floidId + "  [" + floidServerThreadInfo1 + "] [" + floidServerThreadInfo1.getFloidServerRunnable() + "]");
                    try {
                        floidServerThreadInfo1.getFloidServerRunnable().stop = true;
                        floidServerThreadInfo1.getFloidServerRunnable().socket.close();
                        floidServerThreadInfo1.getFloidServerThread().interrupt();
                    } catch (Exception e) {
                        logger.error("Exception interrupting thread: " + floidServerThreadInfo1.getThreadID() + " Floid ID: " + floidServerThreadInfo1.getFloidServerRunnable().getFloidId() + "  Pings: " + floidServerThreadInfo1.getNumberOfPings(), e);
                    }
                } catch (Exception e) {
                    logger.error("Caught exception stopping thread", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception stopping threads");
        }
    }

    private static final FloidClientMessage sNullCommandClientMessage = makeNullCommandClientMessage();


    private FloidClientMessage handleMessage(FloidServerMessage floidServerMessage, final FloidServerThreadInfo floidServerThreadInfo, final List<FloidServerThreadInfo> currentFloidServerThreadInfoList) {
        if (floidServerMessage == null) {
            logger.error("Received null floid server message");
            return null;
        }
        final long systemTimeInMillis = System.currentTimeMillis();
        try {
            // Verify the message:
            if (floidServerMessage.getMessageType() == FloidServerMessage.FloidServerMessageType.STANDARD) {
                // Standard:
                // Verify the string message is not null:
                if (floidServerMessage.getStringMessage() == null) {
                    logger.error("Received null string message in Floid Server Message");
                    return sNullCommandClientMessage;
                }
                // Log the message:
                logger.info("[" + floidServerThreadInfo.getThreadID() + "] [" + floidServerThreadInfo.getFloidServerRunnable() + "] " + floidId + "-" + floidUuid + " <- " + "Standard Msg: " + floidServerMessage.getStringMessage());
            } else if (floidServerMessage.getMessageType() == FloidServerMessage.FloidServerMessageType.IMAGE) {
                // Image:
                if (floidServerMessage.getImageMessage() == null) {
                    logger.error("Received null image message in Floid Server Message");
                    return sNullCommandClientMessage;
                }
                logger.info("[" + floidServerThreadInfo.getThreadID() + "] [" + floidServerThreadInfo.getFloidServerRunnable() + "] " + floidId + "-" + floidUuid + " <- " + "Image Msg");
            } else {
                logger.error("Floid Server Message - unknown type: " + floidServerMessage.getMessageType());
                return sNullCommandClientMessage;
            }

            String messageClass;
            try {
                messageClass = floidServerMessage.getMessageClass();
            } catch (Exception e) {
                logger.error("Exception getting messageClass", e);
                return sNullCommandClientMessage;
            }
            if (messageClass == null) {
                logger.error("messageClass is null");
                return sNullCommandClientMessage;
            }
            // Floid ID:
            final int newFloidId;
            try {
                newFloidId = floidServerMessage.getFloidId();
            } catch (Exception e) {
                logger.error("Exception getting newFloidId", e);
                return sNullCommandClientMessage;
            }
            if (newFloidId == -1) {
                logger.error("newFloidId is -1");
                return sNullCommandClientMessage;
            }
            // Floid UUID:
            final String newFloidUuid;
            try {
                newFloidUuid = floidServerMessage.getFloidUuid();
            } catch (Exception e) {
                logger.error("Exception getting newFloidUuid", e);
                return sNullCommandClientMessage;
            }
            if (newFloidUuid == null) {
                logger.error("newFloidUuid is null");
                return sNullCommandClientMessage;
            }
            // TODO [FUTURE] If there are other ping types, then differentiate them before setting this below:
            floidServerThreadInfo.setPingType(FloidServerThreadInfo.PING_TYPE_FLOID);
            floidServerThreadInfo.increaseNumberOfPings();
            // Stop any alternate threads here:
            stopAlternateFloidIdThreads(floidServerThreadInfo, newFloidId, currentFloidServerThreadInfoList);
            // Are the floid id and uuid the same or either new?
            if (floidId == newFloidId && floidUuid.equals(newFloidUuid)) {
                // Same - we update them asynchronously as the database record is already created:
                // Create a new thread to handle the id status asynchronously:
                Runnable floidIdStatusHandlerRunnable = new Runnable() {
                    @Override
                    public void run() {
                        handleFloidIdUpdate(newFloidId, newFloidUuid, systemTimeInMillis);
                    }
                };
                Thread floidIdStatusHandlerThread = new Thread(floidIdStatusHandlerRunnable);
                floidIdStatusHandlerThread.start();
            } else {
                // Different - we want to wait for the full database update (create) to succeed before returning
                handleFloidIdUpdate(newFloidId, newFloidUuid, systemTimeInMillis);
            }
            // Copy over the new ids:
            floidId = newFloidId;
            floidUuid = newFloidUuid;
            //noinspection
            if (messageClass.equals(FloidDroidStatus.class.getName())) {
                // FloidDroidStatus:
                final FloidDroidStatus floidDroidStatus = new FloidDroidStatus();
                JSONObject floidServerMessageJson;
                try {
                    floidServerMessageJson = new JSONObject(floidServerMessage.getStringMessage());
                    // What do I want to get here?
                    floidDroidStatus.fromJSON(floidServerMessageJson);
                } catch (Exception e) {
                    logger.error("Exception processing message", e);
                    return sNullCommandClientMessage;
                }
//                    logger.trace("Converted to: " + floidDroidStatus.toJSON().toString());
                // Hibernate this object in a separate thread for efficiency:
                Runnable floidDroidStatusHibernateRunnable = new Runnable() {
                    @Override
                    public void run() {
                        // Hibernate this status:
                        try {
                            FloidServerHibernateUtil.hibernateObject(floidDroidStatus);
                        } catch (Exception e) {
                            logger.error("FloidDroidStatus: Hibernate exception: " + e);
                        }
                    }
                };
                Thread floidDroidStatusHibernateThread = new Thread(floidDroidStatusHibernateRunnable);
                floidDroidStatusHibernateThread.start();
                // Send the floid status message to anyone listening:
                sendFloidDroidStatusMessage(floidDroidStatus, floidId, floidUuid);   // Send the floid status out:
                // Create and send a response:
                // RETURN THE NEXT INSTRUCTION IN THE LIST IF THERE IS ONE:
                boolean commandToSend = false;
                DroidInstruction droidInstruction = null;
                synchronized (droidInstructions) {
                    if (droidInstructions.size() > 0) {
                        logger.info("FloidDroidStatus: Found Pending Instruction");
                        // Get the top one:
                        droidInstruction = droidInstructions.get(0);
                        // Remove it:
                        droidInstructions.remove(0);
                    }
                }
                if (droidInstruction != null) {
                    // Process it:
                    // What is the instruction type:
                    if (droidInstruction.getClass() == DroidMission.class) {
                        logger.info("FloidDroidStatus: Pending Instruction: Mission");
                        // We must be in mission mode
                        if (floidDroidStatus.getCurrentMode() == FloidDroidStatus.DROID_MODE_MISSION) {
                            logger.info("FloidDroidStatus: Floid in Mission Mode - sending mission");
                            commandToSend = true;
                        } else {
                            logger.info("FloidDroidStatus: Floid not in Mission Mode - rejecting mission");
                        }
                    } else {
                        logger.info("FloidDroidStatus: Pending instruction: Direct command");
                        // We must be in test mode:
                        //noinspection StatementWithEmptyBody
                        if (floidDroidStatus.getCurrentMode() == FloidDroidStatus.DROID_MODE_TEST) {
                            // TODO [MISSION MODE TEST] FIX WHICH INSTRUCTIONS WE CAN SEND IN THIS MODE:
//                                commandToSend = true;  // Changed from false so that we can get through testing - implement a switch on proper commands
                        }
                        commandToSend = true; // NOTE: For now all commands are sendable to both modes
                    }
                    if (commandToSend) {
                        logger.info("FloidDroidStatus: We have a command to send");
                        // Hibernate it:
                        try {
                            logger.info("FloidDroidStatus: Hibernating command");
                            FloidServerHibernateUtil.hibernateObject(droidInstruction);
                        } catch (Exception e) {
                            logger.error("FloidDroidStatus: DroidInstruction Hibernate exception: " + e);
                        }
                        // Set this response:
                        // Moved above to hibernate
                        try {
                            // Clean out the recursion in this object:
                            if (droidInstruction.getClass() == DroidMission.class) {
                                logger.info("FloidDroidStatus: Cleaning  mission");
                                DroidMission droidMission = (DroidMission) droidInstruction;
                                for (DroidInstruction subInstruction : droidMission.getDroidInstructions()) {
                                    subInstruction.setDroidMission(null);
                                }
                            }
                            logger.info("FloidDroidStatus: Converting command");
                            return makeDroidInstructionClientMessage(droidInstruction);
                        } catch (JSONException jsonException) {
                            logger.error("FloidDroidStatus: JSONException: " + jsonException);
                            return sNullCommandClientMessage;
                        }
                    } else {
                        try {
                            // We had a droid instruction but we were in the wrong mode
                            // Make and send a response to the interface that this instruction didn't get sent...
                            logger.error("FloidDroidStatus: " + ERROR_MESSAGE_WRONG_MODE_FOR_INSTRUCTION + ": " + droidInstruction.getShortType());
                            sendFloidDroidMessage(ERROR_MESSAGE_WRONG_MODE_FOR_INSTRUCTION + ": " + droidInstruction.getShortType(), floidId, floidUuid);
                            return sNullCommandClientMessage;
                        } catch (Exception e) {
                            logger.error("FloidDroidStatus: caught exception", e);
                            return sNullCommandClientMessage;
                        }
                    }
                }
                return sNullCommandClientMessage;
            } else if (messageClass.equals(FloidStatus.class.getName())) {
                try {
                    // FloidStatus:
                    final FloidStatus floidStatus = new FloidStatus();
                    JSONObject floidServerMessageJson;
                    try {
                        floidServerMessageJson = new JSONObject(floidServerMessage.getStringMessage());
                        // What do I want to get here?
                        floidStatus.fromJSON(floidServerMessageJson);
                    } catch (Exception e) {
                        logger.error("Exception processing message", e);
                        return sNullCommandClientMessage;
                    }
//                        logger.trace("Converted to: " + floidStatus.toJSON().toString());
                    // Hibernate this object in a separate thread for efficiency:
                    Runnable floidStatusHibernateRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // Hibernate this status:
                            try {
                                FloidServerHibernateUtil.hibernateObject(floidStatus);
                            } catch (Exception e) {
                                logger.error("FloidStatus: Hibernate exception: " + e);
                            }
                        }
                    };
                    Thread floidStatusHibernateThread = new Thread(floidStatusHibernateRunnable);
                    floidStatusHibernateThread.start();
                    // Send the floid status message to anyone listening:
                    sendFloidStatusMessage(floidStatus, floidId, floidUuid);   // Send the floid status out:
                } catch (Exception e) {
                    logger.error("FloidStatus: caught exception", e);
                }
            }
            // Photo:
            // ------
            else if (messageClass.equals(FloidCommands.FLOID_MESSAGE_PACKET_PHOTO)) {
                try {
                    // Create a new FloidDroidPhoto:
                    final FloidDroidPhoto floidDroidPhoto = new FloidDroidPhoto();
                    floidDroidPhoto.setFloidId(floidId);
                    floidDroidPhoto.setFloidUuid(floidUuid);
                    // Get the image:
                    getImageFromFloidServerImageMessage(floidServerMessage.getImageMessage(), floidDroidPhoto);
                    // Start a thread to decode and send to ux:
                    Runnable floidDroidPhotoHandlerRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // Write photo to a file in a separate thread for efficiency (and optionally hibernate):
                            Runnable floidDroidPhotoWritePhotoRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    // Note: Write the photo:
                                    String photoFileName = FloidServerImageLocation.getImagesLocation() + floidDroidPhoto.getImageName();
                                    staticLogger.info("Writing photo image file: " + photoFileName);
                                    try {
                                        logger.info("Floid Server: Saving photo: " + photoFileName);
                                        FileUtils.writeByteArrayToFile(new File(photoFileName), floidDroidPhoto.getImageBytes());
                                    } catch (Exception e) {
                                        logger.error("Floid Server: Exception saving file: " + photoFileName, e);
                                    }
                                }
                            };
                            Thread floidDroidWritePhotoThread = new Thread(floidDroidPhotoWritePhotoRunnable);
                            floidDroidWritePhotoThread.start();
                            // Hibernate the photo, except for the imageBytes which are marked transient:
                            Runnable floidDroidPhotoHibernateRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    // Hibernate it:
                                    try {
                                        FloidServerHibernateUtil.hibernateObject(floidDroidPhoto);
                                    } catch (Exception e) {
                                        logger.error("FloidDroidPhoto: Hibernate exception: " + e);
                                    }
                                }
                            };
                            Thread floidDroidPhotoHibernateThread = new Thread(floidDroidPhotoHibernateRunnable);
                            floidDroidPhotoHibernateThread.start();
                            // Send a photo message to any listeners:
                            sendFloidDroidPhotoMessage(floidDroidPhoto, floidId, floidUuid);
                        }
                    };
                    Thread floidDroidPhotoHandlerThread = new Thread(floidDroidPhotoHandlerRunnable);
                    floidDroidPhotoHandlerThread.start();
                } catch (Exception e) {
                    logger.error("FloidDroidPhoto: caught exception", e);
                }
            }
            // Video:
            // ------
            else if (messageClass.equals(FloidCommands.FLOID_MESSAGE_PACKET_VIDEO_FRAME)) {
                try {
                    // Create a new FloidDroidVideoFrame:
                    final FloidDroidVideoFrame floidDroidVideoFrame = new FloidDroidVideoFrame();
                    floidDroidVideoFrame.setFloidId(floidId);
                    floidDroidVideoFrame.setFloidUuid(floidUuid);
                    // Get the image:
                    getImageFromFloidServerImageMessage(floidServerMessage.getImageMessage(), floidDroidVideoFrame);
                    // Start a thread to decode and send to ux:
                    Runnable floidDroidVideoHandlerRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // Write video frame to a file in a separate thread for efficiency (and optionally hibernate):
                            Runnable floidDroidWriteVideoFrameRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    String videoFileName = FloidServerImageLocation.getImagesLocation() + floidDroidVideoFrame.getImageName();
                                    staticLogger.info("Writing video frame image file: " + videoFileName);
                                    // Note: Write but do not hibernate the video frame:
                                    try {
                                        logger.info("Floid Server: Saving video image: " + videoFileName);
                                        FileUtils.writeByteArrayToFile(new File(videoFileName), floidDroidVideoFrame.getImageBytes());
                                    } catch (Exception e) {
                                        logger.error("Floid Server: Exception saving file: " + videoFileName, e);
                                    }
                                }
                            };
                            Thread floidDroidWriteVideoFrameThread = new Thread(floidDroidWriteVideoFrameRunnable);
                            floidDroidWriteVideoFrameThread.start();

                            // Hibernate the video frame, except for the imageBytes which are marked transient:
                            Runnable floidDroidVideoFrameHibernateRunnable = new Runnable() {
                                @Override
                                public void run() {
                                    // Hibernate it:
                                    try {
                                        FloidServerHibernateUtil.hibernateObject(floidDroidVideoFrame);
                                    } catch (Exception e) {
                                        logger.error("FloidDroidVideoFrame: Hibernate exception: " + e);
                                    }
                                }
                            };
                            Thread floidDroidVideoFrameHibernateThread = new Thread(floidDroidVideoFrameHibernateRunnable);
                            floidDroidVideoFrameHibernateThread.start();
                            // Send a video preview frame message to any listeners:
                            sendFloidDroidVideoFrameMessage(floidDroidVideoFrame, floidId, floidUuid);
                        }
                    };
                    Thread floidDroidVideoHandlerThread = new Thread(floidDroidVideoHandlerRunnable);
                    floidDroidVideoHandlerThread.start();
                } catch (Exception e) {
                    logger.error("FloidDroidVideoFrame: caught exception", e);
                }
            } else if (messageClass.equals(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG)) {
                try {
                    String debugMessage;
                    try {
                        debugMessage = floidServerMessage.getStringMessage();
                    } catch (Exception e) {
                        logger.error("Exception processing message", e);
                        return sNullCommandClientMessage;
                    }
                    // Get rid of extra newlines:
                    debugMessage = debugMessage.replaceAll("\n\n", "\n");
                    // Split into individual messages:
                    final String[] splitDebugMessages = debugMessage.split("\n");
                    logger.debug("DEBUG MESSAGE RECEIVED - Sending...");
                    // Send each debug message to websocket:
                    Arrays.stream(splitDebugMessages).forEach(singleDebugMessage -> {
                        sendFloidDroidMessage(singleDebugMessage, floidId, floidUuid);
                    });
                    // Hibernate the debug messages in a separate thread for efficiency:
                    Runnable floidDebugHibernateRunnable = new Runnable() {
                        @Override
                        public void run() {
                            Arrays.stream(splitDebugMessages).forEach(singleDebugMessage -> {
                                final FloidDebugMessage floidDebugMessage = new FloidDebugMessage();
                                floidDebugMessage.setFloidId(floidId);
                                floidDebugMessage.setFloidUuid(floidUuid);
                                floidDebugMessage.setDebugMessage(singleDebugMessage);
                                try {
                                    FloidServerHibernateUtil.hibernateObject(floidDebugMessage);
                                } catch (Exception e) {
                                    logger.error("FloidDebugMessage Hibernate exception: " + e);
                                }
                            });
                        }
                    };
                    Thread floidDebugHibernateThread = new Thread(floidDebugHibernateRunnable);
                    floidDebugHibernateThread.start();
                } catch (Exception e) {
                    logger.error("FloidDebug: caught exception", e);
                }
                return sNullCommandClientMessage;
            } else if (messageClass.equals(FloidCommands.FLOID_MESSAGE_PACKET_SYSTEM_LOG)) {
                try {
                    String systemLogMessage;
                    try {
                        systemLogMessage = floidServerMessage.getStringMessage();
                    } catch (Exception e) {
                        logger.error("Exception processing message", e);
                        return sNullCommandClientMessage;
                    }
                    // Get rid of extra newlines and make final:
                    final String fSystemLogMessage = systemLogMessage.replaceAll("\n\n", "\n").replaceAll("  ", "");
                    logger.debug("SYSTEM LOG MESSAGE RECEIVED - Sending...");
                    // Send each the message to websocket:
                    final String systemLogMessagePrefix = "===== Start of retrieved Floid log =====\n";
                    final String systemLogMessagePostfix = "===== End of retrieved Floid log =====\n";
                    sendFloidDroidMessage(systemLogMessagePrefix + fSystemLogMessage + systemLogMessagePostfix, floidId, floidUuid);
                    // Hibernate the system log messages in a separate thread for efficiency:
                    Runnable floidSystemLogHibernateRunnable = new Runnable() {
                        @Override
                        public void run() {
                            final FloidSystemLog floidSystemLog = new FloidSystemLog();
                            floidSystemLog.setFloidId(floidId);
                            floidSystemLog.setFloidUuid(floidUuid);
                            floidSystemLog.setSystemLog(fSystemLogMessage);
                            try {
                                FloidServerHibernateUtil.hibernateObject(floidSystemLog);
                            } catch (Exception e) {
                                logger.error("FloidSystemLog Hibernate exception: " + e);
                            }
                        }
                    };
                    Thread floidSystemLogHibernateThread = new Thread(floidSystemLogHibernateRunnable);
                    floidSystemLogHibernateThread.start();
                } catch (Exception e) {
                    logger.error("FloidSystemLog: caught exception", e);
                }
                return sNullCommandClientMessage;
            }
            // Mission Instruction Status:
            // Mission Starting:
            // -----------------
            else if (messageClass.equals(FloidDroidMissionInstructionStatusMessage.class.getName())) {
                try {
                    // FloidDroidMissionInstructionStatusMessage:
                    final FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = new FloidDroidMissionInstructionStatusMessage();
                    JSONObject floidServerMessageJson;
                    try {
                        floidServerMessageJson = new JSONObject(floidServerMessage.getStringMessage());
                        floidDroidMissionInstructionStatusMessage.fromJSON(floidServerMessageJson);
                    } catch (Exception e) {
                        logger.error("Exception processing message", e);
                        return sNullCommandClientMessage;
                    }
                    // Hibernate this object in a separate thread for efficiency:
                    Runnable floidMissionInstructionStatusHibernateRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // Hibernate it:
                            try {
                                FloidServerHibernateUtil.hibernateObject(floidDroidMissionInstructionStatusMessage);
                            } catch (Exception e) {
                                logger.error("FloidDroidMissionInstructionStatusMessage Hibernate exception: " + e);
                            }
                        }
                    };
                    Thread floidMissionInstructionStatusHibernateThread = new Thread(floidMissionInstructionStatusHibernateRunnable);
                    floidMissionInstructionStatusHibernateThread.start();
                    // Send the floid status message to anyone listening:
                    sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage, floidId, floidUuid);   // Send the floid status out:
                } catch (Exception e) {
                    logger.error("floidDroidMissionInstructionStatusMessage: caught exception", e);
                }
                return sNullCommandClientMessage;
            }
            // Command Response:
            // -----------------
            else if (messageClass.equals(FloidCommands.FLOID_MESSAGE_PACKET_COMMAND_RESPONSE)) {
                // We do not bother to do anything with these for now
//                logger.error("COMMAND RESPONSE RECEIVED -  - NOT YET IMPLEMENTED!");
                return sNullCommandClientMessage;
            } else if (messageClass.equals(FloidCommands.FLOID_MESSAGE_PACKET_TEST_PACKET_RESPONSE)) {
                // We do not bother to do anything with these for now
//                logger.error("TEST PACKET RESPONSE RECEIVED - NOT YET IMPLEMENTED!");
                return sNullCommandClientMessage;
            }
            // Model Status:
            // -------------
            else if (messageClass.equals(FloidModelStatus.class.getName())) {
                try {
                    // Floid Model Status:
                    final FloidModelStatus floidModelStatus = new FloidModelStatus();
                    JSONObject floidServerMessageJson;
                    try {
                        floidServerMessageJson = new JSONObject(floidServerMessage.getStringMessage());
                        floidModelStatus.fromJSON(floidServerMessageJson);
                    } catch (Exception e) {
                        logger.error("Exception processing message", e);
                        return sNullCommandClientMessage;
                    }
//                        logger.trace("Converted to: " + floidModelStatus.toJSON().toString());
                    // Hibernate this object in a separate thread for efficiency:
                    Runnable floidModelStatusHibernateRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // Hibernate it:
                            try {
                                FloidServerHibernateUtil.hibernateObject(floidModelStatus);
                            } catch (Exception e) {
                                logger.error("FloidModelStatus Hibernate exception: " + e);
                            }
                        }
                    };
                    Thread floidModelStatusHibernateThread = new Thread(floidModelStatusHibernateRunnable);
                    floidModelStatusHibernateThread.start();
                    // Send the floid model status message to anyone listening:
                    sendFloidModelStatusMessage(floidModelStatus, floidId, floidUuid);   // Send the floid status out:
                } catch (Exception e) {
                    logger.error("FloidModelStatus: caught exception", e);
                }
                return sNullCommandClientMessage;
            }
            // Model Parameters:
            // -----------------
            else if (messageClass.equals(FloidModelParameters.class.getName())) {
                try {
                    // FloidModelParameters:
                    final FloidModelParameters floidModelParameters = new FloidModelParameters();
                    JSONObject floidServerMessageJson;
                    try {
                        floidServerMessageJson = new JSONObject(floidServerMessage.getStringMessage());
                        floidModelParameters.fromJSON(floidServerMessageJson);
                    } catch (Exception e) {
                        logger.error("Exception processing message", e);
                        return sNullCommandClientMessage;
                    }
//                        logger.trace("Converted to: " + floidModelParameters.toJSON().toString());
                    // Hibernate this object in a separate thread for efficiency:
                    Runnable floidModelParametersHibernateRunnable = new Runnable() {
                        @Override
                        public void run() {
                            // Hibernate it:
                            try {
                                FloidServerHibernateUtil.hibernateObject(floidModelParameters);
                            } catch (Exception e) {
                                logger.error("FloidModelParameters Hibernate exception: " + e);
                            }
                        }
                    };
                    Thread floidModelParametersHibernateThread = new Thread(floidModelParametersHibernateRunnable);
                    floidModelParametersHibernateThread.start();
                    // Send the floid model status message to anyone listening:
                    sendFloidModelParametersMessage(floidModelParameters, floidId, floidUuid);   // Send the floid status out:
                } catch (Exception e) {
                    logger.error("FloidModelParameters: caught exception", e);
                }
                return sNullCommandClientMessage;
            }
            // Unknown Packet:
            // ---------------
            else {
                // Unknown type: - Logic Error
                logger.error("HandlePacket: [UNKNOWN TYPE] " + messageClass);
            }
        } catch (Exception e) {
            logger.error("handlePacket caught exception:", e);
        }
        return sNullCommandClientMessage;
    }

    /**
     * This routine handles the update and messaging for the floid id and uuid - is called synchronously when either one is new to make sure record is created and then updates are asynchronous
     * @param newFloidId the new floid id
     * @param newFloidUuid the new floid uuid
     * @param systemTimeInMillis the current system time in millis
     */
    private void handleFloidIdUpdate(int newFloidId, String newFloidUuid, long systemTimeInMillis) {
        try {
            int updateFloidIdStatus = FloidIdHelper.updateFloidId(newFloidId, newFloidUuid, systemTimeInMillis);
            switch (updateFloidIdStatus) {
                case FloidIdHelper.FLOID_ID_UPDATE_NOT_NEW: {
                    break; // Do nothing
                }
                case FloidIdHelper.FLOID_ID_UPDATE_NEW_FLOID_ID: {
                    FloidIdAndState newFloidIdAndState = new FloidIdAndState();
                    newFloidIdAndState.setFloidId(newFloidId);
                    newFloidIdAndState.setFloidIdCreateTime(systemTimeInMillis);
                    newFloidIdAndState.setFloidIdUpdateTime(systemTimeInMillis);
                    newFloidIdAndState.setFloidActive(true);
                    newFloidIdAndState.setFloidStatus(false);
                    sendFloidNewFloidIdMessage(newFloidIdAndState);
                    break;
                }
                case FloidIdHelper.FLOID_ID_UPDATE_NEW_FLOID_UUID: {
                    FloidUuidAndState newFloidUuidAndState = new FloidUuidAndState();
                    newFloidUuidAndState.setFloidId(newFloidId);
                    newFloidUuidAndState.setFloidUuid(newFloidUuid);
                    newFloidUuidAndState.setFloidUuidCreateTime(systemTimeInMillis);
                    newFloidUuidAndState.setFloidUuidUpdateTime(systemTimeInMillis);
                    newFloidUuidAndState.setFloidActive(true);
                    newFloidUuidAndState.setFloidStatus(false);
                    sendFloidNewFloidUuidMessage(newFloidUuidAndState);
                    break;
                }
            }
        } catch (Exception ex) {
            logger.error("Could not save floidId: " + ex.getMessage());
       }
    }
    /**
     * Get the image from the floid server image data
     * @param floidServerImageMessage the floid server image message
     * @param floidImage the floid image in which to store the image
     */
    private void getImageFromFloidServerImageMessage(FloidServerMessage.FloidServerImageMessage floidServerImageMessage, FloidImage floidImage) {
        try {
            floidImage.setImageName(floidServerImageMessage.getImageName());
            floidImage.setImageType(floidServerImageMessage.getImageType());
            floidImage.setImageExtension(floidServerImageMessage.getImageExtension());
            floidImage.setAltitude(floidServerImageMessage.getAltitude());
            floidImage.setLatitude(floidServerImageMessage.getLatitude());
            floidImage.setLongitude(floidServerImageMessage.getLongitude());
            floidImage.setPan(floidServerImageMessage.getPan());
            floidImage.setTilt(floidServerImageMessage.getTilt());
            floidImage.setImageBytes(floidServerImageMessage.getDataBytes().toByteArray());
        } catch(Exception e) {
            logger.error("Caught exception getting image data from floid server image message", e);
        }
    }

    /**
     * Create a floid client message from the droid instruction
     * @param droidInstruction the droid instruction
     * @return the floid client message
     */
    private static FloidClientMessage makeDroidInstructionClientMessage(DroidInstruction droidInstruction) {
        if(droidInstruction == null) return null;
        return FloidClientMessage
                .newBuilder()
                .setMessageType(FloidClientMessage.FloidClientMessageType.STANDARD)
                .setMessageClass(droidInstruction.getType())
                .setStringMessage(droidInstruction.toJSON().toString())
                .build();
    }

    /**
     * Make a null command client message
     * @return the null command client message
     */
    private static FloidClientMessage makeNullCommandClientMessage() {
        return makeDroidInstructionClientMessage(new NullCommand());
    }

    /**
     * Make a message destination give the parameters and options
     * @param destination the destination
     * @param useFloidId add the floid id
     * @param useFloidUuid add the floid uuid
     * @return the destination appended with the floid id and/or floid uuid with '_' separator
     */
    static String makeFloidMessageDestination(String destination, boolean useFloidId, int floidId, boolean useFloidUuid, String floidUuid){
        String separator="_";
        return destination + (useFloidId?separator+floidId:"") + (useFloidUuid?separator+floidUuid:"");
    }
    /**
     * Set floid droid mission instruction status message
     *
     * @param floidDroidMissionInstructionStatusMessage the floid droid mission instruction status message
     * @param floidId                                   the floid id
     */
    private void sendFloidDroidMissionInstructionStatusMessage(FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage, int floidId, String floidUuid) {
        logger.info("Sending FloidDroidMissionInstructionStatusMessage for Floid: " + floidId);
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_DROID_MISSION_INSTRUCTION_STATUS,
                FLOID_MESSAGE_TYPE_FLOID_DROID_MISSION_INSTRUCTION_STATUS_MESSAGE,
                String.valueOf(getFloidMissionInstructionStatusMessageID()),
                floidId,
                floidUuid,
                makeFloidMessageDestination(FLOID_DROID_MISSION_INSTRUCTION_STATUS_MESSAGE_DESTINATION,
                        true, floidId,
                        true, floidUuid),
                floidDroidMissionInstructionStatusMessage,
                logger);
        if(success) {
            logger.debug("Sent floidDroidMissionInstructionStatus message.");
        } else {
            logger.error("Failed to send floidDroidMissionInstructionStatus message.");
        }
    }

    /**
     * Set a floid status message
     *
     * @param floidStatus the floid status
     * @param floidId     the floid id
     */
    private void sendFloidStatusMessage(FloidStatus floidStatus, int floidId, String floidUuid) {
        logger.info("Sending FloidStatusMessage for Floid: " + floidId);
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_STATUS,
                FLOID_MESSAGE_TYPE_FLOID_STATUS,
                String.valueOf(getFloidStatusMessageID()),
                floidId,
                floidUuid,
                makeFloidMessageDestination(FLOID_STATUS_DESTINATION, true, floidId,true, floidUuid),
                floidStatus,
                logger);
        if(success) {
            logger.debug("Sent floidStatus message.");
        } else {
            logger.error("Failed to send floidStatus message.");
        }
    }

    /**
     * Set a floid model status message
     *
     * @param floidModelStatus the floid model status
     * @param floidId          the floid id
     */
    private void sendFloidModelStatusMessage(FloidModelStatus floidModelStatus, int floidId, String floidUuid) {
        logger.info("Sending FloidModelStatusMessage for Floid: " + floidId);
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_MODEL_STATUS,
                FLOID_MESSAGE_TYPE_FLOID_MODEL_STATUS,
                String.valueOf(getFloidModelStatusMessageID()),
                floidId,
                floidUuid,
                makeFloidMessageDestination(FLOID_MODEL_STATUS_DESTINATION, true, floidId, true, floidUuid),
                floidModelStatus,
                logger);
        if(success) {
            logger.debug("Sent floidModelStatus message.");
        } else {
            logger.error("Failed to send floidModelStatus message.");
        }
    }

    /**
     * Send a floid model parameters message
     *
     * @param floidModelParameters the floid model parameters
     * @param floidId              the floid id
     */
    private void sendFloidModelParametersMessage(FloidModelParameters floidModelParameters, int floidId, String floidUuid) {
        logger.info("Sending FloidModelParametersMessage for Floid: " + floidId);
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_MODEL_PARAMETERS,
                FLOID_MESSAGE_TYPE_FLOID_MODEL_PARAMETERS,
                String.valueOf(getFloidModelParametersMessageID()),
                floidId,
                floidUuid,
                makeFloidMessageDestination(FLOID_MODEL_PARAMETERS_DESTINATION, true, floidId, true, floidUuid),
                floidModelParameters,
                logger);
        if(success) {
            logger.debug("Sent floidModelParameters message.");
        } else {
            logger.error("Failed to send floidModelParameters message.");
        }
    }

    /**
     * Send a floid droid message
     *
     * @param message the message
     * @param floidId the floid id
     */
    private void sendFloidDroidMessage(String message, int floidId, String floidUuid) {
        logger.info("Sending FloidDroidMessage for Floid: " + floidId);
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_DROID_MESSAGE,
                FLOID_MESSAGE_TYPE_FLOID_DROID_MESSAGE,
                String.valueOf(getFloidDroidMessageID()),
                floidId,
                floidUuid,
                makeFloidMessageDestination(FLOID_DROID_MESSAGE_DESTINATION, true, floidId, true, floidUuid),
                message,
                logger);
        if(success) {
            logger.debug("Sent floidDroid message.");
        } else {
            logger.error("Failed to send floidDroid message.");
        }
    }

    /**
     * Send a floid droid status message
     *
     * @param floidDroidStatus the floid droid status message
     * @param floidId          the floid id
     */
    private void sendFloidDroidStatusMessage(FloidDroidStatus floidDroidStatus, int floidId, String floidUuid) {
        logger.info("Sending FloidDroidStatusMessage for Floid: " + floidId);
        // For this one only the message number is set here because reuse of floid droid status:
        floidDroidStatus.setFloidMessageNumber(FloidMessage.getNextFloidMessageNumber());
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_DROID_STATUS,
                FLOID_MESSAGE_TYPE_FLOID_DROID_STATUS,
                String.valueOf(getFloidDroidStatusMessageID()),
                floidId,
                floidUuid,
                makeFloidMessageDestination(FLOID_DROID_STATUS_DESTINATION, true, floidId, true, floidUuid),
                floidDroidStatus,
                logger);
        if(success) {
            logger.debug("Sent floidDroidStatus message.");
        } else {
            logger.error("Failed to send floidDroidStatus message.");
        }
    }

    /**
     * Send a floid droid photo message
     *
     * @param floidDroidPhoto the floid droid photo
     * @param floidId         the floid id
     */
    private void sendFloidDroidPhotoMessage(FloidDroidPhoto floidDroidPhoto, int floidId, String floidUuid) {
        logger.info("Sending FloidDroidPhotoMessage for Floid: " + floidId);
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_DROID_PHOTO,
                FLOID_MESSAGE_TYPE_FLOID_DROID_PHOTO,
                String.valueOf(getFloidDroidPhotoMessageID()),
                floidId,
                floidUuid,
                makeFloidMessageDestination(FLOID_DROID_PHOTO_DESTINATION, true, floidId, true, floidUuid),
                floidDroidPhoto,
                logger);
        if(success) {
            logger.debug("Sent floidDroidPhoto message.");
        } else {
            logger.error("Failed to send floidDroidPhoto message.");
        }
    }

    /**
     * Send a floid droid video preview frame message
     *
     * @param floidDroidVideoFrame the video preview frame
     * @param floidId                     the floid id
     */
    private void sendFloidDroidVideoFrameMessage(FloidDroidVideoFrame floidDroidVideoFrame, int floidId, String floidUuid) {
        logger.info("Sending FloidDroidVideoFrameMessage for Floid: " + floidId);
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_DROID_VIDEO_FRAME,
                FLOID_MESSAGE_TYPE_FLOID_DROID_VIDEO_FRAME,
                String.valueOf(getFloidDroidVideoFrameMessageID()),
                floidId,
                floidUuid,
                makeFloidMessageDestination(FLOID_DROID_VIDEO_FRAME_DESTINATION, true, floidId, true, floidUuid),
                floidDroidVideoFrame,
                logger);
        if(success) {
            logger.debug("Sent floidDroidVideoFrame message.");
        } else {
            logger.error("Failed to send floidDroidVideoFrame message.");
        }
    }

    /**
     * Send a new floid id message
     *
     * @param newFloidIdAndState the new floid id and state
     */
    private void sendFloidNewFloidIdMessage(FloidIdAndState newFloidIdAndState) {
        logger.info("Sending new FloidId message: " + newFloidIdAndState.getFloidId());
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_NEW_FLOID_ID,
                FLOID_MESSAGE_TYPE_NEW_FLOID_ID,
                String.valueOf(getFloidNewFloidIdMessageID()),
                newFloidIdAndState.getFloidId(),
                "",
                makeFloidMessageDestination(FLOID_NEW_FLOID_ID_DESTINATION, false, newFloidIdAndState.getFloidId(), false, ""),
                newFloidIdAndState,
                logger);
        if(success) {
            logger.debug("Sent newFloid message.");
        } else {
            logger.error("Failed to send newFloid message.");
        }
    }

    /**
     * Send a new floid uuid message
     *
     * @param newFloidUuidAndState the floid uuid and state
     */
    private void sendFloidNewFloidUuidMessage(FloidUuidAndState newFloidUuidAndState) {
        logger.info("Sending new FloidUuid message: " + newFloidUuidAndState.getFloidId() + " " + newFloidUuidAndState.getFloidUuid());
        boolean success = setupFloidDroidMessage(
                messagingTemplate,
                FLOID_SUBTOPIC_FLOID_NEW_FLOID_UUID,
                FLOID_MESSAGE_TYPE_NEW_FLOID_UUID,
                String.valueOf(getFloidNewFloidIdMessageID()),
                newFloidUuidAndState.getFloidId(),
                newFloidUuidAndState.getFloidUuid(),
                makeFloidMessageDestination(FLOID_NEW_FLOID_UUID_DESTINATION, true, newFloidUuidAndState.getFloidId(),false, newFloidUuidAndState.getFloidUuid()),
                newFloidUuidAndState,
                logger);
        if(success) {
            logger.debug("Sent newFloid message.");
        } else {
            logger.error("Failed to send newFloid message.");
        }
    }

    /**
     * Static routine to set up and send floid droid message
     * @param messagingTemplate the simple messaging template
     * @param subtopic the subtopic
     * @param parameterType the parameter type
     * @param messageId the id of the message
     * @param floidId the floid id
     * @param floidUuid the floid uuid
     * @param destination the destination
     * @param body the body
     * @param logger the logger
     * @return true unless exception
     */
    static boolean setupFloidDroidMessage(SimpMessagingTemplate messagingTemplate,
                                           String subtopic,
                                           String parameterType,
                                           String messageId,
                                           int floidId,
                                           String floidUuid,
                                           String destination,
                                           Object body,
                                           Logger logger) {
            try {
                if(floidUuid==null) {
                    floidUuid = "";
                }
                Map<String, Object> headers = new HashMap<>();
                headers.put("subtopic", subtopic);
                headers.put("parameter_type", parameterType);
                headers.put("message_id", messageId);
                headers.put("floid_id", String.valueOf(floidId));
                headers.put("floid_uuid", floidUuid);
                if(floidId!=-1) {
                    logger.info("Sending message (" + floidId + " - " + floidUuid + ") to: /topic/" + destination);
                } else {
                    logger.info("Sending message (all) to: /topic/" + destination);
                }
                messagingTemplate.convertAndSend("/topic/" + destination, body, headers);
                return true;
        } catch (Exception e) {
            logger.error("Exception setting up Floid Message.", e);
            return false;
        }
    }
}
