package com.faiglelabs.floid.floidserver;

/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * FloidServerImageLocation
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Helper class for holding injected image location
 */
@Component
public class FloidServerImageLocation {

    private final Logger logger;

    private static String imagesLocation;

    @Autowired
    FloidServerImageLocation(@Value("${images.location}") String imagesLocation) {
        FloidServerImageLocation.imagesLocation = imagesLocation;
        logger = LoggerFactory.getLogger(FloidServerRunnable.class);
        logger.info("Images location: " + FloidServerImageLocation.imagesLocation);
    }

    public static String getImagesLocation() {
        return imagesLocation;
    }
}
