/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * FloidIdHelper
*/
package com.faiglelabs.floid.floidserver;

import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import com.faiglelabs.floid.servertypes.floidid.FloidId;
import com.faiglelabs.floid.servertypes.floidid.FloidIdAndState;
import com.faiglelabs.floid.servertypes.floidid.FloidUuid;
import com.faiglelabs.floid.servertypes.floidid.FloidUuidAndState;
import com.faiglelabs.floid.servertypes.user.FloidUser;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Floid id hibernate helper class
 */
@SuppressWarnings({"WeakerAccess", "rawtypes"})
public class FloidIdHelper {
    /**
     * The constant FLOID_ID_UPDATE_NOT_NEW.
     */
    public final static int FLOID_ID_UPDATE_NOT_NEW = 0;
    /**
     * The constant FLOID_ID_UPDATE_NEW_FLOID_ID.
     */
    public final static int FLOID_ID_UPDATE_NEW_FLOID_ID = 1;
    /**
     * The constant FLOID_ID_UPDATE_NEW_FLOID_UUID.
     */
    public final static int FLOID_ID_UPDATE_NEW_FLOID_UUID = 2;

    private final static Logger logger = LoggerFactory.getLogger(FloidIdHelper.class.getName());

    /**
     * Update floid id int.
     *
     * @param floidId            the floid id
     * @param floidUuid          the floid uuid
     * @param systemTimeInMillis the system time in millis
     * @return FLOID_ID_UPDATE_NOT_NEW, FLOID_ID_UPDATE_NEW_FLOID_ID or FLOID_ID_UPDATE_NEW_FLOID_UUID
     * @throws Exception if error
     */
    public static int updateFloidId(int floidId, String floidUuid, Long systemTimeInMillis) throws Exception {
        int updateFloidIdStatus = FLOID_ID_UPDATE_NOT_NEW; // Default
        // Logic:
        // 1. See if the floidId and floidId/Uuid exist
        FloidId floidIdObject = null;
        try {
            floidIdObject = getHibernateFloidId(floidId);
        } catch (Exception e) {
            // Do nothing - this means no floid id exists...
            logger.info("updateFloidId: new floid id (or not found): " + floidId + " " + e.getMessage() + " " + e.toString());
        }
        if (floidIdObject == null) {
            updateFloidIdStatus = FLOID_ID_UPDATE_NEW_FLOID_ID; // New Floid Id
            // Create a new one:
            floidIdObject = new FloidId();
            floidIdObject.setFloidId(floidId);
            floidIdObject.setFloidIdCreateTime(systemTimeInMillis);
            floidIdObject.setFloidIdUpdateTime(systemTimeInMillis);
        } else {
            // Update this one:
            floidIdObject.setFloidIdUpdateTime(systemTimeInMillis);
        }
        try {
            hibernateFloidId(floidIdObject);
        } catch (Exception e) {
            logger.error("updateFloidId exception: " + e.getMessage() + " " + e.toString());
            throw new Exception();
        }
        FloidUuid floidUuidObject = null;
        try {
            floidUuidObject = getHibernateFloidUuid(floidId, floidUuid);
        } catch (Exception e) {
            // Do nothing - this means no floidUuid exists...
            logger.info("updateFloidId: new floid uuid (or not found): " + floidUuid + " " + e.getMessage() + " " + e.toString());
        }
        if (floidUuidObject == null) {
            if (updateFloidIdStatus == FLOID_ID_UPDATE_NOT_NEW) {
                updateFloidIdStatus = FLOID_ID_UPDATE_NEW_FLOID_UUID; // New Floid UUID
            }
            // Create a new one:
            floidUuidObject = new FloidUuid();
            floidUuidObject.setFloidId(floidId);
            floidUuidObject.setFloidUuid(floidUuid);
            floidUuidObject.setFloidUuidCreateTime(systemTimeInMillis);
            floidUuidObject.setFloidUuidUpdateTime(systemTimeInMillis);
        } else {
            // Update this one:
            floidUuidObject.setFloidUuidUpdateTime(systemTimeInMillis);
        }
        try {
            hibernateFloidUuid(floidUuidObject);
        } catch (Exception e) {
            logger.error("updateFloidId exception: " + e.getMessage() + " " + e.toString());
        }
        return updateFloidIdStatus;
    }

    /**
     * Get a hibernated floid id object from a floid id integer
     *
     * @param floidId the floid id
     * @return the hibernated floid id object
     */
    public static FloidId getHibernateFloidId(int floidId) {
        FloidId floidIdObject = null;
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            if(session!=null) {
                Query query = session.createQuery("select new FloidId(id, floidId, floidIdCreateTime, floidIdUpdateTime) from FloidId where "
                        + FloidCommands.PARAMETER_FLOID_ID + " = :" + FloidCommands.PARAMETER_FLOID_ID
                        + " order by floidIdCreateTime asc");
                if(query!=null) {
                    query.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
                    List<?> floidIdList = query.list();
                    if (floidIdList != null && !floidIdList.isEmpty()) {
                        floidIdObject = (FloidId) floidIdList.get(0);
                    }
                }
                session.close();
            }
        } catch (Exception e) {
            logger.error("getHibernateFloidId: " + e.getMessage() + " " + e.toString());
        }
        return floidIdObject;
    }

    /**
     * Get a hibernated floid user object from a user name
     *
     * @param userName the user name
     * @return the Floid User
     */
    public static FloidUser getHibernateFloidUser(String userName) {
        FloidUser floidUserObject = null;
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            if(session!=null) {
                Query query = session.createQuery("select new FloidUser(id, userName, password, roles) from FloidUser where "
                        + FloidCommands.PARAMETER_USER_NAME + " = :" + FloidCommands.PARAMETER_USER_NAME);
                if(query!=null) {
                    query.setParameter(FloidCommands.PARAMETER_USER_NAME, userName);
                    List<?> floidUserList = query.list();
                    if (floidUserList != null && !floidUserList.isEmpty()) {
                        floidUserObject = (FloidUser) floidUserList.get(0);
                    }
                }
                session.close();
            }
        } catch (Exception e) {
            logger.error("getHibernateFloidUser: " + e.getMessage() + " " + e.toString());
        }
        return floidUserObject;
    }

    /**
     * Get hibernate floid uuid object from a floid id integer and a floid uuid string
     *
     * @param floidId   the floid id
     * @param floidUuid the floid uuid
     * @return the hibernated floid uuid object
     */
    public static FloidUuid getHibernateFloidUuid(int floidId, String floidUuid) {
        FloidUuid floidUuidObject = null;
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            if(session!=null) {
                Query query = session.createQuery("select new FloidUuid(id, floidId, floidUuid, floidUuidCreateTime, floidUuidUpdateTime) from FloidUuid where "
                        + FloidCommands.PARAMETER_FLOID_ID + "=:" + FloidCommands.PARAMETER_FLOID_ID + " and "
                        + FloidCommands.PARAMETER_FLOID_UUID + "=:" + FloidCommands.PARAMETER_FLOID_UUID
                        + " order by floidUuidCreateTime asc");
                if(query!=null) {
                    query.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
                    query.setParameter(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
                    List<?> floidUuidList = query.list();
                    if (floidUuidList != null && !floidUuidList.isEmpty()) {
                        floidUuidObject = (FloidUuid) floidUuidList.get(0);
                    }
                }
                session.close();
            }
        } catch (Exception e) {
            logger.error("getHibernateFloidUuid: " + e.getMessage() + " " + e.toString());
        }
        return floidUuidObject;
    }

    /**
     * Hibernate floid id.
     *
     * @param floidIdObject the floid id object
     */
    public static void hibernateFloidId(FloidId floidIdObject) {
        try {
            FloidServerHibernateUtil.hibernateObject(floidIdObject);
        } catch (Exception e) {
            logger.error("hibernateFloidId: " + e.getMessage() + " " + e.toString());
            throw e;
        }
    }

    /**
     * Hibernate a floid uuid object
     *
     * @param floidUuidObject the floid uuid object
     */
    public static void hibernateFloidUuid(FloidUuid floidUuidObject) {
        try {
            FloidServerHibernateUtil.hibernateObject(floidUuidObject);
        } catch (Exception e) {
            logger.error("hibernateFloidUuid: " + e.getMessage() + " " + e.toString());
            throw e;
        }
    }

    /**
     * Get all floid ids.
     *
     * @return the floid ids
     */
    @SuppressWarnings("unchecked")
    public static List<FloidId> getFloidIds() {
        // Gets a list of floid id's sorted by lastUpdateTime
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("select new FloidId(id, floidId, floidIdCreateTime, floidIdUpdateTime) from FloidId order by floidIdUpdateTime desc");
            List<FloidId> floidIdList = new ArrayList<>();
            try {
                floidIdList = query.list();
            } catch (Exception e) {
                e.printStackTrace();
            }
            session.close();
            return floidIdList;
        } catch (Exception e) {
            logger.error("getFloidIds: " + e.getMessage() + " " + e.toString());
        }
        return new ArrayList<>();
    }

    /**
     * Get all floid uuids for this floid id.
     *
     * @param floidId the floid id
     * @return the floid uuid list
     */
    public static ArrayList<FloidUuid> getFloidUuidList(int floidId) {
        // Gets a list of floid id's sorted by lastUpdateTime
        ArrayList<FloidUuid> floidUuidArrayList = new ArrayList<>();
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("select new FloidUuid(id, floidId, floidUuid, floidUuidCreateTime, floidUuidUpdateTime) from FloidUuid where " + FloidCommands.PARAMETER_FLOID_ID
                    + "=:" + FloidCommands.PARAMETER_FLOID_ID
                    + " order by floidUuidUpdateTime desc");
            //noinspection UnnecessaryBoxing
            query.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            @SuppressWarnings("unchecked")
            List<FloidUuid> floidUuidList = (List<FloidUuid>) query.list();
            session.close();
            floidUuidArrayList.addAll(floidUuidList);
        } catch (Exception e) {
            logger.error("getFloidUuids: " + e.getMessage() + " " + e.toString());
        }
        return floidUuidArrayList;
    }

    /**
     * Gets floid id and state list.
     *
     * @return the floid id and state list
     */
    public static ArrayList<FloidIdAndState> getFloidIdAndStateList() {
        return FloidServerFloidCurrentStatusMonitor.getFloidIdAndStateList();
    }

    /**
     * Gets floid uuid and state list for this floid id
     *
     * @param floidId the floid id
     * @return the floid uuid and state list for this floid id
     */
    public static ArrayList<FloidUuidAndState> getFloidUuidAndStateList(int floidId) {
        // Gets a list of floid id's sorted by lastUpdateTime
        ArrayList<FloidUuidAndState> floidUuidAndStateArrayList = new ArrayList<>();
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("select new FloidUuid(floidId, floidUuid, floidUuidCreateTime, floidUuidUpdateTime) from FloidUuid where "
                    + FloidCommands.PARAMETER_FLOID_ID + "=:" + FloidCommands.PARAMETER_FLOID_ID + " order by floidUuidUpdateTime desc");
            //noinspection UnnecessaryBoxing
            query.setParameter(FloidCommands.PARAMETER_FLOID_ID, floidId);
            @SuppressWarnings("unchecked")
            List<FloidUuid> floidUuidList = (List<FloidUuid>) query.list();
            for (FloidUuid floidUuid : floidUuidList) {
                Query floidStatusQuery = session.createQuery("select floidUuid from FloidStatus where floidUuid='" + floidUuid.getFloidUuid()+"'");
                floidStatusQuery.setMaxResults(1);
                if(floidStatusQuery.list().size() >0) {
                    // Has a floid status:
                    floidUuidAndStateArrayList.add(new FloidUuidAndState(floidUuid, true));
                } else {
                    floidUuidAndStateArrayList.add(new FloidUuidAndState(floidUuid, false));
                }
            }
            session.close();
        } catch (Exception e) {
            logger.error("getFloidUuidAndStateList: " + e.getMessage() + " " + e.toString());
        }
        return floidUuidAndStateArrayList;
    }

    /**
     * Is the floid with this id online in this timeout
     * @param floidId the floid id
     * @param timeoutInSeconds the timeout in seconds
     * @return true if the floid with this id has been online in this timeout
     */
    public static boolean isOnline(int floidId, long timeoutInSeconds) {
        try {
            FloidId floidIdObject = getHibernateFloidId(floidId);
            if(floidIdObject != null) {
                if((floidIdObject.getFloidIdUpdateTime() - (new Date()).getTime()) / 1000 < timeoutInSeconds) {
                    return true;
                }
            }
        } catch (Exception e) {
            // Do nothing - this means no floid id exists...
            logger.info("updateFloidId: new floid id (or not found): " + floidId + " " + e.getMessage() + " " + e.toString());
        }
        return false;
    }

    /**
     * Delete records in database associated with the given id
     * @param floidId the floid id
     * @return true if any records were deleted
     */
    public static boolean deleteFloidId(int floidId) {
        try {
            Session session = FloidServerHibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            @SuppressWarnings("unchecked")
            Query deleteFloidIdQuery = session.createQuery("delete from FloidId where floidId=" + Integer.toString(floidId));
            int floidIdsDeleted = deleteFloidIdQuery.executeUpdate();

            Query deleteFloidUuidQuery = session.createQuery("delete from FloidUuid where floidId=" + Integer.toString(floidId));
            int floidUuidsDeleted = deleteFloidUuidQuery.executeUpdate();

            Query deleteFloidStatusQuery = session.createQuery("delete from FloidStatus where floidId=" + Integer.toString(floidId));
            int floidStatusesDeleted =  deleteFloidStatusQuery.executeUpdate();

            Query deleteFloidModelStatusQuery = session.createQuery("delete from FloidModelStatus where floidId=" + Integer.toString(floidId));
            int floidModelStatusesDeleted =  deleteFloidModelStatusQuery.executeUpdate();

            Query deleteFloidModelParametersQuery = session.createQuery("delete from FloidModelParameters where floidId=" + Integer.toString(floidId));
            int floidModelParametersDeleted =  deleteFloidModelParametersQuery.executeUpdate();

            Query deleteFloidDroidStatusQuery = session.createQuery("delete from FloidDroidStatus where floidId=" + Integer.toString(floidId));
            int floidDroidStatusesDeleted =  deleteFloidDroidStatusQuery.executeUpdate();

            Query deleteFloidDroidPhotoQuery = session.createQuery("delete from FloidDroidPhoto where floidId=" + Integer.toString(floidId));
            int floidDroidPhotosDeleted =  deleteFloidDroidPhotoQuery.executeUpdate();

            Query deleteFloidVideoFrameQuery = session.createQuery("delete from FloidDroidVideoFrame where floidId=" + Integer.toString(floidId));
            int floidDroidVideoFramesDeleted =  deleteFloidVideoFrameQuery.executeUpdate();

            Query deleteFloidDebugMessageQuery = session.createQuery("delete from FloidDebugMessage where floidId=" + Integer.toString(floidId));
            int floidDroidDebugMessagesDeleted =  deleteFloidDebugMessageQuery.executeUpdate();

            Query deleteFloidDroidMissionInstructionStatusMessageQuery = session.createQuery("delete from FloidDroidMissionInstructionStatusMessage where floidId=" + Integer.toString(floidId));
            int floidDroidMissionInstructionStatusMessagesDeleted =  deleteFloidDroidMissionInstructionStatusMessageQuery.executeUpdate();

            transaction.commit();
            session.close();
            if(floidIdsDeleted + floidUuidsDeleted + floidStatusesDeleted + floidModelStatusesDeleted + floidModelParametersDeleted
                    + floidDroidStatusesDeleted + floidDroidPhotosDeleted + floidDroidVideoFramesDeleted + floidDroidDebugMessagesDeleted + floidDroidMissionInstructionStatusMessagesDeleted> 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            logger.error("deleteFloidId: " + e.getMessage() + " " + e.toString());
        }
        return false;
    }
}

