/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * WebSecurityConfig
 */
package com.faiglelabs.floid.floidserver.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    // NOTE: See e.g.: sudo wscat -c -s STOMP http://localhost:8080/handler/websocket

    // Roles:
    public static final String FLOID_ROLE_FLOID_ADMIN = "FLOID_ADMIN";
    public static final String FLOID_ROLE_FLOID_COMMANDER = "FLOID_COMMANDER";
    public static final String FLOID_ROLE_FLOID_USER = "FLOID_USER";
    public static final String FLOID_ROLE_FLOID_MISSION_ADMINISTRATOR = "FLOID_MISSION_ADMINISTRATOR";
    public static final String FLOID_ROLE_FLOID_PIN_SET_ADMINISTRATOR = "FLOID_PIN_SET_ADMINISTRATOR";
    public static final String FLOID_ROLE_FLOID_ID_ADMINISTRATOR = "FLOID_ID_ADMINISTRATOR";
    public static final String FLOID_ROLE_FLOID_APP_DOWNLOAD = "FLOID_APP_DOWNLOAD";

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // We don't need CSRF for this example
        httpSecurity
            .csrf().disable()
                .addFilterBefore(new WebSocketAuthHelperFilter(), UsernamePasswordAuthenticationFilter.class)
                // dont authenticate this particular request
                .authorizeRequests()
                // Allow authentication anonymously:
                .antMatchers("/authenticate").permitAll()
                // Allow the app anonymously:
                .antMatchers("/index.html").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/main.dart.js").permitAll()
                .antMatchers("/main.dart.js.map").permitAll()
                .antMatchers("/FileSaver.js").permitAll()
                .antMatchers("/canvaskit/chromium/canvaskit.js").permitAll()
                .antMatchers("/canvaskit/chromium//canvaskit.wasm").permitAll()
                .antMatchers("/assets/**").permitAll()
                .antMatchers("/flutter_service_worker.js").permitAll()
                // Icons, metadata, etc also anonymously:
                .antMatchers("/*.png").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/favicon-16x16.ico").permitAll()
                .antMatchers("/favicon-32x32.ico").permitAll()
                .antMatchers("/favicon-96x96.ico").permitAll()
                .antMatchers("/manifest.json").permitAll()
                .antMatchers("/browserconfig.xml").permitAll()
                // Static maps:
                .antMatchers("/maps/**").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                // Floid Server Control:
                .antMatchers("/fsc/**").hasAnyRole(FLOID_ROLE_FLOID_ADMIN)
                // WebSocket:
                .antMatchers("/handler/websocket").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                // Elevation:
                .antMatchers("/elevation/**").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                // App download:
                .antMatchers("/app-full-release.apk").hasAnyRole(FLOID_ROLE_FLOID_APP_DOWNLOAD)
                // Floid Server REST Calls:
                .antMatchers("/floidserver/user").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/createDownloadToken").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/floidServerStatus").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/floidList").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/floidIdAndStateList").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/floidUuidAndStateList").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/missionList").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/mission").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/missionJSON").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/pinSetList").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/pinSet").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/gpsPoints").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/gpsPointsFiltered").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/droidGpsPoints").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/lastFloidDroidStatus").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/lastFloidStatus").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/lastFloidModelStatus").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/lastFloidModelParameters").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/droidGpsPointsFiltered").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/lastFloidDroidPhoto").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/lastFloidDroidVideoFrame").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER, FLOID_ROLE_FLOID_USER)
                .antMatchers("/floidserver/performAcquireHomePositionCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performPayloadCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performDebugCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performDelayCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performDeployParachuteCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performDesignateCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performFlyToCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performHeightToCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performHoverCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performLandCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performLiftOffCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performNullCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performPanTiltCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performRetrieveLogsCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performSetParametersCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performShutDownHelisCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performSpeakerCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performSpeakerOnCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performSpeakerOffCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performStartUpHelisCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performStartVideoCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performStopDesignatingCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performStopVideoCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performPhotoStrobeCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performStopPhotoStrobeCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performTakePhotoCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/performTurnToCommand").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_COMMANDER)
                .antMatchers("/floidserver/createNewMission").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_MISSION_ADMINISTRATOR)
                .antMatchers("/floidserver/duplicateMission").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_MISSION_ADMINISTRATOR)
                .antMatchers("/floidserver/executeMission").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_MISSION_ADMINISTRATOR)
                .antMatchers("/floidserver/deleteMission").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_MISSION_ADMINISTRATOR)
                .antMatchers("/floidserver/saveMission").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_MISSION_ADMINISTRATOR)
                .antMatchers("/floidserver/saveMissionJSON").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_MISSION_ADMINISTRATOR)
                .antMatchers("/floidserver/createPinSet").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_PIN_SET_ADMINISTRATOR)
                .antMatchers("/floidserver/deletePinSet").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_PIN_SET_ADMINISTRATOR)
                .antMatchers("/floidserver/savePinSet").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_PIN_SET_ADMINISTRATOR)
                .antMatchers("/floidserver/duplicatePinSet").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_PIN_SET_ADMINISTRATOR)
                .antMatchers("/floidserver/deleteFloidId").hasAnyRole(FLOID_ROLE_FLOID_ADMIN, FLOID_ROLE_FLOID_ID_ADMINISTRATOR)
            // all other requests need to be authenticated:
            .anyRequest().authenticated()
            .and()
            // make sure we use stateless session; session won't be used to
            // store user's state.
            .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
            .and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        // Add a filter to validate the tokens with every request
        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private UserDetailsService jwtUserDetailsService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
