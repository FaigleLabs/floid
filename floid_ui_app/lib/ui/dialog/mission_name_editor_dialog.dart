/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/dialog/mission_name_editor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class MissionNameEditorDialog extends StatelessWidget {
  const MissionNameEditorDialog({
    Key? key,
    required GlobalKey<FormBuilderState> formKey,
    required String missionName,
    required String title,
    required String buttonText,
  })  : _formKey = formKey,
        _missionName = missionName,
        _title = title,
        _buttonText = buttonText,
        super(key: key);

  final GlobalKey<FormBuilderState> _formKey;
  final String _missionName;
  final String _title;
  final String _buttonText;

  @override
  Widget build(BuildContext context) {
    return MissionNameEditor(formKey: _formKey, missionName: _missionName, title: _title, buttonText: _buttonText);
  }
}
