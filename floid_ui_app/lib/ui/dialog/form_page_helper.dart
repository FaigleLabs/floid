/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

@optionalTypeArgs
mixin FormPageHelper<T extends StatefulWidget> on State<T> {
  dynamic getFormValue(GlobalKey<FormBuilderState>? form, String field) {
    if(form == null || form.currentState == null || form.currentState?.fields.containsKey(field) == false)
      return null;

    return form.currentState?.fields[field]?.value;
  }

  void setFormValue(GlobalKey<FormBuilderState>? form, String field, dynamic value) {
    if(form == null || form.currentState == null || form.currentState?.fields.containsKey(field) == false)
      return;
    setState(() {
      form.currentState?.fields[field]?.didChange(value);
    });
  }
}
