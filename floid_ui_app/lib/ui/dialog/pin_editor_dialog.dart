/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/dialog/missioneditors/pin_editor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PinEditorDialog extends StatelessWidget {
  const PinEditorDialog({
    Key? key,
    required GlobalKey<FormBuilderState> formKey,
    required FloidPinAlt floidPinAlt,
  })  : _formKey = formKey,
        _floidPinAlt = floidPinAlt,
        super(key: key);

  final GlobalKey<FormBuilderState> _formKey;
  final FloidPinAlt _floidPinAlt;

  @override
  Widget build(BuildContext context) {
    return PinEditor(formKey: _formKey, floidPinAlt: _floidPinAlt,);
  }
}
