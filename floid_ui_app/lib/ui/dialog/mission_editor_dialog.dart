/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/ui/dialog/missioneditors/mission_editors.dart';

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/dialog/missioneditors/ok_editor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class MissionEditorDialog extends StatelessWidget {
  const MissionEditorDialog({
    Key? key,
    required GlobalKey<FormBuilderState> formKey,
    required DroidInstruction droidInstruction,
    required FloidPinSetAlt floidPinSetAlt,
    bool isExecute = false,
  })  : _formKey = formKey,
        _droidInstruction = droidInstruction,
        _floidPinSetAlt = floidPinSetAlt,
        _isExecute = isExecute,
        super(key: key);

  final GlobalKey<FormBuilderState> _formKey;
  final DroidInstruction _droidInstruction;
  final FloidPinSetAlt _floidPinSetAlt;
  final bool _isExecute;

  @override
  Widget build(BuildContext context) {
      if(_droidInstruction is AcquireHomePositionCommand) return AcquireHomePositionEditor(formKey: _formKey, droidInstruction: _droidInstruction as AcquireHomePositionCommand, isExecute: _isExecute);
      if(_droidInstruction is DelayCommand) return DelayEditor(formKey: _formKey, droidInstruction: _droidInstruction as DelayCommand, isExecute: _isExecute);
      if(_droidInstruction is DesignateCommand) return DesignateEditor(formKey: _formKey, droidInstruction: _droidInstruction as DesignateCommand, floidPinSetAlt: _floidPinSetAlt, isExecute: _isExecute);
      if(_droidInstruction is FlyToCommand) return FlyToEditor(formKey: _formKey, droidInstruction: _droidInstruction as FlyToCommand, floidPinSetAlt: _floidPinSetAlt, isExecute: _isExecute);
      if(_droidInstruction is HeightToCommand) return HeightToEditor(formKey: _formKey, droidInstruction: _droidInstruction as HeightToCommand, isExecute: _isExecute);
      if(_droidInstruction is HoverCommand) return HoverEditor(formKey: _formKey, droidInstruction: _droidInstruction as HoverCommand, isExecute: _isExecute);
      if(_droidInstruction is LiftOffCommand) return LiftOffEditor(formKey: _formKey, droidInstruction: _droidInstruction as LiftOffCommand, isExecute: _isExecute);
      if(_droidInstruction is PanTiltCommand) return PanTiltEditor(formKey: _formKey, droidInstruction: _droidInstruction as PanTiltCommand, floidPinSetAlt: _floidPinSetAlt, isExecute: _isExecute);
      if(_droidInstruction is PayloadCommand) return PayloadEditor(formKey: _formKey, droidInstruction: _droidInstruction as PayloadCommand, isExecute: _isExecute);
      if(_droidInstruction is PhotoStrobeCommand) return PhotoStrobeEditor(formKey: _formKey, droidInstruction: _droidInstruction as PhotoStrobeCommand, isExecute: _isExecute);
      if(_droidInstruction is RetrieveLogsCommand) return RetrieveLogsEditor(formKey: _formKey, droidInstruction: _droidInstruction as RetrieveLogsCommand, isExecute: _isExecute);
      if(_droidInstruction is SetParametersCommand) return SetParametersEditor(formKey: _formKey, droidInstruction: _droidInstruction as SetParametersCommand, floidPinSetAlt: _floidPinSetAlt, isExecute: _isExecute);
      if(_droidInstruction is SpeakerCommand) return SpeakerEditor(formKey: _formKey, droidInstruction: _droidInstruction as SpeakerCommand, floidPinSetAlt: _floidPinSetAlt, isExecute: _isExecute);
      if(_droidInstruction is SpeakerOnCommand) return SpeakerOnEditor(formKey: _formKey, droidInstruction: _droidInstruction as SpeakerOnCommand, floidPinSetAlt: _floidPinSetAlt, isExecute: _isExecute);
      if(_droidInstruction is TurnToCommand) return TurnToEditor(formKey: _formKey, droidInstruction: _droidInstruction as TurnToCommand, isExecute: _isExecute);
      return OkEditor(formKey: _formKey, droidInstruction: _droidInstruction, isExecute: _isExecute);
  }
}
