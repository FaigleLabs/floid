/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/dialog/form_page_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PinSetNameEditor extends StatefulWidget {

  const PinSetNameEditor({
    Key? key,
    required String pinSetName,
    required GlobalKey<FormBuilderState> formKey,
    required String title,
    required String buttonText,
  })
      : _pinSetName = pinSetName,
        _formKey = formKey,
        _title = title,
        _buttonText = buttonText,
        super(key: key);

  final String _pinSetName;
  final GlobalKey<FormBuilderState> _formKey;
  final String _title;
  final String _buttonText;

  @override
  _PinSetNameEditorState createState() => _PinSetNameEditorState(pinSetMame: _pinSetName);
}

class _PinSetNameEditorState extends State<PinSetNameEditor> with FormPageHelper {

  String _pinSetName;
  _PinSetNameEditorState({required String pinSetMame}) : _pinSetName = pinSetMame;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      insetPadding: EdgeInsets.zero,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: widget._formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: Text(widget._title),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: FormBuilderTextField(
                      name: 'pinSetName',
                      decoration: InputDecoration(
                        isDense: true,
                        labelText: 'Pin Set Name',
                      ),
                      initialValue: _pinSetName,
                      onSaved: (value) {
                        if(value!=null) _pinSetName = value;
                      },
                      validator: FormBuilderValidators.minLength(1),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 12),
                    child: ElevatedButton(
                      child: Text(widget._buttonText),
                      onPressed: () {
                        final  FormBuilderState? fCurrentState = widget._formKey.currentState;
                        if (fCurrentState!=null && fCurrentState.validate()) {
                          fCurrentState.save();
                          Navigator.pop(context, _pinSetName);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 12),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

