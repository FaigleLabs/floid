/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/dialog/form_page_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class DesignateEditor extends StatefulWidget {
  static const double LATITUDE_MIN = -90;
  static const double LATITUDE_MAX = 90;

  static const double LONGITUDE_MIN = -360;
  static const double LONGITUDE_MAX = 306;

  static const double ALTITUDE_MIN = -200;
  static const double ALTITUDE_MAX = 10000;

  const DesignateEditor({
    Key? key,
    required DesignateCommand droidInstruction,
    required GlobalKey<FormBuilderState> formKey,
    required FloidPinSetAlt floidPinSetAlt,
    bool isExecute = false,
  })  : _droidInstruction = droidInstruction,
        _formKey = formKey,
        _floidPinSetAlt = floidPinSetAlt,
        _isExecute = isExecute,
        super(key: key);

  final DesignateCommand _droidInstruction;
  final GlobalKey<FormBuilderState> _formKey;
  final FloidPinSetAlt _floidPinSetAlt;
  final bool _isExecute;

  @override
  _DesignateEditorState createState() => _DesignateEditorState(designateCommand: _droidInstruction, floidPinSetAlt: _floidPinSetAlt);
}
/*
    private int device = 0;  // TODO [PanTilt] THIS NEEDS TO BE ABLE TO CHANGE FOR THE CAMERA (0), DESIGNATOR (1) or the others (2 & 3)
    private double x = 0;
    private double y = 0;
    private double z = 0;
    private boolean useAngle = true;
    private double pan = 0;
    private double tilt = 0;
    private String pinName = "";

 */

class _DesignateEditorState extends State<DesignateEditor> with FormPageHelper {
  _DesignateEditorState({required DesignateCommand designateCommand, required FloidPinSetAlt floidPinSetAlt}) {
//    _dropdownDisabled = !designateCommand.usePin;
    // Set initial value only if it exists in the pin list:
    if (floidPinSetAlt.pinAltList.indexWhere((floidPinAlt) => floidPinAlt.pinName == designateCommand.pinName) >= 0) {
      _dropdownInitialValue = designateCommand.pinName;
    }
  }

//  bool _dropdownDisabled = false;
  String? _dropdownInitialValue;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: widget._formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text('Designate'),
                  ),
                  FormBuilderCheckbox(
                    name: 'usePin',
                    title: new Text('Use Pin'),
                    initialValue: widget._droidInstruction.usePin,
                    onSaved: (value) {
                      if(value!=null) widget._droidInstruction.usePin = value;
                    },
                    /*
                    onChanged: (value) {
                      setState(() {
                        if(value!=null) _dropdownDisabled = !value;
                      });
                    },
                    */
                  ),
                  FormBuilderCheckbox(
//                    enabled: _dropdownDisabled,
                    name: 'usePinHeight',
                    title: new Text('Use Pin Altitude'),
                    initialValue: widget._droidInstruction.usePinHeight,
                    onSaved: (value) {
                      if(value!=null) widget._droidInstruction.usePinHeight = value;
                    },
                    /*
                    onChanged: (value) {
                      setState(() {
                        if(value!=null) _dropdownDisabled = !value;
                      });
                    },
                     */
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderDropdown<String>(
                        decoration: InputDecoration(labelText: 'Pin', hintText: 'Choose a Pin:'),
                        name: 'pinName',
                        items: //_dropdownDisabled
//                            ? []
//                            :
                    widget._floidPinSetAlt.pinAltList.map((floidPinAlt) {
                                return DropdownMenuItem<String>(
                                  child: Text(floidPinAlt.pinName),
                                  value: floidPinAlt.pinName,
                                );
                              }).toList(),
                        isDense: true,
                        initialValue: _dropdownInitialValue,
                        onSaved: (value) {
                          if (value != null) {
                            widget._droidInstruction.pinName = value;
                          } else {
                            widget._droidInstruction.pinName = '';
                          }
                        },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
//                      enabled: !_dropdownDisabled,
                      name: 'latitude',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Latitude',
                      ),
                      initialValue: widget._droidInstruction.y.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.y = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(DesignateEditor.LATITUDE_MIN),
                        FormBuilderValidators.max(DesignateEditor.LATITUDE_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
//                      enabled: !_dropdownDisabled,
                      name: 'longitude',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Longitude',
                      ),
                      initialValue: widget._droidInstruction.x.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.x = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(DesignateEditor.LONGITUDE_MIN),
                        FormBuilderValidators.max(DesignateEditor.LONGITUDE_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
//                      enabled: !_dropdownDisabled,
                      name: 'altitude',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Altitude',
                      ),
                      initialValue: widget._droidInstruction.z.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.z = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(DesignateEditor.ALTITUDE_MIN),
                        FormBuilderValidators.max(DesignateEditor.ALTITUDE_MAX),
                      ]),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(widget._isExecute ? 'Execute' : 'Save'),
                      onPressed: () {
                        final FormBuilderState? fCurrenState = widget._formKey
                            .currentState;
                        if (fCurrenState != null && fCurrenState.validate()) {
                          fCurrenState.save();
                          Navigator.pop(context, widget._droidInstruction);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
