/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/dialog/form_page_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PinEditor extends StatefulWidget {
  static const double PIN_LAT_MIN = -90;
  static const double PIN_LAT_MAX = 90;

  static const double PIN_LNG_MIN = -360;
  static const double PIN_LNG_MAX = 306;

  static const double PIN_ALT_MIN = -200;
  static const double PIN_ALT_MAX = 10000;

  const PinEditor({
    Key? key,
    required FloidPinAlt floidPinAlt,
    required GlobalKey<FormBuilderState> formKey,
  })  : _floidPinAlt = floidPinAlt,
        _formKey = formKey,
        super(key: key);

  final FloidPinAlt _floidPinAlt;
  final GlobalKey<FormBuilderState> _formKey;

  @override
  _PinEditorState createState() => _PinEditorState(floidPinAlt: _floidPinAlt);
}

class _PinEditorState extends State<PinEditor> with FormPageHelper {
  _PinEditorState({required FloidPinAlt floidPinAlt}) {
//    _pinHeightDisabled = floidPinAlt.pinHeightAuto;
  }

//  bool _pinHeightDisabled = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: widget._formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: Text(
                      'Pin',
                      style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: FormBuilderTextField(
                      style: TextStyle(fontSize: 24),
                      name: 'pinName',
                      decoration: InputDecoration(
                        isDense: true,
                        labelText: 'Pin Name',
                        labelStyle: TextStyle(fontSize: 20),
                      ),
                      initialValue: widget._floidPinAlt.pinName,
                      onSaved: (value) {
                        if(value!=null) widget._floidPinAlt.pinName = value;
                      },
                      validator: FormBuilderValidators.minLength(1),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: FormBuilderTextField(
                      style: TextStyle(fontSize: 24),
                      name: 'pinLat',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        isDense: true,
                        labelText: 'Latitude',
                        labelStyle: TextStyle(fontSize: 20),
                      ),
                      initialValue: widget._floidPinAlt.pinLat.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._floidPinAlt.pinLat = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(PinEditor.PIN_LAT_MIN),
                        FormBuilderValidators.max(PinEditor.PIN_LAT_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: FormBuilderTextField(
                      style: TextStyle(fontSize: 24),
                      name: 'pinLng',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        isDense: true,
                        labelText: 'Longitude',
                        labelStyle: TextStyle(fontSize: 20),
                      ),
                      initialValue: widget._floidPinAlt.pinLng.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._floidPinAlt.pinLng = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(PinEditor.PIN_LNG_MIN),
                        FormBuilderValidators.max(PinEditor.PIN_LNG_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: FormBuilderCheckbox(
                      checkColor: Colors.yellowAccent,
                      activeColor: Colors.blue,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                      ),
                      name: 'pinHeightAuto',
                      title: new Text(
                        'Auto Pin Height',
                        style: TextStyle(fontSize: 20),
                      ),
                      initialValue: widget._floidPinAlt.pinHeightAuto,
                      onSaved: (value) {
                        if(value!=null)  widget._floidPinAlt.pinHeightAuto = value;
                      },
/*
                      onChanged: (value) {
                        setState(() {
                          if(value!=null) _pinHeightDisabled = value;
                        });
                      },
 */
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: FormBuilderTextField(
                      style: TextStyle(fontSize: 24),
//                      enabled: _pinHeightDisabled,
                      name: 'pinHeight',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        isDense: true,
                        labelText: 'Altitude (m)',
                        labelStyle: TextStyle(fontSize: 20),
                      ),
                      initialValue: widget._floidPinAlt.pinHeight.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._floidPinAlt.pinHeight = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(PinEditor.PIN_ALT_MIN),
                        FormBuilderValidators.max(PinEditor.PIN_ALT_MAX),
                      ]),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: ElevatedButton(
                      child: Text('Save'),
                      onPressed: () {
                        final FormBuilderState? fCurrenState = widget._formKey.currentState;
                        if (fCurrenState!=null && fCurrenState.validate()) {
                          fCurrenState.save();
                          Navigator.pop(context, widget._floidPinAlt);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
