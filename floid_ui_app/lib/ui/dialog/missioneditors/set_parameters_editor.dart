/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/dialog/form_page_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class SetParametersEditor extends StatefulWidget {
  const SetParametersEditor({
    Key? key,
    required SetParametersCommand droidInstruction,
    required GlobalKey<FormBuilderState> formKey,
    required FloidPinSetAlt floidPinSetAlt,
    bool isExecute = false,
  })  : _droidInstruction = droidInstruction,
        _formKey = formKey,
        _floidPinSetAlt = floidPinSetAlt,
        _isExecute = isExecute,
        super(key: key);

  final SetParametersCommand _droidInstruction;
  final GlobalKey<FormBuilderState> _formKey;
  final FloidPinSetAlt _floidPinSetAlt;
  final bool _isExecute;

  @override
  _SetParametersEditorState createState() => _SetParametersEditorState(setParametersCommand: _droidInstruction, floidPinSetAlt: _floidPinSetAlt);
}

class _SetParametersEditorState extends State<SetParametersEditor> with FormPageHelper {
  _SetParametersEditorState({required SetParametersCommand setParametersCommand, required FloidPinSetAlt floidPinSetAlt}) {
//    _dropdownDisabled = setParametersCommand.firstPinIsHome;
    // Set initial value only if it exists in the pin list:
    if (floidPinSetAlt.pinAltList.indexWhere((floidPinAlt) => floidPinAlt.pinName == setParametersCommand.homePinName) >= 0) {
      _dropdownInitialValue = setParametersCommand.homePinName;
    }
  }

//  bool _dropdownDisabled = false;
  String? _dropdownInitialValue;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: widget._formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text('SetParameters'),
                  ),
                  FormBuilderCheckbox(
                    name: 'firstPinIsHome',
                    title: new Text('First Pin is Home'),
                    initialValue: widget._droidInstruction.firstPinIsHome,
                    onSaved: (value) {
                      if(value!=null) widget._droidInstruction.firstPinIsHome = value;
                    },
/*
                    onChanged: (value) {
                      setState(() {
                        if(value!=null) _dropdownDisabled = value;
                      });
                    },
 */
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderDropdown<String>(
                      decoration: InputDecoration(labelText: 'Pin',  hintText: 'Choose a Pin:'),
                      name: 'homePinName',
                      items:
//                      _dropdownDisabled
//                          ? []
//                          :
                        widget._floidPinSetAlt.pinAltList.map((floidPinAlt) {
                              return DropdownMenuItem<String>(
                                child: Text(floidPinAlt.pinName),
                                value: floidPinAlt.pinName,
                              );
                            }).toList(),
                      isDense: true,
                      initialValue: _dropdownInitialValue,
                      onSaved: (value) {
                        if (value != null) {
                          widget._droidInstruction.homePinName = value;
                        } else {
                          widget._droidInstruction.homePinName = '';
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: 'droidParameters',
                      keyboardType: TextInputType.multiline,
                      maxLines: 8,
                      minLines: 5,
                      decoration: InputDecoration(labelText: 'Parameters'),
                      initialValue: widget._droidInstruction.droidParameters.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.droidParameters = value;
                      },
                    ),
                  ),
                  FormBuilderCheckbox(
                    name: 'resetDroidParameters',
                    title: Text('Reset Parameters'),
                    initialValue: widget._droidInstruction.resetDroidParameters,
                    onSaved: (value) {
                      if(value!=null) widget._droidInstruction.resetDroidParameters = value;
                    },
                    onChanged: (value) {
                      setFormValue(widget._formKey, 'firstPinIsHome', value);
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(widget._isExecute ? 'Execute' : 'Save'),
                      onPressed: () {
                        final FormBuilderState? fCurrenState = widget._formKey
                            .currentState;
                        if (fCurrenState != null && fCurrenState.validate()) {
                          fCurrenState.save();
                          Navigator.pop(context, widget._droidInstruction);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
