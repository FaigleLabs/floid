/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class HeightToEditor extends StatelessWidget {
  static const double HEIGHT_TO_MIN = -10000;
  static const double HEIGHT_TO_MAX = 10000;

  const HeightToEditor({
    Key? key,
    required HeightToCommand droidInstruction,
    required GlobalKey<FormBuilderState> formKey,
    bool isExecute = false,
  })  : _droidInstruction = droidInstruction,
        _formKey = formKey,
        _isExecute = isExecute,
        super(key: key);

  final HeightToCommand _droidInstruction;
  final GlobalKey<FormBuilderState> _formKey;
  final bool _isExecute;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text('Height To'),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: 'z',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Height (m)',
                      ),
                      initialValue: _droidInstruction.z.toString(),
                      onSaved: (value) {
                        if(value!=null) _droidInstruction.z = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(HEIGHT_TO_MIN),
                        FormBuilderValidators.max(HEIGHT_TO_MAX),
                      ]),
                    ),
                  ),
                  FormBuilderCheckbox(
                    title: Text('Absolute'),
                    name: 'absolute',
                    initialValue: _droidInstruction.absolute,
                    onSaved: (value) {
                      if(value!=null) _droidInstruction.absolute = value;
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(_isExecute ? 'Execute' : 'Save'),
                      onPressed: () {
                        final FormBuilderState? fCurrentState = _formKey.currentState;
                        if (fCurrentState!=null && fCurrentState.validate()) {
                          fCurrentState.save();
                          Navigator.pop(context, _droidInstruction);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
