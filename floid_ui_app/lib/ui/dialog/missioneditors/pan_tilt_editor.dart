/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/dialog/form_page_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PanTiltEditor extends StatefulWidget {
  static const double PAN_TILT_MIN = -180;
  static const double PAN_TILT_MAX = 180;

  const PanTiltEditor({
    Key? key,
    required PanTiltCommand droidInstruction,
    required GlobalKey<FormBuilderState> formKey,
    required FloidPinSetAlt floidPinSetAlt,
    bool isExecute = false,
  })  : _droidInstruction = droidInstruction,
        _formKey = formKey,
        _floidPinSetAlt = floidPinSetAlt,
        _isExecute = isExecute,
        super(key: key);

  final PanTiltCommand _droidInstruction;
  final GlobalKey<FormBuilderState> _formKey;
  final FloidPinSetAlt _floidPinSetAlt;
  final bool _isExecute;

  @override
  _PanTiltEditorState createState() => _PanTiltEditorState(panTiltCommand: _droidInstruction, floidPinSetAlt: _floidPinSetAlt);
}

class _PanTiltEditorState extends State<PanTiltEditor> with FormPageHelper {
  _PanTiltEditorState({required PanTiltCommand panTiltCommand, required FloidPinSetAlt floidPinSetAlt}) {
//    _dropdownDisabled = panTiltCommand.useAngle;
    // Set initial value only if it exists in the pin list:
    if (floidPinSetAlt.pinAltList.indexWhere((floidPinAlt) => floidPinAlt.pinName == panTiltCommand.pinName) >= 0) {
      _dropdownInitialValue = panTiltCommand.pinName;
    }
    _deviceDropdownInitialValue = panTiltCommand.device;
  }

  int? _deviceDropdownInitialValue;
//  bool _dropdownDisabled = false;
  String? _dropdownInitialValue;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: widget._formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text('Pan Tilt'),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderDropdown<int>(
                      decoration: InputDecoration(labelText: 'Device', hintText:
                      'Choose a Device:'),
                      name: 'device',
                      items: [
                        DropdownMenuItem<int>(
                          child: Text('0'),
                          value: 0,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('1'),
                          value: 1,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('2'),
                          value: 2,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('3'),
                          value: 3,
                        ),
                      ],
                      isDense: true,
                      initialValue: _deviceDropdownInitialValue,
                      onSaved: (value) {
                        if(value!=null)  widget._droidInstruction.device = value;
                      },
                    ),
                  ),
                  FormBuilderCheckbox(
                    name: 'useAngle',
                    title: new Text('Use Angle'),
                    initialValue: widget._droidInstruction.useAngle,
                    onSaved: (value) {
                      if(value!=null)  widget._droidInstruction.useAngle = value;
                    },
/*
                    onChanged: (value) {
                      setState(() {
                        if(value!=null)  _dropdownDisabled = value;
                      });
                    },
 */
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderDropdown<String>(
                      decoration: InputDecoration(labelText: 'Pin', hintText: 'Choose a Pin:'),
                      name: 'pinName',
                      items:
//                      _dropdownDisabled
//                          ? []
//                          :
                      widget._floidPinSetAlt.pinAltList.map((floidPinAlt) {
                              return DropdownMenuItem<String>(
                                child: Text(floidPinAlt.pinName),
                                value: floidPinAlt.pinName,
                              );
                            }).toList(),
                      isDense: true,
                      initialValue: _dropdownInitialValue,
                      onSaved: (value) {
                        if (value != null) {
                          widget._droidInstruction.pinName = value;
                        } else {
                          widget._droidInstruction.pinName = '';
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
//                      enabled: !_dropdownDisabled,
                      name: 'pan',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Pan',
                      ),
                      initialValue: widget._droidInstruction.pan.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.pan = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(PanTiltEditor.PAN_TILT_MIN),
                        FormBuilderValidators.max(PanTiltEditor.PAN_TILT_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
//                      enabled: !_dropdownDisabled,
                      name: 'tilt',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Tilt',
                      ),
                      initialValue: widget._droidInstruction.tilt.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.tilt = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(PanTiltEditor.PAN_TILT_MIN),
                        FormBuilderValidators.max(PanTiltEditor.PAN_TILT_MAX),
                      ]),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(widget._isExecute ? 'Execute' : 'Save'),
                      onPressed: () {
                        final fCurrentState = widget._formKey.currentState;
                        if (fCurrentState!=null && fCurrentState.validate()) {
                          fCurrentState.save();
                          Navigator.pop(context, widget._droidInstruction);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
