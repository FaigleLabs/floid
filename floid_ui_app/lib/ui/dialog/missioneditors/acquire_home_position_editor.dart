/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class AcquireHomePositionEditor extends StatelessWidget {
  static const double REQUIRED_ALTITUDE_DISTANCE_MIN = 1;
  static const double REQUIRED_ALTITUDE_DISTANCE_MAX = 500;

  static const double REQUIRED_ALTIMETER_GOOD_COUNT_MIN = 1;
  static const double REQUIRED_ALTIMETER_GOOD_COUNT_MAX = 50;

  static const double REQUIRED_GPS_GOOD_COUNT_MIN = 1;
  static const double REQUIRED_GPS_GOOD_COUNT_MAX = 50;

  static const double REQUIRED_HEADING_DISTANCE_MIN = 1;
  static const double REQUIRED_HEADING_DISTANCE_MAX = 30;

  static const double REQUIRED_XY_DISTANCE_MIN = 1;
  static const double REQUIRED_XY_DISTANCE_MAX = 500;

  const AcquireHomePositionEditor({
    Key? key,
    required AcquireHomePositionCommand droidInstruction,
    required GlobalKey<FormBuilderState> formKey,
    bool isExecute = false,
  })
      : _droidInstruction = droidInstruction,
        _formKey = formKey,
        _isExecute = isExecute,
        super(key: key);

  final AcquireHomePositionCommand _droidInstruction;
  final GlobalKey<FormBuilderState> _formKey;
  final bool _isExecute;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text('Acquire Home Position'),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: "requiredAltimeterGoodCount",
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                          labelText: 'Required Altimeter Good Count'
                      ),
                      initialValue: _droidInstruction.requiredAltimeterGoodCount.toString(),
                      onSaved: (value) {
                        if(value!=null) _droidInstruction.requiredAltimeterGoodCount = double.parse(value).floor();
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(REQUIRED_ALTIMETER_GOOD_COUNT_MIN),
                        FormBuilderValidators.max(REQUIRED_ALTIMETER_GOOD_COUNT_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: "requiredGPSGoodCount",
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                          labelText: 'Required GPS Good Count Distance'
                      ),
                      initialValue: _droidInstruction.requiredGPSGoodCount.toString(),
                      onSaved: (value) {
                        if(value!=null) _droidInstruction.requiredGPSGoodCount = double.parse(value).floor();
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(REQUIRED_GPS_GOOD_COUNT_MIN),
                        FormBuilderValidators.max(REQUIRED_GPS_GOOD_COUNT_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: "requiredHeadingDistance",
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                          labelText: 'Required Heaading Distance'
                      ),
                      initialValue: _droidInstruction.requiredHeadingDistance.toString(),
                      onSaved: (value) {
                        if(value!=null) _droidInstruction.requiredHeadingDistance = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(REQUIRED_HEADING_DISTANCE_MIN),
                        FormBuilderValidators.max(REQUIRED_HEADING_DISTANCE_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: "requiredXYDistance",
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                          labelText: 'Required XY Distance'
                      ),
                      initialValue: _droidInstruction.requiredXYDistance.toString(),
                      onSaved: (value) {
                        if(value!=null) _droidInstruction.requiredXYDistance = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(REQUIRED_XY_DISTANCE_MIN),
                        FormBuilderValidators.max(REQUIRED_XY_DISTANCE_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: "requiredAltitudeDistance",
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                          labelText: 'Required Altitude Distance'
                      ),
                      initialValue: _droidInstruction.requiredAltitudeDistance.toString(),
                      onSaved: (value) {
                        if(value!=null) _droidInstruction.requiredAltitudeDistance = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(REQUIRED_ALTITUDE_DISTANCE_MIN),
                        FormBuilderValidators.max(REQUIRED_ALTITUDE_DISTANCE_MAX),
                      ]),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(_isExecute ? 'Execute' : 'Save'),
                      onPressed: () {
                        final FormBuilderState? fCurrenState = _formKey
                            .currentState;
                        if (fCurrenState != null && fCurrenState.validate()) {
                          fCurrenState.save();
                          Navigator.pop(context, _droidInstruction);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
