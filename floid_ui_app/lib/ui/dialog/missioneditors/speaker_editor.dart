/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/dialog/form_page_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class SpeakerEditor extends StatefulWidget {
  static const double PITCH_MIN = 0;
  static const double PITCH_MAX = 10;
  static const double RATE_MIN = 0;
  static const double RATE_MAX = 10;
  static const double VOLUME_MIN = 0;
  static const double VOLUME_MAX = 1;
  static const double DELAY_MIN = 0;
  static const double DELAY_MAX = 10000;

  const SpeakerEditor({
    Key? key,
    required SpeakerCommand droidInstruction,
    required GlobalKey<FormBuilderState> formKey,
    required FloidPinSetAlt floidPinSetAlt,
    bool isExecute = false,
  })  : _droidInstruction = droidInstruction,
        _formKey = formKey,
        _floidPinSetAlt = floidPinSetAlt,
        _isExecute = isExecute,
        super(key: key);

  final SpeakerCommand _droidInstruction;
  final GlobalKey<FormBuilderState> _formKey;
  final FloidPinSetAlt _floidPinSetAlt;
  final bool _isExecute;

  @override
  _SpeakerEditorState createState() => _SpeakerEditorState(speakerCommand: _droidInstruction, floidPinSetAlt: _floidPinSetAlt);
}

class _SpeakerEditorState extends State<SpeakerEditor> with FormPageHelper {
  _SpeakerEditorState({required SpeakerCommand speakerCommand, required FloidPinSetAlt floidPinSetAlt}) {
//    _ttsEnabled = ;
    if (speakerCommand.mode == 'tts') {
      _deviceDropdownInitialValue = 'tts';
    } else {
      _deviceDropdownInitialValue = 'file';
    }
    _speakerRepeatDropdownInitialValue = speakerCommand.speakerRepeat;
  }

  String? _deviceDropdownInitialValue;
  int? _speakerRepeatDropdownInitialValue;
//  bool _ttsEnabled = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: widget._formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text('Speaker'),
                  ),
                  FormBuilderCheckbox(
                    name: 'alert',
                    title: new Text('Alert'),
                    initialValue: widget._droidInstruction.alert,
                    onSaved: (value) {
                      if(value!=null) widget._droidInstruction.alert = value;
                    },
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderDropdown<String>(
                      decoration: InputDecoration(labelText: 'Mode',  hintText: 'Choose Mode',),
                      name: 'mode',
                      items: [
                        DropdownMenuItem<String>(
                          child: Text('TTS'),
                          value: 'tts',
                        ),
                        DropdownMenuItem<String>(
                          child: Text('FILE'),
                          value: 'file',
                        ),
                      ],
                      isDense: true,
                      initialValue: _deviceDropdownInitialValue,
/*
                      onChanged: (value) {
                        setState(() {
                          _ttsEnabled = (value == 'tts');
                        });
                      },
 */
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.mode = value;
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
//                      enabled: _ttsEnabled,
                      name: 'tts',
                      decoration: InputDecoration(
                        labelText: 'TTS',
                      ),
                      initialValue: widget._droidInstruction.tts.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.tts = value;
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
//                      enabled: !_ttsEnabled,
                      name: 'fileName',
                      decoration: InputDecoration(
                        labelText: 'File Name',
                      ),
                      initialValue: widget._droidInstruction.fileName.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.fileName = value;
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: 'pitch',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Pitch (0-10)',
                      ),
                      initialValue: widget._droidInstruction.pitch.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.pitch = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(SpeakerEditor.PITCH_MIN),
                        FormBuilderValidators.max(SpeakerEditor.PITCH_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: 'rate',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Rate (0-10)',
                      ),
                      initialValue: widget._droidInstruction.rate.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.rate = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(SpeakerEditor.RATE_MIN),
                        FormBuilderValidators.max(SpeakerEditor.RATE_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: 'volume',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Volume (0-1)',
                      ),
                      initialValue: widget._droidInstruction.volume.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.volume = double.parse(value);
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(SpeakerEditor.VOLUME_MIN),
                        FormBuilderValidators.max(SpeakerEditor.VOLUME_MAX),
                      ]),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderDropdown<int>(
                      decoration: InputDecoration(labelText: 'Repeat', hintText: 'Choose Repeat'),
                      name: 'speakerRepeat',
                      items: [
                        DropdownMenuItem<int>(
                          child: Text('Infinite'),
                          value: 0,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('None'),
                          value: 1,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('1'),
                          value: 2,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('2'),
                          value: 3,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('3'),
                          value: 4,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('5'),
                          value: 5,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('10'),
                          value: 6,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('20'),
                          value: 7,
                        ),
                        DropdownMenuItem<int>(
                          child: Text('100'),
                          value: 8,
                        ),
                      ],
                      isDense: true,
                      initialValue: _speakerRepeatDropdownInitialValue,
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.speakerRepeat = value;
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
//                      enabled: !_ttsEnabled,
                      name: 'delay',
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(
                        labelText: 'Delay',
                      ),
                      initialValue: widget._droidInstruction.pitch.toString(),
                      onSaved: (value) {
                        if(value!=null) widget._droidInstruction.delay = double.parse(value).floor();
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(SpeakerEditor.DELAY_MIN),
                        FormBuilderValidators.max(SpeakerEditor.DELAY_MAX),
                      ]),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(widget._isExecute ? 'Execute' : 'Save'),
                      onPressed: () {
                        final FormBuilderState? fCurrenState = widget._formKey
                            .currentState;
                        if (fCurrenState != null && fCurrenState.validate()) {
                          fCurrenState.save();
                          Navigator.pop(context, widget._droidInstruction);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
