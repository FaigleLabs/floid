/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/dialog/form_page_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class FlyToEditor extends StatefulWidget {
  static const double LATITUDE_MIN = -90;
  static const double LATITUDE_MAX = 90;

  static const double LONGITUDE_MIN = 360;
  static const double LONGITUDE_MAX = -306;

  const FlyToEditor({
    Key? key,
    required FlyToCommand droidInstruction,
    required GlobalKey<FormBuilderState> formKey,
    required FloidPinSetAlt floidPinSetAlt,
    bool isExecute = false,
  })  : _droidInstruction = droidInstruction,
        _formKey = formKey,
        _floidPinSetAlt = floidPinSetAlt,
        _isExecute = isExecute,
        super(key: key);

  final FlyToCommand _droidInstruction;
  final GlobalKey<FormBuilderState> _formKey;
  final FloidPinSetAlt _floidPinSetAlt;
  final bool _isExecute;

  @override
  _FlyToEditorState createState() => _FlyToEditorState(flyToCommand: _droidInstruction, floidPinSetAlt: _floidPinSetAlt);
}

class _FlyToEditorState extends State<FlyToEditor> with FormPageHelper {
  _FlyToEditorState({required FlyToCommand flyToCommand, required FloidPinSetAlt floidPinSetAlt}) {
    _dropdownInitialValue = null;
//    _dropdownDisabled = flyToCommand.flyToHome;
    // Set initial value only if it exists in the pin list:
    if (floidPinSetAlt.pinAltList.indexWhere((floidPinAlt) => floidPinAlt.pinName == flyToCommand.pinName) >= 0) {
      _dropdownInitialValue = flyToCommand.pinName;
    }
  }

//  bool _dropdownDisabled = false;
  String? _dropdownInitialValue;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: widget._formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text('FlyTo'),
                  ),
                  FormBuilderCheckbox(
                    name: 'flyToHome',
                    title: new Text('Fly to Home'),
                    initialValue: widget._droidInstruction.flyToHome,
                    onSaved: (value) {
                      if(value!=null) widget._droidInstruction.flyToHome = value;
                    },
/*
                    onChanged: (value) {
                      setState(() {
                        if(value!=null) _dropdownDisabled = value;
                      });
                    },
 */
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderDropdown<String>(
                      decoration: InputDecoration(labelText: 'Pin', hintText: 'Choose a Pin:'),
                      name: 'homePinName',
                      items:
//                      _dropdownDisabled
//                          ? []
//                          :
                      widget._floidPinSetAlt.pinAltList.map((floidPinAlt) {
                              return DropdownMenuItem<String>(
                                child: Text(floidPinAlt.pinName),
                                value: floidPinAlt.pinName,
                              );
                            }).toList(),
                      isDense: true,
                      initialValue: _dropdownInitialValue,
                      onSaved: (value) {
                        if (value != null) {
                          widget._droidInstruction.pinName = value;
                        } else {
                          widget._droidInstruction.pinName = '';
                        }
                      },
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(widget._isExecute ? 'Execute' : 'Save'),
                      onPressed: () {
                        final FormBuilderState? fCurrentState  = widget._formKey.currentState;
                        if (fCurrentState!=null && fCurrentState.validate()) {
                          fCurrentState.save();
                          Navigator.pop(context, widget._droidInstruction);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
