/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PhotoStrobeEditor extends StatelessWidget {
  static const double PHOTO_STROBE_DELAY_MIN = 1;
  static const double PHOTO_STROBE_DELAY_MAX = 10000;

  const PhotoStrobeEditor({
    Key? key,
    required PhotoStrobeCommand droidInstruction,
    required GlobalKey<FormBuilderState> formKey,
    bool isExecute = false,
  })  : _droidInstruction = droidInstruction,
        _formKey = formKey,
        _isExecute = isExecute,
        super(key: key);

  final PhotoStrobeCommand _droidInstruction;
  final GlobalKey<FormBuilderState> _formKey;
  final bool _isExecute;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text('Photo Strobe'),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                    child: FormBuilderTextField(
                      name: "delay",
                      keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                      decoration: InputDecoration(labelText: 'Delay'),
                      initialValue: _droidInstruction.delay.toString(),
                      onSaved: (value) {
                        if(value!=null) _droidInstruction.delay = double.parse(value).floor();
                      },
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.numeric(),
                        FormBuilderValidators.min(PHOTO_STROBE_DELAY_MIN),
                        FormBuilderValidators.max(PHOTO_STROBE_DELAY_MAX),
                      ]),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text(_isExecute ? 'Execute' : 'Save'),
                      onPressed: () {
                        final FormBuilderState? fCurrenState = _formKey.currentState;
                        if (fCurrenState!=null && fCurrenState.validate()) {
                          fCurrenState.save();
                          Navigator.pop(context, _droidInstruction);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
