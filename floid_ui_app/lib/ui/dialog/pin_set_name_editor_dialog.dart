/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/dialog/pin_set_name_editor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PinSetNameEditorDialog extends StatelessWidget {
  const PinSetNameEditorDialog({
    Key? key,
    required GlobalKey<FormBuilderState> formKey,
    required String pinSetName,
    required String title,
    required String buttonText,
  })
      : _formKey = formKey,
        _pinSetName = pinSetName,
        _title = title,
        _buttonText = buttonText,
        super(key: key);

  final GlobalKey<FormBuilderState> _formKey;
  final String _pinSetName;
  final String _title;
  final String _buttonText;

  @override
  Widget build(BuildContext context) {
    return PinSetNameEditor(formKey: _formKey, pinSetName: _pinSetName, title: _title, buttonText: _buttonText);
  }
}
