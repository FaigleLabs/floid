/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/dialog/form_page_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class MissionNameEditor extends StatefulWidget {

  const MissionNameEditor({
    Key? key,
    required String missionName,
    required GlobalKey<FormBuilderState> formKey,
    required String title,
    required String buttonText,
  })
      : _missionName = missionName,
        _formKey = formKey,
        _title = title,
        _buttonText = buttonText,
        super(key: key);

  final String _missionName;
  final GlobalKey<FormBuilderState> _formKey;
  final String _title;
  final String _buttonText;

  @override
  _MissionNameEditorState createState() => _MissionNameEditorState(missionMame: _missionName);
}

class _MissionNameEditorState extends State<MissionNameEditor> with FormPageHelper {

  String _missionName;
  _MissionNameEditorState({required String missionMame}) : _missionName = missionMame;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      insetPadding: EdgeInsets.zero,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: widget._formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: Text(widget._title),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                    child: FormBuilderTextField(
                      name: 'missionName',
                      decoration: InputDecoration(
                        isDense: true,
                        labelText: 'Mission Name',
                      ),
                      initialValue: _missionName,
                      onSaved: (value) {
                        if(value!=null)_missionName = value;
                      },
                      validator:
                        FormBuilderValidators.minLength(1),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 12),
                    child: ElevatedButton(
                      child: Text(widget._buttonText),
                      onPressed: () {
                        final FormBuilderState? fCurrentState = widget._formKey.currentState;
                        if (fCurrentState!=null && fCurrentState.validate()) {
                          fCurrentState.save();
                          Navigator.pop(context, _missionName);
                        }
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 12),
                    child: ElevatedButton(
                      child: Text('Cancel'),
                      onPressed: () {
                        Navigator.pop(context, null);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

