/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';

import 'login.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthenticationBloc authenticationBloc;
  LoginBloc({
    required this.authenticationBloc,
  })  : super(LoginInitial()) {
    on<LoginEvent>((LoginEvent event, Emitter<LoginState> emit)
    async {
      if (event is LoginButtonPressed) {
        emit(LoginLoading());
        try {
          // Authenticate:
          final token = await event.floidServerRepository.authenticate(event.username, event.password);
          authenticationBloc.add(LoggedIn(token: token));
          emit(LoginInitial());
        } catch (error) {
          emit(LoginFailure(error: error.toString()));
        }
      }
    });
  }
}
