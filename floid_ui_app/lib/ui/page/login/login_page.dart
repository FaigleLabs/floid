/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/settings/settings.dart';
import 'package:floid_ui_app/ui/settings/settings_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'login.dart';

class LoginPage extends StatelessWidget {

  LoginPage({Key? key,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        centerTitle: false,
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 0, 4, 0),
              child: Image(image: AssetImage('assets/fl.png'), height: 28,),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 2, 0, 0),
              child: Text('FaigleLabs', style: TextStyle(fontSize: 24, fontStyle: FontStyle.italic,)),
            ),
          ],
        ),
        backgroundColor: Theme
            .of(context)
            .primaryColor,
        foregroundColor: Theme
            .of(context)
            .splashColor,
      ),
      body:  BlocBuilder<SettingsBloc, SettingsState>(
              builder: (context, settingsState) {
                return SingleChildScrollView(
                  child: Center(
                    child: Column(
                      children: [
                        Text('FLOID', style: TextStyle(fontSize: 32, fontStyle: FontStyle.normal),),
                        Text('UAV System', style: TextStyle(fontSize: 22, fontStyle: FontStyle.italic, height: 0.8),),
                        if(settingsState is SettingsLoaded) Padding(
                          padding: const EdgeInsets.fromLTRB(0, 12, 0, 8),
                          child: LoginForm(),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                          child: Settings(),
                        ),
                        if(settingsState is SettingsLoaded /* && !kIsWeb */)
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Icon(settingsState.secure ? MdiIcons.lockOutline : MdiIcons.lockOpenAlertOutline, size: 18, color: Colors.orangeAccent[400],),
                              Text(settingsState.host, style: TextStyle(color: Colors.orangeAccent[400], fontSize: 20,),),
                              Text(':', style: TextStyle(color: Colors.orangeAccent[400], fontSize: 20,),),
                              Text(settingsState.port.toString(), style: TextStyle(color: Colors.orangeAccent[400], fontSize: 20,),),
                            ],
                          ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 24, 0, 0),
                          child: Text('© Chris Faigle / FaigleLabs\nALL RIGHTS RESERVED', textAlign: TextAlign.center, style: TextStyle(fontSize: 22, color: Theme
                              .of(context)
                              .splashColor,),),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                          child: Text(
                            'The Floid product is strictly licensed and may only be used non-commercially and in compliance with applicable laws', textAlign: TextAlign.center, style: TextStyle(
                              fontSize: 24, color: Theme
                              .of(context)
                              .splashColor, fontStyle: FontStyle.italic),),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 14, 0, 0),
                          child: Text('CHRIS FAIGLE / FAIGLELABS ASSUME NO LIABILITY\nUSE ENTIRELY AT YOUR OWN RISK', textAlign: TextAlign.center, style: TextStyle(fontSize: 14)),
                        ),
                      ],
                    ),
                  ),
                );
              }
          )
    );
  }
}
