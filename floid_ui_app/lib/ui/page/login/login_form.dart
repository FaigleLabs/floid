/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'login.dart';

class LoginForm extends StatefulWidget {
  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  late TextEditingController _usernameController;
  late TextEditingController _passwordController;

  @override void initState() {
    super.initState();
    _passwordController = TextEditingController();
    _usernameController = TextEditingController();
    _usernameController.addListener(() {
      final isButtonActive = _usernameController.text.isNotEmpty
          && _passwordController.text.isNotEmpty;
      setState(() => this.isButtonActive = isButtonActive);
    });
    _passwordController.addListener(() {
      final isButtonActive = _usernameController.text.isNotEmpty
          && _passwordController.text.isNotEmpty;
      setState(() => this.isButtonActive = isButtonActive);
    });
  }

  @override void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  bool isButtonActive = false;

  @override
  Widget build(BuildContext context) {
    _onLoginButtonPressed() {
      // debugPrint('Pressed');
      setState(() {
        isButtonActive = false;
      });
      if(BlocProvider.of<FloidServerRepositoryBloc>(context).state is FloidServerRepositoryStateHasHost) {
        BlocProvider.of<LoginBloc>(context).add(
            LoginButtonPressed(
              floidServerRepository: (BlocProvider.of<FloidServerRepositoryBloc>(context).state as FloidServerRepositoryStateHasHost).floidServerRepository,
              username: _usernameController.text,
              password: _passwordController.text,
            ));
      } else {
        print('Login Button: FloidServerRepositoryState is not FloidServerRepositoryStateHasHost');
      }
    }

    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text('${state.error}'),
              backgroundColor: Theme
                  .of(context)
                  .colorScheme.error,
            ),
          );
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          return Form(
            child: Column(
              children: [
                SizedBox(
                  width: 200,
                  child: TextFormField(
                    autofocus: true,
                    onFieldSubmitted: (value) {
                      debugPrint('Submitted');
                    },
                    textInputAction: TextInputAction.next,
                    style: TextStyle(fontSize: 24),
                    decoration: InputDecoration(labelText: 'Username:',),
                    controller: _usernameController,
                  ),
                ),
                SizedBox(
                  width: 200,
                  child: TextFormField(
                    autofocus: true,
                    onFieldSubmitted: (value) {
                      debugPrint('Submitted');
                      if (isButtonActive &&
                          (state is! LoginLoading)) _onLoginButtonPressed();
                    },
                    textInputAction: TextInputAction.send,
                    style: TextStyle(fontSize: 24),
                    decoration: InputDecoration(labelText: 'Password:'),
                    controller: _passwordController,
                    obscureText: true,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 12, 0, 0),
                  child: ElevatedButton(
                    onPressed:
                    isButtonActive && (state is! LoginLoading)
                        ? _onLoginButtonPressed
                        : null,
                    child: Text('Login', style: TextStyle(fontSize: 18),),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
