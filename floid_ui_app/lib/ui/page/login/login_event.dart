/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:equatable/equatable.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';

abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const []]) : super();
}

class LoginButtonPressed extends LoginEvent {
  final FloidServerRepository floidServerRepository;
  final String username;
  final String password;

  LoginButtonPressed({
    required this.floidServerRepository,
    required this.username,
    required this.password,
  }) : super([username, password]);

  @override
  String toString() =>
      'LoginButtonPressed { username: $username, password: $password }';
  @override
  List<Object> get props => [username, password];
}
