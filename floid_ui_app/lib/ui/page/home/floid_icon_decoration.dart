/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:flutter/material.dart';

class FloidIconDecoration extends StatelessWidget {

  static const Color BACKGROUND_COLOR = Colors.transparent;
  static const Color TRUE_COLOR = Colors.yellowAccent;
  static const Color FALSE_COLOR = Color(0xFFFFA726);
  static const Color TEXT_COLOR = Colors.yellowAccent;
  static const double TEXT_SIZE = 12;
  static const double TOP_LINE_TEXT_SIZE = 10;

  Color iconColor(bool truthiness) {
    if(truthiness) {
      return TRUE_COLOR;
    } else {
      return FALSE_COLOR;
    }
  }

  final bool truthiness;
  final IconData iconData;
  final String label;
  final String? subLabel;
  const FloidIconDecoration({
    Key? key,
    required this.label,
    required this.truthiness,
    required this.iconData,
    this.subLabel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String? fSubLabel = this.subLabel;
    return Container(
      decoration:BoxDecoration(color: Color(0x00000000)),
      child: Padding(
            padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
            child: Column(
              children: <Widget>[
                if(fSubLabel==null) Padding(
                  padding: const EdgeInsets.fromLTRB(1, 2, 1, 0),
                  child: Text(label, style: TextStyle(fontSize: TEXT_SIZE, color: iconColor(truthiness))),
                ),
                if(fSubLabel!=null) Padding(
                  padding: const EdgeInsets.fromLTRB(1, 2, 1, 0),
                  child: Text(label, style: TextStyle(fontSize: TOP_LINE_TEXT_SIZE, color: iconColor(true), fontWeight: FontWeight.w800)),
                ),
                if(fSubLabel!=null) Padding(
                  padding: const EdgeInsets.fromLTRB(0,0,0, 0),
                  child: Text(fSubLabel, style: TextStyle(fontSize: TEXT_SIZE, color: iconColor(truthiness))),
                ),
              ],
            ),
          ),
    );
  }
}
