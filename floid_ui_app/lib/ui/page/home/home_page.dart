/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:convert';
import 'package:floid_ui_app/floidserver/bloc/droid_track_filtered_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_map_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_track_filtered_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_track_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/droid_track_gpx_file.dart';
import 'package:floid_ui_app/ui/js/js_proxy.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/ui/dialog/mission_name_editor_dialog.dart';
import 'package:floid_ui_app/ui/dialog/pin_set_name_editor_dialog.dart';
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:floid_ui_app/ui/keys/keys.dart';
import 'package:floid_ui_app/floidserver/listener/floid_bloc_listeners.dart';
import 'package:floid_ui_app/common/common.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/menu/popup_menu.dart';
import 'package:floid_ui_app/ui/widgets/widgets.dart';
import 'package:floid_ui_app/floidserver/provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class HomePage extends StatelessWidget {
  static const double menuHeaderFontSize = 24;
  static const double menuItemFontSize = 20;
  static IconData FLOID_CHOOSER_ICON = MdiIcons.quadcopter;
  static const double FLOID_CHOOSER_ICON_SIZE = 22;
  static IconData MISSION_CHOOSER_ICON = MdiIcons.briefcaseArrowUpDownOutline;
  static const double MISSION_CHOOSER_ICON_SIZE = 24;
  static IconData PIN_SET_CHOOSER_ICON = MdiIcons.mapMarker;
  static const double PIN_SET_CHOOSER_ICON_SIZE = 24;
  static const Color PANEL_BACKGROUND_COLOR = Color(0xFF222222);
  static const Color PANEL_TITLE_COLOR = Color(0xFFFFFF00);

  HomePage({Key? key}) : super(key: key);

  /// We have a new pin set list, add an event to fetch the first pin set:
  void _processNewPinSetList(BuildContext context, DroidPinSetList droidPinSetList) {
    BlocProvider.of<FloidPinSetBloc>(context).add(FetchFloidPinSet(droidPinSetList: droidPinSetList));
  }

  /// We have a new mission list, add an event to fetch the first mission:
  void _processNewDroidMissionList(BuildContext context, DroidMissionList droidMissionList) {
    BlocProvider.of<FloidMissionBloc>(context).add(FetchFloidMission(droidMissionList: droidMissionList));
  }

  /// Process a new floid id (and state) when loaded...
  void _processNewFloidId(BuildContext context, FloidIdAndState floidIdAndState) {
    BlocProvider.of<FloidIdBloc>(context).add(SetFloidIdAndState(floidIdAndState: floidIdAndState));
  }

  /// Delete specified floid id
  // ignore: unused_element
  void _deleteFloidId(BuildContext context, int floidId) {
    BlocProvider.of<FloidIdBloc>(context).add(DeleteFloidId(floidId: floidId));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FloidServerRepositoryBloc, FloidServerRepositoryState>(builder: (context, floidServerRepositoryState) {
      // Wrap in a Floid Server provider:
      return (floidServerRepositoryState is FloidServerRepositoryStateHasHost)
          ? FloidServerProvider(
        floidServerRepository: floidServerRepositoryState.floidServerRepository,
        child: FloidIdProvider(
          floidServerRepository: floidServerRepositoryState.floidServerRepository,
          child: FloidUuidProvider(
            floidServerRepository: floidServerRepositoryState.floidServerRepository,
            // All of the Floid Bloc Listener logic injected here:
            child: FloidBlocListeners(
              // Floid Pin Set List State:
              child: BlocBuilder<FloidAppStateBloc, FloidAppState>(
                builder: (context, floidAppState) {
                  // Floid Mission List State:
                  return BlocBuilder<FloidPinSetListBloc, FloidPinSetListState>(
                    builder: (context, floidPinSetListState) {
                      // Floid Mission List State:
                      return BlocBuilder<FloidMissionListBloc, FloidMissionListState>(
                        builder: (context, floidMissionListState) {
                          // Floid List State:
                          return BlocBuilder<FloidIdAndStateListBloc, FloidIdAndStateListState>(
                            builder: (context, floidIdAndStateListState) {
                              // Floid Pin Set State:
                              return BlocBuilder<FloidPinSetBloc, FloidPinSetState>(
                                builder: (context, floidPinSetState) {
                                  // Floid Mission State:
                                  return BlocBuilder<FloidMissionBloc, FloidMissionState>(
                                    builder: (context, floidMissionState) {
                                      return BlocBuilder<FloidMessagesBloc, FloidMessagesState>(
                                        builder: (context, floidMessagesState) {
                                          return BlocBuilder<FloidTrackGpxFileBloc, FloidTrackGpxFileState>(
                                            builder: (context, floidTrackGpxFileState) {
                                              return BlocBuilder<DroidTrackGpxFileBloc, DroidTrackGpxFileState>(
                                                builder: (context, droidTrackGpxFileState) {
                                                  return BlocBuilder<FloidTrackFilteredGpxFileBloc, FloidTrackFilteredGpxFileState>(
                                                    builder: (context, floidTrackFilteredGpxFileState) {
                                                      return BlocBuilder<DroidTrackFilteredGpxFileBloc, DroidTrackFilteredGpxFileState>(
                                                        builder: (context, droidTrackFilteredGpxFileState) {
                                                          return BlocBuilder<FloidMapGpxFileBloc, FloidMapGpxFileState>(
                                                            builder: (context, floidMapGpxFileState) {
                                                              return DefaultTabController(
                                                                length: 4,
                                                                child: Scaffold(
                                                                  // Drawer:
                                                                  drawer: Drawer(
                                                                    child: Padding(
                                                                      padding: const EdgeInsets.fromLTRB(0, 48, 0, 0),
                                                                      child: Container(
                                                                        child: Column(
                                                                          mainAxisSize: MainAxisSize.min,
                                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                                          children: <Widget>[
                                                                            Padding(
                                                                              padding: const EdgeInsets.fromLTRB(8, 16, 0, 0),
                                                                              child: Text('Pin Set',
                                                                                style: TextStyle(fontSize: menuHeaderFontSize),
                                                                              ),
                                                                            ),
                                                                            InkWell(child: Padding(
                                                                              padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                              child: Text('Create...',
                                                                                style: TextStyle(fontSize: menuItemFontSize,),),
                                                                            ),
                                                                              onTap: () async {
                                                                                String? newPinSetName =
                                                                                await showDialog<String>(
                                                                                    context: context,
                                                                                    builder: (BuildContext context) {
                                                                                      final _formKey = GlobalKey<FormBuilderState>();
                                                                                      return PinSetNameEditorDialog(formKey: _formKey,
                                                                                          pinSetName: 'New Pin Set',
                                                                                          title: 'Create Pin Set',
                                                                                          buttonText: 'Create');
                                                                                    });
                                                                                if (newPinSetName != null) {
                                                                                  BlocProvider.of<FloidMessagesBloc>(context).add(
                                                                                      AddFloidMessage(message: 'Creating Pin Set: $newPinSetName'));
                                                                                  // Create new pin set:
                                                                                  BlocProvider.of<FloidPinSetBloc>(context).add(CreateFloidPinSetEvent(newPinSetName: newPinSetName));
                                                                                }
                                                                              },),

                                                                            if(floidPinSetState is FloidPinSetLoaded && kIsWeb)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Export...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  JSProxy().saveData(jsonEncode(floidPinSetState.floidPinSetAlt.toJson()),
                                                                                      floidPinSetState.floidPinSetAlt.pinSetName + '_Pin_Set.json');
                                                                                },
                                                                              ),
                                                                            if(floidPinSetState is FloidPinSetLoaded)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Duplicate...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  String? newPinSetName =
                                                                                  await showDialog<String>(
                                                                                      context: context,
                                                                                      builder: (BuildContext context) {
                                                                                        final _formKey = GlobalKey<FormBuilderState>();
                                                                                        return PinSetNameEditorDialog(formKey: _formKey,
                                                                                            pinSetName: 'New Pin Set',
                                                                                            title: 'Duplicate Pin Set',
                                                                                            buttonText: 'Duplicate');
                                                                                      });
                                                                                  if (newPinSetName != null) {
                                                                                    BlocProvider.of<FloidMessagesBloc>(context).add(
                                                                                        AddFloidMessage(
                                                                                            message: 'Duplicating pin set: ' + floidPinSetState.floidPinSetAlt.pinSetName));
                                                                                    BlocProvider.of<FloidPinSetBloc>(context).add(DuplicateFloidPinSetEvent(
                                                                                        newPinSetName: newPinSetName, pinSetId: floidPinSetState.floidPinSetAlt.id));
                                                                                  }
                                                                                },
                                                                              ),
                                                                            if(floidPinSetState is FloidPinSetLoaded)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Delete...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  bool? okToDelete =
                                                                                  await showDialog<bool>(
                                                                                      context: context,
                                                                                      builder: (BuildContext context) {
                                                                                        return SimpleDialog(
                                                                                          title: const Text('Delete Pin Set?', style: TextStyle(color: PANEL_TITLE_COLOR),),
                                                                                          children: <Widget>[
                                                                                            SimpleDialogOption(
                                                                                              onPressed: () {
                                                                                                Navigator.pop(context, true);
                                                                                              },
                                                                                              child: const Text('Ok', style: TextStyle(color: PANEL_TITLE_COLOR),),
                                                                                            ),
                                                                                            SimpleDialogOption(
                                                                                              onPressed: () {
                                                                                                Navigator.pop(context, false);
                                                                                              },
                                                                                              child: const Text('Cancel', style: TextStyle(color: PANEL_TITLE_COLOR),),
                                                                                            ),
                                                                                          ],
                                                                                        );
                                                                                      });
                                                                                  if (okToDelete != null && okToDelete) {
                                                                                    // Delete Pin Set:
                                                                                    BlocProvider.of<FloidMessagesBloc>(context).add(
                                                                                        AddFloidMessage(
                                                                                            message: 'Deleting pin set: ' + floidPinSetState.floidPinSetAlt.pinSetName));
                                                                                    BlocProvider.of<FloidPinSetBloc>(context).add(
                                                                                        DeleteFloidPinSetEvent(pinSetId: floidPinSetState.floidPinSetAlt.id));
                                                                                  }
                                                                                },
                                                                              ),
                                                                            if(kIsWeb)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Import...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  JSProxy().importPinSet((data, name) async {
                                                                                    try {
                                                                                      print('Importing pin set data: ' + data.toString());
                                                                                      String fullDataString = data.toString();
                                                                                      String pinSetJsonBase64 = fullDataString.substring(
                                                                                          'data:application/json;base64,'.length);
                                                                                      print('pinSetJsonBase64: ' + pinSetJsonBase64);
                                                                                      String pinSetJsonBase64Normalized = base64.normalize(pinSetJsonBase64);
                                                                                      print('pinSetJsonBase64Normalized: ' + pinSetJsonBase64Normalized);
                                                                                      Uint8List pinSetJsonUint8List = base64.decode(pinSetJsonBase64Normalized);
                                                                                      print('pinSetJsonUint8List: ' + pinSetJsonUint8List.toString());
                                                                                      String pinSetJson = utf8.decode(pinSetJsonUint8List);
                                                                                      print('pinSetJson: ' + pinSetJson);
                                                                                      int pinSetId = await floidServerRepositoryState.floidServerRepository.importPinSet(pinSetJson);
                                                                                      if (pinSetId != FloidPinSetAlt.NO_PIN_SET) {
                                                                                        // Event to save this pin set id locally:
                                                                                        BlocProvider.of<FloidAppStateBloc>(context).add(SetSavedPinSetIdAppStateEvent(savedPinSetId: pinSetId));
                                                                                        // Event to unload pinset:
                                                                                        BlocProvider.of<FloidPinSetBloc>(context).add(UnloadFloidPinSetEvent());
                                                                                        // Event to load the new list which then loads the pin set from id in app state above:
                                                                                        BlocProvider.of<FloidPinSetListBloc>(context).add(FetchFloidPinSetList());
                                                                                        // And alert:
                                                                                        Alert(context: context, title: 'Pin Set Imported',
                                                                                            style: AlertStyle(backgroundColor: Theme
                                                                                                .of(context)
                                                                                                .canvasColor, titleStyle: TextStyle(color: Theme
                                                                                                .of(context)
                                                                                                .splashColor)),
                                                                                            buttons: [
                                                                                              DialogButton(
                                                                                                color: Theme
                                                                                                    .of(context)
                                                                                                    .highlightColor,
                                                                                                child: Text(
                                                                                                  "OK",
                                                                                                  style: TextStyle(color: Theme
                                                                                                      .of(context)
                                                                                                      .splashColor, fontSize: 20),
                                                                                                ),
                                                                                                onPressed: () => Navigator.pop(context),
                                                                                              ),
                                                                                            ]).show();
                                                                                      } else {
                                                                                        Alert(context: context, title: 'Pin Set Failed to Import',
                                                                                            style: AlertStyle(backgroundColor: Theme
                                                                                                .of(context)
                                                                                                .canvasColor, titleStyle: TextStyle(color: Theme
                                                                                                .of(context)
                                                                                                .splashColor)),
                                                                                            buttons: [
                                                                                              DialogButton(
                                                                                                color: Theme
                                                                                                    .of(context)
                                                                                                    .highlightColor,
                                                                                                child: Text(
                                                                                                  "OK",
                                                                                                  style: TextStyle(color: Theme
                                                                                                      .of(context)
                                                                                                      .splashColor, fontSize: 20),
                                                                                                ),
                                                                                                onPressed: () => Navigator.pop(context),
                                                                                              ),
                                                                                            ]).show();
                                                                                      }
                                                                                    } catch (e) {
                                                                                      print('Exception importing pin set: ' + e.toString());
                                                                                    }
                                                                                  });
                                                                                },
                                                                              ),
                                                                            Padding(
                                                                              padding: const EdgeInsets.fromLTRB(8, 16, 0, 0),
                                                                              child: Text('Mission',
                                                                                style: TextStyle(fontSize: menuHeaderFontSize),
                                                                              ),
                                                                            ),
                                                                            InkWell(child: Padding(
                                                                              padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                              child: Text('Create...',
                                                                                style: TextStyle(fontSize: menuItemFontSize,),),
                                                                            ),
                                                                              onTap: () async {
                                                                                String? newMissionName =
                                                                                await showDialog<String>(
                                                                                    context: context,
                                                                                    builder: (BuildContext context) {
                                                                                      final _formKey = GlobalKey<FormBuilderState>();
                                                                                      return MissionNameEditorDialog(formKey: _formKey,
                                                                                          missionName: 'New Mission',
                                                                                          title: 'Create Mission',
                                                                                          buttonText: 'Create');
                                                                                    });
                                                                                if (newMissionName != null) {
                                                                                  BlocProvider.of<FloidMessagesBloc>(context).add(
                                                                                      AddFloidMessage(message: 'Creating Mission: $newMissionName'));
                                                                                  // Create New Mission:
                                                                                  BlocProvider.of<FloidMissionBloc>(context).add(
                                                                                      CreateFloidMissionEvent(newMissionName: newMissionName));
                                                                                }
                                                                              },
                                                                            ),
                                                                            if(floidMissionState is FloidMissionLoaded && kIsWeb)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Export...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  JSProxy().saveData(jsonEncode(floidMissionState.droidMission.toJson()),
                                                                                      floidMissionState.droidMission.missionName + '_Mission.json');
                                                                                },
                                                                              ),
                                                                            if(floidMissionState is FloidMissionLoaded)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Duplicate...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  String? newMissionName =
                                                                                  await showDialog<String>(
                                                                                      context: context,
                                                                                      builder: (BuildContext context) {
                                                                                        final _formKey = GlobalKey<FormBuilderState>();
                                                                                        return MissionNameEditorDialog(formKey: _formKey,
                                                                                            missionName: 'New Mission',
                                                                                            title: 'Duplicate Mission',
                                                                                            buttonText: 'Duplicate');
                                                                                      });
                                                                                  if (newMissionName != null) {
                                                                                    // Duplicate Mission:
                                                                                    BlocProvider.of<FloidMessagesBloc>(context).add(
                                                                                        AddFloidMessage(
                                                                                            message: 'Duplicating mission: ' + floidMissionState.droidMission.missionName + ' -> ' +
                                                                                                newMissionName));
                                                                                    BlocProvider.of<FloidMissionBloc>(context).add(DuplicateFloidMissionEvent(
                                                                                        newMissionName: newMissionName, missionId: floidMissionState.droidMission.id));
                                                                                  }
                                                                                },
                                                                              ),
                                                                            if(floidMissionState is FloidMissionLoaded)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Delete...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  bool? okToDelete =
                                                                                  await showDialog<bool>(
                                                                                      context: context,
                                                                                      builder: (BuildContext context) {
                                                                                        return SimpleDialog(
                                                                                          title: const Text('Delete Mission?', style: TextStyle(color: PANEL_TITLE_COLOR),),
                                                                                          children: <Widget>[
                                                                                            SimpleDialogOption(
                                                                                              onPressed: () {
                                                                                                Navigator.pop(context, true);
                                                                                              },
                                                                                              child: const Text('Ok'),
                                                                                            ),
                                                                                            SimpleDialogOption(
                                                                                              onPressed: () {
                                                                                                Navigator.pop(context, false);
                                                                                              },
                                                                                              child: const Text('Cancel'),
                                                                                            ),
                                                                                          ],
                                                                                        );
                                                                                      });
                                                                                  if (okToDelete != null && okToDelete) {
                                                                                    // Delete Mission:
                                                                                    BlocProvider.of<FloidMessagesBloc>(context).add(
                                                                                        AddFloidMessage(
                                                                                            message: 'Deleting mission: ' + floidMissionState.droidMission.missionName));
                                                                                    BlocProvider.of<FloidMissionBloc>(context).add(
                                                                                        DeleteFloidMissionEvent(missionId: floidMissionState.droidMission.id));
                                                                                  }
                                                                                },
                                                                              ),
                                                                            if(kIsWeb)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Import...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  JSProxy().importMission((data, name) async {
                                                                                    try {
                                                                                      print('Importing mission: ' + data.toString());
                                                                                      String fullDataString = data.toString();
                                                                                      String missionJsonBase64 = fullDataString.substring(
                                                                                          'data:application/json;base64,'.length);
                                                                                      print('missionJsonBase64: ' + missionJsonBase64);
                                                                                      String missionJsonBase64Normalized = base64.normalize(missionJsonBase64);
                                                                                      print('missionJsonBase64Normalized: ' + missionJsonBase64Normalized);
                                                                                      Uint8List missionJsonUint8List = base64.decode(missionJsonBase64Normalized);
                                                                                      print('missionJsonUint8List: ' + missionJsonUint8List.toString());
                                                                                      String missionJson = utf8.decode(missionJsonUint8List);
                                                                                      print('missionJson: ' + missionJson);

                                                                                      int missionId = await floidServerRepositoryState.floidServerRepository.importMission(missionJson);
                                                                                      if (missionId != DroidMission.NO_MISSION) {
                                                                                        // Event to save this mission id locally:
                                                                                        BlocProvider.of<FloidAppStateBloc>(context).add(SetSavedMissionIdAppStateEvent(savedMissionId: missionId));
                                                                                        // Event to unload mission:
                                                                                        BlocProvider.of<FloidMissionBloc>(context).add(UnloadFloidMissionEvent());
                                                                                        // Event to load the new list which then loads the mission from id in app state above:
                                                                                        BlocProvider.of<FloidMissionListBloc>(context).add(FetchFloidMissionList());
                                                                                        // And alert:
                                                                                        Alert(context: context, title: 'Mission Imported',
                                                                                            style: AlertStyle(backgroundColor: Theme
                                                                                                .of(context)
                                                                                                .canvasColor, titleStyle: TextStyle(color: Theme
                                                                                                .of(context)
                                                                                                .splashColor)),
                                                                                            buttons: [
                                                                                              DialogButton(
                                                                                                color: Theme
                                                                                                    .of(context)
                                                                                                    .highlightColor,
                                                                                                child: Text(
                                                                                                  "OK",
                                                                                                  style: TextStyle(color: Theme
                                                                                                      .of(context)
                                                                                                      .splashColor, fontSize: 20),
                                                                                                ),
                                                                                                onPressed: () => Navigator.pop(context),
                                                                                              ),
                                                                                            ]).show();
                                                                                      } else {
                                                                                        Alert(context: context, title: 'Mission Failed to Import',
                                                                                            style: AlertStyle(backgroundColor: Theme
                                                                                                .of(context)
                                                                                                .canvasColor, titleStyle: TextStyle(color: Theme
                                                                                                .of(context)
                                                                                                .splashColor)),
                                                                                            buttons: [
                                                                                              DialogButton(
                                                                                                color: Theme
                                                                                                    .of(context)
                                                                                                    .highlightColor,
                                                                                                child: Text(
                                                                                                  "OK",
                                                                                                  style: TextStyle(color: Theme
                                                                                                      .of(context)
                                                                                                      .splashColor, fontSize: 20),
                                                                                                ),
                                                                                                onPressed: () => Navigator.pop(context),
                                                                                              ),
                                                                                            ]).show();
                                                                                      }
                                                                                    } catch (e) {
                                                                                      print('Exception importing mission: ' + e.toString());
                                                                                    }
                                                                                  });
                                                                                },
                                                                              ),
                                                                            Padding(
                                                                              padding: const EdgeInsets.fromLTRB(8, 16, 0, 0),
                                                                              child: Text('Floid Id',
                                                                                style: TextStyle(fontSize: menuHeaderFontSize),
                                                                              ),
                                                                            ),
                                                                            InkWell(child: Padding(
                                                                              padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                              child: Text('Delete...',
                                                                                style: TextStyle(fontSize: menuItemFontSize,),),
                                                                            ),
                                                                              onTap: () async {
                                                                                final FloidIdState fFloidIdState = BlocProvider
                                                                                    .of<FloidIdBloc>(context)
                                                                                    .state;
                                                                                int floidId = -1;
                                                                                if (fFloidIdState is FloidIdLoaded) floidId = fFloidIdState.floidIdAndState.floidId;
                                                                                if (fFloidIdState is FloidUuidLoaded) floidId = fFloidIdState.floidIdAndState.floidId;
                                                                                if (floidId != -1) {
                                                                                  bool? okToDelete =
                                                                                  await showDialog<bool>(
                                                                                      context: context,
                                                                                      builder: (BuildContext context) {
                                                                                        return SimpleDialog(
                                                                                          title: Container(
                                                                                              child: const Text('Delete Floid Id?', style: TextStyle(color: PANEL_TITLE_COLOR),)
                                                                                          ),
                                                                                          children: <Widget>[
                                                                                            SimpleDialogOption(
                                                                                              onPressed: () {
                                                                                                Navigator.pop(context, true);
                                                                                              },
                                                                                              child: const Text('Ok'),
                                                                                            ),
                                                                                            SimpleDialogOption(
                                                                                              onPressed: () {
                                                                                                Navigator.pop(context, false);
                                                                                              },
                                                                                              child: const Text('Cancel'),
                                                                                            ),
                                                                                          ],
                                                                                        );
                                                                                      });
                                                                                  if (okToDelete != null && okToDelete) {
                                                                                    // Delete FloidId:
                                                                                    BlocProvider.of<FloidMessagesBloc>(context).add(
                                                                                        AddFloidMessage(
                                                                                            message: 'Deleting floidId: ' + floidId.toString()));
                                                                                    BlocProvider.of<FloidIdBloc>(context).add(DeleteFloidId(floidId: floidId));
                                                                                  }
                                                                                }
                                                                              },
                                                                            ),
                                                                            if(kIsWeb)
                                                                              Padding(
                                                                                padding: const EdgeInsets.fromLTRB(8, 16, 0, 0),
                                                                                child: Text('Gpx File Export',
                                                                                  style: TextStyle(fontSize: menuHeaderFontSize),
                                                                                ),
                                                                              ),
                                                                            if(floidMapGpxFileState is FloidMapGpxFileLoaded && kIsWeb)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Pins And Mission...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  JSProxy().saveData(floidMapGpxFileState.floidMapGpxFile, floidMapGpxFileState.fileName);
                                                                                },
                                                                              ),
                                                                            // Export Floid Track Filtered:
                                                                            if(floidTrackFilteredGpxFileState is FloidTrackFilteredGpxFileLoaded && kIsWeb)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Floid Track Filtered...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  JSProxy().saveData(floidTrackFilteredGpxFileState.floidTrackFilteredGpxFile, floidTrackFilteredGpxFileState.fileName);
                                                                                },
                                                                              ),
                                                                            // Export Floid Track:
                                                                            if(floidTrackGpxFileState is FloidTrackGpxFileLoaded && kIsWeb)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Floid Track Full...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  JSProxy().saveData(floidTrackGpxFileState.floidTrackGpxFile, floidTrackGpxFileState.fileName);
                                                                                },
                                                                              ),
                                                                            // Export Droid Track:
                                                                            if(droidTrackFilteredGpxFileState is DroidTrackFilteredGpxFileLoaded && kIsWeb)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Droid Track Filtered...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  JSProxy().saveData(droidTrackFilteredGpxFileState.droidTrackFilteredGpxFile, droidTrackFilteredGpxFileState.fileName);
                                                                                },
                                                                              ),

                                                                            // Export Droid Track:
                                                                            if(droidTrackGpxFileState is DroidTrackGpxFileLoaded && kIsWeb)
                                                                              InkWell(child: Padding(
                                                                                padding: const EdgeInsets.fromLTRB(32, 8, 8, 8),
                                                                                child: Text('Droid Track Full...',
                                                                                  style: TextStyle(fontSize: menuItemFontSize,),),
                                                                              ),
                                                                                onTap: () async {
                                                                                  JSProxy().saveData(droidTrackGpxFileState.droidTrackGpxFile, droidTrackGpxFileState.fileName);
                                                                                },
                                                                              ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  appBar: AppBar(
                                                                    titleSpacing: 0,
                                                                    toolbarHeight: 38,
                                                                    centerTitle: false,
                                                                    title: Text('Floid UAV', style: TextStyle(fontSize: 20)),
                                                                    backgroundColor: Theme
                                                                        .of(context)
                                                                        .primaryColor,
                                                                    foregroundColor: Theme
                                                                        .of(context)
                                                                        .splashColor,
                                                                    actions: <Widget>[
                                                                      IconButton(
                                                                          icon: Icon(PIN_SET_CHOOSER_ICON, size: PIN_SET_CHOOSER_ICON_SIZE, color: Colors.yellowAccent,),
                                                                          onPressed: () async {
                                                                            DroidPinSetList? newDroidPinSetList = await showDialog<DroidPinSetList>(
                                                                              context: context,
                                                                              builder: (BuildContext context) {
                                                                                return floidPinSetListState is FloidPinSetListLoaded
                                                                                    ? PinSetChooser(pinSetListList: floidPinSetListState.droidPinSetListList,
                                                                                  currentPinSetId: floidPinSetState is FloidPinSetLoaded ? floidPinSetState.floidPinSetAlt.id : -1,)
                                                                                    : LoadingIndicator();
                                                                              },
                                                                            );
                                                                            if (newDroidPinSetList != null) {
                                                                              _processNewPinSetList(context, newDroidPinSetList);
                                                                            }
                                                                          }
                                                                      ),
                                                                      IconButton(
                                                                        icon: Icon(MISSION_CHOOSER_ICON, size: MISSION_CHOOSER_ICON_SIZE, color: Colors.yellowAccent,),
                                                                        onPressed: () async {
                                                                          //ignore: close_sinks
                                                                          FloidMissionListBloc floidMissionListBlocLocal = BlocProvider.of<FloidMissionListBloc>(context);
                                                                          DroidMissionList? newDroidMissionList = await showDialog<DroidMissionList>(
                                                                              context: context,
                                                                              builder: (BuildContext context) {
                                                                                return BlocBuilder<FloidMissionListBloc, FloidMissionListState>(
                                                                                    bloc: floidMissionListBlocLocal,
                                                                                    builder: (context, floidMissionListStateLocal) {
                                                                                      return floidMissionListStateLocal is FloidMissionListLoaded
                                                                                          ? MissionChooser(droidMissionListList: floidMissionListStateLocal.droidMissionListList,
                                                                                        currentMissionId: floidMissionState is FloidMissionLoaded ? floidMissionState.droidMission.id : -1,)
                                                                                          : LoadingIndicator();
                                                                                    });
                                                                              });
                                                                          if (newDroidMissionList != null) {
                                                                            _processNewDroidMissionList(context, newDroidMissionList);
                                                                          }
                                                                        },
                                                                      ),
                                                                      IconButton(
                                                                        icon: Icon(FLOID_CHOOSER_ICON, size: FLOID_CHOOSER_ICON_SIZE, color: Colors.yellowAccent,),
                                                                        onPressed: () async {
                                                                          FloidIdBloc floidIdBloc = BlocProvider.of<FloidIdBloc>(context);
                                                                          final FloidIdState fFloidIdBlocState = floidIdBloc.state;
                                                                          FloidIdAndState? floidIdAndState;
                                                                          if (fFloidIdBlocState is FloidIdLoaded) {
                                                                            FloidIdLoaded floidIdLoaded = fFloidIdBlocState;
                                                                            floidIdAndState = floidIdLoaded.floidIdAndState;
                                                                          }
                                                                          if (fFloidIdBlocState is FloidUuidLoaded) {
                                                                            FloidUuidLoaded floidUuidLoaded = fFloidIdBlocState;
                                                                            floidIdAndState = floidUuidLoaded.floidIdAndState;
                                                                          }
                                                                          FloidIdAndState? newFloidIdAndState = await showDialog<FloidIdAndState>(
                                                                              context: context,
                                                                              builder: (BuildContext context) {
                                                                                return floidIdAndStateListState is FloidIdAndStateListLoaded
                                                                                    ? FloidChooser(floidIdAndStateList: floidIdAndStateListState.floidIdAndStateList,
                                                                                  currentFloidId: floidIdAndState != null ? floidIdAndState.floidId : -1,
                                                                                ) : LoadingIndicator();
                                                                              });
                                                                          if (newFloidIdAndState != null) {
                                                                            _processNewFloidId(context, newFloidIdAndState);
                                                                          }
                                                                        },
                                                                      ),
                                                                      FloidPopupMenu(),
                                                                    ],
                                                                  ),
                                                                  body: Container(
                                                                    // Wrap in Floid Id and Floid Uuid providers:
                                                                    child: Stack(
                                                                      children: <Widget>[
                                                                        // Map:
                                                                        FloidMapWidget(
                                                                            key: FloidKeys.floidMap, floidServerRepository: floidServerRepositoryState.floidServerRepository),
                                                                        // Pin Set:
                                                                        Align(
                                                                          alignment: Alignment.topLeft,
                                                                          child: Column(
                                                                            mainAxisSize: MainAxisSize.min,
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: <Widget>[
                                                                              // Pin Set Controller:
                                                                              Padding(
                                                                                padding: const EdgeInsets.fromLTRB(3, 3, 0, 0),
                                                                                child: PinSetController(),
                                                                              ),
                                                                              // Floid Droid Systems Controller:
                                                                              Padding(
                                                                                padding: const EdgeInsets.fromLTRB(3, 3, 0, 0),
                                                                                child: FloidDroidSystemsController(),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        Align(
                                                                          alignment: Alignment.topRight,
                                                                          child: Column(
                                                                            mainAxisSize: MainAxisSize.min,
                                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                padding: const EdgeInsets.fromLTRB(0, 3, 3, 0),
                                                                                child: MissionController(),
                                                                              ),
                                                                              Padding(
                                                                                padding: const EdgeInsets.fromLTRB(0, 3, 3, 0),
                                                                                child: FloidSystemsController(),
                                                                              ),
                                                                              Padding(
                                                                                padding: const EdgeInsets.fromLTRB(0, 3, 3, 0),
                                                                                child: MissionInstructionStatusController(),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        // Photo / Video:
                                                                        Align(
                                                                          alignment: Alignment.bottomRight,
                                                                          child: Row(
                                                                            mainAxisSize: MainAxisSize.min,
                                                                            children: <Widget>[
                                                                              PhotoVideoController(),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                        // Error List / Execute:
                                                                        Align(
                                                                          alignment: Alignment.bottomCenter,
                                                                          child: Container(
                                                                            padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
                                                                            child: Container(
                                                                              padding: EdgeInsets.fromLTRB(3, 2, 3, 2),
                                                                              decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(6)),
                                                                                color: PANEL_BACKGROUND_COLOR,
                                                                              ),
                                                                              child: Column(
                                                                                mainAxisSize: MainAxisSize.min,
                                                                                children: [
                                                                                  if((floidServerRepositoryState is FloidServerRepositoryStateHasInsecureHost) ||
                                                                                      floidServerRepositoryState.floidServerRepository.floidServerApiClient.inSecure) Text("INSECURE"),
                                                                                  Row(
                                                                                    mainAxisSize: MainAxisSize.min,
                                                                                    children: <Widget>[
                                                                                      ServerStatusController(),
//                                                              FloidHeartbeatStatusController(),
                                                                                      ErrorListController(),
                                                                                      CommandExecuteController(),
                                                                                      MissionExecuteController(),
                                                                                    ],
                                                                                  ),
                                                                                ],
                                                                              ),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        // Messages, legend and map controls:
                                                                        Align(
                                                                          alignment: Alignment.bottomLeft,
                                                                          child: Column(
                                                                            mainAxisSize: MainAxisSize.min,
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                              Container(
                                                                                child: Padding(
                                                                                  padding: const EdgeInsets.fromLTRB(3, 0, 0, 3),
                                                                                  child: Container(
                                                                                    padding: const EdgeInsets.fromLTRB(2, 2, 2, 2),
                                                                                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(6)),
                                                                                      color: PANEL_BACKGROUND_COLOR,
                                                                                    ),
                                                                                    child: Column(
                                                                                      mainAxisSize: MainAxisSize.min,
                                                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                                                      children: <Widget>[
                                                                                        ZoomController(zoom: ZoomController.ZOOM_IN),
                                                                                        ZoomController(zoom: ZoomController.ZOOM_OUT),
                                                                                        ZoomController(zoom: ZoomController.ZOOM_TO_PINS),
                                                                                        MapTypeController(),
                                                                                        ElevationLocationController(),
                                                                                        LegendController(),
                                                                                        MessagesController(),
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              );
                                                            },
                                                          );
                                                        },
                                                      );
                                                    },
                                                  );
                                                },
                                              );
                                            },
                                          );
                                        },
                                      );
                                    },
                                  );
                                },
                              );
                            },
                          );
                        },
                      );
                    },
                  );
                },
              ),
            ),
          ),
        ),
      )
          : LoadingIndicator();
    });
  }
}
