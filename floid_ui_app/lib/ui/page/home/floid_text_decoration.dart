/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:flutter/material.dart';

class FloidTextDecoration extends StatelessWidget {

  static const Color BACKGROUND_COLOR = Color(0x3F000000);
  static const Color TEXT_COLOR = Colors.yellowAccent;
  static const double TEXT_SIZE = 10;
  static const double SUBTEXT_SIZE = 10;

  final String label;
  final String? subLabel;
  const FloidTextDecoration({
    Key? key,
    required this.label,
    this.subLabel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String? fSubLabel = subLabel;
    return DecoratedBox(
        decoration: BoxDecoration(
          color: BACKGROUND_COLOR,
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 2.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(1, 2, 1, 0),
                child: Text(label, style: TextStyle(fontSize: TEXT_SIZE, color: TEXT_COLOR, fontWeight: FontWeight.w800,),),
              ),
              if(fSubLabel!=null) Padding(
                padding: const EdgeInsets.fromLTRB(1, 2, 1, 0),
                child: Text(fSubLabel, style: TextStyle(fontSize: SUBTEXT_SIZE, color: TEXT_COLOR)),
              ),
            ],
          ),
        )
    );
  }
}
