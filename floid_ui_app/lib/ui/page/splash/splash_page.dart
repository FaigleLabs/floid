/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          body: Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 56, 0, 0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text('FLOID', style: TextStyle(fontSize: 32, fontStyle: FontStyle.normal),),
                  Text('FaigleLabs', style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic),),
                ],
              ),
            ),
        ),
    );
  }
}
