/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/ui/map/marker/floid_pin_attributes.dart';

class Gpx {
  final List<String> waypoints = [];
  final List<String> trackPoints = [];
  final List<String> routes = [];
  final String gpxStartTemplate = '<?xml version="1.0" encoding="UTF-8"?>\n'
      '<gpx version="1.1" creator="Faiglelabs"\n'
      '  xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"\n'
      '  xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">\n'
      '  <metadata>\n'
      '    <link href="www.faiglelabs.com">\n'
      '       <text>Faiglelabs</text>\n'
      '    </link>\n'
      '  </metadata>';
  final String gpxEndTemplate = '</gpx>';
  final String mapWaypointTemplate = '  <wpt lat="#LATITUDE#" lon="#LONGITUDE#">\n'
      '    <ele>#ELEVATION#</ele>\n'
      '    <name>#NAME#</name>\n'
      '    <sym>#SYMBOL#</sym>\n'
      '  </wpt>';

  final String trackStartTemplate = '  <trk>\n '
      '    <name>#NAME#</name>\n'
      '    <trkseg>\n';
  final String trackPointTemplate = '      <trkpt lat="#LATITUDE#" lon="#LONGITUDE#">\n'
      '      <ele>#ELEVATION#</ele>\n'
      '    </trkpt>';
  final String trackEndTemplate = '    </trkseg>\n  </trk>';
  final String rteStartTemplate = '  <rte>\n<name>#NAME#</name>\n';
  final String rteEndTemplate = '  </rte>';
  final String routePointTemplate = '    <rtept lat="#LATITUDE#" lon="#LONGITUDE#">\n'
      '      <ele>#ELEVATION#</ele>\n'
      '      <name>#NAME#</name>\n'
      '    </rtept>\n';
  static const String LATITUDE_REPLACEMENT_STRING = '#LATITUDE#';
  static const String LONGITUDE_REPLACEMENT_STRING = '#LONGITUDE#';
  static const String NAME_REPLACEMENT_STRING = '#NAME#';
  static const String ELEVATION_REPLACEMENT_STRING = '#ELEVATION#';
  static const String SYMBOL_REPLACEMENT_STRING = "#SYMBOL#";
  static const String PAYLOAD_PIN_SYMBOL = "https://maps.google.com/mapfiles/kml/paddle/ylw-diamond.png";
  static const String HOME_PIN_SYMBOL = "https://maps.google.com/mapfiles/kml/paddle/go.png";
  static const String FLY_TO_PIN_SYMBOL = "https://maps.google.com/mapfiles/kml/paddle/purple-blank.png";
  static const String STANDARD_PIN_SYMBOL = "https://maps.google.com/mapfiles/kml/paddle/blu-blank.png";
  static const String FLY_TO_CHARACTER_ICON = '↔';
  static const String LIFT_OFF_CHARACTER_ICON = '⇑';
  static const String LAND_CHARACTER_ICON = '⇓';
  static const String LAND_TARGET_CHARACTER_ICON = '◘';
  static const String HEIGHT_TO_ASCEND_CHARACTER_ICON = '↑';
  static const String HEIGHT_TO_DESCEND_CHARACTER_ICON = '↓';
  static const String PAYLOAD_CHARACTER_ICON = '★';
  static const String PHOTO_CHARACTER_ICON = '◇';
  static const String PHOTO_STROBE_ON_CHARACTER_ICON = '⎐';
  static const String PHOTO_STROBE_OFF_CHARACTER_ICON = '⎑';
  static const String VIDEO_ON_CHARACTER_ICON = '⏺';
  static const String VIDEO_OFF_CHARACTER_ICON = '⏹';
  static const String SPEAKER_CHARACTER_ICON = '⊖';
  static const String SPEAKER_ON_CHARACTER_ICON = '⊛';
  static const String SPEAKER_OFF_CHARACTER_ICON = '⊘';

  final String name;
  final String timestamp; // TODO - IMPLEMENT TIMESTAMP - USE FOR FLOID AND DROID LINES
  // Constructor requires name:
  Gpx({required this.name, required this.timestamp} );

  void addRoute({required double latitude, required double longitude, required double elevation, required String name}) {
    routes.add(routePointTemplate.replaceAll(LATITUDE_REPLACEMENT_STRING, latitude.toString())
        .replaceAll(LONGITUDE_REPLACEMENT_STRING, longitude.toString())
        .replaceAll(ELEVATION_REPLACEMENT_STRING, elevation.toString())
        .replaceAll(NAME_REPLACEMENT_STRING, name));
  }

  void addWaypoint({required double latitude, required double longitude, required double elevation, required String name, required FloidPinAttributes floidPinAttributes,}) {
    String symbol;
    if(floidPinAttributes.isHome) {
      symbol = HOME_PIN_SYMBOL;
    } else if(floidPinAttributes.isPayloadDropped) {
      symbol = PAYLOAD_PIN_SYMBOL;
    } else if(floidPinAttributes.isFlyTo) {
      symbol = FLY_TO_PIN_SYMBOL;
    } else {
      symbol = STANDARD_PIN_SYMBOL;
    }
    waypoints.add(
        mapWaypointTemplate.replaceAll(LATITUDE_REPLACEMENT_STRING, latitude.toString())
            .replaceAll(LONGITUDE_REPLACEMENT_STRING, longitude.toString())
            .replaceAll(ELEVATION_REPLACEMENT_STRING, elevation.toString())
            .replaceAll(NAME_REPLACEMENT_STRING, name)
            .replaceAll(SYMBOL_REPLACEMENT_STRING, symbol)
    );
  }

  void addTrackPoint({required double latitude, required double longitude, required double elevation}) {
  trackPoints.add(
        trackPointTemplate.replaceAll(LATITUDE_REPLACEMENT_STRING, latitude.toString())
            .replaceAll(LONGITUDE_REPLACEMENT_STRING, longitude.toString())
            .replaceAll(ELEVATION_REPLACEMENT_STRING, elevation.toString())
    );
  }

  String render() {
    return
      gpxStartTemplate
        + (waypoints.isNotEmpty? '\n' + waypoints.join('\n'):'')
        + (routes.isNotEmpty? '\n' + rteStartTemplate.replaceAll(NAME_REPLACEMENT_STRING, name) + '\n' + routes.join('\n') + '\n' + rteEndTemplate:'')
        + (trackPoints.isNotEmpty? '\n' + trackStartTemplate.replaceAll(NAME_REPLACEMENT_STRING, name) + '\n' + trackPoints.join('\n') + '\n' + trackEndTemplate:'')
        + '\n' + gpxEndTemplate + '\n';
  }
}

class GpxMarkersNeeded {
  bool payload = false;
  bool photo = false;
  bool photoStrobeOn = false;
  bool photoStrobeOff = false;
  bool videoOn = false;
  bool videoOff = false;
  bool speaker = false;
  bool speakerOn = false;
  bool speakerOff = false;

  String renderAndClear() {
    String markers = '';
    if(payload) {
      markers += Gpx.PAYLOAD_CHARACTER_ICON;
      payload = false;
    }
    if(photo) {
      markers += Gpx.PHOTO_CHARACTER_ICON;
      photo = false;
    }
    if(photoStrobeOn) {
      markers += Gpx.PHOTO_STROBE_ON_CHARACTER_ICON;
      photoStrobeOn = false;
    }
    if(photoStrobeOff) {
      markers += Gpx.PHOTO_STROBE_OFF_CHARACTER_ICON;
      photoStrobeOff = false;
    }
    if(videoOn) {
      markers += Gpx.VIDEO_ON_CHARACTER_ICON;
      videoOn = false;
    }
    if(videoOff) {
      markers += Gpx.VIDEO_OFF_CHARACTER_ICON;
      videoOff = false;
    }
    if(speaker) {
      markers += Gpx.SPEAKER_CHARACTER_ICON;
      speaker = false;
    }
    if(speakerOn) {
      markers += Gpx.SPEAKER_ON_CHARACTER_ICON;
      speakerOn = false;
    }
    if(speakerOff) {
      markers += Gpx.SPEAKER_OFF_CHARACTER_ICON;
      speakerOff = false;
    }
    return markers;
  }
}
