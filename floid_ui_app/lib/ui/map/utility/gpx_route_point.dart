/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

class GpxRoutePoint {
  final double lat;
  final double lng;
  final double elevation;
  final String name;

  GpxRoutePoint({required this.lat, required this.lng, required this.elevation, required this.name, });
}
