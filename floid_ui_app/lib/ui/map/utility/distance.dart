/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:math';

class Distance {

  /// Haversine lat/lng distance calculation:
  static double distanceHaversine(double startLat, double startLng, double endLat, double endLng) {
    int R = 6371; // earth's mean radius in km
    double dLat = _rad(endLat - startLat);
    double dLong = _rad(endLng - startLng);
    double a = sin(dLat / 2) * sin(dLat / 2) +
        cos(_rad(startLat)) * cos(_rad(endLat)) * sin(dLong / 2) * sin(dLong / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double d = R * c;
    return d * 1000; // km to m
  }

  /// Convert to radians:
  static double _rad(double x) {
    return x * pi / 180;
  }

  /// Keep angle -180 to 180:
  static double keepInDegreeRange(double degrees) {
    while (degrees <= -180.0) {
      degrees += 360.0;
    }
    while (degrees > 180.0) {
      degrees -= 360.0;
    }
    return degrees;
  }
  // Std trig angles to compass degrees:
  static double degreeToCompass(double degree) {
    return keepInDegreeRange(90-degree);
  }
}