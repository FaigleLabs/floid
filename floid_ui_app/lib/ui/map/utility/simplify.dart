/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/map/line/line.dart';
import 'package:floid_ui_app/ui/map/utility/distance.dart';

class Simplify {
  /// Square distance between 2 points
  static double _getSquareDistance(FloidGpsPoint p1, FloidGpsPoint p2) {
    double dLongitude = p1.longitude - p2.longitude;
    double dLatitude = p1.latitude - p2.latitude;
    return dLatitude * dLatitude + dLongitude * dLongitude;
  }

  /// Square distance from a point to a segment
  static double _getSquareSegmentDistance(FloidGpsPoint p, FloidGpsPoint p1, FloidGpsPoint p2) {
    double longitude = p1.longitude;
    double latitude = p1.latitude;
    double dLongitude = p2.longitude - longitude;
    double dLatitude = p2.latitude - latitude;
    double t = 0;
    if (dLongitude != 0 || dLatitude != 0) {
      t = ((p.longitude - longitude) * dLongitude + (p.latitude - latitude) * dLatitude) / (dLongitude * dLongitude + dLatitude * dLatitude);
      if (t > 1) {
        longitude = p2.longitude;
        latitude = p2.latitude;
      } else if (t > 0) {
        longitude += dLongitude * t;
        latitude += dLatitude * t;
      }
    }
    dLongitude = p.longitude - longitude;
    dLatitude = p.latitude - latitude;
    return dLongitude * dLongitude + dLatitude * dLatitude;
  }

  /// Distance-based simplification
  static List<FloidGpsPoint> _simplifyRadialDistance(List<FloidGpsPoint> points, double sqTolerance) {
    int i = 0;
    int len = points.length;
    if(len==0) return <FloidGpsPoint>[];
    FloidGpsPoint? point;
    FloidGpsPoint prevFloidPoint = points[0];
    List<FloidGpsPoint> newFloidPoints = <FloidGpsPoint>[]
      ..add(prevFloidPoint);

    for (i = 1; i < len; i++) {
      point = points[i];
      if (_getSquareDistance(point, prevFloidPoint) > sqTolerance) {
        newFloidPoints.add(point);
        prevFloidPoint = point;
      }
    }
    if (prevFloidPoint != point) {
      if(point!=null) newFloidPoints.add(point);
    }
    return newFloidPoints;
  }

  /// Simplification using optimized Douglas-Peucker algorithm with recursion elimination
  static List<FloidGpsPoint> _simplifyDouglasPeucker(List<FloidGpsPoint> points, double sqTolerance) {
    int len = points.length;

    List<int> markers = List<int>.filled(len, 0, growable: true); // Create a list of the length filled with zeroes

    int first = 0;
    int last = len - 1;
    double maxSqDist = 0;
    double sqDist = 0;
    int index = 0;
    List<int> firstStack = [];
    List<int> lastStack = [];
    List<FloidGpsPoint> newFloidPoints = [];
    markers[first] = markers[last] = 1; // We always keep the first and last point - mark them as kept
    while (last > 0) {
      maxSqDist = 0;
      for (int i = first + 1; i < last; i++) {
        sqDist = _getSquareSegmentDistance(points[i], points[first], points[last]);
        if (sqDist > maxSqDist) {
          index = i;
          maxSqDist = sqDist;
        }
      }
      if (maxSqDist > sqTolerance) {
        markers[index] = 1;
        firstStack.add(first);
        lastStack.add(index);
        firstStack.add(index);
        lastStack.add(last);
      }
      first = firstStack.removeLast();
      last = lastStack.removeLast();
    }
    for (int i = 0; i < len; i++) {
      if (markers[i] == 1) {
        newFloidPoints.add(points[i]);
      }
    }
    return newFloidPoints;
  }

  /// Simplify a list of points to the given tolerance either radial or Douglas-Peucker
  static List<FloidGpsPoint> simplify(List<FloidGpsPoint> points, double tolerance /* 1.0 */, bool highestQuality /*false*/) {
    double sqTolerance = tolerance * tolerance;
    if (!highestQuality) {
      points = _simplifyRadialDistance(points, sqTolerance);
    }
    points = _simplifyDouglasPeucker(points, sqTolerance);
    return points;
  }

  static List<FloidGpsPoint> _simplifyGpsPointList(List<FloidGpsPoint> floidPoints, FloidLineConfig floidLineConfig) {
    // From: https://github.com/fnicollet/simplify-as3
    List<FloidGpsPoint> simplifiedFloidPoints = Simplify.simplify(floidPoints, floidLineConfig.kink, floidLineConfig.highQuality);
    // If we are too large, update the kink with the alpha so next time we compress more...
    // Repeat simplification until we get under out list size:
    if (simplifiedFloidPoints.length > floidLineConfig.expandPoints) {
      floidLineConfig.kink *= (1 + floidLineConfig.kinkAlpha); // Increase distance we tolerate to meet our criteria
    }
    return simplifiedFloidPoints;
  }

  /// Add a Floid GPS point to a point list that meets the floid line kink and other restrictions:
  static List<FloidGpsPoint> addFloidGpsPoint(FloidGpsPoint floidGpsPoint, List<FloidGpsPoint> points, FloidLineConfig floidLineConfig) {
    bool keep = true;
    // First point?
    if (points.length==0) {
      // Starting point
    } else {
      // Passes min time?
      keep = _filterGpsPointTime(floidGpsPoint, points.last, floidLineConfig.minTime);
      if (keep) {
        // Passes min distance?
        keep = _filterGpsPointDistance(floidGpsPoint, points.last, floidLineConfig.minDistance);
      }
    }
    // Pass time and distance test:
    if (keep) {
      points.add(floidGpsPoint);
      if (points.length > floidLineConfig.expandPoints) {
        points = _simplifyGpsPointList(points, floidLineConfig);
      }
    }
    return points;
  }

  static bool _filterGpsPointTime(FloidGpsPoint floidGpsPoint, FloidGpsPoint currentTestPoint, int minTime) {
  return !((minTime > 0) && (floidGpsPoint.statusTime - currentTestPoint.statusTime < minTime));
  }

  static bool _filterGpsPointDistance(FloidGpsPoint floidGpsPoint, FloidGpsPoint currentTestPoint, double minDistance) {
  return !((minDistance > 0) && (Distance.distanceHaversine(currentTestPoint.latitude, currentTestPoint.longitude,
  floidGpsPoint.latitude, floidGpsPoint.longitude) < minDistance));
  }

}
