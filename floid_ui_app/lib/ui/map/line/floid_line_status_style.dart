/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:core';

class FloidLineStatusStyle {
  double a = 0.0;
  double r = 0.0;
  double g = 0.0;
  double b = 0.0;
  double w = 0.0;
  FloidLineStatusStyle({required this.a,
    required this.r,
    required this.g,
    required this.b,
    required this.w})
      : assert(a!=null), assert(r!=null), assert(g!=null), assert(b!=null), assert(w!=null);

}
