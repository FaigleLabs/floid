/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:core';

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_path_elevation.dart';

class FloidMapLine {
  static const int MAP_LINE_TYPE_UNKNOWN = -1;
  static const int MAP_LINE_TYPE_FLY = 0;
  static const int MAP_LINE_TYPE_DESIGNATE = 1;
  static const int MAP_LINE_TYPE_GPS = 2;

  static const int ELEVATION_STATUS_FAIL = -2;
  static const int ELEVATION_STATUS_UNKNOWN = -1;
  static const int ELEVATION_STATUS_GOOD = 1;

  static const int ALTITUDE_STATUS_UNKNOWN = -1;
  static const int ALTITUDE_STATUS_GOOD = 1;
  static const int ALTITUDE_STATUS_WARNING = 2;
  static const int ALTITUDE_STATUS_ERROR = 3;

  /// The monotonically increasing line id:
  static int nextLineId = 1;
  // Line information:
  int? lineId;
  int? type;
  int? elevationStatus;
  int? altitudeStatus;

  FloidPinAlt? startPin;
  FloidPinAlt? endPin;
  double startLat=0.0;
  double startLng=0.0;
  double endLat=0.0;
  double endLng=0.0;
  FloidPathElevation? floidPathElevation;
  FloidMapLine({this.lineId = -1,
    this.type=MAP_LINE_TYPE_UNKNOWN,
    this.elevationStatus = ELEVATION_STATUS_UNKNOWN,
    this.altitudeStatus = ALTITUDE_STATUS_UNKNOWN,
    this.startPin,
    this.endPin,
    this.startLat = 0.0,
    this.startLng = 0.0,
    this.endLat = 0.0,
    this.endLng=0.0,
    this.floidPathElevation}) {
    elevationStatus??=ELEVATION_STATUS_UNKNOWN;
    altitudeStatus??=ALTITUDE_STATUS_UNKNOWN;
    type??=MAP_LINE_TYPE_UNKNOWN;
  }
}
