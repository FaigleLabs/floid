/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:core';

import 'package:floid_ui_app/ui/map/line/floid_map_line.dart';

class FloidMapDesignateLine extends FloidMapLine {
  FloidMapDesignateLine({lineId, type, elevationStatus, altitudeStatus, startPin, endPin, startLat, startLng, endLat, endLng, floidPathElevation})
      :super(lineId: lineId, type: type, altitudeStatus: altitudeStatus,  elevationStatus: elevationStatus, startPin: startPin,
      endPin:endPin, startLat: startLat, startLng: startLng, endLat: endLat, endLng: endLng,
      floidPathElevation:floidPathElevation);
}
