/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:core';

class FloidLineConfig {
  double kink = 10.0;
  double kinkAlpha = 0.1;
  double shrinkAlpha = 0.75; // We get max 200 points from cache to start
  double expandAlpha = 1.5; // We get max 200 points from cache to start
  int maxPointsStart = 200; // We get max 200 points from cache to start by default
  int minTime = 5000; // Min time between points in milliseconds (5000 - 5 seconds)
  int expandPoints = 100;
  int shrinkPoints = 200;
  bool highQuality = true;
  double minDistance = 2; // Min distance between - 2 meters

  FloidLineConfig({required this.kink,
    required this.kinkAlpha,
    required this.shrinkAlpha,
    required this.expandAlpha,
    required this.maxPointsStart,
    required this.minTime,
    required this.minDistance,
    required this.highQuality,
  })
      : assert(kink != null),
        assert(kinkAlpha != null),
        assert(shrinkAlpha != null),
        assert(expandAlpha != null),
        assert(maxPointsStart != null),
        assert(minTime != null),
        assert(minDistance != null),
        assert(highQuality != null) {
    expandPoints = (maxPointsStart * expandAlpha).floor();
    shrinkPoints = (maxPointsStart * shrinkAlpha).floor();
  }

}
