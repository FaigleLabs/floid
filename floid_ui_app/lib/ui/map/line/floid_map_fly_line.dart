/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:core';

import 'package:floid_ui_app/ui/map/line/floid_map_line.dart';
import 'package:floid_ui_app/ui/map/marker/floid_fly_line_path_elevation_marker.dart';

class FloidMapFlyLine extends FloidMapLine {
  double? height = FloidFlyLinePathElevationMarker.PATH_ELEVATION_MIN_VALUE;
  FloidFlyLinePathElevationMarker? floidFlyLinePathElevationMarker;

  FloidMapFlyLine({lineId, type, elevationStatus, altitudeStatus, startPin, endPin, startLat=0.0, startLng=0.0, endLat=0.0, endLng=0.0, floidPathElevation, this.floidFlyLinePathElevationMarker, this.height})
      :super(lineId: lineId, type: type, altitudeStatus: altitudeStatus,  elevationStatus: elevationStatus, startPin: startPin,
      endPin: endPin, startLat: startLat, startLng: startLng, endLat: endLat, endLng: endLng,
      floidPathElevation: floidPathElevation) {
    height??=FloidFlyLinePathElevationMarker.PATH_ELEVATION_MIN_VALUE;
  }
}
