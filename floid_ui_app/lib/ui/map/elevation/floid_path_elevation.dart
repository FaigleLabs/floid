/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:core';

import 'package:floid_ui_app/ui/map/line/floid_map_fly_line.dart';

class FloidPathElevation {

  static const int RESULT_UNKNOWN = -1;
  static const int RESULT_SUCCESS = 0;
  static const int RESULT_FAIL = -2;

  // Path elevation information:
  double pathElevationStartLat = 0;
  double pathElevationStartLng = 0;
  double pathElevationEndLat = 0;
  double pathElevationEndLng = 0;
  int pathElevationMaxNumberSamples = 0;
  int pathElevationActualNumberSamples = 0;
  double pathElevationRequestedSegmentLength = 0;
  double pathElevationActualSegmentLength = 0;
  double pathElevationMax = 0;
  bool pathElevationValid = false;
  int pathElevationResult = RESULT_UNKNOWN;
  String toolTip = ''; // Note: This could be implemented
  FloidMapFlyLine? floidMapFlyLine = FloidMapFlyLine();
  FloidPathElevation({this.floidMapFlyLine, this.pathElevationStartLat = 0, this.pathElevationStartLng  = 0,
    this.pathElevationEndLat = 0, this.pathElevationEndLng = 0,  this.pathElevationMaxNumberSamples =0, this.pathElevationActualNumberSamples =  0,
     this.pathElevationRequestedSegmentLength =0,  this.pathElevationActualSegmentLength = 0,
     this.pathElevationMax=0,  this.pathElevationValid=false,  this.pathElevationResult = RESULT_UNKNOWN,
     this.toolTip=''}) {
    this.floidMapFlyLine??=FloidMapFlyLine();
  }
}
