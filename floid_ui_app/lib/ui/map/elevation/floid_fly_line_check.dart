/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:core';

class FloidFlyLineCheck {
  static const String STATUS_GOOD = "GOOD";
  static const String STATUS_TIMEOUT = "TIMEOUT";
  static const String STATUS_REQUEST_ERROR = "REQUEST_ERROR";
  static const String STATUS_HEIGHT_WARNING = "HEIGHT_WARNING";
  static const String STATUS_HEIGHT_ERROR = "HEIGHT_ERROR";
  int id = 0;
  int totalNumberOfFlyLines = 0;
  int numberOfRemainingPathRequests = 0;
  double minPathHeightFromWarning = 100000;
  double minPathHeightFromError = 100000;
  String status = STATUS_GOOD;

  FloidFlyLineCheck({required this.id, required this.totalNumberOfFlyLines, required this.numberOfRemainingPathRequests,
    required this.minPathHeightFromWarning, required this.minPathHeightFromError, required this.status});
}
