/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/map/line/floid_map_fly_line.dart';
import 'package:floid_ui_app/ui/map/marker/floid_fly_line_path_elevation_marker.dart';

class FloidPathElevationRequest {
  static const int RESULT_UNKNOWN = -1;
  static const int RESULT_FAIL = 0;
  static const int RESULT_SUCCESS = -1;
  bool valid = false;
  int result = RESULT_UNKNOWN;
  double errorHeight = FloidFlyLinePathElevationMarker.PATH_ELEVATION_MIN_VALUE;
  double warningHeight = FloidFlyLinePathElevationMarker.PATH_ELEVATION_MIN_VALUE;
  FloidMapFlyLine floidMapFlyLine;
  int flyLineCheckId;
  int pathCheckId;

  FloidPathElevationRequest({required this.floidMapFlyLine, required this.flyLineCheckId, required this.pathCheckId})
      : result=RESULT_UNKNOWN,
        valid=false,
        errorHeight = FloidFlyLinePathElevationMarker.PATH_ELEVATION_MIN_VALUE,
        warningHeight = FloidFlyLinePathElevationMarker.PATH_ELEVATION_MIN_VALUE;
}
