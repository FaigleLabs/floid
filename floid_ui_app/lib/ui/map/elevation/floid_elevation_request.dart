/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:core';

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_path_elevation.dart';
import 'package:floid_ui_app/ui/map/marker/floid_map_marker.dart';

class FloidElevationRequest {
  static const String TYPE_UNKNOWN = "UNKNOWN";
  static const String TYPE_PIN = "PIN";

  String type = TYPE_UNKNOWN;
  int elevationRequestId = -1;
  FloidPinAlt floidPinAlt;
  int markerId = -1;
  FloidMapMarker? floidMapMarker;
  int result = FloidPathElevation.RESULT_UNKNOWN;

  FloidElevationRequest({required this.type, required this.elevationRequestId, required this.floidPinAlt, required this.markerId, required this.floidMapMarker})
      : result = FloidPathElevation.RESULT_UNKNOWN;
}
