/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/map/marker/floid_map_marker.dart';

class FloidTargetMarker extends FloidMapMarker {

  FloidStatus floidStatus;

  FloidTargetMarker({required this.floidStatus}) {
    markerLat = floidStatus.gx;
    markerLng = floidStatus.gy;
    markerElevation = floidStatus.gz;
    markerId = FloidMapMarker.nextMarkerId++;
  }
}

