/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/map/marker/marker.dart';

class FloidAcquiredHomeMarker extends FloidMapMarker {

  FloidDroidStatus floidDroidStatus;

  FloidAcquiredHomeMarker({required this.floidDroidStatus}) {
    if (floidDroidStatus.homePositionAcquired) {
      markerLat = floidDroidStatus.homePositionY;
      markerLng = floidDroidStatus.homePositionX;
      markerElevation = floidDroidStatus.homePositionZ;
      markerId = FloidMapMarker.nextMarkerId++;
    }
  }
}
