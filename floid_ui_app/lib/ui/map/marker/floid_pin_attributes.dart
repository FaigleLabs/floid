/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

class FloidPinAttributes {
  FloidPinAttributes({
    this.isHome = false,
    this.isFlyTo = false,
    this.isPayloadDropped = false,
    this.isPayloadBay0 = false,
    this.isPayloadBay1 = false,
    this.isPayloadBay2 = false,
    this.isPayloadBay3 = false,
    this.isVideoOn = false,
    this.isVideoOff = false,
    this.isSpeaker = false,
    this.isSpeakerOn = false,
    this.isSpeakerOff = false,
    this.isPhoto = false,
    this.isHover = false,
    this.isLiftOff = false,
    this.isLand = false,
    this.isAscend = false,
    this.isDescend = false,
    this.isDesignator = false,
    this.isDesignated = false,
    this.isPhotoStrobeOn = false,
    this.isPhotoStrobeOff = false
  });
  bool isHome = false;
  bool isFlyTo = false;
  bool isPayloadDropped = false;
  bool isPayloadBay0 = false;
  bool isPayloadBay1 = false;
  bool isPayloadBay2 = false;
  bool isPayloadBay3 = false;
  bool isVideoOn = false;
  bool isVideoOff = false;
  bool isSpeaker = false;
  bool isSpeakerOn = false;
  bool isSpeakerOff = false;
  bool isPhoto = false;
  bool isHover = false;
  bool isLiftOff = false;
  bool isLand = false;
  bool isAscend = false;
  bool isDescend = false;
  bool isDesignator = false;
  bool isDesignated = false;
  bool isPhotoStrobeOn = false;
  bool isPhotoStrobeOff = false;
}