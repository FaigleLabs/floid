/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:core';

class FloidMapMarker {
  /// The monotonically increasing marker id:
  static int nextMarkerId = 1;
  int markerId = -1;
  int markerIndex = -1;
  double markerLat = 0;
  double markerLng = 0;
  double? markerElevation = 0;
  String markerName = '';
  FloidMapMarker({this.markerId = -1, this.markerIndex = -1, this.markerLat=0, this.markerLng=0, this.markerElevation=0, this.markerName=''});
}
