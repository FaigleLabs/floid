/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/map/marker/floid_map_marker.dart';

class FloidPinMarker extends FloidMapMarker {

  FloidPinAlt floidPinAlt;

  FloidPinMarker({required this.floidPinAlt}) : super(markerLat: floidPinAlt.pinLat,
    markerLng: floidPinAlt.pinLng,
    markerElevation: floidPinAlt.pinHeight,
    markerName: floidPinAlt.pinName,
    markerId: FloidMapMarker.nextMarkerId++);
  int id = -1;
}
