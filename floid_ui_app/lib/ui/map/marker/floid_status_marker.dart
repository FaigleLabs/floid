/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/map/marker/floid_map_marker.dart';

class FloidStatusMarker extends FloidMapMarker {

  FloidStatus floidStatus;

  FloidStatusMarker({required this.floidStatus}) {
    markerLat = floidStatus.jyf;
    markerLng = floidStatus.jxf;
    markerElevation = floidStatus.ka;
    markerId = FloidMapMarker.nextMarkerId++;
  }
}

