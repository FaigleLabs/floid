/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/models/floid_pin_alt.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_path_elevation.dart';
import 'package:floid_ui_app/ui/map/marker/floid_map_marker.dart';

class FloidFlyLinePathElevationMarker extends FloidMapMarker {
  static const double PATH_ELEVATION_MIN_VALUE = -100000;

  FloidPinAlt startPin;
  FloidPinAlt endPin;
  FloidPathElevation floidPathElevation;
  double flightHeight = PATH_ELEVATION_MIN_VALUE;
  double errorOffset = PATH_ELEVATION_MIN_VALUE;
  double warningOffset = PATH_ELEVATION_MIN_VALUE;
  double flightDistanceInMeters = 0;


  FloidFlyLinePathElevationMarker({markerLat,  markerLng, markerElevation, required this.startPin, required this.endPin, required this.flightHeight, required this.errorOffset, required this.warningOffset, required this.floidPathElevation, required this.flightDistanceInMeters}) :
        super(markerLat: markerLat, markerLng: markerLng, markerElevation: markerElevation);

  String distanceToStringWithUnits() {
    if(flightDistanceInMeters<1000) {
      return (flightDistanceInMeters.floor().toString() + ' m');
    }
    return (((flightDistanceInMeters/100).floor()/10).toString() + ' km');
  }
}
