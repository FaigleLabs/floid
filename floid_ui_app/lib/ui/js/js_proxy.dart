/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/trust/floid_server_trust.dart';

import 'js_stub.dart'
if (dart.library.js) 'web_js_proxy.dart'
if (dart.library.io) 'app_js_proxy.dart';


abstract class JSProxy {
  void saveData(String data, String name);
  void downloadFile(FloidServerTrust floidServerTrust, int id, String name, String fileName, String jwtToken);
  void importMission(void callback(Object data, String name));
  void importPinSet(void callback(Object data, String name));
  /// factory constructor to return the correct implementation.
  factory JSProxy() => getJSProxy();
}
