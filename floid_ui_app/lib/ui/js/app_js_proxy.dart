/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:io';
import 'package:dio/adapter.dart';
import 'package:floid_ui_app/floidserver/trust/floid_server_trust.dart';
import 'package:floid_ui_app/ui/js/js_proxy.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';

class AppJSProxy implements JSProxy {
  @override
  void saveData(String data, String name) {
    // Do nothing
  }

  late String _localPath;

  Future<void> _prepareSaveDir() async {
    try {
      Directory saveDir;
      Directory? externalStorageDirectory = await getExternalStorageDirectory();
      Directory applicationDocumentsDirectory = await getApplicationDocumentsDirectory();
      print('externalStorageDirectory: ${externalStorageDirectory != null ? externalStorageDirectory.path : ""}');
      print('applicationDocumentsDirectory: ${applicationDocumentsDirectory.path}');
      saveDir = applicationDocumentsDirectory;
      if (externalStorageDirectory != null) {
        saveDir = externalStorageDirectory;
      }
      _localPath = saveDir.path;
      print('Local Path: $_localPath');
      bool hasExisted = await saveDir.exists();
      if (!hasExisted) {
        saveDir.create();
      }
    } catch (e) {
      print("Exception in _prepareSaveDir $e");
    }
  }

  @override
  void downloadFile(FloidServerTrust floidServerTrust, int id, String name, String fileName, String jwtToken) async {
    try {
      await _prepareSaveDir();
      print('Downloading $name?token=$jwtToken to $_localPath/$fileName');
      var _dio = Dio();
      //check bad certificate
      (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate = (HttpClient client) {
        client.badCertificateCallback = floidServerTrust.badCertificateCallback;
        return null;
      };
      await _dio.download('$name?token=$jwtToken', '$_localPath/$fileName');
      print('Download Completed. Opening: $_localPath/$fileName');
      final OpenResult result = await OpenFilex.open('$_localPath/$fileName', type: 'application/vnd.android.package-archive');
      print('Result of open: ${result.message} ${result.type.name}');
    } catch (e) {
      Fluttertoast.showToast(
        msg: "Download Failed",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 3,
        backgroundColor: Colors.black87,
        textColor: Colors.yellowAccent,
        fontSize: 12.0,
        webBgColor: "#000000",
      );
      print('Download Failed.\n\n' + e.toString());
    }
  }

  @override
  void importMission(void callback(Object data, String name)) {
    // Do nothing
  }

  @override
  void importPinSet(void callback(Object data, String name)) {
    // Do nothing
  }
}

JSProxy getJSProxy() => AppJSProxy();