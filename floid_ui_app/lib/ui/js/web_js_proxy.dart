/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/trust/floid_server_trust.dart';
import 'package:floid_ui_app/ui/js/js_proxy.dart';
// ignore: avoid_web_libraries_in_flutter
import 'dart:js' as js;
// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;

class WebJSProxy implements JSProxy {
  @override
  void saveData(String data, String name) {
    js.context.callMethod("webSaveAs", [html.Blob([data], "text/plain;charset=utf-8"), name]);
  }

  @override
  void downloadFile(FloidServerTrust floidServerTrust, int id,  String name, String fileName, String jwtToken) async {
    print('Downloading: $name?token=$jwtToken to $fileName');
    html.AnchorElement(
        href: '$name?token=$jwtToken')
      ..setAttribute("download", fileName)
      ..click();
  }

  @override
  void importMission(void callback(Object data, String name)) {
    html.FileUploadInputElement uploadInput = html.FileUploadInputElement();
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      // read file content as dataURL
      final List<html.File>? files = uploadInput.files;
      if (files!=null && files.length == 1) {
        final file = files[0];
        final reader = new html.FileReader();
        reader.onLoadEnd.listen((e) {
          final Object? fResult = reader.result;
          if(fResult!=null) {
            print('Loaded file: ' + file.name);
            print('Reader result:' + fResult.toString());
            callback(fResult, file.name);
          }
        });
        reader.readAsDataUrl(file);
      }
    });
  }


  @override
  void importPinSet(void callback(Object data, String name)) {
    html.FileUploadInputElement uploadInput = html.FileUploadInputElement();
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      // read file content as dataURL
      final files = uploadInput.files;
      if (files!=null && files.length == 1) {
        final file = files[0];
        final reader = new html.FileReader();

        reader.onLoadEnd.listen((e) {
          final Object? fResult = reader.result;
          if(fResult!=null) {
            print('Loaded file: ' + file.name);
            print('Reader result:' + fResult.toString());
            callback(fResult, file.name);
          }
        });
        reader.readAsDataUrl(file);
      }
    });
  }
}

JSProxy getJSProxy() => WebJSProxy();
