/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/debug/floid_debug_tokens.dart';

/// Floid debug token filter.

class FloidDebugTokenFilter {
  String _debugTokenBufferString = '';
  String _outputString =  '';

  FloidDebugTokenFilter();

  /// Reset.
  void reset() {
    _debugTokenBufferString = '';
    _outputString = '';
  }

  /// Filter string.
  String filter(String inputString) {
      for (int i = 0; i < inputString.length; ++i) {
//            System.out.println("Processing at: " + i);
        _processChar(inputString.codeUnitAt(i));
    }
    // We always return the output string but clear it in the object
    String tempOutputString = _outputString;
    _outputString = "";
    return tempOutputString;
  }

  /// Flush string.
  String flush() {
    // Returns the _debugTokenBufferString as-is but also clears it
    String tempDebugTokenBufferString = _debugTokenBufferString;
    _debugTokenBufferString = "";
    return tempDebugTokenBufferString;
  }

  void _processChar(int b) {
    // First add it to the end of our current debugTokenBuffer:
    _debugTokenBufferString = _debugTokenBufferString + String.fromCharCode(b);
//        System.out.println("DebugTokenBufferString length: " + _debugTokenBufferString.length);
    if (_debugTokenBufferString.length == 1) {
      // No tokens so far - if this is not one then just output it
      if (b != '#'.codeUnitAt(0)) {
        _processSavedDebugTokenBuffer();
      }
    } else if (_debugTokenBufferString.length == 2) {
      // We already have one token - need another - otherwise reprocess the saved chars:
      if (b != '#'.codeUnitAt(0)) {
        // Nope - output the top char and re-start processing with the rest...
        _processSavedDebugTokenBuffer();
      }
    } else if (_debugTokenBufferString.length > 2) {
      // We are in the hex digits part - check them:
      // Valid hex char?
      if (!(((b >= '0'.codeUnitAt(0) && b <= '9'.codeUnitAt(0)) || ((b >= 'A'.codeUnitAt(0) && b <= 'F'.codeUnitAt(0)))))) {
        _processSavedDebugTokenBuffer();
      }
    }
    // Do we have a whole valid string?
    if (_debugTokenBufferString.length == 6) {  // Format: ##FFFF
      _outputToken();
    }
  }

  void _processSavedDebugTokenBuffer() {
    // We had a no token failure or not a valid hex digit so output the top char and reprocess the rest...
    _printChar(_debugTokenBufferString.codeUnitAt(0));
    // Save the buffer:
    String savedDebugTokenBufferString = _debugTokenBufferString;
    // Clear the buffer:
    _debugTokenBufferString = "";
    // Start processing from the next one in the saved string:
    for (int i = 1; i < savedDebugTokenBufferString.length; ++i) {
      _processChar(savedDebugTokenBufferString.codeUnitAt(i));
    }
  }

  void _outputToken() {
    // Goal: Take characters 2-5 and
    int token = 0;
    token += _convertHexChar(_debugTokenBufferString.codeUnitAt(2)) * 16 * 16 * 16;
    token += _convertHexChar(_debugTokenBufferString.codeUnitAt(3)) * 16 * 16;
    token += _convertHexChar(_debugTokenBufferString.codeUnitAt(4)) * 16;
    token += _convertHexChar(_debugTokenBufferString.codeUnitAt(5));
    _printToken(token);
    _debugTokenBufferString = "";
  }

  int _convertHexChar(int c) {
    // Valid hex char?
    if (c >= '0'.codeUnitAt(0) && c <= '9'.codeUnitAt(0)) {
      return c - '0'.codeUnitAt(0);
    }
    if (c >= 'A'.codeUnitAt(0) && c <= 'F'.codeUnitAt(0)) {
      return c - 'A'.codeUnitAt(0) + 10;
    } else {
      print("ERROR CONVERTING CHARACTER: " + c.toString());
      return 0;
    }
  }

  void _printChar(int c) {
    _outputString = _outputString + String.fromCharCode(c);
  }

  void _printToken(int token) {
    _outputString = _outputString + FloidDebugTokens.getTokenString(token);
  }
}
