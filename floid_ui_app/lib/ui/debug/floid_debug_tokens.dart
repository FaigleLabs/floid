/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
class FloidDebugTokens {
  static final List<String> tokens = [
    '[RESERVED]',
    //   'RESERVED',                            [0]
    '[SETUP] Success',
    //   'TOKEN_SETUP_SUCCESS',
    '[GOAL] State: UNKNOWN',
    //   'TOKEN_UNKNOWN_GOAL_STATE',
    '[GOAL] Completed: ERROR',
    //   'TOKEN_GOAL_COMPLETED_ERROR',
    '[GOAL] Completed: ERROR - BAD MODE',
    //   'TOKEN_GOAL_COMPLETED_MODE_ERROR',
    '[PHYSICS] Rotation Mode: ALL SAME',
    //   'TOKEN_ALL_SAME_ROTATION_MODE',          [5]
    '[PHYSICS] Rotation Mode: COUNTER',
    //   'TOKEN_COUNTER_ROTATION_MODE',
    '[PHYSICS] Angle to target: NEAR',
    //   'ORIENTATION_OF_TARGET_NEAR',
    '[PHYSICS] Angle to target: FAR',
    //   'ORIENTATION_OF_TARGET_FAR',
    '[PHYSICS] Attack Angle: LIFT OFF OR LAND',
    //   'ATTACK_ANGLE_LIFT_OFF_OR_LAND_MODE',
    '[PHYSICS] Attack Angle: NEAR',
    //   'ATTACK_ANGLE_NEAR',                   [10]
    '[PHYSICS] Attack Angle: SCALE',
    //   'ATTACK_ANGLE_SCALE',
    '[PHYSICS] Attack Angle: FAR',
    //   'ATTACK_ANGLE_FAR',
    '[PHYSICS] Roll: STILL',
    //   'ROLL_STILL',
    '[PHYSICS] Roll: SCALE',
    //   'ROLL_SCALE',
    '[PHYSICS] Roll: FAR',
    //   'ROLL_FAR',                            [15]
    '[PHYSICS] Altitude: STILL',
    //   'ALTITUDE_STILL',
    '[PHYSICS] Altitude: SCALE',
    //   'ALTITUDE_SCALE',
    '[PHYSICS] Altitude: FAR',
    //   'ALTITUDE_FAR',
    '[PHYSICS] Heading: STILL',
    //   'HEADING_STILL',
    '[PHYSICS] Heading: SCALE',
    //   'HEADING_SCALE',                       [20]
    '[PHYSICS] Heading: FAR',
    //   'HEADING_FAR',
    '[PHYSICS} XY: STILL',
    //   'XY_STILL',
    '[PHYSICS] XY: SCALE',
    //   'XY_SCALE',
    '[PHYSICS] XY: FAR',
    //   'XY_FAR',
    '[PHYSICS] Velocity: STILL',
    //   'VELOCITY_STILL',                      [25]
    '[PHYSICS] Velocity: SCALE',
    //   'VELOCITY_SCALE',
    '[PHYSICS] Velocity: FAR',
    //   'VELOCITY_FAR',
    '[FLOID] State: JUST LANDED',
    //   'JUST_LANDED',
    '[FLOID] State: TOTAL SHUTDOWN MODE',
    //   'TOKEN_TOTAL_SHUTDOWN_MODE',
    '[FLOID] Packet type: ',
    //   'LABEL_PACKET_TYPE',                    [30]
    '[ALTIMETER] State: INITIALIZED',
    //   'ALTIMETER_INITIALIZED',
    '[ALTIMETER] State: NOT INITIALIZED',
    //   'ALTIMETER_NOT_INITIALIZED',
    '[AUX] State: ON',
    //   'AUX_ON',
    '[AUX] State: OFF',
    //   'AUX_OFF',
    '[DESIGNATOR] State: ON',
    //   'DESIGNATOR_ON',                       [35]
    '[DESIGNATOR] State: OFF',
    //   'DESIGNATOR_OFF',
    '[ALTIMETER] GPS Good Count: OK',
    //   'ALTIMETER_GPS_COUNT_GOOD',
    '[ALTIMETER] GPS Good Count: LOW',
    //   'ALTIMETER_GPS_COUNT_LOW',
    '[ALTIMETER] PYR Good Count: OK',
    //   'ALTIMETER_PYR_COUNT_GOOD',
    '[ALTIMETER] PYR Good Count: LOW',
    //   'ALTIMETER_PYR_COUNT_LOW',             [40]
    '[ALTIMETER] State: INITIALIZING',
    //   'ALTIMETER_INITIALIZING',
    '[PYR] Decode: BAD PACKET NUMBER',
    //   'PYR_BAD_PACKET_NUMBER',
    '[PYR] Decode: BAD TERMINATING CHARACTER',
    //   'PYR_BAD_TERMINATING_CHAR',
    '[PYR] Decode: BAD CHECKSUM',
    //   'PYR_BAD_CHECKSUM',
    '[PYR] Decode: NAN',
    //   'PYR_NAN',                             [45]
    '  Time: ',
    //   'PYR_TIME',
    '  Pitch: ',
    //   'PYR_PITCH',
    '  Roll: ',
    //   'PYR_ROLL',
    '  Heading: ',
    //   'PYR_HEADING',
    '  Temp: ',
    //   'PYR_TEMPERATURE',                     [50]
    '  Altitude: ',
    //   'PYR_ALTITUDE',
    '[PYR] State: ON',
    //   'PYR_START',
    '[PYR] State: OFF',
    //   'PYR_STOP',
    '[PYR] Rate: BAD RATE',
    //   'PYR_BAD_RATE',
    '[PYR] Read: PACKET RECEIVED',
    //   'PYR_PACKET_RECEIVED',                 [55]
    '[PYR] Decode: VALID START CHAR',
    //   'PYR_VALID_START_CHAR',
    '[ESC] Setup/Extra: BAD ESC TYPE',
    //   'ESC_BAD_ESC_TYPE',
    '[ESC] State: START',
    //   'ESC_START',
    '[ESC] State: STOP',
    //   'ESC_STOP',
    '[ESC] Set Value:',
    //   'ESC_SET_VALUE',                       [60]
    '[ESC] Set Range:',
    //   'ESC_SET_RANGE',
    '[ESC] Setup: ON',
    //   'ESC_SETUP_ESC_ON',
    '[ESC] Setup: OFF',
    //   'ESC_SETUP_ESC_OFF',
    '[ESC] Setup: STICK LOW',
    //   'ESC_SETUP_STICK_LOW',
    '[ESC] Setup: STICK HIGH',
    //   'ESC_SETUP_STICK_HIGH',                [65]
    '[ESC] Setup: DELAY',
    //   'ESC_SETUP_DELAY',
    '[ESC] Setup: LONG DELAY',
    //   'ESC_SETUP_LONG_DELAY',
    '[ALTIMETER] Update: ',
    //   'ALTIMETER_UPDATE',
    '[ALTIMETER] Barometer Alt:',
    //   'ALTIMETER_BAROMETER_ALTITUDE',
    '[ALTIMETER] Barometer: OUT OF RANGE',
    //   'ALTIMETER_BAROMETER_OUT_OF_RANGE',    [70]
    '[ALTIMETER] Pre:',
    //   'ALTIMETER_PRE',
    '[ALTIMETER] Post:',
    //   'ALTIMETER_POST',
    '[ALTIMETER] Final:',
    //   'ALTIMETER_FINAL',
    '[ALTIMETER] Alt & Vel:',
    //   'ALTIMETER_ALTITUDE_AND_VELOCITY',
    '[HELI_SERVO] State: START',
    //   'HELI_SERVO_START',                    [75]
    '[HELI_SERVO] State: STOPT',
    //   'HELI_SERVO_STOP',
    '[PAYLOAD] NM: START',
    //   'PAYLOAD_NM_START',
    '[PAYLOAD] NM: STOP',
    //   'PAYLOAD_NM_STOP',
    '[FLOID] Mem: ',
    //   'MEMORY_DISPLAY',
    '[PAN_TILT] State: START',
    //   'PAN_TILT_START',                      [80]
    '[PAN_TILT] State: STOP',
    //   'PAN_TILT_STOP',
    '[GPS] State: START',
    //   'GPS_START',
    '[GPS] State: STOP',
    //   'GPS_STOP',
    '[GPS] Read: ',
    //   'GPS_DECIMAL_READING',
    '[GPS] Decode: UNSUPPORTED SENTENCE',
    //   'GPS_UNSUPPORTED_SENTENCE',            [85]
    '[ESC] Setup: OPTION DELAY',
    //   'ESC_SETUP_OPTION_DELAY',
    '[ESC] Setup: VALUE DELAY',
    //   'ESC_SETUP_VALUE_DELAY',
    '[ESC] Setup: OPTION VALUE',
    //   'ESC_SETUP_OPTION_VALUE',
    '[ESC] Setup: SWITCH BRAKE MODE',
    //   'ESC_SWITCH_BRAKE_MODE',
    '[GOAL] State: ACHIEVED',
    //   'GOAL_ACHIEVED',                       [90]
    '<- TEST PACKET',
    //   'TEST_PACKET_RECEIVED',
    '<- SERVO VALUE',
    //   'HELI_SERVO_VALUE_RECEIVED',
    '<- ESC VALUE',
    //   'HELI_ESC_VALUE_RECEIVED',
    '<- RELAY: ESC: ',
    //   'ESC_RELAY',
    '<- RELAY: ESC EXTRA',
    //   'ESC_EXTRA_RELAY',                     [95]
    '<- RELAY: ESC SETUP',
    //   'ESC_SETUP_RELAY',
    '<- RELAY: NM: ',
    //   'NM_RELAY',
    '<- RELAY: GPS: ',
    //   'GPS_RELAY',
    '<- RELAY: GPS EMULATE',
    //   'GPS_EMULATE_RELAY',
    '<- RELAY: PAN_TILT',
    //   'PAN_TILT_RELAY',                      [100]
    '<- COLLECTIVE VALUE',
    //   'HELI_COLLECTIVE_SERVO_VALUE_RECEIVED',
    '<- LEFT SERVO',
    //   'HELI_LEFT_SERVO_VALUE_RECEIVED',
    '<- RIGHT SERVO',
    //   'HELI_RIGHT_SERVO_VALUE_RECEIVED',
    '<- PITCH SERVO',
    //   'HELI_PITCH_SERVO_VALUE_RECEIVED',
    '<- ERROR: BAD SERVO VALUE',
    //   'HELI_SERVO_VALUE_BAD_SERVO',          [105]
    '<- ERROR: BAD HELI VALUE',
    //   'HELI_SERVO_VALUE_BAD_HELI',
    '<- RELAY: GPS DEVICE: ',
    //   'GPS_DEVICE_RELAY',
    '<- RELAY: PYR DEVICE: ',
    //   'PYR_DEVICE_RELAY',
    '<- SWITCH: GPS RATE: ',
    //   'GPS_RATE_SWITCH',
    '<- RELAY: PYR: ',
    //   'PYR_RELAY',                           [110]
    '<- RELAY: PYR A/D: ',
    //   'PYR_AD_RELAY',
    '<- SWITCH: PYR RATE: ',
    //   'PYR_RATE_SWITCH',
    '<- ERROR: BAD GPS RATE: ',
    //   'GPS_BAD_RATE_VALUE',
    '<- ERROR: BAD PYR RATE: ',
    //   'PYR_BAD_RATE_VALUE',
    '<- RELAY: DESIGNATOR: ',
    //   'DESIGNATOR_RELAY',                    [115]
    '<- RELAY: AUX: ',
    //   'AUX_RELAY',
    '<- PAN TILT VALUES',
    //   'PAN_TILT_VALUES',
    '<- RELAY: PARACHUTE',
    //   'PARACHUTE_RELAY',
    '<- HELI CONTROL',
    //   'HELI_CONTROL',
    '<- RELAY: LED: ',
    //   'LED_RELAY',                           [120]
    '<- RELAY: HELI SERVOS: ',
    //   'HELI_SERVOS_RELAY',
    '<- SWITCH: LIFT OFF MODE',
    //   'LIFT_OFF_MODE_SWITCH',
    '<- ERROR: WRONG MODE FOR LIFT OFF',
    //   'LIFT_OFF_MODE_SWITCH_WRONG_MODE',
    '<- SWITCH: NORMAL MODE',
    //   'NORMAL_MODE_SWITCH',
    '<- ERROR: WRONG MODE FOR NORMAL',
    //   'NORMAL_MODE_SWITCH_WRONG_MODE',
    '<- SWITCH: LAND MODE',
    //   'LAND_MODE_SWITCH',
    '<- ERROR: WRONG MODE FOR LAND',
    //   'LAND_MODE_SWITCH_WRONG_MODE',
    '<- SWITCH: TEST BLADES LOW',
    //   'TEST_BLADES_LOW_SWITCH',
    '<- SWITCH: TEST BLADES ZERO',
    //   'TEST_BLADES_ZERO_SWITCH',
    '<- TEST BLADES HIGH',
    //   'TEST_BLADES_HIGH_SWITCH',             [130]
    '<- SWITCH: TEST GOALS: ON',
    //   'TEST_GOALS_ON_SWITCH',
    '<- ERROR: WRONG MODE FOR TEST GOALS ON',
    //   'TEST_GOALS_ON_SWITCH_WRONG_MODE',
    '<- SWITCH: TEST GOALS: OFF',
    //   'TEST_GOALS_OFF_SWITCH',
    '<- ERROR: WRONG MODE FOR TEST GOALS OFF',
    //   'TEST_GOALS_OFF_SWITCH_WRONG_MODE',
    '<- ERROR: WRONG MODE FOR TEST BLADES LOW',
    //   'TEST_BLADES_LOW_SWITCH_WRONG_MODE',
    '<- ERROR: WRONG MODE FOR TEST BLADES ZERO',
    //   'TEST_BLADES_ZERO_SWITCH_WRONG_MODE',
    '<- ERROR: WRONG MODE FOR TEST BLADES HIGH',
    //   'TEST_BLADES_HIGH_SWITCH_WRONG_MODE',
    '<- RELAY: EEPROM LOAD',
    //   'EEPROM_LOAD_RELAY',
    '<- RELAY: EEPROM SAVE',
    //   'EEPROM_SAVE_RELAY',
    '<- RELAY: EEPROM CLEAR',
    //   'EEPROM_CLEAR_RELAY',                  [140]
    '<- ERROR: WRONG MODE FOR EEPROM LOAD',
    //   'EEPROM_LOAD_RELAY_WRONG_MODE',
    '<- ERROR: WRONG MODE FOR EEPROM SAVE',
    //   'EEPROM_SAVE_RELAY_WRONG_MODE',
    '<- ERROR: WRONG MODE FOR EEPROM CLEAR',
    //   'EEPROM_CLEAR_RELAY_WRONG_MODE',
    'SUCCESS',
    //   'SUCCESS',
    'FAILURE',
    //   'FAILURE',
    '<- RELAY: SERIAL OUTPUT:',
    //   'SERIAL_OUTPUT_RELAY',
    '<- RELAY: UNKNOWN',
    //   'RELAY_UNKNOWN',
    '<- DECLINATION',
    //   'DECLINATION_PACKET',
    '<- PARAMETERS',
    //   'SET_PARAMETERS_PACKET',
    '<- HELIS: ON',
    //   'HELIS_ON_PACKET',                     [150]
    '<- HELIS: OFF',
    //   'HELIS_OFF_PACKET',
    '<- DESIGNATE: ON',
    //   'DESIGNATE_TARGET_PACKET',
    '<- DEPLOY PARACHUTE',
    //   'DEPLOY_PARACHUTE_PACKET',
    '<- DESIGNATE: OFF',
    //   'STOP_DESIGNATING_PACKET',
    '<- POSITION GOAL',
    //   'POSITION_GOAL_PACKET',
    '<- ALTITUDE GOAL',
    //   'ALTITUDE_GOAL_PACKET',
    '<- ROTATION GOAL',
    //   'ROTATION_GOAL_PACKET',
    '<- LIFT OFF',
    //   'LIFT_OFF_PACKET',
    '<- ERROR: WRONG MODE FOR LIFT OFF',
    //   'LIFT_OFF_PACKET_WRONG_MODE'
    '<- ERROR: LIFT OFF ALTITUDE FAILURE',
    //   'LIFT_OFF_ALTITUDE_FAILURE',,          [160]
    '<- LAND',
    //   'LAND_PACKET',
    '<- ERROR: WRONG MODE FOR LAND',
    //   'LAND_PACKET_WRONG_MODE',
    '<- PAYLOAD',
    //   'PAYLOAD_PACKET',
    '<- ERROR: BAD PAYLOAD BAY',
    //   'PAYLOAD_PACKET_BAD_BAY',
    '<- MODE',
    //   'MODE_PACKET',
    '<- ERROR: BAD MODE',
    //   'MODE_PACKET_BAD_MODE',
    '<- ERROR: MODE ERROR - IN FLIGHT',
    //   'MODE_ERROR_IN_FLIGHT',
    '<- GET MODEL PARAMETERS',
    //   'GET_MODEL_PARAMETERS',
    '<- SET MODEL PARAMETERS 1',
    //   'SET_MODEL_PARAMETERS_1',
    '<- SET MODEL PARAMETERS 2',
    //   'SET_MODEL_PARAMETERS_2',              [170]
    '<- SET MODEL PARAMETERS 3',
    //   'SET_MODEL_PARAMETERS_3',
    '<- SET MODEL PARAMETERS 4',
    //   'SET_MODEL_PARAMETERS_4',
    '<- TEST GOALS: ',
    //   'TEST_GOALS_PACKET',
    '<- DEVICE GPS: ',
    //   'DEVICE_GPS_PACKET',
    '<- DEVICE PYR: ',
    //   'DEVICE_PYR_PACKET',
    '<- ERROR: UNKNOWN COMMAND',
    //   'UNKNOWN_COMMAND',
    '<- ERROR: NO RESPONSE SENT',
    //   'NO_RESPONSE_SENT',
    'heli: ',
    // This should be [178]
    'servo: ',
    ' is below ',
    // 180
    ' is above ',
    'attackAngle (d) ',
    'targetOrientationPitch (d) ',
    'targetOrientationRoll (d) ',
    'targetOrientationPitchDelta (d) ',
    'targetOrientationRollDelta (d) ',
    'targetPitchVelocity (d/sec) ',
    'targetRollVelocity (d/sec) ',
    'targetPitchVelocityDelta (d/sec) ',
    'targetRollVelocityDelta (d/sec) ',
    // 190
    'targetAltitudeVelocity  (m/sec) ',
    'targetAltitudeVelocityDelta (m/sec) ',
    'targetHeadingVelocity (d/sec) ',
    'targetHeadingVelocityDelta (d/sec) ',
    'targetXYVelocity (m/sec) ',
    'distanceToTarget (m) ',
    'altitudeToTarget (m) ',
    'deltaHeadingAngleToTarget (d) ',
    'orientationToGoal (d) ',
    'pitchSmoothed (d) ',
    // 200
    'rollSmoothed (d) ',
    ' cyclicValue: [0-1] ',
    ' cyclicDegrees: [Cyclic Range d] ',
    ' collectiveValue: [0-1] ',
    ' collectiveDegrees: [Collective Range d] ',
    ' escValue: [min-max] ',
    ' escPulse: [ESC Pulse Range] ',
    ' servoValue: [min-max() or 0-1()] ',
    ' servoPulse: [Servo Pulse Range d] ',
    ' collectiveDeltaOrientation: ',
    // 210
    ' collectiveDeltaAltitude: ',
    ' collectiveDeltaHeading: ',
    ' collectiveDeltaTotal: ',
    ' calculatedCollectiveBladePitch: ',
    ' calculatedCyclicBladePitch: ',
    ' calculatedBladePitch: ',
    ' calculatedServoDegrees: ',
    ' calculatedServoPulse: ',
    ' cyclicHeadingValue: ',
    '<-- Command: ',
    // 220
    ' GoalId: ',
    ' TEST',
    ' HELI',
    ' PANT',
    ' RELA',
    ' DECL',
    ' SETP',
    ' HEL1',
    ' HEL0',
    ' DESI',
    // 230
    ' PARA',
    ' STPD',
    ' SETP',
    ' SETA',
    ' SETR',
    ' LIFT',
    ' LAND',
    ' PYLD',
    ' DEBG',
    ' MODE',
    // 240
    ' GETM',
    ' MPR1',
    ' MPR2',
    ' MPR3',
    ' MPR4',
    ' TSTG',
    ' UNKN',
    'Heli,',
    'Heli Angular Offset,',
    'Heli Offset To Target,',
    // 250
    'Servo,',
    'Servo Offset To Target,',
    'Servo Cyclic For Heading Rotation,',
    'Servo Target Projection,',
    'Servo Cyclic Value,',
    'x,',
    'y,',
    'z,',
    'x-goal,',
    'y-goal,',
    // 260
    'z-goal,',
    'Ticks,',
    'Acceleration Multiplier,',
    'Distance To Target (m),',
    'Altitude To Target (m),',
    'Orientation Of Target (degrees),',
    'Heading (compass),',
    'Heading goal (compass),',
    'Orientation To Target (degrees),',
    'Orientation To Goal (degrees),',
    // 270
    'Target Orientation Pitch,',
    'Target Orientation Roll,',
    'Pitch Smoothed,',
    'Target Orientation Pitch Delta,',
    'Roll Smoothed,',
    'Target Orientation Roll Delta,',
    'Target Pitch Velocity,',
    'Target Roll Velocity,',
    'Target Pitch Velocity Delta,',
    'Target Roll Velocity Delta,',
    // 280
    'Target Altitude Velocity,',
    'Target Altitude Velocity Delta,',
    'Heading Velocity,',
    'Target Heading Velocity,',
    'Target Heading Velocity Delta,',
    'Velocity Over Ground,',
    'Target XY Velocity,',
    'Collective Delta Orientation,',
    'Collective Delta Altitude,',
    'Collective Delta Heading,',
    // 290
    'Collective Delta Total',
    // This should be [291]
    'gy,',
    // Should be [292]
    'gx,',
    'y,',
    'x,',
    'gz,',
    'a,',
    'ga,',
    'PM,',
    'PX',
    'RM,',
    'RX',
    'AM,',
    'AX',
    'pd,',
    'rd',
    'ad',
    'chd',
    'lhd',
    'ca',
    // Should be [310]
    'c_c->servos #: ',
    //   HELI_SERVOS_FROM_COLLECTIVES_AND_CYCLICS
    'cyclicbladepitch : ',
    //   CYCLIC_BLADE_PITCH
    'value->servo     : ',
    //   HELI_SERVO_FROM_VALUE
    'bladepitch->servo: ',
    //   HELI_SERVO_FROM_BLADE_PITCH
    'degree_val->servo: ',
    //   HELI_SERVO_FROM_DEGREE_VALUE   [315]
    'calccyclcbldeptch: ',
    //   CALC_CYCLIC_BLADE_PITCH
    'collectivbladptch: ',
    //   COLLECTIVE_BLADE_PITCH
    'bladeptch->servdeg: ',
    //   BLADE_PITCH_TO_SERVO_DEGREES
    'servdeg->servpulse: ',
    //   SERVO_DEGREES_TO_SERVO_PULSE
    'escvalue->escpulse: ',
    //   ESC_VALUE_TO_ESC_PULSE         [320]
    'heli: ',
    //   MODEL_HELI
    ' servo: ',
    //   MODEL_SERVO
    ' input: ',
    //   MODEL_INPUT
    ' output: ',
    //   MODEL_OUTPUT
    ' cyclic: ',
    //   MODEL_CYCLIC                   [325]
    ' collective: ',
    //   MODEL_COLLECTIVE
    ' DGPS',
    //   DEVICE_GPS_PACKET
    ' DPYR',
    //   DEVICE_PYR_PACKET
    '___       ___     ___       __     __          \n |  |__| |__     |__  |    /  \\ | |  \\         \n |  |  | |___    |    |___ \\__/ | |__/         \n                                               \n ___         __        ___            __   __  \n|__   /\\  | / _` |    |__  |     /\\  |__) /__` \n|    /~~\\ | \\__> |___ |___ |___ /~~\\ |__) .__/ \n                     ',
    // See here - http://patorjk.com/software/taag/#p=display&f=Stick%20Letters&t=The%20Floid%0AFaiglelabs
    '[ANDROID ACCESSORY] Getting Protocol: ',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_GET_PROTOCOL              (330u)
    '[ANDROID ACCESSORY] Switching Device: ',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_SWITCHING_DEVICE          (331u)
    '[ANDROID ACCESSORY] Protocol: ',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_PROTOCOL                  (332u)
    '[ANDROID ACCESSORY] Starting',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_START                     (333u)
    '[ANDROID ACCESSORY] Waiting For Device',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_USB_WAITING_FOR_DEVICE    (334u)
    '[ANDROID ACCESSORY] Bad Protocol',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_BAD_PROTOCOL                                                 (335u)
    '[ANDROID ACCESSORY] Endpoint Descriptor Error',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_CONFIG_DESCRIPTOR_ERROR                       (336u)
    '[ANDROID ACCESSORY] Endpoint Descriptor Too Large',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_DESCRIPTOR_TOO_LARGE_ERROR                    (337u)
    '[ANDROID ACCESSORY] Endpoint Descriptor Error 2',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_CONFIG_DESCRIPTOR_2_ERROR                     (338u)
    '[ANDROID ACCESSORY] Find Endpoints: Descriptor Configuration',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_DESCRIPTOR_CONFIGURATION                      (339u)
    '[ANDROID ACCESSORY] Find Endpoints: Descriptor Interface',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_DESCRIPTOR_INTERFACE                          (340u)
    '[ANDROID ACCESSORY] Find Endpoints: Descriptor Unknown Error',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_DESCRIPTOR_UNKNOWN                            (341u)
    '[ANDROID ACCESSORY] Find Endpoints: No Endpoints Error',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_NO_ENDPOINTS_ERROR                            (342u)
    '[ANDROID ACCESSORY] Configure Android: In EP: ',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_CONFIGURE_ANDROID_IN_EP                                      (343u)
    '[ANDROID ACCESSORY] Configure Android: Out EP: ',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_CONFIGURE_ANDROID_OUT_EP                                     (344u)
    '[ANDROID ACCESSORY] Configure Android: Can\'t Choose One Config Error',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_CONFIGURE_ANDROID_CANT_CHOOSE_CONFIG_ONE_ERROR               (345u)
    '[ANDROID ACCESSORY] Connected: Reading Device Descriptor',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_READING_DEVICE_DESCRIPTOR                       (346u)
    '[ANDROID ACCESSORY] Connected: Reading Device Descriptor Error',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_READING_DEVICE_DESCRIPTOR_ERROR                 (347u)
    '[ANDROID ACCESSORY] Connected: Found Accessory Device',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_FOUND_ACCESSORY_DEVICE                          (348u)
    '[ANDROID ACCESSORY] Connected: Found Possible Device Switching To Serial Mode',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_FOUND_POSSIBLE_DEVICE_SWITCHING_TO_SERIAL_MODE  (349u)
    '[ANDROID ACCESSORY] Connected: Waiting For Device',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_WAITING_FOR_DEVICE                              (350u)
    '[USB] Reset',
    // FLOID_DEBUG_LABEL_RESET_USB                                                                      (351u)
    '[USB] RCode: ',
    // FLOID_DEBUG_LABEL_USB_RCODE                                                                      (352u)
    '[USB] Sending Setup',
    // FLOID_DEBUG_LABEL_USB_SENDING_SETUP                                                              (353u)
    '[USB] OSCOKIEQ Failed To Assert Error',
    // FLOID_DEBUG_LABEL_USB_OSCOKIEQ_FAILED_TO_ASSERT_ERROR                                            (354u)
    '[USB] Powering On',
    // FLOID_DEBUG_LABEL_USB_POWERING_ON                                                                (355u)
    '[GPS] Setup',
    // FLOID_DEBUG_LABEL_GPS_SETUP                                                                      (356u)
    '[GPS] Kalman X Init Error',
    // FLOID_DEBUG_LABEL_GPS_KALMAN_X_INIT_FAIL                                                         (357u)
    '[GPS] Kalman Y Init Error',
    // FLOID_DEBUG_LABEL_GPS_KALMAN_Y_INIT_FAIL                                                         (358u)
    '[GPS] Using Kalman Z',
    // FLOID_DEBUG_LABEL_GPS_USING_Z_KALMAN                                                             (359u)
    '[GPS] Kalman Z Init Error',
    // FLOID_DEBUG_LABEL_GPS_KALMAN_Z_INIT_FAIL                                                         (360u)
    '[GPS] Kalman Initialized - Setting Rate',
    // FLOID_DEBUG_LABEL_GPS_KALMAN_INITIALIZED_SETTING_RATE                                            (361u)
    '[GPS] Setup Done',
    // FLOID_DEBUG_LABEL_GPS_SETUP_DONE                                                                 (362u)
    '[GPS] Raw Terms: ',
    // FLOID_DEBUG_LABEL_GPS_RAW_TERMS                                                                  (363u)
    '[GPS] New Rate: ',
    // FLOID_DEBUG_LABEL_GPS_NEW_RATE                                                                   (364u)
    '[FLOID] **** NON-PRODUCTION MODE ****',
    // FLOID_DEBUG_LABEL_FLOID_NON_PRODUCTION                                                           (365u)
    '[USB] Timeout',
    // FLOID_DEBUG_LABEL_USB_TIMEOUT                                                                    (366u)
    '[SETUP] Setup Commands',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_COMMANDS                                                           (367u)
    '[SETUP] Setup LEDs',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_LEDS                                                               (368u)
    '[SETUP] Setup Batteries',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_BATTERIES                                                          (369u)
    '[SETUP] Setup Currents',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_CURRENTS                                                           (370u)
    '[SETUP] Setup GPS',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_GPS                                                                (371u)
    '[SETUP] Setup PYR',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_PYR                                                                (372u)
    '[SETUP] Setup Altimeter',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_ALTIMETER                                                          (373u)
    '[SETUP] Setup Model',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_MODEL                                                              (374u)
    '[SETUP] Setup Model Status',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_MODEL_STATUS                                                       (375u)
    '[SETUP] Setup Helis Servos',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_HELI_SERVOS                                                        (376u)
    '[SETUP] Setup ESCs',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_ESCS                                                               (377u)
    '[SETUP] Setup Pan/Tilts',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_PAN_TILTS                                                          (378u)
    '[SETUP] Setup Designator',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_DESIGNATOR                                                         (379u)
    '[SETUP] Setup Camera',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_CAMERA                                                             (380u)
    '[SETUP] Setup Payloads',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_PAYLOADS                                                           (381u)
    '[SETUP] Setup AUX',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_AUX                                                                (382u)
    '[SETUP] Setup Parachute',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_PARACHUTE                                                          (383u)
    '[SETUP] Setup Success',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_SUCCESS                                                            (384u)
    '[SETUP] Setup Error',
    // FLOID_DEBUG_LABEL_FLOID_SETUP_FAILURE                                                            (385u)
    '[ANDROID ACCESSORY] Logic Error: noUsbOutput should be true',
    // FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF                         (386u)
    '[EEPROM] Loading...',
    // FLOID_DEBUG_LABEL_EEPROM_LOADING                                                                 (387u)
    '[EEPROM] Load: Success',
    // FLOID_DEBUG_LABEL_EEPROM_LOAD_SUCCESS                                                            (388u)
    '[EEPROM] Load: Fail',
    // FLOID_DEBUG_LABEL_EEPROM_LOAD_FAILURE                                                            (389u)
    ' HTBT',
    // FLOID_DEBUG_LABEL_HEARTBEAT_PACKET                                                              (390u)
    '[PAYLOAD] NM Fully Contracted: ',
    // FLOID_DEBUG_PAYLOAD_NM_FULLY_CONTRACTED                                                              (391u)
    '[PAYLOAD] Released: ',
    // FLOID_DEBUG_PAYLOAD_RELEASED                                                              (392u)
    '[PAYLOAD] Timed Out: ',
    // FLOID_DEBUG_PAYLOAD_TIMED_OUT                                                              (393u)
    '[SERVO] Bad Servo Type: ',
    'UNUSED',
    '<- RELAY: RUN MODE PRODUCTION RELAY: ',
    // FLOID_DEBUG_RUN_MODE_PRODUCTION_RELAY                (396u)
    '<- RELAY: TEST MODE PRINT DEBUG HEADINGS RELAY: ',
    // FLOID_DEBUG_TEST_MODE_PRINT_DEBUG_HEADINGS_RELAY      (397u)
    '<- RELAY: RUN MODE TEST XY RELAY: ',
    // FLOID_DEBUG_RUN_MODE_TEST_XY_RELAY                   (398u)
    '<- RELAY: RUN MODE TEST ALTITUDE1 RELAY: ',
    // FLOID_DEBUG_RUN_MODE_TEST_ALTITUDE1_RELAY            (399u)
    '<- RELAY: RUN MODE TEST ALTITUDE2 RELAY: ',
    // FLOID_DEBUG_RUN_MODE_TEST_ALTITUDE2_RELAY            (400u)
    '<- RELAY: RUN MODE TEST HEADING1 RELAY: ',
    // FLOID_DEBUG_RUN_MODE_TEST_HEADING1_RELAY             (401u)
    '<- RELAY: RUN MODE TEST PITCH1 RELAY: ',
    // FLOID_DEBUG_RUN_MODE_TEST_PITCH1_RELAY               (402u)
    '<- RELAY: RUN MODE TEST ROLL1 RELAY: ',
    // FLOID_DEBUG_RUN_MODE_TEST_ROLL1_RELAY                (403u)
    '<- RELAY: TEST MODE NO XY RELAY: ',
    // FLOID_DEBUG_TEST_MODE_NO_XY_RELAY                    (404u)
    '<- RELAY: TEST MODE NO ALTITUDE RELAY: ',
    // FLOID_DEBUG_TEST_MODE_NO_ALTITUDE_RELAY              (405u)
    '<- RELAY: TEST MODE NO ATTACK ANGLE RELAY: ',
    // FLOID_DEBUG_TEST_MODE_NO_ATTACK_ANGLE_RELAY          (406u)
    '<- RELAY: TEST MODE NO HEADING RELAY: ',
    // FLOID_DEBUG_TEST_MODE_NO_HEADING_RELAY               (407u)
    '<- RELAY: TEST MODE NO PITCH RELAY: ',
    // FLOID_DEBUG_TEST_MODE_NO_PITCH_RELAY                 (408u)
    '<- RELAY: TEST MODE NO ROLL RELAY: ',
    // FLOID_DEBUG_TEST_MODE_NO_ROLL_RELAY                  (409u)
    '<- RELAY: TEST MODE CHECK PHYSICS MODEL RELAY: ',
    // FLOID_DEBUG_TEST_MODE_CHECK_PHYSICS_MODEL_RELAY      (410u)
    '<- SWITCH: PYR RATE: '
    // FLOID_DEBUG_STATUS_RATE_SWITCH     (411u)
  ];

  /// Get the token string for a token.
  static String getTokenString(int token) {
    try {
      return tokens[token];
    }
    catch (_) {
      return (' *** BAD TOKEN: ' + token.toString() + ' *** ');
    }
  }

  /// Get the number of tokens.
  static int getNumberTokens() {
    return tokens.length;
  }
}