/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';


class MissionChooser extends StatefulWidget {
  final List<DroidMissionList> droidMissionListList;
  final int currentMissionId;

  MissionChooser({Key? key, required this.droidMissionListList, required this.currentMissionId,}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MissionChooserState();
  }
}

class _MissionChooserState extends State<MissionChooser> {

  @override
  Widget build(BuildContext context) {
    final TextStyle? fBodyText = Theme.of(context).textTheme.bodyMedium;
    final TextStyle useBodyText = fBodyText==null ? TextStyle() : fBodyText;
    return
      SimpleDialog(
        titlePadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
        title: Container(alignment: Alignment.topCenter, color: Theme.of(context).splashColor,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Mission', style: TextStyle(fontSize: 24),),
          ),),
        backgroundColor: Theme.of(context).dialogBackgroundColor,
        contentPadding: EdgeInsets.fromLTRB(16, 8, 0, 8),
        children: widget.droidMissionListList.map((droidMissionList) =>
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, droidMissionList);
              },
              padding: EdgeInsets.fromLTRB(0,8,0,0),
              child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
              if(widget.currentMissionId!=-1 && widget.currentMissionId==droidMissionList.missionId)
                Icon(MdiIcons.check, size: 20),
              Text(droidMissionList.missionName,
                style: useBodyText
                    .apply(fontSizeFactor: 1.3)
                    .apply(fontStyle: (widget.currentMissionId!=-1 && widget.currentMissionId==droidMissionList.missionId)?FontStyle.italic:FontStyle.normal),
               ),
                  ],
              ),
            ),
        ).toList(),
      );
  }
}

