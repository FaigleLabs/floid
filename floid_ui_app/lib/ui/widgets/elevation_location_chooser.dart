/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ElevationLocationChooser extends StatefulWidget {
  final List<String> elevationLocations;
  final String? currentElevationLocation;

  ElevationLocationChooser({Key? key, required this.elevationLocations, this.currentElevationLocation}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ElevationLocationChooserState();
  }
}

class _ElevationLocationChooserState extends State<ElevationLocationChooser> {

  @override
  Widget build(BuildContext context) {
    final TextStyle? fBodyText = Theme.of(context).textTheme.bodyMedium;
    final TextStyle useBodyText = fBodyText==null ? TextStyle() : fBodyText;
    return SimpleDialog(
        titlePadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
        contentPadding: const EdgeInsets.fromLTRB(8, 12,  0, 4),
        title: Container(alignment: Alignment.topCenter,
          color: Colors.yellowAccent,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Elevation Location', style: TextStyle(fontSize: 22, color: Colors.black),),
          ),),
        backgroundColor: Colors.black,
        children: widget.elevationLocations.map((elevationLocation) =>
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, elevationLocation);
              },
              padding: EdgeInsets.fromLTRB(16,0,0,8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  if(widget.currentElevationLocation!=null && widget.currentElevationLocation==elevationLocation)
                    Icon(MdiIcons.check, size: 20),
                  Text(
                    elevationLocation,
                    style: (widget.currentElevationLocation!=null && widget.currentElevationLocation==elevationLocation)?useBodyText.apply(fontSizeFactor: 1.3).apply(fontStyle: FontStyle.italic):useBodyText.apply(fontSizeFactor: 1.3),
                  ),
                ],
              ),
            ),
        ).toList(),
      );
  }
}
