/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class FloidUuidChooser extends StatefulWidget {
  final List<FloidUuidAndState> floidUuidAndStateList;
  final String currentFloidUuid;

  FloidUuidChooser({Key? key, required this.floidUuidAndStateList, required this.currentFloidUuid}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _FloidUuidChooserState();
  }
}

class _FloidUuidChooserState extends State<FloidUuidChooser> {

  @override
  Widget build(BuildContext context) {
    final TextStyle? fBodyText = Theme.of(context).textTheme.bodyMedium;
    final TextStyle useBodyText = fBodyText==null ? TextStyle() : fBodyText;
    return SimpleDialog(
      titlePadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      title: Container(alignment: Alignment.topCenter, color: Theme.of(context).splashColor,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Instance', style: TextStyle(fontSize: 24),),
        ),),
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      children: widget.floidUuidAndStateList.map((floidUuidAndState) =>
          SimpleDialogOption(
            padding:  EdgeInsets.symmetric(vertical:0, horizontal: 6.0),
            onPressed: () {
              Navigator.pop(context, floidUuidAndState);
              },
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                if(widget.currentFloidUuid!='' && widget.currentFloidUuid==floidUuidAndState.floidUuid)
                  Icon(MdiIcons.check, size: 20),
                Text(DateFormat.yMMMMd("en_US").add_jm().format(DateTime.fromMillisecondsSinceEpoch(floidUuidAndState.floidUuidCreateTime, isUtc: true)),
                  style: useBodyText.apply(fontSizeFactor: 1.4).apply( fontStyle: floidUuidAndState.floidStatus?FontStyle.italic:FontStyle.normal)
                ),              ],
            ),
          ),
      ).toList(),
    );
  }
}
