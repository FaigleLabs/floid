
/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class FloidChooser extends StatefulWidget {
  final List<FloidIdAndState> floidIdAndStateList;
  final int currentFloidId;

  FloidChooser({Key? key, required this.floidIdAndStateList, required this.currentFloidId}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _FloidChooserState();
  }
}

class _FloidChooserState extends State<FloidChooser> {
  @override
  Widget build(BuildContext context) {
    return
      SimpleDialog(
        titlePadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
        title: Container(alignment: Alignment.topCenter, color: Theme.of(context).splashColor,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Floid', style: TextStyle(fontSize: 24),),
          ),),
        contentPadding: EdgeInsets.fromLTRB(24, 8, 0, 8),
        backgroundColor: Theme.of(context).dialogBackgroundColor,
        children: widget.floidIdAndStateList.map((floidIdAndState) =>
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, floidIdAndState);
              },
              padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  if(widget.currentFloidId!=-1 && widget.currentFloidId==floidIdAndState.floidId)
                    Icon(MdiIcons.check, size: 20),
                Text(
                  floidIdAndState.floidId.toString(),
                  style: TextStyle(fontSize: 18, color: Theme.of(context).textTheme.bodyMedium?.color??Colors.yellowAccent,
                  fontStyle: floidIdAndState.floidStatus?FontStyle.italic:FontStyle.normal,),
              ),
              ],
            ),

            ),
        ).toList(),
      );
  }
}
