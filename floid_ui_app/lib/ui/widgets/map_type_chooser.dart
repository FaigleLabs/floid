/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class MapTypeChooser extends StatefulWidget {
  final List<String> mapTypes;
  final String? currentMapType;

  MapTypeChooser({Key? key, required this.mapTypes, this.currentMapType}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MapTypeChooserState();
  }
}

class _MapTypeChooserState extends State<MapTypeChooser> {

  @override
  Widget build(BuildContext context) {
    final TextStyle? fBodyText = Theme.of(context).textTheme.bodyMedium;
    final TextStyle useBodyText = fBodyText==null ? TextStyle() : fBodyText;
    return SimpleDialog(
        titlePadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
        contentPadding: const EdgeInsets.fromLTRB(8, 12,  0, 4),
        title: Container(alignment: Alignment.topCenter,
          color: Colors.yellowAccent,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Map Type', style: TextStyle(fontSize: 22, color: Colors.black),),
          ),),
        backgroundColor: Colors.black,
        children: widget.mapTypes.map((mapType) =>
            SimpleDialogOption(
              onPressed: () {
                Navigator.pop(context, mapType);
              },
              padding: EdgeInsets.fromLTRB(16,0,0,8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  if(widget.currentMapType!=null && widget.currentMapType==mapType)
                    Icon(MdiIcons.check, size: 20),
                  Text(
                    mapType,
                    style: useBodyText.apply(fontSizeFactor: 1.3)
                    .apply(fontStyle: (widget.currentMapType!=null && widget.currentMapType==mapType)?FontStyle.italic:FontStyle.normal),
                  ),
                ],
              ),
            ),
        ).toList(),
      );
  }
}
