/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FloidServerStatusPoller extends StatefulWidget {
  static const int FLOID_SERVER_STATUS_POLL_PERIOD = 5;
  final Widget child;

  FloidServerStatusPoller({Key? key, required this.child,}) : super(key: key);

  @override
  _FloidServerStatusPoller createState() => _FloidServerStatusPoller();
}

class _FloidServerStatusPoller extends State<FloidServerStatusPoller> {

  late Timer timer;

  void checkServerStatus() async {
    try {
      //ignore: close_sinks
      FloidServerRepositoryBloc floidServerRepositoryBloc = BlocProvider.of<FloidServerRepositoryBloc>(this.context);
      final FloidServerRepositoryState fState = floidServerRepositoryBloc.state;
      if (fState is FloidServerRepositoryStateHasHost) {
        FloidServerRepositoryStateHasHost floidServerRepositoryStateNewHost = fState;
        try {
          FloidServerStatus floidServerStatus = await floidServerRepositoryStateNewHost.floidServerRepository.fetchFloidServerStatus();
          // Set Server Status App state:
          //ignore: close_sinks
          FloidAppStateBloc floidAppStateBloc = BlocProvider.of<FloidAppStateBloc>(this.context);
          floidAppStateBloc.add(SetServerStatusAppStateEvent(floidServerStatus: floidServerStatus));
          // Set Server Status state:
          //ignore: close_sinks
          FloidServerStatusBloc floidServerStatusBloc = BlocProvider.of<FloidServerStatusBloc>(this.context);
          floidServerStatusBloc.add(new NewFloidServerStatusEvent(floidServerStatus: floidServerStatus));
        } catch(e) {
          print('caught error [1] checking server status: ${e.toString()}');
          FloidAppStateBloc floidAppStateBloc = BlocProvider.of<FloidAppStateBloc>(this.context);
          floidAppStateBloc.add(SetServerStatusAppStateEvent(floidServerStatus: FloidServerStatus(FloidServerStatus.SERVER_STATUS_OFFLINE)));
          FloidServerStatusBloc floidServerStatusBloc = BlocProvider.of<FloidServerStatusBloc>(this.context);
          floidServerStatusBloc.add(new NewFloidServerStatusEvent(floidServerStatus: FloidServerStatus(FloidServerStatus.SERVER_STATUS_OFFLINE)));
        }
      }
    } catch(e) {
      print('caught error [2] checking server status: ${e.toString()}');
    }
  }

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: FloidServerStatusPoller.FLOID_SERVER_STATUS_POLL_PERIOD), (Timer t) => checkServerStatus());
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

}