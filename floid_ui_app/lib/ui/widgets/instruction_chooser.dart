/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:flutter/material.dart';

class InstructionChooser extends StatefulWidget {
  final List<String> instructions;

  InstructionChooser({Key? key, required this.instructions}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _InstructionChooserState();
  }
}

class _InstructionChooserState extends State<InstructionChooser> {

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      titlePadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      contentPadding: EdgeInsets.fromLTRB(8, 12, 0, 8),
      title: Container(alignment: Alignment.topCenter,
        color: Colors.yellowAccent,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Instruction', style: TextStyle(fontSize: 22, color: Colors.black),),
        ),),
      backgroundColor: Colors.black,
      children: widget.instructions.map((instruction) =>
          SimpleDialogOption(
            onPressed: () {
              Navigator.pop(context, instruction);
            },
            padding: EdgeInsets.fromLTRB(12, 2, 0, 2),
            child:
                Text(
                  instruction,
                  style: TextStyle(fontSize: 22, color: Colors.yellowAccent,),
                ),
          ),
      ).toList(),
    );
  }
}
