/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FloidStatusPoller extends StatefulWidget {
  static const int FLOID_STATUS_POLL_PERIOD = 10;
  final Widget child;

  FloidStatusPoller({Key? key, required this.child,}) : super(key: key);

  @override
  _FloidStatusPoller createState() => _FloidStatusPoller();
}

class _FloidStatusPoller extends State<FloidStatusPoller> {
  late Timer timer;

  void checkStatus() async {
    try {
      //ignore: close_sinks
      FloidIdBloc floidIdBloc = BlocProvider.of<FloidIdBloc>(this.context);
      final FloidIdState fFloidIdBlocState = floidIdBloc.state;
      FloidIdAndState? floidIdAndState;
      if (fFloidIdBlocState is FloidIdLoaded) {
        FloidIdLoaded floidIdLoaded = fFloidIdBlocState;
        floidIdAndState = floidIdLoaded.floidIdAndState;
      }
      if (fFloidIdBlocState is FloidUuidLoaded) {
        FloidUuidLoaded floidUuidLoaded = fFloidIdBlocState;
        floidIdAndState = floidUuidLoaded.floidIdAndState;
      }
      final FloidServerRepositoryBloc floidServerRepositoryBloc = BlocProvider
          .of<
          FloidServerRepositoryBloc>(this.context);
      if (floidServerRepositoryBloc
          .state is FloidServerRepositoryStateHasHost) {
        FloidServerRepositoryStateHasHost floidServerRepositoryStateNewHost = floidServerRepositoryBloc
            .state as FloidServerRepositoryStateHasHost;
        List<
            FloidIdAndState> floidIdAndStateList = await floidServerRepositoryStateNewHost
            .floidServerRepository.fetchFloidIdAndStateList();
        bool active = false;
        floidIdAndStateList.forEach((floidIdAndStateFromServer) {
          if (floidIdAndState != null &&
              floidIdAndStateFromServer.floidId == floidIdAndState.floidId) {
            active = floidIdAndStateFromServer.floidActive;
          }
        });
        //ignore: close_sinks
        FloidAppStateBloc floidAppStateBloc = BlocProvider.of<
            FloidAppStateBloc>(this.context);
        floidAppStateBloc.add(SetFloidAliveAppStateEvent(floidIsAlive: active));
      }
    } catch (e) {
      print('caught error checking if floid is alive ${e.toString()}');
    }
  }


  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(
        Duration(seconds: FloidStatusPoller.FLOID_STATUS_POLL_PERIOD), (
        Timer t) => checkStatus());
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

}