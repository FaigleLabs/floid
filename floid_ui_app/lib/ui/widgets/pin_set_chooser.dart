/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class PinSetChooser extends StatefulWidget {
  final List<DroidPinSetList> pinSetListList;
  final int currentPinSetId;

  PinSetChooser({Key? key, required this.pinSetListList, required this.currentPinSetId}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PinSetChooserState();
  }
}

class _PinSetChooserState extends State<PinSetChooser> {

  @override
  Widget build(BuildContext context) {
    final TextStyle? fBodyText = Theme.of(context).textTheme.bodyMedium;
    final TextStyle useBodyText = fBodyText==null ? TextStyle() : fBodyText;
    return SimpleDialog(
      titlePadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      title: Container(alignment: Alignment.topCenter, color: Theme.of(context).splashColor,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text('Pin Set', style: TextStyle(fontSize: 24),),
        ),),
      contentPadding: EdgeInsets.fromLTRB(16, 8, 0, 8),
      backgroundColor: Theme.of(context).dialogBackgroundColor,
      children: widget.pinSetListList.map((droidPinSetList) =>
          SimpleDialogOption(
            onPressed: () {
              Navigator.pop(context, droidPinSetList);
              },
            padding: EdgeInsets.fromLTRB(0,8,0,0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                if(widget.currentPinSetId!=-1 && widget.currentPinSetId==droidPinSetList.pinSetId)
                  Icon(MdiIcons.check, size: 20),
            Text(
              droidPinSetList.pinSetName,
              style: useBodyText
                  .apply(fontSizeFactor: 1.3)
                  .apply(fontStyle: (widget.currentPinSetId!=-1 && widget.currentPinSetId==droidPinSetList.pinSetId)?FontStyle.italic:FontStyle.normal),
            ),
            ],

            ),

          ),
      ).toList(),
    );
  }
}
