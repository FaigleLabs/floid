/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:io';
import 'dart:math' as math;
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:floid_ui_app/ui/dialog/pin_editor_dialog.dart';
import 'package:floid_ui_app/ui/keys/keys.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_elevation_request.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_path_elevation.dart';
import 'package:floid_ui_app/ui/map/line/line.dart';
import 'package:floid_ui_app/ui/map/marker/floid_pin_attributes.dart';
import 'package:floid_ui_app/ui/map/marker/marker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:floid_ui_app/ui/map/utility/distance.dart' as dist;
import 'package:positioned_tap_detector_2/positioned_tap_detector_2.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:floid_ui_app/ui/generic/outlined_text.dart';
class FloidMapWidget extends StatefulWidget {
  static final floidMapBoxUrlTemplateUrl = 'https://api.mapbox.com/styles/v1/mapbox/{mapType}/tiles/{z}/{x}/{y}?access_token={accessToken}';
  static final floidUserAgentPackageName = 'com.faiglelabs.floid';
  static final floidMapBoxUrlTemplateStreetsUrl = floidMapBoxUrlTemplateUrl.replaceAll('{mapType}', 'streets-v12');
  static final floidMapBoxUrlTemplateOutdoorsUrl = floidMapBoxUrlTemplateUrl.replaceAll('{mapType}', 'outdoors-v12');
  static final floidMapBoxUrlTemplateLightUrl = floidMapBoxUrlTemplateUrl.replaceAll('{mapType}', 'light-v11');
  static final floidMapBoxUrlTemplateDarkUrl = floidMapBoxUrlTemplateUrl.replaceAll('{mapType}', 'dark-v11');
  static final floidMapBoxUrlTemplateSatelliteUrl = floidMapBoxUrlTemplateUrl.replaceAll('{mapType}', 'satellite-v9');
  static final floidMapBoxUrlTemplateSatelliteStreetsUrl = floidMapBoxUrlTemplateUrl.replaceAll('{mapType}', 'satellite-streets-v12');
  static final floidMapBoxUrlTemplateNavigationPreviewDayUrl = floidMapBoxUrlTemplateUrl.replaceAll('{mapType}', 'navigation-day-v1');
  static final floidMapBoxUrlTemplateNavigationPreviewNightUrl = floidMapBoxUrlTemplateUrl.replaceAll('{mapType}', 'navigation-night-v1');
  static final floidLocalUrlTemplate = "/maps/{z}/{x}/{y}.png";
  final FloidServerRepository floidServerRepository;

  FloidMapWidget({
    GlobalKey? key,
    required this.floidServerRepository,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _FloidMapWidgetState();
  }

  static const Color FLOID_GPS_MAP_LINES_COLOR = Colors.indigoAccent;
  static const double FLOID_GPS_MAP_LINES_STROKE_WIDTH = 5.0;
  static const Color DROID_GPS_MAP_LINES_COLOR = Colors.purple;
  static const double DROID_GPS_MAP_LINES_STROKE_WIDTH = 5.0;
  static const Color MISSION_MAP_LINES_COLOR_GOOD = Color(0xFF0000FF);
  static const Color MISSION_MAP_LINES_COLOR_WARNING = Color(0xFFFB8C00);
  static const Color MISSION_MAP_LINES_COLOR_UNKNOWN = Colors.pink;
  static const Color MISSION_MAP_LINES_COLOR_ERROR = Colors.red;
  static const Color MISSION_MAP_LINES_BORDER_COLOR_ERROR = Color(0xFFFF7F00);
  static const Color MISSION_MAP_LINES_COLOR_NETWORK_ERROR = Colors.black;
  static const double MISSION_MAP_LINES_STROKE_WIDTH = 4.0;
  static const Color DESIGNATE_MAP_LINES_COLOR = Colors.yellowAccent;
  static const double DESIGNATE_MAP_LINES_STROKE_WIDTH = 4.0;
  static const double DESIGNATE_MAP_LINES_BORDER_STROKE_WIDTH = 2.0;
  static const Color DESIGNATE_MAP_LINES_BORDER_COLOR = Color(0xFF64DD17);
  // ignore: non_constant_identifier_names
  static IconData DROID_OBJECT_ICON = MdiIcons.crosshairsGps;
  static const Color DROID_OBJECT_COLOR = Color(0xFF64DD17);
  static const Color DROID_OBJECT_SHADOW_COLOR = Color(0x7F000000);
  static const double DROID_OBJECT_ICON_SIZE = 24;
  // ignore: non_constant_identifier_names
  static IconData FLOID_OBJECT_ICON = MdiIcons.navigation;
  static const Color FLOID_OBJECT_COLOR = Colors.yellowAccent;
  static const Color FLOID_OBJECT_SHADOW_COLOR = Color(0x4F000000);
  static const double FLOID_OBJECT_ICON_SIZE = 36;
  // ignore: non_constant_identifier_names
  static IconData FLOID_OBJECT_SHADOW_ICON = MdiIcons.circle;
  static const Color TARGET_OBJECT_COLOR = Color(0xFFBA68C8);
  // ignore: non_constant_identifier_names
  static IconData TARGET_OBJECT_ICON = MdiIcons.star;
  static const Color TARGET_OBJECT_SHADOW_COLOR = Color(0x7F000000);
  static const double TARGET_OBJECT_ICON_SIZE = 32;
  // ignore: non_constant_identifier_names
  static IconData HOME_OBJECT_ICON = MdiIcons.targetVariant;
  static const Color HOME_OBJECT_COLOR = Colors.lightBlue;
  static const Color HOME_OBJECT_SHADOW_COLOR = Color(0x7F000000);
  static const double HOME_OBJECT_ICON_SIZE = 32;
  // ignore: non_constant_identifier_names
  static IconData MISSION_HOME_ICON = MdiIcons.targetVariant;
  static const Color MISSION_HOME_ICON_COLOR = Colors.black;
  static const Color MISSION_HOME_ICON_SHADOW_COLOR = Color(0x7F000000);
  static const double MISSION_HOME_ICON_SIZE = 32;
  // ignore: non_constant_identifier_names
  static IconData FLIGHT_ARROW_ICON = MdiIcons.arrowRightThin;
  static const double FLIGHT_ARROW_ICON_SIZE = 28;
  static const Color FLIGHT_ARROW_COLOR = Color(0xFF76FF03);
  static const Color FLIGHT_ARROW_SHADOW_COLOR = Color(0x9F000000);
  static const Color BOTTOM_MODAL_BACKGROUND_COLOR = Colors.black;
  static const Color BOTTOM_MODAL_TITLE_COLOR = Colors.black;
  static const Color BOTTOM_MODAL_HEADER_BACKGROUND_COLOR = Color(0xFFFFFF00);
  static const Color BOTTOM_MODAL_TEXT_COLOR = Color(0xFFFFFF00);
  static const Color MARKER_NAME_COLOR = Color(0xFFFFFF00);
  static const Color MARKER_ELEVATION_ERROR_NAME_COLOR = Color(0xFFFF7F00);
  static const Color MARKER_NAME_SHADOW_COLOR = Color(0xFF000000);
  // ignore: non_constant_identifier_names
  static IconData MARKER_ICON = MdiIcons.arrowDownThin;
  static const Color MARKER_ATTRIBUTE_ICON_COLOR = Color(0xFFFFFF00);
  static const Color MARKER_ATTRIBUTE_SHADOW_ICON_COLOR = Color(0xFF3f3f3f);
  static const Color MARKER_ATTRIBUTE_BACKGROUND_COLOR = Color(0xDFFFB8C00);
  static const double MARKER_ATTRIBUTE_SIZE = 12.0;
  static const double MARKER_ATTRIBUTE_HEIGHT = 12.0;
  static const double MARKER_ATTRIBUTE_WIDTH = 12.0;
  static const double MARKER_ATTRIBUTE_SMALL_SIZE = 10.0;
  static const double MARKER_ATTRIBUTE_SMALL_HEIGHT = 10.0;
  static const double MARKER_ATTRIBUTE_SMALL_WIDTH = 10.0;
  // ignore: non_constant_identifier_names
  static IconData PAYLOAD_MARKER_ICON = MdiIcons.alphaXCircleOutline;
  // ignore: non_constant_identifier_names
  static IconData HOME_MARKER_ICON = MdiIcons.home;
  // ignore: non_constant_identifier_names
  static IconData DESIGNATOR_MARKER_ICON = MdiIcons.floorLampTorchiereVariantOutline;
  // ignore: non_constant_identifier_names
  static IconData DESIGNATED_MARKER_ICON = MdiIcons.starCircleOutline;
  // ignore: non_constant_identifier_names
  static IconData LIFT_OFF_MARKER_ICON = MdiIcons.arrowExpandUp;
  // ignore: non_constant_identifier_names
  static IconData LAND_MARKER_ICON = MdiIcons.arrowCollapseDown;
  // ignore: non_constant_identifier_names
  static IconData PHOTO_MARKER_ICON = MdiIcons.camera;
  // ignore: non_constant_identifier_names
  static IconData PHOTO_OFF_MARKER_ICON = MdiIcons.cameraOff;
  // ignore: non_constant_identifier_names
  static IconData VIDEO_MARKER_ICON = MdiIcons.video;
  // ignore: non_constant_identifier_names
  static IconData VIDEO_OFF_MARKER_ICON = MdiIcons.videoOff;
  // ignore: non_constant_identifier_names
  static IconData SPEAKER_MARKER_ICON = MdiIcons.volumeHigh;
  // ignore: non_constant_identifier_names
  static IconData SPEAKER_ON_MARKER_ICON = MdiIcons.bullhorn;
  // ignore: non_constant_identifier_names
  static IconData SPEAKER_OFF_MARKER_ICON = MdiIcons.volumeOff;
  // ignore: non_constant_identifier_names
  static IconData ASCEND_MARKER_ICON = MdiIcons.arrowUpThin;
  // ignore: non_constant_identifier_names
  static IconData DESCEND_MARKER_ICON = MdiIcons.arrowDownThin;
}

class _FloidMapWidgetState extends State<FloidMapWidget> {
  late MapController _mapController;
  late String _templateUrl;
  late bool _addMapBoxAccessToken;
  late bool _addLocalAuthToken;

  @override
  void initState() {
    super.initState();
    _mapController = MapController();
    _templateUrl = FloidMapWidget.floidMapBoxUrlTemplateStreetsUrl;
    _addMapBoxAccessToken = true;
    _addLocalAuthToken = false;
  }

  @override
  Widget build(BuildContext context) {
    // App State:
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
      builder: (context, floidAppState) {
        // Floid State:
        return BlocBuilder<FloidStatusBloc, FloidStatusState>(
          builder: (context, floidStatusState) {
            // Floid Droid State:
            return BlocBuilder<FloidDroidStatusBloc, FloidDroidStatusState>(
              builder: (context, floidDroidStatusState) {
                // Floid Pin Set State:
                return BlocBuilder<FloidPinSetBloc, FloidPinSetState>(
                  builder: (context, floidPinSetState) {
                    // Floid Mission State:
                    return BlocBuilder<FloidMissionBloc, FloidMissionState>(
                      builder: (context, floidMissionState) {
                        // Floid Map Floid GPS Lines State:
                        return BlocBuilder<FloidMapLinesBloc<FloidMapFloidGpsLine>, FloidMapLinesState>(
                          builder: (context, floidMapFloidGpsLinesState) {
                            // Floid Map Droid GPS Lines State:
                            return BlocBuilder<FloidMapLinesBloc<FloidMapFloidDroidGpsLine>, FloidMapLinesState>(
                              builder: (context, floidMapFloidDroidGpsLinesState) {
                                // Floid Map Designate Lines State:
                                return BlocBuilder<FloidMapLinesBloc<FloidMapDesignateLine>, FloidMapLinesState>(
                                  builder: (context, floidMapDesignateLinesState) {
                                    // Floid Map Fly Lines State:
                                    return BlocBuilder<FloidMapLinesBloc<FloidMapFlyLine>, FloidMapLinesState>(
                                      builder: (context, floidMapFlyLinesState) {
                                        // Floid Status Marker State:
                                        return BlocBuilder<FloidMapMarkerBloc<FloidStatusMarker>, FloidMapMarkerState>(
                                          builder: (context, floidStatusMarkerState) {
                                            // Floid Home Marker State:
                                            return BlocBuilder<FloidMapMarkerBloc<FloidAcquiredHomeMarker>, FloidMapMarkerState>(
                                              builder: (context, floidAcquiredHomeMarkerState) {
                                                // Floid Target Marker State:
                                                return BlocBuilder<FloidMapMarkerBloc<FloidTargetMarker>, FloidMapMarkerState>(
                                                  builder: (context, floidTargetMarkerState) {
                                                    // Floid Fly Line Path Elevation Marker State:
                                                    return BlocBuilder<FloidMapMarkersBloc<FloidFlyLinePathElevationMarker>, FloidMapMarkersState>(
                                                      builder: (context, floidFlyLinePathElevationMarkersState) {
                                                        return BlocBuilder<FloidMapMarkersBloc<FloidMissionHomeMarker>, FloidMapMarkersState>(
                                                          builder: (context, floidMissionHomeMarkersState) {
                                                            // Floid Pin Markers State:
                                                            return BlocBuilder<FloidMapMarkersBloc<FloidPinMarker>, FloidMapMarkersState>(
                                                              builder: (context, floidPinMarkersState) {
                                                                // Zoom map to pins bloc listener:
                                                                return BlocListener<FloidAppStateBloc, FloidAppState>(
                                                                  listenWhen: (previousFloidAppState, currentFloidAppState) {
                                                                    if (previousFloidAppState is FloidAppStateLoaded && currentFloidAppState is FloidAppStateLoaded)
                                                                      if (currentFloidAppState.floidAppStateAttributes.zoomToPinsFlag != previousFloidAppState.floidAppStateAttributes.zoomToPinsFlag) {
                                                                      return true;
                                                                    }

                                                                    return false;
                                                                  },
                                                                  listener: (context, floidAppState) {
                                                                    FloidMapMarkersState floidPinMarkersStateLocal = BlocProvider.of<FloidMapMarkersBloc<FloidPinMarker>>(context).state;
                                                                    if (floidPinMarkersStateLocal is FloidMapMarkersLoaded) {
                                                                      final Key? fKey = widget.key;
                                                                      if (fKey != null && fKey is GlobalKey) {
                                                                        final RenderObject? widgetRenderObject = fKey.currentContext?.findRenderObject();
                                                                        if (widgetRenderObject != null && widgetRenderObject is RenderBox) {
                                                                          _zoomMapToMarkers(_mapController, floidPinMarkersStateLocal.floidMapMarkers, widgetRenderObject.size.height.floor(), widgetRenderObject.size.width.floor());
                                                                        }
                                                                      }
                                                                    }
                                                                  },
                                                                  child: BlocListener<FloidAppStateBloc, FloidAppState>(
                                                                    listenWhen: (previousFloidAppState, currentFloidAppState) {
                                                                      if (previousFloidAppState is FloidAppStateLoaded && currentFloidAppState is FloidAppStateLoaded)                                                                      if (currentFloidAppState.floidAppStateAttributes.zoomInFlag != previousFloidAppState.floidAppStateAttributes.zoomInFlag) {
                                                                        return true;
                                                                      }
                                                                      return false;
                                                                    },
                                                                    listener: (context, floidAppState) {
                                                                      FloidMapMarkersState floidPinMarkersStateLocal = BlocProvider.of<FloidMapMarkersBloc<FloidPinMarker>>(context).state;
                                                                      if (floidPinMarkersStateLocal is FloidMapMarkersLoaded) {
                                                                        _zoomIn(_mapController);
                                                                      }
                                                                    },
                                                                    child: BlocListener<FloidAppStateBloc, FloidAppState>(
                                                                      listenWhen: (previousFloidAppState, currentFloidAppState) {
                                                                        if (previousFloidAppState is FloidAppStateLoaded && currentFloidAppState is FloidAppStateLoaded)                                                                        if (currentFloidAppState.floidAppStateAttributes.zoomOutFlag != previousFloidAppState.floidAppStateAttributes.zoomOutFlag) {
                                                                          return true;
                                                                        }

                                                                        return false;
                                                                      },
                                                                      listener: (context, floidAppState) {
                                                                        FloidMapMarkersState floidPinMarkersStateLocal = BlocProvider.of<FloidMapMarkersBloc<FloidPinMarker>>(context).state;
                                                                        if (floidPinMarkersStateLocal is FloidMapMarkersLoaded) {
                                                                          _zoomOut(_mapController);
                                                                        }
                                                                      },
                                                                      child: BlocListener<FloidAppStateBloc, FloidAppState>(
                                                                        listenWhen: (previousFloidAppState, currentFloidAppState) {
                                                                          if (previousFloidAppState is FloidAppStateLoaded && currentFloidAppState is FloidAppStateLoaded)                                                                          if (currentFloidAppState.floidAppStateAttributes.mapType != previousFloidAppState.floidAppStateAttributes.mapType) {
                                                                            return true;
                                                                          }

                                                                          return false;
                                                                        },
                                                                        listener: (context, floidAppState) {
                                                                          if (floidAppState is FloidAppStateLoaded) {
                                                                            switch (floidAppState.floidAppStateAttributes.mapType) {
                                                                              case 'streets-v12':
                                                                                _templateUrl = FloidMapWidget.floidMapBoxUrlTemplateStreetsUrl;
                                                                                _addMapBoxAccessToken = true;
                                                                                _addLocalAuthToken = false;
                                                                                break;
                                                                              case 'outdoors-v12':
                                                                                _templateUrl = FloidMapWidget.floidMapBoxUrlTemplateOutdoorsUrl;
                                                                                _addMapBoxAccessToken = true;
                                                                                _addLocalAuthToken = false;
                                                                                break;
                                                                              case 'light-v11':
                                                                                _templateUrl = FloidMapWidget.floidMapBoxUrlTemplateLightUrl;
                                                                                _addMapBoxAccessToken = true;
                                                                                _addLocalAuthToken = false;
                                                                                break;
                                                                              case 'dark-v11':
                                                                                _templateUrl = FloidMapWidget.floidMapBoxUrlTemplateDarkUrl;
                                                                                _addMapBoxAccessToken = true;
                                                                                _addLocalAuthToken = false;
                                                                                break;
                                                                              case 'satellite-v9':
                                                                                _templateUrl = FloidMapWidget.floidMapBoxUrlTemplateSatelliteUrl;
                                                                                _addMapBoxAccessToken = true;
                                                                                _addLocalAuthToken = false;
                                                                                break;
                                                                              case 'satellite-streets-v12':
                                                                                _templateUrl = FloidMapWidget.floidMapBoxUrlTemplateSatelliteStreetsUrl;
                                                                                _addMapBoxAccessToken = true;
                                                                                _addLocalAuthToken = false;
                                                                                break;
                                                                              case 'navigation-day-v1':
                                                                                _templateUrl = FloidMapWidget.floidMapBoxUrlTemplateNavigationPreviewDayUrl;
                                                                                _addMapBoxAccessToken = true;
                                                                                _addLocalAuthToken = false;
                                                                                break;
                                                                              case 'navigation-night-v1':
                                                                                _templateUrl = FloidMapWidget.floidMapBoxUrlTemplateNavigationPreviewNightUrl;
                                                                                _addMapBoxAccessToken = true;
                                                                                _addLocalAuthToken = false;
                                                                                break;
                                                                              case 'floid-server':
                                                                                FloidServerRepositoryState floidServerRepositoryState = BlocProvider.of<FloidServerRepositoryBloc>(context).state;
                                                                                _templateUrl = (floidServerRepositoryState is FloidServerRepositoryStateHasHost
                                                                                        ? floidServerRepositoryState.floidServerRepository.floidServerApiClient.localMapsUrl
                                                                                        : '') +
                                                                                    FloidMapWidget.floidLocalUrlTemplate;
                                                                                _addMapBoxAccessToken = false;
                                                                                _addLocalAuthToken = true;
                                                                                print("_addLocalAuthToken");
                                                                                break;
                                                                            }
                                                                          }
                                                                        },
                                                                        child: FlutterMap(
                                                                          mapController: _mapController,
                                                                          options: MapOptions(
                                                                            onLongPress: (TapPosition tapPosition, LatLng point) {
                                                                              FloidServerRepositoryState floidServerRepositoryState = BlocProvider.of<FloidServerRepositoryBloc>(context).state;
                                                                              if (floidServerRepositoryState is FloidServerRepositoryStateHasHost && floidAppState is FloidAppStateLoaded) {
                                                                                _addPin(point, context, floidServerRepositoryState.floidServerRepository.floidServerApiClient, floidServerRepositoryState.secure,
                                                                                    floidAppState.floidAppStateAttributes.elevationLocation);
                                                                              }
                                                                            },
                                                                            center: LatLng(34, -70),
                                                                            zoom: 3.0,
                                                                          ),
                                                                          children: [
                                                                            TileLayer(
                                                                              tileProvider: NetworkTileProvider( headers: {
                                                                                  if (_addLocalAuthToken && BlocProvider.of<FloidServerRepositoryBloc>(context).state is FloidServerRepositoryStateHasHost)
                                                                                  HttpHeaders.authorizationHeader: (BlocProvider.of<FloidServerRepositoryBloc>(context).state as FloidServerRepositoryStateHasHost)
                                                                                .floidServerRepository
                                                                                .floidServerApiClient
                                                                                .authenticationToken,
                                                                                },
                                                                              ),
                                                                              userAgentPackageName: FloidMapWidget.floidUserAgentPackageName,
                                                                              urlTemplate: _templateUrl,
                                                                              tileSize: 512,
                                                                              zoomOffset: -1,
                                                                              additionalOptions: {
                                                                                if (_addMapBoxAccessToken) 'accessToken': FloidKeys.floidMapBoxAccessToken,
                                                                              },
                                                                            ),
                                                                            // Mission home marker:
                                                                            MarkerLayer(
                                                                                markers:  floidMissionHomeMarkersState is FloidMapMarkersLoaded
                                                                                    ? floidMissionHomeMarkersState.floidMapMarkers
                                                                                    .map((marker) => marker is FloidMissionHomeMarker
                                                                                    ? Marker(
                                                                                    width: 36,
                                                                                    height: 36,
                                                                                    anchorPos: AnchorPos.align(AnchorAlign.center),
                                                                                    builder: (context) =>
                                                                                        Icon(FloidMapWidget.MISSION_HOME_ICON, size: FloidMapWidget.MISSION_HOME_ICON_SIZE, color: FloidMapWidget.MISSION_HOME_ICON_COLOR),
                                                                                    point: LatLng(marker.markerLat, marker.markerLng))
                                                                                    : Marker(builder: (context) => Icon(FloidMapWidget.HOME_OBJECT_ICON), point: LatLng(0, 0)))
                                                                                    .toList()
                                                                                    : [],
                                                                            ),
                                                                            // Fly lines:
                                                                            PolylineLayer(
                                                                              polylines: floidMapFlyLinesState is FloidMapLinesLoaded
                                                                                  ? floidMapFlyLinesState.floidMapLines.map((floidMapLine) {
                                                                                      Color lineColor = Colors.black;
                                                                                      bool lineDotted = false;
                                                                                      double borderStrokeWidth = 0;
                                                                                      Color borderColor = const Color(0xFFFFFF00);
                                                                                      switch (floidMapLine.elevationStatus) {
                                                                                        case FloidMapLine.ELEVATION_STATUS_UNKNOWN:
                                                                                          lineColor = FloidMapWidget.MISSION_MAP_LINES_COLOR_UNKNOWN;
                                                                                          lineDotted = true;
                                                                                          borderStrokeWidth = 2;
                                                                                          break;
                                                                                        case FloidMapLine.ELEVATION_STATUS_FAIL:
                                                                                          lineColor = FloidMapWidget.MISSION_MAP_LINES_COLOR_ERROR;
                                                                                          lineDotted = true;
                                                                                          borderStrokeWidth = 2;
                                                                                          borderColor = FloidMapWidget.MISSION_MAP_LINES_BORDER_COLOR_ERROR;
                                                                                          break;
                                                                                        case FloidMapLine.ELEVATION_STATUS_GOOD:
                                                                                          switch (floidMapLine.altitudeStatus) {
                                                                                            case FloidMapLine.ALTITUDE_STATUS_GOOD:
                                                                                              lineDotted = false;
                                                                                              lineColor = FloidMapWidget.MISSION_MAP_LINES_COLOR_GOOD;
                                                                                              borderStrokeWidth = 0;
                                                                                              break;
                                                                                            case FloidMapLine.ALTITUDE_STATUS_WARNING:
                                                                                              lineDotted = true;
                                                                                              lineColor = FloidMapWidget.MISSION_MAP_LINES_COLOR_WARNING;
                                                                                              borderStrokeWidth = 2;
                                                                                              break;
                                                                                            case FloidMapLine.ALTITUDE_STATUS_ERROR:
                                                                                              lineDotted = true;
                                                                                              lineColor = FloidMapWidget.MISSION_MAP_LINES_COLOR_ERROR;
                                                                                              borderStrokeWidth = 2;
                                                                                              borderColor = FloidMapWidget.MISSION_MAP_LINES_BORDER_COLOR_ERROR;
                                                                                          }
                                                                                          break;
                                                                                      }
                                                                                      if (floidMapLine.elevationStatus == FloidMapLine.ELEVATION_STATUS_FAIL || floidMapLine.elevationStatus == FloidMapLine.ELEVATION_STATUS_UNKNOWN) {
                                                                                        lineDotted = true;
                                                                                      }
                                                                                      return Polyline(
                                                                                        strokeJoin: StrokeJoin.round,
                                                                                        strokeCap: StrokeCap.round,
                                                                                        borderStrokeWidth: borderStrokeWidth,
                                                                                        strokeWidth: FloidMapWidget.MISSION_MAP_LINES_STROKE_WIDTH,
                                                                                        isDotted: lineDotted,
                                                                                        color: lineColor,
                                                                                        borderColor: borderColor,
                                                                                        points: [
                                                                                          LatLng(floidMapLine.startLat, floidMapLine.startLng),
                                                                                          LatLng(floidMapLine.endLat, floidMapLine.endLng),
                                                                                        ],
                                                                                      );
                                                                                    }).toList()
                                                                                  : [],
                                                                            ),
                                                                            // 3. Designate lines:
                                                                            PolylineLayer(
                                                                              polylines: floidMapDesignateLinesState is FloidMapLinesLoaded
                                                                                  ? floidMapDesignateLinesState.floidMapLines
                                                                                      .map((floidMapLine) => Polyline(
                                                                                            color: FloidMapWidget.DESIGNATE_MAP_LINES_COLOR,
                                                                                            isDotted: true,
                                                                                            borderColor: FloidMapWidget.DESIGNATE_MAP_LINES_BORDER_COLOR,
                                                                                            borderStrokeWidth: FloidMapWidget.DESIGNATE_MAP_LINES_BORDER_STROKE_WIDTH,
                                                                                            strokeWidth: FloidMapWidget.DESIGNATE_MAP_LINES_STROKE_WIDTH,
                                                                                            points: [
                                                                                              LatLng(floidMapLine.startLat, floidMapLine.startLng),
                                                                                              LatLng(floidMapLine.endLat, floidMapLine.endLng),
                                                                                            ],
                                                                                          ))
                                                                                      .toList()
                                                                                  : [],
                                                                            ),
                                                                            // 7. Droid GPS lines:
                                                                            if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.showDroidGpsLines)
                                                                              PolylineLayer(
                                                                                polylines: floidMapFloidDroidGpsLinesState is FloidMapLinesLoaded
                                                                                    ? (floidMapFloidDroidGpsLinesState.floidMapLines
                                                                                        .map((floidMapLine) => Polyline(
                                                                                              color: FloidMapWidget.DROID_GPS_MAP_LINES_COLOR,
                                                                                        //       borderStrokeWidth: 1.0,
                                                                                              strokeWidth: FloidMapWidget.DROID_GPS_MAP_LINES_STROKE_WIDTH,
                                                                                              points: [
                                                                                                LatLng(floidMapLine.startLat, floidMapLine.startLng),
                                                                                                LatLng(floidMapLine.endLat, floidMapLine.endLng),
                                                                                              ],
                                                                                            ))
                                                                                        .toList()
                                                                                      // Add in the final current point from the end:
                                                                                      ..add(Polyline(
                                                                                        color: FloidMapWidget.DROID_GPS_MAP_LINES_COLOR,
                                                                                        // borderStrokeWidth: 1.0,
                                                                                        strokeWidth: FloidMapWidget.DROID_GPS_MAP_LINES_STROKE_WIDTH,
                                                                                        points: (floidDroidStatusState is FloidDroidStatusLoaded && floidMapFloidDroidGpsLinesState.floidMapLines.length > 0)
                                                                                            ? [
                                                                                                LatLng(floidMapFloidDroidGpsLinesState.floidMapLines.last.endLat, floidMapFloidDroidGpsLinesState.floidMapLines.last.endLng),
                                                                                                LatLng(floidDroidStatusState.floidDroidStatus.gpsLatitude, floidDroidStatusState.floidDroidStatus.gpsLongitude),
                                                                                              ]
                                                                                            : [],
                                                                                      )))
                                                                                    : [],
                                                                              ),
                                                                            // 8. Floid GPS lines:
                                                                            if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.showFloidGpsLines)
                                                                              PolylineLayer(
                                                                                polylines: floidMapFloidGpsLinesState is FloidMapLinesLoaded
                                                                                    ? (floidMapFloidGpsLinesState.floidMapLines
                                                                                        .map((floidMapLine) => Polyline(
                                                                                              color: FloidMapWidget.FLOID_GPS_MAP_LINES_COLOR,
                                                                                              // borderStrokeWidth: 1.0,
                                                                                              strokeWidth: FloidMapWidget.FLOID_GPS_MAP_LINES_STROKE_WIDTH,
                                                                                              points: [
                                                                                                LatLng(floidMapLine.startLat, floidMapLine.startLng),
                                                                                                LatLng(floidMapLine.endLat, floidMapLine.endLng),
                                                                                              ],
                                                                                            ))
                                                                                        .toList()
                                                                                      // Add in the final current point from the end:
                                                                                      ..add(Polyline(
                                                                                        color: FloidMapWidget.FLOID_GPS_MAP_LINES_COLOR,
                                                                                        // borderStrokeWidth: 1.0,
                                                                                        strokeWidth: FloidMapWidget.FLOID_GPS_MAP_LINES_STROKE_WIDTH,
                                                                                        points: (floidStatusState is FloidStatusLoaded && floidMapFloidGpsLinesState.floidMapLines.length > 0)
                                                                                            ? [
                                                                                                LatLng(floidMapFloidGpsLinesState.floidMapLines.last.endLat, floidMapFloidGpsLinesState.floidMapLines.last.endLng),
                                                                                                LatLng(floidStatusState.floidStatus.jyf, floidStatusState.floidStatus.jxf),
                                                                                              ]
                                                                                            : [],
                                                                                      )))
                                                                                    : [],
                                                                              ),
                                                                            // All clickable markers need to be in one layer at the top:
                                                                            // 1. Pin Set:
                                                                            MarkerLayer(
                                                                              markers: (floidPinMarkersState is FloidMapMarkersLoaded
                                                                                  ? floidPinMarkersState.floidMapMarkers.map((marker) {
                                                                                      final FloidMapMarker fMarker = marker;
                                                                                      final FloidPinMarker? fPinMarker = (fMarker is FloidPinMarker) ? fMarker : null;
                                                                                      final FloidPinAttributes? fFloidPinAttributes = fPinMarker?.floidPinAlt.floidPinAttributes;
                                                                                      final double? fMarkerElevation = fMarker.markerElevation;
                                                                                      return Marker(
                                                                                        width: 120,
                                                                                        height: 42,
                                                                                        anchorPos: AnchorPos.align(AnchorAlign.top),
                                                                                        point: LatLng(marker.markerLat, marker.markerLng),
                                                                                        builder: (ctx) => Container(
                                                                                          padding: EdgeInsets.all(0),
                                                                                          child: InkWell(
                                                                                            onTap: () {
                                                                                              showModalBottomSheet(
                                                                                                context: context,
                                                                                                builder: (builder) {
                                                                                                  return Padding(
                                                                                                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                                    child: Container(
                                                                                                      color: FloidMapWidget.BOTTOM_MODAL_BACKGROUND_COLOR,
                                                                                                      child: Column(
                                                                                                        mainAxisSize: MainAxisSize.min,
                                                                                                        children: <Widget>[
                                                                                                          InkWell(
                                                                                                            child: Container(
                                                                                                              padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                                              color: FloidMapWidget.BOTTOM_MODAL_BACKGROUND_COLOR,
                                                                                                              child: Column(
                                                                                                                children: [
                                                                                                                  Container(
                                                                                                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                                                    color: FloidMapWidget.BOTTOM_MODAL_HEADER_BACKGROUND_COLOR,
                                                                                                                    child: Row(
                                                                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                                      children: [
                                                                                                                        Padding(
                                                                                                                          padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                          child: Icon(
                                                                                                                            MdiIcons.mapMarker,
                                                                                                                            size: 28,
                                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_TITLE_COLOR,
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                        Text(
                                                                                                                          '${marker.markerName}',
                                                                                                                          style: TextStyle(
                                                                                                                            fontSize: 24,
                                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_TITLE_COLOR,
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                  Padding(
                                                                                                                    padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                    child: Row(
                                                                                                                      children: [
                                                                                                                        Column(
                                                                                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                                          children: [
                                                                                                                            Text(
                                                                                                                              'ID:',
                                                                                                                              style: TextStyle(
                                                                                                                                fontSize: 20,
                                                                                                                                color: FloidMapWidget.BOTTOM_MODAL_TEXT_COLOR,
                                                                                                                              ),
                                                                                                                            ),
                                                                                                                            Text(
                                                                                                                              'Lat/Lng:',
                                                                                                                              style: TextStyle(
                                                                                                                                fontSize: 20,
                                                                                                                                color: FloidMapWidget.BOTTOM_MODAL_TEXT_COLOR,
                                                                                                                              ),
                                                                                                                            ),
                                                                                                                            Text(
                                                                                                                              'Altitude:',
                                                                                                                              style: TextStyle(
                                                                                                                                fontSize: 20,
                                                                                                                                color: FloidMapWidget.BOTTOM_MODAL_TEXT_COLOR,
                                                                                                                              ),
                                                                                                                            ),
                                                                                                                          ],
                                                                                                                        ),
                                                                                                                        Padding(
                                                                                                                          padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                          child: Column(
                                                                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                                            children: [
                                                                                                                              Text(
                                                                                                                                '${marker.markerId.toString()}',
                                                                                                                                style: TextStyle(
                                                                                                                                  fontSize: 20,
                                                                                                                                  color: FloidMapWidget.BOTTOM_MODAL_TEXT_COLOR,
                                                                                                                                ),
                                                                                                                              ),
                                                                                                                              Text(
                                                                                                                                '${marker.markerLat.toStringAsFixed(7)}, ${marker.markerLng.toStringAsFixed(7)}',
                                                                                                                                style: TextStyle(
                                                                                                                                  fontSize: 20,
                                                                                                                                  color: FloidMapWidget.BOTTOM_MODAL_TEXT_COLOR,
                                                                                                                                ),
                                                                                                                              ),
                                                                                                                              Text(
                                                                                                                                '${fMarkerElevation != null ? fMarkerElevation.toStringAsFixed(1) : ""} m',
                                                                                                                                style: TextStyle(
                                                                                                                                  fontSize: 20,
                                                                                                                                  color: FloidMapWidget.BOTTOM_MODAL_TEXT_COLOR,
                                                                                                                                ),
                                                                                                                              ),
                                                                                                                            ],
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                ],
                                                                                                              ),
                                                                                                            ),
                                                                                                            onTap: () async {
                                                                                                              bool secure = true;
                                                                                                              FloidServerApiClient? floidServerApiClient;
                                                                                                              FloidServerRepositoryState floidServerRepositoryState = BlocProvider.of<FloidServerRepositoryBloc>(context).state;
                                                                                                              if (floidServerRepositoryState is FloidServerRepositoryStateHasHost) {
                                                                                                                secure = floidServerRepositoryState.secure;
                                                                                                                floidServerApiClient = floidServerRepositoryState.floidServerRepository.floidServerApiClient;
                                                                                                              }
                                                                                                              //ignore: close_sinks
                                                                                                              FloidPinSetBloc floidPinSetBlocLocal = BlocProvider.of<FloidPinSetBloc>(context);
                                                                                                              if (marker is FloidPinMarker) {
                                                                                                                FloidPinAlt? newFloidPinAlt = await showDialog<FloidPinAlt>(
                                                                                                                    context: context,
                                                                                                                    builder: (BuildContext context) {
                                                                                                                      final _formKey = GlobalKey<FormBuilderState>();
                                                                                                                      return PinEditorDialog(formKey: _formKey, floidPinAlt: marker.floidPinAlt);
                                                                                                                    });
                                                                                                                if (newFloidPinAlt != null) {
                                                                                                                  if (newFloidPinAlt.pinHeightAuto) {
                                                                                                                    FloidElevationRequest floidElevationRequest = FloidElevationRequest(
                                                                                                                      type: FloidElevationRequest.TYPE_PIN,
                                                                                                                      elevationRequestId: -1,
                                                                                                                      floidMapMarker: null,
                                                                                                                      floidPinAlt: newFloidPinAlt,
                                                                                                                      markerId: 0,
                                                                                                                    );
                                                                                                                    if (floidServerApiClient != null && floidAppState is FloidAppStateLoaded)
                                                                                                                      await FloidElevationApiClient(floidServerApiClient: floidServerApiClient)
                                                                                                                          .fetchElevation(floidElevationRequest, secure, floidAppState.floidAppStateAttributes.elevationLocation);
                                                                                                                  }
                                                                                                                  floidPinSetBlocLocal.add(ReplaceFloidPin(index: marker.markerIndex, floidPinAlt: newFloidPinAlt));
                                                                                                                }
                                                                                                              }
                                                                                                            },
                                                                                                          ),
                                                                                                        ],
                                                                                                      ),
                                                                                                    ),
                                                                                                  );
                                                                                                },
                                                                                              );
                                                                                            },
                                                                                            // Pin Marker:
                                                                                            child: Stack(
                                                                                              children: [
                                                                                                // Top icons:
                                                                                                Align(
                                                                                                  alignment: Alignment.topCenter,
                                                                                                  child: Row(
                                                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                    children: [
                                                                                                      Container(
                                                                                                        decoration: BoxDecoration(
                                                                                                          borderRadius: BorderRadius.all(Radius.circular(4)),
                                                                                                          border: Border.all(width: 1, color: Color(0xFF000000)),
                                                                                                          color: FloidMapWidget.MARKER_ATTRIBUTE_BACKGROUND_COLOR,
                                                                                                        ),
                                                                                                        child: Row(
                                                                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                                                                          children: <Widget>[
                                                                                                            // Payload:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isPayloadDropped)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.PAYLOAD_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.PAYLOAD_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Home:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isHome)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.HOME_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.HOME_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // LiftOff:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isLiftOff)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.LIFT_OFF_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SMALL_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.LIFT_OFF_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SMALL_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_SMALL_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_SMALL_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Land:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isLand)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.LAND_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SMALL_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.LAND_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SMALL_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_SMALL_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_SMALL_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Designator:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isDesignator)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.DESIGNATOR_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.DESIGNATOR_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Designated:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isDesignated)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.DESIGNATED_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.DESIGNATED_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Photo/PhotoStrobe:
                                                                                                            if (fFloidPinAttributes != null && (fFloidPinAttributes.isPhoto || fFloidPinAttributes.isPhotoStrobeOn))
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.PHOTO_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.PHOTO_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // PhotoStrobe off:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isPhotoStrobeOff)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.PHOTO_OFF_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.PHOTO_OFF_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Video On:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isVideoOn)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.VIDEO_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.VIDEO_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Video Off:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isVideoOff)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.VIDEO_OFF_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.VIDEO_OFF_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Speaker:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isSpeaker)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.SPEAKER_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.SPEAKER_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Speaker On:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isSpeakerOn)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.SPEAKER_ON_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.SPEAKER_ON_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                            // Speaker Off:
                                                                                                            if (fFloidPinAttributes != null && fFloidPinAttributes.isSpeakerOff)
                                                                                                              Padding(
                                                                                                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                                                                                                                child: MapControllerIcon(
                                                                                                                  iconChild: Icon(
                                                                                                                    FloidMapWidget.SPEAKER_OFF_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  shadowChild: Icon(
                                                                                                                    FloidMapWidget.SPEAKER_OFF_MARKER_ICON,
                                                                                                                    size: FloidMapWidget.MARKER_ATTRIBUTE_SIZE,
                                                                                                                    color: FloidMapWidget.MARKER_ATTRIBUTE_SHADOW_ICON_COLOR,
                                                                                                                  ),
                                                                                                                  height: FloidMapWidget.MARKER_ATTRIBUTE_HEIGHT,
                                                                                                                  width: FloidMapWidget.MARKER_ATTRIBUTE_WIDTH,
                                                                                                                ),
                                                                                                              ),
                                                                                                          ],
                                                                                                        ),
                                                                                                      ),
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                                // Marker pointer:
                                                                                                Align(
                                                                                                  alignment: Alignment.bottomCenter,
                                                                                                  child: Padding(
                                                                                                    padding: EdgeInsets.fromLTRB(48, 23, 0, 0),
                                                                                                    child: Stack(
                                                                                                      children: <Widget>[
                                                                                                        Positioned(
                                                                                                          left: -1.0,
                                                                                                          top: 0.0,
                                                                                                          child: Icon(
                                                                                                            FloidMapWidget.MARKER_ICON,
                                                                                                            size: 24,
                                                                                                            color: FloidMapWidget.MARKER_NAME_SHADOW_COLOR,
                                                                                                          ),
                                                                                                        ),
                                                                                                        Positioned(
                                                                                                          left: 1.0,
                                                                                                          top: 0.0,
                                                                                                          child: Icon(
                                                                                                            FloidMapWidget.MARKER_ICON,
                                                                                                            size: 24,
                                                                                                            color: FloidMapWidget.MARKER_NAME_SHADOW_COLOR,
                                                                                                          ),
                                                                                                        ),
                                                                                                        Positioned(
                                                                                                          left: 0.0,
                                                                                                          top: -1.0,
                                                                                                          child: Icon(
                                                                                                            FloidMapWidget.MARKER_ICON,
                                                                                                            size: 24,
                                                                                                            color: FloidMapWidget.MARKER_NAME_SHADOW_COLOR,
                                                                                                          ),
                                                                                                        ),
                                                                                                        Positioned(
                                                                                                          left: 0.0,
                                                                                                          top: 1.0,
                                                                                                          child: Icon(
                                                                                                            FloidMapWidget.MARKER_ICON,
                                                                                                            size: 24,
                                                                                                            color: FloidMapWidget.MARKER_NAME_SHADOW_COLOR,
                                                                                                          ),
                                                                                                        ),
                                                                                                        Positioned(
                                                                                                          left: -1.0,
                                                                                                          top: -1.0,
                                                                                                          child: Icon(
                                                                                                            FloidMapWidget.MARKER_ICON,
                                                                                                            size: 24,
                                                                                                            color: FloidMapWidget.MARKER_NAME_SHADOW_COLOR,
                                                                                                          ),
                                                                                                        ),
                                                                                                        Positioned(
                                                                                                          left: 1.0,
                                                                                                          top: 1.0,
                                                                                                          child: Icon(
                                                                                                            FloidMapWidget.MARKER_ICON,
                                                                                                            size: 24,
                                                                                                            color: FloidMapWidget.MARKER_NAME_SHADOW_COLOR,
                                                                                                          ),
                                                                                                        ),
                                                                                                        Positioned(
                                                                                                          left: -1.0,
                                                                                                          top: 1.0,
                                                                                                          child: Icon(
                                                                                                            FloidMapWidget.MARKER_ICON,
                                                                                                            size: 24,
                                                                                                            color: FloidMapWidget.MARKER_NAME_SHADOW_COLOR,
                                                                                                          ),
                                                                                                        ),
                                                                                                        Positioned(
                                                                                                          left: 1.0,
                                                                                                          top: -1.0,
                                                                                                          child: Icon(
                                                                                                            FloidMapWidget.MARKER_ICON,
                                                                                                            size: 24,
                                                                                                            color: FloidMapWidget.MARKER_NAME_SHADOW_COLOR,
                                                                                                          ),
                                                                                                        ),
                                                                                                        Positioned(
                                                                                                          left: 0.0,
                                                                                                          top: 0.0,
                                                                                                          child: Icon(
                                                                                                            FloidMapWidget.MARKER_ICON,
                                                                                                            size: 24,
                                                                                                            color: marker.markerElevation != FloidElevationApiClient.SERVER_FAIL_ELEVATION
                                                                                                                ? FloidMapWidget.MARKER_NAME_COLOR
                                                                                                                : FloidMapWidget.MARKER_ELEVATION_ERROR_NAME_COLOR,
                                                                                                          ),
                                                                                                        ),
                                                                                                      ],
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                                // Marker name:
                                                                                                Align(
                                                                                                  alignment: Alignment.topCenter,
                                                                                                  child: Container(
                                                                                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                                                                    height: 40,
                                                                                                    child: Center(
                                                                                                      child: OutlinedText(
                                                                                                        marker.markerName,
                                                                                                        strokeColor: Colors.black,
                                                                                                        textColor: marker.markerElevation != FloidElevationApiClient.SERVER_FAIL_ELEVATION
                                                                                                      ? FloidMapWidget.MARKER_NAME_COLOR
                                                                                                          : FloidMapWidget.MARKER_ELEVATION_ERROR_NAME_COLOR,
                                                                                                        strokeWidth: 2.0,
                                                                                                        style: TextStyle(
                                                                                                          fontFamily: 'Orbitron',
                                                                                                          fontSize: 14,
                                                                                                          fontWeight: FontWeight.w500,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ),
                                                                                                  ),
                                                                                                ),
                                                                                              ],
                                                                                            ),
                                                                                          ),
                                                                                        ),
                                                                                      );
                                                                                    }).toList()
                                                                                  : [])
                                                                                // 4. Fly line path elevation markers:
                                                                                ..addAll(floidFlyLinePathElevationMarkersState is FloidMapMarkersLoaded
                                                                                        ? floidFlyLinePathElevationMarkersState.floidMapMarkers
                                                                                            .map((marker) => marker is FloidFlyLinePathElevationMarker
                                                                                                ? Marker(
                                                                                                    anchorPos: AnchorPos.align(AnchorAlign.center),
                                                                                                    height: FloidMapWidget.FLIGHT_ARROW_ICON_SIZE,
                                                                                                    width: FloidMapWidget.FLIGHT_ARROW_ICON_SIZE,
                                                                                                    point: LatLng(marker.markerLat, marker.markerLng),
                                                                                                    builder: (ctx) => InkWell(
                                                                                                      child: Transform.rotate(
                                                                                                        angle: -math.atan2(marker.endPin.pinLat - marker.startPin.pinLat, marker.endPin.pinLng - marker.startPin.pinLng),
                                                                                                        child: MapControllerIcon(
                                                                                                          height: FloidMapWidget.FLIGHT_ARROW_ICON_SIZE,
                                                                                                          width: FloidMapWidget.FLIGHT_ARROW_ICON_SIZE,
                                                                                                          iconChild:
                                                                                                              Icon(FloidMapWidget.FLIGHT_ARROW_ICON, size: FloidMapWidget.FLIGHT_ARROW_ICON_SIZE, color: FloidMapWidget.FLIGHT_ARROW_COLOR),
                                                                                                          shadowChild: Icon(FloidMapWidget.FLIGHT_ARROW_ICON,
                                                                                                              size: FloidMapWidget.FLIGHT_ARROW_ICON_SIZE, color: FloidMapWidget.FLIGHT_ARROW_SHADOW_COLOR),
                                                                                                        ),
                                                                                                      ),
                                                                                                      onTap: () {
                                                                                                        showModalBottomSheet(
                                                                                                          context: context,
                                                                                                          builder: (builder) {
                                                                                                            final FloidPinAlt? fStartPin = marker.floidPathElevation.floidMapFlyLine?.startPin;
                                                                                                            final FloidPinAlt? fEndPin = marker.floidPathElevation.floidMapFlyLine?.endPin;
                                                                                                            return Padding(
                                                                                                              padding: const EdgeInsets.all(0.0),
                                                                                                              child: Container(
                                                                                                                color: FloidMapWidget.BOTTOM_MODAL_BACKGROUND_COLOR,
                                                                                                                child: Column(
                                                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                                                                  mainAxisSize: MainAxisSize.min,
                                                                                                                  children: <Widget>[
                                                                                                                    Container(
                                                                                                                      color: FloidMapWidget.BOTTOM_MODAL_HEADER_BACKGROUND_COLOR,
                                                                                                                      child: Row(
                                                                                                                        children: <Widget>[
                                                                                                                          Padding(
                                                                                                                            padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                            child: Text(
                                                                                                                              (fStartPin == null ? '' : fStartPin.pinName) + ' --> ' + (fEndPin == null ? '' : fEndPin.pinName),
                                                                                                                              style: TextStyle(
                                                                                                                                fontSize: 20,
                                                                                                                                color: FloidMapWidget.BOTTOM_MODAL_TITLE_COLOR,
                                                                                                                              ),
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                        ],
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                    Row(
                                                                                                                      children: <Widget>[
                                                                                                                        Padding(
                                                                                                                          padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                          child: Text(
                                                                                                                            'Distance:' + marker.distanceToStringWithUnits(),
                                                                                                                            style: TextStyle(fontSize: 16),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                    Row(
                                                                                                                      children: <Widget>[
                                                                                                                        Padding(
                                                                                                                          padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                          child: Text(
                                                                                                                            'Flight altitude: ' + marker.flightHeight.toStringAsFixed(0) + ' m',
                                                                                                                            style: TextStyle(fontSize: 16),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                    Row(
                                                                                                                      children: <Widget>[
                                                                                                                        Padding(
                                                                                                                          padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                          child: Text(
                                                                                                                            'Ground elevation: ' + marker.floidPathElevation.pathElevationMax.toStringAsFixed(0) + ' m',
                                                                                                                            style: TextStyle(fontSize: 16),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                        if (marker.floidPathElevation.pathElevationResult == FloidPathElevation.RESULT_SUCCESS)
                                                                                                                          Text(
                                                                                                                            '    (Valid)',
                                                                                                                            style: TextStyle(fontSize: 16),
                                                                                                                          ),
                                                                                                                        if (marker.floidPathElevation.pathElevationResult == FloidPathElevation.RESULT_FAIL)
                                                                                                                          Text(
                                                                                                                            '    (Failed)',
                                                                                                                            style: TextStyle(fontSize: 16),
                                                                                                                          ),
                                                                                                                        if (marker.floidPathElevation.pathElevationResult == FloidPathElevation.RESULT_UNKNOWN)
                                                                                                                          Text(
                                                                                                                            '    (Unknown) ',
                                                                                                                            style: TextStyle(fontSize: 16),
                                                                                                                          ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                    Row(
                                                                                                                      children: <Widget>[
                                                                                                                        Padding(
                                                                                                                          padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                          child: Text(
                                                                                                                            'Ground clearance: ' + (marker.flightHeight - marker.floidPathElevation.pathElevationMax).toStringAsFixed(0) + ' m',
                                                                                                                            style: TextStyle(fontSize: 16),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ],
                                                                                                                    ),
                                                                                                                  ],
                                                                                                                ),
                                                                                                              ),
                                                                                                            );
                                                                                                          },
                                                                                                        );
                                                                                                      },
                                                                                                    ),
                                                                                                  )
                                                                                                : Marker(builder: (context) => Icon(FloidMapWidget.HOME_OBJECT_ICON), point: LatLng(0, 0)))
                                                                                            .toList()
                                                                                        : [])
                                                                                ..addAll(
                                                                                    // 5. Acquired Home:
                                                                                    floidAcquiredHomeMarkerState is FloidMapMarkerLoaded
                                                                                        ? [
                                                                                            Marker(
                                                                                              width: 36,
                                                                                              height: 36,
                                                                                              anchorPos: AnchorPos.align(AnchorAlign.center),
                                                                                              point: LatLng(floidAcquiredHomeMarkerState.floidMapMarker.markerLat, floidAcquiredHomeMarkerState.floidMapMarker.markerLng),
                                                                                              builder: (ctx) => Container(
                                                                                                child: InkWell(
                                                                                                  splashColor: FloidMapWidget.HOME_OBJECT_COLOR,
                                                                                                  child: Stack(
                                                                                                    children: <Widget>[
                                                                                                      Positioned(
                                                                                                        left: 3.0,
                                                                                                        top: 3.0,
                                                                                                        child: Icon(
                                                                                                          FloidMapWidget.HOME_OBJECT_ICON,
                                                                                                          color: FloidMapWidget.HOME_OBJECT_SHADOW_COLOR,
                                                                                                          size: FloidMapWidget.HOME_OBJECT_ICON_SIZE,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Positioned(
                                                                                                        left: 2.0,
                                                                                                        top: 2.0,
                                                                                                        child: Icon(
                                                                                                          FloidMapWidget.HOME_OBJECT_ICON,
                                                                                                          color: FloidMapWidget.HOME_OBJECT_SHADOW_COLOR,
                                                                                                          size: FloidMapWidget.HOME_OBJECT_ICON_SIZE,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Positioned(
                                                                                                        left: 1.0,
                                                                                                        top: 1.0,
                                                                                                        child: Icon(
                                                                                                          FloidMapWidget.HOME_OBJECT_ICON,
                                                                                                          color: FloidMapWidget.HOME_OBJECT_SHADOW_COLOR,
                                                                                                          size: FloidMapWidget.HOME_OBJECT_ICON_SIZE,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Positioned(
                                                                                                        left: 0.0,
                                                                                                        top: 0.0,
                                                                                                        child: Icon(
                                                                                                          FloidMapWidget.HOME_OBJECT_ICON,
                                                                                                          color: FloidMapWidget.HOME_OBJECT_COLOR,
                                                                                                          size: FloidMapWidget.HOME_OBJECT_ICON_SIZE,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ],
                                                                                                  ),
                                                                                                  onTap: () {
                                                                                                    showModalBottomSheet(
                                                                                                      context: context,
                                                                                                      builder: (builder) {
                                                                                                        return Padding(
                                                                                                          padding: const EdgeInsets.all(0.0),
                                                                                                          child: Container(
                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_BACKGROUND_COLOR,
                                                                                                            child: Column(
                                                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                                                                              mainAxisSize: MainAxisSize.min,
                                                                                                              children: <Widget>[
                                                                                                                Container(
                                                                                                                  color: FloidMapWidget.BOTTOM_MODAL_HEADER_BACKGROUND_COLOR,
                                                                                                                  child: Row(
                                                                                                                    children: <Widget>[
                                                                                                                      Padding(
                                                                                                                        padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                        child: Text(
                                                                                                                          'Home Marker',
                                                                                                                          style: TextStyle(
                                                                                                                            fontSize: 20,
                                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_TITLE_COLOR,
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ),
                                                                                                                    ],
                                                                                                                  ),
                                                                                                                ),
                                                                                                                Row(
                                                                                                                  children: <Widget>[
                                                                                                                    Padding(
                                                                                                                      padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                      child: Text(
                                                                                                                        '${floidAcquiredHomeMarkerState.floidMapMarker.markerLat}, ${floidAcquiredHomeMarkerState.floidMapMarker.markerLng}',
                                                                                                                        style: TextStyle(fontSize: 16),
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  ],
                                                                                                                ),
                                                                                                              ],
                                                                                                            ),
                                                                                                          ),
                                                                                                        );
                                                                                                      },
                                                                                                    );
                                                                                                  },
                                                                                                ),
                                                                                              ),
                                                                                            ),
                                                                                          ]
                                                                                        : [])
                                                                                ..addAll(
                                                                                    // 6. Target Pin:
                                                                                    floidTargetMarkerState is FloidMapMarkerLoaded
                                                                                        ? [
                                                                                            Marker(
                                                                                              width: 36,
                                                                                              height: 36,
                                                                                              anchorPos: AnchorPos.align(AnchorAlign.center),
                                                                                              point: LatLng(floidTargetMarkerState.floidMapMarker.markerLat, floidTargetMarkerState.floidMapMarker.markerLng),
                                                                                              builder: (ctx) => Container(
                                                                                                child: InkWell(
                                                                                                  splashColor: FloidMapWidget.TARGET_OBJECT_COLOR,
                                                                                                  child: Stack(
                                                                                                    children: <Widget>[
                                                                                                      Positioned(
                                                                                                        left: 3.0,
                                                                                                        top: 3.0,
                                                                                                        child: Icon(
                                                                                                          FloidMapWidget.TARGET_OBJECT_ICON,
                                                                                                          color: FloidMapWidget.TARGET_OBJECT_SHADOW_COLOR,
                                                                                                          size: FloidMapWidget.TARGET_OBJECT_ICON_SIZE,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Positioned(
                                                                                                        left: 2.0,
                                                                                                        top: 2.0,
                                                                                                        child: Icon(
                                                                                                          FloidMapWidget.TARGET_OBJECT_ICON,
                                                                                                          color: FloidMapWidget.TARGET_OBJECT_SHADOW_COLOR,
                                                                                                          size: FloidMapWidget.TARGET_OBJECT_ICON_SIZE,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Positioned(
                                                                                                        left: 1.0,
                                                                                                        top: 1.0,
                                                                                                        child: Icon(
                                                                                                          FloidMapWidget.TARGET_OBJECT_ICON,
                                                                                                          color: FloidMapWidget.TARGET_OBJECT_SHADOW_COLOR,
                                                                                                          size: FloidMapWidget.TARGET_OBJECT_ICON_SIZE,
                                                                                                        ),
                                                                                                      ),
                                                                                                      Positioned(
                                                                                                        left: 0.0,
                                                                                                        top: 0.0,
                                                                                                        child: Icon(
                                                                                                          FloidMapWidget.TARGET_OBJECT_ICON,
                                                                                                          color: FloidMapWidget.TARGET_OBJECT_COLOR,
                                                                                                          size: FloidMapWidget.TARGET_OBJECT_ICON_SIZE,
                                                                                                        ),
                                                                                                      ),
                                                                                                    ],
                                                                                                  ),
                                                                                                  onTap: () {
                                                                                                    showModalBottomSheet(
                                                                                                      context: context,
                                                                                                      builder: (builder) {
                                                                                                        return Padding(
                                                                                                          padding: const EdgeInsets.all(0.0),
                                                                                                          child: Container(
                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_BACKGROUND_COLOR,
                                                                                                            child: Column(
                                                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                                                                              mainAxisSize: MainAxisSize.min,
                                                                                                              children: <Widget>[
                                                                                                                Container(
                                                                                                                  color: FloidMapWidget.BOTTOM_MODAL_HEADER_BACKGROUND_COLOR,
                                                                                                                  child: Row(
                                                                                                                    children: <Widget>[
                                                                                                                      Padding(
                                                                                                                        padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                        child: Text(
                                                                                                                          'Target',
                                                                                                                          style: TextStyle(
                                                                                                                            fontSize: 20,
                                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_TITLE_COLOR,
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ),
                                                                                                                    ],
                                                                                                                  ),
                                                                                                                ),
                                                                                                                Row(
                                                                                                                  children: <Widget>[
                                                                                                                    Padding(
                                                                                                                      padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                      child: Text(
                                                                                                                        '${floidTargetMarkerState.floidMapMarker.markerLat}, ${floidTargetMarkerState.floidMapMarker.markerLng}',
                                                                                                                        style: TextStyle(fontSize: 16),
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  ],
                                                                                                                ),
                                                                                                              ],
                                                                                                            ),
                                                                                                          ),
                                                                                                        );
                                                                                                      },
                                                                                                    );
                                                                                                  },
                                                                                                ),
                                                                                              ),
                                                                                            ),
                                                                                          ]
                                                                                        : [])
                                                                                ..addAll(
                                                                                    // 9. Droid Gps Status Marker:
                                                                                    floidDroidStatusState is FloidDroidStatusLoaded
                                                                                        ? [
                                                                                            Marker(
                                                                                              width: 28,
                                                                                              height: 28,
                                                                                              anchorPos: AnchorPos.align(AnchorAlign.center),
                                                                                              point: LatLng(floidDroidStatusState.floidDroidStatus.gpsLatitude, floidDroidStatusState.floidDroidStatus.gpsLongitude),
                                                                                              builder: (ctx) => Container(
                                                                                                child: InkWell(
                                                                                                  splashColor: FloidMapWidget.DROID_OBJECT_COLOR,
                                                                                                  child: MapControllerIcon(
                                                                                                    height: 24,
                                                                                                    width: 24,
                                                                                                    iconChild: Icon(
                                                                                                      FloidMapWidget.DROID_OBJECT_ICON,
                                                                                                      color: FloidMapWidget.DROID_OBJECT_COLOR,
                                                                                                      size: FloidMapWidget.DROID_OBJECT_ICON_SIZE,
                                                                                                    ),
                                                                                                    shadowChild: Icon(
                                                                                                      FloidMapWidget.DROID_OBJECT_ICON,
                                                                                                      color: FloidMapWidget.DROID_OBJECT_SHADOW_COLOR,
                                                                                                      size: FloidMapWidget.DROID_OBJECT_ICON_SIZE,
                                                                                                    ),
                                                                                                  ),
                                                                                                  onTap: () {
                                                                                                    showModalBottomSheet(
                                                                                                      context: context,
                                                                                                      builder: (builder) {
                                                                                                        return Padding(
                                                                                                          padding: const EdgeInsets.all(0.0),
                                                                                                          child: Container(
                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_BACKGROUND_COLOR,
                                                                                                            child: Column(
                                                                                                              crossAxisAlignment: CrossAxisAlignment.center,
                                                                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                                                                              mainAxisSize: MainAxisSize.min,
                                                                                                              children: <Widget>[
                                                                                                                Container(
                                                                                                                  color: FloidMapWidget.BOTTOM_MODAL_HEADER_BACKGROUND_COLOR,
                                                                                                                  child: Row(
                                                                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                    children: <Widget>[
                                                                                                                      Padding(
                                                                                                                        padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                        child: Text(
                                                                                                                          'Droid',
                                                                                                                          style: TextStyle(
                                                                                                                            fontSize: 20,
                                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_TITLE_COLOR,
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ),
                                                                                                                    ],
                                                                                                                  ),
                                                                                                                ),
                                                                                                                Row(
                                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                  children: <Widget>[
                                                                                                                    Text(
                                                                                                                      '${floidDroidStatusState.floidDroidStatus.gpsLatitude}, ${floidDroidStatusState.floidDroidStatus.gpsLongitude}',
                                                                                                                      style: TextStyle(fontSize: 16),
                                                                                                                    ),
                                                                                                                  ],
                                                                                                                ),
                                                                                                              ],
                                                                                                            ),
                                                                                                          ),
                                                                                                        );
                                                                                                      },
                                                                                                    );
                                                                                                  },
                                                                                                ),
                                                                                              ),
                                                                                            ),
                                                                                          ]
                                                                                        : [])
                                                                                ..addAll(
                                                                                    // 10. Status Marker:
                                                                                    floidStatusMarkerState is FloidMapMarkerLoaded
                                                                                        ? [
                                                                                            Marker(
                                                                                              width: 26,
                                                                                              height: 26,
                                                                                              anchorPos: AnchorPos.align(AnchorAlign.center),
                                                                                              point: LatLng(floidStatusMarkerState.floidMapMarker.markerLat, floidStatusMarkerState.floidMapMarker.markerLng),
                                                                                              builder: (ctx) => Container(
                                                                                                child: InkWell(
                                                                                                  splashColor: FloidMapWidget.FLOID_OBJECT_COLOR,
                                                                                                  child:
                                                                                                  Stack(
                                                                                                    children: [
                                                                                                      Icon(FloidMapWidget.FLOID_OBJECT_SHADOW_ICON, color: FloidMapWidget.FLOID_OBJECT_SHADOW_COLOR,
                                                                                                          size: FloidMapWidget.FLOID_OBJECT_ICON_SIZE),
                                                                                                      Transform.rotate(
                                                                                                              angle: dist.Distance.degreeToCompass((floidStatusMarkerState.floidMapMarker as FloidStatusMarker).floidStatus.phs) * (math.pi / 180),
                                                                                                              child: Icon(
                                                                                                                FloidMapWidget.FLOID_OBJECT_ICON,
                                                                                                                color: FloidMapWidget.FLOID_OBJECT_COLOR,
                                                                                                                size: FloidMapWidget.FLOID_OBJECT_ICON_SIZE,
                                                                                                              ),
                                                                                                            ),
                                                                                                    ],
                                                                                                  ),
                                                                                                  onTap: () {
                                                                                                    showModalBottomSheet(
                                                                                                      context: context,
                                                                                                      builder: (builder) {
                                                                                                        return Padding(
                                                                                                          padding: const EdgeInsets.all(0.0),
                                                                                                          child: Container(
                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_BACKGROUND_COLOR,
                                                                                                            child: Column(
                                                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                                                                              mainAxisSize: MainAxisSize.min,
                                                                                                              children: <Widget>[
                                                                                                                Container(
                                                                                                                  color: FloidMapWidget.BOTTOM_MODAL_HEADER_BACKGROUND_COLOR,
                                                                                                                  child: Row(
                                                                                                                    children: <Widget>[
                                                                                                                      Padding(
                                                                                                                        padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                        child: Text(
                                                                                                                          'Floid',
                                                                                                                          style: TextStyle(
                                                                                                                            fontSize: 20,
                                                                                                                            color: FloidMapWidget.BOTTOM_MODAL_TITLE_COLOR,
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ),
                                                                                                                    ],
                                                                                                                  ),
                                                                                                                ),
                                                                                                                Row(
                                                                                                                  children: <Widget>[
                                                                                                                    Padding(
                                                                                                                      padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                                                                                                      child: Text(
                                                                                                                        '(${floidStatusMarkerState.floidMapMarker.markerLat}, ${floidStatusMarkerState.floidMapMarker.markerLng}) - ${floidStatusMarkerState.floidMapMarker.markerElevation}m',
                                                                                                                        style: TextStyle(fontSize: 16),
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  ],
                                                                                                                ),
                                                                                                              ],
                                                                                                            ),
                                                                                                          ),
                                                                                                        );
                                                                                                      },
                                                                                                    );
                                                                                                  },
                                                                                                ),
                                                                                              ),
                                                                                            ),
                                                                                          ]
                                                                                        : []),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                );
                                                              },
                                                            );
                                                          },
                                                        );
                                                      },
                                                    );
                                                  },
                                                );
                                              },
                                            );
                                          },
                                        );
                                      },
                                    );
                                  },
                                );
                              },
                            );
                          },
                        );
                      },
                    );
                  },
                );
              },
            );
          },
        );
      },
    );
  }

  void _addPin(LatLng point, BuildContext buildContext, FloidServerApiClient floidServerApiClient, bool secure, String elevationLocation) async {
    print('adding point at: ' + point.latitude.toString() + ", " + point.longitude.toString());
    FloidPinAlt floidPinAlt = FloidPinAlt(pinName: 'New Pin', pinLat: point.latitude, pinLng: point.longitude, pinHeight: 0, pinHeightAuto: true);
    FloidPinAlt? newFloidPinAlt = await showDialog<FloidPinAlt>(
        context: context,
        builder: (BuildContext context) {
          final _formKey = GlobalKey<FormBuilderState>();
          return PinEditorDialog(formKey: _formKey, floidPinAlt: floidPinAlt);
        });
    if (newFloidPinAlt != null) {
      if (newFloidPinAlt.pinHeightAuto) {
        FloidElevationRequest floidElevationRequest = FloidElevationRequest(
          type: FloidElevationRequest.TYPE_PIN,
          elevationRequestId: -1,
          floidMapMarker: null,
          floidPinAlt: newFloidPinAlt,
          markerId: 0,
        );
        await FloidElevationApiClient(floidServerApiClient: floidServerApiClient).fetchElevation(floidElevationRequest, secure, elevationLocation);
      }
      //ignore: close_sinks
      FloidPinSetBloc floidPinSetBlocLocal = BlocProvider.of<FloidPinSetBloc>(context);
      floidPinSetBlocLocal.add(InsertFloidPin(index: -1, floidPinAlt: newFloidPinAlt));
    }
  }

  void _zoomIn(MapController mapController) {
    if (mapController.zoom < 20) {
      mapController.move(mapController.center, mapController.zoom + 1);
    }
  }

  void _zoomOut(MapController mapController) {
    if (mapController.zoom > 1) {
      mapController.move(mapController.center, mapController.zoom - 1);
    }
  }

  void _zoomMapToMarkers(MapController mapController, List<FloidMapMarker> floidMapMarkers, int height, int width) {
    // Latitude:
    double latMin = 90;
    double latMax = -90;
    // Longitude 1:  (-180 <= l < 180)
    double lngMin1 = 180;
    double lngMax1 = -180;
    // Longitude 2:  (   0 <= l < 360)
    double lngMin2 = 360;
    double lngMax2 = 0;

    // Centers and Zooms (These are stored for reference in case of map changed)
    double latCenter = 0;
    double lngCenter = 0;
    double zoomLevel = 8;
    int markerNumber = 0;
    for (FloidMapMarker floidMapMarker in floidMapMarkers) {
      ++markerNumber;
      try {
        // 1. Adjust Lat:
        // Min
        if (floidMapMarker.markerLat < latMin) {
          latMin = floidMapMarker.markerLat;
        }
        // Max
        if (floidMapMarker.markerLat > latMax) {
          latMax = floidMapMarker.markerLat;
        }
        // 2. Adjust Lng1:
        double pinLng1 = ((floidMapMarker.markerLng + 180) % 360) - 180;
        // Min 1
        if (pinLng1 < lngMin1) {
          lngMin1 = pinLng1;
        }
        // Max 1
        if (pinLng1 > lngMax1) {
          lngMax1 = pinLng1;
        }
        // 2. Adjust Lng2:
        double pinLng2 = ((floidMapMarker.markerLng + 360) % 360);
        // Min 2
        if (pinLng2 < lngMin2) {
          lngMin2 = pinLng2;
        }
        // Max 2
        if (pinLng2 > lngMax2) {
          lngMax2 = pinLng2;
        }
      } catch (e) {
        // Not all markers are FloidPinAlt's, so this catches those and we ignore...
      }
    }
    // No markers?
    if (markerNumber == 0) {
      // Do nothing - we set the defaults
    }
    // Was there only one key?
    else if (markerNumber == 1) {
      // We only had one - it is max, min, etc.
      latCenter = latMin;
      lngCenter = lngMin1;
      zoomLevel = 12;
    } else {
      latCenter = (latMin + latMax) / 2;
//      lngCenter = (lngMin2 + lngMax2) / 2;
      lngCenter = (lngMin1 + lngMax1) / 2;
      zoomLevel = _getBoundsZoomLevel(latMin, latMax, lngMin2, lngMax2, height, width);
    }
    // OK , set the map:
    _centerAndZoomMap(mapController, latCenter, lngCenter, zoomLevel.floor());
  }

  void _centerAndZoomMap(MapController mapController, double latCenter, double lngCenter, int zoomLevel) {
    LatLng latLng = LatLng(latCenter, lngCenter);
    mapController.move(latLng, zoomLevel.toDouble());
  }

  double _getBoundsZoomLevel(double latMin, double latMax, double lngMin, double lngMax, int height, int width) {
    const double WORLD_HEIGHT = 256;
    const double WORLD_WIDTH = 256;
    const double ZOOM_MAX = 21;

    double latFraction = (_latRad(latMax) - _latRad(latMin)) / pi;
    double lngDiff = lngMax - lngMin;
    double lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;
    double latZoom = _zoom(height.toDouble(), WORLD_HEIGHT, latFraction);
    double lngZoom = _zoom(width.toDouble(), WORLD_WIDTH, lngFraction);
    return math.min(latZoom, math.min(lngZoom, ZOOM_MAX));
  }

  double _latRad(double lat) {
    double sinLat = math.sin(lat * pi / 180);
    double radX2 = math.log((1 + sinLat) / (1 - sinLat)) / 2;
    return math.max(math.min(radX2, pi), -pi) / 2;
  }

  double _zoom(double mapPx, double worldPx, double fraction) {
    return (math.log(mapPx / worldPx / fraction) / math.ln2).floor().toDouble();
  }
}
