import 'package:flutter/material.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PopupMenuServerDisplay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final FloidServerRepositoryState floidServerRepositoryState = BlocProvider.of<FloidServerRepositoryBloc>(context).state;
    if(floidServerRepositoryState is FloidServerRepositoryStateHasHost) {
      return
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('CURRENT SERVER:', style: TextStyle(color: Colors.blueAccent, fontSize: 14, fontWeight: FontWeight.bold,),),
            Text('${floidServerRepositoryState.floidServerRepository.floidServerApiClient.rootUrl}',
                style: TextStyle(color: Colors.lightBlueAccent, fontSize: 16, fontWeight: FontWeight.bold)),
          ],
        );
    } else {
      return Text('NULL SERVER');
    }
  }
}
