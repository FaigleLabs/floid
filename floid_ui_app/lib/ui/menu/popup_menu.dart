/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/js/js_proxy.dart';
import 'package:floid_ui_app/ui/keys/keys.dart';
import 'package:floid_ui_app/ui/menu/popup_menu_server_display.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

enum PopupMenuAction { logout, exit, download}

class FloidPopupMenu extends StatefulWidget {
  FloidPopupMenu({Key? key}) : super(key: FloidKeys.extraActionsButton);

  @override
  State<FloidPopupMenu> createState() => _FloidPopupMenu();
}

class _FloidPopupMenu extends State<FloidPopupMenu> {
  static const String DOWNLOAD_APP_FILE_NAME = 'app-full-release.apk';
  static const int DOWNLOAD_APP_FILE_ID = 1;

  @override
  void initState() {
    super.initState();
  }

  _displayLogoutDialog(BuildContext context, AuthenticationBloc authenticationBloc, AuthenticationState authenticationState) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.black,
            title: Text('Logout', style: TextStyle(color:Colors.yellowAccent),),
            actions: <Widget>[
              new TextButton(
               child: new Text('Logout', style: TextStyle(color:Colors.yellowAccent),),
                onPressed: () {
                  // Dispatch a new host setting event:
                  authenticationBloc.add(LoggedOut());
                  Navigator.of(context).pop();
                },
              ),
              new TextButton(
                child: new Text('Cancel', style: TextStyle(color:Colors.yellowAccent),),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  _displayExitDialog(BuildContext context, AuthenticationBloc authenticationBloc, AuthenticationState authenticationState) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.black,
            title: Text('Exit', style: TextStyle(color: Colors.yellowAccent),),
            actions: <Widget>[
              new TextButton(
                child: new Text('Exit', style: TextStyle(color: Colors.yellowAccent),),
                onPressed: () {
                  SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                },
              ),
              new TextButton(
                child: new Text('Cancel', style: TextStyle(color: Colors.yellowAccent)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  _displayDownloadFloidControllerDialog(BuildContext context, AuthenticationBloc authenticationBloc, AuthenticationState authenticationState) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.black,
            title: Text('Download Floid App', style: TextStyle(color:Colors.yellowAccent),),
            actions: <Widget>[
              new TextButton(
                child: new Text('Download', style: TextStyle(color:Colors.yellowAccent),),
                onPressed: () async {
                   final FloidServerApiClient floidServerApiClient = (BlocProvider.of<FloidServerRepositoryBloc>(context).state as FloidServerRepositoryStateHasHost)
                       .floidServerRepository
                       .floidServerApiClient;
                   String reason = '';
                   bool success = false;
                   try {
                     String downloadToken = await floidServerApiClient.fetchDownloadToken(DOWNLOAD_APP_FILE_ID);
                     if (downloadToken.isNotEmpty) {
                       JSProxy().downloadFile(
                           floidServerApiClient.floidServerTrust, DOWNLOAD_APP_FILE_ID, '${floidServerApiClient.downloadUrl}/$DOWNLOAD_APP_FILE_NAME', DOWNLOAD_APP_FILE_NAME, downloadToken);
                     } else {
                       success = false;
                       reason = 'Could not create download token';
                     }
                   } catch(e) {
                     success = false;
                     reason = 'Exception creating download token' + e.toString();
                   }
                  if(!success)
                  {
                    Fluttertoast.showToast(
                      msg: "Download Failed: " + reason,
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 3,
                      backgroundColor: Colors.black87,
                      textColor: Colors.yellowAccent,
                      fontSize: 12.0,
                      webBgColor: "#000000",
                    );
                  }
                  Navigator.of(context).pop();
                },
              ),
              new TextButton(
                child: new Text('Cancel', style: TextStyle(color:Colors.yellowAccent),),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final AuthenticationBloc authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);
    return BlocBuilder<AuthenticationBloc, AuthenticationState>(
      builder: (context, authenticationState) {
        return PopupMenuButton<PopupMenuAction>(
          key: FlutterTodosKeys.extraActionsPopupMenuButton,
          padding: EdgeInsets.all(4),
          position: PopupMenuPosition.under,
          onSelected: (action) {
            switch (action) {
              case PopupMenuAction.download:
                _displayDownloadFloidControllerDialog(context, authenticationBloc, authenticationState);
                break;
              case PopupMenuAction.logout:
                _displayLogoutDialog(context, authenticationBloc, authenticationState);
                break;
              case PopupMenuAction.exit:
                _displayExitDialog(context, authenticationBloc, authenticationState);
                break;
              default:
                break;
            }
          },
          itemBuilder: (BuildContext context) => <PopupMenuItem<PopupMenuAction>>[
            PopupMenuItem<PopupMenuAction>(
              key: FloidKeys.settingsPopupMenu,
              child: PopupMenuServerDisplay(),
              enabled: false,
            ),
            PopupMenuItem<PopupMenuAction>(
              key: FloidKeys.settingsPopupMenu,
              value: PopupMenuAction.download,
              child: Text('Download Floid App'),
            ),
            PopupMenuItem<PopupMenuAction>(
              key: FloidKeys.settingsPopupMenu,
              value: PopupMenuAction.logout,
              child: Text('Logout'),
            ),
            PopupMenuItem<PopupMenuAction>(
              key: FloidKeys.settingsPopupMenu,
              value: PopupMenuAction.exit,
              child: Text('Exit'),
            ),
          ],
        );
      },
    );
  }
}
