/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
// DO NOT EDIT keys.dar ONLY EDIT THE TEMPLATE FILE keys.dart.template
import 'package:flutter/widgets.dart';

class FlutterTodosKeys {
  static final extraActionsPopupMenuButton =
  const Key('__extraActionsPopupMenuButton__');
}

class FloidKeys {
  // Extra Actions
  static final extraActionsButton = const Key('__extraActionsButton__');
  static final settingsPopupMenu = const Key('__popup_menu_settings__');
  static final otherMenu = const Key('__otherMenu__');
  static final floidMap = GlobalKey();
  static final floidMapBoxAccessToken = 'pk.eyJ1IjoiZmFpZ2xlbGFicyIsImEiOiJjazJ3aWZsdzUwZnFvM25scWhnaTR4cGgwIn0.t71Ig5jjp7a2vdVy4_aGEQ';
  static final floidOpenElevationUrl = 'api.open-elevation.com/api/v1/lookup';
  static final floidLocalElevationUrl = '/lookup';
}
