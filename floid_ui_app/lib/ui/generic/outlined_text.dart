/// Places a stroke around text to make it appear outlined
import 'package:flutter/widgets.dart';

class OutlinedText extends StatelessWidget {
  /// Text to display
  final String text;

  /// Original text style (if you weren't outlining)
  ///
  /// Do not specify `color` inside this: use [textColor] instead.
  final TextStyle style;

  /// Text color
  final Color textColor;

  /// Outline stroke color
  final Color strokeColor;

  /// Outline stroke width
  final double strokeWidth;

  const OutlinedText(
      this.text, {
        Key? key,
        this.style = const TextStyle(),
        required this.textColor,
        required this.strokeColor,
        required this.strokeWidth,
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Text(
          text,
          style: style.copyWith(
            foreground: Paint()
              ..strokeWidth = strokeWidth
              ..color = strokeColor
              ..style = PaintingStyle.stroke,
          ),
        ),
        Text(
          text,
          style: style.copyWith(foreground: Paint()..color = textColor),
        ),
      ],
    );
  }
}