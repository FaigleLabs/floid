/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/address/address_proxy.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class SettingsEvent extends Equatable {
  SettingsEvent([List props = const []]) : super();
}

class

FetchSettings extends SettingsEvent {
  FetchSettings() : super([]);

  @override
  List<Object> get props => [];
}

abstract class SettingsState extends Equatable {
  SettingsState([List props = const []]) : super();
}

class SettingsEmpty extends SettingsState {
  @override
  List<Object> get props => [];
}

class SettingsLoading extends SettingsState {
  @override
  List<Object> get props => [];
}

class SettingsLoaded extends SettingsState {
  final String host;
  final int port;
  final bool secure;

  SettingsLoaded({required this.host, required this.port, required this.secure})
      : super([host]);

  @override
  List<Object> get props => [host, port, secure];
}

class SettingsError extends SettingsState {
  @override
  List<Object> get props => [];
}

class HostSettingsEvent extends SettingsEvent {
  final String host;
  final int port;
  final bool secure;

  HostSettingsEvent({required this.host, required this.port, required this.secure})
      : super([host]);

  @override
  List<Object> get props => [host, port, secure];
}

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc() : super(SettingsEmpty()) {
    on<SettingsEvent>((event,emit) async {
      // Load from shared pref:
      if (event is FetchSettings) {
        emit(SettingsLoading());
        AddressProxy addressProxy = AddressProxy();
        String host = await addressProxy.getHost();
        int port = await addressProxy.getPort();
        bool secure = await addressProxy.getSecure();
        emit(SettingsLoaded(
          host: host,
          port: port,
          secure: secure,
        ));
      }
      // Load from user:
      if (event is HostSettingsEvent) {
        bool saveHost = false;
        if (state is SettingsLoaded) {
          SettingsLoaded settingsLoaded = state as SettingsLoaded;
          if (settingsLoaded.host != event.host || settingsLoaded.port != event.port
              || settingsLoaded.secure != event.secure) {
            saveHost = true;
          }
          emit(SettingsEmpty());
        } else {
          saveHost = true;
        }
        if (saveHost) {
          emit(SettingsLoading());
          AddressProxy addressProxy = AddressProxy();
          addressProxy.saveHost(event.host);
          addressProxy.savePort(event.port);
          addressProxy.saveSecure(event.secure);
        }
        emit(SettingsLoaded(
          host: event.host,
          port: event.port,
          secure: event.secure,
        ));
      }
    });
  }
}
