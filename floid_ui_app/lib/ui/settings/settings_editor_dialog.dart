/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/settings/settings_editor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class SettingsEditorDialog extends StatelessWidget {
  const SettingsEditorDialog({
    Key? key,
    required GlobalKey<FormBuilderState> formKey,
    required SettingsEditorSettings settingsEditorSettings,
  })  : _formKey = formKey,
        _settingsEditorSettings = settingsEditorSettings,
        super(key: key);

  final GlobalKey<FormBuilderState> _formKey;
  final SettingsEditorSettings _settingsEditorSettings;

  @override
  Widget build(BuildContext context) {
      return SettingsEditor(formKey: _formKey, settingsEditorSettings: _settingsEditorSettings);
  }
}
