/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/settings/settings_editor.dart';
import 'package:floid_ui_app/ui/settings/settings_editor_dialog.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'settings_bloc.dart';

class Settings extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, settingsState) {
        return ElevatedButton(
          child: Text('Server...', style: TextStyle(color: Colors.white, fontSize: 18),),
          onPressed: () async {
            // ignore: close_sinks
            final settingsBlocLocal = BlocProvider.of<SettingsBloc>(context);
            SettingsEditorSettings? newSettingsEditorSettings = await showDialog<SettingsEditorSettings>(
                context: context,
                builder: (BuildContext context) {
                  final _formKey = GlobalKey<FormBuilderState>();
                  SettingsEditorSettings settingsEditorSettings = SettingsEditorSettings();
                  if (settingsBlocLocal.state is SettingsLoaded) {
                    SettingsLoaded settingsLoaded = settingsBlocLocal.state as SettingsLoaded;
                    settingsEditorSettings.host = settingsLoaded.host;
                    settingsEditorSettings.port = settingsLoaded.port;
                    settingsEditorSettings.secure = settingsLoaded.secure;
                  } else {
                    settingsEditorSettings.host = '';
                    settingsEditorSettings.port = 0;
                    settingsEditorSettings.secure = true;
                  }
                  return SettingsEditorDialog(formKey: _formKey, settingsEditorSettings: settingsEditorSettings,);
                });
            if (newSettingsEditorSettings != null) {
              // Dispatch a host settings event:
              settingsBlocLocal.add(
                  HostSettingsEvent(host: newSettingsEditorSettings.host,
                      port: newSettingsEditorSettings.port,
                      secure: newSettingsEditorSettings.secure));
            }
          },
        );
      },
    );
  }
}
