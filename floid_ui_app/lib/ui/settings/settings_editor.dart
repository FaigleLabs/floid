/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class SettingsEditorSettings {
  late String host;
  late int port;
  late bool secure;
}

class SettingsEditor extends StatelessWidget {
  static const String FLOID_SERVER_HOST = 'floid.faiglelabs.com';
  static const String FLOID_SERVER_PORT = '443';
  static const String LOCAL_SERVER_HOST = '10.0.2.2';
  static const String LOCAL_SERVER_PORT = '8443';

  const SettingsEditor({
    Key? key,
    required GlobalKey<FormBuilderState> formKey,
    required SettingsEditorSettings settingsEditorSettings,
    bool isExecute = false,
  })
      : _formKey = formKey,
        _settingsEditorSettings = settingsEditorSettings,
        super(key: key);

  final GlobalKey<FormBuilderState> _formKey;
  final SettingsEditorSettings _settingsEditorSettings;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.all(12.0),
      backgroundColor: Theme
          .of(context)
          .dialogBackgroundColor,
      content: SingleChildScrollView(
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.always,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(4.0),
                child: Text('Floid Server'),
              ),
              Padding(
                padding: EdgeInsets.all(4.0),
                child: FormBuilderTextField(
                  name: 'host',
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Host',
                  ),
                  initialValue: _settingsEditorSettings.host,
                  onSaved: (value) {
                    final String? fValue = value;
                    if (fValue != null) _settingsEditorSettings.host = fValue;
                  },
                  validator: FormBuilderValidators.minLength(1),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(4.0),
                child: FormBuilderTextField(
                  name: 'port',
                  keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                  decoration: InputDecoration(
                    labelText: 'Port',
                  ),
                  initialValue: _settingsEditorSettings.port.toString(),
                  onSaved: (value) {
                    final String? fValue = value;
                    if (fValue != null) _settingsEditorSettings.port = int.parse(fValue);
                  },
                  validator: FormBuilderValidators.minLength(1),
                ),
              ),
              FormBuilderCheckbox(
                name: 'secure',
                title: new Text('Secure'),
                initialValue: _settingsEditorSettings.secure,
                onSaved: (value) {
                  final bool? fValue = value;
                  if (fValue != null) _settingsEditorSettings.secure = fValue;
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 4, 0),
                    child: ElevatedButton(
                      child: Text('Floid Server'),
                      onPressed: () {
                        FormBuilderState? fCurrentState = _formKey.currentState;
                        fCurrentState?.patchValue({
                          'host': FLOID_SERVER_HOST,
                          'port': FLOID_SERVER_PORT,});
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(4, 0, 0, 0),
                    child: ElevatedButton(
                      child: Text('Local Server'),
                      onPressed: () {
                        FormBuilderState? fCurrentState = _formKey.currentState;
                        fCurrentState?.patchValue({
                          'host': LOCAL_SERVER_HOST,
                          'port': LOCAL_SERVER_PORT,});
                      },
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 24, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 0, 4, 0),
                      child: ElevatedButton(
                        child: Text('Save',),
                        onPressed: () {
                          FormBuilderState? fCurrentState = _formKey.currentState;
                          if (fCurrentState != null && fCurrentState.validate()) {
                            fCurrentState.save();
                            Navigator.pop(context, _settingsEditorSettings);
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(4, 0, 0, 0),
                      child: ElevatedButton(
                        child: Text('Cancel'),
                        onPressed: () {
                          Navigator.pop(context, null);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
