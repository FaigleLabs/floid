/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:flutter/material.dart';
class MapControllerIcon extends StatelessWidget {

  final StatelessWidget iconChild;
  final StatelessWidget shadowChild;
  final double height;
  final double width;

  MapControllerIcon({
    Key? key,
    required this.iconChild,
    required this.shadowChild,
    required this.height,
    required this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child:
      Stack(
        children: <Widget>[
          Positioned(
            left: 1.0,
            top: 1.0,
            child: shadowChild,
          ),
          Positioned(
            left: 1.0,
            top: -1.0,
            child: shadowChild,
          ),
          Positioned(
            left: -1.0,
            top: -1.0,
            child: shadowChild,
          ),
          Positioned(
            left: -1.0,
            top: 1.0,
            child: shadowChild,
          ),
          Positioned(
            left: 0.0,
            top: 1.0,
            child: shadowChild,
          ),
          Positioned(
            left: 1.0,
            top: 0.0,
            child: shadowChild,
          ),
          Positioned(
            left: 0.0,
            top: -1.0,
            child: shadowChild,
          ),
          Positioned(
            left: -1.0,
            top: 0.0,
            child: shadowChild,
          ),
          Positioned(
            left: 0.0,
            top: 0.0,
            child: iconChild,
          ),
        ],
      ),
    );
  }
}