/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:floid_ui_app/ui/controller/floid_errors.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class MissionExecuteController extends StatelessWidget {

  static const double EXECUTE_MISSION_BOX_SIZE = 29;
  static const double EXECUTE_MISSION_ICON_SIZE = 30;
  static const Color EXECUTE_MISSION_SHADOW_COLOR = Color(0xFF4F4F4F);
  static IconData EXECUTE_MISSION_ICON = MdiIcons.playCircle;

  const MissionExecuteController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FloidDroidStatusBloc, FloidDroidStatusState>(
        builder: (context, floidDroidStatusState) {
          return BlocBuilder<FloidAppStateBloc, FloidAppState>(
              builder: (context, floidAppState) {
                FloidErrors? fFloidErrors;
                if (floidAppState is FloidAppStateLoaded) {
                  fFloidErrors =
                      floidAppState.floidAppStateAttributes.floidErrors;
                }
                return BlocBuilder<
                    FloidServerRepositoryBloc,
                    FloidServerRepositoryState>(
                    builder: (context, floidServerRepositoryState) {
                      return Container(
                        padding: EdgeInsets.all(0),
                        child: InkWell(
                          onTap: () {
                            // We need to copy over the bloc providers to the widget tree below...
                            //ignore: close_sinks
                            final FloidMissionBloc floidMissionBlocLocal = BlocProvider
                                .of<FloidMissionBloc>(context);
                            final FloidMissionState floidMissionStateLocal = floidMissionBlocLocal
                                .state;
                            //ignore: close_sinks
                            final FloidPinSetBloc floidPinSetBlocLocal = BlocProvider
                                .of<FloidPinSetBloc>(context);
                            final FloidPinSetState floidPinSetBlocStateLocal = floidPinSetBlocLocal
                                .state;
                            //ignore: close_sinks
                            final FloidIdBloc floidIdBlocLocal = BlocProvider
                                .of<FloidIdBloc>(context);
                            final FloidIdState floidIdBlocStateLocal = floidIdBlocLocal
                                .state;
                            //ignore: close_sinks
                            final FloidAppStateBloc floidAppStateBlocLocal = BlocProvider
                                .of<FloidAppStateBloc>(context);
                            final FloidAppState floidAppStateLocal = floidAppStateBlocLocal
                                .state;

                            //ignore: close_sinks
                            FloidServerRepositoryBloc floidServerRepositoryBlocLocal = BlocProvider
                                .of<FloidServerRepositoryBloc>(context);
                            FloidServerRepositoryState floidServerRepositoryStateLocal = floidServerRepositoryBlocLocal
                                .state;
                            if ((floidAppStateLocal is FloidAppStateLoaded
                                && floidAppStateLocal.floidAppStateAttributes
                                    .floidErrors != null
                                && floidAppStateLocal.floidAppStateAttributes
                                    .floidIsAlive) &&
                                floidServerRepositoryStateLocal is FloidServerRepositoryStateHasHost &&
                                floidMissionStateLocal is FloidMissionLoaded &&
                                floidPinSetBlocStateLocal is FloidPinSetLoaded &&
                                floidIdBlocStateLocal is FloidUuidLoaded) {
                              FloidMissionLoaded floidMissionLoaded = floidMissionStateLocal;
                              FloidPinSetLoaded floidPinSetLoaded = floidPinSetBlocStateLocal;
                              FloidUuidLoaded floidUuidLoaded = floidIdBlocStateLocal;
                              final FloidErrors? fFloidErrors = floidAppStateLocal
                                  .floidAppStateAttributes.floidErrors;
                              if (fFloidErrors != null &&
                                  fFloidErrors.hasCleanSheet()) {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor: Colors.grey[900],
                                        title: Container(
                                          color: Colors.grey[850],
                                          padding: EdgeInsets.fromLTRB(10,10,10,10),
                                          child: Text(floidMissionLoaded.droidMission
                                                  .missionName,
                                            style: TextStyle(color: Colors.yellowAccent),),
                                        ),
                                        titlePadding: EdgeInsets.fromLTRB(0,0,0,0),
                                        content: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text('Floid:' +
                                                floidUuidLoaded.floidIdAndState
                                                    .floidId.toString()),
                                            Text('Pin Set: ' +
                                                floidPinSetLoaded.floidPinSetAlt
                                                    .pinSetName),                                          ],
                                        ),
                                        actions: <Widget>[
                                          TextButton(
                                            style: ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.blue),),
                                            child: Text('Execute',
                                              style: TextStyle(
                                                  color: Colors.yellowAccent),),
                                            onPressed: () {
                                              // Execute the mission event:
                                              floidServerRepositoryStateLocal
                                                  .floidServerRepository
                                                  .executeMission(
                                                  floidUuidLoaded
                                                      .floidIdAndState.floidId,
                                                  floidMissionLoaded
                                                      .droidMission.id,
                                                  floidPinSetLoaded
                                                      .floidPinSetAlt.id);
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          TextButton(
                                            style: ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.blue),),
                                            child: Text('Cancel',
                                              style: TextStyle(
                                                  color: Colors.yellowAccent),),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    });
                              } else if (fFloidErrors != null &&
                                  fFloidErrors.hasOnlyWarnings()) {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        backgroundColor: Colors.grey[900],
                                        title: Container(
                                          color: Colors.grey[850],
                                          padding: EdgeInsets.fromLTRB(10,10,10,10),
                                          child: Text(floidMissionLoaded.droidMission
                                              .missionName,
                                            style: TextStyle(color: Colors.yellowAccent),),
                                        ),
                                        titlePadding: EdgeInsets.fromLTRB(0,0,0,0),
                                        content: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Text('Floid: ' +
                                                floidUuidLoaded.floidIdAndState
                                                    .floidId.toString(),
                                              style: TextStyle(
                                                  color: Colors.yellowAccent),),
                                            Text('Pin set: ' +
                                                floidPinSetLoaded.floidPinSetAlt
                                                    .pinSetName,
                                              style: TextStyle(
                                                  color: Colors.yellowAccent),),
                                          ],
                                        ),
                                        actions: <Widget>[
                                          TextButton(
                                            style: ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.blue),),
                                            child: Text(
                                              'Execute With Warnings',
                                              style: TextStyle(
                                                  color: Colors.yellowAccent),),
                                            onPressed: () {
                                              // Execute the mission event:
                                              floidServerRepositoryStateLocal
                                                  .floidServerRepository
                                                  .executeMission(
                                                  floidUuidLoaded
                                                      .floidIdAndState.floidId,
                                                  floidMissionLoaded
                                                      .droidMission.id,
                                                  floidPinSetLoaded
                                                      .floidPinSetAlt.id);
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                          TextButton(
                                            style: ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.blue),),
                                            child: Text('Cancel',
                                              style: TextStyle(
                                                  color: Colors.yellowAccent),),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    });
                              } else {
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      final FloidErrors? fFloidErrors = floidAppStateLocal
                                          .floidAppStateAttributes.floidErrors;
                                      return AlertDialog(
                                        backgroundColor: Colors.grey[900],
                                        title: Container(
                                          color: Colors.grey[850],
                                          padding: EdgeInsets.fromLTRB(10,10,10,10),
                                          child: Text("FAULT: " + floidMissionLoaded.droidMission
                                              .missionName,
                                            style: TextStyle(color: Colors.yellowAccent),),
                                        ),
                                        titlePadding: EdgeInsets.fromLTRB(0,0,0,0),content: Text('Alive: ' +
                                            (floidAppStateLocal
                                                .floidAppStateAttributes
                                                .floidIsAlive).toString() +
                                            '\nErrors: ' +
                                            (
                                                fFloidErrors != null
                                                    && fFloidErrors.hasErrors())
                                                .toString() +
                                            '\nWarnings: ' +
                                            (
                                                fFloidErrors != null
                                                    &&
                                                    fFloidErrors.hasWarnings())
                                                .toString() +
                                            '\nClean sheet: ' +
                                            (
                                                fFloidErrors != null
                                                    && fFloidErrors
                                                    .hasCleanSheet())
                                                .toString() +
                                            '\nOnly warnings: ' +
                                            (
                                                fFloidErrors != null
                                                    && !fFloidErrors.hasErrors())
                                                .toString(),
                                          style: TextStyle(
                                              color: Colors.yellowAccent),
                                        ),
                                        actions: <Widget>[
                                          TextButton(
                                            style: ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.blue),),
                                            child: Text('Cancel',
                                              style: TextStyle(
                                                  color: Colors.yellowAccent),),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    });
                              }
                            }
                          },
                          child: MapControllerIcon(
                            height: EXECUTE_MISSION_BOX_SIZE,
                            width: EXECUTE_MISSION_BOX_SIZE,
                            iconChild: Icon(
                              EXECUTE_MISSION_ICON,
                              size: EXECUTE_MISSION_ICON_SIZE,
                              color: (floidAppState is FloidAppStateLoaded &&
                                  fFloidErrors != null
                                  && floidAppState.floidAppStateAttributes
                                      .floidIsAlive
                                  && fFloidErrors.hasErrors())
                                  ? ServerStatusController.STATUS_OFF_COLOR
                                  : (floidAppState is FloidAppStateLoaded &&
                                  floidAppState.floidAppStateAttributes
                                      .floidErrors != null
                                  && floidAppState.floidAppStateAttributes
                                      .floidIsAlive
                                  && fFloidErrors != null &&
                                  fFloidErrors.hasWarnings())
                                  ? ServerStatusController.STATUS_ALT_COLOR
                                  : floidAppState is FloidAppStateLoaded &&
                                  floidAppState.floidAppStateAttributes
                                      .floidIsAlive
                                  ? (floidDroidStatusState is FloidDroidStatusLoaded &&
                                  floidDroidStatusState.floidDroidStatus
                                      .currentMode ==
                                      FloidDroidStatus.DROID_MODE_MISSION
                                  ? ServerStatusController.STATUS_ON_COLOR
                                  : ServerStatusController
                                  .STATUS_WRONG_MODE_COLOR)
                                  : ServerStatusController.STATUS_OFF_COLOR,
                            ),
                            shadowChild: Icon(
                              EXECUTE_MISSION_ICON,
                              size: EXECUTE_MISSION_ICON_SIZE,
                              color: EXECUTE_MISSION_SHADOW_COLOR,
                            ),
                          ),
                        ),
                      );
                    });
              });
        });
  }
}
