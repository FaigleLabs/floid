/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:floid_ui_app/ui/dialog/mission_name_editor_dialog.dart';
import 'package:floid_ui_app/ui/page/home/home.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:floid_ui_app/ui/dialog/mission_editor_dialog.dart';
import 'package:floid_ui_app/ui/widgets/instruction_chooser.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:floid_ui_app/common/common.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class MissionController extends StatelessWidget {
  static const Color BOX_COLOR = Color(0x7F2f2f2f);
  static const Color BORDER_DIRTY_COLOR = Color(0xFFFFA726);
  static const Color BORDER_CLEAN_COLOR = Colors.yellowAccent;
  static const Color MODAL_TEXT_COLOR = Colors.yellowAccent;
  static const Color MODAL_BACKGROUND_COLOR = Colors.black;
  static const Color MODAL_TITLE_BACKGROUND_COLOR = Color(0xFF222222);
  static IconData MISSION_ICON = MdiIcons.briefcaseArrowUpDownOutline;
  static IconData EMPTY_ICON = MdiIcons.solid;
  static IconData EDIT_ICON = MdiIcons.pencilOutline;
  static IconData DELETE_ICON = MdiIcons.trashCanOutline;
  static IconData MOVE_UP_ICON = MdiIcons.arrowUpThin;
  static IconData MOVE_DOWN_ICON = MdiIcons.arrowDownThin;
  static IconData NEW_ICON = MdiIcons.plus;

  static const List<String> instructions = [
    'Acquire Home Position',
    'Debug',
    'Delay',
    'Deploy Parachute',
    'Designate',
    'Fly Pattern',
    'Fly To',
    'Height To',
    'Hover',
    'Land',
    'Lift Off',
    'Null',
    'Pan Tilt',
    'Payload',
    'Photo Strobe',
    'Retrieve Logs',
    'Set Parameters',
    'Shut Down Helis',
    'Speaker',
    'Speaker Off',
    'Speaker On',
    'Start Up Helis',
    'Start Video',
    'Stop Designating',
    'Stop Photo Strobe',
    'Stop Video',
    'Take Photo',
    'Turn To',
    'Test',
  ];

  const MissionController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FloidServerRepositoryBloc, FloidServerRepositoryState>(builder: (context, floidServerRepositoryState) {
      // Wrap in a Floid Server provider:
      return BlocBuilder<FloidMissionBloc, FloidMissionState>(builder: (context, floidMissionState) {
        // Floid Mission State:
        return (floidServerRepositoryState is FloidServerRepositoryStateHasHost)
            ? Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(6)),
            color: HomePage.PANEL_BACKGROUND_COLOR,
          ),
          child: InkWell(
            onTap: () {
              //ignore: close_sinks
              FloidMissionBloc floidMissionBlocLocal = BlocProvider.of<FloidMissionBloc>(context);
              //ignore: close_sinks
              FloidPinSetBloc floidPinSetBlocLocal = BlocProvider.of<FloidPinSetBloc>(context);
              //ignore: close_sinks
              FloidAppStateBloc floidAppStateBlocLocal = BlocProvider.of<FloidAppStateBloc>(context);
              //ignore: close_sinks
              FloidMessagesBloc floidMessagesBlocLocal = BlocProvider.of<FloidMessagesBloc>(context);
              if (floidMissionState is FloidMissionLoaded)
                showModalBottomSheet(
                  context: context,
                  builder: (builder) {
                    // Floid App State (Local to dialog!):
                    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
                      bloc: floidAppStateBlocLocal,
                      builder: (context, floidAppStateLocal) {
                        return BlocBuilder<FloidMissionBloc, FloidMissionState>(
                          bloc: floidMissionBlocLocal,
                          builder: (context, floidMissionStateLocal) {
                            return BlocBuilder<FloidPinSetBloc, FloidPinSetState>(
                              bloc: floidPinSetBlocLocal,
                              builder: (context, floidPinSetStateLocal) {
                                return BlocBuilder<FloidMessagesBloc, FloidMessagesState>(
                                  bloc: floidMessagesBlocLocal,
                                  builder: (context, floidMessagesStateLocal) {
                                    int instructionIndex = 0;
                                    return floidMissionStateLocal is FloidMissionLoaded
                                        ? SingleChildScrollView(
                                      padding: const EdgeInsets.all(0),
                                      child: Container(
                                        color: MODAL_BACKGROUND_COLOR,
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              color: MODAL_TITLE_BACKGROUND_COLOR,
                                              child:
                                              InkWell(
                                                child: Text(floidMissionStateLocal.droidMission.missionName, style: TextStyle(fontSize: 24),),
                                                onTap: () async {
                                                  String? newMissionName =
                                                  await showDialog<String>(
                                                      context: context,
                                                      builder: (BuildContext context) {
                                                        final _formKey = GlobalKey<FormBuilderState>();
                                                        return MissionNameEditorDialog(formKey: _formKey,
                                                            missionName: floidMissionStateLocal.droidMission.missionName,
                                                            title: 'Modify Mission Name',
                                                            buttonText: 'Modify');
                                                      });
                                                  if (newMissionName != null) {
                                                    // Add a message:
                                                    floidMessagesBlocLocal.add(AddFloidMessage(message: 'Modifying Mission Name: $newMissionName', noToast: true));
                                                    // Modify Mission Name:
                                                    floidMissionBlocLocal.add(ModifyFloidMissionNameEvent(newMissionName: newMissionName));
                                                  }
                                                },
                                              ),
                                            ),
                                            Container(
                                              color: MODAL_BACKGROUND_COLOR,
                                              child: DataTable(
                                                horizontalMargin: 8,
                                                columnSpacing: 8,
                                                headingRowHeight: 2,
                                                dataRowHeight: 26,
                                                columns: [
                                                  DataColumn(label: Text('', style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 0, fontStyle: FontStyle.italic,))),
                                                  DataColumn(label: Text('', style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 0, fontStyle: FontStyle.italic,))),
                                                  DataColumn(label: Text('', style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 0, fontStyle: FontStyle.italic,))),
                                                ],
                                                rows: floidMissionStateLocal.droidMission.droidInstructions.map((droidInstruction) {
                                                  int currentInstructionIndex = instructionIndex++;
                                                  return DataRow(selected: false, cells: [
                                                    DataCell(
                                                      Row(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: <Widget>[
                                                          currentInstructionIndex > 0
                                                              ? InkWell(
                                                              onTap: () {
                                                                // Move the mission up:
                                                                floidMissionBlocLocal.add(AdjustFloidMission(up: true, index: currentInstructionIndex));
                                                              },
                                                              child: Icon(MOVE_UP_ICON,
                                                                color: MODAL_TEXT_COLOR,
                                                                size: 28,
                                                              ))
                                                              : InkWell(
                                                              onTap: null,
                                                              child: Icon(
                                                                EMPTY_ICON,
                                                                size: 28,
                                                                color: Colors.transparent,
                                                              )),
                                                          currentInstructionIndex < floidMissionStateLocal.droidMission.droidInstructions.length - 1
                                                              ? InkWell(
                                                              onTap: () {
                                                                // Move the mission down:
                                                                floidMissionBlocLocal.add(AdjustFloidMission(up: false, index: currentInstructionIndex));
                                                              },
                                                              child: Icon(MOVE_DOWN_ICON,
                                                                color: MODAL_TEXT_COLOR,
                                                                size: 28,
                                                              ))
                                                              : InkWell(
                                                              onTap: null,
                                                              child: Icon(
                                                                EMPTY_ICON,
                                                                size: 28,
                                                                color: Colors.transparent,
                                                              )),
                                                        ],
                                                      ),
                                                    ),
                                                    DataCell(Text(_instructionPrettyPrint(droidInstruction),
                                                        style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 18, fontWeight: FontWeight.normal))),
                                                    DataCell(
                                                      Row(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: <Widget>[
                                                          // Edit instruction:
                                                          droidInstruction is AcquireHomePositionCommand ||
                                                              droidInstruction is DelayCommand ||
                                                              droidInstruction is DesignateCommand ||
                                                              droidInstruction is FlyToCommand ||
                                                              droidInstruction is HeightToCommand ||
                                                              droidInstruction is HoverCommand ||
                                                              droidInstruction is LiftOffCommand ||
                                                              droidInstruction is PanTiltCommand ||
                                                              droidInstruction is PayloadCommand ||
                                                              droidInstruction is PhotoStrobeCommand ||
                                                              droidInstruction is RetrieveLogsCommand ||
                                                              droidInstruction is SetParametersCommand ||
                                                              droidInstruction is SpeakerCommand ||
                                                              droidInstruction is SpeakerOnCommand ||
                                                              droidInstruction is TurnToCommand
                                                              ? InkWell(
                                                              onTap: () async {
                                                                DroidInstruction? newDroidInstruction = await showDialog<DroidInstruction>(
                                                                    context: context,
                                                                    builder: (BuildContext context) {
                                                                      final _formKey = GlobalKey<FormBuilderState>();
                                                                      FloidPinSetAlt floidPinSetAlt;
                                                                      if (floidPinSetBlocLocal.state is FloidPinSetLoaded) {
                                                                        FloidPinSetLoaded floidPinSetLoaded =
                                                                        floidPinSetBlocLocal.state as FloidPinSetLoaded;
                                                                        floidPinSetAlt = floidPinSetLoaded.floidPinSetAlt;
                                                                      } else {
                                                                        floidPinSetAlt = FloidPinSetAlt();
                                                                      }
                                                                      return MissionEditorDialog(
                                                                          formKey: _formKey,
                                                                          droidInstruction: droidInstruction,
                                                                          floidPinSetAlt: floidPinSetAlt);
                                                                    });
                                                                if (newDroidInstruction != null) {
                                                                  floidMissionBlocLocal.add(ReplaceFloidMissionInstruction(
                                                                      index: currentInstructionIndex, droidInstruction: droidInstruction));
                                                                }
                                                              },
                                                              child: Icon(
                                                                EDIT_ICON,
                                                                color: MODAL_TEXT_COLOR,
                                                                size: 26,
                                                              ))
                                                              : InkWell(
                                                              onTap: null,
                                                              child: Icon(
                                                                EMPTY_ICON,
                                                                color: Colors.transparent,
                                                                size: 26,
                                                              )),
                                                          // Delete:
                                                          InkWell(
                                                              onTap: () {
                                                                // Remove the instruction from the mission:
                                                                floidMissionBlocLocal.add(RemoveFloidMissionInstruction(index: currentInstructionIndex));
                                                              },
                                                              child: Icon(DELETE_ICON,
                                                                color: MODAL_TEXT_COLOR,
                                                                size: 26,
                                                              )),
                                                          InkWell(
                                                            onTap: () async {
                                                              String? newInstruction = await showDialog<String>(
                                                                context: context,
                                                                builder: (BuildContext context) {
                                                                  return InstructionChooser(
                                                                    instructions: instructions,
                                                                  );
                                                                },
                                                              );
                                                              if (newInstruction != null) {
                                                                _handleNewDroidInstruction(newInstruction, currentInstructionIndex, floidMissionBlocLocal);
                                                              }
                                                            },
                                                            child: Icon(
                                                              NEW_ICON,
                                                              size: 28,
                                                              color: MODAL_TEXT_COLOR,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ]);
                                                }).toList()
                                                  ..add(
                                                    // A final '+' (add) at the bottom to insert at end
                                                      DataRow(
                                                        selected: false,
                                                        cells: [
                                                          DataCell(Text('')),
                                                          DataCell(Text('')),
                                                          DataCell(
                                                            Row(
                                                              mainAxisSize: MainAxisSize.min,
                                                              children: <Widget>[
                                                                InkWell(
                                                                    onTap: null,
                                                                    child: Icon(
                                                                      EMPTY_ICON,
                                                                      size: 24,
                                                                      color: Colors.transparent,
                                                                    )),
                                                                InkWell(
                                                                    onTap: null,
                                                                    child: Icon(
                                                                      EMPTY_ICON,
                                                                      size: 24,
                                                                      color: Colors.transparent,
                                                                    )),
                                                                InkWell(
                                                                  onTap: () async {
                                                                    String? newInstruction = await showDialog<String>(
                                                                      context: context,
                                                                      builder: (BuildContext context) {
                                                                        return InstructionChooser(
                                                                          instructions: instructions,
                                                                        );
                                                                      },
                                                                    );
                                                                    if (newInstruction != null) {
                                                                      _handleNewDroidInstruction(newInstruction, -1, floidMissionBlocLocal);
                                                                    }
                                                                  },
                                                                  child: Icon(NEW_ICON,
                                                                    color: MODAL_TEXT_COLOR,
                                                                    size: 24,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      )),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                        : Container();
                                  },
                                );
                              },
                            );
                          },
                        );
                      },
                    );
                  },
                );
            },
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      GestureDetector(
                        child: Column(
                          children: [
                            Icon(
                              MISSION_ICON,
                              size: 26,
                              color: (floidMissionState is FloidMissionLoaded && floidMissionState.dirty) ? Colors.orangeAccent : Colors.yellowAccent,
                            ),
                            if(floidMissionState is FloidMissionLoaded && floidMissionState.dirty)
                              Text('SAVE',
                                style: TextStyle(fontSize: 12, color: Colors.orangeAccent,),
                              ),
                          ],
                        ),
                        onTap: () async {
                          if (floidMissionState is FloidMissionLoaded && floidMissionState.dirty) {
                            Map<String, dynamic> jsonMissionMap = floidMissionState.droidMission.toJson();
                            String jsonMissionString = jsonEncode(jsonMissionMap);
                            int missionId = await floidServerRepositoryState.floidServerRepository.saveMission(jsonMissionString);
                            if (missionId != -1) {
                              // Successful save - turn off the mission dirty flag:
                              BlocProvider.of<FloidMissionBloc>(context).add(SetFloidMissionClean());
                              BlocProvider.of<FloidMessagesBloc>(context).add(
                                  AddFloidMessage(message: 'Mission Saved: ' + floidMissionState.droidMission.missionName));
                            } else {
                              Alert(context: context, title: "Mission Failed to Save",
                                  style: AlertStyle(backgroundColor: Theme
                                      .of(context)
                                      .canvasColor, titleStyle: TextStyle(color: Theme
                                      .of(context)
                                      .splashColor)),
                                  buttons: [
                                    DialogButton(
                                      color: Theme
                                          .of(context)
                                          .highlightColor,
                                      child: Text(
                                        "OK",
                                        style: TextStyle(color: Theme
                                            .of(context)
                                            .splashColor, fontSize: 20),
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                    ),
                                  ]).show();
                            }
                          }
                        },
                      ),
                      Text(floidMissionState is FloidMissionLoaded ? floidMissionState.droidMission.missionName : '',
                          style: TextStyle(fontSize: 18, color: Colors.yellowAccent)),
                      FlightDistanceController(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        )
            : LoadingIndicator();
      });
    });
  }

  void _handleNewDroidInstruction(String newInstruction, int index, FloidMissionBloc floidMissionBlocLocal) {
    DroidInstruction? newDroidInstruction;
    switch (newInstruction) {
      case 'Acquire Home Position':
        newDroidInstruction = AcquireHomePositionCommand();
        break;
      case 'Debug':
        newDroidInstruction = DebugCommand();
        break;
      case 'Delay':
        newDroidInstruction = DelayCommand();
        break;
      case 'Deploy Parachute':
        newDroidInstruction = DeployParachuteCommand();
        break;
      case 'Designate':
        newDroidInstruction = DesignateCommand();
        break;
      case 'Fly Pattern':
        newDroidInstruction = FlyPatternCommand();
        break;
      case 'Fly To':
        newDroidInstruction = FlyToCommand();
        break;
      case 'Height To':
        newDroidInstruction = HeightToCommand();
        break;
      case 'Hover':
        newDroidInstruction = HoverCommand();
        break;
      case 'Land':
        newDroidInstruction = LandCommand();
        break;
      case 'Lift Off':
        newDroidInstruction = LiftOffCommand();
        break;
      case 'Null':
        newDroidInstruction = NullCommand();
        break;
      case 'Pan Tilt':
        newDroidInstruction = PanTiltCommand();
        break;
      case 'Payload':
        newDroidInstruction = PayloadCommand();
        break;
      case 'Photo Strobe':
        newDroidInstruction = PhotoStrobeCommand();
        break;
      case 'Retrieve Logs':
        newDroidInstruction = RetrieveLogsCommand();
        break;
      case 'Set Parameters':
        newDroidInstruction = SetParametersCommand();
        break;
      case 'Shut Down Helis':
        newDroidInstruction = ShutDownHelisCommand();
        break;
      case 'Speaker':
        newDroidInstruction = SpeakerCommand();
        break;
      case 'Speaker Off':
        newDroidInstruction = SpeakerOffCommand();
        break;
      case 'Speaker On':
        newDroidInstruction = SpeakerOnCommand();
        break;
      case 'Start Up Helis':
        newDroidInstruction = StartUpHelisCommand();
        break;
      case 'Start Video':
        newDroidInstruction = StartVideoCommand();
        break;
      case 'Stop Designating':
        newDroidInstruction = StopDesignatingCommand();
        break;
      case 'Stop Photo Strobe':
        newDroidInstruction = StopPhotoStrobeCommand();
        break;
      case 'Stop Video':
        newDroidInstruction = StopVideoCommand();
        break;
      case 'Take Photo':
        newDroidInstruction = TakePhotoCommand();
        break;
      case 'Turn To':
        newDroidInstruction = TurnToCommand();
        break;
      case 'Test':
        newDroidInstruction = TestCommand();
        break;
      default:
        print('Failed to find command' + newInstruction);
        break;
    }
    if (newDroidInstruction != null) {
      // Add a new instruction to the mission:
      floidMissionBlocLocal.add(InsertFloidMissionInstruction(droidInstruction: newDroidInstruction, index: index));
    }
  }
  String _instructionPrettyPrint(DroidInstruction droidInstruction) {
    String instructionPP = droidInstruction.type.substring(droidInstruction.type.lastIndexOf('.') + 1).replaceAll('Command', '');
    if (droidInstruction is AcquireHomePositionCommand) {
      instructionPP = 'Acquire Home Position';
    }
    if (droidInstruction is DelayCommand) {
      instructionPP = instructionPP + ' ' + droidInstruction.delayTime.toString() + 's';
    }
    if (droidInstruction is DesignateCommand) {
      instructionPP = 'Start Designating';
    }
    if (droidInstruction is StopDesignatingCommand) {
      instructionPP = 'Stop Designating';
    }
    if (droidInstruction is FlyToCommand) {
      instructionPP = 'Fly To ${droidInstruction.flyToHome?" Home":" " + droidInstruction.pinName}';
    }
    if (droidInstruction is HeightToCommand) {
      instructionPP = 'Height To ${droidInstruction.z.toString()}m ${droidInstruction.absolute?"absolute":"relative"}';
    }
    if (droidInstruction is HoverCommand) {
      instructionPP = instructionPP + ' ' + droidInstruction.hoverTime.toString() + 's';
    }
    if (droidInstruction is LiftOffCommand) {
      instructionPP = 'Lift Off To ${droidInstruction.z.toString()}m ${droidInstruction.absolute?"absolute":"relative"}';
    }
    if (droidInstruction is PanTiltCommand) {
    }
    if (droidInstruction is PayloadCommand) {
      instructionPP = instructionPP + ' bay: ' + droidInstruction.bay.toString();
    }
    if (droidInstruction is PhotoStrobeCommand) {
      instructionPP = 'Start Photo Strobe';
    }
    if(droidInstruction is RetrieveLogsCommand) {
      instructionPP = 'Retrieve Logs ' + (droidInstruction.logLimit==0?'default':'${droidInstruction.logLimit}' + ' lines');
    }
    if (droidInstruction is StopPhotoStrobeCommand) {
      instructionPP = 'Stop Photo Strobe';
    }
    if (droidInstruction is SetParametersCommand) {
      instructionPP = 'Set Parameters';
    }
    if (droidInstruction is SpeakerCommand) {
      instructionPP = 'Speaker';
    }
    if (droidInstruction is SpeakerOnCommand) {
      instructionPP = 'Speaker On';
    }
    if (droidInstruction is SpeakerOffCommand) {
      instructionPP = 'Speaker Off';
    }
    if (droidInstruction is StartVideoCommand) {
      instructionPP = 'Start Video';
    }
    if (droidInstruction is StopVideoCommand) {
      instructionPP = 'Stop Video';
    }
    if (droidInstruction is StartUpHelisCommand) {
      instructionPP = 'Start Up Helis';
    }
    if (droidInstruction is ShutDownHelisCommand) {
      instructionPP = 'Shut Down Helis';
    }
    if (droidInstruction is TurnToCommand) {
      instructionPP = 'Turn To ' + (droidInstruction.followMode?' Follow Mode':' '+droidInstruction.heading.toString());
    }
    return instructionPP;
  }
}
