/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:floid_ui_app/ui/widgets/floid_map_widget.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class LegendController extends StatelessWidget {
  static const double LEGEND_BUTTON_ICON_BOX_SIZE = 30;
  static const double LEGEND_BUTTON_ICON_SIZE = 30;
  static IconData LEGEND_BUTTON_ICON  = MdiIcons.map;
  static const double LEGEND_ICON_SIZE = 28;
  static const double LEGEND_ICON_SIZE_ALT = 24;
  static const double LEGEND_HEADER_FONT_SIZE = 16;
  static const double LEGEND_FONT_SIZE = 18;
  
  const LegendController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Wrap in a Floid Server provider:
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
      builder: (context, floidAppState) {
        // Floid Mission State:
        return
          Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                InkWell(
                  child:
                  MapControllerIcon(
                    height: LEGEND_BUTTON_ICON_BOX_SIZE,
                    width: LEGEND_BUTTON_ICON_BOX_SIZE,
                    iconChild: Icon(LEGEND_BUTTON_ICON, size: LEGEND_BUTTON_ICON_SIZE, color: Colors.yellowAccent),
                    shadowChild: Icon(
                        LEGEND_BUTTON_ICON, size: LEGEND_BUTTON_ICON_SIZE, color: Colors.grey[800]),
                  ),
                  onTap: () {
                    showModalBottomSheet(
                        context: context,
                        builder: (builder) {
                          return SingleChildScrollView(
                            padding: const EdgeInsets.all(0.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text('Legend', style: TextStyle(fontSize: 24,),),
                                Container(
                                  color: Colors.black,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(12, 4, 0, 0),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Text('Path Elevation', style: TextStyle(fontSize: LEGEND_HEADER_FONT_SIZE,),)
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(MdiIcons.square, color: FloidMapWidget.MISSION_MAP_LINES_COLOR_GOOD,
                                                  size: LEGEND_ICON_SIZE_ALT),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Good', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(MdiIcons.square, color: FloidMapWidget.MISSION_MAP_LINES_COLOR_WARNING,
                                                  size: LEGEND_ICON_SIZE_ALT),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Warning', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(MdiIcons.square, color: FloidMapWidget.MISSION_MAP_LINES_COLOR_ERROR,
                                                  size: LEGEND_ICON_SIZE_ALT),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Error', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),

                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(MdiIcons.square, color: FloidMapWidget.MISSION_MAP_LINES_COLOR_UNKNOWN,
                                                  size: LEGEND_ICON_SIZE_ALT),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Unknown', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),                                        Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                              child: Text('Flight Paths', style: TextStyle(fontSize: LEGEND_HEADER_FONT_SIZE,),),),
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(MdiIcons.square, color: FloidMapWidget.FLOID_GPS_MAP_LINES_COLOR,
                                                  size: LEGEND_ICON_SIZE_ALT),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Floid', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(MdiIcons.square, color: FloidMapWidget.DROID_GPS_MAP_LINES_COLOR,
                                                  size: LEGEND_ICON_SIZE_ALT),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Droid', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                              child: Text('Designator', style: TextStyle(fontSize: LEGEND_HEADER_FONT_SIZE,),),),
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(MdiIcons.square, color: FloidMapWidget.DESIGNATE_MAP_LINES_COLOR,
                                                  size: LEGEND_ICON_SIZE_ALT),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Designate', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                                              child: Text('Objects', style: TextStyle(fontSize: LEGEND_HEADER_FONT_SIZE,),),),
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                                  Icon(FloidMapWidget.FLOID_OBJECT_ICON, color: FloidMapWidget.FLOID_OBJECT_COLOR,
                                                      size: LEGEND_ICON_SIZE),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Floid', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(FloidMapWidget.HOME_OBJECT_ICON, color: FloidMapWidget.HOME_OBJECT_COLOR,
                                                  size: LEGEND_ICON_SIZE),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Home', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(12, 0, 0, 0),
                                          child: Row(
                                            children: <Widget>[
                                              Icon(FloidMapWidget.TARGET_OBJECT_ICON, color: FloidMapWidget.TARGET_OBJECT_COLOR,
                                              size: LEGEND_ICON_SIZE),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(6, 0, 0, 0),
                                                child: Text('Target', style: TextStyle(fontSize: LEGEND_FONT_SIZE,),),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        });
                  },
                ),
              ],
            ),
          );
      },
    );
  }
}
