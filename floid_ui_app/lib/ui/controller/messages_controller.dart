/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class MessagesController extends StatelessWidget {
  static const double MESSAGES_ICON_BOX_SIZE = 30;
  static const double MESSAGES_ICON_SIZE = 30;
  static const Color MESSAGES_ICON_COLOR = Colors.yellowAccent;
  static const Color MESSAGES_ICON_SHADOW_COLOR = Color(0xFF424242);
  // ignore: non_constant_identifier_names
  static IconData MESSAGES_ICON = MdiIcons.cardText;
  static const Color BOX_COLOR = Colors.black;
  static const Color MESSAGES_TEXT_COLOR = Color(0xFFFFFF00);
  static const double MESSAGES_TEXT_SIZE = kIsWeb?16.0:18.0;
  static const Color MESSAGES_BACKGROUND_COLOR = Colors.black;
  static const Color MESSAGES_HEADER_COLOR = Colors.yellowAccent;

  const MessagesController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Wrap in a Floid Server provider:
    return BlocBuilder<FloidMessagesBloc, FloidMessagesState>(
        builder: (context, floidMessagesState) {
          // Floid Mission State:
          return
            Container(
              child: InkWell(
                child:  MapControllerIcon(
                  height: MESSAGES_ICON_BOX_SIZE,
                  width: MESSAGES_ICON_BOX_SIZE,
                  iconChild: Icon(MESSAGES_ICON, size: MESSAGES_ICON_SIZE, color: MESSAGES_ICON_COLOR),
                  shadowChild: Icon(
                      MESSAGES_ICON, size: MESSAGES_ICON_SIZE, color: MESSAGES_ICON_SHADOW_COLOR),
                ),
                onTap: () {
                  // We need to copy over the bloc provider to the widget tree below...
                  //ignore: close_sinks
                  FloidMessagesBloc floidMessagesBlocLocal = BlocProvider.of<FloidMessagesBloc>(context);
                  showModalBottomSheet(
                    context: context,
                    builder: (builder) {
                      // Floid App State (Local to dialog!):
                      return BlocBuilder<FloidMessagesBloc, FloidMessagesState>(
                        bloc: floidMessagesBlocLocal,
                        builder: (context, floidMessagesStateLocal) {
                          // Floid Pin Set State (Local to dialog!):
                          return floidMessagesStateLocal is FloidMessagesLoaded
                              ? SingleChildScrollView(
                            padding: const EdgeInsets.all(0.0),
                            child:
                            GestureDetector(
                              onTap: () {
                                // Copy to clipboard:
                                Clipboard.setData(ClipboardData(text: (floidMessagesStateLocal).messages.join("\n")));
                                // Notify user:
                                Fluttertoast.showToast(
                                  msg: "Copied to clipboard",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER_RIGHT,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Color(0xff222222),
                                  textColor: Colors.yellowAccent,
                                  fontSize: kIsWeb ? 16.0 : 12.0,
                                  webPosition: "center",
                                  webBgColor: "linear-gradient(to right, #333333, #111111)",
                                );
                              },
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text('Messages - Click to copy to clipboard', style: TextStyle(fontSize: 20, color: MESSAGES_HEADER_COLOR,),),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    color: MESSAGES_BACKGROUND_COLOR,
                                    child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children:
                                        floidMessagesStateLocal.messages.map((message) {
                                          return Text(message, style: TextStyle(color: MESSAGES_TEXT_COLOR, fontSize: MESSAGES_TEXT_SIZE),);
                                        }).toList()
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                              : Container();
                        },
                      );
                    },
                  );
                },
              ),
            );
        }
    );
  }
}
