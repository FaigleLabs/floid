/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/controller/server_status_controller.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class FloidHeartbeatStatusController extends StatelessWidget {
  static IconData FLOID_HEARTBEAT_ICON = MdiIcons.heart;
  static IconData FLOID_NO_HEARTBEAT_ICON = MdiIcons.heartOff;
  static IconData FLOID_UNKNOWN_HEARTBEAT_ICON = MdiIcons.heart;
  const FloidHeartbeatStatusController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Wrap in a Floid Server provider:
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
        builder: (context, floidAppState) {
          return BlocBuilder<FloidServerRepositoryBloc, FloidServerRepositoryState>(
              builder: (context, floidServerRepositoryState) {
                return
                  Container(
                    padding: EdgeInsets.all(0),
                    child: Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon((floidAppState is FloidAppStateLoaded) ?
                          floidAppState.floidAppStateAttributes.floidIsAlive ?
                          FLOID_HEARTBEAT_ICON :
                          FLOID_NO_HEARTBEAT_ICON :
                          FLOID_UNKNOWN_HEARTBEAT_ICON,
                            color: (floidAppState is FloidAppStateLoaded) ? floidAppState.floidAppStateAttributes.floidIsAlive ?
                            ServerStatusController.STATUS_ON_COLOR :
                            ServerStatusController.STATUS_OFF_COLOR :
                            ServerStatusController.STATUS_OFF_COLOR,
                            size: 24,
                          ),
                        ],
                      ),
                    ),
                  );
              }
          );
        }
    );
  }
}
