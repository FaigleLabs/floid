/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ServerStatusController extends StatelessWidget {
  static IconData STATUS_ON_ICON = MdiIcons.serverNetwork;
  static IconData STATUS_OFF_ICON = MdiIcons.serverNetworkOff;
  static const Color STATUS_SHADOW_COLOR = Color(0xFF4F4F4F);
  static const double STATUS_BOX_SIZE = 30;
  static const double STATUS_ICON_SIZE = 28;
  static const Color STATUS_OFF_COLOR = Color(0x7FFFFFFF);
  static const Color STATUS_OFF_ALT_COLOR = Colors.red;
  static const Color STATUS_WRONG_MODE_COLOR = Colors.yellowAccent;
  static const Color STATUS_ALT_COLOR = Colors.orangeAccent;
  static const Color STATUS_ON_COLOR = Color(
      0xFF76FF03); //Colors.lightGreenAccent;
  static const Color SERVER_BUTTON_COLOR = Colors.blue;
  static const Color SERVER_BUTTON_TEXT_COLOR = Colors.black;
  static const Color SERVER_BUTTON_DISABLE_COLOR = Color(
      0xFF64B5F6); // Note Color(0xFF64B5F6) = Colors.blue[300]
  static const Color SERVER_BUTTON_DISABLE_TEXT_COLOR = Color(
      0xFF757575); // Note: Color(0xFF757575) = Colors.grey[600]

  const ServerStatusController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Wrap in a Floid Server provider:
    return BlocBuilder<FloidServerStatusBloc, FloidServerStatusState>(
        builder: (context, floidServerStatusState) {
          return BlocBuilder<
              FloidServerRepositoryBloc,
              FloidServerRepositoryState>(
              builder: (context, floidServerRepositoryState) {
                return
                  Container(
                    padding: EdgeInsets.all(0),
                    child: InkWell(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                backgroundColor: Colors.black,
                                title: Text('Floid Server', style: TextStyle(
                                    color: Colors.yellowAccent),),
                                content: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      58, 18, 0, 0),
                                  child: Text(
                                    (floidServerStatusState is FloidServerStatusLoaded
                                        ? floidServerStatusState.floidServerStatus.status: 'UNKNOWN'),
                                    style: TextStyle(fontSize: 24),),
                                ),
                                actions: <Widget>[
                                  new TextButton(
                                    child: new Text(''
                                        'Start Server', style: TextStyle(
                                        color: floidServerStatusState is FloidServerStatusLoaded &&
                                            floidServerStatusState
                                                .floidServerStatus.status ==
                                                FloidServerStatus
                                                    .SERVER_STATUS_RUNNING
                                            ? Colors.grey[600]
                                            : Colors.yellowAccent),),
                                    onPressed: floidServerStatusState is FloidServerStatusLoaded &&
                                        floidServerStatusState.floidServerStatus
                                            .status == FloidServerStatus
                                            .SERVER_STATUS_RUNNING ? null : () {
                                      // Start the server event:
                                      if (floidServerRepositoryState is FloidServerRepositoryStateHasHost) {
                                        floidServerRepositoryState
                                            .floidServerRepository
                                            .startServer();
                                      }
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  new TextButton(
                                    child: new Text('Stop Server',
                                      style: TextStyle(
                                          color: floidServerStatusState is FloidServerStatusLoaded &&
                                              floidServerStatusState
                                                  .floidServerStatus.status !=
                                                  FloidServerStatus
                                                      .SERVER_STATUS_RUNNING
                                              ? Colors.grey[600]
                                              : Colors.yellowAccent),),
                                    onPressed: floidServerStatusState is FloidServerStatusLoaded &&
                                        floidServerStatusState.floidServerStatus
                                            .status != FloidServerStatus
                                            .SERVER_STATUS_RUNNING ? null : () {
                                      // Stop the server event:
                                      if (floidServerRepositoryState is FloidServerRepositoryStateHasHost) {
                                        floidServerRepositoryState
                                            .floidServerRepository.stopServer();
                                      }
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  new TextButton(
                                    child: new Text('Cancel', style: TextStyle(
                                        color: Colors.yellowAccent),),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            }
                        );
                      },
                      child:
                      MapControllerIcon(
                        height: STATUS_BOX_SIZE,
                        width: STATUS_BOX_SIZE,
                        iconChild: Icon(
                          (floidServerStatusState is FloidServerStatusLoaded)
                              ? floidServerStatusState.floidServerStatus
                              .status == FloidServerStatus.SERVER_STATUS_RUNNING
                              ?
                          STATUS_ON_ICON
                              :
                          STATUS_OFF_ICON
                              :
                          STATUS_OFF_ICON,
                          color: (floidServerStatusState is FloidServerStatusLoaded)
                              ? floidServerStatusState.floidServerStatus
                              .status == FloidServerStatus.SERVER_STATUS_RUNNING
                              ?
                          STATUS_ON_COLOR
                              :
                          STATUS_OFF_COLOR
                              :
                          STATUS_OFF_COLOR,
                          size:
                          STATUS_ICON_SIZE,
                        ),
                        shadowChild: Icon(
                          (floidServerStatusState is FloidServerStatusLoaded)
                              ? floidServerStatusState.floidServerStatus
                              .status == FloidServerStatus.SERVER_STATUS_RUNNING
                              ?
                          STATUS_ON_ICON
                              :
                          STATUS_OFF_ICON
                              :
                          STATUS_OFF_ICON,
                          color: STATUS_SHADOW_COLOR,
                          size: STATUS_ICON_SIZE,
                        ),
                      ),
                    ),
                  );
              }
          );
        }
    );
  }
}
