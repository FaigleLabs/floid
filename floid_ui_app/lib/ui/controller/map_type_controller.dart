/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/widgets/map_type_chooser.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'map_controller_icon.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class MapTypeController extends StatelessWidget {
  static const double MAP_TYPE_ICON_BOX_SIZE = 30;
  static const double MAP_TYPE_ICON_SIZE = 30;
  static const Color MAP_TYPE_ICON_COLOR = Colors.yellowAccent;
  static const Color MAP_TYPE_ICON_SHADOW_COLOR = Color(0xFF424242);
  static IconData MAP_TYPE_ICON = MdiIcons.layers;

  const MapTypeController({
    Key? key,
  }) : super(key: key);

  /// We have a new map type, add an event to set this im the app state:
  void _processNewMapType(BuildContext context, String mapType) {
    BlocProvider.of<FloidAppStateBloc>(context).add(
        SetMapTypeAppStateEvent(mapType: mapType));
  }

  @override
  Widget build(BuildContext context) {
    // Wrap in a Floid Server provider:
    return Container(
      child: InkWell(
        child: MapControllerIcon(
          height: MAP_TYPE_ICON_BOX_SIZE,
          width: MAP_TYPE_ICON_BOX_SIZE,
          iconChild: Icon(MAP_TYPE_ICON, size: MAP_TYPE_ICON_SIZE, color: MAP_TYPE_ICON_COLOR),
          shadowChild: Icon(
              MAP_TYPE_ICON, size: MAP_TYPE_ICON_SIZE, color: MAP_TYPE_ICON_SHADOW_COLOR),
        ),
        onTap: () async {
          final FloidAppStateBloc floidAppStateBlocLocal = BlocProvider.of<FloidAppStateBloc>(context);
          String? newMapType = await showDialog<String>(
              context: context,
              builder: (BuildContext context) {
                return BlocBuilder<FloidAppStateBloc, FloidAppState>(
                    bloc: floidAppStateBlocLocal,
                    builder: (context, floidAppStateLocal) {
                      String? currentMapType;
                      if(floidAppStateLocal!=null && floidAppStateLocal is FloidAppStateLoaded) {
                        currentMapType = floidAppStateLocal.floidAppStateAttributes.mapType;
                      }
                      return MapTypeChooser(
                        mapTypes: FloidAppStateAttributes.mapTypes,
                        currentMapType: currentMapType,
                      );
                    });
              });
          if (newMapType != null) {
            _processNewMapType(context, newMapType);
          }
        },
      ),
    );
  }
}
