/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:floid_ui_app/ui/dialog/mission_editor_dialog.dart';
import 'package:floid_ui_app/ui/widgets/instruction_chooser.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class CommandExecuteController extends StatelessWidget {
  static const double COMMAND_EXECUTE_BOX_SIZE = 26;
  static const double COMMAND_EXECUTE_ICON_SIZE = 26;
  static const Color COMMAND_EXECUTE_SHADOW_COLOR = Color(0xFF4F4F4F);
  // ignore: non_constant_identifier_names
  static IconData FLOID_ALIVE_ICON = MdiIcons.quadcopter;

  const CommandExecuteController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Wrap in a Floid Server provider:
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
        builder: (context, floidAppState) {
          return BlocBuilder<
              FloidServerRepositoryBloc,
              FloidServerRepositoryState>(
              builder: (context, floidServerRepositoryState) {
                return Container(
                  padding: EdgeInsets.all(0),
                  child: InkWell(
                    onTap: () async {
                      if (floidAppState is FloidAppStateLoaded &&
                          floidAppState.floidAppStateAttributes.floidIsAlive) {
                        //ignore: close_sinks
                        FloidPinSetBloc floidPinSetBlocLocal = BlocProvider.of<
                            FloidPinSetBloc>(context);
                        //ignore: close_sinks
                        FloidIdBloc floidIdBlocLocal = BlocProvider.of<
                            FloidIdBloc>(context);
                        String? newInstruction = await showDialog<String>(
                          context: context,
                          builder: (BuildContext context) {
                            return InstructionChooser(
                              instructions: MissionController.instructions,
                            );
                          },
                        );
                        if (newInstruction != null) {
                          _handleNewCommandInstruction(
                              context, newInstruction, floidPinSetBlocLocal,
                              floidServerRepositoryState, floidIdBlocLocal);
                        }
                      }
                    },
                    child: MapControllerIcon(
                      height: COMMAND_EXECUTE_BOX_SIZE,
                      width: COMMAND_EXECUTE_BOX_SIZE,
                      iconChild: Icon(floidAppState is FloidAppStateLoaded &&
                          floidAppState.floidAppStateAttributes.floidIsAlive ?
                      FLOID_ALIVE_ICON :
                      FLOID_ALIVE_ICON,
                        color: floidAppState is FloidAppStateLoaded &&
                            floidAppState.floidAppStateAttributes.floidIsAlive ?
                        ServerStatusController.STATUS_ON_COLOR :
                        ServerStatusController.STATUS_OFF_COLOR,
                        size: COMMAND_EXECUTE_ICON_SIZE,
                      ),
                      shadowChild: Icon(floidAppState is FloidAppStateLoaded &&
                          floidAppState.floidAppStateAttributes.floidIsAlive ?
                      FLOID_ALIVE_ICON :
                      FLOID_ALIVE_ICON,
                        color: COMMAND_EXECUTE_SHADOW_COLOR,
                        size: COMMAND_EXECUTE_ICON_SIZE,
                      ),
                    ),
                  ),
                );
              });
        });
  }

  void _handleNewCommandInstruction(BuildContext context, String newInstruction,
      FloidPinSetBloc floidPinSetBlocLocal,
      FloidServerRepositoryState floidServerRepositoryState,
      FloidIdBloc floidIdBloc) async {
    DroidInstruction? initialDroidInstruction;
    final floidIdBlocState = floidIdBloc.state;
    switch (newInstruction) {
      case 'Acquire Home Position':
        initialDroidInstruction = AcquireHomePositionCommand();
        break;
      case 'Debug':
        initialDroidInstruction = DebugCommand();
        break;
      case 'Delay':
        initialDroidInstruction = DelayCommand();
        break;
      case 'Deploy Parachute':
        initialDroidInstruction = DeployParachuteCommand();
        break;
      case 'Designate':
        initialDroidInstruction = DesignateCommand();
        break;
      case 'Fly Pattern':
        initialDroidInstruction = FlyPatternCommand();
        break;
      case 'Fly To':
        initialDroidInstruction = FlyToCommand();
        break;
      case 'Height To':
        initialDroidInstruction = HeightToCommand();
        break;
      case 'Hover':
        initialDroidInstruction = HoverCommand();
        break;
      case 'Land':
        initialDroidInstruction = LandCommand();
        break;
      case 'Lift Off':
        initialDroidInstruction = LiftOffCommand();
        break;
      case 'Null':
        initialDroidInstruction = NullCommand();
        break;
      case 'Pan Tilt':
        initialDroidInstruction = PanTiltCommand();
        break;
      case 'Payload':
        initialDroidInstruction = PayloadCommand();
        break;
      case 'Photo Strobe':
        initialDroidInstruction = PhotoStrobeCommand();
        break;
      case 'Retrieve Logs':
        initialDroidInstruction = RetrieveLogsCommand();
        break;
      case 'Set Parameters':
        initialDroidInstruction = SetParametersCommand();
        break;
      case 'Shut Down Helis':
        initialDroidInstruction = ShutDownHelisCommand();
        break;
      case 'Speaker':
        initialDroidInstruction = SpeakerCommand();
        break;
      case 'Speaker Off':
        initialDroidInstruction = SpeakerOffCommand();
        break;
      case 'Speaker On':
        initialDroidInstruction = SpeakerOnCommand();
        break;
      case 'Start Up Helis':
        initialDroidInstruction = StartUpHelisCommand();
        break;
      case 'Start Video':
        initialDroidInstruction = StartVideoCommand();
        break;
      case 'Stop Designating':
        initialDroidInstruction = StopDesignatingCommand();
        break;
      case 'Stop Photo Strobe':
        initialDroidInstruction = StopPhotoStrobeCommand();
        break;
      case 'Stop Video':
        initialDroidInstruction = StopVideoCommand();
        break;
      case 'Take Photo':
        initialDroidInstruction = TakePhotoCommand();
        break;
      case 'Turn To':
        initialDroidInstruction = TurnToCommand();
        break;
      case 'Test':
        initialDroidInstruction = TestCommand();
        break;
      default:
        print('Failed to find command' + newInstruction);
        break;
    }
    final DroidInstruction? fInitialDroidInstruction = initialDroidInstruction;
    if ((fInitialDroidInstruction != null) &&
        (floidIdBlocState is FloidUuidLoaded)) {
      FloidUuidLoaded floidUuidLoaded = floidIdBlocState;
      DroidInstruction? newDroidInstruction = await showDialog<
          DroidInstruction>(
          context: context,
          builder: (BuildContext context) {
            final _formKey = GlobalKey<FormBuilderState>();
            FloidPinSetAlt floidPinSetAlt;
            if (floidPinSetBlocLocal.state is FloidPinSetLoaded) {
              FloidPinSetLoaded floidPinSetLoaded = floidPinSetBlocLocal
                  .state as FloidPinSetLoaded;
              floidPinSetAlt = floidPinSetLoaded.floidPinSetAlt;
            } else {
              floidPinSetAlt = FloidPinSetAlt();
            }
            return MissionEditorDialog(formKey: _formKey,
              droidInstruction: fInitialDroidInstruction,
              floidPinSetAlt: floidPinSetAlt,
              isExecute: true,
            );
          });
      if (newDroidInstruction != null) {
        if (floidServerRepositoryState is FloidServerRepositoryStateHasHost) {
          FloidPinSetAlt floidPinSetAlt;
          if (floidPinSetBlocLocal.state is FloidPinSetLoaded) {
            FloidPinSetLoaded floidPinSetLoaded = floidPinSetBlocLocal
                .state as FloidPinSetLoaded;
            floidPinSetAlt = floidPinSetLoaded.floidPinSetAlt;
          } else {
            floidPinSetAlt = FloidPinSetAlt();
          }
          floidServerRepositoryState.floidServerRepository.performCommand(
              floidUuidLoaded.floidIdAndState.floidId, newDroidInstruction,
              floidPinSetAlt);
        }
      }
    }
  }
}
