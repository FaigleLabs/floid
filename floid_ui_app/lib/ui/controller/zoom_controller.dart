/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class
ZoomController extends StatelessWidget {
  static const double ZOOM_ICON_BOX_SIZE = 30;
  static const double ZOOM_ICON_SIZE = 30;
  static const Color BOX_COLOR = Colors.black;
  static const Color ZOOM_ICON_COLOR = Colors.yellowAccent;
  static const Color ZOOM_ICON_SHADOW_COLOR = Color(0xff212121);
  static IconData ZOOM_IN_ICON = MdiIcons.plusCircle;
  static IconData ZOOM_OUT_ICON = MdiIcons.minusCircle;
  static IconData ZOOM_TO_PINS_ICON = MdiIcons.binoculars;

  static const double ZOOM_TO_PINS_ICON_SIZE = 32;
  static const Color ZOOM_TO_PINS_ICON_COLOR = Colors.yellowAccent;
  static const Color ZOOM_TO_PINS_ICON_SHADOW_COLOR = Color(0xff212121);

  static const int ZOOM_IN = 0;
  static const int ZOOM_OUT = 1;
  static const int ZOOM_TO_PINS = 2;
  final int zoom;

  ZoomController({
    Key? key,
    required this.zoom,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Wrap in a Floid Server provider:
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
      builder: (context, floidAppState) {
        // Floid Mission State:
        return
          Container(
            child: InkWell(
              child:
              MapControllerIcon(
                height: ZOOM_ICON_BOX_SIZE,
                width: ZOOM_ICON_BOX_SIZE,
                iconChild: zoom==ZOOM_IN ? Icon(ZOOM_IN_ICON, size: ZOOM_ICON_SIZE, color: ZOOM_ICON_COLOR)
                    : zoom==ZOOM_OUT ? Icon(ZOOM_OUT_ICON, size: ZOOM_ICON_SIZE, color: ZOOM_ICON_COLOR)
                    : Icon(ZOOM_TO_PINS_ICON, size: ZOOM_TO_PINS_ICON_SIZE, color: ZOOM_TO_PINS_ICON_COLOR),
                shadowChild: zoom==ZOOM_IN ? Icon(
                    ZOOM_IN_ICON, size: ZOOM_ICON_SIZE, color: ZOOM_ICON_SHADOW_COLOR)
                    : zoom==ZOOM_OUT ? Icon(ZOOM_OUT_ICON, size: ZOOM_ICON_SIZE,
                    color: ZOOM_ICON_SHADOW_COLOR) : Icon(ZOOM_TO_PINS_ICON, size: ZOOM_TO_PINS_ICON_SIZE,
                    color: ZOOM_TO_PINS_ICON_SHADOW_COLOR) ,
              ),
              onTap: () {
                if (floidAppState is FloidAppStateLoaded) {
                  if (zoom==ZOOM_IN) {
                    BlocProvider.of<FloidAppStateBloc>(context).add(
                        ZoomInEvent());
                  } else if (zoom==ZOOM_OUT) {
                    BlocProvider.of<FloidAppStateBloc>(context).add(
                        ZoomOutEvent());
                  } else {
                    BlocProvider.of<FloidAppStateBloc>(context).add(
                        ZoomToPinsAppStateEvent());
                  }
                }
              },
            ),
          );
      },
    );
  }
}
