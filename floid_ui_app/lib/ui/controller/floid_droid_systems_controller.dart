/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/page/home/floid_icon_decoration.dart';
import 'package:floid_ui_app/ui/page/home/floid_text_decoration.dart';
import 'package:floid_ui_app/ui/page/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class FloidDroidSystemsController extends StatelessWidget {
  static const Color BORDER_ON_COLOR = Colors.yellowAccent;
  static const Color BORDER_OFF_COLOR = Colors.orange;
  static const Color TITLE_BOX_COLOR = Colors.black;
  static const Color TITLE_BOX_SHADOW_COLOR = Colors.black;
  static const Color ICON_ON_COLOR = Colors.yellowAccent;
  static const Color ICON_OFF_COLOR = Colors.orange;
  static const Color LINES_ON_COLOR = Colors.yellowAccent;
  static const Color LINES_OFF_COLOR = Colors.orange;
  static IconData DROID_ICON = MdiIcons.cellphone;
  static IconData VIEW_DROID_GPS_LINES_ICON = MdiIcons.mapMarkerRadiusOutline;
  static IconData FLOID_ICON = MdiIcons.quadcopter;
  static IconData SERVER_ICON = MdiIcons.networkOutline;
  static IconData HARDWARE_ERROR_ICON = MdiIcons.flashAlertOutline;
  static IconData MISSION_ICON = MdiIcons.briefcaseArrowUpDownOutline;
  static IconData RUNNING_ICON = MdiIcons.codeBraces;
  static IconData STATUS_NORMAL_ICON = MdiIcons.timerOutline;
  static IconData STATUS_ERROR_ICON = MdiIcons.timerOffOutline;
  static IconData HOME_ICON = MdiIcons.homeOutline;
  static IconData HELIS_ICON = MdiIcons.power;
  static IconData IN_AIR_ICON = MdiIcons.arrowUpThin;
  static IconData ON_GROUND_ICON = MdiIcons.arrowUpThin;
  static const double SECTION_LABEL_FONT_SIZE = 10;

  const FloidDroidSystemsController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
      builder: (context, floidAppState) {
        return BlocBuilder<FloidDroidStatusBloc, FloidDroidStatusState>(
          builder: (context, floidDroidStatusState) {
            // Floid Mission List State:
            return GestureDetector(
              onTap: () {
                if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.floidDroidSystemsInfoOpen) {
                  // We should close:
                  BlocProvider.of<FloidAppStateBloc>(context).add(CloseFloidDroidSystemsEvent());
                } else {
                  // We should open:
                  BlocProvider.of<FloidAppStateBloc>(context).add(OpenFloidDroidSystemsEvent());
                }
              },
              child: Container(
                color: Colors.transparent,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(4.0,2.0,4.0,2.0),
                          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(6)),
                            color: HomePage.PANEL_BACKGROUND_COLOR,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Icon(
                                DROID_ICON,
                                size: 28,
                                color: (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.floidIsAlive) ?
                                ICON_ON_COLOR :
                                ICON_OFF_COLOR,),
                              GestureDetector(
                                onTap: () {
                                  if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.showDroidGpsLines) {
                                    // We should close:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(HideDroidGpsLinesEvent());
                                  } else {
                                    // We should open:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(ShowDroidGpsLinesEvent());
                                  }
                                },
                                child: Icon(
                                  VIEW_DROID_GPS_LINES_ICON,
                                  size: 26,
                                  color: floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.showDroidGpsLines?LINES_ON_COLOR:LINES_OFF_COLOR,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      if(floidDroidStatusState is FloidDroidStatusLoaded && floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.floidDroidSystemsInfoOpen)
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0.0,2.0,0.0,0.0),
                          child: Container(
                            decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8.0)),
                            color: Color(0xCF000000),
                          ),
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      FloidTextDecoration(subLabel: floidDroidStatusState.floidDroidStatus.statusTime.toString(), label: '  TIME  '),
                                      FloidTextDecoration(subLabel: floidDroidStatusState.floidDroidStatus.missionNumber!=DroidMission.NO_MISSION?floidDroidStatusState.floidDroidStatus.missionNumber.toString():'NONE', label: 'MISSION'),
                                      FloidTextDecoration(subLabel: floidDroidStatusState.floidDroidStatus.photoNumber.toString(), label: ' PHOTO '),
                                      FloidTextDecoration(subLabel: floidDroidStatusState.floidDroidStatus.videoNumber.toString(), label: ' VIDEO '),
                                    ],
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(0,2,0,1),
                                    child: Container(
                                      color: Color(0xFFFFFF00),
                                      width: 176,
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.all(0.0),
                                            child: Text('DROID SYSTEMS', style: TextStyle(color:Colors.black, fontSize: SECTION_LABEL_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),

                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      FloidIconDecoration(label: 'SERVER', subLabel: floidDroidStatusState.floidDroidStatus.serverConnected?'ONLINE':'OFFLINE', iconData: SERVER_ICON, truthiness: floidDroidStatusState.floidDroidStatus.serverConnected),
                                      FloidIconDecoration(label: 'FLOID', subLabel: floidDroidStatusState.floidDroidStatus.floidConnected?'ONLINE':'OFFLINE', iconData: FLOID_ICON, truthiness: floidDroidStatusState.floidDroidStatus.floidConnected),  // floid connected
                                      FloidIconDecoration(label: 'HARDWARE', subLabel: !floidDroidStatusState.floidDroidStatus.floidConnected?'UNKNOWN':floidDroidStatusState.floidDroidStatus.floidError?'ERROR':'  OK  ', iconData: HARDWARE_ERROR_ICON, truthiness: !floidDroidStatusState.floidDroidStatus.floidError),  // floid error
                                    ],
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      FloidIconDecoration(label: '  MODE  ', subLabel: floidDroidStatusState.floidDroidStatus.currentMode == FloidDroidStatus.DROID_MODE_MISSION?'MISSION':' TEST  ', iconData: MISSION_ICON, truthiness: floidDroidStatusState.floidDroidStatus.currentMode == FloidDroidStatus.DROID_MODE_MISSION),   // currentMode,),
                                      FloidIconDecoration(label: 'RUNNING', subLabel: floidDroidStatusState.floidDroidStatus.droidStarted?' YES ':' NO ', iconData: RUNNING_ICON, truthiness: floidDroidStatusState.floidDroidStatus.droidStarted), // droidStarted
                                      FloidIconDecoration(label: ' STATUS ', subLabel: floidDroidStatusState.floidDroidStatus.currentStatus == FloidDroidStatus.DROID_STATUS_ERROR?'ERROR'
                                          :floidDroidStatusState.floidDroidStatus.currentStatus == FloidDroidStatus.DROID_STATUS_PROCESSING?'RUN':'IDLE',
                                          iconData: floidDroidStatusState.floidDroidStatus.currentStatus != FloidDroidStatus.DROID_STATUS_ERROR ? STATUS_NORMAL_ICON : STATUS_ERROR_ICON,
                                          truthiness: floidDroidStatusState.floidDroidStatus.currentStatus!=FloidDroidStatus.DROID_STATUS_IDLE),   // currentStatus
                                    ],
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      FloidIconDecoration(label: 'HOME', subLabel: floidDroidStatusState.floidDroidStatus.homePositionAcquired?'YES':' NO ', iconData: HOME_ICON, truthiness: floidDroidStatusState.floidDroidStatus.homePositionAcquired),  // home
                                      FloidIconDecoration(label: 'HELIS', subLabel: floidDroidStatusState.floidDroidStatus.helisStarted?' ON ':'OFF ', iconData: HELIS_ICON, truthiness: floidDroidStatusState.floidDroidStatus.helisStarted),  // helis
                                      FloidIconDecoration(label: 'IN AIR', subLabel: floidDroidStatusState.floidDroidStatus.liftedOff?' YES ':' NO ',  iconData: floidDroidStatusState.floidDroidStatus.liftedOff ? IN_AIR_ICON : ON_GROUND_ICON, truthiness: floidDroidStatusState.floidDroidStatus.liftedOff),  // in-air
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
}
