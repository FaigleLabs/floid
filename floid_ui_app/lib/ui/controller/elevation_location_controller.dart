/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/widgets/elevation_location_chooser.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'map_controller_icon.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ElevationLocationController extends StatelessWidget {
  static const double ELEVATION_LOCATION_ICON_BOX_SIZE = 30;
  static const double ELEVATION_LOCATION_ICON_SIZE = 30;
  static const Color ELEVATION_LOCATION_ICON_COLOR = Colors.yellowAccent;
  static const Color ELEVATION_LOCATION_ICON_SHADOW_COLOR = Color(0xFF424242);
  static IconData ELEVATION_LOCATION_ICON = MdiIcons.elevationDecline;

  const ElevationLocationController({
    Key? key,
  }) : super(key: key);

  /// We have a new map type, add an event to set this im the app state:
  void _processNewElevationLocation(BuildContext context, String elevationLocation) {
    BlocProvider.of<FloidAppStateBloc>(context).add(
        SetElevationLocationAppStateEvent(elevationLocation: elevationLocation));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
        child: MapControllerIcon(
          height: ELEVATION_LOCATION_ICON_BOX_SIZE,
          width: ELEVATION_LOCATION_ICON_BOX_SIZE,
          iconChild: Icon(ELEVATION_LOCATION_ICON, size: ELEVATION_LOCATION_ICON_SIZE, color: ELEVATION_LOCATION_ICON_COLOR),
          shadowChild: Icon(
              ELEVATION_LOCATION_ICON, size: ELEVATION_LOCATION_ICON_SIZE, color: ELEVATION_LOCATION_ICON_SHADOW_COLOR),
        ),
        onTap: () async {
          final FloidAppStateBloc floidAppStateBlocLocal = BlocProvider.of<FloidAppStateBloc>(context);
          String? newElevationLocation = await showDialog<String>(
              context: context,
              builder: (BuildContext context) {
                  return BlocBuilder<FloidAppStateBloc, FloidAppState>(
                      bloc: floidAppStateBlocLocal,
                      builder: (context, floidAppStateLocal) {
                    String? currentElevationLocation;
                    if(floidAppStateLocal!=null && floidAppStateLocal is FloidAppStateLoaded) {
                      currentElevationLocation = floidAppStateLocal.floidAppStateAttributes.elevationLocation;
                    }
                  return ElevationLocationChooser(
                    elevationLocations: FloidAppStateAttributes.elevationLocations,
                    currentElevationLocation: currentElevationLocation,
                  );
                });

              }
          );

          if (newElevationLocation != null) {
            _processNewElevationLocation(context, newElevationLocation);
          }
        },
      ),
    );
  }
}
