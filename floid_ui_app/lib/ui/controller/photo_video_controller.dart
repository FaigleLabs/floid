/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/common/common.dart';
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/bloc/stomp/floid_droid_photo_bloc.dart';
import 'package:floid_ui_app/floidserver/bloc/stomp/floid_droid_video_frame_bloc.dart';
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:floid_ui_app/ui/page/home/home.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class PhotoVideoController extends StatelessWidget {
  static IconData PHOTO_ICON = MdiIcons.camera;
  static IconData VIDEO_ICON = MdiIcons.video;
  static IconData ROTATE_ICON = MdiIcons.rotateRight;
  static IconData ENLARGE_ICON = MdiIcons.arrowTopRight;
  static IconData SHRINK_ICON = MdiIcons.arrowBottomLeft;
  static const Color ICON_COLOR = Colors.yellowAccent;
  static const Color ICON_SHADOW_COLOR = Colors.black;
  static const Color BORDER_COLOR = Color(0xFFFFEE58); // Colors.yellow[400];
  static const double ICON_SIZE = 28;
  static const double PHOTO_PANE_WIDTH_WEB = 320;
  static const double PHOTO_PANE_HEIGHT_WEB = 240;
  static const double PHOTO_PANE_WIDTH_APP = 140;
  static const double PHOTO_PANE_HEIGHT_APP = 105;
  static const double PHOTO_PANE_WIDTH_FULL_WEB = 640;
  static const double PHOTO_PANE_HEIGHT_FULL_WEB = 480;
  static const double PHOTO_PANE_WIDTH_FULL_APP = 320;
  static const double PHOTO_PANE_HEIGHT_FULL_APP = 240;
  static const double VIDEO_PANE_WIDTH_WEB = 320;
  static const double VIDEO_PANE_HEIGHT_WEB = 240;
  static const double VIDEO_PANE_WIDTH_APP = 140;
  static const double VIDEO_PANE_HEIGHT_APP = 105;
  static const double VIDEO_PANE_WIDTH_FULL_WEB = 640;
  static const double VIDEO_PANE_HEIGHT_FULL_WEB = 480;
  static const double VIDEO_PANE_WIDTH_FULL_APP = 320;
  static const double VIDEO_PANE_HEIGHT_FULL_APP = 240;

  const PhotoVideoController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
      builder: (context, floidAppState) {
        return BlocBuilder<FloidDroidPhotoBloc, FloidDroidPhotoState>(
          builder: (context, floidDroidPhotoState) {
            return BlocBuilder<FloidDroidVideoFrameBloc, FloidDroidVideoFrameState>(
              builder: (context, floidDroidVideoFrameState) {
                // Floid Mission List State:
                return Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 4, 4),
                  child: Container(
                    padding: EdgeInsets.fromLTRB(4, 0, 2, 0),
                    decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(6)),
                      color: HomePage.PANEL_BACKGROUND_COLOR,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            if(floidAppState is FloidAppStateLoaded)
                              GestureDetector(
                                onTap: () {
                                  if (floidAppState.floidAppStateAttributes.isPhotoOpen()) {
                                    // We should close:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(ClosePhotoEvent());
                                  } else {
                                    // We should open:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(OpenPhotoEvent());
                                  }
                                },
                                child: MapControllerIcon(
                                  iconChild: Icon(
                                    PHOTO_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_COLOR,
                                  ),
                                  shadowChild: Icon(
                                    PHOTO_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_SHADOW_COLOR,
                                  ),
                                  height: ICON_SIZE,
                                  width: ICON_SIZE,
                                ),
                              ),
                            if(floidAppState is FloidAppStateLoaded)
                              GestureDetector(
                                onTap: () {
                                  if (floidAppState.floidAppStateAttributes.isVideoOpen()) {
                                    // We should close:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(CloseVideoEvent());
                                  } else {
                                    // We should open:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(OpenVideoEvent());
                                  }
                                },
                                child: MapControllerIcon(
                                  iconChild: Icon(
                                    VIDEO_ICON,
                                    size: ICON_SIZE + 4,
                                    color: ICON_COLOR,
                                  ),
                                  shadowChild: Icon(
                                    VIDEO_ICON,
                                    size: ICON_SIZE + 4,
                                    color: ICON_SHADOW_COLOR,
                                  ),
                                  height: ICON_SIZE + 4,
                                  width: ICON_SIZE + 4,
                                ),
                              ),
                            if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.isPhotoOpen())
                              GestureDetector(
                                onTap: () {
                                  BlocProvider.of<FloidAppStateBloc>(context).add(RotatePhotoEvent());
                                },
                                child: MapControllerIcon(
                                  iconChild: Icon(
                                    ROTATE_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_COLOR,
                                  ),
                                  shadowChild: Icon(
                                    ROTATE_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_SHADOW_COLOR,
                                  ),
                                  height: ICON_SIZE,
                                  width: ICON_SIZE,
                                ),
                              ),
                            if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.isVideoOpen())
                              GestureDetector(
                                onTap: () {
                                  BlocProvider.of<FloidAppStateBloc>(context).add(RotateVideoFrameEvent());
                                },
                                child: MapControllerIcon(
                                  iconChild: Icon(
                                    ROTATE_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_COLOR,
                                  ),
                                  shadowChild: Icon(
                                    ROTATE_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_SHADOW_COLOR,
                                  ),
                                  height: ICON_SIZE,
                                  width: ICON_SIZE,
                                ),
                              ),
                            if(floidAppState is FloidAppStateLoaded &&
                                (floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_PHOTO_OPEN ||
                                    floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_VIDEO_OPEN))
                              GestureDetector(
                                onTap: () {
                                  if (floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_PHOTO_OPEN) {
                                    // We should open photo to full screen:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(OpenPhotoFullScreenEvent());
                                  } else {
                                    // We should open video to full screen:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(OpenVideoFullScreenEvent());
                                  }
                                },
                                child: MapControllerIcon(
                                  iconChild: Icon(
                                    ENLARGE_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_COLOR,
                                  ),
                                  shadowChild: Icon(
                                    ENLARGE_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_SHADOW_COLOR,
                                  ),
                                  height: ICON_SIZE,
                                  width: ICON_SIZE,
                                ),
                              ),
                            if(floidAppState is FloidAppStateLoaded &&
                                (floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_PHOTO_FULL_SCREEN ||
                                    floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_VIDEO_FULL_SCREEN))
                              GestureDetector(
                                onTap: () {
                                  if (floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_PHOTO_FULL_SCREEN) {
                                    // We should close:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(OpenPhotoEvent());
                                  } else {
                                    // We should open:
                                    BlocProvider.of<FloidAppStateBloc>(context).add(OpenVideoEvent());
                                  }
                                },
                                child: MapControllerIcon(
                                  iconChild: Icon(
                                    SHRINK_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_COLOR,
                                  ),
                                  shadowChild: Icon(
                                    SHRINK_ICON,
                                    size: ICON_SIZE,
                                    color: ICON_SHADOW_COLOR,
                                  ),
                                  height: ICON_SIZE,
                                  width: ICON_SIZE,
                                ),
                              ),
                          ],
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            if(floidAppState is FloidAppStateLoaded &&
                                floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_PHOTO_OPEN)
                              RotatedBox(
                                quarterTurns: floidAppState.floidAppStateAttributes.photoRotation,
                                child: Container(
                                  padding: EdgeInsets.all(0),
                                  width: kIsWeb ? PHOTO_PANE_WIDTH_WEB : PHOTO_PANE_WIDTH_APP,
                                  height: kIsWeb ? PHOTO_PANE_HEIGHT_WEB : PHOTO_PANE_HEIGHT_APP,
                                  child: floidDroidPhotoState is FloidDroidPhotoLoaded
                                      ? Image.memory(floidDroidPhotoState.floidDroidPhoto.imageBytes, fit: BoxFit.fill,)
                                      : LoadingIndicator(),
                                ),
                              ),
                            if(floidAppState is FloidAppStateLoaded &&
                                floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_PHOTO_FULL_SCREEN)
                              RotatedBox(
                                quarterTurns: floidAppState.floidAppStateAttributes.photoRotation,
                                child: Container(
                                  padding: EdgeInsets.all(0),
                                  width: kIsWeb ? PHOTO_PANE_WIDTH_FULL_WEB : PHOTO_PANE_WIDTH_FULL_APP,
                                  height: kIsWeb ? PHOTO_PANE_HEIGHT_FULL_WEB : PHOTO_PANE_HEIGHT_FULL_APP,
                                  child: floidDroidPhotoState is FloidDroidPhotoLoaded
                                      ? Image.memory(floidDroidPhotoState.floidDroidPhoto.imageBytes, fit: BoxFit.fill,)
                                      : LoadingIndicator(),
                                ),
                              ),
                            if(floidAppState is FloidAppStateLoaded &&
                                floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_VIDEO_OPEN)
                              RotatedBox(
                                quarterTurns: floidAppState.floidAppStateAttributes.videoFrameRotation,
                                child: Container(
                                  padding: EdgeInsets.all(0),
                                  width: kIsWeb ? VIDEO_PANE_WIDTH_WEB : VIDEO_PANE_WIDTH_APP,
                                  height: kIsWeb ? VIDEO_PANE_HEIGHT_WEB : VIDEO_PANE_HEIGHT_APP,
                                  child: floidDroidVideoFrameState is FloidDroidVideoFrameLoaded ? Image.memory(
                                      floidDroidVideoFrameState.floidDroidVideoFrame.imageBytes, fit: BoxFit.fill,) : LoadingIndicator(),
                                ),
                              ),
                            if(floidAppState is FloidAppStateLoaded &&
                                floidAppState.floidAppStateAttributes.photoVideoState == FloidAppStateAttributes.PHOTO_VIDEO_STATE_VIDEO_FULL_SCREEN)
                              RotatedBox(
                                quarterTurns: floidAppState.floidAppStateAttributes.videoFrameRotation,
                                child: Container(
                                  padding: EdgeInsets.all(0),
                                  width: kIsWeb ? VIDEO_PANE_WIDTH_FULL_WEB : VIDEO_PANE_WIDTH_FULL_APP,
                                  height: kIsWeb ? VIDEO_PANE_HEIGHT_FULL_WEB : VIDEO_PANE_HEIGHT_FULL_APP,
                                  child: floidDroidVideoFrameState is FloidDroidVideoFrameLoaded ? Image.memory(
                                      floidDroidVideoFrameState.floidDroidVideoFrame.imageBytes, fit: BoxFit.fill,) : LoadingIndicator(),
                                ),
                              ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          },
        );
      },
    );
  }
}
