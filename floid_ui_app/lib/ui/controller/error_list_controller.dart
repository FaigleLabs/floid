/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:floid_ui_app/ui/controller/floid_errors.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ErrorListController extends StatelessWidget {
  static const Color ERROR_SHADOW_COLOR = Color(0xFF4F4F4F);
  static const double ERROR_BOX_SIZE = 30;
  static const double ERROR_ICON_SIZE = 31;
  static const Color ERROR_TEXT_COLOR = Color(0xFFFF0000);
  static const Color WARNING_TEXT_COLOR = Color(0xFFFFa73D);
  static const Color ERROR_BACKGROUND_COLOR = Colors.black;
  static IconData ERROR_ICON = MdiIcons.alertOutline;
  static IconData WARNING_ICON = MdiIcons.alertOutline;
  static IconData OK_ICON = MdiIcons.checkBold;
  static IconData UNKNOWN_ICON = MdiIcons.crosshairsQuestion;

  const ErrorListController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Wrap in a Floid Server provider:
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
        builder: (context, floidAppState) {
          // Floid Mission State:
          final FloidErrors? fFloidErrors = floidAppState is FloidAppStateLoaded?floidAppState.floidAppStateAttributes.floidErrors:null;
         return
            Container(
              padding: EdgeInsets.all(0),
              child: InkWell(
                onTap: () {
                  // We need to copy over the bloc provider to the widget tree below...
                  //ignore: close_sinks
                  FloidAppStateBloc floidAppStateBlocLocal = BlocProvider.of<FloidAppStateBloc>(context);
                  showModalBottomSheet(
                    context: context,
                    builder: (builder) {
                      // Floid App State (Local to dialog!):
                      return BlocBuilder<FloidAppStateBloc, FloidAppState>(
                        bloc: floidAppStateBlocLocal,
                        builder: (context, floidAppStateLocal) {
                          // Floid Pin Set State (Local to dialog!):
                          if(floidAppStateLocal is FloidAppStateLoaded) {
                          final fFloidErrorsLocal = floidAppStateLocal.floidAppStateAttributes.floidErrors;
                          return SingleChildScrollView(
                            padding: const EdgeInsets.all(0.0),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Text('Mission Errors', style: TextStyle(fontSize: 20,),),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  color: ERROR_BACKGROUND_COLOR,
                                  child: DataTable(
                                    horizontalMargin: 8,
                                    columnSpacing: 4,
                                    headingRowHeight: 0,
                                    dataRowHeight: 22,
                                    columns: [
                                      DataColumn(label: Text('', style: TextStyle(color: ERROR_TEXT_COLOR, fontSize: 16))),
                                      // DataColumn(label: Text('', style: TextStyle(color: ERROR_TEXT_COLOR, fontSize: 16))),
                                    ],
                                    rows: fFloidErrorsLocal!=null?fFloidErrorsLocal.errors.map((final floidErrorInstance) {
                                      final String? fExtraInfo  = floidErrorInstance.extraInfo;
                                      final FloidError? fFloidError = floidErrorInstance.floidError;
                                      return DataRow(selected: false,
                                          cells: [
                                        DataCell(
                                                Row(
                                                  children: [
                                                    Text(fFloidError!=null?fFloidError.message:'', style: TextStyle(color: fFloidError!=null&&fFloidError.severity>0?ERROR_TEXT_COLOR:WARNING_TEXT_COLOR, fontSize: 14)),
                                                    Text(((fExtraInfo!=null&&fExtraInfo.length>0)?(fExtraInfo):''), style: TextStyle(color: fFloidError!=null&&fFloidError.severity>0?ERROR_TEXT_COLOR:WARNING_TEXT_COLOR, fontSize: 14)),
                                                  ],
                                                ),
                                              ),
                                      ]);
                                    }).toList() : [],
                                  ),
                                ),
                              ],
                            ),
                          ); } else { return Container(); }
                        },
                      );
                    },
                  );
                },
                child:
                MapControllerIcon(
                  height: ERROR_BOX_SIZE,
                  width: ERROR_BOX_SIZE,
                  iconChild: Icon((fFloidErrors!=null&&fFloidErrors.hasErrors())
                      ? ERROR_ICON
                      : (fFloidErrors!=null&&fFloidErrors.hasWarnings())
                      ? WARNING_ICON
                      : (fFloidErrors==null
                      || floidAppState is !FloidAppStateLoaded) ? UNKNOWN_ICON : OK_ICON,
                    color: (fFloidErrors!=null&&fFloidErrors.hasErrors())
                        ? ServerStatusController.STATUS_OFF_ALT_COLOR
                        : (fFloidErrors!=null&&fFloidErrors.hasWarnings())
                        ? (fFloidErrors.hasOnlyMissionSkipCheck())?ServerStatusController.STATUS_ON_COLOR:ServerStatusController.STATUS_ALT_COLOR
                        : (fFloidErrors==null
                        || floidAppState is !FloidAppStateLoaded)?ServerStatusController.STATUS_OFF_ALT_COLOR:ServerStatusController.STATUS_ON_COLOR,
                    size: ERROR_ICON_SIZE,
                  ),
                  shadowChild: Icon((fFloidErrors!=null&&fFloidErrors.hasErrors())
                      ? ERROR_ICON
                      : (fFloidErrors!=null&&fFloidErrors.hasWarnings())
                      ? WARNING_ICON
                      : (fFloidErrors==null
                      || floidAppState is !FloidAppStateLoaded) ? UNKNOWN_ICON : OK_ICON,
                    color: ERROR_SHADOW_COLOR,
                    size: ERROR_ICON_SIZE,
                  ),
                ),
              ),
            );
        }
    );
  }
}
