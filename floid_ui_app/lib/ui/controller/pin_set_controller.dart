/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/ui/dialog/pin_set_name_editor_dialog.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_elevation_request.dart';
import 'package:floid_ui_app/ui/page/home/home.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:floid_ui_app/ui/dialog/pin_editor_dialog.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:floid_ui_app/common/common.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class
PinSetController extends StatelessWidget {
  static const Color BOX_COLOR = Colors.black;
  static const Color BORDER_DIRTY_COLOR = Colors.orangeAccent;
  static const Color BORDER_CLEAN_COLOR = Colors.yellowAccent;
  static const Color MODAL_TEXT_COLOR = Colors.yellowAccent;
  static const Color MODAL_BACKGROUND_COLOR = Colors.black;
  static const Color MODAL_TITLE_BACKGROUND_COLOR = Color(0xFF222222);
  static const Color PIN_SET_NAME_COLOR = Colors.yellowAccent;

  static IconData EDIT_ICON = MdiIcons.pencilOutline;
  static IconData DELETE_ICON = MdiIcons.trashCanOutline;
  static IconData MOVE_UP_ICON = MdiIcons.arrowUp;
  static IconData MOVE_DOWN_ICON = MdiIcons.arrowDown;
  static IconData EMPTY_ICON = MdiIcons.solid;
  static IconData PIN_SET_ICON = MdiIcons.mapMarker;

  const PinSetController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FloidServerRepositoryBloc, FloidServerRepositoryState>(
        builder: (context, floidServerRepositoryState) {
          // Wrap in a Floid Server provider:
          return BlocBuilder<FloidPinSetBloc, FloidPinSetState>(
              builder: (context, floidPinSetState) {
                // Floid Mission State:
                return (floidServerRepositoryState is FloidServerRepositoryStateHasHost) ?
                Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(6)),
                    color: HomePage.PANEL_BACKGROUND_COLOR,
                  ),
                  child: InkWell(
                    onTap: () {
                      // We need to copy over the bloc provider to the widget tree below...
                      //ignore: close_sinks
                      FloidPinSetBloc floidPinSetBlocLocal = BlocProvider.of<FloidPinSetBloc>(context);
                      //ignore: close_sinks
                      FloidAppStateBloc floidAppStateBlocLocal = BlocProvider.of<FloidAppStateBloc>(context);
                      //ignore: close_sinks
                      FloidMessagesBloc floidMessagesBlocLocal = BlocProvider.of<FloidMessagesBloc>(context);
                      if (floidPinSetState is FloidPinSetLoaded)
                        showModalBottomSheet(
                          context: context,
                          builder: (builder) {
                            // Floid App State (Local to dialog!):
                            return BlocBuilder<FloidAppStateBloc, FloidAppState>(
                              bloc: floidAppStateBlocLocal,
                              builder: (context, floidAppStateLocal) {
                                // Floid Pin Set State (Local to dialog!):
                                return BlocBuilder<FloidPinSetBloc, FloidPinSetState>(
                                  bloc: floidPinSetBlocLocal,
                                  builder: (context, floidPinSetStateLocal) {
                                    return BlocBuilder<FloidMessagesBloc, FloidMessagesState>(
                                      bloc: floidMessagesBlocLocal,
                                      builder: (context, floidMessagesStateLocal) {
                                        int pinIndex = 0;
                                        return floidPinSetStateLocal is FloidPinSetLoaded
                                            ? SingleChildScrollView(
                                          padding: const EdgeInsets.all(0.0),
                                          child: Container(
                                            color: MODAL_BACKGROUND_COLOR,
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  color: MODAL_TITLE_BACKGROUND_COLOR,
                                                  child:
                                                      InkWell(
                                                        child: Text(floidPinSetStateLocal.floidPinSetAlt.pinSetName, style: TextStyle(fontSize: 20),),
                                                        onTap: () async {
                                                          String? newPinSetName =
                                                          await showDialog<String>(
                                                              context: context,
                                                              builder: (BuildContext context) {
                                                                final _formKey = GlobalKey<FormBuilderState>();
                                                                return PinSetNameEditorDialog(formKey: _formKey,
                                                                    pinSetName: floidPinSetStateLocal.floidPinSetAlt.pinSetName,
                                                                    title: 'Modify Pin Set Name',
                                                                    buttonText: 'Modify');
                                                              });
                                                          if (newPinSetName != null) {
                                                            // Add a message:
                                                            floidMessagesBlocLocal.add(AddFloidMessage(message: 'Modifying Pin Set Name: $newPinSetName', noToast: true));
                                                            // Modify Mission Name:
                                                            floidPinSetBlocLocal.add(ModifyFloidPinSetNameEvent(newPinSetName: newPinSetName));
                                                          }
                                                        },
                                                      ),
                                                ),
                                                Container(
                                                  color: MODAL_BACKGROUND_COLOR,
                                                  padding: EdgeInsets.all(0),
                                                  child: DataTable(
                                                    horizontalMargin: 8,
                                                    columnSpacing: 8,
                                                    headingRowHeight: 2,
                                                    dataRowHeight: 28,
                                                    columns: [
                                                      DataColumn(label: Text('', style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 16),)),
                                                      DataColumn(label: Text('', style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 16),)),
                                                      DataColumn(label: Text('', style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 16),)),
                                                      DataColumn(label: Text('', style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 16),)),
                                                      DataColumn(label: Text(' ', style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 16),)),
                                                    ],
                                                    rows: floidPinSetStateLocal.floidPinSetAlt.pinAltList.map((floidPinAlt) {
                                                      int currentPinIndex = pinIndex++;
                                                      return DataRow(selected: false, cells: [
                                                        DataCell(Text(floidPinAlt.pinName+" ", style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 18),)),
                                                        DataCell(
                                                            Text(floidPinAlt.pinLat.toStringAsFixed(2), style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 18),)),
                                                        DataCell(
                                                            Text(floidPinAlt.pinLng.toStringAsFixed(2), style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 18),)),
                                                        DataCell(
                                                            Text(floidPinAlt.pinHeight.toStringAsFixed(0), style: TextStyle(color: MODAL_TEXT_COLOR, fontSize: 18),)),
                                                        DataCell(
                                                          Row(
                                                            mainAxisSize: MainAxisSize.min,
                                                            children: <Widget>[
                                                              InkWell(
                                                                  onTap: () async {
                                                                    FloidPinAlt? newFloidPinAlt = await showDialog<FloidPinAlt>(
                                                                        context: context,
                                                                        builder: (BuildContext context) {
                                                                          final _formKey = GlobalKey<FormBuilderState>();
                                                                          return PinEditorDialog(formKey: _formKey, floidPinAlt: floidPinAlt);
                                                                        });
                                                                    if (newFloidPinAlt != null) {
                                                                      if (newFloidPinAlt.pinHeightAuto) {
                                                                        FloidElevationRequest floidElevationRequest = FloidElevationRequest(
                                                                          type: FloidElevationRequest.TYPE_PIN,
                                                                          elevationRequestId: -1,
                                                                          floidMapMarker: null,
                                                                          floidPinAlt: newFloidPinAlt,
                                                                          markerId: 0,);
                                                                        FloidServerRepositoryState floidServerRepositoryState = BlocProvider
                                                                            .of<FloidServerRepositoryBloc>(context)
                                                                            .state;
                                                                        if (floidServerRepositoryState is FloidServerRepositoryStateHasHost && floidAppStateLocal is FloidAppStateLoaded) {
                                                                          await FloidElevationApiClient(floidServerApiClient: floidServerRepositoryState.floidServerRepository.floidServerApiClient).fetchElevation(
                                                                              floidElevationRequest, floidServerRepositoryState.secure, floidAppStateLocal.floidAppStateAttributes.elevationLocation);
                                                                        }
                                                                      }
                                                                      floidPinSetBlocLocal.add(ReplaceFloidPin(index: currentPinIndex,
                                                                          floidPinAlt: newFloidPinAlt));
                                                                    }
                                                                  },
                                                                  child: Icon(EDIT_ICON, size: 24, color: MODAL_TEXT_COLOR,)),
                                                              currentPinIndex > 0
                                                                  ? InkWell(
                                                                  onTap: () {
                                                                    // Adjust the pin up:
                                                                    floidPinSetBlocLocal.add(AdjustFloidPinSet(up: true, index: currentPinIndex));
                                                                  },
                                                                  child: Icon(MOVE_UP_ICON, size:32, color: MODAL_TEXT_COLOR,))
                                                                  : InkWell(
                                                                  onTap: null,
                                                                  child: Icon(
                                                                    EMPTY_ICON,
                                                                    color: Colors.transparent,
                                                                    size: 32,
                                                                  )),
                                                              currentPinIndex < floidPinSetState.floidPinSetAlt.pinAltList.length - 1
                                                                  ? InkWell(
                                                                  onTap: () {
                                                                    // Adjust the pin down:
                                                                    floidPinSetBlocLocal.add(AdjustFloidPinSet(up: false, index: currentPinIndex));
                                                                  },
                                                                  child: Icon(MOVE_DOWN_ICON, size: 32, color: MODAL_TEXT_COLOR,))
                                                                  : InkWell(
                                                                  onTap: null,
                                                                  child: Icon(
                                                                    EMPTY_ICON,
                                                                    size: 32,
                                                                    color: Colors.transparent,
                                                                  )),
                                                              InkWell(
                                                                  onTap: () {
                                                                    // Remove the pin:
                                                                    floidPinSetBlocLocal.add(RemoveFloidPin(index: currentPinIndex));
                                                                  },
                                                                  child: Icon(DELETE_ICON, size: 26, color: MODAL_TEXT_COLOR,)),
                                                            ],
                                                          ),
                                                        ),
                                                      ]);
                                                    }).toList(),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                            : Container();
                                      },
                                    );
                                  },
                                );
                              },
                            );
                          },
                        );
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          GestureDetector(
                            child: Column(
                              children: [
                                Icon(
                                  PIN_SET_ICON,
                                  size: 26,
                                  color: (floidPinSetState is FloidPinSetLoaded && floidPinSetState.dirty) ? BORDER_DIRTY_COLOR : BORDER_CLEAN_COLOR,
                                ),
                                if(floidPinSetState is FloidPinSetLoaded && floidPinSetState.dirty)
                                  Text('SAVE',
                                    style: TextStyle(fontSize: 12, color: Colors.orangeAccent,),
                                  ),
                              ],
                            ),
                            onTap: () async {
                              if (floidPinSetState is FloidPinSetLoaded && floidPinSetState.dirty) {
                                if (floidServerRepositoryState is FloidServerRepositoryStateHasHost && floidPinSetState is FloidPinSetLoaded) {
                                  Map<String, dynamic> jsonPinSetMap = floidPinSetState.floidPinSetAlt.toJson();
                                  String jsonPinSetString = jsonEncode(jsonPinSetMap);
                                  int pinSetResultId = await floidServerRepositoryState.floidServerRepository.savePinSet(jsonPinSetString);
                                  if (pinSetResultId != -1) {
                                    // Successful save - turn off the pin set dirty flag:
                                    BlocProvider.of<FloidPinSetBloc>(context).add(SetFloidPinSetClean());
                                    BlocProvider.of<FloidMessagesBloc>(context).add(
                                        AddFloidMessage(message: 'Pin Set Saved: ' + floidPinSetState.floidPinSetAlt.pinSetName));
                                  } else {
                                    Alert(context: context, title: "Pin Set Failed to Save",
                                        style: AlertStyle(backgroundColor: Theme.of(context).canvasColor, titleStyle: TextStyle(color: Theme.of(context).splashColor)),
                                        buttons: [
                                          DialogButton(
                                            color: Theme.of(context).highlightColor,
                                            child: Text(
                                              "OK",
                                              style: TextStyle(color: Theme.of(context).splashColor, fontSize: 20),
                                            ),
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                    ]).show();
                                  }
                                }
                              }
                            },
                          ),
                          Text(floidPinSetState is FloidPinSetLoaded ? floidPinSetState.floidPinSetAlt.pinSetName : '',
                              style: TextStyle(fontSize: 18, color: PIN_SET_NAME_COLOR)),
                        ],
                      ),
                    ),
                  ),
                ) : LoadingIndicator();
              }
          );
        }
    );
  }
}
