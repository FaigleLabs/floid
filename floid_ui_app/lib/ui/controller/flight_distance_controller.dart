/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FlightDistanceController extends StatelessWidget {
  static const Color FLIGHT_DISTANCE_TEXT_COLOR = Colors.yellowAccent;
  const FlightDistanceController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
      builder: (context, floidAppState) {
        // Floid Mission List State:
        return Padding(
          padding: const EdgeInsets.fromLTRB(4.0,0.0,0.0,0.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(floidAppState is FloidAppStateLoaded ? (floidAppState.floidAppStateAttributes.flightDistance<1000?(floidAppState.floidAppStateAttributes.flightDistance.floor().toString() + 'm'):(((floidAppState.floidAppStateAttributes.flightDistance/100).floor()/10).toString() + 'km')) : '',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: FLIGHT_DISTANCE_TEXT_COLOR,)),
            ],
          ),
        );
      },
    );
  }
}
