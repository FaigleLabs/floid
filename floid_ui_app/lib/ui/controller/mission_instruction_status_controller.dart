/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/page/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MissionInstructionStatusController extends StatelessWidget {
  static const Color BOX_COLOR = Colors.black;
  static const Color BORDER_COLOR = Color(0xFFFFEE58); // Colors.yellow[400];
  static const Color SHADOW_COLOR = Colors.black;
  static const double HEADER_FONT_SIZE = 16;
  static const double FONT_SIZE = 12;
  static const double HEADER_ROW_HEIGHT = 18;
  static const double ROW_HEIGHT = 14;
  static const double ROW_SPACER= 16;
  static const Color DEFAULT_TEXT_COLOR = Colors.yellowAccent;

  const MissionInstructionStatusController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<
        FloidDroidMissionInstructionStatusMessageBloc,
        FloidDroidMissionInstructionStatusMessageState>(
      builder: (context, floidDroidMissionInstructionStatusMessageState) {
        return floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded &&
            floidDroidMissionInstructionStatusMessageState
                .floidDroidMissionInstructionStatus.missionNameString.length > 0
            ? Container(
          padding: EdgeInsets.fromLTRB(0, 0, 4, 4),
          child: Container(
            padding: EdgeInsets.fromLTRB(4, 2, 2, 2),
            decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(6)),
              color: HomePage.PANEL_BACKGROUND_COLOR,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded &&
                    floidDroidMissionInstructionStatusMessageState
                        .floidDroidMissionInstructionStatus.missionNameString
                        .length > 0
                    ? SizedBox(
                  height: HEADER_ROW_HEIGHT,
                  width: 160,
                  child: Text(
                    floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded
                        ? '${floidDroidMissionInstructionStatusMessageState
                        .floidDroidMissionInstructionStatus.missionNameString}'
                        : '',
                    style: TextStyle(fontSize: HEADER_FONT_SIZE,
                        color: DEFAULT_TEXT_COLOR),),
                ) : SizedBox(width: 0, height: 0),
          ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(width:ROW_SPACER, height: ROW_HEIGHT),
                    floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded &&
                        floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .missionNameString.length > 0
                        ? SizedBox(
                      height: ROW_HEIGHT,
                      width: 80,
                      child: Text(
                        floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded
                            ? '${floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .missionStatusString}'
                            : '',
                        style: TextStyle(fontSize: FONT_SIZE,
                            color: DEFAULT_TEXT_COLOR),),
                    ) : SizedBox(width: 0, height: 0),
                    floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded &&
                        floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .missionNameString.length > 0
                        ? SizedBox(
                      height: ROW_HEIGHT,
                      width: 80,
                      child: Text(
                        floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded
                            ? '${floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .missionProgressString}'
                            : '',
                        style: TextStyle(fontSize: FONT_SIZE,
                            color: DEFAULT_TEXT_COLOR),),
                    ) : SizedBox(width: 0, height: 0),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded &&
                    floidDroidMissionInstructionStatusMessageState
                        .floidDroidMissionInstructionStatus
                        .instructionNameString.length > 0
                    ? SizedBox(
                  height: HEADER_ROW_HEIGHT,
                  width: 160,
                  child: Text(
                    floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded
                        ? '${floidDroidMissionInstructionStatusMessageState
                        .floidDroidMissionInstructionStatus
                        .instructionNameString}'
                        : '',
                    style: TextStyle(fontSize: HEADER_FONT_SIZE,
                        color: (floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded)
                            ? floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .instructionStatusColor
                            : DEFAULT_TEXT_COLOR),),
                ) : SizedBox(width: 0, height: 0),
                ],),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded &&
                        floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .instructionNameString.length > 0
                        ? SizedBox(width:ROW_SPACER, height: ROW_HEIGHT) : SizedBox(width: 0, height: 0),
                    floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded &&
                        floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .instructionNameString.length > 0
                        ? SizedBox(
                      height: ROW_HEIGHT,
                      width: 80,
                      child: Text(
                        floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded
                            ? '${floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .instructionProgressString}'
                            : '',
                        style: TextStyle(fontSize: FONT_SIZE,
                            color: (floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded)
                                ? floidDroidMissionInstructionStatusMessageState
                                .floidDroidMissionInstructionStatus
                                .instructionStatusColor
                                : DEFAULT_TEXT_COLOR),),
                    ) : SizedBox(width: 0, height: 0),
                    floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded &&
                        floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .instructionNameString.length > 0
                        ? SizedBox(
                      height: ROW_HEIGHT,
                      width: 80,
                      child: Text(
                        floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded
                            ? '${floidDroidMissionInstructionStatusMessageState
                            .floidDroidMissionInstructionStatus
                            .instructionStatusString}'
                            : '',
                        style: TextStyle(fontSize: FONT_SIZE,
                            color: (floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded)
                                ? floidDroidMissionInstructionStatusMessageState
                                .floidDroidMissionInstructionStatus
                                .instructionStatusColor
                                : DEFAULT_TEXT_COLOR),),
                    ) : SizedBox(width: 0, height: 0),
                  ],
                ),
              ],
            ),
          ),
        ) : Container();
      },
    );
  }
}
