/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:collection/collection.dart';
class FloidError {
  static const int WARNING = 0;
  static const int ERROR = 1;
  static const int  MISSION_WARNING_SKIPPING_MISSION_ERROR_CHECK = 0;
  static const int  MISSION_WARNING_FLIGHT_BELOW_PIN_LEVEL = 1;
  static const int  MISSION_WARNING_PHOTO_DURING_VIDEO = 2;
  static const int  MISSION_ERROR_FIRST_ITEM_SET_PARAMETERS = 3;
  static const int  MISSION_ERROR_SECOND_ITEM_ACQUIRE_HOME_POSITION = 4;
  static const int  MISSION_ERROR_NO_HOME_PIN = 5;
  static const int  MISSION_ERROR_FLIGHT_BELOW_PIN_LEVEL = 6;
  static const int  MISSION_ERROR_MISSING_PIN = 7;
  static const int  MISSION_ERROR_BAD_PIN = 8;
  static const int  MISSION_ERROR_BAD_DESIGNATE_PIN = 9;
  static const int  MISSION_ERROR_HOVER_AFTER_LIFT_OFF = 10;
  static const int  MISSION_ERROR_DELAY_ON_GROUND = 11;
  static const int  MISSION_ERROR_BAD_PAN_TILT_PIN = 12;
  static const int  MISSION_ERROR_LIFT_OFF_CANNOT_CHECK_PIN_HEIGHT = 13;
  static const int  MISSION_ERROR_LIFT_OFF_BELOW_PIN_HEIGHT = 14;
  static const int  MISSION_ERROR_LIFT_OFF_BEFORE_START_HELIS = 15;
  static const int  MISSION_ERROR_LIFT_OFF_ALREADY_IN_AIR = 16;
  static const int  MISSION_ERROR_HEIGHT_TO_BELOW_PIN_HEIGHT = 17;
  static const int  MISSION_ERROR_HEIGHT_TO_CANNOT_CHECK_PIN_HEIGHT = 18;
  static const int  MISSION_ERROR_HEIGHT_TO_WITHOUT_LIFT_OFF = 19;
  static const int  MISSION_ERROR_TURN_TO_WITHOUT_LIFT_OFF = 20;
  static const int  MISSION_ERROR_LAND_WITHOUT_LIFT_OFF = 21;
  static const int  MISSION_ERROR_HELIS_ALREADY_STARTED = 22;
  static const int  MISSION_ERROR_HELIS_SHUT_DOWN_WITHOUT_START_UP = 23;
  static const int  MISSION_ERROR_HELIS_SHUT_DOWN_DURING_FLIGHT = 24;
  static const int  MISSION_ERROR_VIDEO_ALREADY_STARTED = 25;
  static const int  MISSION_ERROR_VIDEO_NOT_STARTED = 26;
  static const int  MISSION_ERROR_PAYLOAD_MUST_BE_IN_FLIGHT = 27;
  static const int  MISSION_ERROR_BAY_ALREADY_USED = 28;
  static const int  MISSION_ERROR_INVALID_BAY_NUMBER = 29;
  static const int  MISSION_ERROR_DOES_NOT_FINISH_ON_GROUND = 30;
  static const int  MISSION_ERROR_DOES_NOT_FINISH_WITH_HELIS_OFF = 31;
  static const int  MISSION_ERROR_DOES_NOT_FINISH_WITH_VIDEO_OFF = 32;
  static const int  MISSION_ERROR_DOES_NOT_FINISH_WITH_PHOTO_STROBE_OFF = 33;
  static const int  MISSION_ERROR_PHOTO_STROBE_ALREADY_STARTED = 34;
  static const int  MISSION_ERROR_PHOTO_STROBE_STOPPED_BUT_NOT_STARTED = 35;
  static const int  MISSION_ERROR_FLY_PATTERN_NOT_IMPLEMENTED = 36;
  static const int  MISSION_ERROR_EMPTY_MISSION = 37;
  static const int  MISSION_ERROR_EMPTY_PIN_SET = 38;
  static const int  MISSION_ERROR_RELATIVE_LIFT_OFF_LOW = 39;
  static const int  MISSION_ERROR_MISSION_CHECK_EXCEPTION = 40;
  static const int  MISSION_ERROR_UNKNOWN = 41;

  static const int  INSTRUCTION_ERROR_FLY_TO_SAME_PIN = 101;

  static const int  FLIGHT_PATH_NOT_VALID = 201;
  static const int  FLIGHT_PATH_NULL = 202;
  static const int  FLIGHT_PATH_ELEVATION_STATUS_FAIL = 203;
  static const int  FLIGHT_PATH_ELEVATION_ERROR = 204;
  static const int  FLIGHT_PATH_ELEVATION_WARNING = 205;
  static const int  FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING = 206;


  // ignore: non_constant_identifier_names
  static Map<int, FloidError> FLOID_ERRORS = {
    MISSION_WARNING_SKIPPING_MISSION_ERROR_CHECK: FloidError(token: MISSION_WARNING_SKIPPING_MISSION_ERROR_CHECK, severity: FloidError.WARNING, message: 'Skipping mission error check'),
    MISSION_WARNING_FLIGHT_BELOW_PIN_LEVEL: FloidError(token: MISSION_WARNING_FLIGHT_BELOW_PIN_LEVEL, severity: FloidError.WARNING, message: 'Flight cannot verify height'),
    MISSION_WARNING_PHOTO_DURING_VIDEO: FloidError(token: MISSION_WARNING_PHOTO_DURING_VIDEO, severity: FloidError.WARNING, message: 'Taking photo during video not supported on all devices'),
    MISSION_ERROR_FIRST_ITEM_SET_PARAMETERS: FloidError(token: MISSION_ERROR_FIRST_ITEM_SET_PARAMETERS, severity: FloidError.ERROR, message: 'First instruction must be SetParameters'),
    MISSION_ERROR_SECOND_ITEM_ACQUIRE_HOME_POSITION: FloidError(token: MISSION_ERROR_SECOND_ITEM_ACQUIRE_HOME_POSITION, severity: FloidError.ERROR, message: 'Second instruction must be AcquireHomePosition'),
    MISSION_ERROR_NO_HOME_PIN: FloidError(token: MISSION_ERROR_NO_HOME_PIN, severity: FloidError.ERROR, message: 'No home pin'),
    MISSION_ERROR_FLIGHT_BELOW_PIN_LEVEL: FloidError(token: MISSION_ERROR_FLIGHT_BELOW_PIN_LEVEL, severity: FloidError.ERROR, message: 'Flight below pin level'),
    MISSION_ERROR_MISSING_PIN: FloidError(token: MISSION_ERROR_MISSING_PIN, severity: FloidError.ERROR, message: 'Missing pin: '),
    MISSION_ERROR_BAD_PIN: FloidError(token: MISSION_ERROR_BAD_PIN, severity: FloidError.ERROR, message: 'Bad pin: '),
    MISSION_ERROR_BAD_DESIGNATE_PIN: FloidError(token: MISSION_ERROR_BAD_DESIGNATE_PIN, severity: FloidError.ERROR, message: 'Bad designate pin: '),
    MISSION_ERROR_HOVER_AFTER_LIFT_OFF: FloidError(token: MISSION_ERROR_HOVER_AFTER_LIFT_OFF, severity: FloidError.ERROR, message: 'Hover only after lift off.'),
    MISSION_ERROR_DELAY_ON_GROUND: FloidError(token: MISSION_ERROR_DELAY_ON_GROUND, severity: FloidError.ERROR, message: 'Delay only on ground.'),
    MISSION_ERROR_BAD_PAN_TILT_PIN: FloidError(token: MISSION_ERROR_BAD_PAN_TILT_PIN, severity: FloidError.ERROR, message: 'Bad pan/tilt pin: '),
    MISSION_ERROR_LIFT_OFF_CANNOT_CHECK_PIN_HEIGHT: FloidError(token: MISSION_ERROR_LIFT_OFF_CANNOT_CHECK_PIN_HEIGHT, severity: FloidError.ERROR, message: 'Cannot check lift off pin height'),
    MISSION_ERROR_LIFT_OFF_BELOW_PIN_HEIGHT: FloidError(token: MISSION_ERROR_LIFT_OFF_BELOW_PIN_HEIGHT, severity: FloidError.ERROR, message: 'Lift off below pin height'),
    MISSION_ERROR_LIFT_OFF_BEFORE_START_HELIS: FloidError(token: MISSION_ERROR_LIFT_OFF_BEFORE_START_HELIS, severity: FloidError.ERROR, message: 'Lift off must be after start helis'),
    MISSION_ERROR_LIFT_OFF_ALREADY_IN_AIR: FloidError(token: MISSION_ERROR_LIFT_OFF_ALREADY_IN_AIR, severity: FloidError.ERROR, message: 'Lift off when already in air.'),
    MISSION_ERROR_HEIGHT_TO_BELOW_PIN_HEIGHT: FloidError(token: MISSION_ERROR_HEIGHT_TO_BELOW_PIN_HEIGHT, severity: FloidError.ERROR, message: 'Height to below pin height'),
    MISSION_ERROR_HEIGHT_TO_CANNOT_CHECK_PIN_HEIGHT: FloidError(token: MISSION_ERROR_HEIGHT_TO_CANNOT_CHECK_PIN_HEIGHT, severity: FloidError.ERROR, message: 'Cannot check height to pin height'),
    MISSION_ERROR_HEIGHT_TO_WITHOUT_LIFT_OFF: FloidError(token: MISSION_ERROR_HEIGHT_TO_WITHOUT_LIFT_OFF, severity: FloidError.ERROR, message: 'Height to must be after lift off'),
    MISSION_ERROR_TURN_TO_WITHOUT_LIFT_OFF: FloidError(token: MISSION_ERROR_TURN_TO_WITHOUT_LIFT_OFF, severity: FloidError.ERROR, message: 'Turn to must be after lift off'),
    MISSION_ERROR_LAND_WITHOUT_LIFT_OFF: FloidError(token: MISSION_ERROR_LAND_WITHOUT_LIFT_OFF, severity: FloidError.ERROR, message: 'Land must be after lift off'),
    MISSION_ERROR_HELIS_ALREADY_STARTED: FloidError(token: MISSION_ERROR_HELIS_ALREADY_STARTED, severity: FloidError.ERROR, message: 'Helis already started'),
    MISSION_ERROR_HELIS_SHUT_DOWN_WITHOUT_START_UP: FloidError(token: MISSION_ERROR_HELIS_SHUT_DOWN_WITHOUT_START_UP, severity: FloidError.ERROR, message: 'Heli shut down without start up'),
    MISSION_ERROR_HELIS_SHUT_DOWN_DURING_FLIGHT: FloidError(token: MISSION_ERROR_HELIS_SHUT_DOWN_DURING_FLIGHT, severity: FloidError.ERROR, message: 'Heli shut down during flight'),
    MISSION_ERROR_VIDEO_ALREADY_STARTED: FloidError(token: MISSION_ERROR_VIDEO_ALREADY_STARTED, severity: FloidError.ERROR, message: 'Video already started'),
    MISSION_ERROR_VIDEO_NOT_STARTED: FloidError(token: MISSION_ERROR_VIDEO_NOT_STARTED, severity: FloidError.ERROR, message: 'Video not started'),
    MISSION_ERROR_PAYLOAD_MUST_BE_IN_FLIGHT: FloidError(token: MISSION_ERROR_PAYLOAD_MUST_BE_IN_FLIGHT, severity: FloidError.ERROR, message: 'Payload only in flight'),
    MISSION_ERROR_BAY_ALREADY_USED: FloidError(token: MISSION_ERROR_BAY_ALREADY_USED, severity: FloidError.ERROR, message: 'Bay already used: '),
    MISSION_ERROR_INVALID_BAY_NUMBER: FloidError(token: MISSION_ERROR_INVALID_BAY_NUMBER, severity: FloidError.ERROR, message: 'Invalid bay number: '),
    MISSION_ERROR_DOES_NOT_FINISH_ON_GROUND: FloidError(token: MISSION_ERROR_DOES_NOT_FINISH_ON_GROUND, severity: FloidError.ERROR, message: 'Finishes in air'),
    MISSION_ERROR_DOES_NOT_FINISH_WITH_HELIS_OFF: FloidError(token: MISSION_ERROR_DOES_NOT_FINISH_WITH_HELIS_OFF, severity: FloidError.ERROR, message: 'Finishes with helis on'),
    MISSION_ERROR_DOES_NOT_FINISH_WITH_VIDEO_OFF: FloidError(token: MISSION_ERROR_DOES_NOT_FINISH_WITH_VIDEO_OFF, severity: FloidError.ERROR, message: 'Finishes with video on'),
    MISSION_ERROR_DOES_NOT_FINISH_WITH_PHOTO_STROBE_OFF: FloidError(token: MISSION_ERROR_DOES_NOT_FINISH_WITH_PHOTO_STROBE_OFF, severity: FloidError.ERROR, message: 'Finishes w/photo strobe on'),
    MISSION_ERROR_PHOTO_STROBE_ALREADY_STARTED: FloidError(token: MISSION_ERROR_PHOTO_STROBE_ALREADY_STARTED, severity: FloidError.ERROR, message: 'Photo strobe already started'),
    MISSION_ERROR_PHOTO_STROBE_STOPPED_BUT_NOT_STARTED: FloidError(token: MISSION_ERROR_PHOTO_STROBE_STOPPED_BUT_NOT_STARTED, severity: FloidError.ERROR, message: 'Photo strobe stopped but not started'),
    MISSION_ERROR_FLY_PATTERN_NOT_IMPLEMENTED: FloidError(token: MISSION_ERROR_FLY_PATTERN_NOT_IMPLEMENTED, severity: FloidError.ERROR, message: 'Fly pattern not implemented'),
    MISSION_ERROR_EMPTY_MISSION: FloidError(token: MISSION_ERROR_EMPTY_MISSION, severity: FloidError.ERROR, message: 'Empty mission'),
    MISSION_ERROR_EMPTY_PIN_SET: FloidError(token: MISSION_ERROR_EMPTY_PIN_SET, severity: FloidError.ERROR, message: 'Empty pin set'),
    MISSION_ERROR_RELATIVE_LIFT_OFF_LOW: FloidError(token: MISSION_ERROR_RELATIVE_LIFT_OFF_LOW, severity: FloidError.ERROR, message: 'Lift-off too low'),
    MISSION_ERROR_MISSION_CHECK_EXCEPTION: FloidError(token: MISSION_ERROR_MISSION_CHECK_EXCEPTION, severity: FloidError.ERROR, message: 'Exception Checking Mission'),
    MISSION_ERROR_UNKNOWN: FloidError(token: MISSION_ERROR_UNKNOWN, severity: FloidError.ERROR, message: 'Unknown error'),
    INSTRUCTION_ERROR_FLY_TO_SAME_PIN: FloidError(token: INSTRUCTION_ERROR_FLY_TO_SAME_PIN, severity: FloidError.WARNING, message: 'Fly to same pin'),
    FLIGHT_PATH_NOT_VALID: FloidError(token: FLIGHT_PATH_NOT_VALID, severity: FloidError.ERROR, message: 'Invalid server elevation'),
    FLIGHT_PATH_NULL: FloidError(token: FLIGHT_PATH_NULL, severity: FloidError.ERROR, message: 'Flight path null'),
    FLIGHT_PATH_ELEVATION_STATUS_FAIL: FloidError(token: FLIGHT_PATH_ELEVATION_STATUS_FAIL, severity: FloidError.ERROR, message: 'Path query fail'),
    FLIGHT_PATH_ELEVATION_ERROR: FloidError(token: FLIGHT_PATH_ELEVATION_ERROR, severity: FloidError.ERROR, message: 'Altitude too low: '),
    FLIGHT_PATH_ELEVATION_WARNING: FloidError(token: FLIGHT_PATH_ELEVATION_WARNING, severity: FloidError.WARNING, message: 'Altitude warning'),
    FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING: FloidError(token: FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING, severity: FloidError.WARNING, message: 'Elevation requests in progress'),
  };
  final int token;
  final int severity;
  final String message;
  FloidError({required this.message, required this.severity, required this.token});

  static FloidError getFloidError(int token) {
    print('Getting error for token: ' + token.toString());
    if(FloidError.FLOID_ERRORS.containsKey(token)) {
      FloidError? floidError = FloidError.FLOID_ERRORS[token];
      return floidError!=null?floidError:FloidError(message: 'UNKNOWN', severity: ERROR, token: token);
    }
    return FloidError(message: 'UNKNOWN', severity: ERROR, token: token);
  }
}

class FloidErrorInstance {
  static const int NO_REF_ID = -1;
  final FloidError floidError;
  final String? extraInfo;
  final int referenceId;

  FloidErrorInstance({required this.floidError, this.extraInfo, this.referenceId=NO_REF_ID});
}

class FloidErrors {
  List<FloidErrorInstance> errors;

  FloidErrors({required this.errors});
  
  void add(FloidErrorInstance floidErrorInstance) {
    errors.add(floidErrorInstance);
  }

  void addAll(FloidErrors floidErrors) {
    this.errors.addAll(floidErrors.errors);
  }

  void clear() {
    errors.clear();
  }

  bool hasCleanSheet() {
    return errors.length==0;
  }
  bool hasWarnings() {
    if(errors.length==0) { return false; }
    return errors.firstWhereOrNull((element) {
      final  FloidError? fFloidError = element.floidError;
      return fFloidError==null?false:fFloidError.severity==FloidError.WARNING;
    }) != null;
  }

  bool hasErrors() {
    if(errors.length==0) { return false; }
    return errors.firstWhereOrNull((element) {
      final  FloidError? fFloidError = element.floidError;
      return fFloidError==null?false:fFloidError.severity==FloidError.ERROR;
    }) != null;
  }
  bool hasThisError(int token) {
    return errors.any((e) => e.floidError.token == token);
  }
  bool hasThisErrorWithThisReference(int token, int referenceId) {
    return errors.any((e) => e.floidError.token == token && e.referenceId == referenceId);
  }
  void removeThisError(int token) {
    return errors.removeWhere((e) => e.floidError.token == token);
  }
  void removeThisErrorWithThisReference(int token, int referenceId) {
    return errors.removeWhere((e) => e.floidError.token == token && e.referenceId == referenceId);
  }
  bool hasOnlyMissionSkipCheck() {
    final fErrors = errors;
    if(fErrors.length==1){
      final FloidError? fFloidError = fErrors[0].floidError;
      return fFloidError!=null && fFloidError.token == FloidError.MISSION_WARNING_SKIPPING_MISSION_ERROR_CHECK;
      }
    return false;
  }

  bool hasOnlyWarnings() {
    return hasWarnings() && !hasErrors();
  }
  bool hasErrorsOrWarnings() {
    return errors.length>0;
  }
}
