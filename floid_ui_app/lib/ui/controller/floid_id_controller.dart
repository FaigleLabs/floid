/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/common/common.dart';
import 'package:floid_ui_app/ui/widgets/floid_uuid_chooser.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';

class FloidIdController extends StatelessWidget {
  /// Process a new floid Uuid when loaded...
  void _processNewFloidUuid(BuildContext context, FloidIdAndState? floidIdAndState, FloidUuidAndState? floidUuidAndState) {
    final FloidIdAndState? fFloidIdAndState = floidIdAndState;
    final FloidUuidAndState? fFloidUuidAndState = floidUuidAndState;
    // Tell the Bloc that we have a new Floid Uuid:
    if(fFloidIdAndState!=null &&  fFloidUuidAndState != null)
    BlocProvider.of<FloidIdBloc>(context).add(SetFloidUuidAndState(floidIdAndState: fFloidIdAndState, floidUuidAndState: fFloidUuidAndState));
  }

  const FloidIdController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Wrap in a Floid Server provider:
    return BlocBuilder<FloidIdBloc, FloidIdState>(
        builder: (context, floidIdState) {
          return BlocBuilder<FloidUuidAndStateListBloc, FloidUuidAndStateListState>(
              builder: (context, floidUuidAndStateListState) {
                return BlocBuilder<FloidServerRepositoryBloc, FloidServerRepositoryState>(
                    builder: (context, floidServerRepositoryState) {
                      return
                        InkWell(
                          onTap: () async {
                            FloidIdBloc floidIdBloc = BlocProvider.of<FloidIdBloc>(context);
                            final FloidIdState fFloidIdBlocState = floidIdBloc.state;
                            FloidUuidAndState? floidUuidAndState;
                            if (fFloidIdBlocState is FloidUuidLoaded) {
                              FloidUuidLoaded floidUuidLoaded = fFloidIdBlocState;
                              floidUuidAndState = floidUuidLoaded.floidUuidAndState;
                            }
                            final String currentFloidUuid = floidUuidAndState!=null?floidUuidAndState.floidUuid : '';
                            FloidUuidAndState? newFloidUuidAndState = await showDialog<FloidUuidAndState>(
                              context: context,
                              builder: (BuildContext context) {
                                return floidUuidAndStateListState is FloidUuidAndStateListLoaded
                                    ? FloidUuidChooser(floidUuidAndStateList: floidUuidAndStateListState.floidUuidAndStateList,
                                currentFloidUuid: currentFloidUuid,)
                                    : LoadingIndicator();
                              },
                            );
                            if (newFloidUuidAndState != null) {
                              _processNewFloidUuid(context, floidIdState is FloidIdLoaded ?floidIdState.floidIdAndState:floidIdState is FloidUuidLoaded?floidIdState.floidIdAndState:null, newFloidUuidAndState);
                            }
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 4),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(floidIdState is FloidUuidLoaded ? floidIdState.floidIdAndState.floidId.toString() : '----',
                                    style: TextStyle(fontSize: 18, color: Theme.of(context).textTheme.bodyMedium?.color??Colors.yellowAccent)),
                              ],
                            ),
                          ),
                        );
                    }
                );
              }
          );
        }
    );
  }
}
