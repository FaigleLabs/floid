/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:math' as math;
import 'package:floid_ui_app/ui/controller/controller.dart';
import 'package:floid_ui_app/ui/map/utility/distance.dart';
import 'package:floid_ui_app/ui/page/home/floid_icon_decoration.dart';
import 'package:floid_ui_app/ui/page/home/floid_text_decoration.dart';
import 'package:floid_ui_app/ui/page/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class FloidSystemsController extends StatelessWidget {
  static const Color PROGRESS_BACKGROUND_COLOR_ON = Color(0xFF2F2F2F);
  static const Color PROGRESS_BACKGROUND_COLOR_OFF = Color(0xFF2F2F2F);
  static const Color PROGRESS_FOREGROUND_COLOR = Colors.orange;
  static const Color DIAL_BACKGROUND_COLOR = Color(0x00FFFF00);
  static const Color SERVOS_BACKGROUND_COLOR = Colors.black87;
  static const Color DIAL_CIRCLE_COLOR = Colors.black;
  static const Color DIAL_INDICATOR_COLOR = Colors.yellowAccent;
  static const Color DIAL_TEXT_COLOR = Colors.yellowAccent;
  static const Color GOAL_DIAL_INDICATOR_COLOR = Color(0xFF80D4FF); //Colors.lightBlueAccent;
  static const Color GOAL_DIAL_TEXT_COLOR = Color(0xFF80D4FF); // Colors.lightBlueAccent;
  static const double DIAL_SIZE = 30.0;
  static const double PITCH_DIAL_FONT_SIZE = 10.0;
  static IconData DEVICE_LED_ICON = MdiIcons.ledOutline;
  static IconData DEVICE_GPS_ICON = MdiIcons.mapMarkerOutline;
  static IconData DEVICE_IMU_ICON = MdiIcons.memory;
  static IconData DEVICE_HELIS_ICON = MdiIcons.power;
  static IconData DEVICE_PARACHUTE_ICON = MdiIcons.parachute;
  static IconData DEVICE_AUX_ICON = MdiIcons.powerPlugOutline;
  static IconData DEVICE_DESIGNATOR_ICON = MdiIcons.spotlightBeam;
  static IconData DEVICE_CAMERA_ICON = MdiIcons.cameraOutline;
  static IconData DEVICE_CAMERA_FOLLOW_MODE_ICON = MdiIcons.cameraControl;
  static IconData DEVICE_GPS_FROM_DEVICE_ICON = MdiIcons.cellphoneMarker;
  static IconData DEVICE_GPS_EMULATE_ICON = MdiIcons.orbit;
  static const IconData DIAL_ARROW_ICON = Icons.arrow_upward;
  static const IconData DIAL_ROLL_ICON = Icons.vertical_align_center;
  static IconData FLOID_ICON = MdiIcons.quadcopter;
  static IconData VIEW_FLOID_GPS_LINES_ICON = MdiIcons.mapMarkerRadiusOutline;
  static const double INDICATOR_LINE_HEIGHT = 10.0;
  static const double INDICATOR_WIDTH = 30;
  static const double INDICATOR_FONT_SIZE = 10;
  static const double DIAL_FONT_SIZE = 10;
  static const double SECTION_LABEL_FONT_SIZE = 10;

  // IMU algorithm status display:
  static const int IMU_ALGORITHM_STANDBY                               = 0x01;
  static const String IMU_ALGORITHM_STANDBY_TEXT                       = "STANDBY";
  static const String IMU_ALGORITHM_STANDBY_FALSE_TEXT                 = "";
  static const int IMU_ALGORITHM_SLOW                                  = 0x02;
  static const String IMU_ALGORITHM_SLOW_TEXT                          = "SLOW";
  static const String IMU_ALGORITHM_SLOW_FALSE_TEXT                    = "";
  static const int IMU_ALGORITHM_STILL                                 = 0x04;
  static const String IMU_ALGORITHM_STILL_TEXT                         = "STILL";
  static const String IMU_ALGORITHM_STILL_FALSE_TEXT                   = "";
  static const int IMU_ALGORITHM_STABLE                                = 0x08;
  static const String IMU_ALGORITHM_STABLE_TEXT                        = "STABLE";
  static const String IMU_ALGORITHM_STABLE_FALSE_TEXT                  = "CALIBRATING";
  static const int IMU_ALGORITHM_MAG_TRANSIENT                         = 0x10;
  static const String IMU_ALGORITHM_MAG_TRANSIENT_TEXT                 = "MAG. TRANS";
  static const String IMU_ALGORITHM_MAG_TRANSIENT_FALSE_TEXT           = "";
  static const int IMU_ALGORITHM_UNRELIABLE                            = 0x20;
  static const String IMU_ALGORITHM_UNRELIABLE_TEXT                    = "UNRELIABLE";
  static const String IMU_ALGORITHM_UNRELIABLE_FALSE_TEXT              = "";
  static const int IMU_ALGORITHM_WS_PARAMETERS_SAVED                   = 0x40;
  static const String IMU_ALGORITHM_WS_PARAMETERS_SAVED_TEXT           = "SAVED";
  static const String IMU_ALGORITHM_WS_PARAMETERS_SAVED_FALSE_TEXT     = "";
  static const int IMU_ALGORITHM_WS_PARAMETERS_LOADED                  = 0x80;
  static const String IMU_ALGORITHM_WS_PARAMETERS_LOADED_TEXT          = "LOADED";
  static const String IMU_ALGORITHM_WS_PARAMETERS_LOADED_FALSE_TEXT    = "";
  static const int IMU_ALGORITHM_WS_PARAMETERS_CLEARED                 = 0x100;
  static const String IMU_ALGORITHM_WS_PARAMETERS_CLEARED_TEXT         = "CLEARED";
  static const String IMU_ALGORITHM_WS_PARAMETERS_CLEARED_FALSE_TEXT   = "";

  String addIMUAlgorithmStatusText(String currentText, bool test, String trueText, String falseText) {
    if (test) {
      // Both non-empty, return both space separated:
      if (currentText.length > 0 && trueText.length > 0) {
        return currentText + ' ' + trueText;
      }
      // If current text is non-empty (means true text is empty):
      if (currentText.length > 0) {
        return currentText;
      }
      // Otherwise return the true test:
      return trueText;
    } else {
      // Both non-empty, return both space separated:
      if (currentText.length > 0 && falseText.length > 0) {
        return currentText + ' ' + falseText;
      }
      // If current text is non-empty (means false text is empty):
      if (currentText.length > 0) {
        return currentText;
      }
      // Otherwise return the false test:
      return falseText;
    }
  }

  String createAlgorithmStatusText(int ias) {
    String algorithmStatusText = "";
    algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
        (ias & IMU_ALGORITHM_STANDBY) > 0,
        IMU_ALGORITHM_STANDBY_TEXT,
        IMU_ALGORITHM_STANDBY_FALSE_TEXT);
    algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
        (ias & IMU_ALGORITHM_SLOW) > 0,
        IMU_ALGORITHM_SLOW_TEXT,
        IMU_ALGORITHM_SLOW_FALSE_TEXT);
    algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
        (ias & IMU_ALGORITHM_STILL) > 0,
        IMU_ALGORITHM_STILL_TEXT,
        IMU_ALGORITHM_STILL_FALSE_TEXT);
    algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
        (ias & IMU_ALGORITHM_STABLE) > 0,
        IMU_ALGORITHM_STABLE_TEXT,
        IMU_ALGORITHM_STABLE_FALSE_TEXT);
    algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
        (ias & IMU_ALGORITHM_MAG_TRANSIENT) > 0,
        IMU_ALGORITHM_MAG_TRANSIENT_TEXT,
        IMU_ALGORITHM_MAG_TRANSIENT_FALSE_TEXT);
    algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
        (ias & IMU_ALGORITHM_UNRELIABLE) > 0,
        IMU_ALGORITHM_UNRELIABLE_TEXT,
        IMU_ALGORITHM_UNRELIABLE_FALSE_TEXT);
    algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
        (ias & IMU_ALGORITHM_WS_PARAMETERS_SAVED) > 0,
        IMU_ALGORITHM_WS_PARAMETERS_SAVED_TEXT,
        IMU_ALGORITHM_WS_PARAMETERS_SAVED_FALSE_TEXT);
    algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
        (ias & IMU_ALGORITHM_WS_PARAMETERS_LOADED) > 0,
        IMU_ALGORITHM_WS_PARAMETERS_LOADED_TEXT,
        IMU_ALGORITHM_WS_PARAMETERS_LOADED_FALSE_TEXT);
    algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
        (ias & IMU_ALGORITHM_WS_PARAMETERS_CLEARED) > 0,
        IMU_ALGORITHM_WS_PARAMETERS_CLEARED_TEXT,
        IMU_ALGORITHM_WS_PARAMETERS_CLEARED_FALSE_TEXT);
    return algorithmStatusText;
  }

  const FloidSystemsController({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FloidAppStateBloc, FloidAppState>(
      builder: (context, floidAppState) {
        // Floid Model Parameters:
        return BlocBuilder<FloidModelParametersBloc, FloidModelParametersState>(
          builder: (context, floidModelParametersState) {
            // Floid Model Status:
            return BlocBuilder<FloidModelStatusBloc, FloidModelStatusState>(
              builder: (context, floidModelStatusState) {
                // Floid Status:
                return BlocBuilder<FloidStatusBloc, FloidStatusState>(
                  builder: (context, floidStatusState) {
                    // Floid Droid Status:
                    return BlocBuilder<FloidDroidStatusBloc, FloidDroidStatusState>(
                      builder: (context, floidDroidStatusState) {
                        return GestureDetector(
                          onTap: () {
                            if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.floidSystemsInfoOpen) {
                              // We should close:
                              BlocProvider.of<FloidAppStateBloc>(context).add(CloseFloidSystemsEvent());
                            } else {
                              // We should open:
                              BlocProvider.of<FloidAppStateBloc>(context).add(OpenFloidSystemsEvent());
                            }
                          },
                          child: Container(
                            color: Colors.transparent,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(4.0, 1.0, 4.0, 1.0),
                                      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(6)),
                                        color: HomePage.PANEL_BACKGROUND_COLOR,
                                      ),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Icon(
                                            FLOID_ICON,
                                            size: 28,
                                            color: (floidAppState is FloidAppStateLoaded
                                                && floidAppState.floidAppStateAttributes.floidIsAlive
                                                && floidDroidStatusState is FloidDroidStatusLoaded
                                                && floidDroidStatusState.floidDroidStatus.floidConnected) ?
                                            FloidDroidSystemsController.ICON_ON_COLOR :
                                            FloidDroidSystemsController.ICON_OFF_COLOR,
                                          ),
                                          // Floid Id Controller:
                                          FloidIdController(),
                                          GestureDetector(
                                            onTap: () {
                                              if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.showFloidGpsLines) {
                                                // We should close:
                                                BlocProvider.of<FloidAppStateBloc>(context).add(HideFloidGpsLinesEvent());
                                              } else {
                                                // We should open:
                                                BlocProvider.of<FloidAppStateBloc>(context).add(ShowFloidGpsLinesEvent());
                                              }
                                            },
                                            child: Icon(
                                              VIEW_FLOID_GPS_LINES_ICON,
                                              size: 26,
                                              color: floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.showFloidGpsLines
                                                  ? FloidDroidSystemsController.LINES_ON_COLOR : FloidDroidSystemsController.LINES_OFF_COLOR,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  if(floidStatusState is FloidStatusLoaded && floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.floidSystemsInfoOpen)
                                    Padding(
                                      padding: const EdgeInsets.fromLTRB(0.0, 2.0, 0.0, 0.0),
                                      child: Container(
                                        decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(8.0)),
                                          color: HomePage.PANEL_BACKGROUND_COLOR,
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  FloidTextDecoration(subLabel: floidStatusState.floidStatus.st.toString(), label: 'TIME'),
                                                  FloidTextDecoration(subLabel: floidStatusState.floidStatus.ka.toStringAsFixed(0) + ' m', label: 'ALTITUDE'),
                                                  FloidTextDecoration(subLabel: floidStatusState.floidStatus.kc.toStringAsFixed(2), label: 'DECL'),
                                                ],
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  FloidTextDecoration(subLabel: floidStatusState.floidStatus.js.toString(), label: 'SATS'),
                                                  FloidTextDecoration(subLabel: floidStatusState.floidStatus.jf.toString(), label: 'QUALITY'),
                                                  FloidTextDecoration(subLabel: floidStatusState.floidStatus.jh.toStringAsFixed(2), label: 'HDOP'),
                                                ],
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  FloidTextDecoration(subLabel: createAlgorithmStatusText(floidStatusState.floidStatus.ias), label: 'IMU Algorithm Status'),
                                                ],
                                              ),
                                              // Labels:
                                              Container(
                                                color: Color(0xFFFFFF00),
                                                width: 140,
                                                child: Row(
                                                  mainAxisSize: MainAxisSize.min,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.all(0.0),
                                                      child: Text('FLOID SYSTEMS', style: TextStyle(color: Colors.black, fontSize: SECTION_LABEL_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  FloidIconDecoration(label: 'LED', iconData: DEVICE_LED_ICON, truthiness: floidStatusState.floidStatus.dl), // LED
                                                  FloidIconDecoration(label: 'GPS', iconData: DEVICE_GPS_ICON, truthiness: floidStatusState.floidStatus.dg), // GPS
                                                  FloidIconDecoration(label: 'IMU', iconData: DEVICE_IMU_ICON, truthiness: floidStatusState.floidStatus.dp), // IMU
                                                  FloidIconDecoration(label: 'HEL', iconData: DEVICE_HELIS_ICON, truthiness: floidStatusState.floidStatus.dh), // Helis
                                                ],
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  FloidIconDecoration(label: 'PAR', iconData: DEVICE_PARACHUTE_ICON, truthiness: floidStatusState.floidStatus.de),
                                                  // Parachute
                                                  FloidIconDecoration(label: 'AUX', iconData: DEVICE_AUX_ICON, truthiness: floidStatusState.floidStatus.da),
                                                  // Aux
                                                  FloidIconDecoration(label: 'DES', iconData: DEVICE_DESIGNATOR_ICON, truthiness: floidStatusState.floidStatus.dd),
                                                  // Designator
                                                ],
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  // Camera
                                                  FloidIconDecoration(label: 'CAM', iconData: DEVICE_CAMERA_ICON, truthiness: floidStatusState.floidStatus.dc),
                                                  // Camera follow mode
                                                  FloidIconDecoration(label: 'CFM', iconData: DEVICE_CAMERA_FOLLOW_MODE_ICON, truthiness: floidStatusState.floidStatus.fmf),
                                                  // Droid Gps
                                                  FloidIconDecoration(label: 'DGP', iconData: DEVICE_GPS_FROM_DEVICE_ICON, truthiness: floidStatusState.floidStatus.dv),
                                                  // GPS Emulate
                                                  FloidIconDecoration(label: 'GEM', iconData: DEVICE_GPS_EMULATE_ICON, truthiness: floidStatusState.floidStatus.df),
                                                ],
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(0.0, 2.0, 0.0, 0.0),
                                                child: Container(
                                                  color: SERVOS_BACKGROUND_COLOR,
                                                  child: Column(
                                                    mainAxisSize: MainAxisSize.min,
                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                    children: <Widget>[
                                                      // Labels:
                                                      Container(
                                                        width: 140,
                                                        color: Colors.yellowAccent,
                                                        child: Row(
                                                          mainAxisSize: MainAxisSize.min,
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: [
                                                            Padding(
                                                              padding: EdgeInsets.all(0.0),
                                                              child: Text('SERVOS', style: TextStyle(color: Colors.black, fontSize: SECTION_LABEL_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      // ESC's:
                                                      Row(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: [
                                                          Padding(
                                                            padding: EdgeInsets.fromLTRB(0, 2, 0, 0),
                                                            child: Text('E', style: TextStyle(color: Colors.yellowAccent, fontSize: INDICATOR_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: floidStatusState.floidStatus.e0,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.e0o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: floidStatusState.floidStatus.e1,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.e1o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: floidStatusState.floidStatus.e2,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.e2o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: floidStatusState.floidStatus.e3,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.e3o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      // Left:
                                                      Row(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: [
                                                          Padding(
                                                            padding: EdgeInsets.all(0.0),
                                                            child: Text('L', style: TextStyle(color: Colors.yellowAccent, fontSize: INDICATOR_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignLeft == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s00 : 1.0 - floidStatusState.floidStatus.s00,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s0o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignLeft == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s10 : 1.0 - floidStatusState.floidStatus.s10,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s1o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignLeft == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s20 : 1.0 - floidStatusState.floidStatus.s20,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s2o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignLeft == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s30 : 1.0 - floidStatusState.floidStatus.s30,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s3o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      // Right:
                                                      Row(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: [
                                                          Padding(
                                                            padding: EdgeInsets.all(0.0),
                                                            child: Text('R', style: TextStyle(color: Colors.yellowAccent, fontSize: INDICATOR_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignRight == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s01 : 1.0 - floidStatusState.floidStatus.s01,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s0o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignRight == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s11 : 1.0 - floidStatusState.floidStatus.s11,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s1o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignRight == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s21 : 1.0 - floidStatusState.floidStatus.s21,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s2o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignRight == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s31 : 1.0 - floidStatusState.floidStatus.s31,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s3o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      // Pitch:
                                                      Row(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: [
                                                          Padding(
                                                            padding: EdgeInsets.all(0.0),
                                                            child: Text('P', style: TextStyle(color: Colors.yellowAccent, fontSize: INDICATOR_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignPitch == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s02 : 1.0 - floidStatusState.floidStatus.s02,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s0o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignPitch == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s12 : 1.0 - floidStatusState.floidStatus.s12,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s1o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignPitch == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s22 : 1.0 - floidStatusState.floidStatus.s22,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s2o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding: EdgeInsets.all(1.0),
                                                            child: new LinearPercentIndicator(
                                                              padding: const EdgeInsets.symmetric(horizontal: 0.0),
                                                              width: INDICATOR_WIDTH,
                                                              lineHeight: INDICATOR_LINE_HEIGHT,
                                                              percent: (floidModelParametersState is FloidModelParametersLoaded
                                                                  ? floidModelParametersState.floidModelParameters.servoSignPitch == 1
                                                                  : true)
                                                                  ? floidStatusState.floidStatus.s32 : 1.0 - floidStatusState.floidStatus.s32,
                                                              barRadius: const Radius.circular(1),
                                                              progressColor: PROGRESS_FOREGROUND_COLOR,
                                                              backgroundColor: floidStatusState.floidStatus.s3o ? PROGRESS_BACKGROUND_COLOR_ON : PROGRESS_BACKGROUND_COLOR_OFF,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: EdgeInsets.fromLTRB(0, 5.0, 0, 0),
                                                // Orientation:
                                                child: Container(
                                                  color: Color(0xFFFFFF00),
                                                  width: 140,
                                                  child: Row(
                                                    mainAxisSize: MainAxisSize.min,
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Text('DIRECTION', style: TextStyle(color: Colors.black, fontSize: SECTION_LABEL_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: <Widget>[
                                                  // Facing (Orientation):
                                                  Container(
                                                    margin: EdgeInsets.all(0),
                                                    color: DIAL_BACKGROUND_COLOR,
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Stack(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Container(
                                                                  width: DIAL_SIZE,
                                                                  height: DIAL_SIZE,
                                                                  decoration: new BoxDecoration(
                                                                    color: DIAL_CIRCLE_COLOR,
                                                                    shape: BoxShape.circle,
                                                                  ),
                                                                ),
                                                              ),
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Transform.rotate(
                                                                  angle: Distance.degreeToCompass(floidStatusState.floidStatus.phs) * (math.pi / 180),
                                                                  child:
                                                                  Icon(DIAL_ARROW_ICON, color: DIAL_INDICATOR_COLOR, size: DIAL_SIZE,),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Text('Facing', style: TextStyle(color: DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                          Text(Distance.degreeToCompass(floidStatusState.floidStatus.phs).toStringAsFixed(1) + '°',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                          Text('', style: TextStyle(color: DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  // Facing (Orientation) Goal:
                                                  Container(
                                                    margin: EdgeInsets.all(0),
                                                    color: DIAL_BACKGROUND_COLOR,
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Stack(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Container(
                                                                  width: DIAL_SIZE,
                                                                  height: DIAL_SIZE,
                                                                  decoration: new BoxDecoration(
                                                                    color: DIAL_CIRCLE_COLOR,
                                                                    shape: BoxShape.circle,
                                                                  ),
                                                                ),
                                                              ),
                                                              // If we have goal (ghg), display the orientation goal (goal angle - ga)
                                                              if(floidStatusState.floidStatus.ghg)
                                                                Align(
                                                                  alignment: Alignment.center,
                                                                  child: Transform.rotate(
                                                                    angle: Distance.degreeToCompass(floidStatusState.floidStatus.ga) * (math.pi / 180),
                                                                    child:
                                                                    Icon(DIAL_ARROW_ICON, color: GOAL_DIAL_INDICATOR_COLOR, size: DIAL_SIZE,),
                                                                  ),
                                                                ),
                                                            ],
                                                          ),
                                                          Text('Goal', style: TextStyle(color: GOAL_DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                          Text(floidStatusState.floidStatus.ghg ? Distance.degreeToCompass(floidStatusState.floidStatus.ga).toStringAsFixed(1) + '°' : '',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: GOAL_DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                          Text('', style: TextStyle(color: GOAL_DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  // Moving:
                                                  Container(
                                                    margin: EdgeInsets.all(0),
                                                    color: DIAL_BACKGROUND_COLOR,
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Stack(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Container(
                                                                  width: DIAL_SIZE,
                                                                  height: DIAL_SIZE,
                                                                  decoration: new BoxDecoration(
                                                                    color: DIAL_CIRCLE_COLOR,
                                                                    shape: BoxShape.circle,
                                                                  ),
                                                                ),
                                                              ),
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Transform.rotate(
                                                                  angle: Distance.degreeToCompass(floidStatusState.floidStatus.kd) * (math.pi / 180),
                                                                  child:
                                                                  Icon(DIAL_ARROW_ICON, color: DIAL_INDICATOR_COLOR, size: DIAL_SIZE,),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Text('Moving', style: TextStyle(color: DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                          Text(Distance.degreeToCompass(floidStatusState.floidStatus.kd).toStringAsFixed(1) + '°',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                          Text(floidStatusState.floidStatus.kv.toStringAsFixed(1) + 'm/s',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  // Target Direction:
                                                  Container(
                                                    margin: EdgeInsets.all(0),
                                                    color: DIAL_BACKGROUND_COLOR,
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Stack(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Container(
                                                                  width: DIAL_SIZE,
                                                                  height: DIAL_SIZE,
                                                                  decoration: new BoxDecoration(
                                                                    color: DIAL_CIRCLE_COLOR,
                                                                    shape: BoxShape.circle,
                                                                  ),
                                                                ),
                                                              ),
                                                              // If we have a goal:
                                                              if(floidStatusState.floidStatus.ghg)
                                                                Align(
                                                                  alignment: Alignment.center,
                                                                  child: Transform.rotate(
                                                                    angle: Distance.degreeToCompass(floidStatusState.floidStatus.mo) * (math.pi / 180),
                                                                    child:
                                                                    Icon(DIAL_ARROW_ICON, color: GOAL_DIAL_INDICATOR_COLOR, size: DIAL_SIZE,),
                                                                  ),
                                                                ),
                                                            ],
                                                          ),
                                                          Text('Target', style: TextStyle(color: GOAL_DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                          Text(floidStatusState.floidStatus.ghg ? Distance.degreeToCompass(floidStatusState.floidStatus.mo).toStringAsFixed(1) + '°' : '',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: GOAL_DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                          // ov is target velocity over ground:
                                                          Text(floidStatusState.floidStatus.ghg ? floidStatusState.floidStatus.ov.toStringAsFixed(1) + ' m/s' : '',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: GOAL_DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              // Label:
                                              Container(
                                                color: Color(0xFFFFFF00),
                                                width: 140,
                                                child: Row(
                                                  mainAxisSize: MainAxisSize.min,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.all(0.0),
                                                      child: Text('DISTANCE / MODE', style: TextStyle(color: Colors.black, fontSize: SECTION_LABEL_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              // Distance to target row:
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: <Widget>[
                                                  // Distance to Target:
                                                  Padding(
                                                    padding: const EdgeInsets.fromLTRB(1.0, 0.0, 1.0, 0.0),
                                                    child: Column(
                                                      children: [
                                                        Text('Distance',
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            color: DIAL_INDICATOR_COLOR,
                                                            fontSize: PITCH_DIAL_FONT_SIZE,
                                                          ),),
                                                        Text(floidStatusState.floidStatus.ghg ? floidStatusState.floidStatus.md.toStringAsFixed(1) + ' m' : '0 m',
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            color: GOAL_DIAL_INDICATOR_COLOR,
                                                            fontSize: PITCH_DIAL_FONT_SIZE,
                                                          ),),
                                                        Text('To Target',
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            color: GOAL_DIAL_INDICATOR_COLOR,
                                                            fontSize: PITCH_DIAL_FONT_SIZE,
                                                          ),),
                                                      ],
                                                    ),
                                                  ),
                                                  // Altitude to target:
                                                  Padding(
                                                    padding: const EdgeInsets.fromLTRB(1.0, 0.0, 1.0, 0.0),
                                                    child: Column(
                                                      children: [
                                                        Text('Altitude',
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            color: DIAL_INDICATOR_COLOR,
                                                            fontSize: PITCH_DIAL_FONT_SIZE,
                                                          ),),
                                                        Text(floidStatusState.floidStatus.ghg ? (floidStatusState.floidStatus.ka - floidStatusState.floidStatus.gz).toStringAsFixed(1) + ' m' : '0 m',
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            color: GOAL_DIAL_INDICATOR_COLOR,
                                                            fontSize: PITCH_DIAL_FONT_SIZE,
                                                          ),),
                                                        Text('To Target',
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                            color: GOAL_DIAL_INDICATOR_COLOR,
                                                            fontSize: PITCH_DIAL_FONT_SIZE,
                                                          ),),
                                                      ],
                                                    ),
                                                  ),
                                                  // Mode:
                                                  Column(
                                                    children: [
                                                      Text('Mode',
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(
                                                          color: DIAL_INDICATOR_COLOR,
                                                          fontSize: PITCH_DIAL_FONT_SIZE,
                                                        ),),
                                                      Text(floidStatusState.floidStatus.glo ? 'Lift Off': floidStatusState.floidStatus.gld? 'Land':'Normal',
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(
                                                          color: (floidStatusState.floidStatus.glo | floidStatusState.floidStatus.gld) ? GOAL_DIAL_INDICATOR_COLOR: DIAL_INDICATOR_COLOR,
                                                          fontSize: PITCH_DIAL_FONT_SIZE,
                                                        ),),
                                                      Text('',
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(
                                                          color: DIAL_INDICATOR_COLOR,
                                                          fontSize: PITCH_DIAL_FONT_SIZE,
                                                        ),),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              // Label:
                                              Container(
                                                color: Color(0xFFFFFF00),
                                                width: 140,
                                                child: Row(
                                                  mainAxisSize: MainAxisSize.min,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.all(0.0),
                                                      child: Text('ATTITUDE', style: TextStyle(color: Colors.black, fontSize: SECTION_LABEL_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: <Widget>[
                                                  // Pitch:
                                                  Container(
                                                    margin: EdgeInsets.all(0),
                                                    color: DIAL_BACKGROUND_COLOR,
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Stack(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Container(
                                                                  width: DIAL_SIZE,
                                                                  height: DIAL_SIZE,
                                                                  decoration: new BoxDecoration(
                                                                    color: DIAL_CIRCLE_COLOR,
                                                                    shape: BoxShape.circle,
                                                                  ),
                                                                ),
                                                              ),
                                                              Positioned.fill(
                                                                child: Align(
                                                                  alignment: Alignment.center,
                                                                  child: Text(floidStatusState.floidStatus.pps.toStringAsFixed(1) + '°',
                                                                    textAlign: TextAlign.center,
                                                                    style: TextStyle(
                                                                      color: DIAL_INDICATOR_COLOR,
                                                                      fontSize: PITCH_DIAL_FONT_SIZE,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Text('Pitch', style: TextStyle(color: DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                          Text(floidStatusState.floidStatus.pps.toStringAsFixed(1) + '°',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  // Roll:
                                                  Container(
                                                    margin: EdgeInsets.all(0),
                                                    color: DIAL_BACKGROUND_COLOR,
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Stack(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Container(
                                                                  width: DIAL_SIZE,
                                                                  height: DIAL_SIZE,
                                                                  decoration: new BoxDecoration(
                                                                    color: DIAL_CIRCLE_COLOR,
                                                                    shape: BoxShape.circle,
                                                                  ),
                                                                ),
                                                              ),
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Transform.rotate(
                                                                  angle: (-floidStatusState.floidStatus.prs) * (math.pi / 180),
                                                                  child:
                                                                  Icon(DIAL_ROLL_ICON, color: DIAL_INDICATOR_COLOR, size: DIAL_SIZE,),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Text('Roll', style: TextStyle(color: DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                          Text(floidStatusState.floidStatus.prs.toStringAsFixed(1) + '°',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  // Attack Angle:
                                                  Container(
                                                    margin: EdgeInsets.all(0),
                                                    color: DIAL_BACKGROUND_COLOR,
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Stack(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Container(
                                                                  width: DIAL_SIZE,
                                                                  height: DIAL_SIZE,
                                                                  decoration: new BoxDecoration(
                                                                    color: DIAL_CIRCLE_COLOR,
                                                                    shape: BoxShape.circle,
                                                                  ),
                                                                ),
                                                              ),
                                                              Positioned.fill(
                                                                child: Align(
                                                                  alignment: Alignment.center,
                                                                  child: Text(floidStatusState.floidStatus.mk.toStringAsFixed(1) + '°',
                                                                    textAlign: TextAlign.center,
                                                                    style: TextStyle(
                                                                      color: GOAL_DIAL_INDICATOR_COLOR,
                                                                      fontSize: PITCH_DIAL_FONT_SIZE,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Text('Attack', style: TextStyle(color: GOAL_DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                          Text('',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              // Label:
                                              Container(
                                                color: Color(0xFFFFFF00),
                                                width: 140,
                                                child: Row(
                                                  mainAxisSize: MainAxisSize.min,
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    Padding(
                                                      padding: EdgeInsets.all(0.0),
                                                      child: Text('ALTITUDE', style: TextStyle(color: Colors.black, fontSize: SECTION_LABEL_FONT_SIZE, fontWeight: FontWeight.w700,),),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: <Widget>[
                                                  // Altitude:
                                                  Container(
                                                    margin: EdgeInsets.all(0),
                                                    color: DIAL_BACKGROUND_COLOR,
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Stack(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Container(
                                                                  width: DIAL_SIZE,
                                                                  height: DIAL_SIZE,
                                                                  decoration: new BoxDecoration(
                                                                    color: DIAL_CIRCLE_COLOR,
                                                                    shape: BoxShape.circle,
                                                                  ),
                                                                ),
                                                              ),
                                                              Positioned.fill(
                                                                child: Align(
                                                                  alignment: Alignment.center,
                                                                  child: Text(floidStatusState.floidStatus.ka.toStringAsFixed(1) + ' m',
                                                                    textAlign: TextAlign.center,
                                                                    style: TextStyle(
                                                                      color: DIAL_INDICATOR_COLOR,
                                                                      fontSize: PITCH_DIAL_FONT_SIZE,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Text('Altitude', style: TextStyle(color: DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                          Text(floidStatusState.floidStatus.pzv.toStringAsFixed(1) + ' m/s',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  // Altitude Goal:
                                                  Container(
                                                    margin: EdgeInsets.all(0),
                                                    color: DIAL_BACKGROUND_COLOR,
                                                    child: Padding(
                                                      padding: const EdgeInsets.fromLTRB(2.0, 2.0, 2.0, 0.0),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Stack(
                                                            children: <Widget>[
                                                              Align(
                                                                alignment: Alignment.center,
                                                                child: Container(
                                                                  width: DIAL_SIZE,
                                                                  height: DIAL_SIZE,
                                                                  decoration: new BoxDecoration(
                                                                    color: DIAL_CIRCLE_COLOR,
                                                                    shape: BoxShape.circle,
                                                                  ),
                                                                ),
                                                              ),
                                                              Positioned.fill(
                                                                child: Align(
                                                                  alignment: Alignment.center,
                                                                  child: Text(floidStatusState.floidStatus.gz.toStringAsFixed(1) + ' m',
                                                                    textAlign: TextAlign.center,
                                                                    style: TextStyle(
                                                                      color: GOAL_DIAL_INDICATOR_COLOR,
                                                                      fontSize: PITCH_DIAL_FONT_SIZE,
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          Text('Goal', style: TextStyle(color: GOAL_DIAL_TEXT_COLOR, fontSize: DIAL_FONT_SIZE)),
                                                          Text(floidStatusState.floidStatus.oav.toStringAsFixed(1) + ' m/s',
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                              color: GOAL_DIAL_INDICATOR_COLOR,
                                                              fontSize: PITCH_DIAL_FONT_SIZE,
                                                            ),),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  },
                );
              },
            );
          },
        );
      },
    );
  }
}
