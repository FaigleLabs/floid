/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:io';

import 'package:floid_ui_app/app/floid_settings.dart';
import 'package:floid_ui_app/ui/settings/settings_bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:flutter/services.dart';

class SimpleBlocObserver extends BlocObserver {
  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    print(error);
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // For the app version we explicitly trust the floid_server cert
  // from the build. For the web (browser) version the user must
  // accept the certificate in a browser window
  if(!kIsWeb) {
    ByteData data = await rootBundle.load('assets/raw/floid_web.crt');
    SecurityContext context = SecurityContext.defaultContext;
    context.setTrustedCertificatesBytes(data.buffer.asUint8List());
  }
  // Create our BLoC delegate:
  Bloc.observer = SimpleBlocObserver();
  // Create a user repository:
  final userRepository = UserRepository();

  // Run the app:
  runApp(
    // Create an outer settings bloc:
      BlocProvider<SettingsBloc>(
        create: (context) => SettingsBloc()..add(FetchSettings()),
        child: new FloidSettings(key: null, userRepository: userRepository),
      )
  );
}
