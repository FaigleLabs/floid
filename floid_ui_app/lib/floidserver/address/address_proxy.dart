/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'address_stub.dart'
  if (dart.library.js) 'web_address_proxy.dart'
  if (dart.library.io) 'app_address_proxy.dart';


abstract class AddressProxy {
  static const String HOST_PREFERENCES_KEY = 'host';
  static const String PORT_PREFERENCES_KEY = 'port';
  static const String SECURE_PREFERENCES_KEY = 'secure';
  static const String FLOID_PREFERENCES_KEY = 'floid';
  static const String MISSION_PREFERENCES_KEY = 'mission';
  static const String PIN_SET_PREFERENCES_KEY = 'pinSet';

  Future<String> getHost();
  Future<int> getPort();
  Future<bool> getSecure();

  void saveHost(String host);
  void savePort(int port);
  void saveSecure(bool secure);

  Future<int> getLastFloid();
  Future<int> getLastMission();
  Future<int> getLastPinSet();

  void saveLastFloid(int floidId);
  void saveLastMission(int missionId);
  void saveLastPinSet(int pinSetId);

  /// factory constructor to return the correct implementation.
  factory AddressProxy() => getAddressProxy();
}
