 /*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/address/address_proxy.dart';
// ignore: avoid_web_libraries_in_flutter
import 'dart:html';
import 'package:shared_preferences/shared_preferences.dart';

class WebAddressProxy implements AddressProxy {

      Future<String> getHost() async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            String host;
            if (sharedPreferences.containsKey(AddressProxy.HOST_PREFERENCES_KEY)) {
                  host = sharedPreferences.getString(AddressProxy.HOST_PREFERENCES_KEY)??'floid.faiglelabs.com';
            } else {
                  host = window.location.hostname??'floid.faiglelabs.com';
                  String href = window.location.href;
                  String protocol = window.location.protocol;
                  String port = window.location.port;
                  print('Window href: $href');
                  print('Window host: $host');
                  print('Window protocol: $protocol');
                  print('Window port: $port');
            }
            return host;
      }

      Future<int> getPort() async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            // Hardcoded initial defaults:
            int port = 443;
            if (sharedPreferences.containsKey(AddressProxy.PORT_PREFERENCES_KEY)) {
                  port = sharedPreferences.getInt(AddressProxy.PORT_PREFERENCES_KEY)??443;
            } else {
                  if(window.location.port.length>0) {
                        try {
                              port = int.parse(window.location.port);
                        } catch(_) {
                              // Do nothing...
                              port = await getSecure()?443:80;
                        }
                  }
                  // And store it in stored preferences:
                  sharedPreferences.setInt(AddressProxy.PORT_PREFERENCES_KEY, port);
            }
            return port;
      }

      Future<bool> getSecure() async {
            bool secure = true;
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            if (sharedPreferences.containsKey(AddressProxy.SECURE_PREFERENCES_KEY)) {
                  secure = sharedPreferences.getBool(AddressProxy.SECURE_PREFERENCES_KEY)??secure;
            } else {
                  // Hardcoded initial defaults:
                  secure = window.location.protocol.toLowerCase() == 'https:';
                  // And store it in stored preferences:
                  sharedPreferences.setBool(AddressProxy.SECURE_PREFERENCES_KEY, secure);
            }
            return secure;
      }

      void saveHost(String host) async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            sharedPreferences.setString(AddressProxy.HOST_PREFERENCES_KEY, host);
      }

      void savePort(int port) async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            // And store it in stored preferences:
            sharedPreferences.setInt(AddressProxy.PORT_PREFERENCES_KEY, port);
      }

      void saveSecure(bool secure) async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            sharedPreferences.setBool(AddressProxy.SECURE_PREFERENCES_KEY, secure);
      }

      Future<int> getLastFloid() async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            try {
                  if (sharedPreferences.containsKey(AddressProxy.FLOID_PREFERENCES_KEY)) {
                        return sharedPreferences.getInt(AddressProxy.FLOID_PREFERENCES_KEY)??-1;
                  }
            } catch (_) {}
            return -1;
      }

      Future<int> getLastMission() async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            try {
                  if (sharedPreferences.containsKey(AddressProxy.MISSION_PREFERENCES_KEY)) {
                        return sharedPreferences.getInt(AddressProxy.MISSION_PREFERENCES_KEY)??-1;
                  }
            } catch (_) {}
            return -1;
      }
      Future<int> getLastPinSet() async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            try {
                  if (sharedPreferences.containsKey(AddressProxy.PIN_SET_PREFERENCES_KEY)) {
                        return sharedPreferences.getInt(AddressProxy.PIN_SET_PREFERENCES_KEY)??-1;
                  }
            } catch (_) {}
            return -1;
      }

      void saveLastFloid(int floidId) async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            sharedPreferences.setInt(AddressProxy.FLOID_PREFERENCES_KEY, floidId);
      }

      void saveLastMission(int missionId) async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            sharedPreferences.setInt(AddressProxy.MISSION_PREFERENCES_KEY, missionId);
      }

      void saveLastPinSet(int pinSetId) async {
            SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
            sharedPreferences.setInt(AddressProxy.PIN_SET_PREFERENCES_KEY, pinSetId);
      }

}

AddressProxy getAddressProxy() => WebAddressProxy();