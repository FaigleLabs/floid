/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/bloc/droid_track_filtered_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/droid_track_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_track_filtered_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_track_gpx_file.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';

class FloidUuidProvider extends StatelessWidget {
  final FloidServerRepository floidServerRepository;
  final Widget child;

  FloidUuidProvider({Key? key, required this.floidServerRepository,
    required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<FloidDroidStatusBloc>(
          create: (context) =>
              FloidDroidStatusBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidStatusBloc>(
          create: (context) =>
              FloidStatusBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidDroidMessageBloc>(
          create: (context) =>
              FloidDroidMessageBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidModelParametersBloc>(
          create: (context) =>
              FloidModelParametersBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidModelStatusBloc>(
          create: (context) =>
              FloidModelStatusBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidDroidPhotoBloc>(
          create: (context) =>
              FloidDroidPhotoBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidDroidVideoFrameBloc>(
          create: (context) =>
              FloidDroidVideoFrameBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidDroidMissionInstructionStatusMessageBloc>(
          create: (context) =>
              FloidDroidMissionInstructionStatusMessageBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidGpsPointsFilteredBloc>(
          create: (context) =>
              FloidGpsPointsFilteredBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidDroidGpsPointsFilteredBloc>(
          create: (context) =>
              FloidDroidGpsPointsFilteredBloc(floidServerRepository: floidServerRepository),
        ),
        // Holds floid track filtered gpx file:
        BlocProvider<FloidTrackFilteredGpxFileBloc>(
          create: (context) => FloidTrackFilteredGpxFileBloc(),
        ),
        // Holds floid track gpx file:
        BlocProvider<FloidTrackGpxFileBloc>(
          create: (context) => FloidTrackGpxFileBloc(),
        ),
        // Holds droid track gpx file:
        BlocProvider<DroidTrackGpxFileBloc>(
          create: (context) => DroidTrackGpxFileBloc(),
        ),
        // Holds droid track filtered gpx file:
        BlocProvider<DroidTrackFilteredGpxFileBloc>(
          create: (context) => DroidTrackFilteredGpxFileBloc(),
        ),
        BlocProvider<FloidGpsPointsBloc>(
          create: (context) =>
              FloidGpsPointsBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidDroidGpsPointsBloc>(
          create: (context) =>
              FloidDroidGpsPointsBloc(floidServerRepository: floidServerRepository),
        ),
        BlocProvider<FloidTrackGpxFileBloc>(
          create: (context) =>
              FloidTrackGpxFileBloc(),
        ),
        BlocProvider<DroidTrackGpxFileBloc>(
          create: (context) =>
              DroidTrackGpxFileBloc(),
        ),
      ],
      child: child,
    );
  }
}