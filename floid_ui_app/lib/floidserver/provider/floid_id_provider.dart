/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/bloc/stomp/floid_new_floid_uuid_bloc.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FloidIdProvider extends StatelessWidget {
  final FloidServerRepository floidServerRepository;
  final Widget child;

  FloidIdProvider({Key? key, required this.floidServerRepository,
    required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
      return MultiBlocProvider(
      providers: [
        BlocProvider<FloidNewFloidUuidBloc>(
          create: (context) =>
              FloidNewFloidUuidBloc(floidServerRepository: floidServerRepository),
        ),
      ],
      child: child,
    );
  }
}