/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/widgets/floid_server_status_poller.dart';
import 'package:floid_ui_app/ui/widgets/floid_status_poller.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_map_gpx_file.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/bloc/stomp/stomp.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:floid_ui_app/ui/map/line/line.dart';
import 'package:floid_ui_app/ui/map/marker/marker.dart';

/// These providers only require a repository:
class FloidServerProvider extends StatelessWidget {
  final FloidServerRepository floidServerRepository;
  final Widget child;

    FloidServerProvider({Key? key, required this.floidServerRepository,
    required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        // App State Provider:
        BlocProvider<FloidAppStateBloc>(
          create: (context) => FloidAppStateBloc()..add(LoadAppStateEvent()),
        ),
        // Subscribe to new floid id messages:
        BlocProvider<FloidNewFloidIdBloc>(
          create: (context) => FloidNewFloidIdBloc(floidServerRepository: floidServerRepository)..add(StartFloidNewFloidIdEvent()),
        ),
        // Subscribe to the server status:
        BlocProvider<FloidServerStatusBloc>(
          create: (context) => FloidServerStatusBloc(floidServerRepository: floidServerRepository)..add(StartFloidServerStatusEvent()),
        ),
        // Get the list of id's and states:
        BlocProvider<FloidIdAndStateListBloc>(
          create: (context) => FloidIdAndStateListBloc(floidServerRepository: floidServerRepository)..add(FetchFloidIdAndStateList()),
        ),
        // Get the list of floid id's:
        BlocProvider<FloidListBloc>(
          create: (context) => FloidListBloc(floidServerRepository: floidServerRepository)..add(FetchFloidList()),
        ),
        // Get the list of missions:
        BlocProvider<FloidMissionListBloc>(
          create: (context) => FloidMissionListBloc(floidServerRepository: floidServerRepository)..add(FetchFloidMissionList()),
        ),
        // Get the list of pin sets:
        BlocProvider<FloidPinSetListBloc>(
          create: (context) => FloidPinSetListBloc(floidServerRepository: floidServerRepository)..add(FetchFloidPinSetList()),
        ),
        // Get pin set (triggers when pin set list loads):
        BlocProvider<FloidPinSetBloc>(
          create: (context) => FloidPinSetBloc(floidServerRepository: floidServerRepository),
        ),
        // Get mission (triggered when mission list loads):
        BlocProvider<FloidMissionBloc>(
          create: (context) => FloidMissionBloc(floidServerRepository: floidServerRepository),
        ),
        // Holds floid id and uuid (triggered when floid id and list bloc loads):
        BlocProvider<FloidIdBloc>(
          create: (context) => FloidIdBloc(floidServerRepository: floidServerRepository),
        ),
        // Get the list of Uuid's for this id and their states:
        BlocProvider<FloidUuidAndStateListBloc>(
          create: (context) => FloidUuidAndStateListBloc(floidServerRepository: floidServerRepository),
        ),
        // Map line providers:
        // Floid GPS lines:
        BlocProvider<FloidMapLinesBloc<FloidMapFloidGpsLine>>(
          create: (context) => FloidMapLinesBloc<FloidMapFloidGpsLine>(),
        ),
        // Droid GPS lines:
        BlocProvider<FloidMapLinesBloc<FloidMapFloidDroidGpsLine>>(
          create: (context) => FloidMapLinesBloc<FloidMapFloidDroidGpsLine>(),
        ),
        // Designate lines:
        BlocProvider<FloidMapLinesBloc<FloidMapDesignateLine>>(
          create: (context) => FloidMapLinesBloc<FloidMapDesignateLine>(),
        ),
        // Fly lines:
        BlocProvider<FloidMapLinesBloc<FloidMapFlyLine>>(
          create: (context) => FloidMapLinesBloc<FloidMapFlyLine>(),
        ),
        // Floid Status Marker:
        BlocProvider<FloidMapMarkerBloc<FloidStatusMarker>>(
          create: (context) => FloidMapMarkerBloc<FloidStatusMarker>(),
        ),
        // Floid Home Marker:
        BlocProvider<FloidMapMarkerBloc<FloidAcquiredHomeMarker>>(
          create: (context) => FloidMapMarkerBloc<FloidAcquiredHomeMarker>(),
        ),
        // Floid Target Marker:
        BlocProvider<FloidMapMarkerBloc<FloidTargetMarker>>(
          create: (context) => FloidMapMarkerBloc<FloidTargetMarker>(),
        ),
        // Floid FLy Line Path Elevation Markers:
        BlocProvider<FloidMapMarkersBloc<FloidFlyLinePathElevationMarker>>(
          create: (context) => FloidMapMarkersBloc<FloidFlyLinePathElevationMarker>(),
        ),
        // Floid Mission Home Markers:
        BlocProvider<FloidMapMarkersBloc<FloidMissionHomeMarker>>(
          create: (context) => FloidMapMarkersBloc<FloidMissionHomeMarker>(),
        ),
        // Floid Pin Markers:
        BlocProvider<FloidMapMarkersBloc<FloidPinMarker>>(
          create: (context) => FloidMapMarkersBloc<FloidPinMarker>(),
        ),
        // Holds floid id and uuid (triggered when floid id and list bloc loads):
        BlocProvider<FloidMessagesBloc>(
          create: (context) => FloidMessagesBloc(),
        ),
        // Holds floid mission over pins as a gpx file:
        BlocProvider<FloidMapGpxFileBloc>(
          create: (context) => FloidMapGpxFileBloc(),
        ),
      ],
      // Pollers to query server for current state:
      child: FloidServerStatusPoller(
          child: FloidStatusPoller(
              child: child
          )
      ),
    );
  }
}
