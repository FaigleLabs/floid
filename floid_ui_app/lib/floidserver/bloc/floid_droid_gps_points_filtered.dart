/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:floid_ui_app/ui/map/line/line.dart';
import 'package:floid_ui_app/ui/map/utility/simplify.dart';

abstract class FloidDroidGpsPointsFilteredEvent extends Equatable {
  FloidDroidGpsPointsFilteredEvent([List props = const []]) : super();
}

class FetchFloidDroidGpsPointsFiltered extends FloidDroidGpsPointsFilteredEvent {
  final int floidId;
  final String floidUuid;
  FetchFloidDroidGpsPointsFiltered({required this.floidId,
    required this.floidUuid})
      : super([floidId, floidUuid]);
  @override
  List<Object> get props => [floidId, floidUuid];
}

class RefreshFloidDroidGpsPointsFiltered extends FloidDroidGpsPointsFilteredEvent {
  final int floidId;
  final String floidUuid;

  RefreshFloidDroidGpsPointsFiltered({required this.floidId,
    required this.floidUuid})
      : super([floidId, floidUuid]);
  @override
  List<Object> get props => [floidId, floidUuid];
}

class AddFloidDroidGpsPointFiltered extends FloidDroidGpsPointsFilteredEvent {
  final FloidGpsPoint floidGpsPoint;
  final int floidId;
  final String floidUuid;
  final String timestamp;

  AddFloidDroidGpsPointFiltered({required this.floidGpsPoint, required this.floidId, required this.floidUuid, required this.timestamp})
      : super([floidGpsPoint, floidId, floidUuid, timestamp]);
  @override
  List<Object> get props => [floidGpsPoint, floidId, floidUuid, timestamp];
}

abstract class FloidDroidGpsPointsFilteredState extends Equatable {
  FloidDroidGpsPointsFilteredState([List props = const []]) : super();
}

class FloidDroidGpsPointsFilteredEmpty extends FloidDroidGpsPointsFilteredState {
  @override
  List<Object> get props => [];
}

class FloidDroidGpsPointsFilteredLoading extends FloidDroidGpsPointsFilteredState {
  @override
  List<Object> get props => [];
}

class FloidDroidGpsPointsFilteredLoaded extends FloidDroidGpsPointsFilteredState {
  final List<FloidGpsPoint> gpsPoints;
  final int floidId;
  final String floidUuid;
  final String timestamp;

  FloidDroidGpsPointsFilteredLoaded({required this.gpsPoints, required this.floidId, required this.floidUuid, required this.timestamp})
      : super([gpsPoints, floidId, floidUuid, timestamp]);
  @override
  List<Object> get props => [gpsPoints, floidId, floidUuid, timestamp];
}

class FloidDroidGpsPointsFilteredError extends FloidDroidGpsPointsFilteredState {
  @override
  List<Object> get props => [];
}

class FloidDroidGpsPointsFilteredBloc extends Bloc<FloidDroidGpsPointsFilteredEvent, FloidDroidGpsPointsFilteredState> {
  final FloidServerRepository floidServerRepository;
  FloidLineConfig floidLineConfig = FloidLineConfig(kink: FloidGpsPointsBloc.DEFAULT_KINK,
      kinkAlpha: FloidGpsPointsBloc.DEFAULT_KINK_ALPHA,
      shrinkAlpha: FloidGpsPointsBloc.DEFAULT_SHRINK_ALPHA,
      expandAlpha: FloidGpsPointsBloc.DEFAULT_EXPAND_ALPHA,
      maxPointsStart: FloidGpsPointsBloc.DEFAULT_MAX_POINTS_START,
      minTime: FloidGpsPointsBloc.DEFAULT_MIN_TIME,
      minDistance: FloidGpsPointsBloc.DEFAULT_MIN_DISTANCE,
      highQuality: FloidGpsPointsBloc.DEFAULT_HIGH_QUALITY);

  FloidDroidGpsPointsFilteredBloc({required this.floidServerRepository})
      : super(FloidDroidGpsPointsFilteredEmpty()) {
    on<FloidDroidGpsPointsFilteredEvent>((FloidDroidGpsPointsFilteredEvent event, Emitter<FloidDroidGpsPointsFilteredState> emit) async {
      if (event is FetchFloidDroidGpsPointsFiltered) {
        floidLineConfig = FloidLineConfig(kink: FloidGpsPointsBloc.DEFAULT_KINK,
            kinkAlpha: FloidGpsPointsBloc.DEFAULT_KINK_ALPHA,
            shrinkAlpha: FloidGpsPointsBloc.DEFAULT_SHRINK_ALPHA,
            expandAlpha: FloidGpsPointsBloc.DEFAULT_EXPAND_ALPHA,
            maxPointsStart: FloidGpsPointsBloc.DEFAULT_MAX_POINTS_START,
            minTime: FloidGpsPointsBloc.DEFAULT_MIN_TIME,
            minDistance: FloidGpsPointsBloc.DEFAULT_MIN_DISTANCE,
            highQuality: FloidGpsPointsBloc.DEFAULT_HIGH_QUALITY);
        emit(FloidDroidGpsPointsFilteredLoading());
        try {
          final List<FloidGpsPoint> gpsPoints = await floidServerRepository.fetchDroidGpsPointsFiltered(
              event.floidId,
              event.floidUuid,
              floidLineConfig.maxPointsStart,
              floidLineConfig.minTime,
              floidLineConfig.minDistance,
              floidLineConfig.kink,
              floidLineConfig.kinkAlpha);
          emit(FloidDroidGpsPointsFilteredLoaded(gpsPoints: gpsPoints, floidId: event.floidId, floidUuid: event.floidUuid, timestamp: ''));
        } catch (_) {
          emit(FloidDroidGpsPointsFilteredError());
        }
      }

      if (event is RefreshFloidDroidGpsPointsFiltered) {
        floidLineConfig = FloidLineConfig(kink: FloidGpsPointsBloc.DEFAULT_KINK,
            kinkAlpha: FloidGpsPointsBloc.DEFAULT_KINK_ALPHA,
            shrinkAlpha: FloidGpsPointsBloc.DEFAULT_SHRINK_ALPHA,
            expandAlpha: FloidGpsPointsBloc.DEFAULT_EXPAND_ALPHA,
            maxPointsStart: FloidGpsPointsBloc.DEFAULT_MAX_POINTS_START,
            minTime: FloidGpsPointsBloc.DEFAULT_MIN_TIME,
            minDistance: FloidGpsPointsBloc.DEFAULT_MIN_DISTANCE,
            highQuality: FloidGpsPointsBloc.DEFAULT_HIGH_QUALITY);
        try {
          final List<FloidGpsPoint> gpsPoints = await floidServerRepository.fetchDroidGpsPointsFiltered(
              event.floidId,
              event.floidUuid,
              floidLineConfig.maxPointsStart,
              floidLineConfig.minTime,
              floidLineConfig.minDistance,
              floidLineConfig.kink,
              floidLineConfig.kinkAlpha);
          emit(FloidDroidGpsPointsFilteredLoaded(gpsPoints: gpsPoints, floidId: event.floidId, floidUuid: event.floidUuid, timestamp: ''));
        } catch (_) {
          emit(FloidDroidGpsPointsFilteredError());
        }
      }
      if (event is AddFloidDroidGpsPointFiltered) {
        // We ignore any state but points loaded:
        if (state is FloidDroidGpsPointsFilteredLoaded) {
          try {
            final FloidDroidGpsPointsFilteredLoaded floidGpsPointsLoaded = state as FloidDroidGpsPointsFilteredLoaded;
            // Add the point in subject to the current restrictions:
            List<FloidGpsPoint> floidGpsPoints = Simplify.addFloidGpsPoint(event.floidGpsPoint, floidGpsPointsLoaded.gpsPoints, floidLineConfig);
            emit(FloidDroidGpsPointsFilteredEmpty());
            emit(FloidDroidGpsPointsFilteredLoaded(gpsPoints: floidGpsPoints, floidId: event.floidGpsPoint.floidId, floidUuid: event.floidGpsPoint.floidUuid, timestamp: ''));
          } catch (_) {
            emit(FloidDroidGpsPointsFilteredError());
          }
        } else {
          print('Droid GPS points filtered not loaded - point ignored');
        }
      }
    });
  }
}
