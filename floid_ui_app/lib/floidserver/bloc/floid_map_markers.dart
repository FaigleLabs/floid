/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/map/marker/floid_map_marker.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidMapMarkersEvent extends Equatable {
  FloidMapMarkersEvent([List props = const []]) : super();
}

class SetFloidMapMarkers extends FloidMapMarkersEvent {
  final List<FloidMapMarker> floidMapMarkers;
  final bool zoomToMarkers;
  SetFloidMapMarkers({required this.floidMapMarkers, required this.zoomToMarkers})
      : super([floidMapMarkers]);
  @override
  List<Object> get props => [floidMapMarkers];
}
class ClearFloidMapMarkers extends FloidMapMarkersEvent {
  @override
  List<Object> get props => const [];
}

abstract class FloidMapMarkersState extends Equatable {
  FloidMapMarkersState([List props = const []]) : super();
}

class FloidMapMarkersEmpty extends FloidMapMarkersState {
  @override
  List<Object> get props => [];
}

class FloidMapMarkersLoaded extends FloidMapMarkersState {
  final List<FloidMapMarker> floidMapMarkers;
  final bool zoomToMarkers;

  FloidMapMarkersLoaded({required this.floidMapMarkers, required this.zoomToMarkers})
      : super([floidMapMarkers]);
  @override
  List<Object> get props => [floidMapMarkers];
}

class FloidMapMarkersError extends FloidMapMarkersState {
  @override
  List<Object> get props => [];
}

class FloidMapMarkersBloc<T extends FloidMapMarker> extends Bloc<FloidMapMarkersEvent, FloidMapMarkersState> {
  FloidMapMarkersBloc(): super(FloidMapMarkersEmpty()) {
    on<FloidMapMarkersEvent>((FloidMapMarkersEvent event, Emitter<FloidMapMarkersState> emit) async {
      // Set map Markers:
      if (event is SetFloidMapMarkers) {
        if(event.floidMapMarkers is List<T>) {
          _numberMarkers(event.floidMapMarkers);
          emit(FloidMapMarkersLoaded(floidMapMarkers: event.floidMapMarkers, zoomToMarkers: event.zoomToMarkers));
        } else {
          emit(FloidMapMarkersError());
        }
      }
      // Clear map Markers:
      if(event is ClearFloidMapMarkers) {
        emit(FloidMapMarkersEmpty());
      }
  });
  }

  void _numberMarkers(List<FloidMapMarker> floidMapMarkers) {
    for(int i=0; i<floidMapMarkers.length; ++i) {
      floidMapMarkers[i].markerIndex = i;
    }
  }
}
