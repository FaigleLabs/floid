/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidIdAndStateListEvent extends Equatable {
  FloidIdAndStateListEvent([List props = const []]) : super();
}

class FetchFloidIdAndStateList extends FloidIdAndStateListEvent {
  FetchFloidIdAndStateList()
      : super([]);
  @override
  List<Object> get props => [];
}

class RefreshFloidIdAndStateList extends FloidIdAndStateListEvent {
  RefreshFloidIdAndStateList()
      : super([]);
  @override
  List<Object> get props => [];
}

class AddNewFloidIdAndState extends FloidIdAndStateListEvent {
  final FloidIdAndState floidIdAndState;
  AddNewFloidIdAndState({required this.floidIdAndState})
      : super([floidIdAndState]);
  @override
  List<Object> get props => [floidIdAndState];
}

abstract class FloidIdAndStateListState extends Equatable {
  FloidIdAndStateListState([List props = const []]) : super();
}

class FloidIdAndStateListEmpty extends FloidIdAndStateListState {
  @override
  List<Object> get props => [];
}

class FloidIdAndStateListLoading extends FloidIdAndStateListState {
  @override
  List<Object> get props => [];
}

class FloidIdAndStateListLoaded extends FloidIdAndStateListState {
  final List<FloidIdAndState> floidIdAndStateList;

  FloidIdAndStateListLoaded({required this.floidIdAndStateList})
      : super([floidIdAndStateList]);
  @override
  List<Object> get props => [floidIdAndStateList];
}

class FloidIdAndStateListError extends FloidIdAndStateListState {
  @override
  List<Object> get props => [];
}

class FloidIdAndStateListBloc extends Bloc<FloidIdAndStateListEvent, FloidIdAndStateListState> {
  final FloidServerRepository floidServerRepository;

  FloidIdAndStateListBloc({required this.floidServerRepository})
      : super(FloidIdAndStateListEmpty()) {
    on<FloidIdAndStateListEvent>((FloidIdAndStateListEvent event, Emitter<FloidIdAndStateListState> emit) async {
      if (event is FetchFloidIdAndStateList) {
        emit(FloidIdAndStateListLoading());
        try {
          final List<FloidIdAndState> floidIdAndStateList = await floidServerRepository.fetchFloidIdAndStateList();
          emit(FloidIdAndStateListLoaded(floidIdAndStateList: floidIdAndStateList));
        } catch (_) {
          emit(FloidIdAndStateListError());
        }
      }

      if (event is RefreshFloidIdAndStateList) {
        emit(FloidIdAndStateListLoading());
        try {
          final List<FloidIdAndState> floidIdAndStateList = await floidServerRepository.fetchFloidIdAndStateList();
          emit(FloidIdAndStateListLoaded(floidIdAndStateList: floidIdAndStateList));
        } catch (_) {
          emit(FloidIdAndStateListError());
        }
      }

      if(event is AddNewFloidIdAndState) {
        try {
          if (state is FloidIdAndStateListLoaded) {
            FloidIdAndStateListLoaded floidIdAndStateListLoadedState = state as FloidIdAndStateListLoaded;
            // Add this to the top of the list:
            List<FloidIdAndState> newFloidIdAndStateList = [event.floidIdAndState]..addAll(floidIdAndStateListLoadedState.floidIdAndStateList);
            emit(FloidIdAndStateListLoaded(
              floidIdAndStateList: newFloidIdAndStateList,
            ));
          }
        } catch (_) {
          emit(FloidIdAndStateListError());
        }
      }
    });
  }

}
