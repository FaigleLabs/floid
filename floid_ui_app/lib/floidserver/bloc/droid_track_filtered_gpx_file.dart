/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class DroidTrackFilteredGpxFileEvent extends Equatable {
  DroidTrackFilteredGpxFileEvent([List props = const []]) : super();
}

class SetDroidTrackFilteredGpxFile extends DroidTrackFilteredGpxFileEvent {
  final String droidTrackFilteredGpxFile;
  final String fileName;
  SetDroidTrackFilteredGpxFile({required this.droidTrackFilteredGpxFile, required this.fileName})
      : super([droidTrackFilteredGpxFile, fileName]);
  @override
  List<Object> get props => [droidTrackFilteredGpxFile, fileName];
}

class ClearDroidTrackFilteredGpxFile extends DroidTrackFilteredGpxFileEvent {
  @override
  List<Object> get props => const [];
}

abstract class DroidTrackFilteredGpxFileState extends Equatable {
  DroidTrackFilteredGpxFileState([List props = const []]) : super();
}

class DroidTrackFilteredGpxFileEmpty extends DroidTrackFilteredGpxFileState {
  @override
  List<Object> get props => [];
}

class DroidTrackFilteredGpxFileLoaded extends DroidTrackFilteredGpxFileState {
  final String droidTrackFilteredGpxFile;
  final String fileName;

  DroidTrackFilteredGpxFileLoaded({required this.droidTrackFilteredGpxFile, required this.fileName})
      : super([droidTrackFilteredGpxFile, fileName]);
  @override
  List<Object> get props => [droidTrackFilteredGpxFile, fileName];
}

class DroidTrackFilteredGpxFileError extends DroidTrackFilteredGpxFileState {
  @override
  List<Object> get props => [];
}

class DroidTrackFilteredGpxFileBloc extends Bloc<DroidTrackFilteredGpxFileEvent, DroidTrackFilteredGpxFileState> {
  DroidTrackFilteredGpxFileBloc(): super(DroidTrackFilteredGpxFileEmpty()) {
    on<DroidTrackFilteredGpxFileEvent>((DroidTrackFilteredGpxFileEvent event, Emitter<DroidTrackFilteredGpxFileState> emit) async {
      // Set map gpx file:
      if (event is SetDroidTrackFilteredGpxFile) {
        print(event.droidTrackFilteredGpxFile.length.toString() + ' bytes GPX file (' + event.fileName + ')');
        emit(DroidTrackFilteredGpxFileLoaded(droidTrackFilteredGpxFile: event.droidTrackFilteredGpxFile, fileName: event.fileName));
      }
      // Clear map gpx file:
      if(event is ClearDroidTrackFilteredGpxFile) {
        emit(DroidTrackFilteredGpxFileEmpty());
      }
    });
  }
}
