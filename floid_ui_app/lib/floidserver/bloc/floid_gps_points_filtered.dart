/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:floid_ui_app/ui/map/line/line.dart';
import 'package:floid_ui_app/ui/map/utility/simplify.dart';

abstract class FloidGpsPointsFilteredEvent extends Equatable {
  FloidGpsPointsFilteredEvent([List props = const []]) : super();
}

class FetchFloidGpsPointsFiltered extends FloidGpsPointsFilteredEvent {
  final int floidId;
  final String floidUuid;
  FetchFloidGpsPointsFiltered({required this.floidId,
    required this.floidUuid})
      : super([floidId, floidUuid]);
  @override
  List<Object> get props => [floidId, floidUuid];
}

class RefreshFloidGpsPointsFiltered extends FloidGpsPointsFilteredEvent {
  final int floidId;
  final String floidUuid;

  RefreshFloidGpsPointsFiltered({required this.floidId,
    required this.floidUuid})
      : super([floidId, floidUuid]);
  @override
  List<Object> get props => [floidId, floidUuid];
}

class AddFloidGpsPointFiltered extends FloidGpsPointsFilteredEvent {
  final FloidGpsPoint floidGpsPoint;
  final int floidId;
  final String floidUuid;
  final String timestamp;

  AddFloidGpsPointFiltered({required this.floidGpsPoint, required this.floidId, required this.floidUuid, required this.timestamp})
      : super([floidGpsPoint, floidId, floidUuid, timestamp]);
  @override
  List<Object> get props => [floidGpsPoint, floidId, floidUuid, timestamp];
}

abstract class FloidGpsPointsFilteredState extends Equatable {
  FloidGpsPointsFilteredState([List props = const []]) : super();
}

class FloidGpsPointsFilteredEmpty extends FloidGpsPointsFilteredState {
  @override
  List<Object> get props => [];
}

class FloidGpsPointsFilteredLoading extends FloidGpsPointsFilteredState {
  @override
  List<Object> get props => [];
}

class FloidGpsPointsFilteredLoaded extends FloidGpsPointsFilteredState {
  final List<FloidGpsPoint> gpsPoints;
  final int floidId;
  final String floidUuid;
  final String timestamp;

  FloidGpsPointsFilteredLoaded({required this.gpsPoints, required this.floidId, required this.floidUuid, required this.timestamp})
      : super([gpsPoints, floidId, floidUuid, timestamp]);
  @override
  List<Object> get props => [gpsPoints, floidId, floidUuid, timestamp];
}

class FloidGpsPointsFilteredError extends FloidGpsPointsFilteredState {
  @override
  List<Object> get props => [];
}

class FloidGpsPointsFilteredBloc extends Bloc<FloidGpsPointsFilteredEvent, FloidGpsPointsFilteredState> {
  final FloidServerRepository floidServerRepository;
  FloidLineConfig floidLineConfig = FloidLineConfig(kink: FloidGpsPointsBloc.DEFAULT_KINK,
      kinkAlpha: FloidGpsPointsBloc.DEFAULT_KINK_ALPHA,
      shrinkAlpha: FloidGpsPointsBloc.DEFAULT_SHRINK_ALPHA,
      expandAlpha: FloidGpsPointsBloc.DEFAULT_EXPAND_ALPHA,
      maxPointsStart: FloidGpsPointsBloc.DEFAULT_MAX_POINTS_START,
      minTime: FloidGpsPointsBloc.DEFAULT_MIN_TIME,
      minDistance: FloidGpsPointsBloc.DEFAULT_MIN_DISTANCE,
      highQuality: FloidGpsPointsBloc.DEFAULT_HIGH_QUALITY); // This gets an object when points are loaded

  FloidGpsPointsFilteredBloc({required this.floidServerRepository})
      : super(FloidGpsPointsFilteredEmpty()) {
    on<FloidGpsPointsFilteredEvent>((FloidGpsPointsFilteredEvent event, Emitter<FloidGpsPointsFilteredState> emit) async {
      if (event is FetchFloidGpsPointsFiltered) {
        floidLineConfig = FloidLineConfig(kink: FloidGpsPointsBloc.DEFAULT_KINK,
            kinkAlpha: FloidGpsPointsBloc.DEFAULT_KINK_ALPHA,
            shrinkAlpha: FloidGpsPointsBloc.DEFAULT_SHRINK_ALPHA,
            expandAlpha: FloidGpsPointsBloc.DEFAULT_EXPAND_ALPHA,
            maxPointsStart: FloidGpsPointsBloc.DEFAULT_MAX_POINTS_START,
            minTime: FloidGpsPointsBloc.DEFAULT_MIN_TIME,
            minDistance: FloidGpsPointsBloc.DEFAULT_MIN_DISTANCE,
            highQuality: FloidGpsPointsBloc.DEFAULT_HIGH_QUALITY);
        emit(FloidGpsPointsFilteredLoading());
        try {
          final List<FloidGpsPoint> gpsPoints = await floidServerRepository.fetchGpsPointsFiltered(
              event.floidId,
              event.floidUuid,
              floidLineConfig.maxPointsStart,
              floidLineConfig.minTime,
              floidLineConfig.minDistance,
              floidLineConfig.kink,
              floidLineConfig.kinkAlpha);
          emit(FloidGpsPointsFilteredLoaded(gpsPoints: gpsPoints, floidId: event.floidId, floidUuid: event.floidUuid, timestamp: ''));
        } catch (_) {
          emit(FloidGpsPointsFilteredError());
        }
      }

      if (event is RefreshFloidGpsPointsFiltered) {
        floidLineConfig = FloidLineConfig(kink: FloidGpsPointsBloc.DEFAULT_KINK,
            kinkAlpha: FloidGpsPointsBloc.DEFAULT_KINK_ALPHA,
            shrinkAlpha: FloidGpsPointsBloc.DEFAULT_SHRINK_ALPHA,
            expandAlpha: FloidGpsPointsBloc.DEFAULT_EXPAND_ALPHA,
            maxPointsStart: FloidGpsPointsBloc.DEFAULT_MAX_POINTS_START,
            minTime: FloidGpsPointsBloc.DEFAULT_MIN_TIME,
            minDistance: FloidGpsPointsBloc.DEFAULT_MIN_DISTANCE,
            highQuality: FloidGpsPointsBloc.DEFAULT_HIGH_QUALITY
        );
        try {
          final List<FloidGpsPoint> gpsPoints = await floidServerRepository.fetchGpsPointsFiltered(
              event.floidId,
              event.floidUuid,
              floidLineConfig.maxPointsStart,
              floidLineConfig.minTime,
              floidLineConfig.minDistance,
              floidLineConfig.kink,
              floidLineConfig.kinkAlpha);
          emit(FloidGpsPointsFilteredLoaded(gpsPoints: gpsPoints,  floidId: event.floidId, floidUuid: event.floidUuid, timestamp: ''));
        } catch (_) {
          emit(FloidGpsPointsFilteredError());
        }
      }
      if (event is AddFloidGpsPointFiltered) {
        // We ignore any state but points loaded:
        if (state is FloidGpsPointsFilteredLoaded) {
          try {
            final FloidGpsPointsFilteredLoaded floidGpsPointsLoaded = state as FloidGpsPointsFilteredLoaded;
            // Add the point in subject to the current restrictions:
            List<FloidGpsPoint> floidGpsPoints = Simplify.addFloidGpsPoint(event.floidGpsPoint, floidGpsPointsLoaded.gpsPoints, floidLineConfig);
            emit(FloidGpsPointsFilteredEmpty());
            emit(FloidGpsPointsFilteredLoaded(gpsPoints: floidGpsPoints,  floidId: event.floidId, floidUuid: event.floidUuid, timestamp: ''));
          } catch (_) {
            emit(FloidGpsPointsFilteredError());
          }
        } else {
          print('Floid GPS points filtered not loaded - point ignored');
        }
      }
    });
  }
  
}
