/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidUserEvent extends Equatable {
  FloidUserEvent([List props = const []]) : super();
}

class FetchFloidUser extends FloidUserEvent {
  FetchFloidUser() : super([]);
  @override
  List<Object> get props => [];
}

class RefreshFloidUser extends FloidUserEvent {
  RefreshFloidUser() : super([]);
  @override
  List<Object> get props => [];
}

abstract class FloidUserState extends Equatable {
  FloidUserState([List props = const []]) : super();
}

class FloidUserEmpty extends FloidUserState {
  @override
  List<Object> get props => [];
}

class FloidUserLoading extends FloidUserState {
  @override
  List<Object> get props => [];
}

class FloidUserLoaded extends FloidUserState {
  final String user;

  FloidUserLoaded({required this.user})
      : super([user]);
  @override
  List<Object> get props => [user];
}

class FloidUserError extends FloidUserState {
  @override
  List<Object> get props => [];
}

class FloidUserBloc extends Bloc<FloidUserEvent, FloidUserState> {
  final FloidServerRepository floidServerRepository;

  FloidUserBloc({required this.floidServerRepository})
      : super(FloidUserEmpty()) {
    on<FloidUserEvent>((FloidUserEvent event,
        Emitter<FloidUserState> emit) async {
      if (event is FetchFloidUser) {
        emit(FloidUserLoading());
        try {
          final String user = await floidServerRepository.fetchFloidUser();
          emit(FloidUserLoaded(user: user));
        } catch (_) {
          emit(FloidUserError());
        }
      }
      if (event is RefreshFloidUser) {
        try {
          final String user = await floidServerRepository.fetchFloidUser();
          emit(FloidUserLoaded(user: user));
        } catch (_) {
          emit(FloidUserError());
        }
      }
    });
  }
}
