/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidListEvent extends Equatable {
  FloidListEvent([List props = const []]) : super();
}

class FetchFloidList extends FloidListEvent {
  FetchFloidList()
      : super([]);
  @override
  List<Object> get props => [];
}

class RefreshFloidList extends FloidListEvent {
  RefreshFloidList()
      : super([]);
  @override
  List<Object> get props => [];
}

abstract class FloidListState extends Equatable {
  FloidListState([List props = const []]) : super();
}

class FloidListEmpty extends FloidListState {
  @override
  List<Object> get props => [];
}

class FloidListLoading extends FloidListState {
  @override
  List<Object> get props => [];
}

class FloidListLoaded extends FloidListState {
  final List<int> floidList;

  FloidListLoaded({required this.floidList})
      : super([floidList]);
  @override
  List<Object> get props => [floidList];
}

class FloidListError extends FloidListState {
  @override
  List<Object> get props => [];
}

class FloidListBloc extends Bloc<FloidListEvent, FloidListState> {
  final FloidServerRepository floidServerRepository;

  FloidListBloc({required this.floidServerRepository})
      : super(FloidListEmpty()) {
    on<FloidListEvent>((FloidListEvent event,
        Emitter<FloidListState> emit) async {
      if (event is FetchFloidList) {
        emit(FloidListLoading());
        try {
          final List<int> floidList = await floidServerRepository
              .fetchFloidList();
          emit(FloidListLoaded(floidList: floidList));
        } catch (_) {
          emit(FloidListError());
        }
      }
      if (event is RefreshFloidList) {
        try {
          final List<int> floidList = await floidServerRepository
              .fetchFloidList();
          emit(FloidListLoaded(floidList: floidList));
        } catch (_) {
          emit(FloidListError());
        }
      }
    });
  }
}
