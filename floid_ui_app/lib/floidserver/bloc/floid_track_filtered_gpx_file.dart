/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidTrackFilteredGpxFileEvent extends Equatable {
  FloidTrackFilteredGpxFileEvent([List props = const []]) : super();
}

class SetFloidTrackFilteredGpxFile extends FloidTrackFilteredGpxFileEvent {
  final String floidTrackFilteredGpxFile;
  final String fileName;
  SetFloidTrackFilteredGpxFile({required this.floidTrackFilteredGpxFile, required this.fileName})
      : super([floidTrackFilteredGpxFile, fileName]);
  @override
  List<Object> get props => [floidTrackFilteredGpxFile, fileName];
}

class ClearFloidTrackFilteredGpxFile extends FloidTrackFilteredGpxFileEvent {
  @override
  List<Object> get props => const [];
}

abstract class FloidTrackFilteredGpxFileState extends Equatable {
  FloidTrackFilteredGpxFileState([List props = const []]) : super();
}

class FloidTrackFilteredGpxFileEmpty extends FloidTrackFilteredGpxFileState {
  @override
  List<Object> get props => [];
}

class FloidTrackFilteredGpxFileLoaded extends FloidTrackFilteredGpxFileState {
  final String floidTrackFilteredGpxFile;
  final String fileName;

  FloidTrackFilteredGpxFileLoaded({required this.floidTrackFilteredGpxFile, required this.fileName})
      : super([floidTrackFilteredGpxFile, fileName]);
  @override
  List<Object> get props => [floidTrackFilteredGpxFile, fileName];
}

class FloidTrackFilteredGpxFileError extends FloidTrackFilteredGpxFileState {
  @override
  List<Object> get props => [];
}

class FloidTrackFilteredGpxFileBloc extends Bloc<FloidTrackFilteredGpxFileEvent, FloidTrackFilteredGpxFileState> {

  FloidTrackFilteredGpxFileBloc(): super(FloidTrackFilteredGpxFileEmpty()) {
    on<FloidTrackFilteredGpxFileEvent>((FloidTrackFilteredGpxFileEvent event, Emitter<FloidTrackFilteredGpxFileState> emit) async {
      // Set map gpx file:
      if (event is SetFloidTrackFilteredGpxFile) {
        print(event.floidTrackFilteredGpxFile.length.toString() + ' bytes GPX file (' + event.fileName + ')');
        emit(FloidTrackFilteredGpxFileLoaded(floidTrackFilteredGpxFile: event.floidTrackFilteredGpxFile, fileName: event.fileName));
      }
      // Clear map gpx file:
      if(event is ClearFloidTrackFilteredGpxFile) {
        emit(FloidTrackFilteredGpxFileEmpty());
      }
    });
  }
}
