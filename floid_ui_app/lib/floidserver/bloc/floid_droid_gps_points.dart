/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:floid_ui_app/ui/map/line/line.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

// TODO - This is currently unused - ALL Droid and Floid points are filtered - this gives unfiltered - should have a UI toggle to choose between
abstract class FloidDroidGpsPointsEvent extends Equatable {
  FloidDroidGpsPointsEvent([List props = const []]) : super();
}

class FetchFloidDroidGpsPoints extends FloidDroidGpsPointsEvent {
  final int floidId;
  final String floidUuid;

  FetchFloidDroidGpsPoints({required this.floidId, required this.floidUuid})
      : super([floidId, floidUuid]);
  @override
  List<Object> get props => [floidId, floidUuid];
}

class RefreshFloidDroidGpsPoints extends FloidDroidGpsPointsEvent {
  final int floidId;
  final String floidUuid;
  @override
  List<Object> get props => [floidId, floidUuid];

  RefreshFloidDroidGpsPoints({required this.floidId, required this.floidUuid})
      : super([floidId, floidUuid]);
}
class AddFloidDroidGpsPoint extends FloidDroidGpsPointsEvent {
  final FloidGpsPoint floidGpsPoint;
  final int floidId;
  final String floidUuid;
  final String timestamp;

  AddFloidDroidGpsPoint({required this.floidGpsPoint, required this.floidId, required this.floidUuid, required this.timestamp})
      : super([floidGpsPoint, floidId, floidUuid, timestamp]);
  @override
  List<Object> get props => [floidGpsPoint, floidId, floidUuid, timestamp];
}


abstract class FloidDroidGpsPointsState extends Equatable {
  FloidDroidGpsPointsState([List props = const []]) : super();
}

class FloidDroidGpsPointsEmpty extends FloidDroidGpsPointsState {
  @override
  List<Object> get props => [];
}

class FloidDroidGpsPointsLoading extends FloidDroidGpsPointsState {
  @override
  List<Object> get props => [];
}

class FloidDroidGpsPointsLoaded extends FloidDroidGpsPointsState {
  final List<FloidGpsPoint> gpsPoints;
  final int floidId;
  final String floidUuid;
  final String timestamp;

  FloidDroidGpsPointsLoaded({required this.gpsPoints, required this.floidId, required this.floidUuid, required this.timestamp})
      : super([gpsPoints, floidId, floidUuid, timestamp]);
  @override
  List<Object> get props => [gpsPoints, floidId, floidUuid, timestamp];
}

class FloidDroidGpsPointsError extends FloidDroidGpsPointsState {
  @override
  List<Object> get props => [];
}

class FloidDroidGpsPointsBloc extends Bloc<FloidDroidGpsPointsEvent, FloidDroidGpsPointsState> {
  final FloidServerRepository floidServerRepository;
  FloidLineConfig floidLineConfig = FloidLineConfig(kink: FloidGpsPointsBloc.DEFAULT_KINK,
      kinkAlpha: FloidGpsPointsBloc.DEFAULT_KINK_ALPHA,
      shrinkAlpha: FloidGpsPointsBloc.DEFAULT_SHRINK_ALPHA,
      expandAlpha: FloidGpsPointsBloc.DEFAULT_EXPAND_ALPHA,
      maxPointsStart: FloidGpsPointsBloc.DEFAULT_MAX_POINTS_START,
      minTime: FloidGpsPointsBloc.DEFAULT_MIN_TIME,
      minDistance: FloidGpsPointsBloc.DEFAULT_MIN_DISTANCE,
      highQuality: FloidGpsPointsBloc.DEFAULT_HIGH_QUALITY);

  FloidDroidGpsPointsBloc({required this.floidServerRepository})
      : super(FloidDroidGpsPointsEmpty()) {
    on<FloidDroidGpsPointsEvent>((FloidDroidGpsPointsEvent event, Emitter<FloidDroidGpsPointsState> emit) async {
      if (event is FetchFloidDroidGpsPoints) {
        emit(FloidDroidGpsPointsLoading());
        try {
          final List<FloidGpsPoint> gpsPoints =
          await floidServerRepository.fetchGpsPoints(
              event.floidId,
              event.floidUuid,);
          emit(FloidDroidGpsPointsLoaded(gpsPoints: gpsPoints, floidId: event.floidId, floidUuid: event.floidUuid, timestamp: ''));
        } catch (_) {
          emit(FloidDroidGpsPointsError());
        }
      }
      if (event is RefreshFloidDroidGpsPoints) {
        try {
          final List<FloidGpsPoint> gpsPoints =
          await floidServerRepository.fetchGpsPoints(
              event.floidId,
              event.floidUuid,);
          emit(FloidDroidGpsPointsLoaded(gpsPoints: gpsPoints, floidId: event.floidId, floidUuid: event.floidUuid, timestamp: ''));
        } catch (_) {
          emit(FloidDroidGpsPointsError());
        }
      }
      if (event is AddFloidDroidGpsPoint) {
        // We ignore any state but points loaded:
        if (state is FloidDroidGpsPointsLoaded) {
          try {
            final FloidDroidGpsPointsLoaded floidDroidGpsPointsLoaded = state as FloidDroidGpsPointsLoaded;
            floidDroidGpsPointsLoaded.gpsPoints.add(event.floidGpsPoint);
            emit(FloidDroidGpsPointsEmpty());
            emit(FloidDroidGpsPointsLoaded(gpsPoints: floidDroidGpsPointsLoaded.gpsPoints, floidId: event.floidGpsPoint.floidId, floidUuid: event.floidGpsPoint.floidUuid, timestamp: ''));
          } catch (_) {
            emit(FloidDroidGpsPointsError());
          }
        } else {
          print('Droid GPS points not loaded - point ignored');
        }
      }
    });
  }
}
