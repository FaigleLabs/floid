/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/ui/map/marker/floid_map_marker.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidMapMarkerEvent extends Equatable {
  FloidMapMarkerEvent([List props = const []]) : super();
}

class SetFloidMapMarker extends FloidMapMarkerEvent {
  final FloidMapMarker floidMapMarker;
  SetFloidMapMarker({required this.floidMapMarker})
      : super([floidMapMarker]);
  @override
  List<Object> get props => [floidMapMarker];
}
class ClearFloidMapMarker extends FloidMapMarkerEvent {
  @override
  List<Object> get props => const [];
}

abstract class FloidMapMarkerState extends Equatable {
  FloidMapMarkerState([List props = const []]) : super();
}

class FloidMapMarkerEmpty extends FloidMapMarkerState {
  @override
  List<Object> get props => [];
}

class FloidMapMarkerLoaded extends FloidMapMarkerState {
  final FloidMapMarker floidMapMarker;

  FloidMapMarkerLoaded({required this.floidMapMarker})
      : super([floidMapMarker]);
  @override
  List<Object> get props => [floidMapMarker];
}

class FloidMapMarkerError extends FloidMapMarkerState {
  @override
  List<Object> get props => [];
}

class FloidMapMarkerBloc<T extends FloidMapMarker> extends Bloc<FloidMapMarkerEvent, FloidMapMarkerState> {
  FloidMapMarkerBloc() : super(FloidMapMarkerEmpty()) {
    on<FloidMapMarkerEvent>((FloidMapMarkerEvent event,
        Emitter<FloidMapMarkerState> emit) async {
      // Set map Marker:
      if (event is SetFloidMapMarker) {
        if (event.floidMapMarker is T) {
          emit(FloidMapMarkerLoaded(floidMapMarker: event.floidMapMarker));
        } else {
          emit(FloidMapMarkerError());
        }
      }
      // Clear map Marker:
      if (event is ClearFloidMapMarker) {
        emit(FloidMapMarkerEmpty());
      }
    });
  }
}
