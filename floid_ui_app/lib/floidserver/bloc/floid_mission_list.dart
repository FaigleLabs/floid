/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidMissionListEvent extends Equatable {
  FloidMissionListEvent([List props = const []]) : super();
}

class FetchFloidMissionList extends FloidMissionListEvent {
  FetchFloidMissionList()
      : super();
  @override
  List<Object> get props => [];
}

class RefreshFloidMissionList extends FloidMissionListEvent {
  RefreshFloidMissionList()
      : super();
  @override
  List<Object> get props => [];
}

abstract class FloidMissionListState extends Equatable {
  FloidMissionListState([List props = const []]) : super();
}

class FloidMissionListEmpty extends FloidMissionListState {
  @override
  List<Object> get props => [];
}

class FloidMissionListLoading extends FloidMissionListState {
  @override
  List<Object> get props => [];
}

class FloidMissionListLoaded extends FloidMissionListState {
  final List<DroidMissionList> droidMissionListList;

  FloidMissionListLoaded({required this.droidMissionListList})
      : super([droidMissionListList]);
  @override
  List<Object> get props => [droidMissionListList];
}

class FloidMissionListError extends FloidMissionListState {
  @override
  List<Object> get props => [];
}

class FloidMissionListBloc extends Bloc<FloidMissionListEvent, FloidMissionListState> {
  final FloidServerRepository floidServerRepository;

  FloidMissionListBloc({required this.floidServerRepository})
      : super(FloidMissionListEmpty()) {
    on<FloidMissionListEvent>((FloidMissionListEvent event, Emitter<FloidMissionListState> emit) async {
      if (event is FetchFloidMissionList) {
        emit(FloidMissionListLoading());
        try {
          final List<DroidMissionList> droidMissionListList = await floidServerRepository.fetchMissionList();
          emit(FloidMissionListLoaded(droidMissionListList: droidMissionListList));
        } catch (_) {
          emit(FloidMissionListError());
        }
      }

      if (event is RefreshFloidMissionList) {
        try {
          final List<DroidMissionList> droidMissionListList = await floidServerRepository.fetchMissionList();
          emit(FloidMissionListLoaded(droidMissionListList: droidMissionListList));
        } catch (_) {
          emit(FloidMissionListError());
        }
      }
    });
  }
}
