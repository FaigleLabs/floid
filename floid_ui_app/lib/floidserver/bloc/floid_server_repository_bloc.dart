/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:io';

import 'package:floid_ui_app/floidserver/client/client.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:flutter/foundation.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;

abstract class FloidServerRepositoryEvent extends Equatable {
  FloidServerRepositoryEvent([List props = const []]) : super();
}

class FloidServerRepositoryNewHostEvent extends FloidServerRepositoryEvent {
  final String host;
  final int port;
  final bool secure;

  FloidServerRepositoryNewHostEvent({required this.host, required this.port, required this.secure})
      : super([host]) {
    print('FloidServerRepositoryNewHostEvent(host:$host secure: $secure)');
  }
  @override
  List<Object> get props => [host, secure];
}
class FloidServerRepositoryInsecureHostEvent extends FloidServerRepositoryEvent {
  FloidServerRepositoryInsecureHostEvent()
      : super([]) {
    print('FloidServerRepositoryInsecureHostEvent()');
  }
  @override
  List<Object> get props => [];
}


abstract class FloidServerRepositoryState extends Equatable {
  FloidServerRepositoryState([List props = const []]) : super();
}

class FloidServerRepositoryStateNoHost extends FloidServerRepositoryState {
  @override
  List<Object> get props => [];
}

class FloidServerRepositoryStateHasHost extends FloidServerRepositoryState {
  final String host;
  final int port;
  final bool secure;
  final FloidServerRepository floidServerRepository;
  late final FloidServerRepositoryBloc? floidServerRepositoryBloc;

  FloidServerRepositoryStateHasHost({FloidServerRepositoryBloc? floidServerRepositoryBloc, required this.host, required this.port, required this.secure,})
      : floidServerRepository = FloidServerRepository(floidServerApiClient: FloidServerApiClient(
          httpClient: kIsWeb?http.Client():null,
          httpAppClient: kIsWeb?null:HttpClient(),
          server: '$host:$port',
          secure: secure,)
        ),
        super([host, secure]) {
    print('FloidServerRepositoryStateNewHost(host:$host)');
  }
  void setFloidServerRepositoryBloc(FloidServerRepositoryBloc floidServerRepositoryBloc) {
    this.floidServerRepositoryBloc = floidServerRepositoryBloc;
    // Set the bloc to the API client so it can emit an insecure event in case of untrusted Cert
    floidServerRepository.floidServerApiClient.floidServerRepositoryBloc=floidServerRepositoryBloc;

  }
  FloidServerRepositoryStateHasHost.insecure({required this.floidServerRepository, required this.floidServerRepositoryBloc,
    required this.host,
    required this.port,
    required this.secure,
  });

  @override
  List<Object> get props => [host];
}

class FloidServerRepositoryStateHasInsecureHost extends FloidServerRepositoryStateHasHost {
  FloidServerRepositoryStateHasInsecureHost({required FloidServerRepository floidServerRepository, required FloidServerRepositoryBloc floidServerRepositoryBloc, required String host, required int port, required bool secure
  }) : super.insecure(floidServerRepository: floidServerRepository, floidServerRepositoryBloc: floidServerRepositoryBloc, host: host, port: port, secure: secure);
}

class FloidServerRepositoryBloc extends Bloc<FloidServerRepositoryEvent, FloidServerRepositoryState> {
  final String host;
  final int port;
  final bool secure;

  FloidServerRepositoryBloc({required this.host, required this.port, required this.secure})
      // Start no host;
      : super(FloidServerRepositoryStateHasHost(host: host, port: port, secure: secure))
  {
    // Set this bloc back to the state:
    (state as FloidServerRepositoryStateHasHost).setFloidServerRepositoryBloc(this);
    on<FloidServerRepositoryEvent>((FloidServerRepositoryEvent event, Emitter<FloidServerRepositoryState> emit)
    async {
      if (event is FloidServerRepositoryNewHostEvent) {
        emit(FloidServerRepositoryStateNoHost());
        emit(FloidServerRepositoryStateHasHost(floidServerRepositoryBloc: this, host: event.host, port: event.port, secure: event.secure));
      }
      if (event is FloidServerRepositoryInsecureHostEvent) {
        if(state is FloidServerRepositoryStateHasHost) {
          final FloidServerRepositoryStateHasHost floidServerRepositoryStateHasHost = state as FloidServerRepositoryStateHasHost;
          emit(FloidServerRepositoryStateHasInsecureHost(floidServerRepository: floidServerRepositoryStateHasHost.floidServerRepository, floidServerRepositoryBloc: this, host: floidServerRepositoryStateHasHost.host, port: floidServerRepositoryStateHasHost.port, secure: floidServerRepositoryStateHasHost.secure));
        }
      }
    });
  }
}
