/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/address/address_proxy.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:floid_ui_app/ui/controller/floid_errors.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_path_elevation_request.dart';

class FloidAppStateAttributes {
  static const int PHOTO_VIDEO_STATE_ALL_CLOSED = 0;
  static const int PHOTO_VIDEO_STATE_PHOTO_OPEN = 1;
  static const int PHOTO_VIDEO_STATE_PHOTO_FULL_SCREEN = 2;
  static const int PHOTO_VIDEO_STATE_VIDEO_OPEN = 3;
  static const int PHOTO_VIDEO_STATE_VIDEO_FULL_SCREEN = 4;
  static Map<int,
      Map<int,
          FloidPathElevationRequest>> pendingPathElevationRequestSet = Map();
  static int nextFlyLineCheckId = 0;

  static const List<String> mapTypes = ['streets-v12',
    'outdoors-v12',
    'light-v11',
    'dark-v11',
    'satellite-v9',
    'satellite-streets-v12',
    'navigation-day-v1',
    'navigation-night-v1',
    'floid-server'
  ];
  static const List<String> elevationLocations = ['floid-server',
    'open-elevation',
  ];

  bool readyToExecute = false;
  double flightDistance = -1;
  int zoomToPinsFlag = 0;
  FloidErrors floidErrors = FloidErrors(errors: []);
  bool floidDroidSystemsInfoOpen = false;
  bool floidSystemsInfoOpen = false;
  int photoVideoState = PHOTO_VIDEO_STATE_ALL_CLOSED;
  int photoRotation = 0;
  int videoFrameRotation = 0;
  bool showDroidGpsLines = true;
  bool showFloidGpsLines = true;
  bool showLegend = false;
  int zoomInFlag = 0;
  int zoomOutFlag = 0;
  int savedFloidId = FloidIdAndState.NO_FLOID_ID;
  int savedPinSetId = FloidPinSetAlt.NO_PIN_SET;
  int savedMissionId = DroidMission.NO_MISSION;
  String mapType = mapTypes[0];
  String elevationLocation = elevationLocations[0];

  // Live statuses:
  FloidServerStatus? floidServerStatus;
  bool floidIsAlive = false;

  FloidAppStateAttributes({
    this.readyToExecute = false,
    required this.floidErrors,
    this.flightDistance = -1,
    this.zoomToPinsFlag = 0,
    this.zoomInFlag = 0,
    this.zoomOutFlag = 0,
    this.photoVideoState = PHOTO_VIDEO_STATE_ALL_CLOSED,
    this.showDroidGpsLines = true,
    this.showFloidGpsLines = true,
    this.floidServerStatus,
    this.floidIsAlive = false,
    this.savedFloidId = FloidIdAndState.NO_FLOID_ID,
    this.savedPinSetId = FloidPinSetAlt.NO_PIN_SET,
    this.savedMissionId = DroidMission.NO_MISSION,
    this.mapType = 'streets-v12',
    this.elevationLocation = 'floid-server'}) {
    if (floidServerStatus == null) {
      floidServerStatus =
          FloidServerStatus(FloidServerStatus.SERVER_STATUS_OFF);
    }
    this.floidErrors = FloidErrors(errors: []);
  }

  FloidAppStateAttributes.fromFloidAppStateAttributes(
      FloidAppStateAttributes floidAppStateAttributes) {
    this.zoomToPinsFlag = floidAppStateAttributes.zoomToPinsFlag;
    this.zoomInFlag = floidAppStateAttributes.zoomInFlag;
    this.zoomOutFlag = floidAppStateAttributes.zoomOutFlag;
    this.readyToExecute = floidAppStateAttributes.readyToExecute;
    this.flightDistance = floidAppStateAttributes.flightDistance;
    this.floidErrors = FloidErrors(errors: [])
      ..addAll(floidAppStateAttributes.floidErrors);
    this.floidDroidSystemsInfoOpen =
        floidAppStateAttributes.floidDroidSystemsInfoOpen;
    this.floidSystemsInfoOpen = floidAppStateAttributes.floidSystemsInfoOpen;
    this.photoVideoState = floidAppStateAttributes.photoVideoState;
    this.photoRotation = floidAppStateAttributes.photoRotation;
    this.videoFrameRotation = floidAppStateAttributes.videoFrameRotation;
    this.showDroidGpsLines = floidAppStateAttributes.showDroidGpsLines;
    this.showFloidGpsLines = floidAppStateAttributes.showFloidGpsLines;
    this.showLegend = floidAppStateAttributes.showLegend;
    this.floidIsAlive = floidAppStateAttributes.floidIsAlive;
    this.floidServerStatus = floidAppStateAttributes.floidServerStatus;
    this.savedFloidId = floidAppStateAttributes.savedFloidId;
    this.savedPinSetId = floidAppStateAttributes.savedPinSetId;
    this.savedMissionId = floidAppStateAttributes.savedMissionId;
    this.mapType = floidAppStateAttributes.mapType;
    this.elevationLocation = floidAppStateAttributes.elevationLocation;
  }

  bool isPhotoOpen() {
    return photoVideoState == PHOTO_VIDEO_STATE_PHOTO_OPEN ||
        photoVideoState == PHOTO_VIDEO_STATE_PHOTO_FULL_SCREEN;
  }

  bool isVideoOpen() {
    return photoVideoState == PHOTO_VIDEO_STATE_VIDEO_OPEN ||
        photoVideoState == PHOTO_VIDEO_STATE_VIDEO_FULL_SCREEN;
  }
}

// Events:
abstract class FloidAppStateEvent extends Equatable {
  FloidAppStateEvent([List props = const []]) : super();
}

// Load:
class LoadAppStateEvent extends FloidAppStateEvent {
  LoadAppStateEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Ready to execute:
class SetReadyToExecuteAppStateEvent extends FloidAppStateEvent {
  SetReadyToExecuteAppStateEvent({required this.readyToExecute}) : super([]);
  final bool readyToExecute;
  @override
  List<Object> get props => [];
}
// Zoom to pins:
class ZoomToPinsAppStateEvent extends FloidAppStateEvent {
  ZoomToPinsAppStateEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Zoom in:
class ZoomInEvent extends FloidAppStateEvent {
  ZoomInEvent() : super([]);
  @override
  List<Object> get props => [];
}
// Zoom out:
class ZoomOutEvent extends FloidAppStateEvent {
  ZoomOutEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Open Floid Droid Systems Drawer:
class OpenFloidDroidSystemsEvent extends FloidAppStateEvent {
  OpenFloidDroidSystemsEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Close Floid Droid Systems Drawer:
class CloseFloidDroidSystemsEvent extends FloidAppStateEvent {
  CloseFloidDroidSystemsEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Open Floid Systems Drawer:
class OpenFloidSystemsEvent extends FloidAppStateEvent {
  OpenFloidSystemsEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Close Floid Systems Drawer:
class CloseFloidSystemsEvent extends FloidAppStateEvent {
  CloseFloidSystemsEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Rotate Photo:
class RotatePhotoEvent extends FloidAppStateEvent {
  RotatePhotoEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Rotate Video Frame:
class RotateVideoFrameEvent extends FloidAppStateEvent {
  RotateVideoFrameEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Show Droid Gps Lines:
class ShowDroidGpsLinesEvent extends FloidAppStateEvent {
  ShowDroidGpsLinesEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Hide Droid Gps Lines:
class HideDroidGpsLinesEvent extends FloidAppStateEvent {
  HideDroidGpsLinesEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Show Floid Gps Lines:
class ShowFloidGpsLinesEvent extends FloidAppStateEvent {
  ShowFloidGpsLinesEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Hide Floid Gps Lines:
class HideFloidGpsLinesEvent extends FloidAppStateEvent {
  HideFloidGpsLinesEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Show Legend:
class ShowLegendEvent extends FloidAppStateEvent {
  ShowLegendEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Hide Legend:
class HideLegendEvent extends FloidAppStateEvent {
  HideLegendEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Error lists:
class SetErrorsAppStateEvent extends FloidAppStateEvent {
  final FloidErrors floidErrors;
  // TODO - There is a situation where you want to clear all errors except path elevation not complete
  // TODO - This is not implemented but should be
  final bool clearErrors;
  SetErrorsAppStateEvent({required this.floidErrors, required this.clearErrors,}) : super([]);
  @override
  List<Object> get props => [floidErrors];
}

// Flight distance:
class SetFlightDistanceAppStateEvent extends FloidAppStateEvent {
  final double flightDistance;
  SetFlightDistanceAppStateEvent({required this.flightDistance }) : super([]);
  @override
  List<Object> get props => [flightDistance];
}

// Server status:
class SetServerStatusAppStateEvent extends FloidAppStateEvent {
  final FloidServerStatus floidServerStatus;
  SetServerStatusAppStateEvent({required this.floidServerStatus }) : super([]);
  @override
  List<Object> get props => [floidServerStatus];
}

// Floid alive:
class SetFloidAliveAppStateEvent extends FloidAppStateEvent {
  final bool floidIsAlive;
  SetFloidAliveAppStateEvent({required this.floidIsAlive }) : super([]);
  @override
  List<Object> get props => [floidIsAlive];
}

// Open Photo:
class OpenPhotoEvent extends FloidAppStateEvent {
  OpenPhotoEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Open Video:
class OpenVideoEvent extends FloidAppStateEvent {
  OpenVideoEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Close Photo:
class ClosePhotoEvent extends FloidAppStateEvent {
  ClosePhotoEvent() : super([]);
  @override
  List<Object> get props => [];
}

// Close Video:
class CloseVideoEvent extends FloidAppStateEvent {
  CloseVideoEvent() : super([]);
  @override
  List<Object> get props => [];
}
// Open Photo Full Screen:
class OpenPhotoFullScreenEvent extends FloidAppStateEvent {
  OpenPhotoFullScreenEvent() : super([]);
  @override
  List<Object> get props => [];
}
// Open Video Full Screen:
class OpenVideoFullScreenEvent extends FloidAppStateEvent {
  OpenVideoFullScreenEvent() : super([]);
  @override
  List<Object> get props => [];
}
// Saved Floid Id:
class SetSavedFloidIdAppStateEvent extends FloidAppStateEvent {
  final int savedFloidId;
  SetSavedFloidIdAppStateEvent({required this.savedFloidId }) : super([]);
  @override
  List<Object> get props => [savedFloidId];
}
// Saved Pin Set:
class SetSavedPinSetIdAppStateEvent extends FloidAppStateEvent {
  final int savedPinSetId;
  SetSavedPinSetIdAppStateEvent({required this.savedPinSetId }) : super([]);
  @override
  List<Object> get props => [savedPinSetId];
}
// Saved Mission Id:
class SetSavedMissionIdAppStateEvent extends FloidAppStateEvent {
  final int savedMissionId;
  SetSavedMissionIdAppStateEvent({required this.savedMissionId }) : super([]);
  @override
  List<Object> get props => [savedMissionId];
}
// Saved All Ids:
class SetSavedAllIdsAppStateEvent extends FloidAppStateEvent {
  final int savedFloidId;
  final int savedPinSetId;
  final int savedMissionId;
  SetSavedAllIdsAppStateEvent({required this.savedFloidId,required this.savedPinSetId,required this.savedMissionId }) : super([]);
  @override
  List<Object> get props => [savedFloidId,savedPinSetId,savedMissionId];
}

// Map Type:
class SetMapTypeAppStateEvent extends FloidAppStateEvent {
  final String mapType;
  SetMapTypeAppStateEvent({required this.mapType }) : super([]);
  @override
  List<Object> get props => [mapType];
}


// Elevation Location:
class SetElevationLocationAppStateEvent extends FloidAppStateEvent {
  final String elevationLocation;
  SetElevationLocationAppStateEvent({required this.elevationLocation }) : super([]);
  @override
  List<Object> get props => [elevationLocation];
}

// Outstanding Path Elevation Request:
class OutstandingPathElevationRequestAppStateEvent extends FloidAppStateEvent {
  final FloidPathElevationRequest floidPathElevationRequest;
  OutstandingPathElevationRequestAppStateEvent({required this.floidPathElevationRequest }) : super([]);
  @override
  List<Object> get props => [floidPathElevationRequest];
}

// Path Elevation Request Complete App State Event:
class PathElevationRequestCompleteAppStateEvent extends FloidAppStateEvent {
  final FloidPathElevationRequest floidPathElevationRequest;
  final FloidErrors floidErrors;
  PathElevationRequestCompleteAppStateEvent({required this.floidPathElevationRequest, required this.floidErrors }) : super([]);
  @override
  List<Object> get props => [floidPathElevationRequest, floidErrors];
}

// States:
abstract class FloidAppState {
  FloidAppState([List props = const []]) : super();
}

class FloidAppStateEmpty extends FloidAppState {
}

class FloidAppStateLoaded extends FloidAppState {
  final FloidAppStateAttributes floidAppStateAttributes;
  FloidAppStateLoaded({required this.floidAppStateAttributes})
      : super([floidAppStateAttributes]);
}

class FloidAppStateError extends FloidAppState {
}

class FloidAppStateBloc extends Bloc<FloidAppStateEvent, FloidAppState> {
  FloidAppStateBloc() : super(FloidAppStateLoaded(
      floidAppStateAttributes: FloidAppStateAttributes(
          flightDistance: -1, readyToExecute: false, floidErrors: FloidErrors(errors: []),))) {
    on<FloidAppStateEvent>((FloidAppStateEvent event,
        Emitter<FloidAppState> emit) async {
      if (event is LoadAppStateEvent) {
        AddressProxy addressProxy = AddressProxy();
        int savedFloidId = await addressProxy.getLastFloid();
        int savedMissionId = await addressProxy.getLastMission();
        int savedPinSetId = await addressProxy.getLastPinSet();
        emit(FloidAppStateLoaded(
            floidAppStateAttributes: FloidAppStateAttributes(flightDistance: -1,
                readyToExecute: false,
                floidErrors: FloidErrors(errors: []),
                savedFloidId: savedFloidId,
                savedMissionId: savedMissionId,
                savedPinSetId: savedPinSetId)));
      }
      final FloidAppState currentState = state;
      // Set Errors:
      if (event is SetErrorsAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            if(event.clearErrors) {
              floidAppStateAttributes.floidErrors = FloidErrors(errors: []);
            }
            final FloidErrors fFloidErrors = floidAppStateAttributes.floidErrors;
            fFloidErrors.addAll(event.floidErrors);
            // If we have warnings or errors, we are not ready to execute - we need to have an empty list...
            if (fFloidErrors.hasErrorsOrWarnings()) {
              floidAppStateAttributes.readyToExecute = false;
            }
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetErrorsAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Zoom to Pins:
      if (event is ZoomToPinsAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.zoomToPinsFlag++;
            if (floidAppStateAttributes.zoomToPinsFlag > 2) {
              floidAppStateAttributes.zoomToPinsFlag = 0;
            }
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('ZoomToPinsAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Open Floid Droid Systems:
      if (event is OpenFloidDroidSystemsEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.floidDroidSystemsInfoOpen = true;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('OpenFloidDroidSystemsEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Close Floid Droid Systems:
      if (event is CloseFloidDroidSystemsEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.floidDroidSystemsInfoOpen = false;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('OpenFloidDroidSystemsEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Open Floid Systems:
      if (event is OpenFloidSystemsEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.floidSystemsInfoOpen = true;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('OpenFloidDroidSystemsEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Close Floid Systems:
      if (event is CloseFloidSystemsEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.floidSystemsInfoOpen = false;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('OpenFloidDroidSystemsEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Ready to execute:
      if (event is SetReadyToExecuteAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.readyToExecute = event.readyToExecute;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetReadyToExecuteAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Flight Distance:
      if (event is SetFlightDistanceAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.flightDistance = event.flightDistance;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetFlightDistanceAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Open Photo:
      if (event is OpenPhotoEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.photoVideoState =
                FloidAppStateAttributes.PHOTO_VIDEO_STATE_PHOTO_OPEN;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetFlightDistanceAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Close Photo:
      if (event is ClosePhotoEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.photoVideoState =
                FloidAppStateAttributes.PHOTO_VIDEO_STATE_ALL_CLOSED;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('ClosePhotoEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Open Video:
      if (event is OpenVideoEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.photoVideoState =
                FloidAppStateAttributes.PHOTO_VIDEO_STATE_VIDEO_OPEN;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('OpenVideoEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Close Video:
      if (event is CloseVideoEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.photoVideoState =
                FloidAppStateAttributes.PHOTO_VIDEO_STATE_ALL_CLOSED;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('CloseVideoEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Open Photo Full Screen:
      if (event is OpenPhotoFullScreenEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.photoVideoState =
                FloidAppStateAttributes.PHOTO_VIDEO_STATE_PHOTO_FULL_SCREEN;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('OpenPhotoFullScreenEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Open Video Full Screen:
      if (event is OpenVideoFullScreenEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.photoVideoState =
                FloidAppStateAttributes.PHOTO_VIDEO_STATE_VIDEO_FULL_SCREEN;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('OpenVideoFullScreenEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Rotate Photo:
      if (event is RotatePhotoEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            if (++floidAppStateAttributes.photoRotation == 4) {
              floidAppStateAttributes.photoRotation = 0;
            }
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('RotatePhotoEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Rotate Video:
      if (event is RotateVideoFrameEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            if (++floidAppStateAttributes.videoFrameRotation == 4) {
              floidAppStateAttributes.videoFrameRotation = 0;
            }
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('RotateVideoFrameEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }

      // Show Droid Gps Lines:
      if (event is ShowDroidGpsLinesEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.showDroidGpsLines = true;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('ShowDroidGpsLinesEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Hide Droid Gps Lines:
      if (event is HideDroidGpsLinesEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.showDroidGpsLines = false;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('HideDroidGpsLinesEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }

      // Show Floid Gps Lines:
      if (event is ShowFloidGpsLinesEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.showFloidGpsLines = true;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('ShowFloidGpsLinesEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }

      // Hide Floid Gps Lines:
      if (event is HideFloidGpsLinesEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.showFloidGpsLines = false;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('HideFloidGpsLinesEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }

      // Show Legend:
      if (event is ShowLegendEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.showLegend = true;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('ShowLegendEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }

      // Hide Legend:
      if (event is HideLegendEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.showLegend = false;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('HideLegendEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Set server status:
      if (event is SetServerStatusAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.floidServerStatus = event.floidServerStatus;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetServerStatusAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Floid is alive status:
      if (event is SetFloidAliveAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.floidIsAlive = event.floidIsAlive;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetFloidAliveAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      if (event is ZoomInEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.zoomInFlag++;
            if (floidAppStateAttributes.zoomInFlag > 2) {
              floidAppStateAttributes.zoomInFlag = 0;
            }
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('ZoomInEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      if (event is ZoomOutEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.zoomOutFlag++;
            if (floidAppStateAttributes.zoomOutFlag > 2) {
              floidAppStateAttributes.zoomOutFlag = 0;
            }
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('ZoomOutEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Set Saved Floid Id:
      if (event is SetSavedFloidIdAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.savedFloidId = event.savedFloidId;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetSavedFloidIdAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Set Saved Pin Set Id:
      if (event is SetSavedPinSetIdAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.savedPinSetId = event.savedPinSetId;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetSavedPinSetIdAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Set Saved Mission Id:
      if (event is SetSavedMissionIdAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.savedMissionId = event.savedMissionId;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetSavedMissionIdAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Set All Saved Id:
      if (event is SetSavedAllIdsAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.savedFloidId = event.savedFloidId;
            floidAppStateAttributes.savedPinSetId = event.savedPinSetId;
            floidAppStateAttributes.savedMissionId = event.savedMissionId;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetSavedAllIdsAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // MapType:
      if (event is SetMapTypeAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.mapType = event.mapType;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetMapTypeAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Elevation Location:
      if (event is SetElevationLocationAppStateEvent) {
        try {
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            floidAppStateAttributes.elevationLocation = event.elevationLocation;
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
          }
        } catch (e, stacktrace) {
          print('SetElevationLocationAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Outstanding Path Elevation Request:
      if (event is OutstandingPathElevationRequestAppStateEvent) {
        print("Outstanding path registering");
        try {
          final floidPathElevationRequest = event.floidPathElevationRequest;
          if(floidPathElevationRequest.flyLineCheckId == FloidErrorInstance.NO_REF_ID) {
            print('ERROR: PATH REFERENCE ID IS: ' + FloidErrorInstance.NO_REF_ID.toString());
          }
          // 1. Add this to our list:
          if (FloidAppStateAttributes
              .pendingPathElevationRequestSet[floidPathElevationRequest
              .flyLineCheckId] == null) {
            FloidAppStateAttributes
                .pendingPathElevationRequestSet[floidPathElevationRequest
                .flyLineCheckId] = Map();
          }
          FloidAppStateAttributes
              .pendingPathElevationRequestSet[floidPathElevationRequest
              .flyLineCheckId]?[floidPathElevationRequest.pathCheckId] =
              floidPathElevationRequest;
          if (currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            // Make sure we add a warning to the errors if needed:
            if (addPathElevationNotCompleteWarning(floidAppStateAttributes,
                floidPathElevationRequest.flyLineCheckId)) {
              emit(FloidAppStateLoaded(
                  floidAppStateAttributes: floidAppStateAttributes));
            }
          }
        } catch (e, stacktrace) {
          print('OutstandingPathElevationRequestAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
      // Path Elevation Request Complete:
      if (event is PathElevationRequestCompleteAppStateEvent) {
        try {
          bool modified = false;
          final FloidErrors floidErrors = event.floidErrors;
          if(floidErrors.errors.isNotEmpty) {
            print('Path request complete - errors:');
            floidErrors.errors.forEach((error) {
              print('  ' + error.referenceId.toString() + ' ' + error.floidError.message);
            });
          } else {
            print("Path request complete - ok");
          }
          final floidPathElevationRequest = event.floidPathElevationRequest;
          // Remove this pending request:
          FloidAppStateAttributes
              .pendingPathElevationRequestSet[floidPathElevationRequest
              .flyLineCheckId]?.remove(floidPathElevationRequest.pathCheckId);
          // Check to see if we have any outstanding path requests with this flyline id:
          final Map<int, FloidPathElevationRequest>? pendingPathElevationRequestsForThisId = FloidAppStateAttributes
              .pendingPathElevationRequestSet[floidPathElevationRequest
              .flyLineCheckId];
          if (pendingPathElevationRequestsForThisId != null &&
              pendingPathElevationRequestsForThisId.length == 0) {
            FloidAppStateAttributes.pendingPathElevationRequestSet.remove(
                floidPathElevationRequest.flyLineCheckId);
            print('Done Pending Elevation Requests');
            // Remove the warning:
            if (currentState is FloidAppStateLoaded) {
              final FloidAppStateLoaded floidAppStateLoaded = currentState;
              final FloidAppStateAttributes floidAppStateAttributes = floidAppStateLoaded
                  .floidAppStateAttributes;
              final FloidErrors fFloidErrors = floidAppStateAttributes.floidErrors;
              if(floidErrors.errors.isNotEmpty) {
                print('Current Errors before modification:');
                fFloidErrors.errors.forEach((error) {
                  print('  ' + error.referenceId.toString() + ' ' +
                      error.floidError.message);
                });
              }
              if (fFloidErrors.hasThisErrorWithThisReference(
                  FloidError.FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING,
                  floidPathElevationRequest.flyLineCheckId)) {
                print('Removing Pending Elevation Request Warning');
                fFloidErrors.removeThisErrorWithThisReference(
                    FloidError
                        .FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING,
                    floidPathElevationRequest.flyLineCheckId);
                modified = true;
                print('Current Errors after modification:');
                fFloidErrors.errors.forEach((error) {
                  print('  ' + error.referenceId.toString() + ' ' +
                      error.floidError.message);
                });
              }
            }
          }
          // Emit a new state if needed:
          if ((modified || floidErrors.errors.isNotEmpty) && currentState is FloidAppStateLoaded) {
            final FloidAppStateLoaded floidAppStateLoaded = currentState;
            final FloidAppStateAttributes floidAppStateAttributes = FloidAppStateAttributes
                .fromFloidAppStateAttributes(
                floidAppStateLoaded.floidAppStateAttributes);
            // Add in the new errors:
            floidAppStateAttributes.floidErrors.addAll(floidErrors);
            emit(FloidAppStateLoaded(
                floidAppStateAttributes: floidAppStateAttributes));
            print('Ending Errors:');
            floidAppStateAttributes.floidErrors.errors.forEach((error) {
              print('  ' + error.referenceId.toString() + ' ' + error.floidError.message);
            });
          }
        } catch (e, stacktrace) {
          print('PathElevationRequestCompleteAppStateEvent: ' + e.toString() + ' ' + stacktrace.toString());
          emit(FloidAppStateError());
        }
      }
    });
  }

  bool addPathElevationNotCompleteWarning(
      FloidAppStateAttributes floidAppStateAttributes, int referenceId) {
    print('Adding path elevation not complete warning: ' + referenceId.toString());
    bool modified = false;
    try {
      final FloidErrors floidErrors = floidAppStateAttributes.floidErrors;
      print('Current Errors:');
      floidErrors.errors.forEach((error) {
        print('  ' + error.referenceId.toString() + ' ' + error.floidError.message);
      });
        // See if we are at current referenceId:
        if (referenceId == FloidAppStateAttributes.nextFlyLineCheckId - 1) {
          // Yes - does it have the error:
          if (floidErrors.hasThisError(
              FloidError.FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING)) {
            // Yes - is it old?
            if (!floidErrors.hasThisErrorWithThisReference(
                FloidError.FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING,
                referenceId)) {
              // Yes - old - remove this ref and add new one:
              floidErrors.removeThisError(
                  FloidError
                      .FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING);
              floidErrors.add(FloidErrorInstance(
                  floidError: FloidError.getFloidError(FloidError
                      .FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING),
                  referenceId: referenceId));
              modified = true;
            }
            //    If so, check to see if we already have a warning string about outstanding path requests
          } else {
            // No - does not currently have this error:
            floidErrors.add(FloidErrorInstance(
                floidError: FloidError.getFloidError(FloidError
                    .FLIGHT_PATH_ELEVATION_REQUESTS_IN_PROGRESS_WARNING),
                referenceId: referenceId));
            modified = true;
          }
        }
    } catch(e, stacktrace) {
      print('addPathElevationNotCompleteWarning: ' + e.toString() + ' ' + stacktrace.toString());
    }
    return modified;
  }
}
