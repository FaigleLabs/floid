/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// TODO - This is currently unused - ALL Droid and Floid points are filtered - this gives unfiltered - should have a UI toggle to choose between
abstract class FloidGpsPointsEvent extends Equatable {
  FloidGpsPointsEvent([List props = const []]) : super();
}

class FetchFloidGpsPoints extends FloidGpsPointsEvent {
  final int floidId;
  final String floidUuid;

  FetchFloidGpsPoints({required this.floidId, required this.floidUuid})
      : super([floidId, floidUuid]);
  @override
  List<Object> get props => [floidId, floidUuid];
}

class RefreshFloidGpsPoints extends FloidGpsPointsEvent {
  final int floidId;
  final String floidUuid;

  RefreshFloidGpsPoints({required this.floidId, required this.floidUuid})
      : super([floidId, floidUuid]);
  @override
  List<Object> get props => [floidId, floidUuid];
}

class AddFloidGpsPoint extends FloidGpsPointsEvent {
  final FloidGpsPoint floidGpsPoint;
  AddFloidGpsPoint({required this.floidGpsPoint,})
      : super([floidGpsPoint]);
  @override
  List<Object> get props => [floidGpsPoint];
}

abstract class FloidGpsPointsState extends Equatable {
  FloidGpsPointsState([List props = const []]) : super();
}

class FloidGpsPointsEmpty extends FloidGpsPointsState {
  @override
  List<Object> get props => [];
}

class FloidGpsPointsLoading extends FloidGpsPointsState {
  @override
  List<Object> get props => [];
}

class FloidGpsPointsLoaded extends FloidGpsPointsState {
  final List<FloidGpsPoint> gpsPoints;
  final String name;
  final String timestamp;

  FloidGpsPointsLoaded({required this.gpsPoints, required this.name, required this.timestamp})
      : super([gpsPoints, name, timestamp]);
  @override
  List<Object> get props => [gpsPoints, name, timestamp];
}

class FloidGpsPointsError extends FloidGpsPointsState {
  @override
  List<Object> get props => [];
}

class FloidGpsPointsBloc extends Bloc<FloidGpsPointsEvent, FloidGpsPointsState> {
  static const double DEFAULT_KINK = 10.0;
  static const double DEFAULT_KINK_ALPHA = 0.1;
  static const double DEFAULT_SHRINK_ALPHA = 0.75;
  static const double DEFAULT_EXPAND_ALPHA = 1.5;
  static const int DEFAULT_MAX_POINTS_START = 200;
  static const int DEFAULT_MIN_TIME = 5000;
  static const double DEFAULT_MIN_DISTANCE = 2.0;
  static const bool DEFAULT_HIGH_QUALITY = true;

  final FloidServerRepository floidServerRepository;

  FloidGpsPointsBloc({required this.floidServerRepository})
      : super(FloidGpsPointsEmpty()) {
    on<FloidGpsPointsEvent>((FloidGpsPointsEvent event, Emitter<FloidGpsPointsState> emit) async {
      if (event is FetchFloidGpsPoints) {
        emit(FloidGpsPointsLoading());
        try {
          final List<FloidGpsPoint> gpsPoints =
          await floidServerRepository.fetchGpsPoints(
              event.floidId,
              event.floidUuid);
          emit(FloidGpsPointsLoaded(gpsPoints: gpsPoints, name: '${event.floidId}-${event.floidUuid}', timestamp: ''));
        } catch (_) {
          emit(FloidGpsPointsError());
        }
      }
      if (event is RefreshFloidGpsPoints) {
        try {
          final List<FloidGpsPoint> gpsPoints =
          await floidServerRepository.fetchGpsPoints(
              event.floidId,
              event.floidUuid);
          emit(FloidGpsPointsLoaded(name: '${event.floidId}-${event.floidUuid}', gpsPoints: gpsPoints, timestamp: ''));
        } catch (_) {
          emit(FloidGpsPointsError());
        }
      }
      if (event is AddFloidGpsPoint) {
        // We ignore any state but points loaded:
        if (state is FloidGpsPointsLoaded) {
          try {
            final FloidGpsPointsLoaded floidGpsPointsLoaded = state as FloidGpsPointsLoaded;
            // We add in the point unfiltered:
            floidGpsPointsLoaded.gpsPoints.add(event.floidGpsPoint);
            emit(FloidGpsPointsEmpty());
            emit(FloidGpsPointsLoaded(name: '${event.floidGpsPoint.floidId}-${event.floidGpsPoint.floidUuid}', gpsPoints: floidGpsPointsLoaded.gpsPoints, timestamp: ''));
          } catch (_) {
            emit(FloidGpsPointsError());
          }
        } else {
          print('Floid GPS points not loaded - point ignored');
        }
      }
    });
  }
}
