/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidUuidAndStateListEvent extends Equatable {
  FloidUuidAndStateListEvent([List props = const []]) : super();
}

class FetchFloidUuidAndStateList extends FloidUuidAndStateListEvent {
  final FloidIdAndState floidIdAndState;

  FetchFloidUuidAndStateList({required this.floidIdAndState})
      : super([floidIdAndState]);
  @override
  List<Object> get props => [floidIdAndState];
}

class AddNewFloidUuidAndState extends FloidUuidAndStateListEvent {
  final FloidIdAndState floidIdAndState;
  final FloidUuidAndState floidUuidAndState;
  AddNewFloidUuidAndState({required this.floidIdAndState, required this.floidUuidAndState})
      : super([floidIdAndState, floidUuidAndState]);
  @override
  List<Object> get props => [floidIdAndState, floidUuidAndState];
}

abstract class FloidUuidAndStateListState extends Equatable {
  FloidUuidAndStateListState([List props = const []]) : super();
}

class FloidUuidAndStateListEmpty extends FloidUuidAndStateListState {
  @override
  List<Object> get props => [];
}

class FloidUuidAndStateListLoading extends FloidUuidAndStateListState {
  @override
  List<Object> get props => [];
}

class FloidUuidAndStateListLoaded extends FloidUuidAndStateListState {
  final FloidIdAndState floidIdAndState;
  final List<FloidUuidAndState> floidUuidAndStateList;

  FloidUuidAndStateListLoaded({required this.floidUuidAndStateList, required this.floidIdAndState})
      : super([floidUuidAndStateList]);
  @override
  List<Object> get props => [floidIdAndState,floidUuidAndStateList];
}

class FloidUuidAndStateListError extends FloidUuidAndStateListState {
  @override
  List<Object> get props => [];
}

class FloidUuidAndStateListBloc extends Bloc<FloidUuidAndStateListEvent, FloidUuidAndStateListState> {
  final FloidServerRepository floidServerRepository;

  FloidUuidAndStateListBloc({required this.floidServerRepository})
      : super(FloidUuidAndStateListEmpty()) {
    on<FloidUuidAndStateListEvent>((FloidUuidAndStateListEvent event,
        Emitter<FloidUuidAndStateListState> emit) async {
      if (event is FetchFloidUuidAndStateList) {
        emit(FloidUuidAndStateListLoading());
        try {
          final List<
              FloidUuidAndState> floidUuidAndStateList = await floidServerRepository
              .fetchFloidUuidAndStateList(event.floidIdAndState.floidId);
          emit(FloidUuidAndStateListLoaded(
              floidIdAndState: event.floidIdAndState,
              floidUuidAndStateList: floidUuidAndStateList));
        } catch (_) {
          emit(FloidUuidAndStateListError());
        }
      }
      if (event is AddNewFloidUuidAndState) {
        try {
          if (state is FloidUuidAndStateListLoaded) {
            FloidUuidAndStateListLoaded floidUuidAndStateListLoadedState = state as FloidUuidAndStateListLoaded;
            // Add this to the top of the list:
            List<FloidUuidAndState> newFloidUuidAndStateList = [
              event.floidUuidAndState
            ]
              ..addAll(floidUuidAndStateListLoadedState.floidUuidAndStateList);
            emit(FloidUuidAndStateListLoaded(
              floidIdAndState: event.floidIdAndState,
              floidUuidAndStateList: newFloidUuidAndStateList,
            ));
          }
        } catch (_) {
          emit(FloidUuidAndStateListError());
        }
      }
    });
  }
}
