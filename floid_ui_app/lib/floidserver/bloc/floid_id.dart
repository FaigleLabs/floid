/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidIdEvent extends Equatable {
  FloidIdEvent([List props = const []]) : super();
}

class SetFloidIdAndState extends FloidIdEvent {
  final FloidIdAndState floidIdAndState;

  SetFloidIdAndState({required this.floidIdAndState})
      : super([floidIdAndState]);
  @override
  List<Object> get props => [floidIdAndState];
}

class SetFloidUuidAndState extends FloidIdEvent {
  final FloidIdAndState floidIdAndState;
  final FloidUuidAndState floidUuidAndState;

  SetFloidUuidAndState({required this.floidIdAndState, required this.floidUuidAndState})
      : super([floidIdAndState, floidUuidAndState]);
  @override
  List<Object> get props => [floidIdAndState, floidUuidAndState];
}

class DeleteFloidId extends FloidIdEvent {
  final int floidId;

  DeleteFloidId({required this.floidId})
      : super([floidId]);
  @override
  List<Object> get props => [floidId];
}

abstract class FloidIdState extends Equatable {
  FloidIdState([List props = const []]) : super();
}

class FloidIdEmpty extends FloidIdState {
  @override
  List<Object> get props => [];
}

class FloidIdLoading extends FloidIdState {
  @override
  List<Object> get props => [];
}

class FloidIdLoaded extends FloidIdState {
  final FloidIdAndState floidIdAndState;

  FloidIdLoaded({required this.floidIdAndState})
      : super([floidIdAndState]);
  @override
  List<Object> get props => [floidIdAndState];
}

class FloidUuidLoaded extends FloidIdState {
  final FloidIdAndState floidIdAndState;
  final FloidUuidAndState floidUuidAndState;
  FloidUuidLoaded({required this.floidIdAndState, required this.floidUuidAndState})
      : super([floidIdAndState, floidUuidAndState]);
  @override
  List<Object> get props => [floidIdAndState, floidUuidAndState];
}

class FloidIdError extends FloidIdState {
  @override
  List<Object> get props => [];
}

class FloidIdBloc extends Bloc<FloidIdEvent, FloidIdState> {
  final FloidServerRepository floidServerRepository;
  FloidIdBloc({required this.floidServerRepository}): super(FloidIdEmpty()) {
    on<FloidIdEvent>((FloidIdEvent event, Emitter<FloidIdState> emit) async {
      if (event is SetFloidIdAndState) {
        emit(FloidIdLoaded(floidIdAndState: event.floidIdAndState));
      }
      if (event is SetFloidUuidAndState) {
        emit(FloidUuidLoaded(floidIdAndState: event.floidIdAndState, floidUuidAndState: event.floidUuidAndState));
      }
      // Delete a Floid Id:
      if (event is DeleteFloidId) {
        emit(FloidIdLoading());
        try {
          final bool floidIdDeleted = await floidServerRepository.deleteFloidId(event.floidId);
          if(floidIdDeleted) {
            // Tell the floid id list to load the new list and load the first pin set:
            emit(FloidIdEmpty());
          } else {
            emit(FloidIdError());
          }
        } catch (_) {
          emit(FloidIdError());
        }
      }
    });
  }
}
