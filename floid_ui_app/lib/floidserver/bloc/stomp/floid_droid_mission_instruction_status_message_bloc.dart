/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/floidserver/models/floid_droid_mission_instruction_status.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

abstract class FloidDroidMissionInstructionStatusMessageEvent extends Equatable {
  FloidDroidMissionInstructionStatusMessageEvent([List props = const []]) : super();
}

class NewFloidDroidMissionInstructionStatusMessageEvent extends FloidDroidMissionInstructionStatusMessageEvent {
  final int floidId;
  final String floidUuid;
  final  FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage;

  NewFloidDroidMissionInstructionStatusMessageEvent({
    required this.floidDroidMissionInstructionStatusMessage,
    required this.floidId,
    required this.floidUuid,
  }) : super([floidDroidMissionInstructionStatusMessage, floidId, floidUuid]);

  @override
  List<Object> get props => [floidId, floidUuid, floidDroidMissionInstructionStatusMessage];
}

/// Event for setting the Floid Id and Floid Uuid:
class UpdateFloidDroidMissionInstructionStatusMessageFloidId extends FloidDroidMissionInstructionStatusMessageEvent {
  final int floidId;
  final String floidUuid;

  UpdateFloidDroidMissionInstructionStatusMessageFloidId({
    required this.floidId,
    required this.floidUuid,
  }) : super([floidId]);

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Generic State:
class FloidDroidMissionInstructionStatusMessageState extends Equatable {
  FloidDroidMissionInstructionStatusMessageState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidDroidMissionInstructionStatusMessageEmpty extends FloidDroidMissionInstructionStatusMessageState {

  FloidDroidMissionInstructionStatusMessageEmpty() : super();

  @override
  List<Object> get props => [];
}

/// State loaded:
class FloidDroidMissionInstructionStatusMessageHasId extends FloidDroidMissionInstructionStatusMessageState {
  /// The Floid Id
  final int floidId;
  /// The Floid UUID
  final String floidUuid;

  FloidDroidMissionInstructionStatusMessageHasId({required this.floidId, required this.floidUuid}) : super();

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Floid Droid Mission Instruction Status Message Loaded:
class FloidDroidMissionInstructionStatusMessageLoaded extends FloidDroidMissionInstructionStatusMessageHasId {
  final FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage;
  final FloidDroidMissionInstructionStatus floidDroidMissionInstructionStatus;

  FloidDroidMissionInstructionStatusMessageLoaded({required this.floidDroidMissionInstructionStatusMessage,
    required this.floidDroidMissionInstructionStatus,
    required floidId,
    required floidUuid}) : super(floidId: floidId, floidUuid: floidUuid);

  @override
  List<Object> get props => [floidDroidMissionInstructionStatusMessage, floidId, floidUuid];
}

class FloidDroidMissionInstructionStatusMessageBloc extends Bloc<FloidDroidMissionInstructionStatusMessageEvent, FloidDroidMissionInstructionStatusMessageState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;
  /// The stomp client:
  Future<StompClient>? stompClient;

  /// Subscribe to the FloidDroidMissionInstructionStatusMessage stomp client:
  Future<StompClient> subscribe(int floidId, String floidUuid) {
    // Subscribe to Floid Droid Mission Instruction Status STOMP endpoint
    return floidServerRepository.subscribeFloidDroidMissionInstructionStatus(floidId, floidUuid,
            (Map<String, String> headers, String message) {
          // Dispatch the new floid droid mission instruction status message event:
          FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = FloidDroidMissionInstructionStatusMessage.fromJson(jsonDecode(message));
          add(new NewFloidDroidMissionInstructionStatusMessageEvent(
              floidDroidMissionInstructionStatusMessage: floidDroidMissionInstructionStatusMessage,
              floidId: floidId, floidUuid: floidUuid));
        });
  }

  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidDroidMissionInstructionStatusMessage cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_DROID_MISSION_INSTRUCTION_STATUS_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidDroidMissionInstructionStatusMessage cancel: stompClient null');
    }
  }

  FloidDroidMissionInstructionStatusMessageBloc({required this.floidServerRepository}): super(FloidDroidMissionInstructionStatusMessageEmpty()) {
    on<FloidDroidMissionInstructionStatusMessageEvent>((FloidDroidMissionInstructionStatusMessageEvent event, Emitter<FloidDroidMissionInstructionStatusMessageState> emit) async {
      // We have a new floid id and floid uuid:
      if(event is UpdateFloidDroidMissionInstructionStatusMessageFloidId) {
        // If we had id's before, then cancel:
        if(state is FloidDroidMissionInstructionStatusMessageHasId) {
          try {
            print('Unsubscribing from Floid Droid Mission Instruction Status Message events');
            cancel();
          } catch(e) {
            print('Error cancelling the Floid Droid Mission Instruction Status Message stomp' + e.toString());
          }
        }
        // Subscribe to the new floid id and uuid:
        print('Subscribing to Floid Droid Mission Instruction Status Message events');
        stompClient = subscribe(event.floidId, event.floidUuid);
        // We have loaded the floid Id and Uuid:
        emit(FloidDroidMissionInstructionStatusMessageHasId(floidId: event.floidId, floidUuid: event.floidUuid));
      }

      // We have received a new floid droid mission instruction message:
      if(event is NewFloidDroidMissionInstructionStatusMessageEvent) {
        // Get the previous state so we can update from new message:
        print('NewFloidDroidMissionInstructionStatusMessageEvent(floidDroidMissionInstructionStatusMessage)');
        FloidDroidMissionInstructionStatus? previousFloidDroidMissionInstructionStatus = state is FloidDroidMissionInstructionStatusMessageLoaded? (state as FloidDroidMissionInstructionStatusMessageLoaded).floidDroidMissionInstructionStatus: null;
        FloidDroidMissionInstructionStatus floidDroidMissionInstructionStatus = FloidDroidMissionInstructionStatus(floidDroidMissionInstructionStatusMessage: event.floidDroidMissionInstructionStatusMessage, previousFloidDroidMissionInstructionStatus: previousFloidDroidMissionInstructionStatus);
        FloidDroidMissionInstructionStatusMessageState floidDroidMissionInstructionStatusMessageState = FloidDroidMissionInstructionStatusMessageLoaded(floidId: event.floidId, floidUuid: event.floidUuid, floidDroidMissionInstructionStatusMessage: event.floidDroidMissionInstructionStatusMessage, floidDroidMissionInstructionStatus: floidDroidMissionInstructionStatus);
        emit(floidDroidMissionInstructionStatusMessageState);
      }
    });
  }
}
