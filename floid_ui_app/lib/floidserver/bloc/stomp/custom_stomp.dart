/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import "dart:async";
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:floid_ui_app/floidserver/trust/floid_server_trust.dart';
import "package:stomp/stomp.dart";
import "package:stomp/impl/plugin.dart";
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;

// SEE HERE FOR THE REASON THIS IS USED INSTEAD OF THE vm.dart VERSION THAT COMES WITH IT:
//   https://github.com/rikulo/stomp/issues/19
Future<StompClient> connect(FloidServerTrust floidServerTrust,
    String url,
    {String? host,
      String? login,
      String? passcode,
      String? authorization,
      List<int>? heartbeat,
      void onConnect(StompClient client, Map<String, String>? headers)?,
      void onDisconnect(StompClient client)?,
      void onError(StompClient client, String? message, String? detail,
          Map<String, String>? headers)?,
      void onFault(StompClient client, error, stackTrace)?}) async {
// FROM HERE: https://stackoverflow.com/questions/53721745/dart-upgrade-client-socket-to-websocket
  Random r = new Random();
  String key = base64.encode(List<int>.generate(8, (_) => r.nextInt(255)));

  HttpClient client = HttpClient();
  client.badCertificateCallback = floidServerTrust.badCertificateCallback;
  String stompHttpUrl = url.replaceAll('wss', 'https').replaceAll('ws', 'http');
  HttpClientRequest request = await client.getUrl(Uri.parse(stompHttpUrl));
  request.headers.add('Connection', 'upgrade');
  request.headers.add('Upgrade', 'websocket');
  request.headers.add('sec-websocket-version', '13'); // insert the correct version here
  request.headers.add('sec-websocket-key', key);
  if(authorization!=null) request.headers.add(HttpHeaders.authorizationHeader, authorization);

  HttpClientResponse response = await request.close();
  if(response.statusCode==101) {
    //ignore: close_sinks
    Socket socket = await response.detachSocket();
    //ignore: close_sinks
    WebSocket ws = WebSocket.fromUpgradedSocket(
      socket,
      serverSide: false,
    );
    return connectWith(IOWebSocketChannel(ws),
//        IOWebSocketChannel.connect(url, headers: {HttpHeaders.authorizationHeader: authorization}),
        host: host,
        login: login,
        passcode: passcode,
        authorization: authorization,
        heartbeat: heartbeat,
        onConnect: onConnect,
        onDisconnect: onDisconnect,
        onError: onError,
        onFault: onFault);
  } else {
    throw Exception('Bad Response for: $url = ' + response.statusCode.toString());
  }
}

Future<StompClient> connectWith(IOWebSocketChannel channel,
    {String? host,
      String? login,
      String? passcode,
      String? authorization,
      List<int>? heartbeat,
      void onConnect(StompClient client, Map<String, String>? headers)?,
      void onDisconnect(StompClient client)?,
      void onError(StompClient client, String? message, String? detail,
          Map<String, String>? headers)?,
      void onFault(StompClient client, error, stackTrace)?
    }) =>
    StompClient.connect(_WSStompConnector.startWith(channel),
        host: host,
        login: login,
        passcode: passcode,
        authorization: authorization,
        heartbeat: heartbeat,
        onConnect: onConnect,
        onDisconnect: onDisconnect,
        onError: onError,
        onFault: onFault);

class _WSStompConnector extends StringStompConnector {
  final IOWebSocketChannel _socket;
  StreamSubscription? _listen;


  static _WSStompConnector startWith(IOWebSocketChannel socket) =>
      new _WSStompConnector(socket);

  _WSStompConnector(this._socket) {
    _listen = _socket.stream.listen((data) {
//      print("Read $data");
      if (data != null) {
        final String sData = data.toString();
        final StringCallback? fOnString = onString;
        if (sData.isNotEmpty && fOnString != null) fOnString(sData);
      }
    });

    final ErrorCallback? fOnError = onError;
    if (fOnError != null) _listen?.onError((err) => fOnError(err, null));
    final CloseCallback? fOnClose = onClose;
    if (fOnClose != null) _listen?.onDone(() => fOnClose());

    if (fOnError != null) _socket.stream.handleError((error) =>
        fOnError(error, null));

    _socket.sink.done.then((v) {
      if (fOnClose != null)
        fOnClose();
    });
  }


  @override
  // ignore: non_constant_identifier_names
  void writeString_(String string) {
//    print("Write $string");
    _socket.sink.add(string);
  }

  @override
  Future close() {
//    print("Closing");
    _listen?.cancel();
    _socket.sink.close(status.goingAway);
    return new Future.value();
  }
}
