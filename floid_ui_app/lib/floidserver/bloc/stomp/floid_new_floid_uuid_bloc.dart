/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

abstract class FloidNewFloidUuidEvent extends Equatable {
  FloidNewFloidUuidEvent([List props = const []]) : super();
}

class StartFloidNewFloidUuidEvent extends FloidNewFloidUuidEvent {
  final FloidIdAndState floidIdAndState;
  StartFloidNewFloidUuidEvent({
    required this.floidIdAndState,
  }) : super([floidIdAndState]);

  @override
  String toString() => 'StartFloidNewFloidUuidEvent { floidId: ' + floidIdAndState.floidId.toString() + ' }';
  @override
  List<Object> get props => [floidIdAndState];
}

/// Generic State:
class FloidNewFloidUuidState extends Equatable {
  FloidNewFloidUuidState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidNewFloidUuidEmpty extends FloidNewFloidUuidState {
  FloidNewFloidUuidEmpty() : super();
  @override
  List<Object> get props => [];
}

/// State status loading:
class FloidNewFloidUuidLoading extends FloidNewFloidUuidState {
  FloidNewFloidUuidLoading() : super();
  @override
  List<Object> get props => [];
}

/// State status loaded:
class FloidNewFloidUuidLoaded extends FloidNewFloidUuidState {
  final FloidIdAndState floidIdAndState;
  final FloidUuidAndState floidUuidAndState;
  FloidNewFloidUuidLoaded({required this.floidIdAndState,required this.floidUuidAndState,}) : super();
  @override
  List<Object> get props => [floidIdAndState, floidUuidAndState];
}

/// State error:
class FloidNewFloidUuidError extends FloidNewFloidUuidState {
  FloidNewFloidUuidError() : super();
  @override
  List<Object> get props => [];
}

class NewFloidUuidEvent extends FloidNewFloidUuidEvent {
  final FloidIdAndState floidIdAndState;
  final  FloidUuidAndState floidUuidAndState;
  NewFloidUuidEvent({
    required this.floidIdAndState,
    required this.floidUuidAndState,
  }) : super([floidIdAndState, floidUuidAndState]);

  @override
  String toString() =>
      'NewFloidUuid { floidNewFloidUuid: $floidUuidAndState}';
  @override
  List<Object> get props => [floidIdAndState, floidUuidAndState];
}

class FloidNewFloidUuidBloc extends Bloc<FloidNewFloidUuidEvent, FloidNewFloidUuidState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;

  /// The Floid Id
  FloidIdAndState floidIdAndState = FloidIdAndState();

  /// The stomp client:
  Future<StompClient>? stompClient;

  FloidNewFloidUuidBloc({required this.floidServerRepository})
      : super(FloidNewFloidUuidEmpty()) {
    on<FloidNewFloidUuidEvent>((FloidNewFloidUuidEvent event,
        Emitter<FloidNewFloidUuidState> emit) async {
      if(event is StartFloidNewFloidUuidEvent) {
        try {
          this.floidIdAndState = event.floidIdAndState;
          if (stompClient != null) {
            cancel();
          }
          stompClient = subscribe();
          emit(FloidNewFloidUuidLoading());
        } catch(e) {
          emit(FloidNewFloidUuidError());
        }
      }
      if(event is NewFloidUuidEvent) {
        try {
          FloidNewFloidUuidState floidNewFloidUuidState = FloidNewFloidUuidLoaded(floidUuidAndState: event.floidUuidAndState, floidIdAndState: event.floidIdAndState);
          emit(floidNewFloidUuidState);
        } catch(e) {
          emit(FloidNewFloidUuidError());
        }
      }

    });
  }

  Future<StompClient> subscribe() {
    return floidServerRepository.subscribeFloidNewFloidUuid(
        floidIdAndState.floidId,
            (Map<String, String> headers, String message) {
          // Dispatch the new floid uuid message event:
          FloidUuidAndState floidUuidAndState = FloidUuidAndState.fromJson(
              jsonDecode(message));
          add(NewFloidUuidEvent(floidIdAndState: floidIdAndState,
              floidUuidAndState: floidUuidAndState));
        });
  }

  /// Cancel the connection to the stomp client:
  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidNewFloidUuid cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_NEW_FLOID_UUID_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidNewFloidUuid cancel: stompClient null');
    }
  }
}
