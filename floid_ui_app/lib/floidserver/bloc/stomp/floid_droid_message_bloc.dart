/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

abstract class FloidDroidMessageEvent extends Equatable {
  FloidDroidMessageEvent([List props = const []]) : super();
}

class NewFloidDroidMessageEvent extends FloidDroidMessageEvent {
  final int floidId;
  final String floidUuid;
  final  FloidDroidMessage floidDroidMessage;

  NewFloidDroidMessageEvent({
    required this.floidDroidMessage,
    required this.floidId,
    required this.floidUuid,
  }) : super([floidDroidMessage, floidId, floidUuid]);

  @override
  List<Object> get props => [floidId, floidUuid, floidDroidMessage];
}

/// Event for setting the Floid Id and Floid Uuid:
class UpdateFloidDroidMessageFloidId extends FloidDroidMessageEvent {
  final int floidId;
  final String floidUuid;

  UpdateFloidDroidMessageFloidId({
    required this.floidId,
    required this.floidUuid,
  }) : super([floidId]);

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Generic State:
class FloidDroidMessageState extends Equatable {
  FloidDroidMessageState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidDroidMessageEmpty extends FloidDroidMessageState {

  FloidDroidMessageEmpty() : super();

  @override
  List<Object> get props => [];
}

/// State loaded:
class FloidDroidMessageHasId extends FloidDroidMessageState {
  /// The Floid Id
  final int floidId;
  /// The Floid UUID
  final String floidUuid;

  FloidDroidMessageHasId({required this.floidId, required this.floidUuid}) : super();

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Floid Droid Message Loaded:
class FloidDroidMessageLoaded extends FloidDroidMessageHasId {
  final FloidDroidMessage floidDroidMessage;

  FloidDroidMessageLoaded({required this.floidDroidMessage,
    required floidId, required floidUuid}) : super(floidId: floidId, floidUuid: floidUuid);

  @override
  List<Object> get props => [floidDroidMessage, floidId, floidUuid];
}

class FloidDroidMessageBloc extends Bloc<FloidDroidMessageEvent, FloidDroidMessageState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;
  /// The stomp client:
  Future<StompClient>? stompClient;

  /// Subscribe to the FloidDroidMessage stomp client:
  Future<StompClient> subscribe(int floidId, String floidUuid) {
    return floidServerRepository.subscribeFloidDroidMessage(floidId, floidUuid,
            (Map<String, String> headers, String message) {
          // Dispatch the new floid droid message event:
          FloidDroidMessage floidDroidMessage = FloidDroidMessage(message);
          add(NewFloidDroidMessageEvent(
              floidDroidMessage: floidDroidMessage,
              floidId: floidId, floidUuid: floidUuid));
        });
  }

  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidDroidMessage cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_DROID_MESSAGE_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidDroidMessage cancel: stompClient null');
    }
  }

  FloidDroidMessageBloc({required this.floidServerRepository}): super(FloidDroidMessageEmpty()) {
    on<FloidDroidMessageEvent>((FloidDroidMessageEvent event, Emitter<FloidDroidMessageState> emit) async {
      // We have a new floid id and floid uuid:
      if(event is UpdateFloidDroidMessageFloidId) {
        // If we had id's before, then cancel:
        if(state is FloidDroidMessageHasId) {
          try {
            print('Unsubscribing from Floid Droid Message events');
            cancel();
          } catch(e) {
            print('Error cancelling the Floid Droid Message stomp' + e.toString());
          }
        }
        // Subscribe to the new floid id and uuid:
        print('Subscribing to Floid Droid Message events');
        stompClient = subscribe(event.floidId, event.floidUuid);
        // We have loaded the floid Id and Uuid:
        emit(FloidDroidMessageHasId(floidId: event.floidId, floidUuid: event.floidUuid));
      }

      // We have received a new floid droid message:
      if(event is NewFloidDroidMessageEvent) {
        print('NewFloidDroidMessageEvent(floidDroidMessage)');
        FloidDroidMessageState floidDroidMessageState = FloidDroidMessageLoaded(floidDroidMessage: event.floidDroidMessage, floidId: event.floidId, floidUuid: event.floidUuid);
        emit(floidDroidMessageState);
      }

    });
  }
}
