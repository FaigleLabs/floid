/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

abstract class FloidModelStatusEvent extends Equatable {
  FloidModelStatusEvent([List props = const []]) : super();
}

class NewFloidModelStatusEvent extends FloidModelStatusEvent {
  final int floidId;
  final String floidUuid;
  final  FloidModelStatus floidModelStatus;

  NewFloidModelStatusEvent({
    required this.floidModelStatus,
    required this.floidId,
    required this.floidUuid,
  }) : super([floidModelStatus, floidId, floidUuid]);

  @override
  List<Object> get props => [floidId, floidUuid, floidModelStatus];
}

/// Event for setting the Floid Id and Floid Uuid:
class UpdateFloidModelStatusFloidId extends FloidModelStatusEvent {
  final int floidId;
  final String floidUuid;

  UpdateFloidModelStatusFloidId({
    required this.floidId,
    required this.floidUuid,
  }) : super([floidId]);

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Generic State:
class FloidModelStatusState extends Equatable {
  FloidModelStatusState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidModelStatusEmpty extends FloidModelStatusState {

  FloidModelStatusEmpty() : super();

  @override
  List<Object> get props => [];
}

/// State loaded:
class FloidModelStatusHasId extends FloidModelStatusState {
  /// The Floid Id
  final int floidId;
  /// The Floid UUID
  final String floidUuid;

  FloidModelStatusHasId({required this.floidId, required this.floidUuid}) : super();

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Status Loaded:
class FloidModelStatusLoaded extends FloidModelStatusHasId {
  final FloidModelStatus floidModelStatus;

  FloidModelStatusLoaded({required this.floidModelStatus,
    required floidId, required floidUuid}) : super(floidId: floidId, floidUuid: floidUuid);

  @override
  List<Object> get props => [floidModelStatus, floidId, floidUuid];
}


class FloidModelStatusBloc extends Bloc<FloidModelStatusEvent, FloidModelStatusState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;
  /// The stomp client:
  Future<StompClient>? stompClient;

  /// Subscribe to the FloidModelStatus stomp client:
  Future<StompClient> subscribe(int floidId, String floidUuid) {
    return floidServerRepository.subscribeFloidModelStatus(floidId, floidUuid,
            (Map<String, String> headers, String message) {
          // Dispatch the new floid model status event:
          FloidModelStatus floidModelStatus = FloidModelStatus.fromJson(jsonDecode(message));
          add(new NewFloidModelStatusEvent(
              floidModelStatus: floidModelStatus,
              floidId: floidId, floidUuid: floidUuid));
        });
  }

  /// Get the last Floid Model Status:
  void getLastFloidModelStatus(int floidId, String floidUuid) async {
    try  {
    FloidModelStatus floidModelStatus = await floidServerRepository.fetchLastFloidModelStatus(floidId, floidUuid);
    add(new NewFloidModelStatusEvent(floidModelStatus: floidModelStatus, floidId: floidId, floidUuid: floidUuid));

    } catch(_e) {
      print('Could not get last model status for ' +  floidId.toString() + ' ' + floidUuid);
    }
  }

  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidModelStatus cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_MODEL_STATUS_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidModelStatus cancel: stompClient null');
    }
  }

  FloidModelStatusBloc({required this.floidServerRepository}): super(FloidModelStatusEmpty()) {
    on<FloidModelStatusEvent>((FloidModelStatusEvent event, Emitter<FloidModelStatusState> emit) async {
// We have a new floid id and floid uuid:
      if(event is UpdateFloidModelStatusFloidId) {
        // If we had id's before, then cancel:
        if(state is FloidModelStatusHasId) {
          try {
            print('Unsubscribing from Floid Model Status events');
            cancel();
          } catch(e) {
            print('Error cancelling the floid model status stomp' + e.toString());
          }
        }
        // Get the last values for this status if exists:
        print('Retrieving last Floid Model Status');
        getLastFloidModelStatus(event.floidId, event.floidUuid);
        // Subscribe to the new floid id and uuid:
        print('Subscribing to Floid Model Status events');
        stompClient = subscribe(event.floidId, event.floidUuid);
        // We have loaded the floid Id and Uuid:
        emit(FloidModelStatusHasId(floidId: event.floidId, floidUuid: event.floidUuid));
      }

      // We have received a new floid model status:
      if(event is NewFloidModelStatusEvent) {
        print('NewFloidModelStatusEvent(floidModelStatus)');
        FloidModelStatusState floidModelStatusState = FloidModelStatusLoaded(floidModelStatus: event.floidModelStatus, floidId: event.floidId, floidUuid: event.floidUuid);
        emit(floidModelStatusState);
      }
    });
  }
}
