/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

abstract class FloidDroidVideoFrameEvent extends Equatable {
  FloidDroidVideoFrameEvent([List props = const []]) : super();
}

class NewFloidDroidVideoFrameEvent extends FloidDroidVideoFrameEvent {
  final int floidId;
  final String floidUuid;
  final  FloidDroidVideoFrame floidDroidVideoFrame;

  NewFloidDroidVideoFrameEvent({
    required this.floidDroidVideoFrame,
    required this.floidId,
    required this.floidUuid,
  }) : super([floidDroidVideoFrame, floidId, floidUuid]);

  @override
  List<Object> get props => [floidId, floidUuid, floidDroidVideoFrame];
}

/// Event for setting the Floid Id and Floid Uuid:
class UpdateFloidDroidVideoFrameFloidId extends FloidDroidVideoFrameEvent {
  final int floidId;
  final String floidUuid;

  UpdateFloidDroidVideoFrameFloidId({
    required this.floidId,
    required this.floidUuid,
  }) : super([floidId]);

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Generic State:
class FloidDroidVideoFrameState extends Equatable {
  FloidDroidVideoFrameState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidDroidVideoFrameEmpty extends FloidDroidVideoFrameState {

  FloidDroidVideoFrameEmpty() : super();

  @override
  List<Object> get props => [];
}

/// State loaded:
class FloidDroidVideoFrameHasId extends FloidDroidVideoFrameState {
  /// The Floid Id
  final int floidId;
  /// The Floid UUID
  final String floidUuid;

  FloidDroidVideoFrameHasId({required this.floidId, required this.floidUuid}) : super();

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Floid Droid Video Frame Loaded:
class FloidDroidVideoFrameLoaded extends FloidDroidVideoFrameHasId {
  final FloidDroidVideoFrame floidDroidVideoFrame;

  FloidDroidVideoFrameLoaded({required this.floidDroidVideoFrame,
    required floidId, required floidUuid}) : super(floidId: floidId, floidUuid: floidUuid);

  @override
  List<Object> get props => [floidDroidVideoFrame, floidId, floidUuid];
}


class FloidDroidVideoFrameBloc extends Bloc<FloidDroidVideoFrameEvent, FloidDroidVideoFrameState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;
  /// The stomp client:
  Future<StompClient>? stompClient;

  /// Subscribe to the FloidDroidVideoFrame stomp client:
  Future<StompClient> subscribe(int floidId, String floidUuid) {
    return floidServerRepository.subscribeFloidDroidVideoFrame(floidId, floidUuid,
            (Map<String, String> headers, String message) {
          // Dispatch the new floid droid video frame event:
          FloidDroidVideoFrame floidDroidVideoFrame = FloidDroidVideoFrame.fromJson(jsonDecode(message));
          add(new NewFloidDroidVideoFrameEvent(
              floidDroidVideoFrame: floidDroidVideoFrame,
              floidId: floidId, floidUuid: floidUuid));
        });
  }

  /// Get the last Floid Droid Video Frame:
  void getLastFloidDroidVideoFrame(int floidId, String floidUuid) async {
    try {
    FloidDroidVideoFrame floidDroidVideoFrame= await floidServerRepository.fetchLastFloidDroidVideoFrame(floidId, floidUuid);
    add(new NewFloidDroidVideoFrameEvent(floidDroidVideoFrame: floidDroidVideoFrame, floidId: floidId, floidUuid: floidUuid));
    } catch(_e) {
      print('Could not get last droid video frame for ' +  floidId.toString() + ' ' + floidUuid);
    }
  }

  /// Cancel subscription
  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidDroidVideoFrame cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_DROID_VIDEO_FRAME_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidDroidVideoFrame cancel: stompClient null');
    }
  }

  FloidDroidVideoFrameBloc({required this.floidServerRepository}): super(FloidDroidVideoFrameEmpty()) {
    on<FloidDroidVideoFrameEvent>((FloidDroidVideoFrameEvent event, Emitter<FloidDroidVideoFrameState> emit) async {
// We have a new floid id and floid uuid:
      if(event is UpdateFloidDroidVideoFrameFloidId) {
        // If we had id's before, then cancel:
        if(state is FloidDroidVideoFrameHasId) {
          try {
            print('Unsubscribing from Floid Droid Video Frame events');
            cancel();
          } catch(e) {
            print('Error cancelling the floid droid video frame stomp' + e.toString());
          }
        }
        // Get the last video frame if exists:
        print('Retrieving last Floid Droid Video Frame');
        getLastFloidDroidVideoFrame(event.floidId, event.floidUuid);
        // Subscribe to the new floid id and uuid:
        print('Subscribing to Floid Droid Video Frame events');
        stompClient = subscribe(event.floidId, event.floidUuid);
        // We have loaded the floid Id and Uuid:
        emit(FloidDroidVideoFrameHasId(floidId: event.floidId, floidUuid: event.floidUuid));
      }

      // We have received a new floid droid video frame:
      if(event is NewFloidDroidVideoFrameEvent) {
        print('NewFloidDroidVideoFrameEvent(floidDroidVideoFrame)');
        try {
          if (event.floidDroidVideoFrame.imageBytes != null && event.floidDroidVideoFrame.imageBytes.length>0) {
            FloidDroidVideoFrameState floidDroidVideoFrameState = FloidDroidVideoFrameLoaded(
                floidDroidVideoFrame: event.floidDroidVideoFrame, floidId: event.floidId, floidUuid: event.floidUuid);
            emit(floidDroidVideoFrameState);
          } else {
            emit(FloidDroidVideoFrameEmpty());
          }
        } catch(_) {
          emit(FloidDroidVideoFrameEmpty());
        }
      }
    });
  }
}
