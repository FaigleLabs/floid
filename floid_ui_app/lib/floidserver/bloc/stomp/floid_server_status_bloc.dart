/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

/// Generic State:
class FloidServerStatusState extends Equatable {
  FloidServerStatusState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidServerStatusEmpty extends FloidServerStatusState {
  FloidServerStatusEmpty() : super();
  @override
  List<Object> get props => [];
}

/// State status loading:
class FloidServerStatusLoading extends FloidServerStatusState {
  FloidServerStatusLoading() : super();
  @override
  List<Object> get props => [];
}

/// State status loaded:
class FloidServerStatusLoaded extends FloidServerStatusState {
  final FloidServerStatus floidServerStatus;
  FloidServerStatusLoaded({required this.floidServerStatus,}) : super();
  @override
  List<Object> get props => [floidServerStatus];
}
/// State error:
class FloidServerStatusError extends FloidServerStatusState {
  FloidServerStatusError() : super();
  @override
  List<Object> get props => [];
}


abstract class FloidServerStatusEvent extends Equatable {
  FloidServerStatusEvent([List props = const []]) : super();
}


class NewFloidServerStatusEvent extends FloidServerStatusEvent {
  final  FloidServerStatus floidServerStatus;
  NewFloidServerStatusEvent({
    required this.floidServerStatus,
  }) : super([floidServerStatus]);

  @override
  String toString() => 'NewFloidServerStatus { floidServerStatus: $floidServerStatus}';
  @override
  List<Object> get props => [floidServerStatus];
}

class StartFloidServerStatusEvent extends FloidServerStatusEvent {
  StartFloidServerStatusEvent() : super();

  @override
  String toString() => 'StartFloidServerStatusEvent';
  @override
  List<Object> get props => [];
}



class FloidServerStatusBloc extends Bloc<FloidServerStatusEvent, FloidServerStatusState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;

  /// The stomp client:
  Future<StompClient>? stompClient;

  FloidServerStatusBloc({required this.floidServerRepository})
      : super(FloidServerStatusEmpty()) {
    on<FloidServerStatusEvent>((FloidServerStatusEvent event, Emitter<FloidServerStatusState> emit) async {
      if(event is StartFloidServerStatusEvent) {
        try {
          emit(FloidServerStatusLoading());
          if (stompClient != null) {
            cancel();
          }
          getServerStatus();
          stompClient = subscribe();
          emit(FloidServerStatusLoading());
        } catch(e) {
          emit(FloidServerStatusError());
        }
      }

      if (event is NewFloidServerStatusEvent) {
        try {
          emit(FloidServerStatusLoaded(floidServerStatus: event.floidServerStatus));
        } catch(e) {
          emit(FloidServerStatusError());
        }
      }
    });
  }

  /// Subscribe to the stomp client:
  Future<StompClient> subscribe() {
    return floidServerRepository.subscribeFloidServerStatus(
            (Map<String, String> headers, String message) {
          // Dispatch the new floid server status event:
          FloidServerStatus floidServerStatus = FloidServerStatus(message);
          add(new NewFloidServerStatusEvent(
              floidServerStatus: floidServerStatus));
        });
  }

  /// Cancel the connection to the stomp client:
  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidServerStatus cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_SERVER_STATUS_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidServerStatus cancel: stompClient null');
    }
  }

  /// Get the last Floid Droid Status:
  void getServerStatus() async {
    FloidServerStatus floidServerStatus = await floidServerRepository.fetchFloidServerStatus();
    add(new NewFloidServerStatusEvent(floidServerStatus: floidServerStatus));
  }
}
