/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

abstract class FloidDroidStatusEvent extends Equatable {
  FloidDroidStatusEvent([List props = const []]) : super();
}

class NewFloidDroidStatusEvent extends FloidDroidStatusEvent {
  final int floidId;
  final String floidUuid;
  final  FloidDroidStatus floidDroidStatus;

  NewFloidDroidStatusEvent({
    required this.floidDroidStatus,
    required this.floidId,
    required this.floidUuid,
  }) : super([floidDroidStatus, floidId, floidUuid]);

  @override
  List<Object> get props => [floidId, floidUuid, floidDroidStatus];
}

/// Event for setting the Floid Id and Floid Uuid:
class UpdateFloidDroidStatusFloidId extends FloidDroidStatusEvent {
  final int floidId;
  final String floidUuid;

  UpdateFloidDroidStatusFloidId({
    required this.floidId,
    required this.floidUuid,
  }) : super([floidId]);

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Generic State:
class FloidDroidStatusState extends Equatable {
  FloidDroidStatusState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidDroidStatusEmpty extends FloidDroidStatusState {

  FloidDroidStatusEmpty() : super();

  @override
  List<Object> get props => [];
}

/// State loaded:
class FloidDroidStatusHasId extends FloidDroidStatusState {
  /// The Floid Id
  final int floidId;
  /// The Floid UUID
  final String floidUuid;

  FloidDroidStatusHasId({required this.floidId, required this.floidUuid}) : super();

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Floid Droid Status Loaded:
class FloidDroidStatusLoaded extends FloidDroidStatusHasId {
  final FloidDroidStatus floidDroidStatus;

  FloidDroidStatusLoaded({required this.floidDroidStatus,
    required floidId, required floidUuid}) : super(floidId: floidId, floidUuid: floidUuid);

  @override
  List<Object> get props => [floidDroidStatus, floidId, floidUuid];
}


class FloidDroidStatusBloc extends Bloc<FloidDroidStatusEvent, FloidDroidStatusState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;
  /// The stomp client:
  Future<StompClient>? stompClient;

  /// Subscribe to the FloidDroidStatus stomp client:
  Future<StompClient> subscribe(int floidId, String floidUuid) {
    return floidServerRepository.subscribeFloidDroidStatus(floidId, floidUuid,
            (Map<String, String> headers, String message) {
          // Dispatch the new floid droid status event:
          FloidDroidStatus floidDroidStatus = FloidDroidStatus.fromJson(jsonDecode(message));
          add(new NewFloidDroidStatusEvent(
              floidDroidStatus: floidDroidStatus,
          floidId: floidId, floidUuid: floidUuid));
        });
  }

  /// Get the last Floid Droid Status:
  void getLastFloidDroidStatus(int floidId, String floidUuid) async {
    try {
      FloidDroidStatus floidDroidStatus = await floidServerRepository.fetchLastFloidDroidStatus(floidId, floidUuid);
      add(new NewFloidDroidStatusEvent(floidDroidStatus: floidDroidStatus, floidId: floidId, floidUuid: floidUuid));
    } catch (_e) {
      print('Could not get last floid droid status for ' + floidId.toString()  + ' ' + floidUuid);
    }
  }

  /// Cancel subscription
  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidDroidStatus cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_DROID_STATUS_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidDroidStatus cancel: stompClient null');
    }
  }

  FloidDroidStatusBloc({required this.floidServerRepository}): super(FloidDroidStatusEmpty()) {
    on<FloidDroidStatusEvent>((FloidDroidStatusEvent event, Emitter<FloidDroidStatusState> emit) async {
// We have a new floid id and floid uuid:
      if(event is UpdateFloidDroidStatusFloidId) {
        // If we had id's before, then cancel:
        if(state is FloidDroidStatusHasId || state is FloidDroidStatusLoaded) {
          try {
            print('Unsubscribing from Floid Droid Status events');
            cancel();
          } catch(e) {
            print('Error cancelling the floid droid status stomp' + e.toString());
          }
        }
        // Get the last values for this status if exists:
        print('Retrieving last Floid Droid Status');
        getLastFloidDroidStatus(event.floidId, event.floidUuid);
        // Subscribe to the new floid id and uuid:
        print('Subscribing to Floid Droid Status events');
        stompClient = subscribe(event.floidId, event.floidUuid);
        // We have loaded the floid Id and Uuid:
        emit(FloidDroidStatusHasId(floidId: event.floidId, floidUuid: event.floidUuid));
      }

      // We have received a new floid droid status:
      if(event is NewFloidDroidStatusEvent) {
        print('NewFloidDroidStatusEvent(floidDroidStatus)');
        FloidDroidStatusState floidDroidStatusState = FloidDroidStatusLoaded(floidDroidStatus: event.floidDroidStatus, floidId: event.floidId, floidUuid: event.floidUuid);
        emit(floidDroidStatusState);
      }
    });
  }
}
