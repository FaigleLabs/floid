/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';
import 'dart:convert';

/// Generic State:
class FloidNewFloidIdState extends Equatable {
  FloidNewFloidIdState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidNewFloidIdEmpty extends FloidNewFloidIdState {
  FloidNewFloidIdEmpty() : super();
  @override
  List<Object> get props => [];
}

/// State status loading:
class FloidNewFloidIdLoading extends FloidNewFloidIdState {
  FloidNewFloidIdLoading() : super();
  @override
  List<Object> get props => [];
}

/// State status loaded:
class FloidNewFloidIdLoaded extends FloidNewFloidIdState {
  final FloidIdAndState floidIdAndState;
  FloidNewFloidIdLoaded({required this.floidIdAndState,}) : super();
  @override
  List<Object> get props => [floidIdAndState];
}
/// State error:
class FloidNewFloidIdError extends FloidNewFloidIdState {
  FloidNewFloidIdError() : super();
  @override
  List<Object> get props => [];
}


abstract class FloidNewFloidIdEvent extends Equatable {
  FloidNewFloidIdEvent([List props = const []]) : super();
}


class NewFloidNewFloidIdEvent extends FloidNewFloidIdEvent {
  final  FloidIdAndState floidIdAndState;
  NewFloidNewFloidIdEvent({
    required this.floidIdAndState,
  }) : super([floidIdAndState]);

  @override
  String toString() => 'NewFloidIdAndState { floidIdAndState: $floidIdAndState}';
  @override
  List<Object> get props => [floidIdAndState];
}

class StartFloidNewFloidIdEvent extends FloidNewFloidIdEvent {
  StartFloidNewFloidIdEvent() :super();

  @override
  String toString() => 'StartFloidNewFloidIdEvent';
  @override
  List<Object> get props => [];
}



class FloidNewFloidIdBloc extends Bloc<FloidNewFloidIdEvent, FloidNewFloidIdState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;

  /// The stomp client:
  Future<StompClient>? stompClient;

  FloidNewFloidIdBloc({required this.floidServerRepository})
      : super(FloidNewFloidIdEmpty()) {
    on<FloidNewFloidIdEvent>((FloidNewFloidIdEvent event, Emitter<FloidNewFloidIdState> emit) async {
      if(event is StartFloidNewFloidIdEvent) {
        try {
          emit(FloidNewFloidIdLoading());
          if (stompClient != null) {
            cancel();
          }
          stompClient = subscribe();
          emit(FloidNewFloidIdLoading());
        } catch(e) {
          emit(FloidNewFloidIdError());
        }
      }

      if (event is NewFloidNewFloidIdEvent) {
        try {
          emit(FloidNewFloidIdLoaded(floidIdAndState: event.floidIdAndState));
        } catch(e) {
          emit(FloidNewFloidIdError());
        }
      }
    });
  }

  // Subscribe:
  Future<StompClient> subscribe() {
    return floidServerRepository.subscribeFloidNewFloidId(
            (Map<String, String> headers, String message) {
          // Dispatch the new floid id message event:
          FloidIdAndState floidIdAndState = FloidIdAndState.fromJson(jsonDecode(message));
          add(new NewFloidNewFloidIdEvent(floidIdAndState: floidIdAndState));
        });
  }

  /// Cancel the connection to the stomp client:
  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidNewFloidId cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_NEW_FLOID_ID_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidNewFloidId cancel: stompClient null');
    }
  }
}
