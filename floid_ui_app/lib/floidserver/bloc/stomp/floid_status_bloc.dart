/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

abstract class FloidStatusEvent extends Equatable {
  FloidStatusEvent([List props = const []]) : super();
}

class NewFloidStatusEvent extends FloidStatusEvent {
  final int floidId;
  final String floidUuid;
  final  FloidStatus floidStatus;

  NewFloidStatusEvent({
    required this.floidStatus,
    required this.floidId,
    required this.floidUuid,
  }) : super([floidStatus, floidId, floidUuid]);

  @override
  List<Object> get props => [floidId, floidUuid, floidStatus];
}

/// Event for setting the Floid Id and Floid Uuid:
class UpdateFloidStatusFloidId extends FloidStatusEvent {
  final int floidId;
  final String floidUuid;

  UpdateFloidStatusFloidId({
    required this.floidId,
    required this.floidUuid,
  }) : super([floidId]);

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Generic State:
class FloidStatusState extends Equatable {
  FloidStatusState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidStatusEmpty extends FloidStatusState {

  FloidStatusEmpty() : super();

  @override
  List<Object> get props => [];
}

/// State loaded:
class FloidStatusHasId extends FloidStatusState {
  /// The Floid Id
  final int floidId;
  /// The Floid UUID
  final String floidUuid;

  FloidStatusHasId({required this.floidId, required this.floidUuid}) : super();

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Floid Status Loaded:
class FloidStatusLoaded extends FloidStatusHasId {
  final FloidStatus floidStatus;

  FloidStatusLoaded({required this.floidStatus,
    required floidId, required floidUuid}) : super(floidId: floidId, floidUuid: floidUuid);

  @override
  List<Object> get props => [floidStatus, floidId, floidUuid];
}

class FloidStatusBloc extends Bloc<FloidStatusEvent, FloidStatusState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;

  /// The stomp client:
  Future<StompClient>? stompClient;

  /// Subscribe to the FloidStatus stomp client:
  Future<StompClient> subscribe(int floidId, String floidUuid) {
    return floidServerRepository.subscribeFloidStatus(floidId, floidUuid,
            (Map<String, String> headers, String message) {
          // Dispatch the new floid status event:
          FloidStatus floidStatus = FloidStatus.fromJson(jsonDecode(message));
          add(new NewFloidStatusEvent(
              floidStatus: floidStatus,
              floidId: floidId, floidUuid: floidUuid));
        });
  }

  /// Get the last Floid Status:
  void getLastFloidStatus(int floidId, String floidUuid) async {
    try {
      FloidStatus floidStatus = await floidServerRepository
          .fetchLastFloidStatus(floidId, floidUuid);
      add(new NewFloidStatusEvent(
          floidStatus: floidStatus, floidId: floidId, floidUuid: floidUuid));
    } catch (_e) {
      print('Could not get last floid status for ' + floidId.toString()  + ' ' + floidUuid);
    }
  }

  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidStatus cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_FLOID_STATUS_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidStatus cancel: stompClient null');
    }
  }

  FloidStatusBloc({required this.floidServerRepository})
      : super(FloidStatusEmpty()) {
    on<FloidStatusEvent>((FloidStatusEvent event,
        Emitter<FloidStatusState> emit) async {
      // We have a new floid id and floid uuid:
      if (event is UpdateFloidStatusFloidId) {
        // If we had id's before, then cancel:
        if (state is FloidStatusHasId) {
          try {
            print('Unsubscribing from Floid Status events');
            cancel();
          } catch (e) {
            print(
                'Error cancelling the Floid Droid status stomp' + e.toString());
          }
        }
        // Get the last values for this status if exists:
        print('Retrieving last Floid Status');
        getLastFloidStatus(event.floidId, event.floidUuid);
        // Subscribe to the new floid id and uuid:
        print('Subscribing to Floid Status events');
        stompClient = subscribe(event.floidId, event.floidUuid);
        // We have loaded the floid Id and Uuid:
        emit(FloidStatusHasId(
            floidId: event.floidId, floidUuid: event.floidUuid));
      }
      // We have received a new floid status:
      if (event is NewFloidStatusEvent) {
        print('NewFloidStatusEvent(floidStatus)');
        FloidStatusState floidStatusState = FloidStatusLoaded(
            floidStatus: event.floidStatus,
            floidId: event.floidId,
            floidUuid: event.floidUuid);
        emit(floidStatusState);
      }
    });
  }

}
