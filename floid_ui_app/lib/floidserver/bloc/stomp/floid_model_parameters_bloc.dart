/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

abstract class FloidModelParametersEvent extends Equatable {
  FloidModelParametersEvent([List props = const []]) : super();
}

class NewFloidModelParametersEvent extends FloidModelParametersEvent {
  final int floidId;
  final String floidUuid;
  final FloidModelParameters floidModelParameters;

  NewFloidModelParametersEvent({
    required this.floidModelParameters,
    required this.floidId,
    required this.floidUuid,
  }) : super([floidModelParameters, floidId, floidUuid]);

  @override
  List<Object> get props => [floidId, floidUuid, floidModelParameters];
}

/// Event for setting the Floid Id and Floid Uuid:
class UpdateFloidModelParametersFloidId extends FloidModelParametersEvent {
  final int floidId;
  final String floidUuid;

  UpdateFloidModelParametersFloidId({
    required this.floidId,
    required this.floidUuid,
  }) : super([floidId]);

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Generic State:
class FloidModelParametersState extends Equatable {
  FloidModelParametersState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidModelParametersEmpty extends FloidModelParametersState {

  FloidModelParametersEmpty() : super();

  @override
  List<Object> get props => [];
}

/// State loaded:
class FloidModelParametersHasId extends FloidModelParametersState {
  /// The Floid Id
  final int floidId;
  /// The Floid UUID
  final String floidUuid;

  FloidModelParametersHasId({required this.floidId, required this.floidUuid}) : super();

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Parameters Loaded:
class FloidModelParametersLoaded extends FloidModelParametersHasId {
  final FloidModelParameters floidModelParameters;

  FloidModelParametersLoaded({required this.floidModelParameters,
    required floidId, required floidUuid}) : super(floidId: floidId, floidUuid: floidUuid);

  @override
  List<Object> get props => [floidModelParameters, floidId, floidUuid];
}


class FloidModelParametersBloc extends Bloc<FloidModelParametersEvent, FloidModelParametersState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;

  /// The stomp client:
  Future<StompClient>? stompClient;

  /// Subscribe to the FloidModelParameters stomp client:
  Future<StompClient> subscribe(int floidId, String floidUuid) {
    return floidServerRepository.subscribeFloidModelParameters(
        floidId, floidUuid,
            (Map<String, String> headers, String message) {
          // Dispatch the new floid model parameters event:
          FloidModelParameters floidModelParameters = FloidModelParameters
              .fromJson(jsonDecode(message));
          add(new NewFloidModelParametersEvent(
              floidModelParameters: floidModelParameters,
              floidId: floidId, floidUuid: floidUuid));
        });
  }

  /// Get the last Floid Model Parameters:
  void getLastFloidModelParameters(int floidId, String floidUuid) async {
    try {
      FloidModelParameters floidModelParameters = await floidServerRepository
          .fetchLastFloidModelParameters(floidId, floidUuid);
      add(new NewFloidModelParametersEvent(
          floidModelParameters: floidModelParameters,
          floidId: floidId,
          floidUuid: floidUuid));
    } catch (_e) {
      print('Could not get last model parameters for ' + floidId.toString() + ' ' +
          floidUuid);
    }
  }

  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidModelParameters cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_MODEL_PARAMETERS_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidModelParameters cancel: stompClient null');
    }
  }

  FloidModelParametersBloc({required this.floidServerRepository})
      : super(FloidModelParametersEmpty()) {
    on<FloidModelParametersEvent>((FloidModelParametersEvent event,
        Emitter<FloidModelParametersState> emit) async {
// We have a new floid id and floid uuid:
      if (event is UpdateFloidModelParametersFloidId) {
        // If we had id's before, then cancel:
        if (state is FloidModelParametersHasId) {
          try {
            print('Unsubscribing from Floid Model Parameters events');
            cancel();
          } catch (e) {
            print('Error cancelling the floid model parameters stomp' +
                e.toString());
          }
        }
        // Get the last values for this parameters if exists:
        print('Retrieving last Floid Model Parameters');
        getLastFloidModelParameters(event.floidId, event.floidUuid);
        // Subscribe to the new floid id and uuid:
        print('Subscribing to Floid Model Parameters events');
        stompClient = subscribe(event.floidId, event.floidUuid);
        // We have loaded the floid Id and Uuid:
        emit(FloidModelParametersHasId(
            floidId: event.floidId, floidUuid: event.floidUuid));
      }

      // We have received a new floid model parameters:
      if (event is NewFloidModelParametersEvent) {
        print('NewFloidModelParametersEvent(floidModelParameters)');
        FloidModelParametersState floidModelParametersState = FloidModelParametersLoaded(
            floidModelParameters: event.floidModelParameters,
            floidId: event.floidId,
            floidUuid: event.floidUuid);
        emit(floidModelParametersState);
      }
    });
  }
}
