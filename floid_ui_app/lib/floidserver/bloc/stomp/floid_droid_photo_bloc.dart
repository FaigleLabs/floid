/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:equatable/equatable.dart';
import 'package:stomp/stomp.dart';

abstract class FloidDroidPhotoEvent extends Equatable {
  FloidDroidPhotoEvent([List props = const []]) : super();
}

class NewFloidDroidPhotoEvent extends FloidDroidPhotoEvent {
  final int floidId;
  final String floidUuid;
  final  FloidDroidPhoto floidDroidPhoto;

  NewFloidDroidPhotoEvent({
    required this.floidDroidPhoto,
    required this.floidId,
    required this.floidUuid,
  }) : super([floidDroidPhoto, floidId, floidUuid]);

  @override
  List<Object> get props => [floidId, floidUuid, floidDroidPhoto];
}

/// Event for setting the Floid Id and Floid Uuid:
class UpdateFloidDroidPhotoFloidId extends FloidDroidPhotoEvent {
  final int floidId;
  final String floidUuid;

  UpdateFloidDroidPhotoFloidId({
    required this.floidId,
    required this.floidUuid,
  }) : super([floidId]);

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Generic State:
class FloidDroidPhotoState extends Equatable {
  FloidDroidPhotoState() : super();
  @override
  List<Object> get props => [];
}

/// State empty:
class FloidDroidPhotoEmpty extends FloidDroidPhotoState {

  FloidDroidPhotoEmpty() : super();

  @override
  List<Object> get props => [];
}

/// State loaded:
class FloidDroidPhotoHasId extends FloidDroidPhotoState {
  /// The Floid Id
  final int floidId;
  /// The Floid UUID
  final String floidUuid;

  FloidDroidPhotoHasId({required this.floidId, required this.floidUuid}) : super();

  @override
  List<Object> get props => [floidId, floidUuid];
}

/// Floid Droid Photo Loaded:
class FloidDroidPhotoLoaded extends FloidDroidPhotoHasId {
  final FloidDroidPhoto floidDroidPhoto;

  FloidDroidPhotoLoaded({required this.floidDroidPhoto,
    required floidId, required floidUuid}) : super(floidId: floidId, floidUuid: floidUuid);

  @override
  List<Object> get props => [floidDroidPhoto, floidId, floidUuid];
}


class FloidDroidPhotoBloc extends Bloc<FloidDroidPhotoEvent, FloidDroidPhotoState> {
  /// The Floid Server Repository
  final FloidServerRepository floidServerRepository;
  /// The stomp client:
  Future<StompClient>? stompClient;

  /// Subscribe to the FloidDroidPhoto stomp client:
  Future<StompClient> subscribe(int floidId, String floidUuid) {
    return floidServerRepository.subscribeFloidDroidPhoto(floidId, floidUuid,
            (Map<String, String> headers, String message) {
          // Dispatch the new floid droid photo event:
          FloidDroidPhoto floidDroidPhoto = FloidDroidPhoto.fromJson(jsonDecode(message));
          add(new NewFloidDroidPhotoEvent(
              floidDroidPhoto: floidDroidPhoto,
              floidId: floidId, floidUuid: floidUuid));
        });
  }

  /// Get the last Floid Droid Photo:
  void getLastFloidDroidPhoto(int floidId, String floidUuid) async {
    try {
      FloidDroidPhoto floidDroidPhoto = await floidServerRepository
          .fetchLastFloidDroidPhoto(floidId, floidUuid);
      add(new NewFloidDroidPhotoEvent(floidDroidPhoto: floidDroidPhoto,
          floidId: floidId,
          floidUuid: floidUuid));
    } catch(_e) {
      print('Could not get last photo for ' +  floidId.toString() + ' ' + floidUuid);
    }
  }

  /// Cancel subscription
  void cancel() async {
    if (stompClient != null) {
      StompClient? stompClientReal = stompClient == null
          ? null
          : await stompClient;
      if(stompClientReal==null) {
        print('FloidDroidPhoto cancel: stompClientReal null');
      }
      stompClientReal?.unsubscribe(FloidServerApiClient.FLOID_DROID_PHOTO_STOMP_ID);
      stompClientReal?.disconnect();
    } else {
      print('FloidDroidPhoto cancel: stompClient null');
    }
  }

  FloidDroidPhotoBloc({required this.floidServerRepository}): super(FloidDroidPhotoEmpty()) {
    on<FloidDroidPhotoEvent>((FloidDroidPhotoEvent event, Emitter<FloidDroidPhotoState> emit) async {
      // We have a new floid id and floid uuid:
      if(event is UpdateFloidDroidPhotoFloidId) {
        // If we had id's before, then cancel:
        if(state is FloidDroidPhotoHasId) {
          try {
            print('Unsubscribing from Floid Droid Photo events');
            cancel();
          } catch(e) {
            print('Error cancelling the floid droid photo stomp' + e.toString());
          }
        }
        // Get the last photo if exists:
        print('Retrieving last Floid Droid Photo');
        getLastFloidDroidPhoto(event.floidId, event.floidUuid);
        // Subscribe to the new floid id and uuid:
        print('Subscribing to Floid Droid Photo events');
        stompClient = subscribe(event.floidId, event.floidUuid);
        // We have loaded the floid Id and Uuid:
        emit(FloidDroidPhotoHasId(floidId: event.floidId, floidUuid: event.floidUuid));
      }

      // We have received a new floid droid photo:
      if(event is NewFloidDroidPhotoEvent) {
        try {
          if (event.floidDroidPhoto.imageBytes.isNotEmpty) {
            FloidDroidPhotoState floidDroidPhotoState = FloidDroidPhotoLoaded(
                floidDroidPhoto: event.floidDroidPhoto, floidId: event.floidId, floidUuid: event.floidUuid);
            emit(floidDroidPhotoState);
          } else {
            emit(FloidDroidPhotoEmpty());
          }
        } catch(_) {
          emit(FloidDroidPhotoEmpty());
        }
      }
    });
  }
}
