/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class DroidTrackGpxFileEvent extends Equatable {
  DroidTrackGpxFileEvent([List props = const []]) : super();
}

class SetDroidTrackGpxFile extends DroidTrackGpxFileEvent {
  final String droidTrackGpxFile;
  final String fileName;
  SetDroidTrackGpxFile({required this.droidTrackGpxFile, required this.fileName})
      : super([droidTrackGpxFile, fileName]);
  @override
  List<Object> get props => [droidTrackGpxFile, fileName];
}

class ClearDroidTrackGpxFile extends DroidTrackGpxFileEvent {
  @override
  List<Object> get props => const [];
}

abstract class DroidTrackGpxFileState extends Equatable {
  DroidTrackGpxFileState([List props = const []]) : super();
}

class DroidTrackGpxFileEmpty extends DroidTrackGpxFileState {
  @override
  List<Object> get props => [];
}

class DroidTrackGpxFileLoaded extends DroidTrackGpxFileState {
  final String droidTrackGpxFile;
  final String fileName;

  DroidTrackGpxFileLoaded({required this.droidTrackGpxFile, required this.fileName})
      : super([droidTrackGpxFile, fileName]);
  @override
  List<Object> get props => [droidTrackGpxFile, fileName];
}

class DroidTrackGpxFileError extends DroidTrackGpxFileState {
  @override
  List<Object> get props => [];
}

class DroidTrackGpxFileBloc extends Bloc<DroidTrackGpxFileEvent, DroidTrackGpxFileState> {
  DroidTrackGpxFileBloc(): super(DroidTrackGpxFileEmpty()) {
    on<DroidTrackGpxFileEvent>((DroidTrackGpxFileEvent event, Emitter<DroidTrackGpxFileState> emit) async {
      // Set map gpx file:
      if (event is SetDroidTrackGpxFile) {
        print(event.droidTrackGpxFile.length.toString() + ' bytes GPX file (' + event.fileName + ')');
        emit(DroidTrackGpxFileLoaded(droidTrackGpxFile: event.droidTrackGpxFile, fileName: event.fileName));
      }
      // Clear map gpx file:
      if(event is ClearDroidTrackGpxFile) {
        emit(DroidTrackGpxFileEmpty());
      }
    });
  }
}
