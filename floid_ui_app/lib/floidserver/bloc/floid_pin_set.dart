/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidPinSetEvent extends Equatable {
  FloidPinSetEvent([List props = const []]) : super();
}

class FetchFloidPinSet extends FloidPinSetEvent {
  final DroidPinSetList droidPinSetList;

  FetchFloidPinSet({required this.droidPinSetList})
      : super([droidPinSetList]);
  @override
  List<Object> get props => [droidPinSetList];
}

class UnloadFloidPinSetEvent extends FloidPinSetEvent {
  UnloadFloidPinSetEvent()
      : super([]);
  @override
  List<Object> get props => [];
}
class AdjustFloidPinSet extends FloidPinSetEvent {
  final bool up;
  final int index;

  AdjustFloidPinSet({required this.up, required this.index})
      : super([up, index]);
  @override
  List<Object> get props => [up, index];
}

class RemoveFloidPin extends FloidPinSetEvent {
  final int index;
  RemoveFloidPin({required this.index})
      : super([index]);
  @override
  List<Object> get props => [index];
}

class ReplaceFloidPin extends FloidPinSetEvent {
  final FloidPinAlt floidPinAlt;
  final int index;

  ReplaceFloidPin({required this.floidPinAlt, required this.index})
      : super([floidPinAlt, index]);
  @override
  List<Object> get props => [floidPinAlt, index];
}

class InsertFloidPin extends FloidPinSetEvent {
  final FloidPinAlt floidPinAlt;
  final int index;

  InsertFloidPin({required this.floidPinAlt, required this.index})
      : super([floidPinAlt, index]);
  @override
  List<Object> get props => [floidPinAlt, index];
}

class RefreshFloidPinSet extends FloidPinSetEvent {
  final DroidPinSetList droidPinSetList;

  RefreshFloidPinSet({required this.droidPinSetList})
      : super([droidPinSetList]);
  @override
  List<Object> get props => [droidPinSetList];
}

class CreateFloidPinSetEvent extends FloidPinSetEvent {
  final String newPinSetName;

  CreateFloidPinSetEvent({required this.newPinSetName})
      : super([newPinSetName]);
  @override
  List<Object> get props => [newPinSetName];
}

class DuplicateFloidPinSetEvent extends FloidPinSetEvent {
  final int pinSetId;
  final String newPinSetName;

  DuplicateFloidPinSetEvent({required this.newPinSetName, required this.pinSetId})
      : super([newPinSetName, pinSetId]);
  @override
  List<Object> get props => [newPinSetName, pinSetId];
}

class DeleteFloidPinSetEvent extends FloidPinSetEvent {
  final int pinSetId;

  DeleteFloidPinSetEvent({required this.pinSetId})
      : super([pinSetId]);
  @override
  List<Object> get props => [pinSetId];
}


class ModifyFloidPinSetNameEvent extends FloidPinSetEvent {
  final String newPinSetName;

  ModifyFloidPinSetNameEvent({required this.newPinSetName})
      : super([newPinSetName]);
  @override
  List<Object> get props => [newPinSetName];
}

class SetFloidPinSetClean extends FloidPinSetEvent {
  SetFloidPinSetClean()
      : super([]);
  @override
  List<Object> get props => [];
}

abstract class FloidPinSetState {
  FloidPinSetState([List props = const []]) : super();
}

class FloidPinSetEmpty extends FloidPinSetState {
}

class FloidPinSetLoading extends FloidPinSetState {
}

class FloidPinSetUnloaded extends FloidPinSetState {
}

class FloidPinSetLoaded extends FloidPinSetState {
   final FloidPinSetAlt floidPinSetAlt;
   final bool dirty;
   final bool zoomToMarkers;

  FloidPinSetLoaded({required this.floidPinSetAlt, required this.dirty, required this.zoomToMarkers})
      : super([floidPinSetAlt, dirty, zoomToMarkers]);
}

class FloidPinSetError extends FloidPinSetState {
}

class FloidPinSetBloc extends Bloc<FloidPinSetEvent, FloidPinSetState> {
  final FloidServerRepository floidServerRepository;

  FloidPinSetBloc({required this.floidServerRepository})
      : super(FloidPinSetEmpty()) {
    on<FloidPinSetEvent>((FloidPinSetEvent event, Emitter<FloidPinSetState> emit) async {
      if (event is FetchFloidPinSet) {
        emit(FloidPinSetLoading());
        try {
          final FloidPinSetAlt floidPinSetAlt = await floidServerRepository.fetchPinSet(event.droidPinSetList.pinSetId);
          emit(FloidPinSetLoaded(floidPinSetAlt: floidPinSetAlt, dirty: false, zoomToMarkers: true));
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      // Unload the pin set:
      if (event is UnloadFloidPinSetEvent) {
        emit(FloidPinSetUnloaded());
      }
      // Refresh the pin set:
      if (event is RefreshFloidPinSet) {
        try {
          final FloidPinSetAlt floidPinSetAlt = await floidServerRepository.fetchPinSet(event.droidPinSetList.pinSetId);
          emit(FloidPinSetLoaded(floidPinSetAlt: floidPinSetAlt, dirty: false, zoomToMarkers: true));
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      // Set the pin set to clean:
      if(event is SetFloidPinSetClean) {
        try {
          if(state is FloidPinSetLoaded) {
            FloidPinSetLoaded floidPinSetLoaded = state as FloidPinSetLoaded;
            FloidPinSetAlt floidPinSetAlt = floidPinSetLoaded.floidPinSetAlt;
            emit(FloidPinSetLoaded(floidPinSetAlt: FloidPinSetAlt(id: floidPinSetAlt.id, pinSetName: floidPinSetAlt.pinSetName, pinAltList: floidPinSetAlt.pinAltList), dirty: false, zoomToMarkers: false));
          }
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      if(event is AdjustFloidPinSet) {
        try {
          if(state is FloidPinSetLoaded) {
            FloidPinSetLoaded floidPinSetLoaded = state as FloidPinSetLoaded;
            FloidPinSetAlt floidPinSetAlt = floidPinSetLoaded.floidPinSetAlt;
            if(event.up) {
              FloidPinAlt floidPinAlt = floidPinSetAlt.pinAltList.removeAt(event.index);
              floidPinSetAlt.pinAltList.insert(event.index-1, floidPinAlt );
            } else {
              FloidPinAlt floidPinAlt = floidPinSetAlt.pinAltList.removeAt(event.index);
              floidPinSetAlt.pinAltList.insert(event.index+1, floidPinAlt );
            }
            emit(FloidPinSetLoaded(floidPinSetAlt: FloidPinSetAlt(id: floidPinSetAlt.id, pinSetName: floidPinSetAlt.pinSetName, pinAltList: floidPinSetAlt.pinAltList), dirty: true, zoomToMarkers: false));
          }
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      if(event is RemoveFloidPin) {
        try {
          if (state is FloidPinSetLoaded) {
            FloidPinSetLoaded floidPinSetLoaded = state as FloidPinSetLoaded;
            FloidPinSetAlt floidPinSetAlt = floidPinSetLoaded.floidPinSetAlt;
            floidPinSetAlt.pinAltList.removeAt(event.index);
            emit(FloidPinSetLoaded(floidPinSetAlt: FloidPinSetAlt(id: floidPinSetAlt.id, pinSetName: floidPinSetAlt.pinSetName, pinAltList: floidPinSetAlt.pinAltList), dirty: true, zoomToMarkers: false));
          }
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      if(event is InsertFloidPin) {
        try {
          if (state is FloidPinSetLoaded) {
            FloidPinSetLoaded floidPinSetLoaded = state as FloidPinSetLoaded;
            FloidPinSetAlt floidPinSetAlt = floidPinSetLoaded.floidPinSetAlt;
            if(event.index == -1) {
              floidPinSetAlt.pinAltList.add(event.floidPinAlt);
            } else {
              floidPinSetAlt.pinAltList.insert(event.index, event.floidPinAlt);
            }
            emit(FloidPinSetLoaded(floidPinSetAlt: FloidPinSetAlt(id: floidPinSetAlt.id, pinSetName: floidPinSetAlt.pinSetName, pinAltList: floidPinSetAlt.pinAltList), dirty: true, zoomToMarkers: false));
          }
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      if(event is ReplaceFloidPin) {
        try {
          if (state is FloidPinSetLoaded) {
            FloidPinSetLoaded floidPinSetLoaded = state as FloidPinSetLoaded;
            FloidPinSetAlt floidPinSetAlt = floidPinSetLoaded.floidPinSetAlt;
            floidPinSetAlt.pinAltList.removeAt(event.index);
            floidPinSetAlt.pinAltList.insert(event.index, event.floidPinAlt);
            emit(FloidPinSetLoaded(floidPinSetAlt: FloidPinSetAlt(id: floidPinSetAlt.id, pinSetName: floidPinSetAlt.pinSetName, pinAltList: floidPinSetAlt.pinAltList), dirty: true, zoomToMarkers: false));
          }
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      // Create a new Pin Set:
      if (event is CreateFloidPinSetEvent) {
        emit(FloidPinSetLoading());
        try {
          final int newPinSetId = await floidServerRepository.createPinSet(event.newPinSetName);
          emit(FloidPinSetUnloaded()); // Tell the pin set list to load the new list but not load a new pin set as we are going to load it
          final FloidPinSetAlt floidPinSetAlt = await floidServerRepository.fetchPinSet(newPinSetId);
          emit(FloidPinSetLoaded(floidPinSetAlt: floidPinSetAlt, dirty: false, zoomToMarkers: false));
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      // Duplicate a Pin Set:
      if (event is DuplicateFloidPinSetEvent) {
        emit(FloidPinSetLoading());
        try {
          final int newPinSetId = await floidServerRepository.duplicatePinSet(event.pinSetId, event.newPinSetName);
          emit(FloidPinSetUnloaded()); // Tell the pin set list to load the new list but not load a new pin set as we are going to load it
          final FloidPinSetAlt floidPinSetAlt = await floidServerRepository.fetchPinSet(newPinSetId);
          emit(FloidPinSetLoaded(floidPinSetAlt: floidPinSetAlt, dirty: false, zoomToMarkers: false));
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      // Delete a Pin Set:
      if (event is DeleteFloidPinSetEvent) {
        emit(FloidPinSetLoading());
        try {
          final bool pinSetDeleted = await floidServerRepository.deletePinSet(event.pinSetId);
          if(pinSetDeleted) {
            // Tell the pin set list to load the new list and load the first pin set:
            emit(FloidPinSetEmpty());
          } else {
            emit(FloidPinSetError());
          }
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
      // Modify Pin Set Name;
      if(event is ModifyFloidPinSetNameEvent) {
        try {
          if(state is FloidPinSetLoaded) {
            FloidPinSetLoaded floidPinSetLoaded = state as FloidPinSetLoaded;
            FloidPinSetAlt floidPinSetAlt = floidPinSetLoaded.floidPinSetAlt;
            floidPinSetAlt.pinSetName = event.newPinSetName;
            emit(FloidPinSetLoaded(floidPinSetAlt: FloidPinSetAlt(id: floidPinSetAlt.id, pinSetName: floidPinSetAlt.pinSetName, pinAltList: floidPinSetAlt.pinAltList), dirty: true, zoomToMarkers: false));
          }
        } catch (_) {
          emit(FloidPinSetError());
        }
      }
    });
  }
}
