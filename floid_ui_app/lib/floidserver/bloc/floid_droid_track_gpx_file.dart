/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidDroidTrackGpxFileEvent extends Equatable {
  FloidDroidTrackGpxFileEvent([List props = const []]) : super();
}

class SetFloidDroidTrackGpxFile extends FloidDroidTrackGpxFileEvent {
  final String floidDroidTrackGpxFile;
  final String fileName;
  SetFloidDroidTrackGpxFile({required this.floidDroidTrackGpxFile, required this.fileName})
      : super([floidDroidTrackGpxFile, fileName]);
  @override
  List<Object> get props => [floidDroidTrackGpxFile, fileName];
}

class ClearFloidDroidTrackGpxFile extends FloidDroidTrackGpxFileEvent {
  @override
  List<Object> get props => const [];
}

abstract class FloidDroidTrackGpxFileState extends Equatable {
  FloidDroidTrackGpxFileState([List props = const []]) : super();
}

class FloidDroidTrackGpxFileEmpty extends FloidDroidTrackGpxFileState {
  @override
  List<Object> get props => [];
}

class FloidDroidTrackGpxFileLoaded extends FloidDroidTrackGpxFileState {
  final String floidDroidTrackGpxFile;
  final String fileName;

  FloidDroidTrackGpxFileLoaded({required this.floidDroidTrackGpxFile, required this.fileName})
      : super([floidDroidTrackGpxFile, fileName]);
  @override
  List<Object> get props => [floidDroidTrackGpxFile, fileName];
}

class FloidDroidTrackGpxFileError extends FloidDroidTrackGpxFileState {
  @override
  List<Object> get props => [];
}

class FloidDroidTrackGpxFileBloc extends Bloc<FloidDroidTrackGpxFileEvent, FloidDroidTrackGpxFileState> {

  FloidDroidTrackGpxFileBloc(): super(FloidDroidTrackGpxFileEmpty()) {
    on<FloidDroidTrackGpxFileEvent>((FloidDroidTrackGpxFileEvent event, Emitter<FloidDroidTrackGpxFileState> emit) async {
      // Set map gpx file:
      if (event is SetFloidDroidTrackGpxFile) {
        print(event.floidDroidTrackGpxFile.length.toString() + ' bytes GPX file (' + event.fileName + ')');
        emit(FloidDroidTrackGpxFileLoaded(floidDroidTrackGpxFile: event.floidDroidTrackGpxFile, fileName: event.fileName));
      }
      // Clear map gpx file:
      if(event is ClearFloidDroidTrackGpxFile) {
        emit(FloidDroidTrackGpxFileEmpty());
      }
    });
  }
}
