/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/ui/map/line/floid_map_line.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidMapLinesEvent extends Equatable {
  FloidMapLinesEvent([List props = const []]) : super();
}

class SetFloidMapLines extends FloidMapLinesEvent {
  final List<FloidMapLine> floidMapLines;
  SetFloidMapLines({required this.floidMapLines})
      : super([floidMapLines]);
  @override
  List<Object> get props => [floidMapLines];
}
class ClearFloidMapLines extends FloidMapLinesEvent {
  @override
  List<Object> get props => const [];
}

abstract class FloidMapLinesState extends Equatable {
  FloidMapLinesState([List props = const []]) : super();
}

class FloidMapLinesEmpty extends FloidMapLinesState {
  @override
  List<Object> get props => [];
}

class FloidMapLinesLoaded extends FloidMapLinesState {
  final List<FloidMapLine> floidMapLines;

  FloidMapLinesLoaded({required this.floidMapLines})
      : super([floidMapLines]);
  @override
  List<Object> get props => [floidMapLines];
}

class FloidMapLinesError extends FloidMapLinesState {
  @override
  List<Object> get props => [];
}

class FloidMapLinesBloc<T extends FloidMapLine> extends Bloc<FloidMapLinesEvent, FloidMapLinesState> {

  FloidMapLinesBloc(): super(FloidMapLinesEmpty()) {
    on<FloidMapLinesEvent>((FloidMapLinesEvent event, Emitter<FloidMapLinesState> emit) async {
      // Set map lines:
      if (event is SetFloidMapLines) {
        if(event.floidMapLines is List<T>) {
          if(event.floidMapLines!=null) {
            print(event.floidMapLines.length.toString() + ' map lines loaded');
          } else {
            print('Null map lines loaded');
          }
          emit(FloidMapLinesLoaded(floidMapLines: event.floidMapLines));
        } else {
          emit(FloidMapLinesError());
        }
      }
      // Clear map lines:
      if(event is ClearFloidMapLines) {
            emit(FloidMapLinesEmpty());
      }
    });
  }
}
