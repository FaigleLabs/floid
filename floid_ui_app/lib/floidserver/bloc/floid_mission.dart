/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidMissionEvent extends Equatable {
  FloidMissionEvent([List props = const []]) : super();
}

class FetchFloidMission extends FloidMissionEvent {
  final DroidMissionList droidMissionList;

  FetchFloidMission({required this.droidMissionList})
      : super([droidMissionList]);
  @override
  List<Object> get props => [droidMissionList];
}

class UnloadFloidMissionEvent extends FloidMissionEvent {
  UnloadFloidMissionEvent()
      : super([]);
  @override
  List<Object> get props => [];
}

class AdjustFloidMission extends FloidMissionEvent {
  final bool up;
  final int index;

  AdjustFloidMission({required this.up, required this.index})
      : super([up, index]);
  @override
  List<Object> get props => [up, index];
}

class ReplaceFloidMissionInstruction extends FloidMissionEvent {
  final DroidInstruction droidInstruction;
  final int index;

  ReplaceFloidMissionInstruction({required this.droidInstruction, required this.index})
      : super([droidInstruction, index]);
  @override
  List<Object> get props => [droidInstruction, index];
}

class InsertFloidMissionInstruction extends FloidMissionEvent {
  final DroidInstruction droidInstruction;
  final int index;

  InsertFloidMissionInstruction({required this.droidInstruction, required this.index})
      : super([droidInstruction, index]);
  @override
  List<Object> get props => [droidInstruction, index];
}

class RemoveFloidMissionInstruction extends FloidMissionEvent {
  final int index;

  RemoveFloidMissionInstruction({required this.index})
      : super([index]);
  @override
  List<Object> get props => [index];
}

class SetFloidMissionClean extends FloidMissionEvent {
  SetFloidMissionClean()
      :super([]);
  @override
  List<Object> get props => [];
}

class RefreshFloidMission extends FloidMissionEvent {
  final DroidMissionList droidMissionList;

  RefreshFloidMission({required this.droidMissionList})
      : super([droidMissionList]);
  @override
  List<Object> get props => [droidMissionList];
}

class CreateFloidMissionEvent extends FloidMissionEvent {
  final String newMissionName;

  CreateFloidMissionEvent({required this.newMissionName})
      : super([newMissionName]);
  @override
  List<Object> get props => [newMissionName];
}

class DuplicateFloidMissionEvent extends FloidMissionEvent {
  final int missionId;
  final String newMissionName;

  DuplicateFloidMissionEvent({required this.newMissionName, required this.missionId})
      : super([newMissionName, missionId]);
  @override
  List<Object> get props => [newMissionName, missionId];
}

class DeleteFloidMissionEvent extends FloidMissionEvent {
  final int missionId;

  DeleteFloidMissionEvent({required this.missionId})
      : super([missionId]);
  @override
  List<Object> get props => [missionId];
}

class ModifyFloidMissionNameEvent extends FloidMissionEvent {
  final String newMissionName;

  ModifyFloidMissionNameEvent({required this.newMissionName})
      : super([newMissionName]);
  @override
  List<Object> get props => [newMissionName];
}

abstract class FloidMissionState extends Equatable {
  FloidMissionState([List props = const []]) : super();
}

class FloidMissionEmpty extends FloidMissionState {
  @override
  List<Object> get props => [];
}

class FloidMissionLoading extends FloidMissionState {
  @override
  List<Object> get props => [];
}

// We are unloaded but about to load a new mission - e.g. one just created
class FloidMissionUnloaded extends FloidMissionState {
  @override
  List<Object> get props => [];
}

class FloidMissionLoaded extends FloidMissionState {
  final DroidMission droidMission;
  final bool dirty;

  FloidMissionLoaded({required this.droidMission, required this.dirty})
      : super([droidMission, dirty]);
  @override
  List<Object> get props => [droidMission];
}

class FloidMissionError extends FloidMissionState {
  @override
  List<Object> get props => [];
}

class FloidMissionBloc extends Bloc<FloidMissionEvent, FloidMissionState> {
  final FloidServerRepository floidServerRepository;

  FloidMissionBloc({required this.floidServerRepository})
      : super(FloidMissionEmpty()) {
    on<FloidMissionEvent>((FloidMissionEvent event, Emitter<FloidMissionState> emit) async {
      if (event is FetchFloidMission) {
        emit(FloidMissionLoading());
        try {
          final DroidMission droidMission = await floidServerRepository.fetchMission(event.droidMissionList.missionId);
          emit(FloidMissionLoaded(droidMission: droidMission, dirty: false));
        } catch (_) {
          emit(FloidMissionError());
        }
      }

      if(event is SetFloidMissionClean) {
        try {
          if (state is FloidMissionLoaded) {
            FloidMissionLoaded floidMissionLoaded = state as FloidMissionLoaded;
            DroidMission droidMission = floidMissionLoaded.droidMission;
            emit(FloidMissionLoaded(droidMission: DroidMission(droidInstructions: droidMission.droidInstructions,
                missionName: droidMission.missionName,
                nextInstructionNumber: droidMission.nextInstructionNumber,
                topLevelMission: droidMission.topLevelMission,
                id: droidMission.id,
                type: droidMission.type,
                missionInstructionNumber: droidMission.missionInstructionNumber),
              dirty: false,
            ));
          }
        } catch (_) {
          emit(FloidMissionError());
        }
      }

      if (event is AdjustFloidMission) {
        try {
          if (state is FloidMissionLoaded) {
            FloidMissionLoaded floidMissionLoaded = state as FloidMissionLoaded;
            DroidMission droidMission = floidMissionLoaded.droidMission;
            if (event.up) {
              DroidInstruction droidInstruction = droidMission.droidInstructions.removeAt(event.index);
              droidMission.droidInstructions.insert(event.index - 1, droidInstruction);
            } else {
              DroidInstruction droidInstruction = droidMission.droidInstructions.removeAt(event.index);
              droidMission.droidInstructions.insert(event.index + 1, droidInstruction);
            }
            droidMission.renumber();
            emit(FloidMissionLoaded(droidMission: DroidMission(droidInstructions: droidMission.droidInstructions,
                missionName: droidMission.missionName,
                nextInstructionNumber: droidMission.nextInstructionNumber,
                topLevelMission: droidMission.topLevelMission,
                id: droidMission.id,
                type: droidMission.type,
                missionInstructionNumber: droidMission.missionInstructionNumber),
              dirty: true,
            ));
          }
        } catch (_) {
          emit(FloidMissionError());
        }
      }

      if (event is InsertFloidMissionInstruction) {
        try {
          if (state is FloidMissionLoaded) {
            FloidMissionLoaded floidMissionLoaded = state as FloidMissionLoaded;
            DroidMission droidMission = floidMissionLoaded.droidMission;
            if (event.index == -1) {
              droidMission.droidInstructions.add(event.droidInstruction);
            } else {
              droidMission.droidInstructions.insert(event.index, event.droidInstruction);
            }
            droidMission.renumber();

            emit(FloidMissionLoaded(droidMission: DroidMission(droidInstructions: droidMission.droidInstructions,
                missionName: droidMission.missionName,
                nextInstructionNumber: droidMission.nextInstructionNumber,
                topLevelMission: droidMission.topLevelMission,
                id: droidMission.id,
                type: droidMission.type,
                missionInstructionNumber: droidMission.missionInstructionNumber),
              dirty: true,
            ));
          }
        } catch (_) {
          emit(FloidMissionError());
        }
      }

      if (event is RemoveFloidMissionInstruction) {
        try {
          if (state is FloidMissionLoaded) {
            FloidMissionLoaded floidMissionLoaded = state as FloidMissionLoaded;
            DroidMission droidMission = floidMissionLoaded.droidMission;
            droidMission.droidInstructions.removeAt(event.index);
            droidMission.renumber();
            emit(FloidMissionLoaded(droidMission: DroidMission(droidInstructions: droidMission.droidInstructions,
                missionName: droidMission.missionName,
                nextInstructionNumber: droidMission.nextInstructionNumber,
                topLevelMission: droidMission.topLevelMission,
                id: droidMission.id,
                type: droidMission.type,
                missionInstructionNumber: droidMission.missionInstructionNumber),
              dirty: true,
            ));
          }
        } catch (_) {
          emit(FloidMissionError());
        }
      }

      if (event is ReplaceFloidMissionInstruction) {
        try {
          if (state is FloidMissionLoaded) {
            FloidMissionLoaded floidMissionLoaded = state as FloidMissionLoaded;
            DroidMission droidMission = floidMissionLoaded.droidMission;
            droidMission.droidInstructions.removeAt(event.index);
            droidMission.droidInstructions.insert(event.index, event.droidInstruction);
            droidMission.renumber();
            emit(FloidMissionLoaded(droidMission: DroidMission(droidInstructions: droidMission.droidInstructions,
                missionName: droidMission.missionName,
                nextInstructionNumber: droidMission.nextInstructionNumber,
                topLevelMission: droidMission.topLevelMission,
                id: droidMission.id,
                type: droidMission.type,
                missionInstructionNumber: droidMission.missionInstructionNumber),
              dirty: true,
            ));
          }
        } catch (_) {
          emit(FloidMissionError());
        }
      }
      if (event is RefreshFloidMission) {
        try {
          final DroidMission droidMission = await floidServerRepository.fetchMission(event.droidMissionList.missionId);
          emit(FloidMissionLoaded(droidMission: droidMission, dirty: false,));
        } catch (_) {
          emit(FloidMissionError());
        }
      }
      // Create a new Mission:
      if (event is CreateFloidMissionEvent) {
        emit(FloidMissionLoading());
        try {
          final int newMissionId = await floidServerRepository.createNewMission(event.newMissionName);
          emit(FloidMissionUnloaded()); // Tell the mission list to load the new list but not load a new mission as we are going to load it
          final DroidMission droidMission = await floidServerRepository.fetchMission(newMissionId);
          emit(FloidMissionLoaded(droidMission: droidMission, dirty: false));
        } catch (_) {
          emit(FloidMissionError());
        }
      }
      // Unload the mission:
      if (event is UnloadFloidMissionEvent) {
        emit(FloidMissionUnloaded());
      }
      // Duplicate a Mission:
      if (event is DuplicateFloidMissionEvent) {
        emit(FloidMissionLoading());
        try {
          final int newMissionId = await floidServerRepository.duplicateMission(event.missionId, event.newMissionName);
          emit(FloidMissionUnloaded()); // Tell the mission list to load the new list but not load a new mission as we are going to load it
          final DroidMission droidMission = await floidServerRepository.fetchMission(newMissionId);
          emit(FloidMissionLoaded(droidMission: droidMission, dirty: false));
        } catch (_) {
          emit(FloidMissionError());
        }
      }
      // Delete a Mission:
      if (event is DeleteFloidMissionEvent) {
        emit(FloidMissionLoading());
        try {
          final bool missionDeleted = await floidServerRepository.deleteMission(event.missionId);
          if(missionDeleted) {
            // Tell the mission list to load the new list and load the first mission:
            emit(FloidMissionEmpty());
          } else {
            emit(FloidMissionError());
          }
        } catch (_) {
          emit(FloidMissionError());
        }
      }

      // Modify Floid Mission Name:
      if (event is ModifyFloidMissionNameEvent) {
        try {
          if (state is FloidMissionLoaded) {
            FloidMissionLoaded floidMissionLoaded = state as FloidMissionLoaded;
            DroidMission droidMission = floidMissionLoaded.droidMission;
            droidMission.missionName = event.newMissionName;
            emit(FloidMissionLoaded(droidMission: DroidMission(droidInstructions: droidMission.droidInstructions,
                missionName: droidMission.missionName,
                nextInstructionNumber: droidMission.nextInstructionNumber,
                topLevelMission: droidMission.topLevelMission,
                id: droidMission.id,
                type: droidMission.type,
                missionInstructionNumber: droidMission.missionInstructionNumber),
              dirty: true,
            ));
          }
        } catch (_) {
          emit(FloidMissionError());
        }
      }

    });
  }
}
