/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidTrackGpxFileEvent extends Equatable {
  FloidTrackGpxFileEvent([List props = const []]) : super();
}

class SetFloidTrackGpxFile extends FloidTrackGpxFileEvent {
  final String floidTrackGpxFile;
  final String fileName;
  SetFloidTrackGpxFile({required this.floidTrackGpxFile, required this.fileName})
      : super([floidTrackGpxFile, fileName]);
  @override
  List<Object> get props => [floidTrackGpxFile, fileName];
}

class ClearFloidTrackGpxFile extends FloidTrackGpxFileEvent {
  @override
  List<Object> get props => const [];
}

abstract class FloidTrackGpxFileState extends Equatable {
  FloidTrackGpxFileState([List props = const []]) : super();
}

class FloidTrackGpxFileEmpty extends FloidTrackGpxFileState {
  @override
  List<Object> get props => [];
}

class FloidTrackGpxFileLoaded extends FloidTrackGpxFileState {
  final String floidTrackGpxFile;
  final String fileName;

  FloidTrackGpxFileLoaded({required this.floidTrackGpxFile, required this.fileName})
      : super([floidTrackGpxFile, fileName]);
  @override
  List<Object> get props => [floidTrackGpxFile, fileName];
}

class FloidTrackGpxFileError extends FloidTrackGpxFileState {
  @override
  List<Object> get props => [];
}

class FloidTrackGpxFileBloc extends Bloc<FloidTrackGpxFileEvent, FloidTrackGpxFileState> {

  FloidTrackGpxFileBloc(): super(FloidTrackGpxFileEmpty()) {
    on<FloidTrackGpxFileEvent>((FloidTrackGpxFileEvent event, Emitter<FloidTrackGpxFileState> emit) async {
      // Set map gpx file:
      if (event is SetFloidTrackGpxFile) {
        print(event.floidTrackGpxFile.length.toString() + ' bytes GPX file (' + event.fileName + ')');
        emit(FloidTrackGpxFileLoaded(floidTrackGpxFile: event.floidTrackGpxFile, fileName: event.fileName));
      }
      // Clear map gpx file:
      if(event is ClearFloidTrackGpxFile) {
        emit(FloidTrackGpxFileEmpty());
      }
    });
  }
}
