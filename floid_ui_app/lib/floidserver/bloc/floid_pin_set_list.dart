/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/models/models.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidPinSetListEvent extends Equatable {
  FloidPinSetListEvent([List props = const []]) : super();
}

class FetchFloidPinSetList extends FloidPinSetListEvent {

  FetchFloidPinSetList() : super();
  @override
  List<Object> get props => [];
}

class RefreshFloidPinSetList extends FloidPinSetListEvent {

  RefreshFloidPinSetList() : super();
  @override
  List<Object> get props => [];
}

abstract class FloidPinSetListState extends Equatable {
  FloidPinSetListState([List props = const []]) : super();
}

class FloidPinSetListEmpty extends FloidPinSetListState {
  @override
  List<Object> get props => [];
}

class FloidPinSetListLoading extends FloidPinSetListState {
  @override
  List<Object> get props => [];
}

class FloidPinSetListLoaded extends FloidPinSetListState {
  final List<DroidPinSetList> droidPinSetListList;

  FloidPinSetListLoaded({required this.droidPinSetListList})
      : super([droidPinSetListList]);
  @override
  List<Object> get props => [droidPinSetListList];
}

class FloidPinSetListError extends FloidPinSetListState {
  @override
  List<Object> get props => [];
}

class FloidPinSetListBloc extends Bloc<FloidPinSetListEvent, FloidPinSetListState> {
  final FloidServerRepository floidServerRepository;

  FloidPinSetListBloc({required this.floidServerRepository})
      : super(FloidPinSetListEmpty()) {

    on<FloidPinSetListEvent>((FloidPinSetListEvent event, Emitter<FloidPinSetListState> emit) async {
      if (event is FetchFloidPinSetList) {
        emit(FloidPinSetListLoading());
        try {
          final List<DroidPinSetList> droidPinSetListList = await floidServerRepository.fetchPinSetList();
          emit(FloidPinSetListLoaded(droidPinSetListList: droidPinSetListList));
        } catch (_) {
          emit(FloidPinSetListError());
        }
      }

      if (event is RefreshFloidPinSetList) {
        try {
          final List<DroidPinSetList> droidPinSetListList = await floidServerRepository.fetchPinSetList();
          emit(FloidPinSetListLoaded(droidPinSetListList: droidPinSetListList));
        } catch (_) {
          emit(FloidPinSetListError());
        }
      }
    });
  }

}
