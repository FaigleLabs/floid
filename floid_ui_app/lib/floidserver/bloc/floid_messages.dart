/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/ui/debug/floid_debug_token_filter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:bloc/bloc.dart';

abstract class FloidMessagesEvent  {
  FloidMessagesEvent() : super();
}

class AddFloidMessage extends FloidMessagesEvent {
  final String message;
  final bool noToast;
  AddFloidMessage({required this.message, this.noToast=false})
      : super();
}

abstract class FloidMessagesState {
  FloidMessagesState([List props = const []]) : super();
}

class FloidMessagesEmpty extends FloidMessagesState {
}

class FloidMessagesLoading extends FloidMessagesState {
}

class FloidMessagesLoaded extends FloidMessagesState {
   final List<String> messages;

  FloidMessagesLoaded({required this.messages})
      : super([messages]);
}

class FloidMessagesError extends FloidMessagesState {
}

class FloidMessagesBloc extends Bloc<FloidMessagesEvent, FloidMessagesState> {
  static const int messagesSizeMax = 48;

  FloidMessagesBloc() : super(FloidMessagesEmpty()) {
    on<FloidMessagesEvent>((FloidMessagesEvent event, Emitter<FloidMessagesState> emit) async {
      if (event is AddFloidMessage) {
        List<String> messages;
        if (state is FloidMessagesLoaded) {
          messages = (state as FloidMessagesLoaded).messages;
        } else {
          messages = [];
        }
        // Add the message:
        String filteredMessage = FloidDebugTokenFilter().filter(event.message);
        filteredMessage.replaceAll(
            "com.faiglelabs.floid.servertypes.commands", "c.f.f.s.c");
        filteredMessage.replaceAll(
            "com.faiglelabs.floid.servertypes.mission", "c.f.f.s.m");
        if (filteredMessage != null) {
          // Strip off ending carriage return:
          if (filteredMessage.endsWith('\n')) {
            filteredMessage =
                filteredMessage.substring(0, filteredMessage.length - 1);
          }
          if(!event.noToast) {
            Fluttertoast.showToast(
              msg: filteredMessage,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER_RIGHT,
              timeInSecForIosWeb: 1,
              backgroundColor: Color(0xff222222),
              textColor: Colors.yellowAccent,
              fontSize: kIsWeb ? 16.0 : 12.0,
              webPosition: "center",
              webBgColor: "linear-gradient(to right, #333333, #111111)",
            );
          }
          messages.add(filteredMessage);
          // Cut the list down to size:
          while (messages.length > messagesSizeMax) {
            messages.removeAt(0);
          }
        }
        emit(FloidMessagesLoaded(messages: messages));
      }  
    });
  }

}
