/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class FloidMapGpxFileEvent extends Equatable {
  FloidMapGpxFileEvent([List props = const []]) : super();
}

class SetFloidMapGpxFile extends FloidMapGpxFileEvent {
  final String floidMapGpxFile;
  final String fileName;
  SetFloidMapGpxFile({required this.floidMapGpxFile, required this.fileName})
      : super([floidMapGpxFile, fileName]);
  @override
  List<Object> get props => [floidMapGpxFile, fileName];
}

class ClearFloidMapGpxFile extends FloidMapGpxFileEvent {
  @override
  List<Object> get props => const [];
}

abstract class FloidMapGpxFileState extends Equatable {
  FloidMapGpxFileState([List props = const []]) : super();
}

class FloidMapGpxFileEmpty extends FloidMapGpxFileState {
  @override
  List<Object> get props => [];
}

class FloidMapGpxFileLoaded extends FloidMapGpxFileState {
  final String floidMapGpxFile;
  final String fileName;

  FloidMapGpxFileLoaded({required this.floidMapGpxFile, required this.fileName})
      : super([floidMapGpxFile, fileName]);
  @override
  List<Object> get props => [floidMapGpxFile, fileName];
}

class FloidMapGpxFileError extends FloidMapGpxFileState {
  @override
  List<Object> get props => [];
}

class FloidMapGpxFileBloc extends Bloc<FloidMapGpxFileEvent, FloidMapGpxFileState> {

  FloidMapGpxFileBloc(): super(FloidMapGpxFileEmpty()) {
    on<FloidMapGpxFileEvent>((FloidMapGpxFileEvent event, Emitter<FloidMapGpxFileState> emit) async {
      // Set map gpx file:
      if (event is SetFloidMapGpxFile) {
        print(event.floidMapGpxFile.length.toString() + ' bytes GPX file (' + event.fileName + ')');
        emit(FloidMapGpxFileLoaded(floidMapGpxFile: event.floidMapGpxFile, fileName: event.fileName));
      }
      // Clear map gpx file:
      if(event is ClearFloidMapGpxFile) {
            emit(FloidMapGpxFileEmpty());
      }
    });
  }
}
