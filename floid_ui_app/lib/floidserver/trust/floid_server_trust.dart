/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:io';

import 'package:floid_ui_app/floidserver/client/client.dart';

class FloidServerTrust {
  static const String LOCAL_NETWORK = "192.168";
  static const String ANDROID_EMULATOR_NETWORK = "10.0.2.2";
  final FloidServerApiClient floidServerApiClient;
  FloidServerTrust({required this.floidServerApiClient});

  bool badCertificateCallback(X509Certificate cert, String host, int port) {
    if (host.startsWith(LOCAL_NETWORK)) {
      print('Trusting -->LOCAL NETWORK<-- cert:  host: ' + host + '  port: ' + port.toString() + '   subject: ' + cert.subject + '  hash:' + cert.sha1.toString());
      floidServerApiClient.setInsecure();
      return true;
    } else {
      if (host.startsWith(ANDROID_EMULATOR_NETWORK)) {
        print('Trusting -->ANDROID_EMULATOR_NETWORK<-- cert:  host: ' + host + '  port: ' + port.toString() + '   subject: ' + cert.subject + '  hash:' + cert.sha1.toString());
        floidServerApiClient.setInsecure();
        return true;
      } else {
        print('No trust for cert:  host: $host  port: $port subject: ${cert.subject}  hash: ${cert.sha1}');
        return false;
      }
    }
  }
}
