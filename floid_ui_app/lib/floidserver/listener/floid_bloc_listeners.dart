/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:collection/collection.dart';
import 'package:floid_ui_app/floidserver/address/address_proxy.dart';
import 'package:floid_ui_app/floidserver/bloc/droid_track_filtered_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_droid_track_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_map_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_track_filtered_gpx_file.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_track_gpx_file.dart';
import 'package:floid_ui_app/ui/controller/floid_errors.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_path_elevation.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_path_elevation_request.dart';
import 'package:floid_ui_app/ui/map/marker/floid_pin_attributes.dart';
import 'package:floid_ui_app/ui/map/utility/gpx_route_point.dart';
import 'package:floid_ui_app/ui/map/utility/utility.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/map/marker/marker.dart';
import 'package:floid_ui_app/ui/map/line/line.dart';

class FloidBlocListeners extends StatelessWidget {
  final Widget child;

  // TODO - Note that these are not configurable currently and should be made so
  static const double ERROR_OFFSET = 10;
  static const double WARNING_OFFSET = 15;
  static const int FLOID_LIFT_OFF_TEST_MIN_ALTITUDE = 10;

  FloidBlocListeners({Key? key,
    required this.child}) : super(key: key) {
    print('FloidBlocListeners()');
  }

  /// If we have a new pin set, process against the mission if exists:
  void _processNewPinSet(BuildContext context, FloidServerRepository floidServerRepository, FloidPinSetAlt floidPinSetAlt, bool secure, bool zoomToMarkers) {
    // If we have a pin set and a mission, process the mission over the pin set:
    // And process the mission against the pin-set
    print('Processing new pin set - ZTM: ' + zoomToMarkers.toString());
    // App state loaded?
    FloidAppState floidAppState = BlocProvider
        .of<FloidAppStateBloc>(context)
        .state;
    if (floidAppState is FloidAppStateLoaded) {
      // Pin set loaded?
      if (BlocProvider
          .of<FloidPinSetBloc>(context)
          .state is FloidPinSetLoaded) {
        // Mission loaded?
        if (BlocProvider
            .of<FloidMissionBloc>(context)
            .state is FloidMissionLoaded) {
          _processPinSetAndMission(
              context,
              floidServerRepository,
              floidPinSetAlt,
              (BlocProvider
                  .of<FloidMissionBloc>(context)
                  .state as FloidMissionLoaded).droidMission,
              secure,
              true,
              floidAppState.floidAppStateAttributes,
              zoomToMarkers);
        } else {
          _processPinSetAndMission(
              context,
              floidServerRepository,
              floidPinSetAlt,
              null,
              secure,
              true,
              floidAppState.floidAppStateAttributes,
              zoomToMarkers);
        }
      }
    }
  }

  /// If we have a new mission, process it against the pin set if exists:
  void _processNewMission(BuildContext context, DroidMission? droidMission, FloidServerRepository floidServerRepository, bool secure, FloidAppStateAttributes floidAppStateAttributes) {
    // And process the (possibly null) mission against the pin-set
    if (BlocProvider
            .of<FloidPinSetBloc>(context)
            .state is FloidPinSetLoaded) {
      _processPinSetAndMission(
          context,
          floidServerRepository,
          (BlocProvider
              .of<FloidPinSetBloc>(context)
              .state as FloidPinSetLoaded).floidPinSetAlt,
          droidMission,
          secure,
          false,
          floidAppStateAttributes,
          (BlocProvider
              .of<FloidPinSetBloc>(context)
              .state as FloidPinSetLoaded).zoomToMarkers);
    }
  }

  /// Process a pin set and mission:
  void _processPinSetAndMission(BuildContext context, FloidServerRepository floidServerRepository, FloidPinSetAlt floidPinSetAlt, DroidMission? droidMission, bool secure, bool clearMarkers,
      FloidAppStateAttributes floidAppStateAttributes, bool zoomToMarkers) {
    // Here we will process the mission against the pin set...
    if (droidMission != null) {
      int routePointNumber = 0;
      List<GpxRoutePoint> routePoints = [];
      GpxMarkersNeeded gpxMarkersNeeded = GpxMarkersNeeded();
      List<FloidMapFlyLine> floidMapFlyLines = [];
      List<FloidMapDesignateLine> floidMapDesignateLines = [];
      List<FloidFlyLinePathElevationMarker> floidFlyLinePathElevationMarkers = [];
      List<FloidMissionHomeMarker> missionMarkers = [];
      // List of errors as we process instructions:
      FloidErrors instructionErrors = FloidErrors(errors: []);
      // Create new pin attributes for each floid pin alt
      floidPinSetAlt.pinAltList.forEach((floidPinAlt) {
        floidPinAlt.floidPinAttributes = FloidPinAttributes();
      });
      // The flight distance we will calculate:
      double flightDistance = 0;
      // Get the home (current) pin:
      FloidPinAlt? homePin = getHomePin(floidPinSetAlt, droidMission);
      if (homePin != null) {
        // Add a home pin marker:
        missionMarkers.add(FloidMissionHomeMarker(floidPinAlt: homePin));
      }
      // If we got a home pin, denote it:
      homePin?.floidPinAttributes.isHome = true;
      FloidPinAlt? currentPin = homePin;
      // Get the minimum path height:
      double currentHeight = FloidFlyLinePathElevationMarker.PATH_ELEVATION_MIN_VALUE;
      if (currentPin != null) {
        currentHeight = currentPin.pinHeight;
      }

      FloidPinAlt? nextPin;
      FloidPinAlt? designatedPin;
      // Loop over each instruction:
      droidMission.droidInstructions.forEach((droidInstruction) {
        // Process commands that change elevation:
        if (droidInstruction is LandCommand) {
          final FloidPinAlt? fCurrentPin = currentPin;
          if(fCurrentPin!=null) {
            routePoints.add(GpxRoutePoint(lat: fCurrentPin.pinLat,
              lng: fCurrentPin.pinLng,
              elevation: currentHeight,
              name: '${routePointNumber++} ' + Gpx.LAND_CHARACTER_ICON + gpxMarkersNeeded.renderAndClear(), ));
          }
          // Denote land:
          currentPin?.floidPinAttributes.isLand = true;
          currentHeight = currentPin?.pinHeight ?? FloidFlyLinePathElevationMarker.PATH_ELEVATION_MIN_VALUE;
          final FloidPinAlt? landPin = currentPin;
          // For land command, we also add the pin at the end with a target symbol:
          if(landPin!=null) {
            routePoints.add(GpxRoutePoint(lat: landPin.pinLat,
              lng: landPin.pinLng,
              elevation: currentHeight,
              name: '${routePointNumber++} ' + Gpx.LAND_TARGET_CHARACTER_ICON
                  + gpxMarkersNeeded.renderAndClear(), ));
          }
        } else if (droidInstruction is HeightToCommand) {
          bool ascend = false;
          double originalPinHeight = currentHeight;
          if (droidInstruction.absolute) {
            if (currentHeight < droidInstruction.z) {
              currentPin?.floidPinAttributes.isAscend = true;
              ascend = true;
            }
            if (currentHeight > droidInstruction.z) {
              currentPin?.floidPinAttributes.isDescend = true;
              ascend = false;
            }
            currentHeight = droidInstruction.z.toDouble();
          } else {
            if (droidInstruction.z > 0) {
              currentPin?.floidPinAttributes.isAscend = true;
              ascend = true;
            }
            if (droidInstruction.z < 0) {
              currentPin?.floidPinAttributes.isDescend = true;
              ascend = false;
            }
            currentHeight += droidInstruction.z;
          }
          // Add a route point if not null:
          final FloidPinAlt? heightPin = currentPin;
          if(heightPin!=null) {
            routePoints.add(GpxRoutePoint(lat: heightPin.pinLat,
              lng: heightPin.pinLng,
              elevation: originalPinHeight,
              name: '${routePointNumber++} ' + (ascend?Gpx.HEIGHT_TO_ASCEND_CHARACTER_ICON:Gpx.HEIGHT_TO_DESCEND_CHARACTER_ICON)
                  + gpxMarkersNeeded.renderAndClear(), ));
          }
        } else if (droidInstruction is LiftOffCommand) {
          final FloidPinAlt? fCurrentPin = currentPin;
          if(fCurrentPin!=null) {
            routePoints.add(GpxRoutePoint(lat: fCurrentPin.pinLat,
              lng: fCurrentPin.pinLng,
              elevation: currentHeight,
              name: '${routePointNumber++} ' + Gpx.LIFT_OFF_CHARACTER_ICON
                  + gpxMarkersNeeded.renderAndClear(), ));
          }
          currentPin?.floidPinAttributes.isLiftOff = true;
          if (droidInstruction.absolute) {
            currentHeight = droidInstruction.z.toDouble();
          } else {
            currentHeight += droidInstruction.z;
          }
        }
        // Process commands that change distance:
        if (droidInstruction is FlyToCommand) {
          final FloidPinAlt? fCurrentPin = currentPin;
          if(fCurrentPin!=null) {
            routePoints.add(GpxRoutePoint(lat: fCurrentPin.pinLat,
              lng: fCurrentPin.pinLng,
              elevation: currentHeight,
              name: '${routePointNumber++} ' + Gpx.FLY_TO_CHARACTER_ICON
                  + gpxMarkersNeeded.renderAndClear(), ));
          }
          currentPin?.floidPinAttributes.isFlyTo = true;
          if (droidInstruction.flyToHome) {
            nextPin = homePin;
          } else {
            nextPin = findPinByName(floidPinSetAlt, droidInstruction.pinName);
          }
          final FloidPinAlt? fNextPin = nextPin;
          if (fCurrentPin != null && fNextPin != null) {
            if (fCurrentPin.pinName == fNextPin.pinName) {
              // Add a fly to same pin warning:
              instructionErrors.errors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.INSTRUCTION_ERROR_FLY_TO_SAME_PIN), extraInfo: fCurrentPin.pinName));
            }
            floidMapFlyLines.add(_createFlyLine(
                context,
                floidFlyLinePathElevationMarkers,
                fCurrentPin,
                fNextPin,
                currentHeight,
                ERROR_OFFSET,
                WARNING_OFFSET));
            flightDistance += Distance.distanceHaversine(fCurrentPin.pinLat, fCurrentPin.pinLng, fNextPin.pinLat, fNextPin.pinLng);
          } else {
            flightDistance = -1;
          }
          // Move to next pin:
          currentPin = nextPin;
        }
        // Designate:
        if (droidInstruction is DesignateCommand) {
          if (droidInstruction.usePin) {
            designatedPin = findPinByName(floidPinSetAlt, droidInstruction.pinName);
            // Set the pins as designated:
            designatedPin?.floidPinAttributes.isDesignated = true;
            currentPin?.floidPinAttributes.isDesignator = true;
            final FloidPinAlt? fCurrentPin = currentPin;
            final FloidPinAlt? fDesignatedPin = designatedPin;
            if (fCurrentPin != null && fDesignatedPin != null) {
              floidMapDesignateLines.add(_createDesignateLine(context, fCurrentPin, fDesignatedPin));
            }
          }
          else {
            FloidPinAlt tempPin = FloidPinAlt(pinLat: droidInstruction.x.toDouble(), pinLng: droidInstruction.y.toDouble(), pinHeight: droidInstruction.z.toDouble());
            tempPin.floidPinAttributes = FloidPinAttributes();
            tempPin.floidPinAttributes.isDesignated = true;
            final FloidPinAlt? fCurrentPin = currentPin;
            if (fCurrentPin != null) {
              floidMapDesignateLines.add(_createDesignateLine(context, fCurrentPin, tempPin));
            }
          }
        }
        // Denote the pin as a payload pin if appropriate:
        if (droidInstruction is PayloadCommand) {
          currentPin?.floidPinAttributes.isPayloadDropped = true;
          gpxMarkersNeeded.payload = true;
          switch (droidInstruction.bay) {
            case 0:
              currentPin?.floidPinAttributes.isPayloadBay0 = true;
              break;
            case 1:
              currentPin?.floidPinAttributes.isPayloadBay1 = true;
              break;
            case 2:
              currentPin?.floidPinAttributes.isPayloadBay2 = true;
              break;
            case 3:
              currentPin?.floidPinAttributes.isPayloadBay3 = true;
              break;
          }
        }
        // Hover:
        if (droidInstruction is HoverCommand) {
          currentPin?.floidPinAttributes.isHover = true;
        }
        // PhotoStrobe on:
        if (droidInstruction is PhotoStrobeCommand) {
          currentPin?.floidPinAttributes.isPhotoStrobeOn = true;
          gpxMarkersNeeded.photoStrobeOn = true;
        }
        // PhotoStrobe off:
        if (droidInstruction is StopPhotoStrobeCommand) {
          currentPin?.floidPinAttributes.isPhotoStrobeOff = true;
          gpxMarkersNeeded.photoStrobeOn = true;
        }
        // Photo:
        if (droidInstruction is TakePhotoCommand) {
          currentPin?.floidPinAttributes.isPhoto = true;
          gpxMarkersNeeded.photo = true;
        }
        // Start Video:
        if (droidInstruction is StartVideoCommand) {
          currentPin?.floidPinAttributes.isVideoOn = true;
          gpxMarkersNeeded.videoOn = true;
        }
        // Stop Video:
        if (droidInstruction is StopVideoCommand) {
          currentPin?.floidPinAttributes.isVideoOff = true;
          gpxMarkersNeeded.videoOff = true;
        }
        // Speaker:
        if (droidInstruction is SpeakerCommand) {
          currentPin?.floidPinAttributes.isSpeaker = true;
          gpxMarkersNeeded.speaker = true;
        }
        // Speaker On:
        if (droidInstruction is SpeakerOnCommand) {
          currentPin?.floidPinAttributes.isSpeakerOn = true;
          gpxMarkersNeeded.speakerOn = true;
        }
        // Speaker Off:
        if (droidInstruction is SpeakerOffCommand) {
          currentPin?.floidPinAttributes.isSpeakerOff = true;
          gpxMarkersNeeded.speakerOff = true;
        }
      });
      // Check the fly lines:
      FloidErrors floidFlyLineErrors = _checkFlyLines(
          context, floidMapFlyLines, floidServerRepository.floidServerApiClient, secure, FloidAppStateAttributes.nextFlyLineCheckId++, floidAppStateAttributes);
      FloidErrors floidErrors = _checkMissionForErrors(floidPinSetAlt, droidMission);
      floidErrors..addAll(floidFlyLineErrors)..addAll(instructionErrors);
      // Send the fly lines to the appropriate bloc:
      print('Setting ' + floidMapFlyLines.length.toString() + ' flylines to the bloc');
      for (FloidMapLine floidMapFlyLine in floidMapFlyLines) {
        print('Floid Map Fly Line: ');
        print('ID:        ' + floidMapFlyLine.lineId.toString());
        print('TYPE:      ' + floidMapFlyLine.type.toString());
        print('START LAT: ' + floidMapFlyLine.startLat.toString());
        print('START LNG: ' + floidMapFlyLine.startLng.toString());
        print('END LAT:   ' + floidMapFlyLine.endLat.toString());
        print('END LNG:   ' + floidMapFlyLine.endLng.toString());
      }
      BlocProvider.of<FloidMapLinesBloc<FloidMapFlyLine>>(context).add(SetFloidMapLines(floidMapLines: floidMapFlyLines));
      // Send the designate lines to the appropriate bloc:
      BlocProvider.of<FloidMapLinesBloc<FloidMapDesignateLine>>(context).add(SetFloidMapLines(floidMapLines: floidMapDesignateLines));
      // Send the mission markers to the appropriate bloc:
      BlocProvider.of<FloidMapMarkersBloc<FloidMissionHomeMarker>>(context).add(SetFloidMapMarkers(floidMapMarkers: missionMarkers, zoomToMarkers: false));
      // Send the fly line path elevation markers to the appropriate bloc:
      BlocProvider.of<FloidMapMarkersBloc<FloidFlyLinePathElevationMarker>>(context).add(SetFloidMapMarkers(floidMapMarkers: floidFlyLinePathElevationMarkers, zoomToMarkers: false));
      // Send the flight distance to the app state:
      BlocProvider.of<FloidAppStateBloc>(context).add(SetFlightDistanceAppStateEvent(flightDistance: flightDistance));
      if (clearMarkers) {
        BlocProvider.of<FloidMapMarkersBloc<FloidPinMarker>>(context).add(ClearFloidMapMarkers());
      }
      // Create the pin markers from the pins and send to the bloc:
      BlocProvider.of<FloidMapMarkersBloc<FloidPinMarker>>(context).add(
          SetFloidMapMarkers(floidMapMarkers: floidPinSetAlt.pinAltList.map((floidPinAlt) {
            return FloidPinMarker(floidPinAlt: floidPinAlt);
          }).toList(), zoomToMarkers: zoomToMarkers)
      );
      // Set the errors to the app bloc:
      BlocProvider.of<FloidAppStateBloc>(context).add(SetErrorsAppStateEvent(floidErrors: floidErrors, clearErrors: true));
      // Create the gpx file and send to the bloc:
      Gpx gpx = Gpx(name: droidMission.missionName + ' _ ' + floidPinSetAlt.pinSetName, timestamp: '');
      // Add in the waypoints:
      floidPinSetAlt.pinAltList.forEach((element) {
        gpx.addWaypoint(latitude: element.pinLat, longitude: element.pinLng, elevation: element.pinHeight, name: element.pinName, floidPinAttributes: element.floidPinAttributes,);
      });
      // Add in the RoutePoints
      routePoints.forEach((element) {
        gpx.addRoute(latitude: element.lat, longitude: element.lng, elevation: element.elevation, name: element.name, );
      });
      // Put the GPX file in to the bloc:
      BlocProvider.of<FloidMapGpxFileBloc>(context).add(SetFloidMapGpxFile(floidMapGpxFile: gpx.render(), fileName: '${droidMission.missionName}-${floidPinSetAlt.pinSetName}.gpx'));
    } else {
      // No mission, so create the pin markers from the pins and send to the bloc:
      BlocProvider.of<FloidMapMarkersBloc<FloidPinMarker>>(context).add(
          SetFloidMapMarkers(floidMapMarkers: floidPinSetAlt.pinAltList.map((floidPinAlt) {
            return FloidPinMarker(floidPinAlt: floidPinAlt);
          }).toList(), zoomToMarkers: zoomToMarkers)
      );
      // And then create the gpx file only from the pin markers and send to the bloc:
      Gpx gpx = Gpx(name: floidPinSetAlt.pinSetName, timestamp: '');
      // Add in the waypoints:
      floidPinSetAlt.pinAltList.forEach((element) {
        gpx.addWaypoint(latitude: element.pinLat, longitude: element.pinLng, elevation: element.pinHeight, name: element.pinName, floidPinAttributes: element.floidPinAttributes,);
      });
      // Put the GPX file in to the bloc:
      BlocProvider.of<FloidMapGpxFileBloc>(context).add(SetFloidMapGpxFile(floidMapGpxFile: gpx.render(), fileName: floidPinSetAlt.pinSetName+'.gpx'));
    }
  }

  /// Check Fly lines:
  FloidErrors _checkFlyLines(BuildContext context, List<FloidMapFlyLine> floidMapFlyLines, FloidServerApiClient floidServerApiClient, bool secure, final int flyLineCheckId,
      FloidAppStateAttributes floidAppStateAttributes) {
    final FloidErrors floidErrors = FloidErrors(errors: []);
    int pathCheckId = 0;
    int delay = 0;
    floidMapFlyLines.forEach((floidMapFlyLine) {
      try {
        FloidPathElevationRequest floidPathElevationRequest = FloidPathElevationRequest(
            floidMapFlyLine: floidMapFlyLine, flyLineCheckId: flyLineCheckId, pathCheckId: pathCheckId++);
        if (FloidElevationApiClient.fetchPathElevationFromCache(
            floidPathElevationRequest) == null) {
          // OK, we don't have this one in cache:
          registerOutstandingPathElevationRequest(context, floidPathElevationRequest);
          FloidElevationApiClient(floidServerApiClient: floidServerApiClient)
              .fetchPathElevation(
              floidPathElevationRequest, secure, floidAppStateAttributes.elevationLocation == FloidAppStateAttributes.elevationLocations[0] ? 0 : delay++, floidAppStateAttributes.elevationLocation)
              .then(
                  (FloidPathElevationRequest floidPathElevationRequest) {
                registerPathElevationRequestComplete(context, floidPathElevationRequest, _processPathElevation(floidPathElevationRequest));
                return floidPathElevationRequest;
              })
              .catchError((error) {
            print('_checkFlyLines - 1 - Caught Error: ' + error.toString());
            floidMapFlyLine.elevationStatus = FloidMapLine.ELEVATION_STATUS_FAIL;
            FloidErrors caughtErrors = FloidErrors(errors: <FloidErrorInstance>[])
              ..add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.FLIGHT_PATH_ELEVATION_STATUS_FAIL), extraInfo: error.toString()));
            registerPathElevationRequestComplete(context, floidPathElevationRequest, caughtErrors);
            return floidPathElevationRequest;
          });
        } else {
          floidErrors.addAll(_processPathElevation(floidPathElevationRequest));
        }
      } catch (e, stacktrace) {
        floidMapFlyLine.elevationStatus = FloidMapLine.ELEVATION_STATUS_FAIL;
        print('_checkFlyLines - 2 - Caught exception: ' + e.toString() + ' ' + stacktrace.toString());
      }
    });
    return floidErrors;
  }

  void registerOutstandingPathElevationRequest(BuildContext context, FloidPathElevationRequest floidPathElevationRequest) {
    print('Registering outstanding path request: ' + floidPathElevationRequest.flyLineCheckId.toString() + ' ' + floidPathElevationRequest.pathCheckId.toString());
    BlocProvider.of<FloidAppStateBloc>(context).add(
        OutstandingPathElevationRequestAppStateEvent(floidPathElevationRequest: floidPathElevationRequest));
  }

  void registerPathElevationRequestComplete(BuildContext context, FloidPathElevationRequest floidPathElevationRequest, FloidErrors floidErrors) {
    print('Finishing outstanding path request: ' + floidPathElevationRequest.flyLineCheckId.toString() + ' ' + floidPathElevationRequest.pathCheckId.toString());
    BlocProvider.of<FloidAppStateBloc>(context).add(
        PathElevationRequestCompleteAppStateEvent(floidPathElevationRequest: floidPathElevationRequest, floidErrors: floidErrors));
  }

  ///  Process path elevation - called when we get a path elevation (from cache or provider):
  FloidErrors _processPathElevation(FloidPathElevationRequest? floidPathElevationRequest) {
    final FloidErrors floidErrors = FloidErrors(errors: []);
    String? startPinName = floidPathElevationRequest?.floidMapFlyLine.startPin?.pinName;
    if (startPinName == null) startPinName = 'UNKNOWN';
    String? endPinName = floidPathElevationRequest?.floidMapFlyLine.endPin?.pinName;
    if (endPinName == null) endPinName = 'UNKNOWN';
    String extraInfo = startPinName + ' to ' + endPinName;
    if (floidPathElevationRequest != null) {
      if (floidPathElevationRequest.valid) {
        final FloidMapFlyLine floidMapFlyLine = floidPathElevationRequest.floidMapFlyLine;
        if (floidMapFlyLine != null) {
          final FloidPinAlt? fStartPin = floidPathElevationRequest.floidMapFlyLine.startPin;
          String startPinName = fStartPin == null ? '' : fStartPin.pinName;
          final FloidPinAlt? fEndPin = floidPathElevationRequest.floidMapFlyLine.endPin;
          String endPinName = fEndPin == null ? '' : fEndPin.pinName;
          print('Processing $startPinName to $endPinName');
          final double? fHeight = floidMapFlyLine.height;
          if (fHeight != null) {
            floidMapFlyLine.elevationStatus = FloidMapLine.ELEVATION_STATUS_GOOD;
          } else {
            floidMapFlyLine.elevationStatus = FloidMapLine.ELEVATION_STATUS_FAIL;
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.FLIGHT_PATH_ELEVATION_STATUS_FAIL), extraInfo: extraInfo));
          }
          double flightHeight = fHeight == null ? 0 : fHeight;
          final FloidPathElevation? fFloidPathElevation = floidMapFlyLine.floidPathElevation;
          double pathHeight = fFloidPathElevation == null ? 0 : fFloidPathElevation.pathElevationMax;
          double heightOverGround = flightHeight - pathHeight;
          if (flightHeight - pathHeight < ERROR_OFFSET) {
            // Error:
            floidMapFlyLine.altitudeStatus = FloidMapLine.ALTITUDE_STATUS_ERROR;
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.FLIGHT_PATH_ELEVATION_ERROR), extraInfo: extraInfo));
            print('...altitude status TOO LOW - ERROR LEVEL');
          } else if (heightOverGround < WARNING_OFFSET) {
            // Warning
            floidMapFlyLine.altitudeStatus = FloidMapLine.ALTITUDE_STATUS_WARNING;
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.FLIGHT_PATH_ELEVATION_WARNING), extraInfo: extraInfo));
            print('...altitude status TOO LOW - WARNING LEVEL');
          } else {
            // Good...
            floidMapFlyLine.altitudeStatus = FloidMapLine.ALTITUDE_STATUS_GOOD;
            print('...altitude status good');
          }
        } else {
          // Fly line was null...
          print('_processPathElevation: Fly line was null');
        }
      } else {
        // Was not valid
        floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.FLIGHT_PATH_NOT_VALID), extraInfo: extraInfo));
        print('_processPathElevation: Fly line was not valid');
      }
    } else {
      // Request was null
      floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.FLIGHT_PATH_NULL), extraInfo: extraInfo));
      print('_processPathElevation: Fly line was null');
    }
    return floidErrors;
  }

  /// Check a mission for errors
  FloidErrors _checkMissionForErrors(FloidPinSetAlt floidPinSetAlt, DroidMission droidMission) {
    print("Checking mission for errors");
    // Here we need to check mission only items, in addition to pin-names and heights, such as:
    // Must start with SetParameters, then an optional delay, then acquire home position
    // Must have lift-offs and lands in order
    // Must have a start and stop video in order
    // Must have designate and stop designating in order
    // Must stop designating before landing
    // Must have start photo with strobe before stop photo with strobe
    // Must stop photo strobe before mission end
    FloidErrors floidErrors = FloidErrors(errors: <FloidErrorInstance>[]);
    // Set up:
    bool spOk = false;
    bool skipCheck = false;
    bool ahpOk = false;
    bool helisOn = false;
    bool photoStrobeOn = false;
    bool isLiftedOff = false;
    bool videoIsOn = false;
    bool payloadBay0Used = false;
    bool payloadBay1Used = false;
    bool payloadBay2Used = false;
    bool payloadBay3Used = false;
    try {
      // Check mission length:
      final List<DroidInstruction> fDroidInstructions = droidMission.droidInstructions;
      if (fDroidInstructions.length == 0) {
        // Empty mission:
        floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_EMPTY_MISSION)));
      } else {
        // Check that setParameters is first item:
        final DroidInstruction fDroidInstruction = fDroidInstructions[0];
        if (fDroidInstruction is SetParametersCommand) {
          SetParametersCommand setParametersCommand = fDroidInstruction;
          if (setParametersCommand.droidParameters.indexOf('mission.skip_check=true') >= 0) {
            // Not checking mission further:
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_WARNING_SKIPPING_MISSION_ERROR_CHECK)));
            skipCheck = true;
          }
          spOk = true;
        }
        if (!spOk) {
          // Set parameters was not first item...
          floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_FIRST_ITEM_SET_PARAMETERS)));
        }
        if (!skipCheck) {
          // Empty pin set?
          if (floidPinSetAlt.pinAltList.length == 0) {
            // Empty pin set:
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_EMPTY_PIN_SET)));
          }
          if (droidMission.droidInstructions.length >= 2) {
            if (droidMission.droidInstructions[1] is AcquireHomePositionCommand) {
              ahpOk = true;
            }
          }
          if (!ahpOk) {
            // Acquire home parameters was not the second item...
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_SECOND_ITEM_ACQUIRE_HOME_POSITION)));
          }
          // OK, get the home pin and heights, etc.
          // We will need to store the current pin..in order to know original height for lift off
          double currentHeight = 0;
          bool currentHeightValid = false;
          FloidPinAlt? homePin = getHomePin(floidPinSetAlt, droidMission);
          FloidPinAlt? currentPin = homePin;
          if (currentPin != null) {
            currentHeight = currentPin.pinHeight;
            currentHeightValid = true;
          }
          if (homePin == null) {
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_NO_HOME_PIN)));
          }
          double newHeight = 0; // Placeholder for new height
          droidMission.droidInstructions.forEach((droidInstruction) {
            if (droidInstruction is SetParametersCommand) {
              // No test - originally we could have only one...but now we can have many
              //         - still has the requirements of having SetParameters as the first item but that is checked above
            }
            if (droidInstruction is FlyPatternCommand) {
              floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_FLY_PATTERN_NOT_IMPLEMENTED)));
            }
            if (droidInstruction is PhotoStrobeCommand) {
              if (photoStrobeOn) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_PHOTO_STROBE_ALREADY_STARTED)));
              } else {
                photoStrobeOn = true;
              }
            }
            if (droidInstruction is StopPhotoStrobeCommand) {
              if (photoStrobeOn) {
                photoStrobeOn = false;
              } else {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_PHOTO_STROBE_STOPPED_BUT_NOT_STARTED)));
              }
            }
            if (droidInstruction is FlyToCommand) {
              bool flyToGoodPin = false;
              String? flyToPinName = droidInstruction.pinName;
              FloidPinAlt? flyToPin;
              if (droidInstruction.flyToHome) {
                flyToPin = getHomePin(floidPinSetAlt, droidMission);
              } else if (flyToPinName.isNotEmpty) {
                flyToPin = findPinByName(floidPinSetAlt, flyToPinName);
              }
              if (flyToPin != null) {
                // Pin was good
                flyToGoodPin = true;
                // Now check the height:
                if (currentHeightValid) {
                  if (flyToPin.pinHeight >= currentHeight) {
                    floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_FLIGHT_BELOW_PIN_LEVEL), extraInfo: flyToPinName));
                  }
                }
                else {
                  // Bad current height - add warning:
                  floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_WARNING_FLIGHT_BELOW_PIN_LEVEL), extraInfo: flyToPinName));
                }
              }
              currentPin = flyToPin;
              if (!flyToGoodPin) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_BAD_PIN), extraInfo: flyToPinName));
              }
            }
            if (droidInstruction is DesignateCommand) {
              String designatePinName = droidInstruction.pinName;
              FloidPinAlt? designatePin = findPinByName(floidPinSetAlt, designatePinName);
              if (designatePin == null) {
                // Bad designate pin:
                floidErrors.add(
                    FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_BAD_DESIGNATE_PIN), extraInfo: designatePinName));
              }
            }
            if (droidInstruction is HoverCommand) {
              if (!isLiftedOff) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_HOVER_AFTER_LIFT_OFF), extraInfo: ''));
              }
            }
            if (droidInstruction is DelayCommand) {
              if (isLiftedOff) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_DELAY_ON_GROUND), extraInfo: ''));
              }
            }
            if (droidInstruction is PanTiltCommand) {
              // Check either angles or pinName:
              if (droidInstruction.useAngle) {
                // TODO [PAN-TILT] PanTilt Command: CHECK THAT THESE ANGLES ARE IN RANGE.....ONCE I KNOW WHAT THE SERVOS WILL DO...
              }
              else {
                // Check the pinName
                String panTiltPinName = droidInstruction.pinName;
                FloidPinAlt? panTiltPin = findPinByName(floidPinSetAlt, panTiltPinName);
                if (panTiltPin == null) {
                  // Bad pan/tilt pin:
                  floidErrors.add(
                      FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_BAD_PAN_TILT_PIN), extraInfo: panTiltPinName));
                }
              }
            }
            if (droidInstruction is LiftOffCommand) {
              if (droidInstruction.absolute) {
                newHeight = droidInstruction.z.toDouble();
                currentHeightValid = true;
              }
              else {
                if (currentHeightValid) {
                  newHeight = currentHeight + droidInstruction.z;
                }
                if (droidInstruction.z < FLOID_LIFT_OFF_TEST_MIN_ALTITUDE) {
                  floidErrors.add(FloidErrorInstance(
                      floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_RELATIVE_LIFT_OFF_LOW),
                      extraInfo: FLOID_LIFT_OFF_TEST_MIN_ALTITUDE.toString()));
                }
              }
              if (currentHeightValid) {
                final FloidPinAlt? fCurrentPin = currentPin;
                if (fCurrentPin != null) {
                  if (fCurrentPin.pinHeight >= newHeight) {
                    floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_LIFT_OFF_BELOW_PIN_HEIGHT)));
                  }
                }
                else {
                  floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_LIFT_OFF_CANNOT_CHECK_PIN_HEIGHT)));
                }
              }
              else {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_LIFT_OFF_CANNOT_CHECK_PIN_HEIGHT)));
              }
              currentHeight = newHeight;
              if (!helisOn) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_LIFT_OFF_BEFORE_START_HELIS)));
              }
              if (isLiftedOff) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_LIFT_OFF_ALREADY_IN_AIR)));
              }
              isLiftedOff = true;
            }
            if (droidInstruction is HeightToCommand) {
              if (droidInstruction.absolute) {
                newHeight = droidInstruction.z.toDouble();
                currentHeightValid = true;
              }
              else {
                if (currentHeightValid) {
                  newHeight = currentHeight + droidInstruction.z;
                }
              }
              if (currentHeightValid) {
                final FloidPinAlt? fCurrentPin = currentPin;
                final String pinName = fCurrentPin != null ? (fCurrentPin.pinName) : 'NULL';
                if (fCurrentPin != null) {
                  if (fCurrentPin.pinHeight >= newHeight) {
                    floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_HEIGHT_TO_BELOW_PIN_HEIGHT), extraInfo: pinName),);
                  }
                }
              }
              else {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_HEIGHT_TO_CANNOT_CHECK_PIN_HEIGHT)));
              }
              // Check that we are lifted off
              if (!isLiftedOff) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_HEIGHT_TO_WITHOUT_LIFT_OFF)));
              }
              currentHeight = newHeight;
              currentHeightValid = true;
            }
            if (droidInstruction is TurnToCommand) {
              // Check that we have lifted off
              if (!isLiftedOff) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_TURN_TO_WITHOUT_LIFT_OFF)));
              }
              isLiftedOff = false;
            }
            if (droidInstruction is LandCommand) {
              // Check that we have lifted off
              if (!isLiftedOff) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_LAND_WITHOUT_LIFT_OFF)));
              }
              isLiftedOff = false;
            }
            if (droidInstruction is StartUpHelisCommand) {
              // Check that the helis are not yet started
              if (helisOn) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_HELIS_ALREADY_STARTED)));
              }
              helisOn = true;
            }
            if (droidInstruction is ShutDownHelisCommand) {
              // Check that the helis are started
              // Check that we are not lifted off
              if (!helisOn) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_HELIS_SHUT_DOWN_WITHOUT_START_UP)));
              }
              if (isLiftedOff) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_HELIS_SHUT_DOWN_DURING_FLIGHT)));
              }
              helisOn = false;
            }
            if (droidInstruction is StartVideoCommand) {
              // Check that the video has not started
              if (videoIsOn) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_VIDEO_ALREADY_STARTED)));
              }
              videoIsOn = true;
            }
            if (droidInstruction is StopVideoCommand) {
              // Check that the video has started
              if (!videoIsOn) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_VIDEO_NOT_STARTED)));
              }
              videoIsOn = false;
            }
            if (droidInstruction is TakePhotoCommand) {
              // Check that the video has not started
              if (videoIsOn) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_WARNING_PHOTO_DURING_VIDEO)));
              }
            }
            if (droidInstruction is SpeakerCommand) {
              // Do nothing
            }
            if (droidInstruction is SpeakerOnCommand) {
              // Do nothing
            }
            if (droidInstruction is SpeakerOffCommand) {
              // Do nothing
            }
            if (droidInstruction is PayloadCommand) {
              // Check that we have lifted off:
              if (!isLiftedOff) {
                floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_PAYLOAD_MUST_BE_IN_FLIGHT)));
              }
              // Check the bay:
              int bayNumber = droidInstruction.bay;
              switch (bayNumber) {
                case 0:
                  if (payloadBay0Used) {
                    floidErrors.add(
                        FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_BAY_ALREADY_USED), extraInfo: bayNumber.toString()));
                  }
                  payloadBay0Used = true;
                  break;
                case 1:
                  if (payloadBay1Used) {
                    floidErrors.add(
                        FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_BAY_ALREADY_USED), extraInfo: bayNumber.toString()));
                  }
                  payloadBay1Used = true;
                  break;
                case 2:
                  if (payloadBay2Used) {
                    floidErrors.add(
                        FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_BAY_ALREADY_USED), extraInfo: bayNumber.toString()));
                  }
                  payloadBay2Used = true;
                  break;
                case 3:
                  if (payloadBay3Used) {
                    floidErrors.add(
                        FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_BAY_ALREADY_USED), extraInfo: bayNumber.toString()));
                  }
                  payloadBay3Used = true;
                  break;
                default:
                  floidErrors.add(
                      FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_INVALID_BAY_NUMBER), extraInfo: bayNumber.toString()));
                  break;
              }
            }
          });
          // Finally, we need to make sure the heli landed if took off, turned off video if on, stopped helis if started, etc. etc.
          if (isLiftedOff) {
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_DOES_NOT_FINISH_ON_GROUND)));
          }
          if (helisOn) {
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_DOES_NOT_FINISH_WITH_HELIS_OFF)));
          }
          if (videoIsOn) {
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_DOES_NOT_FINISH_WITH_VIDEO_OFF)));
          }
          if (photoStrobeOn) {
            floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_DOES_NOT_FINISH_WITH_PHOTO_STROBE_OFF)));
          }
        }
      }
    } catch (e, stacktrace) {
      print('_checkMissionForErrors: ' + e.toString() + " " + stacktrace.toString());
      floidErrors.add(FloidErrorInstance(floidError: FloidError.getFloidError(FloidError.MISSION_ERROR_MISSION_CHECK_EXCEPTION), extraInfo: e.toString()));
    }
    return floidErrors;
  }

  /// Create a fly line:
  FloidMapFlyLine _createFlyLine(BuildContext context,
      List<FloidFlyLinePathElevationMarker> floidFlyLinePathElevationMarkers,
      FloidPinAlt startPin, FloidPinAlt endPin, double height,
      double errorOffset, double warningOffset) {
    double flyLineDistanceInMeters = Distance.distanceHaversine(startPin.pinLat, startPin.pinLng, endPin.pinLat, endPin.pinLng);
    FloidPathElevation floidPathElevation = FloidPathElevation(
      pathElevationStartLat: startPin.pinLat,
      pathElevationStartLng: startPin.pinLng,
      pathElevationEndLng: endPin.pinLng,
      pathElevationEndLat: endPin.pinLat,
      pathElevationActualNumberSamples: 0,
      pathElevationRequestedSegmentLength: flyLineDistanceInMeters,
      pathElevationActualSegmentLength: flyLineDistanceInMeters,
      pathElevationMax: FloidFlyLinePathElevationMarker.PATH_ELEVATION_MIN_VALUE,
      pathElevationResult: FloidPathElevation.RESULT_UNKNOWN,
      pathElevationMaxNumberSamples: 0,
      pathElevationValid: false,
      toolTip: '',);
    FloidFlyLinePathElevationMarker floidFlyLinePathElevationMarker = FloidFlyLinePathElevationMarker(
      startPin: startPin,
      endPin: endPin,
      markerLat: (startPin.pinLat * 2 + endPin.pinLat) / 3,
      markerLng: (startPin.pinLng * 2 + endPin.pinLng) / 3,
      flightHeight: height,
      errorOffset: errorOffset,
      warningOffset: warningOffset,
      floidPathElevation: floidPathElevation,
      flightDistanceInMeters: flyLineDistanceInMeters,);
    FloidMapFlyLine floidMapFlyLine = new FloidMapFlyLine(
      type: FloidMapLine.MAP_LINE_TYPE_FLY,
      elevationStatus: FloidMapLine.ELEVATION_STATUS_UNKNOWN,
      altitudeStatus: FloidMapLine.ALTITUDE_STATUS_UNKNOWN,
      lineId: FloidMapLine.nextLineId++,
      startPin: startPin,
      endPin: endPin,
      startLat: startPin.pinLat,
      startLng: startPin.pinLng,
      endLat: endPin.pinLat,
      endLng: endPin.pinLng,
      height: height,
      floidFlyLinePathElevationMarker: floidFlyLinePathElevationMarker,
      floidPathElevation: floidPathElevation,
    );
    // Make the opposing connection from the elevation back to the marker:
    floidFlyLinePathElevationMarker.floidPathElevation.floidMapFlyLine = floidMapFlyLine;
    // Add the path elevation marker to the list:
    floidFlyLinePathElevationMarkers.add(floidFlyLinePathElevationMarker);
    // Return the fly line:
    return floidMapFlyLine;
  }

  /// Create a designator line:
  FloidMapDesignateLine _createDesignateLine(BuildContext context, FloidPinAlt startPin, FloidPinAlt endPin) {
    FloidMapDesignateLine floidMapDesignateLine = new FloidMapDesignateLine(
      type: FloidMapLine.MAP_LINE_TYPE_DESIGNATE,
      elevationStatus: FloidMapLine.ELEVATION_STATUS_UNKNOWN,
      altitudeStatus: FloidMapLine.ALTITUDE_STATUS_UNKNOWN,
      lineId: FloidMapLine.nextLineId++,
      startPin: startPin,
      endPin: endPin,
      startLat: startPin.pinLat,
      startLng: startPin.pinLng,
      endLat: endPin.pinLat,
      endLng: endPin.pinLng,
    );
    return floidMapDesignateLine;
  }

  /// Find a pin by name:
  static FloidPinAlt? findPinByName(FloidPinSetAlt floidPinSetAlt, String pinName) {
    try {
      return floidPinSetAlt.pinAltList.firstWhereOrNull((pin) => pin.pinName == pinName);
    } catch (e) {
      return null;
    }
  }

  /// Get the home pin:
  static FloidPinAlt? getHomePin(FloidPinSetAlt floidPinSetAlt, DroidMission droidMission) {
    if (droidMission == null || floidPinSetAlt == null || floidPinSetAlt.pinAltList.length < 1 ||
        droidMission.droidInstructions.length < 1) {
      return null;
    }
    // Find the home pin by locating the first set parameters command and
    final DroidInstruction? fDroidIInstruction = droidMission.droidInstructions.firstWhereOrNull((droidInstruction) => droidInstruction is SetParametersCommand);
    if (fDroidIInstruction != null && fDroidIInstruction is SetParametersCommand) {
      if (fDroidIInstruction.firstPinIsHome) {
        return floidPinSetAlt.pinAltList.elementAt(0);
      } else {
        return floidPinSetAlt.pinAltList.firstWhereOrNull((floidPinAlt) =>
        floidPinAlt.pinName == fDroidIInstruction.homePinName);
      }
    }
    // No set parameters found - use first pin:
    return floidPinSetAlt.pinAltList.elementAt(0);
  }


  @override
  Widget build(BuildContext context) {
    print('FloidBlocListeners: build()');
    return MultiBlocListener(
      listeners: [
        // App state listener:
        BlocListener<FloidAppStateBloc, FloidAppState>(
          listenWhen: (previousFloidAppState, currentFloidAppState) {
            return previousFloidAppState is FloidAppStateLoaded;
          },
          listener: (context, floidAppState) {
            // Do anything here on app state changes...
          },
        ),
        // Mission List State listener - triggers when mission list loads:
        BlocListener<FloidMissionListBloc, FloidMissionListState>(
          listenWhen: (previousFloidMissionListState, currentFloidMissionListState) {
            return currentFloidMissionListState is FloidMissionListLoaded;
          },
          listener: (context, floidMissionListState) {
            if (floidMissionListState is FloidMissionListLoaded && floidMissionListState.droidMissionListList.length > 0) {
              // On the mission 'empty' or 'unloaded' state we load either the saved mission if exists and in list or first mission:
              if (BlocProvider
                  .of<FloidMissionBloc>(context)
                  .state is FloidMissionEmpty || BlocProvider
                  .of<FloidMissionBloc>(context)
                  .state is FloidMissionUnloaded) {
                FloidAppState floidAppState = BlocProvider
                    .of<FloidAppStateBloc>(context)
                    .state;
                int missionIdIndex = 0;
                if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.savedMissionId != DroidMission.NO_MISSION) {
                  // See if the saved mission exists in the list - otherwise we load the default (element 0)
                  int missionId = floidAppState.floidAppStateAttributes.savedMissionId;
                  for (int i = 0; i < floidMissionListState.droidMissionListList.length; ++i) {
                    if (floidMissionListState.droidMissionListList
                        .elementAt(i)
                        .missionId == missionId) {
                      missionIdIndex = i;
                    }
                  }
                }
                BlocProvider.of<FloidMissionBloc>(context).add(FetchFloidMission(droidMissionList: floidMissionListState.droidMissionListList.elementAt(missionIdIndex)));
              }
            }
          },
        ),
        // Pin Set List State listener - triggers when pin set list loads:
        BlocListener<FloidPinSetListBloc, FloidPinSetListState>(
          listenWhen: (previousFloidPinSetListState, currentFloidPinSetListState) {
            return currentFloidPinSetListState is FloidPinSetListLoaded;
          },
          listener: (context, floidPinSetListState) {
            if (floidPinSetListState is FloidPinSetListLoaded &&
                floidPinSetListState.droidPinSetListList.length > 0) {
              // On the pin set 'empty' state we load the first pin set:
              if (BlocProvider
                  .of<FloidPinSetBloc>(context)
                  .state is FloidPinSetEmpty || BlocProvider
                  .of<FloidPinSetBloc>(context)
                  .state is FloidPinSetUnloaded) {
                FloidAppState floidAppState = BlocProvider
                    .of<FloidAppStateBloc>(context)
                    .state;
                int pinSetIdIndex = 0;
                if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.savedPinSetId != FloidPinSetAlt.NO_PIN_SET) {
                  // See if the saved pin set exists in the list - otherwise we load the default (element 0)
                  int pinSetId = floidAppState.floidAppStateAttributes.savedPinSetId;
                  for (int i = 0; i < floidPinSetListState.droidPinSetListList.length; ++i) {
                    if (floidPinSetListState.droidPinSetListList
                        .elementAt(i)
                        .pinSetId == pinSetId) {
                      pinSetIdIndex = i;
                    }
                  }
                }
                BlocProvider.of<FloidPinSetBloc>(context).add(FetchFloidPinSet(droidPinSetList: floidPinSetListState.droidPinSetListList.elementAt(pinSetIdIndex)));
              }
            }
          },
        ),
        // Floid List State listener - triggers when floid list loads:
        BlocListener<FloidListBloc, FloidListState>(
          listenWhen: (previousFloidListState, currentFloidListState) {
            return currentFloidListState is FloidListLoaded;
          },
          listener: (context, floidListState) {
            // Do nothing - we do not use the floid list, but rather the floidIdAndStateList for all logic...
          },
        ),
        // Floid Id and State List State listener - triggers when floid Id and State list loads:
        BlocListener<FloidIdAndStateListBloc, FloidIdAndStateListState>(
          listenWhen: (previousFloidIdAndStateListState, currentFloidIdAndStateListState) {
            return currentFloidIdAndStateListState is FloidIdAndStateListLoaded;
          },
          listener: (context, floidIdAndStateListState) {
            // OK, when this list loads, we pick either the saved floid id or the first floid in the list:
            if (floidIdAndStateListState is FloidIdAndStateListLoaded && floidIdAndStateListState.floidIdAndStateList.length > 0) {
              FloidAppState floidAppState = BlocProvider
                  .of<FloidAppStateBloc>(context)
                  .state;
              int floidIdIndex = 0;
              if (floidAppState is FloidAppStateLoaded && floidAppState.floidAppStateAttributes.savedFloidId != FloidIdAndState.NO_FLOID_ID) {
                // See if the saved floid exists in the list - otherwise we load the default (element 0)
                int floidId = floidAppState.floidAppStateAttributes.savedFloidId;
                for (int i = 0; i < floidIdAndStateListState.floidIdAndStateList.length; ++i) {
                  if (floidIdAndStateListState.floidIdAndStateList
                      .elementAt(i)
                      .floidId == floidId) {
                    floidIdIndex = i;
                  }
                }
              }
              BlocProvider.of<FloidIdBloc>(context).add(SetFloidIdAndState(floidIdAndState: floidIdAndStateListState.floidIdAndStateList.elementAt(floidIdIndex)));
            }
          },
        ),
        // Mission listener - triggers when mission loads:
        BlocListener<FloidMissionBloc, FloidMissionState>(
          listenWhen: (previousFloidMissionState, currentFloidMissionState) {
            return currentFloidMissionState is FloidMissionLoaded
                || currentFloidMissionState is FloidMissionEmpty
                || currentFloidMissionState is FloidMissionUnloaded;
          },
          listener: (context, floidMissionState) {
            if (floidMissionState is FloidMissionLoaded) {
              FloidServerRepositoryState floidServerRepositoryState = BlocProvider
                  .of<FloidServerRepositoryBloc>(context)
                  .state;
              FloidAppState floidAppState = BlocProvider
                  .of<FloidAppStateBloc>(context)
                  .state;
              if (floidServerRepositoryState is FloidServerRepositoryStateHasHost
                  && floidAppState is FloidAppStateLoaded) {
                _processNewMission(context, floidMissionState.droidMission, floidServerRepositoryState.floidServerRepository, floidServerRepositoryState.secure, floidAppState.floidAppStateAttributes);
              }
              BlocProvider.of<FloidMessagesBloc>(context).add(
                  AddFloidMessage(message: 'Mission: ' + floidMissionState.droidMission.missionName, noToast: true));
              AddressProxy addressProxy = AddressProxy();
              addressProxy.saveLastMission(floidMissionState.droidMission.id);
            }
            // We get either an empty state (reload mission list and then load first mission) or an unloaded state (only do the reload - we have a specific mission we are going to load
            if (floidMissionState is FloidMissionEmpty || floidMissionState is FloidMissionUnloaded) {
              BlocProvider.of<FloidMessagesBloc>(context).add(AddFloidMessage(message: 'Reloading Mission List', noToast: true));
              BlocProvider.of<FloidMissionListBloc>(context).add(FetchFloidMissionList());
            }
          },
        ),
        // Pin Set listener - triggers when pin set loads:
        BlocListener<FloidPinSetBloc, FloidPinSetState>(
          listenWhen: (previousFloidPinSetState, currentFloidPinSetState) {
            return currentFloidPinSetState is FloidPinSetLoaded
                || currentFloidPinSetState is FloidPinSetEmpty
                || currentFloidPinSetState is FloidPinSetUnloaded;
          },
          listener: (context, floidPinSetState) {
            if (floidPinSetState is FloidPinSetLoaded) {
              FloidServerRepositoryState floidServerRepositoryState = BlocProvider
                  .of<FloidServerRepositoryBloc>(context)
                  .state;
              if (floidServerRepositoryState is FloidServerRepositoryStateHasHost) {
                _processNewPinSet(context, floidServerRepositoryState.floidServerRepository, floidPinSetState.floidPinSetAlt, floidServerRepositoryState.secure, floidPinSetState.zoomToMarkers);
              }
              BlocProvider.of<FloidMessagesBloc>(context).add(
                  AddFloidMessage(message: 'Pin Set: ' + floidPinSetState.floidPinSetAlt.pinSetName, noToast: true));
              AddressProxy addressProxy = AddressProxy();
              addressProxy.saveLastPinSet(floidPinSetState.floidPinSetAlt.id);
            }
            // It the current pin set was deleted, we get an 'Unloaded' state - here we want to reload the pin set list:
            if (floidPinSetState is FloidPinSetEmpty || floidPinSetState is FloidPinSetUnloaded) {
              BlocProvider.of<FloidMessagesBloc>(context).add(AddFloidMessage(message: 'Loading Pin Set List', noToast: true));
              BlocProvider.of<FloidPinSetListBloc>(context).add(FetchFloidPinSetList());
            }
          },
        ),
        // Floid Id listener - triggers when set above:
        BlocListener<FloidIdBloc, FloidIdState>(
          listenWhen: (previousFloidIdState, currentFloidIdState) {
            return true;
          },
          listener: (context, floidIdState) {
            if (floidIdState is FloidIdLoaded) {
              print('BLOC: FloidIdLoaded: ${floidIdState.floidIdAndState.floidId}');
              // Load the floid uuid and state list for the chosen floid id:
              BlocProvider.of<FloidUuidAndStateListBloc>(context).add(FetchFloidUuidAndStateList(floidIdAndState: floidIdState.floidIdAndState));
              BlocProvider.of<FloidMessagesBloc>(context).add(AddFloidMessage(message: "Id: " + floidIdState.floidIdAndState.floidId.toString()));
              // Listen for new uuids:
              BlocProvider.of<FloidNewFloidUuidBloc>(context).add(StartFloidNewFloidUuidEvent(floidIdAndState: floidIdState.floidIdAndState));
              AddressProxy addressProxy = AddressProxy();
              addressProxy.saveLastFloid(floidIdState.floidIdAndState.floidId);
            }
            if (floidIdState is FloidIdEmpty) {
              print('BLOC: FloidIdEmpty');
              BlocProvider.of<FloidMessagesBloc>(context).add(AddFloidMessage(message: 'Loading Floid Id List', noToast: true));
              BlocProvider.of<FloidIdAndStateListBloc>(context).add(FetchFloidIdAndStateList());
            }
            if (floidIdState is FloidUuidLoaded) {
              print('BLOC: FloidUuidLoaded: ${floidIdState.floidUuidAndState.floidUuid}');
              // Add a message to the list of messages:
              BlocProvider.of<FloidMessagesBloc>(context).add(AddFloidMessage(message: 'Uuid: ' + floidIdState.floidUuidAndState.floidUuid, noToast: true));
              // Fetch the historical/current data:
              BlocProvider.of<FloidGpsPointsFilteredBloc>(context).add(
                  FetchFloidGpsPointsFiltered(floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
              BlocProvider.of<FloidDroidGpsPointsFilteredBloc>(context).add(
                  FetchFloidDroidGpsPointsFiltered(floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
              // Tell each of the stomp blocs to use this floid id/uuid now:
              BlocProvider.of<FloidDroidStatusBloc>(context).add(
                  UpdateFloidDroidStatusFloidId(floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
              BlocProvider.of<FloidStatusBloc>(context).add(
                  UpdateFloidStatusFloidId(floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
              BlocProvider.of<FloidModelStatusBloc>(context).add(
                  UpdateFloidModelStatusFloidId(floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
              BlocProvider.of<FloidModelParametersBloc>(context).add(
                  UpdateFloidModelParametersFloidId(floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
              BlocProvider.of<FloidDroidMessageBloc>(context).add(
                  UpdateFloidDroidMessageFloidId(floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
              BlocProvider.of<FloidDroidMissionInstructionStatusMessageBloc>(context).add(UpdateFloidDroidMissionInstructionStatusMessageFloidId(
                  floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
              BlocProvider.of<FloidDroidPhotoBloc>(context).add(
                  UpdateFloidDroidPhotoFloidId(floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
              BlocProvider.of<FloidDroidVideoFrameBloc>(context).add(
                  UpdateFloidDroidVideoFrameFloidId(floidId: floidIdState.floidIdAndState.floidId, floidUuid: floidIdState.floidUuidAndState.floidUuid));
            }
          },
        ),
        // Floid Gps Points Filtered Bloc:
        BlocListener<FloidGpsPointsFilteredBloc, FloidGpsPointsFilteredState>(
          listenWhen: (previousFloidGpsPointsFilteredState, currentFloidGpsPointsFilteredState) {
            return true;
          },
          listener: (context, floidGpsPointsFilteredState) {
            if (floidGpsPointsFilteredState is FloidGpsPointsFilteredLoaded) {
              // Create and set map lines from these points:
              FloidGpsPointsFilteredLoaded floidGpsPointsFilteredLoaded = floidGpsPointsFilteredState;
              List<FloidMapFloidGpsLine> floidGpsPointsFilteredLines = [];
              FloidGpsPoint? currentFloidGpsPoint;
              for (FloidGpsPoint floidGpsPoint in floidGpsPointsFilteredLoaded.gpsPoints) {
                if (currentFloidGpsPoint != null) {
                  FloidMapFloidGpsLine floidMapFloidGpsLine = FloidMapFloidGpsLine(
                    floidPathElevation: null,
                    startLat: currentFloidGpsPoint.latitude,
                    startLng: currentFloidGpsPoint.longitude,
                    endLat: floidGpsPoint.latitude,
                    endLng: floidGpsPoint.longitude,
                    type: FloidMapLine.MAP_LINE_TYPE_GPS,
                    lineId: 0,
                    altitudeStatus: FloidMapLine.ALTITUDE_STATUS_GOOD,
                    elevationStatus: FloidMapLine.ELEVATION_STATUS_GOOD,
                    startPin: null,
                    endPin: null,);
                  floidGpsPointsFilteredLines.add(floidMapFloidGpsLine);
                }
                currentFloidGpsPoint = floidGpsPoint;
              }
              // Set these points to the map line:
              BlocProvider.of<FloidMapLinesBloc<FloidMapFloidGpsLine>>(context).add(SetFloidMapLines(floidMapLines: floidGpsPointsFilteredLines));
            }
            // If our new state is empty or loading, we clear the gpx file if loaded:
            if (floidGpsPointsFilteredState is FloidGpsPointsFilteredEmpty || floidGpsPointsFilteredState is FloidGpsPointsFilteredLoading) {
              // Clear the gpx file if loaded:
              if(BlocProvider.of<FloidTrackFilteredGpxFileBloc>(context).state is FloidTrackFilteredGpxFileLoaded) {
                BlocProvider.of<FloidTrackFilteredGpxFileBloc>(context).add(ClearFloidTrackFilteredGpxFile());
              }
            }
          },
        ),
        // Droid Gps Points Filtered Bloc:
        BlocListener<FloidDroidGpsPointsFilteredBloc, FloidDroidGpsPointsFilteredState>(
          listenWhen: (previousFloidDroidGpsPointsFilteredState, currentFloidDroidGpsPointsFilteredState) {
            print('FloidDroidGpsPointsFilteredBloc listener ping');
            return true;
          },
          listener: (context, floidDroidGpsPointsFilteredState) {
            if (floidDroidGpsPointsFilteredState is FloidDroidGpsPointsFilteredLoaded) {
              print('Creating map lines');
              // Create and set map lines from these points:
              FloidDroidGpsPointsFilteredLoaded floidDroidGpsPointsFilteredLoaded = floidDroidGpsPointsFilteredState;
              List<FloidMapFloidDroidGpsLine> floidDroidGpsPointsFilteredLines = [];
              FloidGpsPoint? currentFloidGpsPoint;
              for (FloidGpsPoint floidGpsPoint in floidDroidGpsPointsFilteredLoaded.gpsPoints) {
                if (currentFloidGpsPoint != null) {
                  FloidMapFloidDroidGpsLine fFloidMapFloidDroidGpsLine = FloidMapFloidDroidGpsLine(
                    floidPathElevation: null,
                    startLat: currentFloidGpsPoint.latitude,
                    startLng: currentFloidGpsPoint.longitude,
                    endLat: floidGpsPoint.latitude,
                    endLng: floidGpsPoint.longitude,
                    type: FloidMapLine.MAP_LINE_TYPE_GPS,
                    lineId: 0,
                    altitudeStatus: FloidMapLine.ALTITUDE_STATUS_GOOD,
                    elevationStatus: FloidMapLine.ELEVATION_STATUS_GOOD,
                    startPin: null,
                    endPin: null,);
                  floidDroidGpsPointsFilteredLines.add(fFloidMapFloidDroidGpsLine);
                }
                currentFloidGpsPoint = floidGpsPoint;
              }
              // And then create the gpx file only from the tracks and send to the bloc:
              Gpx gpx = Gpx(name:'${floidDroidGpsPointsFilteredLoaded.floidId}', timestamp: '');
              // Add in the waypoints:
              floidDroidGpsPointsFilteredLoaded.gpsPoints.forEach((element) {
                gpx.addTrackPoint(latitude: element.latitude, longitude: element.longitude, elevation: element.altitude);
              });
              // Put the GPX file in to the bloc:
              BlocProvider.of<DroidTrackFilteredGpxFileBloc>(context).add(SetDroidTrackFilteredGpxFile(droidTrackFilteredGpxFile: gpx.render(), fileName: 'Droid-${floidDroidGpsPointsFilteredLoaded.floidId}.gpx'));
              // Set these points to the map line:
              BlocProvider.of<FloidMapLinesBloc<FloidMapFloidDroidGpsLine>>(context).add(SetFloidMapLines(floidMapLines: floidDroidGpsPointsFilteredLines));
            }
            // If our new state is empty or loading, we clear the gpx file if loaded:
            if (floidDroidGpsPointsFilteredState is FloidDroidGpsPointsFilteredEmpty || floidDroidGpsPointsFilteredState is FloidDroidGpsPointsFilteredLoading) {
              // Clear the gpx file if loaded:
              if(BlocProvider.of<DroidTrackFilteredGpxFileBloc>(context).state is DroidTrackFilteredGpxFileLoaded) {
                BlocProvider.of<DroidTrackFilteredGpxFileBloc>(context).add(ClearDroidTrackFilteredGpxFile());
              }
            }
          },
        ),
        // Floid Gps Points Filtered Bloc:
        BlocListener<FloidGpsPointsFilteredBloc, FloidGpsPointsFilteredState>(
          listenWhen: (previousFloidGpsPointsFilteredState, currentFloidGpsPointsFilteredState) {
            return true;
          },
          listener: (context, floidGpsPointsFilteredState) {
            if (floidGpsPointsFilteredState is FloidGpsPointsFilteredLoaded) {
              // Create and set map lines from these points:
              FloidGpsPointsFilteredLoaded floidGpsPointsFilteredLoaded = floidGpsPointsFilteredState;
              List<FloidMapFloidGpsLine> floidGpsPointsFilteredLines = [];
              FloidGpsPoint? currentFloidGpsPoint;
              for (FloidGpsPoint floidGpsPoint in floidGpsPointsFilteredLoaded.gpsPoints) {
                if (currentFloidGpsPoint != null) {
                  FloidMapFloidGpsLine floidMapFloidGpsLine = FloidMapFloidGpsLine(
                    floidPathElevation: null,
                    startLat: currentFloidGpsPoint.latitude,
                    startLng: currentFloidGpsPoint.longitude,
                    endLat: floidGpsPoint.latitude,
                    endLng: floidGpsPoint.longitude,
                    type: FloidMapLine.MAP_LINE_TYPE_GPS,
                    lineId: 0,
                    altitudeStatus: FloidMapLine.ALTITUDE_STATUS_GOOD,
                    elevationStatus: FloidMapLine.ELEVATION_STATUS_GOOD,
                    startPin: null,
                    endPin: null,);
                  floidGpsPointsFilteredLines.add(floidMapFloidGpsLine);
                }
                currentFloidGpsPoint = floidGpsPoint;
              }
              // And then create the gpx file only from the tracks and send to the bloc:
              Gpx gpx = Gpx(name:'${floidGpsPointsFilteredLoaded.floidId}', timestamp: '');
              // Add in the waypoints:
              floidGpsPointsFilteredLoaded.gpsPoints.forEach((element) {
                gpx.addTrackPoint(latitude: element.latitude, longitude: element.longitude, elevation: element.altitude);
              });
              // Put the GPX file in to the bloc:
              BlocProvider.of<FloidTrackFilteredGpxFileBloc>(context).add(SetFloidTrackFilteredGpxFile(floidTrackFilteredGpxFile: gpx.render(), fileName: 'Floid-${floidGpsPointsFilteredLoaded.floidId}.gpx'));
              // Set these points to the map line:
              BlocProvider.of<FloidMapLinesBloc<FloidMapFloidGpsLine>>(context).add(SetFloidMapLines(floidMapLines: floidGpsPointsFilteredLines));
            }
          },
        ),
        // Floid gps points listener
        BlocListener<FloidGpsPointsBloc, FloidGpsPointsState>(
          listenWhen: (previousFloidGpsPointsState, currentFloidGpsPointsState) {
            return true;
          },
          listener: (context, floidGpsPointsState) {
            if (floidGpsPointsState is FloidGpsPointsLoaded) {
              final FloidGpsPointsLoaded floidGpsPointsLoaded = floidGpsPointsState;
              // And then create the gpx file only from the tracks and send to the bloc:
              Gpx gpx = Gpx(name: floidGpsPointsLoaded.name, timestamp: '');
              // Add in the waypoints:
              floidGpsPointsLoaded.gpsPoints.forEach((element) {
                gpx.addTrackPoint(latitude: element.latitude, longitude: element.longitude, elevation: element.altitude);
              });
              // Put the GPX file in to the bloc:
              BlocProvider.of<FloidTrackGpxFileBloc>(context).add(SetFloidTrackGpxFile(floidTrackGpxFile: gpx.render(), fileName: 'Floid-${floidGpsPointsLoaded.name}.gpx'));
            }
            // If our new state is empty or loading, we clear the gpx file if loaded:
            if (floidGpsPointsState is FloidGpsPointsEmpty || floidGpsPointsState is FloidGpsPointsLoading) {
              // Clear the gpx file if loaded:
              if(BlocProvider.of<FloidTrackGpxFileBloc>(context).state is FloidTrackGpxFileLoaded) {
                BlocProvider.of<FloidTrackGpxFileBloc>(context).add(ClearFloidTrackGpxFile());
              }
            }
          },
        ),
        // Droid gps points listener:
        BlocListener<FloidDroidGpsPointsBloc, FloidDroidGpsPointsState>(
          listenWhen: (previousFloidDroidGpsPointsState, currentFloidDroidGpsPointsState) {
            return true;
          },
          listener: (context, floidDroidGpsPointsState) {
            if (floidDroidGpsPointsState is FloidDroidGpsPointsLoaded) {
              final FloidDroidGpsPointsLoaded floidDroidGpsPointsLoaded = floidDroidGpsPointsState;
              // And then create the gpx file only from the tracks and send to the bloc:
              Gpx gpx = Gpx(name:'${floidDroidGpsPointsLoaded.floidId}', timestamp: '');
              // Add in the waypoints:
              floidDroidGpsPointsLoaded.gpsPoints.forEach((element) {
                gpx.addTrackPoint(latitude: element.latitude, longitude: element.longitude, elevation: element.altitude);
              });
              // Put the GPX file in to the bloc:
              BlocProvider.of<FloidDroidTrackGpxFileBloc>(context).add(SetFloidDroidTrackGpxFile(floidDroidTrackGpxFile: gpx.render(), fileName: 'Droid-${floidDroidGpsPointsLoaded.floidId}.gpx'));
            }
            // If our new state is empty or loading, we clear the gpx file if loaded:
            if (floidDroidGpsPointsState is FloidDroidGpsPointsEmpty || floidDroidGpsPointsState is FloidDroidGpsPointsLoading) {
              // Clear the gpx file if loaded:
              if(BlocProvider.of<FloidDroidTrackGpxFileBloc>(context).state is FloidDroidTrackGpxFileLoaded) {
                BlocProvider.of<FloidDroidTrackGpxFileBloc>(context).add(ClearFloidDroidTrackGpxFile());
              }
            }
          },
        ),
        // Droid Status listener:
        BlocListener<FloidDroidStatusBloc, FloidDroidStatusState>(
          listenWhen: (previousFloidDroidStatusState, currentFloidDroidStatusState) {
            return true;
          },
          listener: (context, floidDroidStatusState) {
            if (floidDroidStatusState is FloidDroidStatusLoaded) {
              FloidDroidStatus floidDroidStatus = floidDroidStatusState.floidDroidStatus;
              // Set the home position:
              if (floidDroidStatus.homePositionAcquired) {
                BlocProvider.of<FloidMapMarkerBloc<FloidAcquiredHomeMarker>>(context).add(
                    SetFloidMapMarker(floidMapMarker: FloidAcquiredHomeMarker(floidDroidStatus: floidDroidStatus)));
              }
              // Send the location to be accumulated by the floid gps points:
              BlocProvider.of<FloidDroidGpsPointsFilteredBloc>(context).add(
                  AddFloidDroidGpsPointFiltered(floidId: floidDroidStatusState.floidId,
                      floidUuid: floidDroidStatus.floidUuid,
                      floidGpsPoint: FloidGpsPoint(
                      id: 0,
                      floidId: floidDroidStatusState.floidId,
                      floidUuid: floidDroidStatusState.floidUuid,
                      latitude: floidDroidStatus.gpsLatitude,
                      longitude: floidDroidStatus.gpsLongitude,
                      altitude: floidDroidStatus.gpsAltitude,
                      statusTime: floidDroidStatus.statusTime),
                      timestamp: ''));
            }
          },
        ),
        // Droid Status listener:
        BlocListener<FloidStatusBloc, FloidStatusState>(
          listenWhen: (previousFloidStatusState, currentFloidStatusState) {
            return true;
          },
          listener: (context, floidStatusState) {
            if (floidStatusState is FloidStatusLoaded) {
              // Set the floid map marker:
              BlocProvider.of<FloidMapMarkerBloc<FloidStatusMarker>>(context).add(
                  SetFloidMapMarker(floidMapMarker: FloidStatusMarker(floidStatus: floidStatusState.floidStatus)));
              // Send the location to be accumulated by the droid gps points:
              BlocProvider.of<FloidGpsPointsFilteredBloc>(context).add(
                  AddFloidGpsPointFiltered(floidId: floidStatusState.floidId,
                      floidUuid: floidStatusState.floidUuid,
                      floidGpsPoint: FloidGpsPoint(
                      id: 0,
                      floidId: floidStatusState.floidId,
                      floidUuid: floidStatusState.floidUuid,
                      latitude: floidStatusState.floidStatus.jyf,
                      longitude: floidStatusState.floidStatus.jxf,
                      altitude: floidStatusState.floidStatus.jmf,
                      statusTime: floidStatusState.floidStatus.std,),
                      timestamp: ''),
                     );
            }
          },
        ),
        // Floid Pin Markers listener:
        BlocListener<FloidMapMarkersBloc<FloidPinMarker>, FloidMapMarkersState>(
          listenWhen: (previousFloidPinMarkersState, currentFloidPinMarkersState) {
            if (currentFloidPinMarkersState is FloidMapMarkersLoaded && previousFloidPinMarkersState is FloidMapMarkersEmpty) {
              return true; // New pins
            }
            if (currentFloidPinMarkersState is FloidMapMarkersLoaded && previousFloidPinMarkersState is FloidMapMarkerError) {
              return true; // New pins
            }
            return false; // Other cases all false
          },
          listener: (context, floidPinMarkersState) {
            if (floidPinMarkersState is FloidMapMarkersLoaded) {
              if (floidPinMarkersState.zoomToMarkers) {
                BlocProvider.of<FloidAppStateBloc>(context).add(ZoomToPinsAppStateEvent());
              }
            }
          },
        ),
        // Droid Message Listener - triggers on new droid message:
        BlocListener<FloidDroidMessageBloc, FloidDroidMessageState>(
          listenWhen: (previousFloidDroidMessageState, currentFloidDroidMessageState) {
            return true;
          },
          listener: (context, floidDroidMessageState) {
            // Add this message to the list of messages:
            if (floidDroidMessageState is FloidDroidMessageLoaded) {
              BlocProvider.of<FloidMessagesBloc>(context).add(AddFloidMessage(message: floidDroidMessageState.floidDroidMessage.message, noToast: true));
            }
          },
        ),
        // New Floid Id Listener - triggers on new droid message:
        BlocListener<FloidNewFloidIdBloc, FloidNewFloidIdState>(
          listenWhen: (previousFloidNewFloidIdState, currentFloidNewFloidIdState) {
            return true;
          },
          listener: (context, floidNewFloidIdState) {
            // Add this message to the list of messages:
            if (floidNewFloidIdState is FloidNewFloidIdLoaded) {
              BlocProvider.of<FloidMessagesBloc>(context).add(AddFloidMessage(message: "Id: " + floidNewFloidIdState.floidIdAndState.floidId.toString()));
              // Set this as the saved floid id:
              BlocProvider.of<FloidAppStateBloc>(context).add(SetSavedFloidIdAppStateEvent(savedFloidId: floidNewFloidIdState.floidIdAndState.floidId));
              // What I want to do here is switch to the new floid id, but first add it to the list of floid ids:
              BlocProvider.of<FloidIdAndStateListBloc>(context).add(AddNewFloidIdAndState(floidIdAndState: floidNewFloidIdState.floidIdAndState));
              // Switch to the new floid id:
              BlocProvider.of<FloidIdBloc>(context).add(SetFloidIdAndState(floidIdAndState: floidNewFloidIdState.floidIdAndState));
            }
          },
        ),
        // New Floid Uuid Listener:
        BlocListener<FloidNewFloidUuidBloc, FloidNewFloidUuidState>(
          listenWhen: (previousFloidNewFloidUuidState, currentFloidNewFloidUuidState) {
            return true;
          },
          listener: (context, floidNewFloidUuidState) {
            // Add this message to the list of messages:
            if (floidNewFloidUuidState is FloidNewFloidUuidLoaded) {
              // BlocProvider.of<FloidMessagesBloc>(context).add(AddFloidMessage(message: floidNewFloidUuidState.floidUuidAndState.floidUuid.toString()));
              // What I want to do here is switch to the new floid uuid, so add it to the list of floid uuids and the BlocListener<FloidUuidAndStateListBloc, FloidUuidAndStateListState> below will change to the new UUID:
              BlocProvider.of<FloidUuidAndStateListBloc>(context).add(
                  AddNewFloidUuidAndState(floidIdAndState: floidNewFloidUuidState.floidIdAndState, floidUuidAndState: floidNewFloidUuidState.floidUuidAndState));
            }
          },
        ),
        // Floid Uuid State List Listener:
        BlocListener<FloidUuidAndStateListBloc, FloidUuidAndStateListState>(
          listenWhen: (previousFloidUuidAndStateListState, currentFloidUuidAndStateListState) {
            return true;
          },
          listener: (context, floidUuidAndStateListState) {
            // Did we get a new list of UUIDs?
            if (floidUuidAndStateListState is FloidUuidAndStateListLoaded) {
              if (floidUuidAndStateListState.floidUuidAndStateList.isNotEmpty) {
                FloidUuidAndState floidUuidAndState = floidUuidAndStateListState.floidUuidAndStateList.first;
                // Switch to the new floid uuid:
                BlocProvider.of<FloidIdBloc>(context).add(SetFloidUuidAndState(floidIdAndState: floidUuidAndStateListState.floidIdAndState, floidUuidAndState: floidUuidAndState));
              }
            }
          },
        ),
        // Mission and Instruction Status Listener - takes messages and sends them on to the messages bloc for display:
        BlocListener<FloidDroidMissionInstructionStatusMessageBloc, FloidDroidMissionInstructionStatusMessageState>(
          listenWhen: (previousFloidDroidMissionInstructionStatusMessageBloc, currentFloidDroidMissionInstructionStatusMessageBloc) {
            return currentFloidDroidMissionInstructionStatusMessageBloc is FloidDroidMissionInstructionStatusMessageLoaded;
          },
          listener: (context, floidDroidMissionInstructionStatusMessageState) {
            if (floidDroidMissionInstructionStatusMessageState is FloidDroidMissionInstructionStatusMessageLoaded) {
              // Set the floid uuid and state list for the chosen floid id:
              List<String> messages = floidDroidMissionInstructionStatusMessageState.floidDroidMissionInstructionStatus.messages;
              messages.forEach((message) {
                BlocProvider.of<FloidMessagesBloc>(context).add(
                    AddFloidMessage(message: message, noToast: true));
              });
            }
          },
        ),
      ],
      child: child,
    );
  }
}
