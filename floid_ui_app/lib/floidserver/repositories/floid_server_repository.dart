/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:async';
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/floidserver/listener/floid_bloc_listeners.dart';
import 'package:stomp/stomp.dart';

class FloidServerRepository {
  final FloidServerApiClient floidServerApiClient;

  FloidServerRepository({required this.floidServerApiClient});

  Future<String> authenticate(String username, String password) async {
    return await floidServerApiClient.authenticate(username, password);
  }

  /// Fetch the floid user:
  Future<String> fetchFloidUser() async {
    return await floidServerApiClient.fetchFloidUser();
  }

  /// Fetch the floid server status:
  Future<FloidServerStatus> fetchFloidServerStatus() async {
    return await floidServerApiClient.fetchFloidServerStatus();
  }

  /// Start server:
  Future<bool> startServer() async {
    return await floidServerApiClient.startServer();
  }

  /// Stop Server:
  Future<bool> stopServer() async {
    return await floidServerApiClient.stopServer();
  }

  /// Fetch the floid list:
  Future<List<int>> fetchFloidList() async {
    return await floidServerApiClient.fetchFloidList();
  }

  /// Fetch the floid id and state list:
  Future<List<FloidIdAndState>> fetchFloidIdAndStateList() async {
    return await floidServerApiClient.fetchFloidIdAndStateList();
  }

  /// Fetch the floid uuid and state list:
  Future<List<FloidUuidAndState>> fetchFloidUuidAndStateList(int floidId) async {
    return await floidServerApiClient.fetchFloidUuidAndStateList(floidId);
  }

  /// Fetch the mission list:
  Future<List<DroidMissionList>> fetchMissionList() async {
    return await floidServerApiClient.fetchMissionList();
  }

  /// Fetch a mission:
  Future<DroidMission> fetchMission(int missionId) async {
    return await floidServerApiClient.fetchMission(missionId);
  }

  /// Fetch the pin set list:
  Future<List<DroidPinSetList>> fetchPinSetList() async {
    return await floidServerApiClient.fetchPinSetList();
  }

  /// Fetch a pin set:
  Future<FloidPinSetAlt> fetchPinSet(int pinSetId) async {
    return await floidServerApiClient.fetchPinSet(pinSetId);
  }

  /// Fetch gps points:
  Future<List<FloidGpsPoint>> fetchGpsPoints(int floidId, String floidUuid) async {
    return await floidServerApiClient.fetchGpsPoints(floidId, floidUuid);
  }

  /// Fetch last Floid Droid Status:
  Future<FloidDroidStatus> fetchLastFloidDroidStatus(int floidId, String floidUuid) async {
    return await floidServerApiClient.fetchLastFloidDroidStatus(floidId, floidUuid);
  }

  /// Fetch last Floid Status:
  Future<FloidStatus> fetchLastFloidStatus(int floidId, String floidUuid) async {
      return await floidServerApiClient.fetchLastFloidStatus(
          floidId, floidUuid);
  }

  /// Fetch last Floid Model Status:
  Future<FloidModelStatus> fetchLastFloidModelStatus(int floidId, String floidUuid) async {
    return await floidServerApiClient.fetchLastFloidModelStatus(floidId, floidUuid);
  }

  /// Fetch last Floid Model Parameters:
  Future<FloidModelParameters> fetchLastFloidModelParameters(int floidId, String floidUuid) async {
    return await floidServerApiClient.fetchLastFloidModelParameters(floidId, floidUuid);
  }

  /// Fetch Last Floid Droid Photo:
  Future<FloidDroidPhoto> fetchLastFloidDroidPhoto(int floidId, String floidUuid) async {
    return await floidServerApiClient.fetchLastFloidDroidPhoto(floidId, floidUuid);
  }

  /// Fetch Last Floid Droid Video Frame:
  Future<FloidDroidVideoFrame> fetchLastFloidDroidVideoFrame(int floidId, String floidUuid) async {
    return await floidServerApiClient.fetchLastFloidDroidVideoFrame(floidId, floidUuid);
  }

  /// Fetch gps points filtered:
  Future<List<FloidGpsPoint>> fetchGpsPointsFiltered(
      int floidId, String floidUuid, int maxPoints, int minTime, double minDistance, double maxKink, double maxKinkAlpha) async {
    return await floidServerApiClient.fetchGpsPointsFiltered(floidId, floidUuid, maxPoints, minTime, minDistance, maxKink, maxKinkAlpha);
  }

  /// Fetch droid gps points:
  Future<List<FloidGpsPoint>> fetchDroidGpsPoints(int floidId, String floidUuid) async {
    return await floidServerApiClient.fetchDroidGpsPoints(floidId, floidUuid);
  }

  /// Fetch gps points filtered:
  Future<List<FloidGpsPoint>> fetchDroidGpsPointsFiltered(
      int floidId, String floidUuid, int maxPoints, int minTime, double minDistance, double maxKink, double maxKinkAlpha) async {
    return await floidServerApiClient.fetchDroidGpsPointsFiltered(floidId, floidUuid, maxPoints, minTime, minDistance, maxKink, maxKinkAlpha);
  }

  /// Execute a mission
  Future<FloidServerCommandResult> executeMission(int floidId, int missionId, int pinSetId) async {
    return await floidServerApiClient.executeMission(floidId, missionId, pinSetId);
  }

  /// Create a new mission
  Future<int> createNewMission(String newMissionName) async {
    return await floidServerApiClient.createNewMission(newMissionName);
  }

  /// Delete a mission
  Future<bool> deleteMission(int missionId) async {
    return await floidServerApiClient.deleteMission(missionId);
  }

  /// Duplicate a mission
  Future<int> duplicateMission(int missionId, String newMissionName) async {
    return await floidServerApiClient.duplicateMission(missionId, newMissionName);
  }

  /// Save a mission
  Future<int> saveMission(String mission) async {
    return await floidServerApiClient.saveMission(mission);
  }

  /// Import a mission
  Future<int> importMission(String mission) async {
    return await floidServerApiClient.importMission(mission);
  }

  /// Create a pin set
  Future<int> createPinSet(String newPinSetName) async {
    return await floidServerApiClient.createPinSet(newPinSetName);
  }

  /// Delete a pin set
  Future<bool> deletePinSet(int pinSetId) async {
    return await floidServerApiClient.deletePinSet(pinSetId);
  }

  /// Duplicate a pin set
  Future<int> duplicatePinSet(int originalPinSetId, String newPinSetName) async {
    return await floidServerApiClient.duplicatePinSet(originalPinSetId, newPinSetName);
  }

  /// Save a pin set
  Future<int> savePinSet(String pinSetAlt) async {
    return await floidServerApiClient.savePinSet(pinSetAlt);
  }

  /// Import a pin set
  Future<int> importPinSet(String pinSetAlt) async {
    return await floidServerApiClient.importPinSet(pinSetAlt);
  }

  /// Delete a floid iid:
  Future<bool> deleteFloidId(int floidId) async {
    return await floidServerApiClient.deleteFloidId(floidId);
  }

  /* *************************
     These take no parameters:
     ************************* */

  /// Subscribe to the Floid Server Status STOMP ports
  Future<StompClient> subscribeFloidServerStatus(void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidServerStatus(callback);
  }

  /// Subscribe to the Floid New Floid Id STOMP ports
  Future<StompClient> subscribeFloidNewFloidId(void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidNewFloidId(callback);
  }

  /* **********************
     These take a floid id:
     ********************** */

  /// Subscribe to the Floid New Floid Uuid STOMP ports
  Future<StompClient> subscribeFloidNewFloidUuid(int floidId, void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidNewFloidUuid(floidId, callback);
  }

  /* ***************************************
     These take a floid id and a floid uuid:
     *************************************** */

  /// Subscribe to the Floid Model Status STOMP ports
  Future<StompClient> subscribeFloidModelStatus(int floidId, String floidUuid, void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidModelStatus(floidId, floidUuid, callback);
  }

  /// Subscribe to the Floid Model Parameters STOMP ports
  Future<StompClient> subscribeFloidModelParameters(int floidId, String floidUuid, void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidModelParameters(floidId, floidUuid, callback);
  }

  /// Subscribe to the Floid Status STOMP ports
  Future<StompClient> subscribeFloidStatus(int floidId, String floidUuid, void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidStatus(floidId, floidUuid, callback);
  }

  /// Subscribe to the Floid Droid Message STOMP ports
  Future<StompClient> subscribeFloidDroidMessage(int floidId, String floidUuid, void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidDroidMessage(floidId, floidUuid, callback);
  }

  /// Subscribe to the Floid Droid Status STOMP ports
  Future<StompClient> subscribeFloidDroidStatus(int floidId, String floidUuid, void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidDroidStatus(floidId, floidUuid, callback);
  }

  /// Subscribe to the Floid Droid Photo STOMP ports
  Future<StompClient> subscribeFloidDroidPhoto(int floidId, String floidUuid, void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidDroidPhoto(floidId, floidUuid, callback);
  }

  /// Subscribe to the Floid Droid Video Frame STOMP ports
  Future<StompClient> subscribeFloidDroidVideoFrame(int floidId, String floidUuid, void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidDroidVideoFrame(floidId, floidUuid, callback);
  }

  /// Subscribe to the Floid Droid Mission Instruction Status STOMP ports
  Future<StompClient> subscribeFloidDroidMissionInstructionStatus(int floidId, String floidUuid, void callback(Map<String, String> headers, String message)) {
    return floidServerApiClient.subscribeFloidDroidMissionInstructionStatus(floidId, floidUuid, callback);
  }

  // Commands:
  /// Perform Command
  Future<FloidServerCommandResult> performCommand(int floidId, DroidInstruction droidInstruction, FloidPinSetAlt floidPinSetAlt) async {
    if (droidInstruction is AcquireHomePositionCommand) {
      return await performAcquireHomePositionCommand(floidId, droidInstruction.requiredXYDistance, droidInstruction.requiredAltitudeDistance,
          droidInstruction.requiredHeadingDistance, droidInstruction.requiredGPSGoodCount, droidInstruction.requiredAltimeterGoodCount);
    } else if (droidInstruction is PayloadCommand) {
      return await performPayloadCommand(floidId, droidInstruction.bay);
    } else if (droidInstruction is DebugCommand) {
      return await performDebugCommand(floidId, droidInstruction.deviceIndex, droidInstruction.debugValue);
    } else if (droidInstruction is DelayCommand) {
      return await performDelayCommand(floidId, droidInstruction.delayTime);
    } else if (droidInstruction is RetrieveLogsCommand) {
      return await performRetrieveLogsCommand(floidId, droidInstruction.logLimit);
    } else if (droidInstruction is DeployParachuteCommand) {
      return await performDeployParachuteCommand(floidId);
    } else if (droidInstruction is DesignateCommand) {
      if (droidInstruction.usePin) {
        FloidPinAlt? designatePin = FloidBlocListeners.findPinByName(floidPinSetAlt, droidInstruction.pinName);
        if (designatePin != null) {
          return await performDesignateCommand(
              floidId, designatePin.pinLat, designatePin.pinLng, droidInstruction.usePinHeight ? designatePin.pinHeight : droidInstruction.z);
        } else {
          print('Could not find the designate pin: ' + droidInstruction.pinName);
        }
      } else {
        return await performDesignateCommand(floidId, droidInstruction.y, droidInstruction.x, droidInstruction.z);
      }
    } else if (droidInstruction is FlyToCommand) {
      String flyToPinName = droidInstruction.pinName;
      FloidPinAlt? flyToPin;

      if (!droidInstruction.flyToHome) {
        flyToPin =
            FloidBlocListeners.findPinByName(floidPinSetAlt, flyToPinName);
        if (flyToPin != null) {
          droidInstruction.x = flyToPin.pinLng;
          droidInstruction.y = flyToPin.pinLat;
        }
      }
      return await performFlyToCommand(floidId, droidInstruction.y, droidInstruction.x, droidInstruction.flyToHome);
    } else if (droidInstruction is HeightToCommand) {
      return await performHeightToCommand(floidId, droidInstruction.absolute, droidInstruction.z);
    } else if (droidInstruction is HoverCommand) {
      return await performHoverCommand(floidId, droidInstruction.hoverTime);
    } else if (droidInstruction is LandCommand) {
      return await performLandCommand(floidId, droidInstruction.z);
    } else if (droidInstruction is LiftOffCommand) {
      return await performLiftOffCommand(floidId, droidInstruction.absolute, droidInstruction.z.floor());
    } else if (droidInstruction is NullCommand) {
      return await performNullCommand(floidId);
    } else if (droidInstruction is PanTiltCommand) {
      return await performPanTiltCommand(
          floidId, droidInstruction.useAngle, droidInstruction.y, droidInstruction.x, droidInstruction.z, droidInstruction.pan, droidInstruction.tilt);
    } else if (droidInstruction is SetParametersCommand) {
      return await performSetParametersCommand(floidId, droidInstruction.droidParameters, droidInstruction.resetDroidParameters);
    } else if (droidInstruction is ShutDownHelisCommand) {
      return await performShutDownHelisCommand(floidId);
    } else if (droidInstruction is SpeakerCommand) {
      return await performSpeakerCommand(floidId, droidInstruction.alert, droidInstruction.mode, droidInstruction.tts, droidInstruction.pitch,
          droidInstruction.rate, droidInstruction.fileName, droidInstruction.volume, droidInstruction.speakerRepeat, droidInstruction.delay);
    } else if (droidInstruction is SpeakerOnCommand) {
      return await performSpeakerOnCommand(floidId, droidInstruction.alert, droidInstruction.mode, droidInstruction.tts, droidInstruction.pitch,
          droidInstruction.rate, droidInstruction.fileName, droidInstruction.volume, droidInstruction.speakerRepeat, droidInstruction.delay);
    } else if (droidInstruction is SpeakerOffCommand) {
      return await performSpeakerOffCommand(floidId);
    } else if (droidInstruction is StartUpHelisCommand) {
      return await performStartUpHelisCommand(floidId);
    } else if (droidInstruction is StartVideoCommand) {
      return await performStartVideoCommand(floidId);
    } else if (droidInstruction is StopDesignatingCommand) {
      return await performStopDesignatingCommand(floidId);
    } else if (droidInstruction is StopVideoCommand) {
      return await performStopVideoCommand(floidId);
    } else if (droidInstruction is PhotoStrobeCommand) {
      return await performPhotoStrobeCommand(floidId, droidInstruction.delay);
    } else if (droidInstruction is StopPhotoStrobeCommand) {
      return await performStopPhotoStrobeCommand(floidId);
    } else if (droidInstruction is TakePhotoCommand) {
      return await performTakePhotoCommand(floidId);
    } else if (droidInstruction is TurnToCommand) {
      return await performTurnToCommand(floidId, droidInstruction.followMode, droidInstruction.heading);
    }
    print('performCommand: Bad Instruction');
    return FloidServerCommandResult()
      ..floidId = floidId
      ..droidInstruction = droidInstruction
      ..result = -15 // BAD_INSTRUCTION_ERROR_CODE
      ..success = false;
  }

  /// Perform Acquire Home Position Command
  Future<FloidServerCommandResult> performAcquireHomePositionCommand(int floidId, double requiredXYDistance, double requiredAltitudeDistance,
      double requiredHeadingDistance, int requiredGPSGoodCount, int requiredAltimeterGoodCount) async {
    return await floidServerApiClient.performAcquireHomePositionCommand(
        floidId, requiredXYDistance, requiredAltitudeDistance, requiredHeadingDistance, requiredGPSGoodCount, requiredAltimeterGoodCount);
  }

  /// Perform Payload Command
  Future<FloidServerCommandResult> performPayloadCommand(int floidId, int bay) async {
    return await floidServerApiClient.performPayloadCommand(floidId, bay);
  }

  /// Perform Debug Command
  Future<FloidServerCommandResult> performDebugCommand(int floidId, int deviceIndex, int deviceOn) async {
    return await floidServerApiClient.performDebugCommand(floidId, deviceIndex, deviceOn);
  }

  /// Perform Delay Command
  Future<FloidServerCommandResult> performDelayCommand(int floidId, int delayTime) async {
    return await floidServerApiClient.performDelayCommand(floidId, delayTime);
  }

  /// Perform Retrieve Logs
  Future<FloidServerCommandResult> performRetrieveLogsCommand(int floidId, int logLimit) async {
    return await floidServerApiClient.performRetrieveLogsCommand(floidId, logLimit);
  }

  /// Perform Deploy Parachute Command
  Future<FloidServerCommandResult> performDeployParachuteCommand(int floidId) async {
    return await floidServerApiClient.performDeployParachuteCommand(floidId);
  }

  /// Perform Designate Command
  Future<FloidServerCommandResult> performDesignateCommand(int floidId, double latitude, double longitude, double height) async {
    return await floidServerApiClient.performDesignateCommand(floidId, latitude, longitude, height);
  }

  /// Perform Fly To Command
  Future<FloidServerCommandResult> performFlyToCommand(int floidId, double latitude, double longitude, bool flyToHome) async {
    return await floidServerApiClient.performFlyToCommand(floidId, latitude, longitude, flyToHome);
  }

  /// Perform Height To Command
  Future<FloidServerCommandResult> performHeightToCommand(int floidId, bool absolute, double height) async {
    return await floidServerApiClient.performHeightToCommand(floidId, absolute, height);
  }

  /// Perform Hover Command
  Future<FloidServerCommandResult> performHoverCommand(int floidId, int time) async {
    return await floidServerApiClient.performHoverCommand(floidId, time);
  }

  /// Perform Land Command
  Future<FloidServerCommandResult> performLandCommand(int floidId, double z) async {
    return await floidServerApiClient.performLandCommand(floidId, z);
  }

  /// Perform Lift Off Command
  Future<FloidServerCommandResult> performLiftOffCommand(int floidId, bool absolute, int z) async {
    return await floidServerApiClient.performLiftOffCommand(floidId, absolute, z);
  }

  /// Perform Null Command
  Future<FloidServerCommandResult> performNullCommand(int floidId) async {
    return await floidServerApiClient.performNullCommand(floidId);
  }

  /// Perform Pan Tilt Command
  Future<FloidServerCommandResult> performPanTiltCommand(int floidId, bool angleMode, double latitude, double longitude, double height, double pan, double tilt) async {
    return await floidServerApiClient.performPanTiltCommand(floidId, angleMode, latitude, longitude, height, pan, tilt);
  }

  /// Perform Set Parameters Command
  Future<FloidServerCommandResult> performSetParametersCommand(int floidId, String droidParameters, bool resetDroidParameters) async {
    return await floidServerApiClient.performSetParametersCommand(floidId, droidParameters, resetDroidParameters);
  }

  /// Perform Shut Down Helis Command
  Future<FloidServerCommandResult> performShutDownHelisCommand(int floidId) async {
    return await floidServerApiClient.performShutDownHelisCommand(floidId);
  }

  /// Perform Speaker Command
  Future<FloidServerCommandResult> performSpeakerCommand(
      int floidId, bool alert, String mode, String tts, double pitch, double rate, String fileName, double volume, int speakerRepeat, int delay) async {
    return await floidServerApiClient.performSpeakerCommand(floidId, alert, mode, tts, pitch, rate, fileName, volume, speakerRepeat, delay);
  }

  /// Perform Speaker On Command
  Future<FloidServerCommandResult> performSpeakerOnCommand(
      int floidId, bool alert, String mode, String tts, double pitch, double rate, String fileName, double volume, int speakerRepeat, int delay) async {
    return await floidServerApiClient.performSpeakerOnCommand(floidId, alert, mode, tts, pitch, rate, fileName, volume, speakerRepeat, delay);
  }

  /// Perform Speaker Off Command
  Future<FloidServerCommandResult> performSpeakerOffCommand(int floidId) async {
    return await floidServerApiClient.performSpeakerOffCommand(floidId);
  }

  /// Perform Start Up Helis Command
  Future<FloidServerCommandResult> performStartUpHelisCommand(int floidId) async {
    return await floidServerApiClient.performStartUpHelisCommand(floidId);
  }

  /// Perform Start Video Command
  Future<FloidServerCommandResult> performStartVideoCommand(int floidId) async {
    return await floidServerApiClient.performStartVideoCommand(floidId);
  }

  /// Perform Stop Designating Command
  Future<FloidServerCommandResult> performStopDesignatingCommand(int floidId) async {
    return await floidServerApiClient.performStopDesignatingCommand(floidId);
  }

  /// Perform Stop Video Command
  Future<FloidServerCommandResult> performStopVideoCommand(int floidId) async {
    return await floidServerApiClient.performStopVideoCommand(floidId);
  }

  /// Perform Photo Strobe Command
  Future<FloidServerCommandResult> performPhotoStrobeCommand(int floidId, int delay) async {
    return await floidServerApiClient.performPhotoStrobeCommand(floidId, delay);
  }

  /// Perform Stop Photo Strobe Command
  Future<FloidServerCommandResult> performStopPhotoStrobeCommand(int floidId) async {
    return await floidServerApiClient.performStopPhotoStrobeCommand(floidId);
  }

  /// Perform Take Photo Command
  Future<FloidServerCommandResult> performTakePhotoCommand(int floidId) async {
    return await floidServerApiClient.performTakePhotoCommand(floidId);
  }

  /// Perform Turn To Command
  Future<FloidServerCommandResult> performTurnToCommand(int floidId, bool followMode, double heading) async {
    return await floidServerApiClient.performTurnToCommand(floidId, followMode, heading);
  }
}
