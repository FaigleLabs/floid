/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:stomp/stomp.dart';
import 'stomp_client_proxy.dart';
import 'package:floid_ui_app/floidserver/trust/floid_server_trust.dart';

class FloidServerApiClient {
  static const int RANDOM_INT_MAX = 4294967295;
  // Paths:
  static const String URL_CREATE_DOWNLOAD_TOKEN = 'createDownloadToken';
  static const String URL_USER = 'user';
  static const String URL_FLOID_LIST = 'floidList';
  static const String URL_FLOID_SERVER_STATUS = 'floidServerStatus';
  static const String URL_FLOID_ID_AND_STATE_LIST = 'floidIdAndStateList';
  static const String URL_FLOID_UUID_AND_STATE_LIST = 'floidUuidAndStateList';
  static const String URL_MISSION_LIST = 'missionList';
  static const String URL_MISSION_JSON = 'missionJSON';
  static const String URL_PIN_SET_LIST = 'pinSetList';
  static const String URL_PIN_SET = 'pinSet';
  static const String URL_GPS_POINTS = 'gpsPoints';
  static const String URL_LAST_FLOID_DROID_STATUS = 'lastFloidDroidStatus';
  static const String URL_LAST_FLOID_STATUS = 'lastFloidStatus';
  static const String URL_LAST_FLOID_MODEL_STATUS = 'lastFloidModelStatus';
  static const String URL_LAST_FLOID_MODEL_PARAMETERS = 'lastFloidModelParameters';
  static const String URL_LAST_FLOID_DROID_PHOTO = 'lastFloidDroidPhoto';
  static const String URL_LAST_FLOID_DROID_VIDEO_FRAME = 'lastFloidDroidVideoFrame';
  static const String URL_GPS_POINTS_FILTERED = 'gpsPointsFiltered';
  static const String URL_DROID_GPS_POINTS = 'droidGpsPoints';
  static const String URL_DROID_GPS_POINTS_FILTERED = 'droidGpsPointsFiltered';
  static const String URL_PERFORM_STOP_VIDEO_COMMAND = 'performStopVideoCommand';
  static const String URL_PERFORM_PHOTO_STROBE_COMMAND = 'performPhotoStrobeCommand';
  static const String URL_PERFORM_STOP_PHOTO_STROBE_COMMAND = 'performStopPhotoStrobeCommand';
  static const String URL_PERFORM_TAKE_PHOTO_COMMAND = 'performTakePhotoCommand';
  static const String URL_PERFORM_PAYLOAD_COMMAND = 'performPayloadCommand';
  static const String URL_EXECUTE_MISSION = 'executeMission';
  static const String URL_CREATE_NEW_MISSION = 'createNewMission';
  static const String URL_DELETE_MISSION = 'deleteMission';
  static const String URL_DUPLICATE_MISSION = 'duplicateMission';
  static const String URL_SAVE_MISSION_JSON = 'saveMissionJSON';
  static const String URL_IMPORT_MISSION = 'importMission';
  static const String URL_CREATE_PIN_SET = 'createPinSet';
  static const String URL_DELETE_PIN_SET = 'deletePinSet';
  static const String URL_DUPLICATE_PIN_SET = 'duplicatePinSet';
  static const String URL_SAVE_PIN_SET = 'savePinSet';
  static const String URL_IMPORT_PIN_SET = 'importPinSet';
  static const String URL_DELETE_FLOID_ID = 'deleteFloidId';
  static const String URL_PERFORM_ACQUIRE_HOME_POSITION_COMMAND = 'performAcquireHomePositionCommand';
  static const String URL_PERFORM_DEBUG_COMMAND = 'performDebugCommand';
  static const String URL_PERFORM_DELAY_COMMAND = 'performDelayCommand';
  static const String URL_PERFORM_DEPLOY_PARACHUTE_COMMAND = 'performDeployParachuteCommand';
  static const String URL_PERFORM_STOP_DESIGNATING_COMMAND = 'performStopDesignatingCommand';
  static const String URL_PERFORM_START_VIDEO_COMMAND = 'performStartVideoCommand';
  static const String URL_PERFORM_START_UP_HELIS_COMMAND = 'performStartUpHelisCommand';
  static const String URL_PERFORM_TURN_TO_COMMAND = 'performTurnToCommand';
  static const String URL_PERFORM_SPEAKER_OFF_COMMAND = 'performSpeakerOffCommand';
  static const String URL_PERFORM_FLY_TO_COMMAND = 'performFlyToCommand';
  static const String URL_PERFORM_DESIGNATE_COMMAND = 'performDesignateCommand';
  static const String URL_PERFORM_HEIGHT_TO_COMMAND = 'performHeightToCommand';
  static const String URL_PERFORM_HOVER_COMMAND = 'performHoverCommand';
  static const String URL_PERFORM_LAND_COMMAND = 'performLandCommand';
  static const String URL_PERFORM_LIFT_OFF_COMMAND = 'performLiftOffCommand';
  static const String URL_PERFORM_NULL_COMMAND = 'performNullCommand';
  static const String URL_PERFORM_PAN_TILT_COMMAND = 'performPanTiltCommand';
  static const String URL_PERFORM_RETRIEVE_LOGS_COMMAND = 'performRetrieveLogsCommand';
  static const String URL_PERFORM_SET_PARAMETERS_COMMAND = 'performSetParametersCommand';
  static const String URL_PERFORM_SHUT_DOWN_HELIS_COMMAND = 'performShutDownHelisCommand';
  static const String URL_PERFORM_SPEAKER_COMMAND = 'performSpeakerCommand';
  static const String URL_PERFORM_SPEAKER_ON_COMMAND = 'performSpeakerOnCommand';
  // Parameters:
  static const String PARAMETER_FLOID_ID = 'floidId';
  static const String PARAMETER_FLOID_UUID = 'floidUuid';
  static const String PARAMETER_PIN_SET_ID = 'pinSetId';
  static const String PARAMETER_FLY_TO_HOME = 'flyToHome';
  static const String PARAMETER_MAX_POINTS = 'maxPoints';
  static const String PARAMETER_MIN_TIME = 'minTime';
  static const String PARAMETER_MIN_DISTANCE = 'minDistance';
  static const String PARAMETER_MAX_KINK = 'maxKink';
  static const String PARAMETER_MAX_KINK_ALPHA = 'maxKinkAlpha';
  static const String PARAMETER_FILE_ID = 'fileId';
  static const String PARAMETER_FOLLOW_MODE = 'followMode';
  static const String PARAMETER_HEADING = 'heading';
  static const String PARAMETER_DELAY = 'delay';
  static const String PARAMETER_MISSION_ID = 'missionId';
  static const String PARAMETER_ORIGINAL_MISSION_ID = 'originalMissionId';
  static const String PARAMETER_NEW_MISSION_NAME = 'newMissionName';
  static const String PARAMETER_MISSION = 'mission';
  static const String PARAMETER_ORIGINAL_PIN_SET_ID = 'originalPinSetId';
  static const String PARAMETER_NEW_PIN_SET_NAME = 'newPinSetName';
  static const String PARAMETER_PIN_SET_ALT = 'pinSetAlt';
  static const String PARAMETER_REQUIRED_XY_DISTANCE = 'requiredXYDistance';
  static const String PARAMETER_REQUIRED_ALTITUDE_DISTANCE = 'requiredAltitudeDistance';
  static const String PARAMETER_REQUIRED_HEADING_DISTANCE = 'requiredHeadingDistance';
  static const String PARAMETER_REQUIRED_GPS_GOOD_COUNT = 'requiredGPSGoodCount';
  static const String PARAMETER_REQUIRED_ALTIMETER_GOOD_COUNT = 'requiredAltimeterGoodCount';
  static const String PARAMETER_BAY = 'bay';
  static const String PARAMETER_DEVICE_INDEX = 'deviceIndex';
  static const String PARAMETER_DEVICE_ON = 'deviceOn';
  static const String PARAMETER_DELAY_TIME = 'delayTime';
  static const String PARAMETER_ANGLE_MODE = 'angleMode';
  static const String PARAMETER_LATITUDE = 'latitude';
  static const String PARAMETER_LONGITUDE = 'longitude';
  static const String PARAMETER_HEIGHT = 'height';
  static const String PARAMETER_PAN = 'pan';
  static const String PARAMETER_TILT = 'tilt';
  static const String PARAMETER_ABSOLUTE = 'absolute';
  static const String PARAMETER_TIME = 'time';
  static const String PARAMETER_Z = 'z';
  static const String PARAMETER_DROID_PARAMETERS = 'droidParameters';
  static const String PARAMETER_RESET_DROID_PARAMETERS = 'resetDroidParameters';
  static const String PARAMETER_ALERT = 'alert';
  static const String PARAMETER_MODE = 'mode';
  static const String PARAMETER_TTS = 'tts';
  static const String PARAMETER_PITCH = 'pitch';
  static const String PARAMETER_RATE = 'rate';
  static const String PARAMETER_FILE_NAME = 'fileName';
  static const String PARAMETER_VOLUME = 'volume';
  static const String PARAMETER_SPEAKER_REPEAT = 'speakerRepeat';
  static const String PARAMETER_LOG_LIMIT = 'logLimit';
  final HttpClient? httpAppClient;
  final http.Client? httpClient;

  final String server;
  final bool secure;
  bool inSecure;
  final String loginUrl;
  String authenticationToken = '';
  final String rootUrl;
  final String baseUrl;
  final String webSocketUrl;
  final String serverControlUrl;
  final String localElevationUrl;
  final String localMapsUrl;
  final String downloadUrl;
  late FloidServerTrust floidServerTrust;
  late FloidServerRepositoryBloc floidServerRepositoryBloc;

  /// Generate a random integer for client connections:
  static String generateRandomId() {
    return Random.secure().nextInt(RANDOM_INT_MAX).toString();
  }

  /// Create the floid server api client:
  FloidServerApiClient({ required this.httpClient,
    required this.httpAppClient,
    required this.server, required this.secure}) :
        rootUrl = secure
            ? 'https://$server'
            : 'http://$server',
        baseUrl = secure
            ? 'https://$server/floidserver'
            : 'http://$server/floidserver',
        serverControlUrl = secure
            ? 'https://$server/fsc'
            : 'http://$server/fsc',
        webSocketUrl = secure
            ? 'wss://$server/handler/websocket'
            : 'ws://$server/handler/websocket',
        loginUrl = secure
            ? 'https://$server/authenticate'
            : 'http://$server/authenticate',
        localElevationUrl = secure
            ? 'https://$server/elevation'
            : 'http://$server/elevation',
        localMapsUrl = secure
            ? 'https://$server'
            : 'http://$server',
        downloadUrl = secure
            ? 'https://$server'
            : 'http://$server',
        inSecure = !secure,

      super() {
    floidServerTrust = new FloidServerTrust(floidServerApiClient: this);
    if (!kIsWeb) {
      // Allow connect to local network:
      httpAppClient?.badCertificateCallback = floidServerTrust.badCertificateCallback;
    }
    print('FloidServerApiClient(baseUrl:$baseUrl webSocketUrl: $webSocketUrl, serverControlUrl: $serverControlUrl)');
  }

  /// Set insecure host
  void setInsecure() {
    if(!inSecure) {
      print("=======> FLOID SERVER IS INSECURE <======");
      inSecure = true;
      floidServerRepositoryBloc.add(new FloidServerRepositoryInsecureHostEvent());
    }
  }

  /// Authenticate:
  Future<String> authenticate(String username, String password) async {
    print("Posting to: $loginUrl");
    Map<String, String> data = { 'username': username, 'password': password};
    if (!kIsWeb) {
      final fHttpApiClient = httpAppClient;
      if (fHttpApiClient != null) {
        HttpClientRequest request = await fHttpApiClient.postUrl(
            Uri.parse(loginUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.add(utf8.encode(json.encode(data)));
        HttpClientResponse loginResponse = await request.close();
        if (loginResponse.statusCode != 200) {
          print(
              "error: loginResponse = " + loginResponse.statusCode.toString());
          throw Exception('error logging in');
        }
        String reply = await loginResponse.transform(utf8.decoder).join();
        String token = json.decode(reply)['token'];
        // Go ahead and set it directly here:
        authenticationToken = 'Bearer $token';
        return token;
      }
    } else {
      final fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response loginResponse =
        await fHttpClient.post(Uri.parse(loginUrl),
            body: json.encode(data),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
            });
        if (loginResponse.statusCode != 200) {
          print("error: loginResponse = " + loginResponse.body);
          throw Exception('error logging in');
        }
        try {
          String token = json.decode(loginResponse.body)['token'];
          // Go ahead and set it directly here:
          authenticationToken = 'Bearer $token';
          return token;
        } catch (e) {
          throw Exception('error parsing token');
        }
      }
    }
    throw Exception('error authenticating');
  }

  //// GET:
  /// Fetch the floid user:
  Future<String> fetchFloidUser() async {
    final String userUrl = '$baseUrl/$URL_USER';
    print("Calling: $userUrl");
    Map<String, String> data = {};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(userUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: loginResponse = " + response.statusCode.toString());
          throw Exception('error getting user');
        }
        String reply = await response.transform(utf8.decoder).join();
        return reply;
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response userResponse = await fHttpClient.get(
          Uri.parse(userUrl),
          headers: {HttpHeaders.authorizationHeader: authenticationToken},
        );
        if (userResponse.statusCode != 200) {
          throw Exception('error getting user');
        }
        return userResponse.body;
      }
    }
    throw Exception('in fetchFloidUser');
  }

  /// Fetch the floid server status:
  Future<FloidServerStatus> fetchFloidServerStatus() async {
    final String floidServerStatusUrl = '$baseUrl/$URL_FLOID_SERVER_STATUS';
    print("Calling: $floidServerStatusUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(floidServerStatusUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: serverStatusResponse = " + response.statusCode.toString());
          return FloidServerStatus(FloidServerStatus.SERVER_STATUS_OFFLINE);
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerStatus(reply);
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response userResponse = await fHttpClient.get(
            Uri.parse(floidServerStatusUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (userResponse.statusCode != 200) {
          print('error getting floid server status');
          return FloidServerStatus(FloidServerStatus.SERVER_STATUS_OFFLINE);
        }
        return FloidServerStatus(userResponse.body);
      }
    }
    throw Exception('in fetchFloidServerStatus');
  }

  /// Start Server:
  Future<bool> startServer() async {
    final String startServerUrl = '$serverControlUrl?request=start';
    print("Posting to: $startServerUrl");
    Map<String, String> data = {};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(startServerUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: startServerResponse = " + response.statusCode.toString());
          return false;
        }
        return true;
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response startServerResponse = await fHttpClient.post(
            Uri.parse(startServerUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken});
        if (startServerResponse.statusCode != 200) {
          print("error: startServerResponse = " + startServerResponse.body);
          return false;
        }
        return true;
      }
    }
    throw Exception('in startServer');
  }

  /// Stop Server:
  Future<bool> stopServer() async {
    final String stopServerUrl = '$serverControlUrl?request=stop';
    print("Posting to: $stopServerUrl");
    Map<String, String> data = {};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(stopServerUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: startServerResponse = " + response.statusCode.toString());
          return false;
        }
        return true;
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response stopServerResponse = await fHttpClient.post(
            Uri.parse(stopServerUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken});
        if (stopServerResponse.statusCode != 200) {
          print("error: startServerResponse = " + stopServerResponse.body);
          return false;
        }
        return true;
      }
    }
    throw Exception('in stopServer');
  }

  /// Fetch the floid list:
  Future<List<int>> fetchFloidList() async {
    final String floidListUrl = '$baseUrl/$URL_FLOID_LIST';
    print("Calling: $floidListUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(floidListUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchFloidList = " + response.statusCode.toString());
          throw Exception('error in fetchFloidList');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          List<int> floidIdList = (jsonDecode(reply) as List).map((e) => e as int).toList();
          return floidIdList;
        } catch (e) {
          print('fetchFloidList caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response floidListResponse = await fHttpClient.get(
            Uri.parse(floidListUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (floidListResponse.statusCode != 200) {
          throw Exception('error getting floid list');
        }
        try {
          List<int> floidIdList = (jsonDecode(floidListResponse.body) as List)
              .map((e) => e as int)
              .toList();
          return floidIdList;
        } catch (e) {
          print('fetchFloidList caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchFloidList');
  }

  /// Fetch the floid id and state list:
  Future<List<FloidIdAndState>> fetchFloidIdAndStateList() async {
    final String floidIdAndStateListUrl = '$baseUrl/$URL_FLOID_ID_AND_STATE_LIST';
    print("Calling: $floidIdAndStateListUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(floidIdAndStateListUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchFloidIdAndStateList = " + response.statusCode.toString());
          throw Exception('error in fetchFloidIdAndStateList');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          List<FloidIdAndState> floidIdAndStateList = (jsonDecode(
              reply) as List)
              .map((e) => new FloidIdAndState.fromJson(e))
              .toList();
          // Debug print list here:
          // floidIdAndStateList.forEach((FloidIdAndState floidIdAndState) {print(floidIdAndState.floidId.toString() + ' ' + floidIdAndState.floidActive.toString() + ' ' + floidIdAndState.floidStatus.toString());});
          return floidIdAndStateList;
        } catch (e) {
          print('fetchFloidIdAndStateList caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response floidListResponse = await fHttpClient.get(
            Uri.parse(
                floidIdAndStateListUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (floidListResponse.statusCode != 200) {
          throw Exception(
              'error getting floid id and state list');
        }
        try {
          List<FloidIdAndState> floidIdAndStateList = (jsonDecode(
              floidListResponse.body) as List).map((e) =>
          new FloidIdAndState.fromJson(e)).toList();
          return floidIdAndStateList;
        } catch (e) {
          print('fetchFloidIdAndStateList caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchFloidIdAndStateList');
  }

  /// Fetch the floid uuid and state list:
  Future<List<FloidUuidAndState>> fetchFloidUuidAndStateList(int floidId) async {
    final String floidUuidAndStateListUrl = '$baseUrl/$URL_FLOID_UUID_AND_STATE_LIST' +
        '?$PARAMETER_FLOID_ID=' + floidId.toString();
    print("Calling: $floidUuidAndStateListUrl");

    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(floidUuidAndStateListUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchFloidUuidAndStateList = " + response.statusCode.toString());
          throw Exception('error in fetchFloidUuidAndStateList');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          List<FloidUuidAndState> floidUuidAndStateList = (jsonDecode(
              reply) as List)
              .map((e) => new FloidUuidAndState.fromJson(e))
              .toList();
          return floidUuidAndStateList;
        } catch (e) {
          print('fetchFloidUuidAndStateList caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response floidListResponse = await fHttpClient.get(
            Uri.parse(floidUuidAndStateListUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (floidListResponse.statusCode != 200) {
          throw Exception(
              'error getting floid uuid and state list');
        }
        try {
          List<FloidUuidAndState> floidUuidAndStateList = (jsonDecode(
              floidListResponse.body) as List).map((e) =>
          new FloidUuidAndState.fromJson(e)).toList();
          return floidUuidAndStateList;
        } catch (e) {
          print('fetchFloidUuidAndStateList caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchFloidUuidAndStateList');
  }

  /// Fetch the mission list:
  Future<List<DroidMissionList>> fetchMissionList() async {
    final String missionListUrl = '$baseUrl/$URL_MISSION_LIST';
    print("Calling: $missionListUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(missionListUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchMissionList = " + response.statusCode.toString());
          throw Exception('error in fetchMissionList');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          List<DroidMissionList> floidMissionList = (jsonDecode(reply) as List)
              .map((e) => new DroidMissionList.fromJson(e))
              .toList();
          return floidMissionList;
        } catch (e) {
          print('fetchMissionList caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response missionListResponse = await fHttpClient.get(
            Uri.parse(missionListUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (missionListResponse.statusCode != 200) {
          throw Exception('error getting floid list');
        }
        try {
          List<DroidMissionList> floidMissionList = (jsonDecode(
              missionListResponse.body) as List).map((e) =>
          new DroidMissionList.fromJson(e)).toList();
          return floidMissionList;
        } catch (e) {
          print('fetchMissionList caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchMissionList');
  }

  /// Fetch a mission:
  Future<DroidMission> fetchMission(int missionId) async {
    final String missionUrl = '$baseUrl/$URL_MISSION_JSON' + '?$PARAMETER_MISSION_ID=' +
        missionId.toString();
    print("Calling: $missionUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(missionUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchMission = " + response.statusCode.toString());
          throw Exception('error in fetchMission');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            DroidMission mission = DroidMission.fromJson(jsonDecode(reply));
            return mission;
          }
        } catch (e) {
          print('fetchMission caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response missionResponse = await fHttpClient.get(
            Uri.parse(missionUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (missionResponse.statusCode != 200) {
          throw Exception(
              'error getting mission');
        }
        try {
          if (missionResponse.body.length > 0) {
            DroidMission mission = DroidMission.fromJson(
                jsonDecode(missionResponse.body));
            return mission;
          }
        } catch (e) {
          print('fetchMission caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchMission');
  }

  /// Fetch the pin set list:
  Future<List<DroidPinSetList>> fetchPinSetList() async {
    final String pinSetListUrl = '$baseUrl/$URL_PIN_SET_LIST';
    print("Calling: $pinSetListUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(pinSetListUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchPinSetList = " + response.statusCode.toString());
          throw Exception('error in fetchPinSetList');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            List<DroidPinSetList> pinSetList = (jsonDecode(reply) as List).map((e) => new DroidPinSetList.fromJson(e)).toList();
            return pinSetList;
          }
        } catch (e) {
          print('fetchPinSetList caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response pinSetListResponse = await fHttpClient.get(
            Uri.parse(pinSetListUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (pinSetListResponse.statusCode != 200) {
          throw Exception('error getting pin set list');
        }
        try {
          if (pinSetListResponse.body.length > 0) {
            List<DroidPinSetList> pinSetList = (jsonDecode(
                pinSetListResponse.body) as List).map((e) =>
            new DroidPinSetList.fromJson(e)).toList();
            return pinSetList;
          }
        } catch (e) {
          print('fetchPinSetList caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchPinSetList');
  }

  /// Fetch a pin set:
  Future<FloidPinSetAlt> fetchPinSet(int pinSetId) async {
    final String pinSetUrl = '$baseUrl/$URL_PIN_SET' + '?$PARAMETER_PIN_SET_ID=' +
        pinSetId.toString();
    print("Calling: $pinSetUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(pinSetUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchPinSet = " + response.statusCode.toString());
          throw Exception('error in fetchPinSet');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            FloidPinSetAlt pinSetAlt = FloidPinSetAlt.fromJson(
                jsonDecode(reply));
            return pinSetAlt;
          }
        } catch (e) {
          print('fetchPinSet caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response pinSetResponse = await fHttpClient.get(
            Uri.parse(pinSetUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (pinSetResponse.statusCode != 200) {
          throw Exception(
              'error getting pin set');
        }
        try {
          if (pinSetResponse.body.length > 0) {
            FloidPinSetAlt pinSetAlt = FloidPinSetAlt.fromJson(
                jsonDecode(pinSetResponse.body));
            return pinSetAlt;
          }
        } catch (e) {
          print('fetchPinSet caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchPinSet');
  }
  /// Fetch gps points:
  Future<List<FloidGpsPoint>> fetchGpsPoints(int floidId,
      String floidUuid) async {
    final String gpsPointListUrl = '$baseUrl/$URL_GPS_POINTS?$PARAMETER_FLOID_ID=' +
        floidId.toString() + '&$PARAMETER_FLOID_UUID=' + floidUuid;
    print("Calling: $gpsPointListUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(gpsPointListUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchGpsPoints = " + response.statusCode.toString());
          throw Exception('error in fetchGpsPoints');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            List<FloidGpsPoint> gpsPointList = (jsonDecode(reply) as List).map((e) => new FloidGpsPoint.fromJson(e)).toList();
            return gpsPointList;
          }
        } catch (e) {
          print('fetchGpsPoints caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response gpsPointListResponse = await fHttpClient.get(
            Uri.parse(gpsPointListUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (gpsPointListResponse.statusCode != 200) {
          throw Exception('error getting gps points');
        }
        try {
          if (gpsPointListResponse.body.length > 0) {
            List<FloidGpsPoint> gpsPointList = (jsonDecode(
                gpsPointListResponse.body) as List).map((e) =>
            new FloidGpsPoint.fromJson(e)).toList();
            return gpsPointList;
          }
        } catch (e) {
          print('fetchGpsPoints caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchGpsPoints');
  }

  /// Fetch Last Floid Droid Status:
  Future<FloidDroidStatus> fetchLastFloidDroidStatus(int floidId,
      String floidUuid) async {
    final String lastFloidDroidStatusUrl = '$baseUrl/$URL_LAST_FLOID_DROID_STATUS?$PARAMETER_FLOID_ID=' +
        floidId.toString() + '&$PARAMETER_FLOID_UUID=' + floidUuid;
    print("Calling: $lastFloidDroidStatusUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(lastFloidDroidStatusUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchLastFloidDroidStatus = " + response.statusCode.toString());
          throw Exception('error in fetchLastFloidDroidStatus');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            return FloidDroidStatus.fromJson(jsonDecode(reply));
          }
        } catch (e) {
          print('fetchLastFloidDroidStatus caught exception $e');
          throw e;
        }
      }
    } else {
      final fHttpClient = httpClient;
      if (fHttpClient != null) {
        final http.Response lastFloidDroidStatusResponse = await fHttpClient
            .get(Uri.parse(lastFloidDroidStatusUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (lastFloidDroidStatusResponse.statusCode != 200) {
          throw Exception('error getting last Floid Droid status');
        }
        if (lastFloidDroidStatusResponse.body.length > 0) {
          try {
            return FloidDroidStatus.fromJson(
                jsonDecode(lastFloidDroidStatusResponse.body));
          } catch (e) {
            print('fetchLastFloidDroidStatus caught exception $e');
            throw e;
          }
        }
      }
    }
    throw Exception('in fetchLastFloidDroidStatus');
  }
  
  /// Fetch Last Floid Status:
  Future<FloidStatus> fetchLastFloidStatus(int floidId,
      String floidUuid) async {
    final String lastFloidStatusUrl = '$baseUrl/$URL_LAST_FLOID_STATUS?$PARAMETER_FLOID_ID=' +
        floidId.toString() + '&$PARAMETER_FLOID_UUID=' + floidUuid;
    print("Calling: $lastFloidStatusUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(lastFloidStatusUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchLastFloidStatus = " +
              response.statusCode.toString());
          throw Exception('error in fetchLastFloidStatus');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            return FloidStatus.fromJson(jsonDecode(reply));
          }
        } catch (e) {
          print('fetchLastFloidStatus caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response lastFloidStatusResponse = await fHttpClient.get(
            Uri.parse(lastFloidStatusUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (lastFloidStatusResponse.statusCode != 200) {
          throw Exception('error getting last Floid status');
        }
        if (lastFloidStatusResponse.body.length > 0) {
          try {
            return FloidStatus.fromJson(
                jsonDecode(lastFloidStatusResponse.body));
          } catch (e) {
            print('fetchLastFloidStatus caught exception $e');
          }
        }
      }
    }
    throw Exception('in fetchLastFloidStatus');
  }

  /// Fetch Last Floid Model Status:
  Future<FloidModelStatus> fetchLastFloidModelStatus(int floidId,
      String floidUuid) async {
    final String lastFloidModelStatusUrl = '$baseUrl/$URL_LAST_FLOID_MODEL_STATUS?$PARAMETER_FLOID_ID=' +
        floidId.toString() + '&$PARAMETER_FLOID_UUID=' + floidUuid;
    print("Calling: $lastFloidModelStatusUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(lastFloidModelStatusUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchLastFloidModelStatus = " + response.statusCode.toString());
          throw Exception('error in fetchLastFloidModelStatus');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            return FloidModelStatus.fromJson(jsonDecode(reply));
          }
        } catch (e) {
          print('fetchLastFloidModelStatus caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response lastFloidModelStatusResponse = await fHttpClient
            .get(Uri.parse(lastFloidModelStatusUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (lastFloidModelStatusResponse.statusCode != 200) {
          throw Exception('error getting last Floid Model Status');
        }
        if (lastFloidModelStatusResponse.body.length > 0) {
          try {
            return FloidModelStatus.fromJson(
                jsonDecode(lastFloidModelStatusResponse.body));
          } catch (e) {
            print('fetchLastFloidModelStatus caught exception $e');
            throw e;
          }
        }
      }
    }
    throw Exception('in fetchLastFloidModelStatus');
  }



  /// Fetch Last Floid Model Parameters:
  Future<FloidModelParameters> fetchLastFloidModelParameters(int floidId,
      String floidUuid) async {
    final String lastFloidModelParametersUrl = '$baseUrl/$URL_LAST_FLOID_MODEL_PARAMETERS?$PARAMETER_FLOID_ID=' +
        floidId.toString() + '&$PARAMETER_FLOID_UUID=' + floidUuid;
    print("Calling: $lastFloidModelParametersUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(lastFloidModelParametersUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print(
              "error: fetchLastFloidModelParameters = " + response.statusCode.toString());
          throw Exception('error in fetchLastFloidModelParameters');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            return FloidModelParameters.fromJson(jsonDecode(reply));
          }
        } catch (e) {
          print('fetchLastFloidModelParameters caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response lastFloidModelParametersResponse = await fHttpClient
            .get(Uri.parse(
            lastFloidModelParametersUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (lastFloidModelParametersResponse.statusCode != 200) {
          throw Exception('error getting last Floid Model Status');
        }
        if (lastFloidModelParametersResponse.body.length > 0) {
          try {
            return FloidModelParameters.fromJson(
                jsonDecode(lastFloidModelParametersResponse.body));
          } catch (e) {
            print('fetchLastFloidModelParameters caught exception $e');
          }
        }
      }
    }
    throw Exception('in fetchLastFloidModelParameters');
  }

  /// Fetch Last Floid Photo:
  Future<FloidDroidPhoto> fetchLastFloidDroidPhoto(int floidId,
      String floidUuid) async {
    final String lastFloidDroidPhotoUrl = '$baseUrl/$URL_LAST_FLOID_DROID_PHOTO?$PARAMETER_FLOID_ID=' +
        floidId.toString() + '&$PARAMETER_FLOID_UUID=' + floidUuid;
    print("Calling: $lastFloidDroidPhotoUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(lastFloidDroidPhotoUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchLastFloidDroidPhoto = " + response.statusCode.toString());
          throw Exception('error in fetchLastFloidDroidPhoto');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            return FloidDroidPhoto.fromJson(jsonDecode(reply));
          }
        } catch (e) {
          print('fetchLastFloidDroidPhoto caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response lastFloidDroidPhotoResponse = await fHttpClient.get(
            Uri.parse(lastFloidDroidPhotoUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (lastFloidDroidPhotoResponse.statusCode != 200) {
          throw Exception('error getting last Floid Droid Photo');
        }
        if (lastFloidDroidPhotoResponse.body.length > 0) {
          try {
            return FloidDroidPhoto.fromJson(
                jsonDecode(lastFloidDroidPhotoResponse.body));
          } catch (e) {
            print('fetchLastFloidDroidPhoto caught exception $e');
            throw e;
          }
        }
      }
    }
    throw Exception('in fetchLastFloidDroidPhoto');
  }

  /// Fetch Last Floid Video Frame:
  Future<FloidDroidVideoFrame> fetchLastFloidDroidVideoFrame(int floidId,
      String floidUuid) async {
    final String lastFloidDroidVideoFrameUrl = '$baseUrl/$URL_LAST_FLOID_DROID_VIDEO_FRAME?$PARAMETER_FLOID_ID=' +
        floidId.toString() + '&$PARAMETER_FLOID_UUID=' + floidUuid;
    print("Calling: $lastFloidDroidVideoFrameUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(lastFloidDroidVideoFrameUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print(
              "error: fetchLastFloidDroidVideoFrame = " + response.statusCode.toString());
          throw Exception('error in fetchLastFloidDroidVideoFrame');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            return FloidDroidVideoFrame.fromJson(jsonDecode(reply));
          }
        } catch (e) {
          print('fetchLastFloidDroidVideoFrame caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response lastFloidDroidVideoFrameResponse = await
        fHttpClient.get(Uri.parse(lastFloidDroidVideoFrameUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (lastFloidDroidVideoFrameResponse.statusCode != 200) {
          throw Exception('error getting last Floid Droid Video Frame');
        }
        if (lastFloidDroidVideoFrameResponse.body.length > 0) {
          try {
            return FloidDroidVideoFrame.fromJson(
                jsonDecode(lastFloidDroidVideoFrameResponse.body));
          } catch (e) {
            print('fetchLastFloidDroidVideoFrame caught exception $e');
            throw e;
          }
        }
      }
    }
    throw Exception('in fetchLastFloidDroidVideoFrame');
  }

  /// Fetch gps points filtered:
  Future<List<FloidGpsPoint>> fetchGpsPointsFiltered(int floidId,
      String floidUuid,
      int maxPoints,
      int minTime,
      double minDistance,
      double maxKink,
      double maxKinkAlpha) async {
    final String gpsPointListUrl = '$baseUrl/$URL_GPS_POINTS_FILTERED?$PARAMETER_FLOID_ID=' +
        floidId.toString()
        + '&$PARAMETER_FLOID_UUID=' + floidUuid
        + '&$PARAMETER_MAX_POINTS=' + maxPoints.toString()
        + '&$PARAMETER_MIN_TIME=' + minTime.toString()
        + '&$PARAMETER_MIN_DISTANCE=' + minDistance.toString()
        + '&$PARAMETER_MAX_KINK=' + maxKink.toString()
        + '&$PARAMETER_MAX_KINK_ALPHA=' + maxKinkAlpha.toString();
    print("Calling: $gpsPointListUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(gpsPointListUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchGpsPointsFiltered = " + response.statusCode.toString());
          throw Exception('error in fetchGpsPointsFiltered');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            List<FloidGpsPoint> gpsPointList = (jsonDecode(reply) as List).map((e) => new FloidGpsPoint.fromJson(e)).toList();
            return gpsPointList;
          }
        } catch (e) {
          print('fetchGpsPointsFiltered caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response gpsPointListResponse = await fHttpClient.get(
            Uri.parse(
                gpsPointListUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (gpsPointListResponse.statusCode != 200) {
          throw Exception('error getting gps points filtered');
        }
        try {
          if (gpsPointListResponse.body.length > 0) {
            List<FloidGpsPoint> gpsPointList = (jsonDecode(
                gpsPointListResponse.body) as List).map((e) =>
            new FloidGpsPoint.fromJson(e)).toList();
            return gpsPointList;
          }
        } catch (e) {
          print('fetchGpsPointsFiltered caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchGpsPointsFiltered');
  }

  /// Fetch droid gps points:
  Future<List<FloidGpsPoint>> fetchDroidGpsPoints(int floidId,
      String floidUuid) async {
    final String droidGpsPointListUrl = '$baseUrl/$URL_DROID_GPS_POINTS?$PARAMETER_FLOID_ID=' +
        floidId.toString() + '&$PARAMETER_FLOID_UUID=' + floidUuid;
    print("Calling: $droidGpsPointListUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(droidGpsPointListUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchDroidGpsPoints = " + response.statusCode.toString());
          throw Exception('error in fetchDroidGpsPoints');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            List<FloidGpsPoint> droidGpsPointList = (jsonDecode(reply) as List)
                .map((e) => new FloidGpsPoint.fromJson(e))
                .toList();
            return droidGpsPointList;
          }
        } catch (e) {
          print('fetchDroidGpsPoints caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response droidGpsPointListResponse = await fHttpClient
            .get(Uri.parse(
            droidGpsPointListUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (droidGpsPointListResponse.statusCode != 200) {
          throw Exception('error getting droid gps points');
        }
        try {
          if (droidGpsPointListResponse.body.length > 0) {
            List<FloidGpsPoint> droidGpsPointList = (jsonDecode(
                droidGpsPointListResponse.body) as List).map((e) => new FloidGpsPoint.fromJson(e)).toList();
            return droidGpsPointList;
          }
        } catch (e) {
          print('fetchDroidGpsPoints caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchDroidGpsPoints');
  }

  /// Fetch gps points filtered:
  Future<List<FloidGpsPoint>> fetchDroidGpsPointsFiltered(int floidId,
      String floidUuid,
      int maxPoints,
      int minTime,
      double minDistance,
      double maxKink,
      double maxKinkAlpha) async {
    final String droidGpsPointListUrl = '$baseUrl/$URL_DROID_GPS_POINTS_FILTERED?$PARAMETER_FLOID_ID=' +
        floidId.toString()
        + '&$PARAMETER_FLOID_UUID=' + floidUuid
        + '&$PARAMETER_MAX_POINTS=' + maxPoints.toString()
        + '&$PARAMETER_MIN_TIME=' + minTime.toString()
        + '&$PARAMETER_MIN_DISTANCE=' + minDistance.toString()
        + '&$PARAMETER_MAX_KINK=' + maxKink.toString()
        + '&$PARAMETER_MAX_KINK_ALPHA=' + maxKinkAlpha.toString();
    print("Calling: $droidGpsPointListUrl");
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.getUrl(
            Uri.parse(droidGpsPointListUrl));
        request.headers.set(
            HttpHeaders.contentTypeHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.acceptHeader, ContentType.json.toString());
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: fetchDroidGpsPointsFiltered = " + response.statusCode.toString());
          throw Exception('error in fetchDroidGpsPointsFiltered');
        }
        String reply = await response.transform(utf8.decoder).join();
        try {
          if (reply.length > 0) {
            List<FloidGpsPoint> droidGpsPointList = (jsonDecode(reply) as List)
                .map((e) => new FloidGpsPoint.fromJson(e))
                .toList();
            return droidGpsPointList;
          }
        } catch (e) {
          print('fetchDroidGpsPointsFiltered caught exception $e');
          throw e;
        }
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response droidGpsPointListResponse = await fHttpClient.get(
            Uri.parse(droidGpsPointListUrl),
            headers: {
              HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              HttpHeaders.authorizationHeader: authenticationToken
            });
        if (droidGpsPointListResponse.statusCode != 200) {
          throw Exception('error getting droid gps points filtered');
        }
        try {
          if (droidGpsPointListResponse.body.length > 0) {
            List<FloidGpsPoint> droidGpsPointList = (jsonDecode(
                droidGpsPointListResponse.body) as List).map((e) => new FloidGpsPoint.fromJson(e)).toList();
            return droidGpsPointList;
          }
        } catch (e) {
          print('fetchDroidGpsPointsFiltered caught exception $e');
          throw e;
        }
      }
    }
    throw Exception('in fetchDroidGpsPointsFiltered');
  }

  //// STOMP:
  /// Subscribe to this floid id and uuid:
  Future<StompClient> subscribeStompUuid(String stompId, String subscription,
      int floidId, String floidUuid,
      void callback(Map<String, String> headers, String message)) {
    // Connect to the stop endpoint:
    return StompClientProxy().connect(floidServerTrust,
        webSocketUrl,
        authorization: authenticationToken,
        onConnect: (StompClient client, Map<String, String>? headers) {
          String fullSubscription = subscription + "_" + floidId.toString() +
              "_" + floidUuid;
          String subscriptionId = stompId + "_" + generateRandomId();
          print("Subscribing to: $fullSubscription - $subscriptionId");
          client.subscribeString(subscriptionId, fullSubscription, callback);
        });
  }

  /// Subscribe to this floid id:
  Future<StompClient> subscribeStompId(String stompId, String subscription,
      int floidId,
      void callback(Map<String, String> headers, String message)) {
    // Connect to the stop endpoint:
    return StompClientProxy().connect(floidServerTrust,
        webSocketUrl,
        authorization: authenticationToken,
        onConnect: (StompClient client, Map<String, String>? headers) {
          String fullSubscription = subscription + "_" + floidId.toString();
          String subscriptionId = stompId + "_" + generateRandomId();
          print("Subscribing to: $fullSubscription - $subscriptionId");
          client.subscribeString(subscriptionId, fullSubscription, callback);
        });
  }

  /// Subscribe to generic:
  Future<StompClient> subscribeStomp(String stompId, String subscription,
      void callback(Map<String, String> headers, String message)) {
    // Connect to the stop endpoint:
    return StompClientProxy().connect(floidServerTrust,
        webSocketUrl,
        authorization: authenticationToken,
        onConnect: (StompClient client, Map<String, String>? headers) {
          String subscriptionId = stompId + "_" + generateRandomId();
          print("Subscribing to: $subscription - $subscriptionId");
          client.subscribeString(
              subscriptionId, subscription, callback);
        });
  }

/* *************************
     These take no parameters:
     ************************* */
  static const String FLOID_SERVER_STATUS_STOMP_ID = "floidServerStatus";
  static const String FLOID_SERVER_STATUS_STOMP_SUBSCRIPTION = "/topic/floidServerStatusDestination";
  static const String FLOID_NEW_FLOID_ID_STOMP_ID = "floidNewFloidId";
  static const String FLOID_NEW_FLOID_ID_STOMP_SUBSCRIPTION = "/topic/floidNewFloidIdDestination";

  /// Subscribe to the Floid Server Status STOMP ports
  Future<StompClient> subscribeFloidServerStatus(void callback(Map<String, String> headers, String message)) =>
      subscribeStomp(
          FloidServerApiClient.FLOID_SERVER_STATUS_STOMP_ID,
          FloidServerApiClient.FLOID_SERVER_STATUS_STOMP_SUBSCRIPTION,
          callback);

  /// Subscribe to the Floid Server Status STOMP ports
  Future<StompClient> subscribeFloidNewFloidId(void callback(Map<String, String> headers, String message)) =>
      subscribeStomp(
          FloidServerApiClient.FLOID_NEW_FLOID_ID_STOMP_ID,
          FloidServerApiClient.FLOID_NEW_FLOID_ID_STOMP_SUBSCRIPTION,
          callback);


/* **********************
     These take a floid id:
     ********************** */
  static const String FLOID_NEW_FLOID_UUID_STOMP_ID = "floidNewFloidUuid";
  static const String FLOID_NEW_FLOID_UUID_STOMP_SUBSCRIPTION = "/topic/floidNewFloidUuidDestination";

  /// Subscribe to the Floid New Floid Uuid STOMP ports
  Future<StompClient> subscribeFloidNewFloidUuid(int floidId,
      void callback(Map<String, String> headers, String message)) =>
      subscribeStompId(
          FloidServerApiClient.FLOID_NEW_FLOID_UUID_STOMP_ID,
          FloidServerApiClient.FLOID_NEW_FLOID_UUID_STOMP_SUBSCRIPTION,
          floidId,
          callback);


/* ***************************************
     These take a floid id and a floid uuid:
     *************************************** */
  static const String FLOID_MODEL_STATUS_STOMP_ID = "floidModelStatus";
  static const String FLOID_MODEL_STATUS_STOMP_SUBSCRIPTION = "/topic/floidModelStatusDestination";
  static const String FLOID_MODEL_PARAMETERS_STOMP_ID = "floidModelParameters";
  static const String FLOID_MODEL_PARAMETERS_STOMP_SUBSCRIPTION = "/topic/floidModelParametersDestination";
  static const String FLOID_FLOID_STATUS_STOMP_ID = "floidStatus";
  static const String FLOID_FLOID_STATUS_STOMP_SUBSCRIPTION = "/topic/floidStatusDestination";
  static const String FLOID_DROID_MESSAGE_STOMP_ID = "floidDroidMessage";
  static const String FLOID_DROID_MESSAGE_STOMP_SUBSCRIPTION = "/topic/floidDroidMessageDestination";
  static const String FLOID_DROID_STATUS_STOMP_ID = "floidDroidStatus";
  static const String FLOID_DROID_STATUS_STOMP_SUBSCRIPTION = "/topic/floidDroidStatusDestination";
  static const String FLOID_DROID_PHOTO_STOMP_ID = "floidDroidPhoto";
  static const String FLOID_DROID_PHOTO_STOMP_SUBSCRIPTION = "/topic/floidDroidPhotoDestination";
  static const String FLOID_DROID_VIDEO_FRAME_STOMP_ID = "floidDroidVideoFrame";
  static const String FLOID_DROID_VIDEO_FRAME_STOMP_SUBSCRIPTION = "/topic/floidDroidVideoFrameDestination";
  static const String FLOID_DROID_MISSION_INSTRUCTION_STATUS_STOMP_ID = "floidDroidMissionInstructionStatusMessage";
  static const String FLOID_DROID_MISSION_INSTRUCTION_STATUS_STOMP_SUBSCRIPTION = "/topic/floidDroidMissionInstructionStatusMessageDestination";

 /// Subscribe to the Floid Model Status STOMP ports
  Future<StompClient> subscribeFloidModelStatus(int floidId, String floidUuid,
      void callback(Map<String, String> headers, String message)) =>
      subscribeStompUuid(
          FloidServerApiClient.FLOID_MODEL_STATUS_STOMP_ID,
          FloidServerApiClient.FLOID_MODEL_STATUS_STOMP_SUBSCRIPTION,
          floidId,
          floidUuid,
          callback);

  /// Subscribe to the Floid Model Parameters STOMP ports
  Future<StompClient> subscribeFloidModelParameters(int floidId,
      String floidUuid,
      void callback(Map<String, String> headers, String message)) =>
      subscribeStompUuid(
          FloidServerApiClient.FLOID_MODEL_PARAMETERS_STOMP_ID,
          FloidServerApiClient.FLOID_MODEL_PARAMETERS_STOMP_SUBSCRIPTION,
          floidId,
          floidUuid,
          callback);

  /// Subscribe to the Floid Droid Message STOMP ports
  Future<StompClient> subscribeFloidDroidMessage(int floidId, String floidUuid,
      void callback(Map<String, String> headers, String message)) =>
      subscribeStompUuid(
          FloidServerApiClient.FLOID_DROID_MESSAGE_STOMP_ID,
          FloidServerApiClient.FLOID_DROID_MESSAGE_STOMP_SUBSCRIPTION,
          floidId,
          floidUuid,
          callback);

  /// Subscribe to the Floid Status STOMP ports
  Future<StompClient> subscribeFloidStatus(int floidId, String floidUuid,
      void callback(Map<String, String> headers, String message)) =>
      subscribeStompUuid(
          FloidServerApiClient.FLOID_FLOID_STATUS_STOMP_ID,
          FloidServerApiClient.FLOID_FLOID_STATUS_STOMP_SUBSCRIPTION,
          floidId,
          floidUuid,
          callback);

  /// Subscribe to the Floid Droid Status STOMP ports
  Future<StompClient> subscribeFloidDroidStatus(int floidId, String floidUuid,
      void callback(Map<String, String> headers, String message)) =>
      subscribeStompUuid(
          FloidServerApiClient.FLOID_DROID_STATUS_STOMP_ID,
          FloidServerApiClient.FLOID_DROID_STATUS_STOMP_SUBSCRIPTION,
          floidId,
          floidUuid,
          callback);

  /// Subscribe to the Floid Droid Photo STOMP ports
  Future<StompClient> subscribeFloidDroidPhoto(int floidId, String floidUuid,
      void callback(Map<String, String> headers, String message)) =>
      subscribeStompUuid(
          FloidServerApiClient.FLOID_DROID_PHOTO_STOMP_ID,
          FloidServerApiClient.FLOID_DROID_PHOTO_STOMP_SUBSCRIPTION,
          floidId,
          floidUuid,
          callback);

  /// Subscribe to the Floid Droid Video Frame STOMP ports
  Future<StompClient> subscribeFloidDroidVideoFrame(int floidId,
      String floidUuid,
      void callback(Map<String, String> headers, String message)) =>
      subscribeStompUuid(
          FloidServerApiClient.FLOID_DROID_VIDEO_FRAME_STOMP_ID,
          FloidServerApiClient.FLOID_DROID_VIDEO_FRAME_STOMP_SUBSCRIPTION,
          floidId,
          floidUuid,
          callback);

  /// Subscribe to the Floid Droid Mission Instruction Status STOMP ports
  Future<StompClient> subscribeFloidDroidMissionInstructionStatus(int floidId,
      String floidUuid,
      void callback(Map<String, String> headers, String message)) =>
      subscribeStompUuid(
          FloidServerApiClient.FLOID_DROID_MISSION_INSTRUCTION_STATUS_STOMP_ID,
          FloidServerApiClient
              .FLOID_DROID_MISSION_INSTRUCTION_STATUS_STOMP_SUBSCRIPTION,
          floidId,
          floidUuid,
          callback);

  /// Set the content type and add form fields to a POST request:
  static void addFormFieldsToRequest(HttpClientRequest request,
      Map<String, String> fields) {
    request.headers.contentType =
    new ContentType("application", "x-www-form-urlencoded", charset: "utf-8");
    bool first = true;
    fields.forEach((key, value) {
      request.add(utf8.encode(
          (first ? '' : '&') + key + '=' + Uri.encodeQueryComponent(value)));
      first = false;
    });
  }

  //// POST:
  /// Execute the given mission over the pin set for the floid id:
  Future<FloidServerCommandResult> executeMission(int floidId, int missionId,
      int pinSetId) async {
    final String executeMissionUrl = '$baseUrl/$URL_EXECUTE_MISSION';
    print("Posting to: $executeMissionUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_MISSION_ID': missionId.toString(),
      '$PARAMETER_PIN_SET_ID': pinSetId.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(executeMissionUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: executeMission = " + response.statusCode.toString());
          throw Exception('error in executeMission');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response response = await fHttpClient.post(
            Uri.parse(executeMissionUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (response.statusCode != 200) {
          print("error: executeMission Response = " + response.body);
          throw Exception('error executing mission');
        }
        return FloidServerCommandResult.fromJson(jsonDecode(response.body));
      }
    }
    throw Exception('in executeMission');
  }


  /// Create a new mission:
  Future<int> createNewMission(String newMissionName) async {
    final String createNewMissionUrl = '$baseUrl/$URL_CREATE_NEW_MISSION';
    print("Posting to: $createNewMissionUrl");
    Map<String, String> data = { 'newMissionName': newMissionName};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(createNewMissionUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: createNewMission = " + response.statusCode.toString());
          throw Exception('error in createNewMission');
        }
        String reply = await response.transform(utf8.decoder).join();
        return int.parse(reply);
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response createNewMissionResponse = await fHttpClient.post(
            Uri.parse(createNewMissionUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (createNewMissionResponse.statusCode != 200) {
          print(
              "error: createMissionResponse = " +
                  createNewMissionResponse.body);
          throw Exception('error creating mission');
        }
        return int.parse(createNewMissionResponse.body);
      }
    }
    throw Exception('in createNewMission');
  }

  /// Delete a mission:
  Future<bool> deleteMission(int missionId) async {
    final String deleteMissionUrl = '$baseUrl/$URL_DELETE_MISSION';
    print("Posting to: $deleteMissionUrl");
    Map<String, String> data = { '$PARAMETER_MISSION_ID': missionId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(deleteMissionUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: deleteMission = " + response.statusCode.toString());
          throw Exception('error in deleteMission');
        }
        String reply = await response.transform(utf8.decoder).join();
        return reply.toLowerCase() == 'true';
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response deleteMissionResponse = await fHttpClient.post(
            Uri.parse(deleteMissionUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (deleteMissionResponse.statusCode != 200) {
          print("error: deleteMissionResponse = " + deleteMissionResponse.body);
          throw Exception('error deleting mission');
        }
        return deleteMissionResponse.body.toLowerCase() == 'true';
      }
    }
    throw Exception('in deleteMission');
  }

  /// Duplicate a mission:
  Future<int> duplicateMission(int originalMissionId,
      String newMissionName) async {
    final String duplicateMissionUrl = '$baseUrl/$URL_DUPLICATE_MISSION';
    print("Posting to: $duplicateMissionUrl");
    Map<String, String> data = {
      '$PARAMETER_ORIGINAL_MISSION_ID': originalMissionId.toString(),
      '$PARAMETER_NEW_MISSION_NAME': newMissionName
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(duplicateMissionUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: duplicateMission = " + response.statusCode.toString());
          throw Exception('error in duplicateMission');
        }
        String reply = await response.transform(utf8.decoder).join();
        return int.parse(reply);
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response duplicateMissionResponse = await fHttpClient.post(
            Uri.parse(duplicateMissionUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (duplicateMissionResponse.statusCode != 200) {
          print("error: duplicateMissionResponse = " +
              duplicateMissionResponse.body);
          throw Exception('error duplicating mission');
        }
        return int.parse(duplicateMissionResponse.body);
      }
    }
    throw Exception('in duplicateMission');
  }

  /// Save a mission:
  Future<int> saveMission(String mission) async {
    final String saveMissionUrl = '$baseUrl/$URL_SAVE_MISSION_JSON'; // NOTE: saveMission is xml document - this is json version
    print("Posting to: $saveMissionUrl");
    Map<String, String> data = { '$PARAMETER_MISSION': mission};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(saveMissionUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: saveMission = " + response.statusCode.toString());
          throw Exception('error in saveMission');
        }
        String reply = await response.transform(utf8.decoder).join();
        return int.parse(reply);
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response saveMissionResponse = await fHttpClient.post(
            Uri.parse(saveMissionUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (saveMissionResponse.statusCode != 200) {
          print("error: saveMissionResponse = " + saveMissionResponse.body);
          throw Exception('error saving mission');
        }
        return int.parse(saveMissionResponse.body);
      }
    }
    throw Exception('in saveMission');
  }

  /// Import a mission:
  Future<int> importMission(String mission) async {
    final String importMissionUrl = '$baseUrl/$URL_IMPORT_MISSION';
    print("Posting to: $importMissionUrl");
    Map<String, String> data = { '$PARAMETER_MISSION': mission};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(importMissionUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: importMission = " + response.statusCode.toString());
          throw Exception('error in importMission');
        }
        String reply = await response.transform(utf8.decoder).join();
        return int.parse(reply);
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response importMissionResponse = await fHttpClient.post(
            Uri.parse(importMissionUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (importMissionResponse.statusCode != 200) {
          print("error: importMissionResponse = " + importMissionResponse.body);
          throw Exception('error importing mission');
        }
        return int.parse(importMissionResponse.body);
      }
    }
    throw Exception('in importMission');
  }

  /// Create a pin set:
  Future<int> createPinSet(String newPinSetName) async {
    final String createPinSetUrl = '$baseUrl/$URL_CREATE_PIN_SET';
    print("Posting to: $createPinSetUrl");
    Map<String, String> data = { '$PARAMETER_NEW_PIN_SET_NAME': newPinSetName};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(createPinSetUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: createPinSet = " + response.statusCode.toString());
          throw Exception('error in createPinSet');
        }
        String reply = await response.transform(utf8.decoder).join();
        return int.parse(reply);
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response createPinSetResponse = await fHttpClient.post(
            Uri.parse(createPinSetUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (createPinSetResponse.statusCode != 200) {
          print("error: createPinSetResponse = " + createPinSetResponse.body);
          throw Exception('error creating mission');
        }
        return int.parse(createPinSetResponse.body);
      }
    }
    throw Exception('in createPinSet');
  }

  /// Delete a pin set:
  Future<bool> deletePinSet(int pinSetId) async {
    final String deletePinSetUrl = '$baseUrl/$URL_DELETE_PIN_SET';
    print("Posting to: $deletePinSetUrl");
    Map<String, String> data = { '$PARAMETER_PIN_SET_ID': pinSetId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(deletePinSetUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: deletePinSet = " + response.statusCode.toString());
          throw Exception('error in deletePinSet');
        }
        String reply = await response.transform(utf8.decoder).join();
        return reply.toLowerCase() == 'true';
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response deletePinSetResponse = await fHttpClient.post(
            Uri.parse(deletePinSetUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (deletePinSetResponse.statusCode != 200) {
          print("error: deletePinSetResponse = " + deletePinSetResponse.body);
          throw Exception('error deleting pin set');
        }
        return deletePinSetResponse.body.toLowerCase() == 'true';
      }
    }
    throw Exception('in deletePinSet');
  }

  /// Duplicate a pin set:
  Future<int> duplicatePinSet(int originalPinSetId,
      String newPinSetName) async {
    final String duplicateMissionUrl = '$baseUrl/$URL_DUPLICATE_PIN_SET';
    print("Posting to: $duplicateMissionUrl");
    Map<String, String> data = {
      '$PARAMETER_ORIGINAL_PIN_SET_ID': originalPinSetId.toString(),
      '$PARAMETER_NEW_PIN_SET_NAME': newPinSetName
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(duplicateMissionUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: duplicatePinSet = " + response.statusCode.toString());
          throw Exception('error in duplicatePinSet');
        }
        String reply = await response.transform(utf8.decoder).join();
        return int.parse(reply);
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response duplicatePinSetResponse = await fHttpClient.post(
            Uri.parse(duplicateMissionUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (duplicatePinSetResponse.statusCode != 200) {
          print(
              "error: duplicatePinSetResponse = " +
                  duplicatePinSetResponse.body);
          throw Exception('error duplicating pin set');
        }
        return int.parse(duplicatePinSetResponse.body);
      }
    }
    throw Exception('in duplicatePinSet');
  }

  /// Save a pin set:
  Future<int> savePinSet(String pinSetAlt) async {
    final String savePinSetUrl = '$baseUrl/$URL_SAVE_PIN_SET';
    print("Posting to: $savePinSetUrl");
    Map<String, String> data = { '$PARAMETER_PIN_SET_ALT': pinSetAlt};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(savePinSetUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: savePinSet = " + response.statusCode.toString());
          throw Exception('error in savePinSet');
        }
        String reply = await response.transform(utf8.decoder).join();
        return int.parse(reply);
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response savePinSetResponse = await fHttpClient.post(
            Uri.parse(savePinSetUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (savePinSetResponse.statusCode != 200) {
          print("error: savePinSetResponse = " + savePinSetResponse.body);
          throw Exception('error saving pin set');
        }
        return int.parse(savePinSetResponse.body);
      }
    }
    throw Exception('in savePinSet');
  }

  /// Import a pin set:
  Future<int> importPinSet(String pinSetAlt) async {
    final String importPinSetUrl = '$baseUrl/$URL_IMPORT_PIN_SET';
    print("Posting to: $importPinSetUrl");
    Map<String, String> data = { '$PARAMETER_PIN_SET_ALT': pinSetAlt};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(importPinSetUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: importPinSet = " + response.statusCode.toString());
          throw Exception('error in importPinSet');
        }
        String reply = await response.transform(utf8.decoder).join();
        return int.parse(reply);
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response importPinSetResponse = await fHttpClient.post(
            Uri.parse(importPinSetUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (importPinSetResponse.statusCode != 200) {
          print("error: importPinSetResponse = " + importPinSetResponse.body);
          throw Exception('error saving pin set');
        }
        return int.parse(importPinSetResponse.body);
      }
    }
    throw Exception('in importPinSet');
  }

  /// Delete a floidId:
  Future<bool> deleteFloidId(int floidId) async {
    final String deleteFloidIdUrl = '$baseUrl/$URL_DELETE_FLOID_ID';
    print("Posting to: $deleteFloidIdUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(deleteFloidIdUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: deleteFloidId = " + response.statusCode.toString());
          throw Exception('error in deleteFloidId');
        }
        String reply = await response.transform(utf8.decoder).join();
        return reply.toLowerCase() == 'true';
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response deleteFloidIdResponse = await fHttpClient.post(
            Uri.parse(deleteFloidIdUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);

        if (deleteFloidIdResponse.statusCode != 200) {
          print("error: deletePinSetResponse = " + deleteFloidIdResponse.body);
          throw Exception('error deleting pin set');
        }
        return deleteFloidIdResponse.body.toLowerCase() == 'true';
      }
    }
    throw Exception('in deleteFloidId');
  }

  // Commands:
  /// Perform an acquire home position command:
  Future<FloidServerCommandResult> performAcquireHomePositionCommand(int floidId,
      double requiredXYDistance,
      double requiredAltitudeDistance,
      double requiredHeadingDistance,
      int requiredGPSGoodCount,
      int requiredAltimeterGoodCount) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_ACQUIRE_HOME_POSITION_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_REQUIRED_XY_DISTANCE': requiredXYDistance.toString(),
      '$PARAMETER_REQUIRED_ALTITUDE_DISTANCE': requiredAltitudeDistance.toString(),
      '$PARAMETER_REQUIRED_HEADING_DISTANCE': requiredHeadingDistance.toString(),
      '$PARAMETER_REQUIRED_GPS_GOOD_COUNT': requiredGPSGoodCount.toString(),
      '$PARAMETER_REQUIRED_ALTIMETER_GOOD_COUNT': requiredAltimeterGoodCount.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performAcquireHomePositionCommand = " +
              response.statusCode.toString());
          throw Exception('error in performAcquireHomePositionCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing acquire home position command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performAcquireHomePositionCommand');
  }

  /// Perform a payload command:
  Future<FloidServerCommandResult> performPayloadCommand(int floidId,
      int bay) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_PAYLOAD_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_BAY': bay.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performPayloadCommand = " + response.statusCode.toString());
          throw Exception('error in performPayloadCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing payload command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performPayloadCommand');
  }

  /// Perform a debug command:
  Future<FloidServerCommandResult> performDebugCommand(int floidId,
      int deviceIndex, int deviceOn) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_DEBUG_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_DEVICE_INDEX': deviceIndex.toString(),
      '$PARAMETER_DEVICE_ON': deviceOn.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performDebugCommand = " + response.statusCode.toString());
          throw Exception('error in performDebugCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing debug command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performDebugCommand');
  }

  /// Perform a delay command:
  Future<FloidServerCommandResult> performDelayCommand(int floidId,
      int delayTime) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_DELAY_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_DELAY_TIME': delayTime.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performDelayCommand = " + response.statusCode.toString());
          throw Exception('error in performDelayCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing delay command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performDelayCommand');
  }

  /// Perform a retrieve logs command:
  Future<FloidServerCommandResult> performRetrieveLogsCommand(int floidId,
      int logLimit) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_RETRIEVE_LOGS_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_LOG_LIMIT': logLimit.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performRetrieveLogsCommand = " + response.statusCode.toString());
          throw Exception('error in performRetrieveLogsCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing retrieve logs command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performRetrieveLogsCommand');
  }

  /// Perform a deploy parachute command:
  Future<FloidServerCommandResult> performDeployParachuteCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_DEPLOY_PARACHUTE_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print(
              "error: performDeployParachuteCommand = " + response.statusCode.toString());
          throw Exception('error in performDeployParachuteCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing deploy parachute command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performDeployParachuteCommand');
  }

  /// Perform a designate command:
  Future<FloidServerCommandResult> performDesignateCommand(int floidId,
      double latitude, double longitude, double height) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_DESIGNATE_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_LATITUDE': latitude.toString(),
      '$PARAMETER_LONGITUDE': longitude.toString(),
      '$PARAMETER_HEIGHT': height.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performDesignateCommand = " + response.statusCode.toString());
          throw Exception('error in performDesignateCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing designate command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performDesignateCommand');
  }

  /// Perform a fly to command:
  Future<FloidServerCommandResult> performFlyToCommand(int floidId,
      double latitude, double longitude, bool flyToHome) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_FLY_TO_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_LATITUDE': latitude.toString(),
      '$PARAMETER_LONGITUDE': longitude.toString(),
      '$PARAMETER_FLY_TO_HOME': flyToHome.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performFlyToCommand = " + response.statusCode.toString());
          throw Exception('error in performFlyToCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing fly to command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performHeightToCommand');
  }

  /// Perform a height to command:
  Future<FloidServerCommandResult> performHeightToCommand(int floidId,
      bool absolute, double height) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_HEIGHT_TO_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_ABSOLUTE': absolute.toString(),
      '$PARAMETER_HEIGHT': height.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performHeightToCommand = " + response.statusCode.toString());
          throw Exception('error in performHeightToCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Client? fHttpClient = this.httpClient;
        if (fHttpClient != null) {
          final http.Response commandResponse = await fHttpClient.post(
              Uri.parse(commandUrl),
              headers: {HttpHeaders.authorizationHeader: authenticationToken},
              body: data);
          if (commandResponse.statusCode != 200) {
            print("error: commandResponse = " + commandResponse.body);
            throw Exception('error performing height to command');
          }
          return FloidServerCommandResult.fromJson(
              jsonDecode(commandResponse.body));
        }
      }
    }
    throw Exception('in performHeightToCommand');
  }

  /// Perform a hover command:
  Future<FloidServerCommandResult> performHoverCommand(int floidId,
      int time) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_HOVER_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_TIME': time.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performHoverCommand = " + response.statusCode.toString());
          throw Exception('error in performHoverCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing hover command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performHoverCommand');
  }


  /// Perform a land command:
  Future<FloidServerCommandResult> performLandCommand(int floidId,
      double z) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_LAND_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_Z': z.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performLandCommand = " + response.statusCode.toString());
          throw Exception('error in performLandCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing land command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performLandCommand');
  }

  /// Perform a lift off command:
  Future<FloidServerCommandResult> performLiftOffCommand(int floidId,
      bool absolute, int z) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_LIFT_OFF_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_ABSOLUTE': absolute.toString(),
      '$PARAMETER_Z': z.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performLiftOffCommand = " + response.statusCode.toString());
          throw Exception('error in performLiftOffCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response? commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse != null && commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing lift off command');
        }
        if (commandResponse != null) return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performLiftOffCommand');
  }

  /// Perform a null command:
  Future<FloidServerCommandResult> performNullCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_NULL_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performNullCommand = " + response.statusCode.toString());
          throw Exception('error in performNullCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing null command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performNullCommand');
  }

  /// Perform a pan/tilt command:
  Future<FloidServerCommandResult> performPanTiltCommand(int floidId,
      bool angleMode, double latitude, double longitude, double height, double pan,
      double tilt) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_PAN_TILT_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_ANGLE_MODE': angleMode.toString(),
      '$PARAMETER_LATITUDE': latitude.toString(),
      '$PARAMETER_LONGITUDE': longitude.toString(),
      '$PARAMETER_HEIGHT': height.toString(),
      '$PARAMETER_PAN': pan.toString(),
      '$PARAMETER_TILT': tilt.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performPanTiltCommand = " + response.statusCode.toString());
          throw Exception('error in performPanTiltCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing pan/tilt command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performPanTiltCommand');
  }

  /// Perform a set parameters command:
  Future<FloidServerCommandResult> performSetParametersCommand(int floidId,
      String droidParameters, bool resetDroidParameters) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_SET_PARAMETERS_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_DROID_PARAMETERS': droidParameters,
      '$PARAMETER_RESET_DROID_PARAMETERS': resetDroidParameters.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performSetParametersCommand = " + response.statusCode.toString());
          throw Exception('error in performSetParametersCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing set parameters command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performSetParametersCommand');
  }

  /// Perform a shut down helis command:
  Future<FloidServerCommandResult> performShutDownHelisCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_SHUT_DOWN_HELIS_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performShutDownHelisCommand = " + response.statusCode.toString());
          throw Exception('error in performShutDownHelisCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing shut down helis command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performShutDownHelisCommand');
  }

  /// Perform a speaker command:
  Future<FloidServerCommandResult> performSpeakerCommand(int floidId,
      bool alert, String mode, String tts, double pitch, double rate, String fileName,
      double volume, int speakerRepeat, int delay) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_SPEAKER_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_ALERT': alert.toString(),
      '$PARAMETER_MODE': mode,
      '$PARAMETER_TTS': tts,
      '$PARAMETER_PITCH': pitch.toString(),
      '$PARAMETER_RATE': rate.toString(),
      '$PARAMETER_FILE_NAME': fileName,
      '$PARAMETER_VOLUME': volume.toString(),
      '$PARAMETER_SPEAKER_REPEAT': speakerRepeat.toString(),
      '$PARAMETER_DELAY': delay.toString(),
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performSpeakerCommand = " + response.statusCode.toString());
          throw Exception('error in performSpeakerCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing speaker command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performSpeakerCommand');
  }

  /// Perform a speaker on command:
  Future<FloidServerCommandResult> performSpeakerOnCommand(int floidId,
      bool alert, String mode, String tts, double pitch, double rate, String fileName,
      double volume, int speakerRepeat, int delay) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_SPEAKER_ON_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_ALERT': alert.toString(),
      '$PARAMETER_MODE': mode,
      '$PARAMETER_TTS': tts,
      '$PARAMETER_PITCH': pitch.toString(),
      '$PARAMETER_RATE': rate.toString(),
      '$PARAMETER_FILE_NAME': fileName,
      '$PARAMETER_VOLUME': volume.toString(),
      '$PARAMETER_SPEAKER_REPEAT': speakerRepeat.toString(),
      '$PARAMETER_DELAY': delay.toString(),
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performSpeakerOnCommand = " + response.statusCode.toString());
          throw Exception('error in performSpeakerOnCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing speaker on command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performSpeakerOnCommand');
  }

  /// Perform a speaker off command:
  Future<FloidServerCommandResult> performSpeakerOffCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_SPEAKER_OFF_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performSpeakerOffCommand = " + response.statusCode.toString());
          throw Exception('error in performSpeakerOffCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing speaker off command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performSpeakerOffCommand');
  }

  /// Perform a start up helis off command:
  Future<FloidServerCommandResult> performStartUpHelisCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_START_UP_HELIS_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performStartUpHelisCommand = " + response.statusCode.toString());
          throw Exception('error in performStartUpHelisCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing start up helis command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performStartUpHelisCommand');
  }

  /// Perform a start video off command:
  Future<FloidServerCommandResult> performStartVideoCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_START_VIDEO_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performStartVideoCommand = " + response.statusCode.toString());
          throw Exception('error in performStartVideoCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing start video command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performStartVideoCommand');
  }

  /// Perform a stop designating command:
  Future<FloidServerCommandResult> performStopDesignatingCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_STOP_DESIGNATING_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print(
              "error: performStopDesignatingCommand = " + response.statusCode.toString());
          throw Exception('error in performStopDesignatingCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing stop designating command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performStopDesignatingCommand');
  }

  /// Perform a stop video command:
  Future<FloidServerCommandResult> performStopVideoCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_STOP_VIDEO_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performStopVideoCommand = " + response.statusCode.toString());
          throw Exception('error in performStopVideoCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing stop video command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performStopVideoCommand');
  }


  /// Perform a photo strobe command:
  Future<FloidServerCommandResult> performPhotoStrobeCommand(int floidId,
      int delay) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_PHOTO_STROBE_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_DELAY': delay.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performPhotoStrobeCommand = " + response.statusCode.toString());
          throw Exception('error in performPhotoStrobeCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing photo strobe command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performPhotoStrobeCommand');
  }

  /// Perform a stop photo strobe command:
  Future<FloidServerCommandResult> performStopPhotoStrobeCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_STOP_PHOTO_STROBE_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print(
              "error: performStopPhotoStrobeCommand = " + response.statusCode.toString());
          throw Exception('error in performStopPhotoStrobeCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing stop photo strobe command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performStopPhotoStrobeCommand');
  }

  /// Perform a take photo command:
  Future<FloidServerCommandResult> performTakePhotoCommand(int floidId) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_TAKE_PHOTO_COMMAND';
    print("performTakePhotoCommand Posting to: $commandUrl");
    Map<String, String> data = { '$PARAMETER_FLOID_ID': floidId.toString()};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performTakePhotoCommand = " + response.statusCode.toString());
          throw Exception('error in performTakePhotoCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      print('performTakePhotoCommand - posting...');
      try {
        final http.Client? fHttpClient = this.httpClient;
        if (fHttpClient != null) {
          final http.Response commandResponse = await fHttpClient.post(
              Uri.parse(
                  commandUrl),
              headers: {HttpHeaders.authorizationHeader: authenticationToken},
              body: data);
          print('performTakePhotoCommand - receiving...');
          if (commandResponse.statusCode != 200) {
            print("performTakePhotoCommand error: commandResponse = " +
                commandResponse.body);
            throw Exception('error performing take photo command');
          }
          return FloidServerCommandResult.fromJson(
              jsonDecode(commandResponse.body));
        }
      } catch (e) {
        print('performTakePhotoCommand Caught error: ' + e.toString());
      }
      return FloidServerCommandResult();
    }
    throw Exception('in performTakePhotoCommand');
  }


  /// Perform a turn to command:
  Future<FloidServerCommandResult> performTurnToCommand(int floidId,
      bool followMode, double heading) async {
    final String commandUrl = '$baseUrl/$URL_PERFORM_TURN_TO_COMMAND';
    print("Posting to: $commandUrl");
    Map<String, String> data = {
      '$PARAMETER_FLOID_ID': floidId.toString(),
      '$PARAMETER_FOLLOW_MODE': followMode.toString(),
      '$PARAMETER_HEADING': heading.toString()
    };
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(commandUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: performTurnToCommand = " + response.statusCode.toString());
          throw Exception('error in performTurnToCommand');
        }
        String reply = await response.transform(utf8.decoder).join();
        return FloidServerCommandResult.fromJson(jsonDecode(reply));
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response commandResponse = await fHttpClient.post(
            Uri.parse(commandUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (commandResponse.statusCode != 200) {
          print("error: commandResponse = " + commandResponse.body);
          throw Exception('error performing turn to command');
        }
        return FloidServerCommandResult.fromJson(
            jsonDecode(commandResponse.body));
      }
    }
    throw Exception('in performTurnToCommand');
  }

  /// Fetch a download token:
  Future<String> fetchDownloadToken(int id) async {
    final String fetchDownloadTokenUrl = '$baseUrl/$URL_CREATE_DOWNLOAD_TOKEN';
    print("Calling: $fetchDownloadTokenUrl");
    Map<String, String> data = { '$PARAMETER_FILE_ID': '$id'};
    if (!kIsWeb) {
      final HttpClient? fHttpAppClient = httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(
            Uri.parse(fetchDownloadTokenUrl));
        request.headers.set(
            HttpHeaders.authorizationHeader, authenticationToken);
        addFormFieldsToRequest(request, data);
        HttpClientResponse response = await request.close();
        if (response.statusCode != 200) {
          print("error: create download token response = " + response.statusCode.toString());
          throw Exception('error getting download token');
        }
        String reply = await response.transform(utf8.decoder).join();
        return reply;
      }
    } else {
      final http.Client? fHttpClient = this.httpClient;
      if (fHttpClient != null) {
        final http.Response userResponse = await fHttpClient.post(
            Uri.parse(fetchDownloadTokenUrl),
            headers: {HttpHeaders.authorizationHeader: authenticationToken},
            body: data);
        if (userResponse.statusCode != 200) {
          throw Exception('error getting download token');
        }
        return userResponse.body;
      }
    }
    throw Exception('in fetchDownloadToken');
  }
}
