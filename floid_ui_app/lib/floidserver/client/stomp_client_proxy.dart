/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/trust/floid_server_trust.dart';
import 'package:stomp/stomp.dart';

import 'stomp_client_stub.dart'
  if (dart.library.js) 'websocket_stomp_client_proxy.dart'
  if (dart.library.io) 'app_stomp_client_proxy.dart';


abstract class StompClientProxy {

  Future<StompClient> connect(FloidServerTrust floidServerTrust,
      String url,
      {String? host,
        String? login,
        String? passcode,
        String authorization,
        List<int>? heartbeat,
        void onConnect(StompClient client, Map<String, String>? headers)?,
        void onDisconnect(StompClient client)?,
        void onError(StompClient client, String? message, String? detail,
            Map<String, String>? headers)?,
        void onFault(StompClient client, error, stackTrace)?});

  /// factory constructor to return the correct implementation.
  factory StompClientProxy() => getStompClientProxy();
}
