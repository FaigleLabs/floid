/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:collection';
import 'dart:convert';
import 'dart:async';
import 'dart:io';

import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/keys/keys.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_elevation_request.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_path_elevation.dart';
import 'package:floid_ui_app/ui/map/elevation/floid_path_elevation_request.dart';
import 'package:floid_ui_app/ui/map/line/floid_map_fly_line.dart';
import 'package:floid_ui_app/ui/map/line/floid_map_line.dart';
import 'package:latlong2/latlong.dart';
import 'package:http/http.dart' as http;


import 'package:flutter/foundation.dart';

class FloidElevationApiClient {

  static Map<String, double> pathElevationsCache = new HashMap<String, double>();
  static Map<String, double> singleElevationCache = new HashMap<String, double>();

  static const int PATH_ELEVATION_MAX_SAMPLES = 50;
  static const int PATH_ELEVATION_REQUESTED_SEGMENT_LENGTH = 50;
  static const double SERVER_FAIL_ELEVATION = -32000.0;

  final FloidServerApiClient floidServerApiClient;

  /// Create the floid server api client:
  FloidElevationApiClient({required this.floidServerApiClient}) : super();

  String _makeSingleElevationIndexString(FloidElevationRequest floidElevationRequest) {
    return floidElevationRequest.floidPinAlt.pinLat.toString() + "_" + floidElevationRequest.floidPinAlt.pinLng.toString();
  }

  /// Fetch single elevation:
  Future<FloidElevationRequest> fetchElevation(FloidElevationRequest floidElevationRequest, bool secure, String elevationLocation) async {
    try {
      // 1. Try to get from cache:
      String singleElevationIndexString = _makeSingleElevationIndexString(floidElevationRequest);
      if (singleElevationCache.containsKey(singleElevationIndexString)) {
        final double? cachedElevation = singleElevationCache[singleElevationIndexString];
        floidElevationRequest.floidPinAlt.pinHeight = cachedElevation == null ? SERVER_FAIL_ELEVATION : cachedElevation;
        if (floidElevationRequest.floidMapMarker != null) {
          floidElevationRequest.floidMapMarker?.markerElevation = floidElevationRequest.floidPinAlt.pinHeight;
        }
        if (floidElevationRequest.floidPinAlt.pinHeight != SERVER_FAIL_ELEVATION) {
          floidElevationRequest.result = FloidPathElevation.RESULT_SUCCESS;
        } else {
          floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
        }
        return floidElevationRequest;
      }
      // 2. Get from network:
      floidElevationRequest.result = FloidPathElevation.RESULT_UNKNOWN;
      // Make the request:
      StringBuffer locationsString = StringBuffer('{\n"locations":\n    [\n');
      locationsString.write('        {\n            "latitude":'
          + floidElevationRequest.floidPinAlt.pinLat.toString()
          + ',\n            "longitude":'
          + floidElevationRequest.floidPinAlt.pinLng.toString()
          + '\n        }');
      locationsString.write('\n    ]\n}\n');
      print('Location String: ' + locationsString.toString());
      Map<String, String> data = { 'locations': locationsString.toString()};
      // Local server or open-elevation - these are the only two implemented so we simplify to a bool:
      bool localServer = (elevationLocation == FloidAppStateAttributes.elevationLocations[0]);
      // Make the request:
      final String requestUrl =
      localServer ? floidServerApiClient.localElevationUrl + FloidKeys.floidLocalElevationUrl
          : (secure ? 'https://' : 'http://') + FloidKeys.floidOpenElevationUrl;
      print("Requesting elevation data from: $requestUrl");
      if (!kIsWeb) {
        print('fetchElevation - non-web version');
        final HttpClient? fHttpAppClient = floidServerApiClient.httpAppClient;
        if (fHttpAppClient != null) {
          HttpClientRequest request = await fHttpAppClient.postUrl(Uri.parse(requestUrl));
          if (localServer) {
            // Add auth:
            request.headers.set(HttpHeaders.authorizationHeader, floidServerApiClient.authenticationToken);
            FloidServerApiClient.addFormFieldsToRequest(request, data);
          } else {
            request.headers.contentType = ContentType.json;
            request.add(utf8.encode(locationsString.toString()));
          }
          HttpClientResponse response = await request.close();
          // Handle the response:
          if (response.statusCode != 200) {
            // Bad response:
            floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
            print('Elevation request response: ' + response.statusCode.toString());
          } else {
            // Process the request:
            try {
              String reply = await response.transform(utf8.decoder).join();
              Map<String, dynamic> elevationResponseMap = json.decode(reply);
              final List<dynamic>? elevationProfile = elevationResponseMap['results'];
              if (elevationProfile == null || elevationProfile.length == 0) {
                floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
              } else {
                // Good result process it:
                double elevation = (elevationProfile[0]['elevation'] as num).toDouble();
                if (elevation == SERVER_FAIL_ELEVATION) {
                  print('FetchElevation - non-web - Server fail');
                  floidElevationRequest.floidPinAlt.pinHeight = elevation.toDouble();
                  floidElevationRequest.floidMapMarker?.markerElevation = elevation.toDouble();
                  floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
                } else {
                  floidElevationRequest.floidPinAlt.pinHeight = elevation.toDouble();
                  floidElevationRequest.floidMapMarker?.markerElevation = elevation.toDouble();
                  floidElevationRequest.result = FloidPathElevation.RESULT_SUCCESS;
                }
              }
            } catch (e, stacktrace) {
              floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
              print('Exception processing single elevation request: ' + e.toString() + ' ' + stacktrace.toString());
            }
          }
        } else {
          print('fetchElevation: HttpAppClient was null');
        }
      } else {
        print('fetchElevation - web version');
        final http.Client? fHttpClient = floidServerApiClient.httpClient;
        if (fHttpClient != null) {
          // Send the request:
          final http.Response response = await fHttpClient.post(Uri.parse(requestUrl),
            body: localServer ? data : locationsString.toString(),
            headers: {
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              if(!localServer) HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              if(localServer) HttpHeaders.authorizationHeader: floidServerApiClient.authenticationToken,
            },);
          // Handle the response:
          if (response.statusCode != 200) {
            // Bad response:
            floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
            print('Elevation request response: ' + response.statusCode.toString());
          } else {
            // Process the request:
            try {
              Map<String, dynamic> elevationResponseMap = json.decode(response.body);
              final List<dynamic>? elevationProfile = elevationResponseMap['results'];
              if (elevationProfile == null || elevationProfile.length == 0) {
                floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
              } else {
                // Good result process it:
                double elevation = (elevationProfile[0]['elevation'] as num).toDouble();
                if (elevation == SERVER_FAIL_ELEVATION) {
                  print('FetchElevation - web - Server fail');
                  floidElevationRequest.floidPinAlt.pinHeight = elevation.toDouble();
                  floidElevationRequest.floidMapMarker?.markerElevation = elevation.toDouble();
                  floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
                } else {
                  floidElevationRequest.floidPinAlt.pinHeight = elevation.toDouble();
                  floidElevationRequest.floidMapMarker?.markerElevation = elevation.toDouble();
                  floidElevationRequest.result = FloidPathElevation.RESULT_SUCCESS;
                }
              }
            } catch (e) {
              floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
              print('Exception processing single elevation request: ' + e.toString());
            }
          }
        } else {
          print('fetchElevation: HttpClient was null');
        }
      }
    } catch(_e, stacktrace) {
      print('Error fetching elevation' + stacktrace.toString());
      floidElevationRequest.result = FloidPathElevation.RESULT_FAIL;
      floidElevationRequest.floidMapMarker?.markerElevation = SERVER_FAIL_ELEVATION;
      floidElevationRequest.floidPinAlt.pinHeight = SERVER_FAIL_ELEVATION;
    }
    return floidElevationRequest;
  }

  // Return the path elevation request modified from cache or null if not in cache:
  static FloidPathElevationRequest? fetchPathElevationFromCache(FloidPathElevationRequest floidPathElevationRequest) {
    FloidMapFlyLine floidMapFlyLine = floidPathElevationRequest.floidMapFlyLine;
    if(floidMapFlyLine.floidPathElevation==null) {
      floidMapFlyLine.floidPathElevation = FloidPathElevation(floidMapFlyLine: floidPathElevationRequest.floidMapFlyLine, pathElevationStartLat: 0, pathElevationStartLng: 0, pathElevationEndLat: 0, pathElevationEndLng: 0, pathElevationMaxNumberSamples: 0, pathElevationActualNumberSamples: 0, pathElevationRequestedSegmentLength: 0, pathElevationActualSegmentLength: 0, pathElevationMax: 0, pathElevationValid: false, pathElevationResult: FloidPathElevation.RESULT_UNKNOWN, toolTip: '');
    }
    final FloidPathElevation? floidPathElevation = floidMapFlyLine.floidPathElevation;
    String floidMapLineCacheIndexForward = _makeMapLineIndexStringForward(floidMapFlyLine);
    if (floidPathElevation!=null && pathElevationsCache.containsKey(floidMapLineCacheIndexForward)) {
      floidPathElevationRequest.valid = true;
      floidPathElevation.pathElevationValid = true;
      final double? pathElevationFromCache = pathElevationsCache[floidMapLineCacheIndexForward];
      floidPathElevation.pathElevationMax = pathElevationFromCache==null?0.0:pathElevationFromCache;
      if(pathElevationFromCache !=null) {
        floidPathElevation.pathElevationResult = FloidPathElevation.RESULT_SUCCESS;
      } else {
        floidPathElevation.pathElevationResult = FloidPathElevation.RESULT_FAIL;
      }
      floidPathElevationRequest.result = FloidPathElevation.RESULT_SUCCESS;
      return floidPathElevationRequest;
    } else {
      String floidMapLineCacheIndexReverse = _makeMapLineIndexStringReverse(floidMapFlyLine);
      if (floidPathElevation!=null && pathElevationsCache.containsKey(floidMapLineCacheIndexReverse)) {
        floidPathElevationRequest.valid = true;
        floidPathElevation.pathElevationValid = true;
        final double? pathElevationFromCache = pathElevationsCache[floidMapLineCacheIndexReverse];
        floidPathElevation.pathElevationMax = pathElevationFromCache==null?0.0:pathElevationFromCache;
        if(pathElevationFromCache !=null) {
          floidPathElevation.pathElevationResult = FloidPathElevation.RESULT_SUCCESS;
        } else {
          floidPathElevation.pathElevationResult = FloidPathElevation.RESULT_FAIL;
        }
        floidPathElevationRequest.result = FloidPathElevation.RESULT_SUCCESS;
        return floidPathElevationRequest;
      }
    }
    return null; // Not in cache
  }

  /// Fetch path elevation:
  Future<FloidPathElevationRequest> fetchPathElevation(FloidPathElevationRequest floidPathElevationRequest, bool secure, int delay, String elevationLocation) async {
    print('fetchPathElevation: delay of ' + delay.toString());
    FloidPathElevationRequest? cachedPathElevation = fetchPathElevationFromCache(floidPathElevationRequest);
    if(cachedPathElevation!=null) {
      return floidPathElevationRequest; // Now filled in...
    }
    floidPathElevationRequest.result = FloidPathElevation.RESULT_UNKNOWN;
    FloidMapFlyLine floidMapFlyLine = floidPathElevationRequest.floidMapFlyLine;
    // Make the index strings for future caching (we will go ahead and cache it both ways...)
    String floidMapLineCacheIndexForward = _makeMapLineIndexStringForward(floidMapFlyLine);
    String floidMapLineCacheIndexReverse = _makeMapLineIndexStringReverse(floidMapFlyLine);
    // Calculate the elevation samples for this path:
    List<LatLng> samples = _calculateSamples(floidMapFlyLine.startLat, floidMapFlyLine.startLng, floidMapFlyLine.endLat, floidMapFlyLine.endLng, PATH_ELEVATION_MAX_SAMPLES);
    // Create POST json locations string:
    StringBuffer locationsString = StringBuffer('{\n"locations":\n    [\n');
    bool firstLocation = true;
    samples.forEach((latLng) {
      locationsString.write((firstLocation?'        ':',\n        ') + '{\n            "latitude":' + latLng.latitude.toString() + ',\n            "longitude":' + latLng.longitude.toString() + '\n        }');
      firstLocation = false;
    });
    locationsString.write('\n    ]\n}\n');
    // print('Locations String: ' + locationsString.toString());
    // Local server or open-elevation - these are the only two implemented so we simplify to a bool:
    bool localServer = (elevationLocation == FloidAppStateAttributes.elevationLocations[0]);
    // Make the request:
    final String requestUrl =
        localServer?floidServerApiClient.localElevationUrl + FloidKeys.floidLocalElevationUrl
            : (secure ? 'https://' : 'http://') + FloidKeys.floidOpenElevationUrl;
    print("Requesting elevation data from: $requestUrl");
    // Send the request:
    Map<String, String> data = { 'locations': locationsString.toString()};
    // Assume start success:
    floidPathElevationRequest.result = FloidPathElevation.RESULT_SUCCESS;
    floidPathElevationRequest.valid = true;
    if(!kIsWeb) {
      print('fetchPathElevation - non-web version');
      final HttpClient? fHttpAppClient = floidServerApiClient.httpAppClient;
      if (fHttpAppClient != null) {
        HttpClientRequest request = await fHttpAppClient.postUrl(Uri.parse(requestUrl));
        if(localServer) {
          // Add auth:
          request.headers.set(HttpHeaders.authorizationHeader, floidServerApiClient.authenticationToken);
          FloidServerApiClient.addFormFieldsToRequest(request, data);
        } else {
          request.headers.contentType = ContentType.json;
          // Open elevation
          request.add(utf8.encode(locationsString.toString()));
        }
        // Delay 2.1 seconds between each call for open elevation:
        await Future.delayed(Duration(milliseconds: delay * 2100), () async {
          final HttpClientResponse response = await request.close();
          // Handle the response:
          if (response.statusCode != 200) {
            // Bad response:
            print('Elevation request error status: ' + response.statusCode.toString());
            floidPathElevationRequest.result = FloidPathElevation.RESULT_FAIL;
            floidPathElevationRequest.valid = false;
          } else {
            // Process the request:
            try {
              String reply = await response.transform(utf8.decoder).join();
              // print('Elevation response: ' + reply);
              Map<String, dynamic> elevationResponseMap = json.decode(reply);
              final List<dynamic>? elevationProfile = elevationResponseMap['results'];
              if (elevationProfile == null || elevationProfile.length == 0) {
                floidPathElevationRequest.result = FloidPathElevation.RESULT_FAIL;
              } else {
                double height = 0;
                elevationProfile.forEach((value) {
                  double elevation = (value['elevation'] as num).toDouble();
                  // Make sure it is a valid elevation:
                  if(elevation == SERVER_FAIL_ELEVATION) {
                    print('FetchPahElevation - non-web - Server fail');
                    floidPathElevationRequest.result = FloidPathElevation.RESULT_FAIL;
                    floidPathElevationRequest.valid = false;
                    floidPathElevationRequest.floidMapFlyLine.altitudeStatus = FloidMapLine.ALTITUDE_STATUS_ERROR;
                    return;
                  }
                  if (elevation > height) {
                    height = elevation;
                  }
                });
                final FloidPathElevation? fFloidPathElevation = floidMapFlyLine.floidPathElevation;
                FloidPathElevation floidPathElevation = fFloidPathElevation == null ? FloidPathElevation() : fFloidPathElevation;
                floidPathElevation.pathElevationMax = height.toDouble();
                floidPathElevation.pathElevationResult = floidPathElevationRequest.result;
                floidPathElevation.pathElevationValid = floidPathElevationRequest.valid;
                // Cache the result (both forwards and backwards:
                pathElevationsCache[floidMapLineCacheIndexForward] = height;
                pathElevationsCache[floidMapLineCacheIndexReverse] = height;
              }
            } catch (e, stacktrace) {
              print(e.toString() + ' ' + stacktrace.toString());
              floidPathElevationRequest.result = FloidPathElevation.RESULT_FAIL;
            }
          }
        });
      } else {
        print('fetchPathElevation: HttpAppClient was null');
      }
    } else {
      print('fetchPathElevation - web version');
      final http.Client? fHttpClient = floidServerApiClient.httpClient;
      if (fHttpClient != null) {
        // Delay 2.1 seconds between each call if open elevation:
        await Future.delayed(Duration(milliseconds: delay * 2100), () async {
          final http.Response response = await fHttpClient.post(Uri.parse(requestUrl),
            body: localServer ? data : locationsString.toString(),
            headers:{
              HttpHeaders.acceptHeader: ContentType.json.toString(),
              if(!localServer) HttpHeaders.contentTypeHeader: ContentType.json.toString(),
              if(localServer) HttpHeaders.authorizationHeader: floidServerApiClient.authenticationToken,
            },);
          // Handle the response:
          if (response.statusCode != 200) {
            // Bad response:
            print('Elevation request error status: ' + response.statusCode.toString());
            floidPathElevationRequest.result = FloidPathElevation.RESULT_FAIL;
          } else {
            // Process the request:
            try {
              print('Elevation response: ' + response.body);
              Map<String, dynamic> elevationResponseMap = json.decode(response.body);
              final List<dynamic>? elevationProfile = elevationResponseMap['results'];
              if (elevationProfile == null || elevationProfile.length == 0) {
                floidPathElevationRequest.result = FloidPathElevation.RESULT_FAIL;
                floidPathElevationRequest.valid = false;
              } else {
                double height = 0;
                elevationProfile.forEach((value) {
                  double elevation = (value['elevation'] as num).toDouble();
                  // print('Elevation: ' + elevation.toString());
                  // Make sure it is a valid elevation:
                  if(elevation == SERVER_FAIL_ELEVATION) {
                    print('FetchPahElevation - web - Server fail');
                    floidPathElevationRequest.result = FloidPathElevation.RESULT_FAIL;
                    floidPathElevationRequest.valid = false;
                    floidPathElevationRequest.floidMapFlyLine.altitudeStatus = FloidMapLine.ALTITUDE_STATUS_ERROR;
                  }
                  if (elevation > height) height = elevation;
                });
                final FloidPathElevation? fFloidPathElevation = floidMapFlyLine.floidPathElevation;
                FloidPathElevation floidPathElevation = fFloidPathElevation == null ? FloidPathElevation() : fFloidPathElevation;
                floidPathElevation.pathElevationMax = height.toDouble();
                floidPathElevation.pathElevationResult = floidPathElevationRequest.result;
                floidPathElevation.pathElevationValid = floidPathElevationRequest.valid;
                // Cache the result (both forwards and backwards:
                pathElevationsCache[floidMapLineCacheIndexForward] = height;
                pathElevationsCache[floidMapLineCacheIndexReverse] = height;
              }
            } catch (e, stacktrace) {
              print(e.toString() + ' ' + stacktrace.toString());
              floidPathElevationRequest.result = FloidPathElevation.RESULT_FAIL;
            }
          }
        });
      } else {
        print('fetchPathElevation: HttpClient was null');
      }
    }
    return floidPathElevationRequest;
  }

  List<LatLng> _calculateSamples(double startLat, double startLng, double endLat, double endLng, int actualSamples) {
    // OK, make an array of sample points to test:
    List<LatLng> sampleLocations = [];
    var i;
    var factor;
    var sampleLat;
    var sampleLng;
    for (i = 0; i < actualSamples; i += 1) {
      factor = i / (actualSamples - 1);    // Note - again we are including both start and end points...
      sampleLat = startLat + (endLat - startLat) * factor;
      sampleLng = startLng + (endLng - startLng) * factor;
      sampleLocations.add(LatLng(sampleLat, sampleLng));
    }
    return sampleLocations;
  }

  static String _makeMapLineIndexStringForward(FloidMapLine floidMapLine) {
    return floidMapLine.startLat.toString()
        + "-" + floidMapLine.startLng.toString()
        + "-" + floidMapLine.endLat.toString()
        + "-" + floidMapLine.endLng.toString()
        + "-" + PATH_ELEVATION_MAX_SAMPLES.toString()
        + "-" + PATH_ELEVATION_REQUESTED_SEGMENT_LENGTH.toString();

  }
  static String _makeMapLineIndexStringReverse(FloidMapLine floidMapLine) {
    return floidMapLine.endLat.toString()
        + "-" + floidMapLine.endLng.toString()
        + "-" + floidMapLine.startLat.toString()
        + "-" + floidMapLine.startLng.toString()
        + "-" + PATH_ELEVATION_MAX_SAMPLES.toString()
        + "-" + PATH_ELEVATION_REQUESTED_SEGMENT_LENGTH.toString();
  }
}
