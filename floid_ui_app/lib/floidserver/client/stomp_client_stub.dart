/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'stomp_client_proxy.dart';

StompClientProxy getStompClientProxy() => throw UnsupportedError(
    'This is only a stub for the analysis tools');