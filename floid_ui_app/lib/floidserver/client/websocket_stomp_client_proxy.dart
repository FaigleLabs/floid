/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/trust/floid_server_trust.dart';
import 'package:stomp/stomp.dart';
import 'package:floid_ui_app/floidserver/client/stomp_client_proxy.dart';
// If browser we use websocket:
import 'package:stomp/websocket.dart' as websocket ;

class WebsocketStompClientProxy implements StompClientProxy {

  Future<StompClient> connect(FloidServerTrust floidServerTrust,
      String url,
      {String? host,
        String? login,
        String? passcode,
        String authorization='',
        List<int>? heartbeat,
        void onConnect(StompClient client, Map<String, String>? headers)?,
        void onDisconnect(StompClient client)?,
        void onError(StompClient client, String? message, String? detail,
            Map<String, String>? headers)?,
        void onFault(StompClient client, error, stackTrace)?}) =>
      websocket.connect(
          floidServerTrust.badCertificateCallback,
          url,
          host: host,
          login: login,
          passcode: passcode,
          authorization: authorization,
          heartbeat: heartbeat,
          onConnect: onConnect,
          onDisconnect: onDisconnect,
          onError: onError,
          onFault: onFault);
}
StompClientProxy getStompClientProxy() => WebsocketStompClientProxy();