/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/ui/map/marker/floid_pin_attributes.dart';

class FloidPinAlt {

  FloidPinAlt({
    this.pinName = '',
    this.pinLat=0,
    this.pinLng=0,
    this.pinHeight=FloidElevationApiClient.SERVER_FAIL_ELEVATION,
    this.pinHeightAuto=false});

  /// The Id.
  int id = -1;

  /// The Pin name.
  String pinName = '';

  /// The Pin lat.
  double pinLat = 0.0;

  /// The Pin lng.
  double pinLng = 0.0;

  /// The Pin height.
  double pinHeight = FloidElevationApiClient.SERVER_FAIL_ELEVATION;

  /// The Pin height auto.
  bool pinHeightAuto = false;

  /// The pin attributes once processed
  FloidPinAttributes floidPinAttributes = FloidPinAttributes();

  FloidPinAlt.fromJson(Map<String, dynamic> json)
      : id = json['id']==null?-1:json['id'],
        pinName = json['pinName']==null?0:json['pinName'],
        pinLat = DroidInstruction.jsonDouble(json,'pinLat'),
        pinLng = DroidInstruction.jsonDouble(json,'pinLng'),
        pinHeight = DroidInstruction.jsonDouble(json,'pinHeight'),
        pinHeightAuto = json['pinHeightAuto']==null?false:json['pinHeightAuto'];

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {
      'id': id,
      'pinName': pinName,
      'pinLat': pinLat,
      'pinLng': pinLng,
      'pinHeight': pinHeight,
      'pinHeightAuto': pinHeightAuto,
    };
    return json;
  }
}
