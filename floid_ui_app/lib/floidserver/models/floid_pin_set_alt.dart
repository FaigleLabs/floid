/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/models/floid_pin_alt.dart';

class FloidPinSetAlt {

  FloidPinSetAlt(
      {this.id = NO_PIN_SET, this.pinSetName = '', this.pinAltList = const <
          FloidPinAlt>[]});

  /// The no pin set constant:
  static const int NO_PIN_SET = -1;

  /// The id:
  int id;

  /// The pin set name:
  String pinSetName;

  /// The list of pins
  List<FloidPinAlt> pinAltList = <FloidPinAlt>[];

  /// Get the droid instruction from JSON
  FloidPinSetAlt.fromJson(Map<String, dynamic> json)
      : id = json['id'] == null ? NO_PIN_SET : json['id'],
        pinSetName = json['pinSetName'] == null ? '' : json['pinSetName'] {
    List<dynamic> list = json['pinAltList'].map((result) =>
    new FloidPinAlt.fromJson(result)).toList();
    pinAltList = <FloidPinAlt>[];
    list.forEach((floidPinAlt) => pinAltList.add(floidPinAlt as FloidPinAlt));
  }

  Map<String, dynamic> toJson() {
    List<Map<String, dynamic>> pinAltListJson = pinAltList.map((floidPinAlt) {
      return floidPinAlt.toJson();
    }).toList();
    Map<String, dynamic> json = {
      'id': id,
      'pinSetName': pinSetName,
      'pinAltList': pinAltListJson,
    };
    return json;
  }
}

