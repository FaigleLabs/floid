/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/models/droid_instruction.dart';

class FloidServerCommandResult {

  FloidServerCommandResult();
  int serverTime = 0;
  DroidInstruction droidInstruction = DroidInstruction();
  String commandName = "";
  int floidId = -1;
  int floidUuid = -1;
  bool success = false;
  int result = 0;
  String resultMessage = '';
  bool remotingSuccess = false;
  bool servletSuccess = false;
  bool serverSuccess = false;
  bool socketSuccess = false;
  bool threadSuccess = false;
  bool queueSuccess = false;
  bool online = false;
  int queueLength = 0;
  int lastSeenTime = 0;

  /// Get the droid instruction from JSON
  FloidServerCommandResult.fromJson(Map<String, dynamic> json)
      : serverTime = json['serverTime']==null?0:json['serverTime'],
        droidInstruction = json['droidInstruction']==null?DroidInstruction():DroidInstruction.instructionFactory(json['droidInstruction']),
        floidId = json['floidId']==null?1:json['floidId'],
        floidUuid = json['floidUuid']==null?-1:json['floidUuid'],
        success = json['success']==null?false:json['success'],
        result = json['result']==null?0:json['result'],
        resultMessage = json['resultMessage']==null?'':json['json'],
        remotingSuccess = json['remotingSuccess']==null?false:json['remotingSuccess'],
        servletSuccess = json['servletSuccess']==null?false:json['servletSuccess'],
        serverSuccess = json['serverSuccess']==null?false:json['serverSuccess'],
        socketSuccess = json['socketSuccess']==null?false:json['socketSuccess'],
        threadSuccess = json['threadSuccess']==null?false:json['threadSuccess'],
        queueSuccess = json['queueSuccess']==null?false:json['queueSuccess'],
        online = json['online']==null?false:json['online'],
        queueLength = json['queueLength']==null?0:json['queueLength'],
        lastSeenTime = json['lastSeenTime']==null?0:json['lastSeenTime'];
}

