/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';

class FloidModelParameters {

  FloidModelParameters();

  /// Blades low degrees
  double bladesLow = -6;
  /// Blades zero degrees
  double bladesZero = 8;
  /// Blades high degrees
  double bladesHigh = 24;
  /// Heli 0 Servo 0 low
  double h0S0Low = 5;
  /// Heli 0 Servo 1 low
  double h0S1Low = 5;
  /// Heli 0 Servo 2 low
  double h0S2Low = 5;
  /// Heli 1 Servo 0 low
  double h1S0Low = 5;
  /// Heli 1 Servo 1 low
  double h1S1Low = 5;
  /// Heli 1 Servo 2 low
  double h1S2Low = 5;
  /// Heli 0 Servo 0 zero
  double h0S0Zero = 28;
  /// Heli 0 Servo 1 zero
  double h0S1Zero = 28;
  /// Heli 0 Servo 2 zero
  double h0S2Zero = 28;
  /// Heli 1 Servo 0 zero
  double h1S0Zero = 28;
  /// Heli 1 Servo 1 zero
  double h1S1Zero = 28;
  /// Heli 1 Servo 2 zero
  double h1S2Zero = 28;
  /// Heli 0 Servo 0 high
  double h0S0High = 55;
  /// Heli 0 Servo 1 high
  double h0S1High = 55;
  /// Heli 0 Servo 2 high
  double h0S2High = 55;
  /// Heli 1 Servo 0 high
  double h1S0High = 55;
  /// Heli 1 Servo 1 high
  double h1S1High = 55;
  /// Heli 1 Servo 2 high
  double h1S2High = 55;
  /// Heli 2 Servo 0 low
  double h2S0Low = 5;
  /// Heli 2 Servo 1 low
  double h2S1Low = 5;
  /// Heli 2 Servo 2 low
  double h2S2Low = 5;
  /// Heli 3 Servo 0 low
  double h3S0Low = 5;
  /// Heli 3 Servo 1 low
  double h3S1Low = 5;
  /// Heli 3 Servo 2 low
  double h3S2Low = 5;
  /// Heli 2 Servo 0 zero
  double h2S0Zero = 28;
  /// Heli 2 Servo 1 zero
  double h2S1Zero = 28;
  /// Heli 2 Servo 2 zero
  double h2S2Zero = 28;
  /// Heli 3 Servo 0 zero
  double h3S0Zero = 28;
  /// Heli 3 Servo 1 zero
  double h3S1Zero = 28;
  /// Heli 3 Servo 2 zero
  double h3S2Zero = 28;
  /// Heli 2 Servo 0 high
  double h2S0High = 55;
  /// Heli 2 Servo 1 high
  double h2S1High = 55;
  /// Heli 2 Servo 2 high
  double h2S2High = 55;
  /// Heli 3 Servo 0 high
  double h3S0High = 55;
  /// Heli 3 Servo 1 high
  double h3S1High = 55;
  /// Heli 3 Servo 2 high
  double h3S2High = 55;
  /// Collective min
  double collectiveMin = 4;
  /// Collective max
  double collectiveMax = 20;
  /// Collective default
  double collectiveDefault = 0.0;
  /// Cyclic range
  double cyclicRange = 4;
  /// Cyclic default
  double cyclicDefault = 0.0;
  /// ESC type
  int escType = 0; // Exceed
  /// ESC pulse min
  int escPulseMin = 1000;
  /// ESC pulse max
  int escPulseMax = 2000;
  /// ESC low value
  double escLowValue = 0.00;
  /// ESC high value
  double escHighValue = 1.00;
  /// Attack angle minimum distance value
  double attackAngleMinDistanceValue = 5;
  /// Attack angle maximum distance value
  double attackAngleMaxDistanceValue = 10;
  /// Attack angle value
  double attackAngleValue = -5;
  /// Pitch delta min value
  double pitchDeltaMinValue = 1.0;
  /// Pitch delta max value
  double pitchDeltaMaxValue = 5.0;
  /// Pitch target velocity max value
  double pitchTargetVelocityMaxValue = 2.0;
  /// Roll delta minimum value
  double rollDeltaMinValue = 1.0;
  /// Roll delta maximum value
  double rollDeltaMaxValue = 5.0;
  /// Roll target velocity maximum value
  double rollTargetVelocityMaxValue = 2.0;
  /// Altitude to target min value
  double altitudeToTargetMinValue = 1.0;
  /// Altitude to target max value
  double altitudeToTargetMaxValue = 3.0;
  /// Altitude to target velocity max value
  double altitudeTargetVelocityMaxValue = 3.0;
  /// Heading delta min value
  double headingDeltaMinValue = 2.0;
  /// Heading delta max value
  double headingDeltaMaxValue = 10.0;
  /// Heading target velocity max value
  double headingTargetVelocityMaxValue = 3.0;
  /// Distance to target min value
  double distanceToTargetMinValue = 2.0;
  /// Distance to target max value
  double distanceToTargetMaxValue = 20.0;
  /// Distance target velocity max value
  double distanceTargetVelocityMaxValue = 3.0;
  /// Orientation min distance value
  double orientationMinDistanceValue = 5.0;
  /// Lift off target altitude delta value
  double liftOffTargetAltitudeDeltaValue = 5.0;
  /// Servo pulse min
  int servoPulseMin = 1000;
  /// Servo pulse max
  int servoPulseMax = 2000;
  /// Servo degree min
  double servoDegreeMin = 0;
  /// Servo pulse max
  double servoDegreeMax = 90;
  /// Servo sign left (1=positive) (0=negative)
  int servoSignLeft = 1;
  /// Servo sign right (1=positive) (0=negative)
  int servoSignRight = 0;
  /// Servo sign pitch (1=positive) (0=negative)
  int servoSignPitch = 1;

  /// Heli 0 Servo offset left (degrees)
  double servoOffsetLeft0 = 60;
  /// Heli 0 Servo offset right (degrees)
  double servoOffsetRight0 = -60;
  /// Heli 0 Servo offset pitch (degrees)
  double servoOffsetPitch0 = 180;

  /// Heli 1 Servo offset left (degrees)
  double servoOffsetLeft1 = 60;
  /// Heli 1 Servo offset right (degrees)
  double servoOffsetRight1 = -60;
  /// Heli 1 Servo offset pitch (degrees)
  double servoOffsetPitch1 = 180;

  /// Heli 2 Servo offset left (degrees)
  double servoOffsetLeft2 = 60;
  /// Heli 2 Servo offset right (degrees)
  double servoOffsetRight2 = -60;
  /// Heli 2 Servo offset pitch (degrees)
  double servoOffsetPitch2 = 180;

  /// Heli 3 Servo offset left (degrees)
  double servoOffsetLeft3 = 60;
  /// Heli 3 Servo offset right (degrees)
  double servoOffsetRight3 = -60;
  /// Heli 3 Servo offset pitch (degrees)
  double servoOffsetPitch3 = 180;




  /// Target velocity for keep still
  double targetVelocityKeepStill = 2.5;
  /// Target velocity for full speed
  double targetVelocityFullSpeed = 10;
  /// Target pitch velocity alpha
  double targetPitchVelocityAlpha = 0.1;
  /// Target roll velocity alpha
  double targetRollVelocityAlpha = 0.1;
  /// Target heading velocity alpha
  double targetHeadingVelocityAlpha = 0.1;
  /// Target altitude velocity alpha
  double targetAltitudeVelocityAlpha = 0.1;
  /// Land mode time required at min altitude to change state to on-ground
  int landModeTimeRequiredAtMinAltitude = 5000;
  /// Start mode
  int startMode = 0;  // FLOID MODE TEST
  /// Rotation mode
  int rotationMode = 0;
  /// Cyclic heading alpha
  double cyclicHeadingAlpha = 0.3;
  // Heli Startup:
  /// Heli startup number of steps
  int heliStartupModeNumberSteps = 40;
  /// Heli startup ticks per step
  int heliStartupModeStepTick = 250;
  // ESC Collective Calc:
  /// ESC collective calculation midpoint
  double floidModelEscCollectiveCalcMidpoint = 0.5;
  /// ESC collective calculation low value
  double floidModelEscCollectiveLowValue = 0;
  /// ESC collective calculation mid value
  double floidModelEscCollectiveMidValue = 0.2;
  /// ESC collective calculation high value
  double floidModelEscCollectiveHighValue= 1.0;
  /// The scaling factor for cyclic alpha values
  double velocityDeltaCyclicAlphaScale = 1.0;
  /// Acceleration multiplier scale
  double accelerationMultiplierScale = 10.0;

  /// Convert from JSON
  FloidModelParameters.fromJson(Map<String, dynamic> json)
      :  bladesLow = DroidInstruction.jsonDouble(json,'bladesLow'),
        bladesZero = DroidInstruction.jsonDouble(json,'bladesZero'),
        bladesHigh = DroidInstruction.jsonDouble(json,'bladesHigh'),
        h0S0Low = DroidInstruction.jsonDouble(json,'h0S0Low'),
        h0S1Low = DroidInstruction.jsonDouble(json,'h0S1Low'),
        h0S2Low = DroidInstruction.jsonDouble(json,'h0S2Low'),
        h1S0Low = DroidInstruction.jsonDouble(json,'h1S0Low'),
        h1S1Low = DroidInstruction.jsonDouble(json,'h1S1Low'),
        h1S2Low = DroidInstruction.jsonDouble(json,'h1S2Low'),
        h0S0Zero = DroidInstruction.jsonDouble(json,'h0S0Zero'),
        h0S1Zero = DroidInstruction.jsonDouble(json,'h0S1Zero'),
        h0S2Zero = DroidInstruction.jsonDouble(json,'h0S2Zero'),
        h1S0Zero = DroidInstruction.jsonDouble(json,'h1S0Zero'),
        h1S1Zero = DroidInstruction.jsonDouble(json,'h1S1Zero'),
        h1S2Zero = DroidInstruction.jsonDouble(json,'h1S2Zero'),
        h0S0High = DroidInstruction.jsonDouble(json,'h0S0High'),
        h0S1High = DroidInstruction.jsonDouble(json,'h0S1High'),
        h0S2High = DroidInstruction.jsonDouble(json,'h0S2High'),
        h1S0High = DroidInstruction.jsonDouble(json,'h1S0High'),
        h1S1High = DroidInstruction.jsonDouble(json,'h1S1High'),
        h1S2High = DroidInstruction.jsonDouble(json,'h1S2High'),
        h2S0Low = DroidInstruction.jsonDouble(json,'h2S0Low'),
        h2S1Low = DroidInstruction.jsonDouble(json,'h2S1Low'),
        h2S2Low = DroidInstruction.jsonDouble(json,'h2S2Low'),
        h3S0Low = DroidInstruction.jsonDouble(json,'h3S0Low'),
        h3S1Low = DroidInstruction.jsonDouble(json,'h3S1Low'),
        h3S2Low = DroidInstruction.jsonDouble(json,'h3S2Low'),
        h2S0Zero = DroidInstruction.jsonDouble(json,'h2S0Zero'),
        h2S1Zero = DroidInstruction.jsonDouble(json,'h2S1Zero'),
        h2S2Zero = DroidInstruction.jsonDouble(json,'h2S2Zero'),
        h3S0Zero = DroidInstruction.jsonDouble(json,'h3S0Zero'),
        h3S1Zero = DroidInstruction.jsonDouble(json,'h3S1Zero'),
        h3S2Zero = DroidInstruction.jsonDouble(json,'h3S2Zero'),
        h2S0High = DroidInstruction.jsonDouble(json,'h2S0High'),
        h2S1High = DroidInstruction.jsonDouble(json,'h2S1High'),
        h2S2High = DroidInstruction.jsonDouble(json,'h2S2High'),
        h3S0High = DroidInstruction.jsonDouble(json,'h3S0High'),
        h3S1High = DroidInstruction.jsonDouble(json,'h3S1High'),
        h3S2High = DroidInstruction.jsonDouble(json,'h3S2High'),
        collectiveMin = DroidInstruction.jsonDouble(json,'collectiveMin'),
        collectiveMax = DroidInstruction.jsonDouble(json,'collectiveMax'),
        collectiveDefault = DroidInstruction.jsonDouble(json,'collectiveDefault'),
        cyclicRange = DroidInstruction.jsonDouble(json,'cyclicRange'),
        cyclicDefault = DroidInstruction.jsonDouble(json,'cyclicDefault'),
        escType = DroidInstruction.jsonInt(json, 'escType'),
        escPulseMin = DroidInstruction.jsonInt(json, 'escPulseMin'),
        escPulseMax = DroidInstruction.jsonInt(json, 'escPulseMax'),
        escLowValue = DroidInstruction.jsonDouble(json, 'escLowValue'),
        escHighValue = DroidInstruction.jsonDouble(json, 'escHighValue'),
        attackAngleMinDistanceValue = DroidInstruction.jsonDouble(json, 'attackAngleMinDistanceValue'),
        attackAngleMaxDistanceValue = DroidInstruction.jsonDouble(json, 'attackAngleMaxDistanceValue'),
        attackAngleValue = DroidInstruction.jsonDouble(json,'attackAngleValue'),
        pitchDeltaMinValue = DroidInstruction.jsonDouble(json,'pitchDeltaMinValue'),
        pitchDeltaMaxValue = DroidInstruction.jsonDouble(json,'pitchDeltaMaxValue'),
        pitchTargetVelocityMaxValue = DroidInstruction.jsonDouble(json,'pitchTargetVelocityMaxValue'),
        rollDeltaMinValue = DroidInstruction.jsonDouble(json,'rollDeltaMinValue'),
        rollDeltaMaxValue = DroidInstruction.jsonDouble(json,'rollDeltaMaxValue'),
        rollTargetVelocityMaxValue = DroidInstruction.jsonDouble(json,'rollTargetVelocityMaxValue'),
        altitudeToTargetMinValue = DroidInstruction.jsonDouble(json,'altitudeToTargetMinValue'),
        altitudeToTargetMaxValue = DroidInstruction.jsonDouble(json,'altitudeToTargetMaxValue'),
        altitudeTargetVelocityMaxValue = DroidInstruction.jsonDouble(json,'altitudeTargetVelocityMaxValue'),
        headingDeltaMinValue = DroidInstruction.jsonDouble(json,'headingDeltaMinValue'),
        headingDeltaMaxValue = DroidInstruction.jsonDouble(json,'headingDeltaMaxValue'),
        headingTargetVelocityMaxValue = DroidInstruction.jsonDouble(json,'headingTargetVelocityMaxValue'),
        distanceToTargetMinValue = DroidInstruction.jsonDouble(json,'distanceToTargetMinValue'),
        distanceToTargetMaxValue = DroidInstruction.jsonDouble(json,'distanceToTargetMaxValue'),
        distanceTargetVelocityMaxValue = DroidInstruction.jsonDouble(json,'distanceTargetVelocityMaxValue'),
        orientationMinDistanceValue = DroidInstruction.jsonDouble(json,'orientationMinDistanceValue'),
        liftOffTargetAltitudeDeltaValue = DroidInstruction.jsonDouble(json,'liftOffTargetAltitudeDeltaValue'),
        servoPulseMin = DroidInstruction.jsonInt(json, 'servoPulseMin'),
        servoPulseMax = DroidInstruction.jsonInt(json, 'servoPulseMax'),
        servoDegreeMin = DroidInstruction.jsonDouble(json, 'servoDegreeMin'),
        servoDegreeMax = DroidInstruction.jsonDouble(json, 'servoDegreeMax'),
        servoSignLeft = DroidInstruction.jsonInt(json, 'servoSignLeft'),
        servoSignRight = DroidInstruction.jsonInt(json, 'servoSignRight'),
        servoSignPitch = DroidInstruction.jsonInt(json, 'servoSignPitch'),
        servoOffsetLeft0 = DroidInstruction.jsonDouble(json,'servoOffsetLeft0'),
        servoOffsetRight0 = DroidInstruction.jsonDouble(json,'servoOffsetRight0'),
        servoOffsetPitch0 = DroidInstruction.jsonDouble(json,'servoOffsetPitch0'),
        servoOffsetLeft1 = DroidInstruction.jsonDouble(json,'servoOffsetLeft1'),
        servoOffsetRight1 = DroidInstruction.jsonDouble(json,'servoOffsetRight1'),
        servoOffsetPitch1 = DroidInstruction.jsonDouble(json,'servoOffsetPitch1'),
        servoOffsetLeft2 = DroidInstruction.jsonDouble(json,'servoOffsetLeft2'),
        servoOffsetRight2 = DroidInstruction.jsonDouble(json,'servoOffsetRight2'),
        servoOffsetPitch2 = DroidInstruction.jsonDouble(json,'servoOffsetPitch2'),
        servoOffsetLeft3 = DroidInstruction.jsonDouble(json,'servoOffsetLeft3'),
        servoOffsetRight3 = DroidInstruction.jsonDouble(json,'servoOffsetRight3'),
        servoOffsetPitch3 = DroidInstruction.jsonDouble(json,'servoOffsetPitch3'),
        targetVelocityKeepStill = DroidInstruction.jsonDouble(json,'targetVelocityKeepStill'),
        targetVelocityFullSpeed = DroidInstruction.jsonDouble(json,'targetVelocityFullSpeed'),
        targetPitchVelocityAlpha = DroidInstruction.jsonDouble(json,'targetPitchVelocityAlpha'),
        targetRollVelocityAlpha = DroidInstruction.jsonDouble(json,'targetRollVelocityAlpha'),
        targetHeadingVelocityAlpha = DroidInstruction.jsonDouble(json,'targetHeadingVelocityAlpha'),
        targetAltitudeVelocityAlpha = DroidInstruction.jsonDouble(json,'targetAltitudeVelocityAlpha'),
        landModeTimeRequiredAtMinAltitude = DroidInstruction.jsonInt(json,'landModeTimeRequiredAtMinAltitude'),
        startMode = DroidInstruction.jsonInt(json,'startMode'),
        rotationMode = DroidInstruction.jsonInt(json,'rotationMode'),
        cyclicHeadingAlpha = DroidInstruction.jsonDouble(json,'cyclicHeadingAlpha'),
        heliStartupModeNumberSteps = DroidInstruction.jsonInt(json,'heliStartupModeNumberSteps'),
        heliStartupModeStepTick = DroidInstruction.jsonInt(json,'heliStartupModeStepTick'),
        floidModelEscCollectiveCalcMidpoint = DroidInstruction.jsonDouble(json,'floidModelEscCollectiveCalcMidpoint'),
        floidModelEscCollectiveLowValue = DroidInstruction.jsonDouble(json,'floidModelEscCollectiveLowValue'),
        floidModelEscCollectiveMidValue = DroidInstruction.jsonDouble(json,'floidModelEscCollectiveMidValue'),
        floidModelEscCollectiveHighValue = DroidInstruction.jsonDouble(json,'floidModelEscCollectiveHighValue'),
        velocityDeltaCyclicAlphaScale = DroidInstruction.jsonDouble(json,'velocityDeltaCyclicAlphaScale'),
        accelerationMultiplierScale = DroidInstruction.jsonDouble(json,'accelerationMultiplierScale');
}
