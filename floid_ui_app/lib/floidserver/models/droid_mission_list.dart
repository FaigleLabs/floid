/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
class DroidMissionList {

  DroidMissionList();

  /// The id
  int id = -1;

  /// The mission id:
  int missionId = -1;

  /// The mission name:
  String missionName = "";

  /// Get the droid mission list from JSON
  DroidMissionList.fromJson(Map<String, dynamic> json)
      : id = json['id']==null?-1:json['id'],
        missionId = json['missionId'],
        missionName = json['missionName'];
}

