/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/models/commands/commands.dart';
import 'droid_mission.dart';

/// Droid Instruction Model
class DroidInstruction {

  DroidInstruction({this.id=NO_INSTRUCTION,this.type='',this.missionInstructionNumber=NO_INSTRUCTION, this.topLevelMission=false,});

  static const int NO_INSTRUCTION = -1;
  bool topLevelMission = false;

  static double jsonDouble(Map<String, dynamic> json, String field) {
    if(json.containsKey(field)) {
      if(json[field]  is double) {
        return json[field];
      }
      if(json[field] is int) {
        return json[field].toDouble();
      }
    }
    return 0;
  }

  static int jsonInt(Map<String, dynamic> json, String field) {
    if(json.containsKey(field)) {
      if(json[field]  is int) {
        return json[field];
      }
    }
    return 0;
  }

  static bool jsonBool(Map<String, dynamic> json, String field) {
    if(json.containsKey(field)) {
      if(json[field]  is bool) {
        return json[field];
      }
    }
    return false;
  }

  static String jsonString(Map<String, dynamic> json, String field) {
    if(json.containsKey(field)) {
      if(json[field]  is String) {
        return json[field];
      }
    }
    return '';
  }
  /// The id
  int id = NO_INSTRUCTION;

  /// The Instruction type
  String type = '';

//  /// A reference to the droid mission
//  DroidMission droidMission;

  /// The current mission instruction number:
  int missionInstructionNumber = NO_INSTRUCTION;

  /// Get the droid instruction from JSON
  DroidInstruction.fromJson(Map<String, dynamic> json)
      : id = json.containsKey('id')?json['id']:-1,
        type = json.containsKey('type')?json['type']:'com.faiglelabs.floid.servertypes.mission.DroidMission',
        topLevelMission = json.containsKey('topLevelMission')?json['topLevelMission']:true,
        missionInstructionNumber = json.containsKey('missionInstructionNumber')?json['missionInstructionNumber']:-1;

  Map<String, dynamic> toJson() =>  <String, dynamic> {
    'id': id,
    'type' : type,
    'topLevelMission' : topLevelMission,
    'missionInstructionNumber' : missionInstructionNumber,
  };

  /// Get the droid instruction from JSON
  factory DroidInstruction.instructionFactory(Map<String, dynamic> json) {
    switch (json['type']) {
      case 'com.faiglelabs.floid.servertypes.mission.DroidMission':
        return DroidMission.fromJson(json);
     case 'com.faiglelabs.floid.servertypes.commands.AcquireHomePositionCommand':
        return AcquireHomePositionCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.DebugCommand':
        return DebugCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.DelayCommand':
        return DelayCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.DeployParachuteCommand':
        return DeployParachuteCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.DesignateCommand':
        return DesignateCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.FlyPatternCommand':
        return FlyPatternCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.FlyToCommand':
        return FlyToCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.HeightToCommand':
        return HeightToCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.HoverCommand':
        return HoverCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.LandCommand':
        return LandCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.LiftOffCommand':
        return LiftOffCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.NullCommand':
        return NullCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.PanTiltCommand':
        return PanTiltCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.PayloadCommand':
        return PayloadCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.PhotoStrobeCommand':
        return PhotoStrobeCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.RetrieveLogsCommand':
        return RetrieveLogsCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.SetParametersCommand':
        return SetParametersCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.ShutDownHelisCommand':
        return ShutDownHelisCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.SpeakerCommand':
        return SpeakerCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.SpeakerOffCommand':
        return SpeakerOffCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.SpeakerOnCommand':
        return SpeakerOnCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.StartUpHelisCommand':
        return StartUpHelisCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.StartVideoCommand':
        return StartVideoCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.StopDesignatingCommand':
        return StopDesignatingCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.StopPhotoStrobeCommand':
        return StopPhotoStrobeCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.StopVideoCommand':
        return StopVideoCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.TakePhotoCommand':
        return TakePhotoCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.TurnToCommand':
        return TurnToCommand.fromJson(json);
      case 'com.faiglelabs.floid.servertypes.commands.testcommands.TestCommand':
        return TestCommand.fromJson(json);
    }
    return new DroidInstruction.fromJson(json);
  }
}

