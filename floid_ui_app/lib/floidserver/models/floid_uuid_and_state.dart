/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/models/floid_id_and_state.dart';

class FloidUuidAndState {
  /// The no floid id constant:
  static const String NO_FLOID_UUID = '';

  FloidUuidAndState();
  
  /// The floid id:
  int floidId = FloidIdAndState.NO_FLOID_ID;
  /// The new floid uuid:
  String floidUuid = NO_FLOID_UUID;
  /// The Floid uuid create time.
  int floidUuidCreateTime = 0;
  /// The Floid uuid update time.
  int floidUuidUpdateTime = 0;
  /// Is the floid currently active
  bool floidActive = false;
  /// Did this ever emit a floid status?
  bool floidStatus = false;
  /// The Floid uuid create time string.
  String floidUuidCreateTimeString = "";
  /// The Floid uuid update time string.
  String floidUuidUpdateTimeString = "";

  FloidUuidAndState.fromJson(Map<String, dynamic> json)
      : floidId = json['floidId']==null?FloidIdAndState.NO_FLOID_ID:json['floidId'],
        floidUuid = json['floidUuid']==null?NO_FLOID_UUID:json['floidUuid'],
        floidUuidCreateTime = json['floidUuidCreateTime']==null?0:json['floidUuidCreateTime'],
        floidUuidUpdateTime = json['floidUuidUpdateTime']==null?0:json['floidUuidUpdateTime'],
        floidActive = json['floidActive']==null?false:json['floidActive'],
        floidStatus = json['floidStatus']==null?false:json['floidStatus'],
        floidUuidCreateTimeString = json['floidUuidCreateTimeString']==null?'':json['floidUuidCreateTimeString'],
        floidUuidUpdateTimeString = json['floidUuidUpdateTimeString']==null?'':json['floidUuidUpdateTimeString'];
}
