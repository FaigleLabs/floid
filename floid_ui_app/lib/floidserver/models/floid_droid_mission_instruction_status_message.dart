/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
class FloidDroidMissionInstructionStatusMessage {

  FloidDroidMissionInstructionStatusMessage();

  /// The message type
  String messageType = "";

  /// The mission name
  String missionName = "";

  /// The mission id
  int missionId = 0;

  /// The instruction id
  int instructionId = 0;

  /// The instruction type
  String instructionType = '';

  /// The instruction status
  int instructionStatus = -1;

  // The instruction status display string
  String instructionStatusDisplayString = '';

  FloidDroidMissionInstructionStatusMessage.fromJson(Map<String, dynamic> json)
      : messageType = json['messageType'],
        missionName = json['missionName'],
        missionId = json['missionId'],
        instructionId = json['instructionId'],
        instructionType = json['instructionType'],
        instructionStatus = json['instructionStatus'],
        instructionStatusDisplayString = json['instructionStatusDisplayString'];

}
