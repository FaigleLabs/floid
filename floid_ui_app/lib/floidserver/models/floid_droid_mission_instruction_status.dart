/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:flutter/material.dart';
import 'floid_droid_mission_instruction_status_message.dart';

class FloidDroidMissionInstructionStatus {

  // Non-completion states:
  static const int PROCESS_BLOCK_NOT_STARTED = 0;
  static const int PROCESS_BLOCK_STARTED = 1;
  static const int PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE = 2;
  static const int PROCESS_BLOCK_COMMAND_RESPONSE_RECEIVED = 3;
  static const int PROCESS_BLOCK_IN_PROGRESS = 4;
  static const int PROCESS_BLOCK_WAITING_FOR_FLOID_GOAL = 5;
  static const int PROCESS_BLOCK_WAITING_FOR_MATCHING_GOAL_ID = 6;
  static const int PROCESS_BLOCK_WAITING_TO_ACHIEVE_GOAL = 7;

  // Completion States:
  static const int PROCESS_BLOCK_COMPLETED_SUCCESS = 20;
  static const int PROCESS_BLOCK_COMPLETED_ERROR = 21;
  static const int PROCESS_BLOCK_COMPLETED_TIMEOUT_ERROR = 22;
  static const int PROCESS_BLOCK_COMPLETED_LOGIC_ERROR = 23;
  static const int PROCESS_BLOCK_COMPLETED_NO_FLOID_ERROR = 24;
  static const int PROCESS_BLOCK_COMPLETED_COMMUNICATION_ERROR = 25;

  static const String MESSAGE_TYPE_MISSION_QUEUED = "MISSION_QUEUED";
  static const String MESSAGE_TYPE_MISSION_CANCELLED = "MISSION_CANCELLED";
  static const String MESSAGE_TYPE_MISSION_STARTING = "MISSION_STARTING";
  static const String MESSAGE_TYPE_MISSION_COMPLETE = "MISSION_COMPLETE";
  static const String MESSAGE_TYPE_MISSION_COMPLETE_ERROR = "MISSION_COMPLETE_ERROR";
  static const String MESSAGE_TYPE_INSTRUCTION_STARTING = "INSTRUCTION_STARTING";
  static const String MESSAGE_TYPE_INSTRUCTION_STATUS_CHANGE = "INSTRUCTION_STATUS_CHANGE";
  static const String MESSAGE_TYPE_INSTRUCTION_COMPLETE = "INSTRUCTION_COMPLETE";

  static const Color INSTRUCTION_STATUS_COLOR_RED = Color(0xFFFF0000);
  static const Color INSTRUCTION_STATUS_COLOR_YELLOW = Color(0xFFFFFF00);
  static const Color INSTRUCTION_STATUS_COLOR_GREEN = Color(0xFF00FF00);
  static const Color INSTRUCTION_STATUS_COLOR_ORANGE = Color(0xFFFF7700);


  /// The originating mission instruction message:
  FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage;
  /// The previous Status
  FloidDroidMissionInstructionStatus? previousFloidDroidMissionInstructionStatus;
  /// The mission id
  String missionIdString = '';
  /// The mission name
  String missionNameString = '';
  /// The mission status
  String missionStatusString = '';
  /// The mission progress
  String missionProgressString = '';
  /// The instruction name
  String instructionNameString = '';
  /// The instruction progress
  String instructionProgressString = '';
  /// The instruction status
  String instructionStatusString = '';
  /// The instruction status color
  Color instructionStatusColor = Colors.black;
  /// The instruction id
  String instructionIdString = '';
  /// Any generated messages
  List<String> messages = <String>[];

  FloidDroidMissionInstructionStatus({
    required this.floidDroidMissionInstructionStatusMessage,
    required this.previousFloidDroidMissionInstructionStatus}) {
    final previousFloidDroidMissionInstructionStatus = this.previousFloidDroidMissionInstructionStatus;
    if (previousFloidDroidMissionInstructionStatus != null) {
      // Copy over if previous status:
      missionIdString =
          previousFloidDroidMissionInstructionStatus.missionIdString;
      missionNameString =
          previousFloidDroidMissionInstructionStatus.missionNameString;
      missionStatusString =
          previousFloidDroidMissionInstructionStatus.missionStatusString;
      missionProgressString =
          previousFloidDroidMissionInstructionStatus.missionProgressString;
      instructionNameString =
          previousFloidDroidMissionInstructionStatus.instructionNameString;
      instructionProgressString =
          previousFloidDroidMissionInstructionStatus.instructionProgressString;
      instructionStatusString =
          previousFloidDroidMissionInstructionStatus.instructionStatusString;
      instructionStatusColor =
          previousFloidDroidMissionInstructionStatus.instructionStatusColor;
      instructionIdString =
          previousFloidDroidMissionInstructionStatus.instructionIdString;
    } else {
      missionIdString = '';
      missionNameString = '';
      missionStatusString = '';
      missionProgressString = '';
      instructionNameString = '';
      instructionProgressString = '';
      instructionStatusString = '';
      instructionStatusColor = Colors.black;
      instructionIdString = '';
    }
    List<String> messages = <String>[];
    String messageType = floidDroidMissionInstructionStatusMessage.messageType;
    int instructionStatus = floidDroidMissionInstructionStatusMessage
        .instructionStatus;
    missionIdString =
        floidDroidMissionInstructionStatusMessage.missionId.toString();
    missionNameString = floidDroidMissionInstructionStatusMessage.missionName;
    // Parse the mission type:
    if (messageType == MESSAGE_TYPE_MISSION_STARTING) {
      // Display mission information - clear instruction information
      missionStatusString = 'RUNNING';
      missionProgressString = 'IN PROGRESS';
      instructionNameString = '';
      instructionProgressString = '';
      instructionStatusString = '';
      instructionIdString = '';
      messages.add(
          'Mission Started: ($missionIdString) $missionNameString $missionProgressString');
    }
    else if (messageType == MESSAGE_TYPE_MISSION_QUEUED) {
      // Display mission information - clear instruction information
      missionStatusString = 'QUEUED';
      missionProgressString = '';
      instructionNameString = '';
      instructionProgressString = '';
      instructionStatusString = '';
      instructionIdString = '';
      messages.add(
          'Mission Queued: ($missionIdString) $missionNameString $missionProgressString');
    }
    else if (messageType == MESSAGE_TYPE_MISSION_CANCELLED) {
      // Display mission information - clear instruction information
      missionStatusString = 'COMPLETED';
      missionProgressString = 'CANCELLED';
      instructionNameString = '';
      instructionProgressString = '';
      instructionStatusString = '';
      instructionIdString = '';
      messages.add(
          'Mission Cancelled: ($missionIdString) $missionNameString $missionProgressString');
    }
    else if (messageType == MESSAGE_TYPE_MISSION_COMPLETE) {
      // Display mission information - clear instruction information
      missionStatusString = 'COMPLETED';
      missionProgressString = 'SUCCESS';
      instructionNameString = '';
      instructionProgressString = '';
      instructionStatusString = '';
      instructionIdString = '';
      messages.add(
          'Mission Completed: ($missionIdString) $missionNameString $missionProgressString');
    }
    else if (messageType == MESSAGE_TYPE_MISSION_COMPLETE_ERROR) {
      // Display mission information - clear instruction information
      missionStatusString = 'COMPLETED';
      missionProgressString = 'ERROR';
      instructionNameString = '';
      instructionProgressString = '';
      instructionStatusString = '';
      instructionIdString = '';
      messages.add(
          'Mission Complete: ($missionIdString) $missionNameString $missionProgressString');
    }
    else if (messageType == MESSAGE_TYPE_INSTRUCTION_STARTING) {
      // Display instruction information
      instructionIdString =
          floidDroidMissionInstructionStatusMessage.instructionId.toString();
      instructionProgressString = 'STARTING';
      instructionStatusString = _makeInstructionStatusString(instructionStatus);
      instructionStatusColor = _makeInstructionStatusColor(instructionStatus);
      instructionNameString = _makeInstructionNameString(
          floidDroidMissionInstructionStatusMessage.instructionType);
      messages.add(
          'Instruction Starting: ($instructionIdString) $instructionNameString $instructionStatusString/$instructionProgressString');
    }
    else if (messageType == MESSAGE_TYPE_INSTRUCTION_STATUS_CHANGE) {
      // Display instruction information
      instructionIdString = '' +
          floidDroidMissionInstructionStatusMessage.instructionId.toString();
      instructionProgressString = 'RUNNING';
      instructionStatusString = _makeInstructionStatusString(instructionStatus);
      instructionStatusColor = _makeInstructionStatusColor(instructionStatus);
      instructionNameString = _makeInstructionNameString(
          floidDroidMissionInstructionStatusMessage.instructionType);
      messages.add(
          'Instruction Status Change: ($instructionIdString) $instructionNameString $instructionStatusString/$instructionProgressString');
    }
    else if (messageType == 'INSTRUCTION_COMPLETE') {
      // Display instruction information
      instructionIdString = '' +
          floidDroidMissionInstructionStatusMessage.instructionId.toString();
      instructionProgressString = 'COMPLETED';
      instructionStatusString = _makeInstructionStatusString(instructionStatus);
      instructionStatusColor = _makeInstructionStatusColor(instructionStatus);
      instructionNameString = _makeInstructionNameString(
          floidDroidMissionInstructionStatusMessage.instructionType);
      messages.add(
          'Instruction Complete: ($instructionIdString) $instructionNameString $instructionStatusString/$instructionProgressString');
    }
  }

  static String _makeInstructionStatusString(int instructionStatus) {
    switch (instructionStatus) {
      case PROCESS_BLOCK_NOT_STARTED:
        {
          return 'NOT STARTED';
        }
      case PROCESS_BLOCK_STARTED:
        {
          return 'STARTED';
        }
      case PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE:
        {
          return 'RESPONSE WT';
        }
      case PROCESS_BLOCK_COMMAND_RESPONSE_RECEIVED:
        {
          return 'RESPONSE OK';
        }
      case PROCESS_BLOCK_IN_PROGRESS:
        {
          return 'IN PROGRESS';
        }
      case PROCESS_BLOCK_WAITING_FOR_FLOID_GOAL:
        {
          return 'WT FL GOAL';
        }
      case PROCESS_BLOCK_WAITING_FOR_MATCHING_GOAL_ID:
        {
          return 'WT GOAL ID';
        }
      case PROCESS_BLOCK_WAITING_TO_ACHIEVE_GOAL:
        {
          return 'WT GOAL';
        }
      case PROCESS_BLOCK_COMPLETED_SUCCESS:
        {
          return 'SUCCESS';
        }
      case PROCESS_BLOCK_COMPLETED_ERROR:
        {
          return 'ERROR';
        }
      case PROCESS_BLOCK_COMPLETED_TIMEOUT_ERROR:
        {
          return 'TIME OUT';
        }
      case PROCESS_BLOCK_COMPLETED_LOGIC_ERROR:
        {
          return 'LOGIC ERROR';
        }
      case PROCESS_BLOCK_COMPLETED_NO_FLOID_ERROR:
        {
          return 'NO FLOID';
        }
      case PROCESS_BLOCK_COMPLETED_COMMUNICATION_ERROR:
        {
          return 'COMM ERROR';
        }
      default:
        {
          return '???????';
        }
    }
  }


  static Color _makeInstructionStatusColor(int instructionStatus) {
    switch (instructionStatus) {
      case PROCESS_BLOCK_NOT_STARTED:
        {
          return INSTRUCTION_STATUS_COLOR_GREEN;
        }
      case PROCESS_BLOCK_STARTED:
        {
          return INSTRUCTION_STATUS_COLOR_YELLOW;
        }
      case PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE:
        {
          return INSTRUCTION_STATUS_COLOR_ORANGE;
        }
      case PROCESS_BLOCK_IN_PROGRESS:
        {
          return INSTRUCTION_STATUS_COLOR_YELLOW;
        }
      case PROCESS_BLOCK_WAITING_FOR_FLOID_GOAL:
        {
          return INSTRUCTION_STATUS_COLOR_YELLOW;
        }
      case PROCESS_BLOCK_WAITING_FOR_MATCHING_GOAL_ID:
        {
          return INSTRUCTION_STATUS_COLOR_YELLOW;
        }
      case PROCESS_BLOCK_WAITING_TO_ACHIEVE_GOAL:
        {
          return INSTRUCTION_STATUS_COLOR_YELLOW;
        }
      case PROCESS_BLOCK_COMPLETED_SUCCESS:
        {
          return INSTRUCTION_STATUS_COLOR_YELLOW;
        }
      case PROCESS_BLOCK_COMPLETED_ERROR:
        {
          return INSTRUCTION_STATUS_COLOR_RED;
        }
      case PROCESS_BLOCK_COMPLETED_TIMEOUT_ERROR:
        {
          return INSTRUCTION_STATUS_COLOR_RED;
        }
      case PROCESS_BLOCK_COMPLETED_LOGIC_ERROR:
        {
          return INSTRUCTION_STATUS_COLOR_RED;
        }
      case PROCESS_BLOCK_COMPLETED_NO_FLOID_ERROR:
        {
          return INSTRUCTION_STATUS_COLOR_RED;
        }
      case PROCESS_BLOCK_COMPLETED_COMMUNICATION_ERROR:
        {
          return INSTRUCTION_STATUS_COLOR_RED;
        }
    }
    return INSTRUCTION_STATUS_COLOR_YELLOW;
  }

  // For display, removes '.', 'Command' and adds ' ' before capital letter if not the first:
  static String _makeInstructionNameString(String instructionType) {
    String instructionNameString;
    List<String> typeArray = instructionType.split('.');
    if (typeArray.length > 0) {
      instructionNameString =
          (typeArray[typeArray.length - 1]).replaceAll('Command', '');
      String instructionNameStringWithSpaces = '';
      for (int k = 0; k < instructionNameString.length; ++k) {
        if ((k != 0) &&
            (instructionNameString.codeUnitAt(k) >= 'A'.codeUnitAt(0)) &&
            (instructionNameString.codeUnitAt(k) <= 'Z'.codeUnitAt(0))) {
          instructionNameStringWithSpaces += ' ' + instructionNameString[k];
        }
        else {
          instructionNameStringWithSpaces += instructionNameString[k];
        }
      }
      instructionNameString = instructionNameStringWithSpaces;
    }
    else {
      instructionNameString = '';
    }
    return instructionNameString;
  }
}
