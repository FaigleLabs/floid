/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/models/floid_image.dart';

class FloidDroidVideoFrame extends FloidImage {

  FloidDroidVideoFrame({
    floidId = -1,
    floidUuid = '',
    imageName = '',
    imageType = '',
    imageExtension = '',
    latitude = 0,
    longitude = 0,
    altitude = 0,
    pan = 0,
    tilt = 0,
    required imageBytesString,
  }):super(floidId: floidId, floidUuid: floidUuid, imageName: imageName,
      imageType: imageType, imageExtension: imageExtension, latitude: latitude,
      longitude: longitude,  altitude: altitude, imageBytesString: imageBytesString);

  FloidDroidVideoFrame.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);
}
