/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';

class FloidDroidStatus {

  FloidDroidStatus();

  /// Droid mode test
  static const int DROID_MODE_TEST = 0;

  /// Droid mode mission
  static const int DROID_MODE_MISSION = 1;

  /// Droid status idle
  static const int DROID_STATUS_IDLE = 0;

  /// Droid status processing
  static const int DROID_STATUS_PROCESSING = 1;

  /// Droid status error
  static const int DROID_STATUS_ERROR = 2;

  /// The Floid Id
  int floidId = -1;
  /// The Floid Uuid
  String floidUuid = '';
  /// The Droid status time
  int statusTime = 0; // Status time
  /// The Droid status number
  int statusNumber = 0;

  // GPS:
  /// The GPS status time
  int gpsStatusTime = 0;

  /// Does the GPS have required accuracy
  bool gpsHasAccuracy = false;

  /// The GPS accuracy
  double gpsAccuracy = 0.0;

  /// The GPS latitude
  double gpsLatitude = 0.0;

  /// The GPS longitude
  double gpsLongitude = 0.0;

  /// Does the GPS have altitude
  bool gpsHasAltitude = false;

  /// Tbe GPS altitude
  double gpsAltitude = 0.0;

  /// Does the GPS have bearing/speed
  bool gpsHasBearing = false;

  /// The GPS bearing
  double gpsBearing = 0.0;

  /// The GPS speed
  double gpsSpeed = 0.0;

  /// THe GPS provider name
  String gpsProvider = "";

  // Floid connected:
  /// Is the Floid connected
  bool floidConnected = false;

  // Floid error:
  /// Does the Floid have an error
  bool floidError = false;

  // Status and mode:
  /// Current Floid status
  int currentStatus = DROID_STATUS_IDLE; // QUESTION: DOES THIS STAY OR GO INTO

  /// Current Floid mode
  int currentMode = DROID_MODE_TEST;

  // Mission number:
  /// Current mission number
  int missionNumber = 0;

  // Droid started:
  /// Is the Droid started
  bool droidStarted = false;

  // Server:
  /// Is the server connected
  bool serverConnected = false;

  /// Does the server have an error
  bool serverError = false;

  // Battery voltage:
  /// Battery voltage
  double batteryVoltage = 0;

  /// Battery level
  double batteryLevel = 0;

  /// Battery raw level
  int batteryRawLevel = 0;

  /// Batter raw scale
  int batteryRawScale = 1;

  // Helis:
  /// Are the helis started
  bool helisStarted = false;

  /// Have we lifted off
  bool liftedOff = false;

  // Parachute:]
  /// Has the parachute been deployed
  bool parachuteDeployed = false;

  // Photo / Video:
  /// Are we taking a photo
  bool takingPhoto = false;

  /// Photo number
  int photoNumber = 0;

  /// Are we performing photo strobe
  bool photoStrobe = false;

  /// Photo strobe delay
  int photoStrobeDelay = 0;

  /// Are we taking video
  bool takingVideo = false;

  /// Video number
  int videoNumber = 0;

  // Home position:
  /// Has the home position been acquired
  bool homePositionAcquired = false;

  /// Home position longitude
  double homePositionX = 0.0;

  /// Home position latitude
  double homePositionY = 0.0;

  /// Home position altitude
  double homePositionZ = 0.0;

  /// Home position heading
  double homePositionHeading = 0.0;

  FloidDroidStatus.fromJson(Map<String, dynamic> json)
      : floidId = DroidInstruction.jsonInt(json, 'floidId'),
        floidUuid = DroidInstruction.jsonString(json, 'floidUuid'),
        statusTime = DroidInstruction.jsonInt(json, 'statusTime'),
        statusNumber = DroidInstruction.jsonInt(json, 'statusNumber'),
        gpsStatusTime = DroidInstruction.jsonInt(json, 'gpsStatusTime'),
        gpsHasAccuracy = DroidInstruction.jsonBool(json, 'gpsHasAccuracy'),
        gpsAccuracy = DroidInstruction.jsonDouble(json, 'gpsAccuracy'),
        gpsLatitude = DroidInstruction.jsonDouble(json, 'gpsLatitude'),
        gpsLongitude = DroidInstruction.jsonDouble(json, 'gpsLongitude'),
        gpsHasAltitude = DroidInstruction.jsonBool(json, 'gpsHasAltitude'),
        gpsAltitude = DroidInstruction.jsonDouble(json, 'gpsAltitude'),
        gpsHasBearing = DroidInstruction.jsonBool(json, 'gpsHasBearing'),
        gpsBearing = DroidInstruction.jsonDouble(json, 'gpsBearing'),
        gpsSpeed = DroidInstruction.jsonDouble(json, 'gpsSpeed'),
        gpsProvider = DroidInstruction.jsonString(json, 'gpsProvider'),
        floidConnected = DroidInstruction.jsonBool(json, 'floidConnected'),
        floidError = DroidInstruction.jsonBool(json, 'floidError'),
        currentStatus = DroidInstruction.jsonInt(json, 'currentStatus'),
        currentMode = DroidInstruction.jsonInt(json, 'currentMode'),
        missionNumber = DroidInstruction.jsonInt(json, 'missionNumber'),
        droidStarted = DroidInstruction.jsonBool(json, 'droidStarted'),
        serverConnected = DroidInstruction.jsonBool(json, 'serverConnected'),
        serverError = DroidInstruction.jsonBool(json, 'serverError'),
        batteryVoltage = DroidInstruction.jsonDouble(json, 'batteryVoltage'),
        batteryLevel = DroidInstruction.jsonDouble(json, 'batteryLevel'),
        batteryRawLevel = DroidInstruction.jsonInt(json, 'batteryRawLevel'),
        batteryRawScale = DroidInstruction.jsonInt(json, 'batteryRawScale'),
        helisStarted = DroidInstruction.jsonBool(json, 'helisStarted'),
        liftedOff = DroidInstruction.jsonBool(json, 'liftedOff'),
        parachuteDeployed = DroidInstruction.jsonBool(json, 'parachuteDeployed'),
        takingPhoto = DroidInstruction.jsonBool(json, 'takingPhoto'),
        photoNumber = DroidInstruction.jsonInt(json, 'photoNumber'),
        photoStrobe = DroidInstruction.jsonBool(json, 'photoStrobe'),
        photoStrobeDelay = DroidInstruction.jsonInt(json, 'photoStrobeDelay'),
        takingVideo = DroidInstruction.jsonBool(json, 'takingVideo'),
        videoNumber = DroidInstruction.jsonInt(json, 'videoNumber'),
        homePositionAcquired = DroidInstruction.jsonBool(json, 'homePositionAcquired'),
        homePositionX = DroidInstruction.jsonDouble(json, 'homePositionX'),
        homePositionY = DroidInstruction.jsonDouble(json, 'homePositionY'),
        homePositionZ = DroidInstruction.jsonDouble(json, 'homePositionZ'),
        homePositionHeading = DroidInstruction.jsonDouble(json, 'homePositionHeading');
}
