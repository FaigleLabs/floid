/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
class
FloidIdAndState {

  FloidIdAndState();
  /// The no floid id constant:
  static const int NO_FLOID_ID = -1;

  /// The new floid id:
  int floidId = NO_FLOID_ID;
  /// Is the floid currently active
  bool floidActive = false;
  /// Did this ever emit a floid status?
  bool floidStatus = false;
  /// The Floid uuid create time.
  int floidIdCreateTime = 0;
  /// The Floid uuid update time.
  int floidIdUpdateTime = 0;
  /// The Floid id create time string.
  String floidIdCreateTimeString = '';
  /// The Floid id update time string.
  String floidIdUpdateTimeString = '';

  FloidIdAndState.fromJson(Map<String, dynamic> json)
      : floidId = json['floidId']==null?NO_FLOID_ID:json['floidId'],
        floidActive = json['floidActive']==null?false:json['floidActive'],
        floidStatus = json['floidStatus']==null?false:json['floidStatus'],
        floidIdCreateTime = json['floidIdCreateTime']==null?0:json['floidIdCreateTime'],
        floidIdUpdateTime = json['floidIdUpdateTime']==null?0:json['floidIdUpdateTime'],
        floidIdCreateTimeString = json['floidIdCreateTimeString']==null?'':json['floidIdCreateTimeString'],
        floidIdUpdateTimeString = json['floidIdUpdateTimeString']==null?'':json['floidIdUpdateTimeString'];
}
