/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'dart:convert';
import 'dart:typed_data';

import 'package:floid_ui_app/floidserver/floidserver.dart';


class FloidImage {
  int floidId;
  String floidUuid;
  String imageName;
  String imageType;
  String imageExtension;
  double latitude;
  double longitude;
  double altitude;
  double pan;
  double tilt;
  String imageBytesString;
  late Uint8List imageBytes = Uint8List(0);

  FloidImage({
    this.floidId = -1,
    this.floidUuid = '',
    this.imageName = '',
    this.imageType = '',
    this.imageExtension = '',
    this.latitude = 0,
    this.longitude = 0,
    this.altitude = 0,
    this.pan = 0,
    this.tilt = 0,
    this.imageBytesString = '',
  }) {
    if(this.imageBytesString.length>0) {
      this.imageBytes = base64Decode(this.imageBytesString);
    }
  }

  FloidImage.fromJson(Map<String, dynamic> json)
      : this.floidId = json['floidId']==null?-1:json['floidId'],
        this.floidUuid = json['floidUuid']==null?-1:json['floidUuid'],
        this.imageName = DroidInstruction.jsonString(json, 'imageName'),
        this.imageType = DroidInstruction.jsonString(json, 'imageType'),
        this.imageExtension = DroidInstruction.jsonString(json, 'imageExtension'),
        this.latitude = DroidInstruction.jsonDouble(json, 'latitude'),
        this.longitude = DroidInstruction.jsonDouble(json, 'longitude'),
        this.altitude = DroidInstruction.jsonDouble(json, 'altitude'),
        this.pan = DroidInstruction.jsonDouble(json, 'pan'),
        this.tilt = DroidInstruction.jsonDouble(json, 'tilt'),
        this.imageBytesString = json['imageBytes']==null?'':json['imageBytes']{
          this.imageBytes = this.imageBytesString.length>0?base64Decode(imageBytesString):Uint8List(0);
        }
}
