import 'package:floid_ui_app/floidserver/floidserver.dart';

class SpeakerOnCommand extends DroidCommand {

  SpeakerOnCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.SpeakerOnCommand', missionInstructionNumber: -1);

  bool alert = false;
  String mode = 'tts';
  String tts = 'Warning';
  double  pitch = 0;
  double  rate = 0;
  String fileName= '';
  double  volume = 1;
  int speakerRepeat = 1; // 1=None, 0=Infinite
  int delay = 0;

  /// Speaker On Command:
  SpeakerOnCommand.fromJson(Map<String, dynamic> json)
      : alert = json['alert'],
        mode = json['mode'],
        tts = json['tts'],
        pitch = DroidInstruction.jsonDouble(json,'pitch'),
        rate = DroidInstruction.jsonDouble(json,'rate'),
        fileName = json['fileName'],
        volume = DroidInstruction.jsonDouble(json,'volume'),
        speakerRepeat = json['speakerRepeat'],
        delay = json['delay'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'alert': alert,
        'mode': mode,
        'tts': tts,
        'pitch': pitch,
        'rate': rate,
        'fileName': fileName,
        'volume': volume,
        'speakerRepeat': speakerRepeat,
        'delay': delay,
      });
    return json;
  }
}

