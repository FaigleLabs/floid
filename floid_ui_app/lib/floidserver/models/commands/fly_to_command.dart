import 'package:floid_ui_app/floidserver/floidserver.dart';

class FlyToCommand extends DroidCommand {

  FlyToCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.FlyToCommand', missionInstructionNumber: -1);

  double  x = 0;
  double  y = 0;
  String pinName = '';
  bool flyToHome = true;

  /// Fly To Command:
  FlyToCommand.fromJson(Map<String, dynamic> json)
      : x = DroidInstruction.jsonDouble(json,'x'),
        y = DroidInstruction.jsonDouble(json,'y'),
        pinName = json['pinName'],
        flyToHome = json['flyToHome'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'x': x,
        'y': y,
        'pinName': pinName,
        'flyToHome': flyToHome,
      });
    return json;
  }
}

