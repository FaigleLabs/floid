import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class FlyPatternCommand extends DroidCommand {

  FlyPatternCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.FlyPatternCommand', missionInstructionNumber: -1);

  /// Fly Pattern Command:
  FlyPatternCommand.fromJson(Map<String, dynamic> json)
      :  super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

