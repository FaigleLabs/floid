import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class TakePhotoCommand extends DroidCommand {

  TakePhotoCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.TakePhotoCommand', missionInstructionNumber: -1);

  /// Take Photo Command:
  TakePhotoCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

