import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class TestCommand extends DroidCommand {

  TestCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.TestCommand', missionInstructionNumber: -1);

  int testDuration = 0;

  /// Test Command:
  TestCommand.fromJson(Map<String, dynamic> json)
      : testDuration = json['testDuration'],
        super.fromJson(json);
  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'testDuration': testDuration,
      });
    return json;
  }
}

