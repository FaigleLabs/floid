import 'package:floid_ui_app/floidserver/floidserver.dart';

class TurnToCommand extends DroidCommand {

  TurnToCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.TurnToCommand', missionInstructionNumber: -1);

  double heading = 0;
  bool followMode = false;

  /// Turn To Command:
  TurnToCommand.fromJson(Map<String, dynamic> json)
      : heading = DroidInstruction.jsonDouble(json,'heading'),
        followMode = json['followMode'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'heading': heading,
        'followMode': followMode,
      });
    return json;
  }
}

