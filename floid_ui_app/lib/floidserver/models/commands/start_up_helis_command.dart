import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class StartUpHelisCommand extends DroidCommand {

  StartUpHelisCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.StartUpHelisCommand', missionInstructionNumber: -1);

  /// Start Up Helis Command:
  StartUpHelisCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

