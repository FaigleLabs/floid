import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class DelayCommand extends DroidCommand {

  DelayCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.DelayCommand', missionInstructionNumber: -1);

  int delayTime = 5;  // Default delay is 5 seconds

  /// Delay Command:
  DelayCommand.fromJson(Map<String, dynamic> json)
      : delayTime = json['delayTime'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'delayTime': delayTime,
      });
    return json;
  }
}

