import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class SpeakerOffCommand extends DroidCommand {

  SpeakerOffCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.SpeakerOffCommand', missionInstructionNumber: -1);

  /// Speaker Off Command:
  SpeakerOffCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

