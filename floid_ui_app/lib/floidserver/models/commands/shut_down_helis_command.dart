import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class ShutDownHelisCommand extends DroidCommand {

  ShutDownHelisCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.ShutDownHelisCommand', missionInstructionNumber: -1);

  /// Shut Down Helis Command:
  ShutDownHelisCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);
  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

