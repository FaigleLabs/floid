import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class HoverCommand extends DroidCommand {

  HoverCommand(): super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.HoverCommand', missionInstructionNumber: -1);

  int hoverTime = 0;

  /// Hover Command:
  HoverCommand.fromJson(Map<String, dynamic> json)
      : hoverTime = json['hoverTime'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'hoverTime': hoverTime,
      });
    return json;
  }
}

