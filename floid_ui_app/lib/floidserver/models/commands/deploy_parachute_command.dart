import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class DeployParachuteCommand extends DroidCommand {

  DeployParachuteCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.DeployParachuteCommand', missionInstructionNumber: -1);

  /// DeployParachuteCommand Command:
  DeployParachuteCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

