import 'package:floid_ui_app/floidserver/floidserver.dart';

class DesignateCommand extends DroidCommand {

  DesignateCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.DesignateCommand', missionInstructionNumber: -1);

  double  x = 0;
  double  y = 0;
  double  z = 0;
  bool usePin = false;
  bool usePinHeight = false;
  String pinName = '';

  /// Designate Command:
  DesignateCommand.fromJson(Map<String, dynamic> json)
      : x = DroidInstruction.jsonDouble(json,'x'),
        y = DroidInstruction.jsonDouble(json,'y'),
        z = DroidInstruction.jsonDouble(json,'z'),
        usePin = json['usePin'],
        usePinHeight = json['usePinHeight'],
        pinName = json['pinName'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'x': x,
        'y': y,
        'z': z,
        'usePin': usePin,
        'usePinHeight': usePinHeight,
        'pinName': pinName,
      });
    return json;
  }
}

