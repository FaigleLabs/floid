import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class PhotoStrobeCommand extends DroidCommand {

  PhotoStrobeCommand(): super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.PhotoStrobeCommand', missionInstructionNumber: -1);

  int delay = 0;

  /// Photo Strobe Command:
  PhotoStrobeCommand.fromJson(Map<String, dynamic> json)
      : delay = json['delay'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'delay': delay,
      });
    return json;
  }
}

