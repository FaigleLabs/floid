import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class SetParametersCommand extends DroidCommand {

  bool firstPinIsHome = false;
  String homePinName = '';
  String droidParameters = '';
  bool resetDroidParameters = false;

  SetParametersCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.SetParametersCommand', missionInstructionNumber: -1);

  /// Set Parameters Command:
  SetParametersCommand.fromJson(Map<String, dynamic> json)
      : firstPinIsHome = json['firstPinIsHome'],
        homePinName = json['homePinName'],
        droidParameters = json['droidParameters'],
        resetDroidParameters = json['resetDroidParameters'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'firstPinIsHome': firstPinIsHome,
        'homePinName': homePinName,
        'droidParameters': droidParameters,
        'resetDroidParameters': resetDroidParameters,
      });
    return json;
  }
}

