import 'package:floid_ui_app/floidserver/floidserver.dart';

class LandCommand extends DroidCommand {

  LandCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.LandCommand', missionInstructionNumber: -1) {
    z = 0;
  }

  /// Land altitude
  double z = 0; // THIS IS NOT EDITABLE / IT BECOMES GOAL-Z - SET ONLY DURING MISSION SEND

  /// Land Command:
  LandCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json) {
      z = DroidInstruction.jsonDouble(json,'z');
  }

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'z': z,
      });
    return json;
  }
}
