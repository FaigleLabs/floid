import 'package:floid_ui_app/floidserver/floidserver.dart';

/// Droid Command Model
class DroidCommand extends DroidInstruction {

  DroidCommand({required int id, required String type, required int missionInstructionNumber}) : super(id: id, type: type, missionInstructionNumber: missionInstructionNumber);

  /// Droid Command is the super-type for all commands
  DroidCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);
  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

