import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class DebugCommand extends DroidCommand {

  DebugCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.DebugCommand', missionInstructionNumber: -1);

  int deviceIndex = 0;
  int debugValue = 0;

  /// Debug Command:
  DebugCommand.fromJson(Map<String, dynamic> json)
      : deviceIndex = json['deviceIndex'],
        debugValue = json['debugValue'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'deviceIndex': deviceIndex,
        'debugValue': debugValue,
      });
    return json;
  }
}

