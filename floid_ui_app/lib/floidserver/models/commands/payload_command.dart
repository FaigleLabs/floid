import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class PayloadCommand extends DroidCommand {

  int bay=0;

  PayloadCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.PayloadCommand', missionInstructionNumber: -1);

  /// Payload Command:
  PayloadCommand.fromJson(Map<String, dynamic> json)
      : bay = json['bay'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'bay': bay,
      });
    return json;
  }
}

