import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class RetrieveLogsCommand extends DroidCommand {

  RetrieveLogsCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.RetrieveLogsCommand', missionInstructionNumber: -1);

  int logLimit = 0;  // 0 is default = 200 lines

  /// Retrieve Logs Command:
  RetrieveLogsCommand.fromJson(Map<String, dynamic> json)
      : logLimit = json['logLimit'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'logLimit': logLimit,
      });
    return json;
  }
}

