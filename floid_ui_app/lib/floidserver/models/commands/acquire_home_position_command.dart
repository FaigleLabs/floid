import 'package:floid_ui_app/floidserver/floidserver.dart';

class AcquireHomePositionCommand extends DroidCommand {

  AcquireHomePositionCommand(): super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.AcquireHomePositionCommand', missionInstructionNumber: -1);

  double  requiredXYDistance = 25.0;
  double  requiredAltitudeDistance = 10.0;
  double  requiredHeadingDistance = 3.0;
  int requiredGPSGoodCount = 20;
  int requiredAltimeterGoodCount = 10;

  /// Acquire Home Position Command:
  AcquireHomePositionCommand.fromJson(Map<String, dynamic> json)
      : requiredXYDistance = DroidInstruction.jsonDouble(json,'requiredXYDistance'),
        requiredAltitudeDistance = DroidInstruction.jsonDouble(json,'requiredAltitudeDistance'),
        requiredHeadingDistance = DroidInstruction.jsonDouble(json,'requiredHeadingDistance'),
        requiredGPSGoodCount = json['requiredGPSGoodCount'],
        requiredAltimeterGoodCount = json['requiredAltimeterGoodCount'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'requiredXYDistance': requiredXYDistance,
        'requiredAltitudeDistance': requiredAltitudeDistance,
        'requiredHeadingDistance': requiredHeadingDistance,
        'requiredGPSGoodCount': requiredGPSGoodCount,
        'requiredAltimeterGoodCount': requiredAltimeterGoodCount,
      });
    return json;
  }

}

