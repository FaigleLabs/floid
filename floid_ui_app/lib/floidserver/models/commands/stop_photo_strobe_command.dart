import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class StopPhotoStrobeCommand extends DroidCommand {

  StopPhotoStrobeCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.StopPhotoStrobeCommand', missionInstructionNumber: -1);

  /// Stop Photo Strobe Command:
  StopPhotoStrobeCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

