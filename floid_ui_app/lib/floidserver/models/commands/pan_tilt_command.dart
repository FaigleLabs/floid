import 'package:floid_ui_app/floidserver/floidserver.dart';

class PanTiltCommand extends DroidCommand {

  PanTiltCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.PanTiltCommand', missionInstructionNumber: -1);

  int device = 0;
  double  x = 0;
  double  y = 0;
  double  z = 0;
  bool useAngle = true;
  double  pan = 0;
  double  tilt = 0;
  String pinName = "";

  /// Pan Tilt Command:
  PanTiltCommand.fromJson(Map<String, dynamic> json)
      : device = json['device'],
        x = DroidInstruction.jsonDouble(json,'x'),
        y = DroidInstruction.jsonDouble(json,'y'),
        z = DroidInstruction.jsonDouble(json,'z'),
        useAngle = json['useAngle'],
        pan = DroidInstruction.jsonDouble(json,'pan'),
        tilt = DroidInstruction.jsonDouble(json,'tilt'),
        pinName = json['pinName'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'device': device,
        'x': x,
        'y': y,
        'z': z,
        'useAngle': useAngle,
        'pan': pan,
        'tilt': tilt,
        'pinName': pinName,
      });
    return json;
  }
}

