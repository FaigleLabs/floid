import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class StopDesignatingCommand extends DroidCommand {

  StopDesignatingCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.StopDesignatingCommand', missionInstructionNumber: -1);

  /// Stop Designating Command:
  StopDesignatingCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

