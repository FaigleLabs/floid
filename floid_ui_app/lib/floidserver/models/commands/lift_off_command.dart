import 'package:floid_ui_app/floidserver/floidserver.dart';

class LiftOffCommand extends DroidCommand {

  LiftOffCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.LiftOffCommand', missionInstructionNumber: -1) {
    z = 0.0;
    absolute = false;
  }
  double z = 0.0;
  bool absolute = false;

  /// Lift Off Command:
  LiftOffCommand.fromJson(Map<String, dynamic> json)
      : z = DroidInstruction.jsonDouble(json,'z'),
        absolute = json['absolute'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'z': z,
        'absolute': absolute,
      });
    return json;
  }
}

