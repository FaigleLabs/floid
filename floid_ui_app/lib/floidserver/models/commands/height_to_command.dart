import 'package:floid_ui_app/floidserver/floidserver.dart';

class HeightToCommand extends DroidCommand {

  HeightToCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.HeightToCommand', missionInstructionNumber: -1);

  double  z = 0;
  bool absolute = false;

  /// Height To Command:
  HeightToCommand.fromJson(Map<String, dynamic> json)
      : z = DroidInstruction.jsonDouble(json,'z'),
        absolute = json['absolute'],
        super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'z': z,
        'absolute': absolute,
      });
    return json;
  }
}

