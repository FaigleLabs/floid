import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class NullCommand extends DroidCommand {

  NullCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.NullCommand', missionInstructionNumber: -1);

  /// Null Command:
  NullCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);
  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

