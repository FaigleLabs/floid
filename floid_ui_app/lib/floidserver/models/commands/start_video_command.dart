import 'package:floid_ui_app/floidserver/models/commands/droid_command.dart';

class StartVideoCommand extends DroidCommand {

  StartVideoCommand() : super(id: -1, type: 'com.faiglelabs.floid.servertypes.commands.StartVideoCommand', missionInstructionNumber: -1);

  /// Start Video Command:
  StartVideoCommand.fromJson(Map<String, dynamic> json)
      : super.fromJson(json);

  @override
  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = super.toJson()
      ..addAll({
      });
    return json;
  }
}

