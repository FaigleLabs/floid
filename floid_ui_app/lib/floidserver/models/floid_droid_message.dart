/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
class FloidDroidMessage {

  FloidDroidMessage(String message) : this.message = message;

  /// The message
  String message;

  FloidDroidMessage.fromJson(Map<String, dynamic> json)
      : message = json['message'];

}
