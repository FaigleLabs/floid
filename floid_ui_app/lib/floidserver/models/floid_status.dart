/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';

class FloidStatus {

  FloidStatus();

  /// Test mode
  static const int FLOID_MODE_TEST = 0;

  /// Production mode:
  static const int FLOID_RUN_MODE_PRODUCTION = 0x00;

  // Run modes for tests:
  static const int FLOID_RUN_MODE_PHYSICS_TEST_XY = 0x01;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1 = 0x02;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2 = 0x04;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1 = 0x08;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1 = 0x10;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1 = 0x20;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_XY_BIT = 0;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1_BIT = 1;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2_BIT = 2;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1_BIT = 3;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1_BIT = 4;
  static const int FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1_BIT = 5;

  // Flags for tests:
  static const int FLOID_TEST_MODE_OFF = 0x00;
  static const int FLOID_TEST_MODE_PHYSICS_NO_XY = 0x01;
  static const int FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE = 0x02;
  static const int FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE = 0x04;
  static const int FLOID_TEST_MODE_PHYSICS_NO_HEADING = 0x08;
  static const int FLOID_TEST_MODE_PHYSICS_NO_PITCH = 0x10;
  static const int FLOID_TEST_MODE_PHYSICS_NO_ROLL = 0x20;
  static const int FLOID_TEST_MODE_PRINT_DEBUG_PHYSICS_HEADINGS = 0x40;
  static const int FLOID_TEST_MODE_CHECK_PHYSICS_MODEL = 0x80;
  static const int FLOID_TEST_MODE_PHYSICS_NO_XY_BIT = 0;
  static const int FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE_BIT = 1;
  static const int FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE_BIT = 2;
  static const int FLOID_TEST_MODE_PHYSICS_NO_HEADING_BIT = 3;
  static const int FLOID_TEST_MODE_PHYSICS_NO_PITCH_BIT = 4;
  static const int FLOID_TEST_MODE_PHYSICS_NO_ROLL_BIT = 5;
  static const int FLOID_TEST_MODE_PRINT_DEBUG_PHYSICS_HEADINGS_BIT = 6;
  static const int FLOID_TEST_MODE_CHECK_PHYSICS_MODEL_BIT = 7;

  // Status time droid - added by the droid, not the arduino
  /// Status time (Droid)
  int std = 0; // Status time droid
  // Status time: - 'st'
  /// Status time
  int st = 0; // STATUS_PACKET_OFFSET_STATUS_TIME
  // Status number: - 'sn'
  /// Status number
  int sn = -1; // STATUS_PACKET_OFFSET_STATUS_NUMBER
  // Floid mode: - 'fm'
  /// Floid mode
  int fm = 0; // STATUS_PACKET_OFFSET_MODE
  // Free memory: - 'ffm'
  /// Free memory
  int ffm = 0; // STATUS_PACKET_OFFSET_FREE_MEMORY
  // Follow mode: -fmf
  /// Follow mode
  bool fmf = false; // STATUS_PACKET_OFFSET_FOLLOW_MODE
  // System Device Status: - 'd'
  /// Camera on
  bool dc = false; // STATUS_PACKET_OFFSET_CAMERA_ON
  /// GPS on
  bool dg = false; // STATUS_PACKET_OFFSET_GPS_ON
  /// GPS emulate
  bool df = false; // STATUS_PACKET_OFFSET_GPS_EMULATE
  /// GPS from device
  bool dv = false; // STATUS_PACKET_OFFSET_GPS_DEVICE
  /// IMU on
  bool dp = false; // STATUS_PACKET_OFFSET_PYR_ON
  /// Designator on
  bool dd = false; // STATUS_PACKET_OFFSET_DESIGNATOR_ON
  /// AUS on
  bool da = false; // STATUS_PACKET_OFFSET_AUX_ON
  /// LED on
  bool dl = false; // STATUS_PACKET_OFFSET_LED_ON
  /// Helis on
  bool dh = false; // STATUS_PACKET_OFFSET_HELIS_ON
  /// Parachute deployed
  bool de = false; // STATUS_PACKET_OFFSET_PARACHUTE_DEPLOYED
  // Physics Model: - 'k'
  /// Direction over ground (std trig degrees - not compass degrees)
  double kd = 0; // STATUS_PACKET_OFFSET_DIRECTION_OVER_GROUND
  /// Velocity over ground - in std trig degrees as above
  double kv = 0; // STATUS_PACKET_OFFSET_VELOCITY_OVER_GROUND
  /// Altitude (m)
  double ka = 0; // STATUS_PACKET_OFFSET_ALTITUDE
  /// Altitude Velocity (m/s)
  double kw = 0; // STATUS_PACKET_OFFSET_ALTITUDE_VELOCITY
  /// Declination
  double kc = 0; // STATUS_PACKET_OFFSET_DECLINATION (declination?)
  // GPS: - 'j'
  /// GPS Time
  int jt = 0; // STATUS_PACKET_OFFSET_GPS_TIME
  /// GPS Longitude degrees decimal
  double jx = 0; // STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL
  /// GPS latitude degrees decimal
  double jy = 0; // STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL
  /// GPS fix quality
  int jf = 0; // STATUS_PACKET_OFFSET_GPS_FIX_QUALITY
  /// GPS altitude (meters)
  double jm = 0; // STATUS_PACKET_OFFSET_GPS_Z_METERS
  /// GPS altitude (feet)
  double jz = 0; // STATUS_PACKET_OFFSET_GPS_Z_FEET
  /// GPS HDOP
  double jh = 0; // STATUS_PACKET_OFFSET_GPS_HDOP
  /// GPS sats
  int js = 0; // STATUS_PACKET_OFFSET_GPS_SATS
  /// GPS longitude filtered
  double jxf = 0; // STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL_FILTERED
  /// GPS latitude filtered
  double jyf = 0; // STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL_FILTERED
  /// GPS altitude meters filtered
  double jmf = 0; // STATUS_PACKET_OFFSET_GPS_Z_METERS_FILTERED
  /// GPS good count
  int jg = 0; // STATUS_PACKET_OFFSET_GPS_GOOD_COUNT
  /// GPS rate
  int jr = 0; // STATUS_PACKET_OFFSET_GPS_RATE
  // IMU: - 'p'
  /// IMU heading
  double ph = 0; // STATUS_PACKET_OFFSET_PYR_HEADING
  /// IMU heading smoothed
  double phs = 0; // STATUS_PACKET_OFFSET_PYR_HEADING_SMOOTHED
  /// IMU heading velocity
  double phv = 0; // STATUS_PACKET_OFFSET_PYR_HEADING_VELOCITY
  /// IMU altitude
  double pz = 0; // STATUS_PACKET_OFFSET_PYR_HEIGHT
  /// IMU altitude smoothed
  double pzs = 0; // STATUS_PACKET_OFFSET_PYR_HEIGHT_SMOOTHED
  /// IMU temperature
  double pt = 0; // STATUS_PACKET_OFFSET_PYR_TEMPERATURE
  /// IMU pitch
  double pp = 0; // STATUS_PACKET_OFFSET_PYR_PITCH
  /// IMU pitch smoothed
  double pps = 0; // STATUS_PACKET_OFFSET_PYR_PITCH_SMOOTHED
  /// IMU device on
  bool pv = false; // STATUS_PACKET_OFFSET_PYR_DEVICE
  /// IMU roll
  double pr = 0; // STATUS_PACKET_OFFSET_PYR_ROLL
  /// IMU roll smoothed
  double prs = 0; // STATUS_PACKET_OFFSET_PYR_ROLL_SMOOTHED
  /// IMU pitch velocity
  double ppv = 0; // STATUS_PACKET_OFFSET_PYR_PITCH_VELOCITY
  /// IMU roll velocity
  double prv = 0; // STATUS_PACKET_OFFSET_PYR_ROLL_VELOCITY
  /// IMU altitude velocity
  double pzv = 0; // STATUS_PACKET_OFFSET_PYR_HEIGHT_VELOCITY
  /// IMU digital mode
  bool pd = false; // STATUS_PACKET_OFFSET_PYR_DIGITAL_MODE
  /// IMU analog rate
  int par = 0; // STATUS_PACKET_OFFSET_PYR_ANALOG_RATE
  /// IMU good count
  int pg = 0; // STATUS_PACKET_OFFSET_PYR_GOOD_COUNT
  /// IMU error count
  int pe = 0; // STATUS_PACKET_OFFSET_PYR_ERROR_COUNT
  // Batteries: - 'b'
  /// Main battery value
  double bm = 0; // STATUS_PACKET_OFFSET_MAIN_BATTERY
  /// Heli 0 battery value
  double b0 = 0; // STATUS_PACKET_OFFSET_HELI0_BATTERY
  /// Heli 1 battery value
  double b1 = 0; // STATUS_PACKET_OFFSET_HELI1_BATTERY
  /// Heli 2 battery value
  double b2 = 0; // STATUS_PACKET_OFFSET_HELI2_BATTERY
  /// Heli 3 battery value
  double b3 = 0; // STATUS_PACKET_OFFSET_HELI3_BATTERY
  /// AUX battery value
  double ba = 0; // STATUS_PACKET_OFFSET_AUX_BATTERY
  // Currents: - 'c'
  /// Heli 0 current
  double c0 = 0; // STATUS_PACKET_OFFSET_HELI0_CURRENT
  /// Heli 1 current
  double c1 = 0; // STATUS_PACKET_OFFSET_HELI1_CURRENT
  /// Heli 2 current
  double c2 = 0; // STATUS_PACKET_OFFSET_HELI2_CURRENT
  /// Heli 3 current
  double c3 = 0; // STATUS_PACKET_OFFSET_HELI3_CURRENT
  /// AUX current
  double ca = 0; // STATUS_PACKET_OFFSET_AUX_CURRENT
  // Servos - 's#'
  /// Heli 0 servos on
  bool s0o = false; // STATUS_PACKET_OFFSET_S_0_ON
  /// Heli 1 servos on
  bool s1o = false; // STATUS_PACKET_OFFSET_S_1_ON
  /// Heli 2 servos on
  bool s2o = false; // STATUS_PACKET_OFFSET_S_2_ON
  /// Heli 3 servos on
  bool s3o = false; // STATUS_PACKET_OFFSET_S_3_ON
  /// Heli 0 Servo 0 (L) value
  double s00 = 0; // STATUS_PACKET_OFFSET_S_0_L_VALUE
  /// Heli 0 Servo 1 (R) value
  double s01 = 1; // STATUS_PACKET_OFFSET_S_0_R_VALUE
  /// Heli 0 Servo 2 (P) value
  double s02 = 0; // STATUS_PACKET_OFFSET_S_0_P_VALUE
  /// Heli 1 Servo 0 (L) value
  double s10 = 0; // STATUS_PACKET_OFFSET_S_1_L_VALUE
  /// Heli 1 Servo 1 (R) value
  double s11 = 1; // STATUS_PACKET_OFFSET_S_1_R_VALUE
  /// Heli 1 Servo 2 (P) value
  double s12 = 0; // STATUS_PACKET_OFFSET_S_1_P_VALUE
  /// Heli 2 Servo 0 (L) value
  double s20 = 0; // STATUS_PACKET_OFFSET_S_2_L_VALUE
  /// Heli 2 Servo 1 (R) value
  double s21 = 1; // STATUS_PACKET_OFFSET_S_2_R_VALUE
  /// Heli 2 Servo 2 (P) value
  double s22 = 0; // STATUS_PACKET_OFFSET_S_2_P_VALUE
  /// Heli 3 Servo 0 (L) value
  double s30 = 0; // STATUS_PACKET_OFFSET_S_3_L_VALUE
  /// Heli 3 Servo 1 (R) value
  double s31 = 1; // STATUS_PACKET_OFFSET_S_3_R_VALUE
  /// Heli 3 Servo 2 (P) value
  double s32 = 0; // STATUS_PACKET_OFFSET_S_3_P_VALUE
  // ESCs: - 'e'
  /// ESC type
  int et = 0; // STATUS_PACKET_OFFSET_ESC_TYPE
  /// ESC 0 on
  bool e0o = false; // STATUS_PACKET_OFFSET_ESC_0_ON
  /// ESC 1 on
  bool e1o = false; // STATUS_PACKET_OFFSET_ESC_1_ON
  /// ESC 2 on
  bool e2o = false; // STATUS_PACKET_OFFSET_ESC_2_ON
  /// ESC 3 on
  bool e3o = false; // STATUS_PACKET_OFFSET_ESC_3_ON
  /// ESC 0 value
  double e0 = 0; // STATUS_PACKET_OFFSET_ESC_0_VALUE
  /// ESC 1 value
  double e1 = 0; // STATUS_PACKET_OFFSET_ESC_1_VALUE
  /// ESC 2 value
  double e2 = 0; // STATUS_PACKET_OFFSET_ESC_2_VALUE
  /// ESC 3 value
  double e3 = 0; // STATUS_PACKET_OFFSET_ESC_3_VALUE
  /// ESC 0 pulse
  int ep0 = 0; // STATUS_PACKET_OFFSET_ESC_0_PULSE_VALUE
  /// ESC 1 pulse
  int ep1 = 0; // STATUS_PACKET_OFFSET_ESC_1_PULSE_VALUE
  /// ESC 2 pulse
  int ep2 = 0; // STATUS_PACKET_OFFSET_ESC_2_PULSE_VALUE
  /// ESC 3 pulse
  int ep3 = 0; // STATUS_PACKET_OFFSET_ESC_3_PULSE_VALUE
  // Goals: - 'g'
  /// Has goal
  bool ghg = false; // STATUS_PACKET_OFFSET_HAS_GOAL
  /// Goal id
  int gid = 0; // STATUS_PACKET_OFFSET_GOAL_ID
  /// Current goal ticks
  int gt = 0; // STATUS_PACKET_OFFSET_GOAL_TICKS
  /// Current goal state
  int gs = 0; // STATUS_PACKET_OFFSET_GOAL_STATE
  /// Goal longitude
  double gx = 0; // STATUS_PACKET_OFFSET_GOAL_X
  /// Goal latitude
  double gy = 0; // STATUS_PACKET_OFFSET_GOAL_Y
  /// Goal altitude
  double gz = 0; // STATUS_PACKET_OFFSET_GOAL_Z
  /// Goal heading (compass degrees)
  double ga = 0; // STATUS_PACKET_OFFSET_GOAL_A  // IN HEADING=COMPASS DEGREES
  /// In flight
  bool gif = false; // STATUS_PACKET_OFFSET_IN_FLIGHT
  /// Lift off mode
  bool glo = false; // STATUS_PACKET_OFFSET_LIFT_OFF_MODE
  /// Land mode
  bool gld = false; // STATUS_PACKET_OFFSET_LAND_MODE
  // Pan Tilt: - 't'
  /// Pan/Tilt 0 mode
  int t0m = 0; // STATUS_PACKET_OFFSET_PT_0_MODE
  /// Pan/Tilt 0 mode
  int t1m = 0; // STATUS_PACKET_OFFSET_PT_1_MODE
  /// Pan/Tilt 0 mode
  int t2m = 0; // STATUS_PACKET_OFFSET_PT_2_MODE
  /// Pan/Tilt 0 mode
  int t3m = 0; // STATUS_PACKET_OFFSET_PT_3_MODE
  /// Pan/Tilt 0 target longitude
  double t0x = 0; // STATUS_PACKET_OFFSET_PT_0_TARGET_X
  /// Pan/Tilt 0 target latitude
  double t0y = 0; // STATUS_PACKET_OFFSET_PT_0_TARGET_Y
  /// Pan/Tilt 0 target altitude
  double t0z = 0; // STATUS_PACKET_OFFSET_PT_0_TARGET_Z
  /// Pan/Tilt 1 target longitude
  double t1x = 0; // STATUS_PACKET_OFFSET_PT_1_TARGET_X
  /// Pan/Tilt 1 target latitude
  double t1y = 0; // STATUS_PACKET_OFFSET_PT_1_TARGET_Y
  /// Pan/Tilt 1 target altitude
  double t1z = 0; // STATUS_PACKET_OFFSET_PT_1_TARGET_Z
  /// Pan/Tilt 2 target longitude
  double t2x = 0; // STATUS_PACKET_OFFSET_PT_2_TARGET_X
  /// Pan/Tilt 2 target latitude
  double t2y = 0; // STATUS_PACKET_OFFSET_PT_2_TARGET_Y
  /// Pan/Tilt 2 target altitude
  double t2z = 0; // STATUS_PACKET_OFFSET_PT_2_TARGET_Z
  /// Pan/Tilt 0 target longitude
  double t3x = 0; // STATUS_PACKET_OFFSET_PT_3_TARGET_X
  /// Pan/Tilt 3 target latitude
  double t3y = 0; // STATUS_PACKET_OFFSET_PT_3_TARGET_Y
  /// Pan/Tilt 3 target altitude
  double t3z = 0; // STATUS_PACKET_OFFSET_PT_3_TARGET_Z
  /// Pan/Tilt 0 on
  bool t0o = false; // STATUS_PACKET_OFFSET_PT_0_ON
  /// Pan/Tilt 1 on
  bool t1o = false; // STATUS_PACKET_OFFSET_PT_1_ON
  /// Pan/Tilt 2 on
  bool t2o = false; // STATUS_PACKET_OFFSET_PT_2_ON
  /// Pan/Tilt 3 on
  bool t3o = false; // STATUS_PACKET_OFFSET_PT_3_ON
  /// Pan/Tilt 0 pan value
  int t0p = 0; // STATUS_PACKET_OFFSET_PT_0_PAN_VALUE
  /// Pan/Tilt 0 tilt value
  int t0t = 0; // STATUS_PACKET_OFFSET_PT_0_TILT_VALUE
  /// Pan/Tilt 1 pan value
  int t1p = 0; // STATUS_PACKET_OFFSET_PT_1_PAN_VALUE
  /// Pan/Tilt 1 tilt value
  int t1t = 0; // STATUS_PACKET_OFFSET_PT_1_TILT_VALUE
  /// Pan/Tilt 2 pan value
  int t2p = 0; // STATUS_PACKET_OFFSET_PT_2_PAN_VALUE
  /// Pan/Tilt 2 tilt value
  int t2t = 0; // STATUS_PACKET_OFFSET_PT_2_TILT_VALUE
  /// Pan/Tilt 3 pan value
  int t3p = 0; // STATUS_PACKET_OFFSET_PT_3_PAN_VALUE
  /// Pan/Tilt 3 tilt value
  int t3t = 0; // STATUS_PACKET_OFFSET_PT_3_TILT_VALUE
  // Payloads: - 'y'
  /// Payload 0 on
  bool y0 = false; // STATUS_PACKET_OFFSET_BAY0
  /// Payload 1 on
  bool y1 = false; // STATUS_PACKET_OFFSET_BAY1
  /// Payload 2 on
  bool y2 = false; // STATUS_PACKET_OFFSET_BAY2
  /// Payload 3 on
  bool y3 = false; // STATUS_PACKET_OFFSET_BAY3
  /// Payload 0 nm value
  int yn0 = 0; // STATUS_PACKET_OFFSET_NM0
  /// Payload 1 nm value
  int yn1 = 0; // STATUS_PACKET_OFFSET_NM1
  /// Payload 2 nm value
  int yn2 = 0; // STATUS_PACKET_OFFSET_NM2
  /// Payload 3 nm value
  int yn3 = 0; // STATUS_PACKET_OFFSET_NM3
  /// Payload 0 goal state
  int yg0 = 0; // STATUS_PACKET_OFFSET_PAYLOAD_0_GOAL_STATE
  /// Payload 1 goal state
  int yg1 = 0; // STATUS_PACKET_OFFSET_PAYLOAD_1_GOAL_STATE
  /// Payload 2 goal state
  int yg2 = 0; // STATUS_PACKET_OFFSET_PAYLOAD_2_GOAL_STATE
  /// Payload 3 goal state
  int yg3 = 0; // STATUS_PACKET_OFFSET_PAYLOAD_3_GOAL_STATE
  // Debug: - 'v'
  /// Is debug on
  bool vd = false; // STATUS_PACKET_OFFSET_DEBUG_ON
  /// Is debug all on
  bool va = false; // STATUS_PACKET_OFFSET_DEBUG_ALL_ON
  /// Is debug AUX on
  bool vx = false; // STATUS_PACKET_OFFSET_DEBUG_AUX_ON
  /// Is debug payloads on
  bool vb = false; // STATUS_PACKET_OFFSET_DEBUG_PAYLOADS_ON
  /// Is debug camera on
  bool vc = false; // STATUS_PACKET_OFFSET_DEBUG_CAMERA_ON
  /// Is debug designator on
  bool ve = false; // STATUS_PACKET_OFFSET_DEBUG_DESIGNATOR_ON
  /// Is debug GPS on
  bool vg = false; // STATUS_PACKET_OFFSET_DEBUG_GPS_ON
  /// Is debug helis on
  bool vh = false; // STATUS_PACKET_OFFSET_DEBUG_HELIS_ON
  /// Is debug memory on
  bool vm = false; // STATUS_PACKET_OFFSET_DEBUG_MEMORY_ON
  /// Is debug pan/tilt on
  bool vt = false; // STATUS_PACKET_OFFSET_DEBUG_PAN_TILT_ON
  /// Is debug physics on
  bool vy = false; // STATUS_PACKET_OFFSET_DEBUG_PHYSICS_ON
  /// Is debug IMU on
  bool vp = false; // STATUS_PACKET_OFFSET_DEBUG_PYR_ON
  /// Is debug altimeter on
  bool vl = false; // STATUS_PACKET_OFFSET_DEBUG_ALTIMETER
  /// Is debug serial on
  bool vs = false; // STATUS_PACKET_OFFSET_SERIAL_ON
  // Altimeter: -'a'
  /// Altimeter initialized
  bool ai = false; // STATUS_PACKET_OFFSET_ALTIMETER_INITIALIZED
  /// Altimeter altitude valid
  bool av = false; // STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE_VALID
  /// Altimeter altitude
  double aa = 0; // STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE
  /// Altimeter delta
  double ad = 0; // STATUS_PACKET_OFFSET_ALTIMETER_DELTA
  /// Altimeter good count
  int ac = 0; // STATUS_PACKET_OFFSET_ALTIMETER_GOOD_COUNT
  // Loop counts and rates - 'l'
  /// Loop count since last update
  int lc = 0; // Loop count since last status update
  /// Loop rate since last update
  double lr = 0; // Loop rate since last status update
  /// Floid status rate
  int sr = 0; // Floid status rate
  // Model: - 'm'
  /// Lift off target altitude
  double mt = 0; // liftOffTargetAltitude = 0;
  /// Distance to target
  double md = 0; // distanceToTarget = 0;
  /// Altitude to target
  double ma = 0; // altitudeToTarget = 0;
  /// Delta heading angle to target
  double mo = 0; // deltaHeadingAngleToTarget = 0;
  /// Orientation to goal
  double mg = 0; // orientationToGoal = 0;
  /// Attack angle
  double mk = 0; // attackAngle = 0;
  // Target: - 'o'
  /// Target orientation pitch
  double oop = 0; // targetOrientationPitch = 0;
  /// Target orientation pitch delta
  double ooq = 0; // targetOrientationPitchDelta = 0;
  /// Target orientation roll
  double oor = 0; // targetOrientationRoll = 0;
  /// Target orientation roll delta
  double oos = 0; // targetOrientationRollDelta = 0;
  /// Target pitch velocity
  double opv = 0; // targetPitchVelocity = 0;
  /// Target pitch velocity delta
  double opw = 0; // targetPitchVelocityDelta = 0;
  /// Target roll velocity
  double orv = 0; // targetRollVelocity = 0;
  /// Target roll velocity delta
  double orw = 0; // targetRollVelocityDelta = 0;
  /// Target altitude velocity
  double oav = 0; // targetAltitudeVelocity = 0;
  /// Target altitude velocity delta
  double oaw = 0; // targetAltitudeVelocityDelta = 0;
  /// Target heading velocity
  double ohv = 0; // targetHeadingVelocity = 0;
  /// Target heading velocity delta
  double ohw = 0; // targetHeadingVelocityDelta = 0;
  /// Target xy velocity (over ground)
  double ov = 0; // targetXYVelocity = 0;
  // Land mode - 'l'
  /// Land mode min altitude check started
  bool lmc = false; // landModeMinAltitudeCheckStarted
  /// Land mode min altitude
  double lma = 0; // landModeMinAltitude
  /// Land mode min altitude time
  int lmt = 0; // landModeMinAltitudeTime
  // Heli Startup Mode: -'h'
  /// Heli startup mode
  bool hsm = false; // heliStartupMode
  // IMU from device:
  /// IMU from device
  bool dpy = false; // devicePYR
  // Debug altimeter:
  /// Debug altimeter
  bool dba = false; // debugAltimeter
  // Run and test modes
  /// Floid run mode
  int rm = 0; // floidRunMode
  /// Floid test mode
  int tm = 0; // floidTestMode
  /// IMU algorithm status:
  int ias = 0;

  /// Convert from JSON
  FloidStatus.fromJson(Map<String, dynamic> json)
      : std = DroidInstruction.jsonInt(json, 'std'),
        st = DroidInstruction.jsonInt(json, 'st'),
        sn = DroidInstruction.jsonInt(json, 'sn'),
        fm = DroidInstruction.jsonInt(json, 'fm'),
        ffm = DroidInstruction.jsonInt(json, 'ffm'),
        fmf = DroidInstruction.jsonBool(json, 'fmf'),
        dc = DroidInstruction.jsonBool(json, 'dc'),
        dg = DroidInstruction.jsonBool(json, 'dg'),
        df = DroidInstruction.jsonBool(json, 'df'),
        dv = DroidInstruction.jsonBool(json, 'dv'),
        dp = DroidInstruction.jsonBool(json, 'dp'),
        dd = DroidInstruction.jsonBool(json, 'dd'),
        da = DroidInstruction.jsonBool(json, 'da'),
        dl = DroidInstruction.jsonBool(json, 'dl'),
        dh = DroidInstruction.jsonBool(json, 'dh'),
        de = DroidInstruction.jsonBool(json, 'de'),
        kd = DroidInstruction.jsonDouble(json, 'kd'),
        kv = DroidInstruction.jsonDouble(json, 'kv'),
        ka = DroidInstruction.jsonDouble(json, 'ka'),
        kw = DroidInstruction.jsonDouble(json, 'kw'),
        kc = DroidInstruction.jsonDouble(json, 'kc'),
        jt = DroidInstruction.jsonInt(json, 'jt'),
        jx = DroidInstruction.jsonDouble(json, 'jx'),
        jy = DroidInstruction.jsonDouble(json, 'jy'),
        jf = DroidInstruction.jsonInt(json, 'jf'),
        jm = DroidInstruction.jsonDouble(json, 'jm'),
        jz = DroidInstruction.jsonDouble(json, 'jz'),
        jh = DroidInstruction.jsonDouble(json, 'jh'),
        js = DroidInstruction.jsonInt(json, 'js'),
        jxf = DroidInstruction.jsonDouble(json, 'jxf'),
        jyf = DroidInstruction.jsonDouble(json, 'jyf'),
        jmf = DroidInstruction.jsonDouble(json, 'jmf'),
        jg = DroidInstruction.jsonInt(json, 'jg'),
        jr = DroidInstruction.jsonInt(json, 'jr'),
        ph = DroidInstruction.jsonDouble(json, 'ph'),
        phs = DroidInstruction.jsonDouble(json, 'phs'),
        phv = DroidInstruction.jsonDouble(json, 'phv'),
        pz = DroidInstruction.jsonDouble(json, 'pz'),
        pzs = DroidInstruction.jsonDouble(json, 'pzs'),
        pt = DroidInstruction.jsonDouble(json, 'pt'),
        pp = DroidInstruction.jsonDouble(json, 'pp'),
        pps = DroidInstruction.jsonDouble(json, 'pps'),
        pv = DroidInstruction.jsonBool(json, 'pv'),
        pr = DroidInstruction.jsonDouble(json, 'pr'),
        prs = DroidInstruction.jsonDouble(json, 'prs'),
        ppv = DroidInstruction.jsonDouble(json, 'ppv'),
        prv = DroidInstruction.jsonDouble(json, 'prv'),
        pzv = DroidInstruction.jsonDouble(json, 'pzv'),
        pd = DroidInstruction.jsonBool(json, 'pd'),
        par = DroidInstruction.jsonInt(json, 'par'),
        pg = DroidInstruction.jsonInt(json, 'pg'),
        pe = DroidInstruction.jsonInt(json, 'pe'),
        bm = DroidInstruction.jsonDouble(json, 'bm'),
        b0 = DroidInstruction.jsonDouble(json, 'b0'),
        b1 = DroidInstruction.jsonDouble(json, 'b1'),
        b2 = DroidInstruction.jsonDouble(json, 'b2'),
        b3 = DroidInstruction.jsonDouble(json, 'b3'),
        ba = DroidInstruction.jsonDouble(json, 'ba'),
        c0 = DroidInstruction.jsonDouble(json, 'c0'),
        c1 = DroidInstruction.jsonDouble(json, 'c1'),
        c2 = DroidInstruction.jsonDouble(json, 'c2'),
        c3 = DroidInstruction.jsonDouble(json, 'c3'),
        ca = DroidInstruction.jsonDouble(json, 'ca'),
        s0o = DroidInstruction.jsonBool(json, 's0o'),
        s1o = DroidInstruction.jsonBool(json, 's1o'),
        s2o = DroidInstruction.jsonBool(json, 's2o'),
        s3o = DroidInstruction.jsonBool(json, 's3o'),
        s00 = DroidInstruction.jsonDouble(json, 's00'),
        s01 = DroidInstruction.jsonDouble(json, 's01'),
        s02 = DroidInstruction.jsonDouble(json, 's02'),
        s10 = DroidInstruction.jsonDouble(json, 's10'),
        s11 = DroidInstruction.jsonDouble(json, 's11'),
        s12 = DroidInstruction.jsonDouble(json, 's12'),
        s20 = DroidInstruction.jsonDouble(json, 's20'),
        s21 = DroidInstruction.jsonDouble(json, 's21'),
        s22 = DroidInstruction.jsonDouble(json, 's22'),
        s30 = DroidInstruction.jsonDouble(json, 's30'),
        s31 = DroidInstruction.jsonDouble(json, 's31'),
        s32 = DroidInstruction.jsonDouble(json, 's32'),
        et = DroidInstruction.jsonInt(json, 'et'),
        e0o = DroidInstruction.jsonBool(json, 'e0o'),
        e1o = DroidInstruction.jsonBool(json, 'e1o'),
        e2o = DroidInstruction.jsonBool(json, 'e2o'),
        e3o = DroidInstruction.jsonBool(json, 'e3o'),
        e0 = DroidInstruction.jsonDouble(json, 'e0'),
        e1 = DroidInstruction.jsonDouble(json, 'e1'),
        e2 = DroidInstruction.jsonDouble(json, 'e2'),
        e3 = DroidInstruction.jsonDouble(json, 'e3'),
        ep0 = DroidInstruction.jsonInt(json, 'ep0'),
        ep1 = DroidInstruction.jsonInt(json, 'ep1'),
        ep2 = DroidInstruction.jsonInt(json, 'ep2'),
        ep3 = DroidInstruction.jsonInt(json, 'ep3'),
        ghg = DroidInstruction.jsonBool(json, 'ghg'),
        gid = DroidInstruction.jsonInt(json, 'gid'),
        gt = DroidInstruction.jsonInt(json, 'gt'),
        gs = DroidInstruction.jsonInt(json, 'gs'),
        gx = DroidInstruction.jsonDouble(json, 'gx'),
        gy = DroidInstruction.jsonDouble(json, 'gy'),
        gz = DroidInstruction.jsonDouble(json, 'gz'),
        ga = DroidInstruction.jsonDouble(json, 'ga'),
        gif = DroidInstruction.jsonBool(json, 'gif'),
        glo = DroidInstruction.jsonBool(json, 'glo'),
        gld = DroidInstruction.jsonBool(json, 'gld'),
        t0m = DroidInstruction.jsonInt(json, 't0m'),
        t1m = DroidInstruction.jsonInt(json, 't1m'),
        t2m = DroidInstruction.jsonInt(json, 't2m'),
        t3m = DroidInstruction.jsonInt(json, 't3m'),
        t0x = DroidInstruction.jsonDouble(json, 't0x'),
        t0y = DroidInstruction.jsonDouble(json, 't0y'),
        t0z = DroidInstruction.jsonDouble(json, 't0z'),
        t1x = DroidInstruction.jsonDouble(json, 't1x'),
        t1y = DroidInstruction.jsonDouble(json, 't1y'),
        t1z = DroidInstruction.jsonDouble(json, 't1z'),
        t2x = DroidInstruction.jsonDouble(json, 't2x'),
        t2y = DroidInstruction.jsonDouble(json, 't2y'),
        t2z = DroidInstruction.jsonDouble(json, 't2z'),
        t3x = DroidInstruction.jsonDouble(json, 't3x'),
        t3y = DroidInstruction.jsonDouble(json, 't3y'),
        t3z = DroidInstruction.jsonDouble(json, 't3z'),
        t0o = DroidInstruction.jsonBool(json, 't0o'),
        t1o = DroidInstruction.jsonBool(json, 't1o'),
        t2o = DroidInstruction.jsonBool(json, 't2o'),
        t3o = DroidInstruction.jsonBool(json, 't3o'),
        t0p = DroidInstruction.jsonInt(json, 't0p'),
        t0t = DroidInstruction.jsonInt(json, 't0t'),
        t1p = DroidInstruction.jsonInt(json, 't1p'),
        t1t = DroidInstruction.jsonInt(json, 't1t'),
        t2p = DroidInstruction.jsonInt(json, 't2p'),
        t2t = DroidInstruction.jsonInt(json, 't2t'),
        t3p = DroidInstruction.jsonInt(json, 't3p'),
        t3t = DroidInstruction.jsonInt(json, 't3t'),
        y0 = DroidInstruction.jsonBool(json, 'y0'),
        y1 = DroidInstruction.jsonBool(json, 'y1'),
        y2 = DroidInstruction.jsonBool(json, 'y2'),
        y3 = DroidInstruction.jsonBool(json, 'y3'),
        yn0 = DroidInstruction.jsonInt(json, 'yn0'),
        yn1 = DroidInstruction.jsonInt(json, 'yn1'),
        yn2 = DroidInstruction.jsonInt(json, 'yn2'),
        yn3 = DroidInstruction.jsonInt(json, 'yn3'),
        yg0 = DroidInstruction.jsonInt(json, 'yg0'),
        yg1 = DroidInstruction.jsonInt(json, 'yg1'),
        yg2 = DroidInstruction.jsonInt(json, 'yg2'),
        yg3 = DroidInstruction.jsonInt(json, 'yg3'),
        vd = DroidInstruction.jsonBool(json, 'vd'),
        va = DroidInstruction.jsonBool(json, 'va'),
        vx = DroidInstruction.jsonBool(json, 'vx'),
        vb = DroidInstruction.jsonBool(json, 'vb'),
        vc = DroidInstruction.jsonBool(json, 'vc'),
        ve = DroidInstruction.jsonBool(json, 've'),
        vg = DroidInstruction.jsonBool(json, 'vg'),
        vh = DroidInstruction.jsonBool(json, 'vh'),
        vm = DroidInstruction.jsonBool(json, 'vm'),
        vt = DroidInstruction.jsonBool(json, 'vt'),
        vy = DroidInstruction.jsonBool(json, 'vy'),
        vp = DroidInstruction.jsonBool(json, 'vp'),
        vl = DroidInstruction.jsonBool(json, 'vl'),
        vs = DroidInstruction.jsonBool(json, 'vs'),
        ai = DroidInstruction.jsonBool(json, 'ai'),
        av = DroidInstruction.jsonBool(json, 'av'),
        aa = DroidInstruction.jsonDouble(json, 'aa'),
        ad = DroidInstruction.jsonDouble(json, 'ad'),
        ac = DroidInstruction.jsonInt(json, 'ac'),
        lc = DroidInstruction.jsonInt(json, 'lc'),
        lr = DroidInstruction.jsonDouble(json, 'lr'),
        sr = DroidInstruction.jsonInt(json, 'sr'),
        mt = DroidInstruction.jsonDouble(json, 'mt'),
        md = DroidInstruction.jsonDouble(json, 'md'),
        ma = DroidInstruction.jsonDouble(json, 'ma'),
        mo = DroidInstruction.jsonDouble(json, 'mo'),
        mg = DroidInstruction.jsonDouble(json, 'mg'),
        mk = DroidInstruction.jsonDouble(json, 'mk'),
        oop = DroidInstruction.jsonDouble(json, 'oop'),
        ooq = DroidInstruction.jsonDouble(json, 'ooq'),
        oor = DroidInstruction.jsonDouble(json, 'oor'),
        oos = DroidInstruction.jsonDouble(json, 'oos'),
        opv = DroidInstruction.jsonDouble(json, 'opv'),
        opw = DroidInstruction.jsonDouble(json, 'opw'),
        orv = DroidInstruction.jsonDouble(json, 'orv'),
        orw = DroidInstruction.jsonDouble(json, 'orw'),
        oav = DroidInstruction.jsonDouble(json, 'oav'),
        oaw = DroidInstruction.jsonDouble(json, 'oaw'),
        ohv = DroidInstruction.jsonDouble(json, 'ohv'),
        ohw = DroidInstruction.jsonDouble(json, 'ohw'),
        ov = DroidInstruction.jsonDouble(json, 'ov'),
        lmc = DroidInstruction.jsonBool(json, 'lmc'),
        lma = DroidInstruction.jsonDouble(json, 'lma'),
        lmt = DroidInstruction.jsonInt(json, 'lmt'),
        hsm = DroidInstruction.jsonBool(json, 'hsm'),
        dpy = DroidInstruction.jsonBool(json, 'dpy'),
        dba = DroidInstruction.jsonBool(json, 'dba'),
        rm = DroidInstruction.jsonInt(json, 'rm'),
        tm = DroidInstruction.jsonInt(json, 'tm'),
        ias = DroidInstruction.jsonInt(json, 'ias');
}
