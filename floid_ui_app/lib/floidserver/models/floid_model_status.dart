/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';

class FloidModelStatus {

  FloidModelStatus();
  /// Heli 0 collective delta orientation
  double collectiveDeltaOrientationH0 = 0;
  /// Heli 1 collective delta orientation
  double collectiveDeltaOrientationH1 = 0;
  /// Heli 2 collective delta orientation
  double collectiveDeltaOrientationH2 = 0;
  /// Heli 3 collective delta orientation
  double collectiveDeltaOrientationH3 = 0;
  /// Heli 0 collective delta altitude
  double collectiveDeltaAltitudeH0 = 0;
  /// Heli 1 collective delta altitude
  double collectiveDeltaAltitudeH1 = 0;
  /// Heli 2 collective delta altitude
  double collectiveDeltaAltitudeH2 = 0;
  /// Heli 3 collective delta altitude
  double collectiveDeltaAltitudeH3 = 0;
  /// Heli 0 collective delta heading
  double collectiveDeltaHeadingH0 = 0;
  /// Heli 1 collective delta heading
  double collectiveDeltaHeadingH1 = 0;
  /// Heli 2 collective delta heading
  double collectiveDeltaHeadingH2 = 0;
  /// Heli 3 collective delta heading
  double collectiveDeltaHeadingH3 = 0;
  /// Heli 0 collective delta total
  double collectiveDeltaTotalH0 = 0;
  /// Heli 1 collective delta total
  double collectiveDeltaTotalH1 = 0;
  /// Heli 2 collective delta total
  double collectiveDeltaTotalH2 = 0;
  /// Heli 3 collective delta total
  double collectiveDeltaTotalH3 = 0;
  /// Heli 0 Servo 0 cyclic value
  double cyclicValueH0S0 = 0;
  /// Heli 0 Servo 1 cyclic value
  double cyclicValueH0S1 = 0;
  /// Heli 0 Servo 2 cyclic value
  double cyclicValueH0S2 = 0;
  /// Heli 1 Servo 0 cyclic value
  double cyclicValueH1S0 = 0;
  /// Heli 1 Servo 1 cyclic value
  double cyclicValueH1S1 = 0;
  /// Heli 1 Servo 2 cyclic value
  double cyclicValueH1S2 = 0;
  /// Heli 2 Servo 0 cyclic value
  double cyclicValueH2S0 = 0;
  /// Heli 2 Servo 1 cyclic value
  double cyclicValueH2S1 = 0;
  /// Heli 2 Servo 2 cyclic value
  double cyclicValueH2S2 = 0;
  /// Heli 3 Servo 0 cyclic value
  double cyclicValueH3S0 = 0;
  /// Heli 3 Servo 1 cyclic value
  double cyclicValueH3S1 = 0;
  /// Heli 3 Servo 2 cyclic value
  double cyclicValueH3S2 = 0;
  /// Heli 0 collective value
  double collectiveValueH0 = 0;
  /// Heli 1 collective value
  double collectiveValueH1 = 0;
  /// Heli 2 collective value
  double collectiveValueH2 = 0;
  /// Heli 3 collective value
  double collectiveValueH3 = 0;
  /// Heli 0 Servo 0 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH0S0 = 0;
  /// Heli 0 Servo 1 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH0S1 = 0;
  /// Heli 0 Servo 2 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH0S2 = 0;
  /// Heli 1 Servo 0 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH1S0 = 0;
  /// Heli 1 Servo 1 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH1S1 = 0;
  /// Heli 1 Servo 2 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH1S2 = 0;
  /// Heli 2 Servo 0 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH2S0 = 0;
  /// Heli 2 Servo 1 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH2S1 = 0;
  /// Heli 2 Servo 2 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH2S2 = 0;
  /// Heli 3 Servo 0 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH3S0 = 0;
  /// Heli 3 Servo 1 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH3S1 = 0;
  /// Heli 3 Servo 2 calculated cyclic blade pitch
  double calculatedCyclicBladePitchH3S2 = 0;
  /// Heli 0 calculated collective blade pitch
  double calculatedCollectiveBladePitchH0 = 0;
  /// Heli 1 calculated collective blade pitch
  double calculatedCollectiveBladePitchH1 = 0;
  /// Heli 2 calculated collective blade pitch
  double calculatedCollectiveBladePitchH2 = 0;
  /// Heli 3 calculated collective blade pitch
  double calculatedCollectiveBladePitchH3 = 0;
  /// Heli 0 Servo 0 calculated blade pitch
  double calculatedBladePitchH0S0 = 0;
  /// Heli 0 Servo 1 calculated blade pitch
  double calculatedBladePitchH0S1 = 0;
  /// Heli 0 Servo 2 calculated blade pitch
  double calculatedBladePitchH0S2 = 0;
  /// Heli 1 Servo 0 calculated blade pitch
  double calculatedBladePitchH1S0 = 0;
  /// Heli 1 Servo 1 calculated blade pitch
  double calculatedBladePitchH1S1 = 0;
  /// Heli 1 Servo 2 calculated blade pitch
  double calculatedBladePitchH1S2 = 0;
  /// Heli 2 Servo 0 calculated blade pitch
  double calculatedBladePitchH2S0 = 0;
  /// Heli 2 Servo 1 calculated blade pitch
  double calculatedBladePitchH2S1 = 0;
  /// Heli 2 Servo 2 calculated blade pitch
  double calculatedBladePitchH2S2 = 0;
  /// Heli 3 Servo 0 calculated blade pitch
  double calculatedBladePitchH3S0 = 0;
  /// Heli 3 Servo 1 calculated blade pitch
  double calculatedBladePitchH3S1 = 0;
  /// Heli 3 Servo 2 calculated blade pitch
  double calculatedBladePitchH3S2 = 0;
  /// Heli 0 Servo 0 calculated servo degrees
  double calculatedServoDegreesH0S0 = 0;
  /// Heli 0 Servo 1 calculated servo degrees
  double calculatedServoDegreesH0S1 = 0;
  /// Heli 0 Servo 2 calculated servo degrees
  double calculatedServoDegreesH0S2 = 0;
  /// Heli 1 Servo 0 calculated servo degrees
  double calculatedServoDegreesH1S0 = 0;
  /// Heli 1 Servo 1 calculated servo degrees
  double calculatedServoDegreesH1S1 = 0;
  /// Heli 1 Servo 2 calculated servo degrees
  double calculatedServoDegreesH1S2 = 0;
  /// Heli 2 Servo 0 calculated servo degrees
  double calculatedServoDegreesH2S0 = 0;
  /// Heli 2 Servo 1 calculated servo degrees
  double calculatedServoDegreesH2S1 = 0;
  /// Heli 2 Servo 2 calculated servo degrees
  double calculatedServoDegreesH2S2 = 0;
  /// Heli 3 Servo 0 calculated servo degrees
  double calculatedServoDegreesH3S0 = 0;
  /// Heli 3 Servo 1 calculated servo degrees
  double calculatedServoDegreesH3S1 = 0;
  /// Heli 3 Servo 2 calculated servo degrees
  double calculatedServoDegreesH3S2 = 0;
  /// Heli 0 Servo 0 calculated servo pulse
  int calculatedServoPulseH0S0 = 0;
  /// Heli 0 Servo 1 calculated servo pulse
  int calculatedServoPulseH0S1 = 0;
  /// Heli 0 Servo 2 calculated servo pulse
  int calculatedServoPulseH0S2 = 0;
  /// Heli 1 Servo 0 calculated servo pulse
  int calculatedServoPulseH1S0 = 0;
  /// Heli 1 Servo 1 calculated servo pulse
  int calculatedServoPulseH1S1 = 0;
  /// Heli 1 Servo 2 calculated servo pulse
  int calculatedServoPulseH1S2 = 0;
  /// Heli 2 Servo 0 calculated servo pulse
  int calculatedServoPulseH2S0 = 0;
  /// Heli 2 Servo 1 calculated servo pulse
  int calculatedServoPulseH2S1 = 0;
  /// Heli 2 Servo 2 calculated servo pulse
  int calculatedServoPulseH2S2 = 0;
  /// Heli 3 Servo 0 calculated servo pulse
  int calculatedServoPulseH3S0 = 0;
  /// Heli 3 Servo 1 calculated servo pulse
  int calculatedServoPulseH3S1 = 0;
  /// Heli 3 Servo 2 calculated servo pulse
  int calculatedServoPulseH3S2 = 0;
  // Cyclic for Heading in Normal Rotation mode:
  /// Cyclic heading in normal rotation mode
  double cyclicHeading = 0;
  /// Amount of cyclic from velocity goals:
  double velocityCyclicAlpha = 0;

  /// Convert from JSON
  FloidModelStatus.fromJson(Map<String, dynamic> json)
      :  collectiveDeltaOrientationH0 = DroidInstruction.jsonDouble(json,'collectiveDeltaOrientationH0'),
        collectiveDeltaOrientationH1 = DroidInstruction.jsonDouble(json,'collectiveDeltaOrientationH1'),
        collectiveDeltaOrientationH2 = DroidInstruction.jsonDouble(json,'collectiveDeltaOrientationH2'),
        collectiveDeltaOrientationH3 = DroidInstruction.jsonDouble(json,'collectiveDeltaOrientationH3'),
        collectiveDeltaAltitudeH0 = DroidInstruction.jsonDouble(json,'collectiveDeltaAltitudeH0'),
        collectiveDeltaAltitudeH1 = DroidInstruction.jsonDouble(json,'collectiveDeltaAltitudeH1'),
        collectiveDeltaAltitudeH2 = DroidInstruction.jsonDouble(json,'collectiveDeltaAltitudeH2'),
        collectiveDeltaAltitudeH3 = DroidInstruction.jsonDouble(json,'collectiveDeltaAltitudeH3'),
        collectiveDeltaHeadingH0 = DroidInstruction.jsonDouble(json,'collectiveDeltaHeadingH0'),
        collectiveDeltaHeadingH1 = DroidInstruction.jsonDouble(json,'collectiveDeltaHeadingH1'),
        collectiveDeltaHeadingH2 = DroidInstruction.jsonDouble(json,'collectiveDeltaHeadingH2'),
        collectiveDeltaHeadingH3 = DroidInstruction.jsonDouble(json,'collectiveDeltaHeadingH3'),
        collectiveDeltaTotalH0 = DroidInstruction.jsonDouble(json,'collectiveDeltaTotalH0'),
        collectiveDeltaTotalH1 = DroidInstruction.jsonDouble(json,'collectiveDeltaTotalH1'),
        collectiveDeltaTotalH2 = DroidInstruction.jsonDouble(json,'collectiveDeltaTotalH2'),
        collectiveDeltaTotalH3 = DroidInstruction.jsonDouble(json,'collectiveDeltaTotalH3'),
        cyclicValueH0S0 = DroidInstruction.jsonDouble(json,'cyclicValueH0S0'),
        cyclicValueH0S1 = DroidInstruction.jsonDouble(json,'cyclicValueH0S1'),
        cyclicValueH0S2 = DroidInstruction.jsonDouble(json,'cyclicValueH0S2'),
        cyclicValueH1S0 = DroidInstruction.jsonDouble(json,'cyclicValueH1S0'),
        cyclicValueH1S1 = DroidInstruction.jsonDouble(json,'cyclicValueH1S1'),
        cyclicValueH1S2 = DroidInstruction.jsonDouble(json,'cyclicValueH1S2'),
        cyclicValueH2S0 = DroidInstruction.jsonDouble(json,'cyclicValueH2S0'),
        cyclicValueH2S1 = DroidInstruction.jsonDouble(json, 'cyclicValueH2S1'),
        cyclicValueH2S2 = DroidInstruction.jsonDouble(json,'cyclicValueH2S2'),
        cyclicValueH3S0 = DroidInstruction.jsonDouble(json,'cyclicValueH3S0'),
        cyclicValueH3S1 = DroidInstruction.jsonDouble(json,'cyclicValueH3S1'),
        cyclicValueH3S2 = DroidInstruction.jsonDouble(json,'cyclicValueH3S2'),
        collectiveValueH0 = DroidInstruction.jsonDouble(json,'collectiveValueH0'),
        collectiveValueH1 = DroidInstruction.jsonDouble(json,'collectiveValueH1'),
        collectiveValueH2 = DroidInstruction.jsonDouble(json,'collectiveValueH2'),
        collectiveValueH3 = DroidInstruction.jsonDouble(json,'collectiveValueH3'),
        calculatedCyclicBladePitchH0S0 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH0S0'),
        calculatedCyclicBladePitchH0S1 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH0S1'),
        calculatedCyclicBladePitchH0S2 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH0S2'),
        calculatedCyclicBladePitchH1S0 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH1S0'),
        calculatedCyclicBladePitchH1S1 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH1S1'),
        calculatedCyclicBladePitchH1S2 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH1S2'),
        calculatedCyclicBladePitchH2S0 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH2S0'),
        calculatedCyclicBladePitchH2S1 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH2S1'),
        calculatedCyclicBladePitchH2S2 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH2S2'),
        calculatedCyclicBladePitchH3S0 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH3S0'),
        calculatedCyclicBladePitchH3S1 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH3S1'),
        calculatedCyclicBladePitchH3S2 = DroidInstruction.jsonDouble(json,'calculatedCyclicBladePitchH3S2'),
        calculatedCollectiveBladePitchH0 = DroidInstruction.jsonDouble(json,'calculatedCollectiveBladePitchH0'),
        calculatedCollectiveBladePitchH1 = DroidInstruction.jsonDouble(json,'calculatedCollectiveBladePitchH1'),
        calculatedCollectiveBladePitchH2 = DroidInstruction.jsonDouble(json,'calculatedCollectiveBladePitchH2'),
        calculatedCollectiveBladePitchH3 = DroidInstruction.jsonDouble(json,'calculatedCollectiveBladePitchH3'),
        calculatedBladePitchH0S0 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH0S0'),
        calculatedBladePitchH0S1 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH0S1'),
        calculatedBladePitchH0S2 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH0S2'),
        calculatedBladePitchH1S0 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH1S0'),
        calculatedBladePitchH1S1 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH1S1'),
        calculatedBladePitchH1S2 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH1S2'),
        calculatedBladePitchH2S0 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH2S0'),
        calculatedBladePitchH2S1 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH2S1'),
        calculatedBladePitchH2S2 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH2S2'),
        calculatedBladePitchH3S0 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH3S0'),
        calculatedBladePitchH3S1 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH3S1'),
        calculatedBladePitchH3S2 = DroidInstruction.jsonDouble(json,'calculatedBladePitchH3S2'),
        calculatedServoDegreesH0S0 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH0S0'),
        calculatedServoDegreesH0S1 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH0S1'),
        calculatedServoDegreesH0S2 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH0S2'),
        calculatedServoDegreesH1S0 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH1S0'),
        calculatedServoDegreesH1S1 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH1S1'),
        calculatedServoDegreesH1S2 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH1S2'),
        calculatedServoDegreesH2S0 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH2S0'),
        calculatedServoDegreesH2S1 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH2S1'),
        calculatedServoDegreesH2S2 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH2S2'),
        calculatedServoDegreesH3S0 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH3S0'),
        calculatedServoDegreesH3S1 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH3S1'),
        calculatedServoDegreesH3S2 = DroidInstruction.jsonDouble(json,'calculatedServoDegreesH3S2'),

        calculatedServoPulseH0S0 = DroidInstruction.jsonInt(json,'calculatedServoPulseH0S0'),
        calculatedServoPulseH0S1 = DroidInstruction.jsonInt(json,'calculatedServoPulseH0S1'),
        calculatedServoPulseH0S2 = DroidInstruction.jsonInt(json,'calculatedServoPulseH0S2'),
        calculatedServoPulseH1S0 = DroidInstruction.jsonInt(json,'calculatedServoPulseH1S0'),
        calculatedServoPulseH1S1 = DroidInstruction.jsonInt(json,'calculatedServoPulseH1S1'),
        calculatedServoPulseH1S2 = DroidInstruction.jsonInt(json,'calculatedServoPulseH1S2'),
        calculatedServoPulseH2S0 = DroidInstruction.jsonInt(json,'calculatedServoPulseH2S0'),
        calculatedServoPulseH2S1 = DroidInstruction.jsonInt(json,'calculatedServoPulseH2S1'),
        calculatedServoPulseH2S2 = DroidInstruction.jsonInt(json,'calculatedServoPulseH2S2'),
        calculatedServoPulseH3S0 = DroidInstruction.jsonInt(json,'calculatedServoPulseH3S0'),
        calculatedServoPulseH3S1 = DroidInstruction.jsonInt(json,'calculatedServoPulseH3S1'),
        calculatedServoPulseH3S2 = DroidInstruction.jsonInt(json,'calculatedServoPulseH3S2'),
        cyclicHeading = DroidInstruction.jsonDouble(json,'cyclicHeading'),
        velocityCyclicAlpha  = DroidInstruction.jsonDouble(json,'velocityCyclicAlpha');
}
