/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/models/droid_instruction.dart';

class DroidMission extends DroidInstruction {

  DroidMission({required this.missionName, required this.topLevelMission, required this.nextInstructionNumber, required this.droidInstructions, required int id, required String type, required int missionInstructionNumber})
      : super(id: id, type: type, missionInstructionNumber: missionInstructionNumber);

  /// The no mission constant:
  static const int NO_MISSION = -1;

  /// The mission name:
  String missionName = '';

  /// The droid instructions for this mission:
  List<DroidInstruction> droidInstructions;

  /// Is it a top-level mission:
  bool topLevelMission = false;

  /// The next instruction number
  int nextInstructionNumber = 0;

  /// Get the droid instruction from JSON
  DroidMission.fromJson(Map<String, dynamic> json)
      : missionName = json.containsKey('missionName')?json['missionName']:'',
        topLevelMission = json.containsKey('topLevelMission')?json['topLevelMission']:true,
        nextInstructionNumber = json.containsKey('nextInstructionNumber')?json['nextInstructionNumber']:-1,
        droidInstructions = <DroidInstruction>[],
        super.fromJson(json) {
    List<dynamic> list = <dynamic>[];
    list = json['droidInstructions'].map((result) => new DroidInstruction.instructionFactory(result)).toList();
    list.forEach((droidInstruction) => droidInstructions.add(droidInstruction as DroidInstruction));
  }

  @override
  Map<String, dynamic> toJson() {
    List<Map<String, dynamic>> droidInstructionsJson = droidInstructions.map((droidInstruction) {
      return droidInstruction.toJson();
    }).toList();
    Map<String, dynamic> json = super.toJson()
      ..addAll({
        'missionName': missionName,
        'topLevelMission': topLevelMission,
        'nextInstructionNumber': nextInstructionNumber,
        'droidInstructions': droidInstructionsJson,
      });
    return json;
  }

  void renumber() {
    int currentInstructionNumber = 0;
    for(DroidInstruction droidInstruction in droidInstructions) {
      droidInstruction.missionInstructionNumber = currentInstructionNumber++;
    }
  }
}
