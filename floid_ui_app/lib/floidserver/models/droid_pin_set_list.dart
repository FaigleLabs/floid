/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
class DroidPinSetList {

  DroidPinSetList();

  /// The id
  int id = -1;

  /// The pin set id:
  int pinSetId = -1;

  /// The pin set name:
  String pinSetName = "";

  /// Get the droid pin setlist from JSON
  DroidPinSetList.fromJson(Map<String, dynamic> json)
      : id = json['id']==null?-2:json['id'],
        pinSetId = json['pinSetId'],
        pinSetName = json['pinSetName'];
}

