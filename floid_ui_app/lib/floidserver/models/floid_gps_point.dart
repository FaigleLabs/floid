/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
import 'package:floid_ui_app/floidserver/floidserver.dart';

class FloidGpsPoint {

  FloidGpsPoint({required this.id,
    required this.floidId,
    required this.floidUuid,
    required this.statusTime,
    required this.latitude,
    required this.longitude,
    required this.altitude});

  /// The id:
  int id = -1;

  /// The new floid id:
  int floidId = FloidIdAndState.NO_FLOID_ID;

  /// The floid uuid:
   String floidUuid = "";

   /// The status time:
   int statusTime;

   /// The latitude:
   double latitude;

   /// The longitude:
  double longitude;

  /// The altitude:
  double altitude;

  FloidGpsPoint.fromJson(Map<String, dynamic> json)
      : id = json['id']==null?-1:json['id'],
        floidId = json['floidId']==null?FloidIdAndState.NO_FLOID_ID:json['floidId'],
        floidUuid = json['floidUuid']==null?FloidUuidAndState.NO_FLOID_UUID:json['floidUuid'],
        statusTime = DroidInstruction.jsonInt(json, 'statusTime'),
        latitude = DroidInstruction.jsonDouble(json, 'latitude'),
        longitude = DroidInstruction.jsonDouble(json, 'longitude'),
        altitude = DroidInstruction.jsonDouble(json, 'altitude');
}
