/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
class FloidServerStatus {

  FloidServerStatus(String status) : this.status = status;
  static const SERVER_STATUS_RUNNING = 'RUNNING';
  static const SERVER_STATUS_STOPPING = 'STOPPING';
  static const SERVER_STATUS_OFF = 'OFF';
  static const SERVER_STATUS_OFFLINE = 'OFFLINE';

  /// The status
  String status;

  FloidServerStatus.fromJson(Map<String, dynamic> json)
      : status = json['status']==null?'UNKNOWN':json['status'];
}
