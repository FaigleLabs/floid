/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

import 'package:floid_ui_app/floidserver/authentication/authentication.dart';
import 'package:bloc/bloc.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {

  final UserRepository userRepository;

  AuthenticationBloc({required this.userRepository})
      : super(AuthenticationUninitialized()) {

    on<AuthenticationEvent>((AuthenticationEvent event, Emitter<AuthenticationState> emit)
    async {
      if (event is  AppStarted) {
        final bool hasToken = await userRepository.hasToken();
        if (hasToken) {
          final String token = await userRepository.getToken();
          emit(AuthenticationAuthenticated(token: token));
        } else {
          emit(AuthenticationUnauthenticated());
        }
      }

      if (event is LoggedIn) {
        emit(AuthenticationLoading());
        await userRepository.persistToken(event.token);
        emit(AuthenticationAuthenticated(token: event.token));
      }

      if (event is LoggedOut) {
        emit(AuthenticationLoading());
        await userRepository.deleteToken();
        emit(AuthenticationUnauthenticated());
      }
    });
  }
}
