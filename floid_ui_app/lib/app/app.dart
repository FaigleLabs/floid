import 'package:floid_ui_app/floidserver/floidserver.dart';
import 'package:floid_ui_app/common/common.dart';
import 'package:floid_ui_app/ui/page/page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class App extends StatelessWidget {
  final UserRepository userRepository;

  App({Key? key, required this.userRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Floid UAV',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Roboto',
        brightness: Brightness.dark,
        primaryColor: Colors.black,
        splashColor: Colors.yellowAccent,
        highlightColor: Colors.blue.shade700,
        hintColor: Colors.lightBlue[300],
        disabledColor: Colors.blueGrey,
        unselectedWidgetColor: Colors.blueGrey,
        indicatorColor: Colors.purple,
        canvasColor: Colors.black,
        dialogBackgroundColor: Color(0xFF222222),
        /*
        dialogTheme: DialogTheme(
          backgroundColor: Colors.black,
        ),*/
        primaryTextTheme: TextTheme(
          titleMedium: TextStyle(
            color: Colors.yellowAccent,
          ),
          titleSmall: TextStyle(
            color: Colors.yellowAccent,
          ),
          bodyMedium: TextStyle(
            color: Colors.yellowAccent,
          ),
          labelLarge: TextStyle(
            color: Colors.yellowAccent,
          ),
          labelSmall: TextStyle(
            color: Colors.yellowAccent,
          ),
          bodyLarge: TextStyle(
            color: Colors.yellowAccent,
          ),
          displayLarge: TextStyle(
            color: Colors.yellowAccent,
          ),
          displayMedium: TextStyle(
            color: Colors.yellowAccent,
          ),
          displaySmall: TextStyle(
            color: Colors.yellowAccent,
          ),
          headlineMedium: TextStyle(
            color: Colors.yellowAccent,
          ),
          headlineSmall: TextStyle(
            color: Colors.yellowAccent,
          ),
          titleLarge: TextStyle(
            color: Colors.black,
          ),
        ),
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: Colors.lightBlue[600],
          selectionColor: Colors.lightBlue[600],
          selectionHandleColor: Colors.lightBlue[600],
        ),
        popupMenuTheme: PopupMenuThemeData(color: Colors.black),
        bottomSheetTheme: BottomSheetThemeData(
          backgroundColor: Colors.black,
          modalBarrierColor: Color(0x2FFFFFFF),
        ),
        iconTheme: IconThemeData(color: Colors.yellowAccent,),
        textTheme: TextTheme(
          titleMedium: TextStyle(
            color: Colors.yellowAccent,
          ),
          titleSmall: TextStyle(
            color: Colors.yellowAccent,
          ),
          bodyMedium: TextStyle(
            color: Colors.yellowAccent,
          ),
          labelLarge: TextStyle(
            color: Colors.yellowAccent,
          ),
          labelSmall: TextStyle(
            color: Colors.yellowAccent,
          ),
          bodyLarge: TextStyle(
            color: Colors.yellowAccent,
          ),
          displayLarge: TextStyle(
            color: Colors.yellowAccent,
          ),
          displayMedium: TextStyle(
            color: Colors.yellowAccent,
          ),
          displaySmall: TextStyle(
            color: Colors.yellowAccent,
          ),
          headlineMedium: TextStyle(
            color: Colors.yellowAccent,
          ),
          headlineSmall: TextStyle(
            color: Colors.yellowAccent,
          ),
          titleLarge: TextStyle(
            color: Colors.black,
          ),
        ), checkboxTheme: CheckboxThemeData(
 fillColor: MaterialStateProperty.resolveWith<Color?>((Set<MaterialState> states) {
 if (states.contains(MaterialState.disabled)) { return null; }
 if (states.contains(MaterialState.selected)) { return Colors.lightBlue[300]; }
 return null;
 }),
 ), radioTheme: RadioThemeData(
 fillColor: MaterialStateProperty.resolveWith<Color?>((Set<MaterialState> states) {
 if (states.contains(MaterialState.disabled)) { return null; }
 if (states.contains(MaterialState.selected)) { return Colors.lightBlue[300]; }
 return null;
 }),
 ), switchTheme: SwitchThemeData(
 thumbColor: MaterialStateProperty.resolveWith<Color?>((Set<MaterialState> states) {
 if (states.contains(MaterialState.disabled)) { return null; }
 if (states.contains(MaterialState.selected)) { return Colors.lightBlue[300]; }
 return null;
 }),
 trackColor: MaterialStateProperty.resolveWith<Color?>((Set<MaterialState> states) {
 if (states.contains(MaterialState.disabled)) { return null; }
 if (states.contains(MaterialState.selected)) { return Colors.lightBlue[300]; }
 return null;
 }),
 ),
      ),
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, authenticationState) {
          if (authenticationState is AuthenticationAuthenticated) {
            return HomePage();
          }
          if (authenticationState is AuthenticationUnauthenticated) {
            return BlocBuilder<FloidServerRepositoryBloc, FloidServerRepositoryState>(
                builder: (context, floidServerRepositoryState) {
                  return floidServerRepositoryState is FloidServerRepositoryStateHasHost ?
                  BlocProvider(
                      create: (context) {
                        return LoginBloc(
                          authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
                        );
                      },
                      child: LoginPage(),
                  ) : LoadingIndicator();
                }
            );
          }
          if (authenticationState is AuthenticationLoading) {
            return LoadingIndicator();
          }
          return SplashPage();
        },
      ),
    );
  }
}
