import 'package:floid_ui_app/app/floid_repository.dart';
import 'package:floid_ui_app/common/common.dart';
import 'package:floid_ui_app/ui/settings/settings_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';

class FloidSettings extends StatelessWidget {
  FloidSettings({
    required Key? key,
    required this.userRepository,
  }) : super(key: key);

  final UserRepository userRepository;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsBloc, SettingsState>(
      builder: (context, settingsState) {
        // Create our repository and authentication bloc:
        return settingsState is SettingsLoaded ? FloidRepositoryWidget(userRepository: userRepository, host: settingsState.host, port: settingsState.port, secure: settingsState.secure): LoadingIndicator();
      },
    );
  }
}
