import 'package:floid_ui_app/floidserver/authentication/authentication.dart';
import 'package:floid_ui_app/floidserver/bloc/floid_server_repository_bloc.dart';
import 'package:floid_ui_app/app/app.dart';
import 'package:floid_ui_app/floidserver/repositories/repositories.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FloidRepositoryWidget extends StatefulWidget {
  final UserRepository userRepository;
  final String host;
  final int port;
  final bool secure;
  FloidRepositoryWidget({
    Key? key,
    required this.userRepository,
    required this.host,
    required this.port,
    required this.secure,
  }) : super(key: key);
  @override
  State<FloidRepositoryWidget> createState() => _FloidRepositoryWidget();
}

class _FloidRepositoryWidget extends State<FloidRepositoryWidget> {
  late FloidServerRepositoryBloc _floidServerRepositoryBloc;

  @override
  void initState() {
    super.initState();
    _floidServerRepositoryBloc = FloidServerRepositoryBloc(host: widget.host, port: widget.port, secure: widget.secure);
  }

  @override
  void didUpdateWidget(FloidRepositoryWidget oldWidget) {
    print('   old host:' + oldWidget.host + ' new host:' + this.widget.host);
    if(oldWidget.host!=this.widget.host) {
      print('New Host - dispatching new host event');
      _floidServerRepositoryBloc.add(new FloidServerRepositoryNewHostEvent(host: widget.host, port: widget.port, secure: widget.secure));
    } else {
      print('Same Host');
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FloidServerRepositoryBloc>(
      create: (context) {
        return this._floidServerRepositoryBloc;
      },
      child: BlocProvider<AuthenticationBloc>(
        create: (context) {
          return AuthenticationBloc(userRepository: widget.userRepository)
            ..add(AppStarted());
        },
        child: App(userRepository: widget.userRepository),
      ),
    );
  }
}
