//Copyright (C) 2013 Potix Corporation. All Rights Reserved.
//History: Thu, Aug 15, 2013  9:48:37 AM
// Author: tomyeh
library stomp_impl_plugin_vm;

import "dart:async";
import "dart:io";

import 'package:stomp/impl/plugin.dart';

/// The implementation on top of [Socket].
class SocketStompConnector extends BytesStompConnector {
  final Socket _socket;

  SocketStompConnector(this._socket) {
    _init();
  }
  void _init() {
    _socket.listen((List<int>? data) {
      if (data != null && data.isNotEmpty) {
        final BytesCallback? fOnBytes = onBytes;
        if(fOnBytes!=null) fOnBytes(data);
      }
    }, onError: (error, stackTrace) {
      final ErrorCallback? fOnError  = onError;
      if(fOnError!=null)fOnError(error, stackTrace);
    }, onDone: () {
      final CloseCallback? fOnClose  = onClose;
      if(fOnClose!=null) fOnClose();
    });
  }

  @override
  Future close() {
    _socket.destroy();
    return new Future.value();
  }

  @override
  void writeBytes_(List<int> bytes) {
    _socket.add(bytes);
  }
  @override
  Future writeStream_(Stream<List<int>> stream)
  => _socket.addStream(stream);
}
