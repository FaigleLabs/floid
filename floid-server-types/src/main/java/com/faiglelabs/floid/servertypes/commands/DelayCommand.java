/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Delay command (delay is on ground, hover is in-air equivalent)
 */
@Entity
public class DelayCommand extends DroidCommand {
    private long delayTime = 0L;

    /**
     * Instantiates a new Delay command.
     */
    public DelayCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        DelayCommand delayCommand = new DelayCommand();
        super.factory(delayCommand, droidMission);
        delayCommand.delayTime = delayTime;
        return delayCommand;
    }

    /**
     * Gets delay time.
     *
     * @return the delay time
     */
    public long getDelayTime() {
        return delayTime;
    }

    /**
     * Sets delay time.
     *
     * @param delayTime the delay time
     */
    public void setDelayTime(long delayTime) {
        this.delayTime = delayTime;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("delayTime", delayTime);
        return jsonObject;
    }

    @Override
    public DelayCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        delayTime = jsonObject.getInt("delayTime");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("delayTime", "" + delayTime);
        return typeElement;
    }
}
