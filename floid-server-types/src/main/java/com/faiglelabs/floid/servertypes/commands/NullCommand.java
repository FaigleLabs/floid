/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;


/**
 * Null command.  Sends a null command to the floid which literally does nothing.
 */
@Entity
public class NullCommand extends DroidCommand {
    /**
     * Instantiates a new Null command.
     */
    public NullCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        NullCommand nullCommand = new NullCommand();
        super.factory(nullCommand, droidMission);
        return nullCommand;
    }

    // This command is sent to the droid when it first connects with no current command set
    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable") JSONObject jsonObject = super.toJSON();
        // No parameters to add
        return jsonObject;
    }

    @Override
    public NullCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable") Element typeElement = super.addToXML(doc);
        // Adds nothing...
        return typeElement;
    }
}
