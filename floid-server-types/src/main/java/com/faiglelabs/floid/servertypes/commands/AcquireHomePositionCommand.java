/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Acquire home position command given the requirements
 */
@Entity
public class AcquireHomePositionCommand extends DroidCommand {
    private double requiredXYDistance = 25.0;
    private double requiredAltitudeDistance = 10.0;
    private double requiredHeadingDistance = 3.0;
    private int requiredGPSGoodCount = 20;
    private int requiredAltimeterGoodCount = 10;

    /**
     * Create a home position command
     */
    public AcquireHomePositionCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        AcquireHomePositionCommand acquireHomePositionCommand = new AcquireHomePositionCommand();
        super.factory(acquireHomePositionCommand, droidMission);
        acquireHomePositionCommand.requiredXYDistance = requiredXYDistance;
        acquireHomePositionCommand.requiredAltitudeDistance = requiredAltitudeDistance;
        acquireHomePositionCommand.requiredHeadingDistance = requiredHeadingDistance;
        acquireHomePositionCommand.requiredGPSGoodCount = requiredGPSGoodCount;
        acquireHomePositionCommand.requiredAltimeterGoodCount = requiredAltimeterGoodCount;
        return acquireHomePositionCommand;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("requiredXYDistance", requiredXYDistance);
        jsonObject.put("requiredAltitudeDistance", requiredAltitudeDistance);
        jsonObject.put("requiredHeadingDistance", requiredHeadingDistance);
        jsonObject.put("requiredGPSGoodCount", requiredGPSGoodCount);
        jsonObject.put("requiredAltimeterGoodCount", requiredAltimeterGoodCount);
        return jsonObject;
    }

    @Override
    public AcquireHomePositionCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        requiredXYDistance = jsonObject.getDouble("requiredXYDistance");
        requiredAltitudeDistance = jsonObject.getDouble("requiredAltitudeDistance");
        requiredHeadingDistance = jsonObject.getDouble("requiredHeadingDistance");
        requiredGPSGoodCount = jsonObject.getInt("requiredGPSGoodCount");
        requiredAltimeterGoodCount = jsonObject.getInt("requiredAltimeterGoodCount");
        return this;
    }

    /**
     * Gets required altimeter good count.
     *
     * @return the required altimeter good count
     */
    public int getRequiredAltimeterGoodCount() {
        return requiredAltimeterGoodCount;
    }

    /**
     * Sets required altimeter good count.
     *
     * @param requiredAltimeterGoodCount the required altimeter good count
     */
    public void setRequiredAltimeterGoodCount(int requiredAltimeterGoodCount) {
        this.requiredAltimeterGoodCount = requiredAltimeterGoodCount;
    }

    /**
     * Gets required altitude distance.
     *
     * @return the required altitude distance
     */
    public double getRequiredAltitudeDistance() {
        return requiredAltitudeDistance;
    }

    /**
     * Sets required altitude distance.
     *
     * @param requiredAltitudeDistance the required altitude distance
     */
    public void setRequiredAltitudeDistance(double requiredAltitudeDistance) {
        this.requiredAltitudeDistance = requiredAltitudeDistance;
    }

    /**
     * Gets required gps good count.
     *
     * @return the required gps good count
     */
    public int getRequiredGPSGoodCount() {
        return requiredGPSGoodCount;
    }

    /**
     * Sets required gps good count.
     *
     * @param requiredGPSGoodCount the required gps good count
     */
    public void setRequiredGPSGoodCount(int requiredGPSGoodCount) {
        this.requiredGPSGoodCount = requiredGPSGoodCount;
    }

    /**
     * Gets required heading distance.
     *
     * @return the required heading distance
     */
    public double getRequiredHeadingDistance() {
        return requiredHeadingDistance;
    }

    /**
     * Sets required heading distance.
     *
     * @param requiredHeadingDistance the required heading distance
     */
    public void setRequiredHeadingDistance(double requiredHeadingDistance) {
        this.requiredHeadingDistance = requiredHeadingDistance;
    }

    /**
     * Gets required xy distance.
     *
     * @return the required xy distance
     */
    public double getRequiredXYDistance() {
        return requiredXYDistance;
    }

    /**
     * Sets required xy distance.
     *
     * @param requiredXYDistance the required xy distance
     */
    public void setRequiredXYDistance(double requiredXYDistance) {
        this.requiredXYDistance = requiredXYDistance;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("requiredXYDistance", "" + requiredXYDistance);
        typeElement.setAttribute("requiredAltitudeDistance", "" + requiredAltitudeDistance);
        typeElement.setAttribute("requiredHeadingDistance", "" + requiredHeadingDistance);
        typeElement.setAttribute("requiredGPSGoodCount", "" + requiredGPSGoodCount);
        typeElement.setAttribute("requiredAltimeterGoodCount", "" + requiredAltimeterGoodCount);
        return typeElement;
    }
}
