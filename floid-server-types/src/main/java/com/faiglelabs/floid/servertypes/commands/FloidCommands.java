/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.servertypes.commands;

/**
 * The type Floid commands.
 */
public class FloidCommands {
    /**
     * The constant COMMAND_PAYLOAD.
     */
    public static final String COMMAND_PAYLOAD = "Payload";
    /**
     * The constant COMMAND_TAKE_PHOTO.
     */
    public static final String COMMAND_TAKE_PHOTO = "TakePhoto";
    /**
     * The constant COMMAND_START_VIDEO.
     */
    public static final String COMMAND_START_VIDEO = "StartVideo";
    /**
     * The constant COMMAND_STOP_VIDEO.
     */
    public static final String COMMAND_STOP_VIDEO = "StopVideo";
    /**
     * The constant COMMAND_SHUT_DOWN_HELIS.
     */
    public static final String COMMAND_SHUT_DOWN_HELIS = "ShutDownHelis";
    /**
     * The constant COMMAND_HEIGHT_TO.
     */
    public static final String COMMAND_HEIGHT_TO = "HeightTo";
    /**
     * The constant COMMAND_FLY_TO.
     */
    public static final String COMMAND_FLY_TO = "FlyTo";
    /**
     * The constant COMMAND_DEPLOY_PARACHUTE.
     */
    public static final String COMMAND_DEPLOY_PARACHUTE = "DeployParachute";
    /**
     * The constant COMMAND_START_UP_HELIS.
     */
    public static final String COMMAND_START_UP_HELIS = "StartUpHelis";
    /**
     * The constant COMMAND_ACQUIRE_HOME_POSITION.
     */
    public static final String COMMAND_ACQUIRE_HOME_POSITION = "AcquireHomePosition";
    /**
     * The constant COMMAND_DELAY.
     */
    public static final String COMMAND_DELAY = "Delay";
    /**
     * The constant COMMAND_HOVER.
     */
    public static final String COMMAND_HOVER = "Hover";
    /**
     * The constant COMMAND_LAND.
     */
    public static final String COMMAND_LAND = "Land";
    /**
     * The constant COMMAND_SET_PARAMETERS.
     */
    public static final String COMMAND_SET_PARAMETERS = "SetParameters";
    /**
     * The constant COMMAND_DESIGNATE.
     */
    public static final String COMMAND_DESIGNATE = "Designate";
    /**
     * The constant COMMAND_STOP_DESIGNATING.
     */
    public static final String COMMAND_STOP_DESIGNATING = "StopDesignating";
    /**
     * The constant COMMAND_SPEAKER.
     */
    public static final String COMMAND_SPEAKER = "Speaker";
    /**
     * The constant COMMAND_SPEAKER_ON.
     */
    public static final String COMMAND_SPEAKER_ON = "SpeakerOn";
    /**
     * The constant COMMAND_SPEAKER_OFF.
     */
    public static final String COMMAND_SPEAKER_OFF = "SpeakerOff";
    /**
     * The constant COMMAND_FLY_PATTERN.
     */
    public static final String COMMAND_FLY_PATTERN = "FlyPattern";
    /**
     * The constant COMMAND_PAN_TILT.
     */
    public static final String COMMAND_PAN_TILT = "PanTilt";
    /**
     * The constant COMMAND_TURN_TO.
     */
    public static final String COMMAND_TURN_TO = "TurnTo";
    /**
     * The constant COMMAND_LIFT_OFF.
     */
    public static final String COMMAND_LIFT_OFF = "LiftOff";
    /**
     * The constant COMMAND_MISSION.
     */
    public static final String COMMAND_MISSION = "Mission";
    /**
     * The constant COMMAND_PHOTO_STROBE.
     */
    public static final String COMMAND_PHOTO_STROBE = "PhotoStrobe";
    /**
     * The constant COMMAND_STOP_PHOTO_STROBE.
     */
    public static final String COMMAND_STOP_PHOTO_STROBE = "StopPhotoStrobe";
    /**
     * The constant PARAMETER_PIN.
     */
    public static final String PARAMETER_PIN = "pin";
    /**
     * The constant PARAMETER_PIN_NAME.
     */
    public static final String PARAMETER_PIN_NAME = "pinName";
    /**
     * The constant PARAMETER_HOME_PIN_NAME.
     */
    public static final String PARAMETER_HOME_PIN_NAME = "homePinName";
    /**
     * The constant PARAMETER_FLY_TO_HOME.
     */
    public static final String PARAMETER_FLY_TO_HOME = "flyToHome";
    /**
     * The constant PARAMETER_FOLLOW_MODE.
     */
    public static final String PARAMETER_FOLLOW_MODE = "followMode";
    /**
     * The constant PARAMETER_ID.
     */
    public static final String PARAMETER_ID = "id";
    /**
     * The constant PARAMETER_RESET_DROID_PARAMETERS.
     */
    public static final String PARAMETER_RESET_DROID_PARAMETERS = "resetDroidParameters";
    /**
     * The constant PARAMETER_FIRST_PIN_IS_HOME.
     */
    public static final String PARAMETER_FIRST_PIN_IS_HOME = "firstPinIsHome";
    /**
     * The constant PARAMETER_DROID_PARAMETERS.
     */
    public static final String PARAMETER_DROID_PARAMETERS = "droidParameters";
    /**
     * The constant PARAMETER_MISSION_NAME.
     */
    public static final String PARAMETER_MISSION_NAME = "missionName";
    /**
     * The constant PARAMETER_TOP_LEVEL_MISSION.
     */
    public static final String PARAMETER_TOP_LEVEL_MISSION = "topLevelMission";
    /**
     * The constant PARAMETER_BAY.
     */
    public static final String PARAMETER_BAY = "bay";
    /**
     * The constant PARAMETER_ABSOLUTE.
     */
    public static final String PARAMETER_ABSOLUTE = "absolute";
    /**
     * The constant PARAMETER_DELAY_TIME.
     */
    public static final String PARAMETER_DELAY_TIME = "delayTime";
    /**
     * The constant PARAMETER_HOVER_TIME.
     */
    public static final String PARAMETER_HOVER_TIME = "hoverTime";
    /**
     * The constant PARAMETER_PAN.
     */
    public static final String PARAMETER_PAN = "pan";
    /**
     * The constant PARAMETER_TILT.
     */
    public static final String PARAMETER_TILT = "tilt";
    /**
     * The constant PARAMETER_ANGLE.
     */
    public static final String PARAMETER_ANGLE = "angle";
    /**
     * The constant PARAMETER_DEVICE.
     */
    public static final String PARAMETER_DEVICE = "device";
    /**
     * The constant PARAMETER_DELAY.
     */
    public static final String PARAMETER_DELAY = "delay";
    /**
     * The constant PARAMETER_MODE.
     */
    public static final String PARAMETER_MODE = "mode";
    /**
     * The constant PARAMETER_TRUE.
     */
    public static final String PARAMETER_TRUE = "true";
    /**
     * The constant PARAMETER_FALSE.
     */
    public static final String PARAMETER_FALSE = "false";
    /**
     * The constant PARAMETER_ALERT.
     */
    public static final String PARAMETER_ALERT = "alert";
    /**
     * The constant PARAMETER_TTS.
     */
    public static final String PARAMETER_TTS = "tts";
    /**
     * The constant PARAMETER_SPEAKER_REPEAT.
     */
    public static final String PARAMETER_SPEAKER_REPEAT = "speakerRepeat";
    /**
     * The constant PARAMETER_USE_PIN.
     */
    public static final String PARAMETER_USE_PIN = "usePin";
    /**
     * The constant PARAMETER_USE_PIN_HEIGHT.
     */
    public static final String PARAMETER_USE_PIN_HEIGHT = "usePinHeight";
    /**
     * The constant PARAMETER_VOLUME.
     */
    public static final String PARAMETER_VOLUME = "volume";
    /**
     * The constant PARAMETER_RATE.
     */
    public static final String PARAMETER_RATE = "rate";
    /**
     * The constant PARAMETER_PITCH.
     */
    public static final String PARAMETER_PITCH = "pitch";
    /**
     * The constant PARAMETER_FILE_NAME.
     */
    public static final String PARAMETER_FILE_NAME = "fileName";
    /**
     * The constant PARAMETER_X.
     */
    public static final String PARAMETER_X = "x";
    /**
     * The constant PARAMETER_Y.
     */
    public static final String PARAMETER_Y = "y";
    /**
     * The constant PARAMETER_Z.
     */
    public static final String PARAMETER_Z = "z";
    /**
     * The constant PARAMETER_PAN_TILT.
     */
    public static final String PARAMETER_PAN_TILT = "panTilt";
    /**
     * The constant PARAMETER_HEADING.
     */
    public static final String PARAMETER_HEADING = "heading";
    /**
     * The constant PARAMETER_COMMAND.
     */
    public static final String PARAMETER_COMMAND = "command";
    /**
     * The constant PARAMETER_LABEL.
     */
    public static final String PARAMETER_LABEL = "label";
    /**
     * The constant PARAMETER_ICON.
     */
    public static final String PARAMETER_ICON = "icon";
    /**
     * The constant PARAMETER_MESSAGE.
     */
    public static final String PARAMETER_MESSAGE = "message";
    /**
     * The constant PARAMETER_FLOID_ID.
     */
    public static final String PARAMETER_FLOID_ID = "floidId";
    /**
     * The constant PARAMETER_USER_NAME.
     */
    public static final String PARAMETER_USER_NAME = "userName";
    /**
     * The constant PARAMETER_FLOID_UUID.
     */
    public static final String PARAMETER_FLOID_UUID = "floidUuid";
    /**
     * The constant PARAMETER_TYPE.
     */
    public static final String PARAMETER_TYPE = "type";

    // Floid Message Strings:
    // ======================
    /**
     * The constant FLOID_MESSAGE_PACKET_MISSION_QUEUED.
     */
    public  static final String FLOID_MESSAGE_PACKET_MISSION_QUEUED = "MISSION_QUEUED";
    /**
     * The constant FLOID_MESSAGE_PACKET_MISSION_CANCELLED.
     */
    public  static final String FLOID_MESSAGE_PACKET_MISSION_CANCELLED = "MISSION_CANCELLED";
    /**
     * The constant FLOID_MESSAGE_PACKET_MISSION_STARTING.
     */
    public  static final String FLOID_MESSAGE_PACKET_MISSION_STARTING = "MISSION_STARTING";
    /**
     * The constant FLOID_MESSAGE_PACKET_MISSION_COMPLETE.
     */
    public  static final String FLOID_MESSAGE_PACKET_MISSION_COMPLETE = "MISSION_COMPLETE";
    /**
     * The constant FLOID_MESSAGE_PACKET_MISSION_COMPLETE_ERROR.
     */
    public  static final String FLOID_MESSAGE_PACKET_MISSION_COMPLETE_ERROR = "MISSION_COMPLETE_ERROR";
    /**
     * The constant FLOID_MESSAGE_PACKET_INSTRUCTION_STARTING.
     */
    public  static final String FLOID_MESSAGE_PACKET_INSTRUCTION_STARTING = "INSTRUCTION_STARTING";
    /**
     * The constant FLOID_MESSAGE_PACKET_INSTRUCTION_STATUS_CHANGE.
     */
    public  static final String FLOID_MESSAGE_PACKET_INSTRUCTION_STATUS_CHANGE = "INSTRUCTION_STATUS_CHANGE";
    /**
     * The constant FLOID_MESSAGE_PACKET_INSTRUCTION_COMPLETE.
     */
    public  static final String FLOID_MESSAGE_PACKET_INSTRUCTION_COMPLETE = "INSTRUCTION_COMPLETE";
    /**
     * The constant FLOID_MESSAGE_PACKET_COMMAND_RESPONSE.
     */
    public  static final String FLOID_MESSAGE_PACKET_COMMAND_RESPONSE = "COMMAND_RESPONSE";
    /**
     * The constant FLOID_MESSAGE_PACKET_PHOTO.
     */
    public  static final String FLOID_MESSAGE_PACKET_PHOTO = "PHOTO";
    /**
     * The constant FLOID_MESSAGE_PACKET_VIDEO_FRAME.
     */
    public  static final String FLOID_MESSAGE_PACKET_VIDEO_FRAME = "VIDEO_FRAME";
    /**
     * The constant FLOID_MESSAGE_PACKET_DEBUG.
     */
    public  static final String FLOID_MESSAGE_PACKET_DEBUG = "DEBUG";
    /**
     * The constant FLOID_MESSAGE_PACKET_SYSTEM_LOG.
     */
    public  static final String FLOID_MESSAGE_PACKET_SYSTEM_LOG = "SYSTEM_LOG";
    /**
     * The constant FLOID_MESSAGE_FLOID_STATUS.
     */
    public  static final String FLOID_MESSAGE_PACKET_FLOID_STATUS = "FLOID_STATUS";
    /**
     * The constant FLOID_MESSAGE_PACKET_FLOID_DROID_STATUS.
     */
    public  static final String FLOID_MESSAGE_PACKET_FLOID_DROID_STATUS = "FLOID_DROID_STATUS";
    /**
     * The constant FLOID_MESSAGE_PACKET_INSTRUCTION_STATUS.
     */
    public  static final String FLOID_MESSAGE_PACKET_INSTRUCTION_STATUS = "INSTRUCTION_STATUS";
    /**
     * The constant FLOID_MESSAGE_PACKET_MODEL_STATUS.
     */
    public  static final String FLOID_MESSAGE_PACKET_MODEL_STATUS = "MODEL_STATUS";
    /**
     * The constant FLOID_MESSAGE_MODEL_PARAMETERS.
     */
    public  static final String FLOID_MESSAGE_PACKET_MODEL_PARAMETERS = "MODEL_PARAMETERS";
    /**
     * The constant FLOID_MESSAGE_PACKET_TEST_PACKET_RESPONSE.
     */
    public  static final String FLOID_MESSAGE_PACKET_TEST_PACKET_RESPONSE = "TEST_PACKET_RESPONSE";
    /**
     * The constant PARAMETER_REQUIRED_XY_DISTANCE.
     */
    public static final String PARAMETER_REQUIRED_XY_DISTANCE = "requiredXYDistance";
    /**
     * The constant PARAMETER_REQUIRED_ALTITUDE_DISTANCE.
     */
    public static final String PARAMETER_REQUIRED_ALTITUDE_DISTANCE = "requiredAltitudeDistance";
    /**
     * The constant PARAMETER_REQUIRED_HEADING_DISTANCE.
     */
    public static final String PARAMETER_REQUIRED_HEADING_DISTANCE = "requiredHeadingDistance";
    /**
     * The constant PARAMETER_REQUIRED_GPS_GOOD_COUNT.
     */
    public static final String PARAMETER_REQUIRED_GPS_GOOD_COUNT = "requiredGPSGoodCount";
    /**
     * The constant PARAMETER_REQUIRED_ALTIMETER_GOOD_COUNT.
     */
    public static final String PARAMETER_REQUIRED_ALTIMETER_GOOD_COUNT = "requiredAltimeterGoodCount";
    /**
     * The constant PARAMETER_STATUS_NUMBER.
     */
    public static final String PARAMETER_STATUS_NUMBER = "statusNumber";
    /**
     * The constant PARAMETER_FLOID_STATUS.
     */
    public static final String PARAMETER_FLOID_STATUS = "floidStatus";
    /**
     * The constant TABLE_FLOID_STATUS.
     */
    public static final String TABLE_FLOID_STATUS = "floidStatus";
    /**
     * The constant FLOID_DROID_STATUS_TABLE_NAME.
     */
    public static final  String FLOID_DROID_STATUS_TABLE_NAME = "floidDroidStatus";
    /**
     * The constant FLOID_DROID_STATUS_TABLE_KEY_NAME.
     */
    public static final String FLOID_DROID_STATUS_TABLE_KEY_NAME = "floidDroidStatus";
    /**
     * The constant PARAMETER_FLOID_MODEL_PARAMETERS.
     */
    public static final String PARAMETER_FLOID_MODEL_PARAMETERS = "floidModelParameters";
    /**
     * The constant TABLE_FLOID_MODEL_PARAMETERS.
     */
    public static final String TABLE_FLOID_MODEL_PARAMETERS = "floidModelParameters";
    /**
     * The constant PARAMETER_FLOID_MODEL_PARAMETERS.
     */
    public static final String PARAMETER_FLOID_MODEL_STATUS = "floidModelStatus";
    /**
     * The constant TABLE_FLOID_MODEL_PARAMETERS.
     */
    public static final String TABLE_FLOID_MODEL_STATUS = "floidModelStatus";
}
