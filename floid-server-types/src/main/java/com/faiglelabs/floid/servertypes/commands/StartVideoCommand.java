/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;


/**
 * Start video command.  Starts recording video - must be followed by a stop video command.
 */
@Entity
public class StartVideoCommand extends DroidCommand {
    /**
     * Instantiates a new Start video command.
     */
    public StartVideoCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        StartVideoCommand startVideoCommand = new StartVideoCommand();
        super.factory(startVideoCommand, droidMission);
        return startVideoCommand;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable") JSONObject jsonObject = super.toJSON();
        // No parameters to add...
        return jsonObject;
    }

    @Override
    public StartVideoCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable") Element typeElement = super.addToXML(doc);
        // Adds nothing...
        return typeElement;
    }
}
