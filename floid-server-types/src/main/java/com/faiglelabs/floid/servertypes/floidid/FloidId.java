/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.floidid;

import com.faiglelabs.floid.database.FloidDatabaseTables;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The floid id.
 */
@Entity
@Table(name = FloidDatabaseTables.FLOID_ID,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidIdCreateTimeIndex", columnList = "floidIdCreateTime"),
        @Index(name="floidIdUpdateTimeIndex", columnList = "floidIdUpdateTime")
})
@SuppressWarnings("unused")
public class FloidId implements Serializable {
    /**
     * Token for no valid id
     */
    @SuppressWarnings("WeakerAccess")
    public final static Long NO_FLOID_ID = -1L;
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * The Floid id.
     */
    protected int floidId;
    /**
     * The Floid id create time.
     */
    protected long floidIdCreateTime;
    /**
     * The Floid id update time.
     */
    protected long floidIdUpdateTime;

    /**
     * Instantiates a new Floid id.
     *
     * @param floidId           the floid id
     * @param floidIdCreateTime the floid id create time
     * @param floidIdUpdateTime the floid id update time
     */
    public FloidId(int floidId, long floidIdCreateTime, long floidIdUpdateTime) {
        this.floidId = floidId;
        this.floidIdCreateTime = floidIdCreateTime;
        this.floidIdUpdateTime = floidIdUpdateTime;
    }

    /**
     * Instantiates a new Floid id.
     *
     * @param id                the id
     * @param floidId           the floid id
     * @param floidIdCreateTime the floid id create time
     * @param floidIdUpdateTime the floid id update time
     */
    public FloidId(Long id, int floidId, long floidIdCreateTime, long floidIdUpdateTime) {
        this.id = id;
        this.floidId = floidId;
        this.floidIdCreateTime = floidIdCreateTime;
        this.floidIdUpdateTime = floidIdUpdateTime;
    }

    /**
     * Instantiates a new Floid id.
     */
    public FloidId() {
        super();
    }

    /**
     * Gets floid id.
     *
     * @return the floid id
     */
    public int getFloidId() {
        return floidId;
    }

    /**
     * Sets floid id.
     *
     * @param floidId the floid id
     */
    public void setFloidId(int floidId) {
        this.floidId = floidId;
    }

    /**
     * Gets floid id create time.
     *
     * @return the floid id create time
     */
    public long getFloidIdCreateTime() {
        return floidIdCreateTime;
    }

    /**
     * Sets floid id create time.
     *
     * @param floidIdCreateTime the floid id create time
     */
    public void setFloidIdCreateTime(long floidIdCreateTime) {
        this.floidIdCreateTime = floidIdCreateTime;
    }

    /**
     * Gets floid id update time.
     *
     * @return the floid id update time
     */
    public long getFloidIdUpdateTime() {
        return floidIdUpdateTime;
    }

    /**
     * Sets floid id update time.
     *
     * @param floidIdUpdateTime the floid id update time
     */
    public void setFloidIdUpdateTime(long floidIdUpdateTime) {
        this.floidIdUpdateTime = floidIdUpdateTime;
    }
}
