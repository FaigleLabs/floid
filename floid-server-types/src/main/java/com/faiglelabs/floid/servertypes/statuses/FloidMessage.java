/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.statuses;

import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * A Floid message.
 */
@SuppressWarnings("WeakerAccess")
@MappedSuperclass
@DiscriminatorColumn(length=255)
public class FloidMessage implements Serializable {
    /**
     * The next floid message number
     */
    private static long nextFloidMessageNumber = 0;
    @Transient
    private static final String PARAMETER_FLOID_MESSAGE_NUMBER = "floidMessageNumber";
    @Transient
    private static final String PARAMETER_TIMESTAMP = "timestamp";
    /**
     * The Id.
     */
    @Id
    @GeneratedValue
    protected Long id;
    /**
     * The Floid id.
     */
    protected int floidId = 0;
    /**
     * The Floid uuid.
     */
    protected String floidUuid = "";
    /**
     * The Type.
     */
    protected String type = "";
    /**
     * The Floid message number.
     */
    protected Long floidMessageNumber = 0L;

    /**
     * The timestamp
     */
    protected long timestamp = (new Date()).getTime();

    /**
     * Gets id.
     *
     * @return the id
     */
    @SuppressWarnings("unused")
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    @SuppressWarnings("unused")
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets floid uuid.
     *
     * @return the floid uuid
     */
    @SuppressWarnings("unused")
    public String getFloidUuid() {
        return floidUuid;
    }

    /**
     * Sets floid uuid.
     *
     * @param floidUuid the floid uuid
     */
    @SuppressWarnings("unused")
    public void setFloidUuid(String floidUuid) {
        this.floidUuid = floidUuid;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    @SuppressWarnings("unused")
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    @SuppressWarnings("unused")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Convert this to json
     *
     * @return the json object
     * @throws JSONException the json exception
     */
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(FloidCommands.PARAMETER_TYPE, type);
        jsonObject.put(FloidCommands.PARAMETER_FLOID_UUID, floidUuid);
        jsonObject.put(FloidCommands.PARAMETER_FLOID_ID, floidId);
        jsonObject.put(PARAMETER_FLOID_MESSAGE_NUMBER, floidMessageNumber);
        return jsonObject;
    }

    /**
     * Convert this from json
     *
     * @param jsonObject the json object
     * @return the floid message
     * @throws JSONException the json exception
     */
    public FloidMessage fromJSON(JSONObject jsonObject) throws JSONException {
        type = jsonObject.getString(FloidCommands.PARAMETER_TYPE);
        floidUuid = jsonObject.getString(FloidCommands.PARAMETER_FLOID_UUID);
        floidId = jsonObject.getInt(FloidCommands.PARAMETER_FLOID_ID);
        floidMessageNumber = jsonObject.getLong(PARAMETER_FLOID_MESSAGE_NUMBER);
        return this;
    }

    public FloidMessage fromMap(Map<String, Object> map) {
        type = (String)map.get(FloidCommands.PARAMETER_TYPE);
        floidUuid = (String)map.get(FloidCommands.PARAMETER_FLOID_UUID);
        floidId = (int)map.get(FloidCommands.PARAMETER_FLOID_ID);
        floidMessageNumber = (Long)map.get(PARAMETER_FLOID_MESSAGE_NUMBER);
        try {
            timestamp = (Long)map.get(PARAMETER_TIMESTAMP);
        } catch (Exception e) {
            // Do nothing..
        }
        return this;
    }
    /**
     * Gets floid id.
     *
     * @return the floid id
     */
    @SuppressWarnings("unused")
    public int getFloidId() {
        return floidId;
    }

    /**
     * Sets floid id.
     *
     * @param floidId the floid id
     */
    @SuppressWarnings("unused")
    public void setFloidId(int floidId) {
        this.floidId = floidId;
    }

    /**
     * Gets floid message number.
     *
     * @return the floid message number
     */
    @SuppressWarnings("unused")
    public Long getFloidMessageNumber() {
        return floidMessageNumber;
    }

    /**
     * Sets floid message number.
     *
     * @param floidMessageNumber the floid message number
     */
    @SuppressWarnings("unused")
    public void setFloidMessageNumber(Long floidMessageNumber) {
        this.floidMessageNumber = floidMessageNumber;
    }

    /**
     * Return and update the next floid message number
     * @return the next floid message number
     */
    public static long getNextFloidMessageNumber() {
        return nextFloidMessageNumber++;
    }

}
