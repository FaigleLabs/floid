
/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.servertypes.statuses;

import com.faiglelabs.floid.database.FloidDatabaseTables;
import com.faiglelabs.floid.utils.HexUtils;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Map;

/**
 * Floid status - the current status of the floid device
 */
@SuppressWarnings("unused")
@Entity
@Table(name = FloidDatabaseTables.FLOID_STATUS,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidUuidIndex", columnList = "floidUuid"),
        @Index(name="timestampIndex", columnList = "timestamp"),
        @Index(name="typeIndex", columnList = "type"),
        @Index(name="floidMessageNumberIndex", columnList = "floidMessageNumber"),
        @Index(name="stdIndex", columnList = "std")
})
public class FloidStatus extends FloidMessage implements Cloneable {
    /**
     * Test mode
     */
    public final static int FLOID_MODE_TEST = 0;
    /**
     * Mission mode
     */
    public final static int FLOID_MODE_MISSION = 1;
    private static final int FLOID_RUN_MODE_PRODUCTION                          = 0x00;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_XY                     = 0x01;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1             = 0x02;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2             = 0x04;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1              = 0x08;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1                = 0x10;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1                 = 0x20;

    private static final int FLOID_RUN_MODE_PHYSICS_TEST_XY_BIT                 = 0;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1_BIT         = 1;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2_BIT         = 2;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1_BIT          = 3;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1_BIT            = 4;
    private static final int FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1_BIT             = 5;


    // TEST MODES:
    private static final int FLOID_TEST_MODE_OFF                                = 0x00;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_XY                      = 0x01;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE                = 0x02;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE            = 0x04;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_HEADING                 = 0x08;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_PITCH                   = 0x10;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_ROLL                    = 0x20;
    private static final int FLOID_TEST_MODE_PRINT_DEBUG_PHYSICS_HEADINGS       = 0x40;
    private static final int FLOID_TEST_MODE_CHECK_PHYSICS_MODEL                = 0x80;

    private static final int FLOID_TEST_MODE_PHYSICS_NO_XY_BIT                  = 0;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE_BIT            = 1;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE_BIT        = 2;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_HEADING_BIT             = 3;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_PITCH_BIT               = 4;
    private static final int FLOID_TEST_MODE_PHYSICS_NO_ROLL_BIT                = 5;
    private static final int FLOID_TEST_MODE_PRINT_DEBUG_PHYSICS_HEADINGS_BIT   = 6;
    private static final int FLOID_TEST_MODE_CHECK_PHYSICS_MODEL_BIT            = 7;
    // IMU algorithm status display:
    public static final int IMU_ALGORITHM_STANDBY                               = 0x01;
    public static final String IMU_ALGORITHM_STANDBY_TEXT                       = "STANDBY";
    public static final String IMU_ALGORITHM_STANDBY_FALSE_TEXT                 = "";
    public static final int IMU_ALGORITHM_SLOW                                  = 0x02;
    public static final String IMU_ALGORITHM_SLOW_TEXT                          = "SLOW";
    public static final String IMU_ALGORITHM_SLOW_FALSE_TEXT                    = "";
    public static final int IMU_ALGORITHM_STILL                                 = 0x04;
    public static final String IMU_ALGORITHM_STILL_TEXT                         = "STILL";
    public static final String IMU_ALGORITHM_STILL_FALSE_TEXT                   = "";
    public static final int IMU_ALGORITHM_STABLE                                = 0x08;
    public static final String IMU_ALGORITHM_STABLE_TEXT                        = "STABLE";
    public static final String IMU_ALGORITHM_STABLE_FALSE_TEXT                  = "CALIBRATING";
    public static final int IMU_ALGORITHM_MAG_TRANSIENT                         = 0x10;
    public static final String IMU_ALGORITHM_MAG_TRANSIENT_TEXT                 = "MAG. TRANS";
    public static final String IMU_ALGORITHM_MAG_TRANSIENT_FALSE_TEXT           = "";
    public static final int IMU_ALGORITHM_UNRELIABLE                            = 0x20;
    public static final String IMU_ALGORITHM_UNRELIABLE_TEXT                    = "UNRELIABLE";
    public static final String IMU_ALGORITHM_UNRELIABLE_FALSE_TEXT              = "";
    public static final int IMU_ALGORITHM_WS_PARAMETERS_SAVED                   = 0x40;
    public static final String IMU_ALGORITHM_WS_PARAMETERS_SAVED_TEXT           = "SAVED";
    public static final String IMU_ALGORITHM_WS_PARAMETERS_SAVED_FALSE_TEXT     = "";
    public static final int IMU_ALGORITHM_WS_PARAMETERS_LOADED                  = 0x80;
    public static final String IMU_ALGORITHM_WS_PARAMETERS_LOADED_TEXT          = "LOADED";
    public static final String IMU_ALGORITHM_WS_PARAMETERS_LOADED_FALSE_TEXT    = "";
    public static final int IMU_ALGORITHM_WS_PARAMETERS_CLEARED                 = 0x100;
    public static final String IMU_ALGORITHM_WS_PARAMETERS_CLEARED_TEXT         = "CLEARED";
    public static final String IMU_ALGORITHM_WS_PARAMETERS_CLEARED_FALSE_TEXT   = "";

    @Override
    public FloidStatus clone() throws CloneNotSupportedException {
        return (FloidStatus) super.clone();
    }

    /**
     * Constructor
     */
    public FloidStatus() {
        type = getClass().getName();
        floidMessageNumber = getNextFloidMessageNumber();
    }

    // Status time droid - added by the droid, not the arduino
    /**
     * Status time (Droid)
     */
    protected long std = 0L;   // Status time droid
    // Status time: - 'st'
    /**
     * Status time
     */
    protected int st;    // STATUS_PACKET_OFFSET_STATUS_TIME
    // Status number: - 'sn'
    /**
     * Status number
     */
    protected int sn = -1;    // STATUS_PACKET_OFFSET_STATUS_NUMBER
    // Floid mode: - 'fm'
    /**
     * Floid mode
     */
    protected int fm;    // STATUS_PACKET_OFFSET_MODE
    // Free memory: - 'ffm'
    /**
     * Free memory
     */
    protected int ffm;    // STATUS_PACKET_OFFSET_FREE_MEMORY
    // Follow mode: -fmf
    /**
     * Follow mode
     */
    protected boolean fmf;    // STATUS_PACKET_OFFSET_FOLLOW_MODE
    //System Device Status: - 'd'
    /**
     * Camera on
     */
    protected boolean dc;    // STATUS_PACKET_OFFSET_CAMERA_ON
    /**
     * GPS on
     */
    protected boolean dg;    // STATUS_PACKET_OFFSET_GPS_ON
    /**
     * GPS emulate
     */
    protected boolean df;    // STATUS_PACKET_OFFSET_GPS_EMULATE
    /**
     * GPS from device
     */
    protected boolean dv;    // STATUS_PACKET_OFFSET_GPS_DEVICE
    /**
     * IMU on
     */
    protected boolean dp;    // STATUS_PACKET_OFFSET_PYR_ON
    /**
     * Designator on
     */
    protected boolean dd;    // STATUS_PACKET_OFFSET_DESIGNATOR_ON
    /**
     * AUS on
     */
    protected boolean da;    // STATUS_PACKET_OFFSET_AUX_ON
    /**
     * LED on
     */
    protected boolean dl;    // STATUS_PACKET_OFFSET_LED_ON
    /**
     * Helis on
     */
    protected boolean dh;    // STATUS_PACKET_OFFSET_HELIS_ON
    /**
     * Parachute deployed
     */
    protected boolean de;    // STATUS_PACKET_OFFSET_PARACHUTE_DEPLOYED
    // Physics Model: - 'k'
    /**
     * Direction over ground (in std trig angle - not compass degrees)
     */
    protected double kd;    // STATUS_PACKET_OFFSET_DIRECTION_OVER_GROUND
    /**
     * Velocity over ground - (velocity we are moving in the direction over ground)
     */
    protected double kv;    // STATUS_PACKET_OFFSET_VELOCITY_OVER_GROUND
    /**
     * Altitude (m)
     */
    protected double ka;    // STATUS_PACKET_OFFSET_ALTITUDE
    /**
     * Altitude velocity (m/s)
     */
    protected double kw;    // STATUS_PACKET_OFFSET_ALTITUDE_VELOCITY
    /**
     * Declination
     */
    protected double kc;    // STATUS_PACKET_OFFSET_DECLINATION (declination?)
    //GPS: - 'j'
    /**
     * GPS Time
     */
    protected int jt;    // STATUS_PACKET_OFFSET_GPS_TIME
    /**
     * GPS Longitude degrees decimal
     */
    protected double jx;    // STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL
    /**
     * GPS latitude degrees decimal
     */
    protected double jy;    // STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL
    /**
     * GPS fix quality
     */
    protected int jf;    // STATUS_PACKET_OFFSET_GPS_FIX_QUALITY
    /**
     * GPS altitude (meters)
     */
    protected double jm;    // STATUS_PACKET_OFFSET_GPS_Z_METERS
    /**
     * GPS altitude (feet)
     */
    protected double jz;    // STATUS_PACKET_OFFSET_GPS_Z_FEET
    /**
     * GPS HDOP
     */
    protected double jh;    // STATUS_PACKET_OFFSET_GPS_HDOP
    /**
     * GPS sats
     */
    protected int js;    // STATUS_PACKET_OFFSET_GPS_SATS
    /**
     * GPS longitude filtered
     */
    protected double jxf;   // STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL_FILTERED
    /**
     * GPS latitude filtered
     */
    protected double jyf;   // STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL_FILTERED
    /**
     * GPS altitude meters filtered
     */
    protected double jmf;   // STATUS_PACKET_OFFSET_GPS_Z_METERS_FILTERED
    /**
     * GPS good count
     */
    protected int jg;    // STATUS_PACKET_OFFSET_GPS_GOOD_COUNT
    /**
     * GPS rate
     */
    protected int jr;    // STATUS_PACKET_OFFSET_GPS_RATE
    // IMU: - 'p'
    /**
     * IMU heading
     */
    protected double ph;    // STATUS_PACKET_OFFSET_PYR_HEADING
    /**
     * IMU heading smoothed
     */
    protected double phs;   // STATUS_PACKET_OFFSET_PYR_HEADING_SMOOTHED
    /**
     * IMU heading velocity
     */
    protected double phv;   // STATUS_PACKET_OFFSET_PYR_HEADING_VELOCITY
    /**
     * IMU altitude
     */
    protected double pz;    // STATUS_PACKET_OFFSET_PYR_HEIGHT
    /**
     * IMU altitude smoothed
     */
    protected double pzs;   // STATUS_PACKET_OFFSET_PYR_HEIGHT_SMOOTHED
    /**
     * IMU temperature
     */
    protected double pt;    // STATUS_PACKET_OFFSET_PYR_TEMPERATURE
    /**
     * IMU pitch
     */
    protected double pp;    // STATUS_PACKET_OFFSET_PYR_PITCH
    /**
     * IMU pitch smoothed
     */
    protected double pps;   // STATUS_PACKET_OFFSET_PYR_PITCH_SMOOTHED
    /**
     * IMU device on
     */
    protected boolean pv;    // STATUS_PACKET_OFFSET_PYR_DEVICE
    /**
     * IMU roll
     */
    protected double pr;    // STATUS_PACKET_OFFSET_PYR_ROLL
    /**
     * IMU roll smoothed
     */
    protected double prs;   // STATUS_PACKET_OFFSET_PYR_ROLL_SMOOTHED
    /**
     * IMU pitch velocity
     */
    protected double ppv;   // STATUS_PACKET_OFFSET_PYR_PITCH_VELOCITY
    /**
     * IMU roll velocity
     */
    protected double prv;   // STATUS_PACKET_OFFSET_PYR_ROLL_VELOCITY
    /**
     * IMU altitude velocity
     */
    protected double pzv;   // STATUS_PACKET_OFFSET_PYR_HEIGHT_VELOCITY
    /**
     * IMU digital mode
     */
    protected boolean pd;    // STATUS_PACKET_OFFSET_PYR_DIGITAL_MODE
    /**
     * IMU analog rate
     */
    protected int par;   // STATUS_PACKET_OFFSET_PYR_ANALOG_RATE
    /**
     * IMU good count
     */
    protected int pg;    // STATUS_PACKET_OFFSET_PYR_GOOD_COUNT
    /**
     * IMU error count
     */
    protected int pe;    // STATUS_PACKET_OFFSET_PYR_ERROR_COUNT
    // Batteries: - 'b'
    /**
     * Main battery value
     */
    protected double bm;     // STATUS_PACKET_OFFSET_MAIN_BATTERY
    /**
     * Heli 0 battery value
     */
    protected double b0;     // STATUS_PACKET_OFFSET_HELI0_BATTERY
    /**
     * Heli 1 battery value
     */
    protected double b1;     // STATUS_PACKET_OFFSET_HELI1_BATTERY
    /**
     * Heli 2 battery value
     */
    protected double b2;     // STATUS_PACKET_OFFSET_HELI2_BATTERY
    /**
     * Heli 3 battery value
     */
    protected double b3;     // STATUS_PACKET_OFFSET_HELI3_BATTERY
    /**
     * AUX battery value
     */
    protected double ba;     // STATUS_PACKET_OFFSET_AUX_BATTERY
    // Currents: - 'c'
    /**
     * Heli 0 current
     */
    protected double c0;     // STATUS_PACKET_OFFSET_HELI0_CURRENT
    /**
     * Heli 1 current
     */
    protected double c1;     // STATUS_PACKET_OFFSET_HELI1_CURRENT
    /**
     * Heli 2 current
     */
    protected double c2;     // STATUS_PACKET_OFFSET_HELI2_CURRENT
    /**
     * Heli 3 current
     */
    protected double c3;     // STATUS_PACKET_OFFSET_HELI3_CURRENT
    /**
     * AUX current
     */
    protected double ca;     // STATUS_PACKET_OFFSET_AUX_CURRENT
    // Servos - 's#'
    /**
     * Heli 0 servos on
     */
    protected boolean s0o;    // STATUS_PACKET_OFFSET_S_0_ON
    /**
     * Heli 1 servos on
     */
    protected boolean s1o;    // STATUS_PACKET_OFFSET_S_1_ON
    /**
     * Heli 2 servos on
     */
    protected boolean s2o;    // STATUS_PACKET_OFFSET_S_2_ON
    /**
     * Heli 3 servos on
     */
    protected boolean s3o;    // STATUS_PACKET_OFFSET_S_3_ON
    /**
     * Heli 0 Servo 0 (L) value
     */
    protected double s00;    // STATUS_PACKET_OFFSET_S_0_L_VALUE
    /**
     * Heli 0 Servo 1 (R) value
     */
    protected double s01;    // STATUS_PACKET_OFFSET_S_0_R_VALUE
    /**
     * Heli 0 Servo 2 (P) value
     */
    protected double s02;    // STATUS_PACKET_OFFSET_S_0_P_VALUE
    /**
     * Heli 1 Servo 0 (L) value
     */
    protected double s10;    // STATUS_PACKET_OFFSET_S_1_L_VALUE
    /**
     * Heli 1 Servo 1 (R) value
     */
    protected double s11;    // STATUS_PACKET_OFFSET_S_1_R_VALUE
    /**
     * Heli 1 Servo 2 (P) value
     */
    protected double s12;    // STATUS_PACKET_OFFSET_S_1_P_VALUE
    /**
     * Heli 2 Servo 0 (L) value
     */
    protected double s20;    // STATUS_PACKET_OFFSET_S_2_L_VALUE
    /**
     * Heli 2 Servo 1 (R) value
     */
    protected double s21;    // STATUS_PACKET_OFFSET_S_2_R_VALUE
    /**
     * Heli 2 Servo 2 (P) value
     */
    protected double s22;    // STATUS_PACKET_OFFSET_S_2_P_VALUE
    /**
     * Heli 3 Servo 0 (L) value
     */
    protected double s30;    // STATUS_PACKET_OFFSET_S_3_L_VALUE
    /**
     * Heli 3 Servo 1 (R) value
     */
    protected double s31;    // STATUS_PACKET_OFFSET_S_3_R_VALUE
    /**
     * Heli 3 Servo 2 (P) value
     */
    protected double s32;    // STATUS_PACKET_OFFSET_S_3_P_VALUE
    // ESCs: - 'e'
    /**
     * ESC type
     */
    protected int et;     // STATUS_PACKET_OFFSET_ESC_TYPE
    /**
     * ESC 0 on
     */
    protected boolean e0o;    // STATUS_PACKET_OFFSET_ESC_0_ON
    /**
     * ESC 1 on
     */
    protected boolean e1o;    // STATUS_PACKET_OFFSET_ESC_1_ON
    /**
     * ESC 2 on
     */
    protected boolean e2o;    // STATUS_PACKET_OFFSET_ESC_2_ON
    /**
     * ESC 3 on
     */
    protected boolean e3o;    // STATUS_PACKET_OFFSET_ESC_3_ON
    /**
     * ESC 0 value
     */
    protected double e0;     // STATUS_PACKET_OFFSET_ESC_0_VALUE
    /**
     * ESC 1 value
     */
    protected double e1;     // STATUS_PACKET_OFFSET_ESC_1_VALUE
    /**
     * ESC 2 value
     */
    protected double e2;     // STATUS_PACKET_OFFSET_ESC_2_VALUE
    /**
     * ESC 3 value
     */
    protected double e3;     // STATUS_PACKET_OFFSET_ESC_3_VALUE
    /**
     * ESC 0 pulse
     */
    protected int ep0;    // STATUS_PACKET_OFFSET_ESC_0_PULSE_VALUE
    /**
     * ESC 1 pulse
     */
    protected int ep1;    // STATUS_PACKET_OFFSET_ESC_1_PULSE_VALUE
    /**
     * ESC 2 pulse
     */
    protected int ep2;    // STATUS_PACKET_OFFSET_ESC_2_PULSE_VALUE
    /**
     * ESC 3 pulse
     */
    protected int ep3;    // STATUS_PACKET_OFFSET_ESC_3_PULSE_VALUE
    // Goals: - 'g'
    /**
     * Has goal
     */
    protected boolean ghg;    // STATUS_PACKET_OFFSET_HAS_GOAL
    /**
     * Goal id
     */
    protected int gid;    // STATUS_PACKET_OFFSET_GOAL_ID
    /**
     * Current goal ticks
     */
    protected int gt;     // STATUS_PACKET_OFFSET_GOAL_TICKS
    /**
     * Current goal state
     */
    protected int gs;     // STATUS_PACKET_OFFSET_GOAL_STATE
    /**
     * Goal longitude
     */
    protected double gx;     // STATUS_PACKET_OFFSET_GOAL_X
    /**
     * Goal latitude
     */
    protected double gy;     // STATUS_PACKET_OFFSET_GOAL_Y
    /**
     * Goal altitude
     */
    protected double gz;     // STATUS_PACKET_OFFSET_GOAL_Z
    /**
     * Goal heading (compass degrees)
     */
    protected double ga;     // STATUS_PACKET_OFFSET_GOAL_A  // IN HEADING=COMPASS DEGREES
    /**
     * In flight
     */
    protected boolean gif;    // STATUS_PACKET_OFFSET_IN_FLIGHT
    /**
     * Lift off mode
     */
    protected boolean glo;    // STATUS_PACKET_OFFSET_LIFT_OFF_MODE
    /**
     * Land mode
     */
    protected boolean gld;    // STATUS_PACKET_OFFSET_LAND_MODE
    // Pan Tilt: - 't'
    /**
     * Pan/Tilt 0 mode
     */
    protected int t0m;    // STATUS_PACKET_OFFSET_PT_0_MODE
    /**
     * Pan/Tilt 0 mode
     */
    protected int t1m;    // STATUS_PACKET_OFFSET_PT_1_MODE
    /**
     * Pan/Tilt 0 mode
     */
    protected int t2m;    // STATUS_PACKET_OFFSET_PT_2_MODE
    /**
     * Pan/Tilt 0 mode
     */
    protected int t3m;    // STATUS_PACKET_OFFSET_PT_3_MODE
    /**
     * Pan/Tilt 0 target longitude
     */
    protected double t0x;    // STATUS_PACKET_OFFSET_PT_0_TARGET_X
    /**
     * Pan/Tilt 0 target latitude
     */
    protected double t0y;    // STATUS_PACKET_OFFSET_PT_0_TARGET_Y
    /**
     * Pan/Tilt 0 target altitude
     */
    protected double t0z;    // STATUS_PACKET_OFFSET_PT_0_TARGET_Z
    /**
     * Pan/Tilt 1 target longitude
     */
    protected double t1x;    // STATUS_PACKET_OFFSET_PT_1_TARGET_X
    /**
     * Pan/Tilt 1 target latitude
     */
    protected double t1y;    // STATUS_PACKET_OFFSET_PT_1_TARGET_Y
    /**
     * Pan/Tilt 1 target altitude
     */
    protected double t1z;    // STATUS_PACKET_OFFSET_PT_1_TARGET_Z
    /**
     * Pan/Tilt 2 target longitude
     */
    protected double t2x;    // STATUS_PACKET_OFFSET_PT_2_TARGET_X
    /**
     * Pan/Tilt 2 target latitude
     */
    protected double t2y;    // STATUS_PACKET_OFFSET_PT_2_TARGET_Y
    /**
     * Pan/Tilt 2 target altitude
     */
    protected double t2z;    // STATUS_PACKET_OFFSET_PT_2_TARGET_Z
    /**
     * Pan/Tilt 0 target longitude
     */
    protected double t3x;    // STATUS_PACKET_OFFSET_PT_3_TARGET_X
    /**
     * Pan/Tilt 3 target latitude
     */
    protected double t3y;    // STATUS_PACKET_OFFSET_PT_3_TARGET_Y
    /**
     * Pan/Tilt 3 target altitude
     */
    protected double t3z;    // STATUS_PACKET_OFFSET_PT_3_TARGET_Z
    /**
     * Pan/Tilt 0 on
     */
    protected boolean t0o;    // STATUS_PACKET_OFFSET_PT_0_ON
    /**
     * Pan/Tilt 1 on
     */
    protected boolean t1o;    // STATUS_PACKET_OFFSET_PT_1_ON
    /**
     * Pan/Tilt 2 on
     */
    protected boolean t2o;    // STATUS_PACKET_OFFSET_PT_2_ON
    /**
     * Pan/Tilt 3 on
     */
    protected boolean t3o;    // STATUS_PACKET_OFFSET_PT_3_ON
    /**
     * Pan/Tilt 0 pan value
     */
    protected int t0p;    // STATUS_PACKET_OFFSET_PT_0_PAN_VALUE
    /**
     * Pan/Tilt 0 tilt value
     */
    protected int t0t;    // STATUS_PACKET_OFFSET_PT_0_TILT_VALUE
    /**
     * Pan/Tilt 1 pan value
     */
    protected int t1p;    // STATUS_PACKET_OFFSET_PT_1_PAN_VALUE
    /**
     * Pan/Tilt 1 tilt value
     */
    protected int t1t;    // STATUS_PACKET_OFFSET_PT_1_TILT_VALUE
    /**
     * Pan/Tilt 2 pan value
     */
    protected int t2p;    // STATUS_PACKET_OFFSET_PT_2_PAN_VALUE
    /**
     * Pan/Tilt 2 tilt value
     */
    protected int t2t;    // STATUS_PACKET_OFFSET_PT_2_TILT_VALUE
    /**
     * Pan/Tilt 3 pan value
     */
    protected int t3p;    // STATUS_PACKET_OFFSET_PT_3_PAN_VALUE
    /**
     * Pan/Tilt 3 tilt value
     */
    protected int t3t;    // STATUS_PACKET_OFFSET_PT_3_TILT_VALUE
    // Payloads: - 'y'
    /**
     * Payload 0 on
     */
    protected boolean y0;     // STATUS_PACKET_OFFSET_BAY0
    /**
     * Payload 1 on
     */
    protected boolean y1;     // STATUS_PACKET_OFFSET_BAY1
    /**
     * Payload 2 on
     */
    protected boolean y2;     // STATUS_PACKET_OFFSET_BAY2
    /**
     * Payload 3 on
     */
    protected boolean y3;     // STATUS_PACKET_OFFSET_BAY3
    /**
     * Payload 0 nm value
     */
    protected int yn0;    // STATUS_PACKET_OFFSET_NM0
    /**
     * Payload 1 nm value
     */
    protected int yn1;    // STATUS_PACKET_OFFSET_NM1
    /**
     * Payload 2 nm value
     */
    protected int yn2;    // STATUS_PACKET_OFFSET_NM2
    /**
     * Payload 3 nm value
     */
    protected int yn3;    // STATUS_PACKET_OFFSET_NM3
    /**
     * Payload 0 goal state
     */
    protected int yg0;    // STATUS_PACKET_OFFSET_PAYLOAD_0_GOAL_STATE
    /**
     * Payload 1 goal state
     */
    protected int yg1;    // STATUS_PACKET_OFFSET_PAYLOAD_1_GOAL_STATE
    /**
     * Payload 2 goal state
     */
    protected int yg2;    // STATUS_PACKET_OFFSET_PAYLOAD_2_GOAL_STATE
    /**
     * Payload 3 goal state
     */
    protected int yg3;    // STATUS_PACKET_OFFSET_PAYLOAD_3_GOAL_STATE
    // Debug: - 'v'
    /**
     * Is debug on
     */
    protected boolean vd;     // STATUS_PACKET_OFFSET_DEBUG_ON
    /**
     * Is debug all on
     */
    protected boolean va;     // STATUS_PACKET_OFFSET_DEBUG_ALL_ON
    /**
     * Is debug AUX on
     */
    protected boolean vx;     // STATUS_PACKET_OFFSET_DEBUG_AUX_ON
    /**
     * Is debug payloads on
     */
    protected boolean vb;     // STATUS_PACKET_OFFSET_DEBUG_PAYLOADS_ON
    /**
     * Is debug camera on
     */
    protected boolean vc;     // STATUS_PACKET_OFFSET_DEBUG_CAMERA_ON
    /**
     * Is debug designator on
     */
    protected boolean ve;     // STATUS_PACKET_OFFSET_DEBUG_DESIGNATOR_ON
    /**
     * Is debug GPS on
     */
    protected boolean vg;     // STATUS_PACKET_OFFSET_DEBUG_GPS_ON
    /**
     * Is debug helis on
     */
    protected boolean vh;     // STATUS_PACKET_OFFSET_DEBUG_HELIS_ON
    /**
     * Is debug memory on
     */
    protected boolean vm;     // STATUS_PACKET_OFFSET_DEBUG_MEMORY_ON
    /**
     * Is debug pan/tilt on
     */
    protected boolean vt;     // STATUS_PACKET_OFFSET_DEBUG_PAN_TILT_ON
    /**
     * Is debug physics on
     */
    protected boolean vy;     // STATUS_PACKET_OFFSET_DEBUG_PHYSICS_ON
    /**
     * Is debug IMU on
     */
    protected boolean vp;     // STATUS_PACKET_OFFSET_DEBUG_PYR_ON
    /**
     * Is debug altimeter on
     */
    protected boolean vl;     // STATUS_PACKET_OFFSET_DEBUG_ALTIMETER
    /**
     * Is debug serial on
     */
    protected boolean vs;     // STATUS_PACKET_OFFSET_SERIAL_ON
    // Altimeter: -'a'
    /**
     * Altimeter initialized
     */
    protected boolean ai;     // STATUS_PACKET_OFFSET_ALTIMETER_INITIALIZED
    /**
     * Altimeter altitude valid
     */
    protected boolean av;     // STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE_VALID
    /**
     * Altimeter altitude
     */
    protected double aa;     // STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE
    /**
     * Altimeter delta
     */
    protected double ad;     // STATUS_PACKET_OFFSET_ALTIMETER_DELTA
    /**
     * Altimeter good count
     */
    protected int ac;     // STATUS_PACKET_OFFSET_ALTIMETER_GOOD_COUNT
    // Loop counts and rates - 'l'
    /**
     * Loop count since last update
     */
    protected int lc;     // Loop count since last status update
    /**
     * Loop rate since last update
     */
    protected double lr;     // Loop rate since last status update
    /**
     * Floid status rate
     */
    protected int sr;     // Floid status rate
    // Model: - 'm'
    /**
     * Lift off target altitude
     */
    protected double mt;     // liftOffTargetAltitude;
    /**
     * Distance to target
     */
    protected double md;     // distanceToTarget;
    /**
     * Altitude to target
     */
    protected double ma;     // altitudeToTarget;
    /**
     * Delta heading angle to target - in standard angles, NOT COMPASS
     */
    protected double mo;     // deltaHeadingAngleToTarget; Standard angles, NOT COMPASS
    /**
     * Orientation to goal - in standard angles, NOT COMPASS
     */
    protected double mg;     // orientationToGoal; Standard angles, NOT COMPASS
    /**
     * Attack angle
     */
    protected double mk;     // attackAngle;
    // Target: - 'o'
    /**
     * Target orientation pitch
     */
    protected double oop;    // targetOrientationPitch;
    /**
     * Target orientation pitch delta
     */
    protected double ooq;    // targetOrientationPitchDelta;
    /**
     * Target orientation roll
     */
    protected double oor;    // targetOrientationRoll;
    /**
     * Target orientation roll delta
     */
    protected double oos;    // targetOrientationRollDelta;
    /**
     * Target pitch velocity
     */
    protected double opv;    // targetPitchVelocity;
    /**
     * Target pitch velocity delta
     */
    protected double opw;    // targetPitchVelocityDelta;
    /**
     * Target roll velocity
     */
    protected double orv;    // targetRollVelocity;
    /**
     * Target roll velocity delta
     */
    protected double orw;    // targetRollVelocityDelta;
    /**
     * Target altitude velocity
     */
    protected double oav;    // targetAltitudeVelocity;
    /**
     * Target altitude velocity delta
     */
    protected double oaw;    // targetAltitudeVelocityDelta;
    /**
     * Target heading velocity
     */
    protected double ohv;    // targetHeadingVelocity;
    /**
     * Target heading velocity delta
     */
    protected double ohw;    // targetHeadingVelocityDelta;
    /**
     * Target xy velocity (over ground)
     */
    protected double ov;     // targetXYVelocity;
    // Land mode - 'l'
    /**
     * Land mode min altitude check started
     */
    protected boolean lmc;    // landModeMinAltitudeCheckStarted
    /**
     * Land mode min altitude
     */
    protected double lma;    // landModeMinAltitude
    /**
     * Land mode min altitude time
     */
    protected int lmt;    // landModeMinAltitudeTime
    // Heli Startup Mode: -'h'
    /**
     * Heli startup mode
     */
    protected boolean hsm;    // heliStartupMode
    // IMU from device:
    /**
     * IMU from device
     */
    protected boolean dpy;    // devicePYR
    // Debug altimeter:
    /**
     * Debug altimeter
     */
    protected boolean dba;    // debugAltimeter
    // Run and test modes
    /**
     * Floid run mode
     */
    protected byte    rm;     // floidRunMode;
    /**
     * Floid test mode
     */
    protected byte    tm;     // floidTestMode;

    /**
     * IMU algorithm status:
     */
    protected int    ias;    // IMU algorithm status;

    @Override
    public FloidStatus fromMap(Map<String, Object> map) throws JSONException {
        super.fromMap(map);

        if(map.containsKey("std")) {
            std = (long)map.get("std");
        }
        st = (int)map.get("st");
        sn = (int)map.get("sn");
        fm = (int)map.get("fm");
        ffm = (int)map.get("ffm");
        fmf = (Boolean)map.get("fmf");
        dc = (Boolean)map.get("dc");
        dg = (Boolean)map.get("dg");
        df = (Boolean)map.get("df");
        dv = (Boolean)map.get("dv");
        dp = (Boolean)map.get("dp");
        dd = (Boolean)map.get("dd");
        da = (Boolean)map.get("da");
        dl = (Boolean)map.get("dl");
        dh = (Boolean)map.get("dh");
        de = (Boolean)map.get("de");
        kd = (double)map.get("kd");
        kv = (double)map.get("kv");
        ka = (double)map.get("ka");
        kw = (double)map.get("kw");
        kc = (double)map.get("kc");
        jt = (int)map.get("jt");
        jx = (double)map.get("jx");
        jy = (double)map.get("jy");
        jf = (int)map.get("jf");
        jz = (double)map.get("jz");
        jm = (double)map.get("jm");
        jh = (double)map.get("jh");
        js = (int)map.get("js");
        jxf = (double)map.get("jxf");
        jyf = (double)map.get("jyf");
        jmf = (double)map.get("jmf");
        jg = (int)map.get("jg");
        jr = (int)map.get("jr");
        ph = (double)map.get("ph");
        phs = (double)map.get("phs");
        phv = (double)map.get("phv");
        pz = (double)map.get("pz");
        pzs = (double)map.get("pzs");
        pt = (double)map.get("pt");
        pp = (double)map.get("pp");
        pps = (double)map.get("pps");
        pv = (Boolean)map.get("pv");
        pr = (double)map.get("pr");
        prs = (double)map.get("prs");
        ppv = (double)map.get("ppv");
        prv = (double)map.get("prv");
        pzv = (double)map.get("pzv");
        pg = (int)map.get("pg");
        pe = (int)map.get("pe");
        bm = (double)map.get("bm");
        b0 = (double)map.get("b0");
        b1 = (double)map.get("b1");
        b2 = (double)map.get("b2");
        b3 = (double)map.get("b3");
        ba = (double)map.get("ba");
        c0 = (double)map.get("c0");
        c1 = (double)map.get("c1");
        c2 = (double)map.get("c2");
        c3 = (double)map.get("c3");
        ca = (double)map.get("ca");
        s0o = (Boolean)map.get("s0o");
        s1o = (Boolean)map.get("s1o");
        s2o = (Boolean)map.get("s2o");
        s3o = (Boolean)map.get("s3o");
        s00 = (double)map.get("s00");
        s01 = (double)map.get("s01");
        s02 = (double)map.get("s02");
        s10 = (double)map.get("s10");
        s11 = (double)map.get("s11");
        s12 = (double)map.get("s12");
        s20 = (double)map.get("s20");
        s21 = (double)map.get("s21");
        s22 = (double)map.get("s22");
        s30 = (double)map.get("s30");
        s31 = (double)map.get("s31");
        s32 = (double)map.get("s32");
        et = (int)map.get("et");
        e0o = (Boolean)map.get("e0o");
        e1o = (Boolean)map.get("e1o");
        e2o = (Boolean)map.get("e2o");
        e3o = (Boolean)map.get("e3o");
        e0 = (double)map.get("e0");
        e1 = (double)map.get("e1");
        e2 = (double)map.get("e2");
        e3 = (double)map.get("e3");
        ep0 = (int)map.get("ep0");
        ep1 = (int)map.get("ep1");
        ep2 = (int)map.get("ep2");
        ep3 = (int)map.get("ep3");
        ghg = (Boolean)map.get("ghg");
        gid = (int)map.get("gid");
        gt = (int)map.get("gt");
        gs = (int)map.get("gs");
        gx = (double)map.get("gx");
        gy = (double)map.get("gy");
        gz = (double)map.get("gz");
        ga = (double)map.get("ga");
        gif = (Boolean)map.get("gif");
        glo = (Boolean)map.get("glo");
        gld = (Boolean)map.get("gld");
        t0m = (int)map.get("t0m");
        t1m = (int)map.get("t1m");
        t2m = (int)map.get("t2m");
        t3m = (int)map.get("t3m");
        t0x = (double)map.get("t0x");
        t0y = (double)map.get("t0y");
        t0z = (double)map.get("t0z");
        t1x = (double)map.get("t1x");
        t1y = (double)map.get("t1y");
        t1z = (double)map.get("t1z");
        t2x = (double)map.get("t2x");
        t2y = (double)map.get("t2y");
        t2z = (double)map.get("t2z");
        t3x = (double)map.get("t3x");
        t3y = (double)map.get("t3y");
        t3z = (double)map.get("t3z");
        t0o = (Boolean)map.get("t0o");
        t1o = (Boolean)map.get("t1o");
        t2o = (Boolean)map.get("t2o");
        t3o = (Boolean)map.get("t3o");
        t0p = (int)map.get("t0p");
        t0t = (int)map.get("t0t");
        t1p = (int)map.get("t1p");
        t1t = (int)map.get("t1t");
        t2p = (int)map.get("t2p");
        t2t = (int)map.get("t2t");
        t3p = (int)map.get("t3p");
        t3t = (int)map.get("t3t");
        y0 = (Boolean)map.get("y0");
        y1 = (Boolean)map.get("y1");
        y2 = (Boolean)map.get("y2");
        y3 = (Boolean)map.get("y3");
        yn0 = (int)map.get("yn0");
        yn1 = (int)map.get("yn1");
        yn2 = (int)map.get("yn2");
        yn3 = (int)map.get("yn3");
        yg0 = (int)map.get("yg0");
        yg1 = (int)map.get("yg1");
        yg2 = (int)map.get("yg2");
        yg3 = (int)map.get("yg3");
        vd = (Boolean)map.get("vd");
        va = (Boolean)map.get("va");
        vx = (Boolean)map.get("vx");
        vb = (Boolean)map.get("vb");
        vc = (Boolean)map.get("vc");
        ve = (Boolean)map.get("ve");
        vg = (Boolean)map.get("vg");
        vh = (Boolean)map.get("vh");
        vm = (Boolean)map.get("vm");
        vt = (Boolean)map.get("vt");
        vy = (Boolean)map.get("vy");
        vp = (Boolean)map.get("vp");
        vl = (Boolean)map.get("vl");
        vs = (Boolean)map.get("vs");
        pd = (Boolean)map.get("pd");
        ai = (Boolean)map.get("ai");
        av = (Boolean)map.get("av");
        aa = (double)map.get("aa");
        ad = (double)map.get("ad");
        ac = (int)map.get("ac");
        lc = (int)map.get("lc");
        lr = (double)map.get("lr");
        sr = (int)map.get("sr");
        mt = (double)map.get("mt");
        md = (double)map.get("md");
        ma = (double)map.get("ma");
        mo = (double)map.get("mo");
        mg = (double)map.get("mg");
        mk = (double)map.get("mk");
        oop = (double)map.get("oop");     // targetOrientationPitch;
        ooq = (double)map.get("ooq");     // targetOrientationPitchDelta;
        oor = (double)map.get("oor");     // targetOrientationRoll;
        oos = (double)map.get("oos");     // targetOrientationRollDelta;
        opv = (double)map.get("opv");     // targetPitchVelocity;
        opw = (double)map.get("opw");     // targetPitchVelocityDelta;
        orv = (double)map.get("orv");     // targetRollVelocity;
        orw = (double)map.get("orw");     // targetRollVelocityDelta;
        oav = (double)map.get("oav");     // targetAltitudeVelocity;
        oaw = (double)map.get("oaw");     // targetAltitudeVelocityDelta;
        ohv = (double)map.get("ohv");     // targetHeadingVelocity;
        ohw = (double)map.get("ohw");     // targetHeadingVelocityDelta;
        ov = (double)map.get("ov");      // targetXYVelocity;
        lmc = (Boolean)map.get("lmc");
        lma = (double)map.get("lma");
        lmt = (int)map.get("lmt");
        hsm = (Boolean)map.get("hsm");
        dpy = (Boolean)map.get("dpy");
        dba = (Boolean)map.get("dba");
        rm = (Byte)map.get("rm");
        tm = (Byte)map.get("tm");
        ias = (int)map.get("ias");
        return this;
    }

    @Override
    public FloidStatus fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        if(jsonObject.has("std")) {
            std = jsonObject.getLong("std");
        }
        st = jsonObject.getInt("st");
        sn = jsonObject.getInt("sn");
        fm = jsonObject.getInt("fm");
        ffm = jsonObject.getInt("ffm");
        fmf = jsonObject.getBoolean("fmf");
        dc = jsonObject.getBoolean("dc");
        dg = jsonObject.getBoolean("dg");
        df = jsonObject.getBoolean("df");
        dv = jsonObject.getBoolean("dv");
        dp = jsonObject.getBoolean("dp");
        dd = jsonObject.getBoolean("dd");
        da = jsonObject.getBoolean("da");
        dl = jsonObject.getBoolean("dl");
        dh = jsonObject.getBoolean("dh");
        de = jsonObject.getBoolean("de");
        kd = jsonObject.getDouble("kd");
        kv = jsonObject.getDouble("kv");
        ka = jsonObject.getDouble("ka");
        kw = jsonObject.getDouble("kw");
        kc = jsonObject.getDouble("kc");
        jt = jsonObject.getInt("jt");
        jx = jsonObject.getDouble("jx");
        jy = jsonObject.getDouble("jy");
        jf = jsonObject.getInt("jf");
        jz = jsonObject.getDouble("jz");
        jm = jsonObject.getDouble("jm");
        jh = jsonObject.getDouble("jh");
        js = jsonObject.getInt("js");
        jxf = jsonObject.getDouble("jxf");
        jyf = jsonObject.getDouble("jyf");
        jmf = jsonObject.getDouble("jmf");
        jg = jsonObject.getInt("jg");
        jr = jsonObject.getInt("jr");
        ph = jsonObject.getDouble("ph");
        phs = jsonObject.getDouble("phs");
        phv = jsonObject.getDouble("phv");
        pz = jsonObject.getDouble("pz");
        pzs = jsonObject.getDouble("pzs");
        pt = jsonObject.getDouble("pt");
        pp = jsonObject.getDouble("pp");
        pps = jsonObject.getDouble("pps");
        pv = jsonObject.getBoolean("pv");
        pr = jsonObject.getDouble("pr");
        prs = jsonObject.getDouble("prs");
        ppv = jsonObject.getDouble("ppv");
        prv = jsonObject.getDouble("prv");
        pzv = jsonObject.getDouble("pzv");
        pg = jsonObject.getInt("pg");
        pe = jsonObject.getInt("pe");
        bm = jsonObject.getDouble("bm");
        b0 = jsonObject.getDouble("b0");
        b1 = jsonObject.getDouble("b1");
        b2 = jsonObject.getDouble("b2");
        b3 = jsonObject.getDouble("b3");
        ba = jsonObject.getDouble("ba");
        c0 = jsonObject.getDouble("c0");
        c1 = jsonObject.getDouble("c1");
        c2 = jsonObject.getDouble("c2");
        c3 = jsonObject.getDouble("c3");
        ca = jsonObject.getDouble("ca");
        s0o = jsonObject.getBoolean("s0o");
        s1o = jsonObject.getBoolean("s1o");
        s2o = jsonObject.getBoolean("s2o");
        s3o = jsonObject.getBoolean("s3o");
        s00 = jsonObject.getDouble("s00");
        s01 = jsonObject.getDouble("s01");
        s02 = jsonObject.getDouble("s02");
        s10 = jsonObject.getDouble("s10");
        s11 = jsonObject.getDouble("s11");
        s12 = jsonObject.getDouble("s12");
        s20 = jsonObject.getDouble("s20");
        s21 = jsonObject.getDouble("s21");
        s22 = jsonObject.getDouble("s22");
        s30 = jsonObject.getDouble("s30");
        s31 = jsonObject.getDouble("s31");
        s32 = jsonObject.getDouble("s32");
        et = jsonObject.getInt("et");
        e0o = jsonObject.getBoolean("e0o");
        e1o = jsonObject.getBoolean("e1o");
        e2o = jsonObject.getBoolean("e2o");
        e3o = jsonObject.getBoolean("e3o");
        e0 = jsonObject.getDouble("e0");
        e1 = jsonObject.getDouble("e1");
        e2 = jsonObject.getDouble("e2");
        e3 = jsonObject.getDouble("e3");
        ep0 = jsonObject.getInt("ep0");
        ep1 = jsonObject.getInt("ep1");
        ep2 = jsonObject.getInt("ep2");
        ep3 = jsonObject.getInt("ep3");
        ghg = jsonObject.getBoolean("ghg");
        gid = jsonObject.getInt("gid");
        gt = jsonObject.getInt("gt");
        gs = jsonObject.getInt("gs");
        gx = jsonObject.getDouble("gx");
        gy = jsonObject.getDouble("gy");
        gz = jsonObject.getDouble("gz");
        ga = jsonObject.getDouble("ga");
        gif = jsonObject.getBoolean("gif");
        glo = jsonObject.getBoolean("glo");
        gld = jsonObject.getBoolean("gld");
        t0m = jsonObject.getInt("t0m");
        t1m = jsonObject.getInt("t1m");
        t2m = jsonObject.getInt("t2m");
        t3m = jsonObject.getInt("t3m");
        t0x = jsonObject.getDouble("t0x");
        t0y = jsonObject.getDouble("t0y");
        t0z = jsonObject.getDouble("t0z");
        t1x = jsonObject.getDouble("t1x");
        t1y = jsonObject.getDouble("t1y");
        t1z = jsonObject.getDouble("t1z");
        t2x = jsonObject.getDouble("t2x");
        t2y = jsonObject.getDouble("t2y");
        t2z = jsonObject.getDouble("t2z");
        t3x = jsonObject.getDouble("t3x");
        t3y = jsonObject.getDouble("t3y");
        t3z = jsonObject.getDouble("t3z");
        t0o = jsonObject.getBoolean("t0o");
        t1o = jsonObject.getBoolean("t1o");
        t2o = jsonObject.getBoolean("t2o");
        t3o = jsonObject.getBoolean("t3o");
        t0p = jsonObject.getInt("t0p");
        t0t = jsonObject.getInt("t0t");
        t1p = jsonObject.getInt("t1p");
        t1t = jsonObject.getInt("t1t");
        t2p = jsonObject.getInt("t2p");
        t2t = jsonObject.getInt("t2t");
        t3p = jsonObject.getInt("t3p");
        t3t = jsonObject.getInt("t3t");
        y0 = jsonObject.getBoolean("y0");
        y1 = jsonObject.getBoolean("y1");
        y2 = jsonObject.getBoolean("y2");
        y3 = jsonObject.getBoolean("y3");
        yn0 = jsonObject.getInt("yn0");
        yn1 = jsonObject.getInt("yn1");
        yn2 = jsonObject.getInt("yn2");
        yn3 = jsonObject.getInt("yn3");
        yg0 = jsonObject.getInt("yg0");
        yg1 = jsonObject.getInt("yg1");
        yg2 = jsonObject.getInt("yg2");
        yg3 = jsonObject.getInt("yg3");
        vd = jsonObject.getBoolean("vd");
        va = jsonObject.getBoolean("va");
        vx = jsonObject.getBoolean("vx");
        vb = jsonObject.getBoolean("vb");
        vc = jsonObject.getBoolean("vc");
        ve = jsonObject.getBoolean("ve");
        vg = jsonObject.getBoolean("vg");
        vh = jsonObject.getBoolean("vh");
        vm = jsonObject.getBoolean("vm");
        vt = jsonObject.getBoolean("vt");
        vy = jsonObject.getBoolean("vy");
        vp = jsonObject.getBoolean("vp");
        vl = jsonObject.getBoolean("vl");
        vs = jsonObject.getBoolean("vs");
        pd = jsonObject.getBoolean("pd");
        ai = jsonObject.getBoolean("ai");
        av = jsonObject.getBoolean("av");
        aa = jsonObject.getDouble("aa");
        ad = jsonObject.getDouble("ad");
        ac = jsonObject.getInt("ac");
        lc = jsonObject.getInt("lc");
        lr = jsonObject.getDouble("lr");
        sr = jsonObject.getInt("sr");
        mt = jsonObject.getDouble("mt");
        md = jsonObject.getDouble("md");
        ma = jsonObject.getDouble("ma");
        mo = jsonObject.getDouble("mo");
        mg = jsonObject.getDouble("mg");
        mk = jsonObject.getDouble("mk");
        oop = jsonObject.getDouble("oop");     // targetOrientationPitch;
        ooq = jsonObject.getDouble("ooq");     // targetOrientationPitchDelta;
        oor = jsonObject.getDouble("oor");     // targetOrientationRoll;
        oos = jsonObject.getDouble("oos");     // targetOrientationRollDelta;
        opv = jsonObject.getDouble("opv");     // targetPitchVelocity;
        opw = jsonObject.getDouble("opw");     // targetPitchVelocityDelta;
        orv = jsonObject.getDouble("orv");     // targetRollVelocity;
        orw = jsonObject.getDouble("orw");     // targetRollVelocityDelta;
        oav = jsonObject.getDouble("oav");     // targetAltitudeVelocity;
        oaw = jsonObject.getDouble("oaw");     // targetAltitudeVelocityDelta;
        ohv = jsonObject.getDouble("ohv");     // targetHeadingVelocity;
        ohw = jsonObject.getDouble("ohw");     // targetHeadingVelocityDelta;
        ov = jsonObject.getDouble("ov");      // targetXYVelocity;
        lmc = jsonObject.getBoolean("lmc");
        lma = jsonObject.getDouble("lma");
        lmt = jsonObject.getInt("lmt");
        hsm = jsonObject.getBoolean("hsm");
        dpy = jsonObject.getBoolean("dpy");
        dba = jsonObject.getBoolean("dba");
        rm = HexUtils.fromHex(jsonObject.getString("rm"));
        tm = HexUtils.fromHex(jsonObject.getString("tm"));
        ias = jsonObject.getInt("ias");
        return this;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("type", type);
        jsonObject.put("std", std);
        jsonObject.put("st", st);
        jsonObject.put("sn", sn);
        jsonObject.put("fm", fm);
        jsonObject.put("ffm", ffm);
        jsonObject.put("fmf", fmf);
        jsonObject.put("dc", dc);
        jsonObject.put("dg", dg);
        jsonObject.put("df", df);
        jsonObject.put("dv", dv);
        jsonObject.put("dp", dp);
        jsonObject.put("dd", dd);
        jsonObject.put("da", da);
        jsonObject.put("dl", dl);
        jsonObject.put("dh", dh);
        jsonObject.put("de", de);
        jsonObject.put("kd", kd);
        jsonObject.put("kv", kv);
        jsonObject.put("ka", ka);
        jsonObject.put("kw", kw);
        jsonObject.put("kc", kc);
        jsonObject.put("jt", jt);
        jsonObject.put("jx", jx);
        jsonObject.put("jy", jy);
        jsonObject.put("jf", jf);
        jsonObject.put("jz", jz);
        jsonObject.put("jm", jm);
        jsonObject.put("jh", jh);
        jsonObject.put("js", js);
        jsonObject.put("jxf", jxf);
        jsonObject.put("jyf", jyf);
        jsonObject.put("jmf", jmf);
        jsonObject.put("jg", jg);
        jsonObject.put("jr", jr);
        jsonObject.put("ph", ph);
        jsonObject.put("phs", phs);
        jsonObject.put("phv", phv);
        jsonObject.put("pz", pz);
        jsonObject.put("pzs", pzs);
        jsonObject.put("pt", pt);
        jsonObject.put("pp", pp);
        jsonObject.put("pps", pps);
        jsonObject.put("pv", pv);
        jsonObject.put("pr", pr);
        jsonObject.put("prs", prs);
        jsonObject.put("ppv", ppv);
        jsonObject.put("prv", prv);
        jsonObject.put("pzv", pzv);
        jsonObject.put("pg", pg);
        jsonObject.put("pe", pe);
        jsonObject.put("bm", bm);
        jsonObject.put("b0", b0);
        jsonObject.put("b1", b1);
        jsonObject.put("b2", b2);
        jsonObject.put("b3", b3);
        jsonObject.put("ba", ba);
        jsonObject.put("c0", c0);
        jsonObject.put("c1", c1);
        jsonObject.put("c2", c2);
        jsonObject.put("c3", c3);
        jsonObject.put("ca", ca);
        jsonObject.put("s0o", s0o);
        jsonObject.put("s1o", s1o);
        jsonObject.put("s2o", s2o);
        jsonObject.put("s3o", s3o);
        jsonObject.put("s00", s00);
        jsonObject.put("s01", s01);
        jsonObject.put("s02", s02);
        jsonObject.put("s10", s10);
        jsonObject.put("s11", s11);
        jsonObject.put("s12", s12);
        jsonObject.put("s20", s20);
        jsonObject.put("s21", s21);
        jsonObject.put("s22", s22);
        jsonObject.put("s30", s30);
        jsonObject.put("s31", s31);
        jsonObject.put("s32", s32);
        jsonObject.put("et", et);
        jsonObject.put("e0o", e0o);
        jsonObject.put("e1o", e1o);
        jsonObject.put("e2o", e2o);
        jsonObject.put("e3o", e3o);
        jsonObject.put("e0", e0);
        jsonObject.put("e1", e1);
        jsonObject.put("e2", e2);
        jsonObject.put("e3", e3);
        jsonObject.put("ep0", ep0);
        jsonObject.put("ep1", ep1);
        jsonObject.put("ep2", ep2);
        jsonObject.put("ep3", ep3);
        jsonObject.put("ghg", ghg);
        jsonObject.put("gid", gid);
        jsonObject.put("gt", gt);
        jsonObject.put("gs", gs);
        jsonObject.put("gx", gx);
        jsonObject.put("gy", gy);
        jsonObject.put("gz", gz);
        jsonObject.put("ga", ga);
        jsonObject.put("gif", gif);
        jsonObject.put("glo", glo);
        jsonObject.put("gld", gld);
        jsonObject.put("t0m", t0m);
        jsonObject.put("t1m", t1m);
        jsonObject.put("t2m", t2m);
        jsonObject.put("t3m", t3m);
        jsonObject.put("t0x", t0x);
        jsonObject.put("t0y", t0y);
        jsonObject.put("t0z", t0z);
        jsonObject.put("t1x", t1x);
        jsonObject.put("t1y", t1y);
        jsonObject.put("t1z", t1z);
        jsonObject.put("t2x", t2x);
        jsonObject.put("t2y", t2y);
        jsonObject.put("t2z", t2z);
        jsonObject.put("t3x", t3x);
        jsonObject.put("t3y", t3y);
        jsonObject.put("t3z", t3z);
        jsonObject.put("t0o", t0o);
        jsonObject.put("t1o", t1o);
        jsonObject.put("t2o", t2o);
        jsonObject.put("t3o", t3o);
        jsonObject.put("t0p", t0p);
        jsonObject.put("t0t", t0t);
        jsonObject.put("t1p", t1p);
        jsonObject.put("t1t", t1t);
        jsonObject.put("t2p", t2p);
        jsonObject.put("t2t", t2t);
        jsonObject.put("t3p", t3p);
        jsonObject.put("t3t", t3t);
        jsonObject.put("y0", y0);
        jsonObject.put("y1", y1);
        jsonObject.put("y2", y2);
        jsonObject.put("y3", y3);
        jsonObject.put("yn0", yn0);
        jsonObject.put("yn1", yn1);
        jsonObject.put("yn2", yn2);
        jsonObject.put("yn3", yn3);
        jsonObject.put("yg0", yg0);
        jsonObject.put("yg1", yg1);
        jsonObject.put("yg2", yg2);
        jsonObject.put("yg3", yg3);
        jsonObject.put("vd", vd);
        jsonObject.put("va", va);
        jsonObject.put("vx", vx);
        jsonObject.put("vb", vb);
        jsonObject.put("vc", vc);
        jsonObject.put("ve", ve);
        jsonObject.put("vg", vg);
        jsonObject.put("vh", vh);
        jsonObject.put("vm", vm);
        jsonObject.put("vt", vt);
        jsonObject.put("vy", vy);
        jsonObject.put("vp", vp);
        jsonObject.put("vl", vl);
        jsonObject.put("vs", vs);
        jsonObject.put("pd", pd);
        jsonObject.put("par", par);
        jsonObject.put("ai", ai);
        jsonObject.put("av", av);
        jsonObject.put("aa", aa);
        jsonObject.put("ad", ad);
        jsonObject.put("ac", ac);
        jsonObject.put("lc", lc);
        jsonObject.put("lr", lr);
        jsonObject.put("sr", sr);
        jsonObject.put("mt", mt);
        jsonObject.put("md", md);
        jsonObject.put("ma", ma);
        jsonObject.put("mo", mo);
        jsonObject.put("mg", mg);
        jsonObject.put("mk", mk);
        jsonObject.put("oop", oop);     // targetOrientationPitch;
        jsonObject.put("ooq", ooq);     // targetOrientationPitchDelta;
        jsonObject.put("oor", oor);     // targetOrientationRoll;
        jsonObject.put("oos", oos);     // targetOrientationRollDelta;
        jsonObject.put("opv", opv);     // targetPitchVelocity;
        jsonObject.put("opw", opw);     // targetPitchVelocityDelta;
        jsonObject.put("orv", orv);     // targetRollVelocity;
        jsonObject.put("orw", orw);     // targetRollVelocityDelta;
        jsonObject.put("oav", oav);     // targetAltitudeVelocity;
        jsonObject.put("oaw", oaw);     // targetAltitudeVelocityDelta;
        jsonObject.put("ohv", ohv);     // targetHeadingVelocity;
        jsonObject.put("ohw", ohw);     // targetHeadingVelocityDelta;
        jsonObject.put("ov", ov);        // targetXYVelocity;
        jsonObject.put("lmc", lmc);
        jsonObject.put("lma", lma);
        jsonObject.put("lmt", lmt);
        jsonObject.put("hsm", hsm);
        jsonObject.put("dpy", dpy);
        jsonObject.put("dba", dba);
        jsonObject.put("rm", HexUtils.toHex(rm));
        jsonObject.put("tm", HexUtils.toHex(tm));
        jsonObject.put("ias", ias);
        return jsonObject;
    }

    /**
     * Get the heli 0 battery value
     *
     * @return the value
     */
    public double getB0() {
        return b0;
    }

    /**
     * Set the heli 0 battery value
     *
     * @param b0 the value
     */
    public void setB0(double b0) {
        this.b0 = b0;
    }

    /**
     * Get the heli 1 battery value
     *
     * @return the value
     */
    public double getB1() {
        return b1;
    }

    /**
     * Set the heli 1 battery value
     *
     * @param b1 the value
     */
    public void setB1(double b1) {
        this.b1 = b1;
    }

    /**
     * Get the heli 2 battery value
     *
     * @return the value
     */
    public double getB2() {
        return b2;
    }

    /**
     * Set the heli 2 battery value
     *
     * @param b2 the value
     */
    public void setB2(double b2) {
        this.b2 = b2;
    }

    /**
     * Get the heli 3 battery value
     *
     * @return the value
     */
    public double getB3() {
        return b3;
    }

    /**
     * Set the heli 3 battery value
     *
     * @param b3 the value
     */
    public void setB3(double b3) {
        this.b3 = b3;
    }

    /**
     * Get the auxiliary battery value
     *
     * @return the value
     */
    public double getBa() {
        return ba;
    }

    /**
     * Set the auxiliary battery value
     *
     * @param ba the value
     */
    public void setBa(double ba) {
        this.ba = ba;
    }

    /**
     * Get the main battery value
     *
     * @return the value
     */
    public double getBm() {
        return bm;
    }

    /**
     * Set the main battery value
     *
     * @param bm the value
     */
    public void setBm(double bm) {
        this.bm = bm;
    }

    /**
     * Get the heli 0 current value
     *
     * @return the value
     */
    public double getC0() {
        return c0;
    }

    /**
     * Set the heli 0 current value
     *
     * @param c0 the value
     */
    public void setC0(double c0) {
        this.c0 = c0;
    }

    /**
     * Get the heli 1 current value
     *
     * @return the value
     */
    public double getC1() {
        return c1;
    }

    /**
     * Set the heli 1 current value
     *
     * @param c1 the value
     */
    public void setC1(double c1) {
        this.c1 = c1;
    }

    /**
     * Get the heli 2 current value
     *
     * @return the value
     */
    public double getC2() {
        return c2;
    }

    /**
     * Set the heli 2 current value
     *
     * @param c2 the value
     */
    public void setC2(double c2) {
        this.c2 = c2;
    }

    /**
     * Set the heli 3 current value
     *
     * @return the value
     */
    public double getC3() {
        return c3;
    }

    /**
     * Set the heli 3 current value
     *
     * @param c3 the value
     */
    public void setC3(double c3) {
        this.c3 = c3;
    }

    /**
     * Get the auxiliary current value
     *
     * @return the value
     */
    public double getCa() {
        return ca;
    }

    /**
     * Set the auxiliary current value
     *
     * @param ca the value
     */
    public void setCa(double ca) {
        this.ca = ca;
    }

    /**
     * Is auxiliary device on
     *
     * @return true if on
     */
    public boolean isDa() {
        return da;
    }

    /**
     * Set auxiliary device on
     *
     * @param da true if on
     */
    public void setDa(boolean da) {
        this.da = da;
    }

    /**
     * Get the free memory in bytes
     *
     * @return the free memory in bytes
     */
    public int getFm() {
        return fm;
    }

    /**
     * Set the free memory in bytes
     *
     * @param fm the free memory in bytes
     */
    public void setFm(int fm) {
        this.fm = fm;
    }

    /**
     * Is camera (pan/tilt) device on
     *
     * @return true if on
     */
    public boolean isDc() {
        return dc;
    }

    /**
     * Set camera (pan/tilt) device on
     *
     * @param dc true if on
     */
    public void setDc(boolean dc) {
        this.dc = dc;
    }

    /**
     * Is designator device on
     *
     * @return true if on
     */
    public boolean isDd() {
        return dd;
    }

    /**
     * Set designator device on
     *
     * @param dd true if on
     */
    public void setDd(boolean dd) {
        this.dd = dd;
    }

    /**
     * Is gps device on
     *
     * @return true if on
     */
    public boolean isDg() {
        return dg;
    }

    /**
     * Set gps device on
     *
     * @param dg true if on
     */
    public void setDg(boolean dg) {
        this.dg = dg;
    }

    /**
     * Are helis on
     *
     * @return true if on
     */
    public boolean isDh() {
        return dh;
    }

    /**
     * Set helis on
     *
     * @param dh true if on
     */
    public void setDh(boolean dh) {
        this.dh = dh;
    }

    /**
     * Is parachute deployed
     *
     * @return true if deployed
     */
    public boolean isDe() {
        return de;
    }

    /**
     * Set parachute deployed
     *
     * @param de true if deployed
     */
    public void setDe(boolean de) {
        this.de = de;
    }

    /**
     * Is IMU on
     *
     * @return true if on
     */
    public boolean isDp() {
        return dp;
    }

    /**
     * Set IMU on
     *
     * @param dp true if on
     */
    public void setDp(boolean dp) {
        this.dp = dp;
    }

    /**
     * Get the esc 0 value
     *
     * @return the value
     */
    public double getE0() {
        return e0;
    }

    /**
     * Set the esc 0 value
     *
     * @param e0 the value
     */
    public void setE0(double e0) {
        this.e0 = e0;
    }

    /**
     * Is esc 0 on
     *
     * @return true if on
     */
    public boolean isE0o() {
        return e0o;
    }

    /**
     * Set esc 0 on
     *
     * @param e0o true if on
     */
    public void setE0o(boolean e0o) {
        this.e0o = e0o;
    }

    /**
     * Get esc 1 value
     *
     * @return the value
     */
    public double getE1() {
        return e1;
    }

    /**
     * Set esc 1 value
     *
     * @param e1 the value
     */
    public void setE1(double e1) {
        this.e1 = e1;
    }

    /**
     * Is esc 1 on
     *
     * @return true if on
     */
    public boolean isE1o() {
        return e1o;
    }

    /**
     * Set esc 1 on
     *
     * @param e1o true if on
     */
    public void setE1o(boolean e1o) {
        this.e1o = e1o;
    }

    /**
     * Get esc 2 value
     *
     * @return the value
     */
    public double getE2() {
        return e2;
    }

    /**
     * Set esc 2 value
     *
     * @param e2 the value
     */
    public void setE2(double e2) {
        this.e2 = e2;
    }

    /**
     * Is esc 2 on
     *
     * @return true if on
     */
    public boolean isE2o() {
        return e2o;
    }

    /**
     * Set esc 2 on
     *
     * @param e2o true if on
     */
    public void setE2o(boolean e2o) {
        this.e2o = e2o;
    }

    /**
     * Get the esc 3 value
     *
     * @return the value
     */
    public double getE3() {
        return e3;
    }

    /**
     * Set the esc 3 value
     *
     * @param e3 the value
     */
    public void setE3(double e3) {
        this.e3 = e3;
    }

    /**
     * Is esc 3 on
     *
     * @return true if on
     */
    public boolean isE3o() {
        return e3o;
    }

    /**
     * Set esc 3 on
     *
     * @param e3o true if on
     */
    public void setE3o(boolean e3o) {
        this.e3o = e3o;
    }

    /**
     * Get the esc type
     *
     * @return the esc type
     */
    public int getEt() {
        return et;
    }

    /**
     * Set the esc type
     *
     * @param et the esc type
     */
    public void setEt(int et) {
        this.et = et;
    }

    /**
     * Get the goal angle (in heading=compass degrees)
     *
     * @return the goal angle
     */
    public double getGa() {
        return ga;
    }

    /**
     * Set the goal angle (in heading=compass degrees)
     *
     * @param ga the goal angle
     */
    public void setGa(double ga) {
        this.ga = ga;
    }

    /**
     * Does the floid have a goal
     *
     * @return true if goal
     */
    public boolean isGhg() {
        return ghg;
    }

    /**
     * Set the floid has a goal
     *
     * @param ghg true if has a goal
     */
    public void setGhg(boolean ghg) {
        this.ghg = ghg;
    }

    /**
     * Get the goal id
     *
     * @return the goal id
     */
    public int getGid() {
        return gid;
    }

    /**
     * Set the goal id
     *
     * @param gid the goal id
     */
    public void setGid(int gid) {
        this.gid = gid;
    }

    /**
     * Is the floid in flight
     *
     * @return true if in flight
     */
    public boolean isGif() {
        return gif;
    }

    /**
     * Set the floid in flight
     *
     * @param gif true if in flight
     */
    public void setGif(boolean gif) {
        this.gif = gif;
    }

    /**
     * Is the floid in land mode
     *
     * @return true if in land mode
     */
    public boolean isGld() {
        return gld;
    }

    /**
     * Set the floid in land mode
     *
     * @param gld true if in land mode
     */
    public void setGld(boolean gld) {
        this.gld = gld;
    }

    /**
     * Is the floid in lift off mode
     *
     * @return true if in lift off mode
     */
    public boolean isGlo() {
        return glo;
    }

    /**
     * Set the floid in lift off mode
     *
     * @param glo true if in lift off mode
     */
    public void setGlo(boolean glo) {
        this.glo = glo;
    }

    /**
     * Get the goal state
     *
     * @return the goal state
     */
    public int getGs() {
        return gs;
    }

    /**
     * Set the goal state
     *
     * @param gs the goal state
     */
    public void setGs(int gs) {
        this.gs = gs;
    }

    /**
     * Get the goal ticks
     *
     * @return the goal ticks
     */
    public int getGt() {
        return gt;
    }

    /**
     * Set the goal ticks
     *
     * @param gt the goal ticks
     */
    public void setGt(int gt) {
        this.gt = gt;
    }

    /**
     * Get the goal longitude (x)
     *
     * @return the goal longitude
     */
    public double getGx() {
        return gx;
    }

    /**
     * Set the goal longitude (x)
     *
     * @param gx the goal longitude
     */
    public void setGx(double gx) {
        this.gx = gx;
    }

    /**
     * Get the goal latitude (y)
     *
     * @return the goal latitude
     */
    public double getGy() {
        return gy;
    }

    /**
     * Set the goal latitude (y)
     *
     * @param gy the goal latitude
     */
    public void setGy(double gy) {
        this.gy = gy;
    }

    /**
     * Get the goal altitude (z)
     *
     * @return the goal altitude
     */
    public double getGz() {
        return gz;
    }

    /**
     * Set the goal altitude (z)
     *
     * @param gz the goal altitude
     */
    public void setGz(double gz) {
        this.gz = gz;
    }

    /**
     * Get the gps fix quality
     *
     * @return the gps fix quality
     */
    public int getJf() {
        return jf;
    }

    /**
     * Set the gps fix quality
     *
     * @param jf the gps fix quality
     */
    public void setJf(int jf) {
        this.jf = jf;
    }

    /**
     * Get the gps HDOP
     *
     * @return the gps HDOP
     */
    public double getJh() {
        return jh;
    }

    /**
     * Set the gps HDOP
     *
     * @param jh the gps HDOP
     */
    public void setJh(double jh) {
        this.jh = jh;
    }

    /**
     * Get the gps altitude (z) in meters
     *
     * @return the gps altitude (z) in meters
     */
    public double getJm() {
        return jm;
    }

    /**
     * Set the gps altitude (z) in meters (does not automatically set jz
     *
     * @param jm the gps altitude (z) in meters
     */
    public void setJm(double jm) {
        this.jm = jm;
    }

    /**
     * Get the number of gps satellites
     *
     * @return the number of gps satellites
     */
    public int getJs() {
        return js;
    }

    /**
     * Set the number of gps satellites
     *
     * @param js the number of gps satellites
     */
    public void setJs(int js) {
        this.js = js;
    }

    /**
     * Get the gps time
     *
     * @return the gps time
     */
    public int getJt() {
        return jt;
    }

    /**
     * Set the gps time
     *
     * @param jt the gps time
     */
    public void setJt(int jt) {
        this.jt = jt;
    }

    /**
     * Get the gps longitude (x) in degrees decimal format
     *
     * @return the gps longitude (x) in degrees decimal format
     */
    public double getJx() {
        return jx;
    }

    /**
     * Set the gps longitude (x) in degrees decimal format
     *
     * @param jx the gps longitude (x) in degrees decimal format
     */
    public void setJx(double jx) {
        this.jx = jx;
    }

    /**
     * Get the gps latitude (y) in degrees decimal format
     *
     * @return the gps latitude (y) in degrees decimal format
     */
    public double getJy() {
        return jy;
    }

    /**
     * Set the gps latitude (y) in degrees decimal format
     *
     * @param jy the gps latitude (y) in degrees decimal format
     */
    public void setJy(double jy) {
        this.jy = jy;
    }

    /**
     * Get the gps altitude (z) in feet
     *
     * @return the gps altitude (z) in feet
     */
    public double getJz() {
        return jz;
    }

    /**
     * Set the gps altitude (z) in feet
     *
     * @param jz the gps altitude (z) in feet
     */
    public void setJz(double jz) {
        this.jz = jz;
    }

    /**
     * Get the gps rate
     *
     * @return the gps rate
     */
    public int getJr() {
        return jr;
    }

    /**
     * Set the gps rate
     *
     * @param jr the gps rate
     */
    public void setJr(int jr) {
        this.jr = jr;
    }

    /**
     * Get the calculated altitude from the altimeter (preferred) or the gps
     *
     * @return the calculated altitude from the altimeter (preferred) or the gps
     */
    public double getKa() {
        return ka;
    }

    /**
     * Set the calculated altitude from the altimeter (preferred) or the gps
     *
     * @param ka the calculated altitude from the altimeter (preferred) or the gps
     */
    public void setKa(double ka) {
        this.ka = ka;
    }

    /**
     * Get the direction over ground (std trig degrees)
     *
     * @return the direction over ground (std trig degrees)
     */
    public double getKd() {
        return kd;
    }

    /**
     * Set the direction over ground (std trig degrees)
     *
     * @param kd the direction over ground (std trig degrees)
     */
    public void setKd(double kd) {
        this.kd = kd;
    }

    /**
     * Get the velocity over ground (m/s)
     *
     * @return the velocity over ground (m/s)
     */
    public double getKv() {
        return kv;
    }

    /**
     * Set the velocity over ground (m/s)
     *
     * @param kv the velocity over ground m/s
     */
    public void setKv(double kv) {
        this.kv = kv;
    }

    /**
     * Get the imu heading (compass degrees)
     *
     * @return the imu heading (compass degrees)
     */
    public double getPh() {
        return ph;
    }

    /**
     * Set the imu heading (compass degrees)
     *
     * @param ph the imu heading (compass degrees)
     */
    public void setPh(double ph) {
        this.ph = ph;
    }

    /**
     * Get the altitude velocity (m/s)
     *
     * @return the altitude velocity (m/s)
     */
    public double getPzv() {
        return pzv;
    }

    /**
     * Set the altitude velocity (m/s)
     *
     * @param pzv the altitude velocity (m/s)
     */
    public void setPzv(double pzv) {
        this.pzv = pzv;
    }

    /**
     * Get the imu pitch (degrees)
     *
     * @return the imu pitch (degrees)
     */
    public double getPp() {
        return pp;
    }

    /**
     * Set the imu pitch (degrees)
     *
     * @param pp the imu pitch (degrees)
     */
    public void setPp(double pp) {
        this.pp = pp;
    }

    /**
     * Get the imu pitch velocity (degrees/s)
     *
     * @return the imu pitch velocity (degrees/s)
     */
    public double getPpv() {
        return ppv;
    }

    /**
     * Set the imu pitch velocity (degrees/s)
     *
     * @param ppv the imu pitch velocity (degrees/s)
     */
    public void setPpv(double ppv) {
        this.ppv = ppv;
    }

    /**
     * Get the imu roll (degrees)
     *
     * @return the imu roll (degrees)
     */
    public double getPr() {
        return pr;
    }

    /**
     * Set the imu roll (degrees)
     *
     * @param pr the imu roll (degrees)
     */
    public void setPr(double pr) {
        this.pr = pr;
    }

    /**
     * Get the imu roll velocity (degrees/s)
     *
     * @return the imu roll velocity (degrees/s)
     */
    public double getPrv() {
        return prv;
    }

    /**
     * Set the imu roll velocity (degrees/s)
     *
     * @param prv the imu roll velocity (degrees/s)
     */
    public void setPrv(double prv) {
        this.prv = prv;
    }

    /**
     * Get the temperature (Celsius)
     *
     * @return the temperature (Celsius)
     */
    public double getPt() {
        return pt;
    }

    /**
     * Set the temperature (celsius)
     *
     * @param pt the temperature (celsius)
     */
    public void setPt(double pt) {
        this.pt = pt;
    }

    /**
     * Get imu height (m) - sea level from pressure
     *
     * @return the imu height (m) - sea level from pressure
     */
    public double getPz() {
        return pz;
    }

    /**
     * Set the imu height (m) - sea level from pressure
     *
     * @param pz the imu height (m) - sea level from pressure
     */
    public void setPz(double pz) {
        this.pz = pz;
    }

    /**
     * Get servo 0-0 aka heli-0,L value
     *
     * @return servo 0-0 aka heli-0,L value
     */
    public double getS00() {
        return s00;
    }

    /**
     * Set servo 0-0 aka heli-0,L value
     *
     * @param s00 servo 0-0 aka heli-0,L value
     */
    public void setS00(double s00) {
        this.s00 = s00;
    }

    /**
     * Get servo 0-1 aka heli-0,R value
     *
     * @return servo 0-1 aka heli-0,R value
     */
    public double getS01() {
        return s01;
    }

    /**
     * Set servo 0-1 aka heli-0,R value
     *
     * @param s01 servo 0-1 aka heli-0,R value
     */
    public void setS01(double s01) {
        this.s01 = s01;
    }

    /**
     * Get servo 0-2 aka heli-0,P value
     *
     * @return servo 0-2 aka heli-0,P value
     */
    public double getS02() {
        return s02;
    }

    /**
     * Set servo 0-2 aka heli-0,P value
     *
     * @param s02 servo 0-2 aka heli-0,P value
     */
    public void setS02(double s02) {
        this.s02 = s02;
    }

    /**
     * Are heli 0 servos on
     *
     * @return true if heli 0 servos are on
     */
    public boolean isS0o() {
        return s0o;
    }

    /**
     * Set heli 0 servos on
     *
     * @param s0o true if heli 0 servos on
     */
    public void setS0o(boolean s0o) {
        this.s0o = s0o;
    }

    /**
     * Get servo 1-0 aka heli-1,L value
     *
     * @return servo 1-0 aka heli-1,L value
     */
    public double getS10() {
        return s10;
    }

    /**
     * Set servo 1-0 aka heli-1,L value
     *
     * @param s10 servo 1-0 aka heli-1,L value
     */
    public void setS10(double s10) {
        this.s10 = s10;
    }

    /**
     * Get servo 1-1 aka heli-1,R value
     *
     * @return servo 1-1 aka heli-1,R value
     */
    public double getS11() {
        return s11;
    }

    /**
     * Set servo 1-1 aka heli-1,R value
     *
     * @param s11 servo 1-1 aka heli-1,R value
     */
    public void setS11(double s11) {
        this.s11 = s11;
    }

    /**
     * Get servo 1-2 aka heli-1,P value
     *
     * @return servo 1-2 aka heli-1,P value
     */
    public double getS12() {
        return s12;
    }

    /**
     * Set servo 1-2 aka heli-1,P value
     *
     * @param s12 servo 1-2 aka heli-1,P value
     */
    public void setS12(double s12) {
        this.s12 = s12;
    }

    /**
     * Are heli 2 servos on
     *
     * @return true if heli 2 servos are on
     */
    public boolean isS1o() {
        return s1o;
    }

    /**
     * Set heli 2 servos on
     *
     * @param s1o true if heli 2 servos on
     */
    public void setS1o(boolean s1o) {
        this.s1o = s1o;
    }

    /**
     * Get servo 2-0 aka heli-2,L value
     *
     * @return servo 2-0 aka heli-2,L value
     */
    public double getS20() {
        return s20;
    }

    /**
     * Set servo 2-0 aka heli-2,L value
     *
     * @param s20 servo 2-0 aka heli-2,L value
     */
    public void setS20(double s20) {
        this.s20 = s20;
    }

    /**
     * Get servo 2-1 aka heli-2,R value
     *
     * @return servo 2-1 aka heli-2,R value
     */
    public double getS21() {
        return s21;
    }

    /**
     * Set servo 2-1 aka heli-2,R value
     *
     * @param s21 servo 2-1 aka heli-2,R value
     */
    public void setS21(double s21) {
        this.s21 = s21;
    }

    /**
     * Get servo 2-2 aka heli-2,P value
     *
     * @return servo 2-2 aka heli-2,P value
     */
    public double getS22() {
        return s22;
    }

    /**
     * Set servo 2-2 aka heli-2,P value
     *
     * @param s22 servo 2-2 aka heli-2,P value
     */
    public void setS22(double s22) {
        this.s22 = s22;
    }

    /**
     * Are heli 2 servos on
     *
     * @return true if heli 2 servos are on
     */
    public boolean isS2o() {
        return s2o;
    }

    /**
     * Set heli 2 servos on
     *
     * @param s2o true if heli 2 servos on
     */
    public void setS2o(boolean s2o) {
        this.s2o = s2o;
    }

    /**
     * Get servo 3-0 aka heli-3,L value
     *
     * @return servo 3-0 aka heli-3,L value
     */
    public double getS30() {
        return s30;
    }

    /**
     * Set servo 3-0 aka heli-3,L value
     *
     * @param s30 servo 3-0 aka heli-3,L value
     */
    public void setS30(double s30) {
        this.s30 = s30;
    }

    /**
     * Get servo 3-1 aka heli-3,R value
     *
     * @return servo 3-1 aka heli-3,R value
     */
    public double getS31() {
        return s31;
    }

    /**
     * Set servo 3-1 aka heli-3,R value
     *
     * @param s31 servo 3-1 aka heli-3,R value
     */
    public void setS31(double s31) {
        this.s31 = s31;
    }

    /**
     * Get servo 3-2 aka heli-3,P value
     *
     * @return servo 3-2 aka heli-3,P value
     */
    public double getS32() {
        return s32;
    }

    /**
     * Set servo 3-2 aka heli-3,P value
     *
     * @param s32 servo 3-2 aka heli-3,P value
     */
    public void setS32(double s32) {
        this.s32 = s32;
    }

    /**
     * Are heli 3 servos on
     *
     * @return true if heli 3 servos are on
     */
    public boolean isS3o() {
        return s3o;
    }

    /**
     * Set heli 3 servos on
     *
     * @param s3o true if heli 3 servos on
     */
    public void setS3o(boolean s3o) {
        this.s3o = s3o;
    }

    /**
     * Get the floid status time
     *
     * @return the status time
     */
    public int getSt() {
        return st;
    }

    /**
     * Get the droid status time
     *
     * @return the droid status time
     */
    public long getStd() {
        return std;
    }

    /**
     * Set the floid status time
     *
     * @param st the floid status time
     */
    public void setSt(int st) {
        this.st = st;
    }

    /**
     * Set the droid status time
     *
     * @param std the droid status time
     */
    public void setStd(long std) {
        this.std = std;
    }

    /**
     * Get the status number
     *
     * @return the status number
     */
    public int getSn() {
        return sn;
    }

    /**
     * Set the status number
     *
     * @param sn the status number
     */
    public void setSn(int sn) {
        this.sn = sn;
    }

    /**
     * Get the pan/tilt 0 mode
     *
     * @return the pan/tilt 0 mode
     */
    public int getT0m() {
        return t0m;
    }

    /**
     * Set the pan/tilt 0 mode
     *
     * @param t0m the pan/tilt 0 mode
     */
    public void setT0m(int t0m) {
        this.t0m = t0m;
    }

    /**
     * Is pan/tilt 0 on
     *
     * @return true if pan/tilt 0 on
     */
    public boolean isT0o() {
        return t0o;
    }

    /**
     * Set pan/tilt 0 on
     *
     * @param t0o pan/tilt 0 on
     */
    public void setT0o(boolean t0o) {
        this.t0o = t0o;
    }

    /**
     * Get the pan/tilt 0 value
     *
     * @return the pan/tilt 0 value
     */
    public int getT0p() {
        return t0p;
    }

    /**
     * Set the pan/tilt 0 value
     *
     * @param t0p the pan/tilt 0 value
     */
    public void setT0p(int t0p) {
        this.t0p = t0p;
    }

    /**
     * Get the pan/tilt 0 tilt value
     *
     * @return the pan/tilt 0 tilt value
     */
    public int getT0t() {
        return t0t;
    }

    /**
     * Set the pan/tilt 0 tilt value
     *
     * @param t0t the pan/tilt 0 tilt value
     */
    public void setT0t(int t0t) {
        this.t0t = t0t;
    }

    /**
     * Get the pan/tilt 0 target x (longitude)
     *
     * @return the pan/tilt 0 target x (longitude)
     */
    public double getT0x() {
        return t0x;
    }

    /**
     * Set the pan/tilt 0 target x (longitude)
     *
     * @param t0x the pan/tilt 0 target x (longitude)
     */
    public void setT0x(double t0x) {
        this.t0x = t0x;
    }

    /**
     * Get the pan/tilt 0 target y (latitude)
     *
     * @return the pan/tilt 0 target y (latitude)
     */
    public double getT0y() {
        return t0y;
    }

    /**
     * Set the pan/tilt 0 target y (latitude)
     *
     * @param t0y the pan/tilt 0 target y (latitude)
     */
    public void setT0y(double t0y) {
        this.t0y = t0y;
    }

    /**
     * Get the pan/tilt 0 target z (altitude)
     *
     * @return the pan/tilt 0 target z (altitude)
     */
    public double getT0z() {
        return t0z;
    }

    /**
     * Set the pan/tilt 0 target z (altitude)
     *
     * @param t0z the pan/tilt 0 target z (altitude)
     */
    public void setT0z(double t0z) {
        this.t0z = t0z;
    }

    /**
     * Get the pan/tilt 1 mode
     *
     * @return the pan/tilt 1 mode
     */
    public int getT1m() {
        return t1m;
    }

    /**
     * Set the pan/tilt 1 mode
     *
     * @param t1m the pan/tilt 1 mode
     */
    public void setT1m(int t1m) {
        this.t1m = t1m;
    }

    /**
     * Is pan/tilt 1 on
     *
     * @return true if pan/tilt 1 on
     */
    public boolean isT1o() {
        return t1o;
    }

    /**
     * Set pan/tilt 1 on
     *
     * @param t1o pan/tilt 1 on
     */
    public void setT1o(boolean t1o) {
        this.t1o = t1o;
    }

    /**
     * Get the pan/tilt 1 pan value
     *
     * @return the pan/tilt 1 pan value
     */
    public int getT1p() {
        return t1p;
    }

    /**
     * Set the pan/tilt 1 pan value
     *
     * @param t1p the pan/tilt 1 pan value
     */
    public void setT1p(int t1p) {
        this.t1p = t1p;
    }

    /**
     * Get the pan/tilt 1 tilt value
     *
     * @return the pan/tilt 1 tilt value
     */
    public int getT1t() {
        return t1t;
    }

    /**
     * Set the pan/tilt 1 tilt value
     *
     * @param t1t the pan/tilt 1 tilt value
     */
    public void setT1t(int t1t) {
        this.t1t = t1t;
    }

    /**
     * Get the pan/tilt 1 target x (longitude)
     *
     * @return the pan/tilt 1 target x (longitude)
     */
    public double getT1x() {
        return t1x;
    }

    /**
     * Set the pan/tilt 1 target x (longitude)
     *
     * @param t1x the pan/tilt 1 target x (longitude)
     */
    public void setT1x(double t1x) {
        this.t1x = t1x;
    }

    /**
     * Get the pan/tilt 1 target y (latitude)
     *
     * @return the pan/tilt 1 target y (latitude)
     */
    public double getT1y() {
        return t1y;
    }

    /**
     * Set the pan/tilt 1 target y (latitude)
     *
     * @param t1y the pan/tilt 1 target y (latitude)
     */
    public void setT1y(double t1y) {
        this.t1y = t1y;
    }

    /**
     * Get the pan/tilt 1 target z (altitude)
     *
     * @return the pan/tilt 1 target z (altitude)
     */
    public double getT1z() {
        return t1z;
    }

    /**
     * Set the pan/tilt 1 target z (altitude)
     *
     * @param t1z the pan/tilt 1 target z (altitude)
     */
    public void setT1z(double t1z) {
        this.t1z = t1z;
    }

    /**
     * Get the pan/tilt 2 mode
     *
     * @return the pan/tilt 2 mode
     */
    public int getT2m() {
        return t2m;
    }

    /**
     * Set the pan/tilt 2 mode
     *
     * @param t2m the pan/tilt 2 mode
     */
    public void setT2m(int t2m) {
        this.t2m = t2m;
    }

    /**
     * Is pan/tilt 2 on
     *
     * @return true if pan/tilt 2 on
     */
    public boolean isT2o() {
        return t2o;
    }

    /**
     * Set pan/tilt 2 on
     *
     * @param t2o pan/tilt 2 on
     */
    public void setT2o(boolean t2o) {
        this.t2o = t2o;
    }

    /**
     * Get the pan/tilt 2 pan value
     *
     * @return the pan/tilt 2 pan value
     */
    public int getT2p() {
        return t2p;
    }

    /**
     * Set the pan/tilt 2 pan value
     *
     * @param t2p the pan/tilt 2 pan value
     */
    public void setT2p(int t2p) {
        this.t2p = t2p;
    }

    /**
     * Get the pan/tilt 2 tilt value
     *
     * @return the pan/tilt 2 tilt value
     */
    public int getT2t() {
        return t2t;
    }

    /**
     * Set the pan/tilt 2 tilt value
     *
     * @param t2t the pan/tilt 2 tilt value
     */
    public void setT2t(int t2t) {
        this.t2t = t2t;
    }

    /**
     * Get the pan/tilt 2 target x (longitude)
     *
     * @return the pan/tilt 2 target x (longitude)
     */
    public double getT2x() {
        return t2x;
    }

    /**
     * Set the pan/tilt 2 target x (longitude)
     *
     * @param t2x the pan/tilt 2 target x (longitude)
     */
    public void setT2x(double t2x) {
        this.t2x = t2x;
    }

    /**
     * Get the pan/tilt 2 target y (latitude)
     *
     * @return the pan/tilt 2 target y (latitude)
     */
    public double getT2y() {
        return t2y;
    }

    /**
     * Set the pan/tilt 2 target y (latitude)
     *
     * @param t2y the pan/tilt 2 target y (latitude)
     */
    public void setT2y(double t2y) {
        this.t2y = t2y;
    }

    /**
     * Get the pan/tilt 2 target z (altitude)
     *
     * @return the pan/tilt 2 target z (altitude)
     */
    public double getT2z() {
        return t2z;
    }

    /**
     * Set the pan/tilt 2 target z (altitude)
     *
     * @param t2z the pan/tilt 2 target z (altitude)
     */
    public void setT2z(double t2z) {
        this.t2z = t2z;
    }

    /**
     * Get the pan/tilt 3 mode
     *
     * @return the pan/tilt 3 mode
     */
    public int getT3m() {
        return t3m;
    }

    /**
     * Set the pan/tilt 3 mode
     *
     * @param t3m the pan/tilt 3 mode
     */
    public void setT3m(int t3m) {
        this.t3m = t3m;
    }

    /**
     * Is pan/tilt 3 on
     *
     * @return true if pan/tilt 3 on
     */
    public boolean isT3o() {
        return t3o;
    }

    /**
     * Set pan/tilt 3 on
     *
     * @param t3o pan/tilt 3 on
     */
    public void setT3o(boolean t3o) {
        this.t3o = t3o;
    }

    /**
     * Get the pan/tilt 3 pan value
     *
     * @return the pan/tilt 3 pan value
     */
    public int getT3p() {
        return t3p;
    }

    /**
     * Set the pan/tilt 3 pan value
     *
     * @param t3p the pan/tilt 3 pan value
     */
    public void setT3p(int t3p) {
        this.t3p = t3p;
    }

    /**
     * Get the pan/tilt 3 tilt value
     *
     * @return the pan/tilt 3 tilt value
     */
    public int getT3t() {
        return t3t;
    }

    /**
     * Set the pan/tilt 3 tilt value
     *
     * @param t3t the pan/tilt 3 tilt value
     */
    public void setT3t(int t3t) {
        this.t3t = t3t;
    }

    /**
     * Get the pan/tilt 3 target x (longitude)
     *
     * @return the pan/tilt 3 target x (longitude)
     */
    public double getT3x() {
        return t3x;
    }

    /**
     * Set the pan/tilt 3 target x (longitude)
     *
     * @param t3x the pan/tilt 3 target x (longitude)
     */
    public void setT3x(double t3x) {
        this.t3x = t3x;
    }

    /**
     * Get the pan/tilt 3 target y (latitude)
     *
     * @return the pan/tilt 3 target y (latitude)
     */
    public double getT3y() {
        return t3y;
    }

    /**
     * Set the pan/tilt 3 target y (latitude)
     *
     * @param t3y the pan/tilt 3 target y (latitude)
     */
    public void setT3y(double t3y) {
        this.t3y = t3y;
    }

    /**
     * Get the pan/tilt 3 target y (latitude)
     *
     * @return the pan/tilt 3 target y (latitude)
     */
    public double getT3z() {
        return t3z;
    }

    /**
     * Set the pan/tilt 3 target z (altitude)
     *
     * @param t3z the pan/tilt 3 target z (altitude)
     */
    public void setT3z(double t3z) {
        this.t3z = t3z;
    }

    /**
     * Is bay 0 open
     *
     * @return true if bay 0 open
     */
    public boolean isY0() {
        return y0;
    }

    /**
     * Set bay 0 open
     *
     * @param y0 true if bay 0 is open
     */
    public void setY0(boolean y0) {
        this.y0 = y0;
    }

    /**
     * Is bay 1 open
     *
     * @return true if bay 1 open
     */
    public boolean isY1() {
        return y1;
    }

    /**
     * Set bay 1 open
     *
     * @param y1 true if bay 1 is open
     */
    public void setY1(boolean y1) {
        this.y1 = y1;
    }

    /**
     * Is bay 2 open
     *
     * @return true if bay 2 open
     */
    public boolean isY2() {
        return y2;
    }

    /**
     * Set bay 2 open
     *
     * @param y2 true if bay 2 is open
     */
    public void setY2(boolean y2) {
        this.y2 = y2;
    }

    /**
     * Is bay 3 open
     *
     * @return true if bay 3 open
     */
    public boolean isY3() {
        return y3;
    }

    /**
     * Set bay 3 open
     *
     * @param y3 true if bay 3 is open
     */
    public void setY3(boolean y3) {
        this.y3 = y3;
    }

    /**
     * Get the payload 0 goal state
     *
     * @return the payload 0 goal state
     */
    public int getYg0() {
        return yg0;
    }

    /**
     * Set the payload 0 goal state
     *
     * @param yg0 the payload 0 goal state
     */
    public void setYg0(int yg0) {
        this.yg0 = yg0;
    }

    /**
     * Get the payload 1 goal state
     *
     * @return the payload 1 goal state
     */
    public int getYg1() {
        return yg1;
    }

    /**
     * Set the payload 1 goal state
     *
     * @param yg1 the payload 1 goal state
     */
    public void setYg1(int yg1) {
        this.yg1 = yg1;
    }

    /**
     * Get the payload 2 goal state
     *
     * @return the payload 2 goal state
     */
    public int getYg2() {
        return yg2;
    }

    /**
     * Set the payload 2 goal state
     *
     * @param yg2 the payload 2 goal state
     */
    public void setYg2(int yg2) {
        this.yg2 = yg2;
    }

    /**
     * Get the payload 3 goal state
     *
     * @return the payload 3 goal state
     */
    public int getYg3() {
        return yg3;
    }

    /**
     * Set the payload 3 goal state
     *
     * @param yg3 the payload 3 goal state
     */
    public void setYg3(int yg3) {
        this.yg3 = yg3;
    }

    /**
     * Get the nm0 value
     *
     * @return the nm0 value
     */
    public int getYn0() {
        return yn0;
    }

    /**
     * Set the nm0 value
     *
     * @param yn0 the nm0 value
     */
    public void setYn0(int yn0) {
        this.yn0 = yn0;
    }

    /**
     * Get the nm1 value
     *
     * @return the nm1 value
     */
    public int getYn1() {
        return yn1;
    }

    /**
     * Set the nm1 value
     *
     * @param yn1 the nm1 value
     */
    public void setYn1(int yn1) {
        this.yn1 = yn1;
    }

    /**
     * Get the nm2 value
     *
     * @return the nm2 value
     */
    public int getYn2() {
        return yn2;
    }

    /**
     * Set the nm2 value
     *
     * @param yn2 the nm2 value
     */
    public void setYn2(int yn2) {
        this.yn2 = yn2;
    }

    /**
     * Get the nm3 value
     *
     * @return the nm3 value
     */
    public int getYn3() {
        return yn3;
    }

    /**
     * Set the nm3 value
     *
     * @param yn3 the nm3 value
     */
    public void setYn3(int yn3) {
        this.yn3 = yn3;
    }

    /**
     * Is led on
     *
     * @return true if led on
     */
    public boolean isDl() {
        return dl;
    }

    /**
     * Set led on
     *
     * @param dl true if led on
     */
    public void setDl(boolean dl) {
        this.dl = dl;
    }

    /**
     * Get the imu analog rate
     *
     * @return the imu analog rate
     */
    public int getPar() {
        return par;
    }

    /**
     * Set the imu analog rate
     *
     * @param par the imu analog rate
     */
    public void setPar(int par) {
        this.par = par;
    }

    /**
     * Is the imu in digital mode
     *
     * @return true if imu is in digital mode
     */
    public boolean isPd() {
        return pd;
    }

    /**
     * Set the imu in digital mode
     *
     * @param pd true if imu is in digital mode
     */
    public void setPd(boolean pd) {
        this.pd = pd;
    }

    /**
     * Is all debug on
     *
     * @return true if all debug is on
     */
    public boolean isVa() {
        return va;
    }

    /**
     * Set all debug on
     *
     * @param va true if all debug is on
     */
    public void setVa(boolean va) {
        this.va = va;
    }

    /**
     * Is debug payloads on
     *
     * @return true if debug payloads is on
     */
    public boolean isVb() {
        return vb;
    }

    /**
     * Set debug payloads on
     *
     * @param vb true if debug payloads is on
     */
    public void setVb(boolean vb) {
        this.vb = vb;
    }

    /**
     * Is debug camera (pan/tilt) on
     *
     * @return true if debug camera (pan/tilt) is on
     */
    public boolean isVc() {
        return vc;
    }

    /**
     * Set debug camera (pan/tilt) on
     *
     * @param vc true if debug camera (pan/tilt) is on
     */
    public void setVc(boolean vc) {
        this.vc = vc;
    }

    /**
     * Is debug on
     *
     * @return true if debug is on
     */
    public boolean isVd() {
        return vd;
    }

    /**
     * Set debug on
     *
     * @param vd true if debug is on
     */
    public void setVd(boolean vd) {
        this.vd = vd;
    }

    /**
     * Is debug designator on
     *
     * @return true if debug designator is on
     */
    public boolean isVe() {
        return ve;
    }

    /**
     * Set debug designator on
     *
     * @param ve true if debug designator is on
     */
    public void setVe(boolean ve) {
        this.ve = ve;
    }

    /**
     * Is debug gps on
     *
     * @return true if debug gps is on
     */
    public boolean isVg() {
        return vg;
    }

    /**
     * Set debug gps on
     *
     * @param vg true if debug gps is on
     */
    public void setVg(boolean vg) {
        this.vg = vg;
    }

    /**
     * Is debug helis on
     *
     * @return true if debug helis is on
     */
    public boolean isVh() {
        return vh;
    }

    /**
     * Set debug helis on
     *
     * @param vh true if debug helis is on
     */
    public void setVh(boolean vh) {
        this.vh = vh;
    }

    /**
     * Is debug memory on
     *
     * @return true if debug memory is on
     */
    public boolean isVm() {
        return vm;
    }

    /**
     * Set debug memory on
     *
     * @param vm true if debug memory is on
     */
    public void setVm(boolean vm) {
        this.vm = vm;
    }

    /**
     * Is debug imu on
     *
     * @return true if debug imu is on
     */
    public boolean isVp() {
        return vp;
    }

    /**
     * Is debug imu on
     *
     * @param vp true if debug imu is on
     */
    public void setVp(boolean vp) {
        this.vp = vp;
    }

    /**
     * Is debug pan/tilt on
     *
     * @return true if debug pan/tilt is on
     */
    public boolean isVt() {
        return vt;
    }

    /**
     * Set debug pan/tilt on
     *
     * @param vt true if debug pan/tilt is on
     */
    public void setVt(boolean vt) {
        this.vt = vt;
    }

    /**
     * Is debug aux on
     *
     * @return true if debug aux is on
     */
    public boolean isVx() {
        return vx;
    }

    /**
     * Set debug aux on
     *
     * @param vx true if debug aux is on
     */
    public void setVx(boolean vx) {
        this.vx = vx;
    }

    /**
     * Is debug physics on
     *
     * @return true if debug physics is on
     */
    public boolean isVy() {
        return vy;
    }

    /**
     * Set debug physics on
     *
     * @param vy true if debug physics is on
     */
    public void setVy(boolean vy) {
        this.vy = vy;
    }

    /**
     * Get the free memory in bytes
     *
     * @return the free memory in bytes
     */
    public int getFfm() {
        return ffm;
    }

    /**
     * Set the free memory in bytes
     *
     * @param ffm the free memory in bytes
     */
    public void setFfm(int ffm) {
        this.ffm = ffm;
    }

    /**
     * Get the imu pitch smoothed
     *
     * @return the imu pitch smoothed
     */
    public double getPps() {
        return pps;
    }

    /**
     * Set the imu pitch smoothed
     *
     * @param pps the imu pitch smoothed
     */
    public void setPps(double pps) {
        this.pps = pps;
    }

    /**
     * Get the imu roll smoothed
     *
     * @return the imu roll smoothed
     */
    public double getPrs() {
        return prs;
    }

    /**
     * Set the imu roll smoothed
     *
     * @param prs the imu roll smoothed
     */
    public void setPrs(double prs) {
        this.prs = prs;
    }

    /**
     * Get the imu altitude (m) smoothed
     *
     * @return the imu altitude (m) smoothed
     */
    public double getPzs() {
        return pzs;
    }

    /**
     * Set the imu altitude (m) smoothed
     *
     * @param pzs the imu altitude (m) smoothed
     */
    public void setPzs(double pzs) {
        this.pzs = pzs;
    }

    /**
     * Get the imu heading (compass degrees) smoothed
     *
     * @return the imu heading (compass degrees) smoothed
     */
    public double getPhs() {
        return phs;
    }

    /**
     * Set the imu heading (std trig angles - NOT compass degrees) smoothed
     *
     * @param phs the imu heading (std trig angles - NOT compass degrees) smoothed
     */
    public void setPhs(double phs) {
        this.phs = phs;
    }

    /**
     * Get the imu heading velocity (m/s)
     *
     * @return the imu heading velocity (compass degrees/s)
     */
    public double getPhv() {
        return phv;
    }

    /**
     * Set the imu heading velocity (m/s)
     *
     * @param phv the imu heading velocity (compass degrees/s)
     */
    public void setPhv(double phv) {
        this.phv = phv;
    }

    /**
     * Get the altimeter altitude (m)
     *
     * @return the altimeter altitude (m)
     */
    public double getAa() {
        return aa;
    }

    /**
     * Set the altimeter altitude (m)
     *
     * @param aa the altimeter altitude (m)
     */
    public void setAa(double aa) {
        this.aa = aa;
    }

    /**
     * Get the altimeter good reading (continuously good) count
     *
     * @return the altimeter good reading (continuously good) count
     */
    public int getAc() {
        return ac;
    }

    /**
     * Set the altimeter good reading (continuously good) count
     *
     * @param ac the altimeter good reading (continuously good) count
     */
    public void setAc(int ac) {
        this.ac = ac;
    }

    /**
     * Get the altimeter delta (from barometer to gps) (m)
     *
     * @return the altimeter delta (from barometer to gps) (m)
     */
    public double getAd() {
        return ad;
    }

    /**
     * Set the altimeter delta (from barometer to gps) (m)
     *
     * @param ad the altimeter delta (from barometer to gps) (m)
     */
    public void setAd(double ad) {
        this.ad = ad;
    }

    /**
     * Is the altimeter initialized
     *
     * @return true if the altimeter is initialized
     */
    public boolean isAi() {
        return ai;
    }

    /**
     * Set the altimeter initialized
     *
     * @param ai true if the altimeter is initialized
     */
    public void setAi(boolean ai) {
        this.ai = ai;
    }

    /**
     * Is the altimeter valid
     *
     * @return true if the altimeter is valid
     */
    public boolean isAv() {
        return av;
    }

    /**
     * Set the altimeter valid
     *
     * @param av true if the altimeter is valid
     */
    public void setAv(boolean av) {
        this.av = av;
    }

    /**
     * Get the gps good count
     *
     * @return the gps good count
     */
    public int getJg() {
        return jg;
    }

    /**
     * Set the gps good count
     *
     * @param jg the gps good count
     */
    public void setJg(int jg) {
        this.jg = jg;
    }

    /**
     * Get the gps longitude (x) degrees decimal filtered
     *
     * @return the gps longitude (x) degrees decimal filtered
     */
    public double getJxf() {
        return jxf;
    }

    /**
     * Set the gps longitude (x) degrees decimal filtered
     *
     * @param jxf the gps longitude (x) degrees decimal filtered
     */
    public void setJxf(double jxf) {
        this.jxf = jxf;
    }

    /**
     * Get the gps latitude (y) degrees decimal filtered
     *
     * @return the gps latitude (y) degrees decimal filtered
     */
    public double getJyf() {
        return jyf;
    }

    /**
     * Set the gps latitude (y) degrees decimal filtered
     *
     * @param jyf the gps latitude (y) degrees decimal filtered
     */
    public void setJyf(double jyf) {
        this.jyf = jyf;
    }

    /**
     * Get the gps altitude (z) meters filtered
     *
     * @return the gps altitude (z) meters filtered
     */
    public double getJmf() {
        return jmf;
    }

    /**
     * Set the gps altitude (z) meters filtered
     *
     * @param jmf the gps altitude (z) meters filtered
     */
    public void setJmf(double jmf) {
        this.jmf = jmf;
    }

    /**
     * Get the imu error count (accumulated)
     *
     * @return the imu error count (accumulated)
     */
    public int getPe() {
        return pe;
    }

    /**
     * Set the imu error count (accumulated)
     *
     * @param pe the imu error count (accumulated)
     */
    public void setPe(int pe) {
        this.pe = pe;
    }

    /**
     * Get the imu good count
     *
     * @return the imu good count
     */
    public int getPg() {
        return pg;
    }

    /**
     * Set the imu good count
     *
     * @param pg the imu good count
     */
    public void setPg(int pg) {
        this.pg = pg;
    }

    /**
     * Get the esc 0 pulse value
     *
     * @return the esc 0 pulse value
     */
    public int getEp0() {
        return ep0;
    }

    /**
     * Set the esc 0 pulse value
     *
     * @param ep0 the esc 0 pulse value
     */
    public void setEp0(int ep0) {
        this.ep0 = ep0;
    }

    /**
     * Get the esc 1 pulse value
     *
     * @return the esc 1 pulse value
     */
    public int getEp1() {
        return ep1;
    }

    /**
     * Set the esc 1 pulse value
     *
     * @param ep1 the esc 1 pulse value
     */
    public void setEp1(int ep1) {
        this.ep1 = ep1;
    }

    /**
     * Get the esc 2 pulse value
     *
     * @return the esc 2 pulse value
     */
    public int getEp2() {
        return ep2;
    }

    /**
     * Set the esc 2 pulse value
     *
     * @param ep2 the esc 2 pulse value
     */
    public void setEp2(int ep2) {
        this.ep2 = ep2;
    }

    /**
     * Get the esc 3 pulse value
     *
     * @return the esc 3 pulse value
     */
    public int getEp3() {
        return ep3;
    }

    /**
     * Set the esc 3 pulse value
     *
     * @param ep3 the esc 3 pulse value
     */
    public void setEp3(int ep3) {
        this.ep3 = ep3;
    }

    /**
     * Get altitude velocity (m/s)
     *
     * @return altitude velocity (m/s)
     */
    public double getKw() {
        return kw;
    }

    /**
     * Set altitude velocity (m/s)
     *
     * @param kw altitude velocity (m/s)
     */
    public void setKw(double kw) {
        this.kw = kw;
    }

    /**
     * Get the declination
     *
     * @return the declination
     */
    public double getKc() {
        return kc;
    }

    /**
     * Set the declination
     *
     * @param kc the declination
     */
    public void setKc(double kc) {
        this.kc = kc;
    }

    /**
     * Get loop count since last status update
     *
     * @return loop count since last status update
     */
    public int getLc() {
        return lc;
    }

    /**
     * Set loop count since last status update
     *
     * @param lc loop count since last status update
     */
    public void setLc(int lc) {
        this.lc = lc;
    }

    /**
     * Get loop rate since last status update
     *
     * @return loop rate since last status update
     */
    public double getLr() {
        return lr;
    }

    /**
     * Set loop rate since last status update
     *
     * @param lr loop rate since last status update
     */
    public void setLr(double lr) {
        this.lr = lr;
    }

    /**
     * Is floid serial on
     *
     * @return floid serial on
     */
    public boolean isVs() {
        return vs;
    }

    /**
     * Set floid serial on
     *
     * @param vs floid serial on
     */
    public void setVs(boolean vs) {
        this.vs = vs;
    }

    /**
     * Get the floid status rate
     *
     * @return the floid status rate
     */
    public int getSr() {
        return sr;
    }

    /**
     * Set the floid status rate
     *
     * @param sr the floid status rate
     */
    public void setSr(int sr) {
        this.sr = sr;
    }

    /**
     * Get altitude to target (m)
     *
     * @return altitude to target (m)
     */
    public double getMa() {
        return ma;
    }

    /**
     * Set altitude to target (m)
     *
     * @param ma altitude to target (m)
     */
    public void setMa(double ma) {
        this.ma = ma;
    }

    /**
     * Get distance to target (m)
     *
     * @return distance to target (m)
     */
    public double getMd() {
        return md;
    }

    /**
     * Set distance to target (m)
     *
     * @param md distance to target (m)
     */
    public void setMd(double md) {
        this.md = md;
    }

    /**
     * Get orientation to goal
     *
     * @return orientation to goal
     */
    public double getMg() {
        return mg;
    }

    /**
     * Set orientation to goal
     *
     * @param mg orientation to goal
     */
    public void setMg(double mg) {
        this.mg = mg;
    }

    /**
     * Get attack angle
     *
     * @return attack angle
     */
    public double getMk() {
        return mk;
    }

    /**
     * Set attack angle
     *
     * @param mk attack angle
     */
    public void setMk(double mk) {
        this.mk = mk;
    }

    /**
     * Get delta heading angle to target
     *
     * @return delta heading angle to target
     */
    public double getMo() {
        return mo;
    }

    /**
     * Set delta heading angle to target
     *
     * @param mo delta heading angle to target
     */
    public void setMo(double mo) {
        this.mo = mo;
    }

    /**
     * Get the lift off target altitude
     *
     * @return the lift off target altitude
     */
    public double getMt() {
        return mt;
    }

    /**
     * Set the lift off target altitude
     *
     * @param mt the lift off target altitude
     */
    public void setMt(double mt) {
        this.mt = mt;
    }

    /**
     * Get the target altitude (z) velocity
     *
     * @return the target altitude (z) velocity
     */
    public double getOav() {
        return oav;
    }

    /**
     * Set the target altitude (z) velocity
     *
     * @param oav the target altitude (z) velocity
     */
    public void setOav(double oav) {
        this.oav = oav;
    }

    /**
     * Get the target altitude (z) velocity delta
     *
     * @return the target altitude (z) velocity delta
     */
    public double getOaw() {
        return oaw;
    }

    /**
     * Set the target altitude (z) velocity delta
     *
     * @param oaw the target altitude (z) velocity delta
     */
    public void setOaw(double oaw) {
        this.oaw = oaw;
    }

    /**
     * Get the target heading velocity
     *
     * @return the target heading velocity
     */
    public double getOhv() {
        return ohv;
    }

    /**
     * Set the target heading velocity
     *
     * @param ohv the target heading velocity
     */
    public void setOhv(double ohv) {
        this.ohv = ohv;
    }

    /**
     * Get the target heading velocity delta
     *
     * @return the target heading velocity delta
     */
    public double getOhw() {
        return ohw;
    }

    /**
     * Set the target heading velocity delta
     *
     * @param ohw the target heading velocity delta
     */
    public void setOhw(double ohw) {
        this.ohw = ohw;
    }

    /**
     * Get the target orientation pitch
     *
     * @return the target orientation pitch
     */
    public double getOop() {
        return oop;
    }

    /**
     * Set the target orientation pitch
     *
     * @param oop the target orientation pitch
     */
    public void setOop(double oop) {
        this.oop = oop;
    }

    /**
     * Get the target orientation pitch delta
     *
     * @return the target orientation pitch delta
     */
    public double getOoq() {
        return ooq;
    }

    /**
     * Set the target orientation pitch delta
     *
     * @param ooq the target orientation pitch delta
     */
    public void setOoq(double ooq) {
        this.ooq = ooq;
    }

    /**
     * Get the target orientation roll
     *
     * @return the target orientation roll
     */
    public double getOor() {
        return oor;
    }

    /**
     * Set the target orientation roll
     *
     * @param oor the target orientation roll
     */
    public void setOor(double oor) {
        this.oor = oor;
    }

    /**
     * Get the target orientation roll delta
     *
     * @return the target orientation roll delta
     */
    public double getOos() {
        return oos;
    }

    /**
     * Set the target orientation roll delta
     *
     * @param oos the target orientation roll delta
     */
    public void setOos(double oos) {
        this.oos = oos;
    }

    /**
     * Get the target orientation pitch velocity
     *
     * @return the target orientation pitch velocity
     */
    public double getOpv() {
        return opv;
    }

    /**
     * Set the target orientation pitch velocity
     *
     * @param opv the target orientation pitch velocity
     */
    public void setOpv(double opv) {
        this.opv = opv;
    }

    /**
     * Get the target orientation pitch velocity delta
     *
     * @return the target orientation pitch velocity delta
     */
    public double getOpw() {
        return opw;
    }

    /**
     * Set the target orientation pitch velocity delta
     *
     * @param opw the target orientation pitch velocity delta
     */
    public void setOpw(double opw) {
        this.opw = opw;
    }

    /**
     * Get the target roll velocity
     *
     * @return the target roll velocity
     */
    public double getOrv() {
        return orv;
    }

    /**
     * Set the target roll velocity
     *
     * @param orv the target roll velocity
     */
    public void setOrv(double orv) {
        this.orv = orv;
    }

    /**
     * Get the target roll velocity delta
     *
     * @return the target roll velocity delta
     */
    public double getOrw() {
        return orw;
    }

    /**
     * Set the target roll velocity delta
     *
     * @param orw the target roll velocity
     */
    public void setOrw(double orw) {
        this.orw = orw;
    }

    /**
     * Get the target xy (over ground) velocity
     *
     * @return the target xy (over ground velocity)
     */
    public double getOv() {
        return ov;
    }

    /**
     * Set the target xy (over ground) velocity
     *
     * @param ov the target xy (over ground) velocity
     */
    public void setOv(double ov) {
        this.ov = ov;
    }

    /**
     * Get the land mode min altitude (m)
     *
     * @return the land mode min altitude (m)
     */
    public double getLma() {
        return lma;
    }

    /**
     * Set the land mode min altitude (m)
     *
     * @param lma the land mode min altitude (m)
     */
    public void setLma(double lma) {
        this.lma = lma;
    }

    /**
     * Is the land mode min altitude check started
     *
     * @return true if the land mode min altitude check is started
     */
    public boolean isLmc() {
        return lmc;
    }

    /**
     * Set the land mode min altitude check started
     *
     * @param lmc true if the land mode min altitude check is started
     */
    public void setLmc(boolean lmc) {
        this.lmc = lmc;
    }

    /**
     * Get the land mode min altitude time
     *
     * @return the land mode min altitude time
     */
    public int getLmt() {
        return lmt;
    }

    /**
     * Get the land mode min altitude time
     *
     * @param lmt the land mode min altitude time
     */
    public void setLmt(int lmt) {
        this.lmt = lmt;
    }

    /**
     * Is the floid in follow mode (floid points front to target)
     *
     * @return true if the floid is in follow mode (floid points front to target)
     */
    public boolean isFmf() {
        return fmf;
    }

    /**
     * Set the floid in follow mode (floid points front to target)
     *
     * @param fmf true if the floid is in follow mode (floid points front to target)
     */
    public void setFmf(boolean fmf) {
        this.fmf = fmf;
    }

    /**
     * Is gps emulate on
     *
     * @return true if gps emulate is on
     */
    public boolean isDf() {
        return df;
    }

    /**
     * Set gps emulate on
     *
     * @param df true if gps emulate is on
     */
    public void setDf(boolean df) {
        this.df = df;
    }

    /**
     * Is gps from (droid) device
     *
     * @return true if gps from (droid) device
     */
    public boolean isDv() {
        return dv;
    }

    /**
     * Set gps from (droid) device
     *
     * @param dv true if gps is from (droid) device
     */
    public void setDv(boolean dv) {
        this.dv = dv;
    }

    /**
     * Is imu device on
     *
     * @return true if imu device is on
     */
    public boolean isPv() {
        return pv;
    }

    /**
     * Set imu device on
     *
     * @param pv true if imu device is on
     */
    public void setPv(boolean pv) {
        this.pv = pv;
    }

    /**
     * Is floid in heli startup mode
     *
     * @return true if floid is in heli startup mode
     */
    public boolean isHsm() {
        return hsm;
    }

    /**
     * Set floid is in heli startup mode
     *
     * @param hsm true if floid is in heli startup mode
     */
    public void setHsm(boolean hsm) {
        this.hsm = hsm;
    }

    /**
     * Is debug altimeter on
     *
     * @return true if debug altimeter is on
     */
    public boolean isVl() {
        return vl;
    }

    /**
     * Set debug altimeter on
     *
     * @param vl true if debug altimeter on
     */
    public void setVl(boolean vl) {
        this.vl = vl;
    }

    /**
     * Is device PYR being used
     *
     * @return the boolean
     */
    public boolean isDpy() {
        return dpy;
    }

    /**
     * Sets device PYR being used
     *
     * @param dpy the dpy
     */
    public void setDpy(boolean dpy) {
        this.dpy = dpy;
    }

    /**
     * Is debug altimeter on
     *
     * @return the boolean
     */
    public boolean isDba() {
        return dba;
    }

    /**
     * Sets debug altimeter on
     *
     * @param dba the dba
     */
    public void setDba(boolean dba) {
        this.dba = dba;
    }

    /**
     * Get the run mode
     *
     * @return the run mode
     */
    public byte getRm() {
        return rm;
    }

    /**
     * Set the run mode
     *
     * @param rm the run mode
     */
    public void setRm(byte rm) {
        this.rm = rm;
    }

    /**
     * Get the test mode
     *
     * @return the test mode
     */
    public byte getTm() {
        return tm;
    }

    /**
     * Set the test mode
     *
     * @param tm the test mode
     */
    public void setTm(byte tm) {
        this.tm = tm;
    }

    /**
     * Is floid production mode boolean.
     *
     * @return the boolean
     */
    public boolean isFloidProductionMode() { return rm == FLOID_RUN_MODE_PRODUCTION && tm == FLOID_TEST_MODE_OFF; }

    /**
     * Is test mode physics no xy boolean.
     *
     * @return the boolean
     */
    public boolean isTestModePhysicsNoXY() { return (tm & FLOID_TEST_MODE_PHYSICS_NO_XY) > 0; }

    /**
     * Is test mode physics no altitude boolean.
     *
     * @return the boolean
     */
    public boolean isTestModePhysicsNoAltitude() { return (tm & FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE) > 0; }

    /**
     * Is test mode physics no attack angle boolean.
     *
     * @return the boolean
     */
    public boolean isTestModePhysicsNoAttackAngle() { return (tm & FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE) > 0; }

    /**
     * Is test mode physics no heading boolean.
     *
     * @return the boolean
     */
    public boolean isTestModePhysicsNoHeading() { return (tm & FLOID_TEST_MODE_PHYSICS_NO_HEADING) > 0; }

    /**
     * Is test mode physics no pitch boolean.
     *
     * @return the boolean
     */
    public boolean isTestModePhysicsNoPitch() { return (tm & FLOID_TEST_MODE_PHYSICS_NO_PITCH) > 0; }

    /**
     * Is test mode physics no roll boolean.
     *
     * @return the boolean
     */
    public boolean isTestModePhysicsNoRoll() { return (tm & FLOID_TEST_MODE_PHYSICS_NO_ROLL) > 0; }

    /**
     * Is test mode print debug physics headings boolean.
     *
     * @return the boolean
     */
    public boolean isTestModePrintDebugPhysicsHeadings() { return (tm & FLOID_TEST_MODE_PRINT_DEBUG_PHYSICS_HEADINGS) > 0; }

    /**
     * Is test mode check physics model boolean.
     *
     * @return the boolean
     */
    public boolean isTestModeCheckPhysicsModel() { return (tm & FLOID_TEST_MODE_CHECK_PHYSICS_MODEL) > 0; }

    /**
     * Is run mode physics test xy boolean.
     *
     * @return the boolean
     */
    public boolean isRunModePhysicsTestXY() { return (rm & FLOID_RUN_MODE_PHYSICS_TEST_XY) > 0; }

    /**
     * Is run mode physics test altitude 1 boolean.
     *
     * @return the boolean
     */
    public boolean isRunModePhysicsTestAltitude1() { return (rm & FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1) > 0; }

    /**
     * Is run mode physics test altitude 2 boolean.
     *
     * @return the boolean
     */
    public boolean isRunModePhysicsTestAltitude2() { return (rm & FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2) > 0; }

    /**
     * Is run mode physics test heading 1 boolean.
     *
     * @return the boolean
     */
    public boolean isRunModePhysicsTestHeading1() { return (rm & FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1) > 0; }

    /**
     * Is run mode physics test pitch 1 boolean.
     *
     * @return the boolean
     */
    public boolean isRunModePhysicsTestPitch1() { return (rm & FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1) > 0; }

    /**
     * Is run mode physics test roll 1 boolean.
     *
     * @return the boolean
     */
    public boolean isRunModePhysicsTestRoll1() { return (rm & FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1) > 0; }

    /**
     * Get the IMU algorithm status
     * @return the IMU algorithm status
     */
    public int getIas() {
        return ias;
    }

    /**
     * Set the IMU algorithm status
     * @param ias the IMU algorithm status
     */
    public void setIas(int ias) {
        this.ias = ias;
    }
}
