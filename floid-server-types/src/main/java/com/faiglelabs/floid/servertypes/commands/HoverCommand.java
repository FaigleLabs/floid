/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;


/**
 * Hover command.  Hover is in air while delay is on ground.
 */
@Entity
public class HoverCommand extends DroidCommand {
    private long hoverTime = 0L;

    /**
     * Instantiates a new Hover command.
     */
    public HoverCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        HoverCommand hoverCommand = new HoverCommand();
        super.factory(hoverCommand, droidMission);
        hoverCommand.hoverTime = hoverTime;
        return hoverCommand;
    }

    /**
     * Gets hover time.
     *
     * @return the hover time
     */
    public long getHoverTime() {
        return hoverTime;
    }

    /**
     * Sets hover time.
     *
     * @param hoverTime the hover time
     */
    public void setHoverTime(long hoverTime) {
        this.hoverTime = hoverTime;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("hoverTime", hoverTime);
        return jsonObject;
    }

    @Override
    public HoverCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        hoverTime = jsonObject.getInt("hoverTime");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("hoverTime", "" + hoverTime);
        return typeElement;
    }
}
