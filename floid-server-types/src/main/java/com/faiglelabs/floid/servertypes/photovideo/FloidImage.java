/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.photovideo;

import com.faiglelabs.floid.servertypes.statuses.FloidMessage;

import javax.persistence.*;

/**
 * Generic metadata for floid image
 */
@edu.umd.cs.findbugs.annotations.SuppressFBWarnings("EI_EXPOSE_REP")
@MappedSuperclass
public class FloidImage extends FloidMessage {
    protected String imageName;
    protected String imageType;
    protected String imageExtension;
    protected double latitude;
    protected double longitude;
    protected double altitude;
    // TODO [FUTURE]
    protected double pan;
    // TODO [FUTURE]
    protected double tilt;
    @Transient
    protected byte[] imageBytes;

    /**
     * Get photo bytes byte [ ].
     *
     * @return the byte [ ]
     */
    @Column(columnDefinition = "LONGBLOB")
    public byte[] getImageBytes() {
        return imageBytes;
    }

    /**
     * Sets photo bytes.
     *
     * @param imageBytes the photo bytes
     */
    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    /**
     * Gets image name.
     *
     * @return the image name
     */
    public String getImageName() {
        return imageName;
    }

    /**
     * Sets image name.
     *
     * @param imageName the image name
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**
     * Gets image type.
     *
     * @return the image type
     */
    @SuppressWarnings("unused")
    public String getImageType() {
        return imageType;
    }

    /**
     * Sets image type.
     *
     * @param imageType the image type
     */
    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    /**
     * Gets image extension.
     *
     * @return the image extension
     */
    @SuppressWarnings("unused")
    public String getImageExtension() {
        return imageExtension;
    }

    /**
     * Sets image extension.
     *
     * @param imageExtension the image extension
     */
    public void setImageExtension(String imageExtension) {
        this.imageExtension = imageExtension;
    }

    /**
     * Gets latitude.
     *
     * @return the latitude
     */
    @SuppressWarnings("unused")
    public double getLatitude() {
        return latitude;
    }

    /**
     * Sets latitude.
     *
     * @param latitude the latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * Gets longitude.
     *
     * @return the longitude
     */
    @SuppressWarnings("unused")
    public double getLongitude() {
        return longitude;
    }

    /**
     * Sets longitude.
     *
     * @param longitude the longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * Gets altitude.
     *
     * @return the altitude
     */
    @SuppressWarnings("unused")
    public double getAltitude() {
        return altitude;
    }

    /**
     * Sets altitude.
     *
     * @param altitude the altitude
     */
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    /**
     * Gets pan.
     *
     * @return the pan
     */
    @SuppressWarnings("unused")
    public double getPan() {
        return pan;
    }

    /**
     * Sets pan.
     *
     * @param pan the pan
     */
    public void setPan(double pan) {
        this.pan = pan;
    }

    /**
     * Gets tilt.
     *
     * @return the tilt
     */
    @SuppressWarnings("unused")
    public double getTilt() {
        return tilt;
    }

    /**
     * Sets tilt.
     *
     * @param tilt the tilt
     */
    public void setTilt(double tilt) {
        this.tilt = tilt;
    }
}
