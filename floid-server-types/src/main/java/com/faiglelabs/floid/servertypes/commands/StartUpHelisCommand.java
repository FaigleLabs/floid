/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;


/**
 * Start up helis command.  This initiates a the start up sequence where helis are turned on and brought to default positions.
 */
@Entity
public class StartUpHelisCommand extends DroidCommand {
    /**
     * Instantiates a new Start up helis command.
     */
    public StartUpHelisCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        StartUpHelisCommand startUpHelisCommand = new StartUpHelisCommand();
        super.factory(startUpHelisCommand, droidMission);
        return startUpHelisCommand;
    }

    /**
     * Instantiates a new Start up helis command.
     *
     * @param jsonObject the json object
     */
    @SuppressWarnings("unused")
    public StartUpHelisCommand(JSONObject jsonObject) {
        try {
            this.fromJSON(jsonObject);
        } catch (Exception ee) {
            //	Log.e("JSON ERR", ee.toString());
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable") JSONObject jsonObject = super.toJSON();
        // No properties to add...
        return jsonObject;
    }

    @Override
    public StartUpHelisCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable") Element typeElement = super.addToXML(doc);
        // Adds nothing...
        return typeElement;
    }
}
