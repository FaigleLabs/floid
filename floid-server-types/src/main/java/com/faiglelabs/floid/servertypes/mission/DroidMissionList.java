/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.mission;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Droid mission list.
 */
public class DroidMissionList implements Serializable {
    // TODO - FIX ME!!! - GET RID OF ID HERE...
    @Id
    @GeneratedValue
    private Long id;
    private Long missionId;
    private String missionName;

    /**
     * Instantiates a new Droid mission list.
     */
    public DroidMissionList() {
        super();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @SuppressWarnings("unused")
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    @SuppressWarnings("unused")
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Instantiates a new Droid mission list.
     *
     * @param missionId   the mission id
     * @param missionName the mission name
     */
    public DroidMissionList(Long missionId, String missionName) {
        this.missionId = missionId;
        this.missionName = missionName;
    }

    /**
     * Gets mission id.
     *
     * @return the mission id
     */
    @SuppressWarnings("unused")
    public Long getMissionId() {
        return missionId;
    }

    /**
     * Sets mission id.
     *
     * @param missionId the mission id
     */
    @SuppressWarnings("unused")
    public void setMissionId(Long missionId) {
        this.missionId = missionId;
    }

    /**
     * Gets mission name.
     *
     * @return the mission name
     */
    @SuppressWarnings("unused")
    public String getMissionName() {
        return missionName;
    }

    /**
     * Sets mission name.
     *
     * @param missionName the mission name
     */
    @SuppressWarnings("unused")
    public void setMissionName(String missionName) {
        this.missionName = missionName;
    }
}
