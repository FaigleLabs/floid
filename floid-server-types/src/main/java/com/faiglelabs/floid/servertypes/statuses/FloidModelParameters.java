/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.statuses;

import com.faiglelabs.floid.database.FloidDatabaseTables;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * The floid model parameters
 */
@Entity
@Table(name = FloidDatabaseTables.FLOID_MODEL_PARAMETERS,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidUuidIndex", columnList = "floidUuid"),
        @Index(name="timestampIndex", columnList = "timestamp"),
        @Index(name="typeIndex", columnList = "type"),
        @Index(name="floidMessageNumberIndex", columnList = "floidMessageNumber")
})
@SuppressWarnings("unused")
public class FloidModelParameters extends FloidMessage {
    /**
     * Instantiates a new Floid model parameters.
     */
    public FloidModelParameters() {
        type = getClass().getName();
        floidMessageNumber = getNextFloidMessageNumber();
    }

    public FloidModelParameters(Long id,
                                int floidId,
                                String floidUuid,
                                Long floidMessageNumber,
                                long timestamp,
                                double bladesLow, double bladesZero, double bladesHigh,
                                double h0S0Low, double h0S1Low, double h0S2Low, double h1S0Low, double h1S1Low, double h1S2Low,
                                double h0S0Zero, double h0S1Zero, double h0S2Zero, double h1S0Zero, double h1S1Zero, double h1S2Zero,
                                double h0S0High, double h0S1High, double h0S2High, double h1S0High, double h1S1High, double h1S2High,
                                double h2S0Low, double h2S1Low, double h2S2Low, double h3S0Low, double h3S1Low, double h3S2Low,
                                double h2S0Zero, double h2S1Zero, double h2S2Zero, double h3S0Zero, double h3S1Zero, double h3S2Zero,
                                double h2S0High, double h2S1High, double h2S2High, double h3S0High, double h3S1High, double h3S2High,
                                double collectiveMin, double collectiveMax, double collectiveDefault, double cyclicRange, double cyclicDefault,
                                int escType, int escPulseMin, int escPulseMax, double escLowValue, double escHighValue,
                                double attackAngleMinDistanceValue, double attackAngleMaxDistanceValue, double attackAngleValue,
                                double pitchDeltaMinValue, double pitchDeltaMaxValue, double pitchTargetVelocityMaxValue, double rollDeltaMinValue,
                                double rollDeltaMaxValue, double rollTargetVelocityMaxValue, double altitudeToTargetMinValue,
                                double altitudeToTargetMaxValue, double altitudeTargetVelocityMaxValue, double headingDeltaMinValue,
                                double headingDeltaMaxValue, double headingTargetVelocityMaxValue, double distanceToTargetMinValue,
                                double distanceToTargetMaxValue, double distanceTargetVelocityMaxValue, double orientationMinDistanceValue,
                                double liftOffTargetAltitudeDeltaValue, int servoPulseMin, int servoPulseMax, double servoDegreeMin,
                                double servoDegreeMax, int servoSignLeft, int servoSignRight, int servoSignPitch, double servoOffsetLeft0,
                                double servoOffsetRight0, double servoOffsetPitch0, double servoOffsetLeft1,
                                double servoOffsetRight1, double servoOffsetPitch1, double servoOffsetLeft2,
                                double servoOffsetRight2, double servoOffsetPitch2, double servoOffsetLeft3,
                                double servoOffsetRight3, double servoOffsetPitch3, double targetVelocityKeepStill, double targetVelocityFullSpeed,
                                double targetPitchVelocityAlpha, double targetRollVelocityAlpha, double targetHeadingVelocityAlpha,
                                double targetAltitudeVelocityAlpha, int landModeTimeRequiredAtMinAltitude, int startMode, int rotationMode,
                                double cyclicHeadingAlpha, int heliStartupModeNumberSteps, int heliStartupModeStepTick,
                                double floidModelEscCollectiveCalcMidpoint, double floidModelEscCollectiveLowValue,
                                double floidModelEscCollectiveMidValue , double floidModelEscCollectiveHighValue,
                                double velocityDeltaCyclicAlphaScale, double accelerationMultiplierScale) {
        this.id = id;
        this.floidId = floidId;
        this.floidUuid = floidUuid;
        this.floidMessageNumber = floidMessageNumber;
        this.type = getClass().getName();
        this.timestamp = timestamp;
        this.bladesLow = bladesLow;
        this.bladesZero = bladesZero;
        this.bladesHigh = bladesHigh;
        this.h0S0Low = h0S0Low;
        this.h0S1Low = h0S1Low;
        this.h0S2Low = h0S2Low;
        this.h1S0Low = h1S0Low;
        this.h1S1Low = h1S1Low;
        this.h1S2Low = h1S2Low;
        this.h0S0Zero = h0S0Zero;
        this.h0S1Zero = h0S1Zero;
        this.h0S2Zero = h0S2Zero;
        this.h1S0Zero = h1S0Zero;
        this.h1S1Zero = h1S1Zero;
        this.h1S2Zero = h1S2Zero;
        this.h0S0High = h0S0High;
        this.h0S1High = h0S1High;
        this.h0S2High = h0S2High;
        this.h1S0High = h1S0High;
        this.h1S1High = h1S1High;
        this.h1S2High = h1S2High;
        this.h2S0Low = h2S0Low;
        this.h2S1Low = h2S1Low;
        this.h2S2Low = h2S2Low;
        this.h3S0Low = h3S0Low;
        this.h3S1Low = h3S1Low;
        this.h3S2Low = h3S2Low;
        this.h2S0Zero = h2S0Zero;
        this.h2S1Zero = h2S1Zero;
        this.h2S2Zero = h2S2Zero;
        this.h3S0Zero = h3S0Zero;
        this.h3S1Zero = h3S1Zero;
        this.h3S2Zero = h3S2Zero;
        this.h2S0High = h2S0High;
        this.h2S1High = h2S1High;
        this.h2S2High = h2S2High;
        this.h3S0High = h3S0High;
        this.h3S1High = h3S1High;
        this.h3S2High = h3S2High;
        this.collectiveMin = collectiveMin;
        this.collectiveMax = collectiveMax;
        this.collectiveDefault = collectiveDefault;
        this.cyclicRange = cyclicRange;
        this.cyclicDefault = cyclicDefault;
        this.escLowValue = escLowValue;
        this.escHighValue = escHighValue;
        this.attackAngleMinDistanceValue = attackAngleMinDistanceValue;
        this.attackAngleMaxDistanceValue = attackAngleMaxDistanceValue;
        this.attackAngleValue = attackAngleValue;
        this.pitchDeltaMinValue = pitchDeltaMinValue;
        this.pitchDeltaMaxValue = pitchDeltaMaxValue;
        this.pitchTargetVelocityMaxValue = pitchTargetVelocityMaxValue;
        this.rollDeltaMinValue = rollDeltaMinValue;
        this.rollDeltaMaxValue = rollDeltaMaxValue;
        this.rollTargetVelocityMaxValue = rollTargetVelocityMaxValue;
        this.altitudeToTargetMinValue = altitudeToTargetMinValue;
        this.altitudeToTargetMaxValue = altitudeToTargetMaxValue;
        this.altitudeTargetVelocityMaxValue = altitudeTargetVelocityMaxValue;
        this.headingDeltaMinValue = headingDeltaMinValue;
        this.headingDeltaMaxValue = headingDeltaMaxValue;
        this.headingTargetVelocityMaxValue = headingTargetVelocityMaxValue;
        this.distanceToTargetMinValue = distanceToTargetMinValue;
        this.distanceToTargetMaxValue = distanceToTargetMaxValue;
        this.distanceTargetVelocityMaxValue = distanceTargetVelocityMaxValue;
        this.orientationMinDistanceValue = orientationMinDistanceValue;
        this.liftOffTargetAltitudeDeltaValue = liftOffTargetAltitudeDeltaValue;


        this.servoPulseMin = servoPulseMin;
        this.servoPulseMax = servoPulseMax;

        this.servoDegreeMin = servoDegreeMin;
        this.servoDegreeMax = servoDegreeMax;

        this.servoSignLeft = servoSignLeft ;
        this.servoSignRight = servoSignRight;
        this.servoSignPitch = servoSignPitch;

        this.servoOffsetLeft0 = servoOffsetLeft0;
        this.servoOffsetRight0 = servoOffsetRight0;
        this.servoOffsetPitch0 = servoOffsetPitch0;
        this.servoOffsetLeft1 = servoOffsetLeft1;
        this.servoOffsetRight1 = servoOffsetRight1;
        this.servoOffsetPitch1 = servoOffsetPitch1;
        this.servoOffsetLeft2 = servoOffsetLeft2;
        this.servoOffsetRight2 = servoOffsetRight2;
        this.servoOffsetPitch2 = servoOffsetPitch2;
        this.servoOffsetLeft3 = servoOffsetLeft3;
        this.servoOffsetRight3 = servoOffsetRight3;
        this.servoOffsetPitch3 = servoOffsetPitch3;

        this.targetVelocityKeepStill = targetVelocityKeepStill;
        this.targetVelocityFullSpeed = targetVelocityFullSpeed;
        this.targetPitchVelocityAlpha = targetPitchVelocityAlpha;
        this.targetRollVelocityAlpha = targetRollVelocityAlpha;
        this.targetHeadingVelocityAlpha = targetHeadingVelocityAlpha;
        this.targetAltitudeVelocityAlpha = targetAltitudeVelocityAlpha;

        this.landModeTimeRequiredAtMinAltitude = landModeTimeRequiredAtMinAltitude;
        this.startMode = startMode;
        this.rotationMode = rotationMode;

        this.cyclicHeadingAlpha = cyclicHeadingAlpha;

        this.heliStartupModeNumberSteps = heliStartupModeNumberSteps;
        this.heliStartupModeStepTick = heliStartupModeStepTick ;

        this.floidModelEscCollectiveCalcMidpoint = floidModelEscCollectiveCalcMidpoint;
        this.floidModelEscCollectiveLowValue     = floidModelEscCollectiveLowValue;
        this.floidModelEscCollectiveMidValue     = floidModelEscCollectiveMidValue;
        this.floidModelEscCollectiveHighValue    = floidModelEscCollectiveHighValue;

        this.velocityDeltaCyclicAlphaScale = velocityDeltaCyclicAlphaScale;
        this.accelerationMultiplierScale = accelerationMultiplierScale;
    }

    /**
     * Blades low degrees
     */
    protected double bladesLow = -6;
    /**
     * Blades zero degrees
     */
    protected double bladesZero = 8;
    /**
     * Blades high degrees
     */
    protected double bladesHigh = 24;
    /**
     * Heli 0 Servo 0 low
     */
    protected double h0S0Low = 5;
    /**
     * Heli 0 Servo 1 low
     */
    protected double h0S1Low = 5;
    /**
     * Heli 0 Servo 2 low
     */
    protected double h0S2Low = 5;
    /**
     * Heli 1 Servo 0 low
     */
    protected double h1S0Low = 5;
    /**
     * Heli 1 Servo 1 low
     */
    protected double h1S1Low = 5;
    /**
     * Heli 1 Servo 2 low
     */
    protected double h1S2Low = 5;
    /**
     * Heli 0 Servo 0 zero
     */
    protected double h0S0Zero = 28;
    /**
     * Heli 0 Servo 1 zero
     */
    protected double h0S1Zero = 28;
    /**
     * Heli 0 Servo 2 zero
     */
    protected double h0S2Zero = 28;
    /**
     * Heli 1 Servo 0 zero
     */
    protected double h1S0Zero = 28;
    /**
     * Heli 1 Servo 1 zero
     */
    protected double h1S1Zero = 28;
    /**
     * Heli 1 Servo 2 zero
     */
    protected double h1S2Zero = 28;
    /**
     * Heli 0 Servo 0 high
     */
    protected double h0S0High = 55;
    /**
     * Heli 0 Servo 1 high
     */
    protected double h0S1High = 55;
    /**
     * Heli 0 Servo 2 high
     */
    protected double h0S2High = 55;
    /**
     * Heli 1 Servo 0 high
     */
    protected double h1S0High = 55;
    /**
     * Heli 1 Servo 1 high
     */
    protected double h1S1High = 55;
    /**
     * Heli 1 Servo 2 high
     */
    protected double h1S2High = 55;
    /**
     * Heli 2 Servo 0 low
     */
    protected double h2S0Low = 5;
    /**
     * Heli 2 Servo 1 low
     */
    protected double h2S1Low = 5;
    /**
     * Heli 2 Servo 2 low
     */
    protected double h2S2Low = 5;
    /**
     * Heli 3 Servo 0 low
     */
    protected double h3S0Low = 5;
    /**
     * Heli 3 Servo 1 low
     */
    protected double h3S1Low = 5;
    /**
     * Heli 3 Servo 2 low
     */
    protected double h3S2Low = 5;
    /**
     * Heli 2 Servo 0 zero
     */
    protected double h2S0Zero = 28;
    /**
     * Heli 2 Servo 1 zero
     */
    protected double h2S1Zero = 28;
    /**
     * Heli 2 Servo 2 zero
     */
    protected double h2S2Zero = 28;
    /**
     * Heli 3 Servo 0 zero
     */
    protected double h3S0Zero = 28;
    /**
     * Heli 3 Servo 1 zero
     */
    protected double h3S1Zero = 28;
    /**
     * Heli 3 Servo 2 zero
     */
    protected double h3S2Zero = 28;
    /**
     * Heli 2 Servo 0 high
     */
    protected double h2S0High = 55;
    /**
     * Heli 2 Servo 1 high
     */
    protected double h2S1High = 55;
    /**
     * Heli 2 Servo 2 high
     */
    protected double h2S2High = 55;
    /**
     * Heli 3 Servo 0 high
     */
    protected double h3S0High = 55;
    /**
     * Heli 3 Servo 1 high
     */
    protected double h3S1High = 55;
    /**
     * Heli 3 Servo 2 high
     */
    protected double h3S2High = 55;
    /**
     * Collective min
     */
    protected double collectiveMin = 4;
    /**
     * Collective max
     */
    protected double collectiveMax = 20;
    /**
     * Collective default
     */
    protected double collectiveDefault = 0.0;
    /**
     * Cyclic range
     */
    protected double cyclicRange = 4;
    /**
     * Cyclic default
     */
    protected double cyclicDefault = 0.0;
    /**
     * ESC type
     */
    protected int escType = 0; // Exceed
    /**
     * ESC pulse min
     */
    protected int escPulseMin = 1000;
    /**
     * ESC pulse max
     */
    protected int escPulseMax = 2000;
    /**
     * ESC low value
     */
    protected double escLowValue = 0.00;
    /**
     * ESC high value
     */
    protected double escHighValue = 1.00;
    /**
     * Attack angle minimum distance value
     */
    protected double attackAngleMinDistanceValue = 5;
    /**
     * Attack angle maximum distance value
     */
    protected double attackAngleMaxDistanceValue = 10;
    /**
     * Attack angle value
     */
    protected double attackAngleValue = -5;
    /**
     * Pitch delta min value
     */
    protected double pitchDeltaMinValue = 1.0;
    /**
     * Pitch delta max value
     */
    protected double pitchDeltaMaxValue = 5.0;
    /**
     * Pitch target velocity max value
     */
    protected double pitchTargetVelocityMaxValue = 2.0;
    /**
     * Roll delta minimum value
     */
    protected double rollDeltaMinValue = 1.0;
    /**
     * Roll delta maximum value
     */
    protected double rollDeltaMaxValue = 5.0;
    /**
     * Roll target velocity maximum value
     */
    protected double rollTargetVelocityMaxValue = 2.0;
    /**
     * Altitude to target min value
     */
    protected double altitudeToTargetMinValue = 1.0;
    /**
     * Altitude to target max value
     */
    protected double altitudeToTargetMaxValue = 3.0;
    /**
     * Altitude to target velocity max value
     */
    protected double altitudeTargetVelocityMaxValue = 3.0;
    /**
     * Heading delta min value
     */
    protected double headingDeltaMinValue = 2.0;
    /**
     * Heading delta max value
     */
    protected double headingDeltaMaxValue = 10.0;
    /**
     * Heading target velocity max value
     */
    protected double headingTargetVelocityMaxValue = 3.0;
    /**
     * Distance to target min value
     */
    protected double distanceToTargetMinValue = 2.0;
    /**
     * Distance to target max value
     */
    protected double distanceToTargetMaxValue = 20.0;
    /**
     * Distance target velocity max value
     */
    protected double distanceTargetVelocityMaxValue = 3.0;
    /**
     * Orientation min distance value
     */
    protected double orientationMinDistanceValue = 5.0;
    /**
     * Lift off target altitude delta value
     */
    protected double liftOffTargetAltitudeDeltaValue = 5.0;
    /**
     * Servo pulse min
     */
    protected int servoPulseMin = 1000;
    /**
     * Servo pulse max
     */
    protected int servoPulseMax = 2000;
    /**
     * Servo degree min
     */
    protected double servoDegreeMin = 0;
    /**
     * Servo pulse max
     */
    protected double servoDegreeMax = 90;
    /**
     * Servo sign left (1=positive) (0=negative)
     */
    protected int servoSignLeft = 1;
    /**
     * Servo sign right (1=positive) (0=negative)
     */
    protected int servoSignRight = 0;
    /**
     * Servo sign pitch (1=positive) (0=negative)
     */
    protected int servoSignPitch = 1;
    /**
     * Heli 0 Servo offset left (degrees)
     */
    protected double servoOffsetLeft0 = 60;
    /**
     * Heli 0 Servo offset right (degrees)
     */
    protected double servoOffsetRight0 = -60;
    /**
     * Heli 0 Servo offset pitch (degrees)
     */
    protected double servoOffsetPitch0 = 180;

    /**
     * Heli 1 Servo offset left (degrees)
     */
    protected double servoOffsetLeft1 = 60;
    /**
     * Heli 1 Servo offset right (degrees)
     */
    protected double servoOffsetRight1 = -60;
    /**
     * Heli 1 Servo offset pitch (degrees)
     */
    protected double servoOffsetPitch1 = 180;

    /**
     * Heli 2 Servo offset left (degrees)
     */
    protected double servoOffsetLeft2 = 60;
    /**
     * Heli 2 Servo offset right (degrees)
     */
    protected double servoOffsetRight2 = -60;
    /**
     * Heli 2 Servo offset pitch (degrees)
     */
    protected double servoOffsetPitch2 = 180;

    /**
     * Heli 3 Servo offset left (degrees)
     */
    protected double servoOffsetLeft3 = 60;
    /**
     * Heli 3 Servo offset right (degrees)
     */
    protected double servoOffsetRight3 = -60;
    /**
     * Heli 3 Servo offset pitch (degrees)
     */
    protected double servoOffsetPitch3 = 180;

    /**
     * Target velocity for keep still
     */
    protected double targetVelocityKeepStill = 2.5;
    /**
     * Target velocity for full speed
     */
    protected double targetVelocityFullSpeed = 10;
    /**
     * Target pitch velocity alpha
     */
    protected double targetPitchVelocityAlpha = 0.1;
    /**
     * Target roll velocity alpha
     */
    protected double targetRollVelocityAlpha = 0.1;
    /**
     * Target heading velocity alpha
     */
    protected double targetHeadingVelocityAlpha = 0.1;
    /**
     * Target altitude velocity alpha
     */
    protected double targetAltitudeVelocityAlpha = 0.1;
    /**
     * Land mode time required at min altitude to change state to on-ground
     */
    protected int landModeTimeRequiredAtMinAltitude = 5000;
    /**
     * Start mode
     */
    protected int startMode = 0;  // FLOID MODE TEST
    /**
     * Rotation mode
     */
    protected int rotationMode = 0;
    /**
     * Cyclic heading alpha
     */
    protected double cyclicHeadingAlpha = 0.3;
    // Heli Startup:
    /**
     * Heli startup number of steps
     */
    protected int heliStartupModeNumberSteps = 40;
    /**
     * Heli startup ticks per step
     */
    protected int heliStartupModeStepTick = 250;
    // ESC Collective Calc:
    /**
     * ESC collective calculation midpoint
     */
    protected double floidModelEscCollectiveCalcMidpoint = 0.5;
    /**
     * ESC collective calculation low value
     */
    protected double floidModelEscCollectiveLowValue     = 0;
    /**
     * ESC collective calculation mid value
     */
    protected double floidModelEscCollectiveMidValue     = 0.2;
    /**
     * ESC collective calculation high value
     */
    protected double floidModelEscCollectiveHighValue    = 1.0;
    /**
     * The scaling factor for cyclic alpha values
     */
    protected double velocityDeltaCyclicAlphaScale = 0.1;
    /**
     * Acceleration multiplier scale
     */
    protected double accelerationMultiplierScale = 10.0;

    @Override
    public FloidModelParameters fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        bladesLow = jsonObject.getDouble("bladesLow");
        bladesZero = jsonObject.getDouble("bladesZero");
        bladesHigh = jsonObject.getDouble("bladesHigh");
        h0S0Low = jsonObject.getDouble("h0S0Low");
        h0S1Low = jsonObject.getDouble("h0S1Low");
        h0S2Low = jsonObject.getDouble("h0S2Low");
        h1S0Low = jsonObject.getDouble("h1S0Low");
        h1S1Low = jsonObject.getDouble("h1S1Low");
        h1S2Low = jsonObject.getDouble("h1S2Low");
        h0S0Zero = jsonObject.getDouble("h0S0Zero");
        h0S1Zero = jsonObject.getDouble("h0S1Zero");
        h0S2Zero = jsonObject.getDouble("h0S2Zero");
        h1S0Zero = jsonObject.getDouble("h1S0Zero");
        h1S1Zero = jsonObject.getDouble("h1S1Zero");
        h1S2Zero = jsonObject.getDouble("h1S2Zero");
        h0S0High = jsonObject.getDouble("h0S0High");
        h0S1High = jsonObject.getDouble("h0S1High");
        h0S2High = jsonObject.getDouble("h0S2High");
        h1S0High = jsonObject.getDouble("h1S0High");
        h1S1High = jsonObject.getDouble("h1S1High");
        h1S2High = jsonObject.getDouble("h1S2High");
        h2S0Low = jsonObject.getDouble("h2S0Low");
        h2S1Low = jsonObject.getDouble("h2S1Low");
        h2S2Low = jsonObject.getDouble("h2S2Low");
        h3S0Low = jsonObject.getDouble("h3S0Low");
        h3S1Low = jsonObject.getDouble("h3S1Low");
        h3S2Low = jsonObject.getDouble("h3S2Low");
        h2S0Zero = jsonObject.getDouble("h2S0Zero");
        h2S1Zero = jsonObject.getDouble("h2S1Zero");
        h2S2Zero = jsonObject.getDouble("h2S2Zero");
        h3S0Zero = jsonObject.getDouble("h3S0Zero");
        h3S1Zero = jsonObject.getDouble("h3S1Zero");
        h3S2Zero = jsonObject.getDouble("h3S2Zero");
        h2S0High = jsonObject.getDouble("h2S0High");
        h2S1High = jsonObject.getDouble("h2S1High");
        h2S2High = jsonObject.getDouble("h2S2High");
        h3S0High = jsonObject.getDouble("h3S0High");
        h3S1High = jsonObject.getDouble("h3S1High");
        h3S2High = jsonObject.getDouble("h3S2High");
        collectiveMin = jsonObject.getDouble("collectiveMin");
        collectiveMax = jsonObject.getDouble("collectiveMax");
        collectiveDefault = jsonObject.getDouble("collectiveDefault");
        cyclicRange = jsonObject.getDouble("cyclicRange");
        cyclicDefault = jsonObject.getDouble("cyclicDefault");
        escType = jsonObject.getInt("escType");
        escPulseMin = jsonObject.getInt("escPulseMin");
        escPulseMax = jsonObject.getInt("escPulseMax");
        escLowValue = jsonObject.getDouble("escLowValue");
        escHighValue = jsonObject.getDouble("escHighValue");
        attackAngleMinDistanceValue = jsonObject.getDouble("attackAngleMinDistanceValue");
        attackAngleMaxDistanceValue = jsonObject.getDouble("attackAngleMaxDistanceValue");
        attackAngleValue = jsonObject.getDouble("attackAngleValue");
        pitchDeltaMinValue = jsonObject.getDouble("pitchDeltaMinValue");
        pitchDeltaMaxValue = jsonObject.getDouble("pitchDeltaMaxValue");
        pitchTargetVelocityMaxValue = jsonObject.getDouble("pitchTargetVelocityMaxValue");
        rollDeltaMinValue = jsonObject.getDouble("rollDeltaMinValue");
        rollDeltaMaxValue = jsonObject.getDouble("rollDeltaMaxValue");
        rollTargetVelocityMaxValue = jsonObject.getDouble("rollTargetVelocityMaxValue");
        altitudeToTargetMinValue = jsonObject.getDouble("altitudeToTargetMinValue");
        altitudeToTargetMaxValue = jsonObject.getDouble("altitudeToTargetMaxValue");
        altitudeTargetVelocityMaxValue = jsonObject.getDouble("altitudeTargetVelocityMaxValue");
        headingDeltaMinValue = jsonObject.getDouble("headingDeltaMinValue");
        headingDeltaMaxValue = jsonObject.getDouble("headingDeltaMaxValue");
        headingTargetVelocityMaxValue = jsonObject.getDouble("headingTargetVelocityMaxValue");
        distanceToTargetMinValue = jsonObject.getDouble("distanceToTargetMinValue");
        distanceToTargetMaxValue = jsonObject.getDouble("distanceToTargetMaxValue");
        distanceTargetVelocityMaxValue = jsonObject.getDouble("distanceTargetVelocityMaxValue");
        orientationMinDistanceValue = jsonObject.getDouble("orientationMinDistanceValue");
        liftOffTargetAltitudeDeltaValue = jsonObject.getDouble("liftOffTargetAltitudeDeltaValue");
        servoPulseMin = jsonObject.getInt("servoPulseMin");
        servoPulseMax = jsonObject.getInt("servoPulseMax");
        servoDegreeMin = jsonObject.getDouble("servoDegreeMin");
        servoDegreeMax = jsonObject.getDouble("servoDegreeMax");
        servoSignLeft = jsonObject.getInt("servoSignLeft");
        servoSignRight = jsonObject.getInt("servoSignRight");
        servoSignPitch = jsonObject.getInt("servoSignPitch");
        servoOffsetLeft0 = jsonObject.getDouble("servoOffsetLeft0");
        servoOffsetRight0 = jsonObject.getDouble("servoOffsetRight0");
        servoOffsetPitch0 = jsonObject.getDouble("servoOffsetPitch0");

        servoOffsetLeft1 = jsonObject.getDouble("servoOffsetLeft1");
        servoOffsetRight1 = jsonObject.getDouble("servoOffsetRight1");
        servoOffsetPitch1 = jsonObject.getDouble("servoOffsetPitch1");

        servoOffsetLeft2 = jsonObject.getDouble("servoOffsetLeft2");
        servoOffsetRight2 = jsonObject.getDouble("servoOffsetRight2");
        servoOffsetPitch2 = jsonObject.getDouble("servoOffsetPitch2");

        servoOffsetLeft3 = jsonObject.getDouble("servoOffsetLeft3");
        servoOffsetRight3 = jsonObject.getDouble("servoOffsetRight3");
        servoOffsetPitch3 = jsonObject.getDouble("servoOffsetPitch3");

        targetVelocityKeepStill = jsonObject.getDouble("targetVelocityKeepStill");
        targetVelocityFullSpeed = jsonObject.getDouble("targetVelocityFullSpeed");
        targetPitchVelocityAlpha = jsonObject.getDouble("targetPitchVelocityAlpha");
        targetRollVelocityAlpha = jsonObject.getDouble("targetRollVelocityAlpha");
        targetHeadingVelocityAlpha = jsonObject.getDouble("targetHeadingVelocityAlpha");
        targetAltitudeVelocityAlpha = jsonObject.getDouble("targetAltitudeVelocityAlpha");
        landModeTimeRequiredAtMinAltitude = jsonObject.getInt("landModeTimeRequiredAtMinAltitude");
        startMode = jsonObject.getInt("startMode");
        rotationMode = jsonObject.getInt("rotationMode");
        cyclicHeadingAlpha = jsonObject.getDouble("cyclicHeadingAlpha");
        heliStartupModeNumberSteps = jsonObject.getInt("heliStartupModeNumberSteps");
        heliStartupModeStepTick = jsonObject.getInt("heliStartupModeStepTick");
        floidModelEscCollectiveCalcMidpoint = jsonObject.getDouble("floidModelEscCollectiveCalcMidpoint");
        floidModelEscCollectiveLowValue = jsonObject.getDouble("floidModelEscCollectiveLowValue");
        floidModelEscCollectiveMidValue = jsonObject.getDouble("floidModelEscCollectiveMidValue");
        floidModelEscCollectiveHighValue = jsonObject.getDouble("floidModelEscCollectiveHighValue");
        velocityDeltaCyclicAlphaScale = jsonObject.getDouble("velocityDeltaCyclicAlphaScale");
        accelerationMultiplierScale = jsonObject.getDouble("accelerationMultiplierScale");

        return this;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("bladesLow", bladesLow);
        jsonObject.put("bladesZero", bladesZero);
        jsonObject.put("bladesHigh", bladesHigh);
        jsonObject.put("h0S0Low", h0S0Low);
        jsonObject.put("h0S1Low", h0S1Low);
        jsonObject.put("h0S2Low", h0S2Low);
        jsonObject.put("h1S0Low", h1S0Low);
        jsonObject.put("h1S1Low", h1S1Low);
        jsonObject.put("h1S2Low", h1S2Low);
        jsonObject.put("h0S0Zero", h0S0Zero);
        jsonObject.put("h0S1Zero", h0S1Zero);
        jsonObject.put("h0S2Zero", h0S2Zero);
        jsonObject.put("h1S0Zero", h1S0Zero);
        jsonObject.put("h1S1Zero", h1S1Zero);
        jsonObject.put("h1S2Zero", h1S2Zero);
        jsonObject.put("h0S0High", h0S0High);
        jsonObject.put("h0S1High", h0S1High);
        jsonObject.put("h0S2High", h0S2High);
        jsonObject.put("h1S0High", h1S0High);
        jsonObject.put("h1S1High", h1S1High);
        jsonObject.put("h1S2High", h1S2High);
        jsonObject.put("h2S0Low", h2S0Low);
        jsonObject.put("h2S1Low", h2S1Low);
        jsonObject.put("h2S2Low", h2S2Low);
        jsonObject.put("h3S0Low", h3S0Low);
        jsonObject.put("h3S1Low", h3S1Low);
        jsonObject.put("h3S2Low", h3S2Low);
        jsonObject.put("h2S0Zero", h2S0Zero);
        jsonObject.put("h2S1Zero", h2S1Zero);
        jsonObject.put("h2S2Zero", h2S2Zero);
        jsonObject.put("h3S0Zero", h3S0Zero);
        jsonObject.put("h3S1Zero", h3S1Zero);
        jsonObject.put("h3S2Zero", h3S2Zero);
        jsonObject.put("h2S0High", h2S0High);
        jsonObject.put("h2S1High", h2S1High);
        jsonObject.put("h2S2High", h2S2High);
        jsonObject.put("h3S0High", h3S0High);
        jsonObject.put("h3S1High", h3S1High);
        jsonObject.put("h3S2High", h3S2High);
        jsonObject.put("collectiveMin", collectiveMin);
        jsonObject.put("collectiveMax", collectiveMax);
        jsonObject.put("collectiveDefault", collectiveDefault);
        jsonObject.put("cyclicRange", cyclicRange);
        jsonObject.put("cyclicDefault", cyclicDefault);
        jsonObject.put("escType", escType);
        jsonObject.put("escPulseMin", escPulseMin);
        jsonObject.put("escPulseMax", escPulseMax);
        jsonObject.put("escLowValue", escLowValue);
        jsonObject.put("escHighValue", escHighValue);
        jsonObject.put("attackAngleMinDistanceValue", attackAngleMinDistanceValue);
        jsonObject.put("attackAngleMaxDistanceValue", attackAngleMaxDistanceValue);
        jsonObject.put("attackAngleValue", attackAngleValue);
        jsonObject.put("pitchDeltaMinValue", pitchDeltaMinValue);
        jsonObject.put("pitchDeltaMaxValue", pitchDeltaMaxValue);
        jsonObject.put("pitchTargetVelocityMaxValue", pitchTargetVelocityMaxValue);
        jsonObject.put("rollDeltaMinValue", rollDeltaMinValue);
        jsonObject.put("rollDeltaMaxValue", rollDeltaMaxValue);
        jsonObject.put("rollTargetVelocityMaxValue", rollTargetVelocityMaxValue);
        jsonObject.put("altitudeToTargetMinValue", altitudeToTargetMinValue);
        jsonObject.put("altitudeToTargetMaxValue", altitudeToTargetMaxValue);
        jsonObject.put("altitudeTargetVelocityMaxValue", altitudeTargetVelocityMaxValue);
        jsonObject.put("headingDeltaMinValue", headingDeltaMinValue);
        jsonObject.put("headingDeltaMaxValue", headingDeltaMaxValue);
        jsonObject.put("headingTargetVelocityMaxValue", headingTargetVelocityMaxValue);
        jsonObject.put("distanceToTargetMinValue", distanceToTargetMinValue);
        jsonObject.put("distanceToTargetMaxValue", distanceToTargetMaxValue);
        jsonObject.put("distanceTargetVelocityMaxValue", distanceTargetVelocityMaxValue);
        jsonObject.put("orientationMinDistanceValue", orientationMinDistanceValue);
        jsonObject.put("liftOffTargetAltitudeDeltaValue", liftOffTargetAltitudeDeltaValue);
        jsonObject.put("servoPulseMin", servoPulseMin);
        jsonObject.put("servoPulseMax", servoPulseMax);
        jsonObject.put("servoDegreeMin", servoDegreeMin);
        jsonObject.put("servoDegreeMax", servoDegreeMax);
        jsonObject.put("servoSignLeft", servoSignLeft);
        jsonObject.put("servoSignRight", servoSignRight);
        jsonObject.put("servoSignPitch", servoSignPitch);
        jsonObject.put("servoOffsetLeft0", servoOffsetLeft0);
        jsonObject.put("servoOffsetRight0", servoOffsetRight0);
        jsonObject.put("servoOffsetPitch0", servoOffsetPitch0);

        jsonObject.put("servoOffsetLeft1", servoOffsetLeft1);
        jsonObject.put("servoOffsetRight1", servoOffsetRight1);
        jsonObject.put("servoOffsetPitch1", servoOffsetPitch1);

        jsonObject.put("servoOffsetLeft2", servoOffsetLeft2);
        jsonObject.put("servoOffsetRight2", servoOffsetRight2);
        jsonObject.put("servoOffsetPitch2", servoOffsetPitch2);

        jsonObject.put("servoOffsetLeft3", servoOffsetLeft3);
        jsonObject.put("servoOffsetRight3", servoOffsetRight3);
        jsonObject.put("servoOffsetPitch3", servoOffsetPitch3);

        jsonObject.put("targetVelocityKeepStill", targetVelocityKeepStill);
        jsonObject.put("targetVelocityFullSpeed", targetVelocityFullSpeed);
        jsonObject.put("targetPitchVelocityAlpha", targetPitchVelocityAlpha);
        jsonObject.put("targetRollVelocityAlpha", targetRollVelocityAlpha);
        jsonObject.put("targetHeadingVelocityAlpha", targetHeadingVelocityAlpha);
        jsonObject.put("targetAltitudeVelocityAlpha", targetAltitudeVelocityAlpha);
        jsonObject.put("landModeTimeRequiredAtMinAltitude", landModeTimeRequiredAtMinAltitude);
        jsonObject.put("startMode", startMode);
        jsonObject.put("rotationMode", rotationMode);
        jsonObject.put("cyclicHeadingAlpha", cyclicHeadingAlpha);
        jsonObject.put("heliStartupModeNumberSteps", heliStartupModeNumberSteps);
        jsonObject.put("heliStartupModeStepTick", heliStartupModeStepTick);
        jsonObject.put("floidModelEscCollectiveCalcMidpoint", floidModelEscCollectiveCalcMidpoint);
        jsonObject.put("floidModelEscCollectiveLowValue", floidModelEscCollectiveLowValue);
        jsonObject.put("floidModelEscCollectiveMidValue", floidModelEscCollectiveMidValue);
        jsonObject.put("floidModelEscCollectiveHighValue", floidModelEscCollectiveHighValue);
        jsonObject.put("velocityDeltaCyclicAlphaScale", velocityDeltaCyclicAlphaScale);
        jsonObject.put("accelerationMultiplierScale", accelerationMultiplierScale);

        return jsonObject;
    }

    /**
     * Gets altitude target velocity max value.
     *
     * @return the altitude target velocity max value
     */
    public double getAltitudeTargetVelocityMaxValue() {
        return altitudeTargetVelocityMaxValue;
    }

    /**
     * Sets altitude target velocity max value.
     *
     * @param altitudeTargetVelocityMaxValue the altitude target velocity max value
     */
    public void setAltitudeTargetVelocityMaxValue(double altitudeTargetVelocityMaxValue) {
        this.altitudeTargetVelocityMaxValue = altitudeTargetVelocityMaxValue;
    }

    /**
     * Gets altitude to target max value.
     *
     * @return the altitude to target max value
     */
    public double getAltitudeToTargetMaxValue() {
        return altitudeToTargetMaxValue;
    }

    /**
     * Sets altitude to target max value.
     *
     * @param altitudeToTargetMaxValue the altitude to target max value
     */
    public void setAltitudeToTargetMaxValue(double altitudeToTargetMaxValue) {
        this.altitudeToTargetMaxValue = altitudeToTargetMaxValue;
    }

    /**
     * Gets altitude to target min value.
     *
     * @return the altitude to target min value
     */
    public double getAltitudeToTargetMinValue() {
        return altitudeToTargetMinValue;
    }

    /**
     * Sets altitude to target min value.
     *
     * @param altitudeToTargetMinValue the altitude to target min value
     */
    public void setAltitudeToTargetMinValue(double altitudeToTargetMinValue) {
        this.altitudeToTargetMinValue = altitudeToTargetMinValue;
    }

    /**
     * Gets attack angle max distance value.
     *
     * @return the attack angle max distance value
     */
    public double getAttackAngleMaxDistanceValue() {
        return attackAngleMaxDistanceValue;
    }

    /**
     * Sets attack angle max distance value.
     *
     * @param attackAngleMaxDistanceValue the attack angle max distance value
     */
    public void setAttackAngleMaxDistanceValue(double attackAngleMaxDistanceValue) {
        this.attackAngleMaxDistanceValue = attackAngleMaxDistanceValue;
    }

    /**
     * Gets attack angle min distance value.
     *
     * @return the attack angle min distance value
     */
    public double getAttackAngleMinDistanceValue() {
        return attackAngleMinDistanceValue;
    }

    /**
     * Sets attack angle min distance value.
     *
     * @param attackAngleMinDistanceValue the attack angle min distance value
     */
    public void setAttackAngleMinDistanceValue(double attackAngleMinDistanceValue) {
        this.attackAngleMinDistanceValue = attackAngleMinDistanceValue;
    }

    /**
     * Gets attack angle value.
     *
     * @return the attack angle value
     */
    public double getAttackAngleValue() {
        return attackAngleValue;
    }

    /**
     * Sets attack angle value.
     *
     * @param attackAngleValue the attack angle value
     */
    public void setAttackAngleValue(double attackAngleValue) {
        this.attackAngleValue = attackAngleValue;
    }

    /**
     * Gets blades high.
     *
     * @return the blades high
     */
    public double getBladesHigh() {
        return bladesHigh;
    }

    /**
     * Sets blades high.
     *
     * @param bladesHigh the blades high
     */
    public void setBladesHigh(double bladesHigh) {
        this.bladesHigh = bladesHigh;
    }

    /**
     * Gets blades low.
     *
     * @return the blades low
     */
    public double getBladesLow() {
        return bladesLow;
    }

    /**
     * Sets blades low.
     *
     * @param bladesLow the blades low
     */
    public void setBladesLow(double bladesLow) {
        this.bladesLow = bladesLow;
    }

    /**
     * Gets blades zero.
     *
     * @return the blades zero
     */
    public double getBladesZero() {
        return bladesZero;
    }

    /**
     * Sets blades zero.
     *
     * @param bladesZero the blades zero
     */
    public void setBladesZero(double bladesZero) {
        this.bladesZero = bladesZero;
    }

    /**
     * Gets collective default.
     *
     * @return the collective default
     */
    public double getCollectiveDefault() {
        return collectiveDefault;
    }

    /**
     * Sets collective default.
     *
     * @param collectiveDefault the collective default
     */
    public void setCollectiveDefault(double collectiveDefault) {
        this.collectiveDefault = collectiveDefault;
    }

    /**
     * Gets collective max.
     *
     * @return the collective max
     */
    public double getCollectiveMax() {
        return collectiveMax;
    }

    /**
     * Sets collective max.
     *
     * @param collectiveMax the collective max
     */
    public void setCollectiveMax(double collectiveMax) {
        this.collectiveMax = collectiveMax;
    }

    /**
     * Gets collective min.
     *
     * @return the collective min
     */
    public double getCollectiveMin() {
        return collectiveMin;
    }

    /**
     * Sets collective min.
     *
     * @param collectiveMin the collective min
     */
    public void setCollectiveMin(double collectiveMin) {
        this.collectiveMin = collectiveMin;
    }

    /**
     * Gets cyclic default.
     *
     * @return the cyclic default
     */
    public double getCyclicDefault() {
        return cyclicDefault;
    }

    /**
     * Sets cyclic default.
     *
     * @param cyclicDefault the cyclic default
     */
    public void setCyclicDefault(double cyclicDefault) {
        this.cyclicDefault = cyclicDefault;
    }

    /**
     * Gets cyclic range.
     *
     * @return the cyclic range
     */
    public double getCyclicRange() {
        return cyclicRange;
    }

    /**
     * Sets cyclic range.
     *
     * @param cyclicRange the cyclic range
     */
    public void setCyclicRange(double cyclicRange) {
        this.cyclicRange = cyclicRange;
    }

    /**
     * Gets distance target velocity max value.
     *
     * @return the distance target velocity max value
     */
    public double getDistanceTargetVelocityMaxValue() {
        return distanceTargetVelocityMaxValue;
    }

    /**
     * Sets distance target velocity max value.
     *
     * @param distanceTargetVelocityMaxValue the distance target velocity max value
     */
    public void setDistanceTargetVelocityMaxValue(double distanceTargetVelocityMaxValue) {
        this.distanceTargetVelocityMaxValue = distanceTargetVelocityMaxValue;
    }

    /**
     * Gets distance to target max value.
     *
     * @return the distance to target max value
     */
    public double getDistanceToTargetMaxValue() {
        return distanceToTargetMaxValue;
    }

    /**
     * Sets distance to target max value.
     *
     * @param distanceToTargetMaxValue the distance to target max value
     */
    public void setDistanceToTargetMaxValue(double distanceToTargetMaxValue) {
        this.distanceToTargetMaxValue = distanceToTargetMaxValue;
    }

    /**
     * Gets distance to target min value.
     *
     * @return the distance to target min value
     */
    public double getDistanceToTargetMinValue() {
        return distanceToTargetMinValue;
    }

    /**
     * Sets distance to target min value.
     *
     * @param distanceToTargetMinValue the distance to target min value
     */
    public void setDistanceToTargetMinValue(double distanceToTargetMinValue) {
        this.distanceToTargetMinValue = distanceToTargetMinValue;
    }

    /**
     * Gets esc high value.
     *
     * @return the esc high value
     */
    public double getEscHighValue() {
        return escHighValue;
    }

    /**
     * Sets esc high value.
     *
     * @param escHighValue the esc high value
     */
    public void setEscHighValue(double escHighValue) {
        this.escHighValue = escHighValue;
    }

    /**
     * Gets esc low value.
     *
     * @return the esc low value
     */
    public double getEscLowValue() {
        return escLowValue;
    }

    /**
     * Sets esc low value.
     *
     * @param escLowValue the esc low value
     */
    public void setEscLowValue(double escLowValue) {
        this.escLowValue = escLowValue;
    }

    /**
     * Gets esc pulse max.
     *
     * @return the esc pulse max
     */
    public int getEscPulseMax() {
        return escPulseMax;
    }

    /**
     * Sets esc pulse max.
     *
     * @param escPulseMax the esc pulse max
     */
    public void setEscPulseMax(int escPulseMax) {
        this.escPulseMax = escPulseMax;
    }

    /**
     * Gets esc pulse min.
     *
     * @return the esc pulse min
     */
    public int getEscPulseMin() {
        return escPulseMin;
    }

    /**
     * Sets esc pulse min.
     *
     * @param escPulseMin the esc pulse min
     */
    public void setEscPulseMin(int escPulseMin) {
        this.escPulseMin = escPulseMin;
    }

    /**
     * Gets esc type.
     *
     * @return the esc type
     */
    public int getEscType() {
        return escType;
    }

    /**
     * Sets esc type.
     *
     * @param escType the esc type
     */
    public void setEscType(int escType) {
        this.escType = escType;
    }

    /**
     * Gets h 0 s 0 high.
     *
     * @return the h 0 s 0 high
     */
    public double getH0S0High() {
        return h0S0High;
    }

    /**
     * Sets h 0 s 0 high.
     *
     * @param h0S0High the h 0 s 0 high
     */
    public void setH0S0High(double h0S0High) {
        this.h0S0High = h0S0High;
    }

    /**
     * Gets h 0 s 0 low.
     *
     * @return the h 0 s 0 low
     */
    public double getH0S0Low() {
        return h0S0Low;
    }

    /**
     * Sets h 0 s 0 low.
     *
     * @param h0S0Low the h 0 s 0 low
     */
    public void setH0S0Low(double h0S0Low) {
        this.h0S0Low = h0S0Low;
    }

    /**
     * Gets h 0 s 0 zero.
     *
     * @return the h 0 s 0 zero
     */
    public double getH0S0Zero() {
        return h0S0Zero;
    }

    /**
     * Sets h 0 s 0 zero.
     *
     * @param h0S0Zero the h 0 s 0 zero
     */
    public void setH0S0Zero(double h0S0Zero) {
        this.h0S0Zero = h0S0Zero;
    }

    /**
     * Gets h 0 s 1 high.
     *
     * @return the h 0 s 1 high
     */
    public double getH0S1High() {
        return h0S1High;
    }

    /**
     * Sets h 0 s 1 high.
     *
     * @param h0S1High the h 0 s 1 high
     */
    public void setH0S1High(double h0S1High) {
        this.h0S1High = h0S1High;
    }

    /**
     * Gets h 0 s 1 low.
     *
     * @return the h 0 s 1 low
     */
    public double getH0S1Low() {
        return h0S1Low;
    }

    /**
     * Sets h 0 s 1 low.
     *
     * @param h0S1Low the h 0 s 1 low
     */
    public void setH0S1Low(double h0S1Low) {
        this.h0S1Low = h0S1Low;
    }

    /**
     * Gets h 0 s 1 zero.
     *
     * @return the h 0 s 1 zero
     */
    public double getH0S1Zero() {
        return h0S1Zero;
    }

    /**
     * Sets h 0 s 1 zero.
     *
     * @param h0S1Zero the h 0 s 1 zero
     */
    public void setH0S1Zero(double h0S1Zero) {
        this.h0S1Zero = h0S1Zero;
    }

    /**
     * Gets h 0 s 2 high.
     *
     * @return the h 0 s 2 high
     */
    public double getH0S2High() {
        return h0S2High;
    }

    /**
     * Sets h 0 s 2 high.
     *
     * @param h0S2High the h 0 s 2 high
     */
    public void setH0S2High(double h0S2High) {
        this.h0S2High = h0S2High;
    }

    /**
     * Gets h 0 s 2 low.
     *
     * @return the h 0 s 2 low
     */
    public double getH0S2Low() {
        return h0S2Low;
    }

    /**
     * Sets h 0 s 2 low.
     *
     * @param h0S2Low the h 0 s 2 low
     */
    public void setH0S2Low(double h0S2Low) {
        this.h0S2Low = h0S2Low;
    }

    /**
     * Gets h 0 s 2 zero.
     *
     * @return the h 0 s 2 zero
     */
    public double getH0S2Zero() {
        return h0S2Zero;
    }

    /**
     * Sets h 0 s 2 zero.
     *
     * @param h0S2Zero the h 0 s 2 zero
     */
    public void setH0S2Zero(double h0S2Zero) {
        this.h0S2Zero = h0S2Zero;
    }

    /**
     * Gets h 1 s 0 high.
     *
     * @return the h 1 s 0 high
     */
    public double getH1S0High() {
        return h1S0High;
    }

    /**
     * Sets h 1 s 0 high.
     *
     * @param h1S0High the h 1 s 0 high
     */
    public void setH1S0High(double h1S0High) {
        this.h1S0High = h1S0High;
    }

    /**
     * Gets h 1 s 0 low.
     *
     * @return the h 1 s 0 low
     */
    public double getH1S0Low() {
        return h1S0Low;
    }

    /**
     * Sets h 1 s 0 low.
     *
     * @param h1S0Low the h 1 s 0 low
     */
    public void setH1S0Low(double h1S0Low) {
        this.h1S0Low = h1S0Low;
    }

    /**
     * Gets h 1 s 0 zero.
     *
     * @return the h 1 s 0 zero
     */
    public double getH1S0Zero() {
        return h1S0Zero;
    }

    /**
     * Sets h 1 s 0 zero.
     *
     * @param h1S0Zero the h 1 s 0 zero
     */
    public void setH1S0Zero(double h1S0Zero) {
        this.h1S0Zero = h1S0Zero;
    }

    /**
     * Gets h 1 s 1 high.
     *
     * @return the h 1 s 1 high
     */
    public double getH1S1High() {
        return h1S1High;
    }

    /**
     * Sets h 1 s 1 high.
     *
     * @param h1S1High the h 1 s 1 high
     */
    public void setH1S1High(double h1S1High) {
        this.h1S1High = h1S1High;
    }

    /**
     * Gets h 1 s 1 low.
     *
     * @return the h 1 s 1 low
     */
    public double getH1S1Low() {
        return h1S1Low;
    }

    /**
     * Sets h 1 s 1 low.
     *
     * @param h1S1Low the h 1 s 1 low
     */
    public void setH1S1Low(double h1S1Low) {
        this.h1S1Low = h1S1Low;
    }

    /**
     * Gets h 1 s 1 zero.
     *
     * @return the h 1 s 1 zero
     */
    public double getH1S1Zero() {
        return h1S1Zero;
    }

    /**
     * Sets h 1 s 1 zero.
     *
     * @param h1S1Zero the h 1 s 1 zero
     */
    public void setH1S1Zero(double h1S1Zero) {
        this.h1S1Zero = h1S1Zero;
    }

    /**
     * Gets h 1 s 2 high.
     *
     * @return the h 1 s 2 high
     */
    public double getH1S2High() {
        return h1S2High;
    }

    /**
     * Sets h 1 s 2 high.
     *
     * @param h1S2High the h 1 s 2 high
     */
    public void setH1S2High(double h1S2High) {
        this.h1S2High = h1S2High;
    }

    /**
     * Gets h 1 s 2 low.
     *
     * @return the h 1 s 2 low
     */
    public double getH1S2Low() {
        return h1S2Low;
    }

    /**
     * Sets h 1 s 2 low.
     *
     * @param h1S2Low the h 1 s 2 low
     */
    public void setH1S2Low(double h1S2Low) {
        this.h1S2Low = h1S2Low;
    }

    /**
     * Gets h 1 s 2 zero.
     *
     * @return the h 1 s 2 zero
     */
    public double getH1S2Zero() {
        return h1S2Zero;
    }

    /**
     * Sets h 1 s 2 zero.
     *
     * @param h1S2Zero the h 1 s 2 zero
     */
    public void setH1S2Zero(double h1S2Zero) {
        this.h1S2Zero = h1S2Zero;
    }

    /**
     * Gets h 2 s 0 high.
     *
     * @return the h 2 s 0 high
     */
    public double getH2S0High() {
        return h2S0High;
    }

    /**
     * Sets h 2 s 0 high.
     *
     * @param h2S0High the h 2 s 0 high
     */
    public void setH2S0High(double h2S0High) {
        this.h2S0High = h2S0High;
    }

    /**
     * Gets h 2 s 0 low.
     *
     * @return the h 2 s 0 low
     */
    public double getH2S0Low() {
        return h2S0Low;
    }

    /**
     * Sets h 2 s 0 low.
     *
     * @param h2S0Low the h 2 s 0 low
     */
    public void setH2S0Low(double h2S0Low) {
        this.h2S0Low = h2S0Low;
    }

    /**
     * Gets h 2 s 0 zero.
     *
     * @return the h 2 s 0 zero
     */
    public double getH2S0Zero() {
        return h2S0Zero;
    }

    /**
     * Sets h 2 s 0 zero.
     *
     * @param h2S0Zero the h 2 s 0 zero
     */
    public void setH2S0Zero(double h2S0Zero) {
        this.h2S0Zero = h2S0Zero;
    }

    /**
     * Gets h 2 s 1 high.
     *
     * @return the h 2 s 1 high
     */
    public double getH2S1High() {
        return h2S1High;
    }

    /**
     * Sets h 2 s 1 high.
     *
     * @param h2S1High the h 2 s 1 high
     */
    public void setH2S1High(double h2S1High) {
        this.h2S1High = h2S1High;
    }

    /**
     * Gets h 2 s 1 low.
     *
     * @return the h 2 s 1 low
     */
    public double getH2S1Low() {
        return h2S1Low;
    }

    /**
     * Sets h 2 s 1 low.
     *
     * @param h2S1Low the h 2 s 1 low
     */
    public void setH2S1Low(double h2S1Low) {
        this.h2S1Low = h2S1Low;
    }

    /**
     * Gets h 2 s 1 zero.
     *
     * @return the h 2 s 1 zero
     */
    public double getH2S1Zero() {
        return h2S1Zero;
    }

    /**
     * Sets h 2 s 1 zero.
     *
     * @param h2S1Zero the h 2 s 1 zero
     */
    public void setH2S1Zero(double h2S1Zero) {
        this.h2S1Zero = h2S1Zero;
    }

    /**
     * Gets h 2 s 2 high.
     *
     * @return the h 2 s 2 high
     */
    public double getH2S2High() {
        return h2S2High;
    }

    /**
     * Sets h 2 s 2 high.
     *
     * @param h2S2High the h 2 s 2 high
     */
    public void setH2S2High(double h2S2High) {
        this.h2S2High = h2S2High;
    }

    /**
     * Gets h 2 s 2 low.
     *
     * @return the h 2 s 2 low
     */
    public double getH2S2Low() {
        return h2S2Low;
    }

    /**
     * Sets h 2 s 2 low.
     *
     * @param h2S2Low the h 2 s 2 low
     */
    public void setH2S2Low(double h2S2Low) {
        this.h2S2Low = h2S2Low;
    }

    /**
     * Gets h 2 s 2 zero.
     *
     * @return the h 2 s 2 zero
     */
    public double getH2S2Zero() {
        return h2S2Zero;
    }

    /**
     * Sets h 2 s 2 zero.
     *
     * @param h2S2Zero the h 2 s 2 zero
     */
    public void setH2S2Zero(double h2S2Zero) {
        this.h2S2Zero = h2S2Zero;
    }

    /**
     * Gets h 3 s 0 high.
     *
     * @return the h 3 s 0 high
     */
    public double getH3S0High() {
        return h3S0High;
    }

    /**
     * Sets h 3 s 0 high.
     *
     * @param h3S0High the h 3 s 0 high
     */
    public void setH3S0High(double h3S0High) {
        this.h3S0High = h3S0High;
    }

    /**
     * Gets h 3 s 0 low.
     *
     * @return the h 3 s 0 low
     */
    public double getH3S0Low() {
        return h3S0Low;
    }

    /**
     * Sets h 3 s 0 low.
     *
     * @param h3S0Low the h 3 s 0 low
     */
    public void setH3S0Low(double h3S0Low) {
        this.h3S0Low = h3S0Low;
    }

    /**
     * Gets h 3 s 0 zero.
     *
     * @return the h 3 s 0 zero
     */
    public double getH3S0Zero() {
        return h3S0Zero;
    }

    /**
     * Sets h 3 s 0 zero.
     *
     * @param h3S0Zero the h 3 s 0 zero
     */
    public void setH3S0Zero(double h3S0Zero) {
        this.h3S0Zero = h3S0Zero;
    }

    /**
     * Gets h 3 s 1 high.
     *
     * @return the h 3 s 1 high
     */
    public double getH3S1High() {
        return h3S1High;
    }

    /**
     * Sets h 3 s 1 high.
     *
     * @param h3S1High the h 3 s 1 high
     */
    public void setH3S1High(double h3S1High) {
        this.h3S1High = h3S1High;
    }

    /**
     * Gets h 3 s 1 low.
     *
     * @return the h 3 s 1 low
     */
    public double getH3S1Low() {
        return h3S1Low;
    }

    /**
     * Sets h 3 s 1 low.
     *
     * @param h3S1Low the h 3 s 1 low
     */
    public void setH3S1Low(double h3S1Low) {
        this.h3S1Low = h3S1Low;
    }

    /**
     * Gets h 3 s 1 zero.
     *
     * @return the h 3 s 1 zero
     */
    public double getH3S1Zero() {
        return h3S1Zero;
    }

    /**
     * Sets h 3 s 1 zero.
     *
     * @param h3S1Zero the h 3 s 1 zero
     */
    public void setH3S1Zero(double h3S1Zero) {
        this.h3S1Zero = h3S1Zero;
    }

    /**
     * Gets h 3 s 2 high.
     *
     * @return the h 3 s 2 high
     */
    public double getH3S2High() {
        return h3S2High;
    }

    /**
     * Sets h 3 s 2 high.
     *
     * @param h3S2High the h 3 s 2 high
     */
    public void setH3S2High(double h3S2High) {
        this.h3S2High = h3S2High;
    }

    /**
     * Gets h 3 s 2 low.
     *
     * @return the h 3 s 2 low
     */
    public double getH3S2Low() {
        return h3S2Low;
    }

    /**
     * Sets h 3 s 2 low.
     *
     * @param h3S2Low the h 3 s 2 low
     */
    public void setH3S2Low(double h3S2Low) {
        this.h3S2Low = h3S2Low;
    }

    /**
     * Gets h 3 s 2 zero.
     *
     * @return the h 3 s 2 zero
     */
    public double getH3S2Zero() {
        return h3S2Zero;
    }

    /**
     * Sets h 3 s 2 zero.
     *
     * @param h3S2Zero the h 3 s 2 zero
     */
    public void setH3S2Zero(double h3S2Zero) {
        this.h3S2Zero = h3S2Zero;
    }

    /**
     * Gets heading delta max value.
     *
     * @return the heading delta max value
     */
    public double getHeadingDeltaMaxValue() {
        return headingDeltaMaxValue;
    }

    /**
     * Sets heading delta max value.
     *
     * @param headingDeltaMaxValue the heading delta max value
     */
    public void setHeadingDeltaMaxValue(double headingDeltaMaxValue) {
        this.headingDeltaMaxValue = headingDeltaMaxValue;
    }

    /**
     * Gets heading delta min value.
     *
     * @return the heading delta min value
     */
    public double getHeadingDeltaMinValue() {
        return headingDeltaMinValue;
    }

    /**
     * Sets heading delta min value.
     *
     * @param headingDeltaMinValue the heading delta min value
     */
    public void setHeadingDeltaMinValue(double headingDeltaMinValue) {
        this.headingDeltaMinValue = headingDeltaMinValue;
    }

    /**
     * Gets heading target velocity max value.
     *
     * @return the heading target velocity max value
     */
    public double getHeadingTargetVelocityMaxValue() {
        return headingTargetVelocityMaxValue;
    }

    /**
     * Sets heading target velocity max value.
     *
     * @param headingTargetVelocityMaxValue the heading target velocity max value
     */
    public void setHeadingTargetVelocityMaxValue(double headingTargetVelocityMaxValue) {
        this.headingTargetVelocityMaxValue = headingTargetVelocityMaxValue;
    }

    /**
     * Gets lift off target altitude delta value.
     *
     * @return the lift off target altitude delta value
     */
    public double getLiftOffTargetAltitudeDeltaValue() {
        return liftOffTargetAltitudeDeltaValue;
    }

    /**
     * Sets lift off target altitude delta value.
     *
     * @param liftOffTargetAltitudeDeltaValue the lift off target altitude delta value
     */
    public void setLiftOffTargetAltitudeDeltaValue(double liftOffTargetAltitudeDeltaValue) {
        this.liftOffTargetAltitudeDeltaValue = liftOffTargetAltitudeDeltaValue;
    }

    /**
     * Gets orientation min distance value.
     *
     * @return the orientation min distance value
     */
    public double getOrientationMinDistanceValue() {
        return orientationMinDistanceValue;
    }

    /**
     * Sets orientation min distance value.
     *
     * @param orientationMinDistanceValue the orientation min distance value
     */
    public void setOrientationMinDistanceValue(double orientationMinDistanceValue) {
        this.orientationMinDistanceValue = orientationMinDistanceValue;
    }

    /**
     * Gets pitch delta max value.
     *
     * @return the pitch delta max value
     */
    public double getPitchDeltaMaxValue() {
        return pitchDeltaMaxValue;
    }

    /**
     * Sets pitch delta max value.
     *
     * @param pitchDeltaMaxValue the pitch delta max value
     */
    public void setPitchDeltaMaxValue(double pitchDeltaMaxValue) {
        this.pitchDeltaMaxValue = pitchDeltaMaxValue;
    }

    /**
     * Gets pitch delta min value.
     *
     * @return the pitch delta min value
     */
    public double getPitchDeltaMinValue() {
        return pitchDeltaMinValue;
    }

    /**
     * Sets pitch delta min value.
     *
     * @param pitchDeltaMinValue the pitch delta min value
     */
    public void setPitchDeltaMinValue(double pitchDeltaMinValue) {
        this.pitchDeltaMinValue = pitchDeltaMinValue;
    }

    /**
     * Gets pitch target velocity max value.
     *
     * @return the pitch target velocity max value
     */
    public double getPitchTargetVelocityMaxValue() {
        return pitchTargetVelocityMaxValue;
    }

    /**
     * Sets pitch target velocity max value.
     *
     * @param pitchTargetVelocityMaxValue the pitch target velocity max value
     */
    public void setPitchTargetVelocityMaxValue(double pitchTargetVelocityMaxValue) {
        this.pitchTargetVelocityMaxValue = pitchTargetVelocityMaxValue;
    }

    /**
     * Gets roll delta max value.
     *
     * @return the roll delta max value
     */
    public double getRollDeltaMaxValue() {
        return rollDeltaMaxValue;
    }

    /**
     * Sets roll delta max value.
     *
     * @param rollDeltaMaxValue the roll delta max value
     */
    public void setRollDeltaMaxValue(double rollDeltaMaxValue) {
        this.rollDeltaMaxValue = rollDeltaMaxValue;
    }

    /**
     * Gets roll delta min value.
     *
     * @return the roll delta min value
     */
    public double getRollDeltaMinValue() {
        return rollDeltaMinValue;
    }

    /**
     * Sets roll delta min value.
     *
     * @param rollDeltaMinValue the roll delta min value
     */
    public void setRollDeltaMinValue(double rollDeltaMinValue) {
        this.rollDeltaMinValue = rollDeltaMinValue;
    }

    /**
     * Gets roll target velocity max value.
     *
     * @return the roll target velocity max value
     */
    public double getRollTargetVelocityMaxValue() {
        return rollTargetVelocityMaxValue;
    }

    /**
     * Sets roll target velocity max value.
     *
     * @param rollTargetVelocityMaxValue the roll target velocity max value
     */
    public void setRollTargetVelocityMaxValue(double rollTargetVelocityMaxValue) {
        this.rollTargetVelocityMaxValue = rollTargetVelocityMaxValue;
    }

    /**
     * Gets servo degree max.
     *
     * @return the servo degree max
     */
    public double getServoDegreeMax() {
        return servoDegreeMax;
    }

    /**
     * Sets servo degree max.
     *
     * @param servoDegreeMax the servo degree max
     */
    public void setServoDegreeMax(double servoDegreeMax) {
        this.servoDegreeMax = servoDegreeMax;
    }

    /**
     * Gets servo degree min.
     *
     * @return the servo degree min
     */
    public double getServoDegreeMin() {
        return servoDegreeMin;
    }

    /**
     * Sets servo degree min.
     *
     * @param servoDegreeMin the servo degree min
     */
    public void setServoDegreeMin(double servoDegreeMin) {
        this.servoDegreeMin = servoDegreeMin;
    }

    /**
     * Gets heli 0 servo offset left.
     *
     * @return the heli 0 servo offset left
     */
    public double getServoOffsetLeft0() {
        return servoOffsetLeft0;
    }

    /**
     * Sets heli 0 servo offset left.
     *
     * @param servoOffsetLeft0 the heli 0 servo offset left
     */
    public void setServoOffsetLeft0(double servoOffsetLeft0) {
        this.servoOffsetLeft0 = servoOffsetLeft0;
    }

    /**
     * Gets heli 0 servo offset pitch.
     *
     * @return the heli 0 servo offset pitch
     */
    public double getServoOffsetPitch0() {
        return servoOffsetPitch0;
    }

    /**
     * Sets heli 0 servo offset pitch.
     *
     * @param servoOffsetPitch0 the heli 0 servo offset pitch
     */
    public void setServoOffsetPitch0(double servoOffsetPitch0) {
        this.servoOffsetPitch0 = servoOffsetPitch0;
    }

    /**
     * Gets heli 0 servo offset right.
     *
     * @return the heli 0 servo offset right
     */
    public double getServoOffsetRight0() {
        return servoOffsetRight0;
    }

    /**
     * Sets heli 0 servo offset right.
     *
     * @param servoOffsetRight0 the heli 0 servo offset right
     */
    public void setServoOffsetRight0(double servoOffsetRight0) {
        this.servoOffsetRight0 = servoOffsetRight0;
    }

    /**
     * Gets heli 1 servo offset left.
     *
     * @return the heli 1 servo offset left
     */
    public double getServoOffsetLeft1() {
        return servoOffsetLeft1;
    }

    /**
     * Sets heli 1 servo offset left.
     *
     * @param servoOffsetLeft1 the heli 1 servo offset left
     */
    public void setServoOffsetLeft1(double servoOffsetLeft1) {
        this.servoOffsetLeft1 = servoOffsetLeft1;
    }

    /**
     * Gets heli 1 servo offset pitch.
     *
     * @return the heli 1 servo offset pitch
     */
    public double getServoOffsetPitch1() {
        return servoOffsetPitch1;
    }

    /**
     * Sets heli 1 servo offset pitch.
     *
     * @param servoOffsetPitch1 the heli 1 servo offset pitch
     */
    public void setServoOffsetPitch1(double servoOffsetPitch1) {
        this.servoOffsetPitch1 = servoOffsetPitch1;
    }

    /**
     * Gets heli 1 servo offset right.
     *
     * @return the heli 1 servo offset right
     */
    public double getServoOffsetRight1() {
        return servoOffsetRight1;
    }

    /**
     * Sets heli 1 servo offset right.
     *
     * @param servoOffsetRight1 the heli 1 servo offset right
     */
    public void setServoOffsetRight1(double servoOffsetRight1) {
        this.servoOffsetRight1 = servoOffsetRight1;
    }
    /**
     * Gets heli 2 servo offset left.
     *
     * @return the heli 2 servo offset left
     */
    public double getServoOffsetLeft2() {
        return servoOffsetLeft2;
    }

    /**
     * Sets heli 2 servo offset left.
     *
     * @param servoOffsetLeft2 the heli 2 servo offset left
     */
    public void setServoOffsetLeft2(double servoOffsetLeft2) {
        this.servoOffsetLeft2 = servoOffsetLeft2;
    }

    /**
     * Gets heli 2 servo offset pitch.
     *
     * @return the heli 2 servo offset pitch
     */
    public double getServoOffsetPitch2() {
        return servoOffsetPitch2;
    }

    /**
     * Sets heli 2 servo offset pitch.
     *
     * @param servoOffsetPitch2 the heli 2 servo offset pitch
     */
    public void setServoOffsetPitch2(double servoOffsetPitch2) {
        this.servoOffsetPitch2 = servoOffsetPitch2;
    }

    /**
     * Gets heli 2 servo offset right.
     *
     * @return the heli 2 servo offset right
     */
    public double getServoOffsetRight2() {
        return servoOffsetRight2;
    }

    /**
     * Sets heli 2 servo offset right.
     *
     * @param servoOffsetRight2 the heli 2 servo offset right
     */
    public void setServoOffsetRight2(double servoOffsetRight2) {
        this.servoOffsetRight2 = servoOffsetRight2;
    }

    /**
     * Gets heli 3 servo offset left.
     *
     * @return the heli 3 servo offset left
     */
    public double getServoOffsetLeft3() {
        return servoOffsetLeft3;
    }

    /**
     * Sets heli 3 servo offset left.
     *
     * @param servoOffsetLeft3 the heli 3 servo offset left
     */
    public void setServoOffsetLeft3(double servoOffsetLeft3) {
        this.servoOffsetLeft3 = servoOffsetLeft3;
    }


    /**
     * Gets heli 3 servo offset pitch.
     *
     * @return the heli 3 servo offset pitch
     */
    public double getServoOffsetPitch3() {
        return servoOffsetPitch3;
    }

    /**
     * Sets heli 3 servo offset pitch.
     *
     * @param servoOffsetPitch3 the heli 3 servo offset pitch
     */
    public void setServoOffsetPitch3(double servoOffsetPitch3) {
        this.servoOffsetPitch3 = servoOffsetPitch3;
    }

    /**
     * Gets heli 3 servo offset right.
     *
     * @return the heli 3 servo offset right
     */
    public double getServoOffsetRight3() {
        return servoOffsetRight3;
    }

    /**
     * Sets heli 3 servo offset right.
     *
     * @param servoOffsetRight3 the heli 3 servo offset right
     */
    public void setServoOffsetRight3(double servoOffsetRight3) {
        this.servoOffsetRight3 = servoOffsetRight3;
    }

    /**
     * Gets servo pulse max.
     *
     * @return the servo pulse max
     */
    public int getServoPulseMax() {
        return servoPulseMax;
    }

    /**
     * Sets servo pulse max.
     *
     * @param servoPulseMax the servo pulse max
     */
    public void setServoPulseMax(int servoPulseMax) {
        this.servoPulseMax = servoPulseMax;
    }

    /**
     * Gets servo pulse min.
     *
     * @return the servo pulse min
     */
    public int getServoPulseMin() {
        return servoPulseMin;
    }

    /**
     * Sets servo pulse min.
     *
     * @param servoPulseMin the servo pulse min
     */
    public void setServoPulseMin(int servoPulseMin) {
        this.servoPulseMin = servoPulseMin;
    }

    /**
     * Gets servo sign left.
     *
     * @return the servo sign left
     */
    public int getServoSignLeft() {
        return servoSignLeft;
    }

    /**
     * Sets servo sign left.
     *
     * @param servoSignLeft the servo sign left
     */
    public void setServoSignLeft(int servoSignLeft) {
        this.servoSignLeft = servoSignLeft;
    }

    /**
     * Gets servo sign pitch.
     *
     * @return the servo sign pitch
     */
    public int getServoSignPitch() {
        return servoSignPitch;
    }

    /**
     * Sets servo sign pitch.
     *
     * @param servoSignPitch the servo sign pitch
     */
    public void setServoSignPitch(int servoSignPitch) {
        this.servoSignPitch = servoSignPitch;
    }

    /**
     * Gets servo sign right.
     *
     * @return the servo sign right
     */
    public int getServoSignRight() {
        return servoSignRight;
    }

    /**
     * Sets servo sign right.
     *
     * @param servoSignRight the servo sign right
     */
    public void setServoSignRight(int servoSignRight) {
        this.servoSignRight = servoSignRight;
    }

    /**
     * Gets target altitude velocity alpha.
     *
     * @return the target altitude velocity alpha
     */
    public double getTargetAltitudeVelocityAlpha() {
        return targetAltitudeVelocityAlpha;
    }

    /**
     * Sets target altitude velocity alpha.
     *
     * @param targetAltitudeVelocityAlpha the target altitude velocity alpha
     */
    public void setTargetAltitudeVelocityAlpha(double targetAltitudeVelocityAlpha) {
        this.targetAltitudeVelocityAlpha = targetAltitudeVelocityAlpha;
    }

    /**
     * Gets target heading velocity alpha.
     *
     * @return the target heading velocity alpha
     */
    public double getTargetHeadingVelocityAlpha() {
        return targetHeadingVelocityAlpha;
    }

    /**
     * Sets target heading velocity alpha.
     *
     * @param targetHeadingVelocityAlpha the target heading velocity alpha
     */
    public void setTargetHeadingVelocityAlpha(double targetHeadingVelocityAlpha) {
        this.targetHeadingVelocityAlpha = targetHeadingVelocityAlpha;
    }

    /**
     * Gets target pitch velocity alpha.
     *
     * @return the target pitch velocity alpha
     */
    public double getTargetPitchVelocityAlpha() {
        return targetPitchVelocityAlpha;
    }

    /**
     * Sets target pitch velocity alpha.
     *
     * @param targetPitchVelocityAlpha the target pitch velocity alpha
     */
    public void setTargetPitchVelocityAlpha(double targetPitchVelocityAlpha) {
        this.targetPitchVelocityAlpha = targetPitchVelocityAlpha;
    }

    /**
     * Gets target roll velocity alpha.
     *
     * @return the target roll velocity alpha
     */
    public double getTargetRollVelocityAlpha() {
        return targetRollVelocityAlpha;
    }

    /**
     * Sets target roll velocity alpha.
     *
     * @param targetRollVelocityAlpha the target roll velocity alpha
     */
    public void setTargetRollVelocityAlpha(double targetRollVelocityAlpha) {
        this.targetRollVelocityAlpha = targetRollVelocityAlpha;
    }

    /**
     * Gets target velocity full speed.
     *
     * @return the target velocity full speed
     */
    public double getTargetVelocityFullSpeed() {
        return targetVelocityFullSpeed;
    }

    /**
     * Sets target velocity full speed.
     *
     * @param targetVelocityFullSpeed the target velocity full speed
     */
    public void setTargetVelocityFullSpeed(double targetVelocityFullSpeed) {
        this.targetVelocityFullSpeed = targetVelocityFullSpeed;
    }

    /**
     * Gets target velocity keep still.
     *
     * @return the target velocity keep still
     */
    public double getTargetVelocityKeepStill() {
        return targetVelocityKeepStill;
    }

    /**
     * Sets target velocity keep still.
     *
     * @param targetVelocityKeepStill the target velocity keep still
     */
    public void setTargetVelocityKeepStill(double targetVelocityKeepStill) {
        this.targetVelocityKeepStill = targetVelocityKeepStill;
    }

    /**
     * Gets start mode.
     *
     * @return the start mode
     */
    public int getStartMode() {
        return startMode;
    }

    /**
     * Sets start mode.
     *
     * @param startMode the start mode
     */
    public void setStartMode(int startMode) {
        this.startMode = startMode;
    }

    /**
     * Gets land mode time required at min altitude.
     *
     * @return the land mode time required at min altitude
     */
    public int getLandModeTimeRequiredAtMinAltitude() {
        return landModeTimeRequiredAtMinAltitude;
    }

    /**
     * Sets land mode time required at min altitude.
     *
     * @param landModeTimeRequiredAtMinAltitude the land mode time required at min altitude
     */
    public void setLandModeTimeRequiredAtMinAltitude(int landModeTimeRequiredAtMinAltitude) {
        this.landModeTimeRequiredAtMinAltitude = landModeTimeRequiredAtMinAltitude;
    }

    /**
     * Gets rotation mode.
     *
     * @return the rotation mode
     */
    public int getRotationMode() {
        return rotationMode;
    }

    /**
     * Sets rotation mode.
     *
     * @param rotationMode the normal rotation mode
     */
    public void setRotationMode(int rotationMode) {
        this.rotationMode = rotationMode;
    }

    /**
     * Gets cyclic heading alpha.
     *
     * @return the cyclic heading alpha
     */
    public double getCyclicHeadingAlpha() {
        return cyclicHeadingAlpha;
    }

    /**
     * Sets cyclic heading alpha.
     *
     * @param cyclicHeadingAlpha the cyclic heading alpha
     */
    public void setCyclicHeadingAlpha(double cyclicHeadingAlpha) {
        this.cyclicHeadingAlpha = cyclicHeadingAlpha;
    }

    /**
     * Gets heli startup mode number steps.
     *
     * @return the heli startup mode number steps
     */
    public int getHeliStartupModeNumberSteps() {
        return heliStartupModeNumberSteps;
    }

    /**
     * Sets heli startup mode number steps.
     *
     * @param heliStartupModeNumberSteps the heli startup mode number steps
     */
    public void setHeliStartupModeNumberSteps(int heliStartupModeNumberSteps) {
        this.heliStartupModeNumberSteps = heliStartupModeNumberSteps;
    }

    /**
     * Gets heli startup mode step tick.
     *
     * @return the heli startup mode step tick
     */
    public int getHeliStartupModeStepTick() {
        return heliStartupModeStepTick;
    }

    /**
     * Sets heli startup mode step tick.
     *
     * @param heliStartupModeStepTick the heli startup mode step tick
     */
    public void setHeliStartupModeStepTick(int heliStartupModeStepTick) {
        this.heliStartupModeStepTick = heliStartupModeStepTick;
    }

    /**
     * Gets floid model esc collective calc midpoint.
     *
     * @return the floid model esc collective calc midpoint
     */
    public double getFloidModelEscCollectiveCalcMidpoint() {
        return floidModelEscCollectiveCalcMidpoint;
    }

    /**
     * Sets floid model esc collective calc midpoint.
     *
     * @param floidModelEscCollectiveCalcMidpoint the floid model esc collective calc midpoint
     */
    public void setFloidModelEscCollectiveCalcMidpoint(double floidModelEscCollectiveCalcMidpoint) {
        this.floidModelEscCollectiveCalcMidpoint = floidModelEscCollectiveCalcMidpoint;
    }

    /**
     * Gets floid model esc collective low value.
     *
     * @return the floid model esc collective low value
     */
    public double getFloidModelEscCollectiveLowValue() {
        return floidModelEscCollectiveLowValue;
    }

    /**
     * Sets floid model esc collective low value.
     *
     * @param floidModelEscCollectiveLowValue the floid model esc collective low value
     */
    public void setFloidModelEscCollectiveLowValue(double floidModelEscCollectiveLowValue) {
        this.floidModelEscCollectiveLowValue = floidModelEscCollectiveLowValue;
    }

    /**
     * Gets floid model esc collective mid value.
     *
     * @return the floid model esc collective mid value
     */
    public double getFloidModelEscCollectiveMidValue() {
        return floidModelEscCollectiveMidValue;
    }

    /**
     * Sets floid model esc collective mid value.
     *
     * @param floidModelEscCollectiveMidValue the floid model esc collective mid value
     */
    public void setFloidModelEscCollectiveMidValue(double floidModelEscCollectiveMidValue) {
        this.floidModelEscCollectiveMidValue = floidModelEscCollectiveMidValue;
    }

    /**
     * Gets floid model esc collective high value.
     *
     * @return the floid model esc collective high value
     */
    public double getFloidModelEscCollectiveHighValue() {
        return floidModelEscCollectiveHighValue;
    }

    /**
     * Sets floid model esc collective high value.
     *
     * @param floidModelEscCollectiveHighValue the floid model esc collective high value
     */
    public void setFloidModelEscCollectiveHighValue(double floidModelEscCollectiveHighValue) {
        this.floidModelEscCollectiveHighValue = floidModelEscCollectiveHighValue;
    }

    public double getVelocityDeltaCyclicAlphaScale() {
        return velocityDeltaCyclicAlphaScale;
    }

    public void setVelocityDeltaCyclicAlphaScale(double velocityDeltaCyclicAlphaScale) {
        this.velocityDeltaCyclicAlphaScale = velocityDeltaCyclicAlphaScale;
    }

    public double getAccelerationMultiplierScale() {
        return accelerationMultiplierScale;
    }

    public void setAccelerationMultiplierScale(double accelerationMultiplierScale) {
        this.accelerationMultiplierScale = accelerationMultiplierScale;
    }
}
