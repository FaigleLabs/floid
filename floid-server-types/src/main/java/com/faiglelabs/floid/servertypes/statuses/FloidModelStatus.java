/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.statuses;

import com.faiglelabs.floid.database.FloidDatabaseTables;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * The floid model status
 */
@Entity
@Table(name = FloidDatabaseTables.FLOID_MODEL_STATUS,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidUuidIndex", columnList = "floidUuid"),
        @Index(name="timestampIndex", columnList = "timestamp"),
        @Index(name="typeIndex", columnList = "type"),
        @Index(name="floidMessageNumberIndex", columnList = "floidMessageNumber")
})
@SuppressWarnings("unused")
public class FloidModelStatus extends FloidMessage implements Cloneable {
    @Override
    public FloidModelStatus clone() throws CloneNotSupportedException {
        return (FloidModelStatus) super.clone();
    }


    /**
     * Create a floid model status
     */
    public FloidModelStatus() {
        type = getClass().getName();
        floidMessageNumber = getNextFloidMessageNumber();
    }

    /**
     * Create a floid model status
     */
    public FloidModelStatus(
            Long id,
            int floidId,
            String floidUuid,
            Long floidMessageNumber,
            long timestamp,
            double collectiveDeltaOrientationH0,
            double collectiveDeltaOrientationH1,
            double collectiveDeltaOrientationH2,
            double collectiveDeltaOrientationH3,
            double collectiveDeltaAltitudeH0,
            double collectiveDeltaAltitudeH1,
            double collectiveDeltaAltitudeH2,
            double collectiveDeltaAltitudeH3,
            double collectiveDeltaHeadingH0,
            double collectiveDeltaHeadingH1,
            double collectiveDeltaHeadingH2,
            double collectiveDeltaHeadingH3,
            double collectiveDeltaTotalH0,
            double collectiveDeltaTotalH1,
            double collectiveDeltaTotalH2,
            double collectiveDeltaTotalH3,
            double cyclicValueH0S0,
            double cyclicValueH0S1,
            double cyclicValueH0S2,
            double cyclicValueH1S0,
            double cyclicValueH1S1,
            double cyclicValueH1S2,
            double cyclicValueH2S0,
            double cyclicValueH2S1,
            double cyclicValueH2S2,
            double cyclicValueH3S0,
            double cyclicValueH3S1,
            double cyclicValueH3S2,
            double collectiveValueH0,
            double collectiveValueH1,
            double collectiveValueH2,
            double collectiveValueH3,
            double calculatedCyclicBladePitchH0S0,
            double calculatedCyclicBladePitchH0S1,
            double calculatedCyclicBladePitchH0S2,
            double calculatedCyclicBladePitchH1S0,
            double calculatedCyclicBladePitchH1S1,
            double calculatedCyclicBladePitchH1S2,
            double calculatedCyclicBladePitchH2S0,
            double calculatedCyclicBladePitchH2S1,
            double calculatedCyclicBladePitchH2S2,
            double calculatedCyclicBladePitchH3S0,
            double calculatedCyclicBladePitchH3S1,
            double calculatedCyclicBladePitchH3S2,
            double calculatedCollectiveBladePitchH0,
            double calculatedCollectiveBladePitchH1,
            double calculatedCollectiveBladePitchH2,
            double calculatedCollectiveBladePitchH3,
            double calculatedBladePitchH0S0,
            double calculatedBladePitchH0S1,
            double calculatedBladePitchH0S2,
            double calculatedBladePitchH1S0,
            double calculatedBladePitchH1S1,
            double calculatedBladePitchH1S2,
            double calculatedBladePitchH2S0,
            double calculatedBladePitchH2S1,
            double calculatedBladePitchH2S2,
            double calculatedBladePitchH3S0,
            double calculatedBladePitchH3S1,
            double calculatedBladePitchH3S2,
            double calculatedServoDegreesH0S0,
            double calculatedServoDegreesH0S1,
            double calculatedServoDegreesH0S2,
            double calculatedServoDegreesH1S0,
            double calculatedServoDegreesH1S1,
            double calculatedServoDegreesH1S2,
            double calculatedServoDegreesH2S0,
            double calculatedServoDegreesH2S1,
            double calculatedServoDegreesH2S2,
            double calculatedServoDegreesH3S0,
            double calculatedServoDegreesH3S1,
            double calculatedServoDegreesH3S2,
            int calculatedServoPulseH0S0,
            int calculatedServoPulseH0S1,
            int calculatedServoPulseH0S2,
            int calculatedServoPulseH1S0,
            int calculatedServoPulseH1S1,
            int calculatedServoPulseH1S2,
            int calculatedServoPulseH2S0,
            int calculatedServoPulseH2S1,
            int calculatedServoPulseH2S2,
            int calculatedServoPulseH3S0,
            int calculatedServoPulseH3S1,
            int calculatedServoPulseH3S2,
            double cyclicHeading,
            double velocityCyclicAlpha
    ) {
        this.id = id;
        this.floidId = floidId;
        this.floidUuid = floidUuid;
        this.floidMessageNumber = floidMessageNumber;
        this.type = getClass().getName();
        this.timestamp = timestamp;
        this.collectiveDeltaOrientationH0 = collectiveDeltaOrientationH0;
        this.collectiveDeltaOrientationH1 = collectiveDeltaOrientationH1;
        this.collectiveDeltaOrientationH2 = collectiveDeltaOrientationH2;
        this.collectiveDeltaOrientationH3 = collectiveDeltaOrientationH3;
        this.collectiveDeltaAltitudeH0 = collectiveDeltaAltitudeH0;
        this.collectiveDeltaAltitudeH1 = collectiveDeltaAltitudeH1;
        this.collectiveDeltaAltitudeH2 = collectiveDeltaAltitudeH2;
        this.collectiveDeltaAltitudeH3 = collectiveDeltaAltitudeH3;
        this.collectiveDeltaHeadingH0 = collectiveDeltaHeadingH0;
        this.collectiveDeltaHeadingH1 = collectiveDeltaHeadingH1;
        this.collectiveDeltaHeadingH2 = collectiveDeltaHeadingH2;
        this.collectiveDeltaHeadingH3 = collectiveDeltaHeadingH3;
        this.collectiveDeltaTotalH0 = collectiveDeltaTotalH0;
        this.collectiveDeltaTotalH1 = collectiveDeltaTotalH1;
        this.collectiveDeltaTotalH2 = collectiveDeltaTotalH2;
        this.collectiveDeltaTotalH3 = collectiveDeltaTotalH3;
        this.cyclicValueH0S0 = cyclicValueH0S0;
        this.cyclicValueH0S1 = cyclicValueH0S1;
        this.cyclicValueH0S2 = cyclicValueH0S2;
        this.cyclicValueH1S0 = cyclicValueH1S0;
        this.cyclicValueH1S1 = cyclicValueH1S1;
        this.cyclicValueH1S2 = cyclicValueH1S2;
        this.cyclicValueH2S0 = cyclicValueH2S0;
        this.cyclicValueH2S1 = cyclicValueH2S1;
        this.cyclicValueH2S2 = cyclicValueH2S2;
        this.cyclicValueH3S0 = cyclicValueH3S0;
        this.cyclicValueH3S1 = cyclicValueH3S1;
        this.cyclicValueH3S2 = cyclicValueH3S2;
        this.collectiveValueH0 = collectiveValueH0;
        this.collectiveValueH1 = collectiveValueH1;
        this.collectiveValueH2 = collectiveValueH2;
        this.collectiveValueH3 = collectiveValueH3;
        this.calculatedCyclicBladePitchH0S0 = calculatedCyclicBladePitchH0S0;
        this.calculatedCyclicBladePitchH0S1 = calculatedCyclicBladePitchH0S1;
        this.calculatedCyclicBladePitchH0S2 = calculatedCyclicBladePitchH0S2;
        this.calculatedCyclicBladePitchH1S0 = calculatedCyclicBladePitchH1S0;
        this.calculatedCyclicBladePitchH1S1 = calculatedCyclicBladePitchH1S1;
        this.calculatedCyclicBladePitchH1S2 = calculatedCyclicBladePitchH1S2;
        this.calculatedCyclicBladePitchH2S0 = calculatedCyclicBladePitchH2S0;
        this.calculatedCyclicBladePitchH2S1 = calculatedCyclicBladePitchH2S1;
        this.calculatedCyclicBladePitchH2S2 = calculatedCyclicBladePitchH2S2;
        this.calculatedCyclicBladePitchH3S0 = calculatedCyclicBladePitchH3S0;
        this.calculatedCyclicBladePitchH3S1 = calculatedCyclicBladePitchH3S1;
        this.calculatedCyclicBladePitchH3S2 = calculatedCyclicBladePitchH3S2;
        this.calculatedCollectiveBladePitchH0 = calculatedCollectiveBladePitchH0;
        this.calculatedCollectiveBladePitchH1 = calculatedCollectiveBladePitchH1;
        this.calculatedCollectiveBladePitchH2 = calculatedCollectiveBladePitchH2;
        this.calculatedCollectiveBladePitchH3 = calculatedCollectiveBladePitchH3;
        this.calculatedBladePitchH0S0 = calculatedBladePitchH0S0;
        this.calculatedBladePitchH0S1 = calculatedBladePitchH0S1;
        this.calculatedBladePitchH0S2 = calculatedBladePitchH0S2;
        this.calculatedBladePitchH1S0 = calculatedBladePitchH1S0;
        this.calculatedBladePitchH1S1 = calculatedBladePitchH1S1;
        this.calculatedBladePitchH1S2 = calculatedBladePitchH1S2;
        this.calculatedBladePitchH2S0 = calculatedBladePitchH2S0;
        this.calculatedBladePitchH2S1 = calculatedBladePitchH2S1;
        this.calculatedBladePitchH2S2 = calculatedBladePitchH2S2;
        this.calculatedBladePitchH3S0 = calculatedBladePitchH3S0;
        this.calculatedBladePitchH3S1 = calculatedBladePitchH3S1;
        this.calculatedBladePitchH3S2 = calculatedBladePitchH3S2;
        this.calculatedServoDegreesH0S0 = calculatedServoDegreesH0S0;
        this.calculatedServoDegreesH0S1 = calculatedServoDegreesH0S1;
        this.calculatedServoDegreesH0S2 = calculatedServoDegreesH0S2;
        this.calculatedServoDegreesH1S0 = calculatedServoDegreesH1S0;
        this.calculatedServoDegreesH1S1 = calculatedServoDegreesH1S1;
        this.calculatedServoDegreesH1S2 = calculatedServoDegreesH1S2;
        this.calculatedServoDegreesH2S0 = calculatedServoDegreesH2S0;
        this.calculatedServoDegreesH2S1 = calculatedServoDegreesH2S1;
        this.calculatedServoDegreesH2S2 = calculatedServoDegreesH2S2;
        this.calculatedServoDegreesH3S0 = calculatedServoDegreesH3S0;
        this.calculatedServoDegreesH3S1 = calculatedServoDegreesH3S1;
        this.calculatedServoDegreesH3S2 = calculatedServoDegreesH3S2;
        this.calculatedServoPulseH0S0 = calculatedServoPulseH0S0;
        this.calculatedServoPulseH0S1 = calculatedServoPulseH0S1;
        this.calculatedServoPulseH0S2 = calculatedServoPulseH0S2;
        this.calculatedServoPulseH1S0 = calculatedServoPulseH1S0;
        this.calculatedServoPulseH1S1 = calculatedServoPulseH1S1;
        this.calculatedServoPulseH1S2 = calculatedServoPulseH1S2;
        this.calculatedServoPulseH2S0 = calculatedServoPulseH2S0;
        this.calculatedServoPulseH2S1 = calculatedServoPulseH2S1;
        this.calculatedServoPulseH2S2 = calculatedServoPulseH2S2;
        this.calculatedServoPulseH3S0 = calculatedServoPulseH3S0;
        this.calculatedServoPulseH3S1 = calculatedServoPulseH3S1;
        this.calculatedServoPulseH3S2 = calculatedServoPulseH3S2;
        this.cyclicHeading = cyclicHeading;
        this.velocityCyclicAlpha = velocityCyclicAlpha;
    }

    /**
     * Heli 0 collective delta orientation
     */
    protected double collectiveDeltaOrientationH0;
    /**
     * Heli 1 collective delta orientation
     */
    protected double collectiveDeltaOrientationH1;
    /**
     * Heli 2 collective delta orientation
     */
    protected double collectiveDeltaOrientationH2;
    /**
     * Heli 3 collective delta orientation
     */
    protected double collectiveDeltaOrientationH3;
    /**
     * Heli 0 collective delta altitude
     */
    protected double collectiveDeltaAltitudeH0;
    /**
     * Heli 1 collective delta altitude
     */
    protected double collectiveDeltaAltitudeH1;
    /**
     * Heli 2 collective delta altitude
     */
    protected double collectiveDeltaAltitudeH2;
    /**
     * Heli 3 collective delta altitude
     */
    protected double collectiveDeltaAltitudeH3;
    /**
     * Heli 0 collective delta heading
     */
    protected double collectiveDeltaHeadingH0;
    /**
     * Heli 1 collective delta heading
     */
    protected double collectiveDeltaHeadingH1;
    /**
     * Heli 2 collective delta heading
     */
    protected double collectiveDeltaHeadingH2;
    /**
     * Heli 3 collective delta heading
     */
    protected double collectiveDeltaHeadingH3;
    /**
     * Heli 0 collective delta total
     */
    protected double collectiveDeltaTotalH0;
    /**
     * Heli 1 collective delta total
     */
    protected double collectiveDeltaTotalH1;
    /**
     * Heli 2 collective delta total
     */
    protected double collectiveDeltaTotalH2;
    /**
     * Heli 3 collective delta total
     */
    protected double collectiveDeltaTotalH3;
    /**
     * Heli 0 Servo 0 cyclic value
     */
    protected double cyclicValueH0S0;
    /**
     * Heli 0 Servo 1 cyclic value
     */
    protected double cyclicValueH0S1;
    /**
     * Heli 0 Servo 2 cyclic value
     */
    protected double cyclicValueH0S2;
    /**
     * Heli 1 Servo 0 cyclic value
     */
    protected double cyclicValueH1S0;
    /**
     * Heli 1 Servo 1 cyclic value
     */
    protected double cyclicValueH1S1;
    /**
     * Heli 1 Servo 2 cyclic value
     */
    protected double cyclicValueH1S2;
    /**
     * Heli 2 Servo 0 cyclic value
     */
    protected double cyclicValueH2S0;
    /**
     * Heli 2 Servo 1 cyclic value
     */
    protected double cyclicValueH2S1;
    /**
     * Heli 2 Servo 2 cyclic value
     */
    protected double cyclicValueH2S2;
    /**
     * Heli 3 Servo 0 cyclic value
     */
    protected double cyclicValueH3S0;
    /**
     * Heli 3 Servo 1 cyclic value
     */
    protected double cyclicValueH3S1;
    /**
     * Heli 3 Servo 2 cyclic value
     */
    protected double cyclicValueH3S2;
    /**
     * Heli 0 collective value
     */
    protected double collectiveValueH0;
    /**
     * Heli 1 collective value
     */
    protected double collectiveValueH1;
    /**
     * Heli 2 collective value
     */
    protected double collectiveValueH2;
    /**
     * Heli 3 collective value
     */
    protected double collectiveValueH3;
    /**
     * Heli 0 Servo 0 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH0S0;
    /**
     * Heli 0 Servo 1 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH0S1;
    /**
     * Heli 0 Servo 2 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH0S2;
    /**
     * Heli 1 Servo 0 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH1S0;
    /**
     * Heli 1 Servo 1 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH1S1;
    /**
     * Heli 1 Servo 2 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH1S2;
    /**
     * Heli 2 Servo 0 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH2S0;
    /**
     * Heli 2 Servo 1 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH2S1;
    /**
     * Heli 2 Servo 2 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH2S2;
    /**
     * Heli 3 Servo 0 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH3S0;
    /**
     * Heli 3 Servo 1 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH3S1;
    /**
     * Heli 3 Servo 2 calculated cyclic blade pitch
     */
    protected double calculatedCyclicBladePitchH3S2;
    /**
     * Heli 0 calculated collective blade pitch
     */
    protected double calculatedCollectiveBladePitchH0;
    /**
     * Heli 1 calculated collective blade pitch
     */
    protected double calculatedCollectiveBladePitchH1;
    /**
     * Heli 2 calculated collective blade pitch
     */
    protected double calculatedCollectiveBladePitchH2;
    /**
     * Heli 3 calculated collective blade pitch
     */
    protected double calculatedCollectiveBladePitchH3;
    /**
     * Heli 0 Servo 0 calculated blade pitch
     */
    protected double calculatedBladePitchH0S0;
    /**
     * Heli 0 Servo 1 calculated blade pitch
     */
    protected double calculatedBladePitchH0S1;
    /**
     * Heli 0 Servo 2 calculated blade pitch
     */
    protected double calculatedBladePitchH0S2;
    /**
     * Heli 1 Servo 0 calculated blade pitch
     */
    protected double calculatedBladePitchH1S0;
    /**
     * Heli 1 Servo 1 calculated blade pitch
     */
    protected double calculatedBladePitchH1S1;
    /**
     * Heli 1 Servo 2 calculated blade pitch
     */
    protected double calculatedBladePitchH1S2;
    /**
     * Heli 2 Servo 0 calculated blade pitch
     */
    protected double calculatedBladePitchH2S0;
    /**
     * Heli 2 Servo 1 calculated blade pitch
     */
    protected double calculatedBladePitchH2S1;
    /**
     * Heli 2 Servo 2 calculated blade pitch
     */
    protected double calculatedBladePitchH2S2;
    /**
     * Heli 3 Servo 0 calculated blade pitch
     */
    protected double calculatedBladePitchH3S0;
    /**
     * Heli 3 Servo 1 calculated blade pitch
     */
    protected double calculatedBladePitchH3S1;
    /**
     * Heli 3 Servo 2 calculated blade pitch
     */
    protected double calculatedBladePitchH3S2;
    /**
     * Heli 0 Servo 0 calculated servo degrees
     */
    protected double calculatedServoDegreesH0S0;
    /**
     * Heli 0 Servo 1 calculated servo degrees
     */
    protected double calculatedServoDegreesH0S1;
    /**
     * Heli 0 Servo 2 calculated servo degrees
     */
    protected double calculatedServoDegreesH0S2;
    /**
     * Heli 1 Servo 0 calculated servo degrees
     */
    protected double calculatedServoDegreesH1S0;
    /**
     * Heli 1 Servo 1 calculated servo degrees
     */
    protected double calculatedServoDegreesH1S1;
    /**
     * Heli 1 Servo 2 calculated servo degrees
     */
    protected double calculatedServoDegreesH1S2;
    /**
     * Heli 2 Servo 0 calculated servo degrees
     */
    protected double calculatedServoDegreesH2S0;
    /**
     * Heli 2 Servo 1 calculated servo degrees
     */
    protected double calculatedServoDegreesH2S1;
    /**
     * Heli 2 Servo 2 calculated servo degrees
     */
    protected double calculatedServoDegreesH2S2;
    /**
     * Heli 3 Servo 0 calculated servo degrees
     */
    protected double calculatedServoDegreesH3S0;
    /**
     * Heli 3 Servo 1 calculated servo degrees
     */
    protected double calculatedServoDegreesH3S1;
    /**
     * Heli 3 Servo 2 calculated servo degrees
     */
    protected double calculatedServoDegreesH3S2;
    /**
     * Heli 0 Servo 0 calculated servo pulse
     */
    protected int calculatedServoPulseH0S0;
    /**
     * Heli 0 Servo 1 calculated servo pulse
     */
    protected int calculatedServoPulseH0S1;
    /**
     * Heli 0 Servo 2 calculated servo pulse
     */
    protected int calculatedServoPulseH0S2;
    /**
     * Heli 1 Servo 0 calculated servo pulse
     */
    protected int calculatedServoPulseH1S0;
    /**
     * Heli 1 Servo 1 calculated servo pulse
     */
    protected int calculatedServoPulseH1S1;
    /**
     * Heli 1 Servo 2 calculated servo pulse
     */
    protected int calculatedServoPulseH1S2;
    /**
     * Heli 2 Servo 0 calculated servo pulse
     */
    protected int calculatedServoPulseH2S0;
    /**
     * Heli 2 Servo 1 calculated servo pulse
     */
    protected int calculatedServoPulseH2S1;
    /**
     * Heli 2 Servo 2 calculated servo pulse
     */
    protected int calculatedServoPulseH2S2;
    /**
     * Heli 3 Servo 0 calculated servo pulse
     */
    protected int calculatedServoPulseH3S0;
    /**
     * Heli 3 Servo 1 calculated servo pulse
     */
    protected int calculatedServoPulseH3S1;
    /**
     * Heli 3 Servo 2 calculated servo pulse
     */
    protected int calculatedServoPulseH3S2;
    // Cyclic for Heading in Normal Rotation mode:
    /**
     * Cyclic heading in normal rotation mode
     */
    protected double cyclicHeading;
    /**
     * Current velocity cyclic alpha
     */
    protected double velocityCyclicAlpha;

    @Override
    public FloidModelStatus fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        collectiveDeltaOrientationH0 = jsonObject.getDouble("collectiveDeltaOrientationH0");
        collectiveDeltaOrientationH1 = jsonObject.getDouble("collectiveDeltaOrientationH1");
        collectiveDeltaOrientationH2 = jsonObject.getDouble("collectiveDeltaOrientationH2");
        collectiveDeltaOrientationH3 = jsonObject.getDouble("collectiveDeltaOrientationH3");
        collectiveDeltaAltitudeH0 = jsonObject.getDouble("collectiveDeltaAltitudeH0");
        collectiveDeltaAltitudeH1 = jsonObject.getDouble("collectiveDeltaAltitudeH1");
        collectiveDeltaAltitudeH2 = jsonObject.getDouble("collectiveDeltaAltitudeH2");
        collectiveDeltaAltitudeH3 = jsonObject.getDouble("collectiveDeltaAltitudeH3");
        collectiveDeltaHeadingH0 = jsonObject.getDouble("collectiveDeltaHeadingH0");
        collectiveDeltaHeadingH1 = jsonObject.getDouble("collectiveDeltaHeadingH1");
        collectiveDeltaHeadingH2 = jsonObject.getDouble("collectiveDeltaHeadingH2");
        collectiveDeltaHeadingH3 = jsonObject.getDouble("collectiveDeltaHeadingH3");
        collectiveDeltaTotalH0 = jsonObject.getDouble("collectiveDeltaTotalH0");
        collectiveDeltaTotalH1 = jsonObject.getDouble("collectiveDeltaTotalH1");
        collectiveDeltaTotalH2 = jsonObject.getDouble("collectiveDeltaTotalH2");
        collectiveDeltaTotalH3 = jsonObject.getDouble("collectiveDeltaTotalH3");
        cyclicValueH0S0 = jsonObject.getDouble("cyclicValueH0S0");
        cyclicValueH0S1 = jsonObject.getDouble("cyclicValueH0S1");
        cyclicValueH0S2 = jsonObject.getDouble("cyclicValueH0S2");
        cyclicValueH1S0 = jsonObject.getDouble("cyclicValueH1S0");
        cyclicValueH1S1 = jsonObject.getDouble("cyclicValueH1S1");
        cyclicValueH1S2 = jsonObject.getDouble("cyclicValueH1S2");
        cyclicValueH2S0 = jsonObject.getDouble("cyclicValueH2S0");
        cyclicValueH2S1 = jsonObject.getDouble("cyclicValueH2S1");
        cyclicValueH2S2 = jsonObject.getDouble("cyclicValueH2S2");
        cyclicValueH3S0 = jsonObject.getDouble("cyclicValueH3S0");
        cyclicValueH3S1 = jsonObject.getDouble("cyclicValueH3S1");
        cyclicValueH3S2 = jsonObject.getDouble("cyclicValueH3S2");
        collectiveValueH0 = jsonObject.getDouble("collectiveValueH0");
        collectiveValueH1 = jsonObject.getDouble("collectiveValueH1");
        collectiveValueH2 = jsonObject.getDouble("collectiveValueH2");
        collectiveValueH3 = jsonObject.getDouble("collectiveValueH3");
        calculatedCyclicBladePitchH0S0 = jsonObject.getDouble("calculatedCyclicBladePitchH0S0");
        calculatedCyclicBladePitchH0S1 = jsonObject.getDouble("calculatedCyclicBladePitchH0S1");
        calculatedCyclicBladePitchH0S2 = jsonObject.getDouble("calculatedCyclicBladePitchH0S2");
        calculatedCyclicBladePitchH1S0 = jsonObject.getDouble("calculatedCyclicBladePitchH1S0");
        calculatedCyclicBladePitchH1S1 = jsonObject.getDouble("calculatedCyclicBladePitchH1S1");
        calculatedCyclicBladePitchH1S2 = jsonObject.getDouble("calculatedCyclicBladePitchH1S2");
        calculatedCyclicBladePitchH2S0 = jsonObject.getDouble("calculatedCyclicBladePitchH2S0");
        calculatedCyclicBladePitchH2S1 = jsonObject.getDouble("calculatedCyclicBladePitchH2S1");
        calculatedCyclicBladePitchH2S2 = jsonObject.getDouble("calculatedCyclicBladePitchH2S2");
        calculatedCyclicBladePitchH3S0 = jsonObject.getDouble("calculatedCyclicBladePitchH3S0");
        calculatedCyclicBladePitchH3S1 = jsonObject.getDouble("calculatedCyclicBladePitchH3S1");
        calculatedCyclicBladePitchH3S2 = jsonObject.getDouble("calculatedCyclicBladePitchH3S2");
        calculatedCollectiveBladePitchH0 = jsonObject.getDouble("calculatedCollectiveBladePitchH0");
        calculatedCollectiveBladePitchH1 = jsonObject.getDouble("calculatedCollectiveBladePitchH1");
        calculatedCollectiveBladePitchH2 = jsonObject.getDouble("calculatedCollectiveBladePitchH2");
        calculatedCollectiveBladePitchH3 = jsonObject.getDouble("calculatedCollectiveBladePitchH3");
        calculatedBladePitchH0S0 = jsonObject.getDouble("calculatedBladePitchH0S0");
        calculatedBladePitchH0S1 = jsonObject.getDouble("calculatedBladePitchH0S1");
        calculatedBladePitchH0S2 = jsonObject.getDouble("calculatedBladePitchH0S2");
        calculatedBladePitchH1S0 = jsonObject.getDouble("calculatedBladePitchH1S0");
        calculatedBladePitchH1S1 = jsonObject.getDouble("calculatedBladePitchH1S1");
        calculatedBladePitchH1S2 = jsonObject.getDouble("calculatedBladePitchH1S2");
        calculatedBladePitchH2S0 = jsonObject.getDouble("calculatedBladePitchH2S0");
        calculatedBladePitchH2S1 = jsonObject.getDouble("calculatedBladePitchH2S1");
        calculatedBladePitchH2S2 = jsonObject.getDouble("calculatedBladePitchH2S2");
        calculatedBladePitchH3S0 = jsonObject.getDouble("calculatedBladePitchH3S0");
        calculatedBladePitchH3S1 = jsonObject.getDouble("calculatedBladePitchH3S1");
        calculatedBladePitchH3S2 = jsonObject.getDouble("calculatedBladePitchH3S2");
        calculatedServoDegreesH0S0 = jsonObject.getDouble("calculatedServoDegreesH0S0");
        calculatedServoDegreesH0S1 = jsonObject.getDouble("calculatedServoDegreesH0S1");
        calculatedServoDegreesH0S2 = jsonObject.getDouble("calculatedServoDegreesH0S2");
        calculatedServoDegreesH1S0 = jsonObject.getDouble("calculatedServoDegreesH1S0");
        calculatedServoDegreesH1S1 = jsonObject.getDouble("calculatedServoDegreesH1S1");
        calculatedServoDegreesH1S2 = jsonObject.getDouble("calculatedServoDegreesH1S2");
        calculatedServoDegreesH2S0 = jsonObject.getDouble("calculatedServoDegreesH2S0");
        calculatedServoDegreesH2S1 = jsonObject.getDouble("calculatedServoDegreesH2S1");
        calculatedServoDegreesH2S2 = jsonObject.getDouble("calculatedServoDegreesH2S2");
        calculatedServoDegreesH3S0 = jsonObject.getDouble("calculatedServoDegreesH3S0");
        calculatedServoDegreesH3S1 = jsonObject.getDouble("calculatedServoDegreesH3S1");
        calculatedServoDegreesH3S2 = jsonObject.getDouble("calculatedServoDegreesH3S2");
        calculatedServoPulseH0S0 = jsonObject.getInt("calculatedServoPulseH0S0");
        calculatedServoPulseH0S1 = jsonObject.getInt("calculatedServoPulseH0S1");
        calculatedServoPulseH0S2 = jsonObject.getInt("calculatedServoPulseH0S2");
        calculatedServoPulseH1S0 = jsonObject.getInt("calculatedServoPulseH1S0");
        calculatedServoPulseH1S1 = jsonObject.getInt("calculatedServoPulseH1S1");
        calculatedServoPulseH1S2 = jsonObject.getInt("calculatedServoPulseH1S2");
        calculatedServoPulseH2S0 = jsonObject.getInt("calculatedServoPulseH2S0");
        calculatedServoPulseH2S1 = jsonObject.getInt("calculatedServoPulseH2S1");
        calculatedServoPulseH2S2 = jsonObject.getInt("calculatedServoPulseH2S2");
        calculatedServoPulseH3S0 = jsonObject.getInt("calculatedServoPulseH3S0");
        calculatedServoPulseH3S1 = jsonObject.getInt("calculatedServoPulseH3S1");
        calculatedServoPulseH3S2 = jsonObject.getInt("calculatedServoPulseH3S2");
        cyclicHeading = jsonObject.getDouble("cyclicHeading");
        velocityCyclicAlpha = jsonObject.getDouble("velocityCyclicAlpha");
        return this;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("collectiveDeltaOrientationH0", collectiveDeltaOrientationH0);
        jsonObject.put("collectiveDeltaOrientationH1", collectiveDeltaOrientationH1);
        jsonObject.put("collectiveDeltaOrientationH2", collectiveDeltaOrientationH2);
        jsonObject.put("collectiveDeltaOrientationH3", collectiveDeltaOrientationH3);
        jsonObject.put("collectiveDeltaAltitudeH0", collectiveDeltaAltitudeH0);
        jsonObject.put("collectiveDeltaAltitudeH1", collectiveDeltaAltitudeH1);
        jsonObject.put("collectiveDeltaAltitudeH2", collectiveDeltaAltitudeH2);
        jsonObject.put("collectiveDeltaAltitudeH3", collectiveDeltaAltitudeH3);
        jsonObject.put("collectiveDeltaHeadingH0", collectiveDeltaHeadingH0);
        jsonObject.put("collectiveDeltaHeadingH1", collectiveDeltaHeadingH1);
        jsonObject.put("collectiveDeltaHeadingH2", collectiveDeltaHeadingH2);
        jsonObject.put("collectiveDeltaHeadingH3", collectiveDeltaHeadingH3);
        jsonObject.put("collectiveDeltaTotalH0", collectiveDeltaTotalH0);
        jsonObject.put("collectiveDeltaTotalH1", collectiveDeltaTotalH1);
        jsonObject.put("collectiveDeltaTotalH2", collectiveDeltaTotalH2);
        jsonObject.put("collectiveDeltaTotalH3", collectiveDeltaTotalH3);
        jsonObject.put("cyclicValueH0S0", cyclicValueH0S0);
        jsonObject.put("cyclicValueH0S1", cyclicValueH0S1);
        jsonObject.put("cyclicValueH0S2", cyclicValueH0S2);
        jsonObject.put("cyclicValueH1S0", cyclicValueH1S0);
        jsonObject.put("cyclicValueH1S1", cyclicValueH1S1);
        jsonObject.put("cyclicValueH1S2", cyclicValueH1S2);
        jsonObject.put("cyclicValueH2S0", cyclicValueH2S0);
        jsonObject.put("cyclicValueH2S1", cyclicValueH2S1);
        jsonObject.put("cyclicValueH2S2", cyclicValueH2S2);
        jsonObject.put("cyclicValueH3S0", cyclicValueH3S0);
        jsonObject.put("cyclicValueH3S1", cyclicValueH3S1);
        jsonObject.put("cyclicValueH3S2", cyclicValueH3S2);
        jsonObject.put("collectiveValueH0", collectiveValueH0);
        jsonObject.put("collectiveValueH1", collectiveValueH1);
        jsonObject.put("collectiveValueH2", collectiveValueH2);
        jsonObject.put("collectiveValueH3", collectiveValueH3);
        jsonObject.put("calculatedCyclicBladePitchH0S0", calculatedCyclicBladePitchH0S0);
        jsonObject.put("calculatedCyclicBladePitchH0S1", calculatedCyclicBladePitchH0S1);
        jsonObject.put("calculatedCyclicBladePitchH0S2", calculatedCyclicBladePitchH0S2);
        jsonObject.put("calculatedCyclicBladePitchH1S0", calculatedCyclicBladePitchH1S0);
        jsonObject.put("calculatedCyclicBladePitchH1S1", calculatedCyclicBladePitchH1S1);
        jsonObject.put("calculatedCyclicBladePitchH1S2", calculatedCyclicBladePitchH1S2);
        jsonObject.put("calculatedCyclicBladePitchH2S0", calculatedCyclicBladePitchH2S0);
        jsonObject.put("calculatedCyclicBladePitchH2S1", calculatedCyclicBladePitchH2S1);
        jsonObject.put("calculatedCyclicBladePitchH2S2", calculatedCyclicBladePitchH2S2);
        jsonObject.put("calculatedCyclicBladePitchH3S0", calculatedCyclicBladePitchH3S0);
        jsonObject.put("calculatedCyclicBladePitchH3S1", calculatedCyclicBladePitchH3S1);
        jsonObject.put("calculatedCyclicBladePitchH3S2", calculatedCyclicBladePitchH3S2);
        jsonObject.put("calculatedCollectiveBladePitchH0", calculatedCollectiveBladePitchH0);
        jsonObject.put("calculatedCollectiveBladePitchH1", calculatedCollectiveBladePitchH1);
        jsonObject.put("calculatedCollectiveBladePitchH2", calculatedCollectiveBladePitchH2);
        jsonObject.put("calculatedCollectiveBladePitchH3", calculatedCollectiveBladePitchH3);
        jsonObject.put("calculatedBladePitchH0S0", calculatedBladePitchH0S0);
        jsonObject.put("calculatedBladePitchH0S1", calculatedBladePitchH0S1);
        jsonObject.put("calculatedBladePitchH0S2", calculatedBladePitchH0S2);
        jsonObject.put("calculatedBladePitchH1S0", calculatedBladePitchH1S0);
        jsonObject.put("calculatedBladePitchH1S1", calculatedBladePitchH1S1);
        jsonObject.put("calculatedBladePitchH1S2", calculatedBladePitchH1S2);
        jsonObject.put("calculatedBladePitchH2S0", calculatedBladePitchH2S0);
        jsonObject.put("calculatedBladePitchH2S1", calculatedBladePitchH2S1);
        jsonObject.put("calculatedBladePitchH2S2", calculatedBladePitchH2S2);
        jsonObject.put("calculatedBladePitchH3S0", calculatedBladePitchH3S0);
        jsonObject.put("calculatedBladePitchH3S1", calculatedBladePitchH3S1);
        jsonObject.put("calculatedBladePitchH3S2", calculatedBladePitchH3S2);
        jsonObject.put("calculatedServoDegreesH0S0", calculatedServoDegreesH0S0);
        jsonObject.put("calculatedServoDegreesH0S1", calculatedServoDegreesH0S1);
        jsonObject.put("calculatedServoDegreesH0S2", calculatedServoDegreesH0S2);
        jsonObject.put("calculatedServoDegreesH1S0", calculatedServoDegreesH1S0);
        jsonObject.put("calculatedServoDegreesH1S1", calculatedServoDegreesH1S1);
        jsonObject.put("calculatedServoDegreesH1S2", calculatedServoDegreesH1S2);
        jsonObject.put("calculatedServoDegreesH2S0", calculatedServoDegreesH2S0);
        jsonObject.put("calculatedServoDegreesH2S1", calculatedServoDegreesH2S1);
        jsonObject.put("calculatedServoDegreesH2S2", calculatedServoDegreesH2S2);
        jsonObject.put("calculatedServoDegreesH3S0", calculatedServoDegreesH3S0);
        jsonObject.put("calculatedServoDegreesH3S1", calculatedServoDegreesH3S1);
        jsonObject.put("calculatedServoDegreesH3S2", calculatedServoDegreesH3S2);
        jsonObject.put("calculatedServoPulseH0S0", calculatedServoPulseH0S0);
        jsonObject.put("calculatedServoPulseH0S1", calculatedServoPulseH0S1);
        jsonObject.put("calculatedServoPulseH0S2", calculatedServoPulseH0S2);
        jsonObject.put("calculatedServoPulseH1S0", calculatedServoPulseH1S0);
        jsonObject.put("calculatedServoPulseH1S1", calculatedServoPulseH1S1);
        jsonObject.put("calculatedServoPulseH1S2", calculatedServoPulseH1S2);
        jsonObject.put("calculatedServoPulseH2S0", calculatedServoPulseH2S0);
        jsonObject.put("calculatedServoPulseH2S1", calculatedServoPulseH2S1);
        jsonObject.put("calculatedServoPulseH2S2", calculatedServoPulseH2S2);
        jsonObject.put("calculatedServoPulseH3S0", calculatedServoPulseH3S0);
        jsonObject.put("calculatedServoPulseH3S1", calculatedServoPulseH3S1);
        jsonObject.put("calculatedServoPulseH3S2", calculatedServoPulseH3S2);
        jsonObject.put("cyclicHeading", cyclicHeading);
        jsonObject.put("velocityCyclicAlpha", velocityCyclicAlpha);
        return jsonObject;
    }

    /**
     * Gets calculated blade pitch h 0 s 0.
     *
     * @return the calculated blade pitch h 0 s 0
     */
    public double getCalculatedBladePitchH0S0() {
        return calculatedBladePitchH0S0;
    }

    /**
     * Sets calculated blade pitch h 0 s 0.
     *
     * @param calculatedBladePitchH0S0 the calculated blade pitch h 0 s 0
     */
    public void setCalculatedBladePitchH0S0(double calculatedBladePitchH0S0) {
        this.calculatedBladePitchH0S0 = calculatedBladePitchH0S0;
    }

    /**
     * Gets calculated blade pitch h 0 s 1.
     *
     * @return the calculated blade pitch h 0 s 1
     */
    public double getCalculatedBladePitchH0S1() {
        return calculatedBladePitchH0S1;
    }

    /**
     * Sets calculated blade pitch h 0 s 1.
     *
     * @param calculatedBladePitchH0S1 the calculated blade pitch h 0 s 1
     */
    public void setCalculatedBladePitchH0S1(double calculatedBladePitchH0S1) {
        this.calculatedBladePitchH0S1 = calculatedBladePitchH0S1;
    }

    /**
     * Gets calculated blade pitch h 0 s 2.
     *
     * @return the calculated blade pitch h 0 s 2
     */
    public double getCalculatedBladePitchH0S2() {
        return calculatedBladePitchH0S2;
    }

    /**
     * Sets calculated blade pitch h 0 s 2.
     *
     * @param calculatedBladePitchH0S2 the calculated blade pitch h 0 s 2
     */
    public void setCalculatedBladePitchH0S2(double calculatedBladePitchH0S2) {
        this.calculatedBladePitchH0S2 = calculatedBladePitchH0S2;
    }

    /**
     * Gets calculated blade pitch h 1 s 0.
     *
     * @return the calculated blade pitch h 1 s 0
     */
    public double getCalculatedBladePitchH1S0() {
        return calculatedBladePitchH1S0;
    }

    /**
     * Sets calculated blade pitch h 1 s 0.
     *
     * @param calculatedBladePitchH1S0 the calculated blade pitch h 1 s 0
     */
    public void setCalculatedBladePitchH1S0(double calculatedBladePitchH1S0) {
        this.calculatedBladePitchH1S0 = calculatedBladePitchH1S0;
    }

    /**
     * Gets calculated blade pitch h 1 s 1.
     *
     * @return the calculated blade pitch h 1 s 1
     */
    public double getCalculatedBladePitchH1S1() {
        return calculatedBladePitchH1S1;
    }

    /**
     * Sets calculated blade pitch h 1 s 1.
     *
     * @param calculatedBladePitchH1S1 the calculated blade pitch h 1 s 1
     */
    public void setCalculatedBladePitchH1S1(double calculatedBladePitchH1S1) {
        this.calculatedBladePitchH1S1 = calculatedBladePitchH1S1;
    }

    /**
     * Gets calculated blade pitch h 1 s 2.
     *
     * @return the calculated blade pitch h 1 s 2
     */
    public double getCalculatedBladePitchH1S2() {
        return calculatedBladePitchH1S2;
    }

    /**
     * Sets calculated blade pitch h 1 s 2.
     *
     * @param calculatedBladePitchH1S2 the calculated blade pitch h 1 s 2
     */
    public void setCalculatedBladePitchH1S2(double calculatedBladePitchH1S2) {
        this.calculatedBladePitchH1S2 = calculatedBladePitchH1S2;
    }

    /**
     * Gets calculated blade pitch h 2 s 0.
     *
     * @return the calculated blade pitch h 2 s 0
     */
    public double getCalculatedBladePitchH2S0() {
        return calculatedBladePitchH2S0;
    }

    /**
     * Sets calculated blade pitch h 2 s 0.
     *
     * @param calculatedBladePitchH2S0 the calculated blade pitch h 2 s 0
     */
    public void setCalculatedBladePitchH2S0(double calculatedBladePitchH2S0) {
        this.calculatedBladePitchH2S0 = calculatedBladePitchH2S0;
    }

    /**
     * Gets calculated blade pitch h 2 s 1.
     *
     * @return the calculated blade pitch h 2 s 1
     */
    public double getCalculatedBladePitchH2S1() {
        return calculatedBladePitchH2S1;
    }

    /**
     * Sets calculated blade pitch h 2 s 1.
     *
     * @param calculatedBladePitchH2S1 the calculated blade pitch h 2 s 1
     */
    public void setCalculatedBladePitchH2S1(double calculatedBladePitchH2S1) {
        this.calculatedBladePitchH2S1 = calculatedBladePitchH2S1;
    }

    /**
     * Gets calculated blade pitch h 2 s 2.
     *
     * @return the calculated blade pitch h 2 s 2
     */
    public double getCalculatedBladePitchH2S2() {
        return calculatedBladePitchH2S2;
    }

    /**
     * Sets calculated blade pitch h 2 s 2.
     *
     * @param calculatedBladePitchH2S2 the calculated blade pitch h 2 s 2
     */
    public void setCalculatedBladePitchH2S2(double calculatedBladePitchH2S2) {
        this.calculatedBladePitchH2S2 = calculatedBladePitchH2S2;
    }

    /**
     * Gets calculated blade pitch h 3 s 0.
     *
     * @return the calculated blade pitch h 3 s 0
     */
    public double getCalculatedBladePitchH3S0() {
        return calculatedBladePitchH3S0;
    }

    /**
     * Sets calculated blade pitch h 3 s 0.
     *
     * @param calculatedBladePitchH3S0 the calculated blade pitch h 3 s 0
     */
    public void setCalculatedBladePitchH3S0(double calculatedBladePitchH3S0) {
        this.calculatedBladePitchH3S0 = calculatedBladePitchH3S0;
    }

    /**
     * Gets calculated blade pitch h 3 s 1.
     *
     * @return the calculated blade pitch h 3 s 1
     */
    public double getCalculatedBladePitchH3S1() {
        return calculatedBladePitchH3S1;
    }

    /**
     * Sets calculated blade pitch h 3 s 1.
     *
     * @param calculatedBladePitchH3S1 the calculated blade pitch h 3 s 1
     */
    public void setCalculatedBladePitchH3S1(double calculatedBladePitchH3S1) {
        this.calculatedBladePitchH3S1 = calculatedBladePitchH3S1;
    }

    /**
     * Gets calculated blade pitch h 3 s 2.
     *
     * @return the calculated blade pitch h 3 s 2
     */
    public double getCalculatedBladePitchH3S2() {
        return calculatedBladePitchH3S2;
    }

    /**
     * Sets calculated blade pitch h 3 s 2.
     *
     * @param calculatedBladePitchH3S2 the calculated blade pitch h 3 s 2
     */
    public void setCalculatedBladePitchH3S2(double calculatedBladePitchH3S2) {
        this.calculatedBladePitchH3S2 = calculatedBladePitchH3S2;
    }

    /**
     * Gets calculated collective blade pitch h 0.
     *
     * @return the calculated collective blade pitch h 0
     */
    public double getCalculatedCollectiveBladePitchH0() {
        return calculatedCollectiveBladePitchH0;
    }

    /**
     * Sets calculated collective blade pitch h 0.
     *
     * @param calculatedCollectiveBladePitchH0 the calculated collective blade pitch h 0
     */
    public void setCalculatedCollectiveBladePitchH0(double calculatedCollectiveBladePitchH0) {
        this.calculatedCollectiveBladePitchH0 = calculatedCollectiveBladePitchH0;
    }

    /**
     * Gets calculated collective blade pitch h 1.
     *
     * @return the calculated collective blade pitch h 1
     */
    public double getCalculatedCollectiveBladePitchH1() {
        return calculatedCollectiveBladePitchH1;
    }

    /**
     * Sets calculated collective blade pitch h 1.
     *
     * @param calculatedCollectiveBladePitchH1 the calculated collective blade pitch h 1
     */
    public void setCalculatedCollectiveBladePitchH1(double calculatedCollectiveBladePitchH1) {
        this.calculatedCollectiveBladePitchH1 = calculatedCollectiveBladePitchH1;
    }

    /**
     * Gets calculated collective blade pitch h 2.
     *
     * @return the calculated collective blade pitch h 2
     */
    public double getCalculatedCollectiveBladePitchH2() {
        return calculatedCollectiveBladePitchH2;
    }

    /**
     * Sets calculated collective blade pitch h 2.
     *
     * @param calculatedCollectiveBladePitchH2 the calculated collective blade pitch h 2
     */
    public void setCalculatedCollectiveBladePitchH2(double calculatedCollectiveBladePitchH2) {
        this.calculatedCollectiveBladePitchH2 = calculatedCollectiveBladePitchH2;
    }

    /**
     * Gets calculated collective blade pitch h 3.
     *
     * @return the calculated collective blade pitch h 3
     */
    public double getCalculatedCollectiveBladePitchH3() {
        return calculatedCollectiveBladePitchH3;
    }

    /**
     * Sets calculated collective blade pitch h 3.
     *
     * @param calculatedCollectiveBladePitchH3 the calculated collective blade pitch h 3
     */
    public void setCalculatedCollectiveBladePitchH3(double calculatedCollectiveBladePitchH3) {
        this.calculatedCollectiveBladePitchH3 = calculatedCollectiveBladePitchH3;
    }

    /**
     * Gets calculated cyclic blade pitch h 0 s 0.
     *
     * @return the calculated cyclic blade pitch h 0 s 0
     */
    public double getCalculatedCyclicBladePitchH0S0() {
        return calculatedCyclicBladePitchH0S0;
    }

    /**
     * Sets calculated cyclic blade pitch h 0 s 0.
     *
     * @param calculatedCyclicBladePitchH0S0 the calculated cyclic blade pitch h 0 s 0
     */
    public void setCalculatedCyclicBladePitchH0S0(double calculatedCyclicBladePitchH0S0) {
        this.calculatedCyclicBladePitchH0S0 = calculatedCyclicBladePitchH0S0;
    }

    /**
     * Gets calculated cyclic blade pitch h 0 s 1.
     *
     * @return the calculated cyclic blade pitch h 0 s 1
     */
    public double getCalculatedCyclicBladePitchH0S1() {
        return calculatedCyclicBladePitchH0S1;
    }

    /**
     * Sets calculated cyclic blade pitch h 0 s 1.
     *
     * @param calculatedCyclicBladePitchH0S1 the calculated cyclic blade pitch h 0 s 1
     */
    public void setCalculatedCyclicBladePitchH0S1(double calculatedCyclicBladePitchH0S1) {
        this.calculatedCyclicBladePitchH0S1 = calculatedCyclicBladePitchH0S1;
    }

    /**
     * Gets calculated cyclic blade pitch h 0 s 2.
     *
     * @return the calculated cyclic blade pitch h 0 s 2
     */
    public double getCalculatedCyclicBladePitchH0S2() {
        return calculatedCyclicBladePitchH0S2;
    }

    /**
     * Sets calculated cyclic blade pitch h 0 s 2.
     *
     * @param calculatedCyclicBladePitchH0S2 the calculated cyclic blade pitch h 0 s 2
     */
    public void setCalculatedCyclicBladePitchH0S2(double calculatedCyclicBladePitchH0S2) {
        this.calculatedCyclicBladePitchH0S2 = calculatedCyclicBladePitchH0S2;
    }

    /**
     * Gets calculated cyclic blade pitch h 1 s 0.
     *
     * @return the calculated cyclic blade pitch h 1 s 0
     */
    public double getCalculatedCyclicBladePitchH1S0() {
        return calculatedCyclicBladePitchH1S0;
    }

    /**
     * Sets calculated cyclic blade pitch h 1 s 0.
     *
     * @param calculatedCyclicBladePitchH1S0 the calculated cyclic blade pitch h 1 s 0
     */
    public void setCalculatedCyclicBladePitchH1S0(double calculatedCyclicBladePitchH1S0) {
        this.calculatedCyclicBladePitchH1S0 = calculatedCyclicBladePitchH1S0;
    }

    /**
     * Gets calculated cyclic blade pitch h 1 s 1.
     *
     * @return the calculated cyclic blade pitch h 1 s 1
     */
    public double getCalculatedCyclicBladePitchH1S1() {
        return calculatedCyclicBladePitchH1S1;
    }

    /**
     * Sets calculated cyclic blade pitch h 1 s 1.
     *
     * @param calculatedCyclicBladePitchH1S1 the calculated cyclic blade pitch h 1 s 1
     */
    public void setCalculatedCyclicBladePitchH1S1(double calculatedCyclicBladePitchH1S1) {
        this.calculatedCyclicBladePitchH1S1 = calculatedCyclicBladePitchH1S1;
    }

    /**
     * Gets calculated cyclic blade pitch h 1 s 2.
     *
     * @return the calculated cyclic blade pitch h 1 s 2
     */
    public double getCalculatedCyclicBladePitchH1S2() {
        return calculatedCyclicBladePitchH1S2;
    }

    /**
     * Sets calculated cyclic blade pitch h 1 s 2.
     *
     * @param calculatedCyclicBladePitchH1S2 the calculated cyclic blade pitch h 1 s 2
     */
    public void setCalculatedCyclicBladePitchH1S2(double calculatedCyclicBladePitchH1S2) {
        this.calculatedCyclicBladePitchH1S2 = calculatedCyclicBladePitchH1S2;
    }

    /**
     * Gets calculated cyclic blade pitch h 2 s 0.
     *
     * @return the calculated cyclic blade pitch h 2 s 0
     */
    public double getCalculatedCyclicBladePitchH2S0() {
        return calculatedCyclicBladePitchH2S0;
    }

    /**
     * Sets calculated cyclic blade pitch h 2 s 0.
     *
     * @param calculatedCyclicBladePitchH2S0 the calculated cyclic blade pitch h 2 s 0
     */
    public void setCalculatedCyclicBladePitchH2S0(double calculatedCyclicBladePitchH2S0) {
        this.calculatedCyclicBladePitchH2S0 = calculatedCyclicBladePitchH2S0;
    }

    /**
     * Gets calculated cyclic blade pitch h 2 s 1.
     *
     * @return the calculated cyclic blade pitch h 2 s 1
     */
    public double getCalculatedCyclicBladePitchH2S1() {
        return calculatedCyclicBladePitchH2S1;
    }

    /**
     * Sets calculated cyclic blade pitch h 2 s 1.
     *
     * @param calculatedCyclicBladePitchH2S1 the calculated cyclic blade pitch h 2 s 1
     */
    public void setCalculatedCyclicBladePitchH2S1(double calculatedCyclicBladePitchH2S1) {
        this.calculatedCyclicBladePitchH2S1 = calculatedCyclicBladePitchH2S1;
    }

    /**
     * Gets calculated cyclic blade pitch h 2 s 2.
     *
     * @return the calculated cyclic blade pitch h 2 s 2
     */
    public double getCalculatedCyclicBladePitchH2S2() {
        return calculatedCyclicBladePitchH2S2;
    }

    /**
     * Sets calculated cyclic blade pitch h 2 s 2.
     *
     * @param calculatedCyclicBladePitchH2S2 the calculated cyclic blade pitch h 2 s 2
     */
    public void setCalculatedCyclicBladePitchH2S2(double calculatedCyclicBladePitchH2S2) {
        this.calculatedCyclicBladePitchH2S2 = calculatedCyclicBladePitchH2S2;
    }

    /**
     * Gets calculated cyclic blade pitch h 3 s 0.
     *
     * @return the calculated cyclic blade pitch h 3 s 0
     */
    public double getCalculatedCyclicBladePitchH3S0() {
        return calculatedCyclicBladePitchH3S0;
    }

    /**
     * Sets calculated cyclic blade pitch h 3 s 0.
     *
     * @param calculatedCyclicBladePitchH3S0 the calculated cyclic blade pitch h 3 s 0
     */
    public void setCalculatedCyclicBladePitchH3S0(double calculatedCyclicBladePitchH3S0) {
        this.calculatedCyclicBladePitchH3S0 = calculatedCyclicBladePitchH3S0;
    }

    /**
     * Gets calculated cyclic blade pitch h 3 s 1.
     *
     * @return the calculated cyclic blade pitch h 3 s 1
     */
    public double getCalculatedCyclicBladePitchH3S1() {
        return calculatedCyclicBladePitchH3S1;
    }

    /**
     * Sets calculated cyclic blade pitch h 3 s 1.
     *
     * @param calculatedCyclicBladePitchH3S1 the calculated cyclic blade pitch h 3 s 1
     */
    public void setCalculatedCyclicBladePitchH3S1(double calculatedCyclicBladePitchH3S1) {
        this.calculatedCyclicBladePitchH3S1 = calculatedCyclicBladePitchH3S1;
    }

    /**
     * Gets calculated cyclic blade pitch h 3 s 2.
     *
     * @return the calculated cyclic blade pitch h 3 s 2
     */
    public double getCalculatedCyclicBladePitchH3S2() {
        return calculatedCyclicBladePitchH3S2;
    }

    /**
     * Sets calculated cyclic blade pitch h 3 s 2.
     *
     * @param calculatedCyclicBladePitchH3S2 the calculated cyclic blade pitch h 3 s 2
     */
    public void setCalculatedCyclicBladePitchH3S2(double calculatedCyclicBladePitchH3S2) {
        this.calculatedCyclicBladePitchH3S2 = calculatedCyclicBladePitchH3S2;
    }

    /**
     * Gets calculated servo degrees h 0 s 0.
     *
     * @return the calculated servo degrees h 0 s 0
     */
    public double getCalculatedServoDegreesH0S0() {
        return calculatedServoDegreesH0S0;
    }

    /**
     * Sets calculated servo degrees h 0 s 0.
     *
     * @param calculatedServoDegreesH0S0 the calculated servo degrees h 0 s 0
     */
    public void setCalculatedServoDegreesH0S0(double calculatedServoDegreesH0S0) {
        this.calculatedServoDegreesH0S0 = calculatedServoDegreesH0S0;
    }

    /**
     * Gets calculated servo degrees h 0 s 1.
     *
     * @return the calculated servo degrees h 0 s 1
     */
    public double getCalculatedServoDegreesH0S1() {
        return calculatedServoDegreesH0S1;
    }

    /**
     * Sets calculated servo degrees h 0 s 1.
     *
     * @param calculatedServoDegreesH0S1 the calculated servo degrees h 0 s 1
     */
    public void setCalculatedServoDegreesH0S1(double calculatedServoDegreesH0S1) {
        this.calculatedServoDegreesH0S1 = calculatedServoDegreesH0S1;
    }

    /**
     * Gets calculated servo degrees h 0 s 2.
     *
     * @return the calculated servo degrees h 0 s 2
     */
    public double getCalculatedServoDegreesH0S2() {
        return calculatedServoDegreesH0S2;
    }

    /**
     * Sets calculated servo degrees h 0 s 2.
     *
     * @param calculatedServoDegreesH0S2 the calculated servo degrees h 0 s 2
     */
    public void setCalculatedServoDegreesH0S2(double calculatedServoDegreesH0S2) {
        this.calculatedServoDegreesH0S2 = calculatedServoDegreesH0S2;
    }

    /**
     * Gets calculated servo degrees h 1 s 0.
     *
     * @return the calculated servo degrees h 1 s 0
     */
    public double getCalculatedServoDegreesH1S0() {
        return calculatedServoDegreesH1S0;
    }

    /**
     * Sets calculated servo degrees h 1 s 0.
     *
     * @param calculatedServoDegreesH1S0 the calculated servo degrees h 1 s 0
     */
    public void setCalculatedServoDegreesH1S0(double calculatedServoDegreesH1S0) {
        this.calculatedServoDegreesH1S0 = calculatedServoDegreesH1S0;
    }

    /**
     * Gets calculated servo degrees h 1 s 1.
     *
     * @return the calculated servo degrees h 1 s 1
     */
    public double getCalculatedServoDegreesH1S1() {
        return calculatedServoDegreesH1S1;
    }

    /**
     * Sets calculated servo degrees h 1 s 1.
     *
     * @param calculatedServoDegreesH1S1 the calculated servo degrees h 1 s 1
     */
    public void setCalculatedServoDegreesH1S1(double calculatedServoDegreesH1S1) {
        this.calculatedServoDegreesH1S1 = calculatedServoDegreesH1S1;
    }

    /**
     * Gets calculated servo degrees h 1 s 2.
     *
     * @return the calculated servo degrees h 1 s 2
     */
    public double getCalculatedServoDegreesH1S2() {
        return calculatedServoDegreesH1S2;
    }

    /**
     * Sets calculated servo degrees h 1 s 2.
     *
     * @param calculatedServoDegreesH1S2 the calculated servo degrees h 1 s 2
     */
    public void setCalculatedServoDegreesH1S2(double calculatedServoDegreesH1S2) {
        this.calculatedServoDegreesH1S2 = calculatedServoDegreesH1S2;
    }

    /**
     * Gets calculated servo degrees h 2 s 0.
     *
     * @return the calculated servo degrees h 2 s 0
     */
    public double getCalculatedServoDegreesH2S0() {
        return calculatedServoDegreesH2S0;
    }

    /**
     * Sets calculated servo degrees h 2 s 0.
     *
     * @param calculatedServoDegreesH2S0 the calculated servo degrees h 2 s 0
     */
    public void setCalculatedServoDegreesH2S0(double calculatedServoDegreesH2S0) {
        this.calculatedServoDegreesH2S0 = calculatedServoDegreesH2S0;
    }

    /**
     * Gets calculated servo degrees h 2 s 1.
     *
     * @return the calculated servo degrees h 2 s 1
     */
    public double getCalculatedServoDegreesH2S1() {
        return calculatedServoDegreesH2S1;
    }

    /**
     * Sets calculated servo degrees h 2 s 1.
     *
     * @param calculatedServoDegreesH2S1 the calculated servo degrees h 2 s 1
     */
    public void setCalculatedServoDegreesH2S1(double calculatedServoDegreesH2S1) {
        this.calculatedServoDegreesH2S1 = calculatedServoDegreesH2S1;
    }

    /**
     * Gets calculated servo degrees h 2 s 2.
     *
     * @return the calculated servo degrees h 2 s 2
     */
    public double getCalculatedServoDegreesH2S2() {
        return calculatedServoDegreesH2S2;
    }

    /**
     * Sets calculated servo degrees h 2 s 2.
     *
     * @param calculatedServoDegreesH2S2 the calculated servo degrees h 2 s 2
     */
    public void setCalculatedServoDegreesH2S2(double calculatedServoDegreesH2S2) {
        this.calculatedServoDegreesH2S2 = calculatedServoDegreesH2S2;
    }

    /**
     * Gets calculated servo degrees h 3 s 0.
     *
     * @return the calculated servo degrees h 3 s 0
     */
    public double getCalculatedServoDegreesH3S0() {
        return calculatedServoDegreesH3S0;
    }

    /**
     * Sets calculated servo degrees h 3 s 0.
     *
     * @param calculatedServoDegreesH3S0 the calculated servo degrees h 3 s 0
     */
    public void setCalculatedServoDegreesH3S0(double calculatedServoDegreesH3S0) {
        this.calculatedServoDegreesH3S0 = calculatedServoDegreesH3S0;
    }

    /**
     * Gets calculated servo degrees h 3 s 1.
     *
     * @return the calculated servo degrees h 3 s 1
     */
    public double getCalculatedServoDegreesH3S1() {
        return calculatedServoDegreesH3S1;
    }

    /**
     * Sets calculated servo degrees h 3 s 1.
     *
     * @param calculatedServoDegreesH3S1 the calculated servo degrees h 3 s 1
     */
    public void setCalculatedServoDegreesH3S1(double calculatedServoDegreesH3S1) {
        this.calculatedServoDegreesH3S1 = calculatedServoDegreesH3S1;
    }

    /**
     * Gets calculated servo degrees h 3 s 2.
     *
     * @return the calculated servo degrees h 3 s 2
     */
    public double getCalculatedServoDegreesH3S2() {
        return calculatedServoDegreesH3S2;
    }

    /**
     * Sets calculated servo degrees h 3 s 2.
     *
     * @param calculatedServoDegreesH3S2 the calculated servo degrees h 3 s 2
     */
    public void setCalculatedServoDegreesH3S2(double calculatedServoDegreesH3S2) {
        this.calculatedServoDegreesH3S2 = calculatedServoDegreesH3S2;
    }

    /**
     * Gets calculated servo pulse h 0 s 0.
     *
     * @return the calculated servo pulse h 0 s 0
     */
    public int getCalculatedServoPulseH0S0() {
        return calculatedServoPulseH0S0;
    }

    /**
     * Sets calculated servo pulse h 0 s 0.
     *
     * @param calculatedServoPulseH0S0 the calculated servo pulse h 0 s 0
     */
    public void setCalculatedServoPulseH0S0(int calculatedServoPulseH0S0) {
        this.calculatedServoPulseH0S0 = calculatedServoPulseH0S0;
    }

    /**
     * Gets calculated servo pulse h 0 s 1.
     *
     * @return the calculated servo pulse h 0 s 1
     */
    public int getCalculatedServoPulseH0S1() {
        return calculatedServoPulseH0S1;
    }

    /**
     * Sets calculated servo pulse h 0 s 1.
     *
     * @param calculatedServoPulseH0S1 the calculated servo pulse h 0 s 1
     */
    public void setCalculatedServoPulseH0S1(int calculatedServoPulseH0S1) {
        this.calculatedServoPulseH0S1 = calculatedServoPulseH0S1;
    }

    /**
     * Gets calculated servo pulse h 0 s 2.
     *
     * @return the calculated servo pulse h 0 s 2
     */
    public int getCalculatedServoPulseH0S2() {
        return calculatedServoPulseH0S2;
    }

    /**
     * Sets calculated servo pulse h 0 s 2.
     *
     * @param calculatedServoPulseH0S2 the calculated servo pulse h 0 s 2
     */
    public void setCalculatedServoPulseH0S2(int calculatedServoPulseH0S2) {
        this.calculatedServoPulseH0S2 = calculatedServoPulseH0S2;
    }

    /**
     * Gets calculated servo pulse h 1 s 0.
     *
     * @return the calculated servo pulse h 1 s 0
     */
    public int getCalculatedServoPulseH1S0() {
        return calculatedServoPulseH1S0;
    }

    /**
     * Sets calculated servo pulse h 1 s 0.
     *
     * @param calculatedServoPulseH1S0 the calculated servo pulse h 1 s 0
     */
    public void setCalculatedServoPulseH1S0(int calculatedServoPulseH1S0) {
        this.calculatedServoPulseH1S0 = calculatedServoPulseH1S0;
    }

    /**
     * Gets calculated servo pulse h 1 s 1.
     *
     * @return the calculated servo pulse h 1 s 1
     */
    public int getCalculatedServoPulseH1S1() {
        return calculatedServoPulseH1S1;
    }

    /**
     * Sets calculated servo pulse h 1 s 1.
     *
     * @param calculatedServoPulseH1S1 the calculated servo pulse h 1 s 1
     */
    public void setCalculatedServoPulseH1S1(int calculatedServoPulseH1S1) {
        this.calculatedServoPulseH1S1 = calculatedServoPulseH1S1;
    }

    /**
     * Gets calculated servo pulse h 1 s 2.
     *
     * @return the calculated servo pulse h 1 s 2
     */
    public int getCalculatedServoPulseH1S2() {
        return calculatedServoPulseH1S2;
    }

    /**
     * Sets calculated servo pulse h 1 s 2.
     *
     * @param calculatedServoPulseH1S2 the calculated servo pulse h 1 s 2
     */
    public void setCalculatedServoPulseH1S2(int calculatedServoPulseH1S2) {
        this.calculatedServoPulseH1S2 = calculatedServoPulseH1S2;
    }

    /**
     * Gets calculated servo pulse h 2 s 0.
     *
     * @return the calculated servo pulse h 2 s 0
     */
    public int getCalculatedServoPulseH2S0() {
        return calculatedServoPulseH2S0;
    }

    /**
     * Sets calculated servo pulse h 2 s 0.
     *
     * @param calculatedServoPulseH2S0 the calculated servo pulse h 2 s 0
     */
    public void setCalculatedServoPulseH2S0(int calculatedServoPulseH2S0) {
        this.calculatedServoPulseH2S0 = calculatedServoPulseH2S0;
    }

    /**
     * Gets calculated servo pulse h 2 s 1.
     *
     * @return the calculated servo pulse h 2 s 1
     */
    public int getCalculatedServoPulseH2S1() {
        return calculatedServoPulseH2S1;
    }

    /**
     * Sets calculated servo pulse h 2 s 1.
     *
     * @param calculatedServoPulseH2S1 the calculated servo pulse h 2 s 1
     */
    public void setCalculatedServoPulseH2S1(int calculatedServoPulseH2S1) {
        this.calculatedServoPulseH2S1 = calculatedServoPulseH2S1;
    }

    /**
     * Gets calculated servo pulse h 2 s 2.
     *
     * @return the calculated servo pulse h 2 s 2
     */
    public int getCalculatedServoPulseH2S2() {
        return calculatedServoPulseH2S2;
    }

    /**
     * Sets calculated servo pulse h 2 s 2.
     *
     * @param calculatedServoPulseH2S2 the calculated servo pulse h 2 s 2
     */
    public void setCalculatedServoPulseH2S2(int calculatedServoPulseH2S2) {
        this.calculatedServoPulseH2S2 = calculatedServoPulseH2S2;
    }

    /**
     * Gets calculated servo pulse h 3 s 0.
     *
     * @return the calculated servo pulse h 3 s 0
     */
    public int getCalculatedServoPulseH3S0() {
        return calculatedServoPulseH3S0;
    }

    /**
     * Sets calculated servo pulse h 3 s 0.
     *
     * @param calculatedServoPulseH3S0 the calculated servo pulse h 3 s 0
     */
    public void setCalculatedServoPulseH3S0(int calculatedServoPulseH3S0) {
        this.calculatedServoPulseH3S0 = calculatedServoPulseH3S0;
    }

    /**
     * Gets calculated servo pulse h 3 s 1.
     *
     * @return the calculated servo pulse h 3 s 1
     */
    public int getCalculatedServoPulseH3S1() {
        return calculatedServoPulseH3S1;
    }

    /**
     * Sets calculated servo pulse h 3 s 1.
     *
     * @param calculatedServoPulseH3S1 the calculated servo pulse h 3 s 1
     */
    public void setCalculatedServoPulseH3S1(int calculatedServoPulseH3S1) {
        this.calculatedServoPulseH3S1 = calculatedServoPulseH3S1;
    }

    /**
     * Gets calculated servo pulse h 3 s 2.
     *
     * @return the calculated servo pulse h 3 s 2
     */
    public int getCalculatedServoPulseH3S2() {
        return calculatedServoPulseH3S2;
    }

    /**
     * Sets calculated servo pulse h 3 s 2.
     *
     * @param calculatedServoPulseH3S2 the calculated servo pulse h 3 s 2
     */
    public void setCalculatedServoPulseH3S2(int calculatedServoPulseH3S2) {
        this.calculatedServoPulseH3S2 = calculatedServoPulseH3S2;
    }

    /**
     * Gets collective delta altitude h 0.
     *
     * @return the collective delta altitude h 0
     */
    public double getCollectiveDeltaAltitudeH0() {
        return collectiveDeltaAltitudeH0;
    }

    /**
     * Sets collective delta altitude h 0.
     *
     * @param collectiveDeltaAltitudeH0 the collective delta altitude h 0
     */
    public void setCollectiveDeltaAltitudeH0(double collectiveDeltaAltitudeH0) {
        this.collectiveDeltaAltitudeH0 = collectiveDeltaAltitudeH0;
    }

    /**
     * Gets collective delta altitude h 1.
     *
     * @return the collective delta altitude h 1
     */
    public double getCollectiveDeltaAltitudeH1() {
        return collectiveDeltaAltitudeH1;
    }

    /**
     * Sets collective delta altitude h 1.
     *
     * @param collectiveDeltaAltitudeH1 the collective delta altitude h 1
     */
    public void setCollectiveDeltaAltitudeH1(double collectiveDeltaAltitudeH1) {
        this.collectiveDeltaAltitudeH1 = collectiveDeltaAltitudeH1;
    }

    /**
     * Gets collective delta altitude h 2.
     *
     * @return the collective delta altitude h 2
     */
    public double getCollectiveDeltaAltitudeH2() {
        return collectiveDeltaAltitudeH2;
    }

    /**
     * Sets collective delta altitude h 2.
     *
     * @param collectiveDeltaAltitudeH2 the collective delta altitude h 2
     */
    public void setCollectiveDeltaAltitudeH2(double collectiveDeltaAltitudeH2) {
        this.collectiveDeltaAltitudeH2 = collectiveDeltaAltitudeH2;
    }

    /**
     * Gets collective delta altitude h 3.
     *
     * @return the collective delta altitude h 3
     */
    public double getCollectiveDeltaAltitudeH3() {
        return collectiveDeltaAltitudeH3;
    }

    /**
     * Sets collective delta altitude h 3.
     *
     * @param collectiveDeltaAltitudeH3 the collective delta altitude h 3
     */
    public void setCollectiveDeltaAltitudeH3(double collectiveDeltaAltitudeH3) {
        this.collectiveDeltaAltitudeH3 = collectiveDeltaAltitudeH3;
    }

    /**
     * Gets collective delta heading h 0.
     *
     * @return the collective delta heading h 0
     */
    public double getCollectiveDeltaHeadingH0() {
        return collectiveDeltaHeadingH0;
    }

    /**
     * Sets collective delta heading h 0.
     *
     * @param collectiveDeltaHeadingH0 the collective delta heading h 0
     */
    public void setCollectiveDeltaHeadingH0(double collectiveDeltaHeadingH0) {
        this.collectiveDeltaHeadingH0 = collectiveDeltaHeadingH0;
    }

    /**
     * Gets collective delta heading h 1.
     *
     * @return the collective delta heading h 1
     */
    public double getCollectiveDeltaHeadingH1() {
        return collectiveDeltaHeadingH1;
    }

    /**
     * Sets collective delta heading h 1.
     *
     * @param collectiveDeltaHeadingH1 the collective delta heading h 1
     */
    public void setCollectiveDeltaHeadingH1(double collectiveDeltaHeadingH1) {
        this.collectiveDeltaHeadingH1 = collectiveDeltaHeadingH1;
    }

    /**
     * Gets collective delta heading h 2.
     *
     * @return the collective delta heading h 2
     */
    public double getCollectiveDeltaHeadingH2() {
        return collectiveDeltaHeadingH2;
    }

    /**
     * Sets collective delta heading h 2.
     *
     * @param collectiveDeltaHeadingH2 the collective delta heading h 2
     */
    public void setCollectiveDeltaHeadingH2(double collectiveDeltaHeadingH2) {
        this.collectiveDeltaHeadingH2 = collectiveDeltaHeadingH2;
    }

    /**
     * Gets collective delta heading h 3.
     *
     * @return the collective delta heading h 3
     */
    public double getCollectiveDeltaHeadingH3() {
        return collectiveDeltaHeadingH3;
    }

    /**
     * Sets collective delta heading h 3.
     *
     * @param collectiveDeltaHeadingH3 the collective delta heading h 3
     */
    public void setCollectiveDeltaHeadingH3(double collectiveDeltaHeadingH3) {
        this.collectiveDeltaHeadingH3 = collectiveDeltaHeadingH3;
    }

    /**
     * Gets collective delta orientation h 0.
     *
     * @return the collective delta orientation h 0
     */
    public double getCollectiveDeltaOrientationH0() {
        return collectiveDeltaOrientationH0;
    }

    /**
     * Sets collective delta orientation h 0.
     *
     * @param collectiveDeltaOrientationH0 the collective delta orientation h 0
     */
    public void setCollectiveDeltaOrientationH0(double collectiveDeltaOrientationH0) {
        this.collectiveDeltaOrientationH0 = collectiveDeltaOrientationH0;
    }

    /**
     * Gets collective delta orientation h 1.
     *
     * @return the collective delta orientation h 1
     */
    public double getCollectiveDeltaOrientationH1() {
        return collectiveDeltaOrientationH1;
    }

    /**
     * Sets collective delta orientation h 1.
     *
     * @param collectiveDeltaOrientationH1 the collective delta orientation h 1
     */
    public void setCollectiveDeltaOrientationH1(double collectiveDeltaOrientationH1) {
        this.collectiveDeltaOrientationH1 = collectiveDeltaOrientationH1;
    }

    /**
     * Gets collective delta orientation h 2.
     *
     * @return the collective delta orientation h 2
     */
    public double getCollectiveDeltaOrientationH2() {
        return collectiveDeltaOrientationH2;
    }

    /**
     * Sets collective delta orientation h 2.
     *
     * @param collectiveDeltaOrientationH2 the collective delta orientation h 2
     */
    public void setCollectiveDeltaOrientationH2(double collectiveDeltaOrientationH2) {
        this.collectiveDeltaOrientationH2 = collectiveDeltaOrientationH2;
    }

    /**
     * Gets collective delta orientation h 3.
     *
     * @return the collective delta orientation h 3
     */
    public double getCollectiveDeltaOrientationH3() {
        return collectiveDeltaOrientationH3;
    }

    /**
     * Sets collective delta orientation h 3.
     *
     * @param collectiveDeltaOrientationH3 the collective delta orientation h 3
     */
    public void setCollectiveDeltaOrientationH3(double collectiveDeltaOrientationH3) {
        this.collectiveDeltaOrientationH3 = collectiveDeltaOrientationH3;
    }

    /**
     * Gets collective delta total h 0.
     *
     * @return the collective delta total h 0
     */
    public double getCollectiveDeltaTotalH0() {
        return collectiveDeltaTotalH0;
    }

    /**
     * Sets collective delta total h 0.
     *
     * @param collectiveDeltaTotalH0 the collective delta total h 0
     */
    public void setCollectiveDeltaTotalH0(double collectiveDeltaTotalH0) {
        this.collectiveDeltaTotalH0 = collectiveDeltaTotalH0;
    }

    /**
     * Gets collective delta total h 1.
     *
     * @return the collective delta total h 1
     */
    public double getCollectiveDeltaTotalH1() {
        return collectiveDeltaTotalH1;
    }

    /**
     * Sets collective delta total h 1.
     *
     * @param collectiveDeltaTotalH1 the collective delta total h 1
     */
    public void setCollectiveDeltaTotalH1(double collectiveDeltaTotalH1) {
        this.collectiveDeltaTotalH1 = collectiveDeltaTotalH1;
    }

    /**
     * Gets collective delta total h 2.
     *
     * @return the collective delta total h 2
     */
    public double getCollectiveDeltaTotalH2() {
        return collectiveDeltaTotalH2;
    }

    /**
     * Sets collective delta total h 2.
     *
     * @param collectiveDeltaTotalH2 the collective delta total h 2
     */
    public void setCollectiveDeltaTotalH2(double collectiveDeltaTotalH2) {
        this.collectiveDeltaTotalH2 = collectiveDeltaTotalH2;
    }

    /**
     * Gets collective delta total h 3.
     *
     * @return the collective delta total h 3
     */
    public double getCollectiveDeltaTotalH3() {
        return collectiveDeltaTotalH3;
    }

    /**
     * Sets collective delta total h 3.
     *
     * @param collectiveDeltaTotalH3 the collective delta total h 3
     */
    public void setCollectiveDeltaTotalH3(double collectiveDeltaTotalH3) {
        this.collectiveDeltaTotalH3 = collectiveDeltaTotalH3;
    }

    /**
     * Gets collective value h 0.
     *
     * @return the collective value h 0
     */
    public double getCollectiveValueH0() {
        return collectiveValueH0;
    }

    /**
     * Sets collective value h 0.
     *
     * @param collectiveValueH0 the collective value h 0
     */
    public void setCollectiveValueH0(double collectiveValueH0) {
        this.collectiveValueH0 = collectiveValueH0;
    }

    /**
     * Gets collective value h 1.
     *
     * @return the collective value h 1
     */
    public double getCollectiveValueH1() {
        return collectiveValueH1;
    }

    /**
     * Sets collective value h 1.
     *
     * @param collectiveValueH1 the collective value h 1
     */
    public void setCollectiveValueH1(double collectiveValueH1) {
        this.collectiveValueH1 = collectiveValueH1;
    }

    /**
     * Gets collective value h 2.
     *
     * @return the collective value h 2
     */
    public double getCollectiveValueH2() {
        return collectiveValueH2;
    }

    /**
     * Sets collective value h 2.
     *
     * @param collectiveValueH2 the collective value h 2
     */
    public void setCollectiveValueH2(double collectiveValueH2) {
        this.collectiveValueH2 = collectiveValueH2;
    }

    /**
     * Gets collective value h 3.
     *
     * @return the collective value h 3
     */
    public double getCollectiveValueH3() {
        return collectiveValueH3;
    }

    /**
     * Sets collective value h 3.
     *
     * @param collectiveValueH3 the collective value h 3
     */
    public void setCollectiveValueH3(double collectiveValueH3) {
        this.collectiveValueH3 = collectiveValueH3;
    }

    /**
     * Gets cyclic value h 0 s 0.
     *
     * @return the cyclic value h 0 s 0
     */
    public double getCyclicValueH0S0() {
        return cyclicValueH0S0;
    }

    /**
     * Sets cyclic value h 0 s 0.
     *
     * @param cyclicValueH0S0 the cyclic value h 0 s 0
     */
    public void setCyclicValueH0S0(double cyclicValueH0S0) {
        this.cyclicValueH0S0 = cyclicValueH0S0;
    }

    /**
     * Gets cyclic value h 0 s 1.
     *
     * @return the cyclic value h 0 s 1
     */
    public double getCyclicValueH0S1() {
        return cyclicValueH0S1;
    }

    /**
     * Sets cyclic value h 0 s 1.
     *
     * @param cyclicValueH0S1 the cyclic value h 0 s 1
     */
    public void setCyclicValueH0S1(double cyclicValueH0S1) {
        this.cyclicValueH0S1 = cyclicValueH0S1;
    }

    /**
     * Gets cyclic value h 0 s 2.
     *
     * @return the cyclic value h 0 s 2
     */
    public double getCyclicValueH0S2() {
        return cyclicValueH0S2;
    }

    /**
     * Sets cyclic value h 0 s 2.
     *
     * @param cyclicValueH0S2 the cyclic value h 0 s 2
     */
    public void setCyclicValueH0S2(double cyclicValueH0S2) {
        this.cyclicValueH0S2 = cyclicValueH0S2;
    }

    /**
     * Gets cyclic value h 1 s 0.
     *
     * @return the cyclic value h 1 s 0
     */
    public double getCyclicValueH1S0() {
        return cyclicValueH1S0;
    }

    /**
     * Sets cyclic value h 1 s 0.
     *
     * @param cyclicValueH1S0 the cyclic value h 1 s 0
     */
    public void setCyclicValueH1S0(double cyclicValueH1S0) {
        this.cyclicValueH1S0 = cyclicValueH1S0;
    }

    /**
     * Gets cyclic value h 1 s 1.
     *
     * @return the cyclic value h 1 s 1
     */
    public double getCyclicValueH1S1() {
        return cyclicValueH1S1;
    }

    /**
     * Sets cyclic value h 1 s 1.
     *
     * @param cyclicValueH1S1 the cyclic value h 1 s 1
     */
    public void setCyclicValueH1S1(double cyclicValueH1S1) {
        this.cyclicValueH1S1 = cyclicValueH1S1;
    }

    /**
     * Gets cyclic value h 1 s 2.
     *
     * @return the cyclic value h 1 s 2
     */
    public double getCyclicValueH1S2() {
        return cyclicValueH1S2;
    }

    /**
     * Sets cyclic value h 1 s 2.
     *
     * @param cyclicValueH1S2 the cyclic value h 1 s 2
     */
    public void setCyclicValueH1S2(double cyclicValueH1S2) {
        this.cyclicValueH1S2 = cyclicValueH1S2;
    }

    /**
     * Gets cyclic value h 2 s 0.
     *
     * @return the cyclic value h 2 s 0
     */
    public double getCyclicValueH2S0() {
        return cyclicValueH2S0;
    }

    /**
     * Sets cyclic value h 2 s 0.
     *
     * @param cyclicValueH2S0 the cyclic value h 2 s 0
     */
    public void setCyclicValueH2S0(double cyclicValueH2S0) {
        this.cyclicValueH2S0 = cyclicValueH2S0;
    }

    /**
     * Gets cyclic value h 2 s 1.
     *
     * @return the cyclic value h 2 s 1
     */
    public double getCyclicValueH2S1() {
        return cyclicValueH2S1;
    }

    /**
     * Sets cyclic value h 2 s 1.
     *
     * @param cyclicValueH2S1 the cyclic value h 2 s 1
     */
    public void setCyclicValueH2S1(double cyclicValueH2S1) {
        this.cyclicValueH2S1 = cyclicValueH2S1;
    }

    /**
     * Gets cyclic value h 2 s 2.
     *
     * @return the cyclic value h 2 s 2
     */
    public double getCyclicValueH2S2() {
        return cyclicValueH2S2;
    }

    /**
     * Sets cyclic value h 2 s 2.
     *
     * @param cyclicValueH2S2 the cyclic value h 2 s 2
     */
    public void setCyclicValueH2S2(double cyclicValueH2S2) {
        this.cyclicValueH2S2 = cyclicValueH2S2;
    }

    /**
     * Gets cyclic value h 3 s 0.
     *
     * @return the cyclic value h 3 s 0
     */
    public double getCyclicValueH3S0() {
        return cyclicValueH3S0;
    }

    /**
     * Sets cyclic value h 3 s 0.
     *
     * @param cyclicValueH3S0 the cyclic value h 3 s 0
     */
    public void setCyclicValueH3S0(double cyclicValueH3S0) {
        this.cyclicValueH3S0 = cyclicValueH3S0;
    }

    /**
     * Gets cyclic value h 3 s 1.
     *
     * @return the cyclic value h 3 s 1
     */
    public double getCyclicValueH3S1() {
        return cyclicValueH3S1;
    }

    /**
     * Sets cyclic value h 3 s 1.
     *
     * @param cyclicValueH3S1 the cyclic value h 3 s 1
     */
    public void setCyclicValueH3S1(double cyclicValueH3S1) {
        this.cyclicValueH3S1 = cyclicValueH3S1;
    }

    /**
     * Gets cyclic value h 3 s 2.
     *
     * @return the cyclic value h 3 s 2
     */
    public double getCyclicValueH3S2() {
        return cyclicValueH3S2;
    }

    /**
     * Sets cyclic value h 3 s 2.
     *
     * @param cyclicValueH3S2 the cyclic value h 3 s 2
     */
    public void setCyclicValueH3S2(double cyclicValueH3S2) {
        this.cyclicValueH3S2 = cyclicValueH3S2;
    }

    /**
     * Gets cyclic heading.
     *
     * @return the cyclic heading
     */
    public double getCyclicHeading() {
        return cyclicHeading;
    }

    /**
     * Sets cyclic heading.
     *
     * @param cyclicHeading the cyclic heading
     */
    public void setCyclicHeading(double cyclicHeading) {
        this.cyclicHeading = cyclicHeading;
    }

    /**
     * Gets the velocity cyclic alpha
     * @return the velocity cyclic alpha
     */
    public double getVelocityCyclicAlpha() {
        return velocityCyclicAlpha;
    }
    /**
     * Sets the velocity cyclic alpha
     * @param velocityCyclicAlpha  the velocity cyclic alpha
     */
    public void setVelocityCyclicAlpha(double velocityCyclicAlpha) {
        this.velocityCyclicAlpha = velocityCyclicAlpha;
    }
}
