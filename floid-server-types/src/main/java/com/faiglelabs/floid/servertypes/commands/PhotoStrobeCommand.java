/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Photo strobe command.  Takes a photo every delay seconds.
 */
@Entity
public class PhotoStrobeCommand extends DroidCommand {
    private int delay = 0;

    /**
     * Instantiates a new Photo strobe command.
     */
    public PhotoStrobeCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        PhotoStrobeCommand photoStrobeCommand = new PhotoStrobeCommand();
        super.factory(photoStrobeCommand, droidMission);
        photoStrobeCommand.delay = delay;
        return photoStrobeCommand;
    }

    /**
     * Gets delay.
     *
     * @return the delay
     */
    @SuppressWarnings("unused")
    public int getDelay() {
        return delay;
    }

    /**
     * Sets delay.
     *
     * @param delay the delay
     */
    @SuppressWarnings("unused")
    public void setDelay(int delay) {
        this.delay = delay;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("delay", delay);
        return jsonObject;
    }

    @Override
    public PhotoStrobeCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        delay = jsonObject.getInt("delay");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("delay", "" + delay);
        return typeElement;
    }
}
