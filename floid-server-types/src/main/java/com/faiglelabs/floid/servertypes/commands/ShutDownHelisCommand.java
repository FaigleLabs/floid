/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Shut down helis command.  Follows a start helis command to turn off the helis. On ground only command.
 */
@Entity
public class ShutDownHelisCommand extends DroidCommand {
    /**
     * Instantiates a new Shut down helis command.
     */
    public ShutDownHelisCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        ShutDownHelisCommand shutDownHelisCommand = new ShutDownHelisCommand();
        super.factory(shutDownHelisCommand, droidMission);
        return shutDownHelisCommand;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable") JSONObject jsonObject = super.toJSON();
        // No parameters to add...
        return jsonObject;
    }

    @Override
    public ShutDownHelisCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable") Element typeElement = super.addToXML(doc);
        // Adds nothing...
        return typeElement;
    }
}
