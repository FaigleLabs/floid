/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Lift off command.  Specify altitude and if relative to acquired home altitude or is absolute.
 */
@Entity
public class LiftOffCommand extends DroidCommand {
    private double z = 0.0;
    private boolean absolute = false;

    /**
     * Instantiates a new lift off command.
     */
    public LiftOffCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        LiftOffCommand liftOffCommand = new LiftOffCommand();
        super.factory(liftOffCommand, droidMission);
        liftOffCommand.z = z;
        liftOffCommand.absolute = absolute;
        return liftOffCommand;
    }

    /**
     * Gets z.
     *
     * @return the z
     */
    public double getZ() {
        return z;
    }

    /**
     * Sets z.
     *
     * @param z the z
     */
    public void setZ(double z) {
        this.z = z;
    }

    /**
     * Is absolute boolean.
     *
     * @return the boolean
     */
    public boolean isAbsolute() {
        return absolute;
    }

    /**
     * Sets absolute.
     *
     * @param absolute the absolute
     */
    public void setAbsolute(boolean absolute) {
        this.absolute = absolute;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("z", z);
        jsonObject.put("absolute", absolute);
        return jsonObject;
    }

    @Override
    public LiftOffCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        z = jsonObject.getDouble("z");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("z", "" + z);
        typeElement.setAttribute("absolute", "" + absolute);
        return typeElement;
    }
}
