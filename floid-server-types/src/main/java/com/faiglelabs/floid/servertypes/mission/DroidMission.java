/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.mission;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Droid mission - represents a mission (set of instructions) as an instruction
 */
@Entity
public class DroidMission extends DroidInstruction {
    /**
     * No mission indicator
     */
    public static final long NO_MISSION = -1L;

    private String missionName;

    @OneToMany(mappedBy = "droidMission", fetch = FetchType.EAGER, cascade = javax.persistence.CascadeType.ALL, targetEntity = DroidInstruction.class, orphanRemoval = true)
    @OrderBy("missionInstructionNumber")
    private List<DroidInstruction> droidInstructions = new ArrayList<>();
    private int nextInstructionNumber = 0;

    /**
     * Instantiates a new Droid mission.
     */
    public DroidMission() {
        super();
    }

    /**
     * Copy constructor for droid mission
     * @param droidMission the droid mission from which to copy
     */
    @SuppressWarnings("CopyConstructorMissesField")
    public DroidMission(DroidMission droidMission) {
            super(droidMission);
        this.missionName = droidMission.getMissionName();
        // copy the droid instructions:
        for(DroidInstruction droidInstruction:droidMission.getDroidInstructions()) {
            this.droidInstructions.add(droidInstruction.factory(this));
        }
    }

    /**
     * Add instruction to this mission
     *
     * @param droidInstruction the droid instruction
     */
    public void addInstruction(DroidInstruction droidInstruction) {
        droidInstruction.setMissionInstructionNumber(droidInstructions.size());
        droidInstructions.add(droidInstruction);
    }

    /**
     * Gets mission name.
     *
     * @return the mission name
     */
    public String getMissionName() {
        return missionName;
    }

    /**
     * Sets mission name.
     *
     * @param missionName the mission name
     */
    public void setMissionName(String missionName) {
        this.missionName = missionName;
    }

    /**
     * Gets next instruction number.
     *
     * @return the next instruction number
     */
    @SuppressWarnings("unused")
    public int getNextInstructionNumber() {
        return nextInstructionNumber;
    }

    /**
     * Sets next instruction number.
     *
     * @param nextInstructionNumber the next instruction number
     */
    @SuppressWarnings("unused")
    public void setNextInstructionNumber(int nextInstructionNumber) {
        this.nextInstructionNumber = nextInstructionNumber;
    }

    /**
     * Gets next instruction.
     *
     * @return the next instruction
     */
    @SuppressWarnings("unused")
    @JsonIgnore
    public DroidInstruction getNextInstruction() {
        if (nextInstructionNumber < droidInstructions.size()) {
            return (droidInstructions.get(nextInstructionNumber++));  // NOTE: We reset this to zero every time a mission is created
        } else {
            return null;
        }
    }

    /**
     * Gets droid instructions.
     *
     * @return the droid instructions
     */
    public List<DroidInstruction> getDroidInstructions() {
        return droidInstructions;
    }

    /**
     * Sets droid instructions.
     *
     * @param droidInstructions the droid instructions
     */
    @SuppressWarnings("unused")
    public void setDroidInstructions(List<DroidInstruction> droidInstructions) {
        this.droidInstructions = droidInstructions;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("missionName", missionName);
        jsonObject.put("droidInstructions", droidInstructions);
        return jsonObject;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public DroidMission fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        missionName = jsonObject.getString("missionName");
        droidInstructions.clear();
        JSONArray droidInstructionsJSONArray = jsonObject.getJSONArray("droidInstructions");
        for (int i = 0; i < droidInstructionsJSONArray.length(); ++i) {
            JSONObject droidInstructionJSONObject = droidInstructionsJSONArray.getJSONObject(i);
            // OK, now use the type field to reflect and generate the type:
            String droidInstructionType = droidInstructionJSONObject.getString("type");
            System.out.println("DroidInstruction: " + droidInstructionType);
            // OK, 
            Class[] classParam = null;
            Object[] objectParam = null;
            try {
                Class cl = Class.forName(droidInstructionType);
                @SuppressWarnings({"unchecked", "ConstantConditions"})
                java.lang.reflect.Constructor co = cl.getConstructor(classParam);  // We know classParam is always null - it needs to be null of the right type for the constructor we want
                @SuppressWarnings("ConstantConditions") Object o = co.newInstance(objectParam);
                // OK, get the fromJSON method from the object's class to invoke on this
                Method fromJSONMethod = o.getClass().getMethod("fromJSON", JSONObject.class);
                fromJSONMethod.invoke(o, droidInstructionJSONObject);
                droidInstructions.add((DroidInstruction) o);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("missionName", missionName);
        typeElement.setAttribute("topLevelMission", "" + isTopLevelMission());
        // OK, now add all our items:
        for (DroidInstruction droidInstruction : droidInstructions) {
            droidInstruction.addToXML(doc);
        }
        return typeElement;
    }
}
