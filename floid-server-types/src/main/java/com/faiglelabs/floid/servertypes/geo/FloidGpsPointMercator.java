/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.servertypes.geo;

/**
 * A GPS point in Mercator form
 */
public class FloidGpsPointMercator extends FloidGpsPoint {
    private static final double SCALE = (10000.0/90.0) * 1000.0; // 10000 km over 90 degrees in meters
    private double latMercator;
    private double lngMercator;

    /**
     * Create a GPS Point in Mercator form from the GPS point
     * @param floidGpsPoint the GPS point in normal form
     */
    public FloidGpsPointMercator(FloidGpsPoint floidGpsPoint) {
        super(floidGpsPoint);
        // Project this point onto a Mercator grid:
        latMercator = floidGpsPoint.getLatitude() * SCALE;
        lngMercator = floidGpsPoint.getLongitude() * SCALE * Math.cos(Math.toRadians(floidGpsPoint.getLatitude()));
    }

    /**
     * Factory a new floid GPS point from this configuration
     * @return the factories GPS Point
     */
    public FloidGpsPoint floidGpsPointFactory() {
        return new FloidGpsPoint(this.getFloidId(), this.getFloidUuid(), this.getStatusTime(), this.getLatitude(), this.getLongitude(), this.getAltitude());
    }

    /**
     * Get the Lat in Mercator form for this point
     * @return the Lat in Mercator form for this point
     */
    public double getLatMercator() {
        return latMercator;
    }

    /**
     * Set the Lat in Mercator form to the point
     * @param latMercator the Lat in Mercator form
     */
    @SuppressWarnings("unused")
    public void setLatMercator(double latMercator) {
        this.latMercator = latMercator;
    }

    /**
     * Get the Lng in Mercator form for this point
     * @return the Lng in Mercator form for this point
     */
    public double getLngMercator() {
        return lngMercator;
    }

    /**
     * Set the Lng in Mercator form to the point
     * @param lngMercator the Lng in Mercator form
     */
    @SuppressWarnings("unused")
    public void setLngMercator(double lngMercator) {
        this.lngMercator = lngMercator;
    }
}
