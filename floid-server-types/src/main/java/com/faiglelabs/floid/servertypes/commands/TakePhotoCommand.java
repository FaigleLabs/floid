/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Take photo command. Takes a single photo.
 */
@Entity
public class TakePhotoCommand extends DroidCommand {
    /**
     * Instantiates a new Take photo command.
     */
    public TakePhotoCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        StopVideoCommand stopVideoCommand = new StopVideoCommand();
        super.factory(stopVideoCommand, droidMission);
        return stopVideoCommand;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable") JSONObject jsonObject = super.toJSON();
        // No parameters to add...
        return jsonObject;
    }

    @Override
    public TakePhotoCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable") Element typeElement = super.addToXML(doc);
        // Adds nothing...
        return typeElement;
    }
}
