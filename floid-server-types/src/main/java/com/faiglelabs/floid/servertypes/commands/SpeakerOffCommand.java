/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;


/**
 * Speaker off command.  Follows a speaker on command and stops the ongoing speaker process.s
 */
@Entity
public class SpeakerOffCommand extends DroidCommand {
    /**
     * Instantiates a new Speaker off command.
     */
    public SpeakerOffCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        SpeakerOffCommand speakerOffCommand = new SpeakerOffCommand();
        super.factory(speakerOffCommand, droidMission);
        return speakerOffCommand;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable") JSONObject jsonObject = super.toJSON();
        // No parameters to add
        return jsonObject;
    }

    @Override
    public SpeakerOffCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable") Element typeElement = super.addToXML(doc);
        // Adds nothing...
        return typeElement;
    }

}

