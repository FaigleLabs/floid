/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.statuses;

import com.faiglelabs.floid.database.FloidDatabaseTables;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Floid debug message.
 */
@Entity
@Table(name = FloidDatabaseTables.FLOID_DEBUG_MESSAGE,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidUuidIndex", columnList = "floidUuid"),
        @Index(name="timestampIndex", columnList = "timestamp"),
        @Index(name="typeIndex", columnList = "type"),
        @Index(name="floidMessageNumberIndex", columnList = "floidMessageNumber")
})
public class FloidDebugMessage extends FloidMessage {

    @Column(length = 15000)
    private String debugMessage;

    /**
     * Instantiates a new Floid debug message.
     */
    public FloidDebugMessage() {
        debugMessage = "";
        floidMessageNumber = getNextFloidMessageNumber();
    }

    /**
     * Gets debug message.
     *
     * @return the debug message
     */
    @SuppressWarnings("unused")
    public String getDebugMessage() {
        return debugMessage;
    }

    /**
     * Sets debug message.
     *
     * @param debugMessage the debug message
     */
    public void setDebugMessage(String debugMessage) {
        this.debugMessage = debugMessage;
    }

    @Override
    public FloidDebugMessage fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        debugMessage = jsonObject.getString("debugMessage");
        return this;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("debugMessage", debugMessage);
        return jsonObject;
    }
}
