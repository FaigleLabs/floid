/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * FloidServerThreadDumper.java
 */

package com.faiglelabs.floid.servertypes.pins;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Alternate version of pin set with id
 */
@SuppressWarnings("WeakerAccess")
public class FloidPinSetAlt {

    /**
     * The Id.
     */
    public Long id;
    /**
     * The pin set name
     */
    public String pinSetName;
    /**
     * The list of the floid pins in alternate form
     */
    public List<FloidPinAlt> pinAltList;

    /**
     * Instantiates a new Floid pin set alt.
     */
    public FloidPinSetAlt() {
        super();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets pin alt list.
     *
     * @return the pin alt list
     */
    public List<FloidPinAlt> getPinAltList() {
        return pinAltList;
    }

    /**
     * Sets pin alt list.
     *
     * @param pinAltList the pin alt list
     */
    public void setPinAltList(List<FloidPinAlt> pinAltList) {
        this.pinAltList = pinAltList;
    }

    /**
     * Gets pin set name.
     *
     * @return the pin set name
     */
    public String getPinSetName() {
        return pinSetName;
    }

    /**
     * Sets pin set name.
     *
     * @param pinSetName the pin set name
     */
    public void setPinSetName(String pinSetName) {
        this.pinSetName = pinSetName;
    }

    /**
     * Convert from JSON
     * @param jsonObject the JSON Object
     * @return a FloidPinSetAlt
     * @throws JSONException upon error
     */
    public FloidPinSetAlt fromJSON(JSONObject jsonObject) throws JSONException {
        try {
            id = jsonObject.getLong("id");
            pinSetName = jsonObject.getString("pinSetName");
            pinAltList = new ArrayList<>();
            JSONArray floidPinAltJSONArray = jsonObject.getJSONArray("pinAltList");
            for (int i = 0; i < floidPinAltJSONArray.length(); ++i) {
                FloidPinAlt floidPinAlt = new FloidPinAlt().fromJSON(floidPinAltJSONArray.getJSONObject(i));
                pinAltList.add(floidPinAlt);
            }
        } catch (Exception e) {
            // Do nothing - there is no global logger for all places this type is used :)
        }
        return this;
    }
}
