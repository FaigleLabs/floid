/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Retrieve logs command
 */
@Entity
public class RetrieveLogsCommand extends DroidCommand {
    /**
     * The log limit 0 = default (200 lines)
     */
    private int logLimit = 0;

    /**
     * Instantiates a new Retrieve Logs command.
     */
    public RetrieveLogsCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        RetrieveLogsCommand retrieveLogsCommand = new RetrieveLogsCommand();
        super.factory(retrieveLogsCommand, droidMission);
        retrieveLogsCommand.logLimit = logLimit;
        return retrieveLogsCommand;
    }

    /**
     * Gets log limit.
     *
     * @return the log limit
     */
    public int getLogLimit() {
        return logLimit;
    }

    /**
     * Sets log limit.
     *
     * @param logLimit the log limit
     */
    public void setLogLimit(int logLimit) {
        this.logLimit = logLimit;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("logLimit", logLimit);
        return jsonObject;
    }

    @Override
    public RetrieveLogsCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        logLimit = jsonObject.getInt("logLimit");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("logLimit", "" + logLimit);
        return typeElement;
    }
}
