/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.pins;

import com.faiglelabs.floid.database.FloidDatabaseTables;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type Floid pin.
 */
@Entity
@Table(name = FloidDatabaseTables.FLOID_PIN,
        indexes = {
                @Index(name = "idIndex", columnList = "id")
        })
public class FloidPin implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private FloidPinSet pinSet;
    private String pinName;
    private Double pinLat;
    private Double pinLng;
    private Double pinHeight;
    private boolean pinHeightAuto;

    /**
     * Instantiates a new Floid pin.
     */
    public FloidPin() {
        super();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets pin height.
     *
     * @return the pin height
     */
    public Double getPinHeight() {
        return pinHeight;
    }

    /**
     * Sets pin height.
     *
     * @param pinHeight the pin height
     */
    public void setPinHeight(Double pinHeight) {
        this.pinHeight = pinHeight;
    }

    /**
     * Gets pin lat.
     *
     * @return the pin lat
     */
    public Double getPinLat() {
        return pinLat;
    }

    /**
     * Sets pin lat.
     *
     * @param pinLat the pin lat
     */
    public void setPinLat(Double pinLat) {
        this.pinLat = pinLat;
    }

    /**
     * Gets pin lng.
     *
     * @return the pin lng
     */
    public Double getPinLng() {
        return pinLng;
    }

    /**
     * Sets pin lng.
     *
     * @param pinLng the pin lng
     */
    public void setPinLng(Double pinLng) {
        this.pinLng = pinLng;
    }

    /**
     * Gets pin name.
     *
     * @return the pin name
     */
    public String getPinName() {
        return pinName;
    }

    /**
     * Sets pin name.
     *
     * @param pinName the pin name
     */
    public void setPinName(String pinName) {
        this.pinName = pinName;
    }

    /**
     * Gets pin set.
     *
     * @return the pin set
     */
    @SuppressWarnings("unused")
    public FloidPinSet getPinSet() {
        return pinSet;
    }

    /**
     * Sets pin set.
     *
     * @param pinSet the pin set
     */
    @SuppressWarnings("unused, WeakerAccess")
    public void setPinSet(FloidPinSet pinSet) {
        this.pinSet = pinSet;
    }

    /**
     * Is pin height auto boolean.
     *
     * @return the boolean
     */
    public boolean isPinHeightAuto() {
        return pinHeightAuto;
    }

    /**
     * Sets pin height auto.
     *
     * @param pinHeightAuto the pin height auto
     */
    public void setPinHeightAuto(boolean pinHeightAuto) {
        this.pinHeightAuto = pinHeightAuto;
    }


}
