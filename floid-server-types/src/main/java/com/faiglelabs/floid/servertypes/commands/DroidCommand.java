/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;


/**
 * Droid command - the supertype for all commands
 */
@Entity
public class DroidCommand extends DroidInstruction {
    /**
     * Instantiates a new Droid command.
     */
    public DroidCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        DroidCommand droidCommand = new DroidCommand();
        super.factory(droidCommand, droidMission);
        return droidCommand;
    }

    @SuppressWarnings("EmptyMethod")
    @Override
    public DroidInstruction factory(DroidInstruction droidInstruction, DroidMission droidMission) {
        return super.factory(droidInstruction, droidMission);
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable")
        JSONObject jsonObject = super.toJSON();

        return jsonObject;
    }

    @Override
    public DroidCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable")
        Element typeElement = super.addToXML(doc);
        return typeElement;
    }

}
