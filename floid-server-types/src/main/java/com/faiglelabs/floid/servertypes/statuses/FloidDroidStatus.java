/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.statuses;

import com.faiglelabs.floid.database.FloidDatabaseTables;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Floid Droid Status
 */
@Entity
@Table(name = FloidDatabaseTables.FLOID_DROID_STATUS,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidUuidIndex", columnList = "floidUuid"),
        @Index(name="timestampIndex", columnList = "timestamp"),
        @Index(name="typeIndex", columnList = "type"),
        @Index(name="floidMessageNumberIndex", columnList = "floidMessageNumber"),
        @Index(name="statusNumberIndex", columnList = "statusNumber")
})
@SuppressWarnings("unused, WeakerAccess")
public class FloidDroidStatus extends FloidMessage {
    /**
     * Droid mode test
     */
    public final static int DROID_MODE_TEST = 0;
    /**
     * Droid mode mission
     */
    public final static int DROID_MODE_MISSION = 1;
    /**
     * Droid status idle
     */
    public final static int DROID_STATUS_IDLE = 0;
    /**
     * Droid status processing
     */
    public final static int DROID_STATUS_PROCESSING = 1;
    /**
     * Droid status error
     */
    public final static int DROID_STATUS_ERROR = 2;

    /**
     * The Droid status time
     */
    protected long statusTime = 0L;  // Status time
    /**
     * The Droid status number
     */
    protected long statusNumber = 0L;
    // GPS:
    /**
     * The GPS status time
     */
    protected long gpsStatusTime = 0L;
    /**
     * Does the GPS have required accuracy
     */
    protected boolean gpsHasAccuracy = false;
    /**
     * The GPS accuracy
     */
    protected float gpsAccuracy = 0.0f;
    /**
     * The GPS latitude
     */
    protected double gpsLatitude = 0.0;
    /**
     * The GPS longitude
     */
    protected double gpsLongitude = 0.0;
    /**
     * Does the GPS have altitude
     */
    protected boolean gpsHasAltitude = false;
    /**
     * Tbe GPS altitude
     */
    protected double gpsAltitude = 0.0;
    /**
     * Does the GPS have bearing/speed
     */
    protected boolean gpsHasBearing = false;
    /**
     * The GPS bearing
     */
    protected float gpsBearing = 0.0f;
    /**
     * The GPS speed
     */
    protected float gpsSpeed = 0.0f;
    /**
     * THe GPS provider name
     */
    protected String gpsProvider = "";

    // Floid connected:
    /**
     * Is the Floid connected
     */
    protected boolean floidConnected = false;
    // Floid error:
    /**
     * Does the Floid have an error
     */
    protected boolean floidError = false;
    // Status and mode:
    /**
     * Current Floid status
     */
    protected int currentStatus = -1;                  // QUESTION: DOES THIS STAY OR GO INTO
    /**
     * Current Floid mode
     */
    protected int currentMode = DROID_MODE_TEST;
    // Mission number:
    /**
     * Current mission number
     */
    protected int missionNumber = -1;
    // Droid started:
    /**
     * Is the Droid started
     */
    protected boolean droidStarted = false;
    // Server:
    /**
     * Is the server connected
     */
    protected boolean serverConnected = false;
    /**
     * Does the server have an error
     */
    protected boolean serverError = false;
    // Battery voltage:
    /**
     * Battery voltage
     */
    protected double batteryVoltage = 0;
    /**
     * Battery level
     */
    protected double batteryLevel = -1.0;
    /**
     * Battery raw level
     */
    protected int batteryRawLevel = 0;
    /**
     * Batter raw scale
     */
    protected int batteryRawScale = 1;
    // Helis:
    /**
     * Are the helis started
     */
    protected boolean helisStarted = false;
    /**
     * Have we lifted off
     */
    protected boolean liftedOff = false;
    // Parachute:]
    /**
     * Has the parachute been deployed
     */
    protected boolean parachuteDeployed = false;
    // Photo / Video:
    /**
     * Are we taking a photo
     */
    protected boolean takingPhoto = false;
    /**
     * Photo number
     */
    protected int photoNumber = -1;
    /**
     * Are we performing photo strobe
     */
    protected boolean photoStrobe = false;
    /**
     * Photo strobe delay
     */
    protected int photoStrobeDelay = 0;
    /**
     * Are we taking video
     */
    protected boolean takingVideo = false;
    /**
     * Video number
     */
    protected int videoNumber = -1;
    // Home position:
    /**
     * Has the home position been acquired
     */
    protected boolean homePositionAcquired = false;
    /**
     * Home position longitude
     */
    protected double homePositionX = 0.0;
    /**
     * Home position latitude
     */
    protected double homePositionY = 0.0;
    /**
     * Home position altitude
     */
    protected double homePositionZ = 0.0;
    /**
     * Home position heading
     */
    protected double homePositionHeading = 0.0;

    /**
     * Instantiates a new Floid droid status.
     */
    public FloidDroidStatus() {
        type = getClass().getName();
        floidMessageNumber = getNextFloidMessageNumber();
    }


    public FloidDroidStatus(
            long id,
            int floidId,
            String floidUuid,
            long floidMessageNumber,
            long timestamp,
            long statusTime,
            long statusNumber,
            long gpsStatusTime,
            boolean gpsHasAccuracy,
            float gpsAccuracy,
            double gpsLatitude,
            double gpsLongitude,
            boolean gpsHasAltitude,
            double gpsAltitude,
            boolean gpsHasBearing,
            float gpsBearing,
            float gpsSpeed,
            String gpsProvider,
            boolean floidConnected,
            boolean floidError,
            int currentStatus,
            int currentMode,
            int missionNumber,
            boolean droidStarted,
            boolean serverConnected,
            boolean serverError,
            double batteryVoltage,
            double batteryLevel,
            int batteryRawLevel,
            int batteryRawScale,
            boolean helisStarted,
            boolean liftedOff,
            boolean parachuteDeployed,
            boolean takingPhoto,
            int photoNumber,
            boolean photoStrobe,
            int photoStrobeDelay,
            boolean takingVideo,
            int videoNumber,
            boolean homePositionAcquired,
            double homePositionX,
            double homePositionY,
            double homePositionZ,
            double homePositionHeading
    ) {
        this.type = getClass().getName();
        this.id = id;
        this.floidId = floidId;
        this.floidUuid = floidUuid;
        this.floidMessageNumber = floidMessageNumber;
        this.timestamp = timestamp;
        this.statusTime = statusTime;
        this.statusNumber = statusNumber;
        this.gpsStatusTime = gpsStatusTime;
        this.gpsHasAccuracy = gpsHasAccuracy;
        this.gpsAccuracy = gpsAccuracy;
        this.gpsLatitude = gpsLatitude;
        this.gpsLongitude = gpsLongitude;
        this.gpsHasAltitude = gpsHasAltitude;
        this.gpsAltitude = gpsAltitude;
        this.gpsHasBearing = gpsHasBearing;
        this.gpsBearing = gpsBearing;
        this.gpsSpeed = gpsSpeed;
        this.gpsProvider = gpsProvider;
        this.floidConnected = floidConnected;
        this.floidError = floidError;
        this.currentStatus = currentStatus;
        this.currentMode = currentMode;
        this.missionNumber = missionNumber;
        this.droidStarted = droidStarted;
        this.serverConnected = serverConnected;
        this.serverError = serverError;
        this.batteryVoltage = batteryVoltage;
        this.batteryLevel = batteryLevel;
        this.batteryRawLevel = batteryRawLevel;
        this.batteryRawScale = batteryRawScale;
        this.helisStarted = helisStarted;
        this.liftedOff = liftedOff;
        this.parachuteDeployed = parachuteDeployed;
        this.takingPhoto = takingPhoto;
        this.photoNumber = photoNumber;
        this.photoStrobe = photoStrobe;
        this.photoStrobeDelay = photoStrobeDelay;
        this.takingVideo = takingVideo;
        this.videoNumber = videoNumber;
        this.homePositionAcquired = homePositionAcquired;
        this.homePositionX = homePositionX;
        this.homePositionY = homePositionY;
        this.homePositionZ = homePositionZ;
        this.homePositionHeading = homePositionHeading;
    }
    /**
     * Get the droid battery voltage
     *
     * @return the droid battery voltage
     */
    public double getBatteryVoltage() {
        return batteryVoltage;
    }

    /**
     * Set the droid battery voltage
     *
     * @param batteryVoltage the droid battery voltage
     */
    public void setBatteryVoltage(double batteryVoltage) {
        this.batteryVoltage = batteryVoltage;
    }

    /**
     * Is the droid started
     *
     * @return true if droid is started
     */
    public boolean isDroidStarted() {
        return droidStarted;
    }

    /**
     * Set the droid started
     *
     * @param droidStarted true if droid is started
     */
    public void setDroidStarted(boolean droidStarted) {
        this.droidStarted = droidStarted;
    }

    /**
     * Get the current droid mode
     *
     * @return the current droid mode
     */
    public int getCurrentMode() {
        return currentMode;
    }

    /**
     * Set the current droid mode
     *
     * @param currentMode the current droid mode
     */
    public void setCurrentMode(int currentMode) {
        this.currentMode = currentMode;
    }

    /**
     * Get the current photo number
     *
     * @return the current photo number
     */
    public int getPhotoNumber() {
        return photoNumber;
    }

    /**
     * Set the current photo number
     *
     * @param photoNumber the current photo number
     */
    public void setPhotoNumber(int photoNumber) {
        this.photoNumber = photoNumber;
    }

    /**
     * Get the current video number
     *
     * @return the current video number
     */
    public int getVideoNumber() {
        return videoNumber;
    }

    /**
     * Set the current video number
     *
     * @param videoNumber the current video number
     */
    public void setVideoNumber(int videoNumber) {
        this.videoNumber = videoNumber;
    }

    /**
     * Is the droid lifted off
     *
     * @return true if droid is lifted off
     */
    public boolean isLiftedOff() {
        return liftedOff;
    }

    /**
     * Set the droid is lifted off
     *
     * @param liftedOff true if droid is lifted off
     */
    public void setLiftedOff(boolean liftedOff) {
        this.liftedOff = liftedOff;
    }

    /**
     * Is the floid server connected
     *
     * @return true if the floid server is connected
     */
    public boolean isServerConnected() {
        return serverConnected;
    }

    /**
     * Set the floid server connected
     *
     * @param serverConnected true if the floid server is connected
     */
    public void setServerConnected(boolean serverConnected) {
        this.serverConnected = serverConnected;
    }

    /**
     * Is the floid server in error from the droid view
     *
     * @return true if the floid server is in error from the droid view
     */
    public boolean isServerError() {
        return serverError;
    }

    /**
     * Set the floid server in error from the droid view
     *
     * @param serverError true if the floid server is in error from the droid view
     */
    public void setServerError(boolean serverError) {
        this.serverError = serverError;
    }

    /**
     * Is the droid taking a photo (including video snapshot while video is running)
     *
     * @return true if the droid is taking a photo (including video snapshot while video is running)
     */
    public boolean isTakingPhoto() {
        return takingPhoto;
    }

    /**
     * Set the droid taking a photo (including video snapshot while video is running)
     *
     * @param takingPhoto true if the droid is taking a photo (including video snapshot while video is running)
     */
    public void setTakingPhoto(boolean takingPhoto) {
        this.takingPhoto = takingPhoto;
    }

    /**
     * Is the droid taking a video
     *
     * @return true if the droid is taking a video
     */
    public boolean isTakingVideo() {
        return takingVideo;
    }

    /**
     * Set the droid taking a video
     *
     * @param takingVideo true if the droid is taking a video
     */
    public void setTakingVideo(boolean takingVideo) {
        this.takingVideo = takingVideo;
    }

    /**
     * Are the helis started
     *
     * @return true if the helis are started
     */
    public boolean isHelisStarted() {
        return helisStarted;
    }

    /**
     * Set the helis started
     *
     * @param helisStarted true if the helis are started
     */
    public void setHelisStarted(boolean helisStarted) {
        this.helisStarted = helisStarted;
    }

    /**
     * Is the parachute deployed
     *
     * @return true if the parachute is deployed
     */
    public boolean isParachuteDeployed() {
        return parachuteDeployed;
    }

    /**
     * St the parachute deployed
     *
     * @param parachuteDeployed true if the parachute is deployed
     */
    public void setParachuteDeployed(boolean parachuteDeployed) {
        this.parachuteDeployed = parachuteDeployed;
    }

    /**
     * Get the current droid status - DROID_STATUS_IDLE, DROID_STATUS_PROCESSING or DROID_STATUS_ERROR
     *
     * @return the current droid status
     */
    public int getCurrentStatus() {
        return currentStatus;
    }

    /**
     * Set the current droid status - DROID_STATUS_IDLE, DROID_STATUS_PROCESSING or DROID_STATUS_ERROR
     *
     * @param currentStatus the current droid status - DROID_STATUS_IDLE, DROID_STATUS_PROCESSING or DROID_STATUS_ERROR
     */
    public void setCurrentStatus(int currentStatus) {
        this.currentStatus = currentStatus;
    }

    /**
     * Get the droid status time
     *
     * @return the droid status time
     */
    public long getStatusTime() {
        return statusTime;
    }

    /**
     * Set the droid status time
     *
     * @param statusTime the droid status time
     */
    public void setStatusTime(long statusTime) {
        this.statusTime = statusTime;
    }

    /**
     * Is the home position acquired
     *
     * @return true if the home position is acquired
     */
    public boolean isHomePositionAcquired() {
        return homePositionAcquired;
    }

    /**
     * Set the home position acquired
     *
     * @param homePositionAcquired true if the home position is acquired
     */
    public void setHomePositionAcquired(boolean homePositionAcquired) {
        this.homePositionAcquired = homePositionAcquired;
    }

    /**
     * Gets home position longitude (x)
     *
     * @return the home position longitude (x)
     */
    public double getHomePositionX() {
        return homePositionX;
    }

    /**
     * Sets home position longitude (x)
     *
     * @param homePositionX the home position longitude (x)
     */
    public void setHomePositionX(double homePositionX) {
        this.homePositionX = homePositionX;
    }

    /**
     * Gets home position latitude (y)
     *
     * @return the home position latitude (y)
     */
    public double getHomePositionY() {
        return homePositionY;
    }

    /**
     * Sets home position latitude (y)
     *
     * @param homePositionY the home position latitude (y)
     */
    public void setHomePositionY(double homePositionY) {
        this.homePositionY = homePositionY;
    }

    /**
     * Gets home position altitude (z)
     *
     * @return the home position altitude (z)
     */
    public double getHomePositionZ() {
        return homePositionZ;
    }

    /**
     * Sets home position altitude (z)
     *
     * @param homePositionZ the home position altitude (z)
     */
    public void setHomePositionZ(double homePositionZ) {
        this.homePositionZ = homePositionZ;
    }

    /**
     * Gets home position heading.
     *
     * @return the home position heading
     */
    public double getHomePositionHeading() {
        return homePositionHeading;
    }

    /**
     * Sets home position heading.
     *
     * @param homePositionHeading the home position heading
     */
    public void setHomePositionHeading(double homePositionHeading) {
        this.homePositionHeading = homePositionHeading;
    }

    /**
     * Gets mission number.
     *
     * @return the mission number
     */
    public int getMissionNumber() {
        return missionNumber;
    }

    /**
     * Sets mission number.
     *
     * @param missionNumber the mission number
     */
    public void setMissionNumber(int missionNumber) {
        this.missionNumber = missionNumber;
    }

    /**
     * Is photo strobe boolean.
     *
     * @return the boolean
     */
    public boolean isPhotoStrobe() {
        return photoStrobe;
    }

    /**
     * Sets photo strobe.
     *
     * @param photoStrobe the photo strobe
     */
    public void setPhotoStrobe(boolean photoStrobe) {
        this.photoStrobe = photoStrobe;
    }

    /**
     * Gets photo strobe delay.
     *
     * @return the photo strobe delay
     */
    public int getPhotoStrobeDelay() {
        return photoStrobeDelay;
    }

    /**
     * Sets photo strobe delay.
     *
     * @param photoStrobeDelay the photo strobe delay
     */
    public void setPhotoStrobeDelay(int photoStrobeDelay) {
        this.photoStrobeDelay = photoStrobeDelay;
    }

    /**
     * Calc is ready to fly boolean.
     *
     * @return true if ready to fly
     */
    public boolean calcIsReadyToFly() {
        return droidStarted
                && floidConnected
                && !floidError
                && serverConnected
                && !serverError
                && helisStarted
                && !parachuteDeployed;
    }

    /**
     * Calc is flying boolean.
     *
     * @return true if flying
     */
    public boolean calcIsFlying() {
        return liftedOff
                && !parachuteDeployed
                && floidConnected
                && !floidError;
    }

    /**
     * Gets status number.
     *
     * @return the status number
     */
    public long getStatusNumber() {
        return statusNumber;
    }

    /**
     * Sets status number.
     *
     * @param statusNumber the status number
     */
    public void setStatusNumber(long statusNumber) {
        this.statusNumber = statusNumber;
    }

    /**
     * Is floid connected
     *
     * @return true if floid connected
     */
    public boolean isFloidConnected() {
        return floidConnected;
    }

    /**
     * Sets floid connected.
     *
     * @param floidConnected true if floid connected
     */
    public void setFloidConnected(boolean floidConnected) {
        this.floidConnected = floidConnected;
    }

    /**
     * Is floid in error.
     *
     * @return true if the floid is in error
     */
    public boolean isFloidError() {
        return floidError;
    }

    /**
     * Sets floid error.
     *
     * @param floidError true if the floid isi n error
     */
    public void setFloidError(boolean floidError) {
        this.floidError = floidError;
    }

    @Override
    public FloidDroidStatus fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        batteryVoltage = jsonObject.getDouble("batteryVoltage");
        batteryLevel = jsonObject.getDouble("batteryLevel");
        batteryRawLevel = jsonObject.getInt("batteryRawLevel");
        batteryRawScale = jsonObject.getInt("batteryRawScale");
        statusTime = jsonObject.getLong("statusTime");
        statusNumber = jsonObject.getLong("statusNumber");
        currentMode = jsonObject.getInt("currentMode");
        currentStatus = jsonObject.getInt("currentStatus");
        droidStarted = jsonObject.getBoolean("droidStarted");
        floidConnected = jsonObject.getBoolean("floidConnected");
        floidError = jsonObject.getBoolean("floidError");
        serverConnected = jsonObject.getBoolean("serverConnected");
        serverError = jsonObject.getBoolean("serverError");
        helisStarted = jsonObject.getBoolean("helisStarted");
        liftedOff = jsonObject.getBoolean("liftedOff");
        parachuteDeployed = jsonObject.getBoolean("parachuteDeployed");
        photoNumber = jsonObject.getInt("photoNumber");
        takingPhoto = jsonObject.getBoolean("takingPhoto");
        videoNumber = jsonObject.getInt("videoNumber");
        takingVideo = jsonObject.getBoolean("takingVideo");
        missionNumber = jsonObject.getInt("missionNumber");
        homePositionAcquired = jsonObject.getBoolean("homePositionAcquired");
        homePositionX = jsonObject.getDouble("homePositionX");
        homePositionY = jsonObject.getDouble("homePositionY");
        homePositionZ = jsonObject.getDouble("homePositionZ");
        homePositionHeading = jsonObject.getDouble("homePositionHeading");
        photoStrobe = jsonObject.getBoolean("photoStrobe");
        photoStrobeDelay = jsonObject.getInt("photoStrobeDelay");

        gpsStatusTime = jsonObject.getLong("gpsStatusTime");
        gpsHasAccuracy = jsonObject.getBoolean("gpsHasAccuracy");
        gpsAccuracy = (float) jsonObject.getDouble("gpsAccuracy");
        gpsLatitude = jsonObject.getDouble("gpsLatitude");
        gpsLongitude = jsonObject.getDouble("gpsLongitude");
        gpsHasAltitude = jsonObject.getBoolean("gpsHasAltitude");
        gpsAltitude = (float) jsonObject.getDouble("gpsAltitude");
        gpsHasBearing = jsonObject.getBoolean("gpsHasBearing");
        gpsBearing = (float) jsonObject.getDouble("gpsBearing");
        gpsSpeed = (float) jsonObject.getDouble("gpsSpeed");
        gpsProvider = jsonObject.getString("gpsProvider");
        return this;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();

        jsonObject.put("batteryVoltage", batteryVoltage);
        jsonObject.put("batteryLevel", batteryLevel);
        jsonObject.put("batteryRawLevel", batteryRawLevel);
        jsonObject.put("batteryRawScale", batteryRawScale);
        jsonObject.put("statusTime", statusTime);
        jsonObject.put("statusNumber", statusNumber);
        jsonObject.put("currentMode", currentMode);
        jsonObject.put("currentStatus", currentStatus);
        jsonObject.put("droidStarted", droidStarted);
        jsonObject.put("floidConnected", floidConnected);
        jsonObject.put("floidError", floidError);
        jsonObject.put("serverConnected", serverConnected);
        jsonObject.put("serverError", serverError);
        jsonObject.put("helisStarted", helisStarted);
        jsonObject.put("liftedOff", liftedOff);
        jsonObject.put("parachuteDeployed", parachuteDeployed);
        jsonObject.put("photoNumber", photoNumber);
        jsonObject.put("takingPhoto", takingPhoto);
        jsonObject.put("videoNumber", videoNumber);
        jsonObject.put("takingVideo", takingVideo);
        jsonObject.put("missionNumber", missionNumber);
        jsonObject.put("homePositionAcquired", homePositionAcquired);
        jsonObject.put("homePositionX", homePositionX);
        jsonObject.put("homePositionY", homePositionY);
        jsonObject.put("homePositionZ", homePositionZ);
        jsonObject.put("homePositionHeading", homePositionHeading);
        jsonObject.put("photoStrobe", photoStrobe);
        jsonObject.put("photoStrobeDelay", photoStrobeDelay);
        jsonObject.put("gpsStatusTime", gpsStatusTime);
        jsonObject.put("gpsHasAccuracy", gpsHasAccuracy);
        jsonObject.put("gpsAccuracy", gpsAccuracy);
        jsonObject.put("gpsLatitude", gpsLatitude);
        jsonObject.put("gpsLongitude", gpsLongitude);
        jsonObject.put("gpsHasAltitude", gpsHasAltitude);
        jsonObject.put("gpsAltitude", gpsAltitude);
        jsonObject.put("gpsHasBearing", gpsHasBearing);
        jsonObject.put("gpsBearing", gpsBearing);
        jsonObject.put("gpsSpeed", gpsSpeed);
        jsonObject.put("gpsProvider", gpsProvider);
        return jsonObject;
    }

    /**
     * Gets status string.
     *
     * @return the status string
     */
    public String getStatusString() {
        switch (currentStatus) {
            case DROID_STATUS_IDLE: {
                return "Idle";
            }
            case DROID_STATUS_PROCESSING: {
                return "Processing";
            }
            case DROID_STATUS_ERROR: {
                return "Error";
            }
            default: {
                return "Unknown";
            }
        }
    }

    /**
     * Gets mode string.
     *
     * @return the mode string
     */
    public String getModeString() {
        switch (currentMode) {
            case DROID_MODE_TEST: {
                return "Test";
            }
            case DROID_MODE_MISSION: {
                return "Mission";
            }
            default: {
                return "Unknown";
            }
        }
    }

    /**
     * Gets battery level.
     *
     * @return the battery level
     */
    public double getBatteryLevel() {
        return batteryLevel;
    }

    /**
     * Sets battery level.
     *
     * @param batteryLevel the battery level
     */
    public void setBatteryLevel(double batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    /**
     * Gets battery raw level.
     *
     * @return the battery raw level
     */
    public int getBatteryRawLevel() {
        return batteryRawLevel;
    }

    /**
     * Sets battery raw level.
     *
     * @param batteryRawLevel the battery raw level
     */
    public void setBatteryRawLevel(int batteryRawLevel) {
        this.batteryRawLevel = batteryRawLevel;
    }

    /**
     * Gets battery raw scale.
     *
     * @return the battery raw scale
     */
    public int getBatteryRawScale() {
        return batteryRawScale;
    }

    /**
     * Sets battery raw scale.
     *
     * @param batteryRawScale the battery raw scale
     */
    public void setBatteryRawScale(int batteryRawScale) {
        this.batteryRawScale = batteryRawScale;
    }

    /**
     * Gets gps status time.
     *
     * @return the gps status time
     */
    public long getGpsStatusTime() {
        return gpsStatusTime;
    }

    /**
     * Sets gps status time.
     *
     * @param gpsStatusTime the gps status time
     */
    public void setGpsStatusTime(long gpsStatusTime) {
        this.gpsStatusTime = gpsStatusTime;
    }

    /**
     * Is gps has accuracy boolean.
     *
     * @return the boolean
     */
    public boolean isGpsHasAccuracy() {
        return gpsHasAccuracy;
    }

    /**
     * Sets gps has accuracy.
     *
     * @param gpsHasAccuracy the gps has accuracy
     */
    public void setGpsHasAccuracy(boolean gpsHasAccuracy) {
        this.gpsHasAccuracy = gpsHasAccuracy;
    }

    /**
     * Gets gps accuracy.
     *
     * @return the gps accuracy
     */
    public float getGpsAccuracy() {
        return gpsAccuracy;
    }

    /**
     * Sets gps accuracy.
     *
     * @param gpsAccuracy the gps accuracy
     */
    public void setGpsAccuracy(float gpsAccuracy) {
        this.gpsAccuracy = gpsAccuracy;
    }

    /**
     * Gets gps latitude.
     *
     * @return the gps latitude
     */
    public double getGpsLatitude() {
        return gpsLatitude;
    }

    /**
     * Sets gps latitude.
     *
     * @param gpsLatitude the gps latitude
     */
    public void setGpsLatitude(double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    /**
     * Gets gps longitude.
     *
     * @return the gps longitude
     */
    public double getGpsLongitude() {
        return gpsLongitude;
    }

    /**
     * Sets gps longitude.
     *
     * @param gpsLongitude the gps longitude
     */
    public void setGpsLongitude(double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    /**
     * Is gps has altitude boolean.
     *
     * @return the boolean
     */
    public boolean isGpsHasAltitude() {
        return gpsHasAltitude;
    }

    /**
     * Sets gps has altitude.
     *
     * @param gpsHasAltitude the gps has altitude
     */
    public void setGpsHasAltitude(boolean gpsHasAltitude) {
        this.gpsHasAltitude = gpsHasAltitude;
    }

    /**
     * Gets gps altitude.
     *
     * @return the gps altitude
     */
    public double getGpsAltitude() {
        return gpsAltitude;
    }

    /**
     * Sets gps altitude.
     *
     * @param gpsAltitude the gps altitude
     */
    public void setGpsAltitude(double gpsAltitude) {
        this.gpsAltitude = gpsAltitude;
    }

    /**
     * Is gps has bearing boolean.
     *
     * @return the boolean
     */
    public boolean isGpsHasBearing() {
        return gpsHasBearing;
    }

    /**
     * Sets gps has bearing.
     *
     * @param gpsHasBearing the gps has bearing
     */
    public void setGpsHasBearing(boolean gpsHasBearing) {
        this.gpsHasBearing = gpsHasBearing;
    }

    /**
     * Gets gps bearing.
     *
     * @return the gps bearing
     */
    public float getGpsBearing() {
        return gpsBearing;
    }

    /**
     * Sets gps bearing.
     *
     * @param gpsBearing the gps bearing
     */
    public void setGpsBearing(float gpsBearing) {
        this.gpsBearing = gpsBearing;
    }

    /**
     * Gets gps speed.
     *
     * @return the gps speed
     */
    public float getGpsSpeed() {
        return gpsSpeed;
    }

    /**
     * Sets gps speed.
     *
     * @param gpsSpeed the gps speed
     */
    public void setGpsSpeed(float gpsSpeed) {
        this.gpsSpeed = gpsSpeed;
    }

    /**
     * Gets gps provider.
     *
     * @return the gps provider
     */
    public String getGpsProvider() {
        return gpsProvider;
    }

    /**
     * Sets gps provider.
     *
     * @param gpsProvider the gps provider
     */
    public void setGpsProvider(String gpsProvider) {
        this.gpsProvider = gpsProvider;
    }
}
