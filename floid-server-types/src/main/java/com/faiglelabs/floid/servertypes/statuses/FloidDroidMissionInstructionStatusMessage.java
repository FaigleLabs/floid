/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.statuses;

import com.faiglelabs.floid.database.FloidDatabaseTables;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * A mission instruction status message.
 */
@SuppressWarnings("unused")
@Entity
@Table(name = FloidDatabaseTables.FLOID_DROID_MISSION_INSTRUCTION_STATUS_MESSAGE,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidUuidIndex", columnList = "floidUuid"),
        @Index(name="timestampIndex", columnList = "timestamp"),
        @Index(name="typeIndex", columnList = "type"),
        @Index(name="floidMessageNumberIndex", columnList = "floidMessageNumber")
})
public class FloidDroidMissionInstructionStatusMessage extends FloidMessage {
    private String messageType;
    private String missionName;
    private long missionId;
    private long instructionId;
    private String instructionType;
    private int instructionStatus;
    private String instructionStatusDisplayString;

    /**
     * Instantiates a new mission instruction status message.
     */
    public FloidDroidMissionInstructionStatusMessage() {
        type = getClass().getName();
        messageType = "";
        missionName = "";
        missionId = DroidMission.NO_MISSION;
        instructionId = DroidInstruction.NO_INSTRUCTION;
        instructionType = "";
        instructionStatus = -1;
        instructionStatusDisplayString = "";
        floidMessageNumber = getNextFloidMessageNumber();
    }

    /**
     * Gets instruction id.
     *
     * @return the instruction id
     */
    @SuppressWarnings("unused")
    public long getInstructionId() {
        return instructionId;
    }

    /**
     * Sets instruction id.
     *
     * @param instructionId the instruction id
     */
    @SuppressWarnings("unused")
    public void setInstructionId(long instructionId) {
        this.instructionId = instructionId;
    }

    /**
     * Gets instruction status.
     *
     * @return the instruction status
     */
    @SuppressWarnings("unused")
    public int getInstructionStatus() {
        return instructionStatus;
    }

    /**
     * Sets instruction status.
     *
     * @param instructionStatus the instruction status
     */
    @SuppressWarnings("unused")
    public void setInstructionStatus(int instructionStatus) {
        this.instructionStatus = instructionStatus;
    }

    /**
     * Gets instruction status display string.
     *
     * @return the instruction status display string
     */
    public String getInstructionStatusDisplayString() {
        return instructionStatusDisplayString;
    }

    /**
     * Sets instruction status display string.
     *
     * @param instructionStatusDisplayString the instruction status display string
     */
    public void setInstructionStatusDisplayString(String instructionStatusDisplayString) {
        this.instructionStatusDisplayString = instructionStatusDisplayString;
    }

    /**
     * Gets instruction type.
     *
     * @return the instruction type
     */
    public String getInstructionType() {
        return instructionType;
    }

    /**
     * Sets instruction type.
     *
     * @param instructionType the instruction type
     */
    public void setInstructionType(String instructionType) {
        this.instructionType = instructionType;
    }

    /**
     * Gets message type.
     *
     * @return the message type
     */
    @SuppressWarnings("unused")
    public String getMessageType() {
        return messageType;
    }

    /**
     * Sets message type.
     *
     * @param messageType the message type
     */
    @SuppressWarnings("unused")
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * Gets mission id.
     *
     * @return the mission id
     */
    @SuppressWarnings("unused")
    public long getMissionId() {
        return missionId;
    }

    /**
     * Sets mission id.
     *
     * @param missionId the mission id
     */
    @SuppressWarnings("unused")
    public void setMissionId(long missionId) {
        this.missionId = missionId;
    }

    /**
     * Gets mission name.
     *
     * @return the mission name
     */
    @SuppressWarnings("unused")
    public String getMissionName() {
        return missionName;
    }

    /**
     * Sets mission name.
     *
     * @param missionName the mission name
     */
    @SuppressWarnings("unused")
    public void setMissionName(String missionName) {
        this.missionName = missionName;
    }

    @Override
    public FloidDroidMissionInstructionStatusMessage fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        messageType = jsonObject.getString("messageType");
        missionName = jsonObject.getString("missionName");
        missionId = jsonObject.getLong("missionId");
        instructionId = jsonObject.getLong("instructionId");
        instructionType = jsonObject.getString("instructionType");
        instructionStatus = jsonObject.getInt("instructionStatus");
        instructionStatusDisplayString = jsonObject.getString("instructionStatusDisplayString");
        return this;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("messageType", messageType);
        jsonObject.put("missionName", missionName);
        jsonObject.put("missionId", missionId);
        jsonObject.put("instructionId", instructionId);
        jsonObject.put("instructionType", instructionType);
        jsonObject.put("instructionStatus", instructionStatus);
        jsonObject.put("instructionStatusDisplayString", instructionStatusDisplayString);
        return jsonObject;
    }
}
