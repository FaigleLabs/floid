/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.servertypes.floidid;

import javax.persistence.Entity;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Floid uuid and state.
 */
@SuppressWarnings("WeakerAccess")
@Entity
public class FloidUuidAndState extends FloidUuid implements Serializable {
    private static final long FLOID_UPTIME_INDICATOR_MILLIS = 3000L;
    /**
     * Is the Floid active.
     */
    protected boolean floidActive = false;
    /**
     * The Floid uuid create time string.
     */
    protected String floidUuidCreateTimeString = "";
    /**
     * The Floid uuid update time string.
     */
    protected String floidUuidUpdateTimeString = "";
    /**
     * Emitted a floid status.
     */
    protected boolean floidStatus = false;

    /**
     * Instantiates a new Floid uuid and state.
     *
     * @param floidId           the floid id
     * @param floidUuid         the floid uuid
     * @param floidIdCreateTime the floid id create time
     * @param floidIdUpdateTime the floid id update time
     */
    @SuppressWarnings("unused")
    public FloidUuidAndState(int floidId, String floidUuid, long floidIdCreateTime, long floidIdUpdateTime, boolean floidStatus) {
        super(floidId, floidUuid, floidIdCreateTime, floidIdUpdateTime);
        this.floidStatus=floidStatus;
        setState();
    }

    /**
     * Instantiates a new Floid uuid and state.
     *
     * @param id                the id
     * @param floidId           the floid id
     * @param floidUuid         the floid uuid
     * @param floidIdCreateTime the floid id create time
     * @param floidIdUpdateTime the floid id update time
     */
    public FloidUuidAndState(Long id, int floidId, String floidUuid, long floidIdCreateTime, long floidIdUpdateTime, boolean floidStatus) {
        super(id, floidId, floidUuid, floidIdCreateTime, floidIdUpdateTime);
        this.floidStatus=floidStatus;
        setState();
    }

    /**
     * Instantiates a new Floid uuid and state.
     *
     * @param floidUuid the floid uuid
     */
    public FloidUuidAndState(FloidUuid floidUuid) {
        this(floidUuid.getId(), floidUuid.floidId, floidUuid.floidUuid, floidUuid.floidUuidCreateTime, floidUuid.floidUuidUpdateTime, false);
        if (floidUuid.getId() != null) {
            this.setId(floidUuid.getId());
        } else {
            this.setId(FloidId.NO_FLOID_ID);
        }
    }
    /**
     * Instantiates a new Floid uuid and state.
     *
     * @param floidUuid the floid uuid
     */
    public FloidUuidAndState(FloidUuid floidUuid,  boolean floidStatus) {
        this(floidUuid.getId(), floidUuid.floidId, floidUuid.floidUuid, floidUuid.floidUuidCreateTime, floidUuid.floidUuidUpdateTime, floidStatus);
        if (floidUuid.getId() != null) {
            this.setId(floidUuid.getId());
        } else {
            this.setId(FloidId.NO_FLOID_ID);
        }
    }

    /**
     * Instantiates a new Floid uuid and state.
     */
    public FloidUuidAndState() {
        super();
        setState();
    }

    private void setState() {
        long floidUpTime = System.currentTimeMillis() - floidUuidUpdateTime;
        if (floidUpTime < FLOID_UPTIME_INDICATOR_MILLIS) {
            floidActive = true;
        }
        @SuppressWarnings("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, MMM d, yyyy h:mm a");
        try {
            Date floidUuidCreateDate = new Date();
            floidUuidCreateDate.setTime(floidUuidCreateTime);
            floidUuidCreateTimeString = simpleDateFormat.format(floidUuidCreateDate);
        } catch (Exception e) {
            // Do nothing
        }
        try {
            Date floidUuidUpdateDate = new Date();
            floidUuidUpdateDate.setTime(floidUuidUpdateTime);
            floidUuidUpdateTimeString = simpleDateFormat.format(floidUuidUpdateDate);
        } catch (Exception e) {
            // Do nothing
        }
    }

    @Override
    public void setFloidUuidUpdateTime(long floidUuidUpdateTime) {
        super.setFloidUuidUpdateTime(floidUuidUpdateTime);
        setState();
    }

    @Override
    public void setFloidUuidCreateTime(long floidUuidCreateTime) {
        super.setFloidUuidCreateTime(floidUuidCreateTime);
        setState();
    }

    /**
     * Gets floid active.
     *
     * @return the floid active
     */
    @SuppressWarnings("unused")
    public boolean getFloidActive() {
        return floidActive;
    }

    /**
     * Sets floid active.
     *
     * @param floidActive the floid active
     */
    public void setFloidActive(boolean floidActive) {
        this.floidActive = floidActive;
    }

    /**
     * Gets floid uuid create time string.
     *
     * @return the floid uuid create time string
     */
    @SuppressWarnings("unused")
    public String getFloidUuidCreateTimeString() {
        return floidUuidCreateTimeString;
    }

    /**
     * Sets floid uuid create time string.
     *
     * @param floidUuidCreateTimeString the floid uuid create time string
     */
    @SuppressWarnings("unused")
    public void setFloidUuidCreateTimeString(String floidUuidCreateTimeString) {
        this.floidUuidCreateTimeString = floidUuidCreateTimeString;
    }

    /**
     * Gets floid uuid update time string.
     *
     * @return the floid uuid update time string
     */
    @SuppressWarnings("unused")
    public String getFloidUuidUpdateTimeString() {
        return floidUuidUpdateTimeString;
    }

    /**
     * Sets floid uuid update time string.
     *
     * @param floidUuidUpdateTimeString the floid uuid update time string
     */
    @SuppressWarnings("unused")
    public void setFloidUuidUpdateTimeString(String floidUuidUpdateTimeString) {
        this.floidUuidUpdateTimeString = floidUuidUpdateTimeString;
    }

    public boolean isFloidStatus() {
        return floidStatus;
    }

    public void setFloidStatus(boolean floidStatus) {
        this.floidStatus = floidStatus;
    }

}
