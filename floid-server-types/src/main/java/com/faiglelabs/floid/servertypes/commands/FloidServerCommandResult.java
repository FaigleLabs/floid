/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;

import java.util.Date;

/**
 * Generic result object from a floid server command remoting call
 */
public class FloidServerCommandResult {
    private long    serverTime = new Date().getTime();
    private DroidInstruction droidInstruction = null;
    private String commandName = "";
    private int    floidId = -1;
    private String    floidUuid = "";
    private boolean success = false;
    private int     result = 0;
    private String  resultMessage = "";
    private boolean remotingSuccess = false;
    private boolean servletSuccess = false;
    private boolean serverSuccess = false;
    private boolean socketSuccess = false;
    private boolean threadSuccess = false;
    private boolean queueSuccess = false;
    private boolean online = false;
    private int     queueLength = 0;
    private long    lastSeenTime = 0L;


    /**
     * Instantiates a new Floid server command result.
     */
    public FloidServerCommandResult() {}

    /**
     * Instantiates a new Floid server command result.
     *
     * @param floidId          the floid id
     * @param droidInstruction the droid instruction
     */
    public FloidServerCommandResult(int floidId, DroidInstruction droidInstruction) {
        this.floidId = floidId;
        this.droidInstruction = droidInstruction;
        this.commandName = droidInstruction.getClass().getSimpleName();
    }

    /**
     * Is online boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isOnline() {
        return online;
    }

    /**
     * Sets online.
     *
     * @param online the online
     */
    public void setOnline(boolean online) {
        this.online = online;
    }

    /**
     * Gets command name.
     *
     * @return the command name
     */
    @SuppressWarnings("unused")
    public String getCommandName() {
        return commandName;
    }

    /**
     * Sets command name.
     *
     * @param commandName the command name
     */
    @SuppressWarnings("unused")
    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    /**
     * Gets droid instruction.
     *
     * @return the droid instruction
     */
    @SuppressWarnings("unused")
    public DroidInstruction getDroidInstruction() {
        return droidInstruction;
    }

    /**
     * Sets droid instruction.
     *
     * @param droidInstruction the droid instruction
     */
    public void setDroidInstruction(DroidInstruction droidInstruction) {
        this.droidInstruction = droidInstruction;
        this.commandName = droidInstruction.getClass().getSimpleName();
    }

    /**
     * Gets floid id.
     *
     * @return the floid id
     */
    @SuppressWarnings("unused")
    public int getFloidId() {
        return floidId;
    }

    /**
     * Sets floid id.
     *
     * @param floidId the floid id
     */
    @SuppressWarnings("unused")
    public void setFloidId(int floidId) {
        this.floidId = floidId;
    }

    /**
     * Gets floid uuid.
     *
     * @return the floid uuid
     */
    @SuppressWarnings("unused")
    public String getFloidUuid() {
        return floidUuid;
    }

    /**
     * Sets floid uuid.
     *
     * @param floidUuid the floid uuid
     */
    @SuppressWarnings("unused")
    public void setFloidUuid(String floidUuid) {
        this.floidUuid = floidUuid;
    }

    /**
     * Is success boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets success.
     *
     * @param success the success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Gets result.
     *
     * @return the result
     */
    @SuppressWarnings("unused")
    public int getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result the result
     */
    public void setResult(int result) {
        this.result = result;
    }

    /**
     * Gets result message.
     *
     * @return the result message
     */
    public String getResultMessage() {
        return resultMessage;
    }

    /**
     * Sets result message.
     *
     * @param resultMessage the result message
     */
    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    /**
     * Is remoting success boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isRemotingSuccess() {
        return remotingSuccess;
    }

    /**
     * Sets remoting success.
     *
     * @param remotingSuccess the remoting success
     */
    public void setRemotingSuccess(boolean remotingSuccess) {
        this.remotingSuccess = remotingSuccess;
    }

    /**
     * Is servlet success boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isServletSuccess() {
        return servletSuccess;
    }

    /**
     * Sets servlet success.
     *
     * @param servletSuccess the servlet success
     */
    public void setServletSuccess(boolean servletSuccess) {
        this.servletSuccess = servletSuccess;
    }

    /**
     * Is server success boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isServerSuccess() {
        return serverSuccess;
    }

    /**
     * Sets server success.
     *
     * @param serverSuccess the server success
     */
    public void setServerSuccess(boolean serverSuccess) {
        this.serverSuccess = serverSuccess;
    }

    /**
     * Is socket success boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isSocketSuccess() {
        return socketSuccess;
    }

    /**
     * Sets socket success.
     *
     * @param socketSuccess the socket success
     */
    @SuppressWarnings("unused")
    public void setSocketSuccess(boolean socketSuccess) {
        this.socketSuccess = socketSuccess;
    }

    /**
     * Is thread success boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isThreadSuccess() {
        return threadSuccess;
    }

    /**
     * Sets thread success.
     *
     * @param threadSuccess the thread success
     */
    public void setThreadSuccess(boolean threadSuccess) {
        this.threadSuccess = threadSuccess;
    }

    /**
     * Is queue success boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isQueueSuccess() {
        return queueSuccess;
    }

    /**
     * Sets queue success.
     *
     * @param queueSuccess the queue success
     */
    public void setQueueSuccess(boolean queueSuccess) {
        this.queueSuccess = queueSuccess;
    }

    /**
     * Gets queue length.
     *
     * @return the queue length
     */
    @SuppressWarnings("unused")
    public int getQueueLength() {
        return queueLength;
    }

    /**
     * Sets queue length.
     *
     * @param queueLength the queue length
     */
    public void setQueueLength(int queueLength) {
        this.queueLength = queueLength;
    }

    /**
     * Gets server time.
     *
     * @return the server time
     */
    @SuppressWarnings("unused")
    public long getServerTime() {
        return serverTime;
    }

    /**
     * Sets server time.
     *
     * @param serverTime the server time
     */
    @SuppressWarnings("unused")
    public void setServerTime(long serverTime) {
        this.serverTime = serverTime;
    }

    /**
     * Gets last seen time.
     *
     * @return the last seen time
     */
    @SuppressWarnings("unused")
    public long getLastSeenTime() {
        return lastSeenTime;
    }

    /**
     * Sets last seen time.
     *
     * @param lastSeenTime the last seen time
     */
    @SuppressWarnings("unused")
    public void setLastSeenTime(long lastSeenTime) {
        this.lastSeenTime = lastSeenTime;
    }
}
