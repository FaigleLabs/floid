/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;
// TODO [FLY PATTERN] THIS IS COMPLETELY UNIMPLEMENTED IN TERMS OF PARAMETERS, ETC. ETC.

/**
 * Fly pattern command.
 */
@Entity
public class FlyPatternCommand extends DroidCommand {
    /**
     * Instantiates a new Fly pattern command.
     */
    public FlyPatternCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        FlyPatternCommand flyPatternCommand = new FlyPatternCommand();
        super.factory(flyPatternCommand, droidMission);
        return flyPatternCommand;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable") JSONObject jsonObject = super.toJSON();
        // No parameters to add...
        return jsonObject;
    }

    @Override
    public FlyPatternCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable") Element typeElement = super.addToXML(doc);
        // Adds nothing...
        return typeElement;
    }
}
