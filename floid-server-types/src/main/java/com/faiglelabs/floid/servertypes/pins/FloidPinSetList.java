/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.pins;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Floid pin set list - normal forms
 */
public class FloidPinSetList implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    private Long pinSetId;
    private String pinSetName;

    /**
     * Instantiates a new Floid pin set list.
     */
    public FloidPinSetList() {
        super();
    }

    /**
     * Instantiates a new Floid pin set list.
     *
     * @param pinSetId   the pin set id
     * @param pinSetName the pin set name
     */
    @SuppressWarnings("unused")
    public FloidPinSetList(Long pinSetId, String pinSetName) {
        this.pinSetId = pinSetId;
        this.pinSetName = pinSetName;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @SuppressWarnings("unused")
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    @SuppressWarnings("unused")
    public void setId(Long id) {
        this.id = id;
    }


    /**
     * Gets pin set id.
     *
     * @return the pin set id
     */
    @SuppressWarnings("unused")
    public Long getPinSetId() {
        return pinSetId;
    }

    /**
     * Sets pin set id.
     *
     * @param pinSetId the pin set id
     */
    @SuppressWarnings("unused")
    public void setPinSetId(Long pinSetId) {
        this.pinSetId = pinSetId;
    }

    /**
     * Gets pin set name.
     *
     * @return the pin set name
     */
    @SuppressWarnings("unused")
    public String getPinSetName() {
        return pinSetName;
    }

    /**
     * Sets pin set name.
     *
     * @param pinSetName the pin set name
     */
    @SuppressWarnings("unused")
    public void setPinSetName(String pinSetName) {
        this.pinSetName = pinSetName;
    }

}
