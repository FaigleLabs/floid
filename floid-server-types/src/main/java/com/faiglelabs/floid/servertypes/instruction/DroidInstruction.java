/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.instruction;

import com.faiglelabs.floid.database.FloidDatabaseTables;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.Serializable;

/**
 * The type Droid instruction.
 */
@SuppressWarnings("WeakerAccess")
@Entity
@Table(name = FloidDatabaseTables.DROID_INSTRUCTION,
        indexes = {
                @Index(name = "idIndex", columnList = "id"),
                @Index(name = "typeIndex", columnList = "type"),
                @Index(name = "topLevelMissionIndex", columnList = "topLevelMission")
        })
@DiscriminatorColumn(length=100)
public class DroidInstruction implements Serializable {
    /**
     * No instruction indicator
     */
    public static final long NO_INSTRUCTION = -1L;
    @Id
    @GeneratedValue
    private Long id;
    private String type;
    @ManyToOne
    @JsonIgnore
    private DroidMission droidMission = null;
    private boolean topLevelMission = false;
    private long missionInstructionNumber = NO_INSTRUCTION;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets droid mission.
     *
     * @return the droid mission
     */
    @SuppressWarnings("unused")
    @JsonIgnore
    @JsonBackReference
    public DroidMission getDroidMission() {
        return droidMission;
    }

    /**
     * Sets droid mission.
     *
     * @param droidMission the droid mission
     */
    public void setDroidMission(DroidMission droidMission) {
        this.droidMission = droidMission;
    }

    /**
     * Factory a droid instruction from a droid mission
     * @param droidMission the droid mission
     * @return the droid instruction
     */
    public DroidInstruction factory(DroidMission droidMission) {
        DroidInstruction droidInstruction = new DroidInstruction();
        droidInstruction.setDroidMission(droidMission);
        droidInstruction.setType(this.getType());
        droidInstruction.setTopLevelMission(this.topLevelMission);
        return droidInstruction;
    }

    /**
     * Factory into a droid instruction from a droid mission
     * @param droidInstruction the droid instruction
     * @param droidMission the droid mission
     * @return the modified droid instruction
     */
    public DroidInstruction factory(DroidInstruction droidInstruction, DroidMission droidMission) {
        droidInstruction.setType(this.getType());
        droidInstruction.setDroidMission(droidMission);
        droidInstruction.setTopLevelMission(topLevelMission);
        return droidInstruction;
    }
    /**
     * Gets mission instruction number.
     *
     * @return the mission instruction number
     */
    @SuppressWarnings("unused")
    public long getMissionInstructionNumber() {
        return missionInstructionNumber;
    }

    /**
     * Sets mission instruction number.
     *
     * @param missionInstructionNumber the mission instruction number
     */
    public void setMissionInstructionNumber(long missionInstructionNumber) {
        this.missionInstructionNumber = missionInstructionNumber;
    }

    /**
     * Instantiates a new Droid instruction.
     */
    public DroidInstruction() {
        type = getClass().getName();
    }

    /**
     * Instantiates a new Droid instruction from an existing droid instruction
     * @param droidInstruction the droid instruction
     */
    @SuppressWarnings("CopyConstructorMissesField")
    public DroidInstruction(DroidInstruction droidInstruction) {
        type = droidInstruction.getType();
        topLevelMission = droidInstruction.isTopLevelMission();
    }
    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }
    /**
     * Gets a simplified version of the type.
     *
     * @return the simplified version of the type
     */
    public String getSimplifiedType() {
        return type.replace("com.faiglelabs.floid.servertypes.commands.", "");
    }

    /**
     * Gets short type.
     *
     * @return the short version of the type
     */
    public String getShortType() {
        if(type!=null) {
            return  type.substring(type. lastIndexOf('.') + 1);
        }
        return "";
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    @SuppressWarnings("unused")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Convert to json object.
     *
     * @return the json object
     * @throws JSONException the json exception
     */
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        if (getId() != null) {
            jsonObject.put("id", getId());
        } else {
            jsonObject.put("id", -1);
        }
        jsonObject.put("type", getType());
        jsonObject.put("topLevelMission", isTopLevelMission());
        return jsonObject;
    }

    /**
     * Load this from json
     *
     * @param jsonObject the json object
     * @return the droid instruction
     * @throws JSONException the json exception
     */
    @SuppressWarnings("UnusedReturnValue")
    public DroidInstruction fromJSON(JSONObject jsonObject) throws JSONException {
        id = jsonObject.getLong("id");
        if (id == -1) {
            id = null;  //clear out the id if we made it -1 earlier...
        }
        type = jsonObject.getString("type");
        topLevelMission = jsonObject.getBoolean("topLevelMission");
        return this;
    }

    /**
     * Add this to xml document and return element created.
     *
     * @param doc the doc
     * @return the element
     */
    public Element addToXML(Document doc) {
        Element typeElement = doc.createElement(type);
        doc.appendChild(typeElement);
        typeElement.setAttribute("id", "" + getId());
        typeElement.setAttribute("type", "" + getType());
        typeElement.setAttribute("topLevelMission", "" + isTopLevelMission());
        return typeElement;
    }

    /**
     * Is this a top level mission
     *
     * @return true if top level mission
     */
    public boolean isTopLevelMission() {
        return topLevelMission;
    }

    /**
     * Sets top level mission.
     *
     * @param topLevelMission true if top level mission
     */
    public void setTopLevelMission(boolean topLevelMission) {
        this.topLevelMission = topLevelMission;
    }



    /**
     * Convert to xml document.
     *
     * @return the document
     */
    @SuppressWarnings("unused")
    public Document toXML() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            addToXML(doc);
            return doc;
        } catch (Exception e) {
            return null;
        }
    }
}
