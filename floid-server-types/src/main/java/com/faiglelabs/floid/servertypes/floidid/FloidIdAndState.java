/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.servertypes.floidid;

import javax.persistence.Entity;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The Floid id and its state.
 */
@SuppressWarnings("WeakerAccess")
@Entity
public class FloidIdAndState extends FloidId implements Serializable {
    private static final long FLOID_UPTIME_INDICATOR_MILLIS = 3000L;
    /**
     * Is the Floid active.
     */
    protected boolean floidActive = false;
    /**
     * The Floid id create time string.
     */
    protected String floidIdCreateTimeString = "";
    /**
     * The Floid id update time string.
     */
    protected String floidIdUpdateTimeString = "";
    /**
     * Emitted a floid status.
     */
    protected boolean floidStatus = false;

    /**
     * Instantiates a new Floid id and state.
     *
     * @param floidId           the floid id
     * @param floidIdCreateTime the floid id create time
     * @param floidIdUpdateTime the floid id update time
     */
    @SuppressWarnings("unused")
    public FloidIdAndState(int floidId, long floidIdCreateTime, long floidIdUpdateTime, boolean floidStatus) {
        super(floidId, floidIdCreateTime, floidIdUpdateTime);
        this.floidStatus=floidStatus;
        setState();
    }

    /**
     * Instantiates a new Floid id and state.
     *
     * @param id                the id
     * @param floidId           the floid id
     * @param floidIdCreateTime the floid id create time
     * @param floidIdUpdateTime the floid id update time
     */
    public FloidIdAndState(Long id, int floidId, long floidIdCreateTime, long floidIdUpdateTime, boolean floidStatus) {
        super(id, floidId, floidIdCreateTime, floidIdUpdateTime);
        this.floidStatus=floidStatus;
        setState();
    }

    /**
     * Instantiates a new Floid id and state.
     *
     * @param floidId the floid id
     */
    public FloidIdAndState(FloidId floidId) {
        this(floidId.getId(), floidId.floidId, floidId.floidIdCreateTime, floidId.floidIdUpdateTime, false);
        if (floidId.getId() != null) {
            this.setId(floidId.getId());
        } else {
            this.setId(FloidId.NO_FLOID_ID);
        }
    }
    /**
     * Instantiates a new Floid id and state.
     *
     * @param floidId the floid id
     * @param floidStatus whether this id has ever received a status from the floid
     */
    public FloidIdAndState(FloidId floidId, boolean floidStatus) {
        this(floidId.getId(), floidId.floidId, floidId.floidIdCreateTime, floidId.floidIdUpdateTime, floidStatus);
        if (floidId.getId() != null) {
            this.setId(floidId.getId());
        } else {
            this.setId(FloidId.NO_FLOID_ID);
        }
    }

    /**
     * Instantiates a new Floid id and state.
     */
    public FloidIdAndState() {
        super();
    }

    private void setState() {
        long floidUpTime = System.currentTimeMillis() - floidIdUpdateTime;
        if (floidUpTime < FLOID_UPTIME_INDICATOR_MILLIS) {
            floidActive = true;
        }
        @SuppressWarnings("SimpleDateFormat")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, MMM d, yyyy h:mm a");
        try {
            Date floidIdCreateDate = new Date();
            floidIdCreateDate.setTime(floidIdCreateTime);
            floidIdCreateTimeString = simpleDateFormat.format(floidIdCreateDate);
        } catch (Exception e) {
            // Do nothing
        }
        try {
            Date floidIdUpdateDate = new Date();
            floidIdUpdateDate.setTime(floidIdUpdateTime);
            floidIdUpdateTimeString = simpleDateFormat.format(floidIdUpdateDate);
        } catch (Exception e) {
            // Do nothing
        }
    }

    @Override
    public void setFloidIdUpdateTime(long floidIdUpdateTime) {
        super.setFloidIdUpdateTime(floidIdUpdateTime);
        setState();
    }

    @Override
    public void setFloidIdCreateTime(long floidIdCreateTime) {
        super.setFloidIdCreateTime(floidIdUpdateTime);
        setState();
    }

    /**
     * Gets floid active.
     *
     * @return the floid active
     */
    @SuppressWarnings("unused")
    public boolean getFloidActive() {
        return floidActive;
    }

    /**
     * Sets floid active.
     *
     * @param floidActive the floid active
     */
    public void setFloidActive(boolean floidActive) {
        this.floidActive = floidActive;
    }

    /**
     * Gets floid id create time string.
     *
     * @return the floid id create time string
     */
    @SuppressWarnings("unused")
    public String getFloidIdCreateTimeString() {
        return floidIdCreateTimeString;
    }

    /**
     * Sets floid id create time string.
     *
     * @param floidIdCreateTimeString the floid id create time string
     */
    @SuppressWarnings("unused")
    public void setFloidIdCreateTimeString(String floidIdCreateTimeString) {
        this.floidIdCreateTimeString = floidIdCreateTimeString;
    }

    /**
     * Gets floid id update time string.
     *
     * @return the floid id update time string
     */
    @SuppressWarnings("unused")
    public String getFloidIdUpdateTimeString() {
        return floidIdUpdateTimeString;
    }

    /**
     * Sets floid id update time string.
     *
     * @param floidIdUpdateTimeString the floid id update time string
     */
    @SuppressWarnings("unused")
    public void setFloidIdUpdateTimeString(String floidIdUpdateTimeString) {
        this.floidIdUpdateTimeString = floidIdUpdateTimeString;
    }

    public boolean isFloidStatus() {
        return floidStatus;
    }

    public void setFloidStatus(boolean floidStatus) {
        this.floidStatus = floidStatus;
    }
}
