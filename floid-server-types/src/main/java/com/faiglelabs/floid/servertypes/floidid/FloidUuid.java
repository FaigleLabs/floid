/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.floidid;

import com.faiglelabs.floid.database.FloidDatabaseTables;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type Floid uuid.
 */
@Entity
@SuppressWarnings("unused")
@Table(name = FloidDatabaseTables.FLOID_UUID,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidUuidIndex", columnList = "floidUuid"),
        @Index(name="floidUuidCreateTimeIndex", columnList = "floidUuidCreateTime"),
        @Index(name="floidUuidUpdateTimeIndex", columnList = "floidUuidUpdateTime")
})
public class FloidUuid implements Serializable {
    /**
     * The Floid id.
     */
    protected int floidId;
    /**
     * The Floid uuid.
     */
    protected String floidUuid;
    /**
     * The Floid uuid create time.
     */
    protected long floidUuidCreateTime;
    /**
     * The Floid uuid update time.
     */
    protected long floidUuidUpdateTime;
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Instantiates a new Floid uuid.
     *
     * @param floidId             the floid id
     * @param floidUuid           the floid uuid
     * @param floidUuidCreateTime the floid uuid create time
     * @param floidUuidUpdateTime the floid uuid update time
     */
    public FloidUuid(int floidId, String floidUuid, long floidUuidCreateTime, long floidUuidUpdateTime) {
        this.floidId = floidId;
        this.floidUuid = floidUuid;
        this.floidUuidCreateTime = floidUuidCreateTime;
        this.floidUuidUpdateTime = floidUuidUpdateTime;
    }

    /**
     * Instantiates a new Floid uuid.
     *
     * @param id                  the id
     * @param floidId             the floid id
     * @param floidUuid           the floid uuid
     * @param floidUuidCreateTime the floid uuid create time
     * @param floidUuidUpdateTime the floid uuid update time
     */
    public FloidUuid(Long id, int floidId, String floidUuid, long floidUuidCreateTime, long floidUuidUpdateTime) {
        this.id = id;
        this.floidId = floidId;
        this.floidUuid = floidUuid;
        this.floidUuidCreateTime = floidUuidCreateTime;
        this.floidUuidUpdateTime = floidUuidUpdateTime;
    }

    /**
     * Instantiates a new Floid uuid.
     */
    public FloidUuid() {
        super();
    }

    /**
     * Gets floid uuid.
     *
     * @return the floid uuid
     */
    public String getFloidUuid() {
        return floidUuid;
    }

    /**
     * Sets floid uuid.
     *
     * @param floidUuid the floid uuid
     */
    public void setFloidUuid(String floidUuid) {
        this.floidUuid = floidUuid;
    }

    /**
     * Gets floid uuid create time.
     *
     * @return the floid uuid create time
     */
    public long getFloidUuidCreateTime() {
        return floidUuidCreateTime;
    }

    /**
     * Sets floid uuid create time.
     *
     * @param floidUuidCreateTime the floid uuid create time
     */
    public void setFloidUuidCreateTime(long floidUuidCreateTime) {
        this.floidUuidCreateTime = floidUuidCreateTime;
    }

    /**
     * Gets floid uuid update time.
     *
     * @return the floid uuid update time
     */
    public long getFloidUuidUpdateTime() {
        return floidUuidUpdateTime;
    }

    /**
     * Sets floid uuid update time.
     *
     * @param floidUuidUpdateTime the floid uuid update time
     */
    public void setFloidUuidUpdateTime(long floidUuidUpdateTime) {
        this.floidUuidUpdateTime = floidUuidUpdateTime;
    }

    /**
     * Gets floid id.
     *
     * @return the floid id
     */
    public int getFloidId() {
        return floidId;
    }

    /**
     * Sets floid id.
     *
     * @param floidId the floid id
     */
    public void setFloidId(int floidId) {
        this.floidId = floidId;
    }
}
