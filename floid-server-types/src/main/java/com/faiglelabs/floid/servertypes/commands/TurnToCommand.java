/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Turn to command.  Specify either heading or to follow flight direction.
 */
@Entity
public class TurnToCommand extends DroidCommand {
    private double heading = 0;
    private boolean followMode = false;

    /**
     * Instantiates a new Turn to command.
     */
    public TurnToCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        TurnToCommand turnToCommand = new TurnToCommand();
        super.factory(turnToCommand, droidMission);
        turnToCommand.heading = heading;
        turnToCommand.followMode = followMode;
        return turnToCommand;
    }

    /**
     * Is follow mode boolean.
     *
     * @return the boolean
     */
    public boolean isFollowMode() {
        return followMode;
    }

    /**
     * Gets follow mode.
     *
     * @return the follow mode
     */
    @SuppressWarnings("unused")
    public boolean getFollowMode() {
        return followMode;
    }

    /**
     * Sets follow mode.
     *
     * @param followMode the follow mode
     */
    public void setFollowMode(boolean followMode) {
        this.followMode = followMode;
    }

    /**
     * Gets heading.
     *
     * @return the heading
     */
    public double getHeading() {
        return heading;
    }

    /**
     * Sets heading.
     *
     * @param heading the heading
     */
    public void setHeading(double heading) {
        this.heading = heading;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("heading", heading);
        jsonObject.put("followMode", Boolean.valueOf(followMode).toString());
        return jsonObject;
    }

    @Override
    public TurnToCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);

        heading = jsonObject.getDouble("heading");
        followMode = jsonObject.getBoolean("followMode");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("heading", "" + heading);
        typeElement.setAttribute("followMode", Boolean.valueOf(followMode).toString());
        return typeElement;
    }
}
