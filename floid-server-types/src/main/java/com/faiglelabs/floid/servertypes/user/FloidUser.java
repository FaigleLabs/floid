/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.servertypes.user;

import com.faiglelabs.floid.database.FloidDatabaseTables;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The Floid user information
 */
@SuppressWarnings("WeakerAccess")
@Entity
@Table(name = FloidDatabaseTables.FLOID_USER,
        indexes = {
        @Index(name="userNameIndex", columnList = "userName")
})
public class FloidUser implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Instantiates a new Floid user.
     *
     * @param id the id
     * @param userName the username
     * @param password the password
     * @param roles the roles
     */
    public FloidUser(Long id, String userName, String password, String roles) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.roles = roles;
    }

    /**
     * Instantiates a new Floid user.
     */
    public FloidUser() {
        super();
    }


    /**
     * The user name.
     */
    protected String userName = "";
    /**
     * The (encrypted) password.
     */
    protected String password = "";
    /**
     * The list of roles, comma separated
     */
    @Column(length = 4096)
    protected String roles = "";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}
