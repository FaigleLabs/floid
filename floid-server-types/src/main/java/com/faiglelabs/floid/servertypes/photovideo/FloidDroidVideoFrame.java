/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.photovideo;

import com.faiglelabs.floid.database.FloidDatabaseTables;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Floid droid video preview frame.
 */
@edu.umd.cs.findbugs.annotations.SuppressFBWarnings("EI_EXPOSE_REP")
@Entity
@Table(name = FloidDatabaseTables.FLOID_DROID_VIDEO_FRAME,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidUuidIndex", columnList = "floidUuid"),
        @Index(name="timestampIndex", columnList = "timestamp"),
        @Index(name="typeIndex", columnList = "type"),
        @Index(name="floidMessageNumberIndex", columnList = "floidMessageNumber")
})
public class FloidDroidVideoFrame extends FloidImage {
    /**
     * Instantiates a new Floid droid video preview frame.
     */
    public FloidDroidVideoFrame() {
        type = getClass().getName();
        floidMessageNumber = getNextFloidMessageNumber();
    }

    public FloidDroidVideoFrame(
            int floidId,
            String floidUuid,
            String imageName,
            String imageType,
            String imageExtension,
            double latitude,
            double longitude,
            double altitude,
            double pan,
            double tilt)
    {
        this.type = getClass().getName();
        this.floidId = floidId;
        this.floidUuid = floidUuid;
        this.imageName = imageName;
        this.imageType = imageType;
        this.imageExtension = imageExtension;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.pan = pan;
        this.tilt = tilt;
    }

}
