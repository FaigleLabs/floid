/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
/*
 * FloidServerThreadDumper.java
 */
package com.faiglelabs.floid.servertypes.statuses;

import java.io.File;

/**
 * The type Floid imaging status.
 */
public class FloidImagingStatus {

    /**
     * Camera state: None.
     */
    public static final int STATE_NONE = 0;
    /**
     * Camera state: Showing camera preview.
     */
    public static final int STATE_PREVIEW = 1;
    /**
     * Camera state: Waiting for the focus to be locked.
     */
    public static final int STATE_WAITING_FOCUS_LOCK = 2;
    /**
     * Camera state: Waiting for the focus to be locked but caught in the preview capture due to legacy hardware.
     */
    public static final int STATE_WAITING_FOR_FOCUS_LOCK_IN_PREVIEW = 3;
    /**
     * Camera state: Waiting for the exposure to be precapture state.
     */
    public static final int STATE_WAITING_PRECAPTURE = 4;
    /**
     * Camera state: Waiting for the exposure state to be something other than precapture.
     */
    public static final int STATE_WAITING_NON_PRECAPTURE = 5;
    /**
     * Camera state: Picture was requested.
     */
    public static final int STATE_PICTURE_REQUESTED = 6;
    /**
     * Camera state: Picture was taken.
     */
    public static final int STATE_PICTURE_TAKEN = 7;
    /**
     * The constant NO_CAMERA_ID_STRING.
     */
    public final static String NO_CAMERA_ID_STRING = "";
    private static final String[] STATE_NAMES = {
            "NONE",
            "PREVIEW",
            "WAITING_FOCUS_LOCK",
            "WAITING_FOR_FOCUS_LOCK_IN_PREVIEW",
            "WAITING_PRECAPTURE",
            "WAITING_NON_PRECAPTURE",
            "PICTURE_REQUESTED",
            "PICTURE_TAKEN"
    };
    /**
     * Are we initialized
     */
    protected boolean mStatusInitialized = false;
    /**
     * Are we taking a photo (either photo actual or video snapshot actual depending on whether currently taking video
     */
    protected boolean mStatusTakingPhoto = false;
    /**
     * Has the photo camera id changed since the last time it was opened?
     */
    protected boolean mStatusPhotoCameraIdDirty = true;
    /**
     * Have any of the photo camera properties changed since the last time it was opened?
     */
    protected boolean mStatusPhotoCameraPropertiesDirty = true;
    /**
     * Is the photo camera open
     */
    protected boolean mStatusPhotoCameraOpen = false;
    /**
     * Are the photo surfaces set up
     */
    protected boolean mStatusPhotoSurfacesSetup = false;
    /**
     * Is the photo capture request set up
     */
    protected boolean mStatusPhotoCaptureRequestSetup = false;
    /**
     * Is the camera capture session configured
     */
    protected boolean mStatusPhotoCameraCaptureSessionConfigured = false;
    // Video Streaming and snapshot:
    /**
     * Are we taking video
     */
    protected boolean mStatusTakingVideo = false;
    /**
     * Are we taking a video snapshot
     */
    protected boolean mStatusTakingVideoSnapshot = false;
    /**
     * Is the video camera id dirty
     */
    protected boolean mStatusVideoCameraIdDirty = true;
    /**
     * Are the video camera properties dirty
     */
    protected boolean mStatusVideoCameraPropertiesDirty = true;
    /**
     * Is the video camera open
     */
    protected boolean mStatusVideoCameraOpen = false;
    /**
     * Are the video surfaces set up
     */
    protected boolean mStatusVideoSurfacesSetup = false;
    /**
     * Does the video support snapshot (NOTE: Disabled - caused crash on Moto X)
     */
    protected boolean mStatusSupportsVideoSnapshot = false;
    /**
     * Is the video capture request set up
     */
    protected boolean mStatusVideoCaptureRequestSetup = false;
    /**
     * Is the video camera capture session configured
     */
    protected boolean mStatusVideoCameraCaptureSessionConfigured = false;
    /**
     * Number of remaining capture sessions to configure
     */
    protected int mNumberOfRemainingCaptureSessionsToCreate = 0;
    /**
     * The photo video storage path.
     */
    protected File mPhotoVideoStoragePath = null;
    /**
     * The photo camera id.
     */
    protected String mPhotoCameraId;
    /**
     * The video camera id.
     */
    protected String mVideoCameraId;
    /**
     * The current state of camera state for taking pictures.
     */
    protected int mPhotoState = STATE_NONE;
    /**
     * Whether the current photo camera device supports a flash.
     */
    protected boolean mFlashSupported;
    /**
     * Whether the current photo camera device has only legacy support.
     */
    protected boolean mHardwareIsLegacy;

    /**
     * Instantiates a new Floid imaging status.
     */
    @SuppressWarnings("WeakerAccess")
    public FloidImagingStatus() {
        this.mPhotoCameraId = NO_CAMERA_ID_STRING;
        this.mVideoCameraId = NO_CAMERA_ID_STRING;
    }

    /**
     * Get the state name for a state
     * @param state the state
     * @return the state name
     */
    public static String getStateName(int state) {
        if(state >=0 && state < STATE_NAMES.length) {
            return STATE_NAMES[state];
        } else {
            return "UNKNOWN";
        }
    }

    /**
     * Is status initialized
     *
     * @return status initialized
     */
    public boolean isStatusInitialized() {
        return mStatusInitialized;
    }

    /**
     * Set status initialized
     *
     * @param mStatusInitialized status initialized
     */
    public void setStatusInitialized(boolean mStatusInitialized) {
        this.mStatusInitialized = mStatusInitialized;
    }

    /**
     * Is status taking photo
     *
     * @return status taking photo
     */
    public boolean isStatusTakingPhoto() {
        return mStatusTakingPhoto;
    }

    /**
     * Set status taking photo
     *
     * @param statusTakingPhoto status taking photo
     */
    public void setStatusTakingPhoto(boolean statusTakingPhoto) {
        this.mStatusTakingPhoto = statusTakingPhoto;
    }

    /**
     * Is status photo camera id dirty
     *
     * @return status photo camera id dirty
     */
    public boolean isStatusPhotoCameraIdDirty() {
        return mStatusPhotoCameraIdDirty;
    }

    /**
     * Set status photo camera id dirty
     *
     * @param statusPhotoCameraIdDirty status photo camera id dirty
     */
    public void setStatusPhotoCameraIdDirty(boolean statusPhotoCameraIdDirty) {
        this.mStatusPhotoCameraIdDirty = statusPhotoCameraIdDirty;
    }

    /**
     * Is status photo camera properties dirty
     *
     * @return status photo camera properties dirty
     */
    public boolean isStatusPhotoCameraPropertiesDirty() {
        return mStatusPhotoCameraPropertiesDirty;
    }

    /**
     * Set status photo camera properties dirty
     *
     * @param statusPhotoCameraPropertiesDirty status photo camera properties dirty
     */
    public void setStatusPhotoCameraPropertiesDirty(boolean statusPhotoCameraPropertiesDirty) {
        this.mStatusPhotoCameraPropertiesDirty = statusPhotoCameraPropertiesDirty;
    }

    /**
     * Is status photo camera open
     *
     * @return status photo camera open
     */
    public boolean isStatusPhotoCameraOpen() {
        return mStatusPhotoCameraOpen;
    }

    /**
     * Set status photo camera open
     *
     * @param statusPhotoCameraOpen status photo camera open
     */
    public void setStatusPhotoCameraOpen(boolean statusPhotoCameraOpen) {
        this.mStatusPhotoCameraOpen = statusPhotoCameraOpen;
    }

    /**
     * Is status photo surfaces set up
     *
     * @return status photo surfaces set up
     */
    public boolean isStatusPhotoSurfacesSetup() {
        return mStatusPhotoSurfacesSetup;
    }

    /**
     * Set status photo surfaces set up
     *
     * @param statusPhotoSurfacesSetup status photo surfaces set up
     */
    public void setStatusPhotoSurfacesSetup(boolean statusPhotoSurfacesSetup) {
        this.mStatusPhotoSurfacesSetup = statusPhotoSurfacesSetup;
    }

    /**
     * Is status photo capture request setup boolean.
     *
     * @return the boolean
     */
    public boolean isStatusPhotoCaptureRequestSetup() {
        return mStatusPhotoCaptureRequestSetup;
    }

    /**
     * Sets status photo capture request setup.
     *
     * @param statusPhotoCaptureRequestSetup the status photo capture request setup
     */
    public void setStatusPhotoCaptureRequestSetup(boolean statusPhotoCaptureRequestSetup) {
        this.mStatusPhotoCaptureRequestSetup = statusPhotoCaptureRequestSetup;
    }

    /**
     * Is status photo camera capture session configured boolean.
     *
     * @return the boolean
     */
    public boolean isStatusPhotoCameraCaptureSessionConfigured() {
        return mStatusPhotoCameraCaptureSessionConfigured;
    }

    /**
     * Sets status photo camera capture session configured.
     *
     * @param statusPhotoCameraCaptureSessionConfigured the status photo camera capture session configured
     */
    public void setStatusPhotoCameraCaptureSessionConfigured(boolean statusPhotoCameraCaptureSessionConfigured) {
        this.mStatusPhotoCameraCaptureSessionConfigured = statusPhotoCameraCaptureSessionConfigured;
    }

    /**
     * Is status taking video boolean.
     *
     * @return the boolean
     */
    public boolean isStatusTakingVideo() {
        return mStatusTakingVideo;
    }

    /**
     * Sets status taking video.
     *
     * @param statusTakingVideo the status taking video
     */
    public void setStatusTakingVideo(boolean statusTakingVideo) {
        this.mStatusTakingVideo = statusTakingVideo;
    }

    /**
     * Is status taking video snapshot boolean.
     *
     * @return the boolean
     */
    public boolean isStatusTakingVideoSnapshot() {
        return mStatusTakingVideoSnapshot;
    }

    /**
     * Sets status taking video snapshot.
     *
     * @param statusTakingVideoSnapshot the status taking video snapshot
     */
    public void setStatusTakingVideoSnapshot(boolean statusTakingVideoSnapshot) {
        this.mStatusTakingVideoSnapshot = statusTakingVideoSnapshot;
    }

    /**
     * Is status video camera id dirty boolean.
     *
     * @return the boolean
     */
    public boolean isStatusVideoCameraIdDirty() {
        return mStatusVideoCameraIdDirty;
    }

    /**
     * Sets status video camera id dirty.
     *
     * @param statusVideoCameraIdDirty the status video camera id dirty
     */
    public void setStatusVideoCameraIdDirty(boolean statusVideoCameraIdDirty) {
        this.mStatusVideoCameraIdDirty = statusVideoCameraIdDirty;
    }

    /**
     * Is status video camera properties dirty boolean.
     *
     * @return the boolean
     */
    public boolean isStatusVideoCameraPropertiesDirty() {
        return mStatusVideoCameraPropertiesDirty;
    }

    /**
     * Sets status video camera properties dirty.
     *
     * @param statusVideoCameraPropertiesDirty the status video camera properties dirty
     */
    public void setStatusVideoCameraPropertiesDirty(boolean statusVideoCameraPropertiesDirty) {
        this.mStatusVideoCameraPropertiesDirty = statusVideoCameraPropertiesDirty;
    }

    /**
     * Is status video camera open boolean.
     *
     * @return the boolean
     */
    public boolean isStatusVideoCameraOpen() {
        return mStatusVideoCameraOpen;
    }

    /**
     * Sets status video camera open.
     *
     * @param statusVideoCameraOpen the status video camera open
     */
    public void setStatusVideoCameraOpen(boolean statusVideoCameraOpen) {
        this.mStatusVideoCameraOpen = statusVideoCameraOpen;
    }

    /**
     * Is status video surfaces setup boolean.
     *
     * @return the boolean
     */
    public boolean isStatusVideoSurfacesSetup() {
        return mStatusVideoSurfacesSetup;
    }

    /**
     * Sets status video surfaces setup.
     *
     * @param statusVideoSurfacesSetup the status video surfaces setup
     */
    public void setStatusVideoSurfacesSetup(boolean statusVideoSurfacesSetup) {
        this.mStatusVideoSurfacesSetup = statusVideoSurfacesSetup;
    }

    /**
     * Is status supports video snapshot boolean.
     *
     * @return the boolean
     */
    public boolean isStatusSupportsVideoSnapshot() {
        return mStatusSupportsVideoSnapshot;
    }

    /**
     * Sets status supports video snapshot.
     *
     * @param statusSupportsVideoSnapshot the status supports video snapshot
     */
    public void setStatusSupportsVideoSnapshot(boolean statusSupportsVideoSnapshot) {
        this.mStatusSupportsVideoSnapshot = statusSupportsVideoSnapshot;
    }

    /**
     * Is status video capture request setup boolean.
     *
     * @return the boolean
     */
    public boolean isStatusVideoCaptureRequestSetup() {
        return mStatusVideoCaptureRequestSetup;
    }

    /**
     * Sets status video capture request setup.
     *
     * @param statusVideoCaptureRequestSetup the status video capture request setup
     */
    public void setStatusVideoCaptureRequestSetup(boolean statusVideoCaptureRequestSetup) {
        this.mStatusVideoCaptureRequestSetup = statusVideoCaptureRequestSetup;
    }

    /**
     * Is status video camera capture session configured boolean.
     *
     * @return the boolean
     */
    public boolean isStatusVideoCameraCaptureSessionConfigured() {
        return mStatusVideoCameraCaptureSessionConfigured;
    }

    /**
     * Sets status video camera capture session configured.
     *
     * @param statusVideoCameraCaptureSessionConfigured the status video camera capture session configured
     */
    public void setStatusVideoCameraCaptureSessionConfigured(boolean statusVideoCameraCaptureSessionConfigured) {
        this.mStatusVideoCameraCaptureSessionConfigured = statusVideoCameraCaptureSessionConfigured;
    }

    /**
     * Gets number of remaining capture sessions to create.
     *
     * @return the number of remaining capture sessions to create
     */
    public int getNumberOfRemainingCaptureSessionsToCreate() {
        return mNumberOfRemainingCaptureSessionsToCreate;
    }

    /**
     * Sets number of remaining capture sessions to create.
     *
     * @param numberOfRemainingCaptureSessionsToCreate the number of remaining capture sessions to create
     */
    public void setNumberOfRemainingCaptureSessionsToCreate(int numberOfRemainingCaptureSessionsToCreate) {
        this.mNumberOfRemainingCaptureSessionsToCreate = numberOfRemainingCaptureSessionsToCreate;
    }

    /**
     * Gets photo video storage path.
     *
     * @return the photo video storage path
     */
    public File getPhotoVideoStoragePath() {
        return mPhotoVideoStoragePath;
    }

    /**
     * Sets photo video storage path.
     *
     * @param photoVideoStoragePath the photo video storage path
     */
    public void setPhotoVideoStoragePath(File photoVideoStoragePath) {
        this.mPhotoVideoStoragePath = photoVideoStoragePath;
    }

    /**
     * Gets photo camera id.
     *
     * @return the photo camera id
     */
    public String getPhotoCameraId() {
        return mPhotoCameraId;
    }

    /**
     * Sets photo camera id.
     *
     * @param mPhotoCameraId the m photo camera id
     */
    public void setPhotoCameraId(String mPhotoCameraId) {
        this.mPhotoCameraId = mPhotoCameraId;
    }

    /**
     * Gets video camera id.
     *
     * @return the video camera id
     */
    public String getVideoCameraId() {
        return mVideoCameraId;
    }

    /**
     * Sets video camera id.
     *
     * @param videoCameraId the video camera id
     */
    public void setVideoCameraId(String videoCameraId) {
        this.mVideoCameraId = videoCameraId;
    }

    /**
     * Gets photo state.
     *
     * @return the photo state
     */
    public int getPhotoState() {
        return mPhotoState;
    }

    /**
     * Sets photo state.
     *
     * @param photoState the photo state
     */
    public void setPhotoState(int photoState) {
        this.mPhotoState = photoState;
    }

    /**
     * Is flash supported boolean.
     *
     * @return the boolean
     */
    public boolean isFlashSupported() {
        return mFlashSupported;
    }

    /**
     * Sets flash supported.
     *
     * @param flashSupported the flash supported
     */
    public void setFlashSupported(boolean flashSupported) {
        this.mFlashSupported = flashSupported;
    }

    /**
     * Is hardware is legacy boolean.
     *
     * @return the boolean
     */
    public boolean isHardwareIsLegacy() {
        return mHardwareIsLegacy;
    }

    /**
     * Sets hardware is legacy.
     *
     * @param hardwareIsLegacy the hardware is legacy
     */
    public void setHardwareIsLegacy(boolean hardwareIsLegacy) {
        this.mHardwareIsLegacy = hardwareIsLegacy;
    }
}
