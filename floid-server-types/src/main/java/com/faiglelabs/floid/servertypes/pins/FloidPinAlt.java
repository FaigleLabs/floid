/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.pins;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Alt version of pin with id
 */
@SuppressWarnings("WeakerAccess")
public class FloidPinAlt {
    /**
     * The Id.
     */
    public Long id;

    /**
     * The Pin name.
     */
    public String pinName;
    /**
     * The Pin lat.
     */
    public Double pinLat;
    /**
     * The Pin lng.
     */
    public Double pinLng;
    /**
     * The Pin height.
     */
    public Double pinHeight;
    /**
     * The Pin height auto.
     */
    public boolean pinHeightAuto;

    /**
     * Instantiates a new Floid pin alt.
     */
    public FloidPinAlt() {
        super();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets pin height.
     *
     * @return the pin height
     */
    public Double getPinHeight() {
        return pinHeight;
    }

    /**
     * Sets pin height.
     *
     * @param pinHeight the pin height
     */
    public void setPinHeight(Double pinHeight) {
        this.pinHeight = pinHeight;
    }

    /**
     * Gets pin lat.
     *
     * @return the pin lat
     */
    public Double getPinLat() {
        return pinLat;
    }

    /**
     * Sets pin lat.
     *
     * @param pinLat the pin lat
     */
    public void setPinLat(Double pinLat) {
        this.pinLat = pinLat;
    }

    /**
     * Gets pin lng.
     *
     * @return the pin lng
     */
    public Double getPinLng() {
        return pinLng;
    }

    /**
     * Sets pin lng.
     *
     * @param pinLng the pin lng
     */
    public void setPinLng(Double pinLng) {
        this.pinLng = pinLng;
    }

    /**
     * Gets pin name.
     *
     * @return the pin name
     */
    public String getPinName() {
        return pinName;
    }

    /**
     * Sets pin name.
     *
     * @param pinName the pin name
     */
    public void setPinName(String pinName) {
        this.pinName = pinName;
    }

    /**
     * Is pin height auto boolean.
     *
     * @return the boolean
     */
    public boolean isPinHeightAuto() {
        return pinHeightAuto;
    }

    /**
     * Sets pin height auto.
     *
     * @param pinHeightAuto the pin height auto
     */
    public void setPinHeightAuto(boolean pinHeightAuto) {
        this.pinHeightAuto = pinHeightAuto;
    }

    /**
     * Convert from JSON
     * @param jsonObject the JSON Object
     * @return a FloidPinAlt
     * @throws JSONException upon error
     */
    public FloidPinAlt fromJSON(JSONObject jsonObject) throws JSONException {
        try {
            id = jsonObject.getLong("id");
            pinName = jsonObject.getString("pinName");
            pinLat = jsonObject.getDouble("pinLat");
            pinLng = jsonObject.getDouble("pinLng");
            pinHeightAuto = jsonObject.getBoolean("pinHeightAuto");
            pinHeight = jsonObject.getDouble("pinHeight");
        } catch (Exception e) {
        }
        return this;
    }

}
