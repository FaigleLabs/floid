/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.geo;

/**
 * A Floid gps point
 */
public class FloidGpsPoint {

    private int floidId;
    private String floidUuid;
    private Long statusTime;
    private Double latitude;
    private Double longitude;
    private Double altitude;

    /**
     * Instantiates a new Floid gps point.
     *
     * @param floidId    the floid id
     * @param floidUuid  the floid uuid
     * @param statusTime the status time
     * @param latitude   the latitude
     * @param longitude  the longitude
     * @param altitude   the altitude
     */
    public FloidGpsPoint(int floidId, String floidUuid, Long statusTime, Double latitude, Double longitude, Double altitude) {
        this.floidId = floidId;
        this.floidUuid = floidUuid;
        this.statusTime = statusTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    /**
     * Create a new FloidGpsPoint from a current one:
     * @param floidGpsPoint the current one
     */
    public FloidGpsPoint(FloidGpsPoint floidGpsPoint) {
        this.floidId = floidGpsPoint.floidId;
        this.floidUuid = floidGpsPoint.floidUuid;
        this.statusTime = floidGpsPoint.statusTime;
        this.latitude = floidGpsPoint.latitude;
        this.longitude = floidGpsPoint.longitude;
        this.altitude = floidGpsPoint.altitude;
    }
    /**
     * Instantiates a new Floid gps point.
     */
    public FloidGpsPoint() {
        super();
    }

    /**
     * Gets floid id.
     *
     * @return the floid id
     */
    @SuppressWarnings("unused")
    public int getFloidId() {
        return floidId;
    }

    /**
     * Sets floid id.
     *
     * @param floidId the floid id
     */
    @SuppressWarnings("unused")
    public void setFloidId(int floidId) {
        this.floidId = floidId;
    }

    /**
     * Gets floid uuid.
     *
     * @return the floid uuid
     */
    @SuppressWarnings("unused")
    public String getFloidUuid() {
        return floidUuid;
    }

    /**
     * Sets floid uuid.
     *
     * @param floidUuid the floid uuid
     */
    @SuppressWarnings("unused")
    public void setFloidUuid(String floidUuid) {
        this.floidUuid = floidUuid;
    }

    /**
     * Gets status time.
     *
     * @return the status time
     */
    @SuppressWarnings("unused")
    public Long getStatusTime() {
        return statusTime;
    }

    /**
     * Sets status time.
     *
     * @param statusTime the status time
     */
    @SuppressWarnings("unused")
    public void setStatusTime(Long statusTime) {
        this.statusTime = statusTime;
    }

    /**
     * Gets latitude.
     *
     * @return the latitude
     */
    @SuppressWarnings("unused")
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Sets latitude.
     *
     * @param latitude the latitude
     */
    @SuppressWarnings("unused")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * Gets longitude.
     *
     * @return the longitude
     */
    @SuppressWarnings("unused")
    public Double getLongitude() {
        return longitude;
    }

    /**
     * Sets longitude.
     *
     * @param longitude the longitude
     */
    @SuppressWarnings("unused")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * Gets altitude.
     *
     * @return the altitude
     */
    @SuppressWarnings("unused")
    public Double getAltitude() {
        return altitude;
    }

    /**
     * Sets altitude.
     *
     * @param altitude the altitude
     */
    @SuppressWarnings("unused")
    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }
}
