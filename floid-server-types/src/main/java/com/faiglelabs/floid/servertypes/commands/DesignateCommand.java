/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Designate command - designates either pin or lat,lng, alt
 */
@Entity
public class DesignateCommand extends DroidCommand {
    private double x = 0;
    private double y = 0;
    private double z = 0;
    private boolean usePin = false;
    private boolean usePinHeight = false;
    private String pinName = "";

    /**
     * Instantiates a new Designate command.
     */
    public DesignateCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        DesignateCommand designateCommand = new DesignateCommand();
        super.factory(designateCommand, droidMission);
        designateCommand.x = x;
        designateCommand.y = y;
        designateCommand.z = z;
        designateCommand.usePin = usePin;
        designateCommand.usePinHeight = usePinHeight;
        designateCommand.pinName = pinName;
        return designateCommand;
    }

    /**
     * Gets pin name.
     *
     * @return the pin name
     */
    public String getPinName() {
        return pinName;
    }

    /**
     * Sets pin name.
     *
     * @param pinName the pin name
     */
    public void setPinName(String pinName) {
        this.pinName = pinName;
    }

    /**
     * Gets x.
     *
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * Sets x.
     *
     * @param x the x
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Gets y.
     *
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * Sets y.
     *
     * @param y the y
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Gets z.
     *
     * @return the z
     */
    public double getZ() {
        return z;
    }

    /**
     * Sets z.
     *
     * @param z the z
     */
    public void setZ(double z) {
        this.z = z;
    }

    /**
     * Is use pin boolean.
     *
     * @return the boolean
     */
    public boolean isUsePin() {
        return usePin;
    }

    /**
     * Sets use pin.
     *
     * @param usePin the use pin
     */
    public void setUsePin(boolean usePin) {
        this.usePin = usePin;
    }

    /**
     * Is use pin height boolean.
     *
     * @return the boolean
     */
    public boolean isUsePinHeight() {
        return usePinHeight;
    }

    /**
     * Sets use pin height.
     *
     * @param usePinHeight the use pin height
     */
    public void setUsePinHeight(boolean usePinHeight) {
        this.usePinHeight = usePinHeight;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("x", x);
        jsonObject.put("y", y);
        jsonObject.put("z", z);
        jsonObject.put("usePin", usePin);
        jsonObject.put("usePinHeight", usePinHeight);
        jsonObject.put("pinName", pinName);
        return jsonObject;
    }

    @Override
    public DesignateCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        x = jsonObject.getDouble("x");
        y = jsonObject.getDouble("y");
        z = jsonObject.getDouble("z");
        usePin = jsonObject.getBoolean("usePin");
        usePinHeight = jsonObject.getBoolean("usePinHeight");
        pinName = jsonObject.getString("pinName");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("x", "" + x);
        typeElement.setAttribute("y", "" + y);
        typeElement.setAttribute("z", "" + z);
        typeElement.setAttribute("usePin", "" + usePin);
        typeElement.setAttribute("usePinHeight", "" + usePinHeight);
        typeElement.setAttribute("pinName", pinName);
        return typeElement;
    }
}
