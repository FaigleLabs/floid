/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Debug command
 */
@Entity
public class DebugCommand extends DroidCommand {
    private int deviceIndex = 0;
    private int debugValue = 0;

    /**
     * Instantiates a new Debug command.
     */
    public DebugCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        DebugCommand debugCommand = new DebugCommand();
        super.factory(debugCommand, droidMission);
        debugCommand.deviceIndex = deviceIndex;
        debugCommand.debugValue = debugValue;
        return debugCommand;
    }

    /**
     * Gets device index.
     *
     * @return the device index
     */
    @SuppressWarnings("unused")
    public int getDeviceIndex() {
        return deviceIndex;
    }

    /**
     * Sets device index.
     *
     * @param debugIndex the debug index
     */
    public void setDeviceIndex(int debugIndex) {
        this.deviceIndex = debugIndex;
    }

    /**
     * Gets debug value.
     *
     * @return the debug value
     */
    @SuppressWarnings("unused")
    public int getDebugValue() {
        return debugValue;
    }

    /**
     * Sets debug value.
     *
     * @param debugValue the debug value
     */
    public void setDebugValue(int debugValue) {
        this.debugValue = debugValue;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("debugIndex", deviceIndex);
        jsonObject.put("debugValue", debugValue);
        return jsonObject;
    }

    @Override
    public DebugCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        deviceIndex = jsonObject.getInt("debugIndex");
        debugValue = jsonObject.getInt("debugValue");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("debugIndex", "" + deviceIndex);
        typeElement.setAttribute("debugValue", "" + debugValue);
        return typeElement;
    }
}
