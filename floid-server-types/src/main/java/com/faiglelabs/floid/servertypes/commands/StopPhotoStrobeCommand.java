/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Stop photo strobe command.  Follows a start photo strobe command to turn off the photo strobe.
 */
@Entity
public class StopPhotoStrobeCommand extends DroidCommand {
    /**
     * Instantiates a new Stop photo strobe command.
     */
    public StopPhotoStrobeCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        StopPhotoStrobeCommand stopPhotoStrobeCommand = new StopPhotoStrobeCommand();
        super.factory(stopPhotoStrobeCommand, droidMission);
        return stopPhotoStrobeCommand;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable") JSONObject jsonObject = super.toJSON();
        // No parameters to add...
        return jsonObject;
    }

    @Override
    public StopPhotoStrobeCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable") Element typeElement = super.addToXML(doc);
        // Adds nothing...
        return typeElement;
    }
}
