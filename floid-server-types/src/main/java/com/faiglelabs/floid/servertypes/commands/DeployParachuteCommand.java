/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Deploy parachute command.
 */
@Entity
public class DeployParachuteCommand extends DroidCommand {
    /**
     * Instantiates a new Deploy parachute command.
     */
    public DeployParachuteCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        DeployParachuteCommand deployParachuteCommand = new DeployParachuteCommand();
        super.factory(deployParachuteCommand, droidMission);
        return deployParachuteCommand;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        @SuppressWarnings("UnnecessaryLocalVariable")
        JSONObject jsonObject = super.toJSON();
        // No parameters to add
        return jsonObject;
    }

    @Override
    public DeployParachuteCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        @SuppressWarnings("UnnecessaryLocalVariable")
        Element typeElement = super.addToXML(doc);
        // Adds nothing
        return typeElement;
    }
}
