/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;


/**
 * Land command.
 */
@Entity
public class LandCommand extends DroidCommand {
    private double z = 0.0;

    /**
     * Instantiates a new Land command.
     */
    public LandCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        LandCommand landCommand = new LandCommand();
        super.factory(landCommand, droidMission);
        landCommand.z = z;
        return landCommand;
    }

    /**
     * Gets z.
     *
     * @return the z
     */
    @SuppressWarnings("unused")
    public double getZ() {
        return z;
    }

    /**
     * Sets z.
     *
     * @param z the z
     */
    @SuppressWarnings("unused")
    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("z", z);
        return jsonObject;
    }

    @Override
    public LandCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        z = jsonObject.getDouble("z");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("z", "" + z);
        return typeElement;
    }
}
