/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.statuses;

import com.faiglelabs.floid.database.FloidDatabaseTables;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 * Floid system log.
 */
@Entity
@Table(name = FloidDatabaseTables.FLOID_SYSTEM_LOG,
        indexes = {
        @Index(name="floidIdIndex", columnList = "floidId"),
        @Index(name="floidUuidIndex", columnList = "floidUuid"),
        @Index(name="timestampIndex", columnList = "timestamp"),
        @Index(name="typeIndex", columnList = "type"),
        @Index(name="floidMessageNumberIndex", columnList = "floidMessageNumber")
})
public class FloidSystemLog extends FloidMessage {

    static final private int SYSTEM_LOG_COLUMN_LENGTH = 15000;
    /**
     * The system log text
     */
    @Column(length = 15000)
    private String systemLog;

    /**
     * Instantiates a new Floid system log.
     */
    public FloidSystemLog() {
        systemLog = "";
        floidMessageNumber = getNextFloidMessageNumber();
    }
    /**
     * Instantiates a new Floid system log.
     */
    public FloidSystemLog(String systemLog) {
        setSystemLog(systemLog); // Make sure it gets trimmed
        floidMessageNumber = getNextFloidMessageNumber();
    }

    /**
     * Gets system log.
     *
     * @return the system log
     */
    @SuppressWarnings("unused")
    public String getSystemLog() {
        return systemLog;
    }

    /**
     * Sets system log.
     *
     * @param systemLog the system log
     */
    public void setSystemLog(String systemLog) {
        // Trim to limit:
        if(systemLog.length()>SYSTEM_LOG_COLUMN_LENGTH) {
            this.systemLog = systemLog.substring(systemLog.length()-SYSTEM_LOG_COLUMN_LENGTH);
        } else {
            this.systemLog = systemLog;
        }
    }

    @Override
    public FloidSystemLog fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        systemLog = jsonObject.getString("systemLog");
        return this;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("systemLog", systemLog);
        return jsonObject;
    }
}
