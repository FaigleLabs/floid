/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * The Speaker On command.
 * Performs alert, text to speech and/or file at a specified volume with option repeat and delay.
 * Differs from Speaker command in that this is performed asynchronously and requires a Speaker Off command.
 */
@Entity
public class SpeakerOnCommand extends DroidCommand {
    private boolean alert;
    private String mode;
    @Column(length = 2048)
    private String tts;
    private double pitch;
    private double rate;
    @Column(length = 1024)
    private String fileName;
    private double volume;
    private int speakerRepeat;
    private int delay;

    /**
     * Instantiates a new Speaker on command.
     */
    public SpeakerOnCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        SpeakerOnCommand speakerOnCommand = new SpeakerOnCommand();
        super.factory(speakerOnCommand, droidMission);
        speakerOnCommand.alert = alert;
        speakerOnCommand.mode = mode;
        speakerOnCommand.tts = tts;
        speakerOnCommand.pitch = pitch;
        speakerOnCommand.rate = rate;
        speakerOnCommand.fileName = fileName;
        speakerOnCommand.volume = volume;
        speakerOnCommand.speakerRepeat = speakerRepeat;
        speakerOnCommand.delay = delay;
        return speakerOnCommand;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("alert", alert);
        jsonObject.put("mode", mode);
        jsonObject.put("tts", tts);
        jsonObject.put("pitch", pitch);
        jsonObject.put("rate", rate);
        jsonObject.put("fileName", fileName);
        jsonObject.put("volume", volume);
        jsonObject.put("speakerRepeat", speakerRepeat);
        jsonObject.put("delay", delay);
        return jsonObject;
    }

    @Override
    public SpeakerOnCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        alert = jsonObject.getBoolean("alert");
        mode = jsonObject.getString("mode");
        tts = jsonObject.getString("tts");
        pitch = jsonObject.getDouble("pitch");
        rate = jsonObject.getDouble("rate");
        fileName = jsonObject.getString("fileName");
        volume = jsonObject.getDouble("volume");
        speakerRepeat = jsonObject.getInt("speakerRepeat");
        delay = jsonObject.getInt("delay");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);

        typeElement.setAttribute("alert", "" + alert);
        typeElement.setAttribute("mode", "" + mode);
        typeElement.setAttribute("tts", "" + tts);
        typeElement.setAttribute("pitch", "" + pitch);
        typeElement.setAttribute("rate", "" + rate);
        typeElement.setAttribute("fileName", "" + fileName);
        typeElement.setAttribute("volume", "" + volume);
        typeElement.setAttribute("speakerRepeat", "" + speakerRepeat);
        typeElement.setAttribute("delay", "" + delay);

        return typeElement;
    }

    /**
     * Is alert boolean.
     *
     * @return the boolean
     */
    public boolean isAlert() {
        return alert;
    }

    /**
     * Sets alert.
     *
     * @param alert the alert
     */
    public void setAlert(boolean alert) {
        this.alert = alert;
    }

    /**
     * Gets mode.
     *
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets mode.
     *
     * @param mode the mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Gets tts.
     *
     * @return the tts
     */
    public String getTts() {
        return tts;
    }

    /**
     * Sets tts.
     *
     * @param tts the tts
     */
    public void setTts(String tts) {
        this.tts = tts;
    }

    /**
     * Gets pitch.
     *
     * @return the pitch
     */
    public double getPitch() {
        return pitch;
    }

    /**
     * Sets pitch.
     *
     * @param pitch the pitch
     */
    public void setPitch(double pitch) {
        this.pitch = pitch;
    }

    /**
     * Gets rate.
     *
     * @return the rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * Sets rate.
     *
     * @param rate the rate
     */
    public void setRate(double rate) {
        this.rate = rate;
    }

    /**
     * Gets file name.
     *
     * @return the file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets file name.
     *
     * @param fileName the file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets volume.
     *
     * @return the volume
     */
    public double getVolume() {
        return volume;
    }

    /**
     * Sets volume.
     *
     * @param volume the volume
     */
    public void setVolume(double volume) {
        this.volume = volume;
    }

    /**
     * Gets speaker repeat.
     *
     * @return the speaker repeat
     */
    public int getSpeakerRepeat() {
        return speakerRepeat;
    }

    /**
     * Sets speaker repeat.
     *
     * @param speakerRepeat the speaker repeat
     */
    public void setSpeakerRepeat(int speakerRepeat) {
        this.speakerRepeat = speakerRepeat;
    }

    /**
     * Gets delay.
     *
     * @return the delay
     */
    public int getDelay() {
        return delay;
    }

    /**
     * Sets delay.
     *
     * @param delay the delay
     */
    public void setDelay(int delay) {
        this.delay = delay;
    }

}
