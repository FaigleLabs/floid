/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.pins;

import com.faiglelabs.floid.database.FloidDatabaseTables;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Floid pin set.
 */
@Entity
@Table(name = FloidDatabaseTables.FLOID_PIN_SET,
        indexes = {
                @Index(name = "idIndex", columnList = "id")
        })
public class FloidPinSet implements Serializable {
    /**
     * No pin set indicator
     */
    public static final long NO_PIN_SET = -1L;
    @Id
    @GeneratedValue
    private Long id;

    private String pinSetName;

    @SuppressWarnings("deprecation")
    @OneToMany(mappedBy = "pinSet", fetch = FetchType.EAGER, cascade = javax.persistence.CascadeType.ALL, targetEntity = FloidPin.class)
    @org.hibernate.annotations.Cascade(value = org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    private List<FloidPin> pinList = new ArrayList<>();

    /**
     * Instantiates a new Floid pin set.
     */
    public FloidPinSet() {
        super();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets pin list.
     *
     * @return the pin list
     */
    public List<FloidPin> getPinList() {
        return pinList;
    }

    /**
     * Sets pin list.
     *
     * @param pinList the pin list
     */
    @SuppressWarnings("unused")
    public void setPinList(List<FloidPin> pinList) {
        this.pinList = pinList;
    }

    /**
     * Gets pin set name.
     *
     * @return the pin set name
     */
    public String getPinSetName() {
        return pinSetName;
    }

    /**
     * Sets pin set name.
     *
     * @param pinSetName the pin set name
     */
    public void setPinSetName(String pinSetName) {
        this.pinSetName = pinSetName;
    }

    /**
     * Add a floid pin to this pinset
     *
     * @param floidPin the floid pin
     */
    public void add(FloidPin floidPin) {
        floidPin.setPinSet(this);
        pinList.add(floidPin);
    }
}
