/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Fly to command.  Flies to a pin, lat/lng or acquired home
 */
@Entity
public class FlyToCommand extends DroidCommand {
    private double x = 0;
    private double y = 0;
    private String pinName = "";
    private boolean flyToHome = false;

    /**
     * Instantiates a new Fly to command.
     */
    public FlyToCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        FlyToCommand flyToCommand = new FlyToCommand();
        super.factory(flyToCommand, droidMission);
        flyToCommand.x = x;
        flyToCommand.y = y;
        flyToCommand.pinName = pinName;
        flyToCommand.flyToHome = flyToHome;
        return flyToCommand;
    }

    /**
     * Is fly to home boolean.
     *
     * @return the boolean
     */
    public boolean isFlyToHome() {
        return flyToHome;
    }

    /**
     * Gets fly to home.
     *
     * @return the fly to home
     */
    public boolean getFlyToHome() {
        return flyToHome;
    }

    /**
     * Sets fly to home.
     *
     * @param flyToHome the fly to home
     */
    public void setFlyToHome(boolean flyToHome) {
        this.flyToHome = flyToHome;
    }

    /**
     * Gets pin name.
     *
     * @return the pin name
     */
    public String getPinName() {
        return pinName;
    }

    /**
     * Sets pin name.
     *
     * @param pinName the pin name
     */
    public void setPinName(String pinName) {
        this.pinName = pinName;
    }

    /**
     * Gets x.
     *
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * Sets x.
     *
     * @param x the x
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Gets y.
     *
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * Sets y.
     *
     * @param y the y
     */
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("x", x);
        jsonObject.put("y", y);
        jsonObject.put("pinName", pinName);
        jsonObject.put("flyToHome", flyToHome);

        return jsonObject;
    }

    @Override
    public FlyToCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        x = jsonObject.getDouble("x");
        y = jsonObject.getDouble("y");
        pinName = jsonObject.getString("pinName");
        flyToHome = jsonObject.getBoolean("flyToHome");

        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("x", "" + x);
        typeElement.setAttribute("y", "" + y);
        typeElement.setAttribute("pinName", pinName);
        typeElement.setAttribute("flyToHome", Boolean.valueOf(flyToHome).toString());
        return typeElement;
    }
}
