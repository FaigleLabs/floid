/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Pan tilt command.  Controls pan tilt for a device to point to a pin, a lat/lng/alt or directly control the pan and tilt angles
 * Not currently fully implemented
 */
@Entity
public class PanTiltCommand extends DroidCommand {
    private int device = 0;  // TODO [PanTilt] THIS NEEDS TO BE ABLE TO CHANGE FOR THE CAMERA (0), DESIGNATOR (1) or the others (2 & 3)
    private double x = 0;
    private double y = 0;
    private double z = 0;
    private boolean useAngle = true;
    private double pan = 0;
    private double tilt = 0;
    private String pinName = "";

    /**
     * Instantiates a new Pan tilt command.
     */
    public PanTiltCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        PanTiltCommand panTiltCommand = new PanTiltCommand();
        super.factory(panTiltCommand, droidMission);
        panTiltCommand.x = x;
        panTiltCommand.y = y;
        panTiltCommand.z = z;
        panTiltCommand.useAngle = useAngle;
        panTiltCommand.pan = pan;
        panTiltCommand.tilt = tilt;
        panTiltCommand.pinName = pinName;
        return panTiltCommand;
    }

    /**
     * Gets device.
     *
     * @return the device
     */
    public int getDevice() {
        return device;
    }

    /**
     * Sets device.
     *
     * @param device the device
     */
    public void setDevice(int device) {
        this.device = device;
    }

    /**
     * Gets pan.
     *
     * @return the pan
     */
    public double getPan() {
        return pan;
    }

    /**
     * Sets pan.
     *
     * @param pan the pan
     */
    public void setPan(double pan) {
        this.pan = pan;
    }

    /**
     * Gets pin name.
     *
     * @return the pin name
     */
    public String getPinName() {
        return pinName;
    }

    /**
     * Sets pin name.
     *
     * @param pinName the pin name
     */
    public void setPinName(String pinName) {
        this.pinName = pinName;
    }

    /**
     * Gets tilt.
     *
     * @return the tilt
     */
    public double getTilt() {
        return tilt;
    }

    /**
     * Sets tilt.
     *
     * @param tilt the tilt
     */
    public void setTilt(double tilt) {
        this.tilt = tilt;
    }

    /**
     * Is use angle boolean.
     *
     * @return the boolean
     */
    public boolean isUseAngle() {
        return useAngle;
    }

    /**
     * Gets use angle.
     *
     * @return the use angle
     */
    @SuppressWarnings("unused")
    public boolean getUseAngle() {
        return useAngle;
    }

    /**
     * Sets use angle.
     *
     * @param useAngle the use angle
     */
    public void setUseAngle(boolean useAngle) {
        this.useAngle = useAngle;
    }

    /**
     * Gets x.
     *
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * Sets x.
     *
     * @param x the x
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Gets y.
     *
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * Sets y.
     *
     * @param y the y
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Gets z.
     *
     * @return the z
     */
    public double getZ() {
        return z;
    }

    /**
     * Sets z.
     *
     * @param z the z
     */
    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("device", device);
        jsonObject.put("x", x);
        jsonObject.put("y", y);
        jsonObject.put("z", z);
        jsonObject.put("useAngle", useAngle);
        jsonObject.put("pan", pan);
        jsonObject.put("tilt", tilt);
        jsonObject.put("pinName", pinName);

        return jsonObject;
    }

    @Override
    public PanTiltCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        device = jsonObject.getInt("device");
        x = jsonObject.getDouble("x");
        y = jsonObject.getDouble("y");
        z = jsonObject.getDouble("z");
        useAngle = jsonObject.getBoolean("useAngle");
        pan = jsonObject.getDouble("pan");
        tilt = jsonObject.getDouble("tilt");
        pinName = jsonObject.getString("pinName");

        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("device", "" + device);
        typeElement.setAttribute("x", "" + x);
        typeElement.setAttribute("y", "" + y);
        typeElement.setAttribute("z", "" + z);
        typeElement.setAttribute("useAngle", Boolean.valueOf(useAngle).toString());
        typeElement.setAttribute("pan", "" + pan);
        typeElement.setAttribute("tilt", "" + tilt);
        typeElement.setAttribute("pinName", "" + pinName);
        return typeElement;
    }
}
