/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Set parameters command.  Specifically sets first pin is home or home pin name, a set of droid parameters and whether to reset the parameters
 */
@Entity
public class SetParametersCommand extends DroidCommand {
    private static final String PARAMETER_FIRST_PIN_IS_HOME = "firstPinIsHome";
    private static final String PARAMETER_DROID_PARAMETERS = "droidParameters";
    private static final String PARAMETER_RESET_DROID_PARAMETERS = "resetDroidParameters";
    private static final String PARAMETER_HOME_PIN_NAME = "homePinName";
    private boolean firstPinIsHome = false;
    private String homePinName = "";
    @Column(length = 2048)
    private String droidParameters = "";
    private boolean resetDroidParameters = false;

    /**
     * Instantiates a new Set parameters command.
     */
    public SetParametersCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        SetParametersCommand setParametersCommand = new SetParametersCommand();
        super.factory(setParametersCommand, droidMission);
        setParametersCommand.firstPinIsHome = firstPinIsHome;
        setParametersCommand.homePinName = homePinName;
        setParametersCommand.droidParameters = droidParameters;
        setParametersCommand.resetDroidParameters = resetDroidParameters;
        return setParametersCommand;
    }

    /**
     * Is first pin is home boolean.
     *
     * @return the boolean
     */
    public boolean isFirstPinIsHome() {
        return firstPinIsHome;
    }

    /**
     * Gets first pin is home.
     *
     * @return the first pin is home
     */
    @SuppressWarnings("unused")
    public boolean getFirstPinIsHome() {
        return firstPinIsHome;
    }

    /**
     * Sets first pin is home.
     *
     * @param firstPinIsHome the first pin is home
     */
    public void setFirstPinIsHome(boolean firstPinIsHome) {
        this.firstPinIsHome = firstPinIsHome;
    }

    /**
     * Gets home pin name.
     *
     * @return the home pin name
     */
    public String getHomePinName() {
        return homePinName;
    }

    /**
     * Sets home pin name.
     *
     * @param homePinName the home pin name
     */
    public void setHomePinName(String homePinName) {
        this.homePinName = homePinName;
    }

    /**
     * Gets droid parameters.
     *
     * @return the droid parameters
     */
    public String getDroidParameters() {
        return droidParameters;
    }

    /**
     * Set the droid parameters
     *
     * @param droidParameters the floid droid parameters - space (or \n) separated property=value entries
     *                        each property has format: ([a-z._A-Z]+=VALUE)
     *                        e.g. "droid.parameter.a=DEFAULT droid.parameter.b=100"
     */
    public void setDroidParameters(String droidParameters) {
        this.droidParameters = droidParameters;
    }

    /**
     * Is reset droid parameters boolean.
     *
     * @return the boolean
     */
    public boolean isResetDroidParameters() {
        return resetDroidParameters;
    }

    /**
     * Sets reset droid parameters.
     *
     * @param resetDroidParameters true if command is to reset droid parameters
     */
    public void setResetDroidParameters(boolean resetDroidParameters) {
        this.resetDroidParameters = resetDroidParameters;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put(PARAMETER_FIRST_PIN_IS_HOME, firstPinIsHome);
        jsonObject.put(PARAMETER_DROID_PARAMETERS, droidParameters);
        jsonObject.put(PARAMETER_RESET_DROID_PARAMETERS, resetDroidParameters);
        jsonObject.put(PARAMETER_HOME_PIN_NAME, homePinName);
        return jsonObject;
    }

    @Override
    public SetParametersCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        firstPinIsHome = jsonObject.getBoolean(PARAMETER_FIRST_PIN_IS_HOME);
        homePinName = jsonObject.getString(PARAMETER_HOME_PIN_NAME);
        droidParameters = jsonObject.getString(PARAMETER_DROID_PARAMETERS);
        resetDroidParameters = jsonObject.getBoolean(PARAMETER_RESET_DROID_PARAMETERS);
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute(PARAMETER_FIRST_PIN_IS_HOME, Boolean.valueOf(firstPinIsHome).toString());
        typeElement.setAttribute(PARAMETER_HOME_PIN_NAME, "" + homePinName);
        typeElement.setAttribute(PARAMETER_DROID_PARAMETERS, "" + droidParameters);
        typeElement.setAttribute(PARAMETER_RESET_DROID_PARAMETERS, Boolean.valueOf(resetDroidParameters).toString());
        return typeElement;
    }
}
