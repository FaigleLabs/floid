/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands.testcommands;

import com.faiglelabs.floid.servertypes.commands.DroidCommand;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;

import javax.persistence.Entity;


/**
 * Test command. Sends a test command to the floid and gets a test response.
 */
@Entity
public class TestCommand extends DroidCommand {
    private long testDuration = 0L;

    /**
     * Instantiates a new Debug command.
     */
    public TestCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        TestCommand testCommand = new TestCommand();
        super.factory(testCommand, droidMission);
        testCommand.testDuration = testDuration;
        return testCommand;
    }

    /**
     * Gets test duration.
     *
     * @return the test duration
     */
    @SuppressWarnings("unused")
    public long getTestDuration() {
        return testDuration;
    }

    /**
     * Sets test duration.
     *
     * @param testDuration the test duration
     */
    @SuppressWarnings("unused")
    public void setTestDuration(long testDuration) {
        this.testDuration = testDuration;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();

        jsonObject.put("testDuration", testDuration);

        return jsonObject;
    }


    @Override
    public TestCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);

        testDuration = jsonObject.getLong("testDuration");

        return this;
    }
}
