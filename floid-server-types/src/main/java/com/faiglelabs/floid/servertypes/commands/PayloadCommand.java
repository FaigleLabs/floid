/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid.servertypes.commands;

import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.persistence.Entity;

/**
 * Payload command. Performs payload drop given a bay
 */
@Entity
public class PayloadCommand extends DroidCommand {
    private int bay = 0;

    /**
     * Instantiates a new Payload command.
     */
    public PayloadCommand() {
        super();
    }

    @Override
    public DroidInstruction factory(DroidMission droidMission) {
        PayloadCommand payloadCommand = new PayloadCommand();
        super.factory(payloadCommand, droidMission);
        payloadCommand.bay = bay;
        return payloadCommand;
    }

    /**
     * Gets bay.
     *
     * @return the bay
     */
    public int getBay() {
        return bay;
    }

    /**
     * Sets bay.
     *
     * @param bay the bay
     */
    public void setBay(int bay) {
        this.bay = bay;
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put("bay", bay);
        return jsonObject;
    }

    @Override
    public PayloadCommand fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        bay = jsonObject.getInt("bay");
        return this;
    }

    @Override
    public Element addToXML(Document doc) {
        Element typeElement = super.addToXML(doc);
        typeElement.setAttribute("bay", "" + bay);
        return typeElement;
    }
}
