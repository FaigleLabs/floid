/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.debug;


/**
 * Floid debug token filter.
 */
@SuppressWarnings("unused")
public class FloidDebugTokenFilter {
    private String debugTokenBufferString = "";
    private String outputString = "";

    /**
     * Reset.
     */
    public void reset() {
        debugTokenBufferString = "";
        outputString = "";
    }

    /**
     * Filter string.
     *
     * @param inputString the input string
     * @return the string
     */
    public String filter(String inputString) {
        if(inputString != null) {
            for (int i = 0; i < inputString.length(); ++i) {
//            System.out.println("Processing at: " + i);
                processChar(inputString.charAt(i));
            }
        }
        
        // We always return the output string but clear it in the object
        String tempOutputString = outputString;
        outputString = "";
        return tempOutputString;
    }

    /**
     * Flush string.
     *
     * @return the string
     */
    public String flush() {
        // Returns the debugTokenBufferString as-is but also clears it
        String tempDebugTokenBufferString = debugTokenBufferString;
        debugTokenBufferString = "";
        return tempDebugTokenBufferString;
    }

    private void processChar(char b) {
        // First add it to the end of our current debugTokenBuffer:
        debugTokenBufferString = debugTokenBufferString + b;
//        System.out.println("DebugTokenBufferString length: " + debugTokenBufferString.length());
        if (debugTokenBufferString.length() == 1) {
            // No tokens so far - if this is not one then just output it
            if (b != '#') {
                processSavedDebugTokenBuffer();
            }
        } else if (debugTokenBufferString.length() == 2) {
            // We already have one token - need another - otherwise reprocess the saved chars:
            if (b != '#') {
                // Nope - output the top char and re-start processing with the rest...
                processSavedDebugTokenBuffer();
            }
        } else if (debugTokenBufferString.length() > 2) {
            // We are in the hex digits part - check them:
            // Valid hex char?
            if (!(((b >= '0' && b <= '9') || ((b >= 'A' && b <= 'F'))))) {
                processSavedDebugTokenBuffer();
            }
        }
        // Do we have a whole valid string?
        if (debugTokenBufferString.length() == 6) // ##FFFF
        {
            outputToken();
        }
    }

    private void processSavedDebugTokenBuffer() {
        // We had a no token failure or not a valid hex digit so output the top char and reprocess the rest...
        printChar(debugTokenBufferString.charAt(0));
        // Save the buffer:
        String savedDebugTokenBufferString = debugTokenBufferString;
        // Clear the buffer:
        debugTokenBufferString = "";
        // Start processing from the next one in the saved string:
        for (int i = 1; i < savedDebugTokenBufferString.length(); ++i) {
            processChar(savedDebugTokenBufferString.charAt(i));
        }
    }

    private void outputToken() {
        // Goal: Take characters 2-5 and 
        int token = 0;
        token += convertHexChar(debugTokenBufferString.charAt(2)) * 16 * 16 * 16;
        token += convertHexChar(debugTokenBufferString.charAt(3)) * 16 * 16;
        token += convertHexChar(debugTokenBufferString.charAt(4)) * 16;
        token += convertHexChar(debugTokenBufferString.charAt(5));
        printToken(token);
        debugTokenBufferString = "";
    }

    private int convertHexChar(char c) {
        // Valid hex char?
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'A' && c <= 'F') {
            return c - 'A' + 10;
        } else {
            System.out.println("ERROR CONVERTING CHARACTER: " + c);
            return 0;
        }
    }

    private void printChar(char c) {
        outputString = outputString + c;
    }

    private void printToken(int token) {
        outputString = outputString + FloidDebugTokens.getTokenString(token);
    }
}
