/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.utils;

/**
 * Distance Utilities
 */
public class DistanceUtils {
    /**
     * Calculate the distance between two points
     *
     * @param lat1 latitude first location
     * @param lng1 longitude first location
     * @param lat2 latitude second location
     * @param lng2 longitude second location
     * @return the distance between the two points in meters
     */
    // Returns meters:
    public static double calcLatLngDistance(double lat1, double lng1, double lat2, double lng2) {
        double deltaLong = Math.toRadians(lng1 - lng2);
        double sinDeltaLong = Math.sin(deltaLong);
        double cosDeltaLong = Math.cos(deltaLong);
        double lat1Radians = Math.toRadians(lat1);
        double lat2Radians = Math.toRadians(lat2);
        double sinLat1 = Math.sin(lat1Radians);
        double cosLat1 = Math.cos(lat1Radians);
        double sinLat2 = Math.sin(lat2Radians);
        double cosLat2 = Math.cos(lat2Radians);
        double llDelta = (cosLat1 * sinLat2) - (sinLat1 * cosLat2 * cosDeltaLong);
        double llDeltaSquared = (llDelta * llDelta) + (cosLat2 * sinDeltaLong) * (cosLat2 * sinDeltaLong);
        double llDeltaNorm = Math.sqrt(llDeltaSquared);
        double denom = (sinLat1 * sinLat2) + (cosLat1 * cosLat2 * cosDeltaLong);
        double delta = Math.atan2(llDeltaNorm, denom);
        return delta * 6372795;
        // Note: Original code below:
        /*
        float distance_between (float lat1, float long1, float lat2, float long2, float units_per_meter)
        {
          // returns distance in meters between two positions, both specified
            // as signed decimal-degrees latitude and longitude. Uses great-circle
            // distance computation for hypothesised sphere of radius 6372795 meters.
            // Because Earth is no exact sphere, rounding errors may be up to 0.5%.
          float delta = radians(long1-long2);
          float sdlong = sin(delta);
          float cdlong = cos(delta);
          lat1 = radians(lat1);
          lat2 = radians(lat2);
          float slat1 = sin(lat1);
          float clat1 = cos(lat1);
          float slat2 = sin(lat2);
          float clat2 = cos(lat2);
          delta = (clat1 * slat2) - (slat1 * clat2 * cdlong);
          delta = sq(delta);
          delta += sq(clat2 * sdlong);
          delta = sqrt(delta);
          float denom = (slat1 * slat2) + (clat1 * clat2 * cdlong);
          delta = atan2(delta, denom);
          return delta * 6372795 * units_per_meter;
        }
        */
    }
    // Parameters in degrees:

    /**
     * Calculate the angular distance between two headings in degrees
     *
     * @param h  first heading
     * @param h1 second heading
     * @return the angular distance in degrees
     */
    public static double calcHeadingDistance(double h, double h1) {
        return keepInDegreeRange(h1 - h);
    }

    /**
     * @param degrees angle in degrees
     * @return the angle clamped to (-180,180]
     */
    @SuppressWarnings("WeakerAccess")
    public static double keepInDegreeRange(double degrees) {
        while (degrees <= -180.0) {
            degrees += 360.0;
        }
        while (degrees > 180.0) {
            degrees -= 360.0;
        }
        return degrees;
    }

    // Compass heading to std trig angle
    public static double compassToDegrees(double heading)
    {
        return(keepInDegreeRange(90-heading));
    }
    // Std trig angle to compass degrees
    public static double  degreesToCompass(double degrees)
    {
        return(keepInDegreeRange(90-degrees));
    }
}
