/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.utils;

import com.faiglelabs.floid.servertypes.statuses.FloidDroidStatus;
import com.faiglelabs.proto.floid.FloidServerMessages.FloidServerMessage;

import java.io.BufferedInputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

/**
 * Utility routines for communicating messages to and from the floid server
 */
public class FloidServerMessageUtils {
    /**
     * The floid message start tag
     */
    private static final String FLOID_MESSAGE_START_TAG = "!FLOID!";
    /**
     * The floid message end tag
     */
    private static final String FLOID_MESSAGE_END_TAG = "!DIOLF!";
    /**
     * Create a standard server message
     * @param floidDroidStatus the current floid droid status
     * @param messageClass the class type of the message
     * @param floidMessageType the floid message type
     * @param messageString the message as a string
     * @return the server message
     */
    public static FloidServerMessage createStandardFloidServerMessage(FloidDroidStatus floidDroidStatus,
                                                                      String messageClass,
                                                                      String floidMessageType,
                                                                      String messageString) {
        return FloidServerMessage.newBuilder()
                .setFloidId(floidDroidStatus.getFloidId())
                .setFloidUuid(floidDroidStatus.getFloidUuid())
                .setMessageType(FloidServerMessage.FloidServerMessageType.STANDARD)
                .setMessageClass(messageClass)
                .setFloidMessageType(floidMessageType)
                .setStringMessage(messageString).build();
    }
    /**
     * Create an image server message
     * @param floidDroidStatus the current floid droid status
     * @param messageClass the class type of the message
     * @param floidMessageType the floid message type
     * @param floidServerImageMessage the floid image message
     * @return the server message
     */
    public static FloidServerMessage createImageFloidServerMessage(FloidDroidStatus floidDroidStatus, String messageClass, String floidMessageType, FloidServerMessage.FloidServerImageMessage floidServerImageMessage) {
        return FloidServerMessage.newBuilder()
                .setFloidId(floidDroidStatus.getFloidId())
                .setFloidUuid(floidDroidStatus.getFloidUuid())
                .setMessageType(FloidServerMessage.FloidServerMessageType.IMAGE)
                .setMessageClass(messageClass)
                .setFloidMessageType(floidMessageType)
                .setImageMessage(floidServerImageMessage).build();
    }
    /**
     * Write a floid message to the print stream
     * @param floidServerMessageBytes the message
     * @return true if success
     */
    public static boolean writeFloidMessage(PrintStream printStream, byte[] floidServerMessageBytes, FloidServerMessageStatus floidServerMessageStatus) {
        try {
            printStream.write(FLOID_MESSAGE_START_TAG.getBytes(StandardCharsets.UTF_8));
            printStream.write(intToByteArray(floidServerMessageBytes.length));
            printStream.write(floidServerMessageBytes);
            printStream.write(FLOID_MESSAGE_END_TAG.getBytes(StandardCharsets.UTF_8));
            printStream.flush();
            return true;
        } catch (Exception e) {
            floidServerMessageStatus.setError(true);
            floidServerMessageStatus.getLogs().append(e.toString()).append("\n");
        }
        return false;
    }
    /**
     * Read a floid message from the buffered input stream
     * @param bufferedInputStream the buffered input stream
     * @return the byte array of the floid message or null
     */
    public static byte[] readFloidMessage(BufferedInputStream bufferedInputStream, FloidServerMessageStatus floidServerMessageStatus) {
        try {
            boolean foundFloidTag = readTag(bufferedInputStream, FLOID_MESSAGE_START_TAG, false, floidServerMessageStatus);
            if(foundFloidTag) {
                int floidMessageLength = readLength(bufferedInputStream, floidServerMessageStatus);
                if(floidMessageLength>0) {
                    byte[] floidMessage = readMessage(bufferedInputStream, floidMessageLength, floidServerMessageStatus);
                    if(floidMessage!=null && floidMessage.length > 0) {
                        if(readTag(bufferedInputStream, FLOID_MESSAGE_END_TAG, true, floidServerMessageStatus)) {
                            return floidMessage;
                        }
                    }
                }
            }
            return null;
        } catch(Exception e) {
            floidServerMessageStatus.setError(true);
            floidServerMessageStatus.getLogs().append(e.toString()).append("\n");
            // Do nothing
            return null;
        }
    }

    /**
     * Read a tag from the buffered input stream
     * @param bufferedInputStream the buffered input stream
     * @param tag the expected tag
     * @param mustBeNext if the tag must be the next item or anywhere in stream
     * @return true if found or false if not found or not next
     */
    private static boolean readTag(BufferedInputStream bufferedInputStream, String tag, boolean mustBeNext, FloidServerMessageStatus floidServerMessageStatus) {
        int tagPosition = 0;
        while(tagPosition < tag.length()) {
            try {
                int i = bufferedInputStream.read();
                if (i == -1) {
                    // EOF
                    floidServerMessageStatus.getLogs().append("ReadTag: EOF").append("\n");
                    floidServerMessageStatus.setError(true);
                    return false;
                }
                if(((char)(i&0xff))==tag.charAt(tagPosition)) {
                    ++tagPosition;
                } else {
                    if(mustBeNext) {
                        floidServerMessageStatus.getLogs().append("Read Tag: Tag Not First").append("\n");
                        floidServerMessageStatus.setError(true);
                        return false;
                    }
                    // Check if this is a new start perhaps
                    if((tagPosition > 0) && ((char)(i&0xff))==tag.charAt(0)) {
                        tagPosition = 1;
                    } else {
                        tagPosition = 0;
                    }
                }
            } catch (Exception e) {
                floidServerMessageStatus.setError(true);
                floidServerMessageStatus.getLogs().append("Read Tag: ").append(e.toString()).append("\n");
                return false;
            }
        }
        return true;
    }
    /**
     * Read the length of the message buffer that is next
     * @param bufferedInputStream the buffered input stream
     * @return the length of the message buffer that follows or -1 if error
     */
    private static int readLength(BufferedInputStream bufferedInputStream, FloidServerMessageStatus floidServerMessageStatus) {
        try {
            int i = bufferedInputStream.read();
            if(i > -1) {
                int length = (0x000000ff&i)<<24;
                i = bufferedInputStream.read();
                if(i > -1) {
                    length += (0x000000ff&i)<<16;
                    i = bufferedInputStream.read();
                    if(i > -1) {
                        length += (0x000000ff&i)<<8;
                        i = bufferedInputStream.read();
                        if (i > -1) {
                            length += (0x000000ff&i)<<0;
                            return length;
                        }
                    }
                }
            }
            floidServerMessageStatus.getLogs().append("Read Length: EOF").append("\n");
            floidServerMessageStatus.setError(true);
        } catch (Exception e) {
            floidServerMessageStatus.setError(true);
            floidServerMessageStatus.getLogs().append("Read Length: ").append(e.toString()).append("\n");
        }
        return -1;
    }

    /**
     * Read a message from the buffered input stream of size length
     * @param bufferedInputStream the buffered input stream
     * @param length the desired length
     * @return the byte array of the message or null if could not read entire length
     */
    private static byte[] readMessage(BufferedInputStream bufferedInputStream, int length, FloidServerMessageStatus floidServerMessageStatus) {
        byte[] message = new byte[length];
        int currentOffset = 0;
        try {
            while(currentOffset < length) {
                int bytesRead = bufferedInputStream.read(message, currentOffset, length-currentOffset);
                if(bytesRead == -1) {
                    // EOF:
                    floidServerMessageStatus.setError(true);
                    floidServerMessageStatus.getLogs().append("Read Message: EOF").append("\n");
                    return null;
                } else {
                    currentOffset += bytesRead;
                }
            }
            return message;
        } catch (Exception e) {
            floidServerMessageStatus.setError(true);
            floidServerMessageStatus.getLogs().append("Read Message: ").append(e.toString()).append("\n");
            return null;
        }
    }

    /**
     * Convert an int to a byte array
     * @param data the integer
     * @return a byte array of size 4
     */
    private static byte[] intToByteArray( int data ) {
        byte[] result = new byte[4];
        result[0] = (byte) ((data & 0xFF000000) >> 24);
        result[1] = (byte) ((data & 0x00FF0000) >> 16);
        result[2] = (byte) ((data & 0x0000FF00) >> 8);
        result[3] = (byte) ((data & 0x000000FF) >> 0);
        return result;
    }
}
