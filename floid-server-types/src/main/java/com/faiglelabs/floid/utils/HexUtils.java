/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.utils;

/**
 * Simple hex utils
 */
public class HexUtils {
    /**
     * Single byte to hex string
     * @param b the byte
     * @return the hex string
     */
    public static String toHex(byte b) {
        try {
            return "" + Character.forDigit((b >> 4) & 0xF, 16)
                    + Character.forDigit((b & 0xF), 16);
        } catch (Exception e) {
            return "00";
        }
    }

    /**
     * Single byte from hex string
     * @param h the hex string
     * @return the byte (or zero if error)
     */
    public static byte fromHex(String h) {
        try {
            return (byte) ((Character.digit(h.charAt(0), 16) << 4)
                    + Character.digit(h.charAt(1), 16));
        } catch (Exception e) {
            return 0;
        }
    }
}
