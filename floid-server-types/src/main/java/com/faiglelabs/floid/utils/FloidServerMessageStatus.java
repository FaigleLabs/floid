/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid.utils;

public class FloidServerMessageStatus {
    private boolean error;
    private StringBuilder logs;

    public FloidServerMessageStatus() {
        error = false;
        logs = new StringBuilder();
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public StringBuilder getLogs() {
        return logs;
    }

    public void setLogs(StringBuilder logs) {
        this.logs = logs;
    }
}
