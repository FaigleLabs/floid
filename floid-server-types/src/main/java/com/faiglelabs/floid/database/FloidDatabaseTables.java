package com.faiglelabs.floid.database;

public class FloidDatabaseTables {
    public static final String FLOID_USER = "floid_user";
    public static final String FLOID_ID = "floid_id";
    public static final String FLOID_UUID = "floid_uuid";
    public static final String FLOID_PIN = "floid_pin";
    public static final String FLOID_PIN_SET = "floid_pin_set";
    public static final String FLOID_STATUS = "floid_status";
    public static final String FLOID_DROID_STATUS = "floid_droid_status";
    public static final String FLOID_MODEL_STATUS = "floid_model_status";
    public static final String FLOID_MODEL_PARAMETERS = "floid_model_parameters";
    public static final String DROID_INSTRUCTION = "droid_instruction";
    public static final String FLOID_DROID_PHOTO = "floid_droid_photo";
    public static final String FLOID_DROID_VIDEO_FRAME = "floid_droid_video_frame";
    public static final String FLOID_DEBUG_MESSAGE = "floid_debug_message";
    public static final String FLOID_DROID_MISSION_INSTRUCTION_STATUS_MESSAGE = "floid_droid_mission_instruction_status_message";
    public static final String FLOID_SYSTEM_LOG = "floid_system_log";
}
