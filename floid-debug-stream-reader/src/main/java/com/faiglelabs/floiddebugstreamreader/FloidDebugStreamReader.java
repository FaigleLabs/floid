/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floiddebugstreamreader;

import com.faiglelabs.floid.debug.FloidDebugTokenFilter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Floid debug stream reader
 */
// TODO [UPGRADE] MAKE THIS HAVE THE OPTION TO OPEN A SERIAL PORT AT A BAUD ETC ETC
// FloidDebugStreamReader [port [baud]]
//   e.g. FloidDebugStreamReader /usb/tty.name 115200
public class FloidDebugStreamReader {
    private final static FloidDebugTokenFilter floidDebugTokenFilter = new FloidDebugTokenFilter();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        switch (args.length) {
            case 0: {
                // Std in:
                try {
                    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                    //noinspection InfiniteLoopStatement
                    while (true) {
                        String output = floidDebugTokenFilter.filter(br.readLine());
                        System.out.println(output);
                    }
                } catch (IOException io) {
                    io.printStackTrace();
                }
                String finalOutput = floidDebugTokenFilter.flush();
                System.out.println(finalOutput);
            }
            break;
            case 1: {
                // Serial port:
                // Parameters: port
                String serialPortName = args[0];
                // Execute a serial streamer from the port:
                new FloidDebugSerialStreamer(serialPortName);
            }
            break;
            default: {
                System.out.println("Number of args: " + args.length);
                printUsage();
            }
        }
    }

    private static void printUsage() {
        System.out.println("java -jar \"FloidDebugStreamReader.jar\" [port]");
    }
}
