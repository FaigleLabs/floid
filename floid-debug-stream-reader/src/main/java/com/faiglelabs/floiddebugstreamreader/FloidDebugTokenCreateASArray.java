/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floiddebugstreamreader;

import com.faiglelabs.floid.debug.FloidDebugTokens;

/**
 * Create debug token string as as array
 */
public class FloidDebugTokenCreateASArray {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.print("private static var tokens : Array = [\n");
        for (int i = 0; i < FloidDebugTokens.getNumberTokens(); ++i) {
            // Note:format is AS array declaration:
            //    private static var tokens : Array = ["test1","test2","test3","test4"];
            System.out.print("                                      \"" + escapeString(FloidDebugTokens.getTokenString(i)) + "\"");
            if (i < FloidDebugTokens.getNumberTokens() - 1) {
                System.out.print(", ");
            }
            System.out.println();
        }
        System.out.println("                                    ];\n");
    }

    /**
     * Escape certain characters (\", \n and \\)for transport
     *
     * @param inputString unescaped string
     * @return the escaped string
     */
    private static String escapeString(String inputString) {
        StringBuilder outputStringBuilder = new StringBuilder();
        for (int i = 0; i < inputString.length(); ++i) {
            char c = inputString.charAt(i);
            switch (c) {
                case '\"': {
                    outputStringBuilder.append("\\\"");
                }
                break;
                case '\\': {
                    outputStringBuilder.append("\\\\");
                }
                break;
                case '\n': {
                    outputStringBuilder.append("\\n");
                }
                break;
                default: {
                    outputStringBuilder.append(c);
                }
                break;
            }
        }
        return outputStringBuilder.toString();
    }
}
