/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floiddebugstreamreader;

import com.faiglelabs.floid.debug.FloidDebugTokenFilter;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

/**
 * Streamer for floid debug serial
 */
@SuppressWarnings("WeakerAccess")
public class FloidDebugSerialStreamer implements SerialPortEventListener {
    private SerialPort mSerialPort;
    private final FloidDebugTokenFilter mFloidDebugTokenFilter = new FloidDebugTokenFilter();

    /**
     * Create and execute a debug serial streamer for this port:
     *
     * @param portName the port
     */
    public FloidDebugSerialStreamer(String portName) {
        try {
            mSerialPort = new SerialPort(portName);
            if (mSerialPort.openPort()) {
                mSerialPort.setParams(SerialPort.BAUDRATE_115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
                mSerialPort.addEventListener(this);
                Thread.sleep(1000000L); // Sleep forever...
            } else {
                System.out.println("Error opening port: " + portName);
            }
        } catch (InterruptedException | SerialPortException e2) {
            e2.printStackTrace();
            mSerialPort = null;
        }
    }

    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {
        if (serialPortEvent.isBREAK()) {
            System.out.println("BREAK");
        }
        if (serialPortEvent.isCTS()) {
            System.out.println("CTS");
        }
        if (serialPortEvent.isDSR()) {
            System.out.println("DSR");
        }
        if (serialPortEvent.isERR()) {
            System.out.println("ERR");
        }
        if (serialPortEvent.isRING()) {
            System.out.println("RING");
        }
        if (serialPortEvent.isRLSD()) {
            System.out.println("RLSD");
        }
        if (serialPortEvent.isRLSD()) {
            System.out.println("RLSD");
        }
        if (serialPortEvent.isRXFLAG()) {
            System.out.println("RXFLAG");
        }
        if (serialPortEvent.isTXEMPTY()) {
            System.out.println("TXEMPTY");
        }
        if (serialPortEvent.isRXCHAR()) {
            System.out.print(serialPortEvent.getEventValue() + " bytes\n");
            try {
                byte[] buffer = mSerialPort.readBytes(serialPortEvent.getEventValue());
                if (buffer != null) {
                    String output = mFloidDebugTokenFilter.filter(new String(buffer));
                    if (output.length() > 0) {
                        System.out.print(output);
                    }
                }
            } catch (SerialPortException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
