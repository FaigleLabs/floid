## Floid - Autonomous UAV/UAS System

### (c) Chris Faigle / faiglelabs
All uses and copying prohibited except by license 

faiglelabs@gmail.com


#### Add 'floid' menu command:
    In menu directory run:
      ./install-floid-shell-command.sh
#### Build
    ./gradlew build [build all projects]
    ./gradlew tasks [list all tasks]
    ./gradlew floid-server:tasks   [list all tasks from sub-project]
    ./gradlew floid-server:tomcatRun [run Floid server locally] [edit gradle.properties to set up debugging on port 7000]
    ./gradlew floid-controller:uploadController -Pupload_port="/dev/tty.usbserial-AJV9OOTG" [upload Floid controller]
    ./gradlew floid-pyr:uploadPyr -Pupload_port="/dev/tty.usbserial-A600e1Ao" [upload Floid IMU code]
              (plug in pyr via FTDI cable and look for port like above)
    ./gradlew floid-app:app:installFullDebug [build and Install Floid Android app [debug variant]
    ./gradlew floid-app:app:assemble  [assemble Floid Android app - all variants]
    ./gradlew floid-app:app:assembleRelease  [assemble Floid Android app - release]
    ./gradlew floid-debug-serial:run [run the debug stream reader]

#### Floid Menu
    Floid:
       0. Floid Setup
       1. Floid Software
       2. Floid Server Cloud
       3. Floid Consoles
    Installs:
       A. System Installs
    Private Directory:
       Y. List contents of private directory
    Quit:
       q. Quit

#### Floid Setup Menu:
       README:
         0. View README
       Mission Controller App Signing Key:
         G. Set up Floid App (APK) Signing Key
         H. List contents of Floid App Signing KeyStore
       Mission Controller Client and Floid Server Mutual Keys:
         I. Set up Floid Server and Client KeyStores and Certificates
         J. List contents of Floid Server KeyStore
         K. List contents of Floid Client KeyStore
         L. List certificate from local Floid Server (localhost:6002)
         M. List certificate from cloud Floid Server (floid.faiglelabs.com:6002)
       Web Server Keys and Usernames:
         N. Set up Floid Web Server KeyStores and Certificates
         O. Set up Floid Web Server Usernames and Passwords
       Map Keys:
         P. Set up Map Credentials
       Floid Database:
         S. Set up localhost Floid Database (floid) and Floid Server user (floid-server)
         U. Export Floid Database (Full)
         V. Export Floid Database (Schema)
         W. Import Floid Database (Full)
         X. Import Floid Database (Schema)
         Y. Clear Floid Database - todo - break this into multiple calls

#### Floid Software Menu:
    README:                
      0. View README
    Build:
      1. Build All Projects
    Local Server:
      2. Run Local Floid Server (https://localhost:8443/)
      @. Run Local Floid Server (with debug output) (https://localhost:8443/)
      !. Run already built Local Floid Server (https://localhost:8443/)
      3. Run Local Floid Server - DEBUG - port 5005
      %. Run already built Local Floid Server - DEBUG - port 5005
      4. Kill Local Floid Server
    Controller:
      5. Upload Floid Controller (Manual Port)
      6. Upload Floid Controller (Auto Port)
    IMU:
      7. Upload Floid IMU (Manual Port)
      8. Upload Floid IMU (Auto Port)
    Debug Serial Reader:
      9. Run Floid Controller Serial Debug Reader
    Mission Controller:
      A. Assemble Floid App (All Variants)
      B. Assemble Floid App (All Variants) - Signed for release <- Play Store
         To release, first:
         * Bump 'version' number in floid-app/build.gradle and floid-app/app/build.gradle
         * Bump 'versionCode' by 1 in floid-app/app/build.gradle
         * Set 'versionName' to same as 'version' in floid-app/app/build.gradle
         Then this command produces: floid-app/app/build/outputs/apk/full/release/app-full-release.apk
         Go to Floid Consoles -> Google Play to release
      C. Install Debug Floid App Locally
      D. Build Floid App as App Bundle (All Variants) Signed for Release
    Mission Commander:
      a. Listen to local websocket (8080) as SU
      b. Run Flutter Web on Chrome
      c. Build Signed Release Flutter Android App (APK)
      d. Build Signed Release Flutter Android App Split Per ABI (APK)   <-- 3 APK FILES - USE FOR PLAY STORE RELEASE
         To release, first:
         * Bump 'version' in floid_ui_app/pubspec.yaml
         * Run flutter clean (below)
         Then this command produces: 3 APKs in floid_ui_app/android/build/app/outputs/flutter-apk/
         Go to Floid Consoles -> Google Play to release
      e. Build Signed Release Flutter Android App Bundle (AAB)
      f. Clean Flutter (flutter clean)
      g. Upgrade Flutter (flutter upgrade)
      h. Upgrade Flutter pubspec.yaml dependencies (flutter pub upgrade)
      i. Run App Standalone on Device
      j. Start Android Emulator (TODO)
      k. Build Flutter Web
    Dependencies:
      l. Print Gradle Dependencies
      m. Print Flutter Dependencies
    Misc:
      V. Show Versions
      Y. Visualize (with Gource)
      Z. Count lines

#### Floid Server Cloud:
    Floid Server SQL:
      1. List SQL instances
      2. Describe
      3. See Operations
      4. Open Shell (password: faiglelabs) - creates: 127.0.0.1:9470
      5. Pull run data for floid id (first connect via 4 above) - creates [table]-[floidId].csv
      6. Pull run data for floid and uuid (first connect via 4 above)- creates [table]-[floidId]-[floidUuid].csv
      7. Create Instance
      8. Set Password
      9. Import Starter Data
      0. Additional Operations
    Floid Server Pod:
      a. Deploy Pod
      b. See Pod Status
      c. Get Events
      d. Delete Pod
      e. Create Service (Exposes http:8080 and https:8443)
      f. Delete Service
      g. Forward Tomcat Port to 8080
      h. Shell (ash) to Floid Server node in Floid Server pod
      i. Get faiglelabs-floid-server logs
    Cloud SQL Proxy Stuff:
      k. Create Proxy User: No password, Restricted to cloudsqlproxy machine nameset: (https://cloud.google.com/sql/docs/mysql/create-manage-users)
      l. Create SQL Proxy Service Account (gcloud): (https://cloud.google.com/sdk/gcloud/reference/iam/service-accounts/create)
      m. Get SQL Proxy Service Account Policies
      n. Describe SQL Proxy Service Account
      o. Delete Proxy User
    Tomcat Alpine Docker Image:
      p. Build Tomcat Alpine 9 Image  ** Be sure to first successfully run: Software --> Build All Projects
      r. Push Tomcat Alpine 9 Image   ** Be sure to first successfully run: Server Cloud --> Build Tomcat Alpine 9 Image
    Server Maps and Elevation Data:
      s. Create Cloud Storage Bucket: floid-server-maps
      t. Create Cloud Storage Bucket: floid-server-elevations
      u. Copy local /opt/floid-server-maps/maps to GCS Bucket: floid-server-maps
      v. Copy local /opt/elevations/elevations to GCS Bucket: floid-server-elevations
      w. Copy GCS Bucket: floid-server-maps to local /opt/floid-server-maps/maps
      x. Copy GCS Bucket: floid-server-elevations to local /opt/floid-server-elevations/elevations

#### Floid Consoles:
       1. Google Play
       2. Google Analytics
       3. Google Compute Engine
       4. Google Container Engine
       5. Google Cloud SQL
       6. Google Account
       7. StackDriver Metrics
       8. Floid Server Pod

#### Structure
    ├── db ........................ downoaded database files and schemas (SQL / initially empty)
    ├── etc ....................... etc files
    │   ├── docs .................. documentation (mixed)
    │   ├── parameter-files ....... sample parameter files (XML)
    │   └── test-missions ......... test missions (XML)
    ├── floid-app ................. floid app outer
    │   └── app ................... floid app implementation (Java / Android)
    ├── floid-controller .......... floid controller (C / Arduino / Mega ADK)
    ├── floid-debug-serial ........ serial debug reader (Java)
    ├── floid-debug-stream-reader . stream debug reader (Java)
    ├── floid-hardware ............ floid hardware designs (Eagle)
    ├── floid-imu ................. floid imu (C / Arduino / Teensy)
    ├── floid-line-smooth-test .... line smoothing test suite (Java)
    ├── floid-server .............. floid server (Java/Tomcat)
    ├── floid-server-cloud ........ floid server cloud deployment operations (Docker / Kubernetes)
    ├── floid-server-types ........ floid server universal types (Java)
    ├── floid-servo-setup ......... floid servo setup program (Java)
    ├── floid-servo-setup-board ... floid server setup board (C / Arduino / Mega ADK)
    ├── floid_ui_app .............. floid server interface (Web & App) (Flutter)
    ├── menu ...................... menu system (bash)
    ├── private ................... private keys and properties (mixed / initially empty)
    └── utils ..................... embedded utility files

#### Produce Floid Board
  + Load board and schematic - either FloidBoard9r1 or HeliBoard9r1
  + Go to board view: [Window->'2 Schematic']
  + Go to cam processor: [File->Cam Processor]
  + Open job file: [File->Open:Job...]
  + Choose floid-sfe-gerb274x.cam
  + Select 'Process Job'
  + Move the following files into a separate folder:
    * .dri Drill File
    * .gbl Bottom Route Layer
    * .gbs Bottom Solder
    * .gto Top Silkscreen
    * .gtl Top Route Layer
    * .gts Top Solder
  + Read Gerber files with MCN Gerber Viewer and verify each layer
  + Zip folder and submit to PCB shop

#### Prerequisites - Software
  + [PlatformIO](http://platformio.org)
  + [Android Studio](http://developer.android.com/sdk/index.html)
  + [git](http://www.git-scm.com/)
  + [gradle](http://gradle.org/)
  + [mySQL](http://www.mysql.com/) (to run Floid Server)
#### Prerequisites - Hardware
+ [Eagle 6.5+](http://www.cadsoftusa.com/)
+ [SparkFun libraries](https://github.com/sparkfun/SparkFun-Eagle-Libraries)
+ [MCN Gerber Viewer](http://www.mcn-audio.com/sharewares/)
