set datafile separator comma
set terminal png size 2000,1600 enhanced font
set output '__DIRECTORY__/floid-goal-target-orientation-__FLOID_ID__-__FLOID_UUID__.png'
plot "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"mo" title "Orientation to Target (mo) (std degrees)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"ga" title "Goal Angle (ga) (compass)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"mg" title "Orientation To Goal (mg) (std degrees)" with points ps 3 pointtype 7
