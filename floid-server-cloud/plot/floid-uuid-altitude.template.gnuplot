set datafile separator comma
set terminal png size 2000,1600 enhanced font
set output '__DIRECTORY__/floid-altitude-__FLOID_ID__-__FLOID_UUID__.png'
plot "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"aa" title "Altitude (aa)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"jmf" title "GPS Altitude (jmf)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"pzv" title "Altitude Velocity (pzv)" with points ps 3 pointtype 7
