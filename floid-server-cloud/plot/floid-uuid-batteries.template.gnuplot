set datafile separator comma
set terminal png size 2000,1600 enhanced font
set output '__DIRECTORY__/floid-batteries-__FLOID_ID__-__FLOID_UUID__.png'
plot "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"bm" title "Main (bm)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"b0" title "Battery 0 (b0)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"b1" title "Battery 1 (b1)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"b2" title "Battery 2 (b2)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"b3" title "Battery 3 (b3)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"ba" title "Battery Aux (ba)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"c0" title "Current 0 (c0)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"c1" title "Current 1 (c1)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"c2" title "Current 2 (c2)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"c3" title "Current 3 (c3)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"ca" title "Current Aux (ca)" with points ps 3 pointtype 7
