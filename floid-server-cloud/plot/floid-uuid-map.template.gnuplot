set datafile separator comma
set terminal png size 2000,1600 enhanced font
set output '__DIRECTORY__/floid-map-__FLOID_ID__-__FLOID_UUID__.png'
plot "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "jxf":"jyf" title "floid" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-droid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "gpsLongitude":"gpsLatitude" title "droid" with points ps 3 pointtype 7
