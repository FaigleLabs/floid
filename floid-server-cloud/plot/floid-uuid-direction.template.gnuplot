set datafile separator comma
set terminal png size 2000,1600 enhanced font
set output '__DIRECTORY__/floid-direction-__FLOID_ID__-__FLOID_UUID__.png'
plot "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"phs" title "Heading (phs)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"phv" title "Heading Velocity (phv)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"kd" title "Direction Over Ground (kd)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__-__FLOID_UUID__.csv" using "timestamp":"kv" title "Velocity (kv)" with points ps 3 pointtype 7
