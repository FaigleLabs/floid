set datafile separator comma
set terminal png size 2000,1600 enhanced font
set output '__DIRECTORY__/floid-pitch-roll-__FLOID_ID__.png'
plot "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"pps" title "Pitch (pps)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"ppv" title "Pitch Velocity (v)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"prs" title "Roll (prs)" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"prv" title "Roll Velocity (prv)" with points ps 3 pointtype 7
