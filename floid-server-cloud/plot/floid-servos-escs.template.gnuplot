set datafile separator comma
set terminal png size 2000,1600 enhanced font
set output '__DIRECTORY__/floid-servos-escs-__FLOID_ID__.png'
plot "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s00" title "Servo 0-0" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s01" title "Servo 0-1" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s02" title "Servo 0-2" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s10" title "Servo 1-0" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s11" title "Servo 1-1" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s12" title "Servo 1-2" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s20" title "Servo 2-0" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s21" title "Servo 2-1" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s22" title "Servo 2-2" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s30" title "Servo 3-0" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s31" title "Servo 3-1" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"s32" title "Servo 3-2" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"e0" title "ESC 0" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"e1" title "ESC 1" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"e2" title "ESC 2" with points ps 3 pointtype 7, \
     "__DIRECTORY__/floid-status-__FLOID_ID__.csv" using "timestamp":"e3" title "ESC 3" with points ps 3 pointtype 7
