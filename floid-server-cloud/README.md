# Floid Server Kubernetes
## Structure:
  + The Floid Server is deployed as follows:
    * A gcloud Service Account
    * A CloudSQL Database w/root password faiglelabs
    * A CloudSQL User on that database (w/no password)
    * A single pod with a single node running Tomcat on Alpine, called faiglelabs-floid-server
  + Tomcat node:
    * Is a Docker image that copies over the Floid Server war to the Tomcat apps directory
    * Then uses the cloud proxy with uses the gcloud Service Account to proxy the box
    * Tomcat uses the CloudSQL User that is created
  + Database:
    * Is created and then loaded with the floid.sql starter code
  + To deploy the floid-server:
    * Build all projects locally
    * Choose p: Build Tomcat Alpine 9 Image
    * Choose r: Push Tomcat Alpine 9 Image
    * Choose a: Deploy Pod
    * Choose b: See Pod Status and repeat until container is "Running"
    * Choose e: Create Service
  + To re-deploy the floid-server:
    * Build all projects locally
    * Choose p, r, d and a (build tomcat image, push tomcat image, delete current pod, deploy pod