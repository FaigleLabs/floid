#!/bin/sh

# Run Cloud SQL Proxy:
/cloud_sql_proxy -instances=faiglelabs-25d6e:us-central1:faiglelabs-floid-server-sql=tcp:3306 -credential_file=/scripts/floid-service.json &

# Connect Google Cloud Storage:
cp /go/bin/gcsfuse /usr/local/bin
# Elevations:
mkdir /opt/floid-server-elevations
gcsfuse --key-file /scripts/floid-service.json --implicit-dirs floid-server-elevations /opt/floid-server-elevations
# Maps:
mkdir /opt/floid-server-maps
gcsfuse --key-file /scripts/floid-service.json --implicit-dirs floid-server-maps /opt/floid-server-maps
# Images:
mkdir /opt/floid-server-images
gcsfuse --key-file /scripts/floid-service.json --implicit-dirs -o allow_other -file-mode=777 -dir-mode=777 floid-server-images /opt/floid-server-images

# Run Spring Boot from War:
cd /usr/local/tomcat/webapps

# Run normally:
java -jar ROOT.war > ../logs/faiglelabs_springboot.log
# Run w/network debug:
#java -Djavax.net.debug=all -jar ROOT.war > ../logs/faiglelabs_springboot.log

# NOTE TO CONNECT:
#mysql --host 127.0.0.1 -u floid-server

