/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floiddebugserial;

import com.faiglelabs.floid.debug.FloidDebugTokenFilter;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The main floid debug serial frame
 */
class FloidDebugSerialFrame extends JFrame implements SerialPortEventListener, ActionListener {
    private final JTextField fileNameTextField;
    private final JButton connectButton;
    private final JButton disconnectButton;
    private final JTextArea serialDisplayTextArea;
    private SerialPort serialPort = null;
    private FloidDebugTokenFilter floidDebugTokenFilter = new FloidDebugTokenFilter();

    /**
     * JFrame interface for the floid debug serial app
     *
     * @throws HeadlessException if headless - needs ui
     */
    FloidDebugSerialFrame() throws HeadlessException {
        super();
        // Create the main panel and set a layout:
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        JPanel filePanel = new JPanel();
        filePanel.setSize(new Dimension(800, 500));
        filePanel.setMinimumSize(new Dimension(800, 500));
        filePanel.setMaximumSize(new Dimension(800, 500));
        //      FlowLayout filePanelFlowLayout = new FlowLayout();
        //      filePanel.setLayout(filePanelFlowLayout);
        filePanel.setLayout(new BoxLayout(filePanel, BoxLayout.PAGE_AXIS));
        fileNameTextField = new JTextField(30);
        fileNameTextField.setText("/dev/tty.usbserial-AJV9OOTG");
        filePanel.add(fileNameTextField);
        JButton browseButton = new JButton("Browse..");
        browseButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        JFileChooser fileChooser = new JFileChooser();
                        if (fileChooser.showOpenDialog(rootPane) == JFileChooser.APPROVE_OPTION) {
                            fileNameTextField.setText(fileChooser.getSelectedFile().getAbsolutePath());
                        }
                    }
                }
        );
        filePanel.add(browseButton);
        // Connect Button:
        connectButton = new JButton("Connect");
        connectButton.addActionListener(this);
        filePanel.add(connectButton);
        // Disconnect button:
        disconnectButton = new JButton("Disconnect");
        disconnectButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        if (serialPort != null) {
                            try {
                                serialPort.closePort();
                            } catch (SerialPortException e) {
                                // do nothing...
                            }
                            serialPort = null;
                        }
                        connectButton.setEnabled(true);
                        disconnectButton.setEnabled(false);
                    }
                }
        );
        disconnectButton.setEnabled(false);
        filePanel.add(disconnectButton);
        // Turn on gps button:
        JButton turnOnGpsButton = new JButton("Turn on GPS");
        turnOnGpsButton.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        // TODO [TURN ON GPS] - IMPLEMENT THIS HERE
                                                    /*
                                                        if(serialPort != null)
                                                        {
                                                            try
                                                            {
                                                                serialPort.closePort();
                                                            }
                                                            catch(SerialPortException e)
                                                            {
                                                                // do nothing...
                                                            }
                                                            serialPort = null;
                                                        }
                                                        connectButton.setEnabled(true);
                                                        disconnectButton.setEnabled(false);
                                                    */
                    }
                }
        );
        turnOnGpsButton.setEnabled(true);
        filePanel.add(turnOnGpsButton);

        mainPanel.add(filePanel);
        serialDisplayTextArea = new JTextArea("");
        serialDisplayTextArea.setFont(new Font("Courier", Font.PLAIN, 16));
        JScrollPane serialDisplayScrollPane = new JScrollPane(serialDisplayTextArea);
        mainPanel.add(serialDisplayScrollPane);
        this.getContentPane().add(mainPanel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            String serialPortName = fileNameTextField.getText();
            serialPort = new SerialPort(serialPortName);
            if (serialPort.openPort()) {
                serialPort.setParams(SerialPort.BAUDRATE_115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
                connectButton.setEnabled(false);
                disconnectButton.setEnabled(true);
                serialPort.addEventListener(this);
                floidDebugTokenFilter = new FloidDebugTokenFilter();
                Thread.sleep(100L); // quick snooze
            } else {
                System.out.println("Failed to open port: " + serialPortName);
            }
        } catch (InterruptedException | SerialPortException e2) {
            e2.printStackTrace();
            serialPort = null;
        }
    }

    @Override
    public void serialEvent(SerialPortEvent serialPortEvent) {
        if (serialPortEvent.isBREAK()) {
            System.out.println("BREAK");
        }
        if (serialPortEvent.isCTS()) {
            System.out.println("CTS");
        }
        if (serialPortEvent.isDSR()) {
            System.out.println("DSR");
        }
        if (serialPortEvent.isERR()) {
            System.out.println("ERR");
        }
        if (serialPortEvent.isRING()) {
            System.out.println("RING");
        }
        if (serialPortEvent.isRLSD()) {
            System.out.println("RLSD");
        }
        if (serialPortEvent.isRXFLAG()) {
            System.out.println("RXFLAG");
        }
        if (serialPortEvent.isTXEMPTY()) {
            System.out.println("TXEMPTY");
        }
        if (serialPortEvent.isRXCHAR()) {
            System.out.print(serialPortEvent.getEventValue() + " bytes\n");
            try {
                byte[] buffer = serialPort.readBytes(serialPortEvent.getEventValue());
                if (buffer != null) {
                    String output = floidDebugTokenFilter.filter(new String(buffer));
                    if (output.length() > 0) {
                        serialDisplayTextArea.append(output);
                    }
                }
            } catch (SerialPortException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
