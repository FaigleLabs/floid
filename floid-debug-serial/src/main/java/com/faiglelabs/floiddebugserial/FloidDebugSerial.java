/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floiddebugserial;

/**
 * Main class for starting the serial debugger
 */
public class FloidDebugSerial {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Running");

        FloidDebugSerialFrame floidDebugSerialFrame = new FloidDebugSerialFrame();

        floidDebugSerialFrame.setSize(1100, 900);

        floidDebugSerialFrame.setVisible(true);
    }
}
