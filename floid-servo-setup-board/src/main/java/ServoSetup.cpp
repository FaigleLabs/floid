////////////////////////////////////////////
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
////////////////////////////////////////////
/////////////////////////////////////////////////
// Commands:                                   //
// ---------                                   //
// c          Connect / Reset (Optional)       //
// #h         Change to heli                   //
// f          ESC On                           //
// g          ESC Off                          //
// #[lrpe]    left, right, pitch or esc value  //
/////////////////////////////////////////////////
#include <Arduino.h>
#include <Servo.h>
#include "ServoSetup.h"
// Baud:
const int HOST_BAUD_RATE      = 19200;

#define NUMBER_HELICOPTERS (4)
#define NUMBER_HELI_SERVOS (3)

// Servos and ESCs:
// Digital pins:                                                    L   R   P
const int servoPins[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS]   = {
                                                                  {30, 32, 34},
                                                                  { 3,  4,  5},
                                                                  {37, 35, 33},
                                                                  {46, 48, 45}
                                                                };
const int escPins[NUMBER_HELICOPTERS]                         = {36, 6, 31, 47};
const int heliOnPins[NUMBER_HELICOPTERS]                      = {38, 8, 39, 49};
const int servoInitialValues[NUMBER_HELI_SERVOS]              = {1500, 1500, 1500};
const int escInitialValue                                     = 1000;
Servo     heliServos[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];
Servo     escServos[NUMBER_HELICOPTERS];
int       currentHeli                                         = 0;
int       v                                                   = 0;


void setup()
{
  delay(2000);               // Wait 2 seconds

  for(int heli=0; heli<NUMBER_HELICOPTERS; ++heli)
  {
    for(int servo=0; servo<NUMBER_HELI_SERVOS; ++servo)
    {
      heliServos[heli][servo].attach(servoPins[heli][servo]);
      heliServos[heli][servo].write(servoInitialValues[servo]);
    }
    escServos[heli].attach(escPins[heli]);
    escServos[heli].write(escInitialValue);
  }

  // Attach to the serial line:
  Serial.begin(HOST_BAUD_RATE);       // Open serial line:
  delay(2000);                        // Wait 2 seconds
  Serial.println("Ready");            // Let the control program know we are ready...
  reset();
}

/*
Pick helis:
  #h
Turn esc on / off
  f - esc on
  g - esc off
Set values:
  #l
  #r
  #p
  #e
*/
 
void loop()
{
  if ( Serial.available())
  {
    char ch = Serial.read();

    switch(ch)
    {
      case '0'...'9':
            {
              Serial.print("v: ");
              Serial.print(v);
              Serial.print(" -> ");
              v = v * 10 + (ch - '0');
              Serial.println(v);
            }
            break;

      case 'l':
            {
              heliServos[currentHeli][0].write(v);
              Serial.print("H: ");
              Serial.print(currentHeli);
              Serial.print("  L: ");
              Serial.println(v);
              reset();
            }
            break;

      case 'r':
            {
              heliServos[currentHeli][1].write(v);
              Serial.print("H: ");
              Serial.print(currentHeli);
              Serial.print("  R: ");
              Serial.println(v);
              reset();
            }
            break;

      case 'p':
            {
              heliServos[currentHeli][2].write(v);
              Serial.print("H: ");
              Serial.print(currentHeli);
              Serial.print("  P: ");
              Serial.println(v);
              reset();
            }
            break;

      case 'e':
            {
              Serial.print("H: ");
              Serial.print(currentHeli);
              Serial.print("  E: ");
              Serial.println(v);
              escServos[currentHeli].write(v);
              v = 0;
            }
            break;

      case 'f':
            {
              // ESC On:
              Serial.print("H: ");
              Serial.print(currentHeli);
              Serial.println("  ESC ON - 1000");
              escServos[currentHeli].write(1000);  // Needed to make sure helicopter startup works - modify for your needs
              digitalWrite(heliOnPins[currentHeli], HIGH);
              reset();
            }
            break;

      case 'g':
            {
              // ESC Off:
              Serial.print("H: ");
              Serial.print(currentHeli);
              Serial.println("  ESC OFF");
              digitalWrite(heliOnPins[currentHeli], LOW);
              reset();
            }
            break;

      case 'h':
            {
              // Pick heli:
              Serial.print("Change to Heli: ");
              if(v<4)
              {
                currentHeli = v;
                Serial.println(currentHeli);
              }
              else
              {
                Serial.println("Bad Value");
              }
              reset();
            }
            break;
      case 'c': // connect
            {
              reset();
            }
            break;
        default:
            {
              Serial.println("Bad command");
              reset();
            }
            break;
    }
  }
}
void reset()
{
  v=0;
  Serial.println("CMD> ");            // Let the control program know we are ready...
}




