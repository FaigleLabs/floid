#!/bin/bash
# shellcheck disable=SC2164
# shellcheck disable=SC2162
# shellcheck disable=SC2086

# Make working directory one above this one
pushd .. > /dev/null
FLOID_APP_DIRECTORY=../../../../../floid-app/app
FLOID_APP_RAW_DIRECTORY=../../../../../floid-app/app/src/main/res/raw
FLOID_APK_JKS_FILE=floid-apk.jks
FLOID_APK_SIGNING_ALIAS=floid-apk-signing
FLOID_APK_PROPERTIES_TEMPLATE_FILE=floid-apk-template.properties
FLOID_APK_PROPERTIES_FILE=floid-apk.properties
FLOID_SERVER_KEY_DIRECTORY=floid-server/src/main/resources/keys
FLOID_SERVER_KEY_ALIAS=floid_server_key
FLOID_SERVER_KEYSTORE=floid_server.jks
FLOID_SERVER_CERT_DER_FILE=floid_server.der
FLOID_CLIENT_KEYSTORE=floid_client.jks
FLOID_CLIENT_CERT_FILE=floid_client.crt
FLOID_CLIENT_CERT_BKS_FILE=floid_client.bks
FLOID_WEB_JKS_FILE=floid_web.jks
FLOID_WEB_KEYS_P12_FILE=floid_web_keys.p12
FLOID_WEB_KEYS_PEM_FILE=floid_web_keys.pem
FLOID_WEB_CERT_DER_FILE=floid_web_cert.der
FLOID_WEB_CERT_PEM_FILE=floid_web_cert.pem
FLOID_WEB_CERT_FILE=floid_web.crt
FLOID_WEB_KEY_ALIAS=floid_web_key
FLOID_CLOUD_KEYSTORE_DIRECTORY=../../../../../floid-server-cloud/private/
FLOID_CLOUD_SERVER_CONFIG_TEMPLATE=../../../../../floid-server-cloud/server_template.xml
FLOID_CLOUD_SERVER_CONFIG_FILE=../../../../../floid-server-cloud/server.xml
FLOID_UI_APP_KEYS_DIRECTORY=../../../../../floid_ui_app/lib/ui/keys/
FLOID_UI_APP_RAW_DIRECTORY=../../../../../floid_ui_app/assets/raw

clear
while true
do
  echo "______ _       _     _   _   _  ___  _   _   _____           _                 ";
  echo "|  ___| |     (_)   | | | | | |/ _ \| | | | /  ___|         | |                ";
  echo "| |_  | | ___  _  __| | | | | / /_\ \ | | | \ \`--. _   _ ___| |_ ___ _ __ ___  ";
  echo "|  _| | |/ _ \| |/ _\` | | | | |  _  | | | |  \`--. \ | | / __| __/ _ \ '_ \` _ \ ";
  echo "| |   | | (_) | | (_| | | |_| | | | \ \_/ / /\__/ / |_| \__ \ ||  __/ | | | | |";
  echo "\_|   |_|\___/|_|\__,_|  \___/\_| |_/\___/  \____/ \__, |___/\__\___|_| |_| |_|";
  echo "                                                    __/ |                      ";
  echo "                                                   |___/                       ";
  echo "                   ______      _       __     __      __        ";
  echo "                  / ____/___ _(_)___ _/ /__  / /___ _/ /_  _____";
  echo "                 / /_  / __ \`/ / __ \`/ / _ \/ / __ \`/ __ \/ ___/";
  echo "                / __/ / /_/ / / /_/ / /  __/ / /_/ / /_/ (__  ) ";
  echo "               /_/    \__,_/_/\__, /_/\___/_/\__,_/_.___/____/  ";
  echo "                             /____/                             ";
  echo ""
  echo "> Floid Setup Menu:"
  echo "README:"
  echo "  0. View README"
  echo "Mission Controller App Signing Key:"
  echo "  G. Set up Floid App (APK) Signing Key"
  echo "  H. List contents of Floid App Signing KeyStore"
  echo "Mission Controller Client and Floid Server Mutual Keys:"
  echo "  I. Set up Floid Server and Client KeyStores and Certificates"
  echo "  J. List contents of Floid Server KeyStore"
  echo "  K. List contents of Floid Client KeyStore"
  echo "  L. List certificate from local Floid Server (localhost:6002)"
  echo "  M. List certificate from cloud Floid Server (floid.faiglelabs.com:6002)"
  echo "Web Server Keys and Usernames:"
  echo "  N. Set up Floid Web Server KeyStores and Certificates"
  echo "  O. List contents of Floid Web Server KeyStore"
  echo "  P. Set up Floid Web Server Usernames and Passwords"
  echo "Map Keys:"
  echo "  R. Set up Map Credentials"
  echo "Floid Database:"
  echo "  S. Set up localhost Floid Database (floid) and Floid Server user (floid-server)"
  echo "  U. Export Floid Database (Full)"
  echo "  V. Export Floid Database (Schema)"
  echo "  W. Import Floid Database (Full)"
  echo "  X. Import Floid Database (Schema)"
  echo "  Y. Clear Floid Database - todo - break this into multiple calls"
  echo "  1. Pull Floid Messages from local db - ** files will be in floid/db **"
  echo "Elevations:"
  echo "  7. Into /opt/floid-server-elevations/elevations, pull SRTM data and split into (40,000) tiles"
  echo "Quit:"
  echo "  q. Quit"
  echo -e "Select: \c"
  read answer
  case "$answer" in
    0) mdless README.md
       ;;

    G) clear
       echo "Changing to private directory..."
       pushd private > /dev/null
       echo "Backing up ->APK SIGNING<- keystore files if they exist..."
       mkdir bak 2> /dev/null
       mv -f -v $FLOID_APK_JKS_FILE bak 2> /dev/null
       mv -f -v $FLOID_APK_PROPERTIES_FILE bak 2> /dev/null
       echo -e "Enter password for new ->APK SIGNING<- keystore and key: \c"
       read apk_signing_password
       keytool -v -genkeypair -keyalg RSA -keysize 4096 -validity 7305 -alias $FLOID_APK_SIGNING_ALIAS -dname "CN=FloidApp.floid.faiglelabs.com" -keystore $FLOID_APK_JKS_FILE -storepass $apk_signing_password -keypass $apk_signing_password
       echo "Updating passwords for -->APK SIGNING<--"
       sed 's/__GEN__/*** GENERATED FILE - DO NOT EDIT - DO NOT CHECK IN - EDIT TEMPLATE INSTEAD ***/g' $FLOID_APK_PROPERTIES_TEMPLATE_FILE \
           | sed "s/__SIGNING_KEYSTORE_PASSWORD__/$apk_signing_password/g" \
           | sed "s/__SIGNING_KEY_PASSWORD__/$apk_signing_password/g" \
           > $FLOID_APK_PROPERTIES_FILE
       popd
       ;;

    H) clear
       echo "Changing to private directory..."
       pushd private > /dev/null
       keytool -list -v -keystore $FLOID_APK_JKS_FILE
       popd
       ;;

    I) clear
       echo "Changing to key directory..."
       pushd $FLOID_SERVER_KEY_DIRECTORY > /dev/null
       echo "Backing up ->SERVER<- keystore files if they exist..."
       mkdir bak 2>/dev/null
       mv -f -v $FLOID_SERVER_KEYSTORE bak 2> /dev/null
       mv -f -v $FLOID_SERVER_CERT_DER_FILE bak 2> /dev/null
       mv -f -v FLOID_SERVER_CERT_FILE bak 2> /dev/null
       echo "Backing up ->CLIENT<- keystore files if they exist..."
       mv -f -v $FLOID_CLIENT_KEYSTORE bak 2> /dev/null
       mv -f -v $FLOID_CLIENT_CERT_FILE bak 2> /dev/null
       mv -f -v $FLOID_CLIENT_CERT_BKS_FILE bak 2> /dev/null
       echo -e "Enter password for new ->SERVER<- keystore and key: \c"
       read server_password
       echo "Generating ->SERVER<- keystore and private key..."
       keytool -v -genkeypair -keyalg RSA -keysize 4096 -validity 7305 -alias $FLOID_SERVER_KEY_ALIAS -dname "CN=FloidServer.floid.faiglelabs.com" -keystore $FLOID_SERVER_KEYSTORE -storepass $server_password -keypass $server_password
       echo "Exporting ->SERVER<- certificate to floid_server.der (DER FORMAT) [For client to trust server the server must have the private key]..."
       keytool -v -exportcert -alias $FLOID_SERVER_KEY_ALIAS -keystore $FLOID_SERVER_KEYSTORE -file $FLOID_SERVER_CERT_DER_FILE -storepass $server_password
       echo -e "Enter password for new ->CLIENT<- keystore and key: \c"
       read client_password
       echo "Generating ->CLIENT<- keystore and private key..."
       keytool -v -genkeypair -keyalg RSA -keysize 4096 -validity 7305 -alias floid_client_key -dname "CN=FloidClient.floid.faiglelabs.com" -keystore $FLOID_CLIENT_KEYSTORE -storepass $client_password -keypass $client_password
       echo "Exporting ->CLIENT<- certificate..."
       keytool -export -alias floid_client_key -file $FLOID_CLIENT_CERT_FILE -keystore $FLOID_CLIENT_KEYSTORE -storepass $client_password
       echo "Importing ->CLIENT<- certificate as trusted into ->SERVER<- keystore..."
       keytool -import -trustcacerts -noprompt -alias floid_client_key -file $FLOID_CLIENT_CERT_FILE -keystore $FLOID_SERVER_KEYSTORE -storepass $server_password
       echo "Exporting ->CLIENT<- keystore to BKS format..."
       keytool -importkeystore -srckeystore $FLOID_CLIENT_KEYSTORE -srcstoretype JKS -srcstorepass $client_password -destkeystore $FLOID_CLIENT_CERT_BKS_FILE -deststoretype BKS -deststorepass $client_password -provider org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath ../../../../../utils/bcprov-jdk15on-1.50.jar
       echo "Copying ->SERVER<- certificate (DER format) to Floid App..."
       cp -f $FLOID_SERVER_CERT_DER_FILE $FLOID_APP_RAW_DIRECTORY
       echo "Copying ->CLIENT<- keystore (BKS format) to Floid App..."
       cp -f $FLOID_CLIENT_CERT_BKS_FILE $FLOID_APP_RAW_DIRECTORY
       echo -e "Enter JWT secret: \c"
       read JWT_SECRET
       E_JWT_SECRET=`echo -n $JWT_SECRET | base64`
       echo "Encoded JWT secret is : $E_JWT_SECRET"
       echo "Updating passwords on -->SERVER<--"
       sed 's/__GEN__/*** GENERATED FILE - DO NOT EDIT - DO NOT CHECK IN - EDIT TEMPLATE INSTEAD ***/g' ../application_template.properties \
           | sed "s/__SERVER_KEYSTORE_PASSWORD__/$server_password/g" \
           | sed "s/__CLIENT_CERTIFICATE_PASSWORD__/$server_password/g" \
           | sed "s/__WEB_KEY_PASSWORD__/$web_password/g" \
           | sed "s/__WEB_KEY_ALIAS__/$FLOID_WEB_KEY_ALIAS/g" \
           | sed "s/__WEB_JKS_FILE__/$FLOID_WEB_JKS_FILE/g" \
           | sed "s/__JWT_SECRET__/$E_JWT_SECRET/g" \
           > ../application.properties
       echo "Updating passwords on -->CLIENT<--"
       sed 's/__GEN__/*** GENERATED FILE - DO NOT EDIT - DO NOT CHECK IN - EDIT TEMPLATE INSTEAD ***/g' $FLOID_APP_DIRECTORY/gradle_template.properties \
           | sed "s/__CLIENT_KEYSTORE_PASSWORD__/$client_password/g" \
           | sed "s/__CLIENT_CERTIFICATE_PASSWORD__/$client_password/g" \
           > $FLOID_APP_DIRECTORY/gradle.properties
       echo "Returning to original directory..."
       popd > /dev/null
       ;;

    J) keytool -list -v -keystore $FLOID_SERVER_KEY_DIRECTORY/$FLOID_SERVER_KEYSTORE
       ;;

    K) keytool -list -v -keystore $FLOID_SERVER_KEY_DIRECTORY/$FLOID_CLIENT_KEYSTORE
       ;;

    L) openssl s_client -connect localhost:6002 -prexit -showcerts
       ;;

    M) openssl s_client -showcerts -connect floid.faiglelabs.com:6002 -prexit
       ;;

    N) clear
       echo "Changing to key directory..."
       pushd $FLOID_SERVER_KEY_DIRECTORY > /dev/null
       echo "Backing up ->WEB<- keystore files if they exist..."
       mkdir bak 2>/dev/null
       mv -f -v $FLOID_WEB_JKS_FILE bak 2> /dev/null
       mv -f -v $FLOID_WEB_KEYS_P12_FILE bak 2> /dev/null
       mv -f -v $FLOID_WEB_KEYS_PEM_FILE bak 2> /dev/null
       mv -f -v $FLOID_WEB_CERT_DER_FILE bak 2> /dev/null
       mv -f -v $FLOID_WEB_CERT_PEM_FILE bak 2> /dev/null
       echo -e "Enter password for new ->WEB<- keystore and key: \c"
       read web_password
       echo "Generating ->WEB<- keystore and private key..."
       keytool -v -genkeypair -keyalg RSA -keysize 4096 -validity 7305 -alias $FLOID_WEB_KEY_ALIAS -dname "CN=floid.faiglelabs.com" -keystore $FLOID_WEB_JKS_FILE -storepass $web_password -keypass $web_password
       echo "Converting ->WEB<- keystore to PKCS#12 format..."
       keytool -importkeystore \
                    -srckeystore $FLOID_WEB_JKS_FILE       -srcalias $FLOID_WEB_KEY_ALIAS -srcstoretype jks     -srcstorepass $web_password  -srckeypass $web_password \
                    -destkeystore $FLOID_WEB_KEYS_P12_FILE -destalias $FLOID_WEB_KEY_ALIAS -deststoretype pkcs12 -deststorepass $web_password -destkeypass $web_password
       echo "Converting ->WEB<- keystore PKCS#12 format to PEM format..."
       openssl pkcs12  -passin pass:$web_password -passout pass:$web_password -in $FLOID_WEB_KEYS_P12_FILE -out $FLOID_WEB_KEYS_PEM_FILE
       echo "Extracting ->WEB<- certificate to floid_web.der (DER FORMAT)"
       keytool -v -exportcert -alias floid_web_key -keystore $FLOID_WEB_JKS_FILE -file $FLOID_WEB_CERT_DER_FILE -storepass $web_password
       echo "Extracting ->WEB<- certificate DER format to floid_web.pem (PEM FORMAT)"
       openssl x509 -inform DER -in $FLOID_WEB_CERT_DER_FILE -out $FLOID_WEB_CERT_PEM_FILE -text -outform PEM
       echo "Exporting ->WEB<- certificate..."
       keytool -exportcert -rfc -alias $FLOID_WEB_KEY_ALIAS -file $FLOID_WEB_CERT_FILE -keystore $FLOID_WEB_JKS_FILE -storepass $web_password
       mkdir $FLOID_UI_APP_RAW_DIRECTORY 2>/dev/null
       echo "Copying ->WEB<- certificate to floid_ui_app"
       cp  $FLOID_WEB_CERT_FILE $FLOID_UI_APP_RAW_DIRECTORY
       echo "Copying ->WEB<- keystore to Docker/Kubernetes..."
       cp -f $FLOID_WEB_JKS_FILE $FLOID_CLOUD_KEYSTORE_DIRECTORY
       echo "Updating passwords on -->WEB<-- (Docker/Kubernetes)"
       sed 's/__GEN__/*** GENERATED FILE - DO NOT EDIT - DO NOT CHECK IN - EDIT server_template.xml INSTEAD *** /g' $FLOID_CLOUD_SERVER_CONFIG_TEMPLATE \
           | sed "s/__WEB_KEYSTORE_PASSWORD__/$web_password/g" \
           | sed "s/__WEB_KEY_PASSWORD__/$web_password/g" \
           > $FLOID_CLOUD_SERVER_CONFIG_FILE
       echo "Returning to original directory..."
       popd > /dev/null
       ;;

    O) keytool -list -v -keystore $FLOID_SERVER_KEY_DIRECTORY/$FLOID_WEB_JKS_FILE
       ;;

    P) clear
       echo "THIS IS NOT AUTO-IMPLEMENTED - YOU NEED TO INSERT USERS MANUALLY INTO THE DB SUCH AS:"
       echo "INSERT INTO \`floid_user\` (\`id\`, \`password\`, \`roles\`, \`userName\`)
                 VALUES (0, '\$2a\$10\$nPHzqyZQ4SACXv60KQv11OBc6m6zGCYFAYgamXGzTQ28MneX2/5g2', 'FLOID_ADMIN,FLOID_COMMANDER,FLOID_USER', 'floid');"
       ;;

    R) clear
       echo "Changing to key directory..."
       pushd $FLOID_SERVER_KEY_DIRECTORY > /dev/null
       echo -e "Enter Map Box Access Token for ->MAP<- : \c"
       read map_box_access_token
       sed 's/__GEN__/*** GENERATED FILE - DO NOT EDIT - DO NOT CHECK IN - EDIT TEMPLATE INSTEAD ***/g' \
        $FLOID_UI_APP_KEYS_DIRECTORY/keys_dart.template \
           | sed "s/__MAP_BOX_ACCESS_TOKEN__/$map_box_access_token/g" \
           > $FLOID_UI_APP_KEYS_DIRECTORY/keys.dart
       echo "Done."
       echo "Returning to original directory..."
       popd > /dev/null
       ;;

    S) echo "Setting up floid database (floid) and server user (floid-server)..."
       mysql -h localhost -u root -p < menu/create-floid-db.sql
       ;;

    U) echo "Exporting Floid Database (Full) from localhost..."
       mysqldump -h localhost -u root floid  > db/floid.sql
       ;;

    V) echo "Exporting Floid Database (Schema) from localhost..."
       mysqldump --no-data -h localhost -u root floid  > db/floid_schema.sql
       ;;

    W) echo "Importing Floid Database (Full) to localhost..."
       mysql -h localhost -u root < db/floid.sql
       ;;

    X) echo "Importing Floid Database (Schema) to localhost..."
       mysql -h localhost -u root < db/floid_schema.sql
       ;;

    Y) echo "TODO - IMPLEMENT EMPTY DB..."
       ;;

    1) echo "Pull Floid Messages"
       echo "Floid Id:"
       read floidId
       echo "Pull Floid Messages for Floid Id: $floidId - ** files will be in floid/db **"
       pushd db
       ./pull_messages.sh $floidId
       popd
       ;;

    7) echo "Into /opt/floid-server-elevations/elevations, pull SRTM data and split into (40,000) tiles..."
       pushd open-elevation-scripts
       sudo ./create-dataset.sh
       popd
       ;;

    q) popd
       exit
       ;;

  esac
  echo
done
