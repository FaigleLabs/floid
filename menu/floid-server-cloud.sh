#!/bin/bash
# Make working directory floid-server-cloud
pushd ../floid-server-cloud > /dev/null
clear
while true
do
  echo "       ______      _       __     __      __        ";
  echo "      / ____/___ _(_)___ _/ /__  / /___ _/ /_  _____";
  echo "     / /_  / __ \`/ / __ \`/ / _ \/ / __ \`/ __ \/ ___/";
  echo "    / __/ / /_/ / / /_/ / /  __/ / /_/ / /_/ (__  ) ";
  echo "   /_/    \__,_/_/\__, /_/\___/_/\__,_/_.___/____/  ";
  echo "                 /____/                             ";
  echo ""
  echo "> Floid Server Cloud:"
  echo "Floid Server SQL:"
  echo "  1. List SQL instances"
  echo "  2. Describe"
  echo "  3. See Operations"
  echo "  4. Open Shell (password: faiglelabs) - creates: 127.0.0.1:9470"
  echo "  5. Pull run data for floid id (first connect via 4 above) - creates [table]-[floidId].csv"
  echo "  6. Pull run data for floid and uuid (first connect via 4 above)- creates [table]-[floidId]-[floidUuid].csv"
  echo "  7. Create Instance"
  echo "  8. Set Password"
  echo "  9. Import Starter Data"
  echo "  0. Additional Operations"
  echo "Floid Server Pod:"
  echo "  a. Deploy Pod"
  echo "  b. See Pod Status"
  echo "  c. Get Events"
  echo "  d. Delete Pod"
  echo "  e. Create Service (Exposes http:8080 and https:8443)"
  echo "  f. Delete Service"
  echo "  g. Forward Tomcat Port to 8080"
  echo "  h. Shell (ash) to Floid Server node in Floid Server pod"
  echo '  i. Get faiglelabs-floid-server logs'
  echo "Cloud SQL Proxy Stuff:"
  echo "  k. Create Proxy User: No password, Restricted to cloudsqlproxy machine nameset: (https://cloud.google.com/sql/docs/mysql/create-manage-users)"
  echo "  l. Create SQL Proxy Service Account (gcloud): (https://cloud.google.com/sdk/gcloud/reference/iam/service-accounts/create)"
  echo "  m. Get SQL Proxy Service Account Policies"
  echo "  n. Describe SQL Proxy Service Account"
  echo "  o. Delete Proxy User"
  echo "Tomcat Alpine Docker Image:"
  echo "  p. Build Tomcat Alpine 9 Image  ** Be sure to first successfully run: Software --> Build All Projects"
  echo "  r. Push Tomcat Alpine 9 Image   ** Be sure to first successfully run: Server Cloud --> Build Tomcat Alpine 9 Image"
  echo "Server Maps and Elevation Data:"
  echo "  s. Create Cloud Storage Bucket: floid-server-maps"
  echo "  t. Create Cloud Storage Bucket: floid-server-elevations"
  echo "  u. Copy local /opt/floid-server-maps/maps to GCS Bucket: floid-server-maps"
  echo "  v. Copy local /opt/elevations/elevations to GCS Bucket: floid-server-elevations"
  echo "  w. Copy GCS Bucket: floid-server-maps to local /opt/floid-server-maps/maps"
  echo "  x. Copy GCS Bucket: floid-server-elevations to local /opt/floid-server-elevations/elevations"
  echo "Quit:"
  echo "  q. Quit"
  echo -e "Select: \c"
  read answer
  case "$answer" in

    1) echo 'gcloud sql instances list'
       gcloud sql instances list
       ;;

    2) echo 'gcloud sql instances describe faiglelabs-floid-server-sql'
       gcloud sql instances describe faiglelabs-floid-server-sql
       ;;

    3) echo 'gcloud sql operations list -i faiglelabs-floid-server-sql'
       gcloud sql operations list -i faiglelabs-floid-server-sql
       ;;

    4) echo 'gcloud beta sql connect faiglelabs-floid-server-sql --user=root --quiet'
       echo 'password: faiglelabs'
       echo 'creates: 127.0.0.1:9470'
       gcloud beta sql connect faiglelabs-floid-server-sql --user=root
       ;;

    5) echo
       echo "Pull info for floid id (first connect via 4 above) - creates [table]-[floidId].csv"
       echo -e 'Enter floidId or empty to cancel: \c'
       read floidId
       if [ "$floidId" = "" ]; then
          echo cancelled.
       else
         echo -e 'Enter password or space to cancel: \c'
         read password
         if [ "$password" = " " ]; then
            echo cancelled.
         else
           echo -e 'Local[l] or cloud[c]: \c'
           read location
           if [ "$location" = "l" ]; then
             port=3306
             directory="local"
           else
             port=9470
             directory="cloud"
           fi
           
           echo "creating output directory floid-server-cloud/private/data/$directory/$floidId..."
           mkdir -p "private/data/$directory/$floidId"
           
           echo "creating private/data/$directory/$floidId/floid-status-$floidId.csv..."
           echo "select distinct * from floid_status where floidId=$floidId order by std asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-status-$floidId.csv
           echo | cat ../menu/floid-status-headers.csv - private/data/$directory/$floidId/floid-status-$floidId.csv > private/data/$directory/$floidId/floid-status-w-headers-$floidId.csv

           echo "creating private/data/$directory/$floidId/floid-droid-status-$floidId.csv..."
           echo "select distinct * from floid_droid_status where floidId=$floidId order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-droid-status-$floidId.csv

           echo "creating private/data/$directory/$floidId/floid-model-status-$floidId.csv..."
           echo "select distinct * from floid_model_status where floidId=$floidId order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-model-status-$floidId.csv

           echo "creating private/data/$directory/$floidId/floid-model-parameters-$floidId.csv..."
           echo "select distinct * from floid_model_parameters where floidId=$floidId order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-model-parameters-$floidId.csv

           echo "creating private/data/$directory/$floidId/floid-droid-mission-instruction-status-message-$floidId.csv..."
           echo "select distinct * from floid_droid_mission_instruction_status_message where floidId=$floidId order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-droid-mission-instruction-status-message-$floidId.csv

           echo "creating private/data/$directory/$floidId/floid-debug-message-$floidId.csv..."
           echo "select distinct * from floid_debug_message where floidId=$floidId order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-debug-message-$floidId.csv

           echo "creating private/data/$directory/$floidId/floid-droid-photo-$floidId.csv..."
           echo "select distinct * from floid_droid_photo where floidId=$floidId order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-droid-photo-$floidId.csv

           echo "creating private/data/$directory/$floidId/floid-droid_video-frame-$floidId.csv..."
           echo "select distinct * from floid_droid_video_frame where floidId=$floidId order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-droid-video-frame-$floidId.csv

           echo "creating floid/droid map: "
           sed "s/__FLOID_ID__/$floidId/g" plot/floid-map.template.gnuplot | \
              sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
              gnuplot

           echo "creating servos plot: "
           sed "s/__FLOID_ID__/$floidId/g" plot/floid-servos-escs.template.gnuplot | \
              sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
              gnuplot

           echo "creating direction plot: "
           sed "s/__FLOID_ID__/$floidId/g" plot/floid-direction.template.gnuplot | \
              sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
              gnuplot

           echo "creating altitude plot: "
           sed "s/__FLOID_ID__/$floidId/g" plot/floid-altitude.template.gnuplot | \
              sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
              gnuplot

           echo "creating pitch-roll plot: "
           sed "s/__FLOID_ID__/$floidId/g" plot/floid-pitch-roll.template.gnuplot | \
             sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
             gnuplot

           echo "creating batteries plot: "
           sed "s/__FLOID_ID__/$floidId/g" plot/floid-batteries.template.gnuplot | \
             sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
             gnuplot

           echo "creating goal target orientation plot: "
           sed "s/__FLOID_ID__/$floidId/g" plot/floid-goal-target-orientation.template.gnuplot | \
              sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
              gnuplot

         fi
       fi
       ;;

    6) echo
       echo "Pull info for floid id and floidUuid (first connect via 4 above) - creates [table]-[floidId].csv"
       echo -e 'Enter floidId or empty to cancel: \c'
       read floidId
       if [ "$floidId" = "" ]; then
          echo cancelled.
       else
         echo -e 'Enter floidUuid or empty to cancel: \c'
         read floidUuid
         if [ "$floidUuid" = "" ]; then
            echo cancelled.
         else
           echo -e 'Enter password or empty to cancel: \c'
           read password
           if [ "$password" = "" ]; then
              echo cancelled.
           else
             echo -e 'Local[l] or cloud[c]: \c'
             read location
             if [ "$location" = "l" ]; then
               port=3306
               directory="local"
             else
               port=9470
               directory="cloud"
             fi

             echo "creating output directory floid-server-cloud/private/data/$directory/$floidId..."
             mkdir -p "private/data/$directory/$floidId"

             echo "creating private/data/$directory/$floidId/floid-status-$floidId-$floidUuid.csv..."
             echo "select distinct * from floid_status where floidId=$floidId and floidUuid='$floidUuid' order by std asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-status-$floidId-$floidUuid.csv
             echo | cat ../menu/floid-status-headers.csv - private/data/$directory/$floidId/floid-status-$floidId-$floidUuid.csv > private/data/$directory/$floidId/floid-status-w-headers-$floidId-$floidUuid.csv

             echo "creating private/data/$directory/$floidId/floid-droid-status-$floidId-$floidUuid.csv..."
             echo "select distinct * from floid_droid_status where floidId=$floidId and floidUuid='$floidUuid' order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-droid-status-$floidId-$floidUuid.csv

             echo "creating private/data/$directory/$floidId/floid-model-status-$floidId-$floidUuid.csv..."
             echo "select distinct * from floid_model_status where floidId=$floidId and floidUuid='$floidUuid' order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-model-status-$floidId-$floidUuid.csv

             echo "creating private/data/$directory/$floidId/floid-model-parameters-$floidId-$floidUuid.csv..."
             echo "select distinct * from floid_model_parameters where floidId=$floidId and floidUuid='$floidUuid' order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-model-parameters-$floidId-$floidUuid.csv

             echo "creating private/data/$directory/$floidId/floid-droid-mission-instruction-status-message-$floidId-$floidUuid.csv..."
             echo "select distinct * from floid_droid_mission_instruction_status_message where floidId=$floidId and floidUuid='$floidUuid' order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-droid-mission-instruction-status-message-$floidId-$floidUuid.csv

             echo "creating private/data/$directory/$floidId/floid-debug-message-$floidId-$floidUuid.csv..."
             echo "select distinct * from floid_debug_message where floidId=$floidId and floidUuid='$floidUuid' order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-debug-message-$floidId-$floidUuid.csv

             echo "creating private/data/$directory/$floidId/floid-droid-photo-$floidId-$floidUuid.csv..."
             echo "select distinct * from floid_droid_photo where floidId=$floidId and floidUuid='$floidUuid' order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-droid-photo-$floidId-$floidUuid.csv

             echo "creating private/data/$directory/$floidId/floid-droid_video-frame-$floidId-$floidUuid.csv..."
             echo "select distinct * from floid_droid_video_frame where floidId=$floidId and floidUuid='$floidUuid' order by timestamp asc" | mysql --host=127.0.0.1 --port=$port --user=root --password=$password floid | awk '{$1=$1}1' OFS=, | sed 's/\\0/0/g' | sed 's/\x01/1/g' > private/data/$directory/$floidId/floid-droid-video-frame-$floidId-$floidUuid.csv

             echo "creating floid/droid map: "
             sed "s/__FLOID_ID__/$floidId/g" plot/floid-uuid-map.template.gnuplot | \
                sed "s/__FLOID_UUID__/$floidUuid/g" | \
                sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
                gnuplot

             echo "creating servos plot: "
             sed "s/__FLOID_ID__/$floidId/g" plot/floid-uuid-servos-escs.template.gnuplot | \
                sed "s/__FLOID_UUID__/$floidUuid/g" | \
                sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
                gnuplot

             echo "creating direction plot: "
             sed "s/__FLOID_ID__/$floidId/g" plot/floid-uuid-direction.template.gnuplot | \
                sed "s/__FLOID_UUID__/$floidUuid/g" | \
                sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
                gnuplot

             echo "creating altitude plot: "
             sed "s/__FLOID_ID__/$floidId/g" plot/floid-uuid-altitude.template.gnuplot | \
                sed "s/__FLOID_UUID__/$floidUuid/g" | \
                sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
                gnuplot

             echo "creating pitch-roll plot: "
             sed "s/__FLOID_ID__/$floidId/g" plot/floid-uuid-pitch-roll.template.gnuplot | \
                sed "s/__FLOID_UUID__/$floidUuid/g" | \
                sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
                gnuplot

             echo "creating batteries plot: "
             sed "s/__FLOID_ID__/$floidId/g" plot/floid-uuid-batteries.template.gnuplot | \
                sed "s/__FLOID_UUID__/$floidUuid/g" | \
                sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
                gnuplot

             echo "creating goal target orientation plot: "
             sed "s/__FLOID_ID__/$floidId/g" plot/floid-uuid-goal-target-orientation.template.gnuplot | \
                sed "s/__FLOID_UUID__/$floidUuid/g" | \
                sed "s/__DIRECTORY__/private\/data\/$directory\/$floidId/g" | \
                gnuplot

           fi
         fi
       fi
       ;;

    7) echo 'Create Instance:'
       echo '  gcloud beta sql instances create faiglelabs-floid-server --activation-policy=ALWAYS --backup --tier=db-n1-standard-1 --region=us-east1 --gce-zone=us-east1-b --database-version=MYSQL_5_7 --storage-type="HDD" --storage-size="20GB"'
       echo -e "Sure? \c"
       read agree
       if [ "$agree" = "y" ]; then
          gcloud beta sql instances create faiglelabs-floid-server --activation-policy=ALWAYS --backup --tier=db-n1-standard-1 --region=us-east1 --gce-zone=us-east1-b --database-version=MYSQL_5_7 --storage-type="HDD" --storage-size="20GB"
       else
          echo cancelled.
       fi
       ;;

    8) echo 'Set Password:'
       echo '  gcloud sql instances set-root-password faiglelabs-floid-server-sql --password faiglelabs'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          gcloud sql instances set-root-password faiglelabs-floid-server-sql --password faiglelabs
       else
          echo cancelled.
       fi
       ;;

    9) echo 'Import Starter Data:'
       echo '  gcloud beta sql connect faiglelabs-floid-server-sql --user=root < floid.sql'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          gcloud beta sql connect faiglelabs-floid-server-sql --user=root < floid.sql
       else
          echo cancelled.
       fi
       ;;

    0) pushd ../menu > /dev/null
       ./floid-server-cloud-additional.sh
       popd > /dev/null
       ;;

    a) echo 'Deploy Pod:'
       echo '  kubectl create -f faiglelabs-podspec-floid-server.yaml'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          kubectl create -f faiglelabs-podspec-floid-server.yaml
       else
          echo cancelled.
       fi
       ;;

    b) echo 'Get Pod Status:'
       echo '  kubectl get -f faiglelabs-podspec-floid-server.yaml'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          kubectl get -f faiglelabs-podspec-floid-server.yaml
       else
          echo cancelled.
       fi
       ;;

    c) echo 'Get Events:'
       echo '  kubectl get events'
       kubectl get events
       ;;

    d) echo 'Delete Pod:'
       echo '  kubectl delete -f faiglelabs-podspec-floid-server.yaml'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          kubectl delete -f faiglelabs-podspec-floid-server.yaml
       else
          echo cancelled.
       fi
       ;;

    e) echo 'Create Service (Exposes http:8080 and https:8443):'
       echo '  kubectl create -f faiglelabs-servicespec-floid-server.yaml'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          kubectl create -f faiglelabs-servicespec-floid-server.yaml
       else
          echo cancelled.
       fi
       ;;

    f) echo 'Delete Service:'
       echo '  kubectl delete service faiglelabs-floid-server-service'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          kubectl delete service faiglelabs-floid-server-service
       else
          echo cancelled.
       fi
       ;;

    g) echo 'Forward Tomcat Port to 8080:'
       echo '  kubectl port-forward faiglelabs-floid-server 8080:8080'
       echo '  open http://localhost:8080'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          kubectl port-forward faiglelabs-floid-server 8080:8080
          open http://localhost:8080
       else
          echo cancelled.
       fi
       ;;

    h) echo 'Shell (ash) to Floid Server node in Floid Server pod:'
       echo '  kubectl exec -ti faiglelabs-floid-server -c faiglelabs-floid-server -- ash'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          kubectl exec -ti faiglelabs-floid-server -c faiglelabs-floid-server -- ash
       else
          echo cancelled.
       fi
       ;;

    i) echo 'Get faiglelabs-floid-server logs:'
       echo '  kubectl logs faiglelabs-floid-server faiglelabs-floid-server'
       kubectl logs faiglelabs-floid-server faiglelabs-floid-server
       ;;

    k) echo 'Create Proxy User: No password, Restricted to cloudsqlproxy machine nameset: (https://cloud.google.com/sql/docs/mysql/create-manage-users):'
       echo '  gcloud beta sql users create floid-server cloudsqlproxy~% --instance=faiglelabs-floid-server-sql'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          gcloud beta sql users create floid-server cloudsqlproxy~% --instance=faiglelabs-floid-server-sql
       else
          echo cancelled.
       fi
       ;;

    l) echo 'Create gcloud Service Account for sql proxy: (https://cloud.google.com/sdk/gcloud/reference/iam/service-accounts/create):'
       echo '  gcloud iam service-accounts create faiglelabs-floid-server-svc --display_name "FaigleLabs Floid Server Service Account"'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          gcloud iam service-accounts create faiglelabs-floid-server-svc --display_name "FaigleLabs Floid Server Service Account"
       else
          echo cancelled.
       fi
       ;;

    m) echo 'Get SQL Proxy Service Account Policies:'
       echo '  gcloud iam service-accounts get-iam-policy faiglelabs-floid-server-svc@faiglelabs-25d6e.iam.gserviceaccount.com'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          gcloud iam service-accounts get-iam-policy faiglelabs-floid-server-svc@faiglelabs-25d6e.iam.gserviceaccount.com
       else
          echo cancelled.
       fi
       ;;

    n) echo 'Describe SQL Proxy Service Account:'
       echo '  gcloud iam service-accounts describe faiglelabs-floid-server-svc@faiglelabs-25d6e.iam.gserviceaccount.com'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          gcloud iam service-accounts describe faiglelabs-floid-server-svc@faiglelabs-25d6e.iam.gserviceaccount.com
       else
          echo cancelled.
       fi
       ;;

    o) echo 'Delete Proxy User:'
       echo '  gcloud beta sql users delete floid-server cloudsqlproxy~% --instance=faiglelabs-floid-server-sql'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          gcloud beta sql users delete floid-server cloudsqlproxy~% --instance=faiglelabs-floid-server-sql
       else
          echo cancelled.
       fi
       ;;

    p) echo 'Build Tomcat Alpine 9 Image:  ** Be sure to first successfully run: Software --> Build All Projects'
       echo ' Note: Starter Docker file is here: https://github.com/docker-library/tomcat/blob/075a394e34f7329415e87ad5f0d77d46aac24876/9.0/jre8-alpine/Dockerfile'
       echo '  cp ../floid-server/build/libs/floid-server-1.0-SNAPSHOT.war webapps/ROOT.war'
       echo '  docker --debug build --platform linux/amd64 -t gcr.io/faiglelabs-25d6e/floid-server-tomcat:1.0 .'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          cp ../floid-server/build/libs/floid-server-1.0-SNAPSHOT.war webapps/ROOT.war
          docker --debug build --platform linux/amd64 -t gcr.io/faiglelabs-25d6e/floid-server-tomcat:1.0 .
       else
          echo cancelled.
       fi
       ;;

    r) echo 'Push Tomcat Alpine 9 Image:  ** Be sure to first successfully run: Server Cloud --> Build Tomcat Alpine 9 Image'
       echo ' Note: Starter Docker file is here: https://github.com/docker-library/tomcat/blob/075a394e34f7329415e87ad5f0d77d46aac24876/9.0/jre8-alpine/Dockerfile'
       echo '  gcloud auth configure-docker'
       echo '  docker push gcr.io/faiglelabs-25d6e/floid-server-tomcat:1.0'
       echo -e 'Sure? \c'
       read agree
       if [ "$agree" = "y" ]; then
          gcloud auth configure-docker
          docker push gcr.io/faiglelabs-25d6e/floid-server-tomcat:1.0
       else
          echo cancelled.
       fi
       ;;
    s) echo 'Create Cloud Storage Bucket: floid-server-maps'
       echo 'gsutil mb -l us-east1 gs://floid-server-maps/'
       echo -e 'Sure? \c'
             read agree
             if [ "$agree" = "y" ]; then
                gsutil mb -l us-east1 gs://floid-server-maps/
             else
                echo cancelled.
             fi
      ;;
    t) echo 'Create Cloud Storage Bucket: floid-server-elevations'
       echo 'gsutil mb -l us-east1 gs://floid-server-elevations/'
       echo -e 'Sure? \c'
             read agree
             if [ "$agree" = "y" ]; then
                gsutil mb -l us-east1 gs://floid-server-elevations/
             else
                echo cancelled.
             fi
      ;;
    u) echo 'Copy local /opt/floid-server-maps/maps to GCS Bucket: floid-server-maps'
       echo 'THIS MAY NOT BE A SYMLINK - MUST BE ACTUAL DIRECTORY'
       echo 'gsutil cp -r /opt/floid-server-maps/maps gs://floid-server-maps'
       echo -e 'Sure? \c'
                    read agree
                    if [ "$agree" = "y" ]; then
                       gsutil cp -r /opt/floid-server-maps/maps gs://floid-server-maps
                    else
                       echo cancelled.
                    fi
        ;;
    v) echo 'Copy local /opt/floid-server-elevations/elevations to GCS Bucket: floid-server-elevations'
       echo 'THIS MAY NOT BE A SYMLINK - MUST BE ACTUAL DIRECTORY'
       echo 'gsutil cp -r /opt/floid-server-elevations/elevations gs://floid-server-elevations'
       echo -e 'Sure? \c'
        read agree
        if [ "$agree" = "y" ]; then
           gsutil cp -r /opt/floid-server-elevations/elevations gs://floid-server-elevations
        else
           echo cancelled.
        fi
        ;;
    w) echo 'Copy GCS Bucket: floid-server-maps to local /opt/floid-server-maps'
       echo 'gsutil cp -r gs://floid-server-maps /opt/floid-server-maps'
       echo -e 'Sure? \c'
                    read agree
                    if [ "$agree" = "y" ]; then
                       gsutil cp -r gs://floid-server-maps /opt/floid-server-maps
                    else
                       echo cancelled.
                    fi
        ;;
    x) echo 'Copy GCS Bucket: floid-server-elevations to local /opt/floid-server-elevations'
       echo 'gsutil cp -r gs://floid-server-elevations /opt/floid-server-elevations'
       echo -e 'Sure? \c'
        read agree
        if [ "$agree" = "y" ]; then
           gsutil cp -r gs://floid-server-elevations /opt/floid-server-elevations
        else
           echo cancelled.
        fi
        ;;
    q) popd
       exit
       ;;
  esac
  echo
done
popd
