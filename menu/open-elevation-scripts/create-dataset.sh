#!/usr/bin/env bash

OUTDIR="/opt/floid-server-elevations/elevations"
if [ ! -e $OUTDIR ] ; then
    echo $OUTDIR does not exist!
fi

CURDIR=$(pwd)

set -eu

cd $OUTDIR
$CURDIR/download-srtm-data.sh
mv SRTM_NE_250m_TIF/SRTM_NE_250m.tif .
rm -rf SRTM_NE_250m_TIF
mv SRTM_SE_250m_TIF/SRTM_SE_250m.tif .
rm -rf SRTM_SE_250m_TIF
mv SRTM_W_250m_TIF/SRTM_W_250m.tif .
rm -rf SRTM_W_250m_TIF
$CURDIR/create-tiles.sh SRTM_NE_250m.tif 100 100
$CURDIR/create-tiles.sh SRTM_SE_250m.tif 100 100
$CURDIR/create-tiles.sh SRTM_W_250m.tif 100 200

rm -rf SRTM_NE_250m.tif SRTM_SE_250m.tif SRTM_W_250m.tif

cd $CURDIR
