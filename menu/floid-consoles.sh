#!/bin/bash
while true
do
clear
  echo "______ _       _     _   _   _  ___  _   _   _____           _                 ";
  echo "|  ___| |     (_)   | | | | | |/ _ \| | | | /  ___|         | |                ";
  echo "| |_  | | ___  _  __| | | | | / /_\ \ | | | \ \`--. _   _ ___| |_ ___ _ __ ___  ";
  echo "|  _| | |/ _ \| |/ _\` | | | | |  _  | | | |  \`--. \ | | / __| __/ _ \ '_ \` _ \ ";
  echo "| |   | | (_) | | (_| | | |_| | | | \ \_/ / /\__/ / |_| \__ \ ||  __/ | | | | |";
  echo "\_|   |_|\___/|_|\__,_|  \___/\_| |_/\___/  \____/ \__, |___/\__\___|_| |_| |_|";
  echo "                                                    __/ |                      ";
  echo "                                                   |___/                       ";
  echo "                   ______      _       __     __      __        ";
  echo "                  / ____/___ _(_)___ _/ /__  / /___ _/ /_  _____";
  echo "                 / /_  / __ \`/ / __ \`/ / _ \/ / __ \`/ __ \/ ___/";
  echo "                / __/ / /_/ / / /_/ / /  __/ / /_/ / /_/ (__  ) ";
  echo "               /_/    \__,_/_/\__, /_/\___/_/\__,_/_.___/____/  ";
  echo "                             /____/                             ";
  echo ""
  echo "> Floid Consoles:"
  echo "  1. Google Play"
  echo "  2. Google Analytics"
  echo "  3. Google Compute Engine"
  echo "  4. Google Container Engine"
  echo "  5. Google Cloud SQL"
  echo "  6. Google Cloud Storage"
  echo "  7. Google Account"
  echo "  8. StackDriver Metrics"
  echo "  9. Floid Server Pod"
  echo "Quit:"
  echo "  q. Quit"
  echo -e "Select: \c"
  read answer
  case "$answer" in
    1) open https://play.google.com/apps/publish
       clear ;;
    2) open https://analytics.google.com
       clear ;;
    3) open https://console.cloud.google.com/compute
       clear ;;
    4) open https://console.cloud.google.com/kubernetes
       clear;;
    5) open https://console.cloud.google.com/sql
       clear ;;
    6) open https://console.cloud.google.com/storage
           clear ;;
    7) open https://myaccount.google.com
       clear ;;
    8) open https://app.google.stackdriver.com/metrics-explorer?project=faiglelabs-25d6e
       clear;;
    9) open https://console.cloud.google.com/kubernetes/pod/us-west1-b/faiglelabs-www/default/faiglelabs-floid-server?project=faiglelabs-25d6e
       clear;;
    q) exit ;;
  esac
  echo
done

