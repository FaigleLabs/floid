
1;95;0c#!/bin/bash
# Make working directory one above this one
pushd .. > /dev/null
clear
while true
do
  echo "Floid Software:"
  echo "---------------"
  echo "README:                0. View README"
  echo "Build:                 1. Build All Projects"
  echo "Local Server:          2. Run Local Floid Server (https://localhost:8443/)"
  echo "                       @. Run Local Floid Server (with debug output) (https://localhost:8443/)"
  echo "                       !. Run already built Local Floid Server (https://localhost:8443/)"
  echo "                       3. Run Local Floid Server - DEBUG - port 5005"
  echo "                       %. Run already built Local Floid Server - DEBUG - port 5005"
  echo "                       4. Kill Local Floid Server"
  echo "Controller:            5. Upload Floid Controller (Manual Port)"
  echo "                       6. Upload Floid Controller (Auto Port)"
  echo "IMU:                   7. Upload Floid IMU (Manual Port)"
  echo "                       8. Upload Floid IMU (Auto Port)"
  echo "Debug Serial Reader:   9. Run Floid Controller Serial Debug Reader"
  echo "Mission Controller:    A. Assemble Floid App (All Variants)"
  echo "                       B. Assemble Floid App (All Variants) - Signed for release <- Play Store"
  echo "                          To release, first:"
  echo "                            * Bump 'version' number in floid-app/build.gradle and floid-app/app/build.gradle"
  echo "                            * Bump 'versionCode' by 1 in floid-app/app/build.gradle"
  echo "                            * Set 'versionName' to same as 'version' in floid-app/app/build.gradle"
  echo "                          Then this command produces: floid-app/app/build/outputs/apk/full/release/app-full-release.apk"
  echo "                          Go to Floid Consoles -> Google Play to release"
  echo "                       C. Install Debug Floid App Locally"
  echo "                       D. Build Floid App as App Bundle (All Variants) Signed for Release"
  echo "Mission Commander:     a. Listen to local websocket (8080) as SU"
  echo "                       b. Run Flutter Web on Chrome"
  echo "                       c. Build Signed Release Flutter Android App (APK)"
  echo "                       d. Build Signed Release Flutter Android App Split Per ABI (APK)   <-- 3 APK FILES - USE FOR PLAY STORE RELEASE"
  echo "                          To release, first:"
  echo "                            * Bump 'version' in floid_ui_app/pubspec.yaml"
  echo "                            * Run flutter clean (below)"
  echo "                          Then this command produces: 3 APKs in floid_ui_app/android/build/app/outputs/flutter-apk/"
  echo "                          Go to Floid Consoles -> Google Play to release"
  echo "                       e. Build Signed Release Flutter Android App Bundle (AAB)"
  echo "                       f. Clean Flutter (flutter clean)"
  echo "                       g. Upgrade Flutter (flutter upgrade)"
  echo "                       h. Upgrade Flutter pubspec.yaml dependencies (flutter pub upgrade)"
  echo "                       i. Run App Standalone on Device"
  echo "                       j. Start Android Emulator (TODO)"
  echo "                       k. Build Flutter Web"
  echo "Dependencies:          l. Print Gradle Dependencies"
  echo "                       m. Print Flutter Dependencies"
  echo "Misc:                  V. Show Versions"
  echo "                       Y. Visualize (with Gource)"
  echo "                       Z. Count lines"
  echo "Quit:                  q. Quit"
  echo -e "Select: \c"
  # shellcheck disable=SC2162
  read answer
  case "$answer" in
    0) mdless README.md
       ;;

    1) echo './gradlew clean build'
       ./gradlew clean build
       ;;

    2) echo 'Run Local Floid Server:  (https://localhost:8443/)'
       ./gradlew clean build --no-daemon
       cd floid-server
       java -jar build/libs/floid-server-1.0-SNAPSHOT.war
       cd ..
#       open https://localhost:8443/
       ;;

    @) echo 'Run Local Floid Server (With Debug Output):  (https://localhost:8443/)'
       ./gradlew clean build --no-daemon --debug
       cd floid-server
       java -jar build/libs/floid-server-1.0-SNAPSHOT.war
       cd ..
#       open https://localhost:8443/
       ;;

    !) echo 'Run already built Local Floid Server:  (https://localhost:8443/)'
       cd floid-server
       java -jar build/libs/floid-server-1.0-SNAPSHOT.war
       cd ..
#       open https://localhost:8443/
       ;;

    3) echo 'Run Local Floid Server - REMOTE DEBUG - port 5005'
      ./gradlew clean build --no-daemon
       cd floid-server
       java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -jar build/libs/floid-server-1.0-SNAPSHOT.war
       cd ..
#       open https://localhost:8443/
       ;;

    %) echo 'Run already built Local Floid Server - REMOTE DEBUG - port 5005'
       cd floid-server
       java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -jar build/libs/floid-server-1.0-SNAPSHOT.war
       cd ..
#       open https://localhost:8443/
       ;;

    4) echo 'Stop Local Floid Server:'
       # shellcheck disable=SC2009
       kill -9 `ps x | grep -i java | grep gradle | awk '{print $1}'`
       ;;

    5) echo -e "Floid Controller Upload Port (Manual Port): \c"
       # shellcheck disable=SC2162
       read port
       ./gradlew floid-controller:uploadController -Pupload_port="$port"
       ;;

    6) echo -e "Floid Controller Upload Port (Auto Port)..."
       ./gradlew floid-controller:uploadController -Pupload_port="`ls /dev/tty.usbserial*`"
       ;;

    7) echo -e "Floid IMU Upload Port (Manual Port): \c"
       # shellcheck disable=SC2162
       read port
       ./gradlew floid-imu:uploadImu -Pupload_port="$port"
       ;;

    8) echo -e "Floid IMU Upload Port (Auto Port)..."
       ./gradlew floid-imu:uploadImu -Pupload_port="`ls /dev/tty.usbmodem*`"
       ;;

    9) echo 'Run Floid Controller Serial Debug Reader'
       ./gradlew floid-debug-serial:assemble
       pushd floid-debug-serial/build/distributions > /dev/null
       unzip floid-debug-serial-1.0-SNAPSHOT.zip
       pushd floid-debug-serial-1.0-SNAPSHOT > /dev/null
       sudo bin/floid-debug-serial
       popd > /dev/null
       popd > /dev/null
       ;;


    a) echo 'Listen to local websocket (8080) as SU'
       echo 'sudo wscat -c http://localhost:8080/handler/websocket'
       sudo wscat -c http://localhost:8080/handler/websocket
       ;;

    b) echo 'Run Flutter Web on Chrome'
       echo 'pushd floid_ui_app'
       echo 'flutter run -d chrome'
       echo 'popd'
       pushd floid_ui_app
       flutter run -d chrome
       popd
       ;;

    c) echo 'Build Signed Release Flutter Android App (APK)'
       echo 'pushd floid_ui_app'
       echo 'flutter build apk --release'
       echo 'popd'
       pushd floid_ui_app
       flutter build apk --release
       popd
       ;;

    d) echo 'Build Signed Release Flutter Android App Split Per ABI (APK)'
       echo 'pushd floid_ui_app'
       echo 'flutter build apk --split-per-abi --release'
       echo 'popd'
       pushd floid_ui_app
       flutter build apk --split-per-abi --release
       popd
       ;;

    e) echo 'Build Signed Release Flutter Android App Bundle (AAB)'
       echo 'pushd floid_ui_app'
       echo 'flutter build appbundle --release'
       echo 'popd'
       pushd floid_ui_app
       flutter build appbundle --release
       popd
       ;;


    f) echo 'Clean Flutter (flutter clean)'
       echo 'pushd floid_ui_app'
       echo 'flutter clean'
       echo 'popd'
       pushd floid_ui_app
       flutter clean
       popd
       ;;

    g) echo 'Upgrade Flutter (flutter upgrade)'
       echo 'flutter update'
       flutter upgrade
       ;;

    h) echo 'Upgrade Flutter pubspec.yaml dependencies (flutter pub upgrade)'
       echo 'pushd floid_ui_app'
       echo 'flutter pub upgrade'
       echo 'popd'
       pushd floid_ui_app
       flutter pub upgrade
       popd
       ;;
    i) echo 'Run App Standalone on Device'
       echo 'pushd floid_ui_app'
       echo 'flutter run'
       echo 'popd'
       pushd floid_ui_app
       flutter run
       popd
       ;;

    j) echo 'Start Android Emulator (TODO)'
       ;;

    k) echo 'Build Flutter Web'
       echo 'pushd floid_ui_app'
       echo 'flutter build web'
       echo 'popd'
       pushd floid_ui_app
       flutter build web
       popd
       ;;

    l) echo 'Print Gradle Dependencies'
       pushd floid_floid_app
       ./gradlew dependencies
       popd

       pushd floid-server-types
       ./gradlew dependencies
       popd

       pushd floid-server
       ./gradlew dependencies
       popd

       pushd floid_ui_app
       ./gradlew dependencies
       popd
       ;;

    m) echo 'Print Flutter Dependencies'
       echo 'flutter pub deps -- --style=compact'
       echo 'popd'
       pushd floid_ui_app
       flutter pub deps -- --style=compact
       popd
       ;;

    A) echo './gradlew clean build floid-app:app:assemble'
       ./gradlew clean build floid-app:app:assemble
       ;;

    B) echo './gradlew clean build floid-app:app:assembleRelease'
       ./gradlew clean build floid-app:app:assembleRelease
       ;;

    C) echo './gradlew clean build floid-app:app:installFullDebug'
       ./gradlew clean build floid-app:app:installFullDebug
       ;;

    D) echo './gradlew clean build floid-app:app:bundleRelease'
       ./gradlew clean build floid-app:app:bundleRelease
       ;;

    V) echo
       echo "Gradle:"
       grep 'gradleVersion' `find . -name "build.gradle"`
       grep 'distributionUrl' `find . -name "gradle-wrapper.properties"`
       ls -l `find . -name "gradle-wrapper.jar"`
       cksum `find . -name "gradle-wrapper.jar"`
       echo
       echo "Android Build Tools:"
       grep 'com.android.tools.build' `find . -name "build.gradle"`
       echo
       echo "Androod SDK:"
       grep 'compileSdk\|minSdkVersion\|targetSdkVersion' `find . -name "build.gradle"`
       echo
       echo "JVM:"
       grep 'jvmTarget\|sourceCompatibility\|targetCompatibility' `find . -name "build.gradle"`
       echo
       echo "Kotlin:"
       grep 'ext.kotlin' `find . -name "build.gradle"`
       echo
       echo "Dart:"
       fgrep -r  -i "sdk:" `find . -name "pubspec.yaml"`
       echo
       echo "Flutter:"
       flutter --version
       echo
       echo "JDK:"
       java --version
       echo
       echo "IDEA IntelliJ:"
       cat /Applications/IntelliJ\ IDEA.app/Contents/Resources/build.txt
       echo
       echo "Floid App (Mission Controller) Dependencies:"
       ./gradlew floid-app:app:dependencies
       echo
       ;;

    Y) gource --high-dpi -s .1 --bloom-multiplier .1
       ;;

    Z) menu/count-lines.sh
       ;;

    q) popd
       exit
       ;;

  esac
  echo
done
