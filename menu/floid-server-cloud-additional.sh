#!/bin/bash
clear
while true
do
  echo "______ _       _     _   _   _  ___  _   _   _____           _                 ";
  echo "|  ___| |     (_)   | | | | | |/ _ \| | | | /  ___|         | |                ";
  echo "| |_  | | ___  _  __| | | | | / /_\ \ | | | \ \`--. _   _ ___| |_ ___ _ __ ___  ";
  echo "|  _| | |/ _ \| |/ _\` | | | | |  _  | | | |  \`--. \ | | / __| __/ _ \ '_ \` _ \ ";
  echo "| |   | | (_) | | (_| | | |_| | | | \ \_/ / /\__/ / |_| \__ \ ||  __/ | | | | |";
  echo "\_|   |_|\___/|_|\__,_|  \___/\_| |_/\___/  \____/ \__, |___/\__\___|_| |_| |_|";
  echo "                                                    __/ |                      ";
  echo "                                                   |___/                       ";
  echo "                   ______      _       __     __      __        ";
  echo "                  / ____/___ _(_)___ _/ /__  / /___ _/ /_  _____";
  echo "                 / /_  / __ \`/ / __ \`/ / _ \/ / __ \`/ __ \/ ___/";
  echo "                / __/ / /_/ / / /_/ / /  __/ / /_/ / /_/ (__  ) ";
  echo "               /_/    \__,_/_/\__, /_/\___/_/\__,_/_.___/____/  ";
  echo "                             /____/                             ";
  echo ""
  echo "> Floid Server Cloud Operations:"
  echo "Google Cloud Operations:"
  echo "  1. Authenticate"
  echo "  2. List Projects"
  echo "  3. Get Services"
  echo "  4. List GCR Containers - TODO - MISSING"
  echo "  5. List Local Docker Images"
  echo "  6. Get cluster credentials - sets up kubectl to point to cluster"
  echo "Roles and Policies:"
  echo "  7. List Project Grantable Roles"
  echo "  8. Retrieve Project IAM Policy"
  echo "  9. Upload Project IAM Policy"
  echo "Quit:"
  echo "  q. Quit"
  echo -e "Select: \c"
  read answer
  case "$answer" in
    1) echo 'Authenticate:'
       echo 'gcloud auth application-default login'
       echo -e "Sure? \c"
       read agree
       if [ "$agree" = "y" ]; then
           gcloud auth application-default login
       else
          echo cancelled.
       fi
       ;;
    2) echo 'List Projects:'
       echo '  gcloud projects list'
       gcloud projects list
       ;;
    3) echo 'Get Services:'
       echo '  kubectl get services'
       kubectl get services
       ;;
    5) echo 'List Local Docker Images:'
       echo '  docker images'
       docker images
       ;;

    7) echo 'List Project Grantable Roles:'
       echo '  gcloud iam list-grantable-roles //cloudresourcemanager.googleapis.com/projects/faiglelabs-25d6e'
       echo -e "Sure? \c"
       read agree
       if [ "$agree" = "y" ]; then
          gcloud iam list-grantable-roles //cloudresourcemanager.googleapis.com/projects/faiglelabs-25d6e
       else
          echo cancelled.
       fi
       ;;
    8) echo 'Retrieve Project IAM Policy:'
       echo '  gcloud projects get-iam-policy faiglelabs-25d6e --format json > ../faiglelabs-kubernetes-floid-server/iam.json'
       echo -e "Sure? \c"
       read agree
       if [ "$agree" = "y" ]; then
          gcloud projects get-iam-policy faiglelabs-25d6e --format json > iam.json
       else
          echo cancelled.
       fi
       ;;
    9) echo 'Upload Project IAM Policy:'
       echo '  gcloud projects set-iam-policy faiglelabs-25d6e --format json < ../faiglelabs-kubernetes-floid-server/iam.json'
       echo -e "Sure? \c"
       read agree
       if [ "$agree" = "y" ]; then
          gcloud projects set-iam-policy faiglelabs-25d6e --format json < iam.json
       else
          echo cancelled.
       fi
       ;;
     6) echo 'Get cluster credentials - sets up kubectl to point to cluster'
        echo '  gcloud container clusters get-credentials faiglelabs-cluster-1 --region=us-east1-b'
        echo -e "Sure? \c"
        read agree
        if [ "$agree" = "y" ]; then
          gcloud container clusters get-credentials faiglelabs-cluster-1 --region=us-east1-b
        fi
       ;;
    q) exit
       ;;
  esac
  echo
done

