#!/bin/bash
clear
while true
do
  echo ""
  echo "> Floid Docs:"
  echo "  1. Startup"
  echo "  2. Test Plan"
  echo "  3. Physics Testing"
  echo "  4. Physics model"
  echo "  5. Parts List"
  echo "> Other:"
  echo "  a. Fix database move"
  echo "Quit:"
  echo "  q. Quit"
  echo -e "Select: \c"
  read answer
  case "$answer" in

    1) clear
       cat ../etc/docs/Floid_Startup.txt
       ;;

    2) open ../etc/docs/FloidSystemTestPlan.xls
       clear ;;

    3) clear
       cat ../etc/docs/Floid_Physics_Testing.txt
       ;;

    4) open ../etc/docs/physics_model_1_16.svg
       clear ;;

    5) open ../etc/docs/Floid9Parts.ods
       clear ;;

    a) clear
       cat ../etc/docs/How-To-Fix-Database-Hibernate-MOVE-ME.txt
       ;;
    q) exit ;;
  esac
  echo
done

