-- MySQL dump 10.13  Distrib 5.5.15, for osx10.6 (i386)
--
-- Host: localhost    Database: floid
-- ------------------------------------------------------
-- Server version	5.5.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DroidMissionList`
--

DROP TABLE IF EXISTS `DroidMissionList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DroidMissionList` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `missionId` bigint(20) DEFAULT NULL,
  `missionName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FloidPin`
--

DROP TABLE IF EXISTS `FloidPin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FloidPin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pinHeight` double DEFAULT NULL,
  `pinHeightAuto` bit(1) NOT NULL,
  `pinLat` double DEFAULT NULL,
  `pinLng` double DEFAULT NULL,
  `pinName` varchar(255) DEFAULT NULL,
  `pinSet_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7C1383D140031EB9` (`pinSet_id`),
  CONSTRAINT `FK7C1383D140031EB9` FOREIGN KEY (`pinSet_id`) REFERENCES `FloidPinSet` (`id`),
  CONSTRAINT `FKoq1emu4788w61e0lqhhff2bgg` FOREIGN KEY (`pinSet_id`) REFERENCES `FloidPinSet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=252733 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FloidPinSet`
--

DROP TABLE IF EXISTS `FloidPinSet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FloidPinSet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pinSetName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243745 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FloidPinSetList`
--

DROP TABLE IF EXISTS `FloidPinSetList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FloidPinSetList` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pinSetId` bigint(20) DEFAULT NULL,
  `pinSetName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `droidinstruction`
--

DROP TABLE IF EXISTS `droidinstruction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `droidinstruction` (
  `DTYPE` varchar(255) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `missionInstructionNumber` bigint(20) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `requiredAltimeterGoodCount` int(11) DEFAULT NULL,
  `requiredAltitudeDistance` double DEFAULT NULL,
  `requiredGPSGoodCount` int(11) DEFAULT NULL,
  `requiredHeadingDistance` double DEFAULT NULL,
  `requiredXYDistance` double DEFAULT NULL,
  `bay` int(11) DEFAULT NULL,
  `debugValue` int(11) DEFAULT NULL,
  `deviceIndex` int(11) DEFAULT NULL,
  `delayTime` bigint(20) DEFAULT NULL,
  `pinName` varchar(255) DEFAULT NULL,
  `usePin` bit(1) DEFAULT NULL,
  `usePinHeight` bit(1) DEFAULT NULL,
  `x` double DEFAULT NULL,
  `y` double DEFAULT NULL,
  `z` double DEFAULT NULL,
  `flyToHome` bit(1) DEFAULT NULL,
  `absolute` bit(1) DEFAULT NULL,
  `hoverTime` bigint(20) DEFAULT NULL,
  `device` int(11) DEFAULT NULL,
  `pan` double DEFAULT NULL,
  `tilt` double DEFAULT NULL,
  `useAngle` bit(1) DEFAULT NULL,
  `delay` int(11) DEFAULT NULL,
  `firstPinIsHome` bit(1) DEFAULT NULL,
  `homePinName` varchar(255) DEFAULT NULL,
  `followMode` bit(1) DEFAULT NULL,
  `heading` double DEFAULT NULL,
  `testDuration` bigint(20) DEFAULT NULL,
  `missionName` varchar(255) DEFAULT NULL,
  `nextInstructionNumber` int(11) DEFAULT NULL,
  `topLevelMission` bit(1) DEFAULT NULL,
  `droidMission_id` bigint(20) DEFAULT NULL,
  `alert` bit(1) DEFAULT NULL,
  `mode` varchar(255) DEFAULT NULL,
  `tts` varchar(2048) DEFAULT NULL,
  `pitch` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `fileName` varchar(1024) DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `speakerRepeat` int(11) DEFAULT NULL,
  `droidParameters` varchar(2048) DEFAULT NULL,
  `resetDroidParameters` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK28238492856DF3C7` (`droidMission_id`),
  KEY `missionInstructionNumber` (`missionInstructionNumber`),
  CONSTRAINT `FK28238492856DF3C7` FOREIGN KEY (`droidMission_id`) REFERENCES `DroidInstruction` (`id`),
  CONSTRAINT `FK84v5ludt39mgu6mqxf3gqwvo1` FOREIGN KEY (`droidMission_id`) REFERENCES `DroidInstruction` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243851 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floidgpspoint`
--

DROP TABLE IF EXISTS `floidgpspoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floidgpspoint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `altitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `statusTime` bigint(20) DEFAULT NULL,
  `floidId` int(11) NOT NULL,
  `floidUuid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `floidId` (`floidId`),
  KEY `floidUuid` (`floidUuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floidid`
--

DROP TABLE IF EXISTS `floidid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floidid` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `floidId` bigint(20) NOT NULL,
  `floidIdCreateTime` bigint(20) NOT NULL,
  `floidIdUpdateTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `floidId` (`floidId`),
  KEY `floidIdCreateTime` (`floidIdCreateTime`),
  KEY `floidIdUpdateTime` (`floidIdUpdateTime`)
) ENGINE=InnoDB AUTO_INCREMENT=244015 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floidmessage`
--

DROP TABLE IF EXISTS `floidmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floidmessage` (
  `DTYPE` varchar(255) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `floidId` int(11) NOT NULL,
  `floidMessageNumber` bigint(20) DEFAULT NULL,
  `floidUuid` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `debugMessage` varchar(2048) DEFAULT NULL,
  `instructionId` bigint(20) DEFAULT NULL,
  `instructionStatus` int(11) DEFAULT NULL,
  `instructionStatusDisplayString` varchar(255) DEFAULT NULL,
  `instructionType` varchar(255) DEFAULT NULL,
  `messageType` varchar(255) DEFAULT NULL,
  `missionId` bigint(20) DEFAULT NULL,
  `missionName` varchar(255) DEFAULT NULL,
  `batteryLevel` double DEFAULT NULL,
  `batteryRawLevel` int(11) DEFAULT NULL,
  `batteryRawScale` int(11) DEFAULT NULL,
  `batteryVoltage` double DEFAULT NULL,
  `currentMode` int(11) DEFAULT NULL,
  `currentStatus` int(11) DEFAULT NULL,
  `droidStarted` bit(1) DEFAULT NULL,
  `floidConnected` bit(1) DEFAULT NULL,
  `floidError` bit(1) DEFAULT NULL,
  `gpsAccuracy` float DEFAULT NULL,
  `gpsAltitude` double DEFAULT NULL,
  `gpsBearing` float DEFAULT NULL,
  `gpsHasAccuracy` bit(1) DEFAULT NULL,
  `gpsHasAltitude` bit(1) DEFAULT NULL,
  `gpsHasBearing` bit(1) DEFAULT NULL,
  `gpsLatitude` double DEFAULT NULL,
  `gpsLongitude` double DEFAULT NULL,
  `gpsProvider` varchar(255) DEFAULT NULL,
  `gpsSpeed` float DEFAULT NULL,
  `gpsStatusTime` bigint(20) DEFAULT NULL,
  `helisStarted` bit(1) DEFAULT NULL,
  `homePositionAcquired` bit(1) DEFAULT NULL,
  `homePositionHeading` double DEFAULT NULL,
  `homePositionX` double DEFAULT NULL,
  `homePositionY` double DEFAULT NULL,
  `homePositionZ` double DEFAULT NULL,
  `liftedOff` bit(1) DEFAULT NULL,
  `missionNumber` int(11) DEFAULT NULL,
  `parachuteDeployed` bit(1) DEFAULT NULL,
  `photoNumber` int(11) DEFAULT NULL,
  `photoStrobe` bit(1) DEFAULT NULL,
  `photoStrobeDelay` int(11) DEFAULT NULL,
  `serverConnected` bit(1) DEFAULT NULL,
  `serverError` bit(1) DEFAULT NULL,
  `statusNumber` bigint(20) DEFAULT NULL,
  `statusTime` bigint(20) DEFAULT NULL,
  `takingPhoto` bit(1) DEFAULT NULL,
  `takingVideo` bit(1) DEFAULT NULL,
  `videoNumber` int(11) DEFAULT NULL,
  `aa` double DEFAULT NULL,
  `ac` int(11) DEFAULT NULL,
  `ad` double DEFAULT NULL,
  `ai` bit(1) DEFAULT NULL,
  `av` bit(1) DEFAULT NULL,
  `b0` double DEFAULT NULL,
  `b1` double DEFAULT NULL,
  `b2` double DEFAULT NULL,
  `b3` double DEFAULT NULL,
  `ba` double DEFAULT NULL,
  `bm` double DEFAULT NULL,
  `c0` double DEFAULT NULL,
  `c1` double DEFAULT NULL,
  `c2` double DEFAULT NULL,
  `c3` double DEFAULT NULL,
  `ca` double DEFAULT NULL,
  `da` bit(1) DEFAULT NULL,
  `dc` bit(1) DEFAULT NULL,
  `dd` bit(1) DEFAULT NULL,
  `de` bit(1) DEFAULT NULL,
  `df` bit(1) DEFAULT NULL,
  `dg` bit(1) DEFAULT NULL,
  `dh` bit(1) DEFAULT NULL,
  `dl` bit(1) DEFAULT NULL,
  `dp` bit(1) DEFAULT NULL,
  `dv` bit(1) DEFAULT NULL,
  `e0` double DEFAULT NULL,
  `e0o` bit(1) DEFAULT NULL,
  `e1` double DEFAULT NULL,
  `e1o` bit(1) DEFAULT NULL,
  `e2` double DEFAULT NULL,
  `e2o` bit(1) DEFAULT NULL,
  `e3` double DEFAULT NULL,
  `e3o` bit(1) DEFAULT NULL,
  `ep0` int(11) DEFAULT NULL,
  `ep1` int(11) DEFAULT NULL,
  `ep2` int(11) DEFAULT NULL,
  `ep3` int(11) DEFAULT NULL,
  `et` int(11) DEFAULT NULL,
  `ffm` int(11) DEFAULT NULL,
  `fm` int(11) DEFAULT NULL,
  `fmf` bit(1) DEFAULT NULL,
  `ga` double DEFAULT NULL,
  `ghg` bit(1) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `gif` bit(1) DEFAULT NULL,
  `gld` bit(1) DEFAULT NULL,
  `glo` bit(1) DEFAULT NULL,
  `gs` int(11) DEFAULT NULL,
  `gt` int(11) DEFAULT NULL,
  `gx` double DEFAULT NULL,
  `gy` double DEFAULT NULL,
  `gz` double DEFAULT NULL,
  `hsm` bit(1) DEFAULT NULL,
  `jf` int(11) DEFAULT NULL,
  `jg` int(11) DEFAULT NULL,
  `jh` double DEFAULT NULL,
  `jm` double DEFAULT NULL,
  `jmf` double DEFAULT NULL,
  `jr` int(11) DEFAULT NULL,
  `js` int(11) DEFAULT NULL,
  `jt` int(11) DEFAULT NULL,
  `jx` double DEFAULT NULL,
  `jxf` double DEFAULT NULL,
  `jy` double DEFAULT NULL,
  `jyf` double DEFAULT NULL,
  `jz` double DEFAULT NULL,
  `ka` double DEFAULT NULL,
  `kc` double DEFAULT NULL,
  `kd` double DEFAULT NULL,
  `kv` double DEFAULT NULL,
  `kw` double DEFAULT NULL,
  `lc` int(11) DEFAULT NULL,
  `lma` double DEFAULT NULL,
  `lmc` bit(1) DEFAULT NULL,
  `lmt` int(11) DEFAULT NULL,
  `lr` double DEFAULT NULL,
  `ma` double DEFAULT NULL,
  `md` double DEFAULT NULL,
  `mg` double DEFAULT NULL,
  `mk` double DEFAULT NULL,
  `mo` double DEFAULT NULL,
  `mt` double DEFAULT NULL,
  `oav` double DEFAULT NULL,
  `oaw` double DEFAULT NULL,
  `ohv` double DEFAULT NULL,
  `ohw` double DEFAULT NULL,
  `oop` double DEFAULT NULL,
  `ooq` double DEFAULT NULL,
  `oor` double DEFAULT NULL,
  `oos` double DEFAULT NULL,
  `opv` double DEFAULT NULL,
  `opw` double DEFAULT NULL,
  `orv` double DEFAULT NULL,
  `orw` double DEFAULT NULL,
  `ov` double DEFAULT NULL,
  `par` int(11) DEFAULT NULL,
  `pd` bit(1) DEFAULT NULL,
  `pe` int(11) DEFAULT NULL,
  `pg` int(11) DEFAULT NULL,
  `ph` double DEFAULT NULL,
  `phs` double DEFAULT NULL,
  `phv` double DEFAULT NULL,
  `pp` double DEFAULT NULL,
  `pps` double DEFAULT NULL,
  `ppv` double DEFAULT NULL,
  `pr` double DEFAULT NULL,
  `prs` double DEFAULT NULL,
  `prv` double DEFAULT NULL,
  `pt` double DEFAULT NULL,
  `pv` bit(1) DEFAULT NULL,
  `pz` double DEFAULT NULL,
  `pzs` double DEFAULT NULL,
  `pzv` double DEFAULT NULL,
  `s00` double DEFAULT NULL,
  `s01` double DEFAULT NULL,
  `s02` double DEFAULT NULL,
  `s0o` bit(1) DEFAULT NULL,
  `s10` double DEFAULT NULL,
  `s11` double DEFAULT NULL,
  `s12` double DEFAULT NULL,
  `s1o` bit(1) DEFAULT NULL,
  `s20` double DEFAULT NULL,
  `s21` double DEFAULT NULL,
  `s22` double DEFAULT NULL,
  `s2o` bit(1) DEFAULT NULL,
  `s30` double DEFAULT NULL,
  `s31` double DEFAULT NULL,
  `s32` double DEFAULT NULL,
  `s3o` bit(1) DEFAULT NULL,
  `sn` int(11) DEFAULT NULL,
  `sr` int(11) DEFAULT NULL,
  `st` int(11) DEFAULT NULL,
  `std` bigint(20) DEFAULT NULL,
  `t0m` int(11) DEFAULT NULL,
  `t0o` bit(1) DEFAULT NULL,
  `t0p` int(11) DEFAULT NULL,
  `t0t` int(11) DEFAULT NULL,
  `t0x` double DEFAULT NULL,
  `t0y` double DEFAULT NULL,
  `t0z` double DEFAULT NULL,
  `t1m` int(11) DEFAULT NULL,
  `t1o` bit(1) DEFAULT NULL,
  `t1p` int(11) DEFAULT NULL,
  `t1t` int(11) DEFAULT NULL,
  `t1x` double DEFAULT NULL,
  `t1y` double DEFAULT NULL,
  `t1z` double DEFAULT NULL,
  `t2m` int(11) DEFAULT NULL,
  `t2o` bit(1) DEFAULT NULL,
  `t2p` int(11) DEFAULT NULL,
  `t2t` int(11) DEFAULT NULL,
  `t2x` double DEFAULT NULL,
  `t2y` double DEFAULT NULL,
  `t2z` double DEFAULT NULL,
  `t3m` int(11) DEFAULT NULL,
  `t3o` bit(1) DEFAULT NULL,
  `t3p` int(11) DEFAULT NULL,
  `t3t` int(11) DEFAULT NULL,
  `t3x` double DEFAULT NULL,
  `t3y` double DEFAULT NULL,
  `t3z` double DEFAULT NULL,
  `va` bit(1) DEFAULT NULL,
  `vb` bit(1) DEFAULT NULL,
  `vc` bit(1) DEFAULT NULL,
  `vd` bit(1) DEFAULT NULL,
  `ve` bit(1) DEFAULT NULL,
  `vg` bit(1) DEFAULT NULL,
  `vh` bit(1) DEFAULT NULL,
  `vl` bit(1) DEFAULT NULL,
  `vm` bit(1) DEFAULT NULL,
  `vp` bit(1) DEFAULT NULL,
  `vs` bit(1) DEFAULT NULL,
  `vt` bit(1) DEFAULT NULL,
  `vx` bit(1) DEFAULT NULL,
  `vy` bit(1) DEFAULT NULL,
  `y0` bit(1) DEFAULT NULL,
  `y1` bit(1) DEFAULT NULL,
  `y2` bit(1) DEFAULT NULL,
  `y3` bit(1) DEFAULT NULL,
  `yg0` int(11) DEFAULT NULL,
  `yg1` int(11) DEFAULT NULL,
  `yg2` int(11) DEFAULT NULL,
  `yg3` int(11) DEFAULT NULL,
  `yn0` int(11) DEFAULT NULL,
  `yn1` int(11) DEFAULT NULL,
  `yn2` int(11) DEFAULT NULL,
  `yn3` int(11) DEFAULT NULL,
  `calculatedBladePitchH0S0` double DEFAULT NULL,
  `calculatedBladePitchH0S1` double DEFAULT NULL,
  `calculatedBladePitchH0S2` double DEFAULT NULL,
  `calculatedBladePitchH1S0` double DEFAULT NULL,
  `calculatedBladePitchH1S1` double DEFAULT NULL,
  `calculatedBladePitchH1S2` double DEFAULT NULL,
  `calculatedBladePitchH2S0` double DEFAULT NULL,
  `calculatedBladePitchH2S1` double DEFAULT NULL,
  `calculatedBladePitchH2S2` double DEFAULT NULL,
  `calculatedBladePitchH3S0` double DEFAULT NULL,
  `calculatedBladePitchH3S1` double DEFAULT NULL,
  `calculatedBladePitchH3S2` double DEFAULT NULL,
  `calculatedCollectiveBladePitchH0` double DEFAULT NULL,
  `calculatedCollectiveBladePitchH1` double DEFAULT NULL,
  `calculatedCollectiveBladePitchH2` double DEFAULT NULL,
  `calculatedCollectiveBladePitchH3` double DEFAULT NULL,
  `calculatedCyclicBladePitchH0S0` double DEFAULT NULL,
  `calculatedCyclicBladePitchH0S1` double DEFAULT NULL,
  `calculatedCyclicBladePitchH0S2` double DEFAULT NULL,
  `calculatedCyclicBladePitchH1S0` double DEFAULT NULL,
  `calculatedCyclicBladePitchH1S1` double DEFAULT NULL,
  `calculatedCyclicBladePitchH1S2` double DEFAULT NULL,
  `calculatedCyclicBladePitchH2S0` double DEFAULT NULL,
  `calculatedCyclicBladePitchH2S1` double DEFAULT NULL,
  `calculatedCyclicBladePitchH2S2` double DEFAULT NULL,
  `calculatedCyclicBladePitchH3S0` double DEFAULT NULL,
  `calculatedCyclicBladePitchH3S1` double DEFAULT NULL,
  `calculatedCyclicBladePitchH3S2` double DEFAULT NULL,
  `calculatedServoDegreesH0S0` double DEFAULT NULL,
  `calculatedServoDegreesH0S1` double DEFAULT NULL,
  `calculatedServoDegreesH0S2` double DEFAULT NULL,
  `calculatedServoDegreesH1S0` double DEFAULT NULL,
  `calculatedServoDegreesH1S1` double DEFAULT NULL,
  `calculatedServoDegreesH1S2` double DEFAULT NULL,
  `calculatedServoDegreesH2S0` double DEFAULT NULL,
  `calculatedServoDegreesH2S1` double DEFAULT NULL,
  `calculatedServoDegreesH2S2` double DEFAULT NULL,
  `calculatedServoDegreesH3S0` double DEFAULT NULL,
  `calculatedServoDegreesH3S1` double DEFAULT NULL,
  `calculatedServoDegreesH3S2` double DEFAULT NULL,
  `calculatedServoPulseH0S0` int(11) DEFAULT NULL,
  `calculatedServoPulseH0S1` int(11) DEFAULT NULL,
  `calculatedServoPulseH0S2` int(11) DEFAULT NULL,
  `calculatedServoPulseH1S0` int(11) DEFAULT NULL,
  `calculatedServoPulseH1S1` int(11) DEFAULT NULL,
  `calculatedServoPulseH1S2` int(11) DEFAULT NULL,
  `calculatedServoPulseH2S0` int(11) DEFAULT NULL,
  `calculatedServoPulseH2S1` int(11) DEFAULT NULL,
  `calculatedServoPulseH2S2` int(11) DEFAULT NULL,
  `calculatedServoPulseH3S0` int(11) DEFAULT NULL,
  `calculatedServoPulseH3S1` int(11) DEFAULT NULL,
  `calculatedServoPulseH3S2` int(11) DEFAULT NULL,
  `collectiveDeltaAltitudeH0` double DEFAULT NULL,
  `collectiveDeltaAltitudeH1` double DEFAULT NULL,
  `collectiveDeltaAltitudeH2` double DEFAULT NULL,
  `collectiveDeltaAltitudeH3` double DEFAULT NULL,
  `collectiveDeltaHeadingH0` double DEFAULT NULL,
  `collectiveDeltaHeadingH1` double DEFAULT NULL,
  `collectiveDeltaHeadingH2` double DEFAULT NULL,
  `collectiveDeltaHeadingH3` double DEFAULT NULL,
  `collectiveDeltaOrientationH0` double DEFAULT NULL,
  `collectiveDeltaOrientationH1` double DEFAULT NULL,
  `collectiveDeltaOrientationH2` double DEFAULT NULL,
  `collectiveDeltaOrientationH3` double DEFAULT NULL,
  `collectiveDeltaTotalH0` double DEFAULT NULL,
  `collectiveDeltaTotalH1` double DEFAULT NULL,
  `collectiveDeltaTotalH2` double DEFAULT NULL,
  `collectiveDeltaTotalH3` double DEFAULT NULL,
  `collectiveValueH0` double DEFAULT NULL,
  `collectiveValueH1` double DEFAULT NULL,
  `collectiveValueH2` double DEFAULT NULL,
  `collectiveValueH3` double DEFAULT NULL,
  `cyclicHeading` double DEFAULT NULL,
  `cyclicValueH0S0` double DEFAULT NULL,
  `cyclicValueH0S1` double DEFAULT NULL,
  `cyclicValueH0S2` double DEFAULT NULL,
  `cyclicValueH1S0` double DEFAULT NULL,
  `cyclicValueH1S1` double DEFAULT NULL,
  `cyclicValueH1S2` double DEFAULT NULL,
  `cyclicValueH2S0` double DEFAULT NULL,
  `cyclicValueH2S1` double DEFAULT NULL,
  `cyclicValueH2S2` double DEFAULT NULL,
  `cyclicValueH3S0` double DEFAULT NULL,
  `cyclicValueH3S1` double DEFAULT NULL,
  `cyclicValueH3S2` double DEFAULT NULL,
  `altitudeTargetVelocityMaxValue` double DEFAULT NULL,
  `altitudeToTargetMaxValue` double DEFAULT NULL,
  `altitudeToTargetMinValue` double DEFAULT NULL,
  `attackAngleMaxDistanceValue` double DEFAULT NULL,
  `attackAngleMinDistanceValue` double DEFAULT NULL,
  `attackAngleValue` double DEFAULT NULL,
  `bladesHigh` double DEFAULT NULL,
  `bladesLow` double DEFAULT NULL,
  `bladesZero` double DEFAULT NULL,
  `collectiveDefault` double DEFAULT NULL,
  `collectiveMax` double DEFAULT NULL,
  `collectiveMin` double DEFAULT NULL,
  `cyclicDefault` double DEFAULT NULL,
  `cyclicHeadingAlpha` double DEFAULT NULL,
  `cyclicRange` double DEFAULT NULL,
  `distanceTargetVelocityMaxValue` double DEFAULT NULL,
  `distanceToTargetMaxValue` double DEFAULT NULL,
  `distanceToTargetMinValue` double DEFAULT NULL,
  `escHighValue` double DEFAULT NULL,
  `escLowValue` double DEFAULT NULL,
  `escPulseMax` int(11) DEFAULT NULL,
  `escPulseMin` int(11) DEFAULT NULL,
  `escType` int(11) DEFAULT NULL,
  `h0S0High` double DEFAULT NULL,
  `h0S0Low` double DEFAULT NULL,
  `h0S0Zero` double DEFAULT NULL,
  `h0S1High` double DEFAULT NULL,
  `h0S1Low` double DEFAULT NULL,
  `h0S1Zero` double DEFAULT NULL,
  `h0S2High` double DEFAULT NULL,
  `h0S2Low` double DEFAULT NULL,
  `h0S2Zero` double DEFAULT NULL,
  `h1S0High` double DEFAULT NULL,
  `h1S0Low` double DEFAULT NULL,
  `h1S0Zero` double DEFAULT NULL,
  `h1S1High` double DEFAULT NULL,
  `h1S1Low` double DEFAULT NULL,
  `h1S1Zero` double DEFAULT NULL,
  `h1S2High` double DEFAULT NULL,
  `h1S2Low` double DEFAULT NULL,
  `h1S2Zero` double DEFAULT NULL,
  `h2S0High` double DEFAULT NULL,
  `h2S0Low` double DEFAULT NULL,
  `h2S0Zero` double DEFAULT NULL,
  `h2S1High` double DEFAULT NULL,
  `h2S1Low` double DEFAULT NULL,
  `h2S1Zero` double DEFAULT NULL,
  `h2S2High` double DEFAULT NULL,
  `h2S2Low` double DEFAULT NULL,
  `h2S2Zero` double DEFAULT NULL,
  `h3S0High` double DEFAULT NULL,
  `h3S0Low` double DEFAULT NULL,
  `h3S0Zero` double DEFAULT NULL,
  `h3S1High` double DEFAULT NULL,
  `h3S1Low` double DEFAULT NULL,
  `h3S1Zero` double DEFAULT NULL,
  `h3S2High` double DEFAULT NULL,
  `h3S2Low` double DEFAULT NULL,
  `h3S2Zero` double DEFAULT NULL,
  `headingDeltaMaxValue` double DEFAULT NULL,
  `headingDeltaMinValue` double DEFAULT NULL,
  `headingTargetVelocityMaxValue` double DEFAULT NULL,
  `heliStartupModeNumberSteps` int(11) DEFAULT NULL,
  `heliStartupModeStepTick` int(11) DEFAULT NULL,
  `landModeTimeRequiredAtMinAltitude` int(11) DEFAULT NULL,
  `liftOffTargetAltitudeDeltaValue` double DEFAULT NULL,
  `orientationMinDistanceValue` double DEFAULT NULL,
  `pitchDeltaMaxValue` double DEFAULT NULL,
  `pitchDeltaMinValue` double DEFAULT NULL,
  `pitchTargetVelocityMaxValue` double DEFAULT NULL,
  `rollDeltaMaxValue` double DEFAULT NULL,
  `rollDeltaMinValue` double DEFAULT NULL,
  `rollTargetVelocityMaxValue` double DEFAULT NULL,
  `servoDegreeMax` double DEFAULT NULL,
  `servoDegreeMin` double DEFAULT NULL,
  `servoOffsetLeft0` double DEFAULT NULL,
  `servoOffsetPitch0` double DEFAULT NULL,
  `servoOffsetRight0` double DEFAULT NULL,
  `servoPulseMax` int(11) DEFAULT NULL,
  `servoPulseMin` int(11) DEFAULT NULL,
  `servoSignLeft` int(11) DEFAULT NULL,
  `servoSignPitch` int(11) DEFAULT NULL,
  `servoSignRight` int(11) DEFAULT NULL,
  `startMode` int(11) DEFAULT NULL,
  `targetAltitudeVelocityAlpha` double DEFAULT NULL,
  `targetHeadingVelocityAlpha` double DEFAULT NULL,
  `targetPitchVelocityAlpha` double DEFAULT NULL,
  `targetRollVelocityAlpha` double DEFAULT NULL,
  `targetVelocityFullSpeed` double DEFAULT NULL,
  `targetVelocityKeepStill` double DEFAULT NULL,
  `dba` bit(1) DEFAULT NULL,
  `dgp` bit(1) DEFAULT NULL,
  `dpy` bit(1) DEFAULT NULL,
  `floidModelEscCollectiveCalcMidpoint` double DEFAULT NULL,
  `floidModelEscCollectiveHighValue` double DEFAULT NULL,
  `floidModelEscCollectiveLowValue` double DEFAULT NULL,
  `floidModelEscCollectiveMidValue` double DEFAULT NULL,
  `altitude` double DEFAULT NULL,
  `imageExtension` varchar(255) DEFAULT NULL,
  `imageName` varchar(255) DEFAULT NULL,
  `imageType` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `pan` double DEFAULT NULL,
  `tilt` double DEFAULT NULL,
  `rm` tinyint(4) DEFAULT NULL,
  `tm` tinyint(4) DEFAULT NULL,
  `rotationMode` int(11) DEFAULT '0',
  `timestamp` bigint(20) DEFAULT '0',
  `servoOffsetLeft1` double DEFAULT NULL,
  `servoOffsetLeft2` double DEFAULT NULL,
  `servoOffsetLeft3` double DEFAULT NULL,
  `servoOffsetPitch1` double DEFAULT NULL,
  `servoOffsetPitch2` double DEFAULT NULL,
  `servoOffsetPitch3` double DEFAULT NULL,
  `servoOffsetRight1` double DEFAULT NULL,
  `servoOffsetRight2` double DEFAULT NULL,
  `servoOffsetRight3` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `floidId` (`floidId`),
  KEY `floidUuid` (`floidUuid`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=252732 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floiduser`
--

DROP TABLE IF EXISTS `floiduser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floiduser` (
  `id` bigint(20) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `roles` varchar(4096) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userName` (`userName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floiduuid`
--

DROP TABLE IF EXISTS `floiduuid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floiduuid` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `FloidId` bigint(20) DEFAULT NULL,
  `floidUuid` varchar(255) DEFAULT NULL,
  `floidUuidCreateTime` bigint(20) NOT NULL,
  `floidUuidUpdateTime` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `floidId` (`FloidId`),
  KEY `floidUuid` (`floidUuid`),
  KEY `floidUuidCreateTime` (`floidUuidCreateTime`),
  KEY `floidUuidUpdateTime` (`floidUuidUpdateTime`)
) ENGINE=InnoDB AUTO_INCREMENT=251755 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-13 21:15:52
