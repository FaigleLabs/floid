SET @OutputNamePrefix := 'FloidModelParameters_';
SET @OutputNameExtension := '.csv';
SELECT @floidId;
SELECT @directory;
SELECT @OutputNamePrefix;
SELECT @OutputNameExtension;
SET @OutputName := CONCAT(@directory, '/', @OutputNamePrefix, @floidId, @OutputNameExtension);
SELECT @OutputName;

set @q1 := concat(
        'SELECT
        "DTYPE","id","floidId","floidMessageNumber","floidUuid","type","altitudeTargetVelocityMaxValue","altitudeToTargetMaxValue","altitudeToTargetMinValue","attackAngleMaxDistanceValue","attackAngleMinDistanceValue","attackAngleValue","bladesHigh","bladesLow","bladesZero","collectiveDefault","collectiveMax","collectiveMin","cyclicDefault","cyclicHeadingAlpha","cyclicRange","distanceTargetVelocityMaxValue","distanceToTargetMaxValue","distanceToTargetMinValue","escHighValue","escLowValue","escPulseMax","escPulseMin","escType","h0S0High","h0S0Low","h0S0Zero","h0S1High","h0S1Low","h0S1Zero","h0S2High","h0S2Low","h0S2Zero","h1S0High","h1S0Low","h1S0Zero","h1S1High","h1S1Low","h1S1Zero","h1S2High","h1S2Low","h1S2Zero","h2S0High","h2S0Low","h2S0Zero","h2S1High","h2S1Low","h2S1Zero","h2S2High","h2S2Low","h2S2Zero","h3S0High","h3S0Low","h3S0Zero","h3S1High","h3S1Low","h3S1Zero","h3S2High","h3S2Low","h3S2Zero","headingDeltaMaxValue","headingDeltaMinValue","headingTargetVelocityMaxValue","heliStartupModeNumberSteps","heliStartupModeStepTick","landModeTimeRequiredAtMinAltitude","liftOffTargetAltitudeDeltaValue","orientationMinDistanceValue","pitchDeltaMaxValue","pitchDeltaMinValue","pitchTargetVelocityMaxValue","rollDeltaMaxValue","rollDeltaMinValue","rollTargetVelocityMaxValue","servoDegreeMax","servoDegreeMin","servoOffsetLeft0","servoOffsetPitch0","servoOffsetRight0","servoPulseMax","servoPulseMin","servoSignLeft","servoSignPitch","servoSignRight","startMode","targetAltitudeVelocityAlpha","targetHeadingVelocityAlpha","targetPitchVelocityAlpha","targetRollVelocityAlpha","targetVelocityFullSpeed","targetVelocityKeepStill","rotationMode","servoOffsetLeft1","servoOffsetLeft2","servoOffsetLeft3","servoOffsetPitch1","servoOffsetPitch2","servoOffsetPitch3","servoOffsetRight1","servoOffsetRight2","servoOffsetRight3","timestamp","velocityDeltaCyclicAlphaScale","accelerationMultiplierScale"
        UNION ALL
        SELECT
        DTYPE,id,floidId,floidMessageNumber,floidUuid,type,altitudeTargetVelocityMaxValue,altitudeToTargetMaxValue,altitudeToTargetMinValue,attackAngleMaxDistanceValue,attackAngleMinDistanceValue,attackAngleValue,bladesHigh,bladesLow,bladesZero,collectiveDefault,collectiveMax,collectiveMin,cyclicDefault,cyclicHeadingAlpha,cyclicRange,distanceTargetVelocityMaxValue,distanceToTargetMaxValue,distanceToTargetMinValue,escHighValue,escLowValue,escPulseMax,escPulseMin,escType,h0S0High,h0S0Low,h0S0Zero,h0S1High,h0S1Low,h0S1Zero,h0S2High,h0S2Low,h0S2Zero,h1S0High,h1S0Low,h1S0Zero,h1S1High,h1S1Low,h1S1Zero,h1S2High,h1S2Low,h1S2Zero,h2S0High,h2S0Low,h2S0Zero,h2S1High,h2S1Low,h2S1Zero,h2S2High,h2S2Low,h2S2Zero,h3S0High,h3S0Low,h3S0Zero,h3S1High,h3S1Low,h3S1Zero,h3S2High,h3S2Low,h3S2Zero,headingDeltaMaxValue,headingDeltaMinValue,headingTargetVelocityMaxValue,heliStartupModeNumberSteps,heliStartupModeStepTick,landModeTimeRequiredAtMinAltitude,liftOffTargetAltitudeDeltaValue,orientationMinDistanceValue,pitchDeltaMaxValue,pitchDeltaMinValue,pitchTargetVelocityMaxValue,rollDeltaMaxValue,rollDeltaMinValue,rollTargetVelocityMaxValue,servoDegreeMax,servoDegreeMin,servoOffsetLeft0,servoOffsetPitch0,servoOffsetRight0,servoPulseMax,servoPulseMin,servoSignLeft,servoSignPitch,servoSignRight,startMode,targetAltitudeVelocityAlpha,targetHeadingVelocityAlpha,targetPitchVelocityAlpha,targetRollVelocityAlpha,targetVelocityFullSpeed,targetVelocityKeepStill,rotationMode,servoOffsetLeft1,servoOffsetLeft2,servoOffsetLeft3,servoOffsetPitch1,servoOffsetPitch2,servoOffsetPitch3,servoOffsetRight1,servoOffsetRight2,servoOffsetRight3,timestamp,velocityDeltaCyclicAlphaScale,accelerationMultiplierScale
        INTO OUTFILE \'',
        @OutputName,
        '\' FIELDS TERMINATED BY ","
        ENCLOSED BY "\\""
        LINES TERMINATED BY "\\n"
        from FloidMessage
        where DTYPE="FloidModelParameters" and floidId=@floidId;');

SELECT @q1;
prepare s1 from @q1;
execute s1;deallocate prepare s1;

