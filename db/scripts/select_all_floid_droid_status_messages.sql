SET @OutputNamePrefix := 'FloidDroidStatus_';
SET @OutputNameExtension := '.csv';
SELECT @floidId;
SELECT @directory;
SELECT @OutputNamePrefix;
SELECT @OutputNameExtension;
SET @OutputName := CONCAT(@directory, '/', @OutputNamePrefix, @floidId, @OutputNameExtension);
SELECT @OutputName;

set @q1 := concat(
'SELECT
"DTYPE","id","floidId","floidMessageNumber","floidUuid","type","batteryLevel","batteryRawLevel","batteryRawScale","batteryVoltage","currentMode","currentStatus","droidStarted","floidConnected","floidError","gpsAccuracy","gpsAltitude","gpsBearing","gpsHasAccuracy","gpsHasAltitude","gpsHasBearing","gpsLatitude","gpsLongitude","gpsProvider","gpsSpeed","gpsStatusTime","helisStarted","homePositionAcquired","homePositionHeading","homePositionX","homePositionY","homePositionZ","liftedOff","missionNumber","parachuteDeployed","photoNumber","photoStrobe","photoStrobeDelay","serverConnected","serverError","statusNumber","statusTime","takingPhoto","takingVideo","videoNumber","timestamp"
UNION ALL
SELECT
DTYPE,id,floidId,floidMessageNumber,floidUuid,type,batteryLevel,batteryRawLevel,batteryRawScale,batteryVoltage,currentMode,currentStatus,droidStarted,floidConnected,floidError,gpsAccuracy,gpsAltitude,gpsBearing,gpsHasAccuracy,gpsHasAltitude,gpsHasBearing,gpsLatitude,gpsLongitude,gpsProvider,gpsSpeed,gpsStatusTime,helisStarted,homePositionAcquired,homePositionHeading,homePositionX,homePositionY,homePositionZ,liftedOff,missionNumber,parachuteDeployed,photoNumber,photoStrobe,photoStrobeDelay,serverConnected,serverError,statusNumber,statusTime,takingPhoto,takingVideo,videoNumber,timestamp
INTO OUTFILE \'',
@OutputName,
'\' FIELDS TERMINATED BY ","
ENCLOSED BY "\\""
LINES TERMINATED BY "\\n"
from FloidMessage
where DTYPE="FloidDroidStatus" and floidId=@floidId;');

SELECT @q1;
prepare s1 from @q1;
execute s1;deallocate prepare s1;

