SET @OutputNamePrefix := 'FloidStatus_';
SET @OutputNameExtension := '.csv';
SELECT @floidId;
SELECT @directory;
SELECT @OutputNamePrefix;
SELECT @OutputNameExtension;
SET @OutputName := CONCAT(@directory, '/', @OutputNamePrefix, @floidId, @OutputNameExtension);
SELECT @OutputName;

set @q1 := concat(
'SELECT
"DTYPE","id","floidId","floidMessageNumber","floidUuid","type","aa","ac","ad","ai","av","b0","b1","b2","b3","ba","bm","c0","c1","c2","c3","ca","da","dc","dd","de","df","dg","dh","dl","dp","dv","e0","e0o","e1","e1o","e2","e2o","e3","e3o","ep0","ep1","ep2","ep3","et","ffm","fm","fmf","ga","ghg","gid","gif","gld","glo","gs","gt","gx","gy","gz","hsm","jf","jg","jh","jm","jmf","jr","js","jt","jx","jxf","jy","jyf","jz","ka","kc","kd","kv","kw","lc","lma","lmc","lmt","lr","ma","md","mg","mk","mo","mt","oav","oaw","ohv","ohw","oop","ooq","oor","oos","opv","opw","orv","orw","ov","par","pd","pe","pg","ph","phs","phv","pp","pps","ppv","pr","prs","prv","pt","pv","pz","pzs","pzv","s00","s01","s02","s0o","s10","s11","s12","s1o","s20","s21","s22","s2o","s30","s31","s32","s3o","sn","sr","st","std","t0m","t0o","t0p","t0t","t0x","t0y","t0z","t1m","t1o","t1p","t1t","t1x","t1y","t1z","t2m","t2o","t2p","t2t","t2x","t2y","t2z","t3m","t3o","t3p","t3t","t3x","t3y","t3z","va","vb","vc","vd","ve","vg","vh","vl","vm","vp","vs","vt","vx","vy","y0","y1","y2","y3","yg0","yg1","yg2","yg3","yn0","yn1","yn2","yn3","rm","tm","timestamp"
UNION ALL
SELECT
DTYPE,id,floidId,floidMessageNumber,floidUuid,type,aa,ac,ad,ai,av,b0,b1,b2,b3,ba,bm,c0,c1,c2,c3,ca,da,dc,dd,de,df,dg,dh,dl,dp,dv,e0,e0o,e1,e1o,e2,e2o,e3,e3o,ep0,ep1,ep2,ep3,et,ffm,fm,fmf,ga,ghg,gid,gif,gld,glo,gs,gt,gx,gy,gz,hsm,jf,jg,jh,jm,jmf,jr,js,jt,jx,jxf,jy,jyf,jz,ka,kc,kd,kv,kw,lc,lma,lmc,lmt,lr,ma,md,mg,mk,mo,mt,oav,oaw,ohv,ohw,oop,ooq,oor,oos,opv,opw,orv,orw,ov,par,pd,pe,pg,ph,phs,phv,pp,pps,ppv,pr,prs,prv,pt,pv,pz,pzs,pzv,s00,s01,s02,s0o,s10,s11,s12,s1o,s20,s21,s22,s2o,s30,s31,s32,s3o,sn,sr,st,std,t0m,t0o,t0p,t0t,t0x,t0y,t0z,t1m,t1o,t1p,t1t,t1x,t1y,t1z,t2m,t2o,t2p,t2t,t2x,t2y,t2z,t3m,t3o,t3p,t3t,t3x,t3y,t3z,va,vb,vc,vd,ve,vg,vh,vl,vm,vp,vs,vt,vx,vy,y0,y1,y2,y3,yg0,yg1,yg2,yg3,yn0,yn1,yn2,yn3,rm,tm,timestamp
INTO OUTFILE \'',
@OutputName,
'\' FIELDS TERMINATED BY ","
ENCLOSED BY "\\""
LINES TERMINATED BY "\\n"
from FloidMessage
where DTYPE="FloidStatus" and floidId=@floidId;');

SELECT @q1;
prepare s1 from @q1;
execute s1;deallocate prepare s1;

