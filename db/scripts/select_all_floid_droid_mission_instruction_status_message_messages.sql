SET @OutputNamePrefix := 'FloidDroidMissionInstructionStatusMessage_';
SET @OutputNameExtension := '.csv';
SELECT @floidId;
SELECT @directory;
SELECT @OutputNamePrefix;
SELECT @OutputNameExtension;
SET @OutputName := CONCAT(@directory, '/', @OutputNamePrefix, @floidId, @OutputNameExtension);
SELECT @OutputName;

set @q1 := concat(
'SELECT
"DTYPE","id","floidId","floidMessageNumber","floidUuid","type","instructionId","instructionStatus","instructionStatusDisplayString","instructionType","messageType","missionId","missionName","timestamp"
UNION ALL
SELECT
DTYPE,id,floidId,floidMessageNumber,floidUuid,type,instructionId,instructionStatus,instructionStatusDisplayString,instructionType,messageType,missionId,missionName,timestamp
INTO OUTFILE \'',
@OutputName,
'\' FIELDS TERMINATED BY ","
ENCLOSED BY "\\""
LINES TERMINATED BY "\\n"
from FloidMessage
where DTYPE="FloidDroidMissionInstructionStatusMessage_" and floidId=@floidId;');

SELECT @q1;
prepare s1 from @q1;
execute s1;deallocate prepare s1;

