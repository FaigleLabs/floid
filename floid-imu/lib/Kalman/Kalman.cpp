//----------------------------------------//
//  Chris Faigle/faiglelabs               //
//  Free - port of jKalman                //
//----------------------------------------//
#include "Kalman.h"
Kalman::Kalman()
{
  init();
}
void Kalman::init()
{
  Q_angle  = 0.001; 
  Q_gyro   = 0.003;
  R_angle  = 0.03;
  angle    = 0;
  bias     = 0;
  P_00     = 0;
  P_01     = 0;
  P_10     = 0;
  P_11     = 0;	
}
Kalman::Kalman(float iQ_angle, float iQ_gyro, float iR_angle)
{
  init(iQ_angle, iQ_gyro, iR_angle);
}
void Kalman::init(float iQ_angle, float iQ_gyro, float iR_angle)
{
  Q_angle  = iQ_angle;
  Q_gyro   = iQ_gyro;
  R_angle  = iR_angle;
  angle    = 0;
  bias     = 0;
  P_00     = 0;
  P_01     = 0;
  P_10     = 0;
  P_11     = 0;	
}

#define ONE_PI_DEGREES	(180)
#define TWO_PI_DEGREES	(360)
#define ONE_PI_RADIANS	(3.14159267)
#define TWO_PI_RADIANS	(6.28318531)

float Kalman::kalmanCalculate(float newAngle, float newRate, int looptime, boolean degrees)
{
    dt     =   float(looptime)/1000;
    angle +=   dt * (newRate - bias);
    // Clamp the angle to within +- ONE_PI OF THE OTHER ANGLE - CTF
    if(degrees)
    {
      while(angle - newAngle >= ONE_PI_DEGREES) angle -= TWO_PI_DEGREES;
      while(angle - newAngle < -ONE_PI_DEGREES) angle += TWO_PI_DEGREES;
    }
    else
    {
      while(angle - newAngle >= ONE_PI_RADIANS) angle -= TWO_PI_RADIANS;
      while(angle - newAngle < -ONE_PI_RADIANS) angle += TWO_PI_RADIANS;
    }
    P_00  +=  - dt * (P_10 + P_01) + Q_angle * dt;
    P_01  +=  - dt * P_11;
    P_10  +=  - dt * P_11;
    P_11  +=  + Q_gyro * dt;

    y      = newAngle - angle;
    S      = P_00 + R_angle;
    if(S !=0.0)
    {
      K_0    = P_00 / S;
      K_1    = P_10 / S;
    }
    else
    {
      K_0    = 1.0;
      K_1    = 1.0;
    }
    angle += K_0 * y;
    bias  += K_1 * y;
    P_00  -= K_0 * P_00;
    P_01  -= K_0 * P_01;
    P_10  -= K_1 * P_00;
    P_11  -= K_1 * P_01;

    return angle;
  }

 
