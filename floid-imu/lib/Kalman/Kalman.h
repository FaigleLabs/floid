//----------------------------------------//
//  Chris Faigle/faiglelabs               //
//  Free - port of jKalman                //
//----------------------------------------//
#ifndef _KALMAN_H_
#define _KALMAN_H_
#include <Arduino.h>
// KasBot V2  -  Kalman filter module - http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1284738418 - http://www.x-firm.com/?page_id=145
//               with slightly modifications by Kristian Lauszus
//            - Modified to class version ctf

class Kalman
{
  public:
          Kalman();
          Kalman(float iQ_angle, float iQ_gyro, float iR_angle);
    void  init();
    void  init(float iQ_angle, float iQ_gyro, float iR_angle);
    float kalmanCalculate(float newAngle, float newRate,int looptime, boolean degrees);
  private:
    float Q_angle;
    float Q_gyro;
    float R_angle;

    float angle;
    float bias;
    float P_00;
    float P_01;
    float P_10;
    float P_11;	
    float dt;
    float y;
    float S;
    float K_0;
    float K_1;
};

#endif /* _KALMAN_H_ */
