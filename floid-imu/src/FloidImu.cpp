/*
   FullTest.ino: Example sketch for running EM7180 SENtral sensor hub in master mode.

   Adapted from

     https://github.com/kriswiner/Teensy_Flight_Controller/blob/master/EM7180_MPU9250_BMP280

   This file is part of EM7180.

   EM7180 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   EM7180 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with EM7180.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Arduino.h>
#include "EM7180.h"
#include "Kalman.h"
#include "FloidImu.h"

#include <i2c_t3.h>
#define NOSTOP I2C_NOSTOP

#define ACCELEROMETER_RESOLUTION        (8)
#define GYROSCOPE_RESOLUTION            (2000)
#define MAGNETOMETER_RESOLUTION         (1000)
#define ANALOG_OUTPUT                   (0)
#define DIGITAL_OUTPUT                  (1)
#define PYR_COMMAND_BUFFER_SIZE         (3)
#define ALTITUDE_SMOOTH_RATE            (0.8)
#define PARAMETERS_SAVED                (0x40)
#define PARAMETERS_LOADED               (0x80)
#define PARAMETERS_CLEARED              (0x100)

// USB:
#define USB_PORT    (Serial)
// RX/TX:
#define SERIAL_PORT (Serial1)
// Choices:
// 1. Choose: Digital or analog to start: (Digital for Production)
static int8_t   outputMode                                  = DIGITAL_OUTPUT;
//static int8_t   outputMode                                  = ANALOG_OUTPUT;

// Processing:
// -------------------------
// 1. Choose: USB or RX/TX:
static int sumCount;
static uint32_t lastUpdate;                                         // Processing
static uint32_t lastOutputTime;                                     // Output
static uint32_t outputDelay                                 = 200;  // Output
static uint32_t outputDelays[6]                             = {0, 20, 100, 200, 500, 1000};  // 0-off   20-50Hz   100-10Hz   200-5Hz   500-2Hz   1000-1Hz
static uint32_t lastProcessCommandsTime;                            // Commands
static uint32_t processCommandsDelay                        = 1000; // Commands
const int ledPin = 13;
const int gndPin = 22;
const int powerPin = 21;
static bool led = false;
// Commands:
// -------------------------
static byte     pyrCommandBuffer[PYR_COMMAND_BUFFER_SIZE]   = {0,0,0};

// Communication:
// -------------------------
static uint32_t                 packetNumber                        = 0;
static FloidPYRDataPacket       floidPYRDataPacket;

// Model:
// -------------------------
static float sum;
static float q[4];
static float yaw;
static float pitch;
static float roll;
static int16_t ax, ay, az;
static int16_t gx, gy, gz;
static float temperature, pressure;
static float altitude;
static float rate;
static uint8_t status;
// Device:
// -------------------------
EM7180 em7180;
// Routines:
// -------------------------
void setup() {
    pinMode(ledPin, OUTPUT);
    pinMode(gndPin, OUTPUT);
    digitalWrite(gndPin, LOW);
    pinMode(powerPin, OUTPUT);
    digitalWrite(powerPin, HIGH);
    delay(200);
    blinkLed();
    setupPacket(&floidPYRDataPacket);
#ifdef __MK20DX256__
//    Wire.begin(I2C_MASTER, 0x00, I2C_PINS_16_17, I2C_PULLUP_EXT, I2C_RATE_400);
    Wire.begin(I2C_MASTER, 0x00, I2C_PINS_18_19, I2C_PULLUP_EXT, I2C_RATE_400);
#else
    Wire.begin();
#endif
    USB_PORT.begin(38400);
    SERIAL_PORT.begin(38400);
    delay(200);
    // To display the data packet size set to analog and uncomment below:
    /*
    if(outputMode == ANALOG_OUTPUT) {
        dualPrint("Packet Size: ");
        dualPrintln(sizeof(FloidPYRDataPacket));
        delay(200);
    }
    */
    blinkLed();
    lastOutputTime = 0;
    lastProcessCommandsTime = 0;

    // Warm Start Parameters Saved:
    em7180.warmStartParametersSaved = false;
    // Warm Start Parameters Cleared:
    em7180.warmStartParametersCleared = false;
    // Warm Start Parameters Loaded:
    em7180.warmStartParametersLoaded = false;
    // Start the EM710:
    uint8_t startStatus = em7180.begin(ACCELEROMETER_RESOLUTION, GYROSCOPE_RESOLUTION, MAGNETOMETER_RESOLUTION);
    while (startStatus) {
        dualPrintln(EM7180::errorToString(startStatus));
    }
    // Let's go:
    blinkLed();
}

void loop()
{
    process();
    if(isOutputTime()) output();
    if(isProcessCommandsTime()) processCommands();
}

void blinkLed() {
    led = !led;
    if(led) {
        digitalWrite(ledPin, HIGH);   // set the LED on
    } else {
        digitalWrite(ledPin, LOW);   // set the LED off
    }
}

// PROCESS:
// --------
void process() {
    // Check error status:
    uint8_t errorStatus = em7180.poll();
    if (errorStatus) {
        if(outputMode == ANALOG_OUTPUT) {
            dualPrint("ERROR: ");
            dualPrintln(EM7180::errorToString(errorStatus));
        }
        return;
    }
    // Get the quaternions
    em7180.getQuaternions(q);
    status = em7180.algorithmStatus();
    // keep track of rates
    uint32_t Now = micros();
    float deltat = ((Now - lastUpdate)/1000000.0f); // set integration time by time elapsed since last filter update
    lastUpdate = Now;
    sum += deltat; // sum for averaging filter update rate
    sumCount++;
}

// Save the imu warm-start parameters to its eeprom:
void saveWarmStartParameters() {
    em7180.getWsParams();
    em7180.setPassThroughMode();
    delay(200);
    em7180.writeWsParams();
    em7180.resume();
}
// Reset the imu and begin calibration:
void calibrate() {
   // 1. Erase the EEPROM where the registers are stored:
   em7180.setPassThroughMode();
   delay(200);
   em7180.clearWsParams();
   delay(200);
   em7180.resume();
   delay(200);
   // 2. Issue a restart:
   em7180.begin(ACCELEROMETER_RESOLUTION, GYROSCOPE_RESOLUTION, MAGNETOMETER_RESOLUTION);
}
// OUTPUT:
// -------
boolean isOutputTime() {
    return millis() > lastOutputTime + outputDelay;
}
void updateOutputTime() {
    lastOutputTime = millis();
    blinkLed();
}

void output() {
   if(isOutputTime()) {
       updateOutputs();
       if(outputMode == ANALOG_OUTPUT) {
           analogOutput();
       } else {
           digitalOutput();
       }
       updateOutputTime();
   }
}

void updateOutputs() {
    yaw   = atan2(2.0f * (q[0] * q[1] + q[3] * q[2]), q[3] * q[3] + q[0] * q[0] - q[1] * q[1] - q[2] * q[2]);
    pitch = -asin(2.0f * (q[0] * q[2] - q[3] * q[1]));
    roll  = atan2(2.0f * (q[3] * q[0] + q[1] * q[2]), q[3] * q[3] - q[0] * q[0] - q[1] * q[1] + q[2] * q[2]);
    pitch *= 180.0f / PI;
    yaw   *= 180.0f / PI;
    // Note: We do NOT do declination adjustment here...
    if(yaw < 0) yaw   += 360.0f ; // Ensure yaw stays between 0 and 360
    roll  *= 180.0f / PI;
    em7180.getAccelRaw(ax, ay, az);
    em7180.getGyroRaw(gx, gy, gz);
    em7180.getBaro(pressure, temperature);
    altitude = 145366.45f*(1.0f - pow((pressure/1013.25f), 0.190284f));
    rate = (float)sumCount/sum;
    sumCount = 0;
    sum = 0;
    floidPYRDataPacket.altitude    = ALTITUDE_SMOOTH_RATE * floidPYRDataPacket.altitude + (1.0-ALTITUDE_SMOOTH_RATE) * altitude;
    floidPYRDataPacket.temperature = 9.*temperature/5. + 32.;
    floidPYRDataPacket.pitch = pitch;
    floidPYRDataPacket.heading = yaw;
    floidPYRDataPacket.roll = -roll; // THIS IS CORRECT, HOWEVER INVERTED COMPARED TO INDUSTRY
    floidPYRDataPacket.status = status;
    if(em7180.warmStartParametersSaved) {
        floidPYRDataPacket.status |= PARAMETERS_SAVED;
    }
    if(em7180.warmStartParametersLoaded) {
        floidPYRDataPacket.status |= PARAMETERS_LOADED;
    }
    if(em7180.warmStartParametersCleared) {
        floidPYRDataPacket.status |= PARAMETERS_CLEARED;
    }
}

// DIGITAL OUTPUT:
// ---------------
void digitalOutput() {
    sendPacket(&floidPYRDataPacket, packetNumber++);
}

// ANALOG OUTPUT:
// ---------------
void analogOutput() {
/*
    dualPrint("Accel: ");
    dualPrint(ax);
    dualPrint(", ");
    dualPrint(ay);
    dualPrint(", ");
    dualPrintln(az);
    dualPrint("Gyro: ");
    dualPrint(gx);
    dualPrint(", ");
    dualPrint(gy);
    dualPrint(", ");
    dualPrintln(gz);
*/
    dualPrint("Hardware Yaw, Pitch, Roll: ");
    dualPrint(yaw, 2);
    dualPrint(", ");
    dualPrint(pitch, 2);
    dualPrint(", ");
    dualPrintln(roll, 2);
//    dualPrintln("BMP280:");
    dualPrint("Altimeter temperature = ");
    dualPrint( temperature, 2);
    dualPrint(" C  ("); // temperature in degrees Celsius
    dualPrint(9.*temperature/5. + 32., 2);
    dualPrintln(" F)"); // temperature in degrees Fahrenheit
    dualPrint("Altimeter pressure = ");
    dualPrint(pressure, 2);
    dualPrintln(" mbar");// pressure in millibar
    dualPrint("Altitude = ");
    dualPrint(altitude, 2);
    dualPrintln(" feet");
    dualPrintln(" ");
    dualPrint(rate, 2);
    dualPrintln(" Hz");
    /*
    dualPrint(millis()/1000.0, 1);dualPrint(",");
    dualPrint(yaw);
    dualPrint(",");dualPrint(pitch);
    dualPrint(",");dualPrintln(roll);
    */
}

// PROCESS COMMANDS
// ----------------
boolean isProcessCommandsTime() {
    return millis() > lastProcessCommandsTime + processCommandsDelay;
}

void updateProcessCommandsTime() {
    lastProcessCommandsTime = millis();
}

void processCommands()
{
  // Format:  $C*  where C is command
  while(USB_PORT.available())
  {
    byte b = USB_PORT.read();
    addCommandByte(b);
    checkAndProcessCommand();
  }
  // Format:  $C*  where C is command
  while(SERIAL_PORT.available())
  {
    byte b = SERIAL_PORT.read();
    addCommandByte(b);
    checkAndProcessCommand();
  }
  updateProcessCommandsTime();
}
void addCommandByte(byte b)
{
  // Shift previous bytes left
  for(int i=0; i<PYR_COMMAND_BUFFER_SIZE-1; ++i)
  {
    pyrCommandBuffer[i] = pyrCommandBuffer[i+1];
  }
  // Set last byte:
  pyrCommandBuffer[PYR_COMMAND_BUFFER_SIZE-1] = b;
}
void checkAndProcessCommand()
{
  if(pyrCommandBuffer[0] != (byte)'$') return;
  if(pyrCommandBuffer[2] != (byte)'*') return;
  // Command is in byte 1:
  switch(pyrCommandBuffer[1])
  {
    case (byte)'A':
    {
      outputMode = ANALOG_OUTPUT;
    }
    break;
    case (byte)'D':
    {
      outputMode = DIGITAL_OUTPUT;
    }
    break;
    case (byte)'C':
    {
      // Start calibration:
      calibrate();
    }
    break;
    case (byte)'S':
    {
      // Store WS Parameters:
      saveWarmStartParameters();
    }
    break;
    case (byte)'0':
    {
      outputDelay = outputDelays[0];
    }
    break;
    case (byte)'1':
    {
      outputDelay = outputDelays[1];
    }
    break;
    case (byte)'2':
    {
      outputDelay = outputDelays[2];
    }
    break;
    case (byte)'3':
    {
      outputDelay = outputDelays[3];
    }
    break;
    case (byte)'4':
    {
      outputDelay = outputDelays[4];
    }
    break;
    case (byte)'5':
    {
      outputDelay = outputDelays[5];
    }
    break;
  }
}


void setupPacket(FloidPYRDataPacket* packet)
{
  // Header:
  packet->header[0]    = FLOID_PYR_HEADER_0;
  packet->header[1]    = FLOID_PYR_HEADER_1;
  packet->header[2]    = FLOID_PYR_HEADER_2;
  // Footer:
  packet->footer[0]    = FLOID_PYR_FOOTER_0;
  packet->footer[1]    = FLOID_PYR_FOOTER_1;
  packet->footer[2]    = FLOID_PYR_FOOTER_2;
  packet->status       = 0;
  // Checksum
  packet->checksum[0]  = 0;
  packet->checksum[1]  = 0;
  packet->checksum[2]  = 0;
  packet->checksum[3]  = 0;
  packet->pitch        = 0.0;
  packet->roll         = 0.0;
  packet->heading      = 0.0;
  packet->temperature  = 0.0;
  packet->altitude     = 0.0;
}

void setupPYRData(FloidPYRData* pyrData)
{
  pyrData->pitch        = 0.0;
  pyrData->roll         = 0.0;
  pyrData->heading      = 0.0;
  pyrData->temperature  = 0.0;
  pyrData->altitude     = 0.0;
  for(int i=0; i<3; ++i)
  {
    pyrData->accel[i]   = 0.0;
    pyrData->gyro[i]    = 0.0;
    pyrData->mag[i]     = 0.0;
  }
}
void checksumPacket(FloidPYRDataPacket* packet)
{
  // Checksum starts with the packet number:
  packet->checksum[0] = ((byte*)&(packet->packetNumber))[0];
  packet->checksum[1] = ((byte*)&(packet->packetNumber))[1];
  packet->checksum[2] = ((byte*)&(packet->packetNumber))[2];
  packet->checksum[3] = ((byte*)&(packet->packetNumber))[3];
  checksumData((byte*)&packet->status, packet->checksum, 5 * sizeof(float) + 1);
}
void checksumData(byte* data, byte* checksum, unsigned int length)
{
  for(unsigned int byteIndex=0; byteIndex<length; ++byteIndex)
  {
    checksum[byteIndex%4] ^= data[byteIndex];  // Wha-bam
  }
}

void sendPacket(FloidPYRDataPacket *packet, uint32_t packetNumber)
{
  packet->packetNumber = packetNumber;
  checksumPacket(packet);
  USB_PORT.write((const uint8_t*)packet, (int)sizeof(FloidPYRDataPacket));
  SERIAL_PORT.write((const uint8_t*)packet, (int)sizeof(FloidPYRDataPacket));
}
void printPacketHeading()
{
  dualPrintln(" HEADER    CHK   Pitch   Roll   Heading   Temp Altitude   FOOTER");
}
void printPacket(FloidPYRDataPacket *packet)
{
  checksumPacket(packet);
//  dualPrintln("--------------------------------------");
//  dualPrint("HDR: ");
  for(int headerIndex=0; headerIndex<3; ++headerIndex)
  {
    dualPrint(packet->header[headerIndex]);
    dualPrint("-");
  }
//  dualPrint("CHK: ");
  for(int checksumIndex=0; checksumIndex<4; ++checksumIndex)
  {
    dualPrint(packet->checksum[checksumIndex]);
    dualPrint("-");
  }
  dualPrint(packet->pitch);
  dualPrint("  ");
  dualPrint(packet->roll);
  dualPrint("  ");
  dualPrint(packet->heading);
  dualPrint("    ");
  dualPrint(packet->temperature);
  dualPrint("  ");
  dualPrint(packet->altitude);
  dualPrint("      ");
  for(int footerIndex=0; footerIndex<3; ++footerIndex)
  {
    dualPrint(packet->footer[footerIndex]);
    dualPrint("-");
  }
  dualPrintln();
}
void printDataHeading()
{
  dualPrintln(" Pitch   Roll   Heading   Temp Altitude   ax    ay    az   gx   gy   gz   mx   my   mz");
}
void printData(FloidPYRData *data)
{
  dualPrint(data->pitch);
  dualPrint("\t");
  dualPrint(data->roll);
  dualPrint("  ");
  dualPrint(data->heading);
  dualPrint("    ");
  dualPrint(data->altitude);
  dualPrint("  ");
  dualPrint(data->temperature);
  dualPrint("      ");
  dualPrint(data->accel[0]);
  dualPrint("  ");
  dualPrint(data->accel[1]);
  dualPrint("  ");
  dualPrint(data->accel[2]);
  dualPrint("      ");
  dualPrint(data->gyro[0]);
  dualPrint("  ");
  dualPrint(data->gyro[1]);
  dualPrint("  ");
  dualPrint(data->gyro[2]);
  dualPrint("      ");
  dualPrint(data->mag[0]);
  dualPrint("  ");
  dualPrint(data->mag[1]);
  dualPrint("  ");
  dualPrintln(data->mag[2]);
}


// -------------------------------------------------

size_t dualPrint(const __FlashStringHelper *ifsh) {
  USB_PORT.print(ifsh);
  return SERIAL_PORT.print(ifsh);
}
size_t dualPrint(const String &s) {
  USB_PORT.print(s);
  return SERIAL_PORT.print(s);
}
size_t dualPrint(const char str[]) {
  USB_PORT.print(str);
  return SERIAL_PORT.print(str);
}
size_t dualPrint(char c) {
  USB_PORT.print(c);
  return SERIAL_PORT.print(c);
}
size_t dualPrint(unsigned char c, int base = DEC) {
  USB_PORT.print(c, base);
  return SERIAL_PORT.print(c, base);
}
size_t dualPrint(int n, int base = DEC){
  USB_PORT.print(n, base);
  return SERIAL_PORT.print(n, base);
}
size_t dualPrint(unsigned int n, int base = DEC) {
  USB_PORT.print(n, base);
  return SERIAL_PORT.print(n, base);
}
size_t dualPrint(long n, int base = DEC) {
  USB_PORT.print(n, base);
  return SERIAL_PORT.print(n, base);
}
size_t dualPrint(unsigned long n, int base = DEC) {
  USB_PORT.print(n, base);
  return SERIAL_PORT.print(n, base);
}
size_t dualPrint(double n, int digits = 2) {
  USB_PORT.print(n, digits);
  return SERIAL_PORT.print(n, digits);
}
size_t dualPrint(const Printable& x) {
  USB_PORT.print(x);
  return SERIAL_PORT.print(x);
}


size_t dualPrintln(const __FlashStringHelper *ifsh) {
  USB_PORT.println(ifsh);
  return SERIAL_PORT.println(ifsh);
}
size_t dualPrintln(const String &s) {
  USB_PORT.println(s);
  return SERIAL_PORT.println(s);
}
size_t dualPrintln(const char str[]) {
  USB_PORT.println(str);
  return SERIAL_PORT.println(str);
}
size_t dualPrintln(char c) {
  USB_PORT.println(c);
  return SERIAL_PORT.println(c);
}
size_t dualPrintln(unsigned char c, int base = DEC) {
  USB_PORT.println(c, base);
  return SERIAL_PORT.println(c, base);
}
size_t dualPrintln(int n, int base = DEC){
  USB_PORT.println(n, base);
  return SERIAL_PORT.println(n, base);
}
size_t dualPrintln(unsigned int n, int base = DEC) {
  USB_PORT.println(n, base);
  return SERIAL_PORT.println(n, base);
}
size_t dualPrintln(long n, int base = DEC) {
  USB_PORT.println(n, base);
  return SERIAL_PORT.println(n, base);
}
size_t dualPrintln(unsigned long n, int base = DEC) {
  USB_PORT.println(n, base);
  return SERIAL_PORT.println(n, base);
}
size_t dualPrintln(double n, int digits = 2) {
  USB_PORT.println(n, digits);
  return SERIAL_PORT.println(n, digits);
}
size_t dualPrintln(const Printable& x) {
  USB_PORT.println(x);
  return SERIAL_PORT.println(x);
}
