#ifndef __FLOID_IMU_H__
#define __FLOID_IMU_H__

#include <Arduino.h>
#define FLOID_PYR_HEADER_0 ('!')
#define FLOID_PYR_HEADER_1 ('@')
#define FLOID_PYR_HEADER_2 ('#')
#define FLOID_PYR_FOOTER_0 ('$')
#define FLOID_PYR_FOOTER_1 ('%')
#define FLOID_PYR_FOOTER_2 ('^')

#define FLOID_PACKET_HEADER_0  (0xfa)
#define FLOID_PACKET_HEADER_1  (0x13)
#define FLOID_PACKET_HEADER_2  (0x1a)
#define FLOID_PACKET_HEADER_3  (0xb3)

typedef struct __attribute__((__packed__))
{
  float pitch;
  float roll;
  float heading;
  float temperature;
  float altitude;
  float accel[3];
  float gyro[3];
  float mag[3];
} FloidPYRData;
typedef struct __attribute__((__packed__))
{
  byte              header[3];
  uint32_t          packetNumber;
  uint16_t          status;
  float             pitch;
  float             roll;
  float             heading;
  float             temperature;
  float             altitude;
  byte              checksum[4];
  byte              footer[3];
} FloidPYRDataPacket;  // 3 + 3 + 7*4 + 2 = 6 + 28 + 2 = 36

void blinkLed();
void process();
void saveImuWsParameters();
boolean isOutputTime();
void updateOutputTime();
void output();
void updateOutputs();
void digitalOutput();
void analogOutput();
boolean isProcessCommandsTime();
void updateProcessCommandsTime();
void processCommands();
void addCommandByte(byte b);
void checkAndProcessCommand();
void saveWarmStartParameters();
void calibrate();
void setupPacket(FloidPYRDataPacket* packet);
void setupPYRData(FloidPYRData *pyrData);
void checksumPacket(FloidPYRDataPacket* packet);
void checksumData(byte* data, byte* checksum, unsigned int length);
void sendPacket(FloidPYRDataPacket* packet, uint32_t packetNumber);
void printPacket(FloidPYRDataPacket *packet);
void printDataHeading();
void printData(FloidPYRData *data);
size_t dualPrint(const __FlashStringHelper *);
size_t dualPrint(const String &);
size_t dualPrint(const char[]);
size_t dualPrint(char);
size_t dualPrint(unsigned char, int);
size_t dualPrint(int, int);
size_t dualPrint(unsigned int, int);
size_t dualPrint(long, int);
size_t dualPrint(unsigned long, int);
size_t dualPrint(double, int);
size_t dualPrint(const Printable&);
size_t dualPrintln(const __FlashStringHelper *);
size_t dualPrintln(const String &s);
size_t dualPrintln(const char[]);
size_t dualPrintln(char);
size_t dualPrintln(unsigned char, int);
size_t dualPrintln(int, int);
size_t dualPrintln(unsigned int, int);
size_t dualPrintln(long, int);
size_t dualPrintln(unsigned long, int);
size_t dualPrintln(double, int);
size_t dualPrintln(const Printable&);
size_t dualPrintln(void);

#endif /* __FLOID_IMU_H__ */

