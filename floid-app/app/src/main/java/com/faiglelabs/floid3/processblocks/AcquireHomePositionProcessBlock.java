/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

import com.faiglelabs.floid.servertypes.commands.AcquireHomePositionCommand;
import com.faiglelabs.floid.servertypes.statuses.FloidDroidStatus;
import com.faiglelabs.floid.utils.DistanceUtils;

/**
 * The acquire home position process block
 */
public class AcquireHomePositionProcessBlock extends ProcessBlock
{

    // Our tag for debugging:
    /**
     * The constant TAG.
     */
    @SuppressWarnings("unused")
    private static final String TAG = "AcquireHomePositionProcessBlock";
    /**
     * The constant ACQUIRE_STATE_NONE.
     */
    @SuppressWarnings("WeakerAccess")
    public final static int ACQUIRE_STATE_NONE = 0;
    /**
     * The constant ACQUIRE_STATE_START.
     */
    public final static int ACQUIRE_STATE_START = 1;
    /**
     * The constant ACQUIRE_STATE_TURNING_ON_GPS.
     */
    public final static int ACQUIRE_STATE_TURNING_ON_GPS = 2;
    /**
     * The constant ACQUIRE_STATE_TURNING_ON_IMU.
     */
    public final static int ACQUIRE_STATE_TURNING_ON_IMU = 3;
    /**
     * The constant ACQUIRE_STATE_SETTING_DECLINATION.
     */
    public final static int ACQUIRE_STATE_SETTING_DECLINATION = 4;
    /**
     * The constant ACQUIRE_STATE_WAITING_ON_DECLINATION.
     */
    public final static int ACQUIRE_STATE_WAITING_ON_DECLINATION = 5;
    /**
     * The constant ACQUIRE_STATE_WAITING_ON_GPS_COUNT.
     */
    public final static int ACQUIRE_STATE_WAITING_ON_GPS_COUNT = 6;
    /**
     * The constant ACQUIRE_STATE_WAITING_ON_ALTIMETER_INITIALIZATION.
     */
    public final static int ACQUIRE_STATE_WAITING_ON_ALTIMETER_INITIALIZATION = 7;
    /**
     * The constant ACQUIRE_STATE_WAITING_ON_ALTIMETER_VALIDATION.
     */
    public final static int ACQUIRE_STATE_WAITING_ON_ALTIMETER_VALIDATION = 8;
    /**
     * The constant ACQUIRE_STATE_WAITING_ON_ALTIMETER_COUNT.
     */
    public final static int ACQUIRE_STATE_WAITING_ON_ALTIMETER_COUNT = 9;
    /**
     * The constant ACQUIRE_STATE_ACQUIRING.
     */
    public final static int ACQUIRE_STATE_ACQUIRING = 10;
    /**
     * The constant ACQUIRE_STATE_WAITING_ON_ACQUIRE_COUNT.
     */
    @SuppressWarnings("WeakerAccess")
    public final static int ACQUIRE_STATE_WAITING_ON_ACQUIRE_COUNT = 11;
    /**
     * The constant ACQUIRE_STATE_SINGULARITY.
     */
    @SuppressWarnings("WeakerAccess")
    public final static int ACQUIRE_STATE_SINGULARITY = 12;
    /**
     * The constant ACQUIRE_STATE_WAITING_ON_DISTANCE.
     */
    @SuppressWarnings("WeakerAccess")
    public final static int ACQUIRE_STATE_WAITING_ON_DISTANCE = 13;
    /**
     * The constant ACQUIRE_STATE_WAITING_ON_ALTITUDE.
     */
    @SuppressWarnings("WeakerAccess")
    public final static int ACQUIRE_STATE_WAITING_ON_ALTITUDE = 14;
    /**
     * The constant ACQUIRE_STATE_WAITING_ON_HEADING.
     */
    @SuppressWarnings("WeakerAccess")
    public final static int ACQUIRE_STATE_WAITING_ON_HEADING = 15;
    /**
     * The constant ACQUIRE_STATE_ACQUIRED.
     */
    public final static int ACQUIRE_STATE_ACQUIRED = 16;
    /**
     * The constant ACQUIRE_STATE_FAILED.
     */
    public final static int ACQUIRE_STATE_FAILED = 17;
    private final static int                        ACQUIRE_SIZE                  = 10;
    private              long                       lastFloidStatusUpdateTime     = 0L;
    private              boolean                    homePositionAcquired          = false;
    private              int                        numAcquired                   = 0;
    private final        double[]                   xArray                        = new double[ACQUIRE_SIZE];
    private final        double[]                   yArray                        = new double[ACQUIRE_SIZE];
    private final        double[]                   zArray                        = new double[ACQUIRE_SIZE];
    private final        double[]                   headingArray                  = new double[ACQUIRE_SIZE];
    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    private final        long[]                     statusTimeArray               = new   long[ACQUIRE_SIZE];
    private final        AcquireHomePositionCommand mAcquireHomePositionCommand;
    private              int                        maxGoodGpsCount               = 0;

    private int acquireState = ACQUIRE_STATE_NONE;


    /**
     * Create an acquire home position process blocks
     *
     * @param timeoutInterval            the timeout interval for this request
     * @param acquireHomePositionCommand the acquire home position command
     */
    public AcquireHomePositionProcessBlock(@SuppressWarnings("SameParameterValue") long timeoutInterval, AcquireHomePositionCommand acquireHomePositionCommand)
    {
        super(timeoutInterval, "Acquire Home Position");
        mAcquireHomePositionCommand = acquireHomePositionCommand;
        setToDefault();
    }

    /**
     * Gets last floid status update time.
     *
     * @return the last floid status update time
     */
    public long getLastFloidStatusUpdateTime()
    {
        return lastFloidStatusUpdateTime;
    }
    /**
     * Sets last floid status update time.
     * @param lastFloidStatusUpdateTime the last floid status update time
     */
    public void setLastFloidStatusUpdateTime(int lastFloidStatusUpdateTime)
    {
        this.lastFloidStatusUpdateTime = lastFloidStatusUpdateTime;
    }
    /**
     * Set our default parameters for this command
     */
    public final void setToDefault()
    {
        homePositionAcquired      = false;
        lastFloidStatusUpdateTime = -1;
        numAcquired               = 0;
    }

    /**
     * Perform an acquire and test
     *
     * @param floidDroidStatus the FloidDroidStatus to update
     * @param x                longitude
     * @param y                latitude
     * @param z                altitude
     * @param heading          heading - in std trig angles - NOT compass degrees
     * @param statusTime       the status time
     * @return the current acquisition state after processing
     */
    public int acquireAndTest(FloidDroidStatus floidDroidStatus, double x, double y, double z, double heading, long statusTime) {
        // Set the last status we processed (We can exit from this routine in many places thus we need to set this first)
        lastFloidStatusUpdateTime = statusTime;
        // Values always go at the start - shift the array
        for (int i = ACQUIRE_SIZE - 1; i > 0; --i) {
            xArray[i] = xArray[i - 1];
            yArray[i] = yArray[i - 1];
            zArray[i] = zArray[i - 1];
            headingArray[i] = headingArray[i - 1];
            statusTimeArray[i] = statusTimeArray[i - 1];
        }
        // Copy in our values:
        xArray[0] = x;
        yArray[0] = y;
        zArray[0] = z;
        headingArray[0] = heading;
        statusTimeArray[0] = statusTime;
        // And increase the acquired count
        ++numAcquired;
        // Can we test yet?
        if (numAcquired < ACQUIRE_SIZE) {
            // Nope!
            return ACQUIRE_STATE_WAITING_ON_ACQUIRE_COUNT;       // Did not acquire home position
        }
        // OK, now check to see if we are within range:
        // 1. Calculate the distance between each GPS point and the mean and return false if we are too far:
        for (int i = 1; i < ACQUIRE_SIZE; ++i) {
            double xyDistance = DistanceUtils.calcLatLngDistance(yArray[0], xArray[0], yArray[i], xArray[i]);
            if (xyDistance > mAcquireHomePositionCommand.getRequiredXYDistance()) {
                return ACQUIRE_STATE_WAITING_ON_DISTANCE;
            }
            double zDistance = Math.abs(zArray[i] - zArray[0]);
            if (zDistance > mAcquireHomePositionCommand.getRequiredAltitudeDistance()) {
                return ACQUIRE_STATE_WAITING_ON_ALTITUDE;
            }
            double headingDegreeDistance = Math.abs(DistanceUtils.calcHeadingDistance(headingArray[0], headingArray[i]));
            if (headingDegreeDistance > mAcquireHomePositionCommand.getRequiredHeadingDistance()) {
                return ACQUIRE_STATE_WAITING_ON_HEADING;
            }
        }
        // Denote that we have acquired the home position:
        floidDroidStatus.setHomePositionAcquired(true);
        floidDroidStatus.setHomePositionX(xArray[0]);
        floidDroidStatus.setHomePositionY(yArray[0]);
        floidDroidStatus.setHomePositionZ(zArray[0]);
        floidDroidStatus.setHomePositionHeading(headingArray[0]);
        return ACQUIRE_STATE_ACQUIRED;  // Hurrah - we acquired a home position...
    }


    /**
     * Get the status of the home position acquired
     *
     * @return true is home position has been acquired
     */
    @SuppressWarnings("unused")
    public boolean getHomePositionAcquired()
    {
        return homePositionAcquired;
    }

    /**
     * Get the acquire state
     *
     * @return the acquire state
     */
    public int getAcquireState() {
        return acquireState;
    }

    /**
     * Set the acquire state
     *
     * @param acquireState the new acquire state
     */
    public void setAcquireState(int acquireState) {
        this.acquireState = acquireState;
    }

    /**
     * Get the textual representation of the current acquire state
     *
     * @return the textual representation of the current acquire state
     */
    public String getAcquireStateString() {
        return getAcquireStateString(acquireState);
    }

    /**
     * Get the textual representation of the acquire state
     *
     * @param acquireState the acquire state
     * @return the textual representation of the acquire state
     */
    public String getAcquireStateString(int acquireState) {
        switch(acquireState) {
            case ACQUIRE_STATE_NONE: {
                return "NONE";
            }
            case ACQUIRE_STATE_START: {
                return "START";
            }
            case ACQUIRE_STATE_TURNING_ON_GPS: {
                return "TURNING ON GPS";
            }
            case ACQUIRE_STATE_TURNING_ON_IMU: {
                return "TURNING ON IMU";
            }
            case ACQUIRE_STATE_SETTING_DECLINATION: {
                return "SETTING DECLINATION";
            }
            case ACQUIRE_STATE_WAITING_ON_DECLINATION: {
                return "WAITING ON DECLINATION";
            }
            case ACQUIRE_STATE_WAITING_ON_GPS_COUNT: {
                return "WAITING ON GPS COUNT";
            }
            case ACQUIRE_STATE_WAITING_ON_ALTIMETER_INITIALIZATION: {
                return "WAITING ON ALTIMETER INITIALIZATION";
            }
            case ACQUIRE_STATE_WAITING_ON_ALTIMETER_VALIDATION: {
                return "WAITING ON ALTIMETER VALIDATION";
            }
            case ACQUIRE_STATE_WAITING_ON_ALTIMETER_COUNT: {
                return "WAITING ON ALTIMETER COUNT";
            }
            case ACQUIRE_STATE_ACQUIRING: {
                return "ACQUIRING";
            }
            case ACQUIRE_STATE_WAITING_ON_ACQUIRE_COUNT: {
                return "WAITING ON ACQUIRE COUNT";
            }
            case ACQUIRE_STATE_SINGULARITY: {
                return "SINGULARITY";
            }
            case ACQUIRE_STATE_WAITING_ON_DISTANCE: {
                return "WAITING ON DISTANCE";
            }
            case ACQUIRE_STATE_WAITING_ON_HEADING: {
                return "WAITING ON HEADING";
            }
            case ACQUIRE_STATE_WAITING_ON_ALTITUDE: {
                return "WAITING ON ALTITUDE";
            }
            case ACQUIRE_STATE_ACQUIRED: {
                return "ACQUIRED";
            }
            case ACQUIRE_STATE_FAILED: {
                return "FAILED";
            }
            default: {
                return "UNKNOWN";
            }
        }

    }

    /**
     * Gets max good gps count.
     *
     * @return the max good gps count
     */
    public int getMaxGoodGpsCount() {
        return maxGoodGpsCount;
    }

    /**
     * Sets max good gps count.
     *
     * @param maxGoodGpsCount the max good gps count
     */
    public void setMaxGoodGpsCount(int maxGoodGpsCount) {
        this.maxGoodGpsCount = maxGoodGpsCount;
    }
}
