/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

/**
 * A packet header
 */
class PacketHeader
{
    /**
     * the packet length
     */
    @SuppressWarnings("unused")
    public int    length;
    /**
     * the packet number
     */
    @SuppressWarnings("unused")
    public int    number;
    /**
     * the packet type
     */
    @SuppressWarnings("unused")
    public int    type;
}
