package com.faiglelabs.floid3.parcelables;

import android.os.Parcel;
import android.os.Parcelable;
import com.faiglelabs.floid.servertypes.statuses.FloidModelParameters;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Parcelable version of FloidModelParameters
 */

public class FloidModelParametersParcelable extends FloidModelParameters implements Parcelable {

    /**
     * Parcelable version of FloidModelParameters
     */
    public FloidModelParametersParcelable() {
        super();
        type=this.getClass().getSuperclass().getName();
    }
    @Override
    public FloidModelParametersParcelable fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.bladesLow);
        dest.writeDouble(this.bladesZero);
        dest.writeDouble(this.bladesHigh);
        dest.writeDouble(this.h0S0Low);
        dest.writeDouble(this.h0S1Low);
        dest.writeDouble(this.h0S2Low);
        dest.writeDouble(this.h1S0Low);
        dest.writeDouble(this.h1S1Low);
        dest.writeDouble(this.h1S2Low);
        dest.writeDouble(this.h0S0Zero);
        dest.writeDouble(this.h0S1Zero);
        dest.writeDouble(this.h0S2Zero);
        dest.writeDouble(this.h1S0Zero);
        dest.writeDouble(this.h1S1Zero);
        dest.writeDouble(this.h1S2Zero);
        dest.writeDouble(this.h0S0High);
        dest.writeDouble(this.h0S1High);
        dest.writeDouble(this.h0S2High);
        dest.writeDouble(this.h1S0High);
        dest.writeDouble(this.h1S1High);
        dest.writeDouble(this.h1S2High);
        dest.writeDouble(this.h2S0Low);
        dest.writeDouble(this.h2S1Low);
        dest.writeDouble(this.h2S2Low);
        dest.writeDouble(this.h3S0Low);
        dest.writeDouble(this.h3S1Low);
        dest.writeDouble(this.h3S2Low);
        dest.writeDouble(this.h2S0Zero);
        dest.writeDouble(this.h2S1Zero);
        dest.writeDouble(this.h2S2Zero);
        dest.writeDouble(this.h3S0Zero);
        dest.writeDouble(this.h3S1Zero);
        dest.writeDouble(this.h3S2Zero);
        dest.writeDouble(this.h2S0High);
        dest.writeDouble(this.h2S1High);
        dest.writeDouble(this.h2S2High);
        dest.writeDouble(this.h3S0High);
        dest.writeDouble(this.h3S1High);
        dest.writeDouble(this.h3S2High);
        dest.writeDouble(this.collectiveMin);
        dest.writeDouble(this.collectiveMax);
        dest.writeDouble(this.collectiveDefault);
        dest.writeDouble(this.cyclicRange);
        dest.writeDouble(this.cyclicDefault);
        dest.writeInt(this.escType);
        dest.writeInt(this.escPulseMin);
        dest.writeInt(this.escPulseMax);
        dest.writeDouble(this.escLowValue);
        dest.writeDouble(this.escHighValue);
        dest.writeDouble(this.attackAngleMinDistanceValue);
        dest.writeDouble(this.attackAngleMaxDistanceValue);
        dest.writeDouble(this.attackAngleValue);
        dest.writeDouble(this.pitchDeltaMinValue);
        dest.writeDouble(this.pitchDeltaMaxValue);
        dest.writeDouble(this.pitchTargetVelocityMaxValue);
        dest.writeDouble(this.rollDeltaMinValue);
        dest.writeDouble(this.rollDeltaMaxValue);
        dest.writeDouble(this.rollTargetVelocityMaxValue);
        dest.writeDouble(this.altitudeToTargetMinValue);
        dest.writeDouble(this.altitudeToTargetMaxValue);
        dest.writeDouble(this.altitudeTargetVelocityMaxValue);
        dest.writeDouble(this.headingDeltaMinValue);
        dest.writeDouble(this.headingDeltaMaxValue);
        dest.writeDouble(this.headingTargetVelocityMaxValue);
        dest.writeDouble(this.distanceToTargetMinValue);
        dest.writeDouble(this.distanceToTargetMaxValue);
        dest.writeDouble(this.distanceTargetVelocityMaxValue);
        dest.writeDouble(this.orientationMinDistanceValue);
        dest.writeDouble(this.liftOffTargetAltitudeDeltaValue);
        dest.writeInt(this.servoPulseMin);
        dest.writeInt(this.servoPulseMax);
        dest.writeDouble(this.servoDegreeMin);
        dest.writeDouble(this.servoDegreeMax);
        dest.writeInt(this.servoSignLeft);
        dest.writeInt(this.servoSignRight);
        dest.writeInt(this.servoSignPitch);
        dest.writeDouble(this.servoOffsetLeft0);
        dest.writeDouble(this.servoOffsetRight0);
        dest.writeDouble(this.servoOffsetPitch0);
        dest.writeDouble(this.servoOffsetLeft1);
        dest.writeDouble(this.servoOffsetRight1);
        dest.writeDouble(this.servoOffsetPitch1);
        dest.writeDouble(this.servoOffsetLeft2);
        dest.writeDouble(this.servoOffsetRight2);
        dest.writeDouble(this.servoOffsetPitch2);
        dest.writeDouble(this.servoOffsetLeft3);
        dest.writeDouble(this.servoOffsetRight3);
        dest.writeDouble(this.servoOffsetPitch3);
        dest.writeDouble(this.targetVelocityKeepStill);
        dest.writeDouble(this.targetVelocityFullSpeed);
        dest.writeDouble(this.targetPitchVelocityAlpha);
        dest.writeDouble(this.targetRollVelocityAlpha);
        dest.writeDouble(this.targetHeadingVelocityAlpha);
        dest.writeDouble(this.targetAltitudeVelocityAlpha);
        dest.writeInt(this.landModeTimeRequiredAtMinAltitude);
        dest.writeInt(this.startMode);
        dest.writeInt(this.rotationMode);
        dest.writeDouble(this.cyclicHeadingAlpha);
        dest.writeInt(this.heliStartupModeNumberSteps);
        dest.writeInt(this.heliStartupModeStepTick);
        dest.writeDouble(this.floidModelEscCollectiveCalcMidpoint);
        dest.writeDouble(this.floidModelEscCollectiveLowValue);
        dest.writeDouble(this.floidModelEscCollectiveMidValue);
        dest.writeDouble(this.floidModelEscCollectiveHighValue);
        dest.writeValue(this.id);
        dest.writeString(this.floidUuid);
        dest.writeString(this.type);
        dest.writeInt(this.floidId);
        dest.writeValue(this.floidMessageNumber);
        dest.writeDouble(this.velocityDeltaCyclicAlphaScale);
        dest.writeDouble(this.accelerationMultiplierScale);
    }
    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected FloidModelParametersParcelable(Parcel in) {
        this.bladesLow = in.readDouble();
        this.bladesZero = in.readDouble();
        this.bladesHigh = in.readDouble();
        this.h0S0Low = in.readDouble();
        this.h0S1Low = in.readDouble();
        this.h0S2Low = in.readDouble();
        this.h1S0Low = in.readDouble();
        this.h1S1Low = in.readDouble();
        this.h1S2Low = in.readDouble();
        this.h0S0Zero = in.readDouble();
        this.h0S1Zero = in.readDouble();
        this.h0S2Zero = in.readDouble();
        this.h1S0Zero = in.readDouble();
        this.h1S1Zero = in.readDouble();
        this.h1S2Zero = in.readDouble();
        this.h0S0High = in.readDouble();
        this.h0S1High = in.readDouble();
        this.h0S2High = in.readDouble();
        this.h1S0High = in.readDouble();
        this.h1S1High = in.readDouble();
        this.h1S2High = in.readDouble();
        this.h2S0Low = in.readDouble();
        this.h2S1Low = in.readDouble();
        this.h2S2Low = in.readDouble();
        this.h3S0Low = in.readDouble();
        this.h3S1Low = in.readDouble();
        this.h3S2Low = in.readDouble();
        this.h2S0Zero = in.readDouble();
        this.h2S1Zero = in.readDouble();
        this.h2S2Zero = in.readDouble();
        this.h3S0Zero = in.readDouble();
        this.h3S1Zero = in.readDouble();
        this.h3S2Zero = in.readDouble();
        this.h2S0High = in.readDouble();
        this.h2S1High = in.readDouble();
        this.h2S2High = in.readDouble();
        this.h3S0High = in.readDouble();
        this.h3S1High = in.readDouble();
        this.h3S2High = in.readDouble();
        this.collectiveMin = in.readDouble();
        this.collectiveMax = in.readDouble();
        this.collectiveDefault = in.readDouble();
        this.cyclicRange = in.readDouble();
        this.cyclicDefault = in.readDouble();
        this.escType = in.readInt();
        this.escPulseMin = in.readInt();
        this.escPulseMax = in.readInt();
        this.escLowValue = in.readDouble();
        this.escHighValue = in.readDouble();
        this.attackAngleMinDistanceValue = in.readDouble();
        this.attackAngleMaxDistanceValue = in.readDouble();
        this.attackAngleValue = in.readDouble();
        this.pitchDeltaMinValue = in.readDouble();
        this.pitchDeltaMaxValue = in.readDouble();
        this.pitchTargetVelocityMaxValue = in.readDouble();
        this.rollDeltaMinValue = in.readDouble();
        this.rollDeltaMaxValue = in.readDouble();
        this.rollTargetVelocityMaxValue = in.readDouble();
        this.altitudeToTargetMinValue = in.readDouble();
        this.altitudeToTargetMaxValue = in.readDouble();
        this.altitudeTargetVelocityMaxValue = in.readDouble();
        this.headingDeltaMinValue = in.readDouble();
        this.headingDeltaMaxValue = in.readDouble();
        this.headingTargetVelocityMaxValue = in.readDouble();
        this.distanceToTargetMinValue = in.readDouble();
        this.distanceToTargetMaxValue = in.readDouble();
        this.distanceTargetVelocityMaxValue = in.readDouble();
        this.orientationMinDistanceValue = in.readDouble();
        this.liftOffTargetAltitudeDeltaValue = in.readDouble();
        this.servoPulseMin = in.readInt();
        this.servoPulseMax = in.readInt();
        this.servoDegreeMin = in.readDouble();
        this.servoDegreeMax = in.readDouble();
        this.servoSignLeft = in.readInt();
        this.servoSignRight = in.readInt();
        this.servoSignPitch = in.readInt();
        this.servoOffsetLeft0 = in.readDouble();
        this.servoOffsetRight0 = in.readDouble();
        this.servoOffsetPitch0 = in.readDouble();
        this.servoOffsetLeft1 = in.readDouble();
        this.servoOffsetRight1 = in.readDouble();
        this.servoOffsetPitch1 = in.readDouble();
        this.servoOffsetLeft2 = in.readDouble();
        this.servoOffsetRight2 = in.readDouble();
        this.servoOffsetPitch2 = in.readDouble();
        this.servoOffsetLeft3 = in.readDouble();
        this.servoOffsetRight3 = in.readDouble();
        this.servoOffsetPitch3 = in.readDouble();
        this.targetVelocityKeepStill = in.readDouble();
        this.targetVelocityFullSpeed = in.readDouble();
        this.targetPitchVelocityAlpha = in.readDouble();
        this.targetRollVelocityAlpha = in.readDouble();
        this.targetHeadingVelocityAlpha = in.readDouble();
        this.targetAltitudeVelocityAlpha = in.readDouble();
        this.landModeTimeRequiredAtMinAltitude = in.readInt();
        this.startMode = in.readInt();
        this.rotationMode = in.readInt();
        this.cyclicHeadingAlpha = in.readDouble();
        this.heliStartupModeNumberSteps = in.readInt();
        this.heliStartupModeStepTick = in.readInt();
        this.floidModelEscCollectiveCalcMidpoint = in.readDouble();
        this.floidModelEscCollectiveLowValue = in.readDouble();
        this.floidModelEscCollectiveMidValue = in.readDouble();
        this.floidModelEscCollectiveHighValue = in.readDouble();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.floidUuid = in.readString();
        this.type = in.readString();
        this.floidId = in.readInt();
        this.floidMessageNumber = (Long) in.readValue(Long.class.getClassLoader());
        this.velocityDeltaCyclicAlphaScale = in.readDouble();
        this.accelerationMultiplierScale = in.readDouble();
    }

    /**
     * The creator
     */
    public static final Creator<FloidModelParametersParcelable> CREATOR = new Creator<FloidModelParametersParcelable>() {
        @Override
        public FloidModelParametersParcelable createFromParcel(Parcel source) {
            return new FloidModelParametersParcelable(source);
        }

        @Override
        public FloidModelParametersParcelable[] newArray(int size) {
            return new FloidModelParametersParcelable[size];
        }
    };
}
