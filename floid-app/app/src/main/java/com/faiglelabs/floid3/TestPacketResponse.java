/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

/**
 * Holds test packet response values
 */
@SuppressWarnings("unused")
class TestPacketResponse
{
    /**
     * Test response byte 0
     */
    @SuppressWarnings("WeakerAccess")
    public int     byte_0;
    /**
     * Test response byte 1
     */
    @SuppressWarnings("WeakerAccess")
    public int     byte_1;
    /**
     * Test response byte 2
     */
    @SuppressWarnings("WeakerAccess")
    public int     byte_2;
    /**
     * Test response int 0
     */
    @SuppressWarnings("WeakerAccess")
    public int     int_0;
    /**
     * Test response int 1
     */
    @SuppressWarnings("WeakerAccess")
    public int     int_1;
    /**
     * Test response int 2
     */
    @SuppressWarnings("WeakerAccess")
    public int     int_2;
    /**
     * Test response unsigned int 0
     */
    @SuppressWarnings("WeakerAccess")
    public int     unsigned_int_0;
    /**
     * Test response unsigned int 1
     */
    @SuppressWarnings("WeakerAccess")
    public int     unsigned_int_1;
    /**
     * Test response unsigned int 2
     */
    @SuppressWarnings("WeakerAccess")
    public int     unsigned_int_2;
    /**
     * Test response long int 0
     */
    @SuppressWarnings("WeakerAccess")
    public int     long_int_0;
    /**
     * Test response long int 1
     */
    @SuppressWarnings("WeakerAccess")
    public int     long_int_1;
    /**
     * Test response long int 2
     */
    @SuppressWarnings("WeakerAccess")
    public int     long_int_2;
    /**
     * Test response unsigned long int 0
     */
    @SuppressWarnings("WeakerAccess")
    public int     unsigned_long_int_0;
    /**
     * Test response unsigned long int 1
     */
    @SuppressWarnings("WeakerAccess")
    public int     unsigned_long_int_1;
    /**
     * Test response unsigned long int 2
     */
    @SuppressWarnings("WeakerAccess")
    public int     unsigned_long_int_2;
    
    @Override
    public String toString()
    {
        return
                "byte_0: "         +
                 byte_0            +
                "   byte_1: "      +
                    byte_1         +
                "   byte_2: "      +
                    byte_2         +
                "\nint_0: "    +
                   int_0       +
                "   int_1: "   +
                    int_1      +
                "   int_2: "   +
                    int_2      +
                "\nunsigned_int_0: "  +
                    unsigned_int_0    +
                "   unsigned_int_1: " +
                    unsigned_int_1    +
                "   unsigned_int_2: " +
                    unsigned_int_2    +
                "\nlong_int_0: "   +
                    long_int_0     +
                "   long_int_1: "  +
                    long_int_1     +
                "   long_int_2: "  +
                    long_int_2     +
                "\nunsigned_long_int_0: "      +
                    unsigned_long_int_0        +
                "   unsigned_long_int_1: "     +
                    unsigned_long_int_1        +
                "   unsigned_long_int_2: "     +
                    unsigned_long_int_2        +
                "\n"
                ;
    }
}
