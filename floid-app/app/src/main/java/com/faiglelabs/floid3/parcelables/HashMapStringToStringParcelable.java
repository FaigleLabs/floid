package com.faiglelabs.floid3.parcelables;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Parcelable version of a hash map of strings to strings
 */
public class HashMapStringToStringParcelable implements Parcelable {

    /**
     * The string to string hash map.
     */
    private HashMap<String, String> stringToStringHashMap = new HashMap<>();

    /**
     * Get the hashmap
     * @return the hashmap
     */
    public HashMap<String, String> getStringToStringHashMap() {
        return stringToStringHashMap;
    }

    /**
     * Set the hashmap
     * @param hashMap the hashmap
     */
    public void setStringToStringHashMap(HashMap<String, String> hashMap) {
        stringToStringHashMap = hashMap;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.stringToStringHashMap);
    }

    /**
     * Parcelable version of a hash map of strings to strings
     */
    public HashMapStringToStringParcelable() {
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected HashMapStringToStringParcelable(Parcel in) {
        //noinspection unchecked
        this.stringToStringHashMap = (HashMap<String, String>) in.readSerializable();
    }

    /**
     * The creator
     */
    public static final Creator<HashMapStringToStringParcelable> CREATOR = new Creator<HashMapStringToStringParcelable>() {
        @Override
        public HashMapStringToStringParcelable createFromParcel(Parcel source) {
            return new HashMapStringToStringParcelable(source);
        }

        @Override
        public HashMapStringToStringParcelable[] newArray(int size) {
            return new HashMapStringToStringParcelable[size];
        }
    };
}
