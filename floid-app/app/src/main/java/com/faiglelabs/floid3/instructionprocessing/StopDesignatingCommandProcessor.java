/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.StopDesignatingCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.StopDesignatingProcessBlock;

/**
 * Command processor for stop designating command
 */
public class StopDesignatingCommandProcessor extends InstructionProcessor
{
    /**
     * Create a stop designating command processor
     * @param floidService the floid service
     * @param stopDesignatingCommand the stop designating command
     */
    @SuppressWarnings("WeakerAccess")
	public StopDesignatingCommandProcessor(FloidService floidService, StopDesignatingCommand stopDesignatingCommand)
	{
		super(floidService, stopDesignatingCommand);
		useDefaultGoalProcessing = true;
        completeOnCommandResponse = false;
		setProcessBlock(new StopDesignatingProcessBlock(defaultGoalTimeout));
	}
	@Override
	public boolean setupInstruction()
	{
	    // General idea:
	    // This is generally the same as flyTo and turnTo...
	    super.setupInstruction();
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
	    if(!floidService.getFloidDroidStatus().isLiftedOff())
	    {
	        // Logic error - must be lifted off to stopDesignating:
            sendCommandLogicErrorMessage("Not lifted off.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
	    }
	    // OK, passed all tests...
//        StopDesignatingCommand stopDesignatingCommand = (StopDesignatingCommand)droidInstruction;
        StopDesignatingProcessBlock stopDesignatingProcessBlock = (StopDesignatingProcessBlock)processBlock;
        stopDesignatingProcessBlock.setFloidGoalId(FloidService.NO_GOAL_ID);  // Does not use a goal id - done once the command response is received
        try
        {
            FloidOutgoingCommand stopDesignatingOutgoingCommand = floidService.sendStopDesignatingCommandToFloid(stopDesignatingProcessBlock.getFloidGoalId());
            if(stopDesignatingOutgoingCommand != null)
            {
                stopDesignatingProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                stopDesignatingProcessBlock.setProcessBlockWaitForCommandNumber(stopDesignatingOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Stop Designating command.");
                // We must have failed - set completion with error:
                stopDesignatingProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;    // We are done with this whole command
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
