/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.processblocks;

import com.faiglelabs.floid.servertypes.commands.SpeakerCommand;

/**
 * Process block for speaker command
 */
@SuppressWarnings("SameParameterValue")
public class SpeakerProcessBlock extends ProcessBlock
{
    /**
     * Status not started
     */
    public final static int SPEAKER_PROCESS_STATUS_NOT_STARTED = 0;
    /**
     * Status in alert
     */
    public final static int SPEAKER_PROCESS_STATUS_IN_ALERT = 1;
    /**
     * Status alert complete
     */
    public final static int SPEAKER_PROCESS_STATUS_ALERT_COMPLETE = 2;
    /**
     * Status in tts
     */
    public final static int SPEAKER_PROCESS_STATUS_IN_TTS = 3;
    /**
     * Status tts complete
     */
    public final static int SPEAKER_PROCESS_STATUS_TTS_COMPLETE = 4;
    /**
     * Status in file
     */
    public final static int SPEAKER_PROCESS_STATUS_IN_FILE = 5;
    /**
     * Status file complete
     */
    public final static int SPEAKER_PROCESS_STATUS_FILE_COMPLETE = 6;
    /**
     * Status in delay
     */
    public final static int SPEAKER_PROCESS_STATUS_IN_DELAY = 7;
    /**
     * Status delay complete
     */
    public final static int SPEAKER_PROCESS_STATUS_DELAY_COMPLETE = 8;
    /**
     * Status complete
     */
    public final static int SPEAKER_PROCESS_STATUS_COMPLETE = 9;
    private             int             mSpeakerStatus                      = SPEAKER_PROCESS_STATUS_NOT_STARTED;
    private             long            mDelayEnd                         = 0L;
    private             int             mRepeatCount                        = 0;
    private final       SpeakerCommand  mSpeakerCommand;

    /**
     * Create a speaker process block
     * @param timeoutInterval the timeout interval
     * @param speakerCommand the speaker command
     */
    public SpeakerProcessBlock(long timeoutInterval, SpeakerCommand speakerCommand)
    {
        super(timeoutInterval, "Speaker");
        mSpeakerCommand = speakerCommand;
        setToDefault();
    }

    /**
     * Set the properties to default
     */
    public final void setToDefault()
    {
        mSpeakerStatus      = SPEAKER_PROCESS_STATUS_NOT_STARTED;
        mDelayEnd  = 0L;
        mRepeatCount = 0;
    }

    /**
     * Get the speaker status
     * @return the speaker status
     */
    public int getSpeakerStatus()
    {
        return mSpeakerStatus;
    }

    /**
     * Set the speaker status
     * @param speakerStatus the speaker status
     */
    public void setSpeakerStatus(int speakerStatus)
    {
        mSpeakerStatus = speakerStatus;
    }

    /**
     * Get the delay end
     * @return the delay end
     */
    public long getDelayEnd()
    {
        return mDelayEnd;
    }

    /**
     * Set the delay end
     * @param delayEnd the delay end
     */
    public void setDelayEnd(long delayEnd)
    {
        this.mDelayEnd = delayEnd;
    }

    /**
     * Get the repeating count
     * @return the repeating count
     */
    public int getRepeatCount()
    {
        return mRepeatCount;
    }

    /**
     * Set the repeating count
     * @param repeatCount the repeating count
     */
    @SuppressWarnings("unused")
    public void setRepeatCount(int repeatCount)
    {
        this.mRepeatCount = repeatCount;
    }

    /**
     * Increase the repeating count by one
     */
    public void increaseRepeatCount()
    {
        this.mRepeatCount++;
    }

    /**
     * Get the speaker command
     * @return the speaker command
     */
    @SuppressWarnings("unused")
    public SpeakerCommand getSpeakerCommand()
    {
        return mSpeakerCommand;
    }
    
}
