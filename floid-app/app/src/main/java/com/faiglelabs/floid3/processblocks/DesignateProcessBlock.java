/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for designate command
 */
public class DesignateProcessBlock extends ProcessBlock {
    /**
     * Create a new designate process block
     * @param timeoutInterval the timeout interval
     */
    public DesignateProcessBlock(@SuppressWarnings("SameParameterValue") long timeoutInterval) {
        super(timeoutInterval, "Designate");
    }
}
