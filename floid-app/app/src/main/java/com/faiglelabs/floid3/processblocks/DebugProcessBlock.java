/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Debug command process block
 */
public class DebugProcessBlock extends ProcessBlock
{
    /**
     * Create a new debug process block
     * @param timeoutInterval the timeout interval
     */
    public DebugProcessBlock(@SuppressWarnings("SameParameterValue") long timeoutInterval)
    {
        super(timeoutInterval, "Debug");
    }
}
