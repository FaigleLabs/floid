/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for set parameters command
 */
@SuppressWarnings("SameParameterValue")
public class SetParametersProcessBlock extends ProcessBlock {
    /**
     * Create a new set parameters process block
     * @param timeoutInterval the timeout interval
     */
    public SetParametersProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Set Parameters");
    }
}
