/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.HeightToCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.HeightToProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for height to command
 */
public class HeightToCommandProcessor extends InstructionProcessor
{
    /**
     * Create a height to command processor
     * @param floidService the floid service
     * @param heightToCommand the height to command
     */
    @SuppressWarnings("WeakerAccess")
    public HeightToCommandProcessor(FloidService floidService, HeightToCommand heightToCommand)
    {
        super(floidService, heightToCommand);
        useDefaultGoalProcessing = true;
        completeOnCommandResponse = false;
        setProcessBlock(new HeightToProcessBlock(noGoalTimeout));
    }
    @Override
    public boolean setupInstruction()
    {
        super.setupInstruction();
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
        if(!floidService.getFloidDroidStatus().isLiftedOff())
        {
            // Logic error - must be lifted off to HeightTo:
            sendCommandLogicErrorMessage("Not lifted off.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
        }
        // Log it:
        if(floidService.mUseLogger) Log.d(FloidService.TAG, "===>SETUP " + this.getDroidInstruction().getType());
        HeightToCommand heightToCommand = (HeightToCommand)droidInstruction;
        HeightToProcessBlock heightToProcessBlock = (HeightToProcessBlock)processBlock;
        heightToProcessBlock.setFloidGoalId(floidService.getNextFloidGoalId());
        try
        {
            FloidOutgoingCommand heightToOutgoingCommand = floidService.sendHeightToCommandToFloid(heightToProcessBlock.getFloidGoalId(), (float)heightToCommand.getZ());
            if(heightToOutgoingCommand != null)
            {
                heightToProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                heightToProcessBlock.setProcessBlockWaitForCommandNumber(heightToOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Height To command.");
                // We must have failed - set completion with error:
                heightToProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;  // We are done with this whole command
    }
    @Override
    public boolean tearDownInstruction()
    {
        super.tearDownInstruction();
        return true;
    }
}
