/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Deploy parachute process block
 */
public class DeployParachuteProcessBlock extends ProcessBlock
{
    /**
     * Create a new deploy parachute process block
     * @param timeoutInterval the timeout interval
     */
    public DeployParachuteProcessBlock(@SuppressWarnings("SameParameterValue") long timeoutInterval)
    {
        super(timeoutInterval, "Deploy Parachute");
    }
}
