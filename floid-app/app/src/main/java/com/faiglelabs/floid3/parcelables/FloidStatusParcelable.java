package com.faiglelabs.floid3.parcelables;

import android.os.Parcel;
import android.os.Parcelable;
import com.faiglelabs.floid.servertypes.statuses.FloidStatus;
import org.jetbrains.annotations.NotNull;

/**
 * Parcelable version of Floid Status
 */
public class FloidStatusParcelable extends FloidStatus implements Parcelable {

    /**
     * Parcelable version of FloidStatus
     */
    public FloidStatusParcelable() {
        super();
        type=this.getClass().getSuperclass().getName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.std);
        dest.writeInt(this.st);
        dest.writeInt(this.sn);
        dest.writeInt(this.fm);
        dest.writeInt(this.ffm);
        dest.writeByte(this.fmf ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dc ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dg ? (byte) 1 : (byte) 0);
        dest.writeByte(this.df ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dv ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dp ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dd ? (byte) 1 : (byte) 0);
        dest.writeByte(this.da ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dl ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dh ? (byte) 1 : (byte) 0);
        dest.writeByte(this.de ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.kd);
        dest.writeDouble(this.kv);
        dest.writeDouble(this.ka);
        dest.writeDouble(this.kw);
        dest.writeDouble(this.kc);
        dest.writeInt(this.jt);
        dest.writeDouble(this.jx);
        dest.writeDouble(this.jy);
        dest.writeInt(this.jf);
        dest.writeDouble(this.jm);
        dest.writeDouble(this.jz);
        dest.writeDouble(this.jh);
        dest.writeInt(this.js);
        dest.writeDouble(this.jxf);
        dest.writeDouble(this.jyf);
        dest.writeDouble(this.jmf);
        dest.writeInt(this.jg);
        dest.writeInt(this.jr);
        dest.writeDouble(this.ph);
        dest.writeDouble(this.phs);
        dest.writeDouble(this.phv);
        dest.writeDouble(this.pz);
        dest.writeDouble(this.pzs);
        dest.writeDouble(this.pt);
        dest.writeDouble(this.pp);
        dest.writeDouble(this.pps);
        dest.writeByte(this.pv ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.pr);
        dest.writeDouble(this.prs);
        dest.writeDouble(this.ppv);
        dest.writeDouble(this.prv);
        dest.writeDouble(this.pzv);
        dest.writeByte(this.pd ? (byte) 1 : (byte) 0);
        dest.writeInt(this.par);
        dest.writeInt(this.pg);
        dest.writeInt(this.pe);
        dest.writeDouble(this.bm);
        dest.writeDouble(this.b0);
        dest.writeDouble(this.b1);
        dest.writeDouble(this.b2);
        dest.writeDouble(this.b3);
        dest.writeDouble(this.ba);
        dest.writeDouble(this.c0);
        dest.writeDouble(this.c1);
        dest.writeDouble(this.c2);
        dest.writeDouble(this.c3);
        dest.writeDouble(this.ca);
        dest.writeByte(this.s0o ? (byte) 1 : (byte) 0);
        dest.writeByte(this.s1o ? (byte) 1 : (byte) 0);
        dest.writeByte(this.s2o ? (byte) 1 : (byte) 0);
        dest.writeByte(this.s3o ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.s00);
        dest.writeDouble(this.s01);
        dest.writeDouble(this.s02);
        dest.writeDouble(this.s10);
        dest.writeDouble(this.s11);
        dest.writeDouble(this.s12);
        dest.writeDouble(this.s20);
        dest.writeDouble(this.s21);
        dest.writeDouble(this.s22);
        dest.writeDouble(this.s30);
        dest.writeDouble(this.s31);
        dest.writeDouble(this.s32);
        dest.writeInt(this.et);
        dest.writeByte(this.e0o ? (byte) 1 : (byte) 0);
        dest.writeByte(this.e1o ? (byte) 1 : (byte) 0);
        dest.writeByte(this.e2o ? (byte) 1 : (byte) 0);
        dest.writeByte(this.e3o ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.e0);
        dest.writeDouble(this.e1);
        dest.writeDouble(this.e2);
        dest.writeDouble(this.e3);
        dest.writeInt(this.ep0);
        dest.writeInt(this.ep1);
        dest.writeInt(this.ep2);
        dest.writeInt(this.ep3);
        dest.writeByte(this.ghg ? (byte) 1 : (byte) 0);
        dest.writeInt(this.gid);
        dest.writeInt(this.gt);
        dest.writeInt(this.gs);
        dest.writeDouble(this.gx);
        dest.writeDouble(this.gy);
        dest.writeDouble(this.gz);
        dest.writeDouble(this.ga);
        dest.writeByte(this.gif ? (byte) 1 : (byte) 0);
        dest.writeByte(this.glo ? (byte) 1 : (byte) 0);
        dest.writeByte(this.gld ? (byte) 1 : (byte) 0);
        dest.writeInt(this.t0m);
        dest.writeInt(this.t1m);
        dest.writeInt(this.t2m);
        dest.writeInt(this.t3m);
        dest.writeDouble(this.t0x);
        dest.writeDouble(this.t0y);
        dest.writeDouble(this.t0z);
        dest.writeDouble(this.t1x);
        dest.writeDouble(this.t1y);
        dest.writeDouble(this.t1z);
        dest.writeDouble(this.t2x);
        dest.writeDouble(this.t2y);
        dest.writeDouble(this.t2z);
        dest.writeDouble(this.t3x);
        dest.writeDouble(this.t3y);
        dest.writeDouble(this.t3z);
        dest.writeByte(this.t0o ? (byte) 1 : (byte) 0);
        dest.writeByte(this.t1o ? (byte) 1 : (byte) 0);
        dest.writeByte(this.t2o ? (byte) 1 : (byte) 0);
        dest.writeByte(this.t3o ? (byte) 1 : (byte) 0);
        dest.writeInt(this.t0p);
        dest.writeInt(this.t0t);
        dest.writeInt(this.t1p);
        dest.writeInt(this.t1t);
        dest.writeInt(this.t2p);
        dest.writeInt(this.t2t);
        dest.writeInt(this.t3p);
        dest.writeInt(this.t3t);
        dest.writeByte(this.y0 ? (byte) 1 : (byte) 0);
        dest.writeByte(this.y1 ? (byte) 1 : (byte) 0);
        dest.writeByte(this.y2 ? (byte) 1 : (byte) 0);
        dest.writeByte(this.y3 ? (byte) 1 : (byte) 0);
        dest.writeInt(this.yn0);
        dest.writeInt(this.yn1);
        dest.writeInt(this.yn2);
        dest.writeInt(this.yn3);
        dest.writeInt(this.yg0);
        dest.writeInt(this.yg1);
        dest.writeInt(this.yg2);
        dest.writeInt(this.yg3);
        dest.writeByte(this.vd ? (byte) 1 : (byte) 0);
        dest.writeByte(this.va ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vx ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vb ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vc ? (byte) 1 : (byte) 0);
        dest.writeByte(this.ve ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vg ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vh ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vm ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vt ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vy ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vp ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vl ? (byte) 1 : (byte) 0);
        dest.writeByte(this.vs ? (byte) 1 : (byte) 0);
        dest.writeByte(this.ai ? (byte) 1 : (byte) 0);
        dest.writeByte(this.av ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.aa);
        dest.writeDouble(this.ad);
        dest.writeInt(this.ac);
        dest.writeInt(this.lc);
        dest.writeDouble(this.lr);
        dest.writeInt(this.sr);
        dest.writeDouble(this.mt);
        dest.writeDouble(this.md);
        dest.writeDouble(this.ma);
        dest.writeDouble(this.mo);
        dest.writeDouble(this.mg);
        dest.writeDouble(this.mk);
        dest.writeDouble(this.oop);
        dest.writeDouble(this.ooq);
        dest.writeDouble(this.oor);
        dest.writeDouble(this.oos);
        dest.writeDouble(this.opv);
        dest.writeDouble(this.opw);
        dest.writeDouble(this.orv);
        dest.writeDouble(this.orw);
        dest.writeDouble(this.oav);
        dest.writeDouble(this.oaw);
        dest.writeDouble(this.ohv);
        dest.writeDouble(this.ohw);
        dest.writeDouble(this.ov);
        dest.writeByte(this.lmc ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.lma);
        dest.writeInt(this.lmt);
        dest.writeByte(this.hsm ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dpy ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dba ? (byte) 1 : (byte) 0);
        dest.writeByte(this.rm);
        dest.writeByte(this.tm);
        dest.writeValue(this.id);
        dest.writeString(this.floidUuid);
        dest.writeString(this.type);
        dest.writeInt(this.floidId);
        dest.writeValue(this.floidMessageNumber);
        dest.writeInt(this.ias);
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected FloidStatusParcelable(Parcel in) {
        this.std = in.readLong();
        this.st = in.readInt();
        this.sn = in.readInt();
        this.fm = in.readInt();
        this.ffm = in.readInt();
        this.fmf = in.readByte() != 0;
        this.dc = in.readByte() != 0;
        this.dg = in.readByte() != 0;
        this.df = in.readByte() != 0;
        this.dv = in.readByte() != 0;
        this.dp = in.readByte() != 0;
        this.dd = in.readByte() != 0;
        this.da = in.readByte() != 0;
        this.dl = in.readByte() != 0;
        this.dh = in.readByte() != 0;
        this.de = in.readByte() != 0;
        this.kd = in.readDouble();
        this.kv = in.readDouble();
        this.ka = in.readDouble();
        this.kw = in.readDouble();
        this.kc = in.readDouble();
        this.jt = in.readInt();
        this.jx = in.readDouble();
        this.jy = in.readDouble();
        this.jf = in.readInt();
        this.jm = in.readDouble();
        this.jz = in.readDouble();
        this.jh = in.readDouble();
        this.js = in.readInt();
        this.jxf = in.readDouble();
        this.jyf = in.readDouble();
        this.jmf = in.readDouble();
        this.jg = in.readInt();
        this.jr = in.readInt();
        this.ph = in.readDouble();
        this.phs = in.readDouble();
        this.phv = in.readDouble();
        this.pz = in.readDouble();
        this.pzs = in.readDouble();
        this.pt = in.readDouble();
        this.pp = in.readDouble();
        this.pps = in.readDouble();
        this.pv = in.readByte() != 0;
        this.pr = in.readDouble();
        this.prs = in.readDouble();
        this.ppv = in.readDouble();
        this.prv = in.readDouble();
        this.pzv = in.readDouble();
        this.pd = in.readByte() != 0;
        this.par = in.readInt();
        this.pg = in.readInt();
        this.pe = in.readInt();
        this.bm = in.readDouble();
        this.b0 = in.readDouble();
        this.b1 = in.readDouble();
        this.b2 = in.readDouble();
        this.b3 = in.readDouble();
        this.ba = in.readDouble();
        this.c0 = in.readDouble();
        this.c1 = in.readDouble();
        this.c2 = in.readDouble();
        this.c3 = in.readDouble();
        this.ca = in.readDouble();
        this.s0o = in.readByte() != 0;
        this.s1o = in.readByte() != 0;
        this.s2o = in.readByte() != 0;
        this.s3o = in.readByte() != 0;
        this.s00 = in.readDouble();
        this.s01 = in.readDouble();
        this.s02 = in.readDouble();
        this.s10 = in.readDouble();
        this.s11 = in.readDouble();
        this.s12 = in.readDouble();
        this.s20 = in.readDouble();
        this.s21 = in.readDouble();
        this.s22 = in.readDouble();
        this.s30 = in.readDouble();
        this.s31 = in.readDouble();
        this.s32 = in.readDouble();
        this.et = in.readInt();
        this.e0o = in.readByte() != 0;
        this.e1o = in.readByte() != 0;
        this.e2o = in.readByte() != 0;
        this.e3o = in.readByte() != 0;
        this.e0 = in.readDouble();
        this.e1 = in.readDouble();
        this.e2 = in.readDouble();
        this.e3 = in.readDouble();
        this.ep0 = in.readInt();
        this.ep1 = in.readInt();
        this.ep2 = in.readInt();
        this.ep3 = in.readInt();
        this.ghg = in.readByte() != 0;
        this.gid = in.readInt();
        this.gt = in.readInt();
        this.gs = in.readInt();
        this.gx = in.readDouble();
        this.gy = in.readDouble();
        this.gz = in.readDouble();
        this.ga = in.readDouble();
        this.gif = in.readByte() != 0;
        this.glo = in.readByte() != 0;
        this.gld = in.readByte() != 0;
        this.t0m = in.readInt();
        this.t1m = in.readInt();
        this.t2m = in.readInt();
        this.t3m = in.readInt();
        this.t0x = in.readDouble();
        this.t0y = in.readDouble();
        this.t0z = in.readDouble();
        this.t1x = in.readDouble();
        this.t1y = in.readDouble();
        this.t1z = in.readDouble();
        this.t2x = in.readDouble();
        this.t2y = in.readDouble();
        this.t2z = in.readDouble();
        this.t3x = in.readDouble();
        this.t3y = in.readDouble();
        this.t3z = in.readDouble();
        this.t0o = in.readByte() != 0;
        this.t1o = in.readByte() != 0;
        this.t2o = in.readByte() != 0;
        this.t3o = in.readByte() != 0;
        this.t0p = in.readInt();
        this.t0t = in.readInt();
        this.t1p = in.readInt();
        this.t1t = in.readInt();
        this.t2p = in.readInt();
        this.t2t = in.readInt();
        this.t3p = in.readInt();
        this.t3t = in.readInt();
        this.y0 = in.readByte() != 0;
        this.y1 = in.readByte() != 0;
        this.y2 = in.readByte() != 0;
        this.y3 = in.readByte() != 0;
        this.yn0 = in.readInt();
        this.yn1 = in.readInt();
        this.yn2 = in.readInt();
        this.yn3 = in.readInt();
        this.yg0 = in.readInt();
        this.yg1 = in.readInt();
        this.yg2 = in.readInt();
        this.yg3 = in.readInt();
        this.vd = in.readByte() != 0;
        this.va = in.readByte() != 0;
        this.vx = in.readByte() != 0;
        this.vb = in.readByte() != 0;
        this.vc = in.readByte() != 0;
        this.ve = in.readByte() != 0;
        this.vg = in.readByte() != 0;
        this.vh = in.readByte() != 0;
        this.vm = in.readByte() != 0;
        this.vt = in.readByte() != 0;
        this.vy = in.readByte() != 0;
        this.vp = in.readByte() != 0;
        this.vl = in.readByte() != 0;
        this.vs = in.readByte() != 0;
        this.ai = in.readByte() != 0;
        this.av = in.readByte() != 0;
        this.aa = in.readDouble();
        this.ad = in.readDouble();
        this.ac = in.readInt();
        this.lc = in.readInt();
        this.lr = in.readDouble();
        this.sr = in.readInt();
        this.mt = in.readDouble();
        this.md = in.readDouble();
        this.ma = in.readDouble();
        this.mo = in.readDouble();
        this.mg = in.readDouble();
        this.mk = in.readDouble();
        this.oop = in.readDouble();
        this.ooq = in.readDouble();
        this.oor = in.readDouble();
        this.oos = in.readDouble();
        this.opv = in.readDouble();
        this.opw = in.readDouble();
        this.orv = in.readDouble();
        this.orw = in.readDouble();
        this.oav = in.readDouble();
        this.oaw = in.readDouble();
        this.ohv = in.readDouble();
        this.ohw = in.readDouble();
        this.ov = in.readDouble();
        this.lmc = in.readByte() != 0;
        this.lma = in.readDouble();
        this.lmt = in.readInt();
        this.hsm = in.readByte() != 0;
        this.dpy = in.readByte() != 0;
        this.dba = in.readByte() != 0;
        this.rm = in.readByte();
        this.tm = in.readByte();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.floidUuid = in.readString();
        this.type = in.readString();
        this.floidId = in.readInt();
        this.floidMessageNumber = (Long) in.readValue(Long.class.getClassLoader());
        this.ias = in.readInt();
    }

    /**
     * The creator
     */
    public static final Creator<FloidStatusParcelable> CREATOR = new Creator<FloidStatusParcelable>() {
        @Override
        public FloidStatusParcelable createFromParcel(Parcel source) {
            return new FloidStatusParcelable(source);
        }

        @Override
        public FloidStatusParcelable[] newArray(int size) {
            return new FloidStatusParcelable[size];
        }
    };

    @NotNull
    @Override
    public FloidStatusParcelable clone() throws CloneNotSupportedException {
        return (FloidStatusParcelable) super.clone();
    }
}
