/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.controller;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.faiglelabs.floid3.FloidActivity;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;

/**
 * Floid button controller
 */
public class FloidButtonController implements OnClickListener {
    private final FloidActivity mFloidActivity;
    private final int mCommand;
    private final int mTarget;
    @SuppressWarnings("FieldCanBeLocal")
    private final Button mButton;
    private static final String TAG = "FloidButtonController";


    /**
     * Create a floid button controller
     *
     * @param floidActivity the floid activity
     * @param command       the command
     * @param target        the target
     * @param button        the button
     * @param labelText     the label text
     */
    public FloidButtonController(FloidActivity floidActivity, int command, int target, Button button, String labelText) {
        mFloidActivity = floidActivity;
        mCommand = command;
        mTarget = target;
        mButton = button;
        mButton.setText(labelText);
        mButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mFloidActivity != null) {
            switch (mCommand) {
                case FloidService.TEST_PACKET: {
                    try {
                        FloidOutgoingCommand testOutgoingCommand = new FloidOutgoingCommand(FloidService.TEST_PACKET, FloidService.TEST_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                        mFloidActivity.sendCommandToFloidService(testOutgoingCommand);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Test Packet Command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                }
                break;
                case FloidService.RELAY_PACKET: {
                    try {
                        mFloidActivity.sendRelayCommandToFloidService((byte) mTarget, (byte) FloidService.RELAY_DEVICE_STATE_ON);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Relay Packet Command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                }
                break;
                case FloidService.MODEL_PARAMETERS_1_PACKET: {
                    // This is the token to send all the parameters - not just the first set but no point in having another token since this one is unique with these others in this switch
                    // Model Parameters 1:
                    try {
                        FloidOutgoingCommand floidModelParameters1OutgoingCommand = new FloidOutgoingCommand(FloidService.MODEL_PARAMETERS_1_PACKET, FloidService.MODEL_PARAMETERS_1_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                        mFloidActivity.makeModelParameters1Command(floidModelParameters1OutgoingCommand);
                        mFloidActivity.sendCommandToFloidService(floidModelParameters1OutgoingCommand);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Model Parameters 1 Command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }

                    // Model Parameters 2:
                    try {
                        FloidOutgoingCommand floidModelParameters2OutgoingCommand = new FloidOutgoingCommand(FloidService.MODEL_PARAMETERS_2_PACKET, FloidService.MODEL_PARAMETERS_2_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                        mFloidActivity.makeModelParameters2Command(floidModelParameters2OutgoingCommand);
                        mFloidActivity.sendCommandToFloidService(floidModelParameters2OutgoingCommand);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Model Parameters 2 Command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                    // Model Parameters 3:
                    try {
                        FloidOutgoingCommand floidModelParameters3OutgoingCommand = new FloidOutgoingCommand(FloidService.MODEL_PARAMETERS_3_PACKET, FloidService.MODEL_PARAMETERS_3_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                        mFloidActivity.makeModelParameters3Command(floidModelParameters3OutgoingCommand);
                        mFloidActivity.sendCommandToFloidService(floidModelParameters3OutgoingCommand);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Model Parameters 3 Command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                    // Model Parameters 4:
                    try {
                        FloidOutgoingCommand floidModelParameters4OutgoingCommand = new FloidOutgoingCommand(FloidService.MODEL_PARAMETERS_4_PACKET, FloidService.MODEL_PARAMETERS_4_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                        mFloidActivity.makeModelParameters4Command(floidModelParameters4OutgoingCommand);
                        mFloidActivity.sendCommandToFloidService(floidModelParameters4OutgoingCommand);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Model Parameters 4 Command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                }
                break;
                case FloidService.TEST_GOALS_PACKET: {
                    try {
                        FloidOutgoingCommand floidTestGoalsOutgoingCommand = new FloidOutgoingCommand(FloidService.TEST_GOALS_PACKET, FloidService.TEST_GOALS_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                        mFloidActivity.makeTestGoalsCommand(floidTestGoalsOutgoingCommand);
                        mFloidActivity.sendCommandToFloidService(floidTestGoalsOutgoingCommand);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Test Goals Command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                }
                break;
                case FloidService.GET_MODEL_PARAMETERS_PACKET: {
                    try {
                        FloidOutgoingCommand floidGetModelParametersOutgoingCommand = new FloidOutgoingCommand(FloidService.GET_MODEL_PARAMETERS_PACKET, FloidService.GET_MODEL_PARAMETERS_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                        mFloidActivity.sendCommandToFloidService(floidGetModelParametersOutgoingCommand);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Get Model Parameters Command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                }
                break;
                default: {
                    // Do nothing
                }
                break;
            }
        }
    }
}
