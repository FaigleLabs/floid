/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import com.faiglelabs.floid.servertypes.commands.PhotoStrobeCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.PhotoStrobeProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for the photo strobe command
 */
@SuppressWarnings("unused")
public class PhotoStrobeCommandProcessor extends InstructionProcessor {
	/**
	 * Create a photo strobe command processor
	 *
	 * @param floidService       the floid service
	 * @param photoStrobeCommand the photo strobe command
	 */
	@SuppressWarnings("WeakerAccess")
	public PhotoStrobeCommandProcessor(FloidService floidService, PhotoStrobeCommand photoStrobeCommand) {
		super(floidService, photoStrobeCommand);
		useDefaultGoalProcessing = true; // We are good with default goal processing
		completeOnCommandResponse = false;
		stopMission = false; // We do kill the mission if this command fails
		setProcessBlock(new PhotoStrobeProcessBlock(defaultGoalTimeout));
	}

	@Override
	public boolean setupInstruction() {
		// General idea:
		// Set up to take photo - turn on isTakingPhoto and then turn it off it failure or if picture is taken in callback...
		// Create file name here based on missionNumber_dateTime_photoNumber
		super.setupInstruction();
		if (floidService.getFloidDroidStatus().isPhotoStrobe()) {
			// Logic error - can't take photo strobe if already in photoStrobe
			sendCommandLogicErrorMessage("Already taking photo strobe.");
			processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
			return true;       // we are done but this is ignored above anyway and processInstruction is still called
		}
		if (floidService.getFloidDroidStatus().isTakingPhoto()) {
			// Logic error - can't take two photos at once
			sendCommandLogicErrorMessage("Already taking photo.");
			processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
			return true;       // we are done but this is ignored above anyway and processInstruction is still called
		}
		// OK, passed all tests...take a photo by telling the interface to take a photo...
		PhotoStrobeCommand photoStrobeCommand = (PhotoStrobeCommand) droidInstruction;
		floidService.getFloidDroidStatus().setPhotoStrobe(true);
		floidService.getFloidDroidStatus().setPhotoStrobeDelay(photoStrobeCommand.getDelay());
		floidService.serviceSendTakePhoto();
		processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
		return true;    // We are done with this whole command
	}

	@Override
	public boolean processInstruction() {
		if (super.processInstruction()) {
			return true;
		}
		// DONE?
		if (!floidService.getFloidDroidStatus().isPhotoStrobe())      // OK, kind of a dummy...we set it above directly...
		{
			processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
			return true;
		}
		return false;
	}

	@Override
	public boolean tearDownInstruction() {
		super.tearDownInstruction();
		return true;
	}
}
