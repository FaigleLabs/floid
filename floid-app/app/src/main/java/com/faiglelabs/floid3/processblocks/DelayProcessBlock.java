/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Delay command process block
 */
public class DelayProcessBlock extends ProcessBlock
{
    private long startTime = 0L;

    /**
     * Create a new delay process block
     * @param timeoutInterval the timeout interval
     */
    public DelayProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Delay");
    }
    /**
     * Get the start time
     * @return the start time
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * Set the start time
     * @param startTime the start time
     */
    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
}
