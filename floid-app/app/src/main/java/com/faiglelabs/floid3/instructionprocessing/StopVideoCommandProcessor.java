/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import com.faiglelabs.floid.servertypes.commands.StopVideoCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.StopVideoProcessBlock;

/**
 * Command processor for the stop video command processor
 */
public class StopVideoCommandProcessor extends InstructionProcessor
{
	/**
	 * Create a stop video command processor
	 * @param floidService the floid service
	 * @param stopVideoCommand the stop video command
	 */
	@SuppressWarnings("WeakerAccess")
	public StopVideoCommandProcessor(FloidService floidService, StopVideoCommand stopVideoCommand)
	{
		super(floidService, stopVideoCommand);
		useDefaultGoalProcessing = true;
		completeOnCommandResponse = false;
		setProcessBlock(new StopVideoProcessBlock(defaultGoalTimeout));
	}
	@Override
	public boolean setupInstruction()
	{
	    // General idea:
	    // Set up to take photo - turn on isTakingPhoto and then turn it off it failure or if picture is taken in callback...
	    // Create file name here based on missionNumber_dateTime_photoNumber
	    super.setupInstruction();
        // Set completed status:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
        return false;    // We are not done with this command
	}
	@Override
	public boolean processInstruction()
	{
		if(!floidService.getFloidDroidStatus().isTakingVideo())
		{
			// Logic error - can't take video if already taking video
			sendCommandLogicErrorMessage("Not taking video.");
			processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
		} else {
			// Tell the service we want to stop taking video:
			floidService.serviceSendStopVideo();
			processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
		}
	    return true;
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
