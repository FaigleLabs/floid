/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import com.faiglelabs.floid.servertypes.commands.TakePhotoCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.TakePhotoProcessBlock;

/**
 * Command processor for the take photo command
 */
public class TakePhotoCommandProcessor extends InstructionProcessor {
	/**
	 * Create a take photo command processor
	 *
	 * @param floidService     the floid service
	 * @param takePhotoCommand the take photo command
	 */
	@SuppressWarnings("WeakerAccess")
	public TakePhotoCommandProcessor(FloidService floidService, TakePhotoCommand takePhotoCommand) {
		super(floidService, takePhotoCommand);
		useDefaultGoalProcessing = true;
		completeOnCommandResponse = false;
		setProcessBlock(new TakePhotoProcessBlock(defaultGoalTimeout));
	}

	@Override
	public boolean setupInstruction() {
		// General idea:
		// Set up to take photo - turn on isTakingPhoto and then turn it off it failure or if picture is taken in callback...
		// Create file name here based on missionNumber_dateTime_photoNumber
		super.setupInstruction();
		if (floidService.getFloidDroidStatus().isPhotoStrobe()) {
			// Logic error - can't take photo if in photo strobe:
			sendCommandLogicErrorMessage("Already taking photo strobe.");
			processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
			return true;       // we are done but this is ignored above anyway and processInstruction is still called
		}
		if (floidService.getFloidDroidStatus().isTakingPhoto()) {
			// Logic error - can't take two photos at once
			sendCommandLogicErrorMessage("Already taking photo.");
			processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
			return true;       // we are done but this is ignored above anyway and processInstruction is still called
		}
		// OK, passed all tests...take a photo by telling the interface to take a photo...
		floidService.serviceSendTakePhoto();
		// And set the status to in progress:
		processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
		return true;    // We are done with this whole command
	}

	@Override
	public boolean processInstruction() {
		if (super.processInstruction()) {
			return true;
		}
		// DONE?
		if (!floidService.getFloidDroidStatus().isTakingPhoto()) {
			processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
			return true;
		}
		return false;
	}

	@Override
	public boolean tearDownInstruction() {
		super.tearDownInstruction();
		return true;
	}
}
