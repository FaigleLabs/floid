/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.processblocks;

import com.faiglelabs.floid.servertypes.commands.SpeakerOffCommand;

/**
 * Process block for speaker off command
 */
@SuppressWarnings("SameParameterValue")
public class SpeakerOffProcessBlock extends ProcessBlock {
    private final       SpeakerOffCommand  mSpeakerOffCommand;
    /**
     * Create a new speaker off process block
     * @param timeoutInterval the timeout interval
     * @param speakerOffCommand the speaker off command
     */
    public SpeakerOffProcessBlock(long timeoutInterval, SpeakerOffCommand speakerOffCommand) {
        super(timeoutInterval, "Speaker Off");
        mSpeakerOffCommand = speakerOffCommand;
    }

    /**
     * Get the speaker off command
     * @return the speaker off command
     */
    @SuppressWarnings("unused")
    public SpeakerOffCommand getSpeakerOffCommand() {
        return mSpeakerOffCommand;
    }
}
