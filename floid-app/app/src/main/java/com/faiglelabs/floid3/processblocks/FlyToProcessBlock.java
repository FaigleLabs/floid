/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for fly to command
 */
@SuppressWarnings("SameParameterValue")
public class FlyToProcessBlock extends ProcessBlock {
    /**
     * Create a new fly to process block
     * @param timeoutInterval the timeout interval
     */
    public FlyToProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Fly To");
    }
}
