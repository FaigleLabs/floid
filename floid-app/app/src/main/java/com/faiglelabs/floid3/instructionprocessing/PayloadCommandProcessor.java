/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.PayloadCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.PayloadProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for the payload command
 */
public class PayloadCommandProcessor extends InstructionProcessor
{
    /**
     * Create a payload command processor
     * @param floidService the floid service
     * @param payloadCommand the payload command
     */
    @SuppressWarnings("WeakerAccess")
	public PayloadCommandProcessor(FloidService floidService, PayloadCommand payloadCommand)
	{
		super(floidService, payloadCommand);
		useDefaultGoalProcessing  = false;
		completeOnCommandResponse = false;
		setProcessBlock(new PayloadProcessBlock(defaultGoalTimeout));
	}
	@Override
	public boolean setupInstruction()
	{
	    // General idea:
	    // This is generally the same as flyTo and turnTo...
	    super.setupInstruction();
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
	    if(!floidService.getFloidDroidStatus().isLiftedOff())
	    {
	        // Logic error - must be lifted off to drop payload:
            sendCommandLogicErrorMessage("Not lifted off.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
	    }
	    // OK, passed all tests...
        PayloadCommand      payloadCommand      = (PayloadCommand)droidInstruction;
        PayloadProcessBlock payloadProcessBlock = (PayloadProcessBlock)processBlock;
        payloadProcessBlock.setFloidGoalId(FloidService.NO_GOAL_ID);  // Does not use a goal id
        try
        {
            FloidOutgoingCommand payloadOutgoingCommand = floidService.sendPayloadCommandToFloid(payloadProcessBlock.getFloidGoalId(), (byte)(payloadCommand.getBay()&0x000000ff));
            if(payloadOutgoingCommand != null)
            {
                payloadProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                payloadProcessBlock.setProcessBlockWaitForCommandNumber(payloadOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Payload command.");
                // We must have failed - set completion with error:
                payloadProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;    // We are done with this whole command
	}
	@Override
	public boolean processInstruction()
	{
	    if(super.processInstruction())
	    {
	        return true;
	    }
	    if(processBlock.getProcessBlockStatus() == ProcessBlock.PROCESS_BLOCK_COMMAND_RESPONSE_RECEIVED)
	    {
	        // We had a good command receipt - change status:
	        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
	    }
	    // Get the bay, the goal state for that bay and see if we have completed - this overrides the default processing we turned off above
	    PayloadCommand payloadCommand      = (PayloadCommand)droidInstruction;
	    int            payloadBay          = payloadCommand.getBay();
	    int            payloadBayGoalState;
	    switch(payloadBay)
	    {
	        case 0:
	        {
	            payloadBayGoalState = floidService.getFloidStatus().getYg0();
	        }
	        break;
            case 1:
            {
                payloadBayGoalState = floidService.getFloidStatus().getYg1();
            }
            break;
            case 2:
            {
                payloadBayGoalState = floidService.getFloidStatus().getYg2();
            }
            break;
            case 3:
            {
                payloadBayGoalState = floidService.getFloidStatus().getYg3();
            }
            break;
	        default:
	        {
	            // Error - bad bay:
	            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
	            return true;    // We are done with this whole command
	        }
	    }
	    switch(payloadBayGoalState)
	    {
            case    FloidService.GOAL_STATE_COMPLETE_SUCCESS:
            {
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                return true;  // Done
            }
            case    FloidService.GOAL_STATE_COMPLETE_ERROR:
            {
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return true;  // Done
            }
	    }
	    // We are not done:
	    return false;
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
