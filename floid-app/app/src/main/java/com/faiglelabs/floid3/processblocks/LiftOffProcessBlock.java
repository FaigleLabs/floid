/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for the lift off command
 */
@SuppressWarnings("SameParameterValue")
public class LiftOffProcessBlock extends ProcessBlock {
    /**
     * Create a new lift off process block
     * @param timeoutInterval the timeout interval
     */
    public LiftOffProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Lift Off");
    }
}
