/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.controller;

import com.faiglelabs.floid3.FloidActivity;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.helloandroid.android.horizontalslider.HorizontalSlider;
import com.helloandroid.android.horizontalslider.HorizontalSlider.OnProgressChangeListener;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Floid servo controller
 */
public class FloidServoController implements OnProgressChangeListener
{
    private static final  String            TAG    = "FloidServoController";
    private final FloidActivity mFloidActivity;
    private final         int               mCommand;
    private final         int               mTarget;
    private final         int               mSubTarget;
    private final         HorizontalSlider  mHorizontalSlider;
    private               int               progress = 0;

    /**
     * Create a new floid servo controller
     * @param floidActivity the floid activity
     * @param command the associated command
     * @param target the target
     * @param subTarget the sub target
     * @param viewId the view id
     * @param subViewId the sub view id
     * @param sliderId the slider id
     * @param labelId the label id
     * @param labelText the label text
     */
    public FloidServoController(FloidActivity floidActivity, int command, int target, int subTarget, int viewId, int subViewId, int sliderId, int labelId, String labelText)
	{
		mFloidActivity = floidActivity;
		mCommand            = command;
		mTarget             = target;
		mSubTarget          = subTarget;
        View view           = mFloidActivity.findViewById(viewId);
        ViewGroup  subView  = view.findViewById(subViewId);
        
        ((TextView)subView.findViewById(labelId)).setText(labelText);
        mHorizontalSlider = subView.findViewById(sliderId);
        mHorizontalSlider.setOnProgressChangeListener(this);
        progress = mHorizontalSlider.getProgress();

	}

    /**
     * Set the progress
     * @param progress the progress
     */
	public void setProgress(int progress)
	{
	    mHorizontalSlider.setProgress(progress);
	    this.progress =  progress;
	}


	@Override
	public void onProgressChanged(View view, int progress) {
        this.progress = progress;
    }
    @Override
    public void performClick(View view) {
	    // Note for our sliders we set the scale to 255:
        switch(mCommand)
        {
            case FloidService.PAN_TILT_PACKET:
            {
                try
                {
                    FloidOutgoingCommand panTiltOutgoingCommand = new FloidOutgoingCommand(FloidService.PAN_TILT_PACKET, FloidService.PAN_TILT_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                    panTiltOutgoingCommand.setByteToBuffer( FloidService.PAN_TILT_PACKET_DEVICE_OFFSET, (byte)mTarget);
                    panTiltOutgoingCommand.setByteToBuffer( FloidService.PAN_TILT_PACKET_TYPE_OFFSET, (byte)FloidService.PAN_TILT_PACKET_TYPE_INDIVIDUAL);
                    panTiltOutgoingCommand.setByteToBuffer( FloidService.PAN_TILT_PACKET_INDIVIDUAL_PT_OFFSET, (byte)mSubTarget);
                    panTiltOutgoingCommand.setByteToBuffer( FloidService.PAN_TILT_PACKET_INDIVIDUAL_VALUE_OFFSET, (byte)progress);  // [PAN_TILT] FIX THIS VALUE ONCE WE FIGURE OUT THE PAN/TILT IMPLEMENTATION...
                    mFloidActivity.sendCommandToFloidService(panTiltOutgoingCommand);
                }
                catch(Exception e)
                {
                    if(mFloidActivity.mFloidActivityUseLogger) Log.e(TAG, "Failed to make/send Pan/Tilt command: " + Log.getStackTraceString(e) + " " + e.toString());
                }
            }
            break;
            case FloidService.HELI_CONTROL_PACKET:
            {
                try
                {
                    FloidOutgoingCommand heliControlOutgoingCommand = new FloidOutgoingCommand(FloidService.HELI_CONTROL_PACKET, FloidService.HELI_CONTROL_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                    heliControlOutgoingCommand.setByteToBuffer( FloidService.HELI_CONTROL_PACKET_HELI_OFFSET, (byte)mTarget);
                    heliControlOutgoingCommand.setByteToBuffer( FloidService.HELI_CONTROL_PACKET_SERVO_OFFSET, (byte)mSubTarget);
                    heliControlOutgoingCommand.setFloatToBuffer(FloidService.HELI_CONTROL_PACKET_VALUE_OFFSET, (float)((float)progress/255.0));  // [PAN_TILT] FIX THIS VALUE ONCE WE FIGURE OUT THE PAN/TILT IMPLEMENTATION...
                    mFloidActivity.sendCommandToFloidService(heliControlOutgoingCommand);
                }
                catch(Exception e)
                {
                    if(mFloidActivity.mFloidActivityUseLogger) Log.e(TAG, "Failed to make/send Heli Control command: " + Log.getStackTraceString(e) + " " + e.toString());
                }
            }
            break;
            default: {
                // Do nothing
            }
            break;
        }
    }
}
