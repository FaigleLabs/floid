/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.utilities;

import android.content.Context;
import android.util.Log;
import com.faiglelabs.floid3.FloidService;
import com.physicaloid.lib.Boards;
import com.physicaloid.lib.Physicaloid;
import com.physicaloid.lib.programmer.avr.UploadErrors;

import java.io.InputStream;

// NOTE: PHYSICALOID LIBRARY FROM HERE: https://github.com/xxxajk/PhysicaloidLibrary
/**
 * Utility class to upload the controller firmware
 */
public class UploadController {
    /**
     * The constant TAG.
     */
    public static final String TAG = "FloidService";

    private FloidService mFloidService;

    /**
     * Upload the controller firmware
     * @param context the context
     * @param floidService the floid Service
     */
    public void uploadController(Context context, FloidService floidService) {
        try {
            mFloidService = floidService;
            mFloidService.killAccessoryCommunicator();
            InputStream controllerFirmwareInputStream = context.getResources().openRawResource(context.getResources().getIdentifier("firmware", "raw", context.getPackageName()));
            Physicaloid mPhysicaloid = new Physicaloid(context);
            mPhysicaloid.upload(Boards.ARDUINO_MEGA_2560_ADK, controllerFirmwareInputStream, new ControllerFirmwareUploadCallback());
        } catch (Exception e) {
            mFloidService.uxSendFloidDebugMessage("Exception uploading controller firmware:" + e.getMessage());
            Log.e(TAG, "Exception uploading controller code", e);
        }
    }

    /**
     * Upload the controller firmware
     */
    public class ControllerFirmwareUploadCallback implements Physicaloid.UploadCallBack {

        @Override
        public void onPreUpload() {
            mFloidService.uxSendFloidDebugMessage("Controller Firmware Pre-Upload");
        }

        @Override
        public void onUploading(int i) {
            mFloidService.uxSendFloidDebugMessage("Controller Firmware Uploading: " + i);
        }

        @Override
        public void onPostUpload(boolean b) {
            mFloidService.uxSendFloidDebugMessage("Controller Firmware Post-Upload: " + b);
        }

        @Override
        public void onCancel() {
            mFloidService.uxSendFloidDebugMessage("Controller Firmware Cancel");

        }

        @Override
        public void onError(UploadErrors uploadErrors) {
            mFloidService.uxSendFloidDebugMessage("Controller Firmware Errors: " + uploadErrors.getCode() + " - " + uploadErrors.getDescription());
        }
    }
}
