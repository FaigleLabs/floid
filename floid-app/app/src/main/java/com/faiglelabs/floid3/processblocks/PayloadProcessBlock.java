/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for payload commands
 */
@SuppressWarnings("SameParameterValue")
public class PayloadProcessBlock extends ProcessBlock
{
    /**
     * Create a payload process block with this timeout interval
     * @param timeoutInterval the timeout interval or ProcessBlock.COMMAND_DOES_NOT_TIMEOUT
     */
    public PayloadProcessBlock(long timeoutInterval)
    {
        super(timeoutInterval, "Payload");
    }
}
