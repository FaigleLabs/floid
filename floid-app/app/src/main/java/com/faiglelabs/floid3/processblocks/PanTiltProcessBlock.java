/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for the pan tilt command
 */
@SuppressWarnings("SameParameterValue")
public class PanTiltProcessBlock extends ProcessBlock {
    /**
     * Create a new pan tilt process block
     * @param timeoutInterval the timeout interval
     */
    public PanTiltProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Pan/Tilt");
    }
}
