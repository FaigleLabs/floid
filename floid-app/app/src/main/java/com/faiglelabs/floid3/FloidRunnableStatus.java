/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

/**
 * Status registers for floid runnable
 */
@SuppressWarnings("WeakerAccess")
public class FloidRunnableStatus {
    /**
     * Floid Droid Runnable is null status
     */
    boolean floidDroidRunnableNull = false;
    /**
     * Floid Droid Runnable is alive status
     */
    boolean floidDroidRunnableAlive = false;
    /**
     * Last Floid Droid Runnable loop count
     */
    long lastFloidDroidRunnableLoopCount = 0L;
    /**
     * Last Floid Droid Runnable update time
     */
    long lastFloidDroidRunnableUpdate = System.currentTimeMillis();
    /**
     * Floid Droid Thread warned status
     */
    boolean floidDroidThreadWarned = false;
    /**
     * Floid Droid Thread errored status
     */
    boolean floidDroidThreadErrored = false;
    /**
     * Accessory Communicator Runnable is null status
     */
    boolean accessoryCommunicatorRunnableNull = false;
    /**
     * Accessory Communicator Runnable is alive status
     */
    boolean accessoryCommunicatorRunnableAlive = false;
    /**
     * Last Accessory Communicator Runnable loop count
     */
    long lastAccessoryCommunicatorRunnableLoopCount = 0L;
    /**
     * Last Accessory Communicator Runnable update time
     */
    long lastAccessoryCommunicatorRunnableUpdate = System.currentTimeMillis();
    /**
     * Accessory Communicator Thread warned status
     */
    boolean accessoryCommunicatorThreadWarned = false;
    /**
     * Accessory Communicator Thread errored status
     */
    boolean accessoryCommunicatorThreadErrored = false;

}
