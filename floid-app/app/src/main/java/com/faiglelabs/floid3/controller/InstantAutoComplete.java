/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.controller;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

/**
 * The type Instant auto complete.
 */
public class InstantAutoComplete extends AppCompatAutoCompleteTextView {

    /**
     * Instantiates a new Instant auto complete.
     *
     * @param context the context
     */
    public InstantAutoComplete(Context context) {
        super(context);
    }

    /**
     * Instantiates a new Instant auto complete.
     *
     * @param arg0 the arg 0
     * @param arg1 the arg 1
     */
    public InstantAutoComplete(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
    }

    /**
     * Instantiates a new Instant auto complete.
     *
     * @param arg0 the arg 0
     * @param arg1 the arg 1
     * @param arg2 the arg 2
     */
    public InstantAutoComplete(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction,
                                  Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused && getAdapter() != null) {
            performFiltering(getText(), 0);
        }
    }
}
