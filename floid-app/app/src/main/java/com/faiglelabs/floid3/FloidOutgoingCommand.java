/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

//import android.util.Log;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents an outgoing command
 */
@edu.umd.cs.findbugs.annotations.SuppressFBWarnings("EI_EXPOSE_REP")
public class FloidOutgoingCommand implements Parcelable {
    private int mType;
    private int mCommandNumber;
    private int mGoalId;
    private final byte[] mCommandBuffer;

    /**
     * Create an outgoing (to Arduino) command
     *
     * @param type          the command type
     * @param size          the buffer size
     * @param commandNumber the command number
     * @param goalId        the goal id
     */
    public FloidOutgoingCommand(int type, int size, int commandNumber, int goalId) {
        super();
        mCommandBuffer = new byte[size];
        setType(type);
        setCommandNumber(commandNumber);
        setGoalId(goalId);
    }
    /**
     * Create an outgoing (to Arduino) command
     *
     * @param type          the command type
     * @param commandNumber the command number
     * @param goalId        the goal id
     * @param commandBuffer the command buffer
     */
    @SuppressWarnings("unused")
    public FloidOutgoingCommand(int type, int commandNumber, int goalId, byte[] commandBuffer) {
        super();
        mCommandBuffer = commandBuffer;
        setType(type);
        setGoalId(goalId);
        setCommandNumber(commandNumber);
    }

    /**
     * Get the command buffer
     *
     * @return the command buffer
     */
    @SuppressWarnings("WeakerAccess")
    public byte[] getCommandBuffer() {
        return mCommandBuffer;
    }

    /**
     * Get the command number
     *
     * @return the command number
     */
    @SuppressWarnings("WeakerAccess")
    public int getCommandNumber() {
        return mCommandNumber;
    }

    /**
     * Set the command number
     *
     * @param commandNumber the command number
     */
    @SuppressWarnings("WeakerAccess")
    public void setCommandNumber(int commandNumber) {
        mCommandNumber = commandNumber;
        setIntToBuffer(FloidService.ACC_COMMAND_NUMBER_OFFSET, commandNumber);
    }

    /**
     * Get the goal id
     *
     * @return the goal id
     */
    @SuppressWarnings("unused")
    public int getGoalId() {
        return mGoalId;
    }

    private void setGoalId(int goalId) {
        mGoalId = goalId;
        setIntToBuffer(FloidService.ACC_GOAL_ID_OFFSET, goalId);
    }

    /**
     * Get the command type
     *
     * @return the command type
     */
    @SuppressWarnings("unused")
    public int getType() {
        return mType;
    }

    private void setType(int type) {
        mType = type;
        setByteToBuffer(FloidService.ACC_PACKET_TYPE_OFFSET, (byte) (type & 0x000000ff));
    }

    /**
     * Log the command buffer
     */
    @SuppressWarnings({"EmptyMethod","WeakerAccess"})
    public void logCommandBuffer() {
//        Log.d(FloidService.TAG, "Command Buffer: ");
//        if(mCommandBuffer.length == 0)
//        {
//            Log.d(FloidService.TAG, "EMPTY COMMAND BUFFER");
//        }
//        for(int i=0; i<mCommandBuffer.length; ++i)
//        {
//            Log.d(FloidService.TAG, "i: " + String.format("%02x", mCommandBuffer[i]));
//        }
    }

    /**
     * Set an int to the buffer at the offset
     *
     * @param offset the offset
     * @param value  the int value
     */
    @SuppressWarnings("WeakerAccess")
    public void setIntToBuffer(int offset, int value) {
        //noinspection PointlessArithmeticExpression
        mCommandBuffer[offset + 0] = (byte) ((value & 0xff000000) >> 24);
        mCommandBuffer[offset + 1] = (byte) ((value & 0x00ff0000) >> 16);
        mCommandBuffer[offset + 2] = (byte) ((value & 0x0000ff00) >> 8);
        //noinspection PointlessBitwiseExpression
        mCommandBuffer[offset + 3] = (byte) ((value & 0x000000ff) >> 0);
        // Uncomment this to log the int to the output buffer:
        //        Log.d(TAG, "Setting int to buffer: " + value
        //                 + " : " + Byte.toString(buffer[offset+0])
        //                 + " "   + Byte.toString(buffer[offset+1])
        //                 + " "   + Byte.toString(buffer[offset+2])
        //                 + " "   + Byte.toString(buffer[offset+3]));
    }

    /**
     * Set an short to the buffer at the offset
     *
     * @param offset the offset
     * @param value  the short value
     */
    @SuppressWarnings("WeakerAccess")
    public void setShortToBuffer(int offset, short value) {
        //noinspection PointlessArithmeticExpression
        mCommandBuffer[offset + 0] = (byte) ((value & 0x0000ff00) >> 8);
        //noinspection PointlessBitwiseExpression
        mCommandBuffer[offset + 1] = (byte) ((value & 0x000000ff) >> 0);
    }

    /**
     * Set a byte to the buffer at the offset
     *
     * @param offset the offset
     * @param value  the byte value
     */
    public void setByteToBuffer(int offset, byte value) {
        mCommandBuffer[offset] = value;
    }

    /**
     * Set a float to the buffer at the offset
     *
     * @param offset the offset
     * @param value  the float value
     */
    public void setFloatToBuffer(int offset, float value) {
        int floatInt = Float.floatToIntBits(value);
        //noinspection PointlessArithmeticExpression
        mCommandBuffer[offset + 0] = (byte) ((floatInt & 0xff000000) >> 24);
        mCommandBuffer[offset + 1] = (byte) ((floatInt & 0x00ff0000) >> 16);
        mCommandBuffer[offset + 2] = (byte) ((floatInt & 0x0000ff00) >> 8);
        //noinspection PointlessBitwiseExpression
        mCommandBuffer[offset + 3] = (byte) ((floatInt & 0x000000ff) >> 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mType);
        dest.writeInt(this.mCommandNumber);
        dest.writeInt(this.mGoalId);
        dest.writeByteArray(this.mCommandBuffer);
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected FloidOutgoingCommand(Parcel in) {
        this.mType = in.readInt();
        this.mCommandNumber = in.readInt();
        this.mGoalId = in.readInt();
        this.mCommandBuffer = in.createByteArray();
    }

    /**
     * Static CREATOR
     */
    public static final Creator<FloidOutgoingCommand> CREATOR = new Creator<FloidOutgoingCommand>() {
        @Override
        public FloidOutgoingCommand createFromParcel(Parcel source) {
            return new FloidOutgoingCommand(source);
        }

        @Override
        public FloidOutgoingCommand[] newArray(int size) {
            return new FloidOutgoingCommand[size];
        }
    };
}
