/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for the mission command
 */
@SuppressWarnings("unused")
class MissionProcessBlock extends ProcessBlock {
    /**
     * Create a new mission process block
     * @param timeoutInterval the timeout interval
     */
    public MissionProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Mission");
    }
}
