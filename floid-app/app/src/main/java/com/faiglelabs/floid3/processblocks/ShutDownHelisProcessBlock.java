/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for shut down helis command
 */
@SuppressWarnings("SameParameterValue")
public class ShutDownHelisProcessBlock extends ProcessBlock {
    /**
     * Create a new shut down helis process block
     * @param timeoutInterval the timeout interval
     */
    public ShutDownHelisProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Shut Down Helis");
    }
}
