/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.LandCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.LandProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for the land command
 */
public class LandCommandProcessor extends InstructionProcessor
{
    /**
     * Create a land command processor
     * @param floidService the floid service
     * @param landCommand the land command
     */
    @SuppressWarnings("WeakerAccess")
	public LandCommandProcessor(FloidService floidService, LandCommand landCommand)
	{
		super(floidService, landCommand);
		useDefaultGoalProcessing = true;
        completeOnCommandResponse = false;
        setProcessBlock(new LandProcessBlock(defaultLongGoalTimeout)); // 120 sec default
	}
	@Override
	public boolean setupInstruction()
	{
	    // General idea:
	    // This is generally the same as liftOff but reversed tests...
        // Internally the goal Z is going to be set to be 10m below the
        // current altitude and then decreased by 5m when then is achieved
	    super.setupInstruction();
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
        if(!floidService.getFloidDroidStatus().isLiftedOff())
	    {
	        // Logic error - must be lifted off to land:
            sendCommandLogicErrorMessage("Not lifted off.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
	    }
        LandCommand      landCommand      = (LandCommand)droidInstruction;
        LandProcessBlock landProcessBlock = (LandProcessBlock)processBlock;
        landProcessBlock.setFloidGoalId(floidService.getNextFloidGoalId());
        try
        {
            FloidOutgoingCommand landOutgoingCommand = floidService.sendLandCommandToFloid(landProcessBlock.getFloidGoalId(), (float)landCommand.getZ()); // Note this value is going to be ignored!
            if(landOutgoingCommand != null)
            {
                landProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                landProcessBlock.setProcessBlockWaitForCommandNumber(landOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Land command.");
                // We must have failed - set completion with error:
                landProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;    // We are done with this whole command
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
