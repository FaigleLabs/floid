/*
(c) 2009-2020 Chris Faigle/faiglelabs
All rights reserved.
This code is strictly licensed.
*/
package com.faiglelabs.floid3.controller;

import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import com.faiglelabs.floid3.FloidActivity;
import com.faiglelabs.floid3.R;
import com.faiglelabs.floid3.messages.FloidInterfaceEnableMessage;
import com.faiglelabs.floid3.messages.FloidInterfaceSetSliderMessage;
import com.faiglelabs.floid3.messages.FloidInterfaceSetTextMessage;
import com.helloandroid.android.horizontalslider.HorizontalSlider;
import com.helloandroid.android.horizontalslider.HorizontalSlider.OnProgressChangeListener;

/**
 * Emulation controller for IMU
 */
public class FloidIMUEmulationController implements OnProgressChangeListener {
    @SuppressWarnings("unused")
    private static final String TAG = "FloidIMUEmulationController";
    private final FloidActivity mFloidActivity;
    private final Handler mInterfaceMessageHandler;
    private final HorizontalSlider mHorizontalSliderPitch;
    private final HorizontalSlider mHorizontalSliderHeading;
    private final HorizontalSlider mHorizontalSliderRoll;
    private final HorizontalSlider mHorizontalSliderAltitude;
    private final HorizontalSlider mHorizontalSliderTemperature;
    private final TextView mPitchTextView;
    private final TextView mHeadingTextView;
    private final TextView mRollTextView;
    private final TextView mAltitudeTextView;
    private final TextView mTemperatureTextView;
    // Maps a value of -90 to 90 (degrees) <-> 0 to 180 (slider):
    /**
     * Imu emulation controller pitch min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_PITCH_MIN = -90.0;
    /**
     * Imu emulation controller pitch max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_PITCH_MAX = 90.0;
    /**
     * Imu emulation controller pitch start
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_PITCH_START = 0.0;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_PITCH_OFFSET = -FLOID_PYR_EMULATION_CONTROLLER_PITCH_MIN;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_PITCH_SCALE_MAX = FLOID_PYR_EMULATION_CONTROLLER_PITCH_MAX + FLOID_PYR_EMULATION_CONTROLLER_PITCH_OFFSET;
    /**
     * Imu emulation controller pitch rate min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_PITCH_RATE_MIN = 0.01;
    /**
     * Imu emulation controller pitch rate max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_PITCH_RATE_MAX = 30.00;
    /**
     * Imu emulation controller pitch rate default
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_PITCH_RATE_DEFAULT = 1.00;
    // Maps a value of 0 to 360 (degrees) <-> 0 to 360 (slider):
    /**
     * Imu emulation controller heading min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_HEADING_MIN = 0.0;
    /**
     * Imu emulation controller heading max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_HEADING_MAX = 360.0;
    /**
     * Imu emulation controller heading start
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_HEADING_START = 0.0;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_HEADING_OFFSET = -FLOID_PYR_EMULATION_CONTROLLER_HEADING_MIN;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_HEADING_SCALE_MAX = FLOID_PYR_EMULATION_CONTROLLER_HEADING_MAX + FLOID_PYR_EMULATION_CONTROLLER_HEADING_OFFSET;
    /**
     * Imu emulation controller heading rate min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_HEADING_RATE_MIN = 0.01;
    /**
     * Imu emulation controller heading rate max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_HEADING_RATE_MAX = 30.00;
    /**
     * Imu emulation controller heading rate default
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_HEADING_RATE_DEFAULT = 1.00;
    // Maps a value of -90 to 90 (degrees) <-> 0 to 180 (slider):
    /**
     * Imu emulation controller roll min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ROLL_MIN = -90.0;
    /**
     * Imu emulation controller roll max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ROLL_MAX = 90.0;
    /**
     * Imu emulation controller roll start
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ROLL_START = 0.0;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_ROLL_OFFSET = -FLOID_PYR_EMULATION_CONTROLLER_ROLL_MIN;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_ROLL_SCALE_MAX = FLOID_PYR_EMULATION_CONTROLLER_ROLL_MAX + FLOID_PYR_EMULATION_CONTROLLER_ROLL_OFFSET;
    /**
     * Imu emulation controller roll rate min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ROLL_RATE_MIN = 0.01;
    /**
     * Imu emulation controller roll rate max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ROLL_RATE_MAX = 30.00;
    /**
     * Imu emulation controller roll rate default
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ROLL_RATE_DEFAULT = 1.00;
    // Maps a value of -200 to 10000 (meters) <-> 0 to 10200 (slider):
    /**
     * Imu emulation controller altitude min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MIN = -200.0;
    /**
     * Imu emulation controller altitude max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MAX = 10000.0;
    /**
     * Imu emulation controller altitude start
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_START = 55.0;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_OFFSET = -FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MIN;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_SCALE_MAX = FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MAX + FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_OFFSET;
    /**
     * Imu emulation controller altitude rate min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_RATE_MIN = 0.01;
    /**
     * Imu emulation controller altitude rate max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_RATE_MAX = 30.00;
    /**
     * Imu emulation controller altitude rate default
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_RATE_DEFAULT = 1.00;

    // Maps a value of -20 to 0 (degrees) <-> 0 to 180 (slider):
    /**
     * Imu emulation controller temperature min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MIN = -20.0;
    /**
     * Imu emulation controller temperature max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MAX = 60.0;
    /**
     * Imu emulation controller temperature start
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_START = 30.0;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_OFFSET = -FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MIN;
    private static final double FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_SCALE_MAX = FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MAX + FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_OFFSET;
    // Rate:
    /**
     * Imu emulation controller temperature rate min
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_RATE_MIN = 0.001;
    /**
     * Imu emulation controller temperature rate max
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_RATE_MAX = 30.00;
    /**
     * Imu emulation controller temperature rate default
     */
    public static final double FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_RATE_DEFAULT = 1.00;

    /**
     * Create a new IMU emulation controller
     *
     * @param floidActivity               the floid activity
     * @param interfaceMessageHandler     the interface message handler
     * @param imuEmulationContainerViewId the imu emulation controller view id
     */
    public FloidIMUEmulationController(FloidActivity floidActivity,
                                       Handler interfaceMessageHandler,
                                       int imuEmulationContainerViewId) {
        mFloidActivity = floidActivity;
        mInterfaceMessageHandler = interfaceMessageHandler;
        View pyrEmulationContainerView = mFloidActivity.findViewById(imuEmulationContainerViewId);
        // Pitch:
        View pyrEmulationPitchServoView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_pitch_servo);
        mHorizontalSliderPitch = pyrEmulationPitchServoView.findViewById(R.id.servo_slider_no_label);
        mHorizontalSliderPitch.setMax((int) FLOID_PYR_EMULATION_CONTROLLER_PITCH_SCALE_MAX);
        mHorizontalSliderPitch.setOnProgressChangeListener(this);
        mPitchTextView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_pitch_value);
        setPitch((float) FLOID_PYR_EMULATION_CONTROLLER_PITCH_START);
        // Heading:
        View pyrEmulationHeadingServoView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_heading_servo);
        mHorizontalSliderHeading = pyrEmulationHeadingServoView.findViewById(R.id.servo_slider_no_label);
        mHorizontalSliderHeading.setMax((int) FLOID_PYR_EMULATION_CONTROLLER_HEADING_SCALE_MAX);
        mHorizontalSliderHeading.setOnProgressChangeListener(this);
        mHeadingTextView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_heading_value);
        setHeading((float) FLOID_PYR_EMULATION_CONTROLLER_HEADING_START);
        // Roll:
        View pyrEmulationRollServoView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_roll_servo);
        mHorizontalSliderRoll = pyrEmulationRollServoView.findViewById(R.id.servo_slider_no_label);
        mHorizontalSliderRoll.setMax((int) FLOID_PYR_EMULATION_CONTROLLER_ROLL_SCALE_MAX);
        mHorizontalSliderRoll.setOnProgressChangeListener(this);
        mRollTextView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_roll_value);
        setRoll((float) FLOID_PYR_EMULATION_CONTROLLER_ROLL_START);
        // Altitude:
        View pyrEmulationAltitudeServoView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_altitude_servo);
        mHorizontalSliderAltitude = pyrEmulationAltitudeServoView.findViewById(R.id.servo_slider_no_label);
        mHorizontalSliderAltitude.setMax((int) FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_SCALE_MAX);
        mHorizontalSliderAltitude.setOnProgressChangeListener(this);
        mAltitudeTextView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_altitude_value);
        setAltitude((float) FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_START);
        // Temperature:
        View pyrEmulationTemperatureServoView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_temperature_servo);
        mHorizontalSliderTemperature = pyrEmulationTemperatureServoView.findViewById(R.id.servo_slider_no_label);
        mHorizontalSliderTemperature.setMax((int) FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_SCALE_MAX);
        mHorizontalSliderTemperature.setOnProgressChangeListener(this);
        mTemperatureTextView = pyrEmulationContainerView.findViewById(R.id.pyr_emulation_temperature_value);
        setTemperature((float) FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_START);
    }

    @Override
    public void onProgressChanged(View view, int progress) {
        float pitch = (float) mHorizontalSliderPitch.getProgress() - (float) FLOID_PYR_EMULATION_CONTROLLER_PITCH_OFFSET;
        float heading = (float) mHorizontalSliderHeading.getProgress() - (float) FLOID_PYR_EMULATION_CONTROLLER_HEADING_OFFSET;
        float roll = (float) mHorizontalSliderRoll.getProgress() - (float) FLOID_PYR_EMULATION_CONTROLLER_ROLL_OFFSET;
        float altitude = (float) mHorizontalSliderAltitude.getProgress() - (float) FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_OFFSET;
        float temperature = (float) mHorizontalSliderTemperature.getProgress() - (float) FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_OFFSET;
        mFloidActivity.sendPyrPacketCommandToFloidService(pitch, heading, roll, altitude, temperature);
    }
    @Override
    public void performClick(View view)  {
        // Do nothing  - see FloidServoController for how to use performClick
    }

    // Note: These messages to the activity are in from the activity -e.g. same process and can send references to UI components
    /**
     * Set the pitch
     *
     * @param pitchValue the pitch value
     */
    public void setPitch(float pitchValue) {
        // Make a message to set the slider value and then make a message to set the 
        if (pitchValue < FLOID_PYR_EMULATION_CONTROLLER_PITCH_MIN) {
            pitchValue = (float) FLOID_PYR_EMULATION_CONTROLLER_PITCH_MIN;
        }
        if (pitchValue > FLOID_PYR_EMULATION_CONTROLLER_PITCH_MAX) {
            pitchValue = (float) FLOID_PYR_EMULATION_CONTROLLER_PITCH_MAX;
        }
        // Send the messages to the interface to set the values:
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_SLIDER, new FloidInterfaceSetSliderMessage(mHorizontalSliderPitch, (int) (pitchValue + FLOID_PYR_EMULATION_CONTROLLER_PITCH_OFFSET))).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_TEXT, new FloidInterfaceSetTextMessage(mPitchTextView, Float.toString(pitchValue))).sendToTarget();
    }

    /**
     * Set the heading
     *
     * @param headingValue the heading value
     */
    public void setHeading(float headingValue) {
        while (headingValue < FLOID_PYR_EMULATION_CONTROLLER_HEADING_MIN) {
            headingValue += FLOID_PYR_EMULATION_CONTROLLER_HEADING_MAX;
        }
        while (headingValue > FLOID_PYR_EMULATION_CONTROLLER_HEADING_MAX) {
            headingValue -= FLOID_PYR_EMULATION_CONTROLLER_HEADING_MAX;
        }
        // Send the messages to the interface to set the values:
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_SLIDER, new FloidInterfaceSetSliderMessage(mHorizontalSliderHeading, (int) (headingValue + FLOID_PYR_EMULATION_CONTROLLER_HEADING_OFFSET))).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_TEXT, new FloidInterfaceSetTextMessage(mHeadingTextView, Float.toString(headingValue))).sendToTarget();
    }

    /**
     * Set the roll
     *
     * @param rollValue the roll value
     */
    public void setRoll(float rollValue) {
        if (rollValue < FLOID_PYR_EMULATION_CONTROLLER_ROLL_MIN) {
            rollValue = (float) FLOID_PYR_EMULATION_CONTROLLER_ROLL_MIN;
        }
        if (rollValue > FLOID_PYR_EMULATION_CONTROLLER_ROLL_MAX) {
            rollValue = (float) FLOID_PYR_EMULATION_CONTROLLER_ROLL_MAX;
        }
        // Send the messages to the interface to set the values:
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_SLIDER, new FloidInterfaceSetSliderMessage(mHorizontalSliderRoll, (int) (rollValue + FLOID_PYR_EMULATION_CONTROLLER_ROLL_OFFSET))).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_TEXT, new FloidInterfaceSetTextMessage(mRollTextView, Float.toString(rollValue))).sendToTarget();
    }

    /**
     * Set the altitude
     *
     * @param altitudeValue the altitude value
     */
    public void setAltitude(float altitudeValue) {
        if (altitudeValue < FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MIN) {
            altitudeValue = (float) FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MIN;
        }
        if (altitudeValue > FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MAX) {
            altitudeValue = (float) FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MAX;
        }
        // Send the messages to the interface to set the values:
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_SLIDER, new FloidInterfaceSetSliderMessage(mHorizontalSliderAltitude, (int) (altitudeValue + FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_OFFSET))).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_TEXT, new FloidInterfaceSetTextMessage(mAltitudeTextView, Float.toString(altitudeValue))).sendToTarget();
    }

    /**
     * Set the temperature
     *
     * @param temperatureValue the temperature (in Celsius)
     */
    public void setTemperature(float temperatureValue) {
        if (temperatureValue < FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MIN) {
            temperatureValue = (float) FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MIN;
        }
        if (temperatureValue > FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MAX) {
            temperatureValue = (float) FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MAX;
        }
        // Send the messages to the interface to set the values:
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_SLIDER, new FloidInterfaceSetSliderMessage(mHorizontalSliderTemperature, (int) (temperatureValue + FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_OFFSET))).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_TEXT, new FloidInterfaceSetTextMessage(mTemperatureTextView, Float.toString(temperatureValue))).sendToTarget();
    }

    /**
     * Set enable
     *
     * @param enable enable
     */
    public void setEnable(boolean enable) {
        // Send the messages to the interface to enable this:
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mHorizontalSliderPitch, enable)).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mHorizontalSliderHeading, enable)).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mHorizontalSliderRoll, enable)).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mHorizontalSliderAltitude, enable)).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mHorizontalSliderTemperature, enable)).sendToTarget();
    }
}
