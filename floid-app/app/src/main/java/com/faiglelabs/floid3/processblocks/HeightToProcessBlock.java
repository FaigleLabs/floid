/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for height to command
 */
@SuppressWarnings("SameParameterValue")
public class HeightToProcessBlock extends ProcessBlock {
    /**
     * Create a new height to process block
     * @param timeoutInterval the timeout interval
     */
    public HeightToProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Height To");
    }
}
