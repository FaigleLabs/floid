package com.faiglelabs.floid3.parcelables;

import android.os.Parcel;
import android.os.Parcelable;
import com.faiglelabs.floid.servertypes.statuses.FloidDroidStatus;

/**
 * Parcelable version of FloidDroidStatus
 */
public class FloidDroidStatusParcelable extends FloidDroidStatus implements Parcelable {

    /**
     * Parcelable version of FloidDroidStatus
     */
    public FloidDroidStatusParcelable() {
        super();
        type=this.getClass().getSuperclass().getName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.statusTime);
        dest.writeLong(this.statusNumber);
        dest.writeLong(this.gpsStatusTime);
        dest.writeByte(this.gpsHasAccuracy ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.gpsAccuracy);
        dest.writeDouble(this.gpsLatitude);
        dest.writeDouble(this.gpsLongitude);
        dest.writeByte(this.gpsHasAltitude ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.gpsAltitude);
        dest.writeByte(this.gpsHasBearing ? (byte) 1 : (byte) 0);
        dest.writeFloat(this.gpsBearing);
        dest.writeFloat(this.gpsSpeed);
        dest.writeString(this.gpsProvider);
        dest.writeByte(this.floidConnected ? (byte) 1 : (byte) 0);
        dest.writeByte(this.floidError ? (byte) 1 : (byte) 0);
        dest.writeInt(this.currentStatus);
        dest.writeInt(this.currentMode);
        dest.writeInt(this.missionNumber);
        dest.writeByte(this.droidStarted ? (byte) 1 : (byte) 0);
        dest.writeByte(this.serverConnected ? (byte) 1 : (byte) 0);
        dest.writeByte(this.serverError ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.batteryVoltage);
        dest.writeDouble(this.batteryLevel);
        dest.writeInt(this.batteryRawLevel);
        dest.writeInt(this.batteryRawScale);
        dest.writeByte(this.helisStarted ? (byte) 1 : (byte) 0);
        dest.writeByte(this.liftedOff ? (byte) 1 : (byte) 0);
        dest.writeByte(this.parachuteDeployed ? (byte) 1 : (byte) 0);
        dest.writeByte(this.takingPhoto ? (byte) 1 : (byte) 0);
        dest.writeInt(this.photoNumber);
        dest.writeByte(this.photoStrobe ? (byte) 1 : (byte) 0);
        dest.writeInt(this.photoStrobeDelay);
        dest.writeByte(this.takingVideo ? (byte) 1 : (byte) 0);
        dest.writeInt(this.videoNumber);
        dest.writeByte(this.homePositionAcquired ? (byte) 1 : (byte) 0);
        dest.writeDouble(this.homePositionX);
        dest.writeDouble(this.homePositionY);
        dest.writeDouble(this.homePositionZ);
        dest.writeDouble(this.homePositionHeading);
        dest.writeValue(this.id);
        dest.writeString(this.floidUuid);
        dest.writeString(this.type);
        dest.writeInt(this.floidId);
        dest.writeValue(this.floidMessageNumber);
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected FloidDroidStatusParcelable(Parcel in) {
        this.statusTime = in.readLong();
        this.statusNumber = in.readLong();
        this.gpsStatusTime = in.readLong();
        this.gpsHasAccuracy = in.readByte() != 0;
        this.gpsAccuracy = in.readFloat();
        this.gpsLatitude = in.readDouble();
        this.gpsLongitude = in.readDouble();
        this.gpsHasAltitude = in.readByte() != 0;
        this.gpsAltitude = in.readDouble();
        this.gpsHasBearing = in.readByte() != 0;
        this.gpsBearing = in.readFloat();
        this.gpsSpeed = in.readFloat();
        this.gpsProvider = in.readString();
        this.floidConnected = in.readByte() != 0;
        this.floidError = in.readByte() != 0;
        this.currentStatus = in.readInt();
        this.currentMode = in.readInt();
        this.missionNumber = in.readInt();
        this.droidStarted = in.readByte() != 0;
        this.serverConnected = in.readByte() != 0;
        this.serverError = in.readByte() != 0;
        this.batteryVoltage = in.readDouble();
        this.batteryLevel = in.readDouble();
        this.batteryRawLevel = in.readInt();
        this.batteryRawScale = in.readInt();
        this.helisStarted = in.readByte() != 0;
        this.liftedOff = in.readByte() != 0;
        this.parachuteDeployed = in.readByte() != 0;
        this.takingPhoto = in.readByte() != 0;
        this.photoNumber = in.readInt();
        this.photoStrobe = in.readByte() != 0;
        this.photoStrobeDelay = in.readInt();
        this.takingVideo = in.readByte() != 0;
        this.videoNumber = in.readInt();
        this.homePositionAcquired = in.readByte() != 0;
        this.homePositionX = in.readDouble();
        this.homePositionY = in.readDouble();
        this.homePositionZ = in.readDouble();
        this.homePositionHeading = in.readDouble();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.floidUuid = in.readString();
        this.type = in.readString();
        this.floidId = in.readInt();
        this.floidMessageNumber = (Long) in.readValue(Long.class.getClassLoader());
    }

    /**
     * The creator
     */
    public static final Creator<FloidDroidStatusParcelable> CREATOR = new Creator<FloidDroidStatusParcelable>() {
        @Override
        public FloidDroidStatusParcelable createFromParcel(Parcel source) {
            return new FloidDroidStatusParcelable(source);
        }

        @Override
        public FloidDroidStatusParcelable[] newArray(int size) {
            return new FloidDroidStatusParcelable[size];
        }
    };
}
