/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.messages;

import android.widget.TextView;

/**
 * Floid interface set text message
 */
public class FloidInterfaceSetTextMessage {
    private String text;
    private TextView textView;

    /**
     * Create a floid interface set text message
     *
     * @param textView the text view
     * @param text     the text
     */
    public FloidInterfaceSetTextMessage(TextView textView, String text) {
        this.textView = textView;
        this.text = text;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @return the textView
     */
    public TextView getTextView() {
        return textView;
    }

    /**
     * @param text the text to set
     */
    @SuppressWarnings("unused")
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @param textView the textView to set
     */
    @SuppressWarnings("unused")
    public void setTextView(TextView textView) {
        this.textView = textView;
    }
}
