/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for take photo command
 */
@SuppressWarnings("SameParameterValue")
public class TakePhotoProcessBlock extends ProcessBlock {
    /**
     * Create a new take photo process block
     * @param timeoutInterval the timeout interval
     */
    public TakePhotoProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Take Photo");
    }
}
