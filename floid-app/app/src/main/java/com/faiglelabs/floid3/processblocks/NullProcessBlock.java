/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for the null command
 */
@SuppressWarnings("SameParameterValue")
public class NullProcessBlock extends ProcessBlock {
    /**
     * Create a new null process block
     * @param timeoutInterval the timeout interval
     */
    public NullProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Null Command");
    }
}
