/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.*;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.hardware.GeomagneticField;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.*;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import com.MobileAnarchy.Android.Widgets.Joystick.JoystickMovedListener;
import com.MobileAnarchy.Android.Widgets.Joystick.JoystickView;
import com.faiglelabs.floid.debug.FloidDebugTokenFilter;
import com.faiglelabs.floid.servertypes.statuses.FloidImagingStatus;
import com.faiglelabs.floid.servertypes.statuses.FloidStatus;
import com.faiglelabs.floid3.controller.*;
import com.faiglelabs.floid3.helper.DroidParameterEditState;
import com.faiglelabs.floid3.helper.FloidParameterRadioButtonCheckedChangeListener;
import com.faiglelabs.floid3.imaging.FloidCameraInfo;
import com.faiglelabs.floid3.messages.FloidInterfaceEnableMessage;
import com.faiglelabs.floid3.messages.FloidInterfaceSetSliderMessage;
import com.faiglelabs.floid3.messages.FloidInterfaceSetTextMessage;
import com.faiglelabs.floid3.parcelables.*;
import com.faiglelabs.floid3.validator.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONObject;

import java.io.*;
import java.lang.Process;
import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The type Floid activity.
 */
@SuppressWarnings({"Duplicates", "CodeBlock2Expr", "CommentedOutCode"})
public class FloidActivity extends AppCompatActivity {
    // Our tag for debugging:
    /**
     * The constant TAG.
     */
    private static final String TAG = "FloidActivity";
    // Activity:
    /**
     * The floid activity.
     */
    private Activity mFloidActivity = null;

    // Intent actions:
    /**
     * The constant QUIT_ACTIVITY_INTENT_ACTION.
     */
    public static final String QUIT_ACTIVITY_INTENT_ACTION = "com.faiglelabs.floid3.action.QUIT_ACTIVITY";
    /**
     * The quit activity broadcast receiver.
     */
    private BroadcastReceiver quitActivityBroadcastReceiver;
    // IDs:
    /**
     * The floid activity floid id.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private int mFloidActivityFloidId = 0;
    /**
     * The floid activity floid uuid.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private String mFloidActivityFloidUuid = ""; // Not currently used because it is not an editable field
    // Floid Status:
    /**
     * The floid activity floid status synchronizer.
     */
    private final Object mFloidActivityFloidStatusSynchronizer = new Object();
    /**
     * The floid activity floid status.
     */
    private FloidStatusParcelable mFloidActivityFloidStatus = new FloidStatusParcelable();
    /**
     * The floid activity previous floid status.
     */
    private FloidStatusParcelable mFloidActivityPreviousFloidStatus = null;
    // Model Status:
    /**
     * The floid model status synchronizer.
     */
    private final Object mFloidActivityFloidModelStatusSynchronizer = new Object();
    /**
     * The floid model status.
     */
    private FloidModelStatusParcelable mFloidActivityFloidModelStatus = new FloidModelStatusParcelable();
    /**
     * The previous floid model status.
     */
    private FloidModelStatusParcelable mFloidActivityPreviousFloidModelStatus = null;
    // Model Parameters::
    /**
     * The floid model parameters synchronizer.
     */
    private final Object mFloidActivityFloidModelParametersSynchronizer = new Object();
    /**
     * The floid model parameters.
     */
    private FloidModelParametersParcelable mFloidActivityFloidModelParameters = new FloidModelParametersParcelable();
    /**
     * The droid parameters valid values.
     */
    private static HashMap<String, DroidValidParameters> mFloidActivityDroidParametersValidValues = new HashMap<>();
    /**
     * The droid parameters values.
     */
    private static HashMap<String, String> mFloidActivityDroidParameters = new HashMap<>();
    /**
     * The droid default parameters values.
     */
    private static HashMap<String, String> mFloidActivityDroidDefaultParameters = new HashMap<>();
    /**
     * The droid parameters edit state list
     */
    private final List<DroidParameterEditState> mDroidParameterEditStateList = new ArrayList<>();
    // Get the colors we need:
    /**
     * Panel color
     */
    private int panelColor;
    /**
     * Warning panel color
     */
    private int warningPanelColor;
    /**
     * Edit panel color
     */
    private int editPanelColor;
    /**
     * Parameter panel color
     */
    private int parameterPanelColor;
    /**
     * Modified parameter text color
     */
    private int modifiedParameterValueTextColor;
    /**
     * Unmodified parameter text color
     */
    private int unModifiedParameterValueTextColor;
    /**
     * Enabled edit background color
     */
    private int enabledEditBackgroundColor;
    /**
     * Disabled edit background color
     */
    private int disabledEditBackgroundColor;
    // Droid Status:
    /**
     * The floid droid status.
     */
    private FloidDroidStatusParcelable mFloidActivityFloidDroidStatus = new FloidDroidStatusParcelable();
    // Thread Timings:
    /**
     * The thread timings.
     */
    private final FloidThreadTimings mFloidActivityThreadTimings = new FloidThreadTimings();
    /**
     * The additional status information:
     */
    private AdditionalStatus additionalStatus = new AdditionalStatus();
    // Accessory connected:
    /**
     * The accessory connected.
     */
    private Boolean mFloidActivityAccessoryConnected = false;
    // Server location:
    /**
     * The host ip.
     */
    private String mFloidActivityHostAddress = FloidService.sDefaultHostAddress;
    /**
     * The host port.
     */
    private int mFloidActivityHostPort = FloidService.sDefaultHostPort;
    /**
     * Whether to use logger.
     */
    public boolean mFloidActivityUseLogger = false;
    /**
     * Floid service PID.
     */
    private int mFloidServicePid = 0;
    // Client communicator:
    /**
     * The client communicator on.
     */
    private boolean mFloidActivityClientCommunicatorOn = true;
    // PYR Emulation:
    /**
     * The pyr emulation timer task.
     */
    private TimerTask mPyrEmulationTimerTask = null;
    /**
     * The pyr emulation algorithm current timer.
     */
    private Timer mPyrEmulationAlgorithmCurrentTimer = null;
    /**
     * The pyr emulation algorithm on flag.
     */
    private boolean mPyrEmulationAlgorithmOn = false;
    /**
     * The pyr emulation algorithm last time.
     */
    private long mPyrEmulationAlgorithmLastTime = 0L;
    /**
     * The pyr emulation algorithm rate.
     */
    private int mPyrEmulationAlgorithmRate = 5;
    /**
     * The Pyr emulation first wait.
     */
    private static final long PYR_EMULATION_FIRST_WAIT = 100L;
    /**
     * The Pyr emulation period.
     */
    private static final long PYR_EMULATION_PERIOD = 100L;

    // Floid Service & Intent:
    /**
     * The floid service intent.
     */
    private Intent mFloidServiceIntent = null;
    /**
     * The floid service incoming messenger.
     */
    private Messenger mFloidServiceIncomingMessenger = null;
    /**
     * The floid service outgoing messenger.
     */
    private Messenger mFloidServiceOutgoingMessenger = null;
    /**
     * The floid service is bound.
     */
    private boolean mFloidServiceIsBound = false;
    // UX Messages:
    /**
     * The Floid ux message set droid status.
     */
    public static final int FLOID_UX_MESSAGE_SET_DROID_STATUS = 1;
    /**
     * The Floid ux message set floid status.
     */
    public static final int FLOID_UX_MESSAGE_SET_FLOID_STATUS = 2;
    /**
     * The Floid ux message set model parameters.
     */
    public static final int FLOID_UX_MESSAGE_SET_MODEL_PARAMETERS = 3;
    /**
     * The Floid ux message set model status.
     */
    public static final int FLOID_UX_MESSAGE_SET_MODEL_STATUS = 4;
    /**
     * The Floid ux message set host.
     */
    public static final int FLOID_UX_MESSAGE_SET_HOST = 5;
    /**
     * The Floid ux message set ids.
     */
    public static final int FLOID_UX_MESSAGE_SET_IDS = 6;
    /**
     * The Floid ux message set log.
     */
    public static final int FLOID_UX_MESSAGE_SET_LOG = 7;
    /**
     * The Floid ux message set client communicator.
     */
    public static final int FLOID_UX_MESSAGE_SET_CLIENT_COMMUNICATOR = 8;
    /**
     * The Floid ux message ding comm status.
     */
    public static final int FLOID_UX_MESSAGE_DING_COMM_STATUS = 9;
    /**
     * The Floid ux message ding server status.
     */
    public static final int FLOID_UX_MESSAGE_DING_SERVER_STATUS = 10;
    /**
     * The Floid ux message debug.
     */
    public static final int FLOID_UX_MESSAGE_DEBUG = 11;
    /**
     * The Floid ux message command response.
     */
    public static final int FLOID_UX_MESSAGE_COMMAND_RESPONSE = 12;
    /**
     * The Floid ux message test packet response.
     */
    public static final int FLOID_UX_MESSAGE_TEST_PACKET_RESPONSE = 13;
    /**
     * The Floid ux message to display.
     */
    public static final int FLOID_UX_MESSAGE_MESSAGE_TO_DISPLAY = 14;
    /**
     * The Floid ux message start interface.
     */
    public static final int FLOID_UX_MESSAGE_START_INTERFACE = 15;
    /**
     * The Floid ux message stop interface.
     */
    public static final int FLOID_UX_MESSAGE_STOP_INTERFACE = 16;
    /**
     * The Floid ux message interface set text.
     */
    public static final int FLOID_UX_MESSAGE_INTERFACE_SET_TEXT = 17;
    /**
     * The Floid ux message interface set slider.
     */
    public static final int FLOID_UX_MESSAGE_INTERFACE_SET_SLIDER = 18;
    /**
     * The Floid ux message interface enable.
     */
    public static final int FLOID_UX_MESSAGE_INTERFACE_ENABLE = 19;
    /**
     * The Floid ux message set accessory connected.
     */
    public static final int FLOID_UX_MESSAGE_SET_ACCESSORY_CONNECTED = 20;
    /**
     * The Floid ux message start mission timer.
     */
    public static final int FLOID_UX_MESSAGE_START_MISSION_TIMER = 21;
    /**
     * The Floid ux message update mission timer.
     */
    public static final int FLOID_UX_MESSAGE_UPDATE_MISSION_TIMER = 22;
    /**
     * The Floid ux message cancel mission timer.
     */
    public static final int FLOID_UX_MESSAGE_CANCEL_MISSION_TIMER = 23;
    /**
     * The Floid ux message fail server status.
     */
    public static final int FLOID_UX_MESSAGE_FAIL_SERVER_STATUS = 24;
    /**
     * The Floid ux message set droid parameters.
     */
    public static final int FLOID_UX_MESSAGE_SET_DROID_PARAMETERS = 25;
    /**
     * The Floid ux message set droid parameter valid values.
     */
    public static final int FLOID_UX_MESSAGE_SET_DROID_PARAMETER_VALID_VALUES = 26;
    /**
     * The Floid ux message set droid default parameters.
     */
    public static final int FLOID_UX_MESSAGE_SET_DROID_DEFAULT_PARAMETERS = 27;
    /**
     * The Floid ux message imaging status.
     */
    public static final int FLOID_UX_MESSAGE_IMAGING_STATUS = 28;
    /**
     * The Floid ux message photo.
     */
    public static final int FLOID_UX_MESSAGE_PHOTO = 29;
    /**
     * The Floid ux message video frame.
     */
    public static final int FLOID_UX_MESSAGE_VIDEO_FRAME = 30;
    /**
     * The Floid ux message camera info.
     */
    public static final int FLOID_UX_MESSAGE_CAMERA_INFO = 31;

    /**
     * The Floid ux message set additional status.
     */
    public static final int FLOID_UX_MESSAGE_SET_ADDITIONAL_STATUS = 32;
    /**
     * The Floid ux message update comm status.
     */
    public static final int FLOID_UX_MESSAGE_UPDATE_COMM_STATUS = 33;

    // Battery status:
    /**
     * The constant FLOID_BATTERY_INDICATOR_MINIMUM.
     */
    private static final double FLOID_BATTERY_INDICATOR_MINIMUM = 3.7;    // NOTE: [SetParameter] Candidate for setParameter
    /**
     * The constant FLOID_BATTERY_INDICATOR_LOW.
     */
    private static final double FLOID_BATTERY_INDICATOR_LOW = 3.9;    // NOTE: [SetParameter] Candidate for setParameter
    /**
     * The constant FLOID_BATTERY_INDICATOR_MEDIUM.
     */
    private static final double FLOID_BATTERY_INDICATOR_MEDIUM = 4.2;    // NOTE: [SetParameter] Candidate for setParameter
    /**
     * The constant FLOID_BATTERY_INDICATOR_MAXIMUM.
     */
    private static final double FLOID_BATTERY_INDICATOR_MAXIMUM = 5.0;    // NOTE: [SetParameter] Candidate for setParameter
    /**
     * The constant DROID_BATTERY_INDICATOR_MINIMUM.
     */
    private static final double DROID_BATTERY_INDICATOR_MINIMUM = 0.0;    // NOTE: [SetParameter] Candidate for setParameter
    /**
     * The constant DROID_BATTERY_INDICATOR_LOW.
     */
    private static final double DROID_BATTERY_INDICATOR_LOW = 20.0;   // NOTE: [SetParameter] Candidate for setParameter
    /**
     * The constant DROID_BATTERY_INDICATOR_MEDIUM.
     */
    private static final double DROID_BATTERY_INDICATOR_MEDIUM = 50.0;   // NOTE: [SetParameter] Candidate for setParameter
    /**
     * The constant DROID_BATTERY_INDICATOR_MAXIMUM.
     */
    private static final double DROID_BATTERY_INDICATOR_MAXIMUM = 100.0;  // NOTE: [SetParameter] Candidate for setParameter
    // Debug token filter:
    /**
     * The Floid debug token filter.
     */
    private final FloidDebugTokenFilter floidDebugTokenFilter = new FloidDebugTokenFilter();
    // Display
    /**
     * The display on flag.
     */
    private boolean mDisplayOn = true;
    /**
     * The display metrics.
     */
    private DisplayMetrics mDisplayMetrics = new DisplayMetrics();
    // Border drawables:
    /**
     * The border drawable.
     */
    private Drawable mBorderDrawable = null;
    /**
     * The warning border drawable.
     */
    private Drawable mWarningBorderDrawable = null;
    /**
     * The error border drawable.
     */
    private Drawable mErrorBorderDrawable = null;
    // Communication Status - Floid:
    /**
     * The last floid comm status led.
     */
    private int mLastFloidCommStatusLED = -1;
    /**
     * The floid communicator led on drawable.
     */
    private Drawable mFloidCommunicatorLEDOnDrawable = null;
    /**
     * The floid communicator led off drawable.
     */
    private Drawable mFloidCommunicatorLEDOffDrawable = null;
    /**
     * The array of floid comm led image views:
     */
    private ImageView[] mFloidCommLEDImageViews;
    // Communication Status - Server:
    /**
     * The last server comm status led.
     */
    private int mLastServerCommStatusLED = -1;
    /**
     * The server communicator led on drawable.
     */
    private Drawable mServerCommunicatorLEDOnDrawable = null;
    /**
     * The server communicator led fail drawable.
     */
    private Drawable mServerCommunicatorLEDFailDrawable = null;
    /**
     * The server communicator led off drawable.
     */
    private Drawable mServerCommunicatorLEDOffDrawable = null;
    /**
     * The server communicator led off fail state drawable.
     */
    private Drawable mServerCommunicatorLEDFailOffDrawable = null;
    /**
     * The server communicator led off unknown state drawable.
     */
    private Drawable mServerCommunicatorLEDUnknownOffDrawable = null;
    /**
     * The array of floid comm led image views:
     */
    private ImageView[] mServerCommLEDImageViews;
    /**
     * The array of floid comm led image views:
     */
    private int[] mServerCommLEDStatuses;

    // Server led statuses:
    private static final int SERVER_COMM_STATUS_UNKNOWN = 0;
    private static final int SERVER_COMM_STATUS_SUCCESS = 1;
    private static final int SERVER_COMM_STATUS_FAIL = -1;

    /**
     * The resume sound media player.
     */
    private MediaPlayer mActivitySoundResumeMediaPlayer;
    /**
     * The pause sound media player.
     */
    private MediaPlayer mActivitySoundPauseMediaPlayer;
    /**
     * THe activity sound for resume
     */
    private final static int FLOID_ACTIVITY_SOUND_RESUME = 0;
    /**
     * THe activity sound for pause
     */
    private final static int FLOID_ACTIVITY_SOUND_PAUSE = 1;

    // Options Menu:
    /**
     * The options menu.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private Menu mOptionsMenu = null;
    // GUI Fields for main bar:
    /**
     * The accessory open drawable.
     */
    private Drawable mAccessoryOpenDrawable;
    /**
     * The accessory closed drawable.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private Drawable mAccessoryClosedDrawable;
    /**
     * The additional status text view.
     */
    private TextView additionalStatusTextView;
    /**
     * The floid battery indicator frame layout.
     */
    private FrameLayout mFloidBatteryIndicatorFrameLayout;
    /**
     * The floid battery indicator status text view.
     */
    private TextView mFloidBatteryIndicatorStatusTextView;
    /**
     * The floid battery indicator name text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mFloidBatteryIndicatorNameTextView;
    /**
     * The floid battery indicator linear layout.
     */
    private LinearLayout mFloidBatteryIndicatorLinearLayout;
    /**
     * The droid battery indicator frame layout.
     */
    private FrameLayout mDroidBatteryIndicatorFrameLayout;
    /**
     * The droid battery indicator status text view.
     */
    private TextView mDroidBatteryIndicatorStatusTextView;
    /**
     * The droid battery indicator name text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mDroidBatteryIndicatorNameTextView;
    /**
     * The droid battery indicator linear layout.
     */
    private LinearLayout mDroidBatteryIndicatorLinearLayout;
    // GUI Fields for DroidStatus:
    /**
     * The current status text view.
     */
    private TextView mCurrentStatusTextView;
    /**
     * The droid parameters Interface Container.
     */
    private LinearLayout mDroidParametersContainer;
    /**
     * The droid parameters Set Button.
     */
    private Button mDroidParametersSetButton;
    /**
     * The droid parameters Cancel Button.
     */
    private Button mDroidParametersCancelButton;
    /**
     * The droid parameters Load Button.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private Button mDroidParametersLoadButton;
    /**
     * The droid parameters Save Button.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private Button mDroidParametersSaveButton;
    /**
     * The droid parameters load save auto complete text view.
     */
    private AutoCompleteTextView mDroidParametersLoadSaveAutoCompleteTextView;
    /**
     * The floid droid parameters synchronizer.
     */
    private final Object mFloidActivityDroidParametersSynchronizer = new Object();
    /**
     * The current status label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCurrentStatusLabelTextView;
    /**
     * The current mode text view.
     */
    private TextView mCurrentModeTextView;
    /**
     * The faiglelabs banner text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mFaiglelabsBanner;
    /**
     * The current mode label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCurrentModeLabelTextView;
    /**
     * The droid started text view.
     */
    private TextView mDroidStartedTextView;
    /**
     * The droid started label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mDroidStartedLabelTextView;
    /**
     * The server connected text view.
     */
    private TextView mServerConnectedTextView;
    /**
     * The server connected label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mServerConnectedLabelTextView;
    /**
     * The server error text view.
     */
    private TextView mServerErrorTextView;
    /**
     * The server error label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mServerErrorLabelTextView;
    /**
     * The helis started text view.
     */
    private TextView mHelisStartedTextView;
    /**
     * The helis started label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mHelisStartedLabelTextView;
    /**
     * The lifted off text view.
     */
    private TextView mLiftedOffTextView;
    /**
     * The lifted off label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mLiftedOffLabelTextView;
    /**
     * The parachute deployed text view.
     */
    private TextView mParachuteDeployedTextView;
    /**
     * The parachute deployed label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mParachuteDeployedLabelTextView;
    /**
     * The taking photo text view.
     */
    private TextView mTakingPhotoTextView;
    /**
     * The taking photo label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTakingPhotoLabelTextView;
    /**
     * The photo number text view.
     */
    private TextView mPhotoNumberTextView;
    /**
     * The photo number label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mPhotoNumberLabelTextView;
    /**
     * The taking video text view.
     */
    private TextView mTakingVideoTextView;
    /**
     * The taking video label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTakingVideoLabelTextView;
    /**
     * The video number text view.
     */
    private TextView mVideoNumberTextView;
    /**
     * The video number label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mVideoNumberLabelTextView;
    /**
     * The home position label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mHomePositionLabelTextView;
    /**
     * The home position acquired text view.
     */
    private TextView mHomePositionAcquiredTextView;
    /**
     * The home position lat text view.
     */
    private TextView mHomePositionLatTextView;
    /**
     * The home position lng text view.
     */
    private TextView mHomePositionLngTextView;
    /**
     * The home position height text view.
     */
    private TextView mHomePositionHeightTextView;
    /**
     * The floid connected text view.
     */
    private TextView mFloidConnectedTextView;
    /**
     * The floid connected label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mFloidConnectedLabelTextView;
    /**
     * The floid loop count text view.
     */
// GUI Fields for FloidStatus:
    private TextView mFloidLoopCountTextView;
    /**
     * The floid loop rate text view.
     */
    private TextView mFloidLoopRateTextView;
    /**
     * The floid memory text view.
     */
    private TextView mFloidMemoryTextView;
    /**
     * The display toggle button.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ToggleButton mDisplayToggleButton;
    /**
     * The floid mode toggle button.
     */
    private ToggleButton mFloidModeToggleButton;
    /**
     * The floid mode floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mFloidModeFloidToggleButtonController;
    /**
     * The batteries view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mBatteriesViewGroup;
    // Battery Indicator colors:
    /**
     * The low battery color.
     */
    private int mBatteryLowColor;
    /**
     * The medium battery color.
     */
    private int mBatteryMediumColor;
    // Battery Indicator colors:
    /**
     * The high battery color.
     */
    private int mBatteryHighColor;
    /**
     * The b status text view.
     */
    private TextView mBStatusTextView;            // statusView.findViewById(R.id.b_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_MAIN_BATTERY)));
    /**
     * The b 0 status text view.
     */
    private TextView mB0StatusTextView;           // statusView.findViewById(R.id.b0_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI0_BATTERY)));
    /**
     * The b 1 status text view.
     */
    private TextView mB1StatusTextView;           // statusView.findViewById(R.id.b1_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI1_BATTERY)));
    /**
     * The b 2 status text view.
     */
    private TextView mB2StatusTextView;           // statusView.findViewById(R.id.b2_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI2_BATTERY)));
    /**
     * The b 3 status tet view.
     */
    private TextView mB3StatusTetView;            // statusView.findViewById(R.id.b3_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI3_BATTERY)));
    /**
     * The c 0 status text view.
     */
    private TextView mC0StatusTextView;           // statusView.findViewById(R.id.c0_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI0_CURRENT)));
    /**
     * The c 1 status text view.
     */
    private TextView mC1StatusTextView;           // statusView.findViewById(R.id.c1_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI1_CURRENT)));
    /**
     * The c 2 status text view.
     */
    private TextView mC2StatusTextView;           // statusView.findViewById(R.id.c2_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI2_CURRENT)));
    /**
     * The c 3 status text view.
     */
    private TextView mC3StatusTextView;           // statusView.findViewById(R.id.c3_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI3_CURRENT)));
    /**
     * The bay 0 status text view.
     */
    private TextView mBay0StatusTextView;         // statusView.findViewById(R.id.bay0_status);    // setText("Closed");
    /**
     * The bay 1 status text view.
     */
    private TextView mBay1StatusTextView;         // statusView.findViewById(R.id.bay1_status);    // setText("Open");
    /**
     * The bay 2 status text view.
     */
    private TextView mBay2StatusTextView;         // statusView.findViewById(R.id.bay2_status);    // setText("Closed");
    /**
     * The bay 3 status text view.
     */
    private TextView mBay3StatusTextView;         // statusView.findViewById(R.id.bay3_status);    // setText("Closed");
    /**
     * The nm 0 status text view.
     */
    private TextView mNm0StatusTextView;          // statusView.findViewById(R.id.nm0_status);
    /**
     * The nm 1 status text view.
     */
    private TextView mNm1StatusTextView;          // statusView.findViewById(R.id.nm1_status);
    /**
     * The nm 2 status text view.
     */
    private TextView mNm2StatusTextView;          // statusView.findViewById(R.id.nm2_status);
    /**
     * The nm 3 status text view.
     */
    private TextView mNm3StatusTextView;          // statusView.findViewById(R.id.nm3_status);
    /**
     * The aux current text view.
     */
    private TextView mAuxCurrentTextView;         // statusView.findViewById(R.id.aux_current_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_AUX_CURRENT)));
    /**
     * The aux battery text view.
     */
    private TextView mAuxBatteryTextView;         // statusView.findViewById(R.id.aux_battery_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_AUX_BATTERY)));
    /**
     * The status text view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mStatusTextViewGroup;        // (ViewGroup)findViewById(R.id.status_text_container);
    /**
     * The status text.
     */
    private TextView mStatusText;                 // (TextView) statusTextView.findViewById(R.id.status_text);
    /**
     * The auto scroll status check box.
     */
    private CheckBox mAutoScrollStatusCheckBox;   // (CheckBox)findViewById(R.id.auto_scroll_checkbox);    // isChecked())
    /**
     * The get logs button.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private Button mGetLogsButton;
    /**
     * The status scroll view.
     */
    private ScrollView mStatusScrollView;           // ScrollView) statusTextView.findViewById(R.id.status_text_container);    // fullScroll(ScrollView.FOCUS_DOWN[);
    /**
     * The pyr view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mPyrViewGroup;               // (ViewGroup)findViewById(R.id.pyr);
    /**
     * The pyr heading label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mPyrHeadingLabelTextView;
    /**
     * The pyr heading text view.
     */
    private TextView mPyrHeadingTextView;         // pyrView.findViewById(R.id.pyr_heading);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_HEADING)));
    /**
     * The pyr heading smoothed text view.
     */
    private TextView mPyrHeadingSmoothedTextView; // pyrView.findViewById(R.id.pyr_heading_smoothed);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_HEADING_SMOOTHED)));
    /**
     * The pyr heading velocity text view.
     */
    private TextView mPyrHeadingVelocityTextView; // pyrView.findViewById(R.id.pyr_heading_velocity);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_HEADING_VELOCITY)));
    /**
     * The pyr height label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mPyrHeightLabelTextView;
    /**
     * The pyr height text view.
     */
    private TextView mPyrHeightTextView;          // pyrView.findViewById(R.id.pyr_height);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_HEIGHT)));
    /**
     * The pyr height smoothed text view.
     */
    private TextView mPyrHeightSmoothedTextView;  // pyrView.findViewById(R.id.pyr_height_smoothed);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_HEIGHT_SMOOTHED)));
    /**
     * The pyr temp label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mPyrTempLabelTextView;
    /**
     * The pyr temp text view.
     */
    private TextView mPyrTempTextView;            // pyrView.findViewById(R.id.pyr_temp);
    /**
     * The pyr declination label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mPyrDeclinationLabelTextView;
    /**
     * The pyr declination text view.
     */
    private TextView mPyrDeclinationTextView;
    /**
     * The imu algorithm status text view.
     */
    private TextView mImuAlgorithmStatusTextView;
    /**
     * The imu algorithm calibrate button.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private Button mImuCalibrateButton;
    /**
     * The imu calibrate floid button controller.
     */
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private FloidButtonController mImuCalibrateFloidButtonController;
    /**
     * The imu save button.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private Button mImuSaveButton;
    /**
     * The imu save floid button controller.
     */
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private FloidButtonController mImuSaveFloidButtonController;
    /**
     * The pyr pitch label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mPyrPitchLabelTextView;
    /**
     * The pyr pitch text view.
     */
    private TextView mPyrPitchTextView;
    /**
     * The pyr pitch smoothed text view.
     */
    private TextView mPyrPitchSmoothedTextView;
    /**
     * The pyr roll label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mPyrRollLabelTextView;
    /**
     * The pyr roll text view.
     */
    private TextView mPyrRollTextView;            // pyrView.findViewById(R.id.pyr_roll);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_ROLL)));
    /**
     * The pyr roll smoothed text view.
     */
    private TextView mPyrRollSmoothedTextView;    // pyrView.findViewById(R.id.pyr_roll_smoothed);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_ROLL_SMOOTHED)));
    /**
     * The pyr pitch velocity text view.
     */
    private TextView mPyrPitchVelocityTextView;   // pyrView.findViewById(R.id.pyr_pitch_velocity);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_PITCH_VELOCITY)));
    /**
     * The pyr roll velocity text view.
     */
    private TextView mPyrRollVelocityTextView;    // pyrView.findViewById(R.id.pyr_roll_velocity);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_ROLL_VELOCITY)));
    /**
     * The pyr height velocity text view.
     */
    private TextView mPyrHeightVelocityTextView;  // pyrView.findViewById(R.id.pyr_height_velocity);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, PYR_OFFSET_HEIGHT_VELOCITY)));
    // GPS:
    /**
     * The gps view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mGpsViewGroup;               // (ViewGroup)findViewById(R.id.gps);
    /**
     * The gps time text view.
     */
    private TextView mGpsTimeTextView;            // gpsView.findViewById(R.id.gps_time);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, GPS_OFFSET_TIME)));
    /**
     * The gps x degrees decimal text view.
     */
    private TextView mGpsXDegreesDecimalTextView; // gpsView.findViewById(R.id.gps_x_degrees_decimal);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, GPS_OFFSET_X_DEGREES_DECIMAL)));
    /**
     * The gps y degrees decimal text view.
     */
    private TextView mGpsYDegreesDecimalTextView; // gpsView.findViewById(R.id.gps_y_degrees_decimal);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, GPS_OFFSET_Y_DEGREES_DECIMAL)));
    /**
     * The gps fix quality text view.
     */
    private TextView mGpsFixQualityTextView;      // gpsView.findViewById(R.id.gps_fix_quality);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, GPS_OFFSET_FIX_QUALITY)));
    /**
     * The gps z meters text view.
     */
    private TextView mGpsZMetersTextView;         // gpsView.findViewById(R.id.gps_z_meters);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, GPS_OFFSET_Z_METERS)));
    /**
     * The gps z feet text view.
     */
    private TextView mGpsZFeetTextView;           // gpsView.findViewById(R.id.gps_z_feet);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, GPS_OFFSET_Z_FEET)));
    /**
     * The gps hdop text view.
     */
    private TextView mGpsHDOPTextView;            // gpsView.findViewById(R.id.gps_hdop);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, GPS_OFFSET_HDOP)));
    /**
     * The gps sats text view.
     */
    private TextView mGpsSatsTextView;            // gpsView.findViewById(R.id.gps_sats);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, GPS_OFFSET_SATS)));/*
    // Helis/Servos/ESCs
    /**
     * The helis view group.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private ViewGroup mHelisViewGroup;
    /**
     * The heli 0 view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mHeli0ViewGroup;
    /**
     * The heli 1 view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mHeli1ViewGroup;
    /**
     * The heli 2 view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mHeli2ViewGroup;
    /**
     * The heli 3 view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mHeli3ViewGroup;
    /**
     * The servo 0 l text view.
     */
    private TextView mServo0LTextView;
    /**
     * The servo 0 l pulse text view.
     */
    private TextView mServo0LPulseTextView;
    /**
     * The l 0 servo controller.
     */
    private FloidServoController mL0ServoController;
    /**
     * The servo 0 r text view.
     */
    private TextView mServo0RTextView;
    /**
     * The servo 0 r pulse text view.
     */
    private TextView mServo0RPulseTextView;
    /**
     * The r 0 servo controller.
     */
    private FloidServoController mR0ServoController;
    /**
     * The servo 0 p text view.
     */
    private TextView mServo0PTextView;
    /**
     * The servo 0 p pulse text view.
     */
    private TextView mServo0PPulseTextView;
    /**
     * The p 0 servo controller.
     */
    private FloidServoController mP0ServoController;
    /**
     * The servo 1 l text view.
     */
    private TextView mServo1LTextView;
    /**
     * The servo 1 l pulse text view.
     */
    private TextView mServo1LPulseTextView;
    /**
     * The l 1 servo controller.
     */
    private FloidServoController mL1ServoController;
    /**
     * The servo 1 r text view.
     */
    private TextView mServo1RTextView;
    /**
     * The servo 1 r pulse text view.
     */
    private TextView mServo1RPulseTextView;
    /**
     * The r 1 servo controller.
     */
    private FloidServoController mR1ServoController;
    /**
     * The servo 1 p text view.
     */
    private TextView mServo1PTextView;
    /**
     * The servo 1 p pulse text view.
     */
    private TextView mServo1PPulseTextView;
    /**
     * The p 1 servo controller.
     */
    private FloidServoController mP1ServoController;
    /**
     * The servo 2 l text view.
     */
    private TextView mServo2LTextView;
    /**
     * The servo 2 l pulse text view.
     */
    private TextView mServo2LPulseTextView;
    /**
     * The l 2 servo controller.
     */
    private FloidServoController mL2ServoController;
    /**
     * The servo 2 r text view.
     */
    private TextView mServo2RTextView;
    /**
     * The servo 2 r pulse text view.
     */
    private TextView mServo2RPulseTextView;
    /**
     * The r 2 servo controller.
     */
    private FloidServoController mR2ServoController;
    /**
     * The servo 2 p text view.
     */
    private TextView mServo2PTextView;
    /**
     * The servo 2 p pulse text view.
     */
    private TextView mServo2PPulseTextView;
    /**
     * The p 2 servo controller.
     */
    private FloidServoController mP2ServoController;
    /**
     * The servo 3 l text view.
     */
    private TextView mServo3LTextView;
    /**
     * The servo 3 l pulse text view.
     */
    private TextView mServo3LPulseTextView;
    /**
     * The l 3 servo controller.
     */
    private FloidServoController mL3ServoController;
    /**
     * The servo 3 r text view.
     */
    private TextView mServo3RTextView;
    /**
     * The servo 3 r pulse text view.
     */
    private TextView mServo3RPulseTextView;
    /**
     * The r 3 servo controller.
     */
    private FloidServoController mR3ServoController;
    /**
     * The servo 3 p text view.
     */
    private TextView mServo3PTextView;
    /**
     * The servo 3 p pulse text view.
     */
    private TextView mServo3PPulseTextView;
    /**
     * The p 3 servo controller.
     */
    private FloidServoController mP3ServoController;
    /**
     * The esc 0 value text view.
     */
    private TextView mEsc0ValueTextView;
    /**
     * The esc 0 pulse text view.
     */
    private TextView mEsc0PulseTextView;
    /**
     * The e 0 servo controller.
     */
    private FloidServoController mE0ServoController;
    /**
     * The esc 1 value text view.
     */
    private TextView mEsc1ValueTextView;
    /**
     * The esc 1 pulse text view.
     */
    private TextView mEsc1PulseTextView;
    /**
     * The e 1 servo controller.
     */
    private FloidServoController mE1ServoController;
    /**
     * The esc 2 value text view.
     */
    private TextView mEsc2ValueTextView;
    /**
     * The esc 2 pulse text view.
     */
    private TextView mEsc2PulseTextView;
    /**
     * The e 2 servo controller.
     */
    private FloidServoController mE2ServoController;
    /**
     * The esc 3 value text view.
     */
    private TextView mEsc3ValueTextView;
    /**
     * The esc 3 pulse text view.
     */
    private TextView mEsc3PulseTextView;
    /**
     * The e 3 servo controller.
     */
    private FloidServoController mE3ServoController;
    // Pan/Tilts:
    /**
     * The current pan tilt view id.
     */
    private int mCurrentPanTiltViewId;
    /**
     * The pan tilts view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mPanTiltsViewGroup;
    /**
     * The pan tilt 0 tab label.
     */
    private TextView mPanTilt0TabLabel;
    /**
     * The pan tilt 0 container.
     */
    private ViewGroup mPanTilt0Container;
    /**
     * The pan tilt 0 view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mPanTilt0ViewGroup;
    /**
     * The pan tilt 1 tab label.
     */
    private TextView mPanTilt1TabLabel;
    /**
     * The pan tilt 1 container.
     */
    private ViewGroup mPanTilt1Container;
    /**
     * The pan tilt 1 view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mPanTilt1ViewGroup;
    /**
     * The pan tilt 2 tab label.
     */
    private TextView mPanTilt2TabLabel;
    /**
     * The pan tilt 2 container.
     */
    private ViewGroup mPanTilt2Container;
    /**
     * The pan tilt 2 view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mPanTilt2ViewGroup;
    /**
     * The pan tilt 3 tab label.
     */
    private TextView mPanTilt3TabLabel;
    /**
     * The pan tilt 3 container.
     */
    private ViewGroup mPanTilt3Container;
    /**
     * The pan tilt 3 view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mPanTilt3ViewGroup;
    /**
     * The pan tilt 0 toggle button.
     */
    private ToggleButton mPanTilt0ToggleButton;
    /**
     * The pan tilt 0 floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mPanTilt0FloidToggleButtonController;
    /**
     * The pan tilt 1 toggle button.
     */
    private ToggleButton mPanTilt1ToggleButton;
    /**
     * The pan tilt 1 floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mPanTilt1FloidToggleButtonController;
    /**
     * The pan tilt 2 toggle button.
     */
    private ToggleButton mPanTilt2ToggleButton;
    /**
     * The pan tilt 2 floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mPanTilt2FloidToggleButtonController;
    /**
     * The pan tilt 3 toggle button.
     */
    private ToggleButton mPanTilt3ToggleButton;
    /**
     * The pan tilt 3 floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mPanTilt3FloidToggleButtonController;
    /**
     * The pan tilt 0 mode text view.
     */
    private TextView mPanTilt0ModeTextView;
    /**
     * The pan tilt 1 mode text view.
     */
    private TextView mPanTilt1ModeTextView;
    /**
     * The pan tilt 2 mode text view.
     */
    private TextView mPanTilt2ModeTextView;
    /**
     * The pan tilt 3 mode text view.
     */
    private TextView mPanTilt3ModeTextView;
    /**
     * The pan tilt 0 pan value text view.
     */
    private TextView mPanTilt0PanValueTextView;
    /**
     * The pt 0 p servo controller.
     */
    private FloidServoController mPT0PServoController;
    /**
     * The pan tilt 0 tilt value text view.
     */
    private TextView mPanTilt0TiltValueTextView;
    /**
     * The pt 0 t servo controller.
     */
    private FloidServoController mPT0TServoController;
    /**
     * The pan tilt 1 pan value text view.
     */
    private TextView mPanTilt1PanValueTextView;
    /**
     * The pt 1 p servo controller.
     */
    private FloidServoController mPT1PServoController;
    /**
     * The pan tilt 1 tilt value text view.
     */
    private TextView mPanTilt1TiltValueTextView;
    /**
     * The pt 1 t servo controller.
     */
    private FloidServoController mPT1TServoController;
    /**
     * The pan tilt 2 pan value text view.
     */
    private TextView mPanTilt2PanValueTextView;
    /**
     * The pt 2 p servo controller.
     */
    private FloidServoController mPT2PServoController;
    /**
     * The pan tilt 2 tilt value text view.
     */
    private TextView mPanTilt2TiltValueTextView;
    /**
     * The pt 2 t servo controller.
     */
    private FloidServoController mPT2TServoController;
    /**
     * The pan tilt 3 pan value text view.
     */
    private TextView mPanTilt3PanValueTextView;
    /**
     * The pt 3 p servo controller.
     */
    private FloidServoController mPT3PServoController;
    /**
     * The pan tilt 3 tilt value text view.
     */
    private TextView mPanTilt3TiltValueTextView;
    /**
     * The pt 3 t servo controller.
     */
    private FloidServoController mPT3TServoController;
    /**
     * The pan tilt 0 target x text view.
     */
    private TextView mPanTilt0TargetXTextView;
    /**
     * The pan tilt 0 target y text view.
     */
    private TextView mPanTilt0TargetYTextView;
    /**
     * The pan tilt 0 target z text view.
     */
    private TextView mPanTilt0TargetZTextView;
    /**
     * The pan tilt 1 target x text view.
     */
    private TextView mPanTilt1TargetXTextView;
    /**
     * The pan tilt 1 target y text view.
     */
    private TextView mPanTilt1TargetYTextView;
    /**
     * The pan tilt 1 target z text view.
     */
    private TextView mPanTilt1TargetZTextView;
    /**
     * The pan tilt 2 target x text view.
     */
    private TextView mPanTilt2TargetXTextView;
    /**
     * The pan tilt 2 target y text view.
     */
    private TextView mPanTilt2TargetYTextView;
    /**
     * The pan tilt 2 target z text view.
     */
    private TextView mPanTilt2TargetZTextView;
    /**
     * The pan tilt 3 target x text view.
     */
    private TextView mPanTilt3TargetXTextView;
    /**
     * The pan tilt 3 target y text view.
     */
    private TextView mPanTilt3TargetYTextView;
    /**
     * The pan tilt 3 target z text view.
     */
    private TextView mPanTilt3TargetZTextView;
    // Physics
    /**
     * The physics view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mPhysicsViewGroup;
    // Status
    /**
     * The status view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mStatusViewGroup;
    /**
     * The status time text view.
     */
    private TextView mStatusTimeTextView;
    /**
     * The status number text view.
     */
    private TextView mStatusNumberTextView;
    /**
     * The mode text view.
     */
    private TextView mModeTextView;
    /**
     * The follow mode text view.
     */
    private TextView mFollowModeTextView;
    /**
     * The camera on text view.
     */
    private TextView mCameraOnTextView;
    /**
     * The gps on text view.
     */
    private TextView mGPSOnTextView;
    /**
     * The pyr on text view.
     */
    private TextView mPYROnTextView;
    /**
     * The designator on text view.
     */
    private TextView mDesignatorOnTextView;
    /**
     * The aux on text view.
     */
    private TextView mAuxOnTextView;
    /**
     * The helis on text view.
     */
    private TextView mHelisOnTextView;
    /**
     * The parachute deployed 2 text view.
     */
    private TextView mParachuteDeployed2TextView;
    /**
     * The direction over ground text view (in compass degrees 0=North 270=West)
     */
    private TextView mDirectionOverGroundTextView;
    /**
     * The velocity over ground text view.
     */
    private TextView mVelocityOverGroundTextView;
    /**
     * The altitude text view.
     */
    private TextView mAltitudeTextView;
    /**
     * The altitude velocity text view.
     */
    private TextView mAltitudeVelocityTextView;
    /**
     * The land mode min altitude and time label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mLandModeMinAltitudeAndTimeLabelTextView;
    /**
     * The land mode min altitude text view.
     */
    private TextView mLandModeMinAltitudeTextView;
    /**
     * The land mode min altitude time text view.
     */
    private TextView mLandModeMinAltitudeTimeTextView;
    /**
     * The has goal text view.
     */
    private TextView mHasGoalTextView;
    /**
     * The goal id text view.
     */
    private TextView mGoalIdTextView;
    /**
     * The goal ticks text view.
     */
    private TextView mGoalTicksTextView;
    /**
     * The goal state text view.
     */
    private TextView mGoalStateTextView;
    /**
     * The goal target x text view.
     */
    private TextView mGoalTargetXTextView;
    /**
     * The goal target y text view.
     */
    private TextView mGoalTargetYTextView;
    /**
     * The goal target z text view.
     */
    private TextView mGoalTargetZTextView;
    /**
     * The goal target angle text view.
     */
    private TextView mGoalTargetAngleTextView;
    /**
     * The in flight text view.
     */
    private TextView mInFlightTextView;
    /**
     * The lift-off mode text view.
     */
    private TextView mLiftOffModeTextView;
    /**
     * The land mode text view.
     */
    private TextView mLandModeTextView;
    /**
     * The payload 0 goal state text view.
     */
    private TextView mPayload0GoalStateTextView;
    /**
     * The payload 1 goal state text view.
     */
    private TextView mPayload1GoalStateTextView;
    /**
     * The payload 2 goal state text view.
     */
    private TextView mPayload2GoalStateTextView;
    /**
     * The payload 3 goal state text view.
     */
    private TextView mPayload3GoalStateTextView;
    /**
     * The status time label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mStatusTimeLabelTextView;
    /**
     * The status number label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mStatusNumberLabelTextView;
    /**
     * The mode label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mModeLabelTextView;
    /**
     * The follow mode label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mFollowModeLabelTextView;
    /**
     * The camera on label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mCameraOnLabelTextView;
    /**
     * The gps on label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mGPSOnLabelTextView;
    /**
     * The pyr on label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mPYROnLabelTextView;
    /**
     * The designator on label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mDesignatorOnLabelTextView;
    /**
     * The aux on label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mAuxOnLabelTextView;
    /**
     * The helis on label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mHelisOnLabelTextView;
    /**
     * The parachute deployed 2 label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mParachuteDeployed2LabelTextView;
    /**
     * The direction over ground and velocity label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mDirectionOverGroundAndVelocityLabelTextView;
    /**
     * The altitude and altitude velocity label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mAltitudeAndAltitudeVelocityLabelTextView;
    /**
     * The has goal label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mHasGoalLabelTextView;
    /**
     * The goal id label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mGoalIdLabelTextView;
    /**
     * The goal ticks label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mGoalTicksLabelTextView;
    /**
     * The goal state label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mGoalStateLabelTextView;
    /**
     * The goal target x label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mGoalTargetXLabelTextView;
    /**
     * The goal target y label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mGoalTargetYLabelTextView;
    /**
     * The goal target z label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mGoalTargetZLabelTextView;
    /**
     * The goal target angle label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mGoalTargetAngleLabelTextView;
    /**
     * The in flight label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mInFlightLabelTextView;
    /**
     * The lift-off mode label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mLiftOffModeLabelTextView;
    /**
     * The land mode label text view.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mLandModeLabelTextView;
    /**
     * The payload 0 goal state label text view.
     */
    @SuppressWarnings("unused")
    private TextView mPayload0GoalStateLabelTextView;
    /**
     * The payload 1 goal state label text view.
     */
    @SuppressWarnings("unused")
    private TextView mPayload1GoalStateLabelTextView;
    /**
     * The payload 2 goal state label text view.
     */
    @SuppressWarnings("unused")
    private TextView mPayload2GoalStateLabelTextView;
    /**
     * The payload 3 goal state label text view.
     */
    @SuppressWarnings("unused")
    private TextView mPayload3GoalStateLabelTextView;
    // Run and Test Modes:
    /**
     * The run and test modes view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mRunTestModesViewGroup;
    /**
     * The run mode production toggle button.
     */
    private ToggleButton mRunModeProductionToggleButton;
    /**
     * The goals toggle button.
     */
    private ToggleButton mGoalsToggleButton;
    /**
     * The lift-off mode toggle button.
     */
    private ToggleButton mLiftOffModeToggleButton;
    /**
     * The normal mode toggle button.
     */
    private ToggleButton mNormalModeToggleButton;
    /**
     * The land mode toggle button.
     */
    private ToggleButton mLandModeToggleButton;

    /**
     * The test mode print debug headings toggle button.
     */
    private ToggleButton mTestModePrintDebugHeadingsToggleButton;
    /**
     * The test mode check physics model toggle button.
     */
    private ToggleButton mTestModeCheckPhysicsModelToggleButton;
    /**
     * The run mode test xy toggle button.
     */
    private ToggleButton mRunModeTestXYToggleButton;
    /**
     * The run mode test altitude1 toggle button.
     */
    private ToggleButton mRunModeTestAltitude1ToggleButton;
    /**
     * The run mode test altitude2 toggle button.
     */
    private ToggleButton mRunModeTestAltitude2ToggleButton;
    /**
     * The run mode test heading1 toggle button.
     */
    private ToggleButton mRunModeTestHeading1ToggleButton;
    /**
     * The run mode test pitch1 toggle button.
     */
    private ToggleButton mRunModeTestPitch1ToggleButton;
    /**
     * The run mode test roll1 toggle button.
     */
    private ToggleButton mRunModeTestRoll1ToggleButton;
    /**
     * The test mode no xy toggle button.
     */
    private ToggleButton mTestModeNoXYToggleButton;
    /**
     * The test mode no altitude toggle button.
     */
    private ToggleButton mTestModeNoAltitudeToggleButton;
    /**
     * The test mode no attack angle toggle button.
     */
    private ToggleButton mTestModeNoAttackAngleToggleButton;
    /**
     * The test mode no heading toggle button.
     */
    private ToggleButton mTestModeNoHeadingToggleButton;
    /**
     * The test mode no pitch toggle button.
     */
    private ToggleButton mTestModeNoPitchToggleButton;
    /**
     * The test mode no roll toggle button.
     */
    private ToggleButton mTestModeNoRollToggleButton;
    /**
     * The run mode production toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mRunModeProductionFloidToggleButtonController;
    /**
     * The goals toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mGoalsToggleButtonController;
    /**
     * The test mode print debug headings toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mTestModePrintDebugHeadingsFloidToggleButtonController;

    /**
     * The test mode check physics model toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mTestModeCheckPhysicsModelFloidToggleButtonController;
    /**
     * The run mode test xy toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mRunModeFloidTestXYToggleButtonController;
    /**
     * The run mode test altitude1 toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mRunModeTestAltitude1FloidToggleButtonController;
    /**
     * The run mode test altitude2 toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mRunModeTestAltitude2FloidToggleButtonController;
    /**
     * The run mode test heading1 toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mRunModeTestHeading1FloidToggleButtonController;
    /**
     * The run mode test pitch1 toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mRunModeTestPitch1FloidToggleButtonController;
    /**
     * The run mode test roll1 toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mRunModeTestRoll1FloidToggleButtonController;
    /**
     * The test mode no xy toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mTestModeNoXYFloidToggleButtonController;
    /**
     * The test mode no altitude toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mTestModeNoAltitudeFloidToggleButtonController;
    /**
     * The test mode no attack angle toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mTestModeNoAttackAngleFloidToggleButtonController;
    /**
     * The test mode no heading toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mTestModeNoHeadingFloidToggleButtonController;
    /**
     * The test mode no pitch toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mTestModeNoPitchFloidToggleButtonController;
    /**
     * The test mode no roll toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mTestModeNoRollFloidToggleButtonController;
    // Debug:
    /**
     * The debug view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mDebugViewGroup;
    /**
     * The debug toggle button.
     */
    private ToggleButton mDebugToggleButton;
    /**
     * The debug floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugFloidToggleButtonController;
    /**
     * The debug all toggle button.
     */
    private ToggleButton mDebugAllToggleButton;
    /**
     * The debug all floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugAllFloidToggleButtonController;
    /**
     * The debug aux toggle button.
     */
    private ToggleButton mDebugAuxToggleButton;
    /**
     * The debug aux floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugAuxFloidToggleButtonController;
    /**
     * The debug payloads toggle button.
     */
    private ToggleButton mDebugPayloadsToggleButton;
    /**
     * The debug payloads floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugPayloadsFloidToggleButtonController;
    /**
     * The debug camera toggle button.
     */
    private ToggleButton mDebugCameraToggleButton;
    /**
     * The debug camera floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugCameraFloidToggleButtonController;
    /**
     * The debug designator toggle button.
     */
    private ToggleButton mDebugDesignatorToggleButton;
    /**
     * The debug designator floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugDesignatorFloidToggleButtonController;
    /**
     * The debug gps toggle button.
     */
    private ToggleButton mDebugGpsToggleButton;
    /**
     * The debug gps floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugGpsFloidToggleButtonController;
    /**
     * The debug helis toggle button.
     */
    private ToggleButton mDebugHelisToggleButton;
    /**
     * The debug helis floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugHelisFloidToggleButtonController;
    /**
     * The debug memory toggle button.
     */
    private ToggleButton mDebugMemoryToggleButton;
    /**
     * The debug memory floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugMemoryFloidToggleButtonController;
    /**
     * The debug pan tilt toggle button.
     */
    private ToggleButton mDebugPanTiltToggleButton;
    /**
     * The debug pan tilt floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugPanTiltFloidToggleButtonController;
    /**
     * The debug physics toggle button.
     */
    private ToggleButton mDebugPhysicsToggleButton;
    /**
     * The debug physics floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugPhysicsFloidToggleButtonController;
    /**
     * The debug pyr toggle button.
     */
    private ToggleButton mDebugPyrToggleButton;
    /**
     * The debug pyr floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDebugPyrFloidToggleButtonController;
    /**
     * The debug altimeter toggle button.
     */
    private ToggleButton mDebugAltimeterToggleButton;
    /**
     * The debug altimeter floid toggle button controller.
     */
    @SuppressWarnings({"unused"})
    private FloidToggleButtonController mDebugAltimeterFloidToggleButtonController;
    /**
     * The serial output toggle button.
     */
// Serial output:
    private ToggleButton mSerialOutputToggleButton;
    /**
     * The serial output floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mSerialOutputFloidToggleButtonController;
    // Droid logging:
    /**
     * The droid logging toggle button.
     */
    private ToggleButton mDroidLoggingToggleButton;
    // Mission Start Timer View:
    /**
     * The mission start timer container.
     */
    private LinearLayout mMissionStartTimerContainer;
    /**
     * The mission start timer mission label text.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mMissionStartTimerMissionLabelText;
    /**
     * The mission start timer mission name text.
     */
    private TextView mMissionStartTimerMissionNameText;
    /**
     * The mission start timer text.
     */
    private TextView mMissionStartTimerText;
    /**
     * The mission start timer units text.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private TextView mMissionStartTimerUnitsText;
    /**
     * The mission start timer cancel button.
     */
    private Button mMissionStartTimerCancelButton;
    // View tabs:
    /**
     * The floid view frames.
     */
    private FrameLayout mFloidViewFrames;
    /**
     * The current menu item id.
     */
    private int mCurrentMenuItemId;
    /**
     * The model status container.
     */
    private View mModelStatusContainer;
    /**
     * The pyr emulation container.
     */
    private View mPyrEmulationContainer;
    /**
     * The pyr emulation disabled textview.
     */
    private TextView mPyrEmulationDisabledTextView;
    /**
     * The goals container.
     */
    private View mGoalsContainer;
    /**
     * The imaging container.
     */
    private LinearLayout mImagingContainer;
    /**
     * The imaging tabs labels layout.
     */
    private LinearLayout mImagingTabsLabels;
    /**
     * The imaging tabs contents layout.
     */
    private FrameLayout mImagingTabsContents;
    /**
     * The debug container.
     */
    private View mDebugContainer;
    /**
     * The model container.
     */
    private View mModelContainer;
    /**
     * The droid container.
     */
    private View mDroidContainer;
    /**
     * The parameters container.
     */
    private View mParametersContainer;
    /**
     * The helis container.
     */
    private View mHelisContainer;
    /**
     * The model collective cyclics container.
     */
    private View mModelCollectiveCyclicsContainer;
    /**
     * The pan tilts container.
     */
    private View mPanTiltsContainer;
    /**
     * The about container.
     */
    private View mAboutContainer;
    /**
     * The app build date string.
     */
    private String mAppBuildDateString;
    /**
     * The about build date text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mAboutBuildDateTextView;
    /**
     * The about start text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mAboutStartTextView;
    /**
     * The floid container.
     */
    private View mFloidContainer;
    // Relays and declination control:
    /**
     * The devices view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mDevicesViewGroup;
    /**
     * The test relay button.
     */
    private Button mTestRelayButton;
    /**
     * The led toggle button.
     */
    private ToggleButton mLedToggleButton;
    /**
     * The led floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mLedFloidToggleButtonController;
    /**
     * The gps toggle button.
     */
    private ToggleButton mGpsToggleButton;
    /**
     * The gps floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mGpsFloidToggleButtonController;
    /**
     * The current gps rate selection.
     */
    private int mCurrentGpsRateSelection;
    /**
     * The gps rate spinner.
     */
    private Spinner mGpsRateSpinner;
    /**
     * The gps emulate toggle button.
     */
    private ToggleButton mGpsEmulateToggleButton;
    /**
     * The gps emulate floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mGpsEmulateFloidToggleButtonController;
    /**
     * The gps device toggle button.
     */
    private ToggleButton mGpsDeviceToggleButton;
    /**
     * The gps device floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mGpsDeviceFloidToggleButtonController;
    /**
     * The pyr toggle button.
     */
    private ToggleButton mPyrToggleButton;
    /**
     * The pyr floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mPyrFloidToggleButtonController;
    /**
     * The pyr device toggle button.
     */
    private ToggleButton mPyrDeviceToggleButton;
    /**
     * The pyr device floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mPyrDeviceFloidToggleButtonController;
    // PYR Emulation Items:
    /**
     * The pyr emulate algorithm toggle button.
     */
    private ToggleButton mPyrEmulateAlgorithmToggleButton;
    /**
     * The pyr emulation algorithm rate spinner.
     */
    private Spinner mPyrEmulationAlgorithmRateSpinner;
    /**
     * The pyr emulation algorithm pitch controller.
     */
    private FloidIMUEmulationAlgorithmOptionController mPyrEmulationAlgorithmPitchController;
    /**
     * The pyr emulation algorithm heading controller.
     */
    private FloidIMUEmulationAlgorithmOptionController mPyrEmulationAlgorithmHeadingController;
    /**
     * The pyr emulation algorithm roll controller.
     */
    private FloidIMUEmulationAlgorithmOptionController mPyrEmulationAlgorithmRollController;
    /**
     * The pyr emulation algorithm altitude controller.
     */
    private FloidIMUEmulationAlgorithmOptionController mPyrEmulationAlgorithmAltitudeController;
    /**
     * The pyr emulation algorithm temperature controller.
     */
    private FloidIMUEmulationAlgorithmOptionController mPyrEmulationAlgorithmTemperatureController;
    // Designator, etc
    /**
     * The designator toggle button.
     */
    private ToggleButton mDesignatorToggleButton;
    /**
     * The designator floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mDesignatorFloidToggleButtonController;
    /**
     * The aux toggle button.
     */
    private ToggleButton mAuxToggleButton;
    /**
     * The aux floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mAuxFloidToggleButtonController;
    /**
     * The parachute toggle button.
     */
    private ToggleButton mParachuteToggleButton;
    /**
     * The parachute floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mParachuteFloidToggleButtonController;
    /**
     * The pyr ad toggle button.
     */
    private ToggleButton mPyrADToggleButton;
    /**
     * The pyr ad floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mPyrADFloidToggleButtonController;
    /**
     * The current pyr rate selection.
     */
    private int mCurrentPyrRateSelection;
    /**
     * The pyr rate spinner.
     */
    private Spinner mPyrRateSpinner;
    /**
     * The pyr emulation controller.
     */
    private FloidIMUEmulationController mPyrEmulationController;
    /**
     * The current floid status rate selection.
     */
    private int mCurrentFloidStatusRateSelection;
    /**
     * The floid status rate spinner.
     */
    private Spinner mFloidStatusRateSpinner;
    /**
     * The declination view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mDeclinationViewGroup;
    /**
     * The declination edit text.
     */
    private EditText mDeclinationEditText;
    /**
     * The declination relay button.
     */
    private Button mDeclinationRelayButton;
    /**
     * The declination controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidDeclinationController mDeclinationController;
    // Helis:
    /**
     * The current heli view id.
     */
    private int mCurrentHeliViewId;
    /**
     * The heli 0 tab label.
     */
    private TextView mHeli0TabLabel;
    /**
     * The heli 0 container.
     */
    private ViewGroup mHeli0Container;
    /**
     * The heli 0 esc toggle button.
     */
    private ToggleButton mHeli0EscToggleButton;
    /**
     * The heli 0 esc floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mHeli0EscFloidToggleButtonController;
    /**
     * The heli 0 esc setup button.
     */
    private Button mHeli0EscSetupButton;
    /**
     * The heli 0 esc setup floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mHeli0EscSetupFloidButtonController;
    /**
     * The heli 0 esc extra button.
     */
    private Button mHeli0EscExtraButton;
    /**
     * The heli 0 esc extra floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mHeli0EscExtraFloidButtonController;
    /**
     * The heli 0 servo toggle button.
     */
    private ToggleButton mHeli0ServoToggleButton;
    /**
     * The heli 0 servo floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mHeli0ServoFloidToggleButtonController;
    /**
     * The heli 1 tab label.
     */
    private TextView mHeli1TabLabel;
    /**
     * The heli 1 container.
     */
    private ViewGroup mHeli1Container;
    /**
     * The heli 1 esc toggle button.
     */
    private ToggleButton mHeli1EscToggleButton;
    /**
     * The heli 1 esc floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mHeli1EscFloidToggleButtonController;
    /**
     * The heli 1 esc setup button.
     */
    private Button mHeli1EscSetupButton;
    /**
     * The heli 1 esc setup floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mHeli1EscSetupFloidButtonController;
    /**
     * The heli 1 esc extra button.
     */
    private Button mHeli1EscExtraButton;
    /**
     * The heli 1 esc extra floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mHeli1EscExtraFloidButtonController;
    /**
     * The heli 1 servo toggle button.
     */
    private ToggleButton mHeli1ServoToggleButton;
    /**
     * The heli 1 servo floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mHeli1ServoFloidToggleButtonController;
    /**
     * The heli 2 tab label.
     */
    private TextView mHeli2TabLabel;
    /**
     * The heli 2 container.
     */
    private ViewGroup mHeli2Container;
    /**
     * The heli 2 esc toggle button.
     */
    private ToggleButton mHeli2EscToggleButton;
    /**
     * The heli 2 esc floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mHeli2EscFloidToggleButtonController;
    /**
     * The heli 2 esc setup button.
     */
    private Button mHeli2EscSetupButton;
    /**
     * The heli 2 esc setup floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mHeli2EscSetupFloidButtonController;
    /**
     * The heli 2 esc extra button.
     */
    private Button mHeli2EscExtraButton;
    /**
     * The heli 2 esc extra floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mHeli2EscExtraFloidButtonController;
    /**
     * The heli 2 servo toggle button.
     */
    private ToggleButton mHeli2ServoToggleButton;
    /**
     * The heli 2 servo floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mHeli2ServoFloidToggleButtonController;
    /**
     * The heli 3 tab label.
     */
    private TextView mHeli3TabLabel;
    /**
     * The heli 3 container.
     */
    private ViewGroup mHeli3Container;
    /**
     * The heli 3 esc toggle button.
     */
    private ToggleButton mHeli3EscToggleButton;
    /**
     * The heli 3 esc floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mHeli3EscFloidToggleButtonController;
    /**
     * The heli 3 esc setup button.
     */
    private Button mHeli3EscSetupButton;
    /**
     * The heli 3 esc setup floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mHeli3EscSetupFloidButtonController;
    /**
     * The heli 3 esc extra button.
     */
    private Button mHeli3EscExtraButton;
    /**
     * The heli 3 esc extra floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mHeli3EscExtraFloidButtonController;
    /**
     * The heli 3 servo toggle button.
     */
    private ToggleButton mHeli3ServoToggleButton;
    /**
     * The heli 3 servo floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mHeli3ServoFloidToggleButtonController;
    /**
     * The payloads view group.
     */
// Payloads:
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mPayloadsViewGroup;
    /**
     * The bay 0 toggle button.
     */
    private ToggleButton mBay0ToggleButton;
    /**
     * The bay 0 floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mBay0FloidToggleButtonController;
    /**
     * The bay 1 toggle button.
     */
    private ToggleButton mBay1ToggleButton;
    /**
     * The bay 1 floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mBay1FloidToggleButtonController;
    /**
     * The bay 2 toggle button.
     */
    private ToggleButton mBay2ToggleButton;
    /**
     * The bay 2 floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mBay2FloidToggleButtonController;
    /**
     * The bay 3 toggle button.
     */
    private ToggleButton mBay3ToggleButton;
    /**
     * The bay 3 floid toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mBay3FloidToggleButtonController;
    /**
     * The altimeter view group.
     */
// Altimeter:
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private ViewGroup mAltimeterViewGroup;
    /**
     * The altimeter panel label text view.
     */
    private TextView mAltimeterPanelLabelTextView;
    /**
     * The altimeter label text view.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private TextView mAltimeterLabelTextView;
    /**
     * The altimeter status label text view.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private TextView mAltimeterStatusLabelTextView;
    /**
     * The altimeter gps label text view.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private TextView mAltimeterGpsLabelTextView;
    /**
     * The altimeter pyr label text view.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private TextView mAltimeterPyrLabelTextView;
    /**
     * The altimeter initialized text view.
     */
    private TextView mAltimeterInitializedTextView;
    /**
     * The altimeter altitude valid text view.
     */
    private TextView mAltimeterAltitudeValidTextView;
    /**
     * The altimeter altitude text view.
     */
    private TextView mAltimeterAltitudeTextView;
    /**
     * The altimeter altitude delta text view.
     */
    private TextView mAltimeterAltitudeDeltaTextView;
    /**
     * The altimeter gps x degrees decimal filtered text view.
     */
    private TextView mAltimeterGpsXDegreesDecimalFilteredTextView;
    /**
     * The altimeter gps y degrees decimal filtered text view.
     */
    private TextView mAltimeterGpsYDegreesDecimalFilteredTextView;
    /**
     * The altimeter gps z meters filtered text view.
     */
    private TextView mAltimeterGpsZMetersFilteredTextView;
    /**
     * The altimeter gps good count text view.
     */
    private TextView mAltimeterGpsGoodCountTextView;
    /**
     * The altimeter pyr good count text view.
     */
    private TextView mAltimeterPyrGoodCountTextView;
    /**
     * The altimeter pyr error count text view.
     */
    private TextView mAltimeterPyrErrorCountTextView;
    /**
     * The altimeter good count text view.
     */
    private TextView mAltimeterGoodCountTextView;
    /**
     * The server address group.
     */
// Server Address:
    private ViewGroup mServerAddressGroup;
    /**
     * The server address edit text.
     */
    private EditText mServerAddressEditText;
    /**
     * The server port edit text.
     */
    private EditText mServerPortEditText;
    /**
     * The server address button.
     */
    private Button mServerAddressButton;
    /**
     * The client communicator toggle button.
     */
    private ToggleButton mClientCommunicatorToggleButton;
    // Floid Id:
    /**
     * The floid id group.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private ViewGroup mFloidIdGroup;
    /**
     * The floid id edit text.
     */
    private EditText mFloidIdEditText;
    /**
     * The floid id button.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private Button mFloidIdButton;
    // Download Firmware:
    /**
     * The download firmware group.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private ViewGroup mDownloadFirmwareGroup;
    /**
     * The download firmware button.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private Button mDownloadFirmwareButton;
    // Thread waits:
    /**
     * The thread waits container.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private ViewGroup mThreadWaitsContainer;
    /**
     * The main thread sleep time edit text.
     */
    private EditText mMainThreadSleepTimeEditText;
    /**
     * The accessory communicator no accessory sleep time edit text.
     */
    private EditText mAccessoryCommunicatorNoAccessorySleepTimeEditText;
    /**
     * The accessory communicator incoming message sleep time edit text.
     */
    private EditText mAccessoryCommunicatorIncomingMessageSleepTimeEditText;
    /**
     * The accessory communicator outgoing message sleep time edit text.
     */
    private EditText mAccessoryCommunicatorOutgoingMessageSleepTimeEditText;
    /**
     * The accessory communicator no message sleep time edit text.
     */
    private EditText mAccessoryCommunicatorNoMessageSleepTimeEditText;
    /**
     * The thread waits button.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private Button mThreadWaitsButton;
    // Model Collective/Cyclics:
    /**
     * The model status view group.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private ViewGroup mModelCollectiveCyclicsViewGroup;
    // Model Status:
    /**
     * The model status view group.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private ViewGroup mModelStatusViewGroup;
    /**
     * The model status heli 0 view group.
     */
    private ViewGroup mModelStatusHeli0ViewGroup;
    /**
     * The model status heli 1 view group.
     */
    private ViewGroup mModelStatusHeli1ViewGroup;
    /**
     * The model status heli 2 view group.
     */
    private ViewGroup mModelStatusHeli2ViewGroup;
    /**
     * The model status heli 3 view group.
     */
    private ViewGroup mModelStatusHeli3ViewGroup;
    /**
     * The cc 0 tab label.
     */
    private TextView mCC0TabLabel;
    /**
     * The cc 0 container.
     */
    private ViewGroup mCC0Container;
    /**
     * The cc 1 tab label.
     */
    private TextView mCC1TabLabel;
    /**
     * The cc 1 container.
     */
    private ViewGroup mCC1Container;
    /**
     * The cc 2 tab label.
     */
    private TextView mCC2TabLabel;
    /**
     * The cc 2 container.
     */
    private ViewGroup mCC2Container;
    /**
     * The cc 3 tab label.
     */
    private TextView mCC3TabLabel;
    /**
     * The cc 3 container.
     */
    private ViewGroup mCC3Container;
    /**
     * The model status heli 0 collective servo controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidServoController mModelStatusHeli0CollectiveServoController;
    /**
     * The model status heli 1 collective servo controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidServoController mModelStatusHeli1CollectiveServoController;
    /**
     * The model status heli 2 collective servo controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidServoController mModelStatusHeli2CollectiveServoController;
    /**
     * The model status heli 3 collective servo controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidServoController mModelStatusHeli3CollectiveServoController;
    /**
     * The current cc view id.
     */
    private int mCurrentCCViewId;
    /**
     * The model status heli 0 cyclic joystick view.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private JoystickView mModelStatusHeli0CyclicJoystickView;
    /**
     * The model status heli 1 cyclic joystick view.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private JoystickView mModelStatusHeli1CyclicJoystickView;
    /**
     * The model status heli 2 cyclic joystick view.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private JoystickView mModelStatusHeli2CyclicJoystickView;
    /**
     * The model status heli 3 cyclic joystick view.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private JoystickView mModelStatusHeli3CyclicJoystickView;
    /**
     * The model status heli 0 cyclic x text view.
     */
    private TextView mModelStatusHeli0CyclicXTextView;
    /**
     * The model status heli 0 cyclic y text view.
     */
    private TextView mModelStatusHeli0CyclicYTextView;
    /**
     * The model status heli 1 cyclic x text view.
     */
    private TextView mModelStatusHeli1CyclicXTextView;
    /**
     * The model status heli 1 cyclic y text view.
     */
    private TextView mModelStatusHeli1CyclicYTextView;
    /**
     * The model status heli 2 cyclic x text view.
     */
    private TextView mModelStatusHeli2CyclicXTextView;
    /**
     * The model status heli 2 cyclic y text view.
     */
    private TextView mModelStatusHeli2CyclicYTextView;
    /**
     * The model status heli 3 cyclic x text view.
     */
    private TextView mModelStatusHeli3CyclicXTextView;
    /**
     * The model status heli 3 cyclic y text view.
     */
    private TextView mModelStatusHeli3CyclicYTextView;
    /**
     * The lift-off target altitude text view.
     */
    private TextView mLiftOffTargetAltitudeTextView;
    /**
     * The cyclic heading text view.
     */
    private TextView mCyclicHeadingTextView;
    /**
     * The target orientation pitch text view.
     */
    private TextView mTargetOrientationPitchTextView;
    /**
     * The target orientation pitch delta text view.
     */
    private TextView mTargetOrientationPitchDeltaTextView;
    /**
     * The target orientation roll text view.
     */
    private TextView mTargetOrientationRollTextView;
    /**
     * The target orientation roll delta text view.
     */
    private TextView mTargetOrientationRollDeltaTextView;
    /**
     * The target pitch velocity text view.
     */
    private TextView mTargetPitchVelocityTextView;
    /**
     * The target pitch velocity delta text view.
     */
    private TextView mTargetPitchVelocityDeltaTextView;
    /**
     * The target roll velocity text view.
     */
    private TextView mTargetRollVelocityTextView;
    /**
     * The target roll velocity delta text view.
     */
    private TextView mTargetRollVelocityDeltaTextView;
    /**
     * The target altitude velocity text view.
     */
    private TextView mTargetAltitudeVelocityTextView;
    /**
     * The target altitude velocity delta text view.
     */
    private TextView mTargetAltitudeVelocityDeltaTextView;
    /**
     * The target heading velocity text view.
     */
    private TextView mTargetHeadingVelocityTextView;
    /**
     * The target heading velocity delta text view.
     */
    private TextView mTargetHeadingVelocityDeltaTextView;
    /**
     * The target xy velocity text view.
     */
    private TextView mTargetXYVelocityTextView;
    /**
     * The target orientation pitch label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetOrientationPitchLabelTextView;
    /**
     * The target orientation pitch delta label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetOrientationPitchDeltaLabelTextView;
    /**
     * The target orientation roll label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetOrientationRollLabelTextView;
    /**
     * The cyclic heading label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCyclicHeadingLabelTextView;
    /**
     * The target orientation roll delta label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetOrientationRollDeltaLabelTextView;
    /**
     * The target pitch velocity label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetPitchVelocityLabelTextView;
    /**
     * The target pitch velocity delta label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetPitchVelocityDeltaLabelTextView;
    /**
     * The target roll velocity label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetRollVelocityLabelTextView;
    /**
     * The target roll velocity delta label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetRollVelocityDeltaLabelTextView;
    /**
     * The target altitude velocity label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetAltitudeVelocityLabelTextView;
    /**
     * The target altitude velocity delta label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetAltitudeVelocityDeltaLabelTextView;
    /**
     * The target heading velocity label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetHeadingVelocityLabelTextView;
    /**
     * The target heading velocity delta label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetHeadingVelocityDeltaLabelTextView;
    /**
     * The target xy velocity label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mTargetXYVelocityLabelTextView;
    /**
     * The distance to target text view.
     */
    private TextView mDistanceToTargetTextView;
    /**
     * The altitude to target text view.
     */
    private TextView mAltitudeToTargetTextView;
    /**
     * The delta heading angle to target text view.
     */
    private TextView mDeltaHeadingAngleToTargetTextView;
    /**
     * The orientation to goal text view.
     */
    private TextView mOrientationToGoalTextView;
    /**
     * The attack angle text view.
     */
    private TextView mAttackAngleTextView;
    /**
     * The lift-off target altitude label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mLiftOffTargetAltitudeLabelTextView;
    /**
     * The distance to target label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mDistanceToTargetLabelTextView;
    /**
     * The altitude to target label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mAltitudeToTargetLabelTextView;
    /**
     * The delta heading angle to target label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mDeltaHeadingAngleToTargetLabelTextView;
    /**
     * The orientation to goal label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mOrientationToGoalLabelTextView;
    /**
     * The attack angle label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mAttackAngleLabelTextView;
    /**
     * The collective delta orientation h 0 text view.
     */
    private TextView mCollectiveDeltaOrientationH0TextView;
    /**
     * The collective delta orientation h 1 text view.
     */
    private TextView mCollectiveDeltaOrientationH1TextView;
    /**
     * The collective delta orientation h 2 text view.
     */
    private TextView mCollectiveDeltaOrientationH2TextView;
    /**
     * The collective delta orientation h 3 text view.
     */
    private TextView mCollectiveDeltaOrientationH3TextView;
    /**
     * The collective delta altitude h 0 text view.
     */
    private TextView mCollectiveDeltaAltitudeH0TextView;
    /**
     * The collective delta altitude h 1 text view.
     */
    private TextView mCollectiveDeltaAltitudeH1TextView;
    /**
     * The collective delta altitude h 2 text view.
     */
    private TextView mCollectiveDeltaAltitudeH2TextView;
    /**
     * The collective delta altitude h 3 text view.
     */
    private TextView mCollectiveDeltaAltitudeH3TextView;
    /**
     * The collective delta heading h 0 text view.
     */
    private TextView mCollectiveDeltaHeadingH0TextView;
    /**
     * The collective delta heading h 1 text view.
     */
    private TextView mCollectiveDeltaHeadingH1TextView;
    /**
     * The collective delta heading h 2 text view.
     */
    private TextView mCollectiveDeltaHeadingH2TextView;
    /**
     * The collective delta heading h 3 text view.
     */
    private TextView mCollectiveDeltaHeadingH3TextView;
    /**
     * The collective delta total h 0 text view.
     */
    private TextView mCollectiveDeltaTotalH0TextView;
    /**
     * The collective delta total h 1 text view.
     */
    private TextView mCollectiveDeltaTotalH1TextView;
    /**
     * The collective delta total h 2 text view.
     */
    private TextView mCollectiveDeltaTotalH2TextView;
    /**
     * The collective delta total h 3 text view.
     */
    private TextView mCollectiveDeltaTotalH3TextView;
    /**
     * The velocity cyclic alpha text view.
     */
    private TextView mVelocityCyclicAlphaTextView;
    /**
     * The velocity cyclic alpha label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mVelocityCyclicAlphaLabelTextView;
    /**
     * The cyclic value h 0 s 0 text view.
     */
    private TextView mCyclicValueH0S0TextView;
    /**
     * The cyclic value h 0 s 1 text view.
     */
    private TextView mCyclicValueH0S1TextView;
    /**
     * The cyclic value h 0 s 2 text view.
     */
    private TextView mCyclicValueH0S2TextView;
    /**
     * The cyclic value h 1 s 0 text view.
     */
    private TextView mCyclicValueH1S0TextView;
    /**
     * The cyclic value h 1 s 1 text view.
     */
    private TextView mCyclicValueH1S1TextView;
    /**
     * The cyclic value h 1 s 2 text view.
     */
    private TextView mCyclicValueH1S2TextView;
    /**
     * The cyclic value h 2 s 0 text view.
     */
    private TextView mCyclicValueH2S0TextView;
    /**
     * The cyclic value h 2 s 1 text view.
     */
    private TextView mCyclicValueH2S1TextView;
    /**
     * The cyclic value h 2 s 2 text view.
     */
    private TextView mCyclicValueH2S2TextView;
    /**
     * The cyclic value h 3 s 0 text view.
     */
    private TextView mCyclicValueH3S0TextView;
    /**
     * The cyclic value h 3 s 1 text view.
     */
    private TextView mCyclicValueH3S1TextView;
    /**
     * The cyclic value h 3 s 2 text view.
     */
    private TextView mCyclicValueH3S2TextView;
    /**
     * The collective value h 0 text view.
     */
    private TextView mCollectiveValueH0TextView;
    /**
     * The collective value h 1 text view.
     */
    private TextView mCollectiveValueH1TextView;
    /**
     * The collective value h 2 text view.
     */
    private TextView mCollectiveValueH2TextView;
    /**
     * The collective value h 3 text view.
     */
    private TextView mCollectiveValueH3TextView;
    /**
     * The calculated cyclic blade pitch h 0 s 0 text view.
     */
    private TextView mCalculatedCyclicBladePitchH0S0TextView;
    /**
     * The calculated cyclic blade pitch h 0 s 1 text view.
     */
    private TextView mCalculatedCyclicBladePitchH0S1TextView;
    /**
     * The calculated cyclic blade pitch h 0 s 2 text view.
     */
    private TextView mCalculatedCyclicBladePitchH0S2TextView;
    /**
     * The calculated cyclic blade pitch h 1 s 0 text view.
     */
    private TextView mCalculatedCyclicBladePitchH1S0TextView;
    /**
     * The calculated cyclic blade pitch h 1 s 1 text view.
     */
    private TextView mCalculatedCyclicBladePitchH1S1TextView;
    /**
     * The calculated cyclic blade pitch h 1 s 2 text view.
     */
    private TextView mCalculatedCyclicBladePitchH1S2TextView;
    /**
     * The calculated cyclic blade pitch h 2 s 0 text view.
     */
    private TextView mCalculatedCyclicBladePitchH2S0TextView;
    /**
     * The calculated cyclic blade pitch h 2 s 1 text view.
     */
    private TextView mCalculatedCyclicBladePitchH2S1TextView;
    /**
     * The calculated cyclic blade pitch h 2 s 2 text view.
     */
    private TextView mCalculatedCyclicBladePitchH2S2TextView;
    /**
     * The calculated cyclic blade pitch h 3 s 0 text view.
     */
    private TextView mCalculatedCyclicBladePitchH3S0TextView;
    /**
     * The calculated cyclic blade pitch h 3 s 1 text view.
     */
    private TextView mCalculatedCyclicBladePitchH3S1TextView;
    /**
     * The calculated cyclic blade pitch h 3 s 2 text view.
     */
    private TextView mCalculatedCyclicBladePitchH3S2TextView;
    /**
     * The calculated collective blade pitch h 0 text view.
     */
    private TextView mCalculatedCollectiveBladePitchH0TextView;
    /**
     * The calculated collective blade pitch h 1 text view.
     */
    private TextView mCalculatedCollectiveBladePitchH1TextView;
    /**
     * The calculated collective blade pitch h 2 text view.
     */
    private TextView mCalculatedCollectiveBladePitchH2TextView;
    /**
     * The calculated collective blade pitch h 3 text view.
     */
    private TextView mCalculatedCollectiveBladePitchH3TextView;
    /**
     * The calculated blade pitch h 0 s 0 text view.
     */
    private TextView mCalculatedBladePitchH0S0TextView;
    /**
     * The calculated blade pitch h 0 s 1 text view.
     */
    private TextView mCalculatedBladePitchH0S1TextView;
    /**
     * The calculated blade pitch h 0 s 2 text view.
     */
    private TextView mCalculatedBladePitchH0S2TextView;
    /**
     * The calculated blade pitch h 1 s 0 text view.
     */
    private TextView mCalculatedBladePitchH1S0TextView;
    /**
     * The calculated blade pitch h 1 s 1 text view.
     */
    private TextView mCalculatedBladePitchH1S1TextView;
    /**
     * The calculated blade pitch h 1 s 2 text view.
     */
    private TextView mCalculatedBladePitchH1S2TextView;
    /**
     * The calculated blade pitch h 2 s 0 text view.
     */
    private TextView mCalculatedBladePitchH2S0TextView;
    /**
     * The calculated blade pitch h 2 s 1 text view.
     */
    private TextView mCalculatedBladePitchH2S1TextView;
    /**
     * The calculated blade pitch h 2 s 2 text view.
     */
    private TextView mCalculatedBladePitchH2S2TextView;
    /**
     * The calculated blade pitch h 3 s 0 text view.
     */
    private TextView mCalculatedBladePitchH3S0TextView;
    /**
     * The calculated blade pitch h 3 s 1 text view.
     */
    private TextView mCalculatedBladePitchH3S1TextView;
    /**
     * The calculated blade pitch h 3 s 2 text view.
     */
    private TextView mCalculatedBladePitchH3S2TextView;
    /**
     * The calculated servo degrees h 0 s 0 text view.
     */
    private TextView mCalculatedServoDegreesH0S0TextView;
    /**
     * The calculated servo degrees h 0 s 1 text view.
     */
    private TextView mCalculatedServoDegreesH0S1TextView;
    /**
     * The calculated servo degrees h 0 s 2 text view.
     */
    private TextView mCalculatedServoDegreesH0S2TextView;
    /**
     * The calculated servo degrees h 1 s 0 text view.
     */
    private TextView mCalculatedServoDegreesH1S0TextView;
    /**
     * The calculated servo degrees h 1 s 1 text view.
     */
    private TextView mCalculatedServoDegreesH1S1TextView;
    /**
     * The calculated servo degrees h 1 s 2 text view.
     */
    private TextView mCalculatedServoDegreesH1S2TextView;
    /**
     * The calculated servo degrees h 2 s 0 text view.
     */
    private TextView mCalculatedServoDegreesH2S0TextView;
    /**
     * The calculated servo degrees h 2 s 1 text view.
     */
    private TextView mCalculatedServoDegreesH2S1TextView;
    /**
     * The calculated servo degrees h 2 s 2 text view.
     */
    private TextView mCalculatedServoDegreesH2S2TextView;
    /**
     * The calculated servo degrees h 3 s 0 text view.
     */
    private TextView mCalculatedServoDegreesH3S0TextView;
    /**
     * The calculated servo degrees h 3 s 1 text view.
     */
    private TextView mCalculatedServoDegreesH3S1TextView;
    /**
     * The calculated servo degrees h 3 s 2 text view.
     */
    private TextView mCalculatedServoDegreesH3S2TextView;
    /**
     * The calculated servo pulse h 0 s 0 text view.
     */
    private TextView mCalculatedServoPulseH0S0TextView;
    /**
     * The calculated servo pulse h 0 s 1 text view.
     */
    private TextView mCalculatedServoPulseH0S1TextView;
    /**
     * The calculated servo pulse h 0 s 2 text view.
     */
    private TextView mCalculatedServoPulseH0S2TextView;
    /**
     * The calculated servo pulse h 1 s 0 text view.
     */
    private TextView mCalculatedServoPulseH1S0TextView;
    /**
     * The calculated servo pulse h 1 s 1 text view.
     */
    private TextView mCalculatedServoPulseH1S1TextView;
    /**
     * The calculated servo pulse h 1 s 2 text view.
     */
    private TextView mCalculatedServoPulseH1S2TextView;
    /**
     * The calculated servo pulse h 2 s 0 text view.
     */
    private TextView mCalculatedServoPulseH2S0TextView;
    /**
     * The calculated servo pulse h 2 s 1 text view.
     */
    private TextView mCalculatedServoPulseH2S1TextView;
    /**
     * The calculated servo pulse h 2 s 2 text view.
     */
    private TextView mCalculatedServoPulseH2S2TextView;
    /**
     * The calculated servo pulse h 3 s 0 text view.
     */
    private TextView mCalculatedServoPulseH3S0TextView;
    /**
     * The calculated servo pulse h 3 s 1 text view.
     */
    private TextView mCalculatedServoPulseH3S1TextView;
    /**
     * The calculated servo pulse h 3 s 2 text view.
     */
    private TextView mCalculatedServoPulseH3S2TextView;
    /**
     * The model status heli 0 cyclic x label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mModelStatusHeli0CyclicXLabelTextView;
    /**
     * The model status heli 0 cyclic y label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mModelStatusHeli0CyclicYLabelTextView;
    /**
     * The model status heli 1 cyclic x label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mModelStatusHeli1CyclicXLabelTextView;
    /**
     * The model status heli 1 cyclic y label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mModelStatusHeli1CyclicYLabelTextView;
    /**
     * The model status heli 2 cyclic x label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mModelStatusHeli2CyclicXLabelTextView;
    /**
     * The model status heli 2 cyclic y label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mModelStatusHeli2CyclicYLabelTextView;
    /**
     * The model status heli 3 cyclic x label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mModelStatusHeli3CyclicXLabelTextView;
    /**
     * The model status heli 3 cyclic y label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mModelStatusHeli3CyclicYLabelTextView;
    /**
     * The collective delta orientation h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaOrientationH0LabelTextView;
    /**
     * The collective delta orientation h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaOrientationH1LabelTextView;
    /**
     * The collective delta orientation h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaOrientationH2LabelTextView;
    /**
     * The collective delta orientation h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaOrientationH3LabelTextView;
    /**
     * The collective delta altitude h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaAltitudeH0LabelTextView;
    /**
     * The collective delta altitude h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaAltitudeH1LabelTextView;
    /**
     * The collective delta altitude h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaAltitudeH2LabelTextView;
    /**
     * The collective delta altitude h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaAltitudeH3LabelTextView;
    /**
     * The collective delta heading h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaHeadingH0LabelTextView;
    /**
     * The collective delta heading h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaHeadingH1LabelTextView;
    /**
     * The collective delta heading h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaHeadingH2LabelTextView;
    /**
     * The collective delta heading h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaHeadingH3LabelTextView;
    /**
     * The collective delta total h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaTotalH0LabelTextView;
    /**
     * The collective delta total h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaTotalH1LabelTextView;
    /**
     * The collective delta total h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaTotalH2LabelTextView;
    /**
     * The collective delta total h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveDeltaTotalH3LabelTextView;
    /**
     * The collective value h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveValueH0LabelTextView;
    /**
     * The collective value h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveValueH1LabelTextView;
    /**
     * The collective value h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveValueH2LabelTextView;
    /**
     * The collective value h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCollectiveValueH3LabelTextView;
    /**
     * The calculated collective blade pitch h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedCollectiveBladePitchH0LabelTextView;
    /**
     * The calculated collective blade pitch h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedCollectiveBladePitchH1LabelTextView;
    /**
     * The calculated collective blade pitch h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedCollectiveBladePitchH2LabelTextView;
    /**
     * The calculated collective blade pitch h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedCollectiveBladePitchH3LabelTextView;
    /**
     * The cyclic value h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCyclicValueH0LabelTextView;
    /**
     * The cyclic value h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCyclicValueH1LabelTextView;
    /**
     * The cyclic value h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCyclicValueH2LabelTextView;
    /**
     * The cyclic value h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCyclicValueH3LabelTextView;
    /**
     * The calculated cyclic blade pitch h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedCyclicBladePitchH0LabelTextView;
    /**
     * The calculated cyclic blade pitch h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedCyclicBladePitchH1LabelTextView;
    /**
     * The calculated cyclic blade pitch h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedCyclicBladePitchH2LabelTextView;
    /**
     * The calculated cyclic blade pitch h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedCyclicBladePitchH3LabelTextView;
    /**
     * The calculated blade pitch h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedBladePitchH0LabelTextView;
    /**
     * The calculated blade pitch h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedBladePitchH1LabelTextView;
    /**
     * The calculated blade pitch h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedBladePitchH2LabelTextView;
    /**
     * The calculated blade pitch h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedBladePitchH3LabelTextView;
    /**
     * The calculated servo degrees h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedServoDegreesH0LabelTextView;
    /**
     * The calculated servo degrees h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedServoDegreesH1LabelTextView;
    /**
     * The calculated servo degrees h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedServoDegreesH2LabelTextView;
    /**
     * The calculated servo degrees h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedServoDegreesH3LabelTextView;
    /**
     * The calculated servo pulse h 0 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedServoPulseH0LabelTextView;
    /**
     * The calculated servo pulse h 1 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedServoPulseH1LabelTextView;
    /**
     * The calculated servo pulse h 2 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedServoPulseH2LabelTextView;
    /**
     * The calculated servo pulse h 3 label text view.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mCalculatedServoPulseH3LabelTextView;
    /**
     * The current model view id.
     */
    // Model parameters:
    private int mCurrentModelViewId;
    /**
     * The model view group.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mModelViewGroup;
    /**
     * The model servos tab label.
     */
    private TextView mModelServosTabLabel;
    /**
     * The model servos container.
     */
    private ViewGroup mModelServosContainer;
    /**
     * The model collective cyclic tab label.
     */
    private TextView mModelCollectiveCyclicTabLabel;
    /**
     * The model collective cyclic container.
     */
    private ViewGroup mModelCollectiveCyclicContainer;
    /**
     * The model blades tab label.
     */
    private TextView mModelBladesTabLabel;
    /**
     * The model blades container.
     */
    private ViewGroup mModelBladesContainer;
    /**
     * The model es cs tab label.
     */
    private TextView mModelESCsTabLabel;
    /**
     * The model es cs container.
     */
    private ViewGroup mModelESCsContainer;
    /**
     * The model targets tab label.
     */
    private TextView mModelTargetsTabLabel;
    /**
     * The model targets container.
     */
    private ViewGroup mModelTargetsContainer;
    /**
     * The model physics tab label.
     */
    private TextView mModelPhysicsTabLabel;
    /**
     * The model physics container.
     */
    private ViewGroup mModelPhysicsContainer;
    /**
     * The model relay container.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private ViewGroup mModelRelayContainer;
    /**
     * The model blades low edit text.
     */
    private EditText mModelBladesLowEditText;
    /**
     * The model blades low button.
     */
    private Button mModelBladesLowButton;
    /**
     * The model blades low floid button controller.
     */
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private FloidButtonController mModelBladesLowFloidButtonController;
    /**
     * The model blades zero edit text.
     */
    private EditText mModelBladesZeroEditText;
    /**
     * The model blades zero button.
     */
    private Button mModelBladesZeroButton;
    /**
     * The model blades zero floid button controller.
     */
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private FloidButtonController mModelBladesZeroFloidButtonController;
    /**
     * The model blades high edit text.
     */
    private EditText mModelBladesHighEditText;
    /**
     * The model blades high button.
     */
    private Button mModelBladesHighButton;
    /**
     * The model blades high floid button controller.
     */
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private FloidButtonController mModelBladesHighFloidButtonController;
    /**
     * The model h 0 s 0 low edit text.
     */
    private EditText mModelH0S0LowEditText;
    /**
     * The model h 0 s 1 low edit text.
     */
    private EditText mModelH0S1LowEditText;
    /**
     * The model h 0 s 2 low edit text.
     */
    private EditText mModelH0S2LowEditText;
    /**
     * The model h 0 s 0 zero edit text.
     */
    private EditText mModelH0S0ZeroEditText;
    /**
     * The model h 0 s 1 zero edit text.
     */
    private EditText mModelH0S1ZeroEditText;
    /**
     * The model h 0 s 2 zero edit text.
     */
    private EditText mModelH0S2ZeroEditText;
    /**
     * The model h 0 s 0 high edit text.
     */
    private EditText mModelH0S0HighEditText;
    /**
     * The model h 0 s 1 high edit text.
     */
    private EditText mModelH0S1HighEditText;
    /**
     * The model h 0 s 2 high edit text.
     */
    private EditText mModelH0S2HighEditText;
    /**
     * The model h 1 s 0 low edit text.
     */
    private EditText mModelH1S0LowEditText;
    /**
     * The model h 1 s 1 low edit text.
     */
    private EditText mModelH1S1LowEditText;
    /**
     * The model h 1 s 2 low edit text.
     */
    private EditText mModelH1S2LowEditText;
    /**
     * The model h 1 s 0 zero edit text.
     */
    private EditText mModelH1S0ZeroEditText;
    /**
     * The model h 1 s 1 zero edit text.
     */
    private EditText mModelH1S1ZeroEditText;
    /**
     * The model h 1 s 2 zero edit text.
     */
    private EditText mModelH1S2ZeroEditText;
    /**
     * The model h 1 s 0 high edit text.
     */
    private EditText mModelH1S0HighEditText;
    /**
     * The model h 1 s 1 high edit text.
     */
    private EditText mModelH1S1HighEditText;
    /**
     * The model h 1 s 2 high edit text.
     */
    private EditText mModelH1S2HighEditText;
    /**
     * The model h 2 s 0 low edit text.
     */
    private EditText mModelH2S0LowEditText;
    /**
     * The model h 2 s 1 low edit text.
     */
    private EditText mModelH2S1LowEditText;
    /**
     * The model h 2 s 2 low edit text.
     */
    private EditText mModelH2S2LowEditText;
    /**
     * The model h 2 s 0 zero edit text.
     */
    private EditText mModelH2S0ZeroEditText;
    /**
     * The model h 2 s 1 zero edit text.
     */
    private EditText mModelH2S1ZeroEditText;
    /**
     * The model h 2 s 2 zero edit text.
     */
    private EditText mModelH2S2ZeroEditText;
    /**
     * The model h 2 s 0 high edit text.
     */
    private EditText mModelH2S0HighEditText;
    /**
     * The model h 2 s 1 high edit text.
     */
    private EditText mModelH2S1HighEditText;
    /**
     * The model h 2 s 2 high edit text.
     */
    private EditText mModelH2S2HighEditText;
    /**
     * The model h 3 s 0 low edit text.
     */
    private EditText mModelH3S0LowEditText;
    /**
     * The model h 3 s 1 low edit text.
     */
    private EditText mModelH3S1LowEditText;
    /**
     * The model h 3 s 2 low edit text.
     */
    private EditText mModelH3S2LowEditText;
    /**
     * The model h 3 s 0 zero edit text.
     */
    private EditText mModelH3S0ZeroEditText;
    /**
     * The model h 3 s 1 zero edit text.
     */
    private EditText mModelH3S1ZeroEditText;
    /**
     * The model h 3 s 2 zero edit text.
     */
    private EditText mModelH3S2ZeroEditText;
    /**
     * The model h 3 s 0 high edit text.
     */
    private EditText mModelH3S0HighEditText;
    /**
     * The model h 3 s 1 high edit text.
     */
    private EditText mModelH3S1HighEditText;
    /**
     * The model h 3 s 2 high edit text.
     */
    private EditText mModelH3S2HighEditText;
    /**
     * The model collective min edit text.
     */
    private EditText mModelCollectiveMinEditText;
    /**
     * The model collective max edit text.
     */
    private EditText mModelCollectiveMaxEditText;
    /**
     * The model collective default edit text.
     */
    private EditText mModelCollectiveDefaultEditText;
    /**
     * The model cyclic range edit text.
     */
    private EditText mModelCyclicRangeEditText;
    /**
     * The model cyclic default edit text.
     */
    private EditText mModelCyclicDefaultEditText;
    /**
     * The model esc type spinner.
     */
    private Spinner mModelESCTypeSpinner;
    /**
     * The model esc pulse min edit text.
     */
    private EditText mModelESCPulseMinEditText;
    /**
     * The model esc pulse max edit text.
     */
    private EditText mModelESCPulseMaxEditText;
    /**
     * The model esc low value edit text.
     */
    private EditText mModelESCLowValueEditText;
    /**
     * The model esc high value edit text.
     */
    private EditText mModelESCHighValueEditText;
    /**
     * The model attack angle min distance edit text.
     */
    private EditText mModelAttackAngleMinDistanceEditText;
    /**
     * The model attack angle max distance edit text.
     */
    private EditText mModelAttackAngleMaxDistanceEditText;
    /**
     * The model attack angle value edit text.
     */
    private EditText mModelAttackAngleValueEditText;
    /**
     * The model pitch delta min value edit text.
     */
    private EditText mModelPitchDeltaMinValueEditText;
    /**
     * The model pitch delta max value edit text.
     */
    private EditText mModelPitchDeltaMaxValueEditText;
    /**
     * The model pitch target velocity max value edit text.
     */
    private EditText mModelPitchTargetVelocityMaxValueEditText;
    /**
     * The model roll delta min value edit text.
     */
    private EditText mModelRollDeltaMinValueEditText;
    /**
     * The model roll delta max value edit text.
     */
    private EditText mModelRollDeltaMaxValueEditText;
    /**
     * The model roll target velocity max value edit text.
     */
    private EditText mModelRollTargetVelocityMaxValueEditText;
    /**
     * The model altitude to target min value edit text.
     */
    private EditText mModelAltitudeToTargetMinValueEditText;
    /**
     * The model altitude to target max value edit text.
     */
    private EditText mModelAltitudeToTargetMaxValueEditText;
    /**
     * The model altitude target velocity max value edit text.
     */
    private EditText mModelAltitudeTargetVelocityMaxValueEditText;
    /**
     * The model heading delta min value edit text.
     */
    private EditText mModelHeadingDeltaMinValueEditText;
    /**
     * The model heading delta max value edit text.
     */
    private EditText mModelHeadingDeltaMaxValueEditText;
    /**
     * The model heading target velocity max value edit text.
     */
    private EditText mModelHeadingTargetVelocityMaxValueEditText;
    /**
     * The model distance to target min value edit text.
     */
    private EditText mModelDistanceToTargetMinValueEditText;
    /**
     * The model distance to target max value edit text.
     */
    private EditText mModelDistanceToTargetMaxValueEditText;
    /**
     * The model distance target velocity max value edit text.
     */
    private EditText mModelDistanceTargetVelocityMaxValueEditText;
    /**
     * The model orientation min distance value edit text.
     */
    private EditText mModelOrientationMinDistanceValueEditText;
    /**
     * The model lift off target altitude delta edit text.
     */
    private EditText mModelLiftOffTargetAltitudeDeltaEditText;
    /**
     * The model servo pulse min edit text.
     */
    private EditText mModelServoPulseMinEditText;
    /**
     * The model servo pulse max edit text.
     */
    private EditText mModelServoPulseMaxEditText;
    /**
     * The model servo degree min edit text.
     */
    private EditText mModelServoDegreeMinEditText;
    /**
     * The model servo degree max edit text.
     */
    private EditText mModelServoDegreeMaxEditText;
    /**
     * The model servo sign left toggle button.
     */
    private ToggleButton mModelServoSignLeftToggleButton;
    /**
     * The model servo sign right toggle button.
     */
    private ToggleButton mModelServoSignRightToggleButton;
    /**
     * The model servo sign pitch toggle button.
     */
    private ToggleButton mModelServoSignPitchToggleButton;
    /**
     * The model heli 0 servo offset left edit text.
     */
    private EditText mModelServoOffsetLeftEditText0;
    /**
     * The model heli 0 servo offset right edit text.
     */
    private EditText mModelServoOffsetRightEditText0;
    /**
     * The model heli 0 servo offset pitch edit text.
     */
    private EditText mModelServoOffsetPitchEditText0;
    /**
     * The model heli 1 servo offset left edit text.
     */
    private EditText mModelServoOffsetLeftEditText1;
    /**
     * The model heli 1 servo offset right edit text.
     */
    private EditText mModelServoOffsetRightEditText1;
    /**
     * The model heli 1 servo offset pitch edit text.
     */
    private EditText mModelServoOffsetPitchEditText1;
    /**
     * The model heli 2 servo offset left edit text.
     */
    private EditText mModelServoOffsetLeftEditText2;
    /**
     * The model heli 2 servo offset right edit text.
     */
    private EditText mModelServoOffsetRightEditText2;
    /**
     * The model heli 2 servo offset pitch edit text.
     */
    private EditText mModelServoOffsetPitchEditText2;
    /**
     * The model heli 3 servo offset left edit text.
     */
    private EditText mModelServoOffsetLeftEditText3;
    /**
     * The model heli 3 servo offset right edit text.
     */
    private EditText mModelServoOffsetRightEditText3;
    /**
     * The model heli 3 servo offset pitch edit text.
     */
    private EditText mModelServoOffsetPitchEditText3;
    /**
     * The model target pitch velocity alpha edit text.
     */
    private EditText mModelTargetPitchVelocityAlphaEditText;
    /**
     * The model target roll velocity alpha edit text.
     */
    private EditText mModelTargetRollVelocityAlphaEditText;
    /**
     * The model target heading velocity alpha edit text.
     */
    private EditText mModelTargetHeadingVelocityAlphaEditText;
    /**
     * The model cyclic heading alpha edit text.
     */
    private EditText mModelCyclicHeadingAlphaEditText;
    /**
     * The model velocity delta cyclic alpha scale edit text.
     */
    private EditText mModelVelocityDeltaCyclicAlphaScaleEditText;
    /**
     * The model acceleration scale multiplier edit text.
     */
    private EditText mModelAccelerationScaleMultiplierEditText;
    /**
     * The model target altitude velocity alpha edit text.
     */
    private EditText mModelTargetAltitudeVelocityAlphaEditText;
    /**
     * The model land mode required time at min altitude edit text.
     */
    private EditText mModelLandModeRequiredTimeAtMinAltitudeEditText;
    /**
     * The model heli startup number steps edit text.
     */
    private EditText mModelHeliStartupNumberStepsEditText;
    /**
     * The model heli startup step tick edit text.
     */
    private EditText mModelHeliStartupStepTickEditText;
    /**
     * The model esc collective calculation midpoint.
     */
    private EditText mModelEscCollectiveCalcMidpointEditText;
    /**
     * The model esc collective calculation midpoint.
     */
    private EditText mModelEscCollectiveLowValueEditText;
    /**
     * The model esc collective calculation midpoint.
     */
    private EditText mModelEscCollectiveMidValueEditText;
    /**
     * The model esc collective calculation midpoint.
     */
    private EditText mModelEscCollectiveHighValueEditText;
    /**
     * The model load save auto complete text view.
     */
    private AutoCompleteTextView mModelLoadSaveAutoCompleteTextView;
    /**
     * The model retrieve button.
     */
    private Button mModelRetrieveButton;
    /**
     * The model retrieve floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mModelRetrieveFloidButtonController;
    /**
     * The model set button.
     */
    private Button mModelSetButton;
    /**
     * The model set floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mModelSetFloidButtonController;
    /**
     * The model save button.
     */
    private Button mModelSaveButton;
    /**
     * The model load button.
     */
    private Button mModelLoadButton;
    /**
     * The model load from eeprom button.
     */
    private Button mModelLoadFromEEPROMButton;
    /**
     * The model start mode spinner.
     */
    private Spinner mModelStartModeSpinner;
    /**
     * The model rotation mode spinner.
     */
    private Spinner mModelRotationModeSpinner;
    /**
     * The model load from eeprom floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mModelLoadFromEEPROMFloidButtonController;
    /**
     * The model save to eeprom button.
     */
    private Button mModelSaveToEEPROMButton;
    /**
     * The model save to eeprom floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mModelSaveToEEPROMFloidButtonController;
    /**
     * The model clear eeprom button.
     */
    private Button mModelClearEEPROMButton;
    /**
     * The model clear eeprom floid button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mModelClearEEPROMFloidButtonController;
    // Test target:
    /**
     * The test goals view group.
     */
    private ViewGroup mTestGoalsViewGroup;
    /**
     * The test set goals button.
     */
    private Button mTestSetGoalsButton;
    /**
     * The test set goals button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidButtonController mTestSetGoalsButtonController;
    /**
     * The lift-off mode toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mLiftOffModeToggleButtonController;
    /**
     * The normal mode toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mNormalModeToggleButtonController;
    /**
     * The land mode toggle button controller.
     */
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private FloidToggleButtonController mLandModeToggleButtonController;
    /**
     * The test goals 'x' edit text.
     */
    private EditText mTestGoalsXEditText;
    /**
     * The test goals 'y' edit text.
     */
    private EditText mTestGoalsYEditText;
    /**
     * The test goals 'z' edit text.
     */
    private EditText mTestGoalsZEditText;
    /**
     * The test goals 'a' edit text.
     */
    private EditText mTestGoalsAEditText;
    // Number formatters:
    /**
     * The short decimal format.
     */
    private final DecimalFormat mShortDecimalFormat = new DecimalFormat("0.00");
    /**
     * The one decimal format.
     */
    private final DecimalFormat mOneDecimalFormat = new DecimalFormat("0.0");
    /**
     * The no decimal format.
     */
    private final DecimalFormat mNoDecimalFormat = new DecimalFormat("0");
    /**
     * The long decimal format.
     */
    private final DecimalFormat mLongDecimalFormat = new DecimalFormat("0.0000");
    // Menu tab background colors:
    /**
     * The focused sub menu tab color.
     */
    private int mFocusedSubMenuTabColor;
    /**
     * The un focused sub menu tab color.
     */
    private int mUnFocusedSubMenuTabColor;
    // Menu tab text colors:
    /**
     * The focused sub menu text color.
     */
    private int mFocusedSubMenuTabTextColor;
    /**
     * The un focused sub menu text color.
     */
    private int mUnFocusedSubMenuTabTextColor;

    /**
     * The imaging photo camera id text view.
     */
    private TextView mImagingPhotoCameraIdTextView;

    /**
     * The imaging video camera id text view.
     */
    private TextView mImagingVideoCameraIdTextView;

    /**
     * The imaging supports flash text view.
     */
    private TextView mImagingSupportsFlashTextView;

    /**
     * The imaging legacy hardware text view.
     */
    private TextView mImagingLegacyHardwareTextView;

    /**
     * The imaging photo status text view.
     */
    private TextView mImagingPhotoStateTextView;

    /**
     * The imaging remaining capture sessions text view.
     */
    private TextView mImagingRemainingCaptureSessionsTextView;

    /**
     * The imaging status initialized text view.
     */
    private TextView mImagingStatusInitializedTextView;

    /**
     * The imaging status taking photo text view.
     */
    private TextView mImagingStatusTakingPhotoTextView;

    /**
     * The imaging status photo id dirty text view.
     */
    private TextView mImagingStatusPhotoIdDirtyTextView;

    /**
     * The imaging status photo properties dirty text view.
     */
    private TextView mImagingStatusPhotoPropertiesDirtyTextView;

    /**
     * The imaging status photo camera open text view.
     */
    private TextView mImagingStatusPhotoCameraOpenTextView;

    /**
     * The imaging status photo surfaces set up text view.
     */
    private TextView mImagingStatusPhotoSurfacesSetUpTextView;

    /**
     * The imaging status photo capture request set up text view.
     */
    private TextView mImagingStatusPhotoCaptureRequestSetUpTextView;

    /**
     * The imaging status photo capture session configured text view.
     */
    private TextView mImagingStatusPhotoCaptureSessionConfiguredTextView;

    /**
     * The imaging status taking video text view.
     */
    private TextView mImagingStatusTakingVideoTextView;

    /**
     * The imaging status video id dirty text view.
     */
    private TextView mImagingStatusVideoIdDirtyTextView;

    /**
     * The imaging status video properties dirty text view.
     */
    private TextView mImagingStatusVideoPropertiesDirtyTextView;

    /**
     * The imaging status video camera open text view.
     */
    private TextView mImagingStatusVideoCameraOpenTextView;

    /**
     * The imaging status video surfaces set up text view.
     */
    private TextView mImagingStatusVideoSurfacesSetUpTextView;

    /**
     * The imaging status video capture request set up text view.
     */
    private TextView mImagingStatusVideoCaptureRequestSetUpTextView;

    /**
     * The imaging status video capture session configured text view.
     */
    private TextView mImagingStatusVideoCaptureSessionConfiguredTextView;

    /**
     * The imaging status supports video snapshot text view.
     */
    private TextView mImagingStatusSupportsVideoSnapshotTextView;

    /**
     * The imaging status taking video snapshot text view.
     */
    private TextView mImagingStatusTakingVideoSnapshotTextView;

    /**
     * The imaging take photo button.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mImagingTakePhotoButton;

    /**
     * The imaging start video button.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mImagingStartVideoButton;
    /**
     * The imaging stop video button.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private TextView mImagingStopVideoButton;

    /**
     * The photo image display.
     */
    private ImageView mPhotoImageView;

    /**
     * The video preview image display.
     */
    private ImageView mVideoImageView;

    /**
     * Whether we are quitting during onDestroy
     */
    private boolean quitting = false;

    /**
     * Comm good status
     */
    public static final int COMM_GOOD = 0;
    /**
     * Comm warning status
     */
    public static final int COMM_WARNING = 1;
    /**
     * Comm error status
     */
    public static final int COMM_ERROR = 2;
    /**
     * Comm floid
     */
    public static final int COMM_FLOID = 0;
    /**
     * Comm droid
     */
    public static final int COMM_DROID = 1;

    /**
     * Sets action bar.
     */
    private void setupActionBar() {
        mAccessoryOpenDrawable = ContextCompat.getDrawable(this, R.drawable.faigle_labs_icon);
        mAccessoryClosedDrawable = ContextCompat.getDrawable(this, R.drawable.faigle_labs_icon_disconnected);
        // Action bar:
        androidx.appcompat.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setIcon(mAccessoryClosedDrawable);
            actionBar.setDisplayOptions(0, androidx.appcompat.app.ActionBar.DISPLAY_SHOW_TITLE);
        }
    }

    /**
     * Sets status gui.
     */
    private void setupStatusGui() {
        // Faiglelabs banner:
        mFaiglelabsBanner = findViewById(R.id.faiglelabs_banner);
        mFaiglelabsBanner.setOnClickListener(view -> {
            String url = "http://www.faiglelabs.com";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });
        // Get the panel colors for the altimeter display:
        panelColor = ContextCompat.getColor(this, R.color.PanelColor);
        warningPanelColor = ContextCompat.getColor(this, R.color.WarningPanelColor);
        // FloidDroid Status GUI Items:
        // ============================
        // System Indicator layout:
        RelativeLayout floidSystemIndicatorLayout = findViewById(R.id.floid_system_indicator_layout);
        final ScrollView floidAdditionalIndicatorsLayout = findViewById(R.id.floid_system_additional_indicators_scrollview);
        additionalStatusTextView = findViewById(R.id.additional_status_text_view);
        // If we click the indicators layout the additional system indicators to show/hides:
        floidSystemIndicatorLayout.setOnClickListener(v -> {
            if(floidAdditionalIndicatorsLayout.getVisibility()==View.VISIBLE) {
                floidAdditionalIndicatorsLayout.setVisibility(View.GONE);
            } else {
                floidAdditionalIndicatorsLayout.setVisibility(View.VISIBLE);
            }
        });
        updateAdditionalStatus();
        // Droid status:
        mFloidBatteryIndicatorFrameLayout = findViewById(R.id.floid_battery_status_indicator);
        mFloidBatteryIndicatorStatusTextView = mFloidBatteryIndicatorFrameLayout.findViewById(R.id.floid_battery_indicator_status_label);
        mFloidBatteryIndicatorNameTextView = mFloidBatteryIndicatorFrameLayout.findViewById(R.id.floid_battery_indicator_name_label);
        mFloidBatteryIndicatorLinearLayout = mFloidBatteryIndicatorFrameLayout.findViewById(R.id.floid_battery_drawable_layout);
        mFloidBatteryIndicatorNameTextView.setText(R.string.default_label_floid);
        setBatteryStatusIndicator(mFloidBatteryIndicatorStatusTextView, mFloidBatteryIndicatorLinearLayout, -1.0, FLOID_BATTERY_INDICATOR_MINIMUM, FLOID_BATTERY_INDICATOR_LOW, FLOID_BATTERY_INDICATOR_MEDIUM, FLOID_BATTERY_INDICATOR_MAXIMUM, false, "v");
        mDroidBatteryIndicatorFrameLayout = findViewById(R.id.droid_battery_status_indicator);
        mDroidBatteryIndicatorStatusTextView = mDroidBatteryIndicatorFrameLayout.findViewById(R.id.floid_battery_indicator_status_label);
        mDroidBatteryIndicatorNameTextView = mDroidBatteryIndicatorFrameLayout.findViewById(R.id.floid_battery_indicator_name_label);
        mDroidBatteryIndicatorLinearLayout = mDroidBatteryIndicatorFrameLayout.findViewById(R.id.floid_battery_drawable_layout);
        mDroidBatteryIndicatorNameTextView.setText(R.string.default_label_droid);
        setBatteryStatusIndicator(mDroidBatteryIndicatorStatusTextView, mDroidBatteryIndicatorLinearLayout, -1.0, DROID_BATTERY_INDICATOR_MINIMUM, DROID_BATTERY_INDICATOR_LOW, DROID_BATTERY_INDICATOR_MEDIUM, DROID_BATTERY_INDICATOR_MAXIMUM, true, "%");
        mCurrentStatusTextView = findViewById(R.id.current_status_text);
        mCurrentStatusLabelTextView = findViewById(R.id.current_status_label);
        addToastHelp(mCurrentStatusLabelTextView, "Droid Status: current floid status", this);
        mCurrentModeTextView = findViewById(R.id.current_mode_text);
        mCurrentModeLabelTextView = findViewById(R.id.current_mode_label);
        addToastHelp(mCurrentModeLabelTextView, "Droid Mode: the current floid mode: \"Test\" or \"Mode\"", this);
        mDroidStartedTextView = findViewById(R.id.droid_started_text);
        mDroidStartedLabelTextView = findViewById(R.id.droid_started_label);
        addToastHelp(mDroidStartedLabelTextView, "Droid Started: has the Droid started", this);
        mServerConnectedTextView = findViewById(R.id.server_connected_text);
        mServerConnectedLabelTextView = findViewById(R.id.server_connected_label);
        addToastHelp(mServerConnectedLabelTextView, "Server Connected: is the droid connected to mission server", this);
        mServerErrorTextView = findViewById(R.id.server_error_text);
        mServerErrorLabelTextView = findViewById(R.id.server_error_label);
        addToastHelp(mServerErrorLabelTextView, "Server Error: error communicating with mission server", this);
        mHelisStartedTextView = findViewById(R.id.helis_started_text);
        mHelisStartedLabelTextView = findViewById(R.id.helis_started_label);
        addToastHelp(mHelisStartedLabelTextView, "Helis Started: have the helis been started", this);
        mLiftedOffTextView = findViewById(R.id.lifted_off_text);
        mLiftedOffLabelTextView = findViewById(R.id.lifted_off_label);
        addToastHelp(mLiftedOffLabelTextView, "Lifted Off: has the floid lifted off", this);
        mParachuteDeployedTextView = findViewById(R.id.parachute_deployed_text);
        mParachuteDeployedLabelTextView = findViewById(R.id.parachute_deployed_label);
        addToastHelp(mParachuteDeployedLabelTextView, "Parachute Deployed: has the parachute been deployed", this);
        mTakingPhotoTextView = findViewById(R.id.taking_photo_text);
        mTakingPhotoLabelTextView = findViewById(R.id.taking_photo_label);
        addToastHelp(mTakingPhotoLabelTextView, "Taking Photo: is droid currently taking a photo", this);
        mPhotoNumberTextView = findViewById(R.id.photo_number_text);
        mPhotoNumberLabelTextView = findViewById(R.id.photo_number_label);
        addToastHelp(mPhotoNumberLabelTextView, "Photo Number: current droid photo number", this);
        mTakingVideoTextView = findViewById(R.id.taking_video_text);
        mTakingVideoLabelTextView = findViewById(R.id.taking_video_label);
        addToastHelp(mTakingVideoLabelTextView, "Taking Video: is droid currently taking video", this);
        mVideoNumberTextView = findViewById(R.id.video_number_text);
        mVideoNumberLabelTextView = findViewById(R.id.video_number_label);
        addToastHelp(mVideoNumberLabelTextView, "Video Number: current droid video number", this);
        mHomePositionLabelTextView = findViewById(R.id.home_position_label);
        addToastHelp(mHomePositionLabelTextView, "Home Position: the home position of the droid if acquired", this);
        mHomePositionAcquiredTextView = findViewById(R.id.home_position_acquired_text);
        addToastHelp(mHomePositionAcquiredTextView, "Home Position Acquired: has the droid acquired the home position", this);
        mHomePositionLatTextView = findViewById(R.id.home_position_lat_text);
        addToastHelp(mHomePositionLatTextView, "Home Position Latitude: in Decimal Degrees", this);
        mHomePositionLngTextView = findViewById(R.id.home_position_lng_text);
        addToastHelp(mHomePositionLngTextView, "Home Position Longitude: in Decimal Degrees", this);
        mHomePositionHeightTextView = findViewById(R.id.home_position_height_text);
        addToastHelp(mHomePositionHeightTextView, "Home Position Altitude: in Meters", this);
        mFloidConnectedTextView = findViewById(R.id.floid_connected_text);
        mFloidConnectedLabelTextView = findViewById(R.id.floid_connected_label);
        addToastHelp(mFloidConnectedLabelTextView, "Floid Connected: is a Floid connected", this);
        // Border drawables:
        mBorderDrawable = ContextCompat.getDrawable(this, R.drawable.border);
        mWarningBorderDrawable = ContextCompat.getDrawable(this, R.drawable.warning_border);
        mErrorBorderDrawable = ContextCompat.getDrawable(this, R.drawable.error_border);
        // New Comm Status: (Floid/Server)
        mFloidCommunicatorLEDOnDrawable = ContextCompat.getDrawable(this, R.drawable.floid_communicator_on);
        mFloidCommunicatorLEDOffDrawable = ContextCompat.getDrawable(this, R.drawable.floid_communicator_off);
        mFloidCommLEDImageViews = new ImageView[5];
        mFloidCommLEDImageViews[0] = findViewById(R.id.floid_communicator_status_0);
        mFloidCommLEDImageViews[1] = findViewById(R.id.floid_communicator_status_1);
        mFloidCommLEDImageViews[2] = findViewById(R.id.floid_communicator_status_2);
        mFloidCommLEDImageViews[3] = findViewById(R.id.floid_communicator_status_3);
        mFloidCommLEDImageViews[4] = findViewById(R.id.floid_communicator_status_4);
        mServerCommunicatorLEDOnDrawable = ContextCompat.getDrawable(this, R.drawable.server_communicator_on);
        mServerCommunicatorLEDFailDrawable = ContextCompat.getDrawable(this, R.drawable.server_communicator_fail);
        mServerCommunicatorLEDOffDrawable = ContextCompat.getDrawable(this, R.drawable.server_communicator_off);
        mServerCommunicatorLEDFailOffDrawable = ContextCompat.getDrawable(this, R.drawable.server_communicator_fail_off);
        mServerCommunicatorLEDUnknownOffDrawable = ContextCompat.getDrawable(this, R.drawable.server_communicator_unknown_off);
        mServerCommLEDImageViews = new ImageView[5];
        mServerCommLEDStatuses = new int[5];
        mServerCommLEDImageViews[0] = findViewById(R.id.server_communicator_status_0);
        mServerCommLEDImageViews[1] = findViewById(R.id.server_communicator_status_1);
        mServerCommLEDImageViews[2] = findViewById(R.id.server_communicator_status_2);
        mServerCommLEDImageViews[3] = findViewById(R.id.server_communicator_status_3);
        mServerCommLEDImageViews[4] = findViewById(R.id.server_communicator_status_4);
        mServerCommLEDStatuses[0] = SERVER_COMM_STATUS_UNKNOWN;
        mServerCommLEDStatuses[1] = SERVER_COMM_STATUS_UNKNOWN;
        mServerCommLEDStatuses[2] = SERVER_COMM_STATUS_UNKNOWN;
        mServerCommLEDStatuses[3] = SERVER_COMM_STATUS_UNKNOWN;
        mServerCommLEDStatuses[4] = SERVER_COMM_STATUS_UNKNOWN;
        // Loop rates, display and mission toggles:
        mFloidLoopCountTextView = findViewById(R.id.floid_loop_count);
        mFloidLoopRateTextView = findViewById(R.id.floid_loop_rate);
        mFloidMemoryTextView = findViewById(R.id.floid_memory);
        mFloidModeToggleButton = findViewById(R.id.mode_relay).findViewById(R.id.relay_toggle_button);
        mDisplayToggleButton = findViewById(R.id.display_relay).findViewById(R.id.relay_toggle_button);
        mDisplayToggleButton.setText(R.string.default_label_display);
        mDisplayToggleButton.setTextOn(getResources().getText(R.string.default_label_display));
        mDisplayToggleButton.setTextOff(getResources().getText(R.string.default_label_display));
        mDisplayToggleButton.setChecked(mDisplayOn);
        mDisplayToggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mDisplayOn = true;
                // If we just turned the display on we want to force a re-display of the statuses...
                synchronized (mFloidActivityFloidStatusSynchronizer) {
                    mFloidActivityPreviousFloidStatus = null;
                }
                synchronized (mFloidActivityFloidModelStatusSynchronizer) {
                    mFloidActivityPreviousFloidModelStatus = null;
                }
            } else {
                mDisplayOn = false;
            }

        }
        );
        // Batteries and Currents:
        mBatteriesViewGroup = findViewById(R.id.batteries_current_container);
        mBatteryLowColor = ContextCompat.getColor(this, R.color.BatteryLowColor);
        mBatteryMediumColor = ContextCompat.getColor(this, R.color.BatteryMediumColor);
        mBatteryHighColor = ContextCompat.getColor(this, R.color.BatteryHighColor);
        mBStatusTextView = mBatteriesViewGroup.findViewById(R.id.b_status);     // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_MAIN_BATTERY)));
        mB0StatusTextView = mBatteriesViewGroup.findViewById(R.id.b0_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI0_BATTERY)));
        mB1StatusTextView = mBatteriesViewGroup.findViewById(R.id.b1_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI1_BATTERY)));
        mB2StatusTextView = mBatteriesViewGroup.findViewById(R.id.b2_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI2_BATTERY)));
        mB3StatusTetView = mBatteriesViewGroup.findViewById(R.id.b3_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI3_BATTERY)));
        mC0StatusTextView = mBatteriesViewGroup.findViewById(R.id.c0_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI0_CURRENT)));
        mC1StatusTextView = mBatteriesViewGroup.findViewById(R.id.c1_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI1_CURRENT)));
        mC2StatusTextView = mBatteriesViewGroup.findViewById(R.id.c2_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI2_CURRENT)));
        mC3StatusTextView = mBatteriesViewGroup.findViewById(R.id.c3_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_HELI3_CURRENT)));
        mAuxCurrentTextView = mBatteriesViewGroup.findViewById(R.id.aux_current_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_AUX_CURRENT)));
        mAuxBatteryTextView = mBatteriesViewGroup.findViewById(R.id.aux_battery_status);    // setText(Float.toString(arduinoByteArrayOffsetToFloat(statusMessage, STATUS_MESSAGE_OFFSET_AUX_BATTERY)));
        // Status text and auto scroll:
        mStatusTextViewGroup = findViewById(R.id.statusTextContainer);
        mStatusText = mStatusTextViewGroup.findViewById(R.id.status_text);
        mAutoScrollStatusCheckBox = findViewById(R.id.auto_scroll_checkbox);    // isChecked())
        mStatusScrollView = mStatusTextViewGroup.findViewById(R.id.status_text_scroll_view);    // fullScroll(ScrollView.FOCUS_DOWN);
        // Set up the get logs button:
        mGetLogsButton = findViewById(R.id.get_logs_relay).findViewById(R.id.floid_button);
        mGetLogsButton.setText(R.string.default_label_get_logs);
        mGetLogsButton.setOnClickListener(v -> {
            BufferedReader bufferedReader = null;
            try {
                String logcatCommand = "logcat *:V -d -t 200 -v process";
                if(mFloidServicePid != 0) {
                    logcatCommand = String.format(Locale.getDefault(), "%s --pid=%d", logcatCommand, mFloidServicePid);
                }
                Process process = Runtime.getRuntime().exec(logcatCommand);
                bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                StringBuilder log = new StringBuilder();
                String line;
                String pidString = "(" + mFloidServicePid + ")";
                if(pidString.length()<7) {
                    pidString = "( " + mFloidServicePid + ")";
                }
                String errorPidString = "E" + pidString;
                String warningPidString = "W" + pidString;
                String debugPidString = "D" + pidString;
                String infoPidString = "I" + pidString;
                String verbosePidString = "V" + pidString;
                String floidDroidRunnableString = "(FloidDroidRunnable)";
                String clientCommunicatorString = "(ClientCommunicator)";
                String floidServiceString = "(FloidService)";
                String networkSecurityConfigString = "(NetworkSecurityConfig)";
                String d3FloidServicString = "(d3:floidServic)";
                String textToSpeechString = "(TextToSpeech)";
                while ((line = bufferedReader.readLine()) != null) {
                    line = line.replace(errorPidString, "")
                            .replace(warningPidString, "")
                            .replace(infoPidString, "")
                            .replace(debugPidString, "")
                            .replace(verbosePidString, "")
                            .replace(floidDroidRunnableString, "")
                            .replace(clientCommunicatorString, "")
                            .replace(floidServiceString, "")
                            .replace(networkSecurityConfigString, "")
                            .replace(d3FloidServicString, "")
                            .replace(textToSpeechString, "");
                    log.append(line).append("\n");
                }
                addMessageToStatusText(log.toString());
                bufferedReader.close();
                bufferedReader = null;
            } catch (IOException e) {
                Log.e(TAG, "Exception retrieving logs", e);
            }
            if(bufferedReader!=null) {
                try {
                    bufferedReader.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception closing buffered reader for login", e);
                }
            }
        });
        // IMU:
        mPyrViewGroup = findViewById(R.id.pyr_container);
        mPyrHeadingLabelTextView = mPyrViewGroup.findViewById(R.id.pyr_heading_label);
        addToastHelp(mPyrHeadingLabelTextView, "Heading: floid heading from PYR in compass degrees (includes declination if set)", this);
        mPyrHeadingTextView = mPyrViewGroup.findViewById(R.id.pyr_heading);
        mPyrHeadingSmoothedTextView = mPyrViewGroup.findViewById(R.id.pyr_heading_smoothed);
        mPyrHeadingVelocityTextView = mPyrViewGroup.findViewById(R.id.pyr_heading_velocity);
        mPyrHeightLabelTextView = mPyrViewGroup.findViewById(R.id.pyr_height_label);
        addToastHelp(mPyrHeightLabelTextView, "Height: floid height (calculated from barometer) in meters", this);
        mPyrHeightTextView = mPyrViewGroup.findViewById(R.id.pyr_height);
        mPyrTempLabelTextView = mPyrViewGroup.findViewById(R.id.pyr_temp_label);
        addToastHelp(mPyrTempLabelTextView, "Temp: temperature in degrees Fahrenheit", this);
        mPyrTempTextView = mPyrViewGroup.findViewById(R.id.pyr_temp);
        mPyrDeclinationLabelTextView = mPyrViewGroup.findViewById(R.id.pyr_declination_label);
        addToastHelp(mPyrDeclinationLabelTextView, "Declination: current set magnetic declination", this);
        mPyrDeclinationTextView = mPyrViewGroup.findViewById(R.id.pyr_declination);
        mPyrPitchLabelTextView = mPyrViewGroup.findViewById(R.id.pyr_pitch_label);
        addToastHelp(mPyrPitchLabelTextView, "Pitch: floid pitch in decimal degrees - positive is nose up", this);
        mPyrPitchTextView = mPyrViewGroup.findViewById(R.id.pyr_pitch);
        mPyrPitchSmoothedTextView = mPyrViewGroup.findViewById(R.id.pyr_pitch_smoothed);
        mPyrRollLabelTextView = mPyrViewGroup.findViewById(R.id.pyr_roll_label);
        addToastHelp(mPyrRollLabelTextView, "Roll: floid roll in decimal degrees - positive is right wing up", this);
        mPyrRollTextView = mPyrViewGroup.findViewById(R.id.pyr_roll);
        mPyrRollSmoothedTextView = mPyrViewGroup.findViewById(R.id.pyr_roll_smoothed);
        mPyrPitchVelocityTextView = mPyrViewGroup.findViewById(R.id.pyr_pitch_velocity);
        mPyrRollVelocityTextView = mPyrViewGroup.findViewById(R.id.pyr_roll_velocity);
        mPyrHeightVelocityTextView = mPyrViewGroup.findViewById(R.id.pyr_height_velocity);
        mPyrHeightSmoothedTextView = mPyrViewGroup.findViewById(R.id.pyr_height_smoothed);
        // IMU Algorithm Status:
        mImuAlgorithmStatusTextView = mPyrViewGroup.findViewById(R.id.imu_algorithm_status);
        addToastHelp(mImuAlgorithmStatusTextView, "Algorithm Status: Status of IMU Calibration", this);
        // IMU Calibrate:
        mImuCalibrateButton = mPyrViewGroup.findViewById(R.id.imu_calibrate_relay).findViewById(R.id.floid_button);
        mImuCalibrateFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_IMU_CALIBRATE, mImuCalibrateButton, "Calibrate");
        // IMU Save:
        mImuSaveButton = mPyrViewGroup.findViewById(R.id.imu_save_relay).findViewById(R.id.floid_button);
        mImuSaveFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_IMU_SAVE, mImuSaveButton, "Save");
        // GPS:
        mGpsViewGroup = findViewById(R.id.gps_container);
        mGpsTimeTextView = mGpsViewGroup.findViewById(R.id.gps_time_text);
        addToastHelp(mGpsTimeTextView, "GPS Time: current time in GPS-time - integer format", this);
        mGpsXDegreesDecimalTextView = mGpsViewGroup.findViewById(R.id.gps_x_degrees_decimal_text);
        addToastHelp(mGpsXDegreesDecimalTextView, "Longitude: longitude in decimal degrees", this);
        mGpsYDegreesDecimalTextView = mGpsViewGroup.findViewById(R.id.gps_y_degrees_decimal_text);
        addToastHelp(mGpsYDegreesDecimalTextView, "Latitude: latitude in decimal degrees", this);
        mGpsFixQualityTextView = mGpsViewGroup.findViewById(R.id.gps_fix_quality_text);
        addToastHelp(mGpsFixQualityTextView, "Fix Quality: gps fix quality 0-Invalid 1-GPS 2-DGPS", this);
        mGpsZMetersTextView = mGpsViewGroup.findViewById(R.id.gps_z_meters_text);
        addToastHelp(mGpsZMetersTextView, "Altitude (m): altitude in meters", this);
        mGpsZFeetTextView = mGpsViewGroup.findViewById(R.id.gps_z_feet_text);
        addToastHelp(mGpsZFeetTextView, "Altitude (ft): altitude in feet", this);
        mGpsHDOPTextView = mGpsViewGroup.findViewById(R.id.gps_hdop_text);
        addToastHelp(mGpsHDOPTextView, "HDOP: horizontal degree of precision - less than 1.0 is better", this);
        mGpsSatsTextView = mGpsViewGroup.findViewById(R.id.gps_sats_text);
        addToastHelp(mGpsSatsTextView, "Satellites: number of satellites in current calculation", this);
        // Helis / Servos / Escs:
        mHelisViewGroup = findViewById(R.id.helis_container);
        mHeli0ViewGroup = findViewById(R.id.heli0);
        mHeli1ViewGroup = findViewById(R.id.heli1);
        mHeli2ViewGroup = findViewById(R.id.heli2);
        mHeli3ViewGroup = findViewById(R.id.heli3);
        mServo0LTextView = mHeli0ViewGroup.findViewById(R.id.left_servo_value);
        mServo0RTextView = mHeli0ViewGroup.findViewById(R.id.right_servo_value);
        mServo0PTextView = mHeli0ViewGroup.findViewById(R.id.pitch_servo_value);
        mServo1LTextView = mHeli1ViewGroup.findViewById(R.id.left_servo_value);
        mServo1RTextView = mHeli1ViewGroup.findViewById(R.id.right_servo_value);
        mServo1PTextView = mHeli1ViewGroup.findViewById(R.id.pitch_servo_value);
        mServo2LTextView = mHeli2ViewGroup.findViewById(R.id.left_servo_value);
        mServo2RTextView = mHeli2ViewGroup.findViewById(R.id.right_servo_value);
        mServo2PTextView = mHeli2ViewGroup.findViewById(R.id.pitch_servo_value);
        mServo3LTextView = mHeli3ViewGroup.findViewById(R.id.left_servo_value);
        mServo3RTextView = mHeli3ViewGroup.findViewById(R.id.right_servo_value);
        mServo3PTextView = mHeli3ViewGroup.findViewById(R.id.pitch_servo_value);
        mServo0LPulseTextView = mHeli0ViewGroup.findViewById(R.id.left_servo_pulse);
        mServo0RPulseTextView = mHeli0ViewGroup.findViewById(R.id.right_servo_pulse);
        mServo0PPulseTextView = mHeli0ViewGroup.findViewById(R.id.pitch_servo_pulse);
        mServo1LPulseTextView = mHeli1ViewGroup.findViewById(R.id.left_servo_pulse);
        mServo1RPulseTextView = mHeli1ViewGroup.findViewById(R.id.right_servo_pulse);
        mServo1PPulseTextView = mHeli1ViewGroup.findViewById(R.id.pitch_servo_pulse);
        mServo2LPulseTextView = mHeli2ViewGroup.findViewById(R.id.left_servo_pulse);
        mServo2RPulseTextView = mHeli2ViewGroup.findViewById(R.id.right_servo_pulse);
        mServo2PPulseTextView = mHeli2ViewGroup.findViewById(R.id.pitch_servo_pulse);
        mServo3LPulseTextView = mHeli3ViewGroup.findViewById(R.id.left_servo_pulse);
        mServo3RPulseTextView = mHeli3ViewGroup.findViewById(R.id.right_servo_pulse);
        mServo3PPulseTextView = mHeli3ViewGroup.findViewById(R.id.pitch_servo_pulse);
        mEsc0ValueTextView = mHeli0ViewGroup.findViewById(R.id.esc_value);
        mEsc1ValueTextView = mHeli1ViewGroup.findViewById(R.id.esc_value);
        mEsc2ValueTextView = mHeli2ViewGroup.findViewById(R.id.esc_value);
        mEsc3ValueTextView = mHeli3ViewGroup.findViewById(R.id.esc_value);
        mEsc0PulseTextView = mHeli0ViewGroup.findViewById(R.id.esc_pulse);
        mEsc1PulseTextView = mHeli1ViewGroup.findViewById(R.id.esc_pulse);
        mEsc2PulseTextView = mHeli2ViewGroup.findViewById(R.id.esc_pulse);
        mEsc3PulseTextView = mHeli3ViewGroup.findViewById(R.id.esc_pulse);
        // Pan Tilts:
        mPanTiltsViewGroup = findViewById(R.id.pan_tilt_container);
        mPanTilt0ViewGroup = mPanTiltsViewGroup.findViewById(R.id.pantilt0);
        mPanTilt1ViewGroup = mPanTiltsViewGroup.findViewById(R.id.pantilt1);
        mPanTilt2ViewGroup = mPanTiltsViewGroup.findViewById(R.id.pantilt2);
        mPanTilt3ViewGroup = mPanTiltsViewGroup.findViewById(R.id.pantilt3);
        mPanTilt0ToggleButton = mPanTilt0ViewGroup.findViewById(R.id.pantilt_relay).findViewById(R.id.relay_toggle_button);
        mPanTilt1ToggleButton = mPanTilt1ViewGroup.findViewById(R.id.pantilt_relay).findViewById(R.id.relay_toggle_button);
        mPanTilt2ToggleButton = mPanTilt2ViewGroup.findViewById(R.id.pantilt_relay).findViewById(R.id.relay_toggle_button);
        mPanTilt3ToggleButton = mPanTilt3ViewGroup.findViewById(R.id.pantilt_relay).findViewById(R.id.relay_toggle_button);
        mPanTilt0ModeTextView = mPanTilt0ViewGroup.findViewById(R.id.pantilt_mode_text);
        mPanTilt1ModeTextView = mPanTilt1ViewGroup.findViewById(R.id.pantilt_mode_text);
        mPanTilt2ModeTextView = mPanTilt2ViewGroup.findViewById(R.id.pantilt_mode_text);
        mPanTilt3ModeTextView = mPanTilt3ViewGroup.findViewById(R.id.pantilt_mode_text);
        mPanTilt0PanValueTextView = mPanTilt0ViewGroup.findViewById(R.id.pantilt_pan_value_text);
        mPanTilt0TiltValueTextView = mPanTilt0ViewGroup.findViewById(R.id.pantilt_tilt_value_text);
        mPanTilt1PanValueTextView = mPanTilt1ViewGroup.findViewById(R.id.pantilt_pan_value_text);
        mPanTilt1TiltValueTextView = mPanTilt1ViewGroup.findViewById(R.id.pantilt_tilt_value_text);
        mPanTilt2PanValueTextView = mPanTilt2ViewGroup.findViewById(R.id.pantilt_pan_value_text);
        mPanTilt2TiltValueTextView = mPanTilt2ViewGroup.findViewById(R.id.pantilt_tilt_value_text);
        mPanTilt3PanValueTextView = mPanTilt3ViewGroup.findViewById(R.id.pantilt_pan_value_text);
        mPanTilt3TiltValueTextView = mPanTilt3ViewGroup.findViewById(R.id.pantilt_tilt_value_text);
        mPanTilt0TargetXTextView = mPanTilt0ViewGroup.findViewById(R.id.pantilt_target_x_text);
        mPanTilt0TargetYTextView = mPanTilt0ViewGroup.findViewById(R.id.pantilt_target_y_text);
        mPanTilt0TargetZTextView = mPanTilt0ViewGroup.findViewById(R.id.pantilt_target_z_text);
        mPanTilt1TargetXTextView = mPanTilt1ViewGroup.findViewById(R.id.pantilt_target_x_text);
        mPanTilt1TargetYTextView = mPanTilt1ViewGroup.findViewById(R.id.pantilt_target_y_text);
        mPanTilt1TargetZTextView = mPanTilt1ViewGroup.findViewById(R.id.pantilt_target_z_text);
        mPanTilt2TargetXTextView = mPanTilt2ViewGroup.findViewById(R.id.pantilt_target_x_text);
        mPanTilt2TargetYTextView = mPanTilt2ViewGroup.findViewById(R.id.pantilt_target_y_text);
        mPanTilt2TargetZTextView = mPanTilt2ViewGroup.findViewById(R.id.pantilt_target_z_text);
        mPanTilt3TargetXTextView = mPanTilt3ViewGroup.findViewById(R.id.pantilt_target_x_text);
        mPanTilt3TargetYTextView = mPanTilt3ViewGroup.findViewById(R.id.pantilt_target_y_text);
        mPanTilt3TargetZTextView = mPanTilt3ViewGroup.findViewById(R.id.pantilt_target_z_text);
        // Physics: (NOW NAMED "FLOID" IN THE TAB)
        mPhysicsViewGroup = findViewById(R.id.physics_container);
        mStatusViewGroup = findViewById(R.id.status_container);
        mStatusTimeTextView = mStatusViewGroup.findViewById(R.id.status_time_text);
        mStatusTimeLabelTextView = findViewById(R.id.status_time_label);
        addToastHelp(mStatusTimeLabelTextView, "Status Time: time of the current floid status in millis from boot", this);
        mStatusNumberTextView = mStatusViewGroup.findViewById(R.id.status_number_text);
        mStatusNumberLabelTextView = findViewById(R.id.status_number_label);
        addToastHelp(mStatusNumberLabelTextView, "Status Number: sequential number of the current floid status", this);
        mModeTextView = mStatusViewGroup.findViewById(R.id.mode_text);
        mModeLabelTextView = findViewById(R.id.mode_label);
        addToastHelp(mModeLabelTextView, "Mode: current mode of the floid", this);
        mFollowModeTextView = mStatusViewGroup.findViewById(R.id.follow_mode_text);
        mFollowModeLabelTextView = findViewById(R.id.follow_mode_label);
        addToastHelp(mFollowModeLabelTextView, "Follow Mode: nose follows flight direction otherwise follows set angle", this);
        mCameraOnTextView = mStatusViewGroup.findViewById(R.id.camera_on_text);
        mCameraOnLabelTextView = findViewById(R.id.camera_on_label);
        addToastHelp(mCameraOnLabelTextView, "Camera On: is floid camera subsystem on", this);
        mGPSOnTextView = mStatusViewGroup.findViewById(R.id.gps_on_text);
        mGPSOnLabelTextView = findViewById(R.id.gps_on_label);
        addToastHelp(mGPSOnLabelTextView, "GPS On: is floid gps subsystem on", this);
        mPYROnTextView = mStatusViewGroup.findViewById(R.id.pyr_on_text);
        mPYROnLabelTextView = findViewById(R.id.pyr_on_label);
        addToastHelp(mPYROnLabelTextView, "PYR On: is floid pitch/yaw/roll subsystem on", this);
        mDesignatorOnTextView = mStatusViewGroup.findViewById(R.id.designator_on_text);
        mDesignatorOnLabelTextView = findViewById(R.id.designator_on_label);
        addToastHelp(mDesignatorOnLabelTextView, "Designator On: is floid designator subsystem on", this);
        mAuxOnTextView = mStatusViewGroup.findViewById(R.id.aux_on_text);
        mAuxOnLabelTextView = findViewById(R.id.aux_on_label);
        addToastHelp(mAuxOnLabelTextView, "AUX On: is floid auxiliary subsystem on", this);
        mHelisOnTextView = mStatusViewGroup.findViewById(R.id.helis_on_text);
        mHelisOnLabelTextView = findViewById(R.id.helis_on_label);
        addToastHelp(mHelisOnLabelTextView, "Helis On: are the floid helis on", this);
        mParachuteDeployed2TextView = mStatusViewGroup.findViewById(R.id.parachute_deployed2_text);
        mParachuteDeployed2LabelTextView = findViewById(R.id.parachute_deployed2_label);
        addToastHelp(mParachuteDeployed2LabelTextView, "Parachute Deployed: has the floid parachute been deployed", this);
        mDirectionOverGroundAndVelocityLabelTextView = findViewById(R.id.direction_over_ground_label);
        addToastHelp(mDirectionOverGroundAndVelocityLabelTextView, "Direction over Ground / Velocity: The current moving direction of the floid in heading degrees and the velocity in ft/s", this);
        mDirectionOverGroundTextView = mStatusViewGroup.findViewById(R.id.direction_over_ground_text);
        mVelocityOverGroundTextView = mStatusViewGroup.findViewById(R.id.velocity_over_ground_text);
        mAltitudeAndAltitudeVelocityLabelTextView = findViewById(R.id.altitude_label);
        addToastHelp(mAltitudeAndAltitudeVelocityLabelTextView, "Direction over Ground / Velocity: The current moving direction of the floid in heading degrees and the velocity in ft/s", this);
        mAltitudeTextView = mStatusViewGroup.findViewById(R.id.altitude_text);
        mAltitudeVelocityTextView = mStatusViewGroup.findViewById(R.id.altitude_velocity_text);
        mLandModeMinAltitudeAndTimeLabelTextView = findViewById(R.id.land_mode_min_altitude_and_time_label);
        addToastHelp(mLandModeMinAltitudeAndTimeLabelTextView, "Current Min Altitude for land mode and current time at that min", this);
        mLandModeMinAltitudeTextView = mPhysicsViewGroup.findViewById(R.id.land_mode_min_altitude_text);
        mLandModeMinAltitudeTimeTextView = mPhysicsViewGroup.findViewById(R.id.land_mode_min_altitude_time_text);
        // These goals are now in the "Test Goals" view group::
        mTestGoalsViewGroup = findViewById(R.id.goals_container);
        mHasGoalTextView = mTestGoalsViewGroup.findViewById(R.id.has_goal_text);
        mHasGoalLabelTextView = findViewById(R.id.physics_has_goal_label);
        addToastHelp(mHasGoalLabelTextView, "Has Goal: is floid currently executing a goal", this);
        mGoalIdTextView = mTestGoalsViewGroup.findViewById(R.id.goal_id_text);
        mGoalIdLabelTextView = findViewById(R.id.physics_goal_id_label);
        addToastHelp(mGoalIdLabelTextView, "Goal Id: id of current floid goal", this);
        mGoalTicksTextView = mTestGoalsViewGroup.findViewById(R.id.goal_ticks_text);
        mGoalTicksLabelTextView = findViewById(R.id.physics_goal_ticks_label);
        addToastHelp(mGoalTicksLabelTextView, "Goal Ticks: tick count of current floid goal", this);
        mGoalStateTextView = mTestGoalsViewGroup.findViewById(R.id.goal_state_text);
        mGoalStateLabelTextView = findViewById(R.id.physics_goal_status_label);
        addToastHelp(mGoalStateLabelTextView, "Goal Status: current floid goal status", this);
        mGoalTargetXTextView = mTestGoalsViewGroup.findViewById(R.id.goal_lng_text);
        mGoalTargetXLabelTextView = findViewById(R.id.goal_lng_label);
        addToastHelp(mGoalTargetXLabelTextView, "Target Longitude: target longitude of current floid goal in decimal degrees", this);
        mGoalTargetYTextView = mTestGoalsViewGroup.findViewById(R.id.goal_lat_text);
        mGoalTargetYLabelTextView = findViewById(R.id.goal_lat_label);
        addToastHelp(mGoalTargetYLabelTextView, "Target Latitude: target latitude of current floid goal in decimal degrees", this);
        mGoalTargetZTextView = mTestGoalsViewGroup.findViewById(R.id.goal_alt_text);
        mGoalTargetZLabelTextView = findViewById(R.id.goal_alt_label);
        addToastHelp(mGoalTargetZLabelTextView, "Target Altitude: target altitude of current floid goal in meters", this);
        mGoalTargetAngleTextView = mTestGoalsViewGroup.findViewById(R.id.goal_angle_text);
        mGoalTargetAngleLabelTextView = findViewById(R.id.goal_orientation_label);
        addToastHelp(mGoalTargetAngleLabelTextView, "Target Orientation: target orientation of current floid goal in heading degrees", this);
        mInFlightTextView = mPhysicsViewGroup.findViewById(R.id.in_flight_text);
        mInFlightLabelTextView = findViewById(R.id.in_flight_label);
        addToastHelp(mInFlightLabelTextView, "In Flight: is floid in flight", this);
        mLiftOffModeTextView = mPhysicsViewGroup.findViewById(R.id.lift_off_mode_text);
        mLiftOffModeLabelTextView = findViewById(R.id.lift_off_mode_label);
        addToastHelp(mLiftOffModeLabelTextView, "Lift Off Mode: is floid in lift off mode x", this);
        mLandModeTextView = mPhysicsViewGroup.findViewById(R.id.land_mode_text);
        mLandModeLabelTextView = findViewById(R.id.land_mode_label);
        addToastHelp(mLandModeLabelTextView, "Land Mode: is floid in land mode", this);
        // Run and Test modes:
        mRunTestModesViewGroup = findViewById(R.id.run_test_modes_panel);
        mRunModeProductionToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_production_relay).findViewById(R.id.relay_toggle_button);
        mTestModePrintDebugHeadingsToggleButton = mRunTestModesViewGroup.findViewById(R.id.test_mode_print_debug_headings_relay).findViewById(R.id.relay_toggle_button);
        mTestModeCheckPhysicsModelToggleButton = mRunTestModesViewGroup.findViewById(R.id.test_mode_check_physics_model_relay).findViewById(R.id.relay_toggle_button);
        mRunModeTestXYToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_test_xy_relay).findViewById(R.id.relay_toggle_button);
        mRunModeTestAltitude1ToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_test_altitude1_relay).findViewById(R.id.relay_toggle_button);
        mRunModeTestAltitude2ToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_test_altitude2_relay).findViewById(R.id.relay_toggle_button);
        mRunModeTestHeading1ToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_test_heading1_relay).findViewById(R.id.relay_toggle_button);
        mRunModeTestPitch1ToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_test_pitch1_relay).findViewById(R.id.relay_toggle_button);
        mRunModeTestRoll1ToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_physics_test_roll1_relay).findViewById(R.id.relay_toggle_button);
        mTestModeNoXYToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_physics_no_xy_relay).findViewById(R.id.relay_toggle_button);
        mTestModeNoAltitudeToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_physics_no_altitude_relay).findViewById(R.id.relay_toggle_button);
        mTestModeNoAttackAngleToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_physics_no_attack_angle_relay).findViewById(R.id.relay_toggle_button);
        mTestModeNoHeadingToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_physics_no_heading_relay).findViewById(R.id.relay_toggle_button);
        mTestModeNoPitchToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_physics_no_pitch_relay).findViewById(R.id.relay_toggle_button);
        mTestModeNoRollToggleButton = mRunTestModesViewGroup.findViewById(R.id.run_mode_physics_no_roll_relay).findViewById(R.id.relay_toggle_button);
        // Device relays:
        mDevicesViewGroup = findViewById(R.id.devices_container);
        mTestRelayButton = mDevicesViewGroup.findViewById(R.id.test_relay).findViewById(R.id.floid_button);
        mLedToggleButton = mDevicesViewGroup.findViewById(R.id.led_relay).findViewById(R.id.relay_toggle_button);
        mGpsToggleButton = mDevicesViewGroup.findViewById(R.id.gps_relay).findViewById(R.id.relay_toggle_button);
        mGpsRateSpinner = mDevicesViewGroup.findViewById(R.id.gps_rate_spinner);
        mGpsEmulateToggleButton = mDevicesViewGroup.findViewById(R.id.gps_emulate_relay).findViewById(R.id.relay_toggle_button);
        mGpsDeviceToggleButton = mDevicesViewGroup.findViewById(R.id.gps_device_relay).findViewById(R.id.relay_toggle_button);
        mPyrToggleButton = mDevicesViewGroup.findViewById(R.id.pyr_relay).findViewById(R.id.relay_toggle_button);
        mPyrADToggleButton = mDevicesViewGroup.findViewById(R.id.pyr_ad_relay).findViewById(R.id.relay_toggle_button);
        mPyrRateSpinner = mDevicesViewGroup.findViewById(R.id.pyr_rate_spinner);
        mPyrDeviceToggleButton = mDevicesViewGroup.findViewById(R.id.pyr_device_relay).findViewById(R.id.relay_toggle_button);
        mFloidStatusRateSpinner = mDevicesViewGroup.findViewById(R.id.floid_status_rate_spinner);
        mDesignatorToggleButton = mDevicesViewGroup.findViewById(R.id.designator_relay).findViewById(R.id.relay_toggle_button);
        mAuxToggleButton = mDevicesViewGroup.findViewById(R.id.aux_relay).findViewById(R.id.relay_toggle_button);
        mParachuteToggleButton = mDevicesViewGroup.findViewById(R.id.parachute_relay).findViewById(R.id.relay_toggle_button);
        // Declination Control:
        mDeclinationViewGroup = findViewById(R.id.declinationContainer);
        mDeclinationRelayButton = mDeclinationViewGroup.findViewById(R.id.declination_relay).findViewById(R.id.floid_button);
        mDeclinationEditText = mDeclinationViewGroup.findViewById(R.id.declination_text);
        mDeclinationEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mDeclinationEditText, mDeclinationFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mDeclinationEditText.setText("0.0");
        // ESC and Servo Relays:
        mHeli0EscToggleButton = mHeli0ViewGroup.findViewById(R.id.esc_relay).findViewById(R.id.relay_toggle_button);
        mHeli0EscSetupButton = mHeli0ViewGroup.findViewById(R.id.esc_setup_relay).findViewById(R.id.floid_button);
        mHeli0EscExtraButton = mHeli0ViewGroup.findViewById(R.id.esc_extra_relay).findViewById(R.id.floid_button);
        mHeli0ServoToggleButton = mHeli0ViewGroup.findViewById(R.id.servo_relay).findViewById(R.id.relay_toggle_button);
        mHeli1EscToggleButton = mHeli1ViewGroup.findViewById(R.id.esc_relay).findViewById(R.id.relay_toggle_button);
        mHeli1EscSetupButton = mHeli1ViewGroup.findViewById(R.id.esc_setup_relay).findViewById(R.id.floid_button);
        mHeli1EscExtraButton = mHeli1ViewGroup.findViewById(R.id.esc_extra_relay).findViewById(R.id.floid_button);
        mHeli1ServoToggleButton = mHeli1ViewGroup.findViewById(R.id.servo_relay).findViewById(R.id.relay_toggle_button);
        mHeli2EscToggleButton = mHeli2ViewGroup.findViewById(R.id.esc_relay).findViewById(R.id.relay_toggle_button);
        mHeli2EscSetupButton = mHeli2ViewGroup.findViewById(R.id.esc_setup_relay).findViewById(R.id.floid_button);
        mHeli2EscExtraButton = mHeli2ViewGroup.findViewById(R.id.esc_extra_relay).findViewById(R.id.floid_button);
        mHeli2ServoToggleButton = mHeli2ViewGroup.findViewById(R.id.servo_relay).findViewById(R.id.relay_toggle_button);
        mHeli3EscToggleButton = mHeli3ViewGroup.findViewById(R.id.esc_relay).findViewById(R.id.relay_toggle_button);
        mHeli3EscSetupButton = mHeli3ViewGroup.findViewById(R.id.esc_setup_relay).findViewById(R.id.floid_button);
        mHeli3EscExtraButton = mHeli3ViewGroup.findViewById(R.id.esc_extra_relay).findViewById(R.id.floid_button);
        mHeli3ServoToggleButton = mHeli3ViewGroup.findViewById(R.id.servo_relay).findViewById(R.id.relay_toggle_button);
        // Payloads:
        mPayloadsViewGroup = findViewById(R.id.payloads_container);
        mBay0ToggleButton = mPayloadsViewGroup.findViewById(R.id.bay0_relay).findViewById(R.id.relay_toggle_button);
        mBay1ToggleButton = mPayloadsViewGroup.findViewById(R.id.bay1_relay).findViewById(R.id.relay_toggle_button);
        mBay2ToggleButton = mPayloadsViewGroup.findViewById(R.id.bay2_relay).findViewById(R.id.relay_toggle_button);
        mBay3ToggleButton = mPayloadsViewGroup.findViewById(R.id.bay3_relay).findViewById(R.id.relay_toggle_button);
        mPayload0GoalStateTextView = mPayloadsViewGroup.findViewById(R.id.payload0_goal_status_text);
        mPayload1GoalStateTextView = mPayloadsViewGroup.findViewById(R.id.payload1_goal_status_text);
        mPayload2GoalStateTextView = mPayloadsViewGroup.findViewById(R.id.payload2_goal_status_text);
        mPayload3GoalStateTextView = mPayloadsViewGroup.findViewById(R.id.payload3_goal_status_text);
        mBay0StatusTextView = mPayloadsViewGroup.findViewById(R.id.bay0_status);    // setText("Closed");
        mBay1StatusTextView = mPayloadsViewGroup.findViewById(R.id.bay1_status);    // setText("Open");
        mBay2StatusTextView = mPayloadsViewGroup.findViewById(R.id.bay2_status);    // setText("Closed");
        mBay3StatusTextView = mPayloadsViewGroup.findViewById(R.id.bay3_status);    // setText("Closed");
        mNm0StatusTextView = mPayloadsViewGroup.findViewById(R.id.nm0_status);
        mNm1StatusTextView = mPayloadsViewGroup.findViewById(R.id.nm1_status);
        mNm2StatusTextView = mPayloadsViewGroup.findViewById(R.id.nm2_status);
        mNm3StatusTextView = mPayloadsViewGroup.findViewById(R.id.nm3_status);
        // Debug:
        mDebugViewGroup = findViewById(R.id.debug_container);
        mDebugToggleButton = mDebugViewGroup.findViewById(R.id.debug_relay).findViewById(R.id.relay_toggle_button);
        mDebugAllToggleButton = mDebugViewGroup.findViewById(R.id.debug_all_relay).findViewById(R.id.relay_toggle_button);
        mDebugAuxToggleButton = mDebugViewGroup.findViewById(R.id.debug_aux_relay).findViewById(R.id.relay_toggle_button);
        mDebugPayloadsToggleButton = mDebugViewGroup.findViewById(R.id.debug_payloads_relay).findViewById(R.id.relay_toggle_button);
        mDebugCameraToggleButton = mDebugViewGroup.findViewById(R.id.debug_camera_relay).findViewById(R.id.relay_toggle_button);
        mDebugDesignatorToggleButton = mDebugViewGroup.findViewById(R.id.debug_designator_relay).findViewById(R.id.relay_toggle_button);
        mDebugGpsToggleButton = mDebugViewGroup.findViewById(R.id.debug_gps_relay).findViewById(R.id.relay_toggle_button);
        mDebugHelisToggleButton = mDebugViewGroup.findViewById(R.id.debug_helis_relay).findViewById(R.id.relay_toggle_button);
        mDebugMemoryToggleButton = mDebugViewGroup.findViewById(R.id.debug_memory_relay).findViewById(R.id.relay_toggle_button);
        mDebugPanTiltToggleButton = mDebugViewGroup.findViewById(R.id.debug_pan_tilt_relay).findViewById(R.id.relay_toggle_button);
        mDebugPhysicsToggleButton = mDebugViewGroup.findViewById(R.id.debug_physics_relay).findViewById(R.id.relay_toggle_button);
        mDebugPyrToggleButton = mDebugViewGroup.findViewById(R.id.debug_pyr_relay).findViewById(R.id.relay_toggle_button);
        mDebugAltimeterToggleButton = mDebugViewGroup.findViewById(R.id.debug_altimeter_relay).findViewById(R.id.relay_toggle_button);
        // Serial output:
        mSerialOutputToggleButton = mDebugViewGroup.findViewById(R.id.serial_output_relay).findViewById(R.id.relay_toggle_button);
        // Droid logging:
        mDroidLoggingToggleButton = mDebugViewGroup.findViewById(R.id.droid_logging_relay).findViewById(R.id.relay_toggle_button);
        mDroidLoggingToggleButton.setText(R.string.default_label_log);
        mDroidLoggingToggleButton.setTextOn(getResources().getText(R.string.default_label_log));
        mDroidLoggingToggleButton.setTextOff(getResources().getText(R.string.default_label_log));
        mDroidLoggingToggleButton.setChecked(mFloidActivityUseLogger);
        // Change the logger for the activity and also tell the service:
        mDroidLoggingToggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            // Turn logging on or off:
            mFloidActivityUseLogger = isChecked;
            sendSetLogFloidService();
        }
        );
        // Altimeter:
        mAltimeterViewGroup = findViewById(R.id.altimeter_container);
        mAltimeterPanelLabelTextView = findViewById(R.id.altimeter_panel_label);
        mAltimeterLabelTextView = mAltimeterViewGroup.findViewById(R.id.altimeter_label);
        addToastHelp(mAltimeterLabelTextView, "Altimeter: Altitude in meters and barometer/gps difference in meters", this);
        mAltimeterStatusLabelTextView = mAltimeterViewGroup.findViewById(R.id.altimeter_status_label);
        addToastHelp(mAltimeterStatusLabelTextView, "Altimeter Status: initialized, valid, good and error counts", this);
        mAltimeterGpsLabelTextView = mAltimeterViewGroup.findViewById(R.id.altimeter_gps_label);
        addToastHelp(mAltimeterGpsLabelTextView, "GPS: filtered longitude, latitude and altitude and good count", this);
        mAltimeterPyrLabelTextView = mAltimeterViewGroup.findViewById(R.id.altimeter_pyr_label);
        addToastHelp(mAltimeterPyrLabelTextView, "IMU: IMU good and error counts", this);
        mAltimeterInitializedTextView = mAltimeterViewGroup.findViewById(R.id.altimeter_initialized_text);
        mAltimeterAltitudeValidTextView = mAltimeterViewGroup.findViewById(R.id.altimeter_valid_text);
        mAltimeterAltitudeTextView = mAltimeterViewGroup.findViewById(R.id.altimeter_altitude_text);
        mAltimeterAltitudeDeltaTextView = mAltimeterViewGroup.findViewById(R.id.altimeter_altitude_delta_text);
        mAltimeterGpsXDegreesDecimalFilteredTextView = mAltimeterViewGroup.findViewById(R.id.gps_x_degrees_decimal_filtered_text);
        mAltimeterGpsYDegreesDecimalFilteredTextView = mAltimeterViewGroup.findViewById(R.id.gps_y_degrees_decimal_filtered_text);
        mAltimeterGpsZMetersFilteredTextView = mAltimeterViewGroup.findViewById(R.id.gps_z_meters_filtered_text);
        mAltimeterGpsGoodCountTextView = mAltimeterViewGroup.findViewById(R.id.gps_good_count_text);
        mAltimeterPyrGoodCountTextView = mAltimeterViewGroup.findViewById(R.id.pyr_good_count_text);
        mAltimeterPyrErrorCountTextView = mAltimeterViewGroup.findViewById(R.id.pyr_error_count_text);
        mAltimeterGoodCountTextView = mAltimeterViewGroup.findViewById(R.id.altimeter_good_count_text);
    }

    /**
     * Set up droid parameters.
     */
    private void setupDroidParameters() {
        // Get parameter panel colors:
        editPanelColor = ContextCompat.getColor(this, R.color.DroidParameterEditSectionPanelColor);
        parameterPanelColor = ContextCompat.getColor(this, R.color.DroidParameterPanelColor);
        modifiedParameterValueTextColor = ContextCompat.getColor(this, R.color.DroidParameterModifiedValueTextColor);
        unModifiedParameterValueTextColor = ContextCompat.getColor(this, R.color.DroidParameterUnModifiedValueTextColor);
        enabledEditBackgroundColor = ContextCompat.getColor(this, R.color.DroidParameterEnabledEditBackgroundColor);
        disabledEditBackgroundColor = ContextCompat.getColor(this, R.color.DroidParameterDisabledEditBackgroundColor);
        // Get containers:
        mDroidParametersContainer = findViewById(R.id.droid_parameters_interface_container);
        mDroidParametersSetButton = findViewById(R.id.droid_parameters_set_button);
        mDroidParametersCancelButton = findViewById(R.id.droid_parameters_cancel_button);
        mDroidParametersLoadSaveAutoCompleteTextView = findViewById(R.id.droid_parameters_load_save_autocompletetextview);
        mDroidParametersSaveButton = findViewById(R.id.droid_parameters_save_relay).findViewById(R.id.floid_button);
        mDroidParametersLoadButton = findViewById(R.id.droid_parameters_load_relay).findViewById(R.id.floid_button);

        // Set button:
        mDroidParametersSetButton.setEnabled(false);
        mDroidParametersSetButton.setOnClickListener(v -> {
            HashMap<String, String> newDroidParameters = new HashMap<>();
            for (DroidParameterEditState editState : mDroidParameterEditStateList) {
                newDroidParameters.put(editState.getDroidParameter(), editState.getEditText().getText().toString());
            }
            mFloidActivityDroidParameters.putAll(newDroidParameters);
            // Send the droid parameters to the floid service:
            sendSetDroidParametersToFloidService(newDroidParameters);
            // This resets the editor:
            setupDroidParametersEditor();
        });
        // Cancel Button
        mDroidParametersCancelButton.setEnabled(false);
        mDroidParametersCancelButton.setOnClickListener(v -> {
            // This will do a reset:
            setupDroidParametersEditor();
        });
        // Autocomplete:
        final File droidParametersDirectory = getDir("droid_parameters", MODE_PRIVATE);
        final String[] parametersFileList = droidParametersDirectory.list();
        final ArrayAdapter<String> autoCompleteArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, parametersFileList);
        mDroidParametersLoadSaveAutoCompleteTextView.setAdapter(autoCompleteArrayAdapter);
        mDroidParametersLoadSaveAutoCompleteTextView.setThreshold(0);
        mDroidParametersLoadSaveAutoCompleteTextView.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                mDroidParametersLoadSaveAutoCompleteTextView.showDropDown();
            }
        });
        mDroidParametersLoadSaveAutoCompleteTextView.setOnClickListener(v -> mDroidParametersLoadSaveAutoCompleteTextView.showDropDown());
        mDroidParametersLoadSaveAutoCompleteTextView.setText("");
        // Save button:
        mDroidParametersSaveButton.setText(R.string.default_label_save);
        mDroidParametersSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // Save the droid parameters:
                    synchronized (mFloidActivityDroidParametersSynchronizer) {
                        ContextWrapper c = new ContextWrapper(mFloidActivity);
                        File mPath = new File(c.getFilesDir().getPath());
                        try {
                            // Get the file name to save to:
                            String droidParametersFileName = mPath.getPath() + "/" + mDroidParametersLoadSaveAutoCompleteTextView.getText().toString();
                            if(mFloidActivityUseLogger)
                                Log.d(getClass().getName(), String.format("Saving Droid Parameters to: %s", droidParametersFileName));
                            // Convert the current parameters to a string:
                            String droidParametersJSONString = new Gson().toJson(mFloidActivityDroidParameters);
                            // Open a file, write to it and close it:
                            FileOutputStream droidParametersFileOutputStream = openFileOutput(mDroidParametersLoadSaveAutoCompleteTextView.getText().toString(), MODE_PRIVATE);
                            // Write the parameters:
                            droidParametersFileOutputStream.write(droidParametersJSONString.getBytes());
                            // Close the file:
                            droidParametersFileOutputStream.close();
                            // Notify:
                            CharSequence toastCharSequence = "Droid Parameters saved";
                            Toast.makeText(getApplicationContext(), toastCharSequence, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            CharSequence toastCharSequence = "Droid Parameters save failed";
                            Toast.makeText(mFloidActivity, toastCharSequence, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    CharSequence toastCharSequence = "Droid Parameters save failed";
                    Toast.makeText(mFloidActivity, toastCharSequence, Toast.LENGTH_SHORT).show();
                }
            }
        });
        // Load button:
        mDroidParametersLoadButton.setText(R.string.default_label_load);
        mDroidParametersLoadButton.setOnClickListener(v -> {
            try {
                // Load droid parameters and also send to service
                synchronized (mFloidActivityDroidParametersSynchronizer) {
                    // Get the file name to load from:
                    String droidParametersFileName = mDroidParametersLoadSaveAutoCompleteTextView.getText().toString();
                    ContextWrapper c = new ContextWrapper(mFloidActivity);
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, String.format("Loading Droid Parameters from: %s/%s", c.getFilesDir().getPath(), droidParametersFileName));
                    FileInputStream droidParametersFileInputStream = openFileInput(droidParametersFileName);
                    byte[] droidParametersLoadedBytes = new byte[40000];
                    int droidParametersLoadedByteCount = droidParametersFileInputStream.read(droidParametersLoadedBytes, 0, 40000);
                    droidParametersFileInputStream.close();
                    String droidParametersJSONString = new String(droidParametersLoadedBytes, 0, droidParametersLoadedByteCount);
                    HashMap<String,String> newFloidDroidParameters = new Gson().fromJson(droidParametersJSONString, new TypeToken<HashMap<String, String>>(){}.getType());
                    // Note: we do not replace but rather copy in each in case this is only a subset of current parameters:
                    mFloidActivityDroidParameters.putAll(newFloidDroidParameters);
                    CharSequence toastCharSequence = "Droid Parameters loaded";
                    Toast.makeText(getApplicationContext(), toastCharSequence, Toast.LENGTH_SHORT).show();
                    // Display the parameters and reset the editor:
                    setupDroidParametersEditor();
                    // Send the model to the floid service:
                    sendSetDroidParametersToFloidService(mFloidActivityDroidParameters);                    }
            } catch (Exception e) {
                // Error...
                CharSequence toastCharSequence = "Droid Parameters load failed";
                Toast.makeText(getApplicationContext(), toastCharSequence, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Set up model status.
     */
    private void setupModelStatus() {
        // Model Collective / Cyclics:
        mModelCollectiveCyclicsViewGroup = findViewById(R.id.collective_cyclics_container);
        mModelStatusHeli0ViewGroup = mModelCollectiveCyclicsViewGroup.findViewById(R.id.collective_cyclic_h0);
        mModelStatusHeli1ViewGroup = mModelCollectiveCyclicsViewGroup.findViewById(R.id.collective_cyclic_h1);
        mModelStatusHeli2ViewGroup = mModelCollectiveCyclicsViewGroup.findViewById(R.id.collective_cyclic_h2);
        mModelStatusHeli3ViewGroup = mModelCollectiveCyclicsViewGroup.findViewById(R.id.collective_cyclic_h3);
        mModelStatusHeli0CyclicXTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_x);
        mModelStatusHeli0CyclicYTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_y);
        mModelStatusHeli1CyclicXTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_x);
        mModelStatusHeli1CyclicYTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_y);
        mModelStatusHeli2CyclicXTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_x);
        mModelStatusHeli2CyclicYTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_y);
        mModelStatusHeli3CyclicXTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_x);
        mModelStatusHeli3CyclicYTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_y);
        mModelStatusHeli0CyclicXLabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_x_label);
        addToastHelp(mModelStatusHeli0CyclicXLabelTextView, "Cyclic X: The value of the cyclic in the x (longitude) axis (0-1)", this);
        mModelStatusHeli0CyclicYLabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_y_label);
        addToastHelp(mModelStatusHeli0CyclicYLabelTextView, "Cyclic Y: The value of the cyclic in the y (latitude) axis (0-1)", this);
        mModelStatusHeli1CyclicXLabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_x_label);
        addToastHelp(mModelStatusHeli1CyclicXLabelTextView, "Cyclic X: The value of the cyclic in the x (longitude) axis (0-1)", this);
        mModelStatusHeli1CyclicYLabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_y_label);
        addToastHelp(mModelStatusHeli1CyclicYLabelTextView, "Cyclic Y: The value of the cyclic in the y (latitude) axis (0-1)", this);
        mModelStatusHeli2CyclicXLabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_x_label);
        addToastHelp(mModelStatusHeli2CyclicXLabelTextView, "Cyclic X: The value of the cyclic in the x (longitude) axis (0-1)", this);
        mModelStatusHeli2CyclicYLabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_y_label);
        addToastHelp(mModelStatusHeli2CyclicYLabelTextView, "Cyclic Y: The value of the cyclic in the y (latitude) axis (0-1)", this);
        mModelStatusHeli3CyclicXLabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_x_label);
        addToastHelp(mModelStatusHeli3CyclicXLabelTextView, "Cyclic X: The value of the cyclic in the x (longitude) axis (0-1)", this);
        mModelStatusHeli3CyclicYLabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_y_label);
        addToastHelp(mModelStatusHeli3CyclicYLabelTextView, "Cyclic Y: The value of the cyclic in the y (latitude) axis (0-1)", this);
        mCollectiveDeltaOrientationH0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_delta_orientation);
        mCollectiveDeltaOrientationH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_delta_orientation_label);
        addToastHelp(mCollectiveDeltaOrientationH0LabelTextView, "Delta Collective Orientation-pitch/roll: the change to the collective due to the desired orientation (pitch/roll) velocity", this);
        mCollectiveDeltaOrientationH1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_delta_orientation);
        mCollectiveDeltaOrientationH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_delta_orientation_label);
        addToastHelp(mCollectiveDeltaOrientationH1LabelTextView, "Delta Collective Orientation-pitch/roll: the change to the collective due to the desired orientation (pitch/roll) velocity", this);
        mCollectiveDeltaOrientationH2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_delta_orientation);
        mCollectiveDeltaOrientationH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_delta_orientation_label);
        addToastHelp(mCollectiveDeltaOrientationH2LabelTextView, "Delta Collective Orientation-pitch/roll: the change to the collective due to the desired orientation (pitch/roll) velocity", this);
        mCollectiveDeltaOrientationH3TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_delta_orientation);
        mCollectiveDeltaOrientationH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_delta_orientation_label);
        addToastHelp(mCollectiveDeltaOrientationH3LabelTextView, "Delta Collective Orientation-pitch/roll: the change to the collective due to the desired orientation (pitch/roll) velocity", this);
        mCollectiveDeltaAltitudeH0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_delta_altitude);
        mCollectiveDeltaAltitudeH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_delta_altitude_label);
        addToastHelp(mCollectiveDeltaAltitudeH0LabelTextView, "Delta Collective Altitude-z: the change to the collective due to the desired altitude velocity", this);
        mCollectiveDeltaAltitudeH1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_delta_altitude);
        mCollectiveDeltaAltitudeH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_delta_altitude_label);
        addToastHelp(mCollectiveDeltaAltitudeH1LabelTextView, "Delta Collective Altitude-z: the change to the collective due to the desired altitude velocity", this);
        mCollectiveDeltaAltitudeH2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_delta_altitude);
        mCollectiveDeltaAltitudeH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_delta_altitude_label);
        addToastHelp(mCollectiveDeltaAltitudeH2LabelTextView, "Delta Collective Altitude-z: the change to the collective due to the desired altitude velocity", this);
        mCollectiveDeltaAltitudeH3TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_delta_altitude);
        mCollectiveDeltaAltitudeH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_delta_altitude_label);
        addToastHelp(mCollectiveDeltaAltitudeH3LabelTextView, "Delta Collective Altitude-z: the change to the collective due to the desired altitude velocity", this);
        mCollectiveDeltaHeadingH0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_delta_heading);
        mCollectiveDeltaHeadingH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_delta_heading_label);
        addToastHelp(mCollectiveDeltaHeadingH0LabelTextView, "Delta Collective Heading-yaw: the change to the collective due to the desired heading (yaw) velocity", this);
        mCollectiveDeltaHeadingH1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_delta_heading);
        mCollectiveDeltaHeadingH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_delta_heading_label);
        addToastHelp(mCollectiveDeltaHeadingH1LabelTextView, "Delta Collective Heading-yaw: the change to the collective due to the desired heading (yaw) velocity", this);
        mCollectiveDeltaHeadingH2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_delta_heading);
        mCollectiveDeltaHeadingH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_delta_heading_label);
        addToastHelp(mCollectiveDeltaHeadingH2LabelTextView, "Delta Collective Heading-yaw: the change to the collective due to the desired heading (yaw) velocity", this);
        mCollectiveDeltaHeadingH3TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_delta_heading);
        mCollectiveDeltaHeadingH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_delta_heading_label);
        addToastHelp(mCollectiveDeltaHeadingH3LabelTextView, "Delta Collective Heading-yaw: the change to the collective due to the desired heading (yaw) velocity", this);
        mCollectiveDeltaTotalH0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_delta_total);
        mCollectiveDeltaTotalH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_delta_total_label);
        addToastHelp(mCollectiveDeltaTotalH0LabelTextView, "Delta Collective Total: the total change to the collective due to the desired velocities", this);
        mCollectiveDeltaTotalH1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_delta_total);
        mCollectiveDeltaTotalH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_delta_total_label);
        addToastHelp(mCollectiveDeltaTotalH1LabelTextView, "Delta Collective Total: the total change to the collective due to the desired velocities", this);
        mCollectiveDeltaTotalH2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_delta_total);
        mCollectiveDeltaTotalH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_delta_total_label);
        addToastHelp(mCollectiveDeltaTotalH2LabelTextView, "Delta Collective Total: the total change to the collective due to the desired velocities", this);
        mCollectiveDeltaTotalH3TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_delta_total);
        mCollectiveDeltaTotalH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_delta_total_label);
        addToastHelp(mCollectiveDeltaTotalH3LabelTextView, "Delta Collective Total: the total change to the collective due to the desired velocities", this);
        mCyclicValueH0S0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_s0);
        mCyclicValueH0S1TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_s1);
        mCyclicValueH0S2TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_s2);
        mCyclicValueH1S0TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_s0);
        mCyclicValueH1S1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_s1);
        mCyclicValueH1S2TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_s2);
        mCyclicValueH2S0TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_s0);
        mCyclicValueH2S1TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_s1);
        mCyclicValueH2S2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_s2);
        mCyclicValueH3S0TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_s0);
        mCyclicValueH3S1TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_s1);
        mCyclicValueH3S2TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_s2);
        mCyclicValueH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_value_label);
        addToastHelp(mCyclicValueH0LabelTextView, "Cyclic Value: the cyclic value due to desired directional velocity", this);
        mCyclicValueH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_value_label);
        addToastHelp(mCyclicValueH1LabelTextView, "Cyclic Value: the cyclic value due to desired directional velocity", this);
        mCyclicValueH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_value_label);
        addToastHelp(mCyclicValueH2LabelTextView, "Cyclic Value: the cyclic value due to desired directional velocity", this);
        mCyclicValueH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_value_label);
        addToastHelp(mCyclicValueH3LabelTextView, "Cyclic Value: the cyclic value due to desired directional velocity", this);
        mCollectiveValueH0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_value);
        mCollectiveValueH1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_value);
        mCollectiveValueH2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_value);
        mCollectiveValueH3TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_value);
        mCollectiveValueH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_value_label);
        addToastHelp(mCollectiveValueH0LabelTextView, "Collective Value: the collective value (0-1)", this);
        mCollectiveValueH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_value_label);
        addToastHelp(mCollectiveValueH1LabelTextView, "Collective Value: the collective value (0-1)", this);
        mCollectiveValueH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_value_label);
        addToastHelp(mCollectiveValueH2LabelTextView, "Collective Value: the collective value (0-1)", this);
        mCollectiveValueH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_value_label);
        addToastHelp(mCollectiveValueH3LabelTextView, "Collective Value: the collective value (0-1)", this);
        mCalculatedCyclicBladePitchH0S0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_blade_pitch_s0);
        mCalculatedCyclicBladePitchH0S1TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_blade_pitch_s1);
        mCalculatedCyclicBladePitchH0S2TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_blade_pitch_s2);
        mCalculatedCyclicBladePitchH1S0TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_blade_pitch_s0);
        mCalculatedCyclicBladePitchH1S1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_blade_pitch_s1);
        mCalculatedCyclicBladePitchH1S2TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_blade_pitch_s2);
        mCalculatedCyclicBladePitchH2S0TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_blade_pitch_s0);
        mCalculatedCyclicBladePitchH2S1TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_blade_pitch_s1);
        mCalculatedCyclicBladePitchH2S2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_blade_pitch_s2);
        mCalculatedCyclicBladePitchH3S0TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_blade_pitch_s0);
        mCalculatedCyclicBladePitchH3S1TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_blade_pitch_s1);
        mCalculatedCyclicBladePitchH3S2TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_blade_pitch_s2);
        mCalculatedCyclicBladePitchH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_blade_pitch_label);
        addToastHelp(mCalculatedCyclicBladePitchH0LabelTextView, "Cyclic Blade Pitch: the cyclic value of the blade pitch at this servo location (degrees)", this);
        mCalculatedCyclicBladePitchH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_blade_pitch_label);
        addToastHelp(mCalculatedCyclicBladePitchH1LabelTextView, "Cyclic Blade Pitch: the cyclic value of the blade pitch at this servo location (degrees)", this);
        mCalculatedCyclicBladePitchH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_blade_pitch_label);
        addToastHelp(mCalculatedCyclicBladePitchH2LabelTextView, "Cyclic Blade Pitch: the cyclic value of the blade pitch at this servo location (degrees)", this);
        mCalculatedCyclicBladePitchH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_blade_pitch_label);
        addToastHelp(mCalculatedCyclicBladePitchH3LabelTextView, "Cyclic Blade Pitch: the cyclic value of the blade pitch at this servo location (degrees)", this);
        mCalculatedCollectiveBladePitchH0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_blade_pitch);
        mCalculatedCollectiveBladePitchH1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_blade_pitch);
        mCalculatedCollectiveBladePitchH2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_blade_pitch);
        mCalculatedCollectiveBladePitchH3TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_blade_pitch);
        mCalculatedCollectiveBladePitchH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.collective_blade_pitch_label);
        addToastHelp(mCalculatedCollectiveBladePitchH0LabelTextView, "Collective Blade Pitch: the collective value of the blade pitch for this heli (degrees)", this);
        mCalculatedCollectiveBladePitchH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.collective_blade_pitch_label);
        addToastHelp(mCalculatedCollectiveBladePitchH1LabelTextView, "Collective Blade Pitch: the collective value of the blade pitch for this heli (degrees)", this);
        mCalculatedCollectiveBladePitchH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.collective_blade_pitch_label);
        addToastHelp(mCalculatedCollectiveBladePitchH2LabelTextView, "Collective Blade Pitch: the collective value of the blade pitch for this heli (degrees)", this);
        mCalculatedCollectiveBladePitchH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.collective_blade_pitch_label);
        addToastHelp(mCalculatedCollectiveBladePitchH3LabelTextView, "Collective Blade Pitch: the collective value of the blade pitch for this heli (degrees)", this);
        mCalculatedBladePitchH0S0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.blade_pitch_s0);
        mCalculatedBladePitchH0S1TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.blade_pitch_s1);
        mCalculatedBladePitchH0S2TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.blade_pitch_s2);
        mCalculatedBladePitchH1S0TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.blade_pitch_s0);
        mCalculatedBladePitchH1S1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.blade_pitch_s1);
        mCalculatedBladePitchH1S2TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.blade_pitch_s2);
        mCalculatedBladePitchH2S0TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.blade_pitch_s0);
        mCalculatedBladePitchH2S1TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.blade_pitch_s1);
        mCalculatedBladePitchH2S2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.blade_pitch_s2);
        mCalculatedBladePitchH3S0TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.blade_pitch_s0);
        mCalculatedBladePitchH3S1TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.blade_pitch_s1);
        mCalculatedBladePitchH3S2TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.blade_pitch_s2);
        mCalculatedBladePitchH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.blade_pitch_label);
        addToastHelp(mCalculatedBladePitchH0LabelTextView, "Blade Pitch: the total blade pitch for this heli servo (degrees)", this);
        mCalculatedBladePitchH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.blade_pitch_label);
        addToastHelp(mCalculatedBladePitchH1LabelTextView, "Blade Pitch: the total blade pitch for this heli servo (degrees)", this);
        mCalculatedBladePitchH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.blade_pitch_label);
        addToastHelp(mCalculatedBladePitchH2LabelTextView, "Blade Pitch: the total blade pitch for this heli servo (degrees)", this);
        mCalculatedBladePitchH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.blade_pitch_label);
        addToastHelp(mCalculatedBladePitchH3LabelTextView, "Blade Pitch: the total blade pitch for this heli servo (degrees)", this);
        mCalculatedServoDegreesH0S0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.servo_degrees_s0);
        mCalculatedServoDegreesH0S1TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.servo_degrees_s1);
        mCalculatedServoDegreesH0S2TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.servo_degrees_s2);
        mCalculatedServoDegreesH1S0TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.servo_degrees_s0);
        mCalculatedServoDegreesH1S1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.servo_degrees_s1);
        mCalculatedServoDegreesH1S2TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.servo_degrees_s2);
        mCalculatedServoDegreesH2S0TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.servo_degrees_s0);
        mCalculatedServoDegreesH2S1TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.servo_degrees_s1);
        mCalculatedServoDegreesH2S2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.servo_degrees_s2);
        mCalculatedServoDegreesH3S0TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.servo_degrees_s0);
        mCalculatedServoDegreesH3S1TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.servo_degrees_s1);
        mCalculatedServoDegreesH3S2TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.servo_degrees_s2);
        mCalculatedServoDegreesH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.servo_degrees_label);
        addToastHelp(mCalculatedServoDegreesH0LabelTextView, "Servo Degrees: the servo degree value resulting from the blade pitch (degrees)", this);
        mCalculatedServoDegreesH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.servo_degrees_label);
        addToastHelp(mCalculatedServoDegreesH1LabelTextView, "Servo Degrees: the servo degree value resulting from the blade pitch (degrees)", this);
        mCalculatedServoDegreesH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.servo_degrees_label);
        addToastHelp(mCalculatedServoDegreesH2LabelTextView, "Servo Degrees: the servo degree value resulting from the blade pitch (degrees)", this);
        mCalculatedServoDegreesH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.servo_degrees_label);
        addToastHelp(mCalculatedServoDegreesH3LabelTextView, "Servo Degrees: the servo degree value resulting from the blade pitch (degrees)", this);
        mCalculatedServoPulseH0S0TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.servo_pulses_s0);
        mCalculatedServoPulseH0S1TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.servo_pulses_s1);
        mCalculatedServoPulseH0S2TextView = mModelStatusHeli0ViewGroup.findViewById(R.id.servo_pulses_s2);
        mCalculatedServoPulseH1S0TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.servo_pulses_s0);
        mCalculatedServoPulseH1S1TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.servo_pulses_s1);
        mCalculatedServoPulseH1S2TextView = mModelStatusHeli1ViewGroup.findViewById(R.id.servo_pulses_s2);
        mCalculatedServoPulseH2S0TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.servo_pulses_s0);
        mCalculatedServoPulseH2S1TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.servo_pulses_s1);
        mCalculatedServoPulseH2S2TextView = mModelStatusHeli2ViewGroup.findViewById(R.id.servo_pulses_s2);
        mCalculatedServoPulseH3S0TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.servo_pulses_s0);
        mCalculatedServoPulseH3S1TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.servo_pulses_s1);
        mCalculatedServoPulseH3S2TextView = mModelStatusHeli3ViewGroup.findViewById(R.id.servo_pulses_s2);
        mCalculatedServoPulseH0LabelTextView = mModelStatusHeli0ViewGroup.findViewById(R.id.servo_pulses_label);
        addToastHelp(mCalculatedServoPulseH0LabelTextView, "Servo Pulse: the servo pulse value resulting from the servo degrees (min-max)", this);
        mCalculatedServoPulseH1LabelTextView = mModelStatusHeli1ViewGroup.findViewById(R.id.servo_pulses_label);
        addToastHelp(mCalculatedServoPulseH1LabelTextView, "Servo Pulse: the servo pulse value resulting from the servo degrees (min-max)", this);
        mCalculatedServoPulseH2LabelTextView = mModelStatusHeli2ViewGroup.findViewById(R.id.servo_pulses_label);
        addToastHelp(mCalculatedServoPulseH2LabelTextView, "Servo Pulse: the servo pulse value resulting from the servo degrees (min-max)", this);
        mCalculatedServoPulseH3LabelTextView = mModelStatusHeli3ViewGroup.findViewById(R.id.servo_pulses_label);
        addToastHelp(mCalculatedServoPulseH3LabelTextView, "Servo Pulse: the servo pulse value resulting from the servo degrees (min-max)", this);

        // Model Status:
        mModelStatusViewGroup = findViewById(R.id.model_status_container);
        mLiftOffTargetAltitudeTextView = mModelStatusViewGroup.findViewById(R.id.lift_off_target_altitude);
        mLiftOffTargetAltitudeLabelTextView = mModelStatusViewGroup.findViewById(R.id.lift_off_target_altitude_label);
        addToastHelp(mLiftOffTargetAltitudeLabelTextView, "Lift Off Target Altitude: the target altitude from home position until lift off achieved", this);
        mDistanceToTargetTextView = mModelStatusViewGroup.findViewById(R.id.distance_to_target);
        mDistanceToTargetLabelTextView = mModelStatusViewGroup.findViewById(R.id.distance_to_target_label);
        addToastHelp(mDistanceToTargetLabelTextView, "Distance To Target: distance to target over ground in meters", this);
        mAltitudeToTargetTextView = mModelStatusViewGroup.findViewById(R.id.altitude_to_target);
        mAltitudeToTargetLabelTextView = mModelStatusViewGroup.findViewById(R.id.altitude_to_target_label);
        addToastHelp(mAltitudeToTargetLabelTextView, "Altitude To Target: altitude to target in meters (positive means target above floid) ", this);
        mDeltaHeadingAngleToTargetTextView = mModelStatusViewGroup.findViewById(R.id.orientation_to_target);
        mDeltaHeadingAngleToTargetLabelTextView = mModelStatusViewGroup.findViewById(R.id.orientation_to_target_label);
        addToastHelp(mDeltaHeadingAngleToTargetLabelTextView, "Delta Heading Angle To Target: offset angle from floid heading to target in std degrees", this);
        mOrientationToGoalTextView = mModelStatusViewGroup.findViewById(R.id.orientation_to_goal);
        mOrientationToGoalLabelTextView = mModelStatusViewGroup.findViewById(R.id.orientation_to_goal_label);
        addToastHelp(mOrientationToGoalLabelTextView, "Orientation To Goal: orientation of floid to goal rotation in heading (compass) degrees", this);
        mAttackAngleTextView = mModelStatusViewGroup.findViewById(R.id.attack_angle);
        mAttackAngleLabelTextView = mModelStatusViewGroup.findViewById(R.id.attack_angle_label);
        addToastHelp(mAttackAngleLabelTextView, "Attack Angle: the current angle of attack in degrees (negative is nose down)", this);
        mTargetOrientationPitchTextView = mModelStatusViewGroup.findViewById(R.id.target_orientation_pitch);
        mTargetOrientationPitchLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_orientation_pitch_label);
        addToastHelp(mTargetOrientationPitchLabelTextView, "Target Orientation Pitch: the goal pitch portion of the attack angle due to the target orientation", this);

        mTargetOrientationRollTextView = mModelStatusViewGroup.findViewById(R.id.target_orientation_roll);
        mTargetOrientationRollLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_orientation_roll_label);
        addToastHelp(mTargetOrientationRollLabelTextView, "Target Orientation Roll: the goal roll portion of the attack angle due to the target orientation", this);

        mTargetPitchVelocityTextView = mModelStatusViewGroup.findViewById(R.id.target_pitch_velocity);
        mTargetPitchVelocityLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_pitch_velocity_label);
        addToastHelp(mTargetPitchVelocityLabelTextView, "Target Pitch Velocity: the goal pitch velocity to meet Target Orientation Pitch", this);

        mTargetRollVelocityTextView = mModelStatusViewGroup.findViewById(R.id.target_roll_velocity);
        mTargetRollVelocityLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_roll_velocity_label);
        addToastHelp(mTargetRollVelocityLabelTextView, "Target Roll Velocity: the goal roll velocity to meet Target Orientation Roll", this);

        mTargetAltitudeVelocityTextView = mModelStatusViewGroup.findViewById(R.id.target_altitude_velocity);
        mTargetAltitudeVelocityLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_altitude_velocity_label);
        addToastHelp(mTargetAltitudeVelocityLabelTextView, "Target Altitude Velocity: the goal altitude velocity to meet altitude target", this);

        mTargetHeadingVelocityTextView = mModelStatusViewGroup.findViewById(R.id.target_heading_velocity);
        mTargetHeadingVelocityLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_heading_velocity_label);
        addToastHelp(mTargetHeadingVelocityLabelTextView, "Target Heading Velocity: the goal heading velocity to meeting heading target ", this);

        mTargetOrientationPitchDeltaTextView = mModelStatusViewGroup.findViewById(R.id.target_orientation_pitch_delta);
        mTargetOrientationPitchDeltaLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_orientation_pitch_delta_label);
        addToastHelp(mTargetOrientationPitchDeltaLabelTextView, "Target Orientation Pitch Delta: delta pitch from current to target", this);

        mCyclicHeadingTextView = mModelStatusViewGroup.findViewById(R.id.cyclic_heading);
        mCyclicHeadingLabelTextView = mModelStatusViewGroup.findViewById(R.id.cyclic_heading_label);
        addToastHelp(mCyclicHeadingLabelTextView, "The cyclic heading", this);

        mTargetOrientationRollDeltaTextView = mModelStatusViewGroup.findViewById(R.id.target_orientation_roll_delta);
        mTargetOrientationRollDeltaLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_orientation_roll_delta_label);
        addToastHelp(mTargetOrientationRollDeltaLabelTextView, "Target Orientation Roll Delta: delta roll from current to target", this);

        mTargetPitchVelocityDeltaTextView = mModelStatusViewGroup.findViewById(R.id.target_pitch_velocity_delta);
        mTargetPitchVelocityDeltaLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_pitch_velocity_delta_label);
        addToastHelp(mTargetPitchVelocityDeltaLabelTextView, "Target Pitch Velocity Delta: change in pitch velocity to meet target ", this);

        mTargetRollVelocityDeltaTextView = mModelStatusViewGroup.findViewById(R.id.target_roll_velocity_delta);
        mTargetRollVelocityDeltaLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_roll_velocity_delta_label);
        addToastHelp(mTargetRollVelocityDeltaLabelTextView, "Target Roll Velocity Delta: change in roll velocity to meet target ", this);

        mTargetAltitudeVelocityDeltaTextView = mModelStatusViewGroup.findViewById(R.id.target_altitude_velocity_delta);
        mTargetAltitudeVelocityDeltaLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_altitude_velocity_delta_label);
        addToastHelp(mTargetAltitudeVelocityDeltaLabelTextView, "Target Altitude Velocity Delta: change in altitude velocity to meet target ", this);

        mTargetHeadingVelocityDeltaTextView = mModelStatusViewGroup.findViewById(R.id.target_heading_velocity_delta);
        mTargetHeadingVelocityDeltaLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_heading_velocity_delta_label);
        addToastHelp(mTargetHeadingVelocityDeltaLabelTextView, "Target Heading Velocity Delta: change in heading velocity to meet target", this);

        mTargetXYVelocityTextView = mModelStatusViewGroup.findViewById(R.id.target_xy_velocity);
        mTargetXYVelocityLabelTextView = mModelStatusViewGroup.findViewById(R.id.target_xy_velocity_label);
        addToastHelp(mTargetXYVelocityLabelTextView, "Target XY Velocity: goal over ground velocity to move to target", this);

        mVelocityCyclicAlphaTextView = mModelStatusViewGroup.findViewById(R.id.velocity_cyclic_alpha);
        mVelocityCyclicAlphaLabelTextView = mModelStatusViewGroup.findViewById(R.id.velocity_cyclic_alpha_label);
        addToastHelp(mVelocityCyclicAlphaLabelTextView, "Velocity Cyclic Alpha: Amount of cyclic (0-1) from velocity", this);
    }

    /**
     * Sets model parameters.
     */
    private void setupModelParameters() {
        // Model Parameters:
        mModelViewGroup = findViewById(R.id.model_container);
        mModelServosTabLabel = mModelViewGroup.findViewById(R.id.model_servos_tab_label);
        mModelServosContainer = mModelViewGroup.findViewById(R.id.model_servos_container);
        mModelCollectiveCyclicTabLabel = mModelViewGroup.findViewById(R.id.model_collective_cyclic_tab_label);
        mModelCollectiveCyclicContainer = mModelViewGroup.findViewById(R.id.model_collective_cyclic_container);
        mModelBladesTabLabel = mModelViewGroup.findViewById(R.id.model_blades_tab_label);
        mModelBladesContainer = mModelViewGroup.findViewById(R.id.model_blades_container);
        mModelESCsTabLabel = mModelViewGroup.findViewById(R.id.model_escs_tab_label);
        mModelESCsContainer = mModelViewGroup.findViewById(R.id.model_escs_container);
        mModelHeliStartupNumberStepsEditText = mModelESCsContainer.findViewById(R.id.model_heli_startup_number_steps);
        mModelHeliStartupStepTickEditText = mModelESCsContainer.findViewById(R.id.model_heli_startup_step_tick);
        mModelEscCollectiveCalcMidpointEditText = mModelESCsContainer.findViewById(R.id.model_esc_collective_calc_midpoint);
        mModelEscCollectiveLowValueEditText = mModelESCsContainer.findViewById(R.id.model_esc_collective_calc_low);
        mModelEscCollectiveMidValueEditText = mModelESCsContainer.findViewById(R.id.model_esc_collective_calc_mid);
        mModelEscCollectiveHighValueEditText = mModelESCsContainer.findViewById(R.id.model_esc_collective_calc_high);
        mModelTargetsTabLabel = mModelViewGroup.findViewById(R.id.model_targets_tab_label);
        mModelTargetsContainer = mModelViewGroup.findViewById(R.id.model_targets_container);
        mModelPhysicsTabLabel = mModelViewGroup.findViewById(R.id.model_physics_tab_label);
        mModelPhysicsContainer = mModelViewGroup.findViewById(R.id.model_physics_container);
        mModelRelayContainer = mModelViewGroup.findViewById(R.id.model_relay_container);
        mModelBladesLowEditText = mModelBladesContainer.findViewById(R.id.model_blades_low);
        mModelBladesLowButton = mModelBladesContainer.findViewById(R.id.model_blades_low_relay).findViewById(R.id.floid_button);
        mModelBladesZeroEditText = mModelBladesContainer.findViewById(R.id.model_blades_zero);
        mModelBladesZeroButton = mModelBladesContainer.findViewById(R.id.model_blades_zero_relay).findViewById(R.id.floid_button);
        mModelBladesHighEditText = mModelBladesContainer.findViewById(R.id.model_blades_high);
        mModelBladesHighButton = mModelBladesContainer.findViewById(R.id.model_blades_high_relay).findViewById(R.id.floid_button);
        mModelH0S0LowEditText = mModelBladesContainer.findViewById(R.id.model_h0s0_low);
        mModelH0S1LowEditText = mModelBladesContainer.findViewById(R.id.model_h0s1_low);
        mModelH0S2LowEditText = mModelBladesContainer.findViewById(R.id.model_h0s2_low);
        mModelH0S0ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h0s0_zero);
        mModelH0S1ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h0s1_zero);
        mModelH0S2ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h0s2_zero);
        mModelH0S0HighEditText = mModelBladesContainer.findViewById(R.id.model_h0s0_high);
        mModelH0S1HighEditText = mModelBladesContainer.findViewById(R.id.model_h0s1_high);
        mModelH0S2HighEditText = mModelBladesContainer.findViewById(R.id.model_h0s2_high);
        mModelH1S0LowEditText = mModelBladesContainer.findViewById(R.id.model_h1s0_low);
        mModelH1S1LowEditText = mModelBladesContainer.findViewById(R.id.model_h1s1_low);
        mModelH1S2LowEditText = mModelBladesContainer.findViewById(R.id.model_h1s2_low);
        mModelH1S0ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h1s0_zero);
        mModelH1S1ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h1s1_zero);
        mModelH1S2ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h1s2_zero);
        mModelH1S0HighEditText = mModelBladesContainer.findViewById(R.id.model_h1s0_high);
        mModelH1S1HighEditText = mModelBladesContainer.findViewById(R.id.model_h1s1_high);
        mModelH1S2HighEditText = mModelBladesContainer.findViewById(R.id.model_h1s2_high);
        mModelH2S0LowEditText = mModelBladesContainer.findViewById(R.id.model_h2s0_low);
        mModelH2S1LowEditText = mModelBladesContainer.findViewById(R.id.model_h2s1_low);
        mModelH2S2LowEditText = mModelBladesContainer.findViewById(R.id.model_h2s2_low);
        mModelH2S0ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h2s0_zero);
        mModelH2S1ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h2s1_zero);
        mModelH2S2ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h2s2_zero);
        mModelH2S0HighEditText = mModelBladesContainer.findViewById(R.id.model_h2s0_high);
        mModelH2S1HighEditText = mModelBladesContainer.findViewById(R.id.model_h2s1_high);
        mModelH2S2HighEditText = mModelBladesContainer.findViewById(R.id.model_h2s2_high);
        mModelH3S0LowEditText = mModelBladesContainer.findViewById(R.id.model_h3s0_low);
        mModelH3S1LowEditText = mModelBladesContainer.findViewById(R.id.model_h3s1_low);
        mModelH3S2LowEditText = mModelBladesContainer.findViewById(R.id.model_h3s2_low);
        mModelH3S0ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h3s0_zero);
        mModelH3S1ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h3s1_zero);
        mModelH3S2ZeroEditText = mModelBladesContainer.findViewById(R.id.model_h3s2_zero);
        mModelH3S0HighEditText = mModelBladesContainer.findViewById(R.id.model_h3s0_high);
        mModelH3S1HighEditText = mModelBladesContainer.findViewById(R.id.model_h3s1_high);
        mModelH3S2HighEditText = mModelBladesContainer.findViewById(R.id.model_h3s2_high);
        mModelCollectiveMinEditText = mModelCollectiveCyclicContainer.findViewById(R.id.model_collective_min);
        mModelCollectiveMaxEditText = mModelCollectiveCyclicContainer.findViewById(R.id.model_collective_max);
        mModelCollectiveDefaultEditText = mModelCollectiveCyclicContainer.findViewById(R.id.model_collective_default);
        mModelCyclicRangeEditText = mModelCollectiveCyclicContainer.findViewById(R.id.model_cyclic_range);
        mModelCyclicDefaultEditText = mModelCollectiveCyclicContainer.findViewById(R.id.model_cyclic_default);
        mModelESCTypeSpinner = mModelESCsContainer.findViewById(R.id.model_esc_type);
        mModelESCPulseMinEditText = mModelESCsContainer.findViewById(R.id.model_esc_pulse_min);
        mModelESCPulseMaxEditText = mModelESCsContainer.findViewById(R.id.model_esc_pulse_max);
        mModelESCLowValueEditText = mModelESCsContainer.findViewById(R.id.model_esc_low_value);
        mModelESCHighValueEditText = mModelESCsContainer.findViewById(R.id.model_esc_high_value);
        mModelAttackAngleMinDistanceEditText = mModelPhysicsContainer.findViewById(R.id.model_attack_angle_min_distance_value);
        mModelAttackAngleMaxDistanceEditText = mModelPhysicsContainer.findViewById(R.id.model_attack_angle_max_distance_value);
        mModelAttackAngleValueEditText = mModelPhysicsContainer.findViewById(R.id.model_attack_angle_value);
        mModelPitchDeltaMinValueEditText = mModelPhysicsContainer.findViewById(R.id.model_pitch_delta_min_value);
        mModelPitchDeltaMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_pitch_delta_max_value);
        mModelPitchTargetVelocityMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_pitch_target_velocity_max_value);
        mModelRollDeltaMinValueEditText = mModelPhysicsContainer.findViewById(R.id.model_roll_delta_min_value);
        mModelRollDeltaMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_roll_delta_max_value);
        mModelRollTargetVelocityMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_roll_target_velocity_max_value);
        mModelAltitudeToTargetMinValueEditText = mModelPhysicsContainer.findViewById(R.id.model_altitude_to_target_min_value);
        mModelAltitudeToTargetMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_altitude_to_target_max_value);
        mModelAltitudeTargetVelocityMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_altitude_target_velocity_max_value);
        mModelHeadingDeltaMinValueEditText = mModelPhysicsContainer.findViewById(R.id.model_heading_delta_min_value);
        mModelHeadingDeltaMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_heading_delta_max_value);
        mModelHeadingTargetVelocityMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_heading_target_velocity_max_value);
        mModelDistanceToTargetMinValueEditText = mModelPhysicsContainer.findViewById(R.id.model_distance_to_target_min_value);
        mModelDistanceToTargetMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_distance_to_target_max_value);
        mModelDistanceTargetVelocityMaxValueEditText = mModelPhysicsContainer.findViewById(R.id.model_distance_target_velocity_max_value);
        mModelOrientationMinDistanceValueEditText = mModelPhysicsContainer.findViewById(R.id.model_orientation_min_distance_value);
        mModelAccelerationScaleMultiplierEditText = mModelPhysicsContainer.findViewById(R.id.model_acceleration_scale_multiplier);
        mModelLiftOffTargetAltitudeDeltaEditText = mModelPhysicsContainer.findViewById(R.id.model_lift_off_target_altitude_delta_value);
        mModelServoPulseMinEditText = mModelServosContainer.findViewById(R.id.model_servo_pulse_min);
        mModelServoPulseMaxEditText = mModelServosContainer.findViewById(R.id.model_servo_pulse_max);
        mModelServoDegreeMinEditText = mModelServosContainer.findViewById(R.id.model_servo_degree_min);
        mModelServoDegreeMaxEditText = mModelServosContainer.findViewById(R.id.model_servo_degree_max);
        mModelServoSignLeftToggleButton = mModelServosContainer.findViewById(R.id.model_servo_sign_left_relay).findViewById(R.id.relay_toggle_button);
        mModelServoSignLeftToggleButton.setText(R.string.default_label_positive);
        mModelServoSignLeftToggleButton.setTextOn(getResources().getText(R.string.default_label_positive));
        mModelServoSignLeftToggleButton.setTextOff(getResources().getText(R.string.default_label_positive));
        mModelServoSignRightToggleButton = mModelServosContainer.findViewById(R.id.model_servo_sign_right_relay).findViewById(R.id.relay_toggle_button);
        mModelServoSignRightToggleButton.setText(R.string.default_label_positive);
        mModelServoSignRightToggleButton.setTextOn(getResources().getText(R.string.default_label_positive));
        mModelServoSignRightToggleButton.setTextOff(getResources().getText(R.string.default_label_positive));
        mModelServoSignPitchToggleButton = mModelServosContainer.findViewById(R.id.model_servo_sign_pitch_relay).findViewById(R.id.relay_toggle_button);
        mModelServoSignPitchToggleButton.setText(R.string.default_label_positive);
        mModelServoSignPitchToggleButton.setTextOn(getResources().getText(R.string.default_label_positive));
        mModelServoSignPitchToggleButton.setTextOff(getResources().getText(R.string.default_label_positive));
        mModelServoOffsetLeftEditText0 = mModelServosContainer.findViewById(R.id.model_servo_offset_left_0);
        mModelServoOffsetRightEditText0 = mModelServosContainer.findViewById(R.id.model_servo_offset_right_0);
        mModelServoOffsetPitchEditText0 = mModelServosContainer.findViewById(R.id.model_servo_offset_pitch_0);

        mModelServoOffsetLeftEditText1 = mModelServosContainer.findViewById(R.id.model_servo_offset_left_1);
        mModelServoOffsetRightEditText1 = mModelServosContainer.findViewById(R.id.model_servo_offset_right_1);
        mModelServoOffsetPitchEditText1 = mModelServosContainer.findViewById(R.id.model_servo_offset_pitch_1);

        mModelServoOffsetLeftEditText2 = mModelServosContainer.findViewById(R.id.model_servo_offset_left_2);
        mModelServoOffsetRightEditText2 = mModelServosContainer.findViewById(R.id.model_servo_offset_right_2);
        mModelServoOffsetPitchEditText2 = mModelServosContainer.findViewById(R.id.model_servo_offset_pitch_2);

        mModelServoOffsetLeftEditText3 = mModelServosContainer.findViewById(R.id.model_servo_offset_left_3);
        mModelServoOffsetRightEditText3 = mModelServosContainer.findViewById(R.id.model_servo_offset_right_3);
        mModelServoOffsetPitchEditText3 = mModelServosContainer.findViewById(R.id.model_servo_offset_pitch_3);
        mModelVelocityDeltaCyclicAlphaScaleEditText = mModelTargetsContainer.findViewById(R.id.model_target_velocity_delta_cyclic_alpha_scale);
        mModelTargetPitchVelocityAlphaEditText = mModelTargetsContainer.findViewById(R.id.model_target_pitch_velocity_alpha);
        mModelTargetRollVelocityAlphaEditText = mModelTargetsContainer.findViewById(R.id.model_target_roll_velocity_alpha);
        mModelTargetHeadingVelocityAlphaEditText = mModelTargetsContainer.findViewById(R.id.model_target_heading_velocity_alpha);
        mModelCyclicHeadingAlphaEditText = mModelTargetsContainer.findViewById(R.id.model_cyclic_heading_alpha);
        mModelTargetAltitudeVelocityAlphaEditText = mModelTargetsContainer.findViewById(R.id.model_target_altitude_velocity_alpha);
        mModelLandModeRequiredTimeAtMinAltitudeEditText = mModelTargetsContainer.findViewById(R.id.model_land_mode_required_time_at_min_altitude);
        mModelLoadSaveAutoCompleteTextView = mModelRelayContainer.findViewById(R.id.model_load_save_autocompletetextview);
        mModelRetrieveButton = mModelRelayContainer.findViewById(R.id.model_retrieve_relay).findViewById(R.id.floid_button);
        mModelSetButton = mModelRelayContainer.findViewById(R.id.model_set_relay).findViewById(R.id.floid_button);
        mModelSaveButton = mModelRelayContainer.findViewById(R.id.model_save_relay).findViewById(R.id.floid_button);
        mModelLoadButton = mModelRelayContainer.findViewById(R.id.model_load_relay).findViewById(R.id.floid_button);
        mModelClearEEPROMButton = mModelRelayContainer.findViewById(R.id.model_clear_eeprom_relay).findViewById(R.id.floid_button);
        mModelSaveToEEPROMButton = mModelRelayContainer.findViewById(R.id.model_save_to_eeprom_relay).findViewById(R.id.floid_button);
        mModelLoadFromEEPROMButton = mModelRelayContainer.findViewById(R.id.model_load_from_eeprom_relay).findViewById(R.id.floid_button);
        mModelStartModeSpinner = mModelRelayContainer.findViewById(R.id.model_start_mode_spinner);
        mModelRotationModeSpinner = mModelRelayContainer.findViewById(R.id.model_rotation_mode_spinner);
        mTestSetGoalsButton = mTestGoalsViewGroup.findViewById(R.id.test_set_goals_relay).findViewById(R.id.floid_button);
        mGoalsToggleButton = mTestGoalsViewGroup.findViewById(R.id.goals_relay).findViewById(R.id.relay_toggle_button);
        mLiftOffModeToggleButton = mTestGoalsViewGroup.findViewById(R.id.lift_off_mode_relay).findViewById(R.id.relay_toggle_button);
        mNormalModeToggleButton = mTestGoalsViewGroup.findViewById(R.id.normal_mode_relay).findViewById(R.id.relay_toggle_button);
        mLandModeToggleButton = mTestGoalsViewGroup.findViewById(R.id.land_mode_relay).findViewById(R.id.relay_toggle_button);
        mTestGoalsXEditText = mTestGoalsViewGroup.findViewById(R.id.test_goals_target_x);
        mTestGoalsYEditText = mTestGoalsViewGroup.findViewById(R.id.test_goals_target_y);
        mTestGoalsZEditText = mTestGoalsViewGroup.findViewById(R.id.test_goals_target_z);
        mTestGoalsAEditText = mTestGoalsViewGroup.findViewById(R.id.test_goals_target_a);

        // Set validators for model parameters:
        mModelBladesLowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelBladesLowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelBladesZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelBladesZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelBladesHighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelBladesHighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));

        mModelH0S0LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH0S0LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH0S1LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH0S1LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH0S2LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH0S2LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH0S0ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH0S0ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH0S1ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH0S1ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH0S2ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH0S2ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH0S0HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH0S0HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH0S1HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH0S1HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH0S2HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH0S2HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));

        mModelH1S0LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH1S0LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH1S1LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH1S1LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH1S2LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH1S2LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH1S0ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH1S0ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH1S1ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH1S1ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH1S2ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH1S2ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH1S0HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH1S0HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH1S1HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH1S1HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH1S2HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH1S2HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));

        mModelH2S0LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH2S0LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH2S1LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH2S1LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH2S2LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH2S2LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH2S0ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH2S0ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH2S1ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH2S1ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH2S2ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH2S2ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH2S0HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH2S0HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH2S1HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH2S1HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH2S2HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH2S2HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));

        mModelH3S0LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH3S0LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH3S1LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH3S1LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH3S2LowEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH3S2LowEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH3S0ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH3S0ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH3S1ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH3S1ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH3S2ZeroEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH3S2ZeroEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH3S0HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH3S0HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH3S1HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH3S1HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelH3S2HighEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelH3S2HighEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));

        mModelCollectiveMinEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelCollectiveMinEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelCollectiveMaxEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelCollectiveMaxEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelCollectiveDefaultEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelCollectiveDefaultEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelCyclicRangeEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelCyclicRangeEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelCyclicDefaultEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelCyclicDefaultEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelESCPulseMinEditText.addTextChangedListener(new FloidIntegerEditTextValidator(mModelESCPulseMinEditText, mModelParametersFloidEditTextValidatorCallback, true, 500, true, 2500));
        mModelESCPulseMaxEditText.addTextChangedListener(new FloidIntegerEditTextValidator(mModelESCPulseMaxEditText, mModelParametersFloidEditTextValidatorCallback, true, 500, true, 2500));
        mModelESCLowValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelESCLowValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelESCHighValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelESCLowValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelAttackAngleMinDistanceEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelAttackAngleMinDistanceEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelAttackAngleMaxDistanceEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelAttackAngleMaxDistanceEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelAttackAngleValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelAttackAngleValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelPitchDeltaMinValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelPitchDeltaMinValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelPitchDeltaMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelPitchDeltaMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelPitchTargetVelocityMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelPitchTargetVelocityMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelRollDeltaMinValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelRollDeltaMinValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelRollDeltaMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelRollDeltaMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelRollTargetVelocityMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelRollTargetVelocityMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelAltitudeToTargetMinValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelAltitudeToTargetMinValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelAltitudeToTargetMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelAltitudeToTargetMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelAltitudeTargetVelocityMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelAltitudeTargetVelocityMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelHeadingDeltaMinValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelHeadingDeltaMinValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelHeadingDeltaMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelHeadingDeltaMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelHeadingTargetVelocityMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelHeadingTargetVelocityMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelDistanceToTargetMinValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelDistanceToTargetMinValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelDistanceToTargetMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelDistanceToTargetMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelDistanceTargetVelocityMaxValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelDistanceTargetVelocityMaxValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelOrientationMinDistanceValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelOrientationMinDistanceValueEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelAccelerationScaleMultiplierEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelAccelerationScaleMultiplierEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.01, false, 0.0));
        mModelVelocityDeltaCyclicAlphaScaleEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelVelocityDeltaCyclicAlphaScaleEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.01, false, 0.0));
        mModelLiftOffTargetAltitudeDeltaEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelLiftOffTargetAltitudeDeltaEditText, mModelParametersFloidEditTextValidatorCallback, true, 5.0, true, 10000.0));
        mModelServoPulseMinEditText.addTextChangedListener(new FloidIntegerEditTextValidator(mModelServoPulseMinEditText, mModelParametersFloidEditTextValidatorCallback, true, 500, true, 2500));
        mModelServoPulseMaxEditText.addTextChangedListener(new FloidIntegerEditTextValidator(mModelServoPulseMaxEditText, mModelParametersFloidEditTextValidatorCallback, true, 500, true, 2500));
        mModelServoDegreeMinEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoDegreeMinEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelServoDegreeMaxEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoDegreeMaxEditText, mModelParametersFloidEditTextValidatorCallback, false, 0.0, false, 0.0));
        mModelServoOffsetLeftEditText0.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetLeftEditText0, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetRightEditText0.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetRightEditText0, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetPitchEditText0.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetPitchEditText0, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetLeftEditText1.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetLeftEditText1, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetRightEditText1.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetRightEditText1, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetPitchEditText1.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetPitchEditText1, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetLeftEditText2.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetLeftEditText2, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetRightEditText2.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetRightEditText2, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetPitchEditText2.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetPitchEditText2, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetLeftEditText3.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetLeftEditText3, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetRightEditText3.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetRightEditText3, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelServoOffsetPitchEditText3.addTextChangedListener(new FloidDecimalEditTextValidator(mModelServoOffsetPitchEditText3, mModelParametersFloidEditTextValidatorCallback, true, -180, true, 180));
        mModelTargetPitchVelocityAlphaEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelTargetPitchVelocityAlphaEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.0, true, 1.0));
        mModelTargetRollVelocityAlphaEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelTargetRollVelocityAlphaEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.0, true, 1.0));
        mModelTargetHeadingVelocityAlphaEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelTargetHeadingVelocityAlphaEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.0, true, 1.0));
        mModelTargetAltitudeVelocityAlphaEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelTargetAltitudeVelocityAlphaEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.0, true, 1.0));
        mModelLandModeRequiredTimeAtMinAltitudeEditText.addTextChangedListener(new FloidIntegerEditTextValidator(mModelLandModeRequiredTimeAtMinAltitudeEditText, mModelParametersFloidEditTextValidatorCallback, true, 1, true, 20000));

        // Heli Startup options:
        mModelHeliStartupNumberStepsEditText.addTextChangedListener(new FloidIntegerEditTextValidator(mModelHeliStartupNumberStepsEditText, mModelParametersFloidEditTextValidatorCallback, true, 1, true, 64));
        mModelHeliStartupStepTickEditText.addTextChangedListener(new FloidIntegerEditTextValidator(mModelHeliStartupStepTickEditText, mModelParametersFloidEditTextValidatorCallback, true, 1, true, 2000));

        mModelCyclicHeadingAlphaEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelCyclicHeadingAlphaEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.0, true, 1.0));
        // Validators for test goals:
        mTestGoalsXEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mTestGoalsXEditText, mTestGoalsEditTextValidatorCallback, false, 0.0, false, 0.0));
        mTestGoalsYEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mTestGoalsYEditText, mTestGoalsEditTextValidatorCallback, false, 0.0, false, 0.0));
        mTestGoalsZEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mTestGoalsZEditText, mTestGoalsEditTextValidatorCallback, false, 0.0, false, 0.0));
        mTestGoalsAEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mTestGoalsAEditText, mTestGoalsEditTextValidatorCallback, false, 0.0, false, 0.0));
        // ESC Collective Calc options:
        mModelEscCollectiveCalcMidpointEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelEscCollectiveCalcMidpointEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.0, true, 1.0));
        mModelEscCollectiveLowValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelEscCollectiveLowValueEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.0, true, 1.0));
        mModelEscCollectiveMidValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelEscCollectiveMidValueEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.0, true, 1.0));
        mModelEscCollectiveHighValueEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mModelEscCollectiveHighValueEditText, mModelParametersFloidEditTextValidatorCallback, true, 0.0, true, 1.0));
    }

    /**
     * Sets thread waits.
     */
    private void setupThreadWaits() {
        mThreadWaitsContainer = findViewById(R.id.thread_waits_container);
        mMainThreadSleepTimeEditText = mThreadWaitsContainer.findViewById(R.id.main_thread_sleep_time);
        mMainThreadSleepTimeEditText.setText(String.valueOf(mFloidActivityThreadTimings.getMainThreadSleepTime()));
        mAccessoryCommunicatorNoAccessorySleepTimeEditText = mThreadWaitsContainer.findViewById(R.id.accessory_thread_no_accessory_sleep_time);
        mAccessoryCommunicatorNoAccessorySleepTimeEditText.setText(String.valueOf(mFloidActivityThreadTimings.getAccessoryCommunicatorNoAccessorySleepTime()));
        mAccessoryCommunicatorIncomingMessageSleepTimeEditText = mThreadWaitsContainer.findViewById(R.id.accessory_thread_incoming_message_sleep_time);
        mAccessoryCommunicatorIncomingMessageSleepTimeEditText.setText(String.valueOf(mFloidActivityThreadTimings.getAccessoryCommunicatorIncomingMessageSleepTime()));
        mAccessoryCommunicatorOutgoingMessageSleepTimeEditText = mThreadWaitsContainer.findViewById(R.id.accessory_thread_outgoing_message_sleep_time);
        mAccessoryCommunicatorOutgoingMessageSleepTimeEditText.setText(String.valueOf(mFloidActivityThreadTimings.getAccessoryCommunicatorOutgoingMessageSleepTime()));
        mAccessoryCommunicatorNoMessageSleepTimeEditText = mThreadWaitsContainer.findViewById(R.id.accessory_thread_no_message_sleep_time);
        mAccessoryCommunicatorNoMessageSleepTimeEditText.setText(String.valueOf(mFloidActivityThreadTimings.getAccessoryCommunicatorNoMessageSleepTime()));

        // Thread waits button:
        mThreadWaitsButton = mThreadWaitsContainer.findViewById(R.id.thread_waits_relay).findViewById(R.id.floid_button);
        mThreadWaitsButton.setText(R.string.default_label_set_thread_waits);
        mThreadWaitsButton.setOnClickListener(v -> {
            try {
                mFloidActivityThreadTimings.setMainThreadSleepTime(Long.parseLong(mMainThreadSleepTimeEditText.getText().toString()));
                mFloidActivityThreadTimings.setAccessoryCommunicatorNoAccessorySleepTime(Long.parseLong(mAccessoryCommunicatorNoAccessorySleepTimeEditText.getText().toString()));
                mFloidActivityThreadTimings.setAccessoryCommunicatorIncomingMessageSleepTime(Long.parseLong(mAccessoryCommunicatorIncomingMessageSleepTimeEditText.getText().toString()));
                mFloidActivityThreadTimings.setAccessoryCommunicatorOutgoingMessageSleepTime(Long.parseLong(mAccessoryCommunicatorOutgoingMessageSleepTimeEditText.getText().toString()));
                mFloidActivityThreadTimings.setAccessoryCommunicatorNoMessageSleepTime(Long.parseLong(mAccessoryCommunicatorNoMessageSleepTimeEditText.getText().toString()));
                // Update the Floid Service with the new timings:
                sendFloidThreadTimingsToFloidService(mFloidActivityThreadTimings);
            } catch (Exception e) {
                Toast.makeText(mFloidActivity, "Error setting thread waits", Toast.LENGTH_SHORT).show();
            }
        }
        );
    }

    /**
     * Sets server address.
     */
    private void setupServerAddress() {
        // Server Address:
        mServerAddressGroup = findViewById(R.id.server_container);
        mServerAddressButton = mServerAddressGroup.findViewById(R.id.server_address_relay).findViewById(R.id.floid_button);
        mServerAddressButton.setText(R.string.default_label_set_server);
        mServerAddressButton.setOnClickListener(v -> {
            // Get the address:
            mFloidActivityHostAddress = mServerAddressEditText.getText().toString();
            // Get the port:
            mFloidActivityHostPort = Integer.parseInt(mServerPortEditText.getText().toString());
            // Tell the service about the new host;
            sendHostToFloidService(mFloidActivityHostAddress, mFloidActivityHostPort);
        });
        // Set this to the current host IP:
        mServerAddressEditText = mServerAddressGroup.findViewById(R.id.server_address_text);
        // Set the text:
        mServerAddressEditText.setText(mFloidActivityHostAddress);
        // Set this to the current host port:
        mServerPortEditText = mServerAddressGroup.findViewById(R.id.server_port_text);
        mServerPortEditText.addTextChangedListener(new FloidIntegerEditTextValidator(mServerPortEditText, mServerAddressFloidEditTextValidatorCallback, true, 1, true, 65535));
        mServerPortEditText.setText(String.valueOf(mFloidActivityHostPort));
    }

    /**
     * Sets floid id.
     */
    private void setupFloidId() {
        // Floid Id:
        mFloidIdGroup = findViewById(R.id.floid_id_container);
        mFloidIdButton = mFloidIdGroup.findViewById(R.id.set_floid_id_relay).findViewById(R.id.floid_button);
        mFloidIdButton.setText(R.string.default_label_set_floid_id);
        mFloidIdButton.setOnClickListener(v -> {
            String floidIdString = mFloidIdEditText.getText().toString();
            try {
                mFloidActivityFloidDroidStatus.setFloidId(Integer.parseInt(floidIdString));
                sendFloidIdToFloidService(mFloidActivityFloidDroidStatus.getFloidId());
            } catch (Exception e) {
                Toast.makeText(mFloidActivity, "Failed to save floid id.", Toast.LENGTH_SHORT).show();
                if (mFloidActivityUseLogger) Log.e(TAG, "Failed to save floid id " + e);
            }
        });
        mFloidIdEditText = mFloidIdGroup.findViewById(R.id.floid_id_text);
    }
    /**
     * Upload firmware.
     */
    private void setupUploadFirmware() {
        // Floid Id:
        mDownloadFirmwareGroup = findViewById(R.id.upload_firmware_container);
        mDownloadFirmwareButton = mDownloadFirmwareGroup.findViewById(R.id.upload_firmware_relay).findViewById(R.id.floid_button);
        mDownloadFirmwareButton.setText(R.string.default_label_upload_firmware_button);
        mDownloadFirmwareButton.setOnClickListener(v -> {
            try {
                sendUploadFirmwareToFloidService();
                Toast.makeText(mFloidActivity, "Uploading firmware...", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(mFloidActivity, "Failed to upload firmware.", Toast.LENGTH_SHORT).show();
                if (mFloidActivityUseLogger) Log.e(TAG, "Failed to upload firmware " + e);
            }
        });
    }

    /**
     * Sets client communicator toggle.
     */
    private void setupClientCommunicatorToggle() {

        mClientCommunicatorToggleButton = mServerAddressGroup.findViewById(R.id.client_communicator_relay).findViewById(R.id.relay_toggle_button);
        mClientCommunicatorToggleButton.setText(R.string.default_label_client);
        mClientCommunicatorToggleButton.setTextOn(getResources().getText(R.string.default_label_client));
        mClientCommunicatorToggleButton.setTextOff(getResources().getText(R.string.default_label_client));
        mClientCommunicatorToggleButton.setChecked(mFloidActivityClientCommunicatorOn);
        mClientCommunicatorToggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mFloidActivityClientCommunicatorOn = isChecked;
            sendSetClientCommunicatorStatusToFloidService(mFloidActivityClientCommunicatorOn);
        }
        );
    }

    /**
     * Sets model load save.
     */
    private void setupModelLoadSave() {
        // Set up the model preferences load/save spinner:
        // Find all the current model parameters files
        File modelParametersDirectory = getDir("model_parameters", MODE_PRIVATE);
        String[] parametersFileList = modelParametersDirectory.list();
        ArrayAdapter<String> autoCompleteArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, parametersFileList);
        mModelLoadSaveAutoCompleteTextView.setAdapter(autoCompleteArrayAdapter);
        mModelLoadSaveAutoCompleteTextView.setThreshold(0);
        mModelLoadSaveAutoCompleteTextView.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                mModelLoadSaveAutoCompleteTextView.showDropDown();
            }
        });
        mModelLoadSaveAutoCompleteTextView.setOnClickListener(v -> mModelLoadSaveAutoCompleteTextView.showDropDown());
        mModelLoadSaveAutoCompleteTextView.setText("");
        mModelLoadButton.setText(R.string.default_label_load);
        mModelLoadButton.setOnClickListener(v -> {
            try {
                // Load model parameters and also send to service
                synchronized (mFloidActivityFloidModelParametersSynchronizer) {
                    // Get the file name to load from:
                    String modelParametersFileName = mModelLoadSaveAutoCompleteTextView.getText().toString();
                    ContextWrapper c = new ContextWrapper(mFloidActivity);
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "Loading Model from: " + c.getFilesDir().getPath() + "/" + modelParametersFileName);
                    FileInputStream modelParametersFileInputStream = openFileInput(modelParametersFileName);
                    byte[] modelParametersLoadedBytes = new byte[40000];
                    int modelParametersLoadedByteCount = modelParametersFileInputStream.read(modelParametersLoadedBytes, 0, 40000);
                    modelParametersFileInputStream.close();
                    String modelParametersJSONString = new String(modelParametersLoadedBytes, 0, modelParametersLoadedByteCount);
                    JSONObject modelParametersJSONObject = new JSONObject(modelParametersJSONString);
                    mFloidActivityFloidModelParameters = new FloidModelParametersParcelable().fromJSON(modelParametersJSONObject);
                    CharSequence toastCharSequence = "Model loaded";
                    Toast.makeText(getApplicationContext(), toastCharSequence, Toast.LENGTH_SHORT).show();
                    displayFloidModelParameters();  // Refresh the model on screen
                    // Send the model to the floid service:
                    sendFloidModelToService(mFloidActivityFloidModelParameters);
                }
            } catch (Exception e) {
                // Error...
                CharSequence toastCharSequence = "Model load failed";
                Toast.makeText(getApplicationContext(), toastCharSequence, Toast.LENGTH_SHORT).show();
            }
        });
        mModelSaveButton.setText(R.string.default_label_save);
        mModelSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // Save the model parameters:
                    synchronized (mFloidActivityFloidModelParametersSynchronizer) {
                        getModelParametersFromInterface();
                        ContextWrapper c = new ContextWrapper(mFloidActivity);
                        File mPath = new File(c.getFilesDir().getPath());
                        try {
                            // Get the file name to save to:
                            String modelParametersFileName = mPath.getPath() + "/" + mModelLoadSaveAutoCompleteTextView.getText().toString();
                            if(mFloidActivityUseLogger)
                                Log.d(getClass().getName(), "Saving Model to: " + modelParametersFileName);
                            // Convert the current parameters to a string:
                            String modelParametersJSONString = mFloidActivityFloidModelParameters.toJSON().toString();
                            // Open a file, write to it and close it:
                            FileOutputStream modelParametersFileOutputStream = openFileOutput(mModelLoadSaveAutoCompleteTextView.getText().toString(), MODE_PRIVATE);
                            modelParametersFileOutputStream.write(modelParametersJSONString.getBytes());
                            modelParametersFileOutputStream.close();
                            CharSequence toastCharSequence = "Model saved";
                            Toast.makeText(getApplicationContext(), toastCharSequence, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            CharSequence toastCharSequence = "Model save failed";
                            Toast.makeText(mFloidActivity, toastCharSequence, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception e) {
                    CharSequence toastCharSequence = "Model save failed";
                    Toast.makeText(mFloidActivity, toastCharSequence, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Sets pyr timer task.
     */
// Create our pyr emulation task:
    private void setupPyrTimerTask() {
        mPyrEmulationTimerTask = new TimerTask() {
            @Override
            public void run() {
                // Is pyr emulation algorithm enabled?
                if (mPyrEmulationAlgorithmOn) {
                    boolean timeToProcess = false;
                    // Is it time?
                    if (mPyrEmulationAlgorithmLastTime == 0L) {
                        timeToProcess = true;
                    } else if (System.currentTimeMillis() >= mPyrEmulationAlgorithmLastTime + FloidIMUEmulationAlgorithmOptionController.getmFloidPyrEmulationAlgorithmRates()[mPyrEmulationAlgorithmRate]) {
                        timeToProcess = true;
                    }
                    if (timeToProcess) {
                        mPyrEmulationAlgorithmLastTime = System.currentTimeMillis();
                        // Process a step and get the values:
                        FloidIMUEmulationAlgorithmProcessResult pyrEmulationPitchControllerResult = mPyrEmulationAlgorithmPitchController.processStep();
                        double pyrEmulatedPitch = pyrEmulationPitchControllerResult.getValue();
                        FloidIMUEmulationAlgorithmProcessResult pyrEmulationHeadingControllerResult = mPyrEmulationAlgorithmHeadingController.processStep();
                        double pyrEmulatedHeading = pyrEmulationHeadingControllerResult.getValue();
                        FloidIMUEmulationAlgorithmProcessResult pyrEmulationRollControllerResult = mPyrEmulationAlgorithmRollController.processStep();
                        double pyrEmulatedRoll = pyrEmulationRollControllerResult.getValue();
                        FloidIMUEmulationAlgorithmProcessResult pyrEmulationAltitudeControllerResult = mPyrEmulationAlgorithmAltitudeController.processStep();
                        double pyrEmulatedAltitude = pyrEmulationAltitudeControllerResult.getValue();
                        FloidIMUEmulationAlgorithmProcessResult pyrEmulationTemperatureControllerResult = mPyrEmulationAlgorithmTemperatureController.processStep();
                        double pyrEmulatedTemperature = pyrEmulationTemperatureControllerResult.getValue();
                        // And send them as a packet:
                        sendPyrPacketCommandToFloidService((float) pyrEmulatedPitch, (float) pyrEmulatedHeading, (float) pyrEmulatedRoll, (float) pyrEmulatedAltitude, (float) pyrEmulatedTemperature);
                        // And send a log message:
                        if (mFloidActivityUseLogger)
                            Log.d(TAG, "IMU Emulation: Pitch: " + pyrEmulatedPitch + "  Roll: " + pyrEmulatedRoll + "  Heading: " + pyrEmulatedHeading + "  Altitude: " + pyrEmulatedAltitude + "  Temp: " + pyrEmulatedTemperature);
                    }
                }
            }
        };
    }

    /**
     * Retrieve the current permissions in the manifest
     * @param context the context for the manifest
     * @return array of permission strings
     */
    private static String[] retrievePermissions(Context context) {
        try {
            return context
                    .getPackageManager()
                    .getPackageInfo(context.getPackageName(), PackageManager.PackageInfoFlags.of(PackageManager.GET_PERMISSIONS))
                    .requestedPermissions;
        } catch (PackageManager.NameNotFoundException e) {
            // Do nothing
        }
        return new String[0];
    }

    private static final int FLOID_PERMISSIONS_REQUEST_CODE = 101;

    /**
     * Check permissions
     */
    private void checkPermissions() {
        String[] permissions = retrievePermissions(this);
        ArrayList<String> unGrantedPermissions = new ArrayList<>();
        for (String permission : permissions) {
            int permissionResult = ContextCompat.checkSelfPermission(this, permission);
            if(permissionResult != PackageManager.PERMISSION_GRANTED) {
                unGrantedPermissions.add(permission);
            }
            if(permissionResult == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Permission: " + permission + " -> " + "Granted");
            } else {
                Log.e(TAG, "Permission: " + permission + " -> " + "Denied");
            }
        }
        if (unGrantedPermissions.size() > 0) {
            ActivityResultLauncher<String[]> requestMultiplePermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), result -> {
                result.forEach((permission, permissionGranted) -> {
                    if(permissionGranted) {
                        Log.d(TAG, "Permission: " + permission + " -> " + "Granted");
                    } else {
                        Log.e(TAG, "Permission: " + permission + " -> " + "Denied");
                    }
                });
            });
            requestMultiplePermissions.launch(unGrantedPermissions.toArray(new String[0]));
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //noinspection SwitchStatementWithTooFewBranches
        switch (requestCode) {
            case FLOID_PERMISSIONS_REQUEST_CODE:
                for(int i=0; i<permissions.length; ++i) {
                    Log.d(TAG, "Floid Permission result: " + permissions[i] + " = " + (grantResults[i]==PackageManager.PERMISSION_DENIED?"Denied":"Granted"));
                }
                /*
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    //not granted
//                    finish(); // We go away...
                }
                 */
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set strict mode behavior - detects leaking objects:
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder(StrictMode.getVmPolicy())
                .detectLeakedClosableObjects()
                .build());
        setupActivitySounds();
        mFloidActivity = this;
        checkPermissions();
        // Retrieve Application Data:
        mAppBuildDateString = "";
        try {
            Date buildDate = new Date(BuildConfig.TIMESTAMP);
            SimpleDateFormat formatter = (SimpleDateFormat) SimpleDateFormat.getInstance();
            mAppBuildDateString = formatter.format(buildDate);
        } catch (Exception e) {
            Log.e(TAG, "Failed to get the build date", e);
        }
        // Create a new intent to start the Floid Accessory Communicator Service:
        mFloidServiceIntent = new Intent(this, FloidService.class);
        // Set up the broadcast receiver and functionality to kill the activity from an intent from the service:
        IntentFilter killServiceIntentFilter = new IntentFilter();
        killServiceIntentFilter.addAction(QUIT_ACTIVITY_INTENT_ACTION);
        quitActivityBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // See if it was our intent action:
                if (QUIT_ACTIVITY_INTENT_ACTION.equals(intent.getAction())) {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "Received Quit Activity Intent");
                    quitting = false; // We are quitting, but we were told to by service so don't bother sending a message to it
                    finish();
                } else {
                    Log.e(TAG, "Received Non-Quit Activity Intent");
                }
            }
        };
        registerReceiver(quitActivityBroadcastReceiver, killServiceIntentFilter);
        // Get the metrics and set the view:
        mDisplayMetrics = getResources().getDisplayMetrics();
        setContentView(R.layout.floid);
        // Start the UI:
        if (mFloidActivityUseLogger)
                Log.i(TAG, "Starting UI");

        setupActionBar();
        setupStatusGui();
        setupDroidParameters();
        setupModelStatus();
        setupModelParameters();
        setupThreadWaits();
        setupServerAddress();
        setupFloidId();
        setupUploadFirmware();
        setupClientCommunicatorToggle();
        setupModelLoadSave();

        // Set up the controls (also sets focus to test check box)
        if (mFloidActivityUseLogger) Log.d(TAG, "Setting up controls");
        setupControls();
        // Display if our accessory is connected (default: false)
        setAccessoryIcon(mFloidActivityAccessoryConnected);
        // Update our statuses:
        if (mFloidActivityUseLogger) Log.d(TAG, "Displaying Floid Status");
        displayFloidStatus();
        if (mFloidActivityUseLogger) Log.d(TAG, "Displaying Floid Model Status");
        displayFloidModelStatus();
        if (mFloidActivityUseLogger) Log.d(TAG, "Displaying Floid Model Parameters");
        displayFloidModelParameters();
        if (mFloidActivityUseLogger) Log.d(TAG, "Done onCreate");
    }

    /**
     * Called when destroyed
     */
    @Override
    protected void onDestroy() {
        if (mFloidActivityUseLogger) Log.d(TAG, "OnDestroy");
        if (mFloidActivityUseLogger) Log.d(TAG, "OnDestroy: Un-registering quit activity receiver");
        unregisterReceiver(quitActivityBroadcastReceiver);
        if (quitting) {
            if (mFloidActivityUseLogger) Log.d(TAG, "OnDestroy: Quitting - Stopping FloidService");
            stopService(mFloidServiceIntent);
        } else {
            if (mFloidActivityUseLogger) Log.d(TAG, "OnDestroy: Not quitting");
        }
        if (mFloidActivityUseLogger) Log.d(TAG, "OnDestroy: Calling Super");
        super.onDestroy();
    }

    /**
     * Start pyr emulation.
     */
    private void startPyrEmulation() {
        // First, stop if we need to:
        stopPyrEmulation();
        mPyrEmulationAlgorithmLastTime = 0L; // Denote new start
        mPyrEmulationAlgorithmPitchController.resetToStart();
        mPyrEmulationAlgorithmHeadingController.resetToStart();
        mPyrEmulationAlgorithmRollController.resetToStart();
        mPyrEmulationAlgorithmAltitudeController.resetToStart();
        mPyrEmulationAlgorithmTemperatureController.resetToStart();
        mPyrEmulationAlgorithmCurrentTimer = new Timer();
        setupPyrTimerTask();
        mPyrEmulationAlgorithmCurrentTimer.scheduleAtFixedRate(mPyrEmulationTimerTask, PYR_EMULATION_FIRST_WAIT, PYR_EMULATION_PERIOD);
    }

    /**
     * Stop pyr emulation.
     */
    private void stopPyrEmulation() {
        // Stop pyr emulation here:
        if (mPyrEmulationAlgorithmCurrentTimer != null) {
            mPyrEmulationAlgorithmCurrentTimer.cancel();
            mPyrEmulationAlgorithmCurrentTimer = null;
        }
    }

    /**
     * Sets controls.
     */
    private void setupControls() {
        if (mFloidActivityUseLogger) Log.d(TAG, "..Timer");
        // Retrieve colors:
        mFocusedSubMenuTabColor = ContextCompat.getColor(this, R.color.TabSelectedColor);
        mUnFocusedSubMenuTabColor = ContextCompat.getColor(this, R.color.TabUnselectedColor);
        mFocusedSubMenuTabTextColor = ContextCompat.getColor(this, R.color.TabSelectedTextColor);
        mUnFocusedSubMenuTabTextColor = ContextCompat.getColor(this, R.color.TabUnselectedTextColor);
        // Timer container:
        mMissionStartTimerContainer = findViewById(R.id.mission_start_timer_container);
        mMissionStartTimerMissionLabelText = mMissionStartTimerContainer.findViewById(R.id.mission_start_timer_mission_label);
        mMissionStartTimerMissionNameText = mMissionStartTimerContainer.findViewById(R.id.mission_start_timer_mission_name);
        mMissionStartTimerText = mMissionStartTimerContainer.findViewById(R.id.mission_start_timer_time);
        mMissionStartTimerUnitsText = mMissionStartTimerContainer.findViewById(R.id.mission_start_timer_units);
        mMissionStartTimerCancelButton = mMissionStartTimerContainer.findViewById(R.id.mission_start_timer_cancel_button);
        mMissionStartTimerContainer.setVisibility(View.GONE);   // Invisible for now!
        // View Frames:
        if (mFloidActivityUseLogger) Log.d(TAG, "..View");
        mFloidViewFrames = findViewById(R.id.floid_view_frames);
        mFloidViewFrames.setVisibility(View.VISIBLE);   // Starts Visible
        // View tabs:
        mCurrentMenuItemId = R.id.action_about;  // First is selected (This is really the "Floid" tab)

        if (mFloidActivityUseLogger) Log.d(TAG, "..Model Status");
        mModelStatusContainer = findViewById(R.id.model_status_container);
        mModelStatusContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..PYR Emulation");
        mPyrEmulationContainer = findViewById(R.id.pyr_emulation_container);
        mPyrEmulationContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Debug");
        mDebugContainer = findViewById(R.id.debug_container);
        mDebugContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Goals");
        mGoalsContainer = findViewById(R.id.goals_container);
        mGoalsContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Imaging");
        mImagingContainer = findViewById(R.id.imaging_container);
        mImagingContainer.setVisibility(View.GONE);
        mImagingTabsLabels = findViewById(R.id.imaging_tabs_labels);
        mImagingTabsContents = findViewById(R.id.imaging_tabs_content);
        setupImagingTabs();

        if (mFloidActivityUseLogger) Log.d(TAG, "..Model");
        mModelContainer = findViewById(R.id.model_container);
        mModelContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Droid");
        mDroidContainer = findViewById(R.id.droid_container);
        mDroidContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Parameters");
        mParametersContainer = findViewById(R.id.parameters_container);
        mParametersContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Helis");
        mHelisContainer = findViewById(R.id.helis_container);
        mHelisContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Collective/Cyclic");
        mModelCollectiveCyclicsContainer = findViewById(R.id.collective_cyclics_container);
        mModelCollectiveCyclicsContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Pan/Tilt");
        mPanTiltsContainer = findViewById(R.id.pan_tilt_container);
        mPanTiltsContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..About");
        mAboutContainer = findViewById(R.id.about_container);
        mAboutBuildDateTextView = findViewById(R.id.about_build_date);
        //noinspection ConstantConditions
        mAboutBuildDateTextView.setText(String.format("%s%s", mAppBuildDateString, BuildConfig.FLAVOR.equals("demo") ? " [DEMO]" : " [FULL]"));
        mAboutContainer.setVisibility(View.VISIBLE);
        mAboutStartTextView = findViewById(R.id.about_start);
        mAboutStartTextView.setOnClickListener(aboutStartTextViewOnClickListener);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Physics");
        mFloidContainer = findViewById(R.id.physics_container);
        mFloidContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Model Parameters");
        // Set up the model parameters tabs and their visibility:
        mCurrentModelViewId = R.id.model_servos_tab_label;  // First is selected by default:
        if (mFloidActivityUseLogger) Log.d(TAG, "   - Servos");
        mModelServosTabLabel.setOnClickListener(modelViewOnClickListener);
        mModelServosTabLabel.setBackgroundColor(mFocusedSubMenuTabColor);
        mModelServosTabLabel.setTextColor(mFocusedSubMenuTabTextColor);
        mModelServosContainer.setVisibility(View.VISIBLE);

        if (mFloidActivityUseLogger) Log.d(TAG, "   - C/C");
        mModelCollectiveCyclicTabLabel.setOnClickListener(modelViewOnClickListener);
        mModelCollectiveCyclicTabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mModelCollectiveCyclicTabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mModelCollectiveCyclicContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "   - Blades");
        mModelBladesTabLabel.setOnClickListener(modelViewOnClickListener);
        mModelBladesTabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mModelBladesTabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mModelBladesContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "   - ESCs");
        mModelESCsTabLabel.setOnClickListener(modelViewOnClickListener);
        mModelESCsTabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mModelESCsTabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mModelESCsContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "   - Targets");
        mModelTargetsTabLabel.setOnClickListener(modelViewOnClickListener);
        mModelTargetsTabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mModelTargetsTabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mModelTargetsContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "   - Physics");
        mModelPhysicsTabLabel.setOnClickListener(modelViewOnClickListener);
        mModelPhysicsTabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mModelPhysicsTabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mModelPhysicsContainer.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "Output Controls");
        // Set up output controls:
        setupOutputControls();
        // Set up imaging status:
        setupImagingStatus();

        // Set the default focus:
        mTestRelayButton.requestFocus();
    }

    private void setupImagingStatus() {
        // Displays for photo and video preview frames:
        mPhotoImageView = findViewById(R.id.photo_image);
        mPhotoImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        mVideoImageView = findViewById(R.id.video_image);
        mVideoImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        // Camera
        mImagingPhotoCameraIdTextView = findViewById(R.id.imaging_photo_camera_id);
        mImagingVideoCameraIdTextView = findViewById(R.id.imaging_video_camera_id);
        mImagingSupportsFlashTextView = findViewById(R.id.imaging_supports_flash);
        mImagingLegacyHardwareTextView = findViewById(R.id.imaging_legacy_hardware);
        mImagingPhotoStateTextView = findViewById(R.id.imaging_photo_state);
        mImagingRemainingCaptureSessionsTextView = findViewById(R.id.imaging_remaining_capture_sessions);
        mImagingStatusInitializedTextView = findViewById(R.id.imaging_status_initialized);
        // Photo
        mImagingStatusTakingPhotoTextView = findViewById(R.id.imaging_status_taking_photo);
        mImagingStatusPhotoIdDirtyTextView = findViewById(R.id.imaging_status_photo_id_dirty);
        mImagingStatusPhotoPropertiesDirtyTextView = findViewById(R.id.imaging_status_photo_properties_dirty);
        mImagingStatusPhotoCameraOpenTextView = findViewById(R.id.imaging_status_photo_camera_open);
        mImagingStatusPhotoSurfacesSetUpTextView = findViewById(R.id.imaging_status_photo_surfaces_setup);
        mImagingStatusPhotoCaptureRequestSetUpTextView = findViewById(R.id.imaging_status_photo_capture_request_setup);
        mImagingStatusPhotoCaptureSessionConfiguredTextView = findViewById(R.id.imaging_status_photo_capture_session_configured);
        // Video
        mImagingStatusTakingVideoTextView = findViewById(R.id.imaging_status_taking_video);
        mImagingStatusVideoIdDirtyTextView = findViewById(R.id.imaging_status_video_id_dirty);
        mImagingStatusVideoPropertiesDirtyTextView = findViewById(R.id.imaging_status_video_properties_dirty);
        mImagingStatusVideoCameraOpenTextView = findViewById(R.id.imaging_status_video_camera_open);
        mImagingStatusVideoSurfacesSetUpTextView = findViewById(R.id.imaging_status_video_surfaces_setup);
        mImagingStatusVideoCaptureRequestSetUpTextView = findViewById(R.id.imaging_status_video_capture_request_setup);
        mImagingStatusVideoCaptureSessionConfiguredTextView = findViewById(R.id.imaging_status_video_capture_session_configured);
        mImagingStatusSupportsVideoSnapshotTextView = findViewById(R.id.imaging_status_video_supports_taking_video_snapshot);
        mImagingStatusTakingVideoSnapshotTextView = findViewById(R.id.imaging_status_video_taking_video_snapshot);
        // Set up the take photo and start video buttons:
        mImagingTakePhotoButton = findViewById(R.id.imaging_take_photo_button);
        mImagingTakePhotoButton.setOnClickListener(v -> sendTakePhotoToFloidService());
        mImagingStartVideoButton = findViewById(R.id.imaging_start_video_button);
        mImagingStartVideoButton.setOnClickListener(v -> sendStartVideoToFloidService());
        mImagingStopVideoButton = findViewById(R.id.imaging_stop_video_button);
        mImagingStopVideoButton.setOnClickListener(v -> sendStopVideoToFloidService());

    }
    /**
     * The 'About start' text view on click listener.
     */
    private final OnClickListener aboutStartTextViewOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mAboutContainer.setVisibility(View.GONE);
            mFloidContainer.setVisibility(View.VISIBLE);
            mCurrentMenuItemId = R.id.action_floid;
        }
    };
    /**
     * The Model view on click listener.
     */
    @SuppressWarnings("Duplicates")
    private final OnClickListener modelViewOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int newModelViewId = v.getId();
            TextView currentModelViewLabel = null;
            ViewGroup currentModelViewContainer = null;
            TextView newModelViewLabel = null;
            ViewGroup newModelViewContainer = null;
            // Do nothing
            if (mCurrentModelViewId == R.id.model_servos_tab_label) {
                currentModelViewContainer = mModelServosContainer;
                currentModelViewLabel = mModelServosTabLabel;
            } else if (mCurrentModelViewId == R.id.model_collective_cyclic_tab_label) {
                currentModelViewContainer = mModelCollectiveCyclicContainer;
                currentModelViewLabel = mModelCollectiveCyclicTabLabel;
            } else if (mCurrentModelViewId == R.id.model_blades_tab_label) {
                currentModelViewContainer = mModelBladesContainer;
                currentModelViewLabel = mModelBladesTabLabel;
            } else if (mCurrentModelViewId == R.id.model_escs_tab_label) {
                currentModelViewContainer = mModelESCsContainer;
                currentModelViewLabel = mModelESCsTabLabel;
            } else if (mCurrentModelViewId == R.id.model_targets_tab_label) {
                currentModelViewContainer = mModelTargetsContainer;
                currentModelViewLabel = mModelTargetsTabLabel;
            } else if (mCurrentModelViewId == R.id.model_physics_tab_label) {
                currentModelViewContainer = mModelPhysicsContainer;
                currentModelViewLabel = mModelPhysicsTabLabel;
            }
            // Do nothing
            if (newModelViewId == R.id.model_servos_tab_label) {
                newModelViewContainer = mModelServosContainer;
                newModelViewLabel = mModelServosTabLabel;
            } else if (newModelViewId == R.id.model_collective_cyclic_tab_label) {
                newModelViewContainer = mModelCollectiveCyclicContainer;
                newModelViewLabel = mModelCollectiveCyclicTabLabel;
            } else if (newModelViewId == R.id.model_blades_tab_label) {
                newModelViewContainer = mModelBladesContainer;
                newModelViewLabel = mModelBladesTabLabel;
            } else if (newModelViewId == R.id.model_escs_tab_label) {
                newModelViewContainer = mModelESCsContainer;
                newModelViewLabel = mModelESCsTabLabel;
            } else if (newModelViewId == R.id.model_targets_tab_label) {
                newModelViewContainer = mModelTargetsContainer;
                newModelViewLabel = mModelTargetsTabLabel;
            } else if (newModelViewId == R.id.model_physics_tab_label) {
                newModelViewContainer = mModelPhysicsContainer;
                newModelViewLabel = mModelPhysicsTabLabel;
            }
            if (newModelViewContainer != null) {
                if (currentModelViewContainer != null) {
                    currentModelViewContainer.setVisibility(View.GONE);
                }
                if (currentModelViewLabel != null) {
                    currentModelViewLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
                    currentModelViewLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
                }
                newModelViewContainer.setVisibility(View.VISIBLE);
                newModelViewLabel.setBackgroundColor(mFocusedSubMenuTabColor);
                newModelViewLabel.setTextColor(mFocusedSubMenuTabTextColor);
                mCurrentModelViewId = newModelViewId;
            }
        }
    };
    /**
     * The Heli view on click listener.
     */
    @SuppressWarnings("Duplicates")
    private final OnClickListener heliViewOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int newHeliViewId = v.getId();
            TextView currentHeliViewLabel = null;
            ViewGroup currentHeliViewContainer = null;
            TextView newHeliViewLabel = null;
            ViewGroup newHeliViewContainer = null;
            // Do nothing
            if (mCurrentHeliViewId == R.id.heli0_tab_label) {
                currentHeliViewContainer = mHeli0Container;
                currentHeliViewLabel = mHeli0TabLabel;
            } else if (mCurrentHeliViewId == R.id.heli1_tab_label) {
                currentHeliViewContainer = mHeli1Container;
                currentHeliViewLabel = mHeli1TabLabel;
            } else if (mCurrentHeliViewId == R.id.heli2_tab_label) {
                currentHeliViewContainer = mHeli2Container;
                currentHeliViewLabel = mHeli2TabLabel;
            } else if (mCurrentHeliViewId == R.id.heli3_tab_label) {
                currentHeliViewContainer = mHeli3Container;
                currentHeliViewLabel = mHeli3TabLabel;
            }
            // Do nothing
            if (newHeliViewId == R.id.heli0_tab_label) {
                newHeliViewContainer = mHeli0Container;
                newHeliViewLabel = mHeli0TabLabel;
            } else if (newHeliViewId == R.id.heli1_tab_label) {
                newHeliViewContainer = mHeli1Container;
                newHeliViewLabel = mHeli1TabLabel;
            } else if (newHeliViewId == R.id.heli2_tab_label) {
                newHeliViewContainer = mHeli2Container;
                newHeliViewLabel = mHeli2TabLabel;
            } else if (newHeliViewId == R.id.heli3_tab_label) {
                newHeliViewContainer = mHeli3Container;
                newHeliViewLabel = mHeli3TabLabel;
            }
            if (newHeliViewContainer != null) {
                if (currentHeliViewContainer != null) {
                    currentHeliViewContainer.setVisibility(View.GONE);
                }
                if (currentHeliViewLabel != null) {
                    currentHeliViewLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
                    currentHeliViewLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
                }
                newHeliViewContainer.setVisibility(View.VISIBLE);
                newHeliViewLabel.setBackgroundColor(mFocusedSubMenuTabColor);
                newHeliViewLabel.setTextColor(mFocusedSubMenuTabTextColor);
                mCurrentHeliViewId = newHeliViewId;
            }
        }
    };
    /**
     * The Collective cyclic view on click listener.
     */
    @SuppressWarnings("Duplicates")
    private final OnClickListener collectiveCyclicViewOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int newCCViewId = v.getId();
            TextView currentCCViewLabel = null;
            ViewGroup currentCCViewContainer = null;
            TextView newCCViewLabel = null;
            ViewGroup newCCViewContainer = null;

            // Do nothing
            if (mCurrentCCViewId == R.id.cc0_tab_label) {
                currentCCViewContainer = mCC0Container;
                currentCCViewLabel = mCC0TabLabel;
            } else if (mCurrentCCViewId == R.id.cc1_tab_label) {
                currentCCViewContainer = mCC1Container;
                currentCCViewLabel = mCC1TabLabel;
            } else if (mCurrentCCViewId == R.id.cc2_tab_label) {
                currentCCViewContainer = mCC2Container;
                currentCCViewLabel = mCC2TabLabel;
            } else if (mCurrentCCViewId == R.id.cc3_tab_label) {
                currentCCViewContainer = mCC3Container;
                currentCCViewLabel = mCC3TabLabel;
            }
            // Do nothing
            if (newCCViewId == R.id.cc0_tab_label) {
                newCCViewContainer = mCC0Container;
                newCCViewLabel = mCC0TabLabel;
            } else if (newCCViewId == R.id.cc1_tab_label) {
                newCCViewContainer = mCC1Container;
                newCCViewLabel = mCC1TabLabel;
            } else if (newCCViewId == R.id.cc2_tab_label) {
                newCCViewContainer = mCC2Container;
                newCCViewLabel = mCC2TabLabel;
            } else if (newCCViewId == R.id.cc3_tab_label) {
                newCCViewContainer = mCC3Container;
                newCCViewLabel = mCC3TabLabel;
            }
            if (newCCViewContainer != null) {
                if (currentCCViewContainer != null) {
                    currentCCViewContainer.setVisibility(View.GONE);
                }
                if (currentCCViewLabel != null) {
                    currentCCViewLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
                    currentCCViewLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
                }
                newCCViewContainer.setVisibility(View.VISIBLE);
                newCCViewLabel.setBackgroundColor(mFocusedSubMenuTabColor);
                newCCViewLabel.setTextColor(mFocusedSubMenuTabTextColor);
                mCurrentCCViewId = newCCViewId;
            }
        }
    };

    /**
     * The Pan tilt view on click listener.
     */
    @SuppressWarnings("Duplicates")
    private final OnClickListener panTiltViewOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int newPanTiltViewId = v.getId();
            TextView currentPanTiltViewLabel = null;
            ViewGroup currentPanTiltViewContainer = null;
            TextView newPanTiltViewLabel = null;
            ViewGroup newPanTiltViewContainer = null;
            // Do nothing
            if (mCurrentPanTiltViewId == R.id.pt0_tab_label) {
                currentPanTiltViewContainer = mPanTilt0Container;
                currentPanTiltViewLabel = mPanTilt0TabLabel;
            } else if (mCurrentPanTiltViewId == R.id.pt1_tab_label) {
                currentPanTiltViewContainer = mPanTilt1Container;
                currentPanTiltViewLabel = mPanTilt1TabLabel;
            } else if (mCurrentPanTiltViewId == R.id.pt2_tab_label) {
                currentPanTiltViewContainer = mPanTilt2Container;
                currentPanTiltViewLabel = mPanTilt2TabLabel;
            } else if (mCurrentPanTiltViewId == R.id.pt3_tab_label) {
                currentPanTiltViewContainer = mPanTilt3Container;
                currentPanTiltViewLabel = mPanTilt3TabLabel;
            }
            // Do nothing
            if (newPanTiltViewId == R.id.pt0_tab_label) {
                newPanTiltViewContainer = mPanTilt0Container;
                newPanTiltViewLabel = mPanTilt0TabLabel;
            } else if (newPanTiltViewId == R.id.pt1_tab_label) {
                newPanTiltViewContainer = mPanTilt1Container;
                newPanTiltViewLabel = mPanTilt1TabLabel;
            } else if (newPanTiltViewId == R.id.pt2_tab_label) {
                newPanTiltViewContainer = mPanTilt2Container;
                newPanTiltViewLabel = mPanTilt2TabLabel;
            } else if (newPanTiltViewId == R.id.pt3_tab_label) {
                newPanTiltViewContainer = mPanTilt3Container;
                newPanTiltViewLabel = mPanTilt3TabLabel;
            }
            if (newPanTiltViewContainer != null) {
                if (currentPanTiltViewContainer != null) {
                    currentPanTiltViewContainer.setVisibility(View.GONE);
                }
                if (currentPanTiltViewLabel != null) {
                    currentPanTiltViewLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
                    currentPanTiltViewLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
                }
                newPanTiltViewContainer.setVisibility(View.VISIBLE);
                newPanTiltViewLabel.setBackgroundColor(mFocusedSubMenuTabColor);
                newPanTiltViewLabel.setTextColor(mFocusedSubMenuTabTextColor);
                mCurrentPanTiltViewId = newPanTiltViewId;
            }
        }
    };

    /**
     * Gets model parameters from interface.
     */
    private void getModelParametersFromInterface() {
        // Note: called from save button before save - must match the set button
        mFloidActivityFloidModelParameters.setBladesLow(Double.parseDouble(mModelBladesLowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setBladesZero(Double.parseDouble(mModelBladesZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setBladesHigh(Double.parseDouble(mModelBladesHighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH0S0Low(Double.parseDouble(mModelH0S0LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH0S1Low(Double.parseDouble(mModelH0S1LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH0S2Low(Double.parseDouble(mModelH0S2LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH1S0Low(Double.parseDouble(mModelH1S0LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH1S1Low(Double.parseDouble(mModelH1S1LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH1S2Low(Double.parseDouble(mModelH1S2LowEditText.getText().toString()));

        mFloidActivityFloidModelParameters.setH0S0Zero(Double.parseDouble(mModelH0S0ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH0S1Zero(Double.parseDouble(mModelH0S1ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH0S2Zero(Double.parseDouble(mModelH0S2ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH1S0Zero(Double.parseDouble(mModelH1S0ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH1S1Zero(Double.parseDouble(mModelH1S1ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH1S2Zero(Double.parseDouble(mModelH1S2ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH0S0High(Double.parseDouble(mModelH0S0HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH0S1High(Double.parseDouble(mModelH0S1HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH0S2High(Double.parseDouble(mModelH0S2HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH1S0High(Double.parseDouble(mModelH1S0HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH1S1High(Double.parseDouble(mModelH1S1HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH1S2High(Double.parseDouble(mModelH1S2HighEditText.getText().toString()));

        mFloidActivityFloidModelParameters.setH2S0Low(Double.parseDouble(mModelH2S0LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH2S1Low(Double.parseDouble(mModelH2S1LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH2S2Low(Double.parseDouble(mModelH2S2LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH3S0Low(Double.parseDouble(mModelH3S0LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH3S1Low(Double.parseDouble(mModelH3S1LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH3S2Low(Double.parseDouble(mModelH3S2LowEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH2S0Zero(Double.parseDouble(mModelH2S0ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH2S1Zero(Double.parseDouble(mModelH2S1ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH2S2Zero(Double.parseDouble(mModelH2S2ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH3S0Zero(Double.parseDouble(mModelH3S0ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH3S1Zero(Double.parseDouble(mModelH3S1ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH3S2Zero(Double.parseDouble(mModelH3S2ZeroEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH2S0High(Double.parseDouble(mModelH2S0HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH2S1High(Double.parseDouble(mModelH2S1HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH2S2High(Double.parseDouble(mModelH2S2HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH3S0High(Double.parseDouble(mModelH3S0HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH3S1High(Double.parseDouble(mModelH3S1HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setH3S2High(Double.parseDouble(mModelH3S2HighEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setCollectiveMin(Double.parseDouble(mModelCollectiveMinEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setCollectiveMax(Double.parseDouble(mModelCollectiveMaxEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setCollectiveDefault(Double.parseDouble(mModelCollectiveDefaultEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setCyclicRange(Double.parseDouble(mModelCyclicRangeEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setCyclicDefault(Double.parseDouble(mModelCyclicDefaultEditText.getText().toString()));
        // Shorts x 3:
        mFloidActivityFloidModelParameters.setEscType(mModelESCTypeSpinner.getSelectedItemPosition());
        mFloidActivityFloidModelParameters.setEscPulseMin(Integer.parseInt(mModelESCPulseMinEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setEscPulseMax(Integer.parseInt(mModelESCPulseMaxEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setEscLowValue(Double.parseDouble(mModelESCLowValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setEscHighValue(Double.parseDouble(mModelESCHighValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setAttackAngleMinDistanceValue(Double.parseDouble(mModelAttackAngleMinDistanceEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setAttackAngleMaxDistanceValue(Double.parseDouble(mModelAttackAngleMaxDistanceEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setAttackAngleValue(Double.parseDouble(mModelAttackAngleValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setPitchDeltaMinValue(Double.parseDouble(mModelPitchDeltaMinValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setPitchDeltaMaxValue(Double.parseDouble(mModelPitchDeltaMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setPitchTargetVelocityMaxValue(Double.parseDouble(mModelPitchTargetVelocityMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setRollDeltaMinValue(Double.parseDouble(mModelRollDeltaMinValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setRollDeltaMaxValue(Double.parseDouble(mModelRollDeltaMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setRollTargetVelocityMaxValue(Double.parseDouble(mModelRollTargetVelocityMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setAltitudeToTargetMinValue(Double.parseDouble(mModelAltitudeToTargetMinValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setAltitudeToTargetMaxValue(Double.parseDouble(mModelAltitudeToTargetMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setAltitudeTargetVelocityMaxValue(Double.parseDouble(mModelAltitudeTargetVelocityMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setLandModeTimeRequiredAtMinAltitude(Integer.parseInt(mModelLandModeRequiredTimeAtMinAltitudeEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setStartMode(mModelStartModeSpinner.getSelectedItemPosition());

        mFloidActivityFloidModelParameters.setHeadingDeltaMinValue(Double.parseDouble(mModelHeadingDeltaMinValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setHeadingDeltaMaxValue(Double.parseDouble(mModelHeadingDeltaMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setHeadingTargetVelocityMaxValue(Double.parseDouble(mModelHeadingTargetVelocityMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setDistanceToTargetMinValue(Double.parseDouble(mModelDistanceToTargetMinValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setDistanceToTargetMaxValue(Double.parseDouble(mModelDistanceToTargetMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setDistanceTargetVelocityMaxValue(Double.parseDouble(mModelDistanceTargetVelocityMaxValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setOrientationMinDistanceValue(Double.parseDouble(mModelOrientationMinDistanceValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setLiftOffTargetAltitudeDeltaValue(Double.parseDouble(mModelLiftOffTargetAltitudeDeltaEditText.getText().toString()));
        // Shorts x 2:
        mFloidActivityFloidModelParameters.setServoPulseMin(Integer.parseInt(mModelServoPulseMinEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setServoPulseMax(Integer.parseInt(mModelServoPulseMaxEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setServoDegreeMin(Double.parseDouble(mModelServoDegreeMinEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setServoDegreeMax(Double.parseDouble(mModelServoDegreeMaxEditText.getText().toString()));
        // Bytes x 3:
        mFloidActivityFloidModelParameters.setServoSignLeft(mModelServoSignLeftToggleButton.isChecked() ? (byte) 1 : (byte) 0);
        mFloidActivityFloidModelParameters.setServoSignRight(mModelServoSignRightToggleButton.isChecked() ? (byte) 1 : (byte) 0);
        mFloidActivityFloidModelParameters.setServoSignPitch(mModelServoSignPitchToggleButton.isChecked() ? (byte) 1 : (byte) 0);

        mFloidActivityFloidModelParameters.setServoOffsetLeft0(Double.parseDouble(mModelServoOffsetLeftEditText0.getText().toString()));
        mFloidActivityFloidModelParameters.setServoOffsetRight0(Double.parseDouble(mModelServoOffsetRightEditText0.getText().toString()));
        mFloidActivityFloidModelParameters.setServoOffsetPitch0(Double.parseDouble(mModelServoOffsetPitchEditText0.getText().toString()));

        mFloidActivityFloidModelParameters.setServoOffsetLeft1(Double.parseDouble(mModelServoOffsetLeftEditText1.getText().toString()));
        mFloidActivityFloidModelParameters.setServoOffsetRight1(Double.parseDouble(mModelServoOffsetRightEditText1.getText().toString()));
        mFloidActivityFloidModelParameters.setServoOffsetPitch1(Double.parseDouble(mModelServoOffsetPitchEditText1.getText().toString()));

        mFloidActivityFloidModelParameters.setServoOffsetLeft2(Double.parseDouble(mModelServoOffsetLeftEditText2.getText().toString()));
        mFloidActivityFloidModelParameters.setServoOffsetRight2(Double.parseDouble(mModelServoOffsetRightEditText2.getText().toString()));
        mFloidActivityFloidModelParameters.setServoOffsetPitch2(Double.parseDouble(mModelServoOffsetPitchEditText2.getText().toString()));

        mFloidActivityFloidModelParameters.setServoOffsetLeft3(Double.parseDouble(mModelServoOffsetLeftEditText3.getText().toString()));
        mFloidActivityFloidModelParameters.setServoOffsetRight3(Double.parseDouble(mModelServoOffsetRightEditText3.getText().toString()));
        mFloidActivityFloidModelParameters.setServoOffsetPitch3(Double.parseDouble(mModelServoOffsetPitchEditText3.getText().toString()));
        // Both of these are unused - real ones: DistanceToTargetMinValue and DistanceToTargetMaxValue
        mFloidActivityFloidModelParameters.setTargetVelocityKeepStill(2.5);
        mFloidActivityFloidModelParameters.setTargetVelocityFullSpeed(10.0);
        mFloidActivityFloidModelParameters.setTargetPitchVelocityAlpha(Double.parseDouble(mModelTargetPitchVelocityAlphaEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setTargetRollVelocityAlpha(Double.parseDouble(mModelTargetRollVelocityAlphaEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setTargetHeadingVelocityAlpha(Double.parseDouble(mModelTargetHeadingVelocityAlphaEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setTargetAltitudeVelocityAlpha(Double.parseDouble(mModelTargetAltitudeVelocityAlphaEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setRotationMode(mModelRotationModeSpinner.getSelectedItemPosition());
        mFloidActivityFloidModelParameters.setCyclicHeadingAlpha(Double.parseDouble(mModelCyclicHeadingAlphaEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setHeliStartupModeNumberSteps(Integer.parseInt(mModelHeliStartupNumberStepsEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setHeliStartupModeStepTick(Integer.parseInt(mModelHeliStartupStepTickEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setFloidModelEscCollectiveCalcMidpoint(Double.parseDouble(mModelEscCollectiveCalcMidpointEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setFloidModelEscCollectiveLowValue(Double.parseDouble(mModelEscCollectiveLowValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setFloidModelEscCollectiveMidValue(Double.parseDouble(mModelEscCollectiveMidValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setFloidModelEscCollectiveHighValue(Double.parseDouble(mModelEscCollectiveHighValueEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setVelocityDeltaCyclicAlphaScale(Double.parseDouble(mModelVelocityDeltaCyclicAlphaScaleEditText.getText().toString()));
        mFloidActivityFloidModelParameters.setAccelerationMultiplierScale(Double.parseDouble(mModelAccelerationScaleMultiplierEditText.getText().toString()));
    }

    /**
     * Sets output controls.
     */
    private void setupOutputControls() {
        // 0. Set up mode:
        if (mFloidActivityUseLogger) Log.d(TAG, "..Mode");
        mFloidModeFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.FLOID_MODE_PACKET, 0, mFloidModeToggleButton, "Mission");
        new FloidButtonController(this, FloidService.TEST_PACKET, 0, mTestRelayButton, "TEST PKT");
        // Test goals:
        // ===========
        if (mFloidActivityUseLogger) Log.d(TAG, "..Test Goals");
        mTestSetGoalsButtonController = new FloidButtonController(this, FloidService.TEST_GOALS_PACKET, 0, mTestSetGoalsButton, "Set Goals");
        mGoalsToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_GOALS, mGoalsToggleButton, "GOAL");
        mLiftOffModeToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_LIFT_OFF_MODE_SWITCH, mLiftOffModeToggleButton, "Lift Off");
        mNormalModeToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_NORMAL_MODE_SWITCH, mNormalModeToggleButton, "Normal");
        mLandModeToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_LAND_MODE_SWITCH, mLandModeToggleButton, "Land");
        // Run/Test modes:
        // Set up devices:
        // ===============
        if (mFloidActivityUseLogger) Log.d(TAG, "..Run/Test Modes");
        mRunModeProductionFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_RUN_MODE_PRODUCTION, mRunModeProductionToggleButton, "PROD MODE");
        mTestModePrintDebugHeadingsFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_MODE_PRINT_DEBUG_HEADINGS, mTestModePrintDebugHeadingsToggleButton, "DEBUG HEADERS");
        mTestModeCheckPhysicsModelFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_MODE_CHECK_PHYSICS_MODEL, mTestModeCheckPhysicsModelToggleButton, "CHECK PHYSICS");
        mRunModeFloidTestXYToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_RUN_MODE_TEST_XY, mRunModeTestXYToggleButton, "XY");
        mRunModeTestAltitude1FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_1, mRunModeTestAltitude1ToggleButton, "Alt1");
        mRunModeTestAltitude2FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_2, mRunModeTestAltitude2ToggleButton, "Alt2");
        mRunModeTestHeading1FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_RUN_MODE_TEST_HEADING_1, mRunModeTestHeading1ToggleButton, "H1");
        mRunModeTestPitch1FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_RUN_MODE_TEST_PITCH_1, mRunModeTestPitch1ToggleButton, "P1");
        mRunModeTestRoll1FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_RUN_MODE_TEST_ROLL_1, mRunModeTestRoll1ToggleButton, "R1");
        mTestModeNoXYFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_MODE_NO_XY, mTestModeNoXYToggleButton, "XY");
        mTestModeNoAltitudeFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_MODE_NO_ALTITUDE, mTestModeNoAltitudeToggleButton, "ALT");
        mTestModeNoAttackAngleFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_MODE_NO_ATTACK_ANGLE, mTestModeNoAttackAngleToggleButton, "AA");
        mTestModeNoHeadingFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_MODE_NO_HEADING, mTestModeNoHeadingToggleButton, "H");
        mTestModeNoPitchFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_MODE_NO_PITCH, mTestModeNoPitchToggleButton, "P");
        mTestModeNoRollFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_MODE_NO_ROLL, mTestModeNoRollToggleButton, "R");

        // Set up devices:
        // ===============
        if (mFloidActivityUseLogger) Log.d(TAG, "..Devices");
        // LED:
        mLedFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_LED, mLedToggleButton, "LED");
        // GPS:
        mGpsFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_GPS, mGpsToggleButton, "GPS");
        // GPS Emulate:
        mGpsEmulateFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_GPS_EMULATE, mGpsEmulateToggleButton, "Emulate");
        // GPS From Device:
        mGpsDeviceFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_GPS_DEVICE, mGpsDeviceToggleButton, "Device");
        // Etc. etc.
        mPyrFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_PYR, mPyrToggleButton, "IMU");
        // Etc. etc.
        mPyrADFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_PYR_AD_SWITCH, mPyrADToggleButton, "Digital");
        // GPS Rate Spinner:
        mCurrentGpsRateSelection = mFloidActivityFloidStatus.getJr();
        mGpsRateSpinner.setSelection(mCurrentGpsRateSelection);
        mGpsRateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                      public void onItemSelected(AdapterView<?> adapterView, View view, int selection, long l) {
                                                          if (mCurrentGpsRateSelection != selection) {
                                                              byte newSelectionGPSRateDevice = 0;
                                                              switch (selection) {
                                                                  case 0: {
                                                                      newSelectionGPSRateDevice = (byte) FloidService.RELAY_DEVICE_GPS_RATE_0_SWITCH;
                                                                  }
                                                                  break;
                                                                  case 1: {
                                                                      newSelectionGPSRateDevice = (byte) FloidService.RELAY_DEVICE_GPS_RATE_1_SWITCH;
                                                                  }
                                                                  break;
                                                                  case 2: {
                                                                      newSelectionGPSRateDevice = (byte) FloidService.RELAY_DEVICE_GPS_RATE_2_SWITCH;
                                                                  }
                                                                  break;
                                                                  case 3: {
                                                                      newSelectionGPSRateDevice = (byte) FloidService.RELAY_DEVICE_GPS_RATE_3_SWITCH;
                                                                  }
                                                                  break;
                                                                  case 4: {
                                                                      newSelectionGPSRateDevice = (byte) FloidService.RELAY_DEVICE_GPS_RATE_4_SWITCH;
                                                                  }
                                                                  break;
                                                                  default: {
                                                                      // Do nothing
                                                                  }
                                                                  break;
                                                              }
                                                              sendRelayCommandToFloidService(newSelectionGPSRateDevice, (byte) FloidService.RELAY_DEVICE_STATE_ON);
                                                          }
                                                          mCurrentGpsRateSelection = selection;
                                                      }

                                                      public void onNothingSelected(AdapterView<?> adapterView) {
                                                      }
                                                  }
        );
        // PYR Rate Spinner:
        mCurrentPyrRateSelection = mFloidActivityFloidStatus.getPar();
        mPyrRateSpinner.setSelection(mCurrentPyrRateSelection);
        mPyrRateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                      public void onItemSelected(AdapterView<?> adapterView, View view, int selection, long l) {
                                                          if (mCurrentPyrRateSelection != selection) {
                                                              byte newSelectionPYRRateDevice = 0;
                                                              switch (selection) {
                                                                  case 0: {
                                                                      newSelectionPYRRateDevice = (byte) FloidService.RELAY_DEVICE_PYR_RATE_0_SWITCH;
                                                                  }
                                                                  break;
                                                                  case 1: {
                                                                      newSelectionPYRRateDevice = (byte) FloidService.RELAY_DEVICE_PYR_RATE_1_SWITCH;
                                                                  }
                                                                  break;
                                                                  case 2: {
                                                                      newSelectionPYRRateDevice = (byte) FloidService.RELAY_DEVICE_PYR_RATE_2_SWITCH;
                                                                  }
                                                                  break;
                                                                  case 3: {
                                                                      newSelectionPYRRateDevice = (byte) FloidService.RELAY_DEVICE_PYR_RATE_3_SWITCH;
                                                                  }
                                                                  break;
                                                                  case 4: {
                                                                      newSelectionPYRRateDevice = (byte) FloidService.RELAY_DEVICE_PYR_RATE_4_SWITCH;
                                                                  }
                                                                  break;
                                                                  case 5: {
                                                                      newSelectionPYRRateDevice = (byte) FloidService.RELAY_DEVICE_PYR_RATE_5_SWITCH;
                                                                  }
                                                                  break;
                                                                  default: {
                                                                      // Do nothing
                                                                  }
                                                                  break;
                                                              }
                                                              sendRelayCommandToFloidService(newSelectionPYRRateDevice, (byte) FloidService.RELAY_DEVICE_STATE_ON);
                                                          }
                                                          mCurrentPyrRateSelection = selection;
                                                      }

                                                      public void onNothingSelected(AdapterView<?> adapterView) {
                                                      }
                                                  }
        );
        mPyrRateSpinner.setSelection(mFloidActivityFloidStatus.getPar());
        // PYR Emulation:
        if (mFloidActivityUseLogger) Log.d(TAG, "..PYR_Emulation");
        mPyrEmulationController = setupPyrEmulationController(R.id.pyr_emulation_container);
        View pyrEmulationToggleView = mPyrEmulationContainer.findViewById(R.id.emulate_imu_algorithm_relay);
        mPyrEmulationDisabledTextView = mPyrEmulationContainer.findViewById(R.id.pyr_emulation_disabled_label);
        // PYR Emulation Algorithm:
        mPyrEmulateAlgorithmToggleButton = pyrEmulationToggleView.findViewById(R.id.relay_toggle_button);
        mPyrEmulateAlgorithmToggleButton.setOnClickListener(v -> {
            // Turns on and off the pyr emulation
            mPyrEmulationAlgorithmOn = mPyrEmulateAlgorithmToggleButton.isChecked();
            if (mPyrEmulationAlgorithmOn) {
                startPyrEmulation();
            } else {
                stopPyrEmulation();
            }
        });
        mPyrEmulateAlgorithmToggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mPyrEmulationAlgorithmOn = isChecked;
            if (mPyrEmulationAlgorithmOn) {
                startPyrEmulation();
            } else {
                stopPyrEmulation();
            }
        });
        // PYR Emulation Algorithm rate spinner:
        mPyrEmulationAlgorithmRateSpinner = mPyrEmulationContainer.findViewById(R.id.pyr_emulation_algorithm_rate_spinner);
        mPyrEmulationAlgorithmRateSpinner.setSelection(mPyrEmulationAlgorithmRate);
        mPyrEmulationAlgorithmRateSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(
                    AdapterView<?> parent,
                    View view,
                    int position,
                    long id) {
                mPyrEmulationAlgorithmRate = position;
            }

            @Override
            public void onNothingSelected(
                    AdapterView<?> parent) {
            }
        });
        // PYR Emulation Algorithm Controllers:
        mPyrEmulationAlgorithmPitchController = new FloidIMUEmulationAlgorithmOptionController(
                mFloidDroidMessageHandler,
                mPyrEmulationContainer,
                R.id.emulate_pyr_pitch_min_edit_text,
                R.id.emulate_pyr_pitch_max_edit_text,
                R.id.emulate_pyr_pitch_delta_edit_text,
                R.id.emulate_pyr_pitch_start_edit_text,
                R.id.pyr_emulation_pitch_current_value,
                R.id.pyr_emulation_algorithm_pitch_spinner,
                R.id.pyr_emulation_pitch_absolute_min,
                R.id.pyr_emulation_pitch_absolute_max,
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_PITCH_START,        // Current - defaults to start :)
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_PITCH_MIN,          // Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_PITCH_MAX,          // Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_PITCH_MIN,          // Absolute Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_PITCH_MAX,          // Absolute Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_PITCH_RATE_DEFAULT, // Rate
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_PITCH_RATE_MIN,     // Rate min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_PITCH_RATE_MAX,     // Rate max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_PITCH_START,        // Start
                FloidIMUEmulationAlgorithmOptionController.FLOID_PYR_EMULATION_ALGORITHM_NONE);
        mPyrEmulationAlgorithmHeadingController = new FloidIMUEmulationAlgorithmOptionController(
                mFloidDroidMessageHandler,
                mPyrEmulationContainer,
                R.id.emulate_pyr_heading_min_edit_text,
                R.id.emulate_pyr_heading_max_edit_text,
                R.id.emulate_pyr_heading_delta_edit_text,
                R.id.emulate_pyr_heading_start_edit_text,
                R.id.pyr_emulation_heading_current_value,
                R.id.pyr_emulation_algorithm_heading_spinner,
                R.id.pyr_emulation_heading_absolute_min,
                R.id.pyr_emulation_heading_absolute_max,
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_HEADING_START,        // Current - defaults to start :)
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_HEADING_MIN,          // Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_HEADING_MAX,          // Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_HEADING_MIN,          // Absolute Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_HEADING_MAX,          // Absolute Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_HEADING_RATE_DEFAULT, // Rate
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_HEADING_RATE_MIN,     // Rate min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_HEADING_RATE_MAX,     // Rate max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_HEADING_START,        // Start
                FloidIMUEmulationAlgorithmOptionController.FLOID_PYR_EMULATION_ALGORITHM_NONE);
        mPyrEmulationAlgorithmRollController = new FloidIMUEmulationAlgorithmOptionController(
                mFloidDroidMessageHandler,
                mPyrEmulationContainer,
                R.id.emulate_pyr_roll_min_edit_text,
                R.id.emulate_pyr_roll_max_edit_text,
                R.id.emulate_pyr_roll_delta_edit_text,
                R.id.emulate_pyr_roll_start_edit_text,
                R.id.pyr_emulation_roll_current_value,
                R.id.pyr_emulation_algorithm_roll_spinner,
                R.id.pyr_emulation_roll_absolute_min,
                R.id.pyr_emulation_roll_absolute_max,
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ROLL_START,        // Current - defaults to start :)
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ROLL_MIN,          // Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ROLL_MAX,          // Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ROLL_MIN,          // Absolute Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ROLL_MAX,          // Absolute Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ROLL_RATE_DEFAULT, // Rate
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ROLL_RATE_MIN,     // Rate min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ROLL_RATE_MAX,     // Rate max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ROLL_START,        // Start
                FloidIMUEmulationAlgorithmOptionController.FLOID_PYR_EMULATION_ALGORITHM_NONE);
        mPyrEmulationAlgorithmAltitudeController = new FloidIMUEmulationAlgorithmOptionController(
                mFloidDroidMessageHandler,
                mPyrEmulationContainer,
                R.id.emulate_pyr_altitude_min_edit_text,
                R.id.emulate_pyr_altitude_max_edit_text,
                R.id.emulate_pyr_altitude_delta_edit_text,
                R.id.emulate_pyr_altitude_start_edit_text,
                R.id.pyr_emulation_altitude_current_value,
                R.id.pyr_emulation_algorithm_altitude_spinner,
                R.id.pyr_emulation_altitude_absolute_min,
                R.id.pyr_emulation_altitude_absolute_max,
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_START,        // Current - defaults to start :)
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MIN,          // Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MAX,          // Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MIN,          // Absolute Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_MAX,          // Absolute Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_RATE_DEFAULT, // Rate
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_RATE_MIN,     // Rate min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_RATE_MAX,     // Rate max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_ALTITUDE_START,        // Start
                FloidIMUEmulationAlgorithmOptionController.FLOID_PYR_EMULATION_ALGORITHM_NONE);
        mPyrEmulationAlgorithmTemperatureController = new FloidIMUEmulationAlgorithmOptionController(
                mFloidDroidMessageHandler,
                mPyrEmulationContainer,
                R.id.emulate_pyr_temperature_min_edit_text,
                R.id.emulate_pyr_temperature_max_edit_text,
                R.id.emulate_pyr_temperature_delta_edit_text,
                R.id.emulate_pyr_temperature_start_edit_text,
                R.id.pyr_emulation_temperature_current_value,
                R.id.pyr_emulation_algorithm_temperature_spinner,
                R.id.pyr_emulation_temperature_absolute_min,
                R.id.pyr_emulation_temperature_absolute_max,
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_START,        // Current - defaults to start :)
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MIN,          // Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MAX,          // Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MIN,          // Absolute Min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_MAX,          // Absolute Max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_RATE_DEFAULT, // Rate
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_RATE_MIN,     // Rate min
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_RATE_MAX,     // Rate max
                FloidIMUEmulationController.FLOID_PYR_EMULATION_CONTROLLER_TEMPERATURE_START,        // Start
                FloidIMUEmulationAlgorithmOptionController.FLOID_PYR_EMULATION_ALGORITHM_NONE);
        mPyrDeviceFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_PYR_DEVICE, mPyrDeviceToggleButton, "Device");
        mPyrDeviceToggleButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                // Enable PYR Emulation Interface - see if we should turn on automatic controls or not:
                pyrInterfaceEmulationEnable(true);
            } else {
                // Turn off PYR Emulation Algorithm:
                mPyrEmulateAlgorithmToggleButton.setChecked(false);
                // Disable PYR Emulation Interface:
                pyrInterfaceEmulationEnable(false);
            }
//                Log.d(TAG, "PYR DEVICE IS CHECKED: " + Boolean.toString(isChecked));
        });
        // Interface starts off disabled:
        pyrInterfaceEmulationEnable(false);

        // Floid Status Rate Spinner:
        mCurrentFloidStatusRateSelection = mFloidActivityFloidStatus.getSr();
        mFloidStatusRateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                              public void onItemSelected(AdapterView<?> adapterView, View view, int selection, long l) {
                                                                  if (mCurrentFloidStatusRateSelection != selection) {
                                                                      byte newSelectionFloidStatusRateDevice = 0;
                                                                      switch (selection) {
                                                                          case 0: {
                                                                              newSelectionFloidStatusRateDevice = (byte) FloidService.RELAY_DEVICE_FLOID_STATUS_RATE_0_SWITCH;
                                                                          }
                                                                          break;
                                                                          case 1: {
                                                                              newSelectionFloidStatusRateDevice = (byte) FloidService.RELAY_DEVICE_FLOID_STATUS_RATE_1_SWITCH;
                                                                          }
                                                                          break;
                                                                          case 2: {
                                                                              newSelectionFloidStatusRateDevice = (byte) FloidService.RELAY_DEVICE_FLOID_STATUS_RATE_2_SWITCH;
                                                                          }
                                                                          break;
                                                                          case 3: {
                                                                              newSelectionFloidStatusRateDevice = (byte) FloidService.RELAY_DEVICE_FLOID_STATUS_RATE_3_SWITCH;
                                                                          }
                                                                          break;
                                                                          case 4: {
                                                                              newSelectionFloidStatusRateDevice = (byte) FloidService.RELAY_DEVICE_FLOID_STATUS_RATE_4_SWITCH;
                                                                          }
                                                                          break;
                                                                          case 5: {
                                                                              newSelectionFloidStatusRateDevice = (byte) FloidService.RELAY_DEVICE_FLOID_STATUS_RATE_5_SWITCH;
                                                                          }
                                                                          break;
                                                                          case 6: {
                                                                              newSelectionFloidStatusRateDevice = (byte) FloidService.RELAY_DEVICE_FLOID_STATUS_RATE_6_SWITCH;
                                                                          }
                                                                          break;
                                                                          case 7: {
                                                                              newSelectionFloidStatusRateDevice = (byte) FloidService.RELAY_DEVICE_FLOID_STATUS_RATE_7_SWITCH;
                                                                          }
                                                                          break;
                                                                          default: {
                                                                              // Do nothing
                                                                          }
                                                                          break;
                                                                      }
                                                                      sendRelayCommandToFloidService(newSelectionFloidStatusRateDevice, (byte) FloidService.RELAY_DEVICE_STATE_ON);
                                                                  }
                                                                  mCurrentFloidStatusRateSelection = selection;
                                                              }

                                                              public void onNothingSelected(AdapterView<?> adapterView) {
                                                              }
                                                          }
        );

        mDesignatorFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_DESIGNATOR, mDesignatorToggleButton, "DES");
        mAuxFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_AUX, mAuxToggleButton, "AUX");
        mParachuteFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_PARACHUTE, mParachuteToggleButton, "PAR");
        if (mFloidActivityUseLogger) Log.d(TAG, "..Declination");

        // 1.1 Set up declination controller:
        mDeclinationController = setupDeclinationController(R.id.declinationContainer, R.id.declination_relay, "DECL", R.id.declination_text);
        if (mFloidActivityUseLogger) Log.d(TAG, "..Helis");
        // 2. Set up Helis:
        // Format: heli is in target, servo is in subTarget - these need to be put into a single byte - four bits each
        // Heli 0:
        mHeli0TabLabel = findViewById(R.id.heli0_tab_label);
        mHeli0Container = findViewById(R.id.heli0);
        mHeli0EscFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC0, mHeli0EscToggleButton, "ESC");
        mHeli0EscSetupFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC0_SETUP, mHeli0EscSetupButton, "SETUP");
        mHeli0EscExtraFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC0_EXTRA, mHeli0EscExtraButton, "EXTRA");
        mHeli0ServoFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_S0, mHeli0ServoToggleButton, "Servo");
        mE0ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 0, FloidService.HELI_CONTROL_ESC_SERVO, R.id.heli0, R.id.esc_servo, R.id.servo_slider, R.id.servo_slider_label, "E0");
        mL0ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 0, FloidService.HELI_CONTROL_LEFT_SERVO, R.id.heli0, R.id.left_servo, R.id.servo_slider, R.id.servo_slider_label, "L0");
        mR0ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 0, FloidService.HELI_CONTROL_RIGHT_SERVO, R.id.heli0, R.id.right_servo, R.id.servo_slider, R.id.servo_slider_label, "R0");
        mP0ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 0, FloidService.HELI_CONTROL_PITCH_SERVO, R.id.heli0, R.id.pitch_servo, R.id.servo_slider, R.id.servo_slider_label, "P0");
        // Heli 1:
        mHeli1TabLabel = findViewById(R.id.heli1_tab_label);
        mHeli1Container = findViewById(R.id.heli1);
        mHeli1EscFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC1, mHeli1EscToggleButton, "ESC");
        mHeli1EscSetupFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC1_SETUP, mHeli1EscSetupButton, "SETUP");
        mHeli1EscExtraFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC1_EXTRA, mHeli1EscExtraButton, "EXTRA");
        mHeli1ServoFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_S1, mHeli1ServoToggleButton, "Servo");
        mE1ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 1, FloidService.HELI_CONTROL_ESC_SERVO, R.id.heli1, R.id.esc_servo, R.id.servo_slider, R.id.servo_slider_label, "E1");
        mL1ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 1, FloidService.HELI_CONTROL_LEFT_SERVO, R.id.heli1, R.id.left_servo, R.id.servo_slider, R.id.servo_slider_label, "L1");
        mR1ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 1, FloidService.HELI_CONTROL_RIGHT_SERVO, R.id.heli1, R.id.right_servo, R.id.servo_slider, R.id.servo_slider_label, "R1");
        mP1ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 1, FloidService.HELI_CONTROL_PITCH_SERVO, R.id.heli1, R.id.pitch_servo, R.id.servo_slider, R.id.servo_slider_label, "P1");
        // Heli 2:
        mHeli2TabLabel = findViewById(R.id.heli2_tab_label);
        mHeli2Container = findViewById(R.id.heli2);
        mHeli2EscFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC2, mHeli2EscToggleButton, "ESC");
        mHeli2EscSetupFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC2_SETUP, mHeli2EscSetupButton, "SETUP");
        mHeli2EscExtraFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC2_EXTRA, mHeli2EscExtraButton, "EXTRA");
        mHeli2ServoFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_S2, mHeli2ServoToggleButton, "Servo");
        mE2ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 2, FloidService.HELI_CONTROL_ESC_SERVO, R.id.heli2, R.id.esc_servo, R.id.servo_slider, R.id.servo_slider_label, "E2");
        mL2ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 2, FloidService.HELI_CONTROL_LEFT_SERVO, R.id.heli2, R.id.left_servo, R.id.servo_slider, R.id.servo_slider_label, "L2");
        mR2ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 2, FloidService.HELI_CONTROL_RIGHT_SERVO, R.id.heli2, R.id.right_servo, R.id.servo_slider, R.id.servo_slider_label, "R2");
        mP2ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 2, FloidService.HELI_CONTROL_PITCH_SERVO, R.id.heli2, R.id.pitch_servo, R.id.servo_slider, R.id.servo_slider_label, "P2");
        // Heli 3:
        mHeli3TabLabel = findViewById(R.id.heli3_tab_label);
        mHeli3Container = findViewById(R.id.heli3);
        mHeli3EscFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC3, mHeli3EscToggleButton, "ESC");
        mHeli3EscSetupFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC3_SETUP, mHeli3EscSetupButton, "SETUP");
        mHeli3EscExtraFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_ESC3_EXTRA, mHeli3EscExtraButton, "EXTRA");
        mHeli3ServoFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_S3, mHeli3ServoToggleButton, "Servo");
        mE3ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 3, FloidService.HELI_CONTROL_ESC_SERVO, R.id.heli3, R.id.esc_servo, R.id.servo_slider, R.id.servo_slider_label, "E3");
        mL3ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 3, FloidService.HELI_CONTROL_LEFT_SERVO, R.id.heli3, R.id.left_servo, R.id.servo_slider, R.id.servo_slider_label, "L3");
        mR3ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 3, FloidService.HELI_CONTROL_RIGHT_SERVO, R.id.heli3, R.id.right_servo, R.id.servo_slider, R.id.servo_slider_label, "R3");
        mP3ServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 3, FloidService.HELI_CONTROL_PITCH_SERVO, R.id.heli3, R.id.pitch_servo, R.id.servo_slider, R.id.servo_slider_label, "P3");
        // Set up the heli tabs and their visibility:
        mCurrentHeliViewId = R.id.heli0_tab_label;  // First is selected by default:
        mHeli0TabLabel.setOnClickListener(heliViewOnClickListener);
        mHeli0TabLabel.setBackgroundColor(mFocusedSubMenuTabColor);
        mHeli0TabLabel.setTextColor(mFocusedSubMenuTabTextColor);
        mHeli0Container.setVisibility(View.VISIBLE);

        mHeli1TabLabel.setOnClickListener(heliViewOnClickListener);
        mHeli1TabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mHeli1TabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mHeli1Container.setVisibility(View.GONE);

        mHeli2TabLabel.setOnClickListener(heliViewOnClickListener);
        mHeli2TabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mHeli2TabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mHeli2Container.setVisibility(View.GONE);

        mHeli3TabLabel.setOnClickListener(heliViewOnClickListener);
        mHeli3TabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mHeli3TabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mHeli3Container.setVisibility(View.GONE);
        if (mFloidActivityUseLogger) Log.d(TAG, "..Pan/Tilts");
        // 3. Set up Pan Tilts:
        // PT0:
        mPanTilt0TabLabel = findViewById(R.id.pt0_tab_label);
        mPanTilt0Container = findViewById(R.id.pantilt0);
        mPanTilt0FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_PT0, mPanTilt0ToggleButton, "PT0");
        mPT0PServoController = setupServoController(FloidService.PAN_TILT_PACKET, 0, FloidService.PAN_TILT_PAN_SUBCOMMAND, R.id.pantilt0, R.id.pan_servo, R.id.servo_slider, R.id.servo_slider_label, "Pan");
        mPT0TServoController = setupServoController(FloidService.PAN_TILT_PACKET, 0, FloidService.PAN_TILT_TILT_SUBCOMMAND, R.id.pantilt0, R.id.tilt_servo, R.id.servo_slider, R.id.servo_slider_label, "Tilt");
        // PT1:
        mPanTilt1TabLabel = findViewById(R.id.pt1_tab_label);
        mPanTilt1Container = findViewById(R.id.pantilt1);
        mPanTilt1FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_PT1, mPanTilt1ToggleButton, "PT1");
        mPT1PServoController = setupServoController(FloidService.PAN_TILT_PACKET, 1, FloidService.PAN_TILT_PAN_SUBCOMMAND, R.id.pantilt1, R.id.pan_servo, R.id.servo_slider, R.id.servo_slider_label, "Pan");
        mPT1TServoController = setupServoController(FloidService.PAN_TILT_PACKET, 1, FloidService.PAN_TILT_TILT_SUBCOMMAND, R.id.pantilt1, R.id.tilt_servo, R.id.servo_slider, R.id.servo_slider_label, "Tilt");
        // PT2:
        mPanTilt2TabLabel = findViewById(R.id.pt2_tab_label);
        mPanTilt2Container = findViewById(R.id.pantilt2);
        mPanTilt2FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_PT2, mPanTilt2ToggleButton, "PT2");
        mPT2PServoController = setupServoController(FloidService.PAN_TILT_PACKET, 2, FloidService.PAN_TILT_PAN_SUBCOMMAND, R.id.pantilt2, R.id.pan_servo, R.id.servo_slider, R.id.servo_slider_label, "Pan");
        mPT2TServoController = setupServoController(FloidService.PAN_TILT_PACKET, 2, FloidService.PAN_TILT_TILT_SUBCOMMAND, R.id.pantilt2, R.id.tilt_servo, R.id.servo_slider, R.id.servo_slider_label, "Tilt");
        // PT3:
        mPanTilt3TabLabel = findViewById(R.id.pt3_tab_label);
        mPanTilt3Container = findViewById(R.id.pantilt3);
        mPanTilt3FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_PT3, mPanTilt3ToggleButton, "PT3");
        mPT3PServoController = setupServoController(FloidService.PAN_TILT_PACKET, 3, FloidService.PAN_TILT_PAN_SUBCOMMAND, R.id.pantilt3, R.id.pan_servo, R.id.servo_slider, R.id.servo_slider_label, "Pan");
        mPT3TServoController = setupServoController(FloidService.PAN_TILT_PACKET, 3, FloidService.PAN_TILT_TILT_SUBCOMMAND, R.id.pantilt3, R.id.tilt_servo, R.id.servo_slider, R.id.servo_slider_label, "Tilt");
        // Set up the pan tabs and their visibility:
        mCurrentPanTiltViewId = R.id.pt0_tab_label;  // First is selected by default:
        mPanTilt0TabLabel.setOnClickListener(panTiltViewOnClickListener);
        mPanTilt0TabLabel.setBackgroundColor(mFocusedSubMenuTabColor);
        mPanTilt0TabLabel.setTextColor(mFocusedSubMenuTabTextColor);
        mPanTilt0Container.setVisibility(View.VISIBLE);

        mPanTilt1TabLabel.setOnClickListener(panTiltViewOnClickListener);
        mPanTilt1TabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mPanTilt1TabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mPanTilt1Container.setVisibility(View.GONE);

        mPanTilt2TabLabel.setOnClickListener(panTiltViewOnClickListener);
        mPanTilt2TabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mPanTilt2TabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mPanTilt2Container.setVisibility(View.GONE);

        mPanTilt3TabLabel.setOnClickListener(panTiltViewOnClickListener);
        mPanTilt3TabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mPanTilt3TabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mPanTilt3Container.setVisibility(View.GONE);
        if (mFloidActivityUseLogger) Log.d(TAG, "..Payloads");
        // 4. Set up payloads:
        // Bay0:
        mBay0FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_NM0, mBay0ToggleButton, "Drop 0");
        // Bay1:
        mBay1FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_NM1, mBay1ToggleButton, "Drop 1");
        // Bay2:
        mBay2FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_NM2, mBay2ToggleButton, "Drop 2");
        // Bay3:
        mBay3FloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_NM3, mBay3ToggleButton, "Drop 3");
        // Debug:
        // ======
        mDebugFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_DEBUG, mDebugToggleButton, "Dbg");
        // Debug All:
        mDebugAllFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_ALL_DEVICES, mDebugAllToggleButton, "All");
        // Debug Aux:
        mDebugAuxFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_AUX, mDebugAuxToggleButton, "Aux");
        // Debug Payloads:
        mDebugPayloadsFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_PAYLOADS, mDebugPayloadsToggleButton, "Pay");
        // Debug Camera:
        mDebugCameraFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_CAMERA, mDebugCameraToggleButton, "Cam");
        // Debug Designator:
        mDebugDesignatorFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_DESIGNATOR, mDebugDesignatorToggleButton, "Des");
        // Debug GPS:
        mDebugGpsFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_GPS, mDebugGpsToggleButton, "Gps");
        // Debug Helis:
        mDebugHelisFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_HELIS, mDebugHelisToggleButton, "Hel");
        // Debug Memory:
        mDebugMemoryFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_MEMORY, mDebugMemoryToggleButton, "Mem");
        // Debug PanTilt:
        mDebugPanTiltFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_PAN_TILT, mDebugPanTiltToggleButton, "P/T");
        // Debug Physics:
        mDebugPhysicsFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_PHYSICS, mDebugPhysicsToggleButton, "Phy");
        // Debug PYR:
        mDebugPyrFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_PYR, mDebugPyrToggleButton, "Pyr");
        // Debug PYR:
        mDebugPyrFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.DEBUG_PACKET, FloidService.DEBUG_PACKET_ALTIMETER, mDebugAltimeterToggleButton, "Alt");
        // Serial Output:
        mSerialOutputFloidToggleButtonController = new FloidToggleButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_SERIAL_OUTPUT, mSerialOutputToggleButton, "Ser");
        // Model Status:
        // =============
        if (mFloidActivityUseLogger) Log.d(TAG, "..C/C");
        mCC0TabLabel = findViewById(R.id.cc0_tab_label);
        mCC0Container = findViewById(R.id.collective_cyclic_h0);

        mCC1TabLabel = findViewById(R.id.cc1_tab_label);
        mCC1Container = findViewById(R.id.collective_cyclic_h1);

        mCC2TabLabel = findViewById(R.id.cc2_tab_label);
        mCC2Container = findViewById(R.id.collective_cyclic_h2);

        mCC3TabLabel = findViewById(R.id.cc3_tab_label);
        mCC3Container = findViewById(R.id.collective_cyclic_h3);
        mModelStatusHeli0CollectiveServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 0, FloidService.HELI_CONTROL_COLLECTIVE_SERVO, R.id.collective_cyclic_h0, R.id.collective_slider_view_group, R.id.collective_slider, R.id.collective_slider_label, "COL-0");
        mModelStatusHeli1CollectiveServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 1, FloidService.HELI_CONTROL_COLLECTIVE_SERVO, R.id.collective_cyclic_h1, R.id.collective_slider_view_group, R.id.collective_slider, R.id.collective_slider_label, "COL-1");
        mModelStatusHeli2CollectiveServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 2, FloidService.HELI_CONTROL_COLLECTIVE_SERVO, R.id.collective_cyclic_h2, R.id.collective_slider_view_group, R.id.collective_slider, R.id.collective_slider_label, "COL-2");
        mModelStatusHeli3CollectiveServoController = setupServoController(FloidService.HELI_CONTROL_PACKET, 3, FloidService.HELI_CONTROL_COLLECTIVE_SERVO, R.id.collective_cyclic_h3, R.id.collective_slider_view_group, R.id.collective_slider, R.id.collective_slider_label, "COL-3");
        // Cyclic joysticks:
        mModelStatusHeli0CyclicJoystickView = mModelStatusHeli0ViewGroup.findViewById(R.id.cyclic_joystick);
        mModelStatusHeli0CyclicJoystickView.setOnJoystickMovedListener(new JoystickMovedListener() {
            @Override
            public void OnReturnedToCenter() {
            }

            @Override
            public void OnReleased() {
            }

            @Override
            public void OnMoved(int x, int y) {
                // Set the value to some item here...
                mModelStatusHeli0CyclicXTextView.setText(String.valueOf(x));
                mModelStatusHeli0CyclicYTextView.setText(String.valueOf(y));
            }
        });
        mModelStatusHeli1CyclicJoystickView = mModelStatusHeli1ViewGroup.findViewById(R.id.cyclic_joystick);
        mModelStatusHeli1CyclicJoystickView.setOnJoystickMovedListener(new JoystickMovedListener() {
            @Override
            public void OnReturnedToCenter() {
            }

            @Override
            public void OnReleased() {
            }

            @Override
            public void OnMoved(int x, int y) {
                // Set the value to some item here...
                mModelStatusHeli1CyclicXTextView.setText(String.valueOf(x));
                mModelStatusHeli1CyclicYTextView.setText(String.valueOf(y));
            }
        });
        mModelStatusHeli2CyclicJoystickView = mModelStatusHeli2ViewGroup.findViewById(R.id.cyclic_joystick);
        mModelStatusHeli2CyclicJoystickView.setOnJoystickMovedListener(new JoystickMovedListener() {
            @Override
            public void OnReturnedToCenter() {
            }

            @Override
            public void OnReleased() {
            }

            @Override
            public void OnMoved(int x, int y) {
                // Set the value to some item here...
                mModelStatusHeli2CyclicXTextView.setText(String.valueOf(x));
                mModelStatusHeli2CyclicYTextView.setText(String.valueOf(y));
            }
        });
        mModelStatusHeli3CyclicJoystickView = mModelStatusHeli3ViewGroup.findViewById(R.id.cyclic_joystick);
        mModelStatusHeli3CyclicJoystickView.setOnJoystickMovedListener(new JoystickMovedListener() {
            @Override
            public void OnReturnedToCenter() {
            }

            @Override
            public void OnReleased() {
            }

            @Override
            public void OnMoved(int x, int y) {
                // Set the value to some item here...
                mModelStatusHeli3CyclicXTextView.setText(String.valueOf(x));
                mModelStatusHeli3CyclicYTextView.setText(String.valueOf(y));
            }
        });
        // Set up the cc tabs and their visibility:
        mCurrentCCViewId = R.id.cc0_tab_label;  // First is selected by default:
        mCC0TabLabel.setOnClickListener(collectiveCyclicViewOnClickListener);
        mCC0TabLabel.setBackgroundColor(mFocusedSubMenuTabColor);
        mCC0TabLabel.setTextColor(mFocusedSubMenuTabTextColor);
        mCC0Container.setVisibility(View.VISIBLE);

        mCC1TabLabel.setOnClickListener(collectiveCyclicViewOnClickListener);
        mCC1TabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mCC1TabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mCC1Container.setVisibility(View.GONE);

        mCC2TabLabel.setOnClickListener(collectiveCyclicViewOnClickListener);
        mCC2TabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mCC2TabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mCC2Container.setVisibility(View.GONE);

        mCC3TabLabel.setOnClickListener(collectiveCyclicViewOnClickListener);
        mCC3TabLabel.setBackgroundColor(mUnFocusedSubMenuTabColor);
        mCC3TabLabel.setTextColor(mUnFocusedSubMenuTabTextColor);
        mCC3Container.setVisibility(View.GONE);

        if (mFloidActivityUseLogger) Log.d(TAG, "..Blades");
        // Blades low/zero/high:
        mModelBladesLowFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_BLADES_LOW_SWITCH, mModelBladesLowButton, "Set LOW");
        mModelBladesZeroFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_BLADES_ZERO_SWITCH, mModelBladesZeroButton, "Set ZERO");
        mModelBladesHighFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_TEST_BLADES_HIGH_SWITCH, mModelBladesHighButton, "Set HIGH");
        mModelSetFloidButtonController = new FloidButtonController(this, FloidService.MODEL_PARAMETERS_1_PACKET, 0, mModelSetButton, "Set");
        mModelRetrieveFloidButtonController = new FloidButtonController(this, FloidService.GET_MODEL_PARAMETERS_PACKET, 0, mModelRetrieveButton, "Get");

        // EEPROM Relays:
        if (mFloidActivityUseLogger) Log.d(TAG, "..EEPROM");
        mModelLoadFromEEPROMFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_LOAD_FROM_EEPROM, mModelLoadFromEEPROMButton, "EE Load");
        mModelSaveToEEPROMFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_SAVE_TO_EEPROM, mModelSaveToEEPROMButton, "EE Save");
        mModelClearEEPROMFloidButtonController = new FloidButtonController(this, FloidService.RELAY_PACKET, FloidService.RELAY_DEVICE_CLEAR_EEPROM, mModelClearEEPROMButton, "EE Clear");
        if (mFloidActivityUseLogger) Log.d(TAG, "..Start Mode");
        // Start Mode Spinner:
        mModelStartModeSpinner.setSelection(mFloidActivityFloidModelParameters.getStartMode());
        mModelStartModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                             public void onItemSelected(AdapterView<?> adapterView, View view, int selection, long l) {
                                                                 mFloidActivityFloidModelParameters.setStartMode((byte) selection);
                                                             }

                                                             public void onNothingSelected(AdapterView<?> adapterView) {
                                                             }
                                                         }
        );
        if (mFloidActivityUseLogger) Log.d(TAG, "..Rotation Mode");
        mModelRotationModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                                      public void onItemSelected(AdapterView<?> adapterView, View view, int selection, long l) {
                                                                          mFloidActivityFloidModelParameters.setRotationMode((byte) selection);
                                                                      }

                                                                      public void onNothingSelected(AdapterView<?> adapterView) {
                                                                      }
                                                                  }
        );
        if (mFloidActivityUseLogger) Log.d(TAG, "Done Output Controls");
    }
    private static final int CAMERA_INFO_TAB_STATUS = 0;
    private static final int CAMERA_INFO_TAB_PHOTO = 1;
    private static final int CAMERA_INFO_TAB_VIDEO = 2;
    private void setupImagingTabs() {
        // Get the imaging status tab label:
        LinearLayout imagingStatusTabLabel = (LinearLayout)mImagingTabsLabels.getChildAt(CAMERA_INFO_TAB_STATUS);
        TextView imagingStatusTabLabelText = imagingStatusTabLabel.findViewById(R.id.tab_label_text);
        imagingStatusTabLabelText.setText(R.string.default_label_status);
        imagingStatusTabLabelText.setBackgroundColor(mFocusedSubMenuTabColor);
        imagingStatusTabLabelText.setTextColor(mFocusedSubMenuTabTextColor);
        imagingStatusTabLabelText.setOnClickListener(imagingTabOnClickListener);
        // Get the photo tab label:
        LinearLayout photoTabLabel = (LinearLayout)mImagingTabsLabels.getChildAt(CAMERA_INFO_TAB_PHOTO);
        TextView photoTabLabelText = photoTabLabel.findViewById(R.id.tab_label_text);
        photoTabLabelText.setText(R.string.default_label_photo);
        photoTabLabelText.setBackgroundColor(mUnFocusedSubMenuTabColor);
        photoTabLabelText.setTextColor(mUnFocusedSubMenuTabTextColor);
        photoTabLabelText.setOnClickListener(imagingTabOnClickListener);
        // Get the video tab label:
        LinearLayout videoTabLabel = (LinearLayout)mImagingTabsLabels.getChildAt(CAMERA_INFO_TAB_VIDEO);
        TextView videoTabLabelText = videoTabLabel.findViewById(R.id.tab_label_text);
        videoTabLabelText.setText(R.string.default_label_video);
        videoTabLabelText.setBackgroundColor(mUnFocusedSubMenuTabColor);
        videoTabLabelText.setTextColor(mUnFocusedSubMenuTabTextColor);
        videoTabLabelText.setOnClickListener(imagingTabOnClickListener);

        // Get the imaging status tab contents:
        ScrollView imagingStatusTabContents = (ScrollView)mImagingTabsContents.getChildAt(CAMERA_INFO_TAB_STATUS);
        imagingStatusTabContents.setVisibility(View.VISIBLE);
        // Get the photo tab contents:
        ScrollView photoTabContents = (ScrollView)mImagingTabsContents.getChildAt(CAMERA_INFO_TAB_PHOTO);
        photoTabContents.setVisibility(View.INVISIBLE);
        // Get the video tab contents:
        ScrollView videoTabContents = (ScrollView)mImagingTabsContents.getChildAt(CAMERA_INFO_TAB_VIDEO);
        videoTabContents.setVisibility(View.INVISIBLE);
    }
    private final OnClickListener imagingTabOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            // We get this click on a tab label...
            // Go through each tab label and content
            // Modify tab and hide/show content
            for(int tabIndex=0; tabIndex < mImagingTabsLabels.getChildCount(); ++tabIndex) {
                View labelView = mImagingTabsLabels.getChildAt(tabIndex);
                TextView labelTextView = labelView.findViewById(R.id.tab_label_text);
                View contentsView = mImagingTabsContents.getChildAt(tabIndex);
                if(labelTextView==v) {
                    // Show
                    labelTextView.setBackgroundColor(mFocusedSubMenuTabColor);
                    labelTextView.setTextColor(mFocusedSubMenuTabTextColor);
                    contentsView.setVisibility(View.VISIBLE);
                } else {
                    // Hide
                    labelTextView.setBackgroundColor(mUnFocusedSubMenuTabColor);
                    labelTextView.setTextColor(mUnFocusedSubMenuTabTextColor);
                    contentsView.setVisibility(View.INVISIBLE);
                }
            }
        }
    };



    /**
     * Pyr interface emulation enable.
     *
     * @param enable enable flag
     */
// Enable/Disable the emulation interface:
    private void pyrInterfaceEmulationEnable(boolean enable) {
        // Overall:
        mPyrEmulateAlgorithmToggleButton.setEnabled(enable);
        mPyrEmulationAlgorithmRateSpinner.setEnabled(enable);
        if (enable) {
            mPyrEmulationDisabledTextView.setVisibility(View.GONE);
            // Manual Sliders:
            mPyrEmulationController.setEnable(true);
            // Manual controls:
            mPyrEmulationAlgorithmPitchController.setEnable(true);
            mPyrEmulationAlgorithmHeadingController.setEnable(true);
            mPyrEmulationAlgorithmRollController.setEnable(true);
            mPyrEmulationAlgorithmAltitudeController.setEnable(true);
            mPyrEmulationAlgorithmTemperatureController.setEnable(true);
        } else {
            mPyrEmulationDisabledTextView.setVisibility(View.VISIBLE);
            // Manual Sliders:
            mPyrEmulationController.setEnable(false);
            // Manual controls:
            mPyrEmulationAlgorithmPitchController.setEnable(false);
            mPyrEmulationAlgorithmHeadingController.setEnable(false);
            mPyrEmulationAlgorithmRollController.setEnable(false);
            mPyrEmulationAlgorithmAltitudeController.setEnable(false);
            mPyrEmulationAlgorithmTemperatureController.setEnable(false);
        }
    }

    /**
     * Sets servo controller.
     *
     * @param command   the command
     * @param target    the target
     * @param subTarget the sub-target
     * @param viewId    the view id
     * @param subViewId the sub view id
     * @param sliderId  the slider id
     * @param labelId   the label id
     * @param labelText the label text
     * @return the servo controller
     */
    private FloidServoController setupServoController(int command, int target, int subTarget, int viewId, int subViewId, int sliderId, int labelId, String labelText) {
        return new FloidServoController(this, command, target, subTarget, viewId, subViewId, sliderId, labelId, labelText);
    }

    /**
     * Sets declination controller.
     *
     * @param viewId                the view id
     * @param subViewId             the sub view id
     * @param labelText             the label text
     * @param declinationEditTextId the declination edit text id
     * @return the declination controller
     */
    @SuppressWarnings("SameParameterValue")
    private FloidDeclinationController setupDeclinationController(int viewId, int subViewId, String labelText, int declinationEditTextId) {
        return new FloidDeclinationController(this, viewId, subViewId, labelText, getResources(), declinationEditTextId);
    }

    /**
     * Sets pyr emulation controller.
     *
     * @param pyrEmulationContainerViewId the pyr emulation container view id
     * @return the pyr emulation controller
     */
    @SuppressWarnings("SameParameterValue")
    private FloidIMUEmulationController setupPyrEmulationController(int pyrEmulationContainerViewId) {
        return new FloidIMUEmulationController(this, mFloidDroidMessageHandler, pyrEmulationContainerViewId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.options_menu, menu);
        mOptionsMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int newMenuItemId = item.getItemId();
        View currentViewContainer = null;
        View newViewContainer = null;
        if (newMenuItemId == R.id.action_faiglelabs) {
            String url = "http://www.faiglelabs.com";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
            return true;
        }
        // Do nothing
        if (mCurrentMenuItemId == R.id.action_floid) {
            currentViewContainer = mFloidContainer;
        } else if (mCurrentMenuItemId == R.id.action_droid) {
            currentViewContainer = mDroidContainer;
        } else if (mCurrentMenuItemId == R.id.action_parameters) {
            currentViewContainer = mParametersContainer;
        } else if (mCurrentMenuItemId == R.id.action_pan_tilt) {
            currentViewContainer = mPanTiltsContainer;
        } else if (mCurrentMenuItemId == R.id.action_helis) {
            currentViewContainer = mHelisContainer;
        } else if (mCurrentMenuItemId == R.id.action_collective_cyclic) {
            currentViewContainer = mModelCollectiveCyclicsContainer;
        } else if (mCurrentMenuItemId == R.id.action_model_status) {
            currentViewContainer = mModelStatusContainer;
        } else if (mCurrentMenuItemId == R.id.action_pyr_emulation) {
            currentViewContainer = mPyrEmulationContainer;
        } else if (mCurrentMenuItemId == R.id.action_model) {
            currentViewContainer = mModelContainer;
        } else if (mCurrentMenuItemId == R.id.action_debug) {
            currentViewContainer = mDebugContainer;
        } else if (mCurrentMenuItemId == R.id.action_goals) {
            currentViewContainer = mGoalsContainer;
        } else if (mCurrentMenuItemId == R.id.action_about) {
            currentViewContainer = mAboutContainer;
        } else if (mCurrentMenuItemId == R.id.action_imaging) {
            currentViewContainer = mImagingContainer;
        }
        // Do nothing
        if (newMenuItemId == R.id.action_floid) {
            newViewContainer = mFloidContainer;
        } else if (newMenuItemId == R.id.action_droid) {
            newViewContainer = mDroidContainer;
        } else if (newMenuItemId == R.id.action_parameters) {
            newViewContainer = mParametersContainer;
        } else if (newMenuItemId == R.id.action_helis) {
            newViewContainer = mHelisContainer;
        } else if (newMenuItemId == R.id.action_model) {
            newViewContainer = mModelContainer;
        } else if (newMenuItemId == R.id.action_pan_tilt) {
            newViewContainer = mPanTiltsContainer;
        } else if (newMenuItemId == R.id.action_collective_cyclic) {
            newViewContainer = mModelCollectiveCyclicsContainer;
        } else if (newMenuItemId == R.id.action_model_status) {
            newViewContainer = mModelStatusContainer;
        } else if (newMenuItemId == R.id.action_pyr_emulation) {
            newViewContainer = mPyrEmulationContainer;
        } else if (newMenuItemId == R.id.action_debug) {
            newViewContainer = mDebugContainer;
        } else if (newMenuItemId == R.id.action_goals) {
            newViewContainer = mGoalsContainer;
        } else if (newMenuItemId == R.id.action_imaging) {
            newViewContainer = mImagingContainer;
        } else if (newMenuItemId == R.id.action_about) {
            newViewContainer = mAboutContainer;
        } else if (newMenuItemId == R.id.action_quit) {
            if (mFloidActivityUseLogger) Log.d(TAG, "QUITTING!");
            quitting = true;
            finish();
        }
        if (newViewContainer != null) {
            if (currentViewContainer != null) {
                currentViewContainer.setVisibility(View.GONE);
            }
            newViewContainer.setVisibility(View.VISIBLE);
            mCurrentMenuItemId = newMenuItemId;
        }
        return true;
    }

    /**
     * Fill out the additional status text view in the interface
     */
    private void updateAdditionalStatus() {
        StringBuilder additionalStatusStringBuilder = new StringBuilder();
        for(int i=0; i<AdditionalStatus.getSize(); ++i) {
            additionalStatusStringBuilder.append(AdditionalStatus.getLabel(i)).append(": ").append(additionalStatus.get(i));
            // Add new line if we need one:
            if(i<AdditionalStatus.getSize()-1) {
                additionalStatusStringBuilder.append("\n");
            }
        }
        additionalStatusTextView.setText(additionalStatusStringBuilder.toString());
    }

    private void updateCommStatusBorders(int item, int status) {
        FrameLayout frameLayout = null;
        // Logic:
        switch(item) {
            case COMM_FLOID: // floid:
                frameLayout = mFloidBatteryIndicatorFrameLayout;
                break;
            case COMM_DROID:
                frameLayout = mDroidBatteryIndicatorFrameLayout;
        }
        if(frameLayout!=null) {
            switch(status) {
                case COMM_GOOD:
                    frameLayout.setBackground(mBorderDrawable);
                    break;
                case COMM_WARNING:
                    frameLayout.setBackground(mWarningBorderDrawable);
                    break;
                case COMM_ERROR:
                    frameLayout.setBackground(mErrorBorderDrawable);
                    break;
            }
        }

    }
    /**
     * Sets battery status indicator.
     *
     * @param batteryIndicatorStatusTextView the battery indicator status text view
     * @param batteryIndicatorLinearLayout   the battery indicator linear layout
     * @param value                          the value
     * @param minimum                        the minimum
     * @param low                            the low
     * @param medium                         the medium
     * @param maximum                        the maximum
     * @param percent                        the percent
     * @param postfix                        the string to postfix the output
     */
    @SuppressLint("SetTextI18n")
    private void setBatteryStatusIndicator(TextView batteryIndicatorStatusTextView, LinearLayout batteryIndicatorLinearLayout, double value, double minimum, double low, double medium, double maximum, boolean percent, String postfix) {
        if (value < 0) {
            batteryIndicatorStatusTextView.setText("N/A");
            batteryIndicatorLinearLayout.setBackgroundColor(0xff222222);
            ViewGroup.LayoutParams params = batteryIndicatorLinearLayout.getLayoutParams();
            params.width = 0;
            batteryIndicatorLinearLayout.setLayoutParams(params);
//            Log.d("BATTERY", "SET BACKGROUND COLOR TO 0xff22");
            return;
        }
        // Otherwise set the value:
        if (percent) {
            batteryIndicatorStatusTextView.setText(String.format(Locale.getDefault(), "%d%s", (int) value, postfix));
        } else {
            batteryIndicatorStatusTextView.setText(mShortDecimalFormat.format(value) + postfix);
        }
        int valueScale = Math.min(Math.max((int) ((100 * (value - minimum)) / (maximum - minimum)), 0), 100);
        ViewGroup.LayoutParams params = batteryIndicatorLinearLayout.getLayoutParams();
        params.width = (int) ((valueScale * mDisplayMetrics.density) + 0.5);
        batteryIndicatorLinearLayout.setLayoutParams(params);

        if (value <= low) {
            // Red:
            batteryIndicatorLinearLayout.setBackgroundColor(mBatteryLowColor);
            return;
        }
        if (value <= medium) {
            // Yellow:
            batteryIndicatorLinearLayout.setBackgroundColor(mBatteryMediumColor);
            return;
        }
        // Green:
        batteryIndicatorLinearLayout.setBackgroundColor(mBatteryHighColor);
    }

    /**
     * Add toast help.
     *
     * @param textView   the text view
     * @param helpString the help string
     * @param context    the context
     */
    private void addToastHelp(TextView textView, final String helpString, final Context context) {
        textView.setOnClickListener(new OnClickListener() {
                                        private final String mHelpString = helpString;

                                        @Override
                                        public void onClick(View v) {
                                            Toast toast = Toast.makeText(context, mHelpString, Toast.LENGTH_SHORT);
                                            toast.show();
                                        }
                                    }
        );
    }

    /**
     * The declination floid edit text validator callback.
     */
    private final FloidEditTextValidatorCallback mDeclinationFloidEditTextValidatorCallback = new FloidEditTextValidatorCallback() {
        @Override
        public void updateInterface() {
            mDeclinationRelayButton.setEnabled(mDeclinationEditText.getError() == null);
        }
    };
    /**
     * The model parameters floid edit text validator callback.
     */
    private final FloidEditTextValidatorCallback mModelParametersFloidEditTextValidatorCallback = new FloidEditTextValidatorCallback() {
        @Override
        public void updateInterface() {
            boolean hasError = mServerPortEditText.getError() != null;
            //                Log.e(TAG, "Server Port: " + mServerPortEditText.getError());
            if (mModelBladesLowEditText.getError() != null) {
//                Log.e(TAG, "Blades Low");
                hasError = true;
            }
            if (mModelBladesZeroEditText.getError() != null) {
//                Log.e(TAG, "Blades Zero");
                hasError = true;
            }
            if (mModelBladesHighEditText.getError() != null) {
//                Log.e(TAG, "Blades High");
                hasError = true;
            }
            if (mModelH0S0LowEditText.getError() != null) {
//                Log.e(TAG, "H0S0Low");
                hasError = true;
            }
            if (mModelH0S1LowEditText.getError() != null) {
//                Log.e(TAG, "H0S1Low");
                hasError = true;
            }
            if (mModelH0S2LowEditText.getError() != null) {
//                Log.e(TAG, "H0S2Low");
                hasError = true;
            }
            if (mModelH0S0ZeroEditText.getError() != null) {
//                Log.e(TAG, "H0S0Zero");
                hasError = true;
            }
            if (mModelH0S1ZeroEditText.getError() != null) {
//                Log.e(TAG, "H0S1Zero");
                hasError = true;
            }
            if (mModelH0S2ZeroEditText.getError() != null) {
//                Log.e(TAG, "H0S2Zero");
                hasError = true;
            }
            if (mModelH0S0HighEditText.getError() != null) {
//                Log.e(TAG, "H0S0High");
                hasError = true;
            }
            if (mModelH0S1HighEditText.getError() != null) {
//                Log.e(TAG, "H0S1High");
                hasError = true;
            }
            if (mModelH0S2HighEditText.getError() != null) {
//                Log.e(TAG, "H0S2High");
                hasError = true;
            }
            if (mModelH1S0LowEditText.getError() != null) {
//                Log.e(TAG, "H1S0Low");
                hasError = true;
            }
            if (mModelH1S1LowEditText.getError() != null) {
//                Log.e(TAG, "H1S1Low");
                hasError = true;
            }
            if (mModelH1S2LowEditText.getError() != null) {
//                Log.e(TAG, "H1S2Low");
                hasError = true;
            }
            if (mModelH1S0ZeroEditText.getError() != null) {
//                Log.e(TAG, "H1S0Zero");
                hasError = true;
            }
            if (mModelH1S1ZeroEditText.getError() != null) {
//                Log.e(TAG, "H1S1Zero");
                hasError = true;
            }
            if (mModelH1S2ZeroEditText.getError() != null) {
//                Log.e(TAG, "H1S2Zero");
                hasError = true;
            }
            if (mModelH1S0HighEditText.getError() != null) {
//                Log.e(TAG, "H1S0High");
                hasError = true;
            }
            if (mModelH1S1HighEditText.getError() != null) {
//                Log.e(TAG, "H1S1High");
                hasError = true;
            }
            if (mModelH1S2HighEditText.getError() != null) {
//                Log.e(TAG, "H1S2High");
                hasError = true;
            }
            if (mModelH2S0LowEditText.getError() != null) {
//                Log.e(TAG, "H2S0Low");
                hasError = true;
            }
            if (mModelH2S1LowEditText.getError() != null) {
//                Log.e(TAG, "H2S1Low");
                hasError = true;
            }
            if (mModelH2S2LowEditText.getError() != null) {
//                Log.e(TAG, "H2S2Low");
                hasError = true;
            }
            if (mModelH2S0ZeroEditText.getError() != null) {
//                Log.e(TAG, "H2S0Zero");
                hasError = true;
            }
            if (mModelH2S1ZeroEditText.getError() != null) {
//                Log.e(TAG, "H2S1Zero");
                hasError = true;
            }
            if (mModelH2S2ZeroEditText.getError() != null) {
//                Log.e(TAG, "H2S2Zero");
                hasError = true;
            }
            if (mModelH2S0HighEditText.getError() != null) {
//                Log.e(TAG, "H2S0High");
                hasError = true;
            }
            if (mModelH2S1HighEditText.getError() != null) {
//                Log.e(TAG, "H2S1High");
                hasError = true;
            }
            if (mModelH2S2HighEditText.getError() != null) {
//                Log.e(TAG, "H2S2High");
                hasError = true;
            }
            if (mModelH3S0LowEditText.getError() != null) {
//                Log.e(TAG, "H3S0Low");
                hasError = true;
            }
            if (mModelH3S1LowEditText.getError() != null) {
//                Log.e(TAG, "H3S1Low");
                hasError = true;
            }
            if (mModelH3S2LowEditText.getError() != null) {
//                Log.e(TAG, "H3S2Low");
                hasError = true;
            }
            if (mModelH3S0ZeroEditText.getError() != null) {
//                Log.e(TAG, "H3S0Zero");
                hasError = true;
            }
            if (mModelH3S1ZeroEditText.getError() != null) {
//                Log.e(TAG, "H3S1Zero");
                hasError = true;
            }
            if (mModelH3S2ZeroEditText.getError() != null) {
//                Log.e(TAG, "H3S2Zero");
                hasError = true;
            }
            if (mModelH3S0HighEditText.getError() != null) {
//                Log.e(TAG, "H3S0High");
                hasError = true;
            }
            if (mModelH3S1HighEditText.getError() != null) {
//                Log.e(TAG, "H3S1High");
                hasError = true;
            }
            if (mModelH3S2HighEditText.getError() != null) {
//                Log.e(TAG, "H3S2High");
                hasError = true;
            }
            if (mModelCollectiveMinEditText.getError() != null) {
//                Log.e(TAG, "Collective Min");
                hasError = true;
            }
            if (mModelCollectiveMaxEditText.getError() != null) {
//                Log.e(TAG, "Collective Max");
                hasError = true;
            }
            if (mModelCollectiveDefaultEditText.getError() != null) {
//                Log.e(TAG, "Collective Default");
                hasError = true;
            }
            if (mModelCyclicRangeEditText.getError() != null) {
//                Log.e(TAG, "Cyclic Range");
                hasError = true;
            }
            if (mModelCyclicDefaultEditText.getError() != null) {
//                Log.e(TAG, "Cyclic Default");
                hasError = true;
            }
            if (mModelESCPulseMinEditText.getError() != null) {
//                Log.e(TAG, "ESC Pulse Min");
                hasError = true;
            }
            if (mModelESCPulseMaxEditText.getError() != null) {
//                Log.e(TAG, "ESC Pulse Max");
                hasError = true;
            }
            if (mModelESCLowValueEditText.getError() != null) {
//                Log.e(TAG, "ESC Low");
                hasError = true;
            }
            if (mModelESCHighValueEditText.getError() != null) {
//                Log.e(TAG, "ESC High");
                hasError = true;
            }
            if (mModelAttackAngleMinDistanceEditText.getError() != null) {
//                Log.e(TAG, "Attack Angle Min Distance");
                hasError = true;
            }
            if (mModelAttackAngleMaxDistanceEditText.getError() != null) {
//                Log.e(TAG, "Attack Angle Max Distance");
                hasError = true;
            }
            if (mModelAttackAngleValueEditText.getError() != null) {
//                Log.e(TAG, "Attack Angle Value");
                hasError = true;
            }
            if (mModelPitchDeltaMinValueEditText.getError() != null) {
//                Log.e(TAG, "Pitch Delta Min");
                hasError = true;
            }
            if (mModelPitchDeltaMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Pitch Delta Max");
                hasError = true;
            }
            if (mModelPitchTargetVelocityMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Target Velocity Max");
                hasError = true;
            }
            if (mModelRollDeltaMinValueEditText.getError() != null) {
//                Log.e(TAG, "Roll Delta Min");
                hasError = true;
            }
            if (mModelRollDeltaMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Roll Delta Max");
                hasError = true;
            }
            if (mModelRollTargetVelocityMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Target Velocity Max");
                hasError = true;
            }
            if (mModelAltitudeToTargetMinValueEditText.getError() != null) {
//                Log.e(TAG, "Altitude to Target Min");
                hasError = true;
            }
            if (mModelAltitudeToTargetMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Altitude to Target Max");
                hasError = true;
            }
            if (mModelAltitudeTargetVelocityMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Altitude Target Velocity Max");
                hasError = true;
            }
            if (mModelHeadingDeltaMinValueEditText.getError() != null) {
//                Log.e(TAG, "Heading Delta Min");
                hasError = true;
            }
            if (mModelHeadingDeltaMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Heading Delta Max");
                hasError = true;
            }
            if (mModelHeadingTargetVelocityMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Heading Target Velocity Max");
                hasError = true;
            }
            if (mModelDistanceToTargetMinValueEditText.getError() != null) {
//                Log.e(TAG, "Distance To Target Min");
                hasError = true;
            }
            if (mModelDistanceToTargetMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Distance To Target Max");
                hasError = true;
            }
            if (mModelDistanceTargetVelocityMaxValueEditText.getError() != null) {
//                Log.e(TAG, "Distance Target Velocity Max");
                hasError = true;
            }
            if (mModelOrientationMinDistanceValueEditText.getError() != null) {
//                Log.e(TAG, "Orientation Min Distance");
                hasError = true;
            }
            if (mModelAccelerationScaleMultiplierEditText.getError() != null) {
//                Log.e(TAG, "Acceleration Scale Multiplier");
                hasError = true;
            }
            if (mModelVelocityDeltaCyclicAlphaScaleEditText.getError() != null) {
//                Log.e(TAG, "Velocity Delta Cyclic Alpha");
                hasError = true;
            }
            if (mModelLiftOffTargetAltitudeDeltaEditText.getError() != null) {
//                Log.e(TAG, "Lift Off Target Altitude Delta");
                hasError = true;
            }
            if (mModelServoPulseMinEditText.getError() != null) {
//                Log.e(TAG, "Servo Pulse Min");
                hasError = true;
            }
            if (mModelServoPulseMaxEditText.getError() != null) {
//                Log.e(TAG, "Servo Pulse Max");
                hasError = true;
            }
            if (mModelServoDegreeMinEditText.getError() != null) {
//                Log.e(TAG, "Servo Degree Min");
                hasError = true;
            }
            if (mModelServoDegreeMaxEditText.getError() != null) {
//                Log.e(TAG, "Servo Degree Max");
                hasError = true;
            }
            if (mModelServoOffsetLeftEditText0.getError() != null) {
//                Log.e(TAG, "Heli 0 Servo Offset Left");
                hasError = true;
            }
            if (mModelServoOffsetRightEditText0.getError() != null) {
//                Log.e(TAG, "Heli 0 Servo Offset Right");
                hasError = true;
            }
            if (mModelServoOffsetPitchEditText0.getError() != null) {
//                Log.e(TAG, "Heli 0 Servo Offset Pitch");
                hasError = true;
            }

            if (mModelServoOffsetLeftEditText1.getError() != null) {
//                Log.e(TAG, "Heli 1 Servo Offset Left");
                hasError = true;
            }
            if (mModelServoOffsetRightEditText1.getError() != null) {
//                Log.e(TAG, "Heli 1 Servo Offset Right");
                hasError = true;
            }
            if (mModelServoOffsetPitchEditText1.getError() != null) {
//                Log.e(TAG, "Heli 1 Servo Offset Pitch");
                hasError = true;
            }
            if (mModelServoOffsetLeftEditText2.getError() != null) {
//                Log.e(TAG, "Heli 2 Servo Offset Left");
                hasError = true;
            }
            if (mModelServoOffsetRightEditText2.getError() != null) {
//                Log.e(TAG, "Heli 2 Servo Offset Right");
                hasError = true;
            }
            if (mModelServoOffsetPitchEditText2.getError() != null) {
//                Log.e(TAG, "Heli 2 Servo Offset Pitch");
                hasError = true;
            }
            if (mModelServoOffsetLeftEditText3.getError() != null) {
//                Log.e(TAG, "Heli 3 Servo Offset Left");
                hasError = true;
            }
            if (mModelServoOffsetRightEditText3.getError() != null) {
//                Log.e(TAG, "Heli 3 Servo Offset Right");
                hasError = true;
            }
            if (mModelServoOffsetPitchEditText3.getError() != null) {
//                Log.e(TAG, "Heli 3 Servo Offset Pitch");
                hasError = true;
            }
            if (mModelTargetPitchVelocityAlphaEditText.getError() != null) {
//                Log.e(TAG, "Target Pitch Velocity Alpha");
                hasError = true;
            }
            if (mModelTargetRollVelocityAlphaEditText.getError() != null) {
//                Log.e(TAG, "Target Roll Velocity Alpha");
                hasError = true;
            }
            if (mModelTargetHeadingVelocityAlphaEditText.getError() != null) {
//                Log.e(TAG, "Target Heading Velocity Alpha");
                hasError = true;
            }
            if (mModelTargetAltitudeVelocityAlphaEditText.getError() != null) {
//                Log.e(TAG, "Target Altitude Velocity Alpha");
                hasError = true;
            }
            if (mModelCyclicHeadingAlphaEditText.getError() != null) {
//                Log.e(TAG, "Cyclic Heading Alpha");
                hasError = true;
            }
            if (mModelLandModeRequiredTimeAtMinAltitudeEditText.getError() != null) {
//                Log.e(TAG, "Land Mode Required Time At Min Altitude");
                hasError = true;
            }
            if (mModelHeliStartupNumberStepsEditText.getError() != null) {
//                Log.e(TAG, "Heli Startup Number Steps");
                hasError = true;
            }
            if (mModelHeliStartupStepTickEditText.getError() != null) {
//                Log.e(TAG, "Heli Startup Step Tick");
                hasError = true;
            }
            if (mModelEscCollectiveCalcMidpointEditText.getError() != null) {
//                Log.e(TAG, "ESC Collective Calc Midpoint");
                hasError = true;
            }
            if (mModelEscCollectiveLowValueEditText.getError() != null) {
//                Log.e(TAG, "ESC Collective Low Value");
                hasError = true;
            }
            if (mModelEscCollectiveMidValueEditText.getError() != null) {
//                Log.e(TAG, "ESC Collective Mid Value");
                hasError = true;
            }
            if (mModelEscCollectiveHighValueEditText.getError() != null) {
//                Log.e(TAG, "ESC Collective High Value");
                hasError = true;
            }
            if (!hasError) {
                mModelSaveButton.setEnabled(true);
                mModelSetButton.setEnabled(true);
            } else {
                mModelSaveButton.setEnabled(false);
                mModelSetButton.setEnabled(false);
            }
        }
    };
    /**
     * The server address floid edit text validator callback.
     */
    private final FloidEditTextValidatorCallback mServerAddressFloidEditTextValidatorCallback = new FloidEditTextValidatorCallback() {
        @Override
        public void updateInterface() {
            if ((mServerAddressEditText != null) && (mServerPortEditText != null) && (mServerAddressEditText.getText().toString().length()>0)) {
                if (mServerPortEditText.getError() == null) {
                    if (mServerAddressButton != null) {
                        mServerAddressButton.setEnabled(true);
                        return;
                    }
                }
            }
            if (mServerAddressButton != null) {
                mServerAddressButton.setEnabled(false);
            }
        }
    };
    /**
     * The test goals edit text validator callback.
     */
    private final FloidEditTextValidatorCallback mTestGoalsEditTextValidatorCallback = new FloidEditTextValidatorCallback() {
        @Override
        public void updateInterface() {
            if ((mTestGoalsXEditText != null) && (mTestGoalsYEditText != null) && (mTestGoalsZEditText != null) && (mTestGoalsAEditText != null)) {
                mTestSetGoalsButton.setEnabled((mTestGoalsXEditText.getError() == null)
                        && (mTestGoalsYEditText.getError() == null)
                        && (mTestGoalsZEditText.getError() == null)
                        && (mTestGoalsAEditText.getError() == null));
            }
        }
    };

    /**
     * Turn off floid comm led.
     *
     * @param led the led
     */
    private void turnOffFloidCommLED(int led) {
        if (led >= 0 && led <= 4) {
            mFloidCommLEDImageViews[led].setImageDrawable(mFloidCommunicatorLEDOffDrawable);
        }
    }

    /**
     * Turn off server comm led.
     *
     * @param led the led
     */
    private void turnOffServerCommLED(int led) {
        if (led >= 0 && led <= 4) {
            if(mServerCommLEDStatuses[led] == SERVER_COMM_STATUS_SUCCESS) {
                mServerCommLEDImageViews[led].setImageDrawable(mServerCommunicatorLEDOffDrawable);
            } else if(mServerCommLEDStatuses[led] == SERVER_COMM_STATUS_FAIL) {
                mServerCommLEDImageViews[led].setImageDrawable(mServerCommunicatorLEDFailOffDrawable);
            } else {
                mServerCommLEDImageViews[led].setImageDrawable(mServerCommunicatorLEDUnknownOffDrawable);
            }
        }
    }

    /**
     * Turn on floid comm led.
     *
     * @param led the led
     */
    private void turnOnFloidCommLED(int led) {
        if (led >= 0 && led <= 4) {
            mFloidCommLEDImageViews[led].setImageDrawable(mFloidCommunicatorLEDOnDrawable);
        }
    }

    /**
     * Turn on server comm led.
     *
     * @param led the led
     */
    private void turnOnServerCommLED(int led) {
        if (led >= 0 && led <= 4) {
            mServerCommLEDImageViews[led].setImageDrawable(mServerCommunicatorLEDOnDrawable);
            mServerCommLEDStatuses[led] = SERVER_COMM_STATUS_SUCCESS;
        }
    }

    /**
     * Turn on server comm led to FAIL
     *
     * @param led the led
     */
    private void turnOnFailServerCommLED(int led) {
        if (led >= 0 && led <= 4) {
            mServerCommLEDImageViews[led].setImageDrawable(mServerCommunicatorLEDFailDrawable);
            mServerCommLEDStatuses[led] = SERVER_COMM_STATUS_FAIL;
        }
    }

    /**
     * Ding floid comm status.
     */
    private void dingFloidCommStatus() {
        if (mLastFloidCommStatusLED >= 0) {
            turnOffFloidCommLED(mLastFloidCommStatusLED);
        }
        ++mLastFloidCommStatusLED;
        if (mLastFloidCommStatusLED == 5) {
            mLastFloidCommStatusLED = 0;
        }
        turnOnFloidCommLED(mLastFloidCommStatusLED);
    }

    /**
     * Ding server comm status.
     */
    private void dingServerCommStatus() {
        if (mLastServerCommStatusLED >= 0) {
            turnOffServerCommLED(mLastServerCommStatusLED);
        }
        ++mLastServerCommStatusLED;
        if (mLastServerCommStatusLED == 5) {
            mLastServerCommStatusLED = 0;
        }
        turnOnServerCommLED(mLastServerCommStatusLED);
    }

    /**
     * Fail server comm status.
     */
    private void failServerCommStatus() {
        if (mLastServerCommStatusLED >= 0) {
            turnOffServerCommLED(mLastServerCommStatusLED);
        }
        ++mLastServerCommStatusLED;
        if (mLastServerCommStatusLED == 5) {
            mLastServerCommStatusLED = 0;
        }
        turnOnFailServerCommLED(mLastServerCommStatusLED);
    }

    /**
     * Make model parameters 1 command.
     *
     * @param floidModelParameters1OutgoingCommand the floid model parameters 1 outgoing command
     */
    public void makeModelParameters1Command(FloidOutgoingCommand floidModelParameters1OutgoingCommand) {
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_LOW, Float.parseFloat(mModelBladesLowEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_ZERO, Float.parseFloat(mModelBladesZeroEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_HIGH, Float.parseFloat(mModelBladesHighEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_LOW, Float.parseFloat(mModelH0S0LowEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_LOW, Float.parseFloat(mModelH0S1LowEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_LOW, Float.parseFloat(mModelH0S2LowEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_LOW, Float.parseFloat(mModelH1S0LowEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_LOW, Float.parseFloat(mModelH1S1LowEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_LOW, Float.parseFloat(mModelH1S2LowEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_ZERO, Float.parseFloat(mModelH0S0ZeroEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_ZERO, Float.parseFloat(mModelH0S1ZeroEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_ZERO, Float.parseFloat(mModelH0S2ZeroEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_ZERO, Float.parseFloat(mModelH1S0ZeroEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_ZERO, Float.parseFloat(mModelH1S1ZeroEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_ZERO, Float.parseFloat(mModelH1S2ZeroEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_HIGH, Float.parseFloat(mModelH0S0HighEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_HIGH, Float.parseFloat(mModelH0S1HighEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_HIGH, Float.parseFloat(mModelH0S2HighEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_HIGH, Float.parseFloat(mModelH1S0HighEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_HIGH, Float.parseFloat(mModelH1S1HighEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_HIGH, Float.parseFloat(mModelH1S2HighEditText.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_LEFT, Float.parseFloat(mModelServoOffsetLeftEditText0.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_RIGHT, Float.parseFloat(mModelServoOffsetRightEditText0.getText().toString()));
        floidModelParameters1OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_PITCH, Float.parseFloat(mModelServoOffsetPitchEditText0.getText().toString()));
    }

    /**
     * Make model parameters 2 command.
     *
     * @param floidModelParameters2OutgoingCommand the floid model parameters 2 outgoing command
     */
    public void makeModelParameters2Command(FloidOutgoingCommand floidModelParameters2OutgoingCommand) {
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_LOW, Float.parseFloat(mModelH2S0LowEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_LOW, Float.parseFloat(mModelH2S1LowEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_LOW, Float.parseFloat(mModelH2S2LowEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_LOW, Float.parseFloat(mModelH3S0LowEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_LOW, Float.parseFloat(mModelH3S1LowEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_LOW, Float.parseFloat(mModelH3S2LowEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_ZERO, Float.parseFloat(mModelH2S0ZeroEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_ZERO, Float.parseFloat(mModelH2S1ZeroEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_ZERO, Float.parseFloat(mModelH2S2ZeroEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_ZERO, Float.parseFloat(mModelH3S0ZeroEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_ZERO, Float.parseFloat(mModelH3S1ZeroEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_ZERO, Float.parseFloat(mModelH3S2ZeroEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_HIGH, Float.parseFloat(mModelH2S0HighEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_HIGH, Float.parseFloat(mModelH2S1HighEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_HIGH, Float.parseFloat(mModelH2S2HighEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_HIGH, Float.parseFloat(mModelH3S0HighEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_HIGH, Float.parseFloat(mModelH3S1HighEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_HIGH, Float.parseFloat(mModelH3S2HighEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_MIDPOINT, Float.parseFloat(mModelEscCollectiveCalcMidpointEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_LOW, Float.parseFloat(mModelEscCollectiveLowValueEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_MID, Float.parseFloat(mModelEscCollectiveMidValueEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_HIGH, Float.parseFloat(mModelEscCollectiveHighValueEditText.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_LEFT, Float.parseFloat(mModelServoOffsetLeftEditText1.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_RIGHT, Float.parseFloat(mModelServoOffsetRightEditText1.getText().toString()));
        floidModelParameters2OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_PITCH, Float.parseFloat(mModelServoOffsetPitchEditText1.getText().toString()));
    }

    /**
     * Make model parameters 3 command.
     *
     * @param floidModelParameters3OutgoingCommand the floid model parameters 3 outgoing command
     */
    public void makeModelParameters3Command(FloidOutgoingCommand floidModelParameters3OutgoingCommand) {
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MIN, Float.parseFloat(mModelCollectiveMinEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MAX, Float.parseFloat(mModelCollectiveMaxEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_DEFAULT, Float.parseFloat(mModelCollectiveDefaultEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_RANGE, Float.parseFloat(mModelCyclicRangeEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_DEFAULT, Float.parseFloat(mModelCyclicDefaultEditText.getText().toString()));
        // Shorts x 3:
        floidModelParameters3OutgoingCommand.setShortToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_TYPE, (short) mModelESCTypeSpinner.getSelectedItemPosition());
        floidModelParameters3OutgoingCommand.setShortToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MIN, Short.parseShort(mModelESCPulseMinEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setShortToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MAX, Short.parseShort(mModelESCPulseMaxEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_LOW_VALUE, Float.parseFloat(mModelESCLowValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_HIGH_VALUE, Float.parseFloat(mModelESCHighValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MIN_DISTANCE_VALUE, Float.parseFloat(mModelAttackAngleMinDistanceEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MAX_DISTANCE_VALUE, Float.parseFloat(mModelAttackAngleMaxDistanceEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_VALUE, Float.parseFloat(mModelAttackAngleValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MIN_VALUE, Float.parseFloat(mModelPitchDeltaMinValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MAX_VALUE, Float.parseFloat(mModelPitchDeltaMaxValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_TARGET_VELOCITY_MAX_VALUE, Float.parseFloat(mModelPitchTargetVelocityMaxValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MIN_VALUE, Float.parseFloat(mModelRollDeltaMinValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MAX_VALUE, Float.parseFloat(mModelRollDeltaMaxValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_TARGET_VELOCITY_MAX_VALUE, Float.parseFloat(mModelRollTargetVelocityMaxValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MIN_VALUE, Float.parseFloat(mModelAltitudeToTargetMinValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MAX_VALUE, Float.parseFloat(mModelAltitudeToTargetMaxValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TARGET_VELOCITY_MAX_VALUE, Float.parseFloat(mModelAltitudeTargetVelocityMaxValueEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setIntToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE, Short.parseShort(mModelLandModeRequiredTimeAtMinAltitudeEditText.getText().toString()));
        floidModelParameters3OutgoingCommand.setByteToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_START_MODE, (byte) mModelStartModeSpinner.getSelectedItemPosition());
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_LEFT, Float.parseFloat(mModelServoOffsetLeftEditText2.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_RIGHT, Float.parseFloat(mModelServoOffsetRightEditText2.getText().toString()));
        floidModelParameters3OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_PITCH, Float.parseFloat(mModelServoOffsetPitchEditText2.getText().toString()));
    }

    /**
     * Make model parameters 4 command.
     *
     * @param floidModelParameters4OutgoingCommand the floid model parameters 4 outgoing command
     */
    public void makeModelParameters4Command(FloidOutgoingCommand floidModelParameters4OutgoingCommand) {
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MIN_VALUE, Float.parseFloat(mModelHeadingDeltaMinValueEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MAX_VALUE, Float.parseFloat(mModelHeadingDeltaMaxValueEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_TARGET_VELOCITY_MAX_VALUE, Float.parseFloat(mModelHeadingTargetVelocityMaxValueEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MIN_VALUE, Float.parseFloat(mModelDistanceToTargetMinValueEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MAX_VALUE, Float.parseFloat(mModelDistanceToTargetMaxValueEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TARGET_VELOCITY_MAX_VALUE, Float.parseFloat(mModelDistanceTargetVelocityMaxValueEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_ORIENTATION_MIN_DISTANCE_VALUE, Float.parseFloat(mModelOrientationMinDistanceValueEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE_DELTA_VALUE, Float.parseFloat(mModelLiftOffTargetAltitudeDeltaEditText.getText().toString()));
        // Shorts x 2:
        floidModelParameters4OutgoingCommand.setShortToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MIN, Short.parseShort(mModelServoPulseMinEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setShortToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MAX, Short.parseShort(mModelServoPulseMaxEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MIN, Float.parseFloat(mModelServoDegreeMinEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MAX, Float.parseFloat(mModelServoDegreeMaxEditText.getText().toString()));
        // Bytes x 3:
        floidModelParameters4OutgoingCommand.setByteToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_LEFT, (mModelServoSignLeftToggleButton.isChecked() ? (byte) 1 : (byte) 0));
        floidModelParameters4OutgoingCommand.setByteToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_RIGHT, (mModelServoSignRightToggleButton.isChecked() ? (byte) 1 : (byte) 0));
        floidModelParameters4OutgoingCommand.setByteToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_PITCH, (mModelServoSignPitchToggleButton.isChecked() ? (byte) 1 : (byte) 0));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_LEFT, Float.parseFloat(mModelServoOffsetLeftEditText3.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_RIGHT, Float.parseFloat(mModelServoOffsetRightEditText3.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_PITCH, Float.parseFloat(mModelServoOffsetPitchEditText3.getText().toString()));
        // Both of these are unused - the real ones are DistanceTargetVelocityMax and DistanceTargetVelocityMin:
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_KEEP_STILL, Float.parseFloat("2.5"));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_FULL_SPEED, Float.parseFloat("10.0"));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_PITCH_VELOCITY_ALPHA, Float.parseFloat(mModelTargetPitchVelocityAlphaEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ROLL_VELOCITY_ALPHA, Float.parseFloat(mModelTargetRollVelocityAlphaEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_HEADING_VELOCITY_ALPHA, Float.parseFloat(mModelTargetHeadingVelocityAlphaEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_ALPHA, Float.parseFloat(mModelTargetAltitudeVelocityAlphaEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setByteToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_ROTATION_MODE, (byte) mModelRotationModeSpinner.getSelectedItemPosition());
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_CYCLIC_HEADING_ALPHA, Float.parseFloat(mModelCyclicHeadingAlphaEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setShortToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_NUMBER_STEPS, Short.parseShort(mModelHeliStartupNumberStepsEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setShortToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_STEP_TICK, Short.parseShort(mModelHeliStartupStepTickEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_VELOCITY_DELTA_CYCLIC_ALPHA, Float.parseFloat(mModelVelocityDeltaCyclicAlphaScaleEditText.getText().toString()));
        floidModelParameters4OutgoingCommand.setFloatToBuffer(FloidService.MODEL_PARAMETERS_4_PACKET_OFFSET_ACCELERATION_SCALE_MULTIPLIER, Float.parseFloat(mModelAccelerationScaleMultiplierEditText.getText().toString()));

    }

    /**
     * Make test goals command.
     *
     * @param testGoalsOutgoingCommand the test goals outgoing command
     */
    public void makeTestGoalsCommand(FloidOutgoingCommand testGoalsOutgoingCommand) {
        testGoalsOutgoingCommand.setFloatToBuffer(FloidService.TEST_GOALS_OFFSET_X, Float.parseFloat(mTestGoalsXEditText.getText().toString()));
        testGoalsOutgoingCommand.setFloatToBuffer(FloidService.TEST_GOALS_OFFSET_Y, Float.parseFloat(mTestGoalsYEditText.getText().toString()));
        testGoalsOutgoingCommand.setFloatToBuffer(FloidService.TEST_GOALS_OFFSET_Z, Float.parseFloat(mTestGoalsZEditText.getText().toString()));
        testGoalsOutgoingCommand.setFloatToBuffer(FloidService.TEST_GOALS_OFFSET_A, Float.parseFloat(mTestGoalsAEditText.getText().toString()));
    }

    /**
     * Display droid status.
     */
    private void displayDroidStatus() {
        mCurrentStatusTextView.setText(mFloidActivityFloidDroidStatus.getStatusString());
        mCurrentModeTextView.setText(mFloidActivityFloidDroidStatus.getModeString());
        mDroidStartedTextView.setText((mFloidActivityFloidDroidStatus.isDroidStarted() ? "Yes" : "No"));
        mServerConnectedTextView.setText((mFloidActivityFloidDroidStatus.isServerConnected() ? "Yes" : "No"));
        mServerErrorTextView.setText((mFloidActivityFloidDroidStatus.isServerError() ? "Yes" : "No"));
        mHelisStartedTextView.setText((mFloidActivityFloidDroidStatus.isHelisStarted() ? "Yes" : "No"));
        mLiftedOffTextView.setText((mFloidActivityFloidDroidStatus.isLiftedOff() ? "Yes" : "No"));
        mParachuteDeployedTextView.setText((mFloidActivityFloidDroidStatus.isParachuteDeployed() ? "Yes" : "No"));
        mTakingPhotoTextView.setText((mFloidActivityFloidDroidStatus.isTakingPhoto() ? "Yes" : "No"));
        mPhotoNumberTextView.setText(String.valueOf(mFloidActivityFloidDroidStatus.getPhotoNumber()));
        mTakingVideoTextView.setText((mFloidActivityFloidDroidStatus.isTakingVideo() ? "Yes" : "No"));
        mVideoNumberTextView.setText(String.valueOf(mFloidActivityFloidDroidStatus.getVideoNumber()));
        mHomePositionAcquiredTextView.setText((mFloidActivityFloidDroidStatus.isHomePositionAcquired() ? "Yes" : "No"));
        mHomePositionLatTextView.setText(String.valueOf(mFloidActivityFloidDroidStatus.getHomePositionY()));
        mHomePositionLngTextView.setText(String.valueOf(mFloidActivityFloidDroidStatus.getHomePositionX()));
        mHomePositionHeightTextView.setText(String.valueOf(mFloidActivityFloidDroidStatus.getHomePositionZ()));
        mFloidConnectedTextView.setText(mFloidActivityAccessoryConnected ? "Yes" : "No");
        // Display the battery status:
        setBatteryStatusIndicator(mDroidBatteryIndicatorStatusTextView, mDroidBatteryIndicatorLinearLayout, mFloidActivityFloidDroidStatus.getBatteryLevel(), DROID_BATTERY_INDICATOR_MINIMUM, DROID_BATTERY_INDICATOR_LOW, DROID_BATTERY_INDICATOR_MEDIUM, DROID_BATTERY_INDICATOR_MAXIMUM, true, "%");
    }

    /**
     * Display floid status.
     */
    @SuppressLint("SetTextI18n")
    private void displayFloidStatus() {
        if (mDisplayOn) {
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getLc() != mFloidActivityPreviousFloidStatus.getLc())) {
                mFloidLoopCountTextView.setText(String.valueOf(mFloidActivityFloidStatus.getLc()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getLr() != mFloidActivityPreviousFloidStatus.getLr())) {
                mFloidLoopRateTextView.setText(mNoDecimalFormat.format(mFloidActivityFloidStatus.getLr()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getFfm() != mFloidActivityPreviousFloidStatus.getFfm())) {
                mFloidMemoryTextView.setText(String.valueOf(mFloidActivityFloidStatus.getFfm()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getBm() != mFloidActivityPreviousFloidStatus.getBm())) {
                mBStatusTextView.setText(mOneDecimalFormat.format(mFloidActivityFloidStatus.getBm()) + "v");
                setBatteryStatusIndicator(mFloidBatteryIndicatorStatusTextView, mFloidBatteryIndicatorLinearLayout, mFloidActivityFloidStatus.getBm(), FLOID_BATTERY_INDICATOR_MINIMUM, FLOID_BATTERY_INDICATOR_LOW, FLOID_BATTERY_INDICATOR_MEDIUM, FLOID_BATTERY_INDICATOR_MAXIMUM, false, "v");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getB0() != mFloidActivityPreviousFloidStatus.getB0())) {
                mB0StatusTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getB0()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getB1() != mFloidActivityPreviousFloidStatus.getB1())) {
                mB1StatusTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getB1()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getB2() != mFloidActivityPreviousFloidStatus.getB2())) {
                mB2StatusTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getB2()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getB3() != mFloidActivityPreviousFloidStatus.getB3())) {
                mB3StatusTetView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getB3()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getC0() != mFloidActivityPreviousFloidStatus.getC0())) {
                mC0StatusTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getC0()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getC1() != mFloidActivityPreviousFloidStatus.getC1())) {
                mC1StatusTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getC1()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getC2() != mFloidActivityPreviousFloidStatus.getC2())) {
                mC2StatusTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getC2()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getC3() != mFloidActivityPreviousFloidStatus.getC3())) {
                mC3StatusTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getC3()));
            }
            // Payloads:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isY0() != mFloidActivityPreviousFloidStatus.isY0()) || (mFloidActivityFloidStatus.getYg0() != mFloidActivityPreviousFloidStatus.getYg0())) {
                mBay0StatusTextView.setText(mFloidActivityFloidStatus.isY0() ? "EMPTY" : "LOADED");
                mPayload0GoalStateTextView.setText(getGoalStateText(mFloidActivityFloidStatus.getYg0()));
                // We disable the button if the bay has no load:
                if (mFloidActivityFloidStatus.isY0()) {
                    // Disable the button:
                    mBay0ToggleButton.setEnabled(false);
                    mBay0ToggleButton.setChecked(false);
                } else {
                    // We have a payload:
                    // If we have a status that is not 'Not started' and is not 'Error', we disable:
                    if ((mFloidActivityFloidStatus.getYg0() == FloidService.GOAL_STATE_START) || (mFloidActivityFloidStatus.getYg0() == FloidService.GOAL_STATE_IN_PROGRESS) || (mFloidActivityFloidStatus.getYg0() == FloidService.GOAL_STATE_COMPLETE_SUCCESS)) {
                        mBay0ToggleButton.setEnabled(false);
                        mBay0ToggleButton.setChecked(false);
                    } else {
                        // We either have not tried or we had an error and will allow a retry:
                        mBay0ToggleButton.setEnabled(true);
                        mBay0ToggleButton.setChecked(true);
                    }
                }
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isY1() != mFloidActivityPreviousFloidStatus.isY1()) || (mFloidActivityFloidStatus.getYg1() != mFloidActivityPreviousFloidStatus.getYg1())) {
                mBay1StatusTextView.setText(mFloidActivityFloidStatus.isY1() ? "EMPTY" : "LOADED");
                mPayload1GoalStateTextView.setText(getGoalStateText(mFloidActivityFloidStatus.getYg1()));
                // We disable the button if the bay has no load:
                if (mFloidActivityFloidStatus.isY1()) {
                    // Disable the button:
                    mBay1ToggleButton.setEnabled(false);
                    mBay1ToggleButton.setChecked(false);
                } else {
                    // We have a payload:
                    // If we have a status that isn't 'Not started' and isn't 'error', we disable:
                    if ((mFloidActivityFloidStatus.getYg1() == FloidService.GOAL_STATE_START) || (mFloidActivityFloidStatus.getYg1() == FloidService.GOAL_STATE_IN_PROGRESS) || (mFloidActivityFloidStatus.getYg1() == FloidService.GOAL_STATE_COMPLETE_SUCCESS)) {
                        mBay1ToggleButton.setEnabled(false);
                        mBay1ToggleButton.setChecked(false);
                    } else {
                        // We either have not tried or we had an error and will allow a retry:
                        mBay1ToggleButton.setEnabled(true);
                        mBay1ToggleButton.setChecked(true);
                    }
                }
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isY2() != mFloidActivityPreviousFloidStatus.isY2()) || (mFloidActivityFloidStatus.getYg2() != mFloidActivityPreviousFloidStatus.getYg2())) {
                mBay2StatusTextView.setText(mFloidActivityFloidStatus.isY2() ? "EMPTY" : "LOADED");
                mPayload2GoalStateTextView.setText(getGoalStateText(mFloidActivityFloidStatus.getYg2()));
                // We disable the button if the bay has no load:
                if (mFloidActivityFloidStatus.isY2()) {
                    // Disable the button:
                    mBay2ToggleButton.setEnabled(false);
                    mBay2ToggleButton.setChecked(false);
                } else {
                    // We have a payload:
                    // If we have a status that isn't 'Not started' and isn't 'error's, we disable:
                    if ((mFloidActivityFloidStatus.getYg2() == FloidService.GOAL_STATE_START) || (mFloidActivityFloidStatus.getYg2() == FloidService.GOAL_STATE_IN_PROGRESS) || (mFloidActivityFloidStatus.getYg2() == FloidService.GOAL_STATE_COMPLETE_SUCCESS)) {
                        mBay2ToggleButton.setEnabled(false);
                        mBay2ToggleButton.setChecked(false);
                    } else {
                        // We either have not tried or we had an error and will allow a retry:
                        mBay2ToggleButton.setEnabled(true);
                        mBay2ToggleButton.setChecked(true);
                    }
                }
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isY3() != mFloidActivityPreviousFloidStatus.isY3()) || (mFloidActivityFloidStatus.getYg3() != mFloidActivityPreviousFloidStatus.getYg3())) {
                mBay3StatusTextView.setText(mFloidActivityFloidStatus.isY3() ? "EMPTY" : "LOADED");
                mPayload3GoalStateTextView.setText(getGoalStateText(mFloidActivityFloidStatus.getYg3()));
                // We disable the button if the bay has no load:
                if (mFloidActivityFloidStatus.isY3()) {
                    // Disable the button:
                    mBay3ToggleButton.setEnabled(false);
                    mBay3ToggleButton.setChecked(false);
                } else {
                    // We have a payload:
                    // If we have a status that isn't 'Not started' and isn't 'error', we disable:
                    if ((mFloidActivityFloidStatus.getYg3() == FloidService.GOAL_STATE_START) || (mFloidActivityFloidStatus.getYg3() == FloidService.GOAL_STATE_IN_PROGRESS) || (mFloidActivityFloidStatus.getYg3() == FloidService.GOAL_STATE_COMPLETE_SUCCESS)) {
                        mBay3ToggleButton.setEnabled(false);
                        mBay3ToggleButton.setChecked(false);
                    } else {
                        // We either have not tried or we had an error and will allow a retry:
                        mBay3ToggleButton.setEnabled(true);
                        mBay3ToggleButton.setChecked(true);
                    }
                }
            }
            if ((mFloidActivityPreviousFloidStatus == null) || ((mFloidActivityFloidStatus.getYn0() <= FloidService.NM_CONTRACTED_LEVEL) != (mFloidActivityPreviousFloidStatus.getYn0() <= FloidService.NM_CONTRACTED_LEVEL))) {
                mNm0StatusTextView.setText(mFloidActivityFloidStatus.getYn0() <= FloidService.NM_CONTRACTED_LEVEL ? "CLOSED" : "OPEN");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || ((mFloidActivityFloidStatus.getYn1() <= FloidService.NM_CONTRACTED_LEVEL) != (mFloidActivityPreviousFloidStatus.getYn1() <= FloidService.NM_CONTRACTED_LEVEL))) {
                mNm1StatusTextView.setText(mFloidActivityFloidStatus.getYn1() <= FloidService.NM_CONTRACTED_LEVEL ? "CLOSED" : "OPEN");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || ((mFloidActivityFloidStatus.getYn2() <= FloidService.NM_CONTRACTED_LEVEL) != (mFloidActivityPreviousFloidStatus.getYn2() <= FloidService.NM_CONTRACTED_LEVEL))) {
                mNm2StatusTextView.setText(mFloidActivityFloidStatus.getYn2() <= FloidService.NM_CONTRACTED_LEVEL ? "CLOSED" : "OPEN");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || ((mFloidActivityFloidStatus.getYn3() <= FloidService.NM_CONTRACTED_LEVEL) != (mFloidActivityPreviousFloidStatus.getYn3() <= FloidService.NM_CONTRACTED_LEVEL))) {
                mNm3StatusTextView.setText(mFloidActivityFloidStatus.getYn3() <= FloidService.NM_CONTRACTED_LEVEL ? "CLOSED" : "OPEN");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getCa() != mFloidActivityPreviousFloidStatus.getCa())) {
                mAuxCurrentTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getCa()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getBa() != mFloidActivityPreviousFloidStatus.getBa())) {
                mAuxBatteryTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getBa()));
            }
            // PYR:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPh() != mFloidActivityPreviousFloidStatus.getPh())) {
                mPyrHeadingTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPh()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPhs() != mFloidActivityPreviousFloidStatus.getPhs())) {
                mPyrHeadingSmoothedTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPhs()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPhv() != mFloidActivityPreviousFloidStatus.getPhv())) {
                mPyrHeadingVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPhv()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPz() != mFloidActivityPreviousFloidStatus.getPz())) {
                mPyrHeightTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPz()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPzs() != mFloidActivityPreviousFloidStatus.getPzs())) {
                mPyrHeightSmoothedTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPzs()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPt() != mFloidActivityPreviousFloidStatus.getPt())) {
                mPyrTempTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPt()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getKc() != mFloidActivityPreviousFloidStatus.getKc())) {
                mPyrDeclinationTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getKc()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPp() != mFloidActivityPreviousFloidStatus.getPp())) {
                mPyrPitchTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPp()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPps() != mFloidActivityPreviousFloidStatus.getPps())) {
                mPyrPitchSmoothedTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPps()));
            }
/*
                if((mFloidActivityPreviousFloidStatus==null) || ( mFloidActivityFloidStatus.getPy() != mFloidActivityPreviousFloidStatus.getPy() ))
                {
                    mPyrYawTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPy()));
                }
                if((mFloidActivityPreviousFloidStatus==null) || ( mFloidActivityFloidStatus.getPys() != mFloidActivityPreviousFloidStatus.getPys() ))
                {
                    mPyrYawSmoothedTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPys()));
                }
*/
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPr() != mFloidActivityPreviousFloidStatus.getPr())) {
                mPyrRollTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPr()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPrs() != mFloidActivityPreviousFloidStatus.getPrs())) {
                mPyrRollSmoothedTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPrs()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPpv() != mFloidActivityPreviousFloidStatus.getPpv())) {
                mPyrPitchVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPpv()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPrv() != mFloidActivityPreviousFloidStatus.getPrv())) {
                mPyrRollVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPrv()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPzv() != mFloidActivityPreviousFloidStatus.getPzv())) {
                mPyrHeightVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getPzv()));
            }
            // IMU algorithm status text:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getIas() != mFloidActivityPreviousFloidStatus.getIas())) {
               String algorithmStatusText = "";
               algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
                       (mFloidActivityFloidStatus.getIas() & FloidStatus.IMU_ALGORITHM_STANDBY) > 0,
                       FloidStatus.IMU_ALGORITHM_STANDBY_TEXT,
                       FloidStatus.IMU_ALGORITHM_STANDBY_FALSE_TEXT);
                algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
                        (mFloidActivityFloidStatus.getIas() & FloidStatus.IMU_ALGORITHM_SLOW) > 0,
                        FloidStatus.IMU_ALGORITHM_SLOW_TEXT,
                        FloidStatus.IMU_ALGORITHM_SLOW_FALSE_TEXT);
                algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
                        (mFloidActivityFloidStatus.getIas() & FloidStatus.IMU_ALGORITHM_STILL) > 0,
                        FloidStatus.IMU_ALGORITHM_STILL_TEXT,
                        FloidStatus.IMU_ALGORITHM_STILL_FALSE_TEXT);
                algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
                        (mFloidActivityFloidStatus.getIas() & FloidStatus.IMU_ALGORITHM_STABLE) > 0,
                        FloidStatus.IMU_ALGORITHM_STABLE_TEXT,
                        FloidStatus.IMU_ALGORITHM_STABLE_FALSE_TEXT);
                algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
                        (mFloidActivityFloidStatus.getIas() & FloidStatus.IMU_ALGORITHM_MAG_TRANSIENT) > 0,
                        FloidStatus.IMU_ALGORITHM_MAG_TRANSIENT_TEXT,
                        FloidStatus.IMU_ALGORITHM_MAG_TRANSIENT_FALSE_TEXT);
                algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
                        (mFloidActivityFloidStatus.getIas() & FloidStatus.IMU_ALGORITHM_UNRELIABLE) > 0,
                        FloidStatus.IMU_ALGORITHM_UNRELIABLE_TEXT,
                        FloidStatus.IMU_ALGORITHM_UNRELIABLE_FALSE_TEXT);
                algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
                        (mFloidActivityFloidStatus.getIas() & FloidStatus.IMU_ALGORITHM_WS_PARAMETERS_SAVED) > 0,
                        FloidStatus.IMU_ALGORITHM_WS_PARAMETERS_SAVED_TEXT,
                        FloidStatus.IMU_ALGORITHM_WS_PARAMETERS_SAVED_FALSE_TEXT);
                algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
                        (mFloidActivityFloidStatus.getIas() & FloidStatus.IMU_ALGORITHM_WS_PARAMETERS_CLEARED) > 0,
                        FloidStatus.IMU_ALGORITHM_WS_PARAMETERS_CLEARED_TEXT,
                        FloidStatus.IMU_ALGORITHM_WS_PARAMETERS_CLEARED_FALSE_TEXT);
                algorithmStatusText = addIMUAlgorithmStatusText(algorithmStatusText,
                        (mFloidActivityFloidStatus.getIas() & FloidStatus.IMU_ALGORITHM_WS_PARAMETERS_LOADED) > 0,
                        FloidStatus.IMU_ALGORITHM_WS_PARAMETERS_LOADED_TEXT,
                        FloidStatus.IMU_ALGORITHM_WS_PARAMETERS_LOADED_FALSE_TEXT);
                mImuAlgorithmStatusTextView.setText(algorithmStatusText);
            }
            // GPS:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJt() != mFloidActivityPreviousFloidStatus.getJt())) {
                mGpsTimeTextView.setText(String.valueOf(mFloidActivityFloidStatus.getJt()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJx() != mFloidActivityPreviousFloidStatus.getJx())) {
                mGpsXDegreesDecimalTextView.setText(mLongDecimalFormat.format(mFloidActivityFloidStatus.getJx()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJy() != mFloidActivityPreviousFloidStatus.getJy())) {
                mGpsYDegreesDecimalTextView.setText(mLongDecimalFormat.format(mFloidActivityFloidStatus.getJy()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJf() != mFloidActivityPreviousFloidStatus.getJf())) {
                mGpsFixQualityTextView.setText(String.valueOf(mFloidActivityFloidStatus.getJf()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJm() != mFloidActivityPreviousFloidStatus.getJm())) {
                mGpsZMetersTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getJm()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJz() != mFloidActivityPreviousFloidStatus.getJz())) {
                mGpsZFeetTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getJz()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJh() != mFloidActivityPreviousFloidStatus.getJh())) {
                mGpsHDOPTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getJh()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJs() != mFloidActivityPreviousFloidStatus.getJs())) {
                mGpsSatsTextView.setText(String.valueOf(mFloidActivityFloidStatus.getJs()));
            }
            // Servos:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS00() != mFloidActivityPreviousFloidStatus.getS00())) {
                mServo0LTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS00()));
                mL0ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignLeft() > 0 ? mFloidActivityFloidStatus.getS00() : (1.0 - mFloidActivityFloidStatus.getS00())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS01() != mFloidActivityPreviousFloidStatus.getS01())) {
                mServo0RTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS01()));
                mR0ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignRight() > 0 ? mFloidActivityFloidStatus.getS01() : (1.0 - mFloidActivityFloidStatus.getS01())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS02() != mFloidActivityPreviousFloidStatus.getS02())) {
                mServo0PTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS02()));
                mP0ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignPitch() > 0 ? mFloidActivityFloidStatus.getS02() : (1.0 - mFloidActivityFloidStatus.getS02())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS10() != mFloidActivityPreviousFloidStatus.getS10())) {
                mServo1LTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS10()));
                mL1ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignLeft() > 0 ? mFloidActivityFloidStatus.getS10() : (1.0 - mFloidActivityFloidStatus.getS10())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS11() != mFloidActivityPreviousFloidStatus.getS11())) {
                mServo1RTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS11()));
                mR1ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignRight() > 0 ? mFloidActivityFloidStatus.getS11() : (1.0 - mFloidActivityFloidStatus.getS11())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS12() != mFloidActivityPreviousFloidStatus.getS12())) {
                mServo1PTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS12()));
                mP1ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignPitch() > 0 ? mFloidActivityFloidStatus.getS12() : (1.0 - mFloidActivityFloidStatus.getS12())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS20() != mFloidActivityPreviousFloidStatus.getS20())) {
                mServo2LTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS20()));
                mL2ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignLeft() > 0 ? mFloidActivityFloidStatus.getS20() : (1.0 - mFloidActivityFloidStatus.getS20())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS21() != mFloidActivityPreviousFloidStatus.getS21())) {
                mServo2RTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS21()));
                mR2ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignRight() > 0 ? mFloidActivityFloidStatus.getS21() : (1.0 - mFloidActivityFloidStatus.getS21())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS22() != mFloidActivityPreviousFloidStatus.getS22())) {
                mServo2PTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS22()));
                mP2ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignPitch() > 0 ? mFloidActivityFloidStatus.getS22() : (1.0 - mFloidActivityFloidStatus.getS22())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS30() != mFloidActivityPreviousFloidStatus.getS30())) {
                mServo3LTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS30()));
                mL3ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignLeft() > 0 ? mFloidActivityFloidStatus.getS30() : (1.0 - mFloidActivityFloidStatus.getS30())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS31() != mFloidActivityPreviousFloidStatus.getS31())) {
                mServo3RTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS31()));
                mR3ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignRight() > 0 ? mFloidActivityFloidStatus.getS31() : (1.0 - mFloidActivityFloidStatus.getS31())) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getS32() != mFloidActivityPreviousFloidStatus.getS32())) {
                mServo3PTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getS32()));
                mP3ServoController.setProgress((int) ((mFloidActivityFloidModelParameters.getServoSignPitch() > 0 ? mFloidActivityFloidStatus.getS32() : (1.0 - mFloidActivityFloidStatus.getS32())) * 255));
            }
            // ESCs:
            // Note: Esc Type is not displays but is displayed in the floid model parameters
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getE0() != mFloidActivityPreviousFloidStatus.getE0())) {
                mEsc0ValueTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getE0()));
                mE0ServoController.setProgress((int) (mFloidActivityFloidStatus.getE0() * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getE1() != mFloidActivityPreviousFloidStatus.getE1())) {
                mEsc1ValueTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getE1()));
                mE1ServoController.setProgress((int) (mFloidActivityFloidStatus.getE1() * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getE2() != mFloidActivityPreviousFloidStatus.getE2())) {
                mEsc2ValueTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getE2()));
                mE2ServoController.setProgress((int) (mFloidActivityFloidStatus.getE2() * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getE3() != mFloidActivityPreviousFloidStatus.getE3())) {
                mEsc3ValueTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getE3()));
                mE3ServoController.setProgress((int) (mFloidActivityFloidStatus.getE3() * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getEp0() != mFloidActivityPreviousFloidStatus.getEp0())) {
                mEsc0PulseTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getEp0()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getEp1() != mFloidActivityPreviousFloidStatus.getEp1())) {
                mEsc1PulseTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getEp1()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getEp2() != mFloidActivityPreviousFloidStatus.getEp2())) {
                mEsc2PulseTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getEp2()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getEp3() != mFloidActivityPreviousFloidStatus.getEp3())) {
                mEsc3PulseTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getEp3()));
            }
            // Pan-Tilts:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT0m() != mFloidActivityPreviousFloidStatus.getT0m())) {
                mPanTilt0ModeTextView.setText(getPanTiltModeValueString(mFloidActivityFloidStatus.getT0m()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT1m() != mFloidActivityPreviousFloidStatus.getT1m())) {
                mPanTilt1ModeTextView.setText(getPanTiltModeValueString(mFloidActivityFloidStatus.getT1m()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT2m() != mFloidActivityPreviousFloidStatus.getT2m())) {
                mPanTilt2ModeTextView.setText(getPanTiltModeValueString(mFloidActivityFloidStatus.getT2m()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT3m() != mFloidActivityPreviousFloidStatus.getT3m())) {
                mPanTilt3ModeTextView.setText(getPanTiltModeValueString(mFloidActivityFloidStatus.getT3m()));
            }
            // [PAN_TILT] displayFloidStatus: ADJUST THESE VALUES ONCE THE PAN-TILT MODEL IS REDONE
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT0p() != mFloidActivityPreviousFloidStatus.getT0p())) {
                mPanTilt0PanValueTextView.setText(String.valueOf(mFloidActivityFloidStatus.getT0p()));
                mPT0PServoController.setProgress((int) (((double) mFloidActivityFloidStatus.getT0p() / 180.0) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT0t() != mFloidActivityPreviousFloidStatus.getT0t())) {
                mPanTilt0TiltValueTextView.setText(String.valueOf(mFloidActivityFloidStatus.getT0t()));
                mPT0TServoController.setProgress((int) (((double) mFloidActivityFloidStatus.getT0t() / 180.0) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT1p() != mFloidActivityPreviousFloidStatus.getT1p())) {
                mPanTilt1PanValueTextView.setText(String.valueOf(mFloidActivityFloidStatus.getT1p()));
                mPT1PServoController.setProgress((int) (((double) mFloidActivityFloidStatus.getT1p() / 180.0) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT1t() != mFloidActivityPreviousFloidStatus.getT1t())) {
                mPanTilt1TiltValueTextView.setText(String.valueOf(mFloidActivityFloidStatus.getT1t()));
                mPT1TServoController.setProgress((int) (((double) mFloidActivityFloidStatus.getT1t() / 180.0) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT2p() != mFloidActivityPreviousFloidStatus.getT2p())) {
                mPanTilt2PanValueTextView.setText(String.valueOf(mFloidActivityFloidStatus.getT2p()));
                mPT2PServoController.setProgress((int) (((double) mFloidActivityFloidStatus.getT2p() / 180.0) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT2t() != mFloidActivityPreviousFloidStatus.getT2t())) {
                mPanTilt2TiltValueTextView.setText(String.valueOf(mFloidActivityFloidStatus.getT2t()));
                mPT2TServoController.setProgress((int) (((double) mFloidActivityFloidStatus.getT2t() / 180.0) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT3p() != mFloidActivityPreviousFloidStatus.getT3p())) {
                mPanTilt3PanValueTextView.setText(String.valueOf(mFloidActivityFloidStatus.getT3p()));
                mPT3PServoController.setProgress((int) (((double) mFloidActivityFloidStatus.getT3p() / 180.0) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT3t() != mFloidActivityPreviousFloidStatus.getT3t())) {
                mPanTilt3TiltValueTextView.setText(String.valueOf(mFloidActivityFloidStatus.getT3t()));
                mPT3TServoController.setProgress((int) (((double) mFloidActivityFloidStatus.getT3t() / 180.0) * 255));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT0x() != mFloidActivityPreviousFloidStatus.getT0x())) {
                mPanTilt0TargetXTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT0x()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT0y() != mFloidActivityPreviousFloidStatus.getT0y())) {
                mPanTilt0TargetYTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT0y()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT0z() != mFloidActivityPreviousFloidStatus.getT0z())) {
                mPanTilt0TargetZTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT0z()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT1x() != mFloidActivityPreviousFloidStatus.getT1x())) {
                mPanTilt1TargetXTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT1x()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT1y() != mFloidActivityPreviousFloidStatus.getT1y())) {
                mPanTilt1TargetYTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT1y()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT1z() != mFloidActivityPreviousFloidStatus.getT1z())) {
                mPanTilt1TargetZTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT1z()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT2x() != mFloidActivityPreviousFloidStatus.getT2x())) {
                mPanTilt2TargetXTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT2x()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT2y() != mFloidActivityPreviousFloidStatus.getT2y())) {
                mPanTilt2TargetYTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT2y()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT2z() != mFloidActivityPreviousFloidStatus.getT2z())) {
                mPanTilt2TargetZTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT2z()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT3x() != mFloidActivityPreviousFloidStatus.getT3x())) {
                mPanTilt3TargetXTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT3x()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT3y() != mFloidActivityPreviousFloidStatus.getT3y())) {
                mPanTilt3TargetYTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT3y()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getT3z() != mFloidActivityPreviousFloidStatus.getT3z())) {
                mPanTilt3TargetZTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getT3z()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getSt() != mFloidActivityPreviousFloidStatus.getSt())) {
                mStatusTimeTextView.setText(String.valueOf(mFloidActivityFloidStatus.getSt()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getSn() != mFloidActivityPreviousFloidStatus.getSn())) {
                mStatusNumberTextView.setText(String.valueOf(mFloidActivityFloidStatus.getSn()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getFm() != mFloidActivityPreviousFloidStatus.getFm())) {
                mModeTextView.setText(mFloidActivityFloidStatus.getFm() == 0 ? "Test" : "Mission");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isFmf() != mFloidActivityPreviousFloidStatus.isFmf())) {
                mFollowModeTextView.setText(mFloidActivityFloidStatus.isFmf() ? "On" : "Off");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDc() != mFloidActivityPreviousFloidStatus.isDc())) {
                mCameraOnTextView.setText(mFloidActivityFloidStatus.isDc() ? "On" : "Off");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDg() != mFloidActivityPreviousFloidStatus.isDg())) {
                mGPSOnTextView.setText(mFloidActivityFloidStatus.isDg() ? "On" : "Off");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDp() != mFloidActivityPreviousFloidStatus.isDp())) {
                mPYROnTextView.setText(mFloidActivityFloidStatus.isDp() ? "On" : "Off");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDd() != mFloidActivityPreviousFloidStatus.isDd())) {
                mDesignatorOnTextView.setText(mFloidActivityFloidStatus.isDd() ? "On" : "Off");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDa() != mFloidActivityPreviousFloidStatus.isDa())) {
                mAuxOnTextView.setText(mFloidActivityFloidStatus.isDa() ? "On" : "Off");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDh() != mFloidActivityPreviousFloidStatus.isDh())) {
                mHelisOnTextView.setText(mFloidActivityFloidStatus.isDh() ? "On" : "Off");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDe() != mFloidActivityPreviousFloidStatus.isDe())) {
                mParachuteDeployed2TextView.setText(mFloidActivityFloidStatus.isDe() ? "On" : "Off");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getKd() != mFloidActivityPreviousFloidStatus.getKd())) {
                mDirectionOverGroundTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getKd()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getKv() != mFloidActivityPreviousFloidStatus.getKv())) {
                mVelocityOverGroundTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getKv()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getKa() != mFloidActivityPreviousFloidStatus.getKa())) {
                mAltitudeTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getKa()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getKw() != mFloidActivityPreviousFloidStatus.getKw())) {
                mAltitudeVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getKw()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isGhg() != mFloidActivityPreviousFloidStatus.isGhg())) {
                mHasGoalTextView.setText(mFloidActivityFloidStatus.isGhg() ? "On" : "Off");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getGid() != mFloidActivityPreviousFloidStatus.getGid())) {
                mGoalIdTextView.setText(String.valueOf(mFloidActivityFloidStatus.getGid()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getGt() != mFloidActivityPreviousFloidStatus.getGt())) {
                mGoalTicksTextView.setText(String.valueOf(mFloidActivityFloidStatus.getGt()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getGs() != mFloidActivityPreviousFloidStatus.getGs())) {
                mGoalStateTextView.setText(String.valueOf(mFloidActivityFloidStatus.getGs()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getGx() != mFloidActivityPreviousFloidStatus.getGx())) {
                mGoalTargetXTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getGx()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getGy() != mFloidActivityPreviousFloidStatus.getGy())) {
                mGoalTargetYTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getGy()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getGz() != mFloidActivityPreviousFloidStatus.getGz())) {
                mGoalTargetZTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getGz()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getGa() != mFloidActivityPreviousFloidStatus.getGa())) {
                mGoalTargetAngleTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getGa()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isGif() != mFloidActivityPreviousFloidStatus.isGif())) {
                mInFlightTextView.setText(mFloidActivityFloidStatus.isGif() ? "Yes" : "No");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isGlo() != mFloidActivityPreviousFloidStatus.isGlo())) {
                mLiftOffModeTextView.setText(mFloidActivityFloidStatus.isGlo() ? "Yes" : "No");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isGld() != mFloidActivityPreviousFloidStatus.isGld())) {
                mLandModeTextView.setText(mFloidActivityFloidStatus.isGld() ? "Yes" : "No");
            }
            // Set our states:
            // Mode:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getFm() != mFloidActivityPreviousFloidStatus.getFm())) {
                mFloidModeToggleButton.setChecked(mFloidActivityFloidStatus.getFm() != FloidStatus.FLOID_MODE_TEST);
                // Enable the goals and test interface:
                // Disable the goals and test interface:
                enableFloidTests(mFloidActivityFloidStatus.getFm() == FloidStatus.FLOID_MODE_TEST);
            }
            // LED:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDl() != mFloidActivityPreviousFloidStatus.isDl())) {
                mLedToggleButton.setChecked(mFloidActivityFloidStatus.isDl());
            }
            // GPS:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDg() != mFloidActivityPreviousFloidStatus.isDg())) {
                mGpsToggleButton.setChecked(mFloidActivityFloidStatus.isDg());
            }
            // GPS Rate:
            if (mFloidActivityFloidStatus.getJr() != mCurrentGpsRateSelection) {
                // Set it:
                mCurrentGpsRateSelection = mFloidActivityFloidStatus.getJr();
                mGpsRateSpinner.setSelection(mCurrentGpsRateSelection);
            }
            // GPS Emulation: (df)
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDf() != mFloidActivityPreviousFloidStatus.isDf())) {
                mGpsEmulateToggleButton.setChecked(mFloidActivityFloidStatus.isDf());
            }
            // GPS From Device: (dv)
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDv() != mFloidActivityPreviousFloidStatus.isDv())) {
                mGpsDeviceToggleButton.setChecked(mFloidActivityFloidStatus.isDv());
            }
            // PYR:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDp() != mFloidActivityPreviousFloidStatus.isDp())) {
                mPyrToggleButton.setChecked(mFloidActivityFloidStatus.isDp());
            }
            // PYR A/D:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isPd() != mFloidActivityPreviousFloidStatus.isPd())) {
                mPyrADToggleButton.setChecked(mFloidActivityFloidStatus.isPd());
            }
            // PYR Rate:
            if (mFloidActivityFloidStatus.getPar() != mCurrentPyrRateSelection) {
                // Set it:
                mCurrentPyrRateSelection = mFloidActivityFloidStatus.getPar();
                mPyrRateSpinner.setSelection(mCurrentPyrRateSelection);
            }
            // PYR From Device: (pv)
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isPv() != mFloidActivityPreviousFloidStatus.isPv())) {
                mPyrDeviceToggleButton.setChecked(mFloidActivityFloidStatus.isPv());
            }
            // Status Rate:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getSr() != mFloidActivityPreviousFloidStatus.getSr()) || (mFloidActivityFloidStatus.getSr() != mCurrentFloidStatusRateSelection)) {
                mFloidStatusRateSpinner.setSelection(mFloidActivityFloidStatus.getSr());
                if (mFloidActivityFloidStatus.getSr() != mCurrentFloidStatusRateSelection) {
                    mCurrentFloidStatusRateSelection = mFloidActivityFloidStatus.getSr();
                }
            }
            // Designator:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDd() != mFloidActivityPreviousFloidStatus.isDd())) {
                mDesignatorToggleButton.setChecked(mFloidActivityFloidStatus.isDd());
            }
            // AUX:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDa() != mFloidActivityPreviousFloidStatus.isDa())) {
                mAuxToggleButton.setChecked(mFloidActivityFloidStatus.isDa());
            }
            // Parachute:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isDe() != mFloidActivityPreviousFloidStatus.isDe())) {
                mParachuteToggleButton.setChecked(mFloidActivityFloidStatus.isDe());
            }
            // Heli0Esc:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isE0o() != mFloidActivityPreviousFloidStatus.isE0o())) {
                mHeli0EscToggleButton.setChecked(mFloidActivityFloidStatus.isE0o());
            }
            // Heli0Servo:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isS0o() != mFloidActivityPreviousFloidStatus.isS0o())) {
                mHeli0ServoToggleButton.setChecked(mFloidActivityFloidStatus.isS0o());
            }
            // Heli1Esc:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isE1o() != mFloidActivityPreviousFloidStatus.isE1o())) {
                mHeli1EscToggleButton.setChecked(mFloidActivityFloidStatus.isE1o());
            }
            // Heli1Servo:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isS1o() != mFloidActivityPreviousFloidStatus.isS1o())) {
                mHeli1ServoToggleButton.setChecked(mFloidActivityFloidStatus.isS1o());
            }
            // Heli2Esc:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isE2o() != mFloidActivityPreviousFloidStatus.isE2o())) {
                mHeli2EscToggleButton.setChecked(mFloidActivityFloidStatus.isE2o());
            }
            // Heli2Servo:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isS2o() != mFloidActivityPreviousFloidStatus.isS2o())) {
                mHeli2ServoToggleButton.setChecked(mFloidActivityFloidStatus.isS2o());
            }
            // Heli3Esc:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isE3o() != mFloidActivityPreviousFloidStatus.isE3o())) {
                mHeli3EscToggleButton.setChecked(mFloidActivityFloidStatus.isE3o());
            }
            // Heli3Servo:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isS3o() != mFloidActivityPreviousFloidStatus.isS3o())) {
                mHeli3ServoToggleButton.setChecked(mFloidActivityFloidStatus.isS3o());
            }
            // PanTilts
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isT0o() != mFloidActivityPreviousFloidStatus.isT0o())) {
                mPanTilt0ToggleButton.setChecked(mFloidActivityFloidStatus.isT0o());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isT1o() != mFloidActivityPreviousFloidStatus.isT1o())) {
                mPanTilt1ToggleButton.setChecked(mFloidActivityFloidStatus.isT1o());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isT2o() != mFloidActivityPreviousFloidStatus.isT2o())) {
                mPanTilt2ToggleButton.setChecked(mFloidActivityFloidStatus.isT2o());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isT3o() != mFloidActivityPreviousFloidStatus.isT3o())) {
                mPanTilt3ToggleButton.setChecked(mFloidActivityFloidStatus.isT3o());
            }
            // Debug:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVd() != mFloidActivityPreviousFloidStatus.isVd())) {
                mDebugToggleButton.setChecked(mFloidActivityFloidStatus.isVd());
            }
            // Debug All:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVa() != mFloidActivityPreviousFloidStatus.isVa())) {
                mDebugAllToggleButton.setChecked(mFloidActivityFloidStatus.isVa());
            }
            // Debug Aux:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVx() != mFloidActivityPreviousFloidStatus.isVx())) {
                mDebugAuxToggleButton.setChecked(mFloidActivityFloidStatus.isVx());
            }
            // Debug Payloads:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVb() != mFloidActivityPreviousFloidStatus.isVb())) {
                mDebugPayloadsToggleButton.setChecked(mFloidActivityFloidStatus.isVb());
            }
            // Debug Camera:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVc() != mFloidActivityPreviousFloidStatus.isVc())) {
                mDebugCameraToggleButton.setChecked(mFloidActivityFloidStatus.isVc());
            }
            // Debug Designator:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVe() != mFloidActivityPreviousFloidStatus.isVe())) {
                mDebugDesignatorToggleButton.setChecked(mFloidActivityFloidStatus.isVe());
            }
            // Debug GPS:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVg() != mFloidActivityPreviousFloidStatus.isVg())) {
                mDebugGpsToggleButton.setChecked(mFloidActivityFloidStatus.isVg());
            }
            // Debug Helis:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVh() != mFloidActivityPreviousFloidStatus.isVh())) {
                mDebugHelisToggleButton.setChecked(mFloidActivityFloidStatus.isVh());
            }
            // Debug Memory:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVm() != mFloidActivityPreviousFloidStatus.isVm())) {
                mDebugMemoryToggleButton.setChecked(mFloidActivityFloidStatus.isVm());
            }
            // Debug Pan/Tilt:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVt() != mFloidActivityPreviousFloidStatus.isVt())) {
                mDebugPanTiltToggleButton.setChecked(mFloidActivityFloidStatus.isVt());
            }
            // Debug Physics:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVy() != mFloidActivityPreviousFloidStatus.isVy())) {
                mDebugPhysicsToggleButton.setChecked(mFloidActivityFloidStatus.isVy());
            }
            // Debug Physics:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVp() != mFloidActivityPreviousFloidStatus.isVp())) {
                mDebugPyrToggleButton.setChecked(mFloidActivityFloidStatus.isVp());
            }
            // Debug Physics:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVl() != mFloidActivityPreviousFloidStatus.isVl())) {
                mDebugAltimeterToggleButton.setChecked(mFloidActivityFloidStatus.isVl());
            }
            // Serial Output:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isVs() != mFloidActivityPreviousFloidStatus.isVs())) {
                mSerialOutputToggleButton.setChecked(mFloidActivityFloidStatus.isVs());
            }
            // Altimeter:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isAi() != mFloidActivityPreviousFloidStatus.isAi())) {
                mAltimeterInitializedTextView.setText(mFloidActivityFloidStatus.isAi() ? "Yes" : "No");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isAv() != mFloidActivityPreviousFloidStatus.isAv())) {
                mAltimeterPanelLabelTextView.setBackgroundColor(mFloidActivityFloidStatus.isAv()?panelColor:warningPanelColor);
                mAltimeterAltitudeValidTextView.setText(mFloidActivityFloidStatus.isAv() ? "Yes" : "No");
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getAa() != mFloidActivityPreviousFloidStatus.getAa())) {
                mAltimeterAltitudeTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getAa()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getAd() != mFloidActivityPreviousFloidStatus.getAd())) {
                mAltimeterAltitudeDeltaTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getAd()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPg() != mFloidActivityPreviousFloidStatus.getPg())) {
                mAltimeterPyrGoodCountTextView.setText(String.valueOf(mFloidActivityFloidStatus.getPg()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPe() != mFloidActivityPreviousFloidStatus.getPe())) {
                mAltimeterPyrErrorCountTextView.setText(String.valueOf(mFloidActivityFloidStatus.getPe()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJg() != mFloidActivityPreviousFloidStatus.getJg())) {
                mAltimeterGpsGoodCountTextView.setText(String.valueOf(mFloidActivityFloidStatus.getJg()));
                if (mFloidActivityFloidStatus.getJg() >= 20) {
                    // New gpsGood count and >= 20, so let's start calculating the declination:
                    mDeclinationEditText.setText(String.valueOf(new GeomagneticField((float) mFloidActivityFloidStatus.getJyf()/* Lat:y-degrees-decimal-filtered */,
                                    (float) mFloidActivityFloidStatus.getJxf()/* Lng:x-degrees-decimal-filtered */,
                                    (float) mFloidActivityFloidStatus.getJm() /* AltInMeters:gps-z-meters (Note: could also use gps-z-meters-filtered - getJmf() ) */,
                                    System.currentTimeMillis()
                            ).getDeclination()
                            )
                    );
                }
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getAc() != mFloidActivityPreviousFloidStatus.getAc())) {
                mAltimeterGoodCountTextView.setText(String.valueOf(mFloidActivityFloidStatus.getAc()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJxf() != mFloidActivityPreviousFloidStatus.getJxf())) {
                mAltimeterGpsXDegreesDecimalFilteredTextView.setText(mLongDecimalFormat.format(mFloidActivityFloidStatus.getJxf()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJyf() != mFloidActivityPreviousFloidStatus.getJyf())) {
                mAltimeterGpsYDegreesDecimalFilteredTextView.setText(mLongDecimalFormat.format(mFloidActivityFloidStatus.getJyf()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getJmf() != mFloidActivityPreviousFloidStatus.getJmf())) {
                mAltimeterGpsZMetersFilteredTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getJmf()));
            }
            // Model:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getMt() != mFloidActivityPreviousFloidStatus.getMt())) {
                mLiftOffTargetAltitudeTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getMt()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getMd() != mFloidActivityPreviousFloidStatus.getMd())) {
                mDistanceToTargetTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getMd()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getMa() != mFloidActivityPreviousFloidStatus.getMa())) {
                mAltitudeToTargetTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getMa()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getMo() != mFloidActivityPreviousFloidStatus.getMo())) {
                mDeltaHeadingAngleToTargetTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getMo()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getMg() != mFloidActivityPreviousFloidStatus.getMg())) {
                mOrientationToGoalTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getMg()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getMk() != mFloidActivityPreviousFloidStatus.getMk())) {
                mAttackAngleTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getMk()));
            }
            // Target: -'o'
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOop() != mFloidActivityPreviousFloidStatus.getOop())) {
                mTargetOrientationPitchTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOop()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOoq() != mFloidActivityPreviousFloidStatus.getOoq())) {
                mTargetOrientationPitchDeltaTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOoq()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOor() != mFloidActivityPreviousFloidStatus.getOor())) {
                mTargetOrientationRollTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOor()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOos() != mFloidActivityPreviousFloidStatus.getOos())) {
                mTargetOrientationRollDeltaTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOos()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOpv() != mFloidActivityPreviousFloidStatus.getOpv())) {
                mTargetPitchVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOpv()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOpw() != mFloidActivityPreviousFloidStatus.getOpw())) {
                mTargetPitchVelocityDeltaTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOpw()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOrv() != mFloidActivityPreviousFloidStatus.getOrv())) {
                mTargetRollVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOrv()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOrw() != mFloidActivityPreviousFloidStatus.getOrw())) {
                mTargetRollVelocityDeltaTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOrw()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOav() != mFloidActivityPreviousFloidStatus.getOav())) {
                mTargetAltitudeVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOav()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOaw() != mFloidActivityPreviousFloidStatus.getOaw())) {
                mTargetAltitudeVelocityDeltaTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOaw()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOhv() != mFloidActivityPreviousFloidStatus.getOhv())) {
                mTargetHeadingVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOhv()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOhw() != mFloidActivityPreviousFloidStatus.getOhw())) {
                mTargetHeadingVelocityDeltaTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOhw()));
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getOv() != mFloidActivityPreviousFloidStatus.getOv())) {
                mTargetXYVelocityTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getOv()));
            }
            // Land Mode Altitude minimums and times if land mode min altitude check is on:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isLmc() != mFloidActivityPreviousFloidStatus.isLmc())
                    || (mFloidActivityFloidStatus.getLma() != mFloidActivityPreviousFloidStatus.getLma())
                    || (mFloidActivityFloidStatus.getLmt() != mFloidActivityPreviousFloidStatus.getLmt())) {
                if (mFloidActivityFloidStatus.isLmc()) {
                    mLandModeMinAltitudeTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidStatus.getLma()));
                    mLandModeMinAltitudeTimeTextView.setText(String.valueOf(mFloidActivityFloidStatus.getLmt()));
                } else {
                    // Not land mode check - set display to "N/A"
                    mLandModeMinAltitudeTextView.setText("N/A");
                    mLandModeMinAltitudeTimeTextView.setText("N/A");
                }
            }
            // Set the emulation controllers:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPh() != mFloidActivityPreviousFloidStatus.getPh())) {
                mPyrEmulationController.setHeading((float) mFloidActivityFloidStatus.getPh());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPp() != mFloidActivityPreviousFloidStatus.getPp())) {
                mPyrEmulationController.setPitch((float) mFloidActivityFloidStatus.getPp());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPr() != mFloidActivityPreviousFloidStatus.getPr())) {
                mPyrEmulationController.setRoll((float) mFloidActivityFloidStatus.getPr());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getPt() != mFloidActivityPreviousFloidStatus.getPt())) {
                mPyrEmulationController.setTemperature((float) mFloidActivityFloidStatus.getPt());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.getKa() != mFloidActivityPreviousFloidStatus.getKa())) {
                mPyrEmulationController.setAltitude((float) mFloidActivityFloidStatus.getKa());
            }
            // Run and test Modes:
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isFloidProductionMode() != mFloidActivityPreviousFloidStatus.isFloidProductionMode())) {
                mRunModeProductionToggleButton.setChecked(mFloidActivityFloidStatus.isFloidProductionMode());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isTestModePrintDebugPhysicsHeadings() != mFloidActivityPreviousFloidStatus.isTestModePrintDebugPhysicsHeadings())) {
                mTestModePrintDebugHeadingsToggleButton.setChecked(mFloidActivityFloidStatus.isTestModePrintDebugPhysicsHeadings());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isTestModeCheckPhysicsModel() != mFloidActivityPreviousFloidStatus.isTestModeCheckPhysicsModel())) {
                mTestModeCheckPhysicsModelToggleButton.setChecked(mFloidActivityFloidStatus.isTestModeCheckPhysicsModel());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isRunModePhysicsTestXY() != mFloidActivityPreviousFloidStatus.isRunModePhysicsTestXY())) {
                mRunModeTestXYToggleButton.setChecked(mFloidActivityFloidStatus.isRunModePhysicsTestXY());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isRunModePhysicsTestAltitude1() != mFloidActivityPreviousFloidStatus.isRunModePhysicsTestAltitude1())) {
                mRunModeTestAltitude1ToggleButton.setChecked(mFloidActivityFloidStatus.isRunModePhysicsTestAltitude1());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isRunModePhysicsTestAltitude2() != mFloidActivityPreviousFloidStatus.isRunModePhysicsTestAltitude2())) {
                mRunModeTestAltitude2ToggleButton.setChecked(mFloidActivityFloidStatus.isRunModePhysicsTestAltitude2());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isRunModePhysicsTestHeading1() != mFloidActivityPreviousFloidStatus.isRunModePhysicsTestHeading1())) {
                mRunModeTestHeading1ToggleButton.setChecked(mFloidActivityFloidStatus.isRunModePhysicsTestHeading1());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isRunModePhysicsTestPitch1() != mFloidActivityPreviousFloidStatus.isRunModePhysicsTestPitch1())) {
                mRunModeTestPitch1ToggleButton.setChecked(mFloidActivityFloidStatus.isRunModePhysicsTestPitch1());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isRunModePhysicsTestRoll1() != mFloidActivityPreviousFloidStatus.isRunModePhysicsTestRoll1())) {
                mRunModeTestRoll1ToggleButton.setChecked(mFloidActivityFloidStatus.isRunModePhysicsTestRoll1());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isTestModePhysicsNoXY() != mFloidActivityPreviousFloidStatus.isTestModePhysicsNoXY())) {
                mTestModeNoXYToggleButton.setChecked(mFloidActivityFloidStatus.isTestModePhysicsNoXY());
            }
            // Has Goal?
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isGhg() != mFloidActivityPreviousFloidStatus.isGhg())) {
                mGoalsToggleButton.setChecked(mFloidActivityFloidStatus.isGhg());
            }
            // Lift Off Mode?
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isGlo() != mFloidActivityPreviousFloidStatus.isGlo())) {
                mLiftOffModeToggleButton.setChecked(mFloidActivityFloidStatus.isGlo());
                mLiftOffModeToggleButton.setEnabled(!mFloidActivityFloidStatus.isGlo());
            }
            // Normal Mode?
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isGlo() != mFloidActivityPreviousFloidStatus.isGlo()) || (mFloidActivityFloidStatus.isGld() != mFloidActivityPreviousFloidStatus.isGld())) {
                mLiftOffModeToggleButton.setChecked(!mFloidActivityFloidStatus.isGlo() && !mFloidActivityFloidStatus.isGld());
                mLiftOffModeToggleButton.setEnabled(mFloidActivityFloidStatus.isGlo() || mFloidActivityFloidStatus.isGld());
            }
            // Land Off Mode?
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isGld() != mFloidActivityPreviousFloidStatus.isGld())) {
                mLiftOffModeToggleButton.setChecked(mFloidActivityFloidStatus.isGld());
                mLiftOffModeToggleButton.setEnabled(!mFloidActivityFloidStatus.isGld());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isTestModePhysicsNoAltitude() != mFloidActivityPreviousFloidStatus.isTestModePhysicsNoAltitude())) {
                mTestModeNoAltitudeToggleButton.setChecked(mFloidActivityFloidStatus.isTestModePhysicsNoAltitude());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isTestModePhysicsNoAttackAngle() != mFloidActivityPreviousFloidStatus.isTestModePhysicsNoAttackAngle())) {
                mTestModeNoAttackAngleToggleButton.setChecked(mFloidActivityFloidStatus.isTestModePhysicsNoAttackAngle());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isTestModePhysicsNoHeading() != mFloidActivityPreviousFloidStatus.isTestModePhysicsNoHeading())) {
                mTestModeNoHeadingToggleButton.setChecked(mFloidActivityFloidStatus.isTestModePhysicsNoHeading());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isTestModePhysicsNoPitch() != mFloidActivityPreviousFloidStatus.isTestModePhysicsNoPitch())) {
                mTestModeNoPitchToggleButton.setChecked(mFloidActivityFloidStatus.isTestModePhysicsNoPitch());
            }
            if ((mFloidActivityPreviousFloidStatus == null) || (mFloidActivityFloidStatus.isTestModePhysicsNoRoll() != mFloidActivityPreviousFloidStatus.isTestModePhysicsNoRoll())) {
                mTestModeNoRollToggleButton.setChecked(mFloidActivityFloidStatus.isTestModePhysicsNoRoll());
            }
        }
        // Clone the floid status (shallow - no references):
        try {
            mFloidActivityPreviousFloidStatus = mFloidActivityFloidStatus.clone();
        } catch (CloneNotSupportedException e) {
            Log.e(TAG, "Clone not supported in FloidStatus", e);
        }
    }

    private String addIMUAlgorithmStatusText(String currentText, boolean test, String trueText, String falseText) {
        if (test) {
            // Both non-empty, return both space separated:
            if (currentText.length() > 0 && trueText.length() > 0) {
                return currentText + ' ' + trueText;
            }
            // If current text is non-empty (means true text is empty):
            if (currentText.length() > 0) {
                return currentText;
            }
            // Otherwise return the true test:
            return trueText;
        } else {
            // Both non-empty, return both space separated:
            if (currentText.length() > 0 && falseText.length() > 0) {
                return currentText + ' ' + falseText;
            }
            // If current text is non-empty (means false text is empty):
            if (currentText.length() > 0) {
                return currentText;
            }
            // Otherwise return the false test:
            return falseText;
        }
    }

    /**
     * Gets goal state text.
     *
     * @param goalState the goal state
     * @return the goal state text
     */
    private String getGoalStateText(int goalState) {
        switch (goalState) {
            case FloidService.GOAL_STATE_NONE:
                return "";
            case FloidService.GOAL_STATE_START:
                return "ENGAGED";
            case FloidService.GOAL_STATE_IN_PROGRESS:
                return "OPENED";
            case FloidService.GOAL_STATE_COMPLETE_SUCCESS:
                return "DROPPED";
            case FloidService.GOAL_STATE_COMPLETE_ERROR:
                return "FAILED";
            default:
                return "UNKNOWN";
        }
    }

    /**
     * Enable / disable the floid test goals and physics tests interface
     * @param enable enable if true
     */
    private void enableFloidTests(boolean enable) {
        mGoalsToggleButton.setEnabled(enable);
        mTestSetGoalsButton.setEnabled(enable);
        mTestGoalsXEditText.setEnabled(enable);
        mTestGoalsYEditText.setEnabled(enable);
        mTestGoalsZEditText.setEnabled(enable);
        mTestGoalsAEditText.setEnabled(enable);
        mRunModeProductionToggleButton.setEnabled(enable);
        mTestModePrintDebugHeadingsToggleButton.setEnabled(enable);
        mTestModeCheckPhysicsModelToggleButton.setEnabled(enable);
        mRunModeTestXYToggleButton.setEnabled(enable);
        mRunModeTestAltitude1ToggleButton.setEnabled(enable);
        mRunModeTestAltitude2ToggleButton.setEnabled(enable);
        mRunModeTestHeading1ToggleButton.setEnabled(enable);
        mRunModeTestPitch1ToggleButton.setEnabled(enable);
        mRunModeTestRoll1ToggleButton.setEnabled(enable);
        mTestModeNoXYToggleButton.setEnabled(enable);
        mTestModeNoAltitudeToggleButton.setEnabled(enable);
        mTestModeNoAttackAngleToggleButton.setEnabled(enable);
        mTestModeNoHeadingToggleButton.setEnabled(enable);
        mTestModeNoPitchToggleButton.setEnabled(enable);
        mTestModeNoRollToggleButton.setEnabled(enable);
    }

    /**
     * Display floid model parameters.
     */
    private void displayFloidModelParameters() {
        if (mDisplayOn) {
            mModelLandModeRequiredTimeAtMinAltitudeEditText.setError(null);
            mModelAltitudeTargetVelocityMaxValueEditText.setError(null);
            mModelAltitudeToTargetMaxValueEditText.setError(null);
            mModelAltitudeToTargetMinValueEditText.setError(null);
            mModelAttackAngleMaxDistanceEditText.setError(null);
            mModelAttackAngleMinDistanceEditText.setError(null);
            mModelAttackAngleValueEditText.setError(null);
            mModelBladesHighEditText.setError(null);
            mModelBladesLowEditText.setError(null);
            mModelBladesZeroEditText.setError(null);
            mModelCollectiveDefaultEditText.setError(null);
            mModelCollectiveMaxEditText.setError(null);
            mModelCollectiveMinEditText.setError(null);
            mModelCyclicDefaultEditText.setError(null);
            mModelCyclicRangeEditText.setError(null);
            mModelDistanceTargetVelocityMaxValueEditText.setError(null);
            mModelDistanceToTargetMaxValueEditText.setError(null);
            mModelDistanceToTargetMinValueEditText.setError(null);
            mModelESCHighValueEditText.setError(null);
            mModelESCLowValueEditText.setError(null);
            mModelESCPulseMaxEditText.setError(null);
            mModelESCPulseMinEditText.setError(null);
            mModelH0S0HighEditText.setError(null);
            mModelH0S0LowEditText.setError(null);
            mModelH0S0ZeroEditText.setError(null);
            mModelH0S1HighEditText.setError(null);
            mModelH0S1LowEditText.setError(null);
            mModelH0S1ZeroEditText.setError(null);
            mModelH0S2HighEditText.setError(null);
            mModelH0S2LowEditText.setError(null);
            mModelH0S2ZeroEditText.setError(null);
            mModelH1S0HighEditText.setError(null);
            mModelH1S0LowEditText.setError(null);
            mModelH1S0ZeroEditText.setError(null);
            mModelH1S1HighEditText.setError(null);
            mModelH1S1LowEditText.setError(null);
            mModelH1S1ZeroEditText.setError(null);
            mModelH1S2HighEditText.setError(null);
            mModelH1S2LowEditText.setError(null);
            mModelH1S2ZeroEditText.setError(null);
            mModelH2S0HighEditText.setError(null);
            mModelH2S0LowEditText.setError(null);
            mModelH2S0ZeroEditText.setError(null);
            mModelH2S1HighEditText.setError(null);
            mModelH2S1LowEditText.setError(null);
            mModelH2S1ZeroEditText.setError(null);
            mModelH2S2HighEditText.setError(null);
            mModelH2S2LowEditText.setError(null);
            mModelH2S2ZeroEditText.setError(null);
            mModelH3S0HighEditText.setError(null);
            mModelH3S0LowEditText.setError(null);
            mModelH3S0ZeroEditText.setError(null);
            mModelH3S1HighEditText.setError(null);
            mModelH3S1LowEditText.setError(null);
            mModelH3S1ZeroEditText.setError(null);
            mModelH3S2HighEditText.setError(null);
            mModelH3S2LowEditText.setError(null);
            mModelH3S2ZeroEditText.setError(null);
            mModelHeadingDeltaMaxValueEditText.setError(null);
            mModelHeadingDeltaMinValueEditText.setError(null);
            mModelHeadingTargetVelocityMaxValueEditText.setError(null);
            mModelLiftOffTargetAltitudeDeltaEditText.setError(null);
            mModelOrientationMinDistanceValueEditText.setError(null);
            mModelAccelerationScaleMultiplierEditText.setError(null);
            mModelVelocityDeltaCyclicAlphaScaleEditText.setError(null);
            mModelPitchDeltaMaxValueEditText.setError(null);
            mModelPitchDeltaMinValueEditText.setError(null);
            mModelPitchTargetVelocityMaxValueEditText.setError(null);
            mModelRollDeltaMaxValueEditText.setError(null);
            mModelRollDeltaMinValueEditText.setError(null);
            mModelRollTargetVelocityMaxValueEditText.setError(null);
            mModelServoDegreeMaxEditText.setError(null);
            mModelServoDegreeMinEditText.setError(null);
            mModelServoOffsetLeftEditText0.setError(null);
            mModelServoOffsetPitchEditText0.setError(null);
            mModelServoOffsetRightEditText0.setError(null);
            mModelServoOffsetLeftEditText1.setError(null);
            mModelServoOffsetPitchEditText1.setError(null);
            mModelServoOffsetRightEditText1.setError(null);
            mModelServoOffsetLeftEditText2.setError(null);
            mModelServoOffsetPitchEditText2.setError(null);
            mModelServoOffsetRightEditText2.setError(null);
            mModelServoOffsetLeftEditText3.setError(null);
            mModelServoOffsetPitchEditText3.setError(null);
            mModelServoOffsetRightEditText3.setError(null);
            mModelServoPulseMaxEditText.setError(null);
            mModelServoPulseMinEditText.setError(null);
            mModelTargetAltitudeVelocityAlphaEditText.setError(null);
            mModelTargetHeadingVelocityAlphaEditText.setError(null);
            mModelTargetPitchVelocityAlphaEditText.setError(null);
            mModelTargetRollVelocityAlphaEditText.setError(null);
            mModelCyclicHeadingAlphaEditText.setError(null);
            mModelHeliStartupNumberStepsEditText.setError(null);
            mModelHeliStartupStepTickEditText.setError(null);
            mModelEscCollectiveCalcMidpointEditText.setError(null);
            mModelEscCollectiveLowValueEditText.setError(null);
            mModelEscCollectiveMidValueEditText.setError(null);
            mModelEscCollectiveHighValueEditText.setError(null);

            mModelLandModeRequiredTimeAtMinAltitudeEditText.setText(String.valueOf(mFloidActivityFloidModelParameters.getLandModeTimeRequiredAtMinAltitude()));
            mModelAltitudeTargetVelocityMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getAltitudeTargetVelocityMaxValue()));
            mModelAltitudeToTargetMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getAltitudeToTargetMaxValue()));
            mModelAltitudeToTargetMinValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getAltitudeToTargetMinValue()));
            mModelAttackAngleMaxDistanceEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getAttackAngleMaxDistanceValue()));
            mModelAttackAngleMinDistanceEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getAttackAngleMinDistanceValue()));
            mModelAttackAngleValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getAttackAngleValue()));
            mModelBladesHighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getBladesHigh()));
            mModelBladesLowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getBladesLow()));
            mModelBladesZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getBladesZero()));
            mModelCollectiveDefaultEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getCollectiveDefault()));
            mModelCollectiveMaxEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getCollectiveMax()));
            mModelCollectiveMinEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getCollectiveMin()));
            mModelCyclicDefaultEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getCyclicDefault()));
            mModelCyclicRangeEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getCyclicRange()));
            mModelDistanceTargetVelocityMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getDistanceTargetVelocityMaxValue()));
            mModelDistanceToTargetMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getDistanceToTargetMaxValue()));
            mModelDistanceToTargetMinValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getDistanceToTargetMinValue()));
            mModelESCHighValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getEscHighValue()));
            mModelESCLowValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getEscLowValue()));
            mModelESCPulseMaxEditText.setText(String.valueOf(mFloidActivityFloidModelParameters.getEscPulseMax()));
            mModelESCPulseMinEditText.setText(String.valueOf(mFloidActivityFloidModelParameters.getEscPulseMin()));
            mModelESCTypeSpinner.setSelection(mFloidActivityFloidModelParameters.getEscType());
            mModelH0S0HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH0S0High()));
            mModelH0S0LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH0S0Low()));
            mModelH0S0ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH0S0Zero()));
            mModelH0S1HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH0S1High()));
            mModelH0S1LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH0S1Low()));
            mModelH0S1ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH0S1Zero()));
            mModelH0S2HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH0S2High()));
            mModelH0S2LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH0S2Low()));
            mModelH0S2ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH0S2Zero()));
            mModelH1S0HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH1S0High()));
            mModelH1S0LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH1S0Low()));
            mModelH1S0ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH1S0Zero()));
            mModelH1S1HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH1S1High()));
            mModelH1S1LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH1S1Low()));
            mModelH1S1ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH1S1Zero()));
            mModelH1S2HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH1S2High()));
            mModelH1S2LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH1S2Low()));
            mModelH1S2ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH1S2Zero()));
            mModelH2S0HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH2S0High()));
            mModelH2S0LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH2S0Low()));
            mModelH2S0ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH2S0Zero()));
            mModelH2S1HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH2S1High()));
            mModelH2S1LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH2S1Low()));
            mModelH2S1ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH2S1Zero()));
            mModelH2S2HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH2S2High()));
            mModelH2S2LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH2S2Low()));
            mModelH2S2ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH2S2Zero()));
            mModelH3S0HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH3S0High()));
            mModelH3S0LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH3S0Low()));
            mModelH3S0ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH3S0Zero()));
            mModelH3S1HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH3S1High()));
            mModelH3S1LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH3S1Low()));
            mModelH3S1ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH3S1Zero()));
            mModelH3S2HighEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH3S2High()));
            mModelH3S2LowEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH3S2Low()));
            mModelH3S2ZeroEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getH3S2Zero()));
            mModelHeadingDeltaMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getHeadingDeltaMaxValue()));
            mModelHeadingDeltaMinValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getHeadingDeltaMinValue()));
            mModelHeadingTargetVelocityMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getHeadingTargetVelocityMaxValue()));
            mModelLiftOffTargetAltitudeDeltaEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getLiftOffTargetAltitudeDeltaValue()));
            mModelOrientationMinDistanceValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getOrientationMinDistanceValue()));
            mModelPitchDeltaMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getPitchDeltaMaxValue()));
            mModelPitchDeltaMinValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getPitchDeltaMinValue()));
            mModelPitchTargetVelocityMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getPitchTargetVelocityMaxValue()));
            mModelRollDeltaMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getRollDeltaMaxValue()));
            mModelRollDeltaMinValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getRollDeltaMinValue()));
            mModelRollTargetVelocityMaxValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getRollTargetVelocityMaxValue()));
            mModelServoDegreeMaxEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoDegreeMax()));
            mModelServoDegreeMinEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoDegreeMin()));
            mModelServoOffsetLeftEditText0.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetLeft0()));
            mModelServoOffsetPitchEditText0.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetPitch0()));
            mModelServoOffsetRightEditText0.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetRight0()));
            mModelServoOffsetLeftEditText1.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetLeft1()));
            mModelServoOffsetPitchEditText1.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetPitch1()));
            mModelServoOffsetRightEditText1.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetRight1()));
            mModelServoOffsetLeftEditText2.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetLeft2()));
            mModelServoOffsetPitchEditText2.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetPitch2()));
            mModelServoOffsetRightEditText2.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetRight2()));
            mModelServoOffsetLeftEditText3.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetLeft3()));
            mModelServoOffsetPitchEditText3.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetPitch3()));
            mModelServoOffsetRightEditText3.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getServoOffsetRight3()));
            mModelServoPulseMaxEditText.setText(String.valueOf(mFloidActivityFloidModelParameters.getServoPulseMax()));
            mModelServoPulseMinEditText.setText(String.valueOf(mFloidActivityFloidModelParameters.getServoPulseMin()));
            mModelServoSignLeftToggleButton.setChecked(mFloidActivityFloidModelParameters.getServoSignLeft() > 0);
            mModelServoSignRightToggleButton.setChecked(mFloidActivityFloidModelParameters.getServoSignRight() > 0);
            mModelServoSignPitchToggleButton.setChecked(mFloidActivityFloidModelParameters.getServoSignPitch() > 0);
            mModelTargetAltitudeVelocityAlphaEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getTargetAltitudeVelocityAlpha()));
            mModelTargetHeadingVelocityAlphaEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getTargetHeadingVelocityAlpha()));
            mModelTargetPitchVelocityAlphaEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getTargetPitchVelocityAlpha()));
            mModelTargetRollVelocityAlphaEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getTargetRollVelocityAlpha()));
            mModelCyclicHeadingAlphaEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getCyclicHeadingAlpha()));
            mModelHeliStartupNumberStepsEditText.setText(String.valueOf(mFloidActivityFloidModelParameters.getHeliStartupModeNumberSteps()));
            mModelHeliStartupStepTickEditText.setText(String.valueOf(mFloidActivityFloidModelParameters.getHeliStartupModeStepTick()));
            mModelRotationModeSpinner.setSelection(mFloidActivityFloidModelParameters.getRotationMode());
            mModelStartModeSpinner.setSelection(mFloidActivityFloidModelParameters.getStartMode());
            mModelEscCollectiveCalcMidpointEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getFloidModelEscCollectiveCalcMidpoint()));
            mModelEscCollectiveLowValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getFloidModelEscCollectiveLowValue()));
            mModelEscCollectiveMidValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getFloidModelEscCollectiveMidValue()));
            mModelEscCollectiveHighValueEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getFloidModelEscCollectiveHighValue()));
            mModelVelocityDeltaCyclicAlphaScaleEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getVelocityDeltaCyclicAlphaScale()));
            mModelAccelerationScaleMultiplierEditText.setText(mShortDecimalFormat.format(mFloidActivityFloidModelParameters.getAccelerationMultiplierScale()));
        }
    }

    /**
     * Display floid model status.
     */
    private void displayFloidModelStatus() {
        if (mDisplayOn) {
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaOrientationH0() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaOrientationH0())) {
                mCollectiveDeltaOrientationH0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaOrientationH0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaOrientationH1() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaOrientationH1())) {
                mCollectiveDeltaOrientationH1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaOrientationH1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaOrientationH2() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaOrientationH2())) {
                mCollectiveDeltaOrientationH2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaOrientationH2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaOrientationH3() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaOrientationH3())) {
                mCollectiveDeltaOrientationH3TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaOrientationH3()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaAltitudeH0() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaAltitudeH0())) {
                mCollectiveDeltaAltitudeH0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaAltitudeH0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaAltitudeH1() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaAltitudeH1())) {
                mCollectiveDeltaAltitudeH1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaAltitudeH1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaAltitudeH2() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaAltitudeH2())) {
                mCollectiveDeltaAltitudeH2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaAltitudeH2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaAltitudeH3() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaAltitudeH3())) {
                mCollectiveDeltaAltitudeH3TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaAltitudeH3()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaHeadingH0() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaHeadingH0())) {
                mCollectiveDeltaHeadingH0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaHeadingH0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaHeadingH1() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaHeadingH1())) {
                mCollectiveDeltaHeadingH1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaHeadingH1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaHeadingH2() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaHeadingH2())) {
                mCollectiveDeltaHeadingH2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaHeadingH2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaHeadingH3() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaHeadingH3())) {
                mCollectiveDeltaHeadingH3TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaHeadingH3()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaTotalH0() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaTotalH0())) {
                mCollectiveDeltaTotalH0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaTotalH0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaTotalH1() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaTotalH1())) {
                mCollectiveDeltaTotalH1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaTotalH1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaTotalH2() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaTotalH2())) {
                mCollectiveDeltaTotalH2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaTotalH2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveDeltaTotalH3() != mFloidActivityPreviousFloidModelStatus.getCollectiveDeltaTotalH3())) {
                mCollectiveDeltaTotalH3TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveDeltaTotalH3()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH0S0() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH0S0())) {
                mCyclicValueH0S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH0S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH0S1() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH0S1())) {
                mCyclicValueH0S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH0S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH0S2() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH0S2())) {
                mCyclicValueH0S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH0S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH1S0() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH1S0())) {
                mCyclicValueH1S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH1S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH1S1() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH1S1())) {
                mCyclicValueH1S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH1S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH1S2() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH1S2())) {
                mCyclicValueH1S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH1S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH2S0() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH2S0())) {
                mCyclicValueH2S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH2S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH2S1() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH2S1())) {
                mCyclicValueH2S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH2S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH2S2() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH2S2())) {
                mCyclicValueH2S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH2S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH3S0() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH3S0())) {
                mCyclicValueH3S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH3S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH3S1() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH3S1())) {
                mCyclicValueH3S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH3S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicValueH3S2() != mFloidActivityPreviousFloidModelStatus.getCyclicValueH3S2())) {
                mCyclicValueH3S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCyclicValueH3S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveValueH0() != mFloidActivityPreviousFloidModelStatus.getCollectiveValueH0())) {
                mCollectiveValueH0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveValueH0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveValueH1() != mFloidActivityPreviousFloidModelStatus.getCollectiveValueH1())) {
                mCollectiveValueH1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveValueH1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveValueH2() != mFloidActivityPreviousFloidModelStatus.getCollectiveValueH2())) {
                mCollectiveValueH2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveValueH2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCollectiveValueH3() != mFloidActivityPreviousFloidModelStatus.getCollectiveValueH3())) {
                mCollectiveValueH3TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCollectiveValueH3()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH0S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH0S0())) {
                mCalculatedCyclicBladePitchH0S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH0S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH0S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH0S1())) {
                mCalculatedCyclicBladePitchH0S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH0S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH0S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH0S2())) {
                mCalculatedCyclicBladePitchH0S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH0S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH1S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH1S0())) {
                mCalculatedCyclicBladePitchH1S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH1S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH1S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH1S1())) {
                mCalculatedCyclicBladePitchH1S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH1S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH1S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH1S2())) {
                mCalculatedCyclicBladePitchH1S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH1S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH2S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH2S0())) {
                mCalculatedCyclicBladePitchH2S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH2S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH2S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH2S1())) {
                mCalculatedCyclicBladePitchH2S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH2S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH2S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH2S2())) {
                mCalculatedCyclicBladePitchH2S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH2S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH3S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH3S0())) {
                mCalculatedCyclicBladePitchH3S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH3S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH3S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH3S1())) {
                mCalculatedCyclicBladePitchH3S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH3S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH3S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedCyclicBladePitchH3S2())) {
                mCalculatedCyclicBladePitchH3S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCyclicBladePitchH3S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCollectiveBladePitchH0() != mFloidActivityPreviousFloidModelStatus.getCalculatedCollectiveBladePitchH0())) {
                mCalculatedCollectiveBladePitchH0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCollectiveBladePitchH0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCollectiveBladePitchH1() != mFloidActivityPreviousFloidModelStatus.getCalculatedCollectiveBladePitchH1())) {
                mCalculatedCollectiveBladePitchH1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCollectiveBladePitchH1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCollectiveBladePitchH2() != mFloidActivityPreviousFloidModelStatus.getCalculatedCollectiveBladePitchH2())) {
                mCalculatedCollectiveBladePitchH2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCollectiveBladePitchH2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedCollectiveBladePitchH3() != mFloidActivityPreviousFloidModelStatus.getCalculatedCollectiveBladePitchH3())) {
                mCalculatedCollectiveBladePitchH3TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedCollectiveBladePitchH3()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH0S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH0S0())) {
                mCalculatedBladePitchH0S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH0S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH0S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH0S1())) {
                mCalculatedBladePitchH0S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH0S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH0S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH0S2())) {
                mCalculatedBladePitchH0S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH0S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH1S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH1S0())) {
                mCalculatedBladePitchH1S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH1S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH1S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH1S1())) {
                mCalculatedBladePitchH1S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH1S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH1S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH1S2())) {
                mCalculatedBladePitchH1S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH1S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH2S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH2S0())) {
                mCalculatedBladePitchH2S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH2S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH2S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH2S1())) {
                mCalculatedBladePitchH2S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH2S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH2S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH2S2())) {
                mCalculatedBladePitchH2S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH2S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH3S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH3S0())) {
                mCalculatedBladePitchH3S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH3S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH3S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH3S1())) {
                mCalculatedBladePitchH3S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH3S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedBladePitchH3S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedBladePitchH3S2())) {
                mCalculatedBladePitchH3S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedBladePitchH3S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH0S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH0S0())) {
                mCalculatedServoDegreesH0S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH0S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH0S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH0S1())) {
                mCalculatedServoDegreesH0S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH0S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH0S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH0S2())) {
                mCalculatedServoDegreesH0S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH0S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH1S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH1S0())) {
                mCalculatedServoDegreesH1S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH1S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH1S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH1S1())) {
                mCalculatedServoDegreesH1S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH1S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH1S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH1S2())) {
                mCalculatedServoDegreesH1S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH1S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH2S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH2S0())) {
                mCalculatedServoDegreesH2S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH2S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH2S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH2S1())) {
                mCalculatedServoDegreesH2S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH2S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH2S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH2S2())) {
                mCalculatedServoDegreesH2S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH2S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH3S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH3S0())) {
                mCalculatedServoDegreesH3S0TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH3S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH3S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH3S1())) {
                mCalculatedServoDegreesH3S1TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH3S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoDegreesH3S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoDegreesH3S2())) {
                mCalculatedServoDegreesH3S2TextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getCalculatedServoDegreesH3S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH0S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH0S0())) {
                mCalculatedServoPulseH0S0TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH0S0()));
                mServo0LPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH0S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH0S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH0S1())) {
                mCalculatedServoPulseH0S1TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH0S1()));
                mServo0RPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH0S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH0S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH0S2())) {
                mCalculatedServoPulseH0S2TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH0S2()));
                mServo0PPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH0S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH1S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH1S0())) {
                mCalculatedServoPulseH1S0TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH1S0()));
                mServo1LPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH1S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH1S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH1S1())) {
                mCalculatedServoPulseH1S1TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH1S1()));
                mServo1RPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH1S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH1S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH1S2())) {
                mCalculatedServoPulseH1S2TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH1S2()));
                mServo1PPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH1S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH2S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH2S0())) {
                mCalculatedServoPulseH2S0TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH2S0()));
                mServo2LPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH2S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH2S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH2S1())) {
                mCalculatedServoPulseH2S1TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH2S1()));
                mServo2RPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH2S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH2S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH2S2())) {
                mCalculatedServoPulseH2S2TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH2S2()));
                mServo2PPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH2S2()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH3S0() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH3S0())) {
                mCalculatedServoPulseH3S0TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH3S0()));
                mServo3LPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH3S0()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH3S1() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH3S1())) {
                mCalculatedServoPulseH3S1TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH3S1()));
                mServo3RPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH3S1()));
            }
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCalculatedServoPulseH3S2() != mFloidActivityPreviousFloidModelStatus.getCalculatedServoPulseH3S2())) {
                mCalculatedServoPulseH3S2TextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH3S2()));
                mServo3PPulseTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCalculatedServoPulseH3S2()));
            }
            // Cyclic Heading:
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getCyclicHeading() != mFloidActivityPreviousFloidModelStatus.getCyclicHeading())) {
                mCyclicHeadingTextView.setText(String.valueOf(mFloidActivityFloidModelStatus.getCyclicHeading()));
            }
            // Velocity Cyclic Alpha:
            if ((mFloidActivityPreviousFloidModelStatus == null) || (mFloidActivityFloidModelStatus.getVelocityCyclicAlpha() != mFloidActivityPreviousFloidModelStatus.getVelocityCyclicAlpha())) {
                mVelocityCyclicAlphaTextView.setText(mShortDecimalFormat.format(mFloidActivityFloidModelStatus.getVelocityCyclicAlpha()));
            }
            // [JOYSTICK] 7/1/2013 - WE ARE NOT GOING TO IMPLEMENT THESE AS BELOW - WE WILL HAVE TO TEST THE CYCLICS BY SETTING THE TARGETS...
            // [JOYSTICK] displayFloidStatus: SET THE COLLECTIVE SERVO CONTROLLER AND CYCLIC JOYSTICK VALUES HERE FROM THE STATUS - THIS IS A VALID NOTE
/*
//            private               FloidServoController         mModelStatusHeli0CollectiveServoController;
//            private               FloidServoController         mModelStatusHeli1CollectiveServoController;
//            private               FloidServoController         mModelStatusHeli2CollectiveServoController;
//            private               FloidServoController         mModelStatusHeli3CollectiveServoController;
*/
        }
        // Clone the floid model status (shallow - no references):
        try {
            mFloidActivityPreviousFloidModelStatus = mFloidActivityFloidModelStatus.clone();
        } catch (CloneNotSupportedException e) {
            Log.e(TAG, "Clone not supported in FloidModelStatus", e);
        }
    }

    /**
     * Gets pan tilt mode value string.
     *
     * @param panTiltMode the pan tilt mode
     * @return the pan tilt mode value string
     */
    private String getPanTiltModeValueString(int panTiltMode) {
        switch (panTiltMode) {
            case FloidService.PAN_TILT_MODE_ANGLE: {
                return "ANGLE";
            }
            case FloidService.PAN_TILT_MODE_TARGET: {
                return "TARGET";
            }
            case FloidService.PAN_TILT_MODE_SCAN: {
                return "SCAN";
            }
            default: {
                return "UNKNOWN";
            }
        }
    }

    /**
     * The type Incoming handler.
     */
    private static class IncomingHandler extends Handler {
        /**
         * The S floid droid main.
         */
        private final WeakReference<FloidActivity> sFloidDroidMain;

        /**
         * Instantiates a new Incoming handler.
         *
         * @param floidActivity the floid activity
         */
        IncomingHandler(Looper looper, FloidActivity floidActivity) {
            super(looper);
            sFloidDroidMain = new WeakReference<>(floidActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            FloidActivity floidActivity = sFloidDroidMain.get();
            if (floidActivity != null) {
                floidActivity.handleMessage(msg);
            }
        }
    }

    /**
     * The floid droid message handler.
     */
    private final Handler mFloidDroidMessageHandler = new IncomingHandler(Looper.myLooper(), this);
    // The Handler that gets information back from the FloidDroidRunnable Thread

    /**
     * Handle message.
     *
     * @param message the message
     */
    @SuppressLint("DefaultLocale")
    private void handleMessage(Message message) {
        if (message == null) {
            // Should not happen:
            Log.e(TAG, "UX Message was null...");
            return;
        }
        Bundle data = message.getData();
        if(data!=null) {
            data.setClassLoader(FloidActivity.class.getClassLoader());
        }
        try {
            switch (message.what) {
                case FLOID_UX_MESSAGE_SET_DROID_STATUS: {
//                    if(mFloidActivityUseLogger)
//                        Log.v(TAG, "UX <-- Set Droid Status");
                    if(data!=null) {
                        mFloidActivityFloidDroidStatus = data.getParcelable(FloidService.BUNDLE_KEY_FLOID_DROID_STATUS, FloidDroidStatusParcelable.class);
                        displayDroidStatus();
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_SET_FLOID_STATUS: {
//                    if(mFloidActivityUseLogger)
//                        Log.d(TAG, "UX <-- Set Floid Status");
                    if(data!=null) {
                        mFloidActivityFloidStatus = data.getParcelable(FloidService.BUNDLE_KEY_FLOID_STATUS, FloidStatusParcelable.class);
                        displayFloidStatus();
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_SET_MODEL_STATUS: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Set Model Status");
                    if(data!=null) {
                        mFloidActivityFloidModelStatus = data.getParcelable(FloidService.BUNDLE_KEY_FLOID_MODEL_STATUS, FloidModelStatusParcelable.class);
                        displayFloidModelStatus();
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_SET_MODEL_PARAMETERS: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Get Model Parameters");
                    if(data!=null) {
                        mFloidActivityFloidModelParameters = data.getParcelable(FloidService.BUNDLE_KEY_FLOID_MODEL_PARAMETERS, FloidModelParametersParcelable.class);
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                    displayFloidModelParameters();
                }
                break;
                case FLOID_UX_MESSAGE_SET_HOST: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Set Host");
                    try {
                        if(data!=null) {
                            mFloidActivityHostAddress = data.getString(FloidService.BUNDLE_KEY_HOST_ADDRESS);
                            mFloidActivityHostPort = data.getInt(FloidService.BUNDLE_KEY_HOST_PORT);
                            // Set the host IP:
                            mServerAddressEditText.setText(mFloidActivityHostAddress);
                            mServerPortEditText.setText(String.valueOf(mFloidActivityHostPort));
                        } else {
                            Log.e(TAG, "UX <-- ERROR: Empty Data");
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Exception setting host", e);
                    }
                }
                break;
                case FLOID_UX_MESSAGE_SET_IDS: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Set IDs");
                    if(data!=null) {
                        mFloidActivityFloidId = data.getInt(FloidService.BUNDLE_KEY_FLOID_ID);
                        mFloidActivityFloidUuid = data.getString(FloidService.BUNDLE_KEY_FLOID_UUID);
                        // Set the text:
                        mFloidIdEditText.setText(String.valueOf(mFloidActivityFloidId));
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_SET_LOG: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Set Log");
                    if(data!=null) {
                    mFloidActivityUseLogger = data.getBoolean(FloidService.BUNDLE_KEY_LOG);
                    mFloidServicePid = data.getInt(FloidService.BUNDLE_KEY_FLOID_SERVICE_PID);
                    mDroidLoggingToggleButton.setChecked(mFloidActivityUseLogger);
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_SET_CLIENT_COMMUNICATOR: {
                    if(mFloidActivityUseLogger)
                        Log.v(TAG, "UX <-- Set Client Communicator");
                    if (data != null) {
                        mFloidActivityClientCommunicatorOn = data.getBoolean(FloidService.BUNDLE_KEY_CLIENT_COMMUNICATOR);
                        mClientCommunicatorToggleButton.setChecked(mFloidActivityClientCommunicatorOn);
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_DING_COMM_STATUS: {
 //                   if(mFloidActivityUseLogger)
 //                       Log.v(TAG, "UX <-- Ding Floid Comm Status");
                    dingFloidCommStatus();
                }
                break;
                case FLOID_UX_MESSAGE_DING_SERVER_STATUS: {
//                    if(mFloidActivityUseLogger)
//                        Log.v(TAG, "UX <-- Ding Droid Comm Status");
                    dingServerCommStatus();
                }
                break;
                case FLOID_UX_MESSAGE_FAIL_SERVER_STATUS: {
//                    if(mFloidActivityUseLogger)
//                        Log.v(TAG, "UX <-- Fail Server Status");
                    failServerCommStatus();
                }
                break;
                case FLOID_UX_MESSAGE_DEBUG: {
                    if(mFloidActivityUseLogger)
                        Log.v(TAG, "UX <-- Debug");
                    if (data != null) {
                        String debugMessage = data.getString(FloidService.BUNDLE_KEY_DEBUG_MESSAGE);
                        addMessageToStatusText(debugMessage);
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_COMMAND_RESPONSE: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Command Response");
                    if(data!=null) {
                        String debugMessage = data.getString(FloidService.BUNDLE_KEY_COMMAND_RESPONSE);
                        addMessageToStatusText(debugMessage);
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_TEST_PACKET_RESPONSE: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Test Packet Response");
                    if(data!=null) {
                        String testPacketResponse = data.getString(FloidService.BUNDLE_KEY_TEST_PACKET_RESPONSE);
                        addMessageToStatusText(testPacketResponse);
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_MESSAGE_TO_DISPLAY: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Message To Display");
                    if(data!=null) {
                        String debugMessage = data.getString(FloidService.BUNDLE_KEY_MESSAGE);
                        addMessageToStatusText(debugMessage);
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_START_INTERFACE: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Start Interface");
                    startInterfaceInput();
                }
                break;
                case FLOID_UX_MESSAGE_STOP_INTERFACE: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Stop Interface");
                    stopInterfaceInput();
                }
                break;
                // NOTE: All of these are internal to the activity only, which is why they can pass TextViews:
                case FLOID_UX_MESSAGE_INTERFACE_SET_TEXT: {
//                    if(mFloidActivityUseLogger)
//                        Log.d(TAG, "UX <-- Set Text");
                    FloidInterfaceSetTextMessage floidInterfaceSetTextMessage = (FloidInterfaceSetTextMessage) message.obj;
                    floidInterfaceSetTextMessage.getTextView().setText(floidInterfaceSetTextMessage.getText());
                }
                break;
                case FLOID_UX_MESSAGE_INTERFACE_SET_SLIDER: {
//                    if(mFloidActivityUseLogger)
//                        Log.d(TAG, "UX <-- Set Slider");
                    FloidInterfaceSetSliderMessage floidInterfaceSetSliderMessage = (FloidInterfaceSetSliderMessage) message.obj;
                    floidInterfaceSetSliderMessage.getSlider().setProgress(floidInterfaceSetSliderMessage.getValue());
                }
                break;
                case FLOID_UX_MESSAGE_INTERFACE_ENABLE: {
//                    if(mFloidActivityUseLogger)
//                        Log.d(TAG, "UX <-- Interface Enable");
                    FloidInterfaceEnableMessage floidInterfaceEnableMessage = (FloidInterfaceEnableMessage) message.obj;
                    floidInterfaceEnableMessage.getView().setEnabled(floidInterfaceEnableMessage.isEnable());
                }
                break;
                case FLOID_UX_MESSAGE_SET_ACCESSORY_CONNECTED: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Set Accessory Connected");
                    if (data != null) {
                        mFloidActivityAccessoryConnected = data.getBoolean(FloidService.BUNDLE_KEY_ACCESSORY_CONNECTED);
                        setAccessoryIcon(mFloidActivityAccessoryConnected);
                        mFloidConnectedTextView.setText(mFloidActivityAccessoryConnected ? "Yes" : "No");
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_START_MISSION_TIMER: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Start Mission Timer");
                    if(data!=null) {
                        // Note: Mission name in obj, time in arg1
                        // Set the name and id:
                        mMissionStartTimerMissionNameText.setText(data.getString(FloidService.BUNDLE_KEY_MISSION_START_NAME));
                        // Set the time:
                        mMissionStartTimerText.setText(String.valueOf(data.getInt(FloidService.BUNDLE_KEY_MISSION_START_TIMER_TIME)));
                        // Set up the cancel button:
                        mMissionStartTimerCancelButton.setOnClickListener(v -> uxCancelMissionTimer(true)
                        );
                        // Display the view and set that we are in the mode:
                        mMissionStartTimerContainer.setVisibility(View.VISIBLE);
                        mFloidViewFrames.setVisibility(View.GONE);
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_UPDATE_MISSION_TIMER: {
//                    if(mFloidActivityUseLogger)
//                        Log.d(TAG, "UX <-- Update Mission Timer");
                    if(data!=null) {
                        // Display the new time
                        mMissionStartTimerText.setText(String.format("%d", data.getInt(FloidService.BUNDLE_KEY_MISSION_TIMER_UPDATE)));
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_CANCEL_MISSION_TIMER: {
//                    if(mFloidActivityUseLogger)
//                        Log.d(TAG, "UX <-- Cancel Mission Timer");
                    uxCancelMissionTimer(false);
                }
                break;
                case FLOID_UX_MESSAGE_SET_DROID_DEFAULT_PARAMETERS: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Set Droid Default Parameters");
                    if (data != null) {
                        HashMapStringToStringParcelable droidDefaultParametersParcelable = data.getParcelable(FloidService.BUNDLE_KEY_DROID_DEFAULT_PARAMETERS, HashMapStringToStringParcelable.class);
                        if(droidDefaultParametersParcelable!=null) {
                            uxSetDroidDefaultParameters(droidDefaultParametersParcelable.getStringToStringHashMap());
                        } else {
                            Log.e(TAG, "UX <-- ERROR: Droid Default Parameters was null");
                        }
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_SET_DROID_PARAMETERS: {
                    Log.i(TAG, "UX <-- Set Droid Parameters");
                    if (data != null) {
                        HashMapStringToStringParcelable droidParametersParcelable = data.getParcelable(FloidService.BUNDLE_KEY_FLOID_DROID_PARAMETERS, HashMapStringToStringParcelable.class);
                        if(droidParametersParcelable !=null) {
                            uxSetDroidParameters(droidParametersParcelable.getStringToStringHashMap());
                        } else {
                            Log.e(TAG, "UX <-- ERROR: Droid Parameters was null");
                        }
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_SET_DROID_PARAMETER_VALID_VALUES: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Set Droid Parameter Defaults");
                    if (data != null) {
                        HashMapStringToDroidValidParametersParcelable hashMapStringToDroidValidParametersParcelable = data.getParcelable(FloidService.BUNDLE_KEY_FLOID_DROID_VALID_PARAMETERS, HashMapStringToDroidValidParametersParcelable.class);
                        if(hashMapStringToDroidValidParametersParcelable !=null) {
                            uxSetDroidParameterValidValues(hashMapStringToDroidValidParametersParcelable.getStringToDroidValidParametersHashMap());
                        } else {
                            Log.e(TAG, "UX <-- ERROR: Droid Parameter Defaults was null");
                        }
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_IMAGING_STATUS: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Imaging Status");
                    if (data != null) {
                        FloidImagingStatusParcelable floidImagingStatus = data.getParcelable(FloidService.BUNDLE_KEY_FLOID_IMAGING_STATUS, FloidImagingStatusParcelable.class);
                        if(floidImagingStatus !=null) {
                            uxSetImagingStatus(floidImagingStatus);
                        } else {
                            Log.e(TAG, "UX <-- ERROR: Floid Imaging Status was null");
                        }
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_PHOTO: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Photo");
                    if (data != null) {
                        final byte[] photo = data.getByteArray(FloidService.BUNDLE_KEY_PHOTO);
                        if(photo!=null) {
                            uxSetPhoto(photo);
                        } else {
                            Log.e(TAG, "UX <-- ERROR: Photo was null");
                        }
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_VIDEO_FRAME: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Video Frame");
                    if (data != null) {
                        final byte[] videoFrame = data.getByteArray(FloidService.BUNDLE_KEY_VIDEO_FRAME);
                        if(videoFrame!=null) {
                            uxSetVideoFrame(videoFrame);
                        } else {
                            Log.e(TAG, "UX <-- ERROR: Video Frame was null");
                        }
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_CAMERA_INFO: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Camera Info");
                    if (data != null) {
                        final ArrayList<FloidCameraInfo> cameraInfoArrayList = data.getParcelableArrayList(FloidService.BUNDLE_KEY_CAMERA_INFO_LIST, FloidCameraInfo.class);
                        if(cameraInfoArrayList!=null) {
                            uxSetCameraInfo(cameraInfoArrayList);
                        } else {
                            Log.e(TAG, "UX <-- ERROR: Camera Info List was null");
                        }
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_SET_ADDITIONAL_STATUS: {
                    // Too chatty:
//                    if(mFloidActivityUseLogger) Log.d(TAG, "UX <-- Set Additional Status");
                    if (data != null) {
                        AdditionalStatus newAdditionalStatus = data.getParcelable(FloidService.BUNDLE_KEY_ADDITIONAL_STATUS, AdditionalStatus.class);
                        if(newAdditionalStatus!=null) {
                            additionalStatus = newAdditionalStatus;
                            updateAdditionalStatus();
                        } else {
                            Log.e(TAG, "UX <-- ERROR: Additional Status was null");
                        }
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                case FLOID_UX_MESSAGE_UPDATE_COMM_STATUS: {
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "UX <-- Update Comm Status");
                    if (data != null) {
                        int item = data.getInt(FloidService.BUNDLE_KEY_COMM_ITEM);
                        int state = data.getInt(FloidService.BUNDLE_KEY_COMM_STATE);
                        updateCommStatusBorders(item, state);
                    } else {
                        Log.e(TAG, "UX <-- ERROR: Empty Data");
                    }
                }
                break;
                default: {
                    Log.e(TAG, "UX <-- Unknown Message (? = " + message.what + ")");
                    // Unknown message:
                    sendPlayNotificationToFloidService(FloidService.FLOID_ALERT_SOUND_BUZZER);
                }
                break;
            }
        } catch (Exception e) {
            Log.e(TAG, "Handle Message: Caught Exception", e);
        }
    }

    private void uxSetPhoto(byte[] photoBytes) {
        try {
            Bitmap bitmapFactory = BitmapFactory.decodeByteArray(photoBytes, 0, photoBytes.length);
            mPhotoImageView.setImageBitmap(bitmapFactory);
            mPhotoImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        } catch (Exception e) {
            Log.e(TAG, "Exception setting photo", e);
        }
    }

    private void uxSetVideoFrame(byte[] videoFrameBytes) {
        try {
            Bitmap bitmapFactory = BitmapFactory.decodeByteArray(videoFrameBytes, 0, videoFrameBytes.length);
            mVideoImageView.setImageBitmap(bitmapFactory);
            mVideoImageView.setScaleType(ImageView.ScaleType.FIT_XY);
        } catch (Exception e) {
            Log.e(TAG, "Exception setting video", e);
        }
    }

    private void uxSetImagingStatus(FloidImagingStatus floidImagingStatus) {
        mImagingPhotoCameraIdTextView.setText(floidImagingStatus.getPhotoCameraId());
        mImagingVideoCameraIdTextView.setText(floidImagingStatus.getVideoCameraId());
        mImagingSupportsFlashTextView.setText(Boolean.toString(floidImagingStatus.isFlashSupported()));
        mImagingLegacyHardwareTextView.setText(Boolean.toString(floidImagingStatus.isHardwareIsLegacy()));
        mImagingPhotoStateTextView.setText(FloidImagingStatus.getStateName(floidImagingStatus.getPhotoState()));
        mImagingRemainingCaptureSessionsTextView.setText(String.format(Locale.getDefault(), "%d", floidImagingStatus.getNumberOfRemainingCaptureSessionsToCreate()));
        mImagingStatusInitializedTextView.setText(Boolean.toString(floidImagingStatus.isStatusInitialized()));

        mImagingStatusTakingPhotoTextView.setText(Boolean.toString(floidImagingStatus.isStatusTakingPhoto()));
        mImagingStatusPhotoIdDirtyTextView.setText(Boolean.toString(floidImagingStatus.isStatusPhotoCameraIdDirty()));
        mImagingStatusPhotoPropertiesDirtyTextView.setText(Boolean.toString(floidImagingStatus.isStatusPhotoCameraPropertiesDirty()));
        mImagingStatusPhotoCameraOpenTextView.setText(Boolean.toString(floidImagingStatus.isStatusPhotoCameraOpen()));
        mImagingStatusPhotoSurfacesSetUpTextView.setText(Boolean.toString(floidImagingStatus.isStatusPhotoSurfacesSetup()));
        mImagingStatusPhotoCaptureRequestSetUpTextView.setText(Boolean.toString(floidImagingStatus.isStatusPhotoCaptureRequestSetup()));
        mImagingStatusPhotoCaptureSessionConfiguredTextView.setText(Boolean.toString(floidImagingStatus.isStatusPhotoCameraCaptureSessionConfigured()));

        mImagingStatusTakingVideoTextView.setText(Boolean.toString(floidImagingStatus.isStatusTakingVideo()));
        mImagingStatusVideoIdDirtyTextView.setText(Boolean.toString(floidImagingStatus.isStatusVideoCameraIdDirty()));
        mImagingStatusVideoPropertiesDirtyTextView.setText(Boolean.toString(floidImagingStatus.isStatusVideoCameraPropertiesDirty()));
        mImagingStatusVideoCameraOpenTextView.setText(Boolean.toString(floidImagingStatus.isStatusVideoCameraOpen()));
        mImagingStatusVideoSurfacesSetUpTextView.setText(Boolean.toString(floidImagingStatus.isStatusVideoSurfacesSetup()));
        mImagingStatusVideoCaptureRequestSetUpTextView.setText(Boolean.toString(floidImagingStatus.isStatusVideoCaptureRequestSetup()));
        mImagingStatusVideoCaptureSessionConfiguredTextView.setText(Boolean.toString(floidImagingStatus.isStatusVideoCameraCaptureSessionConfigured()));
        mImagingStatusSupportsVideoSnapshotTextView.setText(Boolean.toString(floidImagingStatus.isStatusSupportsVideoSnapshot()));
        mImagingStatusTakingVideoSnapshotTextView.setText(Boolean.toString(floidImagingStatus.isStatusTakingVideoSnapshot()));
    }

    private void uxSetCameraInfo(List<FloidCameraInfo> cameraInfoList) {
        // Clear out the old camera info list items if they exist after the video tab
        while(mImagingTabsLabels.getChildCount() > CAMERA_INFO_TAB_VIDEO + 1) {
            mImagingTabsLabels.removeViewAt(CAMERA_INFO_TAB_VIDEO + 1);
            mImagingTabsContents.removeViewAt(CAMERA_INFO_TAB_VIDEO + 1);
        }
        for(FloidCameraInfo cameraInfo:cameraInfoList) {
            // Create a new label for the tab:
            LinearLayout cameraInfoTabLabelLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.tab_label, mImagingContainer, false);
            // Get its text view:
            TextView cameraInfoTabLabelTextView = cameraInfoTabLabelLayout.findViewById(R.id.tab_label_text);
            // Add a click listener
            cameraInfoTabLabelTextView.setOnClickListener(imagingTabOnClickListener);
            // Set its state:
            cameraInfoTabLabelTextView.setBackgroundColor(mUnFocusedSubMenuTabColor);
            cameraInfoTabLabelTextView.setTextColor(mUnFocusedSubMenuTabTextColor);
            // Set the text to the camera id:
            cameraInfoTabLabelTextView.setText(cameraInfo.getCameraId());
            // Add this into the tab label:
            mImagingTabsLabels.addView(cameraInfoTabLabelLayout);

            // Create a new tab with a camera info layout: // camera_info_layout
            ScrollView cameraInfoTabScrollView = (ScrollView) getLayoutInflater().inflate(R.layout.camera_info, mImagingContainer, false);
            // Set it to be invisible:
            cameraInfoTabScrollView.setVisibility(View.GONE);
            LinearLayout cameraInfoTabLayout = cameraInfoTabScrollView.findViewById(R.id.camera_info_layout);

            for(String key:cameraInfo.getCameraInfo().keySet()) {
                // Create a new info section and add it to the camera info tab layout:
                LinearLayout cameraInfoSectionLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.camera_info_section, mImagingContainer, false);
                cameraInfoTabLayout.addView(cameraInfoSectionLayout);
                // Set its label:
                TextView cameraInfoSectionLabelTextView = cameraInfoSectionLayout.findViewById(R.id.camera_info_section_label);
                cameraInfoSectionLabelTextView.setText(key);
                // Add in the values:
                final Map<String, List<String>> cameraInfoMap = cameraInfo.getCameraInfo();
                if(cameraInfoMap!=null) {
                    final List<String> infoStringList = cameraInfoMap.get(key);
                    if(infoStringList!=null) {
                        for (String value : infoStringList) {
                            // Create a new info item and add it to the camera info section layout:
                            LinearLayout cameraInfoItemLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.camera_info_item, mImagingContainer, false);
                            cameraInfoSectionLayout.addView(cameraInfoItemLayout);
                            // Set its label:
                            TextView cameraInfoSectionItemValueTextView = cameraInfoItemLayout.findViewById(R.id.camera_info_item_value);
                            cameraInfoSectionItemValueTextView.setText(value);
                        }
                    }
                }
            }
            // Add tab into the contents:
            mImagingTabsContents.addView(cameraInfoTabScrollView);
        }
    }
    /**
     * Set the droid parameters to the UX
     *
     * @param droidParameters the parameters
     */
    private void uxSetDroidParameters(HashMap<String, String> droidParameters) {
        mFloidActivityDroidParameters = droidParameters;
        setupDroidParametersEditor();
    }

    /**
     * Set the droid default parameters
     *
     * @param droidDefaultParameters the droid default parameters
     */
    private void uxSetDroidDefaultParameters(HashMap<String, String> droidDefaultParameters) {
        mFloidActivityDroidDefaultParameters = droidDefaultParameters;
    }

    /**
     * Set the droid parameters valid values
     *
     * @param droidParametersValidValues the droid parameters valid values
     */
    private void uxSetDroidParameterValidValues(HashMap<String, DroidValidParameters> droidParametersValidValues) {
        mFloidActivityDroidParametersValidValues = droidParametersValidValues;
    }

    /**
     * Create a new parameter radio button with the given name:
     *
     * @param name the name for the radio button
     * @return the radio button
     */
    private RadioButton createParameterRadioButton(RadioGroup radioGroup, String name) {
        // Create a new layout for the radio button:
        RadioButton radioButton = (RadioButton) getLayoutInflater().inflate(R.layout.droid_parameter_radio_button, radioGroup, false);
        radioButton.setText(name);
        return radioButton;
    }

    /**
     * Enable all the edit buttons in the edit state list
     *
     * @param droidParameterEditStateList the edit state list
     */
    private void enableAllButtons(List<DroidParameterEditState> droidParameterEditStateList) {
        for (DroidParameterEditState droidParameterEditState : droidParameterEditStateList) {
            droidParameterEditState.getEditButton().setEnabled(true);
        }
    }

    /**
     * Disable all buttons but the specified one in the edit state list
     *
     * @param droidParameterEditStateList the edit state list
     * @param editButton                  the edit button
     */
    private void disableOtherButtons(List<DroidParameterEditState> droidParameterEditStateList, Button editButton) {
        for (DroidParameterEditState droidParameterEditState : droidParameterEditStateList) {
            if (droidParameterEditState.getEditButton() != editButton) {
                droidParameterEditState.getEditButton().setEnabled(false);
            }
        }
    }

    // Save Logic:
    //  At least one value is edited; all edits are closed

    // Cancel Logic:
    //  At least one value is edited
    @SuppressWarnings("unused")
    private void setDroidParameterSaveResetButtonStates(Button saveButton, Button cancelButton, List<DroidParameterEditState> droidParameterEditStateList) {
        boolean allEditsClosed = true;
        boolean atLeastOneParameterEdited = false;
        for (DroidParameterEditState droidParameterEditState : droidParameterEditStateList) {
            // Test this edit state:
            if (droidParameterEditState.isEditing()) {
                allEditsClosed = false;
            }
            if (droidParameterEditState.isDirty()) {
                atLeastOneParameterEdited = true;
            }
        }
        cancelButton.setEnabled(atLeastOneParameterEdited);
        saveButton.setEnabled(atLeastOneParameterEdited && allEditsClosed);
    }

    /**
     * The droid parameters floid edit text validator callback.
     */
    private class FloidDroidParameterEditTextValidatorCallbackImpl extends FloidDroidParameterEditTextValidatorCallback {
        @Override
        public void updateInterface() {
            editState.getEditButton().setEnabled(editState.getEditText().getError() == null);
            // Modified?
            if(editState.getEditText().toString().equalsIgnoreCase(editState.getOriginalValue())) {
                // No:
                editState.getEditText().setTextColor(unModifiedParameterValueTextColor);
                editState.setDirty(false);
            }
            else {
                // Yes:
                editState.getEditText().setTextColor(modifiedParameterValueTextColor);
                editState.setDirty(true);
            }
        }
    }

    /**
     * Set up the droid parameters editing interface
     */
    private void setupDroidParametersEditor() {
        // Clear views:
        mDroidParametersContainer.removeAllViews();
        // Clear edits:
        mDroidParameterEditStateList.clear();
        // Disable the set button:
        mDroidParametersSetButton.setEnabled(false);
        // Disable the cancel button:
        mDroidParametersCancelButton.setEnabled(false);
        // Start with no tag and no section layout:
        String currentTag = null;
        LinearLayout currentParameterSectionLayout = null;
        // Spin through all the valid values sorted:
        for (String droidParameter : new TreeMap<>(mFloidActivityDroidParametersValidValues).keySet()) {
            try {
                // Split on '.' to get parameter tag:
                final String[] parameterSplit = droidParameter.split("\\.");
                String parameterTag = parameterSplit[0];
                // Name is the rest:
                String parameterName = droidParameter.substring(droidParameter.indexOf('.') + 1);//parameterSplit[1];
                if (parameterTag == null) {
                    parameterTag = "none";
                }
                // Sections:
                // If this does not match the current tag:
                if (!parameterTag.equals(currentTag)) {
                    // Move to new tag:
                    currentTag = parameterTag;
                    currentParameterSectionLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.droid_parameters_section, mDroidParametersContainer, false);
                    mDroidParametersContainer.addView(currentParameterSectionLayout);
                    final TextView labelTextView = currentParameterSectionLayout.findViewById(R.id.droid_parameters_section_label);
                    // Set the current tag heading:
                    labelTextView.setText(getString(R.string.parameter_tag_heading, currentTag));
                }
                // Parameter and value and edits:
                // Get the parameter for that value:
                final String droidParameterValue = mFloidActivityDroidParameters.get(droidParameter);
                final String droidParameterValueLowerCase = droidParameterValue==null?"":droidParameterValue.toLowerCase(Locale.ROOT);

                // Get the valid parameters for that value:
                final DroidValidParameters droidValidParameters = mFloidActivityDroidParametersValidValues.get(droidParameter);
                // Get the default value:
                final String droidParameterDefaultParameter = mFloidActivityDroidDefaultParameters.get(droidParameter);
                // Create a new layout for the parameter:
                final LinearLayout currentParameterLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.droid_parameter, mDroidParametersContainer, false);
                currentParameterLayout.setGravity(Gravity.CENTER_VERTICAL);
                if (currentParameterSectionLayout != null) {
                    currentParameterSectionLayout.addView(currentParameterLayout);
                }
                // Put in the name:
                final TextView nameTextView = currentParameterLayout.findViewById(R.id.droid_parameter_name);
                nameTextView.setText(parameterName);
                // Get/set the edit text:
                final EditText editText = currentParameterLayout.findViewById(R.id.droid_parameter_edit_text);
                editText.setText(droidParameterValue);
                editText.setTextColor(unModifiedParameterValueTextColor);
                // Disable EditText if is options only:
                if (droidValidParameters!=null && droidValidParameters.isUseOptionsOnly()) {
                    editText.setVisibility(View.VISIBLE);
                    editText.setEnabled(false);
                }
                // Get the edit section:
                final LinearLayout editSection = currentParameterLayout.findViewById(R.id.droid_parameter_edit_section);
                editSection.setVisibility(View.GONE);
                // Get the edit section top:
                final LinearLayout editSectionTop = currentParameterLayout.findViewById(R.id.droid_parameter_edit_section_top);
                editSectionTop.setVisibility(View.GONE);
                // Get the open edit button:
                final Button editButton = currentParameterLayout.findViewById(R.id.droid_parameter_edit_button);
                // Create a new droid parameter edit state:
                final DroidParameterEditState droidParameterEditState = new DroidParameterEditState(droidParameter, editButton, editText, false, droidParameterValue);
                // Add it to the list (in future HashMap?):
                mDroidParameterEditStateList.add(droidParameterEditState);
                editButton.setOnClickListener(v -> {
                    // Are we starting to edit?
                    if (editSection.getVisibility() == View.GONE) {
                        // Yes, start editing:
                        currentParameterLayout.setBackgroundColor(editPanelColor);
                        editSection.setVisibility(View.VISIBLE);
                        editSectionTop.setVisibility(View.VISIBLE);
                        editButton.setText("-");
                        droidParameterEditState.setEditing(true);
                        if (droidValidParameters!=null && droidValidParameters.isUseOptionsOnly()) {
                            editText.setBackgroundColor(disabledEditBackgroundColor);
                        } else {
                            editText.setBackgroundColor(enabledEditBackgroundColor);
                        }
                        disableOtherButtons(mDroidParameterEditStateList, editButton);
                        mDroidParametersSetButton.setEnabled(false);
                        mDroidParametersCancelButton.setEnabled(false);
                    } else {
                        // No, end editing:
                        droidParameterEditState.setEditing(false);
                        currentParameterLayout.setBackgroundColor(parameterPanelColor);
                        editText.setBackgroundColor(parameterPanelColor);
                        editSection.setVisibility(View.GONE);
                        editSectionTop.setVisibility(View.GONE);
                        editButton.setText("+");
                        enableAllButtons(mDroidParameterEditStateList);
                        setDroidParameterSaveResetButtonStates(mDroidParametersSetButton, mDroidParametersCancelButton, mDroidParameterEditStateList);
                    }
                });
                // Get the set default button:
                final Button setDefaultButton = currentParameterLayout.findViewById(R.id.droid_parameter_edit_set_default);
                setDefaultButton.setOnClickListener(v -> {
                    if(droidParameterDefaultParameter!=null) {
                        editText.setText(droidParameterDefaultParameter);
                        // Are we back to the original value?
                        if (droidParameterDefaultParameter.equals(droidParameterEditState.getOriginalValue())) {
                            // Set edit text back to right color
                            editText.setTextColor(unModifiedParameterValueTextColor);
                            droidParameterEditState.setDirty(false);
                        } else {
                            editText.setTextColor(modifiedParameterValueTextColor);
                            droidParameterEditState.setDirty(true);
                        }
                    }
                });
                // Get the parameter button:
                final Button resetButton = currentParameterLayout.findViewById(R.id.droid_parameter_edit_reset);
                resetButton.setOnClickListener(v -> {
                    editText.setText(droidParameterEditState.getOriginalValue());
                    editText.setTextColor(unModifiedParameterValueTextColor);
                    droidParameterEditState.setDirty(false);
                });
                // Get the radio group:
                final RadioGroup editRadioGroup = editSection.findViewById(R.id.droid_parameter_radio_group);
                // Get the parameter type: droid_parameter_type
                final TextView typeTextView = editSectionTop.findViewById(R.id.droid_parameter_type);
                // Switch on the parameter type for view:
                if(droidValidParameters!=null) {
                    switch (droidValidParameters.getType()) {
                        case DroidValidParameters.BOOLEAN_TYPE: {
                            // Set the parameter type:
                            typeTextView.setText(DroidValidParameters.DROID_PARAMETER_TYPE_NAME_BOOLEAN);
                            final DroidValidParameters droidValidParameters1 = mFloidActivityDroidParametersValidValues.get(droidParameter);
                            if(droidValidParameters1!=null) {
                                final boolean defaultBooleanValue = droidValidParameters1.getBooleanDefault();
                                setDefaultButton.setText(getString(R.string.parameter_default_label, droidParameterDefaultParameter, Boolean.toString(defaultBooleanValue)));
                                // Create the options radio buttons:
                                RadioButton defaultRadioButton = createParameterRadioButton(editRadioGroup, DroidValidParameters.BOOLEAN_DEFAULT);
                                RadioButton trueRadioButton = createParameterRadioButton(editRadioGroup, DroidValidParameters.BOOLEAN_TRUE);
                                RadioButton falseRadioButton = createParameterRadioButton(editRadioGroup, DroidValidParameters.BOOLEAN_FALSE);
                                editRadioGroup.addView(defaultRadioButton);
                                editRadioGroup.addView(trueRadioButton);
                                editRadioGroup.addView(falseRadioButton);
                                if(droidParameterValue!=null) {
                                    if (droidParameterValueLowerCase.equals(DroidValidParameters.BOOLEAN_DEFAULT.toLowerCase(Locale.ROOT)))
                                        defaultRadioButton.setChecked(true);
                                    if (droidParameterValueLowerCase.equals(DroidValidParameters.BOOLEAN_TRUE.toLowerCase(Locale.ROOT)))
                                        trueRadioButton.setChecked(true);
                                    if (droidParameterValueLowerCase.equals(DroidValidParameters.BOOLEAN_FALSE.toLowerCase(Locale.ROOT)))
                                        falseRadioButton.setChecked(true);
                                }
                            }
                        }
                        break;
                        case DroidValidParameters.FLOAT_TYPE: {
                            // Set the parameter type:
                            String floatType = DroidValidParameters.DROID_PARAMETER_TYPE_NAME_FLOAT;
                            // Set up the validator if needed and the float range:
                            if (!droidValidParameters.isUseOptionsOnly()) {
                                if (droidValidParameters.isUseMin() && droidValidParameters.isUseMax()) {
                                    floatType = floatType + " " + droidValidParameters.getFloatRangeMin() + " - " + droidValidParameters.getFloatRangeMax();
                                }
                                if (droidValidParameters.isUseMin() && !droidValidParameters.isUseMax()) {
                                    floatType = floatType + " >= " + droidValidParameters.getFloatRangeMin();
                                }
                                if (!droidValidParameters.isUseMin() && droidValidParameters.isUseMax()) {
                                    floatType = floatType + " <= " + droidValidParameters.getFloatRangeMax();
                                }
                                // Create a new validator callback and pass it in the edit state and edit state list:
                                FloidDroidParameterEditTextValidatorCallbackImpl parameterEditValidatorCallBack = new FloidDroidParameterEditTextValidatorCallbackImpl();
                                parameterEditValidatorCallBack.setEditState(droidParameterEditState);
                                parameterEditValidatorCallBack.setEditStateList(mDroidParameterEditStateList);
                                // Set a text changed listener now with our validator, validator callback and the various min and max
                                List<String> validValues = new ArrayList<>();
                                validValues.add(DroidValidParameters.BOOLEAN_DEFAULT);
                                editText.addTextChangedListener(new FloidDecimalEditTextValidator(editText, parameterEditValidatorCallBack, droidValidParameters.isUseMin(), droidValidParameters.getFloatRangeMin(), droidValidParameters.isUseMax(), droidValidParameters.getFloatRangeMax(), validValues));
                            }
                            typeTextView.setText(floatType);
                            final DroidValidParameters droidValidParameters1 = mFloidActivityDroidParametersValidValues.get(droidParameter);
                            if(droidValidParameters1!=null) {
                                final float defaultFloatValue = droidValidParameters1.getFloatDefault();
                                setDefaultButton.setText(getString(R.string.parameter_default_label, droidParameterDefaultParameter, Float.toString(defaultFloatValue)));
                            }
                            // Create the options radio buttons:
                            // Start with the default button:
                            final RadioButton defaultFloatRadioButton = createParameterRadioButton(editRadioGroup, DroidValidParameters.BOOLEAN_DEFAULT);
                            editRadioGroup.addView(defaultFloatRadioButton);
                            if(droidParameterValueLowerCase.equals(DroidValidParameters.BOOLEAN_DEFAULT.toLowerCase(Locale.ROOT))) {
                                defaultFloatRadioButton.setChecked(true);
                            }
                            // Then add the valid floats:
                            if (droidValidParameters.getValidFloats() != null && droidValidParameters.getValidFloats().size() > 0) {
                                for (Float validParameterFloat : droidValidParameters.getValidFloats()) {
                                    final RadioButton floatRadioButton = createParameterRadioButton(editRadioGroup, Float.toString(validParameterFloat));
                                    editRadioGroup.addView(floatRadioButton);
                                    if(droidParameterValueLowerCase.equals(Float.toString(validParameterFloat))) {
                                        floatRadioButton.setChecked(true);
                                    }
                                }
                            }
                        }
                        break;
                        case DroidValidParameters.INTEGER_TYPE: {
                            // Set the parameter type:
                            String intType = DroidValidParameters.DROID_PARAMETER_TYPE_NAME_INTEGER;
                            if (!droidValidParameters.isUseOptionsOnly()) {
                                if (droidValidParameters.isUseMin() && droidValidParameters.isUseMax()) {
                                    intType = intType + " " + droidValidParameters.getIntRangeMin() + " - " + droidValidParameters.getIntRangeMax();
                                }
                                if (droidValidParameters.isUseMin() && !droidValidParameters.isUseMax()) {
                                    intType = intType + " >=" + droidValidParameters.getIntRangeMin();
                                }
                                if (!droidValidParameters.isUseMin() && droidValidParameters.isUseMax()) {
                                    intType = intType + " <=" + droidValidParameters.getIntRangeMax();
                                }
                                // Create a new validator callback and pass it in the edit state and edit state list:
                                final FloidDroidParameterEditTextValidatorCallbackImpl parameterEditValidatorCallBack = new FloidDroidParameterEditTextValidatorCallbackImpl();
                                parameterEditValidatorCallBack.setEditState(droidParameterEditState);
                                parameterEditValidatorCallBack.setEditStateList(mDroidParameterEditStateList);
                                // Set a text changed listener now with our validator, validator callback and the various min and max
                                final List<String> validValues = new ArrayList<>();
                                validValues.add(DroidValidParameters.BOOLEAN_DEFAULT);
                                editText.addTextChangedListener(new FloidIntegerEditTextValidator(editText, parameterEditValidatorCallBack, droidValidParameters.isUseMin(), droidValidParameters.getIntRangeMin(), droidValidParameters.isUseMax(), droidValidParameters.getIntRangeMax(), validValues));
                            }
                            typeTextView.setText(intType);
                            final DroidValidParameters droidValidParameters1 = mFloidActivityDroidParametersValidValues.get(droidParameter);
                            if(droidValidParameters1!=null) {
                                final int defaultIntegerValue = droidValidParameters1.getIntDefault();
                                setDefaultButton.setText(getString(R.string.parameter_default_label, droidParameterDefaultParameter, Integer.toString(defaultIntegerValue)));
                            }
                            // Create the options radio buttons:
                            // Start with the default button:
                            final RadioButton defaultIntRadioButton = createParameterRadioButton(editRadioGroup, DroidValidParameters.BOOLEAN_DEFAULT);
                            editRadioGroup.addView(defaultIntRadioButton);
                            if (droidParameterValueLowerCase.equals(DroidValidParameters.BOOLEAN_DEFAULT.toLowerCase(Locale.ROOT))) {
                                defaultIntRadioButton.setChecked(true);
                            }
                            // Then add the valid ints:
                            if (droidValidParameters.getValidInts() != null && droidValidParameters.getValidInts().size() > 0) {
                                for (Integer validParameterInteger : droidValidParameters.getValidInts()) {
                                    RadioButton intRadioButton = createParameterRadioButton(editRadioGroup, Integer.toString(validParameterInteger));
                                    editRadioGroup.addView(intRadioButton);
                                    if(droidParameterValueLowerCase.equals(Integer.toString(validParameterInteger))) {
                                        intRadioButton.setChecked(true);
                                    }
                                }
                            }
                        }
                        break;
                        case DroidValidParameters.SCREEN_SIZE_TYPE: {
                            // Set the parameter type:
                            typeTextView.setText(DroidValidParameters.DROID_PARAMETER_TYPE_NAME_STRING);
                            final DroidValidParameters droidValidParameters1 = mFloidActivityDroidParametersValidValues.get(droidParameter);
                            if (droidValidParameters1 != null) {
                                final String defaultScreenSizeValue = droidValidParameters1.getStringDefault();
                                setDefaultButton.setText(getString(R.string.parameter_default_label, droidParameterDefaultParameter, defaultScreenSizeValue));

                                // Create a new validator callback and pass it in the edit state and edit state list:
                                final FloidDroidParameterEditTextValidatorCallbackImpl parameterEditValidatorCallBack = new FloidDroidParameterEditTextValidatorCallbackImpl();
                                parameterEditValidatorCallBack.setEditState(droidParameterEditState);
                                parameterEditValidatorCallBack.setEditStateList(mDroidParameterEditStateList);
                                editText.addTextChangedListener(
                                        droidValidParameters1.getValidStrings() == null ?
                                                new FloidScreenValueEditTextValidator(editText, parameterEditValidatorCallBack, true, 480, true, 640)
                                                : new FloidScreenValueEditTextValidator(editText, parameterEditValidatorCallBack, true, 480, true, 640, droidValidParameters1.getValidStrings())
                                );
                                // Add the string parameters to the edit radio group:
                                addStringParametersToEditRadioGroup(droidParameterValue, droidValidParameters, editRadioGroup);
                            } else {
                                Log.e(TAG, "No valid parameters for screen type");
                            }
                        }
                        break;
                        case DroidValidParameters.STRING_TYPE: {
                            // Set the parameter type:
                            typeTextView.setText(DroidValidParameters.DROID_PARAMETER_TYPE_NAME_STRING);
                            final DroidValidParameters droidValidParameters1 = mFloidActivityDroidParametersValidValues.get(droidParameter);
                            if(droidValidParameters1!=null) {
                                final String defaultStringValue = droidValidParameters1.getStringDefault();
                                setDefaultButton.setText(getString(R.string.parameter_default_label, droidParameterDefaultParameter, defaultStringValue));
                                // Note: No validation for string type
                                // Add the string parameters to the edit radio group:
                                addStringParametersToEditRadioGroup(droidParameterValue, droidValidParameters1, editRadioGroup);
                            }
                        }
                        break;
                        default: {
                            Log.e(TAG, "No such parameter type " + droidValidParameters.getType());
                        }
                        break;
                    }
                }
                // Set the on check change listener for the radio group:
                editRadioGroup.setOnCheckedChangeListener(new FloidParameterRadioButtonCheckedChangeListener(modifiedParameterValueTextColor, unModifiedParameterValueTextColor, droidParameterEditState));
            } catch (Exception e) {
                Log.e(TAG, "Caught exception displaying droid parameters interface: ", e);
            }
        }
    }

    /**
     * Add the string parameters to the edit radio group
     *
     * @param droidParameterValue  the current parameter value (for setting checked)
     * @param droidValidParameters the parameters
     * @param editRadioGroup       the radio group
     */
    private void addStringParametersToEditRadioGroup(String droidParameterValue, DroidValidParameters droidValidParameters, RadioGroup editRadioGroup) {
        // Create the options radio buttons:
        if (droidValidParameters.getValidStrings() != null && droidValidParameters.getValidStrings().size() > 0) {
            for (String validParameterString : droidValidParameters.getValidStrings()) {
                RadioButton stringRadioButton = createParameterRadioButton(editRadioGroup, validParameterString);
                editRadioGroup.addView(stringRadioButton);
                if (droidParameterValue.toLowerCase(Locale.ROOT).equals(validParameterString.toLowerCase(Locale.ROOT)))
                    stringRadioButton.setChecked(true);
            }
        }
    }

    /**
     * Ux cancel mission timer.
     *
     * @param cancelService the cancel service
     */
    private void uxCancelMissionTimer(boolean cancelService) {
        // change the visibilities here and then send a cancel message to the service:
        mMissionStartTimerContainer.setVisibility(View.GONE);
        mFloidViewFrames.setVisibility(View.VISIBLE);
        if (cancelService) {
            sendCancelMissionToFloidService();
        }
    }

    /**
     * Start interface input.
     */
    @SuppressWarnings("EmptyMethod")
    private void startInterfaceInput() {
        // Currently does nothing
    }

    /**
     * Stop interface input.
     */
    @SuppressWarnings("EmptyMethod")
    private void stopInterfaceInput() {
        // Currently does nothing
    }

    /**
     * Add message to status text.
     *
     * @param message the message
     */
    private void addMessageToStatusText(String message) {
        if (message == null) {
            String fullStackTrace = org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace(new Exception());
            Log.e(TAG, "addMessageToStatusText: Null message: " + fullStackTrace);
            return;
        }
        try {
            String debugOutput = floidDebugTokenFilter.filter(message);
            if ((debugOutput != null) && (debugOutput.length() > 0)) {
                mStatusText.append(debugOutput);
                if (mAutoScrollStatusCheckBox.isChecked()) {
                    mStatusScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "addMessageToStatusText: Failed to debug token filter Message: " + message + " Error: " + e.getMessage());
        }
    }

    /**
     * Sets accessory icon.
     *
     * @param accessoryOpen the accessory open
     */
    private void setAccessoryIcon(boolean accessoryOpen) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (accessoryOpen) {
                actionBar.setIcon(mAccessoryOpenDrawable);
            } else
                actionBar.setIcon(mAccessoryClosedDrawable);
        }
    }

    /**
     * The floid service connection.
     */
    private final ServiceConnection mFloidServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder binder) {
            if(mFloidActivityUseLogger)
                Log.i(TAG, "Connected to Floid Service");
            // Get a messenger for this service:
            mFloidServiceOutgoingMessenger = new Messenger(binder);
            mFloidServiceIsBound = true;
            Toast.makeText(FloidActivity.this, "Floid Service Connected", Toast.LENGTH_SHORT).show();
            // Ping the Floid service one time:
            if(mFloidActivityUseLogger)
                Log.d(TAG, "Sending ping message to Floid Service");
            sendPingToFloidService();  // This passes back our handler in the reply to...
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // Set our messenger to null and denote we are no longer bound:
            mFloidServiceOutgoingMessenger = null;
            mFloidServiceIsBound = false;
        }
    };

    /**
     * Send ping to floid service.
     */
    private void sendPingToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Ping");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_PING);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Ping");
                } catch (Exception e) {
                    Log.e(TAG, "sendPingToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendPingToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendPingToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send cancel mission to floid service.
     */
    private void sendCancelMissionToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Cancel Mission");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_CANCEL_MISSION_TIMER);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Cancel Mission");
                } catch (Exception e) {
                    Log.e(TAG, "sendCancelMissionToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendCancelMissionToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendCancelMissionToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send set log floid service.
     */
    private void sendSetLogFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Set Log");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_SET_LOG);
                    floidServiceOutgoingMessage.getData().putBoolean(FloidService.BUNDLE_KEY_LOG, mFloidActivityUseLogger);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, String.format("Set Log: %b", mFloidActivityUseLogger));
                } catch (Exception e) {
                    if(mFloidActivityUseLogger)
                        Log.w(TAG, "sendSetLogFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendSetLogFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            Log.d(TAG, "sendCancelMissionToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send get floid ids to floid service.
     * This is unused because when we are created we ping the floid service, and it returns these ids
     */
    @SuppressWarnings("unused")
    private void sendGetFloidIdsToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Get Floid Ids");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_GET_FLOID_IDS);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Get Floid Id");
                } catch (Exception e) {
                    Log.e(TAG, "sendGetFloidIdToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendGetFloidIdToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendGetFloidIdToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send get floid droid status to floid service.
     */
// This is unused because when we are created we ping the floid service, and it returns these ids:
    @SuppressWarnings("unused")
    private void sendGetFloidDroidStatusToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Get Floid Droid Status");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_GET_FLOID_DROID_STATUS);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Get Floid Droid Status");
                } catch (Exception e) {
                    Log.e(TAG, "sendGetFloidDroidStatusToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendGetFloidDroidStatusToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendGetFloidDroidStatusToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send get floid status to floid service.
     */
// This is unused because when we are created we ping the floid service, and it returns these ids:
    @SuppressWarnings("unused")
    private void sendGetFloidStatusToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Get Floid Status");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_GET_FLOID_STATUS);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Get Floid Status");
                } catch (Exception e) {
                    Log.e(TAG, "sendGetFloidStatusToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendGetFloidStatusToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendGetFloidStatusToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send get model parameters to floid service.
     */
// This is unused because when we are created we ping the floid service, and it returns these ids:
    @SuppressWarnings("unused")
    private void sendGetModelParametersToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Get Model Parameters");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_GET_MODEL_PARAMETERS);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Get Model Parameters");
                } catch (Exception e) {
                    Log.e(TAG, "sendGetModelParametersToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendGetModelParametersToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendGetModelParametersToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send get log to floid service.
     */
// This is unused because when we are created we ping the floid service, and it returns these ids:
    @SuppressWarnings("unused")
    private void sendGetLogToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Get Log");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_GET_LOG);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Get Log");
                } catch (Exception e) {
                    Log.e(TAG, "sendGetLogToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendGetLogToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendGetLogToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send get host to floid service.
     */
// This is unused because when we are created we ping the floid service, and it returns these ids:
    @SuppressWarnings("unused")
    private void sendGetHostToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Get Host");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_GET_HOST);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Get Host");
                } catch (Exception e) {
                    Log.e(TAG, "sendGetHostToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendGetHostToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendGetHostToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send get client communicator status to floid service.
     */
    // This is unused because when we are created we ping the floid service, and it returns these ids:
    @SuppressWarnings("unused")
    private void sendGetClientCommunicatorStatusToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Get Client Communicator Status");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_GET_CLIENT_COMMUNICATOR_STATUS);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Get Client Communicator Status");
                } catch (Exception e) {
                    Log.e(TAG, "sendGetClientCommunicatorStatusToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendGetClientCommunicatorStatusToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendGetClientCommunicatorStatusToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send set client communicator status to floid service.
     *
     * @param enable enable flag
     */
    private void sendSetClientCommunicatorStatusToFloidService(boolean enable) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Set Client Communicator Status");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_SET_CLIENT_COMMUNICATOR_STATUS);
                    floidServiceOutgoingMessage.getData().putBoolean(FloidService.BUNDLE_KEY_CLIENT_COMMUNICATOR, enable);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Set Client Communicator Status");
                } catch (Exception e) {
                    Log.e(TAG, "sendSetClientCommunicatorStatusToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendSetClientCommunicatorStatusToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendSetClientCommunicatorStatusToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send relay command to floid service.
     *
     * @param relayDevice      the relay device
     * @param relayDeviceState the relay device state
     */
    public void sendRelayCommandToFloidService(byte relayDevice, byte relayDeviceState) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Relay Command");
        try {
            FloidOutgoingCommand relayOutgoingCommand = new FloidOutgoingCommand(FloidService.RELAY_PACKET, FloidService.RELAY_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
            relayOutgoingCommand.setByteToBuffer(FloidService.RELAY_PACKET_DEVICE_OFFSET, relayDevice);
            relayOutgoingCommand.setByteToBuffer(FloidService.RELAY_PACKET_STATE_OFFSET, relayDeviceState);
            sendCommandToFloidService(relayOutgoingCommand);
        } catch (Exception e) {
            if (mFloidActivityUseLogger)
                Log.e(TAG, "Failed to make/send Relay command: " + e + " " + Log.getStackTraceString(e) + " " + e);
        }
    }

    /**
     * Send droid parameters command to floid service.
     *
     * @param droidParameters the droid parameters
     */
    private void sendSetDroidParametersToFloidService(HashMap<String, String> droidParameters) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Set Droid Parameters");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_SET_DROID_PARAMETERS);
                    HashMapStringToStringParcelable hashMapStringToStringParcelable = new HashMapStringToStringParcelable();
                    hashMapStringToStringParcelable.setStringToStringHashMap(droidParameters);
                    floidServiceOutgoingMessage.getData().putParcelable(FloidService.BUNDLE_KEY_FLOID_DROID_PARAMETERS, hashMapStringToStringParcelable);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
                } catch (Exception e) {
                    Log.e(TAG, "sendSetDroidParametersToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendSetDroidParametersToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendSetDroidParametersToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send pyr packet command to floid service.
     *
     * @param pitch       the pitch
     * @param heading     the heading
     * @param roll        the roll
     * @param altitude    the altitude
     * @param temperature the temperature
     */
    public void sendPyrPacketCommandToFloidService(float pitch, float heading, float roll, float altitude, float temperature) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Pyr Packet Command");
        try {
            FloidOutgoingCommand pyrPacketOutgoingCommand = new FloidOutgoingCommand(FloidService.DEVICE_PYR_PACKET, FloidService.DEVICE_PYR_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
            pyrPacketOutgoingCommand.setFloatToBuffer(FloidService.DEVICE_PYR_OFFSET_PITCH, pitch);
            pyrPacketOutgoingCommand.setFloatToBuffer(FloidService.DEVICE_PYR_OFFSET_HEADING, heading);
            pyrPacketOutgoingCommand.setFloatToBuffer(FloidService.DEVICE_PYR_OFFSET_ROLL, roll);
            pyrPacketOutgoingCommand.setFloatToBuffer(FloidService.DEVICE_PYR_OFFSET_ALTITUDE, altitude);
            pyrPacketOutgoingCommand.setFloatToBuffer(FloidService.DEVICE_PYR_OFFSET_TEMPERATURE, temperature);
            sendCommandToFloidService(pyrPacketOutgoingCommand);
            sendPlayNotificationToFloidService(FloidService.FLOID_ALERT_SOUND_PYR_PACKET_SENT);
        } catch (Exception e) {
            if (mFloidActivityUseLogger)
                Log.e(TAG, "Failed to make/send PYR Packet command: " + e + " " + Log.getStackTraceString(e));
        }
    }

    /**
     * Send command to floid service.
     *
     * @param floidOutgoingCommand the floid outgoing command
     */
    public void sendCommandToFloidService(FloidOutgoingCommand floidOutgoingCommand) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Command");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_SEND_COMMAND_TO_FLOID);
                    floidServiceOutgoingMessage.getData().putParcelable(FloidService.BUNDLE_KEY_OUTGOING_COMMAND, floidOutgoingCommand);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Send Command to Floid");
                } catch (Exception e) {
                    Log.e(TAG, "sendCommandToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendCommandToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendCommandToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send floid thread timings to floid service.
     *
     * @param floidThreadTimings the floid thread timings
     */
    private void sendFloidThreadTimingsToFloidService(FloidThreadTimings floidThreadTimings) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Thread Timings");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_UPDATE_THREAD_TIMINGS);
                    floidServiceOutgoingMessage.getData().putParcelable(FloidService.BUNDLE_KEY_THREAD_TIMINGS, floidThreadTimings);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Update Thread Timings");
                } catch (Exception e) {
                    Log.e(TAG, "sendFloidThreadTimingsFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendFloidThreadTimingsFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendFloidThreadTimingsFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send mission mode to floid service.
     *
     * @param missionMode the mission mode
     */
    public void sendMissionModeToFloidService(int missionMode) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Mission Mode");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MISSION_MODE);
                    floidServiceOutgoingMessage.getData().putInt(FloidService.BUNDLE_KEY_MISSION_MODE, missionMode);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "Floid Service was sent: Mission Mode");
                } catch (Exception e) {
                    Log.e(TAG, "sendMissionModeToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendMissionModeToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendFloidThreadTimingsFloidService: Floid Service Is Not Bound");
        }
    }
    /**
     * Send play notification to floid service.
     *
     * @param notification the notification
     */
    private void sendPlayNotificationToFloidService(int notification) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Play Notification");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_PLAY_NOTIFICATION);
                    floidServiceOutgoingMessage.getData().putInt(FloidService.BUNDLE_KEY_NOTIFICATION, notification);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
                    Log.d(TAG, "Play Notification: " + notification);
                } catch (Exception e) {
                    Log.e(TAG, "sendPlayNotificationFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendPlayNotificationFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendPlayNotificationFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send floid id to floid service.
     *
     * @param floidId the floid id
     */
    private void sendFloidIdToFloidService(int floidId) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Floid Id");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_SET_FLOID_ID);
                    floidServiceOutgoingMessage.getData().putInt(FloidService.BUNDLE_KEY_FLOID_ID, floidId);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
                    Log.d(TAG, "New Floid Id: " + floidId);
                } catch (Exception e) {
                    Log.e(TAG, "sendFloidIdFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendFloidIdFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendFloidIdFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send upload firmware to floid service.
     *
     */
    private void sendUploadFirmwareToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Upload Firmware");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_UPLOAD_FIRMWARE);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
                } catch (Exception e) {
                    Log.e(TAG, "sendUploadFirmwareToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendUploadFirmwareToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendUploadFirmwareToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send floid model to service.
     *
     * @param floidModelParameters the floid model parameters
     */
    private void sendFloidModelToService(FloidModelParametersParcelable floidModelParameters) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Floid Model");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_SET_MODEL_PARAMETERS);
                    floidServiceOutgoingMessage.getData().putParcelable(FloidService.BUNDLE_KEY_FLOID_MODEL_PARAMETERS, floidModelParameters);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Floid Model");
                } catch (Exception e) {
                    Log.e(TAG, "sendFloidModelToService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.d(TAG, "sendFloidModelToService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.d(TAG, "sendFloidModelToService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send host to floid service.
     *
     * @param hostAddress the host address
     * @param hostPort the host port
     */
    private void sendHostToFloidService(String hostAddress, int hostPort) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Host");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_SET_HOST);
                    floidServiceOutgoingMessage.getData().putString(FloidService.BUNDLE_KEY_HOST_ADDRESS, hostAddress);
                    floidServiceOutgoingMessage.getData().putInt(FloidService.BUNDLE_KEY_HOST_PORT, hostPort);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
                    if(mFloidActivityUseLogger)
                        Log.i(TAG, String.format("New Floid Server:  %s:%d", hostAddress, hostPort));
                } catch (Exception e) {
                    Log.e(TAG, "sendHostFloidService: Caught remote exception", e);
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendHostFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendHostFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send mode to floid service.
     *
     * @param mode the mode
     */
// This is not used because the round trip for a mode change is to have it go to the Floid and come back in the floid status
    @SuppressWarnings("unused")
    private void sendModeToFloidService(int mode) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Mode");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_SET_MODE);
                    floidServiceOutgoingMessage.getData().putInt(FloidService.BUNDLE_KEY_MODE, mode);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
                    if(mFloidActivityUseLogger)
                        Log.i(TAG, "New Mode: " + mode);
                } catch (Exception e) {
                    Log.e(TAG, "sendModeToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendModeToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendModeToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send the take photo command to the floid service
     */
    private void sendTakePhotoToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Take Photo");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_TAKE_PHOTO);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Take Photo");
                } catch (Exception e) {
                    Log.e(TAG, "sendTakePhotoToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendTakePhotoToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendTakePhotoToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send the start video command to the floid service
     */
    private void sendStartVideoToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Start Video");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_START_VIDEO);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Start Video");
                } catch (Exception e) {
                    Log.e(TAG, "sendStartVideoToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendStartVideoToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendStartVideoToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Send the stop video command to the floid service
     */
    private void sendStopVideoToFloidService() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Stop Video");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_STOP_VIDEO);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Stop Video");
                } catch (Exception e) {
                    Log.e(TAG, "sendStopVideoToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendStopVideoToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendStopVideoToFloidService: Floid Service Is Not Bound");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "Resuming");
        playActivitySound(FLOID_ACTIVITY_SOUND_RESUME);
        quitting = false;
        // Create a new messenger to get messages from the Floid Accessory Communicator Service:
        mFloidServiceIncomingMessenger = new Messenger(mFloidDroidMessageHandler);
        // If we were started via a USB device attachment, then we need to let the service know:
        // Bind to the accessory communicator service:
        UsbAccessory usbAccessory = getIntent().getParcelableExtra(UsbManager.EXTRA_ACCESSORY, UsbAccessory.class);
        if (usbAccessory != null) {
            String fullAccessoryName = usbAccessory.getManufacturer() + " " + usbAccessory.getModel() + " " + usbAccessory.getVersion();
            if(mFloidActivityUseLogger)
                Log.d(TAG, "USB Attached: " + fullAccessoryName);
            addMessageToStatusText("USB accessory in onResume Intent: " + fullAccessoryName + "\n");
            mFloidServiceIntent.putExtra(UsbManager.EXTRA_ACCESSORY, usbAccessory);
        }
        startService(mFloidServiceIntent);
        bindService(mFloidServiceIntent, mFloidServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onPause() {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "Pausing");
        playActivitySound(FLOID_ACTIVITY_SOUND_PAUSE);
        // Create a new messenger to get messages from the Floid Accessory Communicator Service:
        mFloidServiceIncomingMessenger = null;
        // Unbind from the FloidService:
        unbindService(mFloidServiceConnection);
        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getAction() != null) {
            if(mFloidActivityUseLogger)
                Log.d(TAG, "New intent: " + intent.getAction());
            addMessageToStatusText("New intent: " + intent.getAction() + "\n");
            super.onNewIntent(intent);
            switch (intent.getAction()) {
                case UsbManager.ACTION_USB_DEVICE_ATTACHED:
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "Intent: device attached");
                    addMessageToStatusText("Intent: device attached\n");
                    UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE, UsbDevice.class);
                    if (usbDevice != null) {
                        Log.d(TAG, "Intent: found device");
                        addMessageToStatusText("Intent: found device\n");
                    }
                    break;
                case UsbManager.ACTION_USB_ACCESSORY_ATTACHED:
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "Intent: usb accessory attached");
                    addMessageToStatusText("Intent: usb accessory attached\n");
                    UsbAccessory usbAccessory = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY, UsbAccessory.class);
                    if (usbAccessory != null) {
                        if(mFloidActivityUseLogger)
                            Log.d(TAG, "Intent: found usb accessory");
                        addMessageToStatusText("Intent: found usb accessory\n");
                        sendOpenAccessoryToFloidService(usbAccessory);
                    }
                    break;
                default:
                    if(mFloidActivityUseLogger)
                        Log.d(TAG, "Intent: not recognized");
                    addMessageToStatusText("Intent: not recognized\n");
                    break;
            }
        } else {
            if(mFloidActivityUseLogger)
                Log.d(TAG, "New intent: with null action");
            addMessageToStatusText("New intent: with null action\n");
        }
    }

    /**
     * Send open accessory message to floid service.
     *
     * @param usbAccessory the accessory for the service to open
     */
    private void sendOpenAccessoryToFloidService(UsbAccessory usbAccessory) {
        if(mFloidActivityUseLogger)
            Log.d(TAG, "UX -> Floid Service: Open Accessory");
        if (mFloidServiceIsBound) {
            if (mFloidServiceOutgoingMessenger != null) {
                try {
                    Message floidServiceOutgoingMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_OPEN_USB_ACCESSORY);
                    floidServiceOutgoingMessage.getData().putParcelable(FloidService.BUNDLE_KEY_USB_ACCESSORY, usbAccessory);
                    floidServiceOutgoingMessage.replyTo = mFloidServiceIncomingMessenger;
                    mFloidServiceOutgoingMessenger.send(floidServiceOutgoingMessage);
                    addMessageToStatusText("Floid Service was sent: Open USB Accessory\n");
//                    Log.d(TAG, "FLOID SERVICE WAS SENT: Open USB Accessory: " + usbAccessory.getModel());
                } catch (Exception e) {
                    addMessageToStatusText("Failed to sendFloid Service : Open USB Accessory - remote exception: " + e.getMessage() + "\n");
                    Log.e(TAG, "sendOpenAccessoryToFloidService: Caught remote exception " + e.getMessage());
                }
            } else {
                addMessageToStatusText("Failed to sendFloid Service : Open USB Accessory - messenger null\n");
                if(mFloidActivityUseLogger)
                    Log.w(TAG, "sendOpenAccessoryToFloidService: Floid Service Outgoing Messenger is null");
            }
        } else {
            addMessageToStatusText("Failed to sendFloid Service : Open USB Accessory - not bound\n");
            if(mFloidActivityUseLogger)
                Log.w(TAG, "sendOpenAccessoryToFloidService: Floid Service Is Not Bound");
        }
    }

    /**
     * Set up sounds for activity.
     */
    private void setupActivitySounds() {
        // Set up sounds for Activity:
        mActivitySoundResumeMediaPlayer = MediaPlayer.create(this, R.raw.activity_sound_resume);
        mActivitySoundPauseMediaPlayer = MediaPlayer.create(this, R.raw.activity_sound_pause);
    }

    /**
     * Play an activity sound
     * @param sound the sound
     */
    private void playActivitySound(int sound) {
        switch(sound) {
            case FLOID_ACTIVITY_SOUND_RESUME: {
                if (mActivitySoundResumeMediaPlayer != null) {
                    mActivitySoundResumeMediaPlayer.start();
                }
            }
            case FLOID_ACTIVITY_SOUND_PAUSE: {
                if (mActivitySoundPauseMediaPlayer != null) {
                    mActivitySoundPauseMediaPlayer.start();
                }
            }
        }
    }
}
