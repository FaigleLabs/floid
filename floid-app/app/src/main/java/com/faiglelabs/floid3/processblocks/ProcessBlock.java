/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

import android.util.Log;

import com.faiglelabs.floid3.FloidService;

/**
 * Generic process block for storing command processing state
 */
public class ProcessBlock
{
    /**
     * The constant TAG.
     */
    private static final String TAG = "ProcessBlock";
    // Non-completion states:
    /**
     * Process block not started constant (not started, non-completion)
     */
    public  static final int  PROCESS_BLOCK_NOT_STARTED                   = 0;
    /**
     * Process block started constant (non-completion)
     */
    public  static final int  PROCESS_BLOCK_STARTED                       = 1;
    /**
     * Process block waiting for command response constant (non-completion)
     */
    public  static final int  PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE  = 2;
    /**
     * Process block command response received constant (non-completion)
     */
    public  static final int  PROCESS_BLOCK_COMMAND_RESPONSE_RECEIVED     = 3;
    /**
     * Process block in progress constant (non-completion)
     */
    public  static final int  PROCESS_BLOCK_IN_PROGRESS                   = 4;
    /**
     * Process block waiting for floid to have a goal
     */
    public  static final int  PROCESS_BLOCK_WAITING_FOR_FLOID_GOAL        = 5;
    /**
     * Process block waiting for matching goal id
     */
    public  static final int  PROCESS_BLOCK_WAITING_FOR_MATCHING_GOAL_ID  = 6;
    /**
     * Process block waiting to achieve goal
     */
    public  static final int  PROCESS_BLOCK_WAITING_TO_ACHIEVE_GOAL       = 7;
    // Completion States:
    /**
     * Process block completed success constant (completion success)
     */
    public  static final int  PROCESS_BLOCK_COMPLETED_SUCCESS             = 20;
    /**
     * Process block completed error constant (completion fail)
     */
    public  static final int  PROCESS_BLOCK_COMPLETED_ERROR               = 21;
    /**
     * Process block completed timeout error constant (completion fail)
     */
    public  static final int  PROCESS_BLOCK_COMPLETED_TIMEOUT_ERROR       = 22;
    /**
     * Process block completed logic error constant (completion fail)
     */
    public  static final int  PROCESS_BLOCK_COMPLETED_LOGIC_ERROR         = 23;
    /**
     * Process block completed no floid error constant (completion fail)
     */
    public  static final int  PROCESS_BLOCK_COMPLETED_NO_FLOID_ERROR      = 24;
    /**
     * Process block completed communication error constant (completion fail)
     */
    public  static final int  PROCESS_BLOCK_COMPLETED_COMMUNICATION_ERROR = 25;
    /**
     * Process block completed cancelled constant (completion fail)
     */
    public  static final int  PROCESS_BLOCK_COMPLETED_CANCELLED           = 26;

    /**
     * Process block default timeout interval constant (default: 1000L)
     */
    private static final long PROCESS_BLOCK_DEFAULT_TIMEOUT_INTERVAL      = 1000L;   // Defaults to 1 second timeout per instruction...
    /**
     * Process block instruction name
     */
    private String instructionName;
    /**
     * Process block status
     */
    private              int  processBlockStatus;
    /**
     * Process block timeout interval
     */
    private              long processBlockTimeoutInterval;
    /**
     * Process block start time
     */
    private              long processBlockStartTime;
    /**
     * Process block process count
     */
    private              long processBlockProcessCount;
    /**
     * Floid goal id for this process block
     */
    private              int  floidGoalId                             = FloidService.NO_GOAL_ID;     // -1 is default to no goal...
    /**
     * Process block command number for which to wait
     */
    private              int  processBlockWaitForCommandNumber        = -1;
    /**
     * Process block does not timeout constant
     */
    @SuppressWarnings("WeakerAccess")
    public static final  int  COMMAND_DOES_NOT_TIMEOUT                = -1;

    /**
     * Create a process block with this timeout interface
     * @param timeoutInterval the timeout interval for this command or COMMAND_DOES_NOT_TIMEOUT
     * @param instructionName the instruction name
     */
    protected ProcessBlock(long timeoutInterval, String instructionName) {
        setInstructionName(instructionName);
        processBlockStatus = PROCESS_BLOCK_NOT_STARTED;
        processBlockTimeoutInterval = PROCESS_BLOCK_DEFAULT_TIMEOUT_INTERVAL;
        processBlockStartTime = System.currentTimeMillis();
        processBlockProcessCount = 0;
        if (timeoutInterval < 0) {
            // No timeout
            this.processBlockTimeoutInterval = COMMAND_DOES_NOT_TIMEOUT;   // Does not time out
        } else {
            if (timeoutInterval == 0) {
                // Default timeout:
                this.processBlockTimeoutInterval = PROCESS_BLOCK_DEFAULT_TIMEOUT_INTERVAL;
            } else {
                // Manual timeout:
                this.processBlockTimeoutInterval = timeoutInterval;
            }
        }
    }

    /**
     * Get the floid goal id for this process block
     * @return the floid goal id for ths process block
     */
    public int getFloidGoalId() {
        return floidGoalId;
    }

    /**
     * Set the floid goal id for this process block
     * @param floidGoalId the floid goal id
     */
    public void setFloidGoalId(int floidGoalId) {
        this.floidGoalId = floidGoalId;
    }

    /**
     * Get the process block status
     * @return the process block status
     */
    public int getProcessBlockStatus() {
        return processBlockStatus;
    }

    /**
     * Set the process block status
     * @param processBlockStatus the process block status
     */
    public void setProcessBlockStatus(int processBlockStatus) {
        switch (processBlockStatus) {
            case PROCESS_BLOCK_NOT_STARTED:
            case PROCESS_BLOCK_STARTED:
            case PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE:
            case PROCESS_BLOCK_COMMAND_RESPONSE_RECEIVED:
            case PROCESS_BLOCK_IN_PROGRESS:
            case PROCESS_BLOCK_COMPLETED_SUCCESS:
            case PROCESS_BLOCK_COMPLETED_ERROR:
            case PROCESS_BLOCK_COMPLETED_TIMEOUT_ERROR:
            case PROCESS_BLOCK_COMPLETED_LOGIC_ERROR:
            case PROCESS_BLOCK_COMPLETED_NO_FLOID_ERROR:
            case PROCESS_BLOCK_COMPLETED_COMMUNICATION_ERROR:
                this.processBlockStatus = processBlockStatus;
                break;
            default:
                Log.e(TAG, "setProcessBlockStatus: Bad process block status: " + processBlockStatus);
                break;
        }
    }

    /**
     * Get the process block start time
     * @return the process block start time
     */
    public long getProcessBlockStartTime() {
        return processBlockStartTime;
    }

    /**
     * Set the process block start time
     * @param processBlockStartTime the process block start time
     */
    public void setProcessBlockStartTime(long processBlockStartTime) {
        this.processBlockStartTime = processBlockStartTime;
    }
    /**
     * Get the process block timeout interval
     * @return the process block timeout interval
     */
    @SuppressWarnings("unused")
    public long getProcessBlockTimeoutInterval() {
        return processBlockTimeoutInterval;
    }

    /**
     * Set the process block timeout interval
     * @param processBlockTimeoutInterval the process block timeout interval or COMMAND_DOES_NOT_TIMEOUT
     */
    @SuppressWarnings("unused")
    public void setProcessBlockTimeoutInterval(long processBlockTimeoutInterval)
    {
        this.processBlockTimeoutInterval = processBlockTimeoutInterval;
    }

    /**
     * Has the process block timed out
     * @return true if the process block has timed out
     */
    public boolean isTimedOut()
    {
        // Check for no timeout flag:
        if(processBlockTimeoutInterval <= 0) // Note: non-specific check
        {
//            if(mFloidService.mUseLogger) Log.i(FloidService.TAG, "  no timeout interval");
            return false;
        }
        long runningTime = System.currentTimeMillis() - processBlockStartTime;
//        if(FloidService.mUseLogger) Log.i(FloidService.TAG, "  checking time out - running: " + runningTime + " max: " + processBlockTimeoutInterval + " left: " + (processBlockTimeoutInterval - runningTime));
        // Otherwise see if we have timed out:
        return (runningTime > processBlockTimeoutInterval);
    }

    /**
     * get the process block process count
     * @return the process block process count
     */
    @SuppressWarnings("unused")
    public long getProcessBlockProcessCount()
    {
        return processBlockProcessCount;
    }

    /**
     * Set the process block process count
     * @param processBlockProcessCount the process block process count
     */
    public void setProcessBlockProcessCount(@SuppressWarnings("SameParameterValue") long processBlockProcessCount)
    {
        this.processBlockProcessCount = processBlockProcessCount;
    }

    /**
     * Bump the process block process count
     */
    public void increaseProcessBlockProcessCount()
    {
        ++processBlockProcessCount;
    }

    /**
     * Get the process block wait for command buffer
     * @return the process block wait for command buffer
     */
    public int getProcessBlockWaitForCommandNumber()
    {
        return processBlockWaitForCommandNumber;
    }

    /**
     * Set the process block wait for command buffer
     * @param processBlockWaitForCommandNumber the process block wait for command buffer
     */
    public void setProcessBlockWaitForCommandNumber(
            int processBlockWaitForCommandNumber)
    {
        this.processBlockWaitForCommandNumber = processBlockWaitForCommandNumber;
    }

    /**
     * Get the instruction name
     * @return the instruction name
     */
    public String getInstructionName() {
        return instructionName;
    }

    /**
     * Set the instruction name
     * @param instructionName the instruction name
     */
    @SuppressWarnings("WeakerAccess")
    public void setInstructionName(String instructionName) {
        this.instructionName = instructionName;
    }
}
