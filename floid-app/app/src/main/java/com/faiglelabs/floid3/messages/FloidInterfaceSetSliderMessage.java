/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.messages;

import com.helloandroid.android.horizontalslider.HorizontalSlider;

/**
 * Interface for slider message
 */
public class FloidInterfaceSetSliderMessage {
    private int value;
    private HorizontalSlider slider;

    /**
     * Create a slider message for this slider
     *
     * @param slider the slider
     * @param value  the value
     */
    public FloidInterfaceSetSliderMessage(HorizontalSlider slider, int value) {
        this.slider = slider;
        this.value = value;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @return the slider
     */
    public HorizontalSlider getSlider() {
        return slider;
    }

    /**
     * @param value the value to set
     */
    @SuppressWarnings("unused")
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * @param slider the slider to set
     */
    @SuppressWarnings("unused")
    public void setSlider(HorizontalSlider slider) {
        this.slider = slider;
    }
}
