package com.faiglelabs.floid3.parcelables;

import android.os.Parcel;
import android.os.Parcelable;
import com.faiglelabs.floid3.DroidValidParameters;

import java.util.HashMap;

/**
 * Parcelable version of HashMap of String to DroidValidParameters
 */
public class HashMapStringToDroidValidParametersParcelable implements Parcelable {

    /**
     * The string to droid valid parameters hashmap.
     */
    private HashMap<String, DroidValidParameters> stringToDroidValidParametersHashMap = new HashMap<>();

    /**
     * Get the hashmap
     * @return the hashmap
     */
    public HashMap<String, DroidValidParameters> getStringToDroidValidParametersHashMap() {
        return stringToDroidValidParametersHashMap;
    }

    /**
     * Set the hashmap
     * @param stringToDroidValidParametersHashMap the hashmap
     */
    public void setStringToDroidValidParametersHashMap(HashMap<String, DroidValidParameters> stringToDroidValidParametersHashMap) {
        this.stringToDroidValidParametersHashMap = stringToDroidValidParametersHashMap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.stringToDroidValidParametersHashMap);
    }

    /**
     * Constructor
     */
    public HashMapStringToDroidValidParametersParcelable() {
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected HashMapStringToDroidValidParametersParcelable(Parcel in) {
        //noinspection unchecked
        this.stringToDroidValidParametersHashMap = (HashMap<String, DroidValidParameters>) in.readSerializable();
    }

    /**
     * Static CREATOR
     */
    public static final Creator<HashMapStringToDroidValidParametersParcelable> CREATOR = new Creator<HashMapStringToDroidValidParametersParcelable>() {
        @Override
        public HashMapStringToDroidValidParametersParcelable createFromParcel(Parcel source) {
            return new HashMapStringToDroidValidParametersParcelable(source);
        }

        @Override
        public HashMapStringToDroidValidParametersParcelable[] newArray(int size) {
            return new HashMapStringToDroidValidParametersParcelable[size];
        }
    };
}
