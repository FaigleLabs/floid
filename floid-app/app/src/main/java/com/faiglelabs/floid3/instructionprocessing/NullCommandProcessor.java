/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import com.faiglelabs.floid.servertypes.commands.NullCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.NullProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for the null command
 */
public class NullCommandProcessor extends InstructionProcessor
{
	/**
	 * Create a null command processor
	 * @param floidService the floid service
	 * @param nullCommand the null command
	 */
	@SuppressWarnings("WeakerAccess")
	public NullCommandProcessor(FloidService floidService, NullCommand nullCommand)
	{
		super(floidService, nullCommand);
		useDefaultGoalProcessing = true;
		completeOnCommandResponse = true;
		setProcessBlock(new NullProcessBlock(noGoalTimeout));
	}
	@Override
	public boolean setupInstruction()
	{
        super.setupInstruction();  // Sets processBlock to started

        
        // Logic: Set us up to get a set number of GPS hits and determine if we have a good home position - ultimately time out after a period of time
        return false;

	}
	@Override
	public boolean processInstruction()
	{
	    if(super.processInstruction())
	    {
	        return true;   // We are already done...
	    }
        NullProcessBlock nullProcessBlock = (NullProcessBlock)processBlock;
        nullProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
        return true;
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
