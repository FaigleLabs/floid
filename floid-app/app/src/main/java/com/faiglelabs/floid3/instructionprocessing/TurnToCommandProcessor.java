/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.TurnToCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.TurnToProcessBlock;

/**
 * Command processor for the turn to command
 */
public class TurnToCommandProcessor extends InstructionProcessor
{
    /**
     * Create a turn to command processor
     * @param floidService the floid service
     * @param turnToCommand the turn to command
     */
    @SuppressWarnings("WeakerAccess")
	public TurnToCommandProcessor(FloidService floidService, TurnToCommand turnToCommand)
	{
		super(floidService, turnToCommand);
        useDefaultGoalProcessing = true;
        completeOnCommandResponse = false;
		setProcessBlock(new TurnToProcessBlock(noGoalTimeout));  // Does not time out...
	}
    @Override
    public boolean setupInstruction()
    {
        super.setupInstruction();
        // Log it:
        if(floidService.mUseLogger) Log.d(FloidService.TAG, "===>SETUP " + this.getDroidInstruction().getType());
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
        TurnToCommand turnToCommand = (TurnToCommand)droidInstruction;
        TurnToProcessBlock turnToProcessBlock = (TurnToProcessBlock)processBlock;
        turnToProcessBlock.setFloidGoalId(floidService.getNextFloidGoalId());
        try
        {
            FloidOutgoingCommand turnToOutgoingCommand = floidService.sendTurnToCommandToFloid(turnToProcessBlock.getFloidGoalId(), (byte)(turnToCommand.getFollowMode()?1:0),(float)turnToCommand.getHeading());
            if(turnToOutgoingCommand != null)
            {
                turnToProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                turnToProcessBlock.setProcessBlockWaitForCommandNumber(turnToOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Turn To command.");
                // We must have failed - set completion with error:
                turnToProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;  // We are done with this whole command
    }
    @Override
    public boolean tearDownInstruction()
    {
        super.tearDownInstruction();
        return true;
    }
}
