/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

import android.widget.EditText;

/**
 * Decimal with max edit text validator
 */
public class FloidDecimalWithMaxEditTextValidator extends FloidDecimalEditTextValidator {
    private final EditText mMaxValueEditText;

    /**
     * Create a decimal with max edit text validator
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the callback
     * @param useMinValue                    use min value
     * @param minValue                       min value
     * @param useMaxValue                    use max value
     * @param maxValue                       max value
     * @param maxValueEditText               maxValueEditText
     */
    public FloidDecimalWithMaxEditTextValidator(EditText editText,
                                                FloidEditTextValidatorCallback floidEditTextValidatorCallback,
                                                @SuppressWarnings("SameParameterValue") boolean useMinValue,
                                                double minValue,
                                                @SuppressWarnings("SameParameterValue") boolean useMaxValue,
                                                double maxValue,
                                                EditText maxValueEditText) {
        super(editText,
                floidEditTextValidatorCallback,
                useMinValue,
                minValue,
                useMaxValue,
                maxValue);
        mMaxValueEditText = maxValueEditText;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        double requiredMax;
        double myValue;
        try {
            requiredMax = Double.valueOf(mMaxValueEditText.getText().toString());
        } catch (Exception e) {
            mMaxValueEditText.setError("Bad Format");
            return;
        }
        try {
            myValue = Double.valueOf(mEditText.getText().toString());
        } catch (Exception e) {
            mEditText.setError("Bad Format");
            return;
        }
        if (myValue > requiredMax) {
            mEditText.setError("Above Max");
            return;
        }
        super.onTextChanged(s, start, before, count);
    }
}
