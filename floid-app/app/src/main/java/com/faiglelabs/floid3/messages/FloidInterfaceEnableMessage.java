/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.messages;

import android.view.View;

/**
 * Floid interface enable message
 */
public class FloidInterfaceEnableMessage {
    private View view;
    private boolean enable;

    /**
     * Create an interface enabled message
     *
     * @param view   the view
     * @param enable true if enable
     */
    public FloidInterfaceEnableMessage(View view, boolean enable) {
        super();
        this.view = view;
        this.enable = enable;
    }

    /**
     * @return the view
     */
    public View getView() {
        return view;
    }

    /**
     * @return the enable
     */
    public boolean isEnable() {
        return enable;
    }

    /**
     * @param view the view to set
     */
    @SuppressWarnings("unused")
    public void setView(View view) {
        this.view = view;
    }

    /**
     * @param enable the enable to set
     */
    @SuppressWarnings("unused")
    public void setEnable(boolean enable) {
        this.enable = enable;
    }

}
