/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Timings for the floid threads
 */
class FloidThreadTimings implements Parcelable
{
    // Thread efficiency throttles:
    // ============================
    private              long   mMainThreadSleepTime                                  = 0L;
    private              long   mAccessoryCommunicatorNoAccessorySleepTime            = 0L;
    private              long   mAccessoryCommunicatorNoMessageSleepTime              = 0L;
    private              long   mAccessoryCommunicatorIncomingMessageSleepTime        = 0L;
    private              long   mAccessoryCommunicatorOutgoingMessageSleepTime        = 0L;
    /**
     * @return the mMainThreadSleepTime
     */
    public long getMainThreadSleepTime() {
        return mMainThreadSleepTime;
    }
    /**
     * @return the mAccessoryCommunicatorNoAccessorySleepTime
     */
    public long getAccessoryCommunicatorNoAccessorySleepTime() {
        return mAccessoryCommunicatorNoAccessorySleepTime;
    }
    /**
     * @return the mAccessoryCommunicatorNoMessageSleepTime
     */
    public long getAccessoryCommunicatorNoMessageSleepTime() {
        return mAccessoryCommunicatorNoMessageSleepTime;
    }
    /**
     * @return the mAccessoryCommunicatorIncomingMessageSleepTime
     */
    public long getAccessoryCommunicatorIncomingMessageSleepTime() {
        return mAccessoryCommunicatorIncomingMessageSleepTime;
    }
    /**
     * @return the mAccessoryCommunicatorOutgoingMessageSleepTime
     */
    public long getAccessoryCommunicatorOutgoingMessageSleepTime() {
        return mAccessoryCommunicatorOutgoingMessageSleepTime;
    }
    /**
     * @param mMainThreadSleepTime the mMainThreadSleepTime to set
     */
    public void setMainThreadSleepTime(long mMainThreadSleepTime) {
        this.mMainThreadSleepTime = mMainThreadSleepTime;
    }
    /**
     * @param mAccessoryCommunicatorNoAccessorySleepTime the mAccessoryCommunicatorNoAccessorySleepTime to set
     */
    public void setAccessoryCommunicatorNoAccessorySleepTime(
            long mAccessoryCommunicatorNoAccessorySleepTime) {
        this.mAccessoryCommunicatorNoAccessorySleepTime = mAccessoryCommunicatorNoAccessorySleepTime;
    }
    /**
     * @param mAccessoryCommunicatorNoMessageSleepTime the mAccessoryCommunicatorNoMessageSleepTime to set
     */
    public void setAccessoryCommunicatorNoMessageSleepTime(
            long mAccessoryCommunicatorNoMessageSleepTime) {
        this.mAccessoryCommunicatorNoMessageSleepTime = mAccessoryCommunicatorNoMessageSleepTime;
    }
    /**
     * @param mAccessoryCommunicatorIncomingMessageSleepTime the mAccessoryCommunicatorIncomingMessageSleepTime to set
     */
    public void setAccessoryCommunicatorIncomingMessageSleepTime(
            long mAccessoryCommunicatorIncomingMessageSleepTime) {
        this.mAccessoryCommunicatorIncomingMessageSleepTime = mAccessoryCommunicatorIncomingMessageSleepTime;
    }
    /**
     * @param mAccessoryCommunicatorOutgoingMessageSleepTime the mAccessoryCommunicatorOutgoingMessageSleepTime to set
     */
    public void setAccessoryCommunicatorOutgoingMessageSleepTime(
            long mAccessoryCommunicatorOutgoingMessageSleepTime) {
        this.mAccessoryCommunicatorOutgoingMessageSleepTime = mAccessoryCommunicatorOutgoingMessageSleepTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mMainThreadSleepTime);
        dest.writeLong(this.mAccessoryCommunicatorNoAccessorySleepTime);
        dest.writeLong(this.mAccessoryCommunicatorNoMessageSleepTime);
        dest.writeLong(this.mAccessoryCommunicatorIncomingMessageSleepTime);
        dest.writeLong(this.mAccessoryCommunicatorOutgoingMessageSleepTime);
    }

    /**
     * Floid thread timings
     */
    public FloidThreadTimings() {
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected FloidThreadTimings(Parcel in) {
        this.mMainThreadSleepTime = in.readLong();
        this.mAccessoryCommunicatorNoAccessorySleepTime = in.readLong();
        this.mAccessoryCommunicatorNoMessageSleepTime = in.readLong();
        this.mAccessoryCommunicatorIncomingMessageSleepTime = in.readLong();
        this.mAccessoryCommunicatorOutgoingMessageSleepTime = in.readLong();
    }

    /**
     * The creator
     */
    public static final Creator<FloidThreadTimings> CREATOR = new Creator<FloidThreadTimings>() {
        @Override
        public FloidThreadTimings createFromParcel(Parcel source) {
            return new FloidThreadTimings(source);
        }

        @Override
        public FloidThreadTimings[] newArray(int size) {
            return new FloidThreadTimings[size];
        }
    };
}
