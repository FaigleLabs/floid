/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for turn to command
 */
@SuppressWarnings("SameParameterValue")
public class TurnToProcessBlock extends ProcessBlock {
    /**
     * Create a new turn to process block
     * @param timeoutInterval the timeout interval
     */
    public TurnToProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Turn To");
    }
}
