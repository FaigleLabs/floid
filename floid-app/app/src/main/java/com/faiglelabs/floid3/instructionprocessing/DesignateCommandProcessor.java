/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.DesignateCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.DesignateProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for the designate command
 */
public class DesignateCommandProcessor extends InstructionProcessor
{
    /**
     * Create a designate command processor
     * @param floidService the floid service
     * @param designateCommand the designate command
     */
    @SuppressWarnings("WeakerAccess")
	public DesignateCommandProcessor(FloidService floidService, DesignateCommand designateCommand)
	{
		super(floidService, designateCommand);
		useDefaultGoalProcessing = false;
        completeOnCommandResponse = false;
		setProcessBlock(new DesignateProcessBlock(defaultGoalTimeout));
	}
	@Override
	public boolean setupInstruction()
	{
	    // General idea:
	    // This is generally the same as flyTo and turnTo...
	    super.setupInstruction();
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
	    if(!floidService.getFloidDroidStatus().isLiftedOff())
	    {
	        // Logic error - must be lifted off to designate:
            sendCommandLogicErrorMessage("Not lifted off.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
	    }
	    // OK, passed all tests...
        DesignateCommand      designateCommand      = (DesignateCommand)droidInstruction;
        DesignateProcessBlock designateProcessBlock = (DesignateProcessBlock)processBlock;
        designateProcessBlock.setFloidGoalId(FloidService.NO_GOAL_ID);   // Does not use a goal id - done once command response received
        // Create a new command buffer object:
        try
        {
            FloidOutgoingCommand designateOutgoingCommand = floidService.sendDesignateCommandToFloid((float)designateCommand.getX(), (float)designateCommand.getY(), (float)designateCommand.getZ());
            if(designateOutgoingCommand != null)
            {
                designateProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                designateProcessBlock.setProcessBlockWaitForCommandNumber(designateOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Designate command.");
                // We must have failed - set completion with error:
                designateProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;    // We are done with this whole command
	}
	@SuppressWarnings("EmptyMethod")
    @Override
	public boolean processInstruction()
	{
        return super.processInstruction();
    }
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
