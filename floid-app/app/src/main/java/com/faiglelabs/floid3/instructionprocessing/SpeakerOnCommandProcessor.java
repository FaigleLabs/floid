/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import com.faiglelabs.floid.servertypes.commands.SpeakerOnCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.R;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.SpeakerOnProcessBlock;

import java.util.Locale;

/**
 * Command processor for the speaker command
 */
public class SpeakerOnCommandProcessor extends InstructionProcessor implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    private final static String TAG = "SpeakerOnCmdProcessor";

    private final static int REPEAT_COUNT_INFINITE = 0;
    private final static int REPEAT_COUNT_NONE = 1;
    private final static int REPEAT_COUNT_ONE = 2;
    private final static int REPEAT_COUNT_TWO = 3;
    private final static int REPEAT_COUNT_THREE = 4;
    private final static int REPEAT_COUNT_FIVE = 5;
    private final static int REPEAT_COUNT_TEN = 6;
    private final static int REPEAT_COUNT_TWENTY = 7;
    private final static int REPEAT_COUNT_ONE_HUNDRED = 8;

    private final SpeakerOnCommand mSpeakerOnCommand;
    private MediaPlayer mMediaPlayer = null;

    /**
     * Create a speaker command processor
     *
     * @param floidService   the floid service
     * @param speakerCommand the speaker command
     */
    public SpeakerOnCommandProcessor(FloidService floidService, SpeakerOnCommand speakerCommand) {
        super(floidService, speakerCommand);
        mSpeakerOnCommand = speakerCommand;
        setProcessBlock(new SpeakerOnProcessBlock(FloidService.COMMAND_NO_TIMEOUT, speakerCommand));        // No timeout
        useDefaultGoalProcessing = false;
        completeOnCommandResponse = false;
    }

    @Override
    public boolean setupInstruction() {
        super.setupInstruction();  // Sets processBlock to started
        SpeakerOnProcessBlock speakerOnProcessBlock = (SpeakerOnProcessBlock) processBlock;
        speakerOnProcessBlock.setToDefault();
        speakerOnProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
        speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
        return false;
    }

    @Override
    public boolean processInstruction() {
        if (super.processInstruction()) {
            return true;   // Already done...
        }
        SpeakerOnProcessBlock speakerOnProcessBlock = (SpeakerOnProcessBlock) processBlock;
        Log.d(TAG, "Pr: " + speakerOnProcessBlock.getProcessBlockStatus());
        Log.d(TAG, "Sp: " + speakerOnProcessBlock.getSpeakerStatus());
        if (speakerOnProcessBlock.getProcessBlockStatus() == ProcessBlock.PROCESS_BLOCK_IN_PROGRESS) {
            speakerOnProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_STARTED);
            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Started");
        }
        // Process the current speaker status:
        switch (speakerOnProcessBlock.getSpeakerStatus()) {
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED: {
                // if we have an alert, start it, otherwise move to alert complete
                if (mSpeakerOnCommand.isAlert()) {
                    // Alert mode:
                    speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Alert");
                    // Start the alert play:
                    mMediaPlayer = MediaPlayer.create(floidService, R.raw.speaker_alert);
                    mMediaPlayer.setLooping(false);
                    mMediaPlayer.setOnErrorListener(this);
                    mMediaPlayer.setOnCompletionListener(this);
                    mMediaPlayer.setVolume((float) mSpeakerOnCommand.getVolume(), (float) mSpeakerOnCommand.getVolume());
                    mMediaPlayer.start();
                } else {
                    // Skip to alert complete:
                    speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_ALERT_COMPLETE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Alert Complete");
                }
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT: {
                // do nothing - we are waiting for the status to change to alert complete in the callback
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_ALERT_COMPLETE: {
                // Tear down the media player:
                if (mMediaPlayer != null) {
                    mMediaPlayer.release();
                    mMediaPlayer = null;
                }
                // Alert complete - start TTS or play File:
                if (mSpeakerOnCommand.getMode().equals("tts")) {
                    if (floidService.mTextToSpeech != null) {
                        // TTS Mode:
                        speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_TTS);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> In TTS");
                        // Check if the locale is already correct:
                        Locale preferredLocale = FloidService.getLanguageLocale(FloidService.safeMapStringGet(floidService.mDroidParametersHashMap, FloidService.DROID_PARAMETERS_SPEECH_LOCALE_OPTION));
                        if (!floidService.mTextToSpeech.getVoice().getLocale().equals(preferredLocale)) {
                            // Set the speech locale:
                            int result = floidService.mTextToSpeech.setLanguage(FloidService.getLanguageLocale(FloidService.safeMapStringGet(floidService.mDroidParametersHashMap, FloidService.DROID_PARAMETERS_SPEECH_LOCALE_OPTION)));
                            if (result == TextToSpeech.LANG_MISSING_DATA
                                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                                Log.e(TAG, "TTS: This Language is not supported");
                            }
                        }
                        floidService.mTextToSpeech.setPitch((float) mSpeakerOnCommand.getPitch());
                        floidService.mTextToSpeech.setSpeechRate((float) mSpeakerOnCommand.getRate());
                        // Set the volume for the TTS:
                        AudioManager audioManager = (AudioManager) floidService.getSystemService(Context.AUDIO_SERVICE);
                        if(audioManager!=null) {
                            int amStreamMusicMaxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) (amStreamMusicMaxVol * mSpeakerOnCommand.getVolume()), 0);
                        }
                        floidService.mTextToSpeech.speak(mSpeakerOnCommand.getTts(), TextToSpeech.QUEUE_FLUSH, null, "");
                    } else {
                        speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_TTS_COMPLETE);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Failed to initialize TTS");
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> TTS Complete");
                    }
                } else {
                    // File mode:
                    speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_FILE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> In File");
                    // Start the media play for the file:
                    Uri fileUri = Uri.parse("file://" + mSpeakerOnCommand.getFileName());
                    mMediaPlayer = MediaPlayer.create(floidService, fileUri);
                    mMediaPlayer.setLooping(false);
                    mMediaPlayer.setOnErrorListener(this);
                    mMediaPlayer.setOnCompletionListener(this);
                    mMediaPlayer.setVolume((float) mSpeakerOnCommand.getVolume(), (float) mSpeakerOnCommand.getVolume());
                    mMediaPlayer.start();
                }
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_TTS: {
                if(floidService.mTextToSpeech!=null) {
                    // Wait for TTS to finish:
                    if (!floidService.mTextToSpeech.isSpeaking()) {
                        speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_TTS_COMPLETE);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> TTS Complete");
                    }
                }
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_TTS_COMPLETE: {
                // if we have a delay, then start it, otherwise move to delay complete
                if (mSpeakerOnCommand.getDelay() > 0) {
                    // Start the delay:
                    speakerOnProcessBlock.setDelayEnd(System.currentTimeMillis() + (mSpeakerOnCommand.getDelay() * 1000L));
                    speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_DELAY);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> In Delay");
                } else {
                    speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_DELAY_COMPLETE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Delay Complete");
                }
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_FILE: {
                // do nothing, we are waiting for the file to complete in the call-back
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_FILE_COMPLETE: {
                // Tear down the media player:
                if (mMediaPlayer != null) {
                    mMediaPlayer.release();
                    mMediaPlayer = null;
                }
                // if we have a delay, then start it, otherwise move to delay complete
                if (mSpeakerOnCommand.getDelay() > 0) {
                    // Start the delay:
                    speakerOnProcessBlock.setDelayEnd(System.currentTimeMillis() + (mSpeakerOnCommand.getDelay() * 1000L));
                    speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_DELAY);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> In Delay");
                } else {
                    speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_DELAY_COMPLETE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Delay Complete");
                }
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_DELAY: {
                // check delay and move to delay complete if done
                if (System.currentTimeMillis() > speakerOnProcessBlock.getDelayEnd()) {
                    // Done with the delay:
                    speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_DELAY_COMPLETE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Delay Complete");
                }
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_DELAY_COMPLETE: {
                // We are done with this loop, bump our repeat count:
                speakerOnProcessBlock.increaseRepeatCount();
                // See if we are to continue, otherwise move to complete:
                switch (mSpeakerOnCommand.getSpeakerRepeat()) {
                    case REPEAT_COUNT_INFINITE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Repeat Forever");
                        speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Not started");
                    }
                    break;
                    case REPEAT_COUNT_NONE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Repeat none");
                        speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Complete");
                    }
                    break;
                    case REPEAT_COUNT_ONE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Repeat 1 time [Remain: " + (2 - speakerOnProcessBlock.getRepeatCount()) + "]");
                        if (speakerOnProcessBlock.getRepeatCount() > 1) {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Complete");
                        } else {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_TWO:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Repeat 2 times [Remain: " + (3 - speakerOnProcessBlock.getRepeatCount()) + "]");
                        if (speakerOnProcessBlock.getRepeatCount() > 2) {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Complete");
                        } else {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_THREE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Repeat 3 times [Remain: " + (4 - speakerOnProcessBlock.getRepeatCount()) + "]");
                        if (speakerOnProcessBlock.getRepeatCount() > 3) {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Complete");
                        } else {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_FIVE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Repeat 5 times [Remain: " + (6 - speakerOnProcessBlock.getRepeatCount()) + "]");
                        if (speakerOnProcessBlock.getRepeatCount() > 5) {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Complete");
                        } else {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_TEN:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Repeat 10 times [Remain: " + (11 - speakerOnProcessBlock.getRepeatCount()) + "]");
                        if (speakerOnProcessBlock.getRepeatCount() > 10) {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Complete");
                        } else {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_TWENTY:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Repeat 20 times [Remain: " + (21 - speakerOnProcessBlock.getRepeatCount()) + "]");
                        if (speakerOnProcessBlock.getRepeatCount() > 20) {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Complete");
                        } else {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_ONE_HUNDRED:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Repeat 100 times [Remain: " + (101 - speakerOnProcessBlock.getRepeatCount()) + "]");
                        if (speakerOnProcessBlock.getRepeatCount() > 100) {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Complete");
                        } else {
                            speakerOnProcessBlock.setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Not started");
                        }
                    }
                    break;
                }
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE: {
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker On Process Block -> Complete Success");
                return true; // We are done...
            }
        }
        return false; // Not done
    }

    @Override
    public boolean tearDownInstruction() {
        super.tearDownInstruction();
        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker on command: instruction tear down");

        // Tear down this right:
        switch (((SpeakerOnProcessBlock) processBlock).getSpeakerStatus()) {
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT:
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_FILE: {
                if (mMediaPlayer != null) {
                    mMediaPlayer.stop();
                    mMediaPlayer.release();
                    mMediaPlayer = null;
                }
            }
            break;
            case SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_TTS: {
                if (floidService.mTextToSpeech != null) {
                    floidService.mTextToSpeech.stop();
                }
            }
            break;
            default: {
                // Do nothing
            }
        }
        ((SpeakerOnProcessBlock) processBlock).setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
        return true;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        // Alert completion?
        if (((SpeakerOnProcessBlock) processBlock).getSpeakerStatus() == SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT) {
            ((SpeakerOnProcessBlock) processBlock).setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_ALERT_COMPLETE);
        } else {
            // Must be file completion
            ((SpeakerOnProcessBlock) processBlock).setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_FILE_COMPLETE);
        }
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (((SpeakerOnProcessBlock) processBlock).getSpeakerStatus() == SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT) {
            ((SpeakerOnProcessBlock) processBlock).setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_ALERT_COMPLETE);
        } else {
            // Must be file completion
            ((SpeakerOnProcessBlock) processBlock).setSpeakerStatus(SpeakerOnProcessBlock.SPEAKER_PROCESS_STATUS_FILE_COMPLETE);
        }
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }
}
