/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

/**
 * Interface for floid edit text callback
 */
public abstract class FloidEditTextValidatorCallback {
    /**
     * Update the interface
     */
    abstract public void updateInterface();
}
