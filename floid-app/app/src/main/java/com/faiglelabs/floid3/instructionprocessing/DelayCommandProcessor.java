/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.os.SystemClock;
import com.faiglelabs.floid.servertypes.commands.DelayCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.DelayProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for the delay command
 */
public class DelayCommandProcessor extends InstructionProcessor
{
    /**
     * Create a delay command processor
     * @param floidService the floid service
     * @param delayCommand the delay command
     */
    @SuppressWarnings("WeakerAccess")
	public DelayCommandProcessor(FloidService floidService, DelayCommand delayCommand)
	{
		super(floidService, delayCommand);
		useDefaultGoalProcessing = false;
		completeOnCommandResponse = false;
		setProcessBlock(new DelayProcessBlock(FloidService.COMMAND_NO_TIMEOUT));   // This command does not time out
	}
	@Override
	public boolean setupInstruction()
	{
        super.setupInstruction();  // Sets processBlock to started
        // It is not really necessary to have the floid connected to 'delay':
/*
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
*/
        if(floidService.getFloidDroidStatus().isLiftedOff())
        {
            // Logic error - must not be lifted off to delay:
            sendCommandLogicErrorMessage("Floid has already lifted off - must hover, not delay.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
        }
        DelayProcessBlock delayProcessBlock = (DelayProcessBlock)processBlock;
        delayProcessBlock.setStartTime(SystemClock.uptimeMillis());
        delayProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
        return false;
	}

	@Override
	public boolean processInstruction()
	{
	    if(super.processInstruction())
	    {
	        return true;   // we are already done..
	    }
	    // Get our command and process block:
        DelayProcessBlock delayProcessBlock = (DelayProcessBlock)processBlock;
        DelayCommand      delayCommand      = (DelayCommand)droidInstruction;
//        if(floidService.mUseLogger) Log.i(FloidService.TAG, "  Delay Process Instruction: " + delayCommand.getDelayTime() + " " +  delayProcessBlock.getStartTime() + " " + ((delayCommand.getDelayTime()*1000) + delayProcessBlock.getProcessBlockStartTime()) + " " + SystemClock.uptimeMillis());

        // OK, check to see if our delay is finished
        if(SystemClock.uptimeMillis() >= ((delayCommand.getDelayTime()*1000) + delayProcessBlock.getStartTime()))
        {
            // Return:
            delayProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
            return true;
        }
        // We did not complete:
        return false;  // This seems like an error condition...
	}
	@Override
	public boolean tearDownInstruction()
	{
	    super.tearDownInstruction();
		return true;
	}

}
