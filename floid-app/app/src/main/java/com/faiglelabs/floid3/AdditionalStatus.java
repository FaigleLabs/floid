/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

import android.os.Parcelable;

/**
 * The type Additional status.
 */
public class AdditionalStatus implements Parcelable {
    /**
     * The constant BYTES_TO_FLOID.
     */
    public static final int BYTES_TO_FLOID = 0;
    /**
     * The constant BYTES_FROM_FLOID.
     */
    public static final int BYTES_FROM_FLOID = 1;
    /**
     * The constant PACKETS_TO_FLOID.
     */
    public static final int PACKETS_TO_FLOID = 2;
    /**
     * The constant PACKETS_FROM_FLOID.
     */
    public static final int PACKETS_FROM_FLOID = 3;
    /**
     * The constant BAD_BYTES_TO_FLOID.
     */
    public static final int BAD_BYTES_TO_FLOID = 4;
    /**
     * The constant BAD_BYTES_FROM_FLOID.
     */
    public static final int BAD_BYTES_FROM_FLOID = 5;
    /**
     * The constant BAD_PACKETS_TO_FLOID.
     */
    public static final int BAD_PACKETS_TO_FLOID = 6;
    /**
     * The constant BAD_PACKETS_FROM_FLOID.
     */
    public static final int BAD_PACKETS_FROM_FLOID = 7;
    /**
     * The constant BAD_PACKET_LENGTHS_FROM_FLOID.
     */
    public static final int BAD_PACKET_LENGTHS_FROM_FLOID = 8;
    /**
     * The constant STATUS_PACKETS_FROM_FLOID.
     */
    public static final int STATUS_PACKETS_FROM_FLOID = 9;
    /**
     * The constant BAD_STATUS_PACKETS_FROM_FLOID.
     */
    public static final int BAD_STATUS_PACKETS_FROM_FLOID = 10;
    /**
     * The constant MODEL_STATUS_PACKETS_FROM_FLOID.
     */
    public static final int MODEL_STATUS_PACKETS_FROM_FLOID = 11;
    /**
     * The constant BAD_MODEL_STATUS_PACKETS_FROM_FLOID.
     */
    public static final int BAD_MODEL_STATUS_PACKETS_FROM_FLOID = 12;
    /**
     * The constant MODEL_PARAMETERS_PACKETS_FROM_FLOID.
     */
    public static final int MODEL_PARAMETERS_PACKETS_FROM_FLOID = 13;
    /**
     * The constant BAD_MODEL_PARAMETERS_PACKETS_FROM_FLOID.
     */
    public static final int BAD_MODEL_PARAMETERS_PACKETS_FROM_FLOID = 14;
    /**
     * The constant DEBUG_PACKETS_FROM_FLOID.
     */
    public static final int DEBUG_PACKETS_FROM_FLOID = 15;
    /**
     * The constant BAD_PACKET_TYPES_FROM_FLOID.
     */
    public static final int BAD_PACKET_TYPES_FROM_FLOID = 16;
    /**
     * The constant BAD_TEST_RESPONSE_PACKETS_FROM_FLOID.
     */
    public static final int BAD_TEST_RESPONSE_PACKETS_FROM_FLOID = 17;
    /**
     * The constant COMMANDS_TO_FLOID.
     */
    public static final int COMMANDS_TO_FLOID = 18;
    /**
     * The constant COMMAND_RESPONSES_FROM_FLOID.
     */
    public static final int COMMAND_RESPONSES_FROM_FLOID = 19;
    /**
     * The constant BAD_COMMAND_RESPONSES_FROM_FLOID.
     */
    public static final int BAD_COMMAND_RESPONSES_FROM_FLOID = 20;
    /**
     * The constant IO_ERROR_FROM_FLOID.
     */
    public static final int IO_ERROR_FROM_FLOID = 21;
    /**
     * The constant BYTES_TO_SERVER.
     */
    public static final int BYTES_TO_SERVER = 22;
    /**
     * The constant BYTES_FROM_SERVER.
     */
    public static final int BYTES_FROM_SERVER = 23;
    /**
     * The constant PACKETS_TO_SERVER.
     */
    public static final int PACKETS_TO_SERVER = 24;
    /**
     * The constant PACKETS_FROM_SERVER.
     */
    public static final int PACKETS_FROM_SERVER = 25;
    /**
     * The constant BAD_BYTES_TO_SERVER.
     */
    public static final int BAD_BYTES_TO_SERVER = 26;
    /**
     * The constant BAD_BYTES_FROM_SERVER.
     */
    public static final int BAD_BYTES_FROM_SERVER = 27;
    /**
     * The constant BAD_PACKETS_TO_SERVER.
     */
    public static final int BAD_PACKETS_TO_SERVER = 28;
    /**
     * The constant BAD_PACKETS_FROM_SERVER.
     */
    public static final int BAD_PACKETS_FROM_SERVER = 29;
    /**
     * The constant BAD_MESSAGES_FROM_SERVER.
     */
    public static final int BAD_MESSAGES_FROM_SERVER = 30;
    /**
     * The constant NULL_RETURN_FROM_SERVER.
     */
    public static final int NULL_RETURN_FROM_SERVER = 31;
    /**
     * The constant COMMANDS_CLEARED.
     */
    public static final int COMMANDS_CLEARED = 32;
    /**
     * The constant THREAD_FLOID_DROID_IS_NULL.
     */
    public static final int THREAD_FLOID_DROID_IS_NULL = 33;
    /**
     * The constant THREAD_FLOID_DROID_IS_ALIVE.
     */
    public static final int THREAD_FLOID_DROID_IS_ALIVE = 34;
    /**
     * The constant THREAD_FLOID_DROID_IS_RUNNING.
     */
    public static final int THREAD_FLOID_DROID_IS_RUNNING = 35;
    /**
     * The constant THREAD_FLOID_COMMUNICATOR_IS_NULL.
     */
    public static final int THREAD_FLOID_COMMUNICATOR_IS_NULL = 36;
    /**
     * The constant THREAD_FLOID_COMMUNICATOR_IS_ALIVE.
     */
    public static final int THREAD_FLOID_COMMUNICATOR_IS_ALIVE = 37;
    /**
     * The constant THREAD_FLOID_COMMUNICATOR_IS_RUNNING.
     */
    public static final int THREAD_FLOID_COMMUNICATOR_IS_RUNNING = 38;
    /**
     * Dirty flag
     */
    private Boolean dirty = false;
    /**
     * Dirty synchronization
     */
    private final Object dirtySync = new Object();

    private static final int size = 39; // Note: Adjust this directly with above and same for strings below - faster than hashmap etc.

    /**
     * Gets size.
     *
     * @return the size
     */
    public static int getSize() {
        return size;
    }

    private static final String[] labels = {
            "BYTES TO FLOID",
            "BYTES FROM FLOID",
            "PACKETS TO FLOID",
            "PACKETS FROM FLOID",
            "BAD BYTES TO FLOID",
            "BAD BYTES FROM FLOID",
            "BAD PACKETS TO FLOID",
            "BAD PACKETS FROM FLOID",
            "BAD PACKET LENGTHS FROM FLOID",
            "STATUS PACKETS FROM FLOID",
            "BAD STATUS PACKETS FROM FLOID",
            "MODEL STATUS PACKETS FROM FLOID",
            "BAD MODEL STATUS PACKETS FROM FLOID",
            "MODEL PARAMETERS PACKETS FROM FLOID",
            "BAD MODEL PARAMETERS PACKETS FROM FLOID",
            "DEBUG PACKETS FROM FLOID",
            "BAD PACKET TYPES FROM FLOID",
            "BAD TEST RESPONSE PACKETS FROM FLOID",
            "COMMANDS TO FLOID",
            "COMMAND RESPONSES FROM FLOID",
            "BAD COMMAND RESPONSES FROM FLOID",
            "IO ERROR FROM FLOID",
            "BYTES TO SERVER",
            "BYTES FROM SERVER",
            "PACKETS TO SERVER",
            "PACKETS FROM SERVER",
            "BAD BYTES TO SERVER",
            "BAD BYTES FROM SERVER",
            "BAD PACKETS TO SERVER",
            "BAD PACKETS FROM SERVER",
            "BAD JSON FROM SERVER",
            "NULL RETURN FROM SERVER",
            "COMMANDS CLEARED",
            "THREAD FLOID DROID IS NULL",
            "THREAD FLOID DROID IS ALIVE",
            "THREAD FLOID DROID IS RUNNING",
            "THREAD FLOID COMMUNICATOR IS NULL",
            "THREAD FLOID COMMUNICATOR IS ALIVE",
            "THREAD FLOID COMMUNICATOR IS RUNNING"
    };

    private long[] values = new long[size];

    /**
     * Reset.
     */
    @SuppressWarnings("unused")
    public void reset() {
        synchronized (dirtySync) {
            for (int i = 0; i < size; ++i) {
                values[i] = 0;
            }
            dirty = true;
        }
    }

    /**
     * Increment.
     *
     * @param key the key
     */
    public void increment(int key) {
        synchronized (dirtySync) {
            if (key >= 0 && key < size) {
                values[key]++;
            }
            dirty = true;
        }
    }

    /**
     * Add.
     *
     * @param key   the key
     * @param value the value
     */
    public void add(int key, long value) {
        synchronized (dirtySync) {
            if (key >= 0 && key < size) {
                values[key] += value;
            }
            dirty = true;
        }
    }

    /**
     * Set.
     *
     * @param key   the key
     * @param value the value
     */
    public void set(int key, long value) {
        synchronized (dirtySync) {
            if (key >= 0 && key < size) {
                values[key] = value;
            }
            dirty = true;
        }
    }

    /**
     * Get long.
     *
     * @param key the key
     * @return the long
     */
    public long get(int key) {
        synchronized (dirtySync) {
            if (key >= 0 && key < size) {
                return values[key];
            } else {
                return 0;
            }
        }
    }

    /**
     * Reset.
     *
     * @param key the key
     */
    @SuppressWarnings("unused")
    public void reset(int key) {
        synchronized (dirtySync) {
            if (key >= 0 && key < size) {
                values[key] = 0;
            }
            dirty = true;
        }
    }

    /**
     * Gets label.
     *
     * @param key the key
     * @return the label
     */
    public static String getLabel(int key) {
        if(key>=0 && key<size) {
            return labels[key];
        } else {
            return "";
        }
    }

    /**
     * Is dirty boolean.
     *
     * @return the boolean
     */
    public Boolean isDirty() {
        return dirty;
    }

    /**
     * Clear dirty.
     */
    @SuppressWarnings("WeakerAccess")
    public void clearDirty() {
        synchronized (dirtySync) {
            dirty = false;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(this.dirty);
        dest.writeLongArray(this.values);
    }

    /**
     * Additional floid thread and communication status items
     */
    public AdditionalStatus() {
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected AdditionalStatus(android.os.Parcel in) {
        this.dirty = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.values = in.createLongArray();
    }

    /**
     * The creator
     */
    public static final Creator<AdditionalStatus> CREATOR = new Creator<AdditionalStatus>() {
        @Override
        public AdditionalStatus createFromParcel(android.os.Parcel source) {
            return new AdditionalStatus(source);
        }

        @Override
        public AdditionalStatus[] newArray(int size) {
            return new AdditionalStatus[size];
        }
    };
}
