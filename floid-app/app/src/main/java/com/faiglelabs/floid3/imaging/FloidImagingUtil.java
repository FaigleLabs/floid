/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.imaging;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.*;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import com.faiglelabs.floid.servertypes.statuses.FloidImagingStatus;

import java.util.Locale;

/**
 * Imaging utility class (static methods)
 */
class FloidImagingUtil {
    // Our tag for debugging:
    /**
     * The constant TAG.
     */
    private static final String TAG = "FloidImaging";

    // Readable logging values
    // Null Map
    private static final String CAMERA_INFO_VALUE_MAP_NULL = "map is null";
    private static final String CAMERA_INFO_VALUE_UNKNOWN = "unknown";
    private static final String CAMERA_INFO_VALUE_SUPPORTED = "supported";
    // Camera Device
    private static final String CAMERA_INFO_TOKEN_CAMERA_DEVICE = "CameraDevice";
    private static final String CAMERA_INFO_VALUE_CAMERA_DEVICE_ID_NULL = "null";
    // Lens Facing
    private static final String CAMERA_INFO_TOKEN_LENS_FACING = "LensFacing";
    private static final String CAMERA_INFO_VALUE_LENS_FACING_FRONT = "Front";
    private static final String CAMERA_INFO_VALUE_LENS_FACING_BACK = "Back";
    // Support Level
    private static final String CAMERA_INFO_TOKEN_SUPPORT_LEVEL = "SupportLevel";
    private static final String CAMERA_INFO_VALUE_SUPPORT_LEVEL_LEGACY = "Legacy";
    private static final String CAMERA_INFO_VALUE_SUPPORT_LEVEL_LIMITED = "Limited";
    private static final String CAMERA_INFO_VALUE_SUPPORT_LEVEL_FULL = "Full";
    // Output Formats
    private static final String CAMERA_INFO_TOKEN_OUTPUT_FORMATS = "OutputFormats";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_JPEG = "JPEG";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_RAW10 = "RAW10";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_RAW_SENSOR = "RAW_SENSOR";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_NV16 = "NV16";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_NV21 = "NV21";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_RGB_565 = "RGB_565";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_YUV_420_888 = "YUV_420_88";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_YUY2 = "YUY2";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_YV12 = "YV12";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_PIXEL_RGB_565 = "RGB_565 (pixel)";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_PIXEL_RGB_888 = "RGB_888 (pixel)";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_PIXEL_RGBA_8888 = "RGBA_8888 (pixel)";
    private static final String CAMERA_INFO_VALUE_OUTPUT_FORMAT_PIXEL_RGBX_8888 = "RGX_8888 (pixel)";
    // Color Correction Aberration Modes
    private static final String CAMERA_INFO_TOKEN_COLOR_CORRECTION_ABERRATION_MODES = "ColorCorrectionAberrationModes";
    private static final String CAMERA_INFO_VALUE_COLOR_CORRECTION_ABERRATION_MODE_FAST = "FAST";
    private static final String CAMERA_INFO_VALUE_COLOR_CORRECTION_ABERRATION_MODE_HIGH_QUALITY = "HIGH_QUALITY";
    private static final String CAMERA_INFO_VALUE_COLOR_CORRECTION_ABERRATION_MODE_OFF = "OFF";
    // Target FPS Ranges
    private static final String CAMERA_INFO_TARGET_FPS_SUPPORTED_RANGES = "TargetFPSSupportedRanges";
    // AE AntiAliasing Modes
    private static final String CAMERA_INFO_TOKEN_AE_ANTIALIASING_MODES = "AEAntiAliasingModes";
    private static final String CAMERA_INFO_VALUE_AE_ANTIALIASING_MODE_50HZ = "50HZ";
    private static final String CAMERA_INFO_VALUE_AE_ANTIALIASING_MODE_60HZ = "60HZ";
    private static final String CAMERA_INFO_VALUE_AE_ANTIALIASING_MODE_AUTO = "AUTO";
    private static final String CAMERA_INFO_VALUE_AE_ANTIALIASING_MODE_OFF = "OFF";
    // AE Modes
    private static final String CAMERA_INFO_TOKEN_AE_MODES = "AEModes";
    private static final String CAMERA_INFO_VALUE_AE_MODE_OFF = "OFF";
    private static final String CAMERA_INFO_VALUE_AE_MODE_ON = "ON";
    private static final String CAMERA_INFO_VALUE_AE_MODE_ALWAYS_FLASH = "ALWAYS_FLASH";
    private static final String CAMERA_INFO_VALUE_AE_MODE_AUTO_FLASH = "AUTO_FLASH";
    private static final String CAMERA_INFO_VALUE_AE_MODE_REDEYE = "REDEYE";
    // AF Modes
    private static final String CAMERA_INFO_TOKEN_AF_MODES = "AFModes";
    private static final String CAMERA_INFO_VALUE_AF_MODE_AUTO = "AUTO";
    private static final String CAMERA_INFO_VALUE_AF_MODE_CONTINUOUS_PICTURE = "CONTINUOUS_PICTURE";
    private static final String CAMERA_INFO_VALUE_AF_MODE_CONTINUOUS_VIDEO = "CONTINUOUS_VIDEO";
    private static final String CAMERA_INFO_VALUE_AF_MODE_EDOF = "EDOF";
    private static final String CAMERA_INFO_VALUE_AF_MODE_MACRO = "MACRO";
    private static final String CAMERA_INFO_VALUE_AF_MODE_OFF = "OFF";
    // Effect Modes
    private static final String CAMERA_INFO_TOKEN_EFFECT_MODES = "EffectModes";
    private static final String CAMERA_INFO_VALUE_EFFECT_MODE_AQUA = "AQUA";
    private static final String CAMERA_INFO_VALUE_EFFECT_MODE_BLACKBOARD = "BLACKBOARD";
    private static final String CAMERA_INFO_VALUE_EFFECT_MODE_MONO = "MONO";
    private static final String CAMERA_INFO_VALUE_EFFECT_MODE_NEGATIVE = "NEGATIVE";
    private static final String CAMERA_INFO_VALUE_EFFECT_MODE_OFF = "OFF";
    private static final String CAMERA_INFO_VALUE_EFFECT_MODE_POSTERIZE = "POSTERIZE";
    private static final String CAMERA_INFO_VALUE_EFFECT_MODE_SEPIA = "SEPIA";
    private static final String CAMERA_INFO_VALUE_EFFECT_MODE_WHITEBOARD = "WHITEBOARD";
    // Scene Modes
    private static final String CAMERA_INFO_TOKEN_SCENE_MODES = "EffectModes";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_ACTION = "ACTION";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_BARCODE = "BARCODE";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_BEACH = "BEACH";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_CANDLELIGHT = "CANDLELIGHT";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_DISABLED = "DISABLED";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_FACE_PRIORITY = "FACE_PRIORITY";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_FIREWORKS = "FIREWORKS";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_HDR = "HDR";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_LANDSCAPE = "LANDSCAPE";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_NIGHT = "NIGHT";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_NIGHT_PORTRAIT = "NIGHT_PORTRAIT";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_PARTY = "PARTY";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_SNOW = "SNOW";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_SPORTS = "SPORTS";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_STEADYPHOTO = "STEADYPHOTO";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_SUNSET = "SUNSET";
    private static final String CAMERA_INFO_VALUE_SCENE_MODE_THEATRE = "THEATRE";
    // Stabilization Modes
    private static final String CAMERA_INFO_TOKEN_STABILIZATION_MODES = "StabilizationModes";
    private static final String CAMERA_INFO_VALUE_STABILIZATION_MODE_OFF = "OFF";
    private static final String CAMERA_INFO_VALUE_STABILIZATION_MODE_ON = "ON";
    // AWB Modes
    private static final String CAMERA_INFO_TOKEN_AWB_MODES = "AWBModes";
    private static final String CAMERA_INFO_VALUE_AWB_MODE_AUTO = "AUTO";
    private static final String CAMERA_INFO_VALUE_AWB_MODE_CLOUDY_DAYLIGHT = "CLOUDY_DAYLIGHT";
    private static final String CAMERA_INFO_VALUE_AWB_MODE_DAYLIGHT = "DAYLIGHT";
    private static final String CAMERA_INFO_VALUE_AWB_MODE_FLUORESCENT = "FLUORESCENT";
    private static final String CAMERA_INFO_VALUE_AWB_MODE_INCANDESCENT = "INCANDESCENT";
    private static final String CAMERA_INFO_VALUE_AWB_MODE_OFF = "OFF";
    private static final String CAMERA_INFO_VALUE_AWB_MODE_SHADE = "SHADE";
    private static final String CAMERA_INFO_VALUE_AWB_MODE_TWILIGHT = "TWILIGHT";
    private static final String CAMERA_INFO_VALUE_AWB_MODE_WARM_FLUORESCENT = "WARM_FLUORESCENT";
    // Supported Video Sizes
    private static final String CAMERA_INFO_SUPPORTED_VIDEO_SIZES = "SupportedVideoSizes";
    // Supported High Speed Video FPS Ranges
    private static final String CAMERA_INFO_TOKEN_SUPPORTED_HIGH_SPEED_VIDEO_FPS_RANGES = "SupportedHighSpeedVideoFPSRanges";
    // Supported High Speed Video Sizes
    private static final String CAMERA_INFO_TOKEN_SUPPORTED_HIGH_SPEED_VIDEO_SIZES = "SupportedHighSpeedVideoSizes";

    /**
     *
     * @param floidCameraInfo the camera info
     * @param token The token
     * @param subToken the sub token
     * @param value the value
     */
    private static void addToCameraInfo(FloidCameraInfo floidCameraInfo, String token, String subToken, String value) {
//    private static void addToCameraInfo(StringBuilder cameraInfoStringBuilder, String token, String subToken, String value) {
        try {
            if (subToken == null) {
                floidCameraInfo.addValue(token, value);
//                cameraInfoStringBuilder.append(token).append(": ").append(value).append('\n');
            } else {
                floidCameraInfo.addValue(token, subToken + ": " + value);
//                cameraInfoStringBuilder.append(token).append(" - ").append(subToken).append(": ").append(value).append('\n');
            }
        } catch (Exception e) {
            Log.e(TAG, "addToCameraInfo: caught exception", e);
        }
    }

    /**
     * Log camera device info.
     *
     * @param cameraManager the camera manager
     * @param cameraId  the camera id
     * @param floidCameraInfo the floid camera info to log to
     * @param useLogger log flag
     */
    static void logCameraInfo(CameraManager cameraManager, String cameraId, FloidCameraInfo floidCameraInfo, boolean useLogger) {
        if(!useLogger) {
            return;
        }
        if (cameraId == null) {
            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_CAMERA_DEVICE, null, CAMERA_INFO_VALUE_CAMERA_DEVICE_ID_NULL);
            return;
        }
        try {
            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_CAMERA_DEVICE, null, cameraId);
            // Note that there are even more camera characteristics than those we log below
            CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
            int deviceLevel = CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY; // Default
            Integer lensFacingInteger = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
            if (lensFacingInteger != null) {
                int lensFacing = lensFacingInteger;
                switch (lensFacing) {
                    case CameraCharacteristics.LENS_FACING_FRONT:
                        addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_LENS_FACING, null, CAMERA_INFO_VALUE_LENS_FACING_FRONT);
                        break;
                    case CameraCharacteristics.LENS_FACING_BACK:
                        addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_LENS_FACING, null, CAMERA_INFO_VALUE_LENS_FACING_BACK);
                        break;
                    default:
                        addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_LENS_FACING, null, CAMERA_INFO_VALUE_UNKNOWN);
                        break;
                }
            } else {
                Log.e(TAG, "lensFacingInteger was null");
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_LENS_FACING, null, CAMERA_INFO_VALUE_UNKNOWN);
            }
            Integer deviceLevelInteger = cameraCharacteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
            if (deviceLevelInteger != null) {
                deviceLevel = deviceLevelInteger;
            }
            switch (deviceLevel) {
                case CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY:
                    addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SUPPORT_LEVEL, null, CAMERA_INFO_VALUE_SUPPORT_LEVEL_LEGACY);
                    break;
                case CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED:
                    addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SUPPORT_LEVEL, null, CAMERA_INFO_VALUE_SUPPORT_LEVEL_LIMITED);
                    break;
                case CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_FULL:
                    addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SUPPORT_LEVEL, null, CAMERA_INFO_VALUE_SUPPORT_LEVEL_FULL);
                    break;
                default:
                    addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SUPPORT_LEVEL, null, CAMERA_INFO_VALUE_UNKNOWN);
                    break;
            }
            final int[] cameraRequestAvailableCapabilities = cameraCharacteristics.get(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);
            if(cameraRequestAvailableCapabilities!=null) {
                for (int cameraRequestAvailableCapability : cameraRequestAvailableCapabilities) {
                    switch (cameraRequestAvailableCapability) {
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_BACKWARD_COMPATIBLE:
                            Log.i(TAG, "Camera: Backward Compatible");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_MANUAL_SENSOR:
                            Log.i(TAG, "Camera: Manual Sensor");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_MANUAL_POST_PROCESSING:
                            Log.i(TAG, "Camera: Post Processing");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_RAW:
                            Log.i(TAG, "Camera: Raw");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_PRIVATE_REPROCESSING:
                            Log.i(TAG, "Camera: Private Processing");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_READ_SENSOR_SETTINGS:
                            Log.i(TAG, "Camera: Read Sensor Setting");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_BURST_CAPTURE:
                            Log.i(TAG, "Camera: Burst Capture");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_YUV_REPROCESSING:
                            Log.i(TAG, "Camera: YUV Reprocessing");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_DEPTH_OUTPUT:
                            Log.i(TAG, "Camera: Depth Output");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_CONSTRAINED_HIGH_SPEED_VIDEO:
                            Log.i(TAG, "Camera: High Speed Video");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_MONOCHROME:
                            Log.i(TAG, "Camera: Monochrome");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_LOGICAL_MULTI_CAMERA:
                            Log.i(TAG, "Camera: Logical Multi Camera");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_OFFLINE_PROCESSING:
                            Log.i(TAG, "Camera: Offline Processing");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_SECURE_IMAGE_DATA:
                            Log.i(TAG, "Camera: Secure Image Data");
                            break;
                        case CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES_SYSTEM_CAMERA:
                            Log.i(TAG, "Camera: System Camera");
                            break;
                        default:
                            Log.i(TAG, "Camera: Unknown " + cameraRequestAvailableCapability);
                            break;
                    }
                }
            } else {
                Log.e(TAG, "Error: cameraRequestAvailableCapabilities was null");
            }
            // Get the stream configurations map:
            StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            if (map != null) {
                logOutputFormatClass(floidCameraInfo, map, MediaRecorder.class);
                logOutputFormatClass(floidCameraInfo, map, ImageReader.class);
                logOutputFormatClass(floidCameraInfo, map, SurfaceTexture.class);
                logOutputFormatImage(floidCameraInfo, map, ImageFormat.JPEG, "JPEG");
                logOutputFormatImage(floidCameraInfo, map, ImageFormat.NV16, "NV16");
                logOutputFormatImage(floidCameraInfo, map, ImageFormat.NV21, "NV21");
                logOutputFormatImage(floidCameraInfo, map, ImageFormat.RAW10, "RAW10");
                logOutputFormatImage(floidCameraInfo, map, ImageFormat.RAW_SENSOR, "RAW_SENSOR");
                logOutputFormatImage(floidCameraInfo, map, ImageFormat.RGB_565, "RGB_565");
                logOutputFormatImage(floidCameraInfo, map, ImageFormat.YUV_420_888, "YUV_420_888");
                logOutputFormatImage(floidCameraInfo, map, ImageFormat.YUY2, "YUY2");
                logOutputFormatImage(floidCameraInfo, map, ImageFormat.YV12, "YV12");
                // Supported FPS Ranges
                Range<Integer>[] highSpeedVideoFpsRanges = map.getHighSpeedVideoFpsRanges();
                for (Range<Integer> highSpeedVideoFpsRange : highSpeedVideoFpsRanges) {
                    addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SUPPORTED_HIGH_SPEED_VIDEO_FPS_RANGES, null, highSpeedVideoFpsRange.getLower().toString() + "->" + highSpeedVideoFpsRange.getUpper().toString());
                }
                // Supported Video Sizes
                Size[] highSpeedVideoSizes = map.getHighSpeedVideoSizes();
                for (Size highSpeedVideoSize : highSpeedVideoSizes) {
                    addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SUPPORTED_HIGH_SPEED_VIDEO_SIZES, null, String.format(Locale.getDefault(), "%dx%d", highSpeedVideoSize.getWidth(), highSpeedVideoSize.getHeight()));
                }
                // Output Formats
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_JPEG, Boolean.toString(map.isOutputSupportedFor(ImageFormat.JPEG)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_RAW10, Boolean.toString(map.isOutputSupportedFor(ImageFormat.RAW10)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_RAW_SENSOR, Boolean.toString(map.isOutputSupportedFor(ImageFormat.RAW_SENSOR)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_NV16, Boolean.toString(map.isOutputSupportedFor(ImageFormat.NV16)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_NV21, Boolean.toString(map.isOutputSupportedFor(ImageFormat.NV21)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_RGB_565, Boolean.toString(map.isOutputSupportedFor(ImageFormat.RGB_565)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_YUV_420_888, Boolean.toString(map.isOutputSupportedFor(ImageFormat.YUV_420_888)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_YUY2, Boolean.toString(map.isOutputSupportedFor(ImageFormat.YUY2)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_YV12, Boolean.toString(map.isOutputSupportedFor(ImageFormat.YV12)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_PIXEL_RGB_565, Boolean.toString(map.isOutputSupportedFor(PixelFormat.RGB_565)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_PIXEL_RGB_888, Boolean.toString(map.isOutputSupportedFor(PixelFormat.RGB_888)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_PIXEL_RGBA_8888, Boolean.toString(map.isOutputSupportedFor(PixelFormat.RGBA_8888)));
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, CAMERA_INFO_VALUE_OUTPUT_FORMAT_PIXEL_RGBX_8888, Boolean.toString(map.isOutputSupportedFor(PixelFormat.RGBX_8888)));
            } else {
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_OUTPUT_FORMATS, null, CAMERA_INFO_VALUE_MAP_NULL);
                Log.e(TAG, "    map was null");
            }

            int[] colorCorrectionAberrationModes = cameraCharacteristics.get(CameraCharacteristics.COLOR_CORRECTION_AVAILABLE_ABERRATION_MODES);
            if (colorCorrectionAberrationModes != null) {
                for (int colorCorrectionAberrationMode : colorCorrectionAberrationModes) {
                    switch (colorCorrectionAberrationMode) {
                        case CameraMetadata.COLOR_CORRECTION_ABERRATION_MODE_FAST:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_COLOR_CORRECTION_ABERRATION_MODES, CAMERA_INFO_VALUE_COLOR_CORRECTION_ABERRATION_MODE_FAST, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.COLOR_CORRECTION_ABERRATION_MODE_HIGH_QUALITY:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_COLOR_CORRECTION_ABERRATION_MODES, CAMERA_INFO_VALUE_COLOR_CORRECTION_ABERRATION_MODE_HIGH_QUALITY, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.COLOR_CORRECTION_ABERRATION_MODE_OFF:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_COLOR_CORRECTION_ABERRATION_MODES, CAMERA_INFO_VALUE_COLOR_CORRECTION_ABERRATION_MODE_OFF, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        default:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_COLOR_CORRECTION_ABERRATION_MODES, CAMERA_INFO_VALUE_UNKNOWN, Integer.toString(colorCorrectionAberrationMode));
                            break;
                    }
                }
            }
            Range<Integer>[] aeAvailableTargetFPSRanges = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
            for(int i=0; i<aeAvailableTargetFPSRanges.length; ++i) {
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_TARGET_FPS_SUPPORTED_RANGES, "" + aeAvailableTargetFPSRanges[i].getLower() + "-" + aeAvailableTargetFPSRanges[i].getUpper(), CAMERA_INFO_VALUE_SUPPORTED);
            }
            // Auto Exposure Anti Banding Modes
            int[] aeAvailableAntiBandingModes = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_ANTIBANDING_MODES);
            if (aeAvailableAntiBandingModes != null) {
                for (int aeAvailableAntiBandingMode : aeAvailableAntiBandingModes) {
                    switch (aeAvailableAntiBandingMode) {
                        case CameraMetadata.CONTROL_AE_ANTIBANDING_MODE_50HZ:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_ANTIALIASING_MODES, CAMERA_INFO_VALUE_AE_ANTIALIASING_MODE_50HZ, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AE_ANTIBANDING_MODE_60HZ:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_ANTIALIASING_MODES, CAMERA_INFO_VALUE_AE_ANTIALIASING_MODE_60HZ, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AE_ANTIBANDING_MODE_AUTO:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_ANTIALIASING_MODES, CAMERA_INFO_VALUE_AE_ANTIALIASING_MODE_AUTO, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AE_ANTIBANDING_MODE_OFF:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_ANTIALIASING_MODES, CAMERA_INFO_VALUE_AE_ANTIALIASING_MODE_OFF, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        default:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_ANTIALIASING_MODES, CAMERA_INFO_VALUE_UNKNOWN, Integer.toString(aeAvailableAntiBandingMode));
                            break;
                    }
                }
            }
            // Auto Exposure Modes
            int[] aeAvailableModes = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_MODES);
            if (aeAvailableModes != null) {
                for (int aeAvailableMode : aeAvailableModes) {
                    switch (aeAvailableMode) {
                        case CameraMetadata.CONTROL_AE_MODE_OFF:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_MODES, CAMERA_INFO_VALUE_AE_MODE_OFF, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AE_MODE_ON:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_MODES, CAMERA_INFO_VALUE_AE_MODE_ON, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AE_MODE_ON_ALWAYS_FLASH:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_MODES, CAMERA_INFO_VALUE_AE_MODE_ALWAYS_FLASH, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AE_MODE_ON_AUTO_FLASH:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_MODES, CAMERA_INFO_VALUE_AE_MODE_AUTO_FLASH, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_MODES, CAMERA_INFO_VALUE_AE_MODE_REDEYE, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        default:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AE_MODES, CAMERA_INFO_VALUE_UNKNOWN, Integer.toString(aeAvailableMode));
                            break;
                    }
                }
            }
            // Auto Focus Modes:
            int[] afAvailableModes = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);
            if (afAvailableModes != null) {
                for (int afAvailableMode : afAvailableModes) {
                    switch (afAvailableMode) {
                        case CameraMetadata.CONTROL_AF_MODE_AUTO:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AF_MODES, CAMERA_INFO_VALUE_AF_MODE_AUTO, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_PICTURE:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AF_MODES, CAMERA_INFO_VALUE_AF_MODE_CONTINUOUS_PICTURE, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AF_MODE_CONTINUOUS_VIDEO:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AF_MODES, CAMERA_INFO_VALUE_AF_MODE_CONTINUOUS_VIDEO, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AF_MODE_EDOF:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AF_MODES, CAMERA_INFO_VALUE_AF_MODE_EDOF, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AF_MODE_MACRO:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AF_MODES, CAMERA_INFO_VALUE_AF_MODE_MACRO, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AF_MODE_OFF:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AF_MODES, CAMERA_INFO_VALUE_AF_MODE_OFF, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        default:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AF_MODES, CAMERA_INFO_VALUE_UNKNOWN, Integer.toString(afAvailableMode));
                            break;
                    }
                }
            }
            // Effects:
            int[] availableEffects = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AVAILABLE_EFFECTS);
            if (availableEffects != null) {
                for (int availableEffect : availableEffects) {
                    switch (availableEffect) {
                        case CameraMetadata.CONTROL_EFFECT_MODE_AQUA:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_EFFECT_MODES, CAMERA_INFO_VALUE_EFFECT_MODE_AQUA, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_EFFECT_MODE_BLACKBOARD:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_EFFECT_MODES, CAMERA_INFO_VALUE_EFFECT_MODE_BLACKBOARD, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_EFFECT_MODE_MONO:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_EFFECT_MODES, CAMERA_INFO_VALUE_EFFECT_MODE_MONO, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_EFFECT_MODE_NEGATIVE:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_EFFECT_MODES, CAMERA_INFO_VALUE_EFFECT_MODE_NEGATIVE, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_EFFECT_MODE_OFF:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_EFFECT_MODES, CAMERA_INFO_VALUE_EFFECT_MODE_OFF, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_EFFECT_MODE_POSTERIZE:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_EFFECT_MODES, CAMERA_INFO_VALUE_EFFECT_MODE_POSTERIZE, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_EFFECT_MODE_SEPIA:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_EFFECT_MODES, CAMERA_INFO_VALUE_EFFECT_MODE_SEPIA, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_EFFECT_MODE_WHITEBOARD:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_EFFECT_MODES, CAMERA_INFO_VALUE_EFFECT_MODE_WHITEBOARD, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        default:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_EFFECT_MODES, CAMERA_INFO_VALUE_UNKNOWN, Integer.toString(availableEffect));
                            break;
                    }
                }
            }
            // Scene Modes
            int[] availableSceneModes = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AVAILABLE_SCENE_MODES);
            if (availableSceneModes != null) {
                for (int availableSceneMode : availableSceneModes) {
                    switch (availableSceneMode) {
                        case CameraMetadata.CONTROL_SCENE_MODE_ACTION:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_ACTION, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_BARCODE:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_BARCODE, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_BEACH:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_BEACH, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_CANDLELIGHT:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_CANDLELIGHT, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_DISABLED:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_DISABLED, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_FACE_PRIORITY:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_FACE_PRIORITY, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_FIREWORKS:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_FIREWORKS, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_HDR:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_HDR, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_LANDSCAPE:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_LANDSCAPE, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_NIGHT:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_NIGHT, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_NIGHT_PORTRAIT:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_NIGHT_PORTRAIT, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_PARTY:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_PARTY, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_SNOW:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_SNOW, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_SPORTS:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_SPORTS, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_STEADYPHOTO:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_STEADYPHOTO, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_SUNSET:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_SUNSET, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_SCENE_MODE_THEATRE:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_SCENE_MODE_THEATRE, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        default:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_SCENE_MODES, CAMERA_INFO_VALUE_UNKNOWN, Integer.toString(availableSceneMode));
                            break;
                    }
                }
            }
            // Video Stabilization Modes
            int[] availableVideoStabilizationModes = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES);
            if (availableVideoStabilizationModes != null) {
                for (int availableVideoStabilizationMode : availableVideoStabilizationModes) {
                    switch (availableVideoStabilizationMode) {
                        case CameraMetadata.CONTROL_VIDEO_STABILIZATION_MODE_OFF:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_STABILIZATION_MODES, CAMERA_INFO_VALUE_STABILIZATION_MODE_OFF, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_VIDEO_STABILIZATION_MODE_ON:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_STABILIZATION_MODES, CAMERA_INFO_VALUE_STABILIZATION_MODE_ON, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        default:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_STABILIZATION_MODES, CAMERA_INFO_VALUE_UNKNOWN, Integer.toString(availableVideoStabilizationMode));
                            break;
                    }
                }
            }
            // Auto White Balance Modes
            int[] awbAvailableModes = cameraCharacteristics.get(CameraCharacteristics.CONTROL_AWB_AVAILABLE_MODES);
            if (awbAvailableModes != null) {
                for (int awbAvailableMode : awbAvailableModes) {
                    switch (awbAvailableMode) {
                        case CameraMetadata.CONTROL_AWB_MODE_AUTO:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_AWB_MODE_AUTO, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AWB_MODE_CLOUDY_DAYLIGHT:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_AWB_MODE_CLOUDY_DAYLIGHT, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AWB_MODE_DAYLIGHT:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_AWB_MODE_DAYLIGHT, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AWB_MODE_FLUORESCENT:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_AWB_MODE_FLUORESCENT, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AWB_MODE_INCANDESCENT:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_AWB_MODE_INCANDESCENT, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AWB_MODE_OFF:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_AWB_MODE_OFF, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AWB_MODE_SHADE:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_AWB_MODE_SHADE, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AWB_MODE_TWILIGHT:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_AWB_MODE_TWILIGHT, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        case CameraMetadata.CONTROL_AWB_MODE_WARM_FLUORESCENT:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_AWB_MODE_WARM_FLUORESCENT, CAMERA_INFO_VALUE_SUPPORTED);
                            break;
                        default:
                            addToCameraInfo(floidCameraInfo, CAMERA_INFO_TOKEN_AWB_MODES, CAMERA_INFO_VALUE_UNKNOWN, Integer.toString(awbAvailableMode));
                            break;
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "logCameraInfo: Caught Exception", e);
        }
    }

    /**
     * Find max video size int [ ].
     *
     * @param cameraManager     the camera manager
     * @param videoCameraDevice the video camera
     * @return the int [ ]
     */
    static int[] findMaxVideoSize(CameraManager cameraManager, CameraDevice videoCameraDevice) // We check for max width
    {
        int[] maxSizeReturn = new int[2]; // Initialized to zero
        try {
            StreamConfigurationMap map = cameraManager.getCameraCharacteristics(videoCameraDevice.getId()).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            if (map != null) {
                Size[] outputSizes = map.getOutputSizes(MediaRecorder.class);
                maxSizeReturn = getHighestWidestSize(outputSizes);
            } else {
                Log.e(TAG, "findMaxVideoSize: map was null");
            }
        } catch (CameraAccessException e) {
            Log.e(TAG, "findMaxVideoSize: Caught exception", e);
        }
        return maxSizeReturn;
    }

    /**
     * Get the highest then widest supported sie
     * @param sizes the list of supported sizes
     * @return the highest then widest size
     */
    private static int[] getHighestWidestSize(Size[] sizes) {
        final int SIZE_INDEX_WIDTH = 0;
        final int SIZE_INDEX_HEIGHT = 1;
        int[] maxSizeReturn = new int[2]; // Initialized to zero
        for (Size outputSize : sizes) {
            if (outputSize.getHeight() > maxSizeReturn[SIZE_INDEX_HEIGHT]) {
                maxSizeReturn[SIZE_INDEX_WIDTH] = outputSize.getWidth();
                maxSizeReturn[SIZE_INDEX_HEIGHT] = outputSize.getHeight();
            } else if (outputSize.getHeight() == maxSizeReturn[SIZE_INDEX_HEIGHT]) {
                if (outputSize.getWidth() > maxSizeReturn[SIZE_INDEX_WIDTH]) {
                    maxSizeReturn[SIZE_INDEX_WIDTH] = outputSize.getWidth();
                    maxSizeReturn[SIZE_INDEX_HEIGHT] = outputSize.getHeight();
                }
            }
        }
        return maxSizeReturn;
    }

    /**
     * Find max photo size int [ ].
     *
     * @param cameraManager     the camera manager
     * @param photoCameraDevice the photo camera Device
     * @param imageFormat the image format
     * @return the int [ ]
     */
    static int[] findMaxPhotoSize(CameraManager cameraManager, CameraDevice photoCameraDevice, int imageFormat)  // We check for max width
    {
        int[] maxSizeReturn = new int[2]; // Initialized to zero
        try {
            StreamConfigurationMap map = cameraManager.getCameraCharacteristics(photoCameraDevice.getId()).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            if (map != null) {
                Size[] outputSizes = map.getOutputSizes(imageFormat);
                maxSizeReturn = getHighestWidestSize(outputSizes);
            } else {
                Log.e(TAG, "findMaxVideoSize: map was null");
            }
        } catch (CameraAccessException e) {
            Log.e(TAG, "findMaxVideoSize: Caught exception", e);
        }
        return maxSizeReturn;
    }

    /**
     * Find closest photo size.
     *
     * @param cameraManager     the camera manager
     * @param photoCameraDevice the photo camera Device
     * @param imageFormat       the image format e.g. @see ImageFormat.JPEG
     * @param height            the height for which we are looking
     * @param useLogger log flag
     * @return the closest photo size
     */
    static int[] findClosestPhotoSize(CameraManager cameraManager, CameraDevice photoCameraDevice, int imageFormat, @SuppressWarnings("SameParameterValue") int height, boolean useLogger)  // We check for max width
    {
        final int SIZE_INDEX_WIDTH = 0;
        final int SIZE_INDEX_HEIGHT = 1;
        // Note: Width, Height
        int[] maxSizeReturn = new int[2]; // Initialized to zero
        try {
            StreamConfigurationMap map = cameraManager.getCameraCharacteristics(photoCameraDevice.getId()).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            if (map != null) {
                Size[] outputSizes = map.getOutputSizes(imageFormat);
                // Try exact match:
                for (Size outputSize : outputSizes) {
                    // Does it match the height?
                    if (outputSize.getHeight() == height) {
                        // Is the width bigger?
                        if (outputSize.getWidth() > maxSizeReturn[SIZE_INDEX_WIDTH]) {
                            // Yes:
                            maxSizeReturn[SIZE_INDEX_WIDTH] = outputSize.getWidth();
                            maxSizeReturn[SIZE_INDEX_HEIGHT] = outputSize.getHeight();
                        }
                    }
                }
                // Did we not match yet?
                if (maxSizeReturn[SIZE_INDEX_HEIGHT] == 0) {
                    // We did not match, see if it is 1088 on this device - sometimes 1080 is 1088
                    if (height == FloidImaging.IMAGE_SIZE_1080) {
                        for (Size outputSize : outputSizes) {
                            // Does it match the height?
                            if (outputSize.getHeight() == FloidImaging.IMAGE_SIZE_1088) {
                                // Is the width bigger?
                                if (outputSize.getWidth() > maxSizeReturn[SIZE_INDEX_WIDTH]) {
                                    // Yes:
                                    maxSizeReturn[SIZE_INDEX_WIDTH] = outputSize.getWidth();
                                    maxSizeReturn[SIZE_INDEX_HEIGHT] = outputSize.getHeight();
                                }
                            }
                        }
                    }
                }
                // Did we not match yet?
                if (maxSizeReturn[SIZE_INDEX_HEIGHT] == 0) {
                    // We did not match, find the closest that is smaller
                    for (Size outputSize : outputSizes) {
                        // Less than height?
                        if (outputSize.getHeight() < height) {
                            // Is it bigger than current?
                            if (outputSize.getHeight() > maxSizeReturn[SIZE_INDEX_HEIGHT]) {
                                // We get it:
                                maxSizeReturn[SIZE_INDEX_WIDTH] = outputSize.getWidth();
                                maxSizeReturn[SIZE_INDEX_HEIGHT] = outputSize.getHeight();
                            } else {
                                // was it equal?
                                if (outputSize.getHeight() == maxSizeReturn[SIZE_INDEX_HEIGHT]) {
                                    // Was the width larger?
                                    if (outputSize.getWidth() > maxSizeReturn[SIZE_INDEX_WIDTH]) {
                                        // Yes:
                                        maxSizeReturn[SIZE_INDEX_WIDTH] = outputSize.getWidth();
                                        maxSizeReturn[SIZE_INDEX_HEIGHT] = outputSize.getHeight();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                Log.e(TAG, "findMaxVideoSize: map was null");
            }
        } catch (CameraAccessException e) {
            Log.e(TAG, "findMaxVideoSize: Caught exception", e);
        }
        if(useLogger) {
            Log.d(TAG, String.format("Photo Size: %dx%d", maxSizeReturn[0], maxSizeReturn[1]));
        }
        return maxSizeReturn;
    }

    /**
     * Find closest video size.
     *
     * @param cameraManager     the camera manager
     * @param videoCameraDevice the video camera
     * @param height            the height
     * @param useLogger log flag
     * @return the closest video size
     */
    static int[] findClosestVideoSize(CameraManager cameraManager, CameraDevice videoCameraDevice, @SuppressWarnings("SameParameterValue") int height, boolean useLogger) {
        if(useLogger)
            Log.d(TAG, "  findClosestVideoSize: videoCamera is " + (videoCameraDevice == null ? "null" : "not null"));
        if(useLogger)
            Log.d(TAG, "  findClosestVideoSize: looking for height: " + height);
        final int SIZE_INDEX_WIDTH = 0;
        final int SIZE_INDEX_HEIGHT = 1;
        int[] sizeReturn = new int[2];
        // Logic:
        // 1. We match on height and take largest width...
        if (videoCameraDevice != null) {
            try {
                StreamConfigurationMap map = cameraManager.getCameraCharacteristics(videoCameraDevice.getId()).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map != null) {
                    Size[] outputSizes = map.getOutputSizes(MediaRecorder.class);
                    for (Size outputSize : outputSizes) {
                        if(useLogger)
                            Log.d(TAG, "Looking for: " + height + "  - candidate: " + outputSize.getWidth() + "x" + outputSize.getHeight());
                        if (outputSize.getHeight() == height) {
                            if(useLogger)
                                Log.d(TAG, "Looking for: " + height + "  - matches height: " + outputSize.getWidth() + "x" + outputSize.getHeight());
                            sizeReturn[SIZE_INDEX_HEIGHT] = outputSize.getHeight();
                            // We get the largest width for this size:
                            if (outputSize.getWidth() > sizeReturn[SIZE_INDEX_WIDTH]) {
                                if(useLogger)
                                    Log.d(TAG, "Looking for: " + height + "  - width choice: " + outputSize.getWidth() + "x" + outputSize.getHeight());
                                sizeReturn[SIZE_INDEX_WIDTH] = outputSize.getWidth();
                            }
                        }
                    }
                } else {
                    Log.e(TAG, "findMaxVideoSize: map was null");
                }
            } catch (CameraAccessException e) {
                Log.e(TAG, "findMaxVideoSize: Caught exception", e);
            }
            // Are we good?
            if (sizeReturn[SIZE_INDEX_HEIGHT] == height) {
                // Yes, we found at least one match:
                if(useLogger)
                    Log.d(TAG, "Looking for: " + height + "  - we have a good match: " + sizeReturn[SIZE_INDEX_WIDTH] + "x" + sizeReturn[SIZE_INDEX_HEIGHT]);
                return sizeReturn;
            }

            // No, we are not...
            // We did not find an exact match... was it 1080, if so look for 1088 - see Android implementation note that says some camera providers do 1088
            if (height == FloidImaging.IMAGE_SIZE_1080) {
                if(useLogger)
                    Log.d(TAG, "  did not find supported height (1080), so we are looking for 640");
                return findClosestVideoSize(cameraManager, videoCameraDevice, FloidImaging.IMAGE_SIZE_640, useLogger);
            }
            if(useLogger)
                Log.w(TAG, "findClosestVideoSize: We did not find a match for video size: " + height);
        } else {
            Log.e(TAG, "findClosestVideoSize: Video camera device was null");
        }
        return sizeReturn; // We did not find one - doh!
    }

    /**
     * Gets max video frame rate.
     *
     * @param videoCameraDevice the video camera device
     * @return the max video frame rate
     */
    @SuppressWarnings({"SameReturnValue", "UnusedParameters"})
    static int getMaxVideoFrameRate(CameraDevice videoCameraDevice) {
        return FloidImaging.VIDEO_DEFAULT_MAX_FRAME_RATE;
    }

    /**
     * Gets camera id for a type
     *
     * @param cameraManager the camera manager
     * @param cameraType the camera type
     * @param useLogger log flag
     * @return the first camera id matching that type
     */
    static String getCameraId(CameraManager cameraManager, int cameraType, boolean useLogger)  // Front or back
    {
        if (cameraManager == null) {
            return FloidImagingStatus.NO_CAMERA_ID_STRING;
        }
        String cameraIdString = FloidImagingStatus.NO_CAMERA_ID_STRING;
        try {
            // Log the camera lens facing info:
            boolean found = false;
            String[] camIds = cameraManager.getCameraIdList();
            // We want to take the first that matches
            for (String cameraId : camIds) {
                try {
                    final CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                    Integer lensFacingInteger = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
                    if (lensFacingInteger != null) {
                        int lensFacing = lensFacingInteger;
                        switch (lensFacing) {
                            case CameraCharacteristics.LENS_FACING_FRONT:
                                if(useLogger)
                                    Log.d(TAG, "Camera ID: " + cameraId + " is a front facing camera");
                                break;
                            case CameraCharacteristics.LENS_FACING_BACK:
                                if(useLogger)
                                    Log.d(TAG, "Camera ID: " + cameraId + " is a back facing camera");
                                break;
                            case CameraCharacteristics.LENS_FACING_EXTERNAL:
                                if(useLogger)
                                    Log.d(TAG, "Camera ID: " + cameraId + " is an external facing camera");
                                break;
                            default:
                                if(useLogger)
                                    Log.d(TAG, "Camera ID: " + cameraId + " lens facing is of unknown type: " + lensFacing);
                                break;
                        }
                        if (lensFacingInteger == cameraType && !found) {
                            if(useLogger)
                                Log.d(TAG, "Found matching camera ID: " + cameraId + " for lens facing: " + lensFacing);
                            cameraIdString = cameraId;
                            found = true;
                        }
                    } else {
                        Log.e(TAG, "lensFacingInteger was null");
                    }
                } catch (CameraAccessException cae) {
                    Log.e(TAG, "getCameraId: Camera access exception from getCameraCharacteristics", cae);
                }
            }
        } catch (CameraAccessException cae2) {
            Log.e(TAG, "getCameraId: Camera access exception", cae2);
        }
        return cameraIdString;
    }

    /**
     * Log the supported output formats for this class from the map
     *
     * @param floidCameraInfo the camera info
     * @param map   the map
     * @param clazz the class
     */
    private static void logOutputFormatClass(FloidCameraInfo floidCameraInfo, StreamConfigurationMap map, Class clazz) {
        if (map.getOutputSizes(clazz) != null) {
            for (Size supportedVideoSize : map.getOutputSizes(MediaRecorder.class)) {
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_SUPPORTED_VIDEO_SIZES, clazz.getSimpleName(), String.format(Locale.getDefault(), "%dx%d", supportedVideoSize.getWidth(), supportedVideoSize.getHeight()));
            }
        } else {
            addToCameraInfo(floidCameraInfo, CAMERA_INFO_SUPPORTED_VIDEO_SIZES, clazz.getSimpleName(), CAMERA_INFO_VALUE_MAP_NULL);
        }
    }

    /**
     * Log the supported output formats for this image format from the map (and tag log with imageFormatName)
     *
     * @param floidCameraInfo the camera info
     * @param map             the map
     * @param imageFormat     the image format (e.g. ImageFormat.JPEG)
     * @param imageFormatName the image format name
     */
    private static void logOutputFormatImage(FloidCameraInfo floidCameraInfo, StreamConfigurationMap map, int imageFormat, String imageFormatName) {
        if (map.getOutputSizes(imageFormat) != null) {
            for (Size supportedVideoSize : map.getOutputSizes(imageFormat)) {
                addToCameraInfo(floidCameraInfo, CAMERA_INFO_SUPPORTED_VIDEO_SIZES, imageFormatName, String.format(Locale.getDefault(), "%dx%d", supportedVideoSize.getWidth(), supportedVideoSize.getHeight()));
            }
        } else {
            addToCameraInfo(floidCameraInfo, CAMERA_INFO_SUPPORTED_VIDEO_SIZES, imageFormatName, CAMERA_INFO_VALUE_MAP_NULL);
        }
    }

    /**
     * See if the camera device supports this format
     *
     * @param cameraManager the camera manager
     * @param cameraDevice  the camera device
     * @param imageFormat   the image format (e.g. ImageFormat.JPEG)
     * @return true if supported
     */
    static boolean checkSupportedFormat(CameraManager cameraManager, CameraDevice cameraDevice, int imageFormat) {
        try {
            StreamConfigurationMap map = cameraManager.getCameraCharacteristics(cameraDevice.getId()).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            if (map != null) {
                return map.isOutputSupportedFor(imageFormat);
            } else {
                Log.e(TAG, "checkSupportedFormat: map was null");
            }
        } catch (CameraAccessException e) {
            Log.e(TAG, "checkSupportedFormat: Caught exception", e);
        }
        return false;
    }

    /**
     * Scan a media file for inclusion in gallery
     * @param context the context
     * @param path the path of the media file
     */
    static void scanMediaFile(Context context, String path) {
        MediaScannerConnection.scanFile(context,
                new String[] { path }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.d("ExternalStorage", "Scanned " + path + ":");
                        Log.d("ExternalStorage", "-> uri=" + uri);
                    }
                });
    }
}
