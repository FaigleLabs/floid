/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.imaging;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.*;

/**
 * Floid Camera Info
 */
public class FloidCameraInfo implements Parcelable {
    private String cameraId;
//    private String cameraInfo;
    private Map<String, List<String>> cameraInfo = new TreeMap<>();

    /**
     * Get the camera id
     * @return the camera id
     */
    public String getCameraId() {
        return cameraId;
    }

    /**
     * Set the camera id
     * @param cameraId the camera id
     */
    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    /**
     * Get the camera info
     * @return the camera info
     */
    public Map<String, List<String>> getCameraInfo() {
        return cameraInfo;
    }

    /**
     * Set the camera info
     * @param cameraInfo the camera info
     */
    @SuppressWarnings("unused")
    public void setCameraInfo(Map<String, List<String>> cameraInfo) {
        this.cameraInfo = cameraInfo;
    }

    /**
     * Add a value to the info via the key
     * @param key the key
     * @param value the value
     */
    public void addValue(String key, String value) {
        if(!cameraInfo.containsKey(key)) {
            cameraInfo.put(key, new ArrayList<String>());
        }
        cameraInfo.get(key).add(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cameraId);
        dest.writeInt(this.cameraInfo.size());
        for (Map.Entry<String, List<String>> entry : this.cameraInfo.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeStringList(entry.getValue());
        }
    }

    /**
     * Floid camera info
     */
    public FloidCameraInfo() {
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected FloidCameraInfo(Parcel in) {
        this.cameraId = in.readString();
        int cameraInfoSize = in.readInt();
        this.cameraInfo = new HashMap<>(cameraInfoSize);
        for (int i = 0; i < cameraInfoSize; i++) {
            String key = in.readString();
            List<String> value = in.createStringArrayList();
            this.cameraInfo.put(key, value);
        }
    }

    /**
     * The creator
     */
    public static final Creator<FloidCameraInfo> CREATOR = new Creator<FloidCameraInfo>() {
        @Override
        public FloidCameraInfo createFromParcel(Parcel source) {
            return new FloidCameraInfo(source);
        }

        @Override
        public FloidCameraInfo[] newArray(int size) {
            return new FloidCameraInfo[size];
        }
    };
}
