/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for land command
 */
@SuppressWarnings("SameParameterValue")
public class LandProcessBlock extends ProcessBlock {
    /**
     * Create a new land process block
     * @param timeoutInterval the timeout interval
     */
    public LandProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Land");
    }
}
