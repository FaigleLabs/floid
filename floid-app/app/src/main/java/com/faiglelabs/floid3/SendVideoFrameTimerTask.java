/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

import java.util.TimerTask;

/**
 * Send video preview frame timer task
 */
@SuppressWarnings("unused")
class SendVideoFrameTimerTask extends TimerTask {
    private final FloidService mFloidService;

    /**
     * Create a send preview frame timer task
     *
     * @param floidService the floid service
     */
    public SendVideoFrameTimerTask(FloidService floidService) {
        mFloidService = floidService;
    }

    @Override
    public void run() {
        if (mFloidService.mFloidDroidStatus.isTakingVideo()) {
            // Send a message to get the frame:
            mFloidService.serviceSendVideoFrame();
        }
    }
}
