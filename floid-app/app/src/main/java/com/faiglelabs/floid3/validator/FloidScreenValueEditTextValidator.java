/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Floid integer edit text validator
 */
@SuppressWarnings("SameParameterValue")
public class FloidScreenValueEditTextValidator extends FloidEditTextValidator {
    private final boolean mUseMinValue;
    private final int mMinValue;
    private final boolean mUseMinValueOther;
    private final int mMinValueOther;
    private final List<String> mValidValues;

    /**
     * Create a new floid screen edit text validator
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the callback
     * @param useMinValue                    use min value
     * @param minValue                       the min value
     * @param useMinValueOther               use other min value
     * @param minValueOther                  the other min value
     */
    public FloidScreenValueEditTextValidator(EditText editText, FloidEditTextValidatorCallback floidEditTextValidatorCallback, boolean useMinValue, int minValue, boolean useMinValueOther, int minValueOther) {
        super(editText, floidEditTextValidatorCallback);
        mUseMinValue = useMinValue;
        mMinValue = minValue;
        mUseMinValueOther = useMinValueOther;
        mMinValueOther = minValueOther;
        mValidValues = new ArrayList<>();
    }
    /**
     * Create a new floid screen edit text validator
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the callback
     * @param useMinValue                    use min value
     * @param minValue                       the min value
     * @param useMinValueOther               use other min value
     * @param minValueOther                  the other min value
     * @param validValues                    the list of valid values
     */
    @SuppressWarnings("unused")
    public FloidScreenValueEditTextValidator(EditText editText, FloidEditTextValidatorCallback floidEditTextValidatorCallback, boolean useMinValue, int minValue, boolean useMinValueOther, int minValueOther, List<String> validValues) {
        super(editText, floidEditTextValidatorCallback);
        mUseMinValue = useMinValue;
        mMinValue = minValue;
        mUseMinValueOther = useMinValueOther;
        mMinValueOther = minValueOther;
        mValidValues = validValues;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        checkText(s.toString());
        mFloidEditTextValidatorCallback.updateInterface();
    }

    private void checkText(String s) {
        // Clear the error:
        mEditText.setError(null);
        // Check if it equals a valid value:
        for (String validValue:mValidValues) {
            if (validValue != null) {
                if (validValue.equalsIgnoreCase(s)) {
                    return;
                }
            }
        }
        try {
            // 0. Format is "#x#"
            // 1. Split on x
            // 2. Both sides must have length > 0
            // 3. Both sides must be integer > 0
            // 4. Min size is mMinValue x mMinOtherValue or opposite
            String[] splitString = s.split("x");
            if(splitString.length != 2) {
                mEditText.setError("Format is #x#");
                return;
            }
            if(splitString[0].length() <1) {
                mEditText.setError("Format is #x#");
                return;
            }
            if(splitString[1].length() <1) {
                mEditText.setError("Format is #x#");
                return;
            }
            int leftSideInt;
            int rightSideInt;
            try {
                leftSideInt = Integer.parseInt(splitString[0]);
            } catch(Exception e) {
                mEditText.setError("Format is #x#");
                return;
            }
            try {
                rightSideInt = Integer.parseInt(splitString[1]);
            } catch(Exception e) {
                mEditText.setError("Format is #x#");
                return;
            }
            if(mUseMinValue) {
                if (leftSideInt < mMinValue) {
                    mEditText.setError("Left side too small - min: " + mMinValue);
                    return;
                }
                if (rightSideInt < mMinValue) {
                    mEditText.setError("Right side too small - min: " + mMinValue);
                    return;
                }
            }
            if(mUseMinValueOther) {
                if (leftSideInt * rightSideInt < mMinValue * mMinValueOther) {
                    mEditText.setError("Size must be at least " + mMinValue + "x" + mMinValueOther + " or " + mMinValueOther + "x" + mMinValue );
                }
            }
        } catch (Exception e) {
            mEditText.setError("Format: #x#");
        }
    }
}
