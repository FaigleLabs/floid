/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.DebugCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.DebugProcessBlock;

/**
 * Processes debug commands
 */
public class DebugCommandProcessor extends InstructionProcessor
{
    /**
     * Create a debug command processor
     * @param floidService the floid service
     * @param debugCommand the debug command
     */
    @SuppressWarnings("WeakerAccess")
    public DebugCommandProcessor(FloidService floidService, DebugCommand debugCommand)
    {
        super(floidService, debugCommand);
        useDefaultGoalProcessing = true;
        completeOnCommandResponse = true;
        setProcessBlock(new DebugProcessBlock(noGoalTimeout));
    }
    @Override
    public boolean setupInstruction()
    {
        super.setupInstruction();
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
        // Log it:
        if(floidService.mUseLogger) Log.d(FloidService.TAG, "===>SETUP " + this.getDroidInstruction().getType());
        DebugCommand      debugCommand      = (DebugCommand)droidInstruction;
        DebugProcessBlock debugProcessBlock = (DebugProcessBlock)processBlock;
        debugProcessBlock.setFloidGoalId(FloidService.NO_GOAL_ID);  // Does not use a goal id
        try
        {
            FloidOutgoingCommand debugControlOutgoingCommand = floidService.sendDebugControlToFloid((byte)debugCommand.getDeviceIndex(), (byte)debugCommand.getDebugValue());
            if(debugControlOutgoingCommand != null)
            {
                debugProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                debugProcessBlock.setProcessBlockWaitForCommandNumber(debugControlOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Debug Control command.");
                // We must have failed - set completion with error:
                debugProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;  // We are done with this whole command
    }
    @SuppressWarnings("EmptyMethod")
    @Override
    public boolean processInstruction()
    {
        return super.processInstruction();
    }
    @Override
    public boolean tearDownInstruction()
    {
        super.tearDownInstruction();
        return true;
    }
}
