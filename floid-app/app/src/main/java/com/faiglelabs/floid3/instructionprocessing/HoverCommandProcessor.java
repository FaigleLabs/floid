/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.os.SystemClock;

import com.faiglelabs.floid.servertypes.commands.HoverCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.HoverProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for hover command
 */
public class HoverCommandProcessor extends InstructionProcessor
{
    /**
     * Create a hover command processor
     * @param floidService the floid service
     * @param hoverCommand the hover command
     */
    @SuppressWarnings("WeakerAccess")
	public HoverCommandProcessor(FloidService floidService, HoverCommand hoverCommand)
	{
		super(floidService, hoverCommand);
        useDefaultGoalProcessing = false;
        completeOnCommandResponse = false;
		setProcessBlock(new HoverProcessBlock(FloidService.COMMAND_NO_TIMEOUT));   // This command does not time out
	}
	@Override
	public boolean setupInstruction()
	{
        super.setupInstruction();  // Sets processBlock to started
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
        if(!floidService.getFloidDroidStatus().isLiftedOff())
        {
            // Logic error - must be lifted off to Hover:
            sendCommandLogicErrorMessage("Not lifted off.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
        }
        HoverProcessBlock hoverProcessBlock = (HoverProcessBlock)processBlock;
        hoverProcessBlock.setStartTime(SystemClock.uptimeMillis());
        hoverProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
        return false;
	}
	@Override
	public boolean processInstruction()
	{
	    // Get our command and process block:
        HoverProcessBlock hoverProcessBlock = (HoverProcessBlock)processBlock;
        HoverCommand      hoverCommand      = (HoverCommand)droidInstruction;
        if(hoverProcessBlock.getProcessBlockStatus() < ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS)
        {
            super.processInstruction();
            // Have we timed out?
            // OK, check to see if our delay is finished
            if(SystemClock.uptimeMillis() >= (hoverCommand.getHoverTime()*1000) + hoverProcessBlock.getStartTime())
            {
                // Return:
                hoverProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                return true;
            }
            if(hoverProcessBlock.isTimedOut())
            {
                // Return:
                hoverProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_TIMEOUT_ERROR);
                return true;
            }
            // OK, check to see if our hover is finished
            if(SystemClock.uptimeMillis() >= hoverCommand.getHoverTime() + hoverProcessBlock.getProcessBlockStartTime())
            {
                // Return:
                hoverProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                return true;
            }
            // We did not complete:
            return false;  // This seems like an error condition...
        }
        return true;   // We are already done...
	}
	@Override
	public boolean tearDownInstruction()
	{
	    super.tearDownInstruction();
		return true;
	}
}
