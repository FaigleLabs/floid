/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.controller;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ToggleButton;

import com.faiglelabs.floid.servertypes.statuses.FloidStatus;
import com.faiglelabs.floid3.FloidActivity;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;

/**
 * Floid toggle button controller
 */
public class FloidToggleButtonController implements OnClickListener {
    private static final String TAG = "FLToggleButton";
    private final FloidActivity mFloidActivity;
    private final int mCommand;
    private final int mTarget;
    private final ToggleButton mToggleButton;

    /**
     * Create a floid toggle button controller
     *
     * @param floidActivity the floid activity
     * @param command       the command
     * @param target        the target
     * @param toggleButton  the toggle button
     * @param labelText     the label text
     */
    public FloidToggleButtonController(FloidActivity floidActivity, int command, int target, ToggleButton toggleButton, String labelText) {
        mFloidActivity = floidActivity;
        mCommand = command;
        mTarget = target;
        mToggleButton = toggleButton;
        mToggleButton.setText(labelText);
        mToggleButton.setTextOn(labelText);
        mToggleButton.setTextOff(labelText);
        mToggleButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (mFloidActivity != null) {
            switch (mCommand) {
                case FloidService.RELAY_PACKET: {
                    try {
                        mFloidActivity.sendRelayCommandToFloidService((byte) mTarget, (byte) (mToggleButton.isChecked() ? FloidService.RELAY_DEVICE_STATE_ON : FloidService.RELAY_DEVICE_STATE_OFF));
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Relay Packet Command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                }
                break;
                case FloidService.DEBUG_PACKET: {
                    try {
                        FloidOutgoingCommand debugOutgoingCommand = new FloidOutgoingCommand(FloidService.DEBUG_CONTROL_PACKET, FloidService.DEBUG_CONTROL_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                        debugOutgoingCommand.setByteToBuffer(FloidService.DEBUG_PACKET_INDEX_OFFSET, (byte) mTarget);
                        // We just send the current state:
                        debugOutgoingCommand.setByteToBuffer(FloidService.DEBUG_PACKET_VALUE_OFFSET, (byte) (mToggleButton.isChecked() ? FloidService.RELAY_DEVICE_STATE_ON : FloidService.RELAY_DEVICE_STATE_OFF));
                        mFloidActivity.sendCommandToFloidService(debugOutgoingCommand);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Debug command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                }
                break;
                case FloidService.FLOID_MODE_PACKET: {
                    try {
                        mFloidActivity.sendMissionModeToFloidService((mToggleButton.isChecked() ? FloidStatus.FLOID_MODE_MISSION : FloidStatus.FLOID_MODE_TEST));
                        FloidOutgoingCommand floidModeOutgoingCommand = new FloidOutgoingCommand(FloidService.FLOID_MODE_PACKET, FloidService.FLOID_MODE_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                        floidModeOutgoingCommand.setByteToBuffer(FloidService.FLOID_MODE_PACKET_MODE_OFFSET, (byte) (mToggleButton.isChecked() ? FloidStatus.FLOID_MODE_MISSION : FloidStatus.FLOID_MODE_TEST));
                        mFloidActivity.sendCommandToFloidService(floidModeOutgoingCommand);
                    } catch (Exception e) {
                        if (mFloidActivity.mFloidActivityUseLogger)
                            Log.e(TAG, "Failed to make/send Floid Mode command: " + Log.getStackTraceString(e) + " " + e.toString());
                    }
                }
                break;
                default: {
                    Log.e(TAG, "onClick: DID NOT SEND THE FOLLOWING COMMAND: " + mCommand);
                }
            }
        }
    }
}
