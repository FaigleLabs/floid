/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import com.faiglelabs.floid.servertypes.commands.StopPhotoStrobeCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.StopPhotoStrobeProcessBlock;

/**
 * Command processor for stop photo strobe command
 */
@SuppressWarnings("unused")
public class StopPhotoStrobeCommandProcessor extends InstructionProcessor
{
	/**
	 * Create a stop photo strobe command processor
	 * @param floidService the floid service
	 * @param stopPhotoStrobeCommand the stop photo strobe command
	 */
    public StopPhotoStrobeCommandProcessor(FloidService floidService, StopPhotoStrobeCommand stopPhotoStrobeCommand)
	{
		super(floidService, stopPhotoStrobeCommand);
		useDefaultGoalProcessing = true; // We are good with default goal processing
		completeOnCommandResponse = false;
		stopMission = false; // We do kill the mission if this command fails
		setProcessBlock(new StopPhotoStrobeProcessBlock(defaultGoalTimeout));
	}
	@Override
	public boolean setupInstruction()
	{
	    // General idea:
	    // Turn off the setPhotoStrobe flag so that more pictures will not be taken - performs no other action
	    super.setupInstruction();
	    if(!floidService.getFloidDroidStatus().isPhotoStrobe())
	    {
            // Logic error - can't stop photo strobe if not taking photo strobe
            sendCommandLogicErrorMessage("Not taking photo strobe - currently not currently taking photo strobe");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
	    }
        floidService.getFloidDroidStatus().setPhotoStrobe(false);
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
        return true;    // We are done with this whole command
	}
	@Override
	public boolean processInstruction()
	{
	    if(super.processInstruction())
	    {
	        return true;
	    }
	    return true;   // We are done with this command anyway...
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
