package com.faiglelabs.floid3.helper;

public class FloidDebugMessageAndTag {
    private final String tag;
    private final String message;
    public FloidDebugMessageAndTag(String tag, String message) {
        this.tag = tag;
        this.message = message;
    }
    public String getTag() {
        return tag;
    }
    public String getMessage() {
        return message;
    }
}
