/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

/**
 * Interface for parameter modified handlers
 */
public interface FloidParameterModifiedHandler {
    /**
     * @param parameter the parameter being modified
     * @param newValue the new parameter value
     * @param floidService the floid service
     */
    @SuppressWarnings({"UnusedParameters", "EmptyMethod"})
    void parameterModifiedPre(String parameter, String newValue, FloidService floidService);

    /**
     * @param parameter the parameter being modified
     * @param originalValue the new parameter value
     * @param floidService the floid service
     */
    @SuppressWarnings("UnusedParameters")
    void parameterModifiedPost(String parameter, String originalValue, FloidService floidService);
}
