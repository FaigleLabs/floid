/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Floid decimal edit text validator
 */
public class FloidDecimalEditTextValidator extends FloidEditTextValidator {
    private final boolean mUseMinValue;
    private final double mMinValue;
    private final boolean mUseMaxValue;
    private final double mMaxValue;
    private List<String> mValidValues;

    /**
     * Create a validator for this edit text
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the callback
     * @param useMinValue                    use min value
     * @param minValue                       min value
     * @param useMaxValue                    use max value
     * @param maxValue                       max value
     */
     public FloidDecimalEditTextValidator(EditText editText,
                                         FloidEditTextValidatorCallback floidEditTextValidatorCallback,
                                         boolean useMinValue, double minValue, boolean useMaxValue,
                                         double maxValue) {
        super(editText, floidEditTextValidatorCallback);
        mUseMinValue = useMinValue;
        mMinValue = minValue;
        mUseMaxValue = useMaxValue;
        mMaxValue = maxValue;
        mValidValues = new ArrayList<>();
    }

    /**
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the callback
     * @param useMinValue                    use min value
     * @param minValue                       min value
     * @param useMaxValue                    use max value
     * @param maxValue                       max value
     * @param validValues                    the list of valid values
     */
    public FloidDecimalEditTextValidator(EditText editText,
                                        FloidEditTextValidatorCallback floidEditTextValidatorCallback,
                                        boolean useMinValue, double minValue, boolean useMaxValue,
                                        double maxValue, List<String> validValues) {
        super(editText, floidEditTextValidatorCallback);
        mUseMinValue = useMinValue;
        mMinValue = minValue;
        mUseMaxValue = useMaxValue;
        mMaxValue = maxValue;
        mValidValues = validValues;
    }

    /**
     * Set the valid values
     * @param validValues the valid values
     */
    @SuppressWarnings("unused")
    public void setValidValues(List<String> validValues) {
        this.mValidValues = validValues;
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        checkText(s.toString());
        mFloidEditTextValidatorCallback.updateInterface();
    }

    private void checkText(String s) {
        mEditText.setError(null);
        // Check if it equals a valid value:
        for (String validValue:mValidValues) {
            if (validValue != null) {
                if (validValue.equalsIgnoreCase(s)) {
                    mEditText.setError(null);
                    return;
                }
            }
        }
        try {
            float f = Float.valueOf(s);
//            float f = Float.valueOf(mEditText.getText().toString());
            if (mUseMinValue) {
                if (f < mMinValue) {
                    mEditText.setError("Value too small: " + mMinValue
                            + " minimum");
                    return;
                }
            }
            if (mUseMaxValue) {
                if (f > mMaxValue) {
                    mEditText.setError("Value too large: " + mMaxValue
                            + " minimum");
                    return;
                }
            }
            mEditText.setError(null);
        } catch (NumberFormatException nfe) {
            mEditText.setError("Decimal value required");
        }
    }
}
