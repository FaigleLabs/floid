/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.hardware.GeomagneticField;
import android.util.Log;
import com.faiglelabs.floid.servertypes.commands.AcquireHomePositionCommand;
import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import com.faiglelabs.floid.servertypes.statuses.FloidDroidStatus;
import com.faiglelabs.floid.servertypes.statuses.FloidStatus;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.AcquireHomePositionProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * The acquire home position command processor
 */
public class AcquireHomePositionCommandProcessor extends InstructionProcessor {
    private boolean mDeclinationSet = false;
    private static final String TAG = "AHPCmdProcessor";
    private static final Long ACQUIRE_HOME_POSITION_TIME_MILLIS = 60000L;

    /**
     * @param floidService               the floid service for this command
     * @param acquireHomePositionCommand the acquire home position command
     */
    @SuppressWarnings("WeakerAccess")
    public AcquireHomePositionCommandProcessor(FloidService floidService, AcquireHomePositionCommand acquireHomePositionCommand) {
        super(floidService, acquireHomePositionCommand);
        setProcessBlock(new AcquireHomePositionProcessBlock(ACQUIRE_HOME_POSITION_TIME_MILLIS, acquireHomePositionCommand));
    }

    @Override
    public boolean setupInstruction() {
        super.setupInstruction();  // Sets processBlock to started

        AcquireHomePositionProcessBlock ahpProcessBlock = (AcquireHomePositionProcessBlock) processBlock;
        ahpProcessBlock.setAcquireState(AcquireHomePositionProcessBlock.ACQUIRE_STATE_START);
        AcquireHomePositionCommand acquireHomePositionCommand = (AcquireHomePositionCommand) droidInstruction;

        // Test to see if we skip these tests.
        // The flag is requiredAltimeterGoodCount=0 to skip:
        if (acquireHomePositionCommand.getRequiredAltimeterGoodCount() > 0) {
            if (!floidService.getFloidDroidStatus().isFloidConnected()) {
                sendCommandLogicErrorMessage("Floid Not Connected");
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return true;       // we are done but this is ignored above anyway and processInstruction is still called            
            }
            // GPS On?
            if (!floidService.getFloidStatus().isDg()) {
                if (ahpProcessBlock.getAcquireState() != AcquireHomePositionProcessBlock.ACQUIRE_STATE_TURNING_ON_GPS) {
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_TURNING_ON_GPS, "");
                    // Command to turn on the gps:
                    floidService.sendRelayCommandToFloid((byte) FloidService.RELAY_DEVICE_GPS, (byte) FloidService.RELAY_DEVICE_STATE_ON);
                }
            }
            // IMU On?
            if (!floidService.getFloidStatus().isDp()) {
                if (ahpProcessBlock.getAcquireState() != AcquireHomePositionProcessBlock.ACQUIRE_STATE_TURNING_ON_IMU) {
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_TURNING_ON_IMU, "");
                    // Command to turn on the pyr:
                    floidService.sendRelayCommandToFloid((byte) FloidService.RELAY_DEVICE_PYR, (byte) FloidService.RELAY_DEVICE_STATE_ON);
                }
            }
        }
        // Logic: Set us up to get a set number of GPS hits (AND PYR FOR HEADING!) and determine if we have a good home position - ultimately time out after a period of time
        ahpProcessBlock.setToDefault();
        useDefaultGoalProcessing = false; // We are not going to wait on particular commands but rather process ourselves
        completeOnCommandResponse = false; // No - do not complete on command response
        ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
        return false;
    }

    @Override
    public boolean processInstruction() {
        if (super.processInstruction()) {
            return true;   // Already done...
        }
        AcquireHomePositionProcessBlock ahpProcessBlock = (AcquireHomePositionProcessBlock) processBlock;
        AcquireHomePositionCommand acquireHomePositionCommand = (AcquireHomePositionCommand) droidInstruction;

        // Do we skip tests (The flag for this is required altimeter good count == 0):
        if (acquireHomePositionCommand.getRequiredAltimeterGoodCount() == 0) {
            ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
            return true; // We are done...
        }
        // Do we need to set the declination?
        if (!mDeclinationSet) {
            // This just keeps track of the max gps good count before setting declination only for debug purposes:
            if (floidService.getFloidStatus().getJg() > ahpProcessBlock.getMaxGoodGpsCount()) {
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "AHP: Max GPS count for declination: " + floidService.getFloidStatus().getJg() + " / " + acquireHomePositionCommand.getRequiredAltimeterGoodCount());
                ahpProcessBlock.setMaxGoodGpsCount(floidService.getFloidStatus().getJg());
            }
            // Do we have enough good GPS counts?
            if (floidService.getFloidStatus().getJg() >= acquireHomePositionCommand.getRequiredGPSGoodCount()) {
                try {
                    float declination = new GeomagneticField((float) floidService.getFloidStatus().getJyf()  /* Lat: y-degrees-decimal-filtered */,
                            (float) floidService.getFloidStatus().getJxf()                                                    /* Lng: x-degrees-decimal-filtered */,
                            (float) floidService.getFloidStatus().getJmf(),                                                   /* Alt: gps-z-meters filtered */
                            System.currentTimeMillis()).getDeclination();
                    if (floidService.mUseLogger) Log.d(FloidService.TAG, "AcquireHomePosition: Setting declination to: " + declination);
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_SETTING_DECLINATION, "");
                    int declinationCommandNumber = floidService.getNextFloidCommandNumber();
                    try {
                        FloidOutgoingCommand declinationOutgoingCommand = new FloidOutgoingCommand(FloidService.DECLINATION_PACKET, FloidService.DECLINATION_PACKET_SIZE, declinationCommandNumber, FloidService.NO_GOAL_ID);
                        declinationOutgoingCommand.setFloatToBuffer(FloidService.DECLINATION_PACKET_DECLINATION_OFFSET, declination);
                        floidService.sendCommandToFloid(declinationOutgoingCommand);
                        mDeclinationSet = true;
                        FloidStatus floidStatus = floidService.getFloidStatus();
                        // We want to skip a round and make sure the declination gets set before starting to get readings:
                        if (floidStatus != null) {
                            ahpProcessBlock.setLastFloidStatusUpdateTime(floidStatus.getSt() + 1000); // Wait an additional 1 second to make sure we are set with declination
                        }
                        return false;
                    } catch (Exception e) {
                        if (floidService.mUseLogger) Log.e(TAG, "Failed to make/send declination command: " + Log.getStackTraceString(e) + " " + e.toString());
                        changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_FAILED, "Failed to make/send declination command");
                        ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                        return true;
                    }
                } catch (Exception e) {
                    if (floidService.mUseLogger) Log.e(TAG, "AcquireHomePosition: Error setting declination", e);
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_FAILED, "Error setting declination");
                    ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                    return true;
                }
            }
        }
        if (ahpProcessBlock.getProcessBlockStatus() == ProcessBlock.PROCESS_BLOCK_IN_PROGRESS) {
            ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_STARTED);
        }
        FloidStatus floidStatus = floidService.getFloidStatus();
        if (floidStatus != null) {
            // Is it newer than the last one we processed?
            if (ahpProcessBlock.getLastFloidStatusUpdateTime() < floidStatus.getSt()) {
                // New one - process it and see if we finished our command.
                return processFloidStatus(floidStatus);
            }
        }
        return false;  // We have not completed
    }

    private boolean processFloidStatus(FloidStatus floidStatus) {
        AcquireHomePositionProcessBlock ahpProcessBlock = (AcquireHomePositionProcessBlock) this.getProcessBlock();
        AcquireHomePositionCommand acquireHomePositionCommand = (AcquireHomePositionCommand) droidInstruction;
        ahpProcessBlock.setLastFloidStatusUpdateTime(floidStatus.getSt());
        try {
            FloidDroidStatus floidDroidStatus = floidService.getFloidDroidStatus();
            if (floidDroidStatus != null) {
                if (!floidDroidStatus.isFloidConnected()) {
                    if (floidService.mUseLogger) Log.e(FloidService.TAG, "AcquireHomePosition: NOT CONNECTED");
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_FAILED, "Floid not connected");
                    ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                    return true;
                }
            } else {
                if (floidService.mUseLogger) Log.e(FloidService.TAG, "AcquireHomePosition: Floid Droid Status is null");
                changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_FAILED, "Floid Droid status is null");
                ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return true; // No droid status
            }
            if (!mDeclinationSet) {
                if (ahpProcessBlock.getAcquireState() != AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_DECLINATION) {
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_DECLINATION, "");
                }
                ahpProcessBlock.setToDefault();  // Declination not set yet...
            } else if (floidStatus.getJg() < acquireHomePositionCommand.getRequiredGPSGoodCount())  // We need a certain GPS good count
            {
                if (ahpProcessBlock.getAcquireState() != AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_GPS_COUNT) {
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_GPS_COUNT, "");
                }
                ahpProcessBlock.setToDefault();  // Not a good GPS
            } else if (!floidStatus.isAi()) {
                // Altimeter not initialized
                if (ahpProcessBlock.getAcquireState() != AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_ALTIMETER_INITIALIZATION) {
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_ALTIMETER_INITIALIZATION, "");
                }
                ahpProcessBlock.setToDefault();  // Not a good Altimeter
            } else if (!floidStatus.isAv()) {
                // Altimeter not valid
                if (ahpProcessBlock.getAcquireState() != AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_ALTIMETER_VALIDATION) {
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_ALTIMETER_VALIDATION, "");
                }
                ahpProcessBlock.setToDefault();  // Not a good Altimeter
            } else if (floidStatus.getAc() < acquireHomePositionCommand.getRequiredAltimeterGoodCount()) {
                if (ahpProcessBlock.getAcquireState() != AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_ALTIMETER_COUNT) {
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_WAITING_ON_ALTIMETER_COUNT, "");
                }
                ahpProcessBlock.setToDefault();  // Not a good Altimeter
            } else {
                // We are ok to test:
                if (ahpProcessBlock.getAcquireState() < AcquireHomePositionProcessBlock.ACQUIRE_STATE_ACQUIRING) {
                    changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_ACQUIRING, "");
                }
                // Test the state:
                int newAcquireState = ahpProcessBlock.acquireAndTest(floidDroidStatus, floidStatus.getJxf(), floidStatus.getJyf(), floidStatus.getKa(), floidStatus.getPhs(), floidStatus.getSt());
                if (newAcquireState != ahpProcessBlock.getAcquireState()) {
                    changeAcquireState(ahpProcessBlock, newAcquireState, "");
                }
                if (newAcquireState == AcquireHomePositionProcessBlock.ACQUIRE_STATE_ACQUIRED) {
                    ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                    return true; // WE ARE DONE
                }
                if (newAcquireState == AcquireHomePositionProcessBlock.ACQUIRE_STATE_FAILED) {
                    ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                    return true; // WE ARE DONE
                }
            }
        } catch (Exception e) {
            if (floidService.mUseLogger) Log.e(FloidService.TAG, "AcquireHomePosition: " + e.toString());
            changeAcquireState(ahpProcessBlock, AcquireHomePositionProcessBlock.ACQUIRE_STATE_FAILED, "Exception: " + e.toString());
            ahpProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true; // We are done
        }
        return false;      // Not done in any case
    }

    private void changeAcquireState(AcquireHomePositionProcessBlock ahpProcessBlock, int state, String message) {
        if (message == null) {
            message = "";
        }
        // Log the state change:
        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "AHP: Changing state from " + ahpProcessBlock.getAcquireStateString() + " (" + ahpProcessBlock.getAcquireState() + ") to " + ahpProcessBlock.getAcquireStateString(state) + " (" + state + ")" + (message.length() > 0 ? (" " + message) : ("")));
        // Change the state:
        ahpProcessBlock.setAcquireState(state);
    }

    @Override
    public boolean tearDownInstruction() {
        super.tearDownInstruction();
        return true;
    }
}
