/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;
import com.faiglelabs.floid.servertypes.commands.LiftOffCommand;
import com.faiglelabs.floid.servertypes.statuses.FloidStatus;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.LiftOffProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for the lift off command
 */
public class LiftOffCommandProcessor extends InstructionProcessor
{
    /**
     * Create a lift off command processor
     * @param floidService the floid service
     * @param liftOffCommand the lift off command
     */
    @SuppressWarnings("WeakerAccess")
	public LiftOffCommandProcessor(FloidService floidService, LiftOffCommand liftOffCommand)
	{
		super(floidService, liftOffCommand);
		useDefaultGoalProcessing = true;
		completeOnCommandResponse = false;
		setProcessBlock(new LiftOffProcessBlock(defaultLongGoalTimeout));
	}
	@Override
	public boolean setupInstruction() {
        // General idea:
        // This is generally the same as FlyTo and TurnTo...
        super.setupInstruction();
        if (!floidService.getFloidDroidStatus().isFloidConnected()) {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
        if(!goodLiftOffStatus()) {
            return true;
        }
        LiftOffCommand liftOffCommand = (LiftOffCommand) droidInstruction;
        LiftOffProcessBlock liftOffProcessBlock = (LiftOffProcessBlock) processBlock;
        liftOffProcessBlock.setFloidGoalId(floidService.getNextFloidGoalId());
        try {
            // Set the lift-off goals as follows but note that these will be replaced in the controller
            // with current lat,lng and heading although goal altitude will remain as set here:
            FloidOutgoingCommand liftOffOutgoingCommand = floidService.sendLiftOffCommandToFloid(liftOffProcessBlock.getFloidGoalId(),
                    (float) floidService.getFloidStatus().getJxf(), // Smoothed Long = x
                    (float) floidService.getFloidStatus().getJyf(), // Smoothed Lat = y
                    (float) (liftOffCommand.getZ() + (liftOffCommand.isAbsolute() ? 0 : floidService.getFloidStatus().getJmf())), // Smoothed altitude plus offset or absolute = z
                    (float) floidService.getFloidStatus().getPhs());  // Smoothed heading in std trig angle
            if (liftOffOutgoingCommand != null) {
                liftOffProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                liftOffProcessBlock.setProcessBlockWaitForCommandNumber(liftOffOutgoingCommand.getCommandNumber());
                return true;
            } else {
                if (floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Lift Off command.");
                // We must have failed - set completion with error:
                liftOffProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        } catch (Exception e) {
            if (floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;    // We are done with this whole command
    }

    /**
     * Check for good lift off status based on mode
     * @return true if status is good
     */
    private boolean goodLiftOffStatus() {
        // We perform this check in all modes:
        if (floidService.getFloidDroidStatus().isLiftedOff()) {
            // Logic error - already lifted off:
            sendCommandLogicErrorMessage("Already lifted off.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return false;       // we are done but this is ignored above anyway and processInstruction is still called
        }
        // We perform these checks only in mission mode:
        if (floidService.getFloidStatus().getFm() == FloidStatus.FLOID_MODE_MISSION) {
            if (!floidService.getFloidDroidStatus().isHelisStarted()) {
                // Logic error - helis not started...
                sendCommandLogicErrorMessage("Helis not started.");
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;       // we are done but this is ignored above anyway and processInstruction is still called
            }
            if (!floidService.getFloidDroidStatus().isHomePositionAcquired()) {
                // Logic error - must have acquired a home position first:
                sendCommandLogicErrorMessage("Home position not acquired.");
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;       // we are done but this is ignored above anyway and processInstruction is still called
            }
        }
        // OK, passed all tests...
        return true;
    }
	@Override
	public boolean processInstruction()
	{
	    // This is processed in standard way - when goal is achieved true and a success status
	    boolean completed = super.processInstruction();
        if(completed)
	    {
            if(processBlock.getProcessBlockStatus() == ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS) {
                floidService.getFloidDroidStatus().setLiftedOff(true);
            }
	    }
        return completed;
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
