/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.controller;

import com.faiglelabs.floid3.FloidActivity;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.R;

import android.content.res.Resources;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Floid declination controller
 */
public class FloidDeclinationController implements OnClickListener
{
    private final FloidActivity mFloidActivity;
    @SuppressWarnings("FieldCanBeLocal")
    private final        Button         mButton;
    private final        int            mDeclinationEditTextId;
    private static final String         TAG                      = "FLDeclinationController";

    /**
     * Create a floid declination controller
     * @param floidActivity the floid activity
     * @param viewId the view id
     * @param subViewId the sub view id
     * @param labelText the label text
     * @param res the resources
     * @param declinationEditTextId the edit text id
     */
    @SuppressWarnings("UnusedParameters")
    public FloidDeclinationController(FloidActivity floidActivity,
                                 int            viewId,
                                 int            subViewId,
                                 String         labelText,
                                 Resources      res,
                                 int            declinationEditTextId)
    {
        mFloidActivity = floidActivity;
        mDeclinationEditTextId  = declinationEditTextId;
        View view               = mFloidActivity.findViewById(viewId);
        ViewGroup subView       = view.findViewById(subViewId);
        ((TextView) subView.getChildAt(0)).setText(labelText);
        mButton                 = subView.findViewById(R.id.floid_button);
        mButton.setOnClickListener(this);
        mButton.setText(labelText);
    }
    public void onClick(View v)
    {
        if (mFloidActivity != null)
        {
            try
            {
                EditText             declinationEditText        = mFloidActivity.findViewById(mDeclinationEditTextId);
                String               declinationString          = declinationEditText.getText().toString();
                float                declinationValue           = Float.valueOf(declinationString);
                FloidOutgoingCommand declinationOutgoingCommand = new FloidOutgoingCommand(FloidService.DECLINATION_PACKET, FloidService.DECLINATION_PACKET_SIZE, -1, FloidService.NO_GOAL_ID);
                declinationOutgoingCommand.setFloatToBuffer(FloidService.DECLINATION_PACKET_DECLINATION_OFFSET, declinationValue);
                mFloidActivity.sendCommandToFloidService(declinationOutgoingCommand);
            }
            catch(Exception e)
            {
                if(mFloidActivity.mFloidActivityUseLogger) Log.e(TAG, "Failed to make/send Declination Command: " + Log.getStackTraceString(e) + " " + e.toString());
            }
        }
    }
}
