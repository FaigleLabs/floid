/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

/**
 * Floid edit text validator
 */
class FloidEditTextValidator implements TextWatcher {
    /**
     * the edit text
     */
    final EditText mEditText;
    /**
     * the validator callback
     */
    final FloidEditTextValidatorCallback mFloidEditTextValidatorCallback;

    /**
     * Create a floid edit text validator
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the callback
     */
    FloidEditTextValidator(EditText editText, FloidEditTextValidatorCallback floidEditTextValidatorCallback) {
        mEditText = editText;
        mFloidEditTextValidatorCallback = floidEditTextValidatorCallback;
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mFloidEditTextValidatorCallback.updateInterface();
    }
}
