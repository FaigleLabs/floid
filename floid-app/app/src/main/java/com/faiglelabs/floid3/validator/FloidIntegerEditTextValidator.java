/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Floid integer edit text validator
 */
@SuppressWarnings("SameParameterValue")
public class FloidIntegerEditTextValidator extends FloidEditTextValidator {
    private final boolean mUseMinValue;
    private final int mMinValue;
    private final boolean mUseMaxValue;
    private final int mMaxValue;
    private final List<String> mValidValues;

    /**
     * Create a new floid integer edit text validator
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the callback
     * @param useMinValue                    use min value
     * @param minValue                       the min value
     * @param useMaxValue                    use max value
     * @param maxValue                       the max value
     */
    public FloidIntegerEditTextValidator(EditText editText, FloidEditTextValidatorCallback floidEditTextValidatorCallback, boolean useMinValue, int minValue, boolean useMaxValue, int maxValue) {
        super(editText, floidEditTextValidatorCallback);
        mUseMinValue = useMinValue;
        mMinValue = minValue;
        mUseMaxValue = useMaxValue;
        mMaxValue = maxValue;
        mValidValues = new ArrayList<>();
    }
    /**
     * Create a new floid integer edit text validator
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the callback
     * @param useMinValue                    use min value
     * @param minValue                       the min value
     * @param useMaxValue                    use max value
     * @param maxValue                       the max value
     * @param validValues                    the list of valid values
     */
    public FloidIntegerEditTextValidator(EditText editText, FloidEditTextValidatorCallback floidEditTextValidatorCallback, boolean useMinValue, int minValue, boolean useMaxValue, int maxValue, List<String> validValues) {
        super(editText, floidEditTextValidatorCallback);
        mUseMinValue = useMinValue;
        mMinValue = minValue;
        mUseMaxValue = useMaxValue;
        mMaxValue = maxValue;
        mValidValues = validValues;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        checkText(s.toString());
        mFloidEditTextValidatorCallback.updateInterface();
    }

    private void checkText(String s) {
        mEditText.setError(null);
        // Check if it equals a valid value:
        for (String validValue:mValidValues) {
            if (validValue != null) {
                if (validValue.equalsIgnoreCase(s)) {
                    mEditText.setError(null);
                    return;
                }
            }
        }
        try {
//            int i = Integer.valueOf(mEditText.getText().toString());
            int i = Integer.parseInt(s);
            if (mUseMinValue) {
                if (i < mMinValue) {
                    mEditText.setError("Value too small: " + mMinValue + " minimum");
                    return;
                }
            }
            if (mUseMaxValue) {
                if (i > mMaxValue) {
                    mEditText.setError("Value too large: " + mMaxValue + " minimum");
                    return;
                }
            }
            mEditText.setError(null);
        } catch (NumberFormatException nfe) {
            mEditText.setError("Integer value required");
        }
    }
}
