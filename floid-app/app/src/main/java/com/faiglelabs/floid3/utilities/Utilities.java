/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.utilities;

import android.graphics.drawable.Drawable;

/**
 * Utility class
 */
@SuppressWarnings("unused")
class Utilities {
    /**
     * Center a drawable around a point
     * @param x the x point
     * @param y the y point
     * @param d the drawable
     */
    @SuppressWarnings("unused")
	static void centerAround(int x, int y, Drawable d) {
		int w = d.getIntrinsicWidth();
		int h = d.getIntrinsicHeight();
		int left = x - w / 2;
		int top = y - h / 2;
		int right = left + w;
		int bottom = top + h;
		d.setBounds(left, top, right, bottom);
	}
}
