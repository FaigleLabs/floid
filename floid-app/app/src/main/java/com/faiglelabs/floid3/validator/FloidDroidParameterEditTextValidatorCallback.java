/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

import com.faiglelabs.floid3.helper.DroidParameterEditState;

import java.util.List;

/**
 * Interface for floid droid parameter edit text callback
 */
public abstract class FloidDroidParameterEditTextValidatorCallback extends FloidEditTextValidatorCallback {

    /**
     * The edit state list:
     */
    @SuppressWarnings("WeakerAccess")
    protected List<DroidParameterEditState> editStateList;
    /**
     * The edit state for the droid parameters
     */
    protected DroidParameterEditState editState;

    /**
     * Get the edit state list
     * @return the edit state list
     */
    @SuppressWarnings("unused")
    public List<DroidParameterEditState> getEditStateList() {
        return editStateList;
    }

    /**
     * Set the edit state list
     * @param editStateList the edit state list
     */
    public void setEditStateList(List<DroidParameterEditState> editStateList) {
        this.editStateList = editStateList;
    }

    /**
     * Get the edit state
     * @return the edit state
     */
    @SuppressWarnings("unused")
    public DroidParameterEditState getEditState() {
        return editState;
    }

    /**
     * Set the edit state
     * @param editState the edit state
     */
    public void setEditState(DroidParameterEditState editState) {
        this.editState = editState;
    }
}
