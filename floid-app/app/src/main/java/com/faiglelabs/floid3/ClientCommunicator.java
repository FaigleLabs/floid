/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

import android.content.Context;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import com.faiglelabs.floid.utils.FloidServerMessageStatus;
import com.faiglelabs.floid.utils.FloidServerMessageUtils;
import com.faiglelabs.proto.floid.FloidServerMessages.FloidServerMessage;

import javax.net.ssl.*;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * The client communicator to connect to a floid server
 */
class ClientCommunicator {
    private static final String TAG = "ClientCommunicator";
    private static final int mConnectTimeout = 500;
    private static final int mSocketTimeout = 4000;
    private final Context mContext;
    private boolean mConnected = false;
    private SocketAddress mFloidServerAddress;
    private SSLSocket mSSLSocket = null;
    private SSLSocketFactory mSSLSocketFactory = null;
    private BufferedInputStream mBufferedInputStream = null;
    private PrintStream mPrintStream = null;


    private final FloidService mFloidService;
    private final String mHostAddress;
    private final int mHostPort;
    private final boolean sslContextGood;
    /**
     * @noinspection FieldCanBeLocal
     */
    @SuppressWarnings("unused")
    private boolean lastConnectFailed = false;

    /**
     * Create a client communicator
     *
     * @param context      the service context
     * @param floidService the floid service
     * @param hostAddress  the host address
     * @param hostPort     the host port
     */
    @SuppressWarnings("WeakerAccess")
    public ClientCommunicator(Context context, FloidService floidService, String hostAddress, int hostPort) {
        mContext = context;
        mHostAddress = hostAddress;
        mHostPort = hostPort;
        mFloidService = floidService;
        if(context!=null) {
            sslContextGood = setupSSLContext();
        } else {
            sslContextGood = false;
        }
        if(sslContextGood) {
            checkAndOpenConnection();
        }
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    private synchronized boolean setupSSLContext() {
        if (mFloidService.mUseLogger) Log.d(TAG, "Setting up SSL Context");
        SSLContext sslContext;
        try {
            try {
                sslContext = SSLContext.getInstance("TLSv1.3");
            } catch (NullPointerException npe) {
                if (mFloidService.mUseLogger) Log.e(TAG, "SSLContext Null Pointer Exception");
                if (mFloidService.mUseLogger) Log.d(TAG, "Failed setting up SSL context");
                return false;
            } catch (NoSuchAlgorithmException nsae) {
                if (mFloidService.mUseLogger) Log.e(TAG, "SSLContext: No Such Algorithm Exception");
                if (mFloidService.mUseLogger) Log.d(TAG, "Failed setting up SSL context");
                return false;
            }
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("X509");
            String keyStorePasswordString = BuildConfig.CLIENT_KEYSTORE_PASSWORD;
            String certificatePasswordString = BuildConfig.CLIENT_CERTIFICATE_PASSWORD;
            char[] keyStorePassword = keyStorePasswordString.toCharArray();
            char[] certificatePassword = certificatePasswordString.toCharArray();
            KeyStore keyStore = KeyStore.getInstance("BKS");
            InputStream keyStoreInputStream = null;
            try {
                if ((mContext != null) && (mContext.getResources() != null)) {
                    keyStoreInputStream = mContext.getResources().openRawResource(R.raw.floid_client); // Note: False positive in FindBugs
                    keyStore.load(keyStoreInputStream, keyStorePassword);
                    keyStoreInputStream.close();
                }
                keyStoreInputStream = null;
            } catch (Exception e) {
                if (mFloidService.mUseLogger) Log.e(TAG, "Exception reading key store input stream", e);
            }
            if (keyStoreInputStream != null) {
                try {
                    keyStoreInputStream.close();
                } catch (Exception e) {
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception closing key store input stream", e);
                }
            }
            keyManagerFactory.init(keyStore, certificatePassword);
            KeyManager[] keyManagerList = keyManagerFactory.getKeyManagers();
            // Load our trusted certificate:
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate ca;
            if ((mContext != null) && (mContext.getResources() != null)) {
                InputStream caInput = mContext.getResources().openRawResource(R.raw.floid_server);
                try {
                    ca = cf.generateCertificate(caInput);
                    if (mFloidService.mUseLogger) Log.i(TAG, "ca=" + ((X509Certificate) ca).getSubjectDN());
                } finally {
                    caInput.close();
                }
            } else {
                return false;
            }
            // Create a trust store containing our trusted CAs
            String trustedKeyStoreType = KeyStore.getDefaultType();
            KeyStore trustedKeyStore = KeyStore.getInstance(trustedKeyStoreType);
            trustedKeyStore.load(null, null);
            trustedKeyStore.setCertificateEntry("floid_server_cert", ca);
            if (mFloidService.mUseLogger) Log.i(TAG, "trustedKeyStore: " + trustedKeyStore);
            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(trustedKeyStore);
            // Init the SSL context with our key and trust managers
            sslContext.init(keyManagerList, tmf.getTrustManagers(), null);
            mSSLSocketFactory = sslContext.getSocketFactory();
            // Generating SSLSocket:
            mFloidServerAddress = new InetSocketAddress(InetAddress.getByName(mHostAddress), mHostPort);
            if (mFloidService.mUseLogger) Log.d(TAG, String.format("FloidServer ADDRESS: %s:%d", mHostAddress, mHostPort));
            mSSLSocket = (SSLSocket) mSSLSocketFactory.createSocket();
            mSSLSocket.setEnabledProtocols(new String[]{"TLSv1.3"});
        } catch (Exception e) {
            if (mFloidService.mUseLogger) Log.e(TAG, e.toString());
            if (mFloidService.mUseLogger) Log.d(TAG, "Failed setting up SSL context");
            setServerStatus(false, true);
            return false;
        }
        if (mFloidService.mUseLogger) Log.d(TAG, "Done setting up SSL context");
        return true;
    }

    /**
     * Returns if connected
     *
     * @return true if connected
     */
    @SuppressWarnings("unused")
    public boolean isConnected() {
        return mConnected;
    }

    /**
     * Clear and close connection as needed
     */
    public synchronized void clear() {
        // Set not connected:
        mConnected = false;
        // Close streams in reverse order:
        if (mPrintStream != null) {
            try {
                mPrintStream.close();
                mPrintStream = null;

            } catch (Exception e) {
                Log.e(TAG, "Exception closing print stream", e);
            }
        }
        if (mBufferedInputStream != null) {
            try {
                mBufferedInputStream.close();
            } catch (Exception e) {
                Log.e(TAG, "Exception closing buffered input stream", e);
            }
            mBufferedInputStream = null;
        }

        if (mSSLSocket != null) {
            if (mSSLSocket.isConnected()) {
                try {
                    mSSLSocket.close();
                } catch (Exception e) {
                    Log.e(TAG, "Exception closing ssl socket", e);
                }
            }
            mSSLSocket = null;
        }
    }

    private boolean checkAndOpenConnection() {
        if(!sslContextGood) {
            return false;
        }
        if (!mFloidService.mClientCommunicatorOn) {
            // We will just make sure we disconnect
            if (mConnected) {
                clear();
            }
            uxSendFailServerStatus();
            return false;       // We do nothing...
        }
        if ((mBufferedInputStream == null) || (mPrintStream == null) || (!mConnected)) {
            try {
                clear();
                if (connect()) {
                    return true;
                } else {
                    setServerStatus(false, true);
                    clear();
                    return false;
                }
            } catch (Exception e) {
                if (mFloidService.mUseLogger) Log.e(TAG, e.toString());
                setServerStatus(false, true);
                clear();
                return false;
            }
        } else {
            // OK - all variables were good - best guess is that we are still connected:
            return true;
        }
    }

    /**
     * Connects to the server
     *
     * @return true if connected, false otherwise
     */
    private synchronized boolean connect() throws Exception {
        setServerStatus(false, false);
        // Open a connection to the server
//        if(FloidService.mUseLogger) Log.d(TAG, "Connecting to Server...");
        try {
            mSSLSocket = (SSLSocket) mSSLSocketFactory.createSocket();
            //mSSLSocket.setEnabledProtocols(new String[]{"TLSv1"});
            // Connect the socket and set the timeout:
            mSSLSocket.connect(mFloidServerAddress, mConnectTimeout);
            mSSLSocket.setSoTimeout(mSocketTimeout);
        } catch (Exception e) {
//            if(FloidService.mUseLogger) Log.d(TAG, "Could not connect to server..." + e.toString());
            setServerStatus(false, true);
            uxSendFailServerStatus();
            lastConnectFailed = true;
            return false;
        }
//        if(FloidService.mUseLogger) Log.d(TAG, "Successful connection...");
        mSSLSocket.startHandshake();
//        if(FloidService.mUseLogger) Log.d(TAG, "Successful ssl handshake: " + " SSLSocket class: " + mSSLSocket.getClass() + "  String: " + mSSLSocket.toString());
        // Get the input and output streams and if we are still here at the end, we are connected!
        mBufferedInputStream = new BufferedInputStream(mSSLSocket.getInputStream());
        mPrintStream = new PrintStream(mSSLSocket.getOutputStream(), true);       // auto-flush enabled
        mConnected = true;
        setServerStatus(true, false);
        uxSendDingServerStatus();
        return true;
    }

    /**
     * Send FloidServerMessage to the server and get a FloidServerMessage response
     *
     * @param floidServerMessage the FloidServerMessage to send
     * @return the returned FloidServerMessage string
     */
    @SuppressWarnings("WeakerAccess")
    public synchronized byte[] sendFloidServerMessage(FloidServerMessage floidServerMessage) {
        if(floidServerMessage==null) {
            Log.e(TAG, "Service -> Server: Cannot Send FloidServerMessage: it is null");
            return null;
        }
        String messageClassName = floidServerMessage.getMessageClass()
                .replace("com.faiglelabs.floid.servertypes.statuses", "c.f.f.s.s")
                .replace("com.faiglelabs.floid.servertypes", "c.f.f.s");
        // if (mFloidService.mUseLogger) Log.v(TAG, "Service -> Server: " + messageClassName);
        byte[] floidServerMessageBytes = floidServerMessage.toByteArray();
        if (!checkAndOpenConnection()) {
            setServerStatus(false, true);
            // [STATS] Increase bad packets to server:
            mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_TO_SERVER);
            // [STATS] Add these to bad outgoing bytes:
            mFloidService.mAdditionalStatus.add(AdditionalStatus.BAD_BYTES_TO_SERVER, floidServerMessageBytes.length);
            return null;
        }
        // Let's try to send the Message:
        try {
            FloidServerMessageStatus floidServerMessageStatus = new FloidServerMessageStatus();
            if((FloidServerMessageUtils.writeFloidMessage(mPrintStream, floidServerMessageBytes, floidServerMessageStatus)) && !mPrintStream.checkError() && !floidServerMessageStatus.isError()) {
                uxSendDingServerStatus();
            } else {
                uxSendFailServerStatus();
                setServerStatus(false, true);
                clear();
                // [STATS] Increase bad packets to server:
                mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_TO_SERVER);
                // [STATS] Add these to bad outgoing bytes:
                mFloidService.mAdditionalStatus.add(AdditionalStatus.BAD_BYTES_TO_SERVER, floidServerMessageBytes.length);
                return null;
            }
        } catch (Exception exception) {
            // Something failed:
            setServerStatus(false, true);
            clear();
            // [STATS] Increase bad packets to server:
            mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_TO_SERVER);
            // [STATS] Add these to bad outgoing bytes:
            mFloidService.mAdditionalStatus.add(AdditionalStatus.BAD_BYTES_TO_SERVER, floidServerMessageBytes.length);
            return null;
        }
        // [STATS] Increase good packets to server:
        mFloidService.mAdditionalStatus.increment(AdditionalStatus.PACKETS_TO_SERVER);
        // [STATS] Add these to good outgoing bytes:
        mFloidService.mAdditionalStatus.add(AdditionalStatus.BYTES_TO_SERVER, floidServerMessageBytes.length);
        // OK, we have sent out line - try to get the response:
        setServerStatus(true, false);
        try {
            return readMessage();
        } catch (Exception e) {
            setServerStatus(false, true);
            clear();
            return null;
        }
    }


    /**
     * Set the server status
     *
     * @param serverConnected is the server connected
     * @param serverError     is there a specific server communication error
     */
    private void setServerStatus(boolean serverConnected, boolean serverError) {
        boolean sendServerConnected = false;
        boolean sendServerDisconnected = false;
        synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
            if (mFloidService.mFloidDroidStatus.isServerConnected() != serverConnected) {
                // Send a message - either connected or disconnected here:
                if (serverConnected) {
                    sendServerConnected = true;
                } else {
                    sendServerDisconnected = true;
                }
            }
            mFloidService.mFloidDroidStatus.setServerConnected(serverConnected);
            mFloidService.mFloidDroidStatus.setServerError(serverError);
        }
        if (sendServerConnected) {
            notifyServerConnected();
        }
        if (sendServerDisconnected) {
            notifyServerDisconnected();
        }
    }

    /**
     * Read a message from the connection
     * @return the message or null if it could not be read
     */
    private byte[] readMessage() {
        if (checkAndOpenConnection()) {
            // We do not try to connect since we only get lines coming back and this will fail faster if we
            // don't connect after a failure only to wait for this to time out
            try {
                FloidServerMessageStatus floidServerMessageStatus = new FloidServerMessageStatus();
                byte[] message = FloidServerMessageUtils.readFloidMessage(mBufferedInputStream, floidServerMessageStatus);
                if (message != null) {
                    // [STATS] Increment the packets coming back from the server:
                    mFloidService.mAdditionalStatus.increment(AdditionalStatus.PACKETS_FROM_SERVER);
                    // [STATS] Add the bytes to those coming from server:
                    mFloidService.mAdditionalStatus.add(AdditionalStatus.BYTES_FROM_SERVER, message.length);
                    uxSendDingServerStatus();
                    return message;
                } else {
                    uxSendFailServerStatus();
                    setServerStatus(false, true);
                    clear();
                    return null;
                }
            } catch (Exception e) {
                // We do not try to connect again at this point:
                setServerStatus(false, true);
                clear();
                uxSendFailServerStatus();
            }
        }
        return null;
    }

    /**
     * Ding the ux server status
     */
    private void uxSendDingServerStatus() {
        // if (mFloidService.mUseLogger) Log.i(TAG, "CC -> UX: Ding"); // Server Status
        try {
            // Ding server status in UX:
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_DING_SERVER_STATUS);
            Messenger outgoingUXMessenger = mFloidService.getFloidActivityMessenger();
            if (outgoingUXMessenger != null) {
                outgoingUXMessenger.send(outgoingUXMessage);
            }
        } catch (Exception e) {
            // Log the error:
            Log.e(TAG, "uxSendDigServerStatus", e);
        }
    }

    /**
     * Send a message to the ux that we failed to communicate to the server
     */
    private void uxSendFailServerStatus() {
        // if (mFloidService.mUseLogger) Log.i(TAG, "CC -> UX: Server Connection Fail Status");
        try {
            // Ding server status in UX:
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_FAIL_SERVER_STATUS);
            Messenger outgoingUXMessenger = mFloidService.getFloidActivityMessenger();
            if (outgoingUXMessenger != null) {
                outgoingUXMessenger.send(outgoingUXMessage);
            }
        } catch (Exception e) {
            // Log the error:
            Log.e(TAG, "uxSendDigServerStatus", e);
        }
    }

    /**
     * Send a message to the ux that we connected to the server
     */
    private void notifyServerConnected() {
        if (mFloidService.mUseLogger)
            Log.i(TAG, "CC -> UX: Play [SERVER CONNECTED]");
        try {
            // Play Server Connected notification sound:
            Message outgoingServiceMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_PLAY_NOTIFICATION);
            outgoingServiceMessage.getData().putInt(FloidService.BUNDLE_KEY_NOTIFICATION, FloidService.FLOID_ALERT_SOUND_SERVER_CONNECTED);
            Messenger outgoingServiceMessenger = mFloidService.getFloidServiceMessenger();
            if (outgoingServiceMessenger != null) {
                outgoingServiceMessenger.send(outgoingServiceMessage);
            }
        } catch (Exception e) {
            Log.e(TAG, "notifyServerConnected", e);
        }
    }

    /**
     * Send a message to the ux that we disconnected from the server
     */
    private void notifyServerDisconnected() {
        if (mFloidService.mUseLogger)
            Log.i(TAG, "CC -> UX: Play [SERVER DISCONNECTED]");
        try {
            // Play Server Connected notification sound:
            Message outgoingServiceMessage = Message.obtain(null, FloidService.FLOID_SERVICE_MESSAGE_PLAY_NOTIFICATION);
            outgoingServiceMessage.getData().putInt(FloidService.BUNDLE_KEY_NOTIFICATION, FloidService.FLOID_ALERT_SOUND_SERVER_DISCONNECTED);
            Messenger outgoingServiceMessenger = mFloidService.getFloidServiceMessenger();
            if (outgoingServiceMessenger != null) {
                outgoingServiceMessenger.send(outgoingServiceMessage);
            }
        } catch (Exception e) {
            Log.e(TAG, "notifyServerDisconnected", e);
        }
    }
}
