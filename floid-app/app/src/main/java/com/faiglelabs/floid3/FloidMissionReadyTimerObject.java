/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

import android.os.Parcel;
import android.os.Parcelable;
import com.faiglelabs.floid.servertypes.mission.DroidMission;

/**
 * Timer object for mission ready countdown
 */
class FloidMissionReadyTimerObject implements Parcelable {
    private final DroidMission mPendingMission;
    private final int mTimeoutInSeconds;
    private long mStartTime;
    private int mLastSecondsDisplayed;

    /**
     * Create a mission ready timer with this pending mission and this timeout
     *
     * @param pendingMission   the pending mission
     * @param timeoutInSeconds timeout in seconds
     */
    @SuppressWarnings("WeakerAccess")
    public FloidMissionReadyTimerObject(DroidMission pendingMission, int timeoutInSeconds) {
        super();
        mPendingMission = pendingMission;
        mTimeoutInSeconds = timeoutInSeconds;
        mStartTime = 0;
    }

    /**
     * Get the last seconds displayed
     *
     * @return the last seconds displayed
     */
    @SuppressWarnings("WeakerAccess")
    public int getLastSecondsDisplayed() {
        return mLastSecondsDisplayed;
    }

    /**
     * Set the last seconds displayed
     *
     * @param startTime the last seconds displayed
     */
    @SuppressWarnings("WeakerAccess")
    public void setStartTime(long startTime) {
        mStartTime = startTime;
    }

    /**
     * Set the start time
     *
     * @param lastSecondsDisplayed the last seconds displayed
     */
    @SuppressWarnings("WeakerAccess")
    public void setLastSecondsDisplayed(int lastSecondsDisplayed) {
        mLastSecondsDisplayed = lastSecondsDisplayed;
    }

    /**
     * Get the pending mission
     *
     * @return the pending mission
     */
    @SuppressWarnings("WeakerAccess")
    public DroidMission getPendingMission() {
        return mPendingMission;
    }

    /**
     * Get the timeout in seconds
     *
     * @return the timeout in seconds
     */
    @SuppressWarnings("WeakerAccess")
    public int getTimeoutInSeconds() {
        return mTimeoutInSeconds;
    }

    /**
     * Get the start time
     *
     * @return the start time
     */
    @SuppressWarnings("WeakerAccess")
    public long getStartTime() {
        return mStartTime;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.mPendingMission);
        dest.writeInt(this.mTimeoutInSeconds);
        dest.writeLong(this.mStartTime);
        dest.writeInt(this.mLastSecondsDisplayed);
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected FloidMissionReadyTimerObject(Parcel in) {
        this.mPendingMission = in.readSerializable(null, DroidMission.class);
        this.mTimeoutInSeconds = in.readInt();
        this.mStartTime = in.readLong();
        this.mLastSecondsDisplayed = in.readInt();
    }

    /**
     * The creator
     */
    public static final Creator<FloidMissionReadyTimerObject> CREATOR = new Creator<FloidMissionReadyTimerObject>() {
        @Override
        public FloidMissionReadyTimerObject createFromParcel(Parcel source) {
            return new FloidMissionReadyTimerObject(source);
        }

        @Override
        public FloidMissionReadyTimerObject[] newArray(int size) {
            return new FloidMissionReadyTimerObject[size];
        }
    };
}
