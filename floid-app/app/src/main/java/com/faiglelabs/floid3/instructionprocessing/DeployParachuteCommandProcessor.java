/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.DeployParachuteCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.DeployParachuteProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for deploy parachute command
 */
public class DeployParachuteCommandProcessor extends InstructionProcessor
{
	/**
	 * Create a deploy parachute command processor
	 * @param floidService the floid service
	 * @param  deployParachuteCommand the deploy parachute command
	 */
	@SuppressWarnings("WeakerAccess")
	public DeployParachuteCommandProcessor(FloidService floidService, DeployParachuteCommand deployParachuteCommand)
	{
		super(floidService, deployParachuteCommand);
		useDefaultGoalProcessing = false;
		completeOnCommandResponse = true;
		setProcessBlock(new DeployParachuteProcessBlock(FloidService.COMMAND_NO_TIMEOUT));     // This command uses the default time out
	}
	@Override
	public boolean setupInstruction()
	{
	    super.setupInstruction();
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
	    if(floidService.mUseLogger) Log.e(FloidService.TAG, "PARACHUTE DEPLOYED: setupInstruction NOT IMPLEMENTED");
	    // See land/liftoff/pantilt, etc to for pointers on implementing this...
		return false;
	}

	@Override
	public boolean processInstruction()
	{
	    if(super.processInstruction())
	    {
	        return true;   // Already done
	    }
        if(floidService.mUseLogger) Log.e(FloidService.TAG, "PARACHUTE DEPLOYED: processInstruction NOT IMPLEMENTED");
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
	    return true;
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
