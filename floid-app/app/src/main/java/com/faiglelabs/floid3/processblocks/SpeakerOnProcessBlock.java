/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.processblocks;

import com.faiglelabs.floid.servertypes.commands.SpeakerOnCommand;

/**
 * Process block for the speaker on command
 */
@SuppressWarnings("SameParameterValue")
public class SpeakerOnProcessBlock extends ProcessBlock {
    /**
     * Status not started
     */
    public final static int SPEAKER_PROCESS_STATUS_NOT_STARTED = 0;
    /**
     * Status in alert
     */
    public final static int SPEAKER_PROCESS_STATUS_IN_ALERT = 1;
    /**
     * Status alert complete
     */
    public final static int SPEAKER_PROCESS_STATUS_ALERT_COMPLETE = 2;
    /**
     * Status in tts
     */
    public final static int SPEAKER_PROCESS_STATUS_IN_TTS = 3;
    /**
     * Status tts complete
     */
    public final static int SPEAKER_PROCESS_STATUS_TTS_COMPLETE = 4;
    /**
     * Status in file
     */
    public final static int SPEAKER_PROCESS_STATUS_IN_FILE = 5;
    /**
     * Status file complete
     */
    public final static int SPEAKER_PROCESS_STATUS_FILE_COMPLETE = 6;
    /**
     * Status in delay
     */
    public final static int SPEAKER_PROCESS_STATUS_IN_DELAY = 7;
    /**
     * Status delay complete
     */
    public final static int SPEAKER_PROCESS_STATUS_DELAY_COMPLETE = 8;
    /**
     * Status complete
     */
    public final static int SPEAKER_PROCESS_STATUS_COMPLETE = 9;
    private int mSpeakerStatus = SPEAKER_PROCESS_STATUS_NOT_STARTED;
    private long mDelayEnd = 0L;
    private int mRepeatCount = 0;
    private final SpeakerOnCommand mSpeakerOnCommand;

    /**
     * Create a speaker on process block
     *
     * @param timeoutInterval  the timeout interval
     * @param speakerOnCommand the speaker on command
     */
    public SpeakerOnProcessBlock(long timeoutInterval, SpeakerOnCommand speakerOnCommand) {
        super(timeoutInterval, "Speaker On");
        mSpeakerOnCommand = speakerOnCommand;
        setToDefault();
    }

    /**
     * Set to default values
     */
    public final void setToDefault() {
        mSpeakerStatus = SPEAKER_PROCESS_STATUS_NOT_STARTED;
        mDelayEnd = 0L;
        mRepeatCount = 0;
    }

    /**
     * Get the speaker status
     *
     * @return the speaker status
     */
    public int getSpeakerStatus() {
        return mSpeakerStatus;
    }

    /**
     * Set the speaker status
     *
     * @param speakerStatus the speaker status
     */
    public void setSpeakerStatus(int speakerStatus) {
        mSpeakerStatus = speakerStatus;
    }

    /**
     * Get the delay end
     *
     * @return the delay end
     */
    public long getDelayEnd() {
        return mDelayEnd;
    }

    /**
     * Set the delay end
     *
     * @param delayEnd the delay end
     */
    public void setDelayEnd(long delayEnd) {
        this.mDelayEnd = delayEnd;
    }

    /**
     * Get the repeat count
     *
     * @return the repeat count
     */
    public int getRepeatCount() {
        return mRepeatCount;
    }

    /**
     * Set the repeat count
     *
     * @param repeatCount the repeat count
     */
    @SuppressWarnings("unused")
    public void setRepeatCount(int repeatCount) {
        this.mRepeatCount = repeatCount;
    }

    /**
     * Increase the repeat count by one
     */
    public void increaseRepeatCount() {
        this.mRepeatCount++;
    }

    /**
     * Get the speaker on command
     *
     * @return the speaker on command
     */
    @SuppressWarnings("unused")
    public SpeakerOnCommand getSpeakerOnCommand() {
        return mSpeakerOnCommand;
    }

}
