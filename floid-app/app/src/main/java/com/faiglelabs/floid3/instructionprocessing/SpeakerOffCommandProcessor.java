/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import com.faiglelabs.floid.servertypes.commands.SpeakerOffCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.SpeakerOffProcessBlock;

/**
 * Command processor for speaker off
 */
public class SpeakerOffCommandProcessor extends InstructionProcessor
{
    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private final        SpeakerOffCommand mSpeakerOffCommand;

    /**
     * Create a speaker of command processor
     * @param floidService the floid service
     * @param speakerOffCommand the speaker off command
     */
    @SuppressWarnings("WeakerAccess")
    public SpeakerOffCommandProcessor(FloidService floidService, SpeakerOffCommand speakerOffCommand)
    {
        super(floidService, speakerOffCommand);
        mSpeakerOffCommand = speakerOffCommand;
        setProcessBlock(new SpeakerOffProcessBlock(FloidService.COMMAND_NO_TIMEOUT, speakerOffCommand));        // No timeout
        useDefaultGoalProcessing = false;
        completeOnCommandResponse = false;
    }

    @Override
    public boolean processInstruction()
    {
        if(super.processInstruction())
        {
            return true;   // Already done...
        }
        // If we have a SpeakerOnCommand running, kill it:
        if(floidService.mSpeakerOnCommandProcessor!=null)
        {
            floidService.mSpeakerOnCommandProcessor.tearDownInstruction();
            floidService.mSpeakerOnCommandProcessor = null;
        }
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
        return true;
    }
}
