/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.controller;

/**
 * Floid IMU emulation algorithm process result
 */
@SuppressWarnings("SameParameterValue")
public class FloidIMUEmulationAlgorithmProcessResult {
    private boolean success;
    private double value;

    /**
     * @return the success
     */
    @SuppressWarnings("unused")
    public boolean isSuccess() {
        return success;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * @param value the value to set
     */
    public void setValue(double value) {
        this.value = value;
    }
}
