/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for the stop photo strobe command
 */
@SuppressWarnings("SameParameterValue")
public class StopPhotoStrobeProcessBlock extends ProcessBlock {
    /**
     * Create a new stop photo strobe process block
     * @param timeoutInterval the timeout interval
     */
    public StopPhotoStrobeProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Stop Photo Strobe");
    }
}
