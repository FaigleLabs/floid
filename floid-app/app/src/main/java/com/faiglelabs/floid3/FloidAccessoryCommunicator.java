/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

import android.content.ContentValues;
import android.os.SystemClock;
import android.util.Log;
import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import com.faiglelabs.floid3.parcelables.FloidModelParametersParcelable;
import com.faiglelabs.floid3.parcelables.FloidModelStatusParcelable;
import com.faiglelabs.floid3.parcelables.FloidStatusParcelable;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

import static com.faiglelabs.floid3.FloidService.*;

/**
 * The type Floid accessory communicator.
 */
public class FloidAccessoryCommunicator implements Runnable {
    /**
     * The constant TAG.
     */
    private static final String TAG = "FloidAccessory";
    // Heartbeat:
    /**
     * The heartbeat period.
     */
    private static final long mHeartbeatPeriod = 5000L;  // We send a heartbeat every five seconds
    /**
     * The heartbeat last time.
     */
    private long mHeartbeatLastTime = 0L;     // Last heartbeat sent
    // Make a message buffer for use later:
    /**
     * The buffer size
     */
    private static final int BUFFER_SIZE = 16384;
    /**
     * The message length.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private int mMessageLength = 0;
    /**
     * The running indicator period.
     */
    private static final long mRunningIndicatorPeriod = 30000L;   // Pop a log message every 30 seconds to show thread is running...
    /**
     * The running indicator time.
     */
    @SuppressWarnings({"FieldCanBeLocal"})
    private long mRunningIndicatorTime = 0L;
    /**
     * The model parameters request time.
     */
    private long mModelParametersRequestTime = 0L;
    /**
     * The constant mModelParametersReRequestPeriod.
     */
    private static final long mModelParametersReRequestPeriod = 8000L; // Wait time between model parameter requests to give time for the first to be satisfied

    /**
     * The floid service.
     */
    private final FloidService mFloidService;

    /**
     * The floid license status (false = demo, true = full)
     */
    private final boolean floidLicense;

    /**
     * The floid accessory communicator runnable loop count.
     */
    private long mFloidAccessoryCommunicatorRunnableLoopCount = 0L;

    /**
     * The floid accessory communicator waiting for command response flag.
     */
    private boolean mFloidAccessoryCommunicatorWaitingForCommandResponse = false;

    /**
     * The floid accessory communicator waiting for command response start time.
     */
    private long mFloidAccessoryCommunicatorWaitingForCommandResponseStartTime = 0L;

    /**
     * The floid accessory communicator command response timeout.
     */
    private static final long mFloidAccessoryCommunicatorCommandResponseTimeout = 500L;

    /**
     * Get the floid accessory communicator runnable loop count:
     * @return the floid accessory communicator runnable loop count
     */
    long getFloidAccessoryCommunicatorRunnableLoopCount() {
        return mFloidAccessoryCommunicatorRunnableLoopCount;
    }

    boolean firstTime = true;
    boolean noAccessory = true;
    /**
     * Instantiates a new Floid accessory communicator.
     *
     * @param floidService the floid service
     * @param floidLicense valid license?
     */
    FloidAccessoryCommunicator(FloidService floidService, boolean floidLicense) {
        super();
        mFloidService = floidService;
        this.floidLicense = floidLicense;
    }

    @Override
    public void run() {
        if (firstTime) {
            mRunningIndicatorTime = SystemClock.uptimeMillis();
            if (mFloidService.mUseLogger) Log.i(TAG, "Starting");
            mFloidService.uxSendFloidMessageToDisplay("Floid Accessory Communicator Thread Starting\n");
            firstTime = false;
        }
        mFloidAccessoryCommunicatorRunnableLoopCount++;
        // Get the time:
        long currentTime = SystemClock.uptimeMillis();
        // Pop running indicator message if time:
        if (currentTime - mRunningIndicatorTime > mRunningIndicatorPeriod) {
            mRunningIndicatorTime = currentTime;
            if (mFloidService.mUseLogger) Log.i(TAG, "Running");
        }
        synchronized (mFloidService.mAccessoryStatusSynchronizer) {
            boolean justClosedAccessory = false;
            if (mFloidService.mCloseAccessoryFlag) {
                if (mFloidService.mUseLogger) Log.i(TAG, "Close Accessory Flag");
                mFloidService.uxSendFloidMessageToDisplay("Close Accessory Flag\n");
                // Clear flag and close accessory:
                mFloidService.mCloseAccessoryFlag = false;
                closeAccessoryClient();
                justClosedAccessory = true;
            }
            if (mFloidService.mNewAccessoryAvailableFlag) {
                mFloidService.uxSendFloidMessageToDisplay("Accessory Available Flag\n");
                if (mFloidService.mUseLogger) Log.i(TAG, "Accessory Available Flag");
                mFloidService.mNewAccessoryAvailableFlag = false;  // Turn off the flag
                if (!justClosedAccessory) {
                    closeAccessoryClient();
                }
                // NOTE: License restriction here:
                if (floidLicense) {
                    mFloidService.uxSendFloidMessageToDisplay("Valid License - opening accessory client\n");
                    openAccessoryClient();
                } else {
                    mFloidService.uxSendFloidMessageToDisplay("Invalid License - not opening accessory client\n");
                }
            }
        }
        boolean clearCommandsWithError = true;  // Default
        // OK, process any messages we may have:
        if (mFloidService.mAccessory != null) {
            // Process inputs:
            if (mFloidService.mInputStream != null) {
                noAccessory = false;
                try {
                    byte[] mMessageBuffer = new byte[BUFFER_SIZE];
                    clearCommandsWithError = false; // OK we might be good...
                    mMessageLength = mFloidService.mInputStream.read(mMessageBuffer);
                    if (mMessageLength > 0) {
                        // [STATS] Add the byte count from floid:
                        mFloidService.mAdditionalStatus.add(AdditionalStatus.BYTES_FROM_FLOID, mMessageLength);
                        if (mFloidService.mUseLogger) Log.d(TAG, "Packet Received.");
                        PacketHeader packetHeader = verifyPacket(mMessageBuffer, mMessageLength);
                        if (packetHeader != null) {
                            // [STATS] Increment the packet count from floid:
                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.PACKETS_FROM_FLOID);
                            // Flip the Communication LED with a message:
                            mFloidService.uxSendDingCommStatus();
                            // Format is in first byte:
                            switch (packetHeader.type) {
                                case STATUS_PACKET: // Status
                                {
                                    if (mFloidService.mUseLogger) Log.i(TAG, "Floid Accessory: <-- Status Packet");
                                    if (mMessageLength != STATUS_PACKET_SIZE) {
                                        // Bad message length:
                                        // [STATS] Bump the bad packet count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_FROM_FLOID);
                                        // [STATS] Bump the bad status length count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_STATUS_PACKETS_FROM_FLOID);
                                        // [STATS] Bump the bad packet length count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKET_LENGTHS_FROM_FLOID);
                                        if (mFloidService.mUseLogger) Log.e(TAG, "Bad status message length: " + mMessageLength + " != " + STATUS_PACKET_SIZE);
                                        mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "STATUS WRONG PACKET SIZE (Expected: " + STATUS_PACKET_SIZE + " Received: " + mMessageLength + ")");
                                    } else {
                                        FloidStatusParcelable floidStatus = decodeFloidStatusPacket(mMessageBuffer, mMessageLength);
                                        if (floidStatus != null) {
                                            // [STATS] Bump the floid status packet count:
                                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.STATUS_PACKETS_FROM_FLOID);
                                            // Set the floid status for the service:
                                            mFloidService.setFloidStatus(floidStatus);
                                            // Send the floid status to the UX:
                                            mFloidService.uxSendFloidStatus();
                                            if (!mFloidService.mReceivedModelParameters) // Note: This is cleared if USB closes
                                            {
                                                // Is it first time (==0) or timeout on last request?
                                                if ((mModelParametersRequestTime == 0L) || ((currentTime - mModelParametersRequestTime) >= mModelParametersReRequestPeriod)) {
                                                    // Update our request time:
                                                    mModelParametersRequestTime = currentTime;
                                                    // Ask the floid for the model parameters:
                                                    mFloidService.sendGetModelParametersCommandToFloid();
                                                }
                                            }
                                            if (mFloidService.mFloidDatabase != null) {
                                                try {
                                                    ContentValues insertContentValues = new ContentValues();
                                                    insertContentValues.put(FloidCommands.PARAMETER_FLOID_ID, Integer.toString(mFloidService.mFloidDroidStatus.getFloidId()));
                                                    insertContentValues.put(FloidCommands.PARAMETER_FLOID_UUID, mFloidService.mFloidDroidStatus.getFloidUuid());
                                                    insertContentValues.put(FloidCommands.PARAMETER_STATUS_NUMBER, mFloidService.mLastFloidStatusNumber);
                                                    insertContentValues.put(FloidCommands.PARAMETER_FLOID_STATUS, floidStatus.toJSON().toString());
                                                    mFloidService.mFloidDatabase.insert(FloidCommands.TABLE_FLOID_STATUS, null, insertContentValues);
                                                } catch (Exception e) {
                                                    if (mFloidService.mUseLogger) Log.w(TAG, "Failed to insert floid status: " + Log.getStackTraceString(e));
                                                }
                                            }
                                        } else {
                                            // [STATS] Bump the bad floid status packet count:
                                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_STATUS_PACKETS_FROM_FLOID);
                                        }
                                    }
                                }
                                break;
                                case MODEL_PARAMETERS_PACKET: // Model parameters
                                {
                                    if (mFloidService.mUseLogger) Log.i(TAG, "Floid Accessory: <-- Model Parameters Packet");
                                    if (mMessageLength != MODEL_PARAMETERS_PACKET_SIZE) {
                                        // Bad message length
                                        // [STATS] Bump the bad packet count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_FROM_FLOID);
                                        // [STATS] Bump the bad model parameters length count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_MODEL_PARAMETERS_PACKETS_FROM_FLOID);
                                        // [STATS] Bump the bad packet length count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKET_LENGTHS_FROM_FLOID);
                                        if (mFloidService.mUseLogger) Log.e(TAG, "Bad model parameters message length: " + mMessageLength + " != " + MODEL_PARAMETERS_PACKET_SIZE);
                                        mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "MODEL PARAMETERS WRONG PACKET SIZE (Expected: " + MODEL_PARAMETERS_PACKET_SIZE + " Received: " + mMessageLength + ")");
                                    } else {
                                        mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "<-- MODEL PARAMETERS");
                                        FloidModelParametersParcelable floidModelParameters = decodeFloidModelParametersPacket(mMessageBuffer, mMessageLength);
                                        if (floidModelParameters != null) {
                                            // [STATS] Bump the model status packet count:
                                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.MODEL_PARAMETERS_PACKETS_FROM_FLOID);
                                            // Set floid model parameters (synchronized):
                                            mFloidService.setFloidModelParameters(floidModelParameters);
                                            // Send the floid model parameters to the UX:
                                            mFloidService.uxSendFloidModelParameters();
                                            mFloidService.mReceivedModelParameters = true;
                                            if (mFloidService.mFloidDatabase != null) {
                                                try {
                                                    ContentValues insertContentValues = new ContentValues();
                                                    insertContentValues.put(FloidCommands.PARAMETER_FLOID_ID, Integer.toString(mFloidService.mFloidDroidStatus.getFloidId()));
                                                    insertContentValues.put(FloidCommands.PARAMETER_FLOID_UUID, mFloidService.mFloidDroidStatus.getFloidUuid());
                                                    insertContentValues.put(FloidCommands.PARAMETER_FLOID_MODEL_PARAMETERS, floidModelParameters.toJSON().toString());
                                                    mFloidService.mFloidDatabase.insert(FloidCommands.TABLE_FLOID_MODEL_PARAMETERS, null, insertContentValues);
                                                } catch (Exception e) {
                                                    if (mFloidService.mUseLogger) Log.w(TAG, "Failed to insert floid model parameters: " + Log.getStackTraceString(e));
                                                }
                                            }
                                        } else {
                                            // [STATS] Bump the bad model status packet count:
                                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_MODEL_PARAMETERS_PACKETS_FROM_FLOID);
                                            mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "<-- MODEL PARAMETERS WAS NULL");
                                        }
                                    }
                                }
                                break;
                                case MODEL_STATUS_PACKET: // Model status
                                {
                                    if (mFloidService.mUseLogger) Log.i(TAG, "Floid Accessory: <-- Model Status Packet");
                                    if (mMessageLength != MODEL_STATUS_PACKET_SIZE) {
                                        // Bad message length
                                        // [STATS] Bump the bad packet count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_FROM_FLOID);
                                        // [STATS] Bump the bad model status length count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_MODEL_STATUS_PACKETS_FROM_FLOID);
                                        // [STATS] Bump the bad packet length count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKET_LENGTHS_FROM_FLOID);
                                        if (mFloidService.mUseLogger) Log.e(TAG, "Bad model status message length: " + mMessageLength + " != " + MODEL_STATUS_PACKET_SIZE);
                                        mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "MODEL STATUS WRONG PACKET SIZE (Expected: " + MODEL_STATUS_PACKET_SIZE + " Received: " + mMessageLength + ")");
                                    } else {
                                        // Removed below - too noisy on front-end - same as floid status - too much:
                                        // mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "<-- MODEL STATUS");
                                        FloidModelStatusParcelable floidModelStatus = decodeFloidModelStatusPacket(mMessageBuffer, mMessageLength);
                                        if (floidModelStatus != null) {
                                            // [STATS] Bump the model status packet count:
                                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.MODEL_STATUS_PACKETS_FROM_FLOID);
                                            // Set floid model status (synchronized):
                                            mFloidService.setFloidModelStatus(floidModelStatus);
                                            // Send the floid model status to the UX:
                                            mFloidService.uxSendFloidModelStatus();
                                            if (mFloidService.mFloidDatabase != null) {
                                                try {
                                                    ContentValues insertContentValues = new ContentValues();
                                                    insertContentValues.put(FloidCommands.PARAMETER_FLOID_ID, Integer.toString(mFloidService.mFloidDroidStatus.getFloidId()));
                                                    insertContentValues.put(FloidCommands.PARAMETER_FLOID_UUID, mFloidService.mFloidDroidStatus.getFloidUuid());
                                                    insertContentValues.put(FloidCommands.PARAMETER_FLOID_MODEL_STATUS, floidModelStatus.toJSON().toString());
                                                    mFloidService.mFloidDatabase.insert(FloidCommands.TABLE_FLOID_MODEL_STATUS, null, insertContentValues);
                                                } catch (Exception e) {
                                                    if (mFloidService.mUseLogger)
                                                        Log.w(TAG, "Failed to insert floid model status: " + Log.getStackTraceString(e));
                                                }
                                            }
                                        } else {
                                            // [STATS] Bump the bad model status packet count:
                                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_MODEL_STATUS_PACKETS_FROM_FLOID);
                                            mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "<-- MODEL STATUS WAS NULL");
                                        }
                                    }
                                }
                                break;
                                case DEBUG_PACKET:  // Debug text
                                {
                                    // [STATS] Bump the debug packet count:
                                    mFloidService.mAdditionalStatus.increment(AdditionalStatus.DEBUG_PACKETS_FROM_FLOID);
                                    if (mFloidService.mUseLogger) Log.i(TAG, "Floid Accessory: <-- Debug Packet");
                                    // Decode packet:
                                    String debugText = decodeDebugPacket(mMessageBuffer, mMessageLength);
                                    // Add to our list of debug messages (synchronized):
                                    mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, debugText);
                                    // Send the floid debug message to the UX:
                                    mFloidService.uxSendFloidDebugMessage(debugText);
                                }
                                break;
                                case COMMAND_RESPONSE_PACKET: // Command Response
                                {
                                    if (mFloidService.mUseLogger) Log.i(TAG, "Floid Accessory: <-- Command Response Packet.");
                                    if (mMessageLength != COMMAND_RESPONSE_PACKET_SIZE) {
                                        // Bad message length
                                        // [STATS] Bump the bad packet count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_FROM_FLOID);
                                        // [STATS] Bump the bad command responses  count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_COMMAND_RESPONSES_FROM_FLOID);
                                        // [STATS] Bump the bad packet length count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKET_LENGTHS_FROM_FLOID);
                                        if (mFloidService.mUseLogger) Log.e(TAG, "Bad command response message length: " + mMessageLength + " != " + COMMAND_RESPONSE_PACKET_SIZE);
                                        mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "COMMAND RESPONSE WRONG PACKET SIZE (Expected: " + COMMAND_RESPONSE_PACKET_SIZE + " Received: " + mMessageLength + ")");
                                    } else {
                                        // Uncomment below for raw notification of command responses:
//                                            mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "<-- COMMAND RESPONSE");
                                        // Get the command response:
                                        FloidCommandResponse floidCommandResponse = decodeCommandResponsePacket(mMessageBuffer, mMessageLength);
                                        if (floidCommandResponse != null) {
                                            // Add the command response to the list:
                                            synchronized (mFloidService.mFloidCommandResponseSynchronizer) {
                                                mFloidService.mFloidCommandResponses.add(floidCommandResponse);
                                            }
                                            // Bump the good command responses count:
                                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.COMMAND_RESPONSES_FROM_FLOID);
                                            // If not a heartbeat response then send to ux and debug:
                                            if (!("HRT".equals(floidCommandResponse.getIdentifier()))) {
                                                String formattedCommandResponseString = "-> " + floidCommandResponse.getCommandNumber() + ":" + floidCommandResponse.getIdentifier() + " [" + floidCommandResponse.getCommandStatusString() + "]";
                                                mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_COMMAND_RESPONSE, formattedCommandResponseString);
                                                // Send the floid command response message to the UX:
                                                mFloidService.uxSendFloidCommandResponseMessage(formattedCommandResponseString);
                                            }
                                        } else {
                                            // Bump the bad command responses count:
                                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_COMMAND_RESPONSES_FROM_FLOID);
                                            if (mFloidService.mUseLogger) Log.e(TAG, "Floid Command Response was null");
                                        }
                                    }
                                    // We are no longer waiting for a command response:
                                    mFloidAccessoryCommunicatorWaitingForCommandResponse = false;
                                }
                                break;
                                case TEST_RESPONSE_PACKET: // Test Response
                                {
                                    if (mFloidService.mUseLogger) Log.i(TAG, "Floid Accessory: <-- Test Packet.");
                                    if (mMessageLength != TEST_RESPONSE_PACKET_SIZE) {
                                        // Bad message length
                                        // [STATS] Bump the bad packet count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_FROM_FLOID);
                                        // [STATS] Bump the bad model parameters length count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_TEST_RESPONSE_PACKETS_FROM_FLOID);
                                        // [STATS] Bump the bad packet length count:
                                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKET_LENGTHS_FROM_FLOID);
                                        if (mFloidService.mUseLogger) Log.e(TAG, "Bad test response message length: " + mMessageLength + " != " + TEST_RESPONSE_PACKET_SIZE);
                                        mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "TEST RESPONSE WRONG PACKET SIZE (Expected: " + TEST_RESPONSE_PACKET_SIZE + " Received: " + mMessageLength + ")");
                                    } else {
                                        TestPacketResponse testPacketResponse = decodeTestResponsePacket(mMessageBuffer, mMessageLength);
                                        if (testPacketResponse != null) {
                                            // Get the tag and the message and add it to the debug message queue:
                                            mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_TEST_PACKET_RESPONSE, testPacketResponse.toString());
                                            // Send the floid test response message to the UX:
                                            mFloidService.uxSendFloidTestResponseMessage(testPacketResponse.toString());
                                        } else {
                                            if (mFloidService.mUseLogger) Log.e(TAG, "Test Packet Response was null");
                                            mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "<-- TEST RESPONSE WAS NULL");
                                        }
                                    }
                                }
                                break;
                                default: {
                                    // Bad packet type:
                                    // [STATS] Bump the bad packet count:
                                    mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_FROM_FLOID);
                                    // [STATS] Bump the bad packet type count:
                                    mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKET_TYPES_FROM_FLOID);
                                    if (mFloidService.mUseLogger) Log.i(TAG, "Floid Accessory: <-- Unknown Packet: " + packetHeader.type);
                                    // Send a message that we had an unknown packet type to the UX:
                                    mFloidService.uxSendFloidMessageToDisplay("Unknown Packet Type: " + packetHeader.type + "\n");
                                    mFloidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "UNKNOWN PACKET TYPE: " + packetHeader.type);
                                }
                            }
                        } else {
                            // Bad bytes/packet type:
                            // [STATS] Bump the bad packet count:
                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_FROM_FLOID);
                            // [STATS] Add the bytes to the bad bytes count:
                            mFloidService.mAdditionalStatus.add(AdditionalStatus.BAD_BYTES_FROM_FLOID, mMessageLength);
                        }
                    }
                } catch (IOException e) {
                    // [STATS] I/O Error:
                    mFloidService.mAdditionalStatus.increment(AdditionalStatus.IO_ERROR_FROM_FLOID);
                    // Clear the commands:
                    clearCommandsWithError = true;  // An i/o error - we will have to clean up commands...
                    noAccessory = true;
                }
            }
            // See if we need to clear the waiting for command response flag:
            if (System.currentTimeMillis() > mFloidAccessoryCommunicatorWaitingForCommandResponseStartTime + mFloidAccessoryCommunicatorCommandResponseTimeout) {
                // Expired - clear it:
                mFloidAccessoryCommunicatorWaitingForCommandResponse = false;
            }
            // Process outputs:
            if ((mFloidService.mOutputStream != null) && (!clearCommandsWithError)) {
                FloidOutgoingCommand floidOutgoingCommand = null;
                // Do we have any commands to send?
                // Make sure we are not waiting for a command before sending the next one:
                if (!mFloidAccessoryCommunicatorWaitingForCommandResponse) {
                    synchronized (mFloidService.mFloidOutgoingCommandsSynchronizer) {
                        if (mFloidService.mFloidOutgoingCommands.size() > 0) {
                            floidOutgoingCommand = mFloidService.mFloidOutgoingCommands.get(0);
                            mFloidService.mFloidOutgoingCommands.remove(0);
                        }
                    }
                }
                // Yes, at least one command:
                if (floidOutgoingCommand != null)
                    try {
                        if (floidOutgoingCommand.getCommandBuffer() == null) {
                            // [STATS] Bump the bad packet to floid count:
                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_TO_FLOID);
                            if (mFloidService.mUseLogger) Log.e(TAG, "Error: command buffer is null for outgoing command");
                        } else {
                            // [STATS] Bump the packet to floid count:
                            mFloidService.mAdditionalStatus.increment(AdditionalStatus.PACKETS_TO_FLOID);
                            // [STATS] Add the bytes to the bytes to floid count:
                            mFloidService.mAdditionalStatus.add(AdditionalStatus.BYTES_TO_FLOID, floidOutgoingCommand.getCommandBuffer().length);
                            if (mFloidService.mUseLogger) Log.d(TAG, "Floid Accessory: -> Floid: Outgoing Command Buffer");
                            floidOutgoingCommand.logCommandBuffer();
                            mFloidService.mOutputStream.write(floidOutgoingCommand.getCommandBuffer());
                            // We are waiting on a command response now:
                            mFloidAccessoryCommunicatorWaitingForCommandResponse = true;
                            mFloidAccessoryCommunicatorWaitingForCommandResponseStartTime = System.currentTimeMillis();
                        }
                    } catch (Exception e) {
                        noAccessory = true;
                        // [STATS] Bump the bad packet to floid count:
                        mFloidService.mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_TO_FLOID);
                        // [STATS] Add the bytes to the bytes to bad floid count:
                        mFloidService.mAdditionalStatus.add(AdditionalStatus.BAD_BYTES_TO_FLOID, floidOutgoingCommand.getCommandBuffer().length);
                        if (mFloidService.mUseLogger) Log.e(TAG, "Exception sending command to Floid", e);
                        // Put a bad command response here:
                        FloidCommandResponse floidCommandResponse = new FloidCommandResponse();
                        floidCommandResponse.setCommandNumber(floidOutgoingCommand.getCommandNumber());
                        floidCommandResponse.setCommandStatus(COMMAND_RESPONSE_COMM_ERROR);
                        synchronized (mFloidService.mFloidCommandResponseSynchronizer) {
                            mFloidService.mFloidCommandResponses.add(floidCommandResponse);
                        }
                        // Denote all remaining command responses are to be cleared:
                        clearCommandsWithError = true;
                    }
                if (currentTime >= mHeartbeatLastTime + mHeartbeatPeriod) {
                    try {
                        // Send heartbeat:
                        mFloidService.sendHeartbeatCommandToFloid();
                        mHeartbeatLastTime = currentTime;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger) Log.w(TAG, "Failed to send heartbeat packet: " + e.getMessage());
                    }
                }
            } else {
                clearCommandsWithError = true;  // Null output stream...
            }
        }
        if (clearCommandsWithError) {
            synchronized (mFloidService.mFloidOutgoingCommandsSynchronizer) {
                // Clear the commands:
                for (FloidOutgoingCommand floidOutgoingCommand : mFloidService.mFloidOutgoingCommands) {
                    // [STATS] Bump the error clear command responses:
                    mFloidService.mAdditionalStatus.increment(AdditionalStatus.COMMANDS_CLEARED);
//                            if(mFloidService.mUseLogger) Log.d(TAG, "Clearing Outgoing Command Buffer:");
                    floidOutgoingCommand.logCommandBuffer();
                    FloidCommandResponse floidCommandResponse = new FloidCommandResponse();
                    floidCommandResponse.setCommandNumber(floidOutgoingCommand.getCommandNumber());
                    // Set the status to communication error:
                    floidCommandResponse.setCommandStatus(COMMAND_RESPONSE_COMM_ERROR);
                    synchronized (mFloidService.mFloidCommandResponseSynchronizer) {
                        mFloidService.mFloidCommandResponses.add(floidCommandResponse);
                    }
                }
                // Clear the list:
                mFloidService.mFloidOutgoingCommands.clear();
            }
        }
    }

    /**
     * Close accessory client.
     */
    private void closeAccessoryClient() {
        mFloidService.uxSendFloidMessageToDisplay("Closing Accessory Client\n");
        if (mFloidService.mUseLogger) Log.i(TAG, "Closing Accessory Client");
        boolean accessoryCloseSuccess = true; // This stays true on the path where we successfully close an accessory
        boolean accessoryCloseError = false;  // This goes to true whenever an exception is gotten along the close path
        try {
            if (mFloidService.mFileDescriptor != null) {
                try {
                    if (mFloidService.mInputStream != null) {
                        Log.d(TAG, "Closing input stream");
                        mFloidService.mInputStream.close();
                    } else {
                        accessoryCloseSuccess = false;
                    }
                } catch (IOException ioException) {
                    Log.d(TAG, "IOException closing input stream", ioException);
                    accessoryCloseSuccess = false;
                    accessoryCloseError = true;
                }
                try {
                    if (mFloidService.mOutputStream != null) {
                        Log.d(TAG, "Closing output stream");
                        mFloidService.mOutputStream.close();
                    } else {
                        accessoryCloseSuccess = false;
                    }
                } catch (IOException ioException) {
                    Log.e(TAG, "IOException closing output stream", ioException);
                    accessoryCloseSuccess = false;
                    accessoryCloseError = true;
                }
                mFloidService.mFileDescriptor.close();
            } else {
                mFloidService.uxSendFloidMessageToDisplay("Null File Descriptor\n");
                accessoryCloseSuccess = false;
            }
        } catch (IOException e) {
            Log.e(TAG, "Exception closing accessory", e);
            mFloidService.uxSendFloidMessageToDisplay("Close Fail: " + e.getMessage() + "\n");
            if (mFloidService.mUseLogger) Log.i(TAG, "Close Fail: " + e.getMessage());
            accessoryCloseError = true;
        } finally {
            mFloidService.mFileDescriptor = null;
            mFloidService.mAccessory = null;
            mFloidService.mInputStream = null;
            mFloidService.mOutputStream = null;
            // Clear so we make sure we ask for the model parameters next time we are connected:
            mFloidService.mReceivedModelParameters = false;
            mModelParametersRequestTime = 0L;
        }
        mFloidService.mFloidDroidStatus.setFloidConnected(false);

        // success & !error  = closed client
        // !success & error  = failed to close
        // success & error   = failed to close
        // !success & !error = no client
        if (accessoryCloseSuccess) {
            if (accessoryCloseError) {
                // Failed to close
                mFloidService.uxSendFloidMessageToDisplay("Failed to close\n");
            } else {
                // Closed successfully
                mFloidService.uxSendFloidMessageToDisplay("Closed successfully\n");
            }
        } else {
            if (accessoryCloseError) {
                // Failed to close
                mFloidService.uxSendFloidMessageToDisplay("Failed to close\n");
            } else {
                // No client
                mFloidService.uxSendFloidMessageToDisplay("No client\n");
            }
        }
        if (!mFloidService.quitting) {
            mFloidService.uxSendSetAccessoryConnectedStatus();
            mFloidService.serviceSendPlayNotification(FLOID_ALERT_SOUND_FLOID_DISCONNECTED);
        }
    }

    /**
     * Open accessory client.
     */
    private void openAccessoryClient() {
        if (mFloidService.mUseLogger) Log.i(TAG, "Opening");
        mFloidService.uxSendFloidMessageToDisplay("Opening\n");
        try {
            // Open the new accessory:
            mFloidService.mAccessory = mFloidService.mNewAccessory;
            mFloidService.mFileDescriptor = mFloidService.mUsbManager.openAccessory(mFloidService.mAccessory);
            if (mFloidService.mFileDescriptor != null) {
                FileDescriptor fd = mFloidService.mFileDescriptor.getFileDescriptor();
                mFloidService.mInputStream = new FileInputStream(fd);
                mFloidService.mOutputStream = new FileOutputStream(fd);
                if (mFloidService.mUseLogger) Log.i(TAG, "Opened");
                mFloidService.uxSendFloidMessageToDisplay("Opened\n");
                if (mFloidService.mUseLogger) Log.i(TAG, "mInputStream: " + mFloidService.mInputStream.toString());
                if (mFloidService.mUseLogger) Log.i(TAG, "mOutputStream: " + mFloidService.mOutputStream.toString());
                mFloidService.mFloidDroidStatus.setFloidConnected(true);
                // Send a message to the interface to flip on the accessory icon:
                if (!mFloidService.quitting) {
                    mFloidService.uxSendSetAccessoryConnectedStatus();
                    mFloidService.serviceSendPlayNotification(FLOID_ALERT_SOUND_FLOID_CONNECTED);
                }
            } else {
                mFloidService.mFloidDroidStatus.setFloidConnected(true);
                mFloidService.mInputStream = null;
                mFloidService.mOutputStream = null;
                if (mFloidService.mUseLogger) Log.e(TAG, "Open Fail: Null file descriptor");
                mFloidService.uxSendFloidMessageToDisplay("Open Fail: Null file descriptor\n");
                mFloidService.mAccessory = null;
            }
        } catch (Exception e) {
            mFloidService.mInputStream = null;
            mFloidService.mOutputStream = null;
            mFloidService.mAccessory = null;
            mFloidService.mFloidDroidStatus.setFloidConnected(false);
            if (mFloidService.mUseLogger) Log.e(TAG, "Open: Fail: " + e.getMessage());
            mFloidService.uxSendFloidMessageToDisplay("Open Fail:  " + e.getMessage() + "\n");
        }
        mFloidService.uxSendFloidDroidStatus();
    }

    /**
     * Verify packet packet header.
     *
     * @param buffer        the buffer
     * @param messageLength the message length
     * @return the packet header
     */
    private PacketHeader verifyPacket(byte[] buffer, int messageLength) {
        try {
            PacketHeader packetHeader = new PacketHeader();
            // Verify minimum size
            if (messageLength < PACKET_HEADER_MINIMUM_SIZE) {
                // Bad message length
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "Message too short: " + messageLength + " < " + PACKET_HEADER_MINIMUM_SIZE);
                mFloidService.uxSendFloidMessageToDisplay("Message too short: " + messageLength + " < " + PACKET_HEADER_MINIMUM_SIZE + "\n");
                return null;
            }
            // Verify the tag: "!FLOID"
            if ((buffer[PACKET_HEADER_FLAG_OFFSET] != '!')
                    || (buffer[PACKET_HEADER_FLAG_OFFSET + 1] != 'F')
                    || (buffer[PACKET_HEADER_FLAG_OFFSET + 2] != 'L')
                    || (buffer[PACKET_HEADER_FLAG_OFFSET + 3] != 'O')
                    || (buffer[PACKET_HEADER_FLAG_OFFSET + 4] != 'I')
                    || (buffer[PACKET_HEADER_FLAG_OFFSET + 5] != 'D')) {
                // Bad tag:
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "Bad tag: " + buffer[PACKET_HEADER_FLAG_OFFSET]
                            + buffer[PACKET_HEADER_FLAG_OFFSET + 1]
                            + buffer[PACKET_HEADER_FLAG_OFFSET + 2]
                            + buffer[PACKET_HEADER_FLAG_OFFSET + 3]
                            + buffer[PACKET_HEADER_FLAG_OFFSET + 4]
                            + buffer[PACKET_HEADER_FLAG_OFFSET + 5]);
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "Message length: " + messageLength);

                mFloidService.uxSendFloidMessageToDisplay("Bad tag: " + buffer[PACKET_HEADER_FLAG_OFFSET]
                        + buffer[PACKET_HEADER_FLAG_OFFSET + 1]
                        + buffer[PACKET_HEADER_FLAG_OFFSET + 2]
                        + buffer[PACKET_HEADER_FLAG_OFFSET + 3]
                        + buffer[PACKET_HEADER_FLAG_OFFSET + 4]
                        + buffer[PACKET_HEADER_FLAG_OFFSET + 5] + "\n");
                return null;
            }
            // Verify the packet length
            int packetLength = arduinoByteArrayOffsetToShortInt(buffer, PACKET_HEADER_LENGTH_OFFSET);
            if (packetLength != messageLength) {
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "packetLength != messageLength: " + packetLength + " != " + messageLength);
                mFloidService.uxSendFloidMessageToDisplay("Bad message length - packet type: " + String.format(Locale.getDefault(), "%02x", buffer[PACKET_HEADER_TYPE_OFFSET]) + "  packetLength != messageLength: " + packetLength + " != " + messageLength + "\n");
                return null;
            }
            packetHeader.length = packetLength;
            // Verify the checksum:
            byte[] packetCheckSum = {0, 0, 0, 0};
            for (int packetIndex = PACKET_HEADER_CHECKSUM_CALC_START; packetIndex < packetLength; ++packetIndex) {
                packetCheckSum[(packetIndex - PACKET_HEADER_CHECKSUM_CALC_START) % 4] ^= buffer[packetIndex];
            }

            if ((buffer[PACKET_HEADER_CHECKSUM_OFFSET] != packetCheckSum[0])
                    || (buffer[PACKET_HEADER_CHECKSUM_OFFSET + 1] != packetCheckSum[1])
                    || (buffer[PACKET_HEADER_CHECKSUM_OFFSET + 2] != packetCheckSum[2])
                    || (buffer[PACKET_HEADER_CHECKSUM_OFFSET + 3] != packetCheckSum[3])) {
                // Bad tag:
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "Bad checksum: " + buffer[PACKET_HEADER_CHECKSUM_OFFSET] + ", "
                            + buffer[PACKET_HEADER_CHECKSUM_OFFSET + 1] + ", "
                            + buffer[PACKET_HEADER_CHECKSUM_OFFSET + 2] + ", "
                            + buffer[PACKET_HEADER_CHECKSUM_OFFSET + 3]
                            + "  !=  "
                            + packetCheckSum[0] + ", "
                            + packetCheckSum[1] + ", "
                            + packetCheckSum[2] + ", "
                            + packetCheckSum[3]);
                mFloidService.uxSendFloidMessageToDisplay("Bad checksum - packet type: " + String.format(Locale.getDefault(), "%02x", buffer[PACKET_HEADER_TYPE_OFFSET]) + "\n");

                mFloidService.uxSendFloidMessageToDisplay("Bad checksum: " + buffer[PACKET_HEADER_CHECKSUM_OFFSET] + ", "
                        + buffer[PACKET_HEADER_CHECKSUM_OFFSET + 1] + ", "
                        + buffer[PACKET_HEADER_CHECKSUM_OFFSET + 2] + ", "
                        + buffer[PACKET_HEADER_CHECKSUM_OFFSET + 3]
                        + "  !=  "
                        + packetCheckSum[0] + ", "
                        + packetCheckSum[1] + ", "
                        + packetCheckSum[2] + ", "
                        + packetCheckSum[3] + "\n");
                return null;
            }
            packetHeader.number = arduinoByteArrayOffsetToLongInt(buffer, PACKET_HEADER_NUMBER_OFFSET);
            packetHeader.type = arduinoByteArrayOffsetToByteInt(buffer, PACKET_HEADER_TYPE_OFFSET);
            // OK, packet was good...
            if (mFloidService.mUseLogger) Log.d(TAG, "Good packet from Floid.");
            return packetHeader;
        } catch (Exception e) {
            if (mFloidService.mUseLogger) {
                Log.e(TAG, "Exception decoding packet header: " + e);
            }
            return null;
        }
    }

    /**
     * Decode command response packet floid command response.
     *
     * @param buffer        the buffer
     * @param messageLength the message length
     * @return the floid command response
     */
    private FloidCommandResponse decodeCommandResponsePacket(byte[] buffer, int messageLength) {
        try {
        if (messageLength != COMMAND_RESPONSE_PACKET_SIZE) {
            if (mFloidService.mUseLogger)
                Log.e(TAG, "Bad Floid Command Response packet length: " + messageLength + " != " + COMMAND_RESPONSE_PACKET_SIZE);
            return null;    // No floidCommandResponse!
        }
        FloidCommandResponse floidCommandResponse = new FloidCommandResponse();
        floidCommandResponse.setCommandNumber(arduinoByteArrayOffsetToLongInt(buffer, COMMAND_RESPONSE_COMMAND_NUMBER_OFFSET));
        floidCommandResponse.setCommandStatus(arduinoByteArrayOffsetToShortInt(buffer, COMMAND_RESPONSE_STATUS_OFFSET));
        String identifier = new String(buffer, COMMAND_RESPONSE_IDENTIFIER_OFFSET, 3);
        floidCommandResponse.setIdentifier(identifier);
        return floidCommandResponse;
        } catch (Exception e) {
            if(mFloidService.mUseLogger) {
                Log.e(TAG, "Exception decoding floid command response packet: " + e);
            }
            return null;
        }
    }

    /**
     * Decode test response packet test packet response.
     *
     * @param buffer        the buffer
     * @param messageLength the message length
     * @return the test packet response
     */
    private TestPacketResponse decodeTestResponsePacket(byte[] buffer, int messageLength) {
        try {
            if (messageLength != TEST_RESPONSE_PACKET_SIZE) {
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "Bad Floid Test Response packet length: " + messageLength + " != " + TEST_RESPONSE_PACKET_SIZE);
                return null;
            }
            TestPacketResponse testPacketResponse = new TestPacketResponse();
            if (mFloidService.mUseLogger)
                Log.e(TAG, "TEST RESPONSE PACKET DECODING CURRENTLY UNIMPLEMENTED");


            testPacketResponse.byte_0 = arduinoByteArrayOffsetToByteInt(buffer, TEST_RESPONSE_PACKET_OFFSET_BYTES);
            testPacketResponse.byte_1 = arduinoByteArrayOffsetToByteInt(buffer, TEST_RESPONSE_PACKET_OFFSET_BYTES + 1);
            testPacketResponse.byte_2 = arduinoByteArrayOffsetToByteInt(buffer, TEST_RESPONSE_PACKET_OFFSET_BYTES + 2);

            testPacketResponse.int_0 = arduinoByteArrayOffsetToShortInt(buffer, TEST_RESPONSE_PACKET_OFFSET_INTS);
            testPacketResponse.int_1 = arduinoByteArrayOffsetToShortInt(buffer, TEST_RESPONSE_PACKET_OFFSET_INTS + 2);
            testPacketResponse.int_2 = arduinoByteArrayOffsetToShortInt(buffer, TEST_RESPONSE_PACKET_OFFSET_INTS + 4);

            testPacketResponse.unsigned_int_0 = arduinoByteArrayOffsetToShortInt(buffer, TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_INTS);
            testPacketResponse.unsigned_int_1 = arduinoByteArrayOffsetToShortInt(buffer, TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_INTS + 2);
            testPacketResponse.unsigned_int_2 = arduinoByteArrayOffsetToShortInt(buffer, TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_INTS + 4);

            testPacketResponse.long_int_0 = arduinoByteArrayOffsetToLongInt(buffer, TEST_RESPONSE_PACKET_OFFSET_LONGS);
            testPacketResponse.long_int_1 = arduinoByteArrayOffsetToLongInt(buffer, TEST_RESPONSE_PACKET_OFFSET_LONGS + 4);
            testPacketResponse.long_int_2 = arduinoByteArrayOffsetToLongInt(buffer, TEST_RESPONSE_PACKET_OFFSET_LONGS + 8);

            testPacketResponse.unsigned_long_int_0 = arduinoByteArrayOffsetToLongInt(buffer, TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_LONGS);
            testPacketResponse.unsigned_long_int_1 = arduinoByteArrayOffsetToLongInt(buffer, TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_LONGS + 4);
            testPacketResponse.unsigned_long_int_2 = arduinoByteArrayOffsetToLongInt(buffer, TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_LONGS + 8);
            return testPacketResponse;
        } catch (Exception e) {
            if (mFloidService.mUseLogger) {
                Log.e(TAG, "Exception decoding testt packet: " + e);
            }
            return null;
        }
    }

    /**
     * Decode floid status packet floid status.
     *
     * @param buffer        the buffer
     * @param messageLength the message length
     * @return the floid status
     */
    private FloidStatusParcelable decodeFloidStatusPacket(byte[] buffer, int messageLength) {
        try {
            if (messageLength != STATUS_PACKET_SIZE) {
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "Bad Floid Status packet length: " + messageLength + " != " + STATUS_PACKET_SIZE);
                return null;    // No floidStatus!
            }

            FloidStatusParcelable floidStatus = new FloidStatusParcelable();
            floidStatus.setFloidId(mFloidService.mFloidDroidStatus.getFloidId());
            floidStatus.setFloidUuid(mFloidService.mFloidDroidStatus.getFloidUuid());
            floidStatus.setStd(System.currentTimeMillis());  // Set the standard time from the Android
            floidStatus.setSt(arduinoByteArrayOffsetToLongInt(buffer, STATUS_PACKET_OFFSET_STATUS_TIME));
            floidStatus.setSn(arduinoByteArrayOffsetToLongInt(buffer, STATUS_PACKET_OFFSET_STATUS_NUMBER));
            floidStatus.setFm(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_FLOID_MODE));
            floidStatus.setFfm(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_FREE_MEMORY));
            floidStatus.setFmf(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_FOLLOW_MODE));
            //System Device Status: - 'd'
            floidStatus.setDc(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_CAMERA_ON));
            floidStatus.setDg(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_GPS_ON));
            floidStatus.setDf(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_GPS_EMULATE));
            floidStatus.setDv(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_GPS_DEVICE));
            floidStatus.setDp(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_PYR_ON));
            floidStatus.setDd(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DESIGNATOR_ON));
            floidStatus.setDa(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_AUX_ON));
            floidStatus.setDl(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_LED_ON));
            floidStatus.setDh(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_HELIS_ON));
            floidStatus.setDe(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_PARACHUTE_DEPLOYED));
            // Physics Model: - 'k'
            floidStatus.setKd(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_DIRECTION_OVER_GROUND));
            floidStatus.setKv(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_VELOCITY_OVER_GROUND));
            floidStatus.setKa(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ALTITUDE));
            floidStatus.setKw(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ALTITUDE_VELOCITY));
            floidStatus.setKc(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_DECLINATION));
            //GPS: - 'j'
            floidStatus.setJt(arduinoByteArrayOffsetToLongInt(buffer, STATUS_PACKET_OFFSET_GPS_TIME));
            floidStatus.setJx(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL));
            floidStatus.setJy(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL));
            floidStatus.setJf(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_GPS_FIX_QUALITY));
            floidStatus.setJm(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GPS_Z_METERS));
            floidStatus.setJz(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GPS_Z_FEET));
            floidStatus.setJh(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GPS_HDOP));
            floidStatus.setJs(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_GPS_SATS));
            floidStatus.setJh(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GPS_HDOP));
            floidStatus.setJxf(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL_FILTERED));
            floidStatus.setJyf(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL_FILTERED));
            floidStatus.setJmf(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GPS_Z_METERS_FILTERED));
            floidStatus.setJg(arduinoByteArrayOffsetToLongInt(buffer, STATUS_PACKET_OFFSET_GPS_GOOD_COUNT));
            // PYR: - 'p'
            floidStatus.setPv(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_PYR_DEVICE));
            floidStatus.setPh(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_HEADING));
            floidStatus.setPhs(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_HEADING_SMOOTHED));
            floidStatus.setPhv(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_HEADING_VELOCITY));
            floidStatus.setPz(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_HEIGHT));
            floidStatus.setPzs(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_HEIGHT_SMOOTHED));
            floidStatus.setPt(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_TEMPERATURE));
            floidStatus.setPp(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_PITCH));
            floidStatus.setPps(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_PITCH_SMOOTHED));
            floidStatus.setPr(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_ROLL));
            floidStatus.setPrs(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_ROLL_SMOOTHED));
            floidStatus.setPpv(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_PITCH_VELOCITY));
            floidStatus.setPrv(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_ROLL_VELOCITY));

            floidStatus.setPzv(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PYR_HEIGHT_VELOCITY));
            floidStatus.setPd(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_PYR_DIGITAL_MODE));
            floidStatus.setPar(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PYR_ANALOG_RATE));
            floidStatus.setPg(arduinoByteArrayOffsetToLongInt(buffer, STATUS_PACKET_OFFSET_PYR_GOOD_COUNT));
            floidStatus.setPe(arduinoByteArrayOffsetToLongInt(buffer, STATUS_PACKET_OFFSET_PYR_ERROR_COUNT));
            // Batteries: - 'b'
            floidStatus.setBm(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_MAIN_BATTERY));
            floidStatus.setB0(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_HELI0_BATTERY));
            floidStatus.setB1(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_HELI1_BATTERY));
            floidStatus.setB2(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_HELI2_BATTERY));
            floidStatus.setB3(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_HELI3_BATTERY));
            floidStatus.setBa(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_AUX_BATTERY));
            // Currents: - 'c'
            floidStatus.setC0(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_HELI0_CURRENT));
            floidStatus.setC1(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_HELI1_CURRENT));
            floidStatus.setC2(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_HELI2_CURRENT));
            floidStatus.setC3(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_HELI3_CURRENT));
            floidStatus.setCa(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_AUX_CURRENT));
            // Servos - 's#'
            floidStatus.setS0o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_S_0_ON));
            floidStatus.setS1o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_S_1_ON));
            floidStatus.setS2o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_S_2_ON));
            floidStatus.setS3o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_S_3_ON));
            floidStatus.setS00(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_0_L_VALUE));
            floidStatus.setS01(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_0_R_VALUE));
            floidStatus.setS02(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_0_P_VALUE));
            floidStatus.setS10(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_1_L_VALUE));
            floidStatus.setS11(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_1_R_VALUE));
            floidStatus.setS12(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_1_P_VALUE));
            floidStatus.setS20(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_2_L_VALUE));
            floidStatus.setS21(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_2_R_VALUE));
            floidStatus.setS22(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_2_P_VALUE));
            floidStatus.setS30(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_3_L_VALUE));
            floidStatus.setS31(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_3_R_VALUE));
            floidStatus.setS32(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_S_3_P_VALUE));
            // ESCs: - 'e'
            floidStatus.setEt(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_ESC_TYPE));
            floidStatus.setE0o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_ESC_0_ON));
            floidStatus.setE1o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_ESC_1_ON));
            floidStatus.setE2o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_ESC_2_ON));
            floidStatus.setE3o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_ESC_3_ON));
            floidStatus.setE0(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ESC_0_VALUE));
            floidStatus.setE1(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ESC_1_VALUE));
            floidStatus.setE2(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ESC_2_VALUE));
            floidStatus.setE3(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ESC_3_VALUE));
            floidStatus.setEp0(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_ESC_0_PULSE));
            floidStatus.setEp1(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_ESC_1_PULSE));
            floidStatus.setEp2(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_ESC_2_PULSE));
            floidStatus.setEp3(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_ESC_3_PULSE));
            // Goals: - 'g'
            floidStatus.setGhg(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_HAS_GOAL));
            floidStatus.setGid(arduinoByteArrayOffsetToLongInt(buffer, STATUS_PACKET_OFFSET_GOAL_ID));
            floidStatus.setGt(arduinoByteArrayOffsetToLongInt(buffer, STATUS_PACKET_OFFSET_GOAL_TICKS));
            floidStatus.setGs(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_GOAL_STATE));
            floidStatus.setGx(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GOAL_X));
            floidStatus.setGy(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GOAL_Y));
            floidStatus.setGz(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GOAL_Z));
            floidStatus.setGa(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_GOAL_A));
            floidStatus.setGif(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_IN_FLIGHT));
            floidStatus.setGlo(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_LIFT_OFF_MODE));
            floidStatus.setGld(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_LAND_MODE));
            // Pan Tilt: - 't'
            floidStatus.setT0m(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_0_MODE));
            floidStatus.setT1m(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_1_MODE));
            floidStatus.setT2m(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_2_MODE));
            floidStatus.setT3m(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_3_MODE));
            floidStatus.setT0x(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_0_TARGET_X));
            floidStatus.setT0y(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_0_TARGET_Y));
            floidStatus.setT0z(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_0_TARGET_Z));
            floidStatus.setT1x(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_1_TARGET_X));
            floidStatus.setT1y(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_1_TARGET_Y));
            floidStatus.setT1z(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_1_TARGET_Z));
            floidStatus.setT2x(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_2_TARGET_X));
            floidStatus.setT2y(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_2_TARGET_Y));
            floidStatus.setT2z(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_2_TARGET_Z));
            floidStatus.setT3x(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_3_TARGET_X));
            floidStatus.setT3y(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_3_TARGET_Y));
            floidStatus.setT3z(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_PT_3_TARGET_Z));
            floidStatus.setT0o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_PT_0_ON));
            floidStatus.setT1o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_PT_1_ON));
            floidStatus.setT2o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_PT_2_ON));
            floidStatus.setT3o(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_PT_3_ON));
            floidStatus.setT0p(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_0_PAN_VALUE));
            floidStatus.setT0t(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_0_TILT_VALUE));
            floidStatus.setT1p(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_1_PAN_VALUE));
            floidStatus.setT1t(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_1_TILT_VALUE));
            floidStatus.setT2p(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_2_PAN_VALUE));
            floidStatus.setT2t(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_2_TILT_VALUE));
            floidStatus.setT3p(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_3_PAN_VALUE));
            floidStatus.setT3t(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PT_3_TILT_VALUE));
            // Payloads: - 'y'
            floidStatus.setY0(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_BAY0));
            floidStatus.setY1(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_BAY1));
            floidStatus.setY2(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_BAY2));
            floidStatus.setY3(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_BAY3));
            floidStatus.setYn0(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_NM0));
            floidStatus.setYn1(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_NM1));
            floidStatus.setYn2(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_NM2));
            floidStatus.setYn3(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_NM3));
            floidStatus.setYg0(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PAYLOAD_0_GOAL_STATE));
            floidStatus.setYg1(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PAYLOAD_1_GOAL_STATE));
            floidStatus.setYg2(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PAYLOAD_2_GOAL_STATE));
            floidStatus.setYg3(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_PAYLOAD_3_GOAL_STATE));
            // Debug: -'v'
            floidStatus.setVd(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_ON));
            floidStatus.setVx(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_AUX_ON));
            floidStatus.setVb(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_PAYLOADS_ON));
            floidStatus.setVc(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_CAMERA_ON));
            floidStatus.setVe(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_DESIGNATOR_ON));
            floidStatus.setVg(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_GPS_ON));
            floidStatus.setVh(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_HELIS_ON));
            floidStatus.setVm(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_MEMORY_ON));
            floidStatus.setVt(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_PAN_TILT_ON));
            floidStatus.setVy(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_PHYSICS_ON));
            floidStatus.setVp(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_PYR_ON));
            floidStatus.setVl(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_DEBUG_ALTIMETER_ON));
            floidStatus.setVa(floidStatus.isVd()
                    && floidStatus.isVx()
                    && floidStatus.isVb()
                    && floidStatus.isVc()
                    && floidStatus.isVe()
                    && floidStatus.isVg()
                    && floidStatus.isVh()
                    && floidStatus.isVm()
                    && floidStatus.isVt()
                    && floidStatus.isVy()
                    && floidStatus.isVp()
                    && floidStatus.isVl());
            // Serial output:
            floidStatus.setVs(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_SERIAL_OUTPUT));
            // Altimeter: -'a'
            floidStatus.setAi(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_ALTIMETER_INITIALIZED));
            floidStatus.setAv(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE_VALID));
            floidStatus.setAa(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE));
            floidStatus.setAd(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ALTIMETER_DELTA));
            floidStatus.setAc(arduinoByteArrayOffsetToLongInt(buffer, STATUS_PACKET_OFFSET_ALTIMETER_GOOD_COUNT));
            // Loop count and rate (since last status)
            floidStatus.setLc(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_LOOP_COUNT));
            floidStatus.setLr(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_LOOP_RATE));
            floidStatus.setSr(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_STATUS_RATE));
            floidStatus.setJr(arduinoByteArrayOffsetToByteInt(buffer, STATUS_PACKET_OFFSET_GPS_RATE));
            // Model: -'m'
            floidStatus.setMt(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE));
            floidStatus.setMd(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_DISTANCE_TO_TARGET));
            floidStatus.setMa(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ALTITUDE_TO_TARGET));
            floidStatus.setMo(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ORIENTATION_TO_TARGET));
            floidStatus.setMg(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ORIENTATION_TO_GOAL));
            floidStatus.setMk(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_ATTACK_ANGLE));
            // Targets:
            floidStatus.setOop(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_ORIENTATION_PITCH));
            floidStatus.setOoq(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_ORIENTATION_PITCH_DELTA));
            floidStatus.setOor(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_ORIENTATION_ROLL));
            floidStatus.setOos(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_ORIENTATION_ROLL_DELTA));
            floidStatus.setOpv(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_PITCH_VELOCITY));
            floidStatus.setOpw(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_PITCH_VELOCITY_DELTA));
            floidStatus.setOrv(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_ROLL_VELOCITY));
            floidStatus.setOrw(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_ROLL_VELOCITY_DELTA));
            floidStatus.setOav(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY));
            floidStatus.setOaw(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_DELTA));
            floidStatus.setOhv(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_HEADING_VELOCITY));
            floidStatus.setOhw(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_HEADING_VELOCITY_DELTA));
            floidStatus.setOv(arduinoByteArrayOffsetToFloat(buffer, STATUS_PACKET_OFFSET_TARGET_XY_VELOCITY));
            // Heli Startup Mode:
            floidStatus.setHsm(arduinoByteArrayOffsetToBoolean(buffer, STATUS_PACKET_OFFSET_HELI_STARTUP_MODE));
            // Run and test modes:
            floidStatus.setRm(buffer[STATUS_PACKET_OFFSET_FLOID_RUN_MODE]);
            floidStatus.setTm(buffer[STATUS_PACKET_OFFSET_FLOID_TEST_MODE]);
            // IMU Algorithm status: - a short int - 16 bits - e.g. uint16_t
            floidStatus.setIas(arduinoByteArrayOffsetToShortInt(buffer, STATUS_PACKET_OFFSET_IMU_ALGORITHM_STATUS));
            return floidStatus;
        } catch (Exception e) {
            if (mFloidService.mUseLogger) {
                Log.e(TAG, "Exception decoding floid status packet: " + e);
            }
            return null;
        }
    }

    /**
     * Decode floid model status packet floid model status.
     *
     * @param buffer        the buffer
     * @param messageLength the message length
     * @return the floid model status
     */
    private FloidModelStatusParcelable decodeFloidModelStatusPacket(byte[] buffer, int messageLength) {
        try {
            if (messageLength != MODEL_STATUS_PACKET_SIZE) {
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "Bad Floid Model Status packet length: " + messageLength + " != " + MODEL_STATUS_PACKET_SIZE);
                return null;    // No floidModelStatus!
            }
            FloidModelStatusParcelable floidModelStatus = new FloidModelStatusParcelable();
            floidModelStatus.setFloidId(mFloidService.mFloidDroidStatus.getFloidId());
            floidModelStatus.setFloidUuid(mFloidService.mFloidDroidStatus.getFloidUuid());


            floidModelStatus.setCollectiveDeltaOrientationH0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ORIENTATION));

            floidModelStatus.setCollectiveDeltaOrientationH1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ORIENTATION + 4));
            floidModelStatus.setCollectiveDeltaOrientationH2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ORIENTATION + 2 * 4));
            floidModelStatus.setCollectiveDeltaOrientationH3(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ORIENTATION + 3 * 4));

            floidModelStatus.setCollectiveDeltaAltitudeH0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ALTITUDE));

            floidModelStatus.setCollectiveDeltaAltitudeH1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ALTITUDE + 4));
            floidModelStatus.setCollectiveDeltaAltitudeH2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ALTITUDE + 2 * 4));
            floidModelStatus.setCollectiveDeltaAltitudeH3(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ALTITUDE + 3 * 4));

            floidModelStatus.setCollectiveDeltaHeadingH0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_HEADING));

            floidModelStatus.setCollectiveDeltaHeadingH1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_HEADING + 4));
            floidModelStatus.setCollectiveDeltaHeadingH2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_HEADING + 2 * 4));
            floidModelStatus.setCollectiveDeltaHeadingH3(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_HEADING + 3 * 4));

            floidModelStatus.setCollectiveDeltaTotalH0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_TOTAL));

            floidModelStatus.setCollectiveDeltaTotalH1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_TOTAL + 4));
            floidModelStatus.setCollectiveDeltaTotalH2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_TOTAL + 2 * 4));
            floidModelStatus.setCollectiveDeltaTotalH3(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_TOTAL + 3 * 4));

            floidModelStatus.setCyclicValueH0S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE));

            floidModelStatus.setCyclicValueH0S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 4));

            floidModelStatus.setCyclicValueH0S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 2 * 4));

            floidModelStatus.setCyclicValueH1S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 12));

            floidModelStatus.setCyclicValueH1S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 12 + 4));

            floidModelStatus.setCyclicValueH1S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 12 + 2 * 4));

            floidModelStatus.setCyclicValueH2S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 2 * 12));

            floidModelStatus.setCyclicValueH2S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 2 * 12 + 4));
            floidModelStatus.setCyclicValueH2S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 2 * 12 + 2 * 4));

            floidModelStatus.setCyclicValueH3S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 3 * 12));

            floidModelStatus.setCyclicValueH3S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 3 * 12 + 4));
            floidModelStatus.setCyclicValueH3S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE + 3 * 12 + 2 * 4));

            floidModelStatus.setCollectiveValueH0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_VALUE));

            floidModelStatus.setCollectiveValueH1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_VALUE + 4));
            floidModelStatus.setCollectiveValueH2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_VALUE + 2 * 4));
            floidModelStatus.setCollectiveValueH3(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_VALUE + 3 * 4));

            floidModelStatus.setCalculatedCyclicBladePitchH0S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH));

            floidModelStatus.setCalculatedCyclicBladePitchH0S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 4));

            floidModelStatus.setCalculatedCyclicBladePitchH0S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 2 * 4));

            floidModelStatus.setCalculatedCyclicBladePitchH1S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 12));

            floidModelStatus.setCalculatedCyclicBladePitchH1S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 12 + 4));

            floidModelStatus.setCalculatedCyclicBladePitchH1S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 12 + 2 * 4));

            floidModelStatus.setCalculatedCyclicBladePitchH2S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 2 * 12));

            floidModelStatus.setCalculatedCyclicBladePitchH2S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 2 * 12 + 4));
            floidModelStatus.setCalculatedCyclicBladePitchH2S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 2 * 12 + 2 * 4));

            floidModelStatus.setCalculatedCyclicBladePitchH3S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 3 * 12));

            floidModelStatus.setCalculatedCyclicBladePitchH3S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 3 * 12 + 4));
            floidModelStatus.setCalculatedCyclicBladePitchH3S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH + 3 * 12 + 2 * 4));

            floidModelStatus.setCalculatedCollectiveBladePitchH0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_COLLECTIVE_BLADE_PITCH));

            floidModelStatus.setCalculatedCollectiveBladePitchH1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_COLLECTIVE_BLADE_PITCH + 4));
            floidModelStatus.setCalculatedCollectiveBladePitchH2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_COLLECTIVE_BLADE_PITCH + 2 * 4));
            floidModelStatus.setCalculatedCollectiveBladePitchH3(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_COLLECTIVE_BLADE_PITCH + 3 * 4));

            floidModelStatus.setCalculatedBladePitchH0S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH));

            floidModelStatus.setCalculatedBladePitchH0S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 4));

            floidModelStatus.setCalculatedBladePitchH0S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 2 * 4));

            floidModelStatus.setCalculatedBladePitchH1S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 12));

            floidModelStatus.setCalculatedBladePitchH1S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 12 + 4));

            floidModelStatus.setCalculatedBladePitchH1S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 12 + 2 * 4));

            floidModelStatus.setCalculatedBladePitchH2S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 2 * 12));

            floidModelStatus.setCalculatedBladePitchH2S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 2 * 12 + 4));
            floidModelStatus.setCalculatedBladePitchH2S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 2 * 12 + 2 * 4));

            floidModelStatus.setCalculatedBladePitchH3S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 3 * 12));

            floidModelStatus.setCalculatedBladePitchH3S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 3 * 12 + 4));
            floidModelStatus.setCalculatedBladePitchH3S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH + 3 * 12 + 2 * 4));

            floidModelStatus.setCalculatedServoDegreesH0S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES));

            floidModelStatus.setCalculatedServoDegreesH0S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 4));

            floidModelStatus.setCalculatedServoDegreesH0S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 2 * 4));

            floidModelStatus.setCalculatedServoDegreesH1S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 12));

            floidModelStatus.setCalculatedServoDegreesH1S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 12 + 4));

            floidModelStatus.setCalculatedServoDegreesH1S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 12 + 2 * 4));

            floidModelStatus.setCalculatedServoDegreesH2S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 2 * 12));

            floidModelStatus.setCalculatedServoDegreesH2S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 2 * 12 + 4));
            floidModelStatus.setCalculatedServoDegreesH2S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 2 * 12 + 2 * 4));

            floidModelStatus.setCalculatedServoDegreesH3S0(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 3 * 12));

            floidModelStatus.setCalculatedServoDegreesH3S1(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 3 * 12 + 4));
            floidModelStatus.setCalculatedServoDegreesH3S2(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES + 3 * 12 + 2 * 4));

            floidModelStatus.setCalculatedServoPulseH0S0(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE));

            floidModelStatus.setCalculatedServoPulseH0S1(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 2));

            floidModelStatus.setCalculatedServoPulseH0S2(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 2 * 2));

            floidModelStatus.setCalculatedServoPulseH1S0(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 6));

            floidModelStatus.setCalculatedServoPulseH1S1(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 6 + 2));

            floidModelStatus.setCalculatedServoPulseH1S2(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 6 + 2 * 2));

            floidModelStatus.setCalculatedServoPulseH2S0(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 2 * 6));

            floidModelStatus.setCalculatedServoPulseH2S1(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 2 * 6 + 2));
            floidModelStatus.setCalculatedServoPulseH2S2(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 2 * 6 + 2 * 2));

            floidModelStatus.setCalculatedServoPulseH3S0(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 3 * 6));

            floidModelStatus.setCalculatedServoPulseH3S1(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 3 * 6 + 2));
            floidModelStatus.setCalculatedServoPulseH3S2(arduinoByteArrayOffsetToShortInt(buffer, MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE + 3 * 6 + 2 * 2));
            floidModelStatus.setCyclicHeading(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_CYCLIC_HEADING));
            floidModelStatus.setVelocityCyclicAlpha(arduinoByteArrayOffsetToFloat(buffer, MODEL_STATUS_PACKET_OFFSET_VELOCITY_CYCLIC_ALPHA));
            return floidModelStatus;
        } catch (Exception e) {
            if(mFloidService.mUseLogger) {
                Log.e(TAG, "Exception decoding model status packet: " + e);
            }
            return null;
        }
    }

    /**
     * Decode floid model parameters packet floid model parameters.
     *
     * @param buffer        the buffer
     * @param messageLength the message length
     * @return the floid model parameters
     */
    private FloidModelParametersParcelable decodeFloidModelParametersPacket(byte[] buffer, int messageLength) {
        try {
            if (messageLength != MODEL_PARAMETERS_PACKET_SIZE) {
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "Bad Floid Model Parameters packet length: " + messageLength + " != " + MODEL_PARAMETERS_PACKET_SIZE);
                return null;    // No floidModelParameters!
            }
            FloidModelParametersParcelable floidModelParameters = new FloidModelParametersParcelable();
            floidModelParameters.setFloidId(mFloidService.mFloidDroidStatus.getFloidId());
            floidModelParameters.setFloidUuid(mFloidService.mFloidDroidStatus.getFloidUuid());
            floidModelParameters.setBladesLow(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_BLADES_LOW));
            floidModelParameters.setBladesZero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_BLADES_ZERO));
            floidModelParameters.setBladesHigh(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_BLADES_HIGH));
            floidModelParameters.setH0S0Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H0S0_LOW));
            floidModelParameters.setH0S1Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H0S1_LOW));
            floidModelParameters.setH0S2Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H0S2_LOW));
            floidModelParameters.setH1S0Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H1S0_LOW));
            floidModelParameters.setH1S1Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H1S1_LOW));
            floidModelParameters.setH1S2Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H1S2_LOW));
            floidModelParameters.setH0S0Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H0S0_ZERO));
            floidModelParameters.setH0S1Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H0S1_ZERO));
            floidModelParameters.setH0S2Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H0S2_ZERO));
            floidModelParameters.setH1S0Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H1S0_ZERO));
            floidModelParameters.setH1S1Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H1S1_ZERO));
            floidModelParameters.setH1S2Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H1S2_ZERO));
            floidModelParameters.setH0S0High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H0S0_HIGH));
            floidModelParameters.setH0S1High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H0S1_HIGH));
            floidModelParameters.setH0S2High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H0S2_HIGH));
            floidModelParameters.setH1S0High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H1S0_HIGH));
            floidModelParameters.setH1S1High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H1S1_HIGH));
            floidModelParameters.setH1S2High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H1S2_HIGH));
            floidModelParameters.setH2S0Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H2S0_LOW));
            floidModelParameters.setH2S1Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H2S1_LOW));
            floidModelParameters.setH2S2Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H2S2_LOW));
            floidModelParameters.setH3S0Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H3S0_LOW));
            floidModelParameters.setH3S1Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H3S1_LOW));
            floidModelParameters.setH3S2Low(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H3S2_LOW));
            floidModelParameters.setH2S0Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H2S0_ZERO));
            floidModelParameters.setH2S1Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H2S1_ZERO));
            floidModelParameters.setH2S2Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H2S2_ZERO));
            floidModelParameters.setH3S0Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H3S0_ZERO));
            floidModelParameters.setH3S1Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H3S1_ZERO));
            floidModelParameters.setH3S2Zero(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H3S2_ZERO));
            floidModelParameters.setH2S0High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H2S0_HIGH));
            floidModelParameters.setH2S1High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H2S1_HIGH));
            floidModelParameters.setH2S2High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H2S2_HIGH));
            floidModelParameters.setH3S0High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H3S0_HIGH));
            floidModelParameters.setH3S1High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H3S1_HIGH));
            floidModelParameters.setH3S2High(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_H3S2_HIGH));
            floidModelParameters.setCollectiveMin(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_COLLECTIVE_MIN));
            floidModelParameters.setCollectiveMax(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_COLLECTIVE_MAX));
            floidModelParameters.setCollectiveDefault(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_COLLECTIVE_DEFAULT));
            floidModelParameters.setCyclicRange(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_CYCLIC_RANGE));
            floidModelParameters.setCyclicDefault(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_CYCLIC_DEFAULT));
            floidModelParameters.setEscType(arduinoByteArrayOffsetToShortInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ESC_TYPE));
            floidModelParameters.setEscPulseMin(arduinoByteArrayOffsetToShortInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ESC_PULSE_MIN));
            floidModelParameters.setEscPulseMax(arduinoByteArrayOffsetToShortInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ESC_PULSE_MAX));
            floidModelParameters.setEscLowValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ESC_LOW_VALUE));
            floidModelParameters.setEscHighValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ESC_HIGH_VALUE));
            floidModelParameters.setAttackAngleMinDistanceValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ATTACK_ANGLE_MIN_DISTANCE_VALUE));
            floidModelParameters.setAttackAngleMaxDistanceValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ATTACK_ANGLE_MAX_DISTANCE_VALUE));
            floidModelParameters.setAttackAngleValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ATTACK_ANGLE_VALUE));
            floidModelParameters.setPitchDeltaMinValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_PITCH_DELTA_MIN_VALUE));
            floidModelParameters.setPitchDeltaMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_PITCH_DELTA_MAX_VALUE));
            floidModelParameters.setPitchTargetVelocityMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_PITCH_TARGET_VELOCITY_MAX_VALUE));
            floidModelParameters.setRollDeltaMinValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ROLL_DELTA_MIN_VALUE));
            floidModelParameters.setRollDeltaMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ROLL_DELTA_MAX_VALUE));
            floidModelParameters.setRollTargetVelocityMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ROLL_TARGET_VELOCITY_MAX_VALUE));
            floidModelParameters.setAltitudeToTargetMinValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ALTITUDE_TO_TARGET_MIN_VALUE));
            floidModelParameters.setAltitudeToTargetMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ALTITUDE_TO_TARGET_MAX_VALUE));
            floidModelParameters.setAltitudeTargetVelocityMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ALTITUDE_TARGET_VELOCITY_MAX_VALUE));
            floidModelParameters.setHeadingDeltaMinValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HEADING_DELTA_MIN_VALUE));
            floidModelParameters.setHeadingDeltaMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HEADING_DELTA_MAX_VALUE));
            floidModelParameters.setHeadingTargetVelocityMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HEADING_TARGET_VELOCITY_MAX_VALUE));
            floidModelParameters.setDistanceToTargetMinValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_DISTANCE_TO_TARGET_MIN_VALUE));
            floidModelParameters.setDistanceToTargetMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_DISTANCE_TO_TARGET_MAX_VALUE));
            floidModelParameters.setDistanceTargetVelocityMaxValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_DISTANCE_TARGET_VELOCITY_MAX_VALUE));
            floidModelParameters.setOrientationMinDistanceValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ORIENTATION_MIN_DISTANCE_VALUE));
            floidModelParameters.setLiftOffTargetAltitudeDeltaValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE_DELTA_VALUE));
            floidModelParameters.setServoPulseMin(arduinoByteArrayOffsetToShortInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_SERVO_PULSE_MIN));
            floidModelParameters.setServoPulseMax(arduinoByteArrayOffsetToShortInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_SERVO_PULSE_MAX));
            floidModelParameters.setServoDegreeMin(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_SERVO_DEGREE_MIN));
            floidModelParameters.setServoDegreeMax(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_SERVO_DEGREE_MAX));
            floidModelParameters.setServoSignLeft(arduinoByteArrayOffsetToSignedByteInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_SERVO_SIGN_LEFT));
            floidModelParameters.setServoSignRight(arduinoByteArrayOffsetToSignedByteInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_SERVO_SIGN_RIGHT));
            floidModelParameters.setServoSignPitch(arduinoByteArrayOffsetToSignedByteInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_SERVO_SIGN_PITCH));
            floidModelParameters.setServoOffsetLeft0(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_0_SERVO_OFFSET_LEFT));
            floidModelParameters.setServoOffsetRight0(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_0_SERVO_OFFSET_RIGHT));
            floidModelParameters.setServoOffsetPitch0(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_0_SERVO_OFFSET_PITCH));
            floidModelParameters.setServoOffsetLeft1(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_1_SERVO_OFFSET_LEFT));
            floidModelParameters.setServoOffsetRight1(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_1_SERVO_OFFSET_RIGHT));
            floidModelParameters.setServoOffsetPitch1(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_1_SERVO_OFFSET_PITCH));
            floidModelParameters.setServoOffsetLeft2(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_2_SERVO_OFFSET_LEFT));
            floidModelParameters.setServoOffsetRight2(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_2_SERVO_OFFSET_RIGHT));
            floidModelParameters.setServoOffsetPitch2(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_2_SERVO_OFFSET_PITCH));
            floidModelParameters.setServoOffsetLeft3(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_3_SERVO_OFFSET_LEFT));
            floidModelParameters.setServoOffsetRight3(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_3_SERVO_OFFSET_RIGHT));
            floidModelParameters.setServoOffsetPitch3(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_3_SERVO_OFFSET_PITCH));
            floidModelParameters.setTargetVelocityKeepStill(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_TARGET_VELOCITY_KEEP_STILL));
            floidModelParameters.setTargetVelocityFullSpeed(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_TARGET_VELOCITY_FULL_SPEED));
            floidModelParameters.setTargetPitchVelocityAlpha(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_TARGET_PITCH_VELOCITY_ALPHA));
            floidModelParameters.setTargetRollVelocityAlpha(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_TARGET_ROLL_VELOCITY_ALPHA));
            floidModelParameters.setTargetHeadingVelocityAlpha(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_TARGET_HEADING_VELOCITY_ALPHA));
            floidModelParameters.setTargetAltitudeVelocityAlpha(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_ALPHA));
            floidModelParameters.setLandModeTimeRequiredAtMinAltitude(arduinoByteArrayOffsetToLongInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE));
            floidModelParameters.setStartMode(arduinoByteArrayOffsetToSignedByteInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_START_MODE));
            floidModelParameters.setRotationMode(arduinoByteArrayOffsetToSignedByteInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ROTATION_MODE));
            floidModelParameters.setCyclicHeadingAlpha(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_CYCLIC_HEADING_ALPHA));
            floidModelParameters.setHeliStartupModeNumberSteps(arduinoByteArrayOffsetToShortInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_STARTUP_NUMBER_STEPS));
            floidModelParameters.setHeliStartupModeStepTick(arduinoByteArrayOffsetToShortInt(buffer, MODEL_PARAMETERS_PACKET_OFFSET_HELI_STARTUP_STEP_TICK));
            floidModelParameters.setFloidModelEscCollectiveCalcMidpoint(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_CALC_MIDPOINT));
            floidModelParameters.setFloidModelEscCollectiveLowValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_LOW_VALUE));
            floidModelParameters.setFloidModelEscCollectiveMidValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_MID_VALUE));
            floidModelParameters.setFloidModelEscCollectiveHighValue(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_HIGH_VALUE));
            floidModelParameters.setVelocityDeltaCyclicAlphaScale(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_VELOCITY_CYCLIC_ALPHA_SCALE));
            floidModelParameters.setAccelerationMultiplierScale(arduinoByteArrayOffsetToFloat(buffer, MODEL_PARAMETERS_PACKET_OFFSET_ACCELERATION_MULTIPLIER_SCALE));

            return floidModelParameters;
        } catch (Exception e) {
            if (mFloidService.mUseLogger) {
                Log.e(TAG, "Exception decoding model parameters packet: " + e);
            }
            return null;
        }
    }

    /**
     * Decode debug packet string.
     *
     * @param buffer        the buffer
     * @param messageLength the message length
     * @return the string
     */
    private String decodeDebugPacket(byte[] buffer, int messageLength) {
        try {
            return new String(buffer, DEBUG_PACKET_START_OFFSET, messageLength - DEBUG_PACKET_START_OFFSET);
        } catch (Exception e) {
            if (mFloidService.mUseLogger) {
                Log.e(TAG, "Exception decoding debug packet: " + e);
            }
            return null;
        }
    }
}

