/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import com.faiglelabs.floid.servertypes.commands.*;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid3.FloidService;

/**
 * Factories an instruction processor for a floid service given a droid instruction
 */
public class InstructionProcessorFactory {
    /**
     * Get an instruction processor instance factoried
     *
     * @param floidService     the floid service
     * @param droidInstruction the droid instruction
     * @return the new instruction process for this instruction
     */
    public static InstructionProcessor getNewInstructionProcessor(FloidService floidService, DroidInstruction droidInstruction) {
        // AcquireHomePosition:
        if (droidInstruction.getClass().equals(AcquireHomePositionCommand.class)) {
            floidService.serviceSendSpeakCommand("Acquiring Home Position");
            return new AcquireHomePositionCommandProcessor(floidService, (AcquireHomePositionCommand) droidInstruction);
        }
        // Debug:
        else if (droidInstruction.getClass().equals(DebugCommand.class)) {
            floidService.serviceSendSpeakCommand("Debug");
            return new DebugCommandProcessor(floidService, (DebugCommand) droidInstruction);
        }
        // Delay:
        else if (droidInstruction.getClass().equals(DelayCommand.class)) {
            floidService.serviceSendSpeakCommand("Delaying");
            return new DelayCommandProcessor(floidService, (DelayCommand) droidInstruction);
        }
        // DeployParachute:
        else if (droidInstruction.getClass().equals(DeployParachuteCommand.class)) {
            floidService.serviceSendSpeakCommand("Deploying Parachute");
            return new DeployParachuteCommandProcessor(floidService, (DeployParachuteCommand) droidInstruction);
        }
        // FlyTo:
        else if (droidInstruction.getClass().equals(FlyToCommand.class)) {
            floidService.serviceSendSpeakCommand("Fly To Command");
            return new FlyToCommandProcessor(floidService, (FlyToCommand) droidInstruction);
        }
        // HeightTo:
        else if (droidInstruction.getClass().equals(HeightToCommand.class)) {
            floidService.serviceSendSpeakCommand("Height To Command");
            return new HeightToCommandProcessor(floidService, (HeightToCommand) droidInstruction);
        }
        // LiftOff:
        else if (droidInstruction.getClass().equals(LiftOffCommand.class)) {
            floidService.serviceSendSpeakCommand("Lifting Off");
            return new LiftOffCommandProcessor(floidService, (LiftOffCommand) droidInstruction);
        }
        // Null:
        else if (droidInstruction.getClass().equals(NullCommand.class)) {
            return new NullCommandProcessor(floidService, (NullCommand) droidInstruction);
        }
        // Retrieve Logs:
        else if (droidInstruction.getClass().equals(RetrieveLogsCommand.class)) {
            floidService.serviceSendSpeakCommand("Retrieving Logs");
            return new RetrieveLogsCommandProcessor(floidService, (RetrieveLogsCommand) droidInstruction);
        }
        // SetParameters:
        else if (droidInstruction.getClass().equals(SetParametersCommand.class)) {
            floidService.serviceSendSpeakCommand("Setting Parameters");
            return new SetParametersCommandProcessor(floidService, (SetParametersCommand) droidInstruction);
        }
        // StartUpHelis:
        else if (droidInstruction.getClass().equals(StartUpHelisCommand.class)) {
            floidService.serviceSendSpeakCommand("Starting Up Helis");
            return new StartUpHelisCommandProcessor(floidService, (StartUpHelisCommand) droidInstruction);
        }
        // ShutDownHelis:
        else if (droidInstruction.getClass().equals(ShutDownHelisCommand.class)) {
            floidService.serviceSendSpeakCommand("Shutting Down Helis");
            return new ShutDownHelisCommandProcessor(floidService, (ShutDownHelisCommand) droidInstruction);
        }
        // TurnTo:
        else if (droidInstruction.getClass().equals(TurnToCommand.class)) {
            floidService.serviceSendSpeakCommand("Turn To Command");
            return new TurnToCommandProcessor(floidService, (TurnToCommand) droidInstruction);
        }
        // Payload:
        else if (droidInstruction.getClass().equals(PayloadCommand.class)) {
            floidService.serviceSendSpeakCommand("Releasing Payload");
            return new PayloadCommandProcessor(floidService, (PayloadCommand) droidInstruction);
        }
        // Designate:
        else if (droidInstruction.getClass().equals(DesignateCommand.class)) {
            floidService.serviceSendSpeakCommand("Designating");
            return new DesignateCommandProcessor(floidService, (DesignateCommand) droidInstruction);
        }
        // FlyPattern:
        else if (droidInstruction.getClass().equals(FlyPatternCommand.class)) {
            floidService.serviceSendSpeakCommand("Flying Pattern");
//            return new FlyPatternCommandProcessor(floidService, (FlyPatternCommand)droidInstruction);   // [FlyPattern] command unimplemented...
        }
        // Hover:
        else if (droidInstruction.getClass().equals(HoverCommand.class)) {
            floidService.serviceSendSpeakCommand("Hovering");
            return new HoverCommandProcessor(floidService, (HoverCommand) droidInstruction);
        }
        // Land:
        else if (droidInstruction.getClass().equals(LandCommand.class)) {
            floidService.serviceSendSpeakCommand("Landing");
            return new LandCommandProcessor(floidService, (LandCommand) droidInstruction);
        }
        // PanTilt:
        else if (droidInstruction.getClass().equals(PanTiltCommand.class)) {
            floidService.serviceSendSpeakCommand("Pan Tilt");
            return new PanTiltCommandProcessor(floidService, (PanTiltCommand) droidInstruction);
        }
        // Speaker:
        else if (droidInstruction.getClass().equals(SpeakerCommand.class)) {
            return new SpeakerCommandProcessor(floidService, (SpeakerCommand) droidInstruction);
        }
        // Speaker On:
        else if (droidInstruction.getClass().equals(SpeakerOnCommand.class)) {
            return new SpeakerOnCommandProcessor(floidService, (SpeakerOnCommand) droidInstruction);
        }
        // Speaker Off:
        else if (droidInstruction.getClass().equals(SpeakerOffCommand.class)) {
            return new SpeakerOffCommandProcessor(floidService, (SpeakerOffCommand) droidInstruction);
        }
        // StartVideo:
        else if (droidInstruction.getClass().equals(StartVideoCommand.class)) {
            floidService.serviceSendSpeakCommand("Starting Video");
            return new StartVideoCommandProcessor(floidService, (StartVideoCommand) droidInstruction);
        }
        // StopVideo:
        else if (droidInstruction.getClass().equals(StopVideoCommand.class)) {
            floidService.serviceSendSpeakCommand("Stopping Video");
            return new StopVideoCommandProcessor(floidService, (StopVideoCommand) droidInstruction);
        }
        // StopDesignating:
        else if (droidInstruction.getClass().equals(StopDesignatingCommand.class)) {
            floidService.serviceSendSpeakCommand("Stopping Designation");
            return new StopDesignatingCommandProcessor(floidService, (StopDesignatingCommand) droidInstruction);
        }
        // PhotoStrobe:
        else if (droidInstruction.getClass().equals(PhotoStrobeCommand.class)) {
            floidService.serviceSendSpeakCommand("Starting Photo Strobe");
            return new PhotoStrobeCommandProcessor(floidService, (PhotoStrobeCommand) droidInstruction);
        }
        // StopPhotoStrobe:
        else if (droidInstruction.getClass().equals(StopPhotoStrobeCommand.class)) {
            floidService.serviceSendSpeakCommand("Stopping Photo Strobe");
            return new StopPhotoStrobeCommandProcessor(floidService, (StopPhotoStrobeCommand) droidInstruction);
        }
        // TakePhoto:
        else if (droidInstruction.getClass().equals(TakePhotoCommand.class)) {
            floidService.serviceSendSpeakCommand("Taking Photo");
            return new TakePhotoCommandProcessor(floidService, (TakePhotoCommand) droidInstruction);
        } else {
            return null;
        }
        return null;
    }
}
