/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for stop video command
 */
@SuppressWarnings("SameParameterValue")
public class StopVideoProcessBlock extends ProcessBlock {
    /**
     * Create a new stop video process block
     * @param timeoutInterval the timeout interval
     */
    public StopVideoProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Stop Video");
    }
}
