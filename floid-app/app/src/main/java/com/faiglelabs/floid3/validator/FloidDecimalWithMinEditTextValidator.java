/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

import android.widget.EditText;

/**
 * Decimal with min edit text validator
 */
public class FloidDecimalWithMinEditTextValidator extends FloidDecimalEditTextValidator {
    private final EditText mMinValueEditText;

    /**
     * Create a new decimal with min edit text validator
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the validator callback
     * @param useMinValue                    true if use min value
     * @param minValue                       the min value
     * @param useMaxValue                    true if use max value
     * @param maxValue                       the max value
     * @param minValueEditText               the min value edit text
     */
    public FloidDecimalWithMinEditTextValidator(EditText editText,
                                                FloidEditTextValidatorCallback floidEditTextValidatorCallback,
                                                @SuppressWarnings("SameParameterValue") boolean useMinValue,
                                                double minValue,
                                                @SuppressWarnings("SameParameterValue") boolean useMaxValue,
                                                double maxValue,
                                                EditText minValueEditText) {
        super(editText,
                floidEditTextValidatorCallback,
                useMinValue,
                minValue,
                useMaxValue,
                maxValue);
        mMinValueEditText = minValueEditText;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        double requiredMin;
        double myValue;
        try {
            requiredMin = Double.valueOf(mMinValueEditText.getText().toString());
        } catch (Exception e) {
            mMinValueEditText.setError("Bad Format");
            return;
        }
        try {
            myValue = Double.valueOf(mEditText.getText().toString());
        } catch (Exception e) {
            mEditText.setError("Bad Format");
            return;
        }
        if (myValue < requiredMin) {
            mEditText.setError("Below Min");
            return;
        }
        super.onTextChanged(s, start, before, count);
    }
}
