/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Delay command process block
 */
public class RetrieveLogsProcessBlock extends ProcessBlock
{
    /**
     * Create a new retrieve logs process block
     * @param timeoutInterval the timeout interval
     */
    public RetrieveLogsProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Retrieve Logs");
    }
 }
