/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import com.faiglelabs.floid.servertypes.commands.StartVideoCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.StartVideoProcessBlock;

/**
 * Command processor for start video command
 */
public class StartVideoCommandProcessor extends InstructionProcessor
{
	/**
	 * Create a start video command processor
	 * @param floidService the floid service
	 * @param startVideoCommand the start video command
	 */
	@SuppressWarnings("WeakerAccess")
	public StartVideoCommandProcessor(FloidService floidService, StartVideoCommand startVideoCommand)
	{
		super(floidService, startVideoCommand);
		useDefaultGoalProcessing = true;
		completeOnCommandResponse = false;
		setProcessBlock(new StartVideoProcessBlock(defaultGoalTimeout));
	}
	@Override
	public boolean setupInstruction()
	{
	    // General idea:
	    // Set up to take photo - turn on isTakingPhoto and then turn it off it failure or if picture is taken in callback...
	    // Create file name here based on missionNumber_dateTime_photoNumber
	    super.setupInstruction();
        if(floidService.getFloidDroidStatus().isTakingVideo())
        {
            // Logic error - can't take video if already taking video
            sendCommandLogicErrorMessage("Already taking video.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
        }
	    if(floidService.getFloidDroidStatus().isPhotoStrobe())
	    {
            // Logic error - can't take video if already in photoStrobe
            sendCommandLogicErrorMessage("Already taking photo strobe.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
	    }
        if(floidService.getFloidDroidStatus().isTakingPhoto())
        {
            // Logic error - camera can only do one thing or the other
            sendCommandLogicErrorMessage("Already taking photo.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
        }
        floidService.serviceSendStartVideo();
        return true;    // We are done with this whole command
	}
	@Override
	public boolean processInstruction()
	{
	    if(super.processInstruction())
	    {
	        return true;
	    }
	    return true;   // We are done with this command anyway...
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
