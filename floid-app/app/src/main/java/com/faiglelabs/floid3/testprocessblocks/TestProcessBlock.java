/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.testprocessblocks;

import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Process block for test command
 */
@SuppressWarnings("unused")
class TestProcessBlock extends ProcessBlock {
    private boolean testOn;
    /**
     * Create a new test process block
     * @param timeoutInterval the timeout interval
     */
    public TestProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Test");
    }

    /**
     * Set the test on
     * @param testOn true if test on
     */
    public void setTestOn(boolean testOn) {
        this.testOn = testOn;
    }

    /**
     * Get the test on
     * @return true if test on
     */
    public boolean getTestOn() {
        return testOn;
    }
}
