/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.helper;

import android.widget.Button;
import android.widget.EditText;

/**
 * Holds an edit state for a Droid Parameter
 */
public class DroidParameterEditState {
    private EditText editText;
    private Button editButton;
    private boolean dirty;
    private String originalValue;
    private boolean editing;
    private String droidParameter;

    /**
     * Instantiates a new Droid parameter edit state.
     *
     * @param droidParameter the droid parameter
     * @param editButton     the edit button
     * @param editText       the edit text
     * @param dirty          the dirty
     * @param originalValue  the original value
     */
    public DroidParameterEditState(String droidParameter, Button editButton, EditText editText, @SuppressWarnings("SameParameterValue") boolean dirty, String originalValue) {
        this.editButton = editButton;
        this.editText = editText;
        this.dirty = dirty;
        this.originalValue = originalValue;
        this.editing = false;
        this.droidParameter = droidParameter;
    }

    /**
     * Gets edit button.
     *
     * @return the edit button
     */
    public Button getEditButton() {
        return editButton;
    }

    /**
     * Gets edit text.
     *
     * @return the edit text
     */
    public EditText getEditText() {
        return editText;
    }

    /**
     * Sets edit text.
     *
     * @param editText the edit text
     */
    @SuppressWarnings("unused")
    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    /**
     * Is dirty boolean.
     *
     * @return the boolean
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * Sets dirty.
     *
     * @param dirty the dirty
     */
    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    /**
     * Gets original value.
     *
     * @return the original value
     */
    public String getOriginalValue() {
        return originalValue;
    }

    /**
     * Sets original value.
     *
     * @param originalValue the original value
     */
    @SuppressWarnings("unused")
    public void setOriginalValue(String originalValue) {
        this.originalValue = originalValue;
    }

    /**
     * Sets edit button.
     *
     * @param editButton the edit button
     */
    @SuppressWarnings("unused")
    public void setEditButton(Button editButton) {
        this.editButton = editButton;
    }

    /**
     * Is editing boolean.
     *
     * @return the boolean
     */
    public boolean isEditing() {
        return editing;
    }

    /**
     * Sets editing.
     *
     * @param editing the editing
     */
    public void setEditing(boolean editing) {
        this.editing = editing;
    }

    /**
     * Gets droid parameter.
     *
     * @return the droid parameter
     */
    public String getDroidParameter() {
        return droidParameter;
    }

    /**
     * Sets droid parameter.
     *
     * @param droidParameter the droid parameter
     */
    @SuppressWarnings("unused")
    public void setDroidParameter(String droidParameter) {
        this.droidParameter = droidParameter;
    }
}

