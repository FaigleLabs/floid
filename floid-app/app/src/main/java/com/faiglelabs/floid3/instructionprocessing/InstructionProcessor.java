/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;
import com.faiglelabs.floid.servertypes.commands.DroidCommand;
import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid3.FloidCommandResponse;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * The type Instruction processor.
 */
@SuppressWarnings("SameParameterValue")
public abstract class InstructionProcessor
{
    /**
     * The Floid service.
     */
    FloidService                       floidService;
    /**
     * The Droid instruction.
     */
    DroidInstruction                   droidInstruction;
    /**
     * The Process block.
     */
    ProcessBlock                       processBlock;
    /**
     * The Use default goal processing.
     */
    boolean                            useDefaultGoalProcessing      = true;  // Defaults to using default goal processing
    /**
     * The Complete on command response.
     */
    boolean                            completeOnCommandResponse     = true;  // Defaults to success state if good command response received - change if you want to continue to process
    /**
     * The Good command send.
     */
    @SuppressWarnings("WeakerAccess")
    protected    boolean               goodCommandSend               = false;
    /**
     * The No goal timeout.
     */
    final static long                  noGoalTimeout                 = 2000L;
    /**
     * The Default goal timeout.
     */
    final static long                  defaultGoalTimeout            = 20000L;
    /**
     * The Default long goal timeout.
     */
    final static long                  defaultLongGoalTimeout        = 120000L;

    /**
     * The stop mission flag
     */
    boolean                            stopMission                   = true;

    /**
     * Instantiates a new Instruction processor.
     *
     * @param floidService     the floid service
     * @param droidInstruction the droid instruction
     */
    InstructionProcessor(FloidService floidService, DroidInstruction droidInstruction)
	{
        if(floidService.mUseLogger) Log.i(FloidService.TAG, "New Instruction: " + droidInstruction.getType());
        this.floidService = floidService;
        this.droidInstruction  = droidInstruction;
		setProcessBlock(null);
	}

    /**
     * Sets floid service.
     *
     * @param floidService the floid service
     */
    @SuppressWarnings("unused")
	public void setFloidService(FloidService floidService)
	{
		this.floidService = floidService;
	}

    /**
     * Gets floid service.
     *
     * @return the floid service
     */
    @SuppressWarnings("unused")
	public FloidService getFloidService()
	{
		return floidService;
	}

    /**
     * Sets droid instruction.
     *
     * @param droidInstruction the droid instruction
     */
    @SuppressWarnings("unused")
    public void setDroidInstruction(DroidCommand droidInstruction)
	{
		this.droidInstruction   = droidInstruction;
	}

    /**
     * Gets droid instruction.
     *
     * @return the droid instruction
     */
    public DroidInstruction getDroidInstruction()
	{
		return droidInstruction;
	}

    /**
     * Sets process block.
     *
     * @param processBlock the process block
     */
    void setProcessBlock(ProcessBlock processBlock)
	{
		this.processBlock = processBlock;
	}

    /**
     * Gets process block.
     *
     * @return the process block
     */
    public ProcessBlock getProcessBlock()
	{
		return processBlock;
	}

    /**
     * Sets instruction.
     *
     * @return true if instruction complete
     */
    @SuppressWarnings("UnusedReturnValue")
    public boolean setupInstruction()
	{
        if(floidService.mUseLogger) Log.i(FloidService.TAG, "Setup Instruction: Default setup");
	    processBlock.setProcessBlockProcessCount(0);
	    processBlock.setProcessBlockStartTime(System.currentTimeMillis());
	    processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_NOT_STARTED);
	    return false;  // Default is that this command is not yet finished - ultimately wrongly implemented commands should timeout
	}

    /**
     * Process instruction boolean.
     *
     * @return true if instruction complete
     */
    public boolean processInstruction() {
//        if (floidService.mUseLogger) Log.d(FloidService.TAG, "InstructionProcessor: processInstruction");
        processBlock.increaseProcessBlockProcessCount();
        // Anything less than completed success is not finished...
        if (processBlock.getProcessBlockStatus() < ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS) {
            // Are we waiting for a command response?
            if (processBlock.getProcessBlockStatus() == ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE) {
                if (floidService.mUseLogger)
                    Log.d(FloidService.TAG, "Waiting on command response: " + processBlock.getProcessBlockWaitForCommandNumber());
                // Check to see if we have received a response with the command number - do not clear the queue so other potentially async commands can find their responses
                FloidCommandResponse floidCommandResponse = getFloidCommandResponse(processBlock.getProcessBlockWaitForCommandNumber(), false);
                if (floidCommandResponse != null) {
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Found matching command response for: " + processBlock.getInstructionName());
                    if (floidService.mUseLogger) Log.d(FloidService.TAG, "Got a command response: ");
                    switch (floidCommandResponse.getCommandStatus()) {
                        case FloidService.COMMAND_RESPONSE_OK: {
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Command response: OK");
                            if (floidService.mUseLogger) Log.d(FloidService.TAG, "  Response was: OK ");
                            if (completeOnCommandResponse) {
                                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Command completed due to OK response");
                                if (floidService.mUseLogger) Log.d(FloidService.TAG, "  Command completed due to OK response - note, not checking command number [TODO] ");
                                // This flag tells us that we change to a completion state if we get a command response:
                                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                            } else {
                                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Command does not complete yet");
                                if (floidService.mUseLogger) Log.d(FloidService.TAG, "  Command does not complete yet");
                                if (useDefaultGoalProcessing) {
                                    if (floidService.mUseLogger)
                                        Log.d(FloidService.TAG, "  [DEFAULT GOAL PROCESSING] Moving command to in progress");
                                    // Otherwise we flag that we got the command response so we can continue default goal processing:
                                    processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
                                } else {
                                    if (floidService.mUseLogger)
                                        Log.d(FloidService.TAG, "  [NOT DEFAULT GOAL PROCESSING] Moving command to command response received");
                                    // Otherwise we flag that we got the command response so the non-default processor can continue processing:
                                    processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMMAND_RESPONSE_RECEIVED);
                                }
                            }
                        }
                        break;
                        case FloidService.COMMAND_RESPONSE_ERROR: {
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Command response: ERROR");
                            if (floidService.mUseLogger) Log.d(FloidService.TAG, "  Response was: ERROR ");
                            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                        }
                        break;
                        case FloidService.COMMAND_RESPONSE_LOGIC_ERROR: {
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Command response: LOGIC ERROR");
                            if (floidService.mUseLogger) Log.d(FloidService.TAG, "  Response was: LOGIC ERROR ");
                            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_LOGIC_ERROR);
                        }
                        break;
                        case FloidService.COMMAND_RESPONSE_COMM_ERROR: {
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Command response: COMMUNICATION ERROR");
                            if (floidService.mUseLogger) Log.d(FloidService.TAG, "  Response was: COMMUNICATION ERROR ");
                            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_COMMUNICATION_ERROR);
                        }
                        break;
                        default: {
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Command response: UNKNOWN");
                            // Logic Error:
                            if (floidService.mUseLogger) Log.d(FloidService.TAG, "  Response was: [UNKNOWN: " + floidCommandResponse.getCommandStatus() + "] ");
                            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                        }
                        break;
                    }
                }
            } else {
                if (useDefaultGoalProcessing) {
                    try {
                        // Do we have a goal?
                        if (processBlock.getFloidGoalId() >= 0) {
                            if (floidService.mUseLogger)
                                Log.i(FloidService.TAG, "  we have a goal id: " + processBlock.getFloidGoalId());
                            // Does this goal Id match the one in the floid status (i.e. we handle these goals automatically - others need to be handled by overriding this method
                            // If the command wants to process the goal then it needs to override this method
                            // Does it have a goal, does it match our id and if so does it have a completed state?
                            // Do we currently have a goal?
                            if (floidService.getFloidStatus().isGhg()) {
                                if (floidService.mUseLogger) Log.d(FloidService.TAG, "Instruction Processor: Has Goal");
                                // Get the goal id:
                                if (floidService.getFloidStatus().getGid() == processBlock.getFloidGoalId()) {
                                    if (floidService.mUseLogger) Log.d(FloidService.TAG, "Instruction Processor:  Gid == floidGoalId() " + processBlock.getFloidGoalId());
                                    // Get the goal status:
                                    switch (floidService.getFloidStatus().getGs()) {
                                        case FloidService.GOAL_STATE_COMPLETE_SUCCESS: {
                                            if (floidService.mUseLogger) Log.i(FloidService.TAG, "Instruction Processor: goal completed success");
                                            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Goal completed success");
                                            return true;
                                        }
                                        case FloidService.GOAL_STATE_COMPLETE_ERROR: {
                                            if (floidService.mUseLogger) Log.i(FloidService.TAG, "Instruction Processor: goal completed error");
                                            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Goal completed error");
                                            return true;
                                        }
                                        default: {
                                            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_TO_ACHIEVE_GOAL);
                                            // Not completed!
                                            return false;
                                        }
                                    }
                                } else {
                                    // Goal ID does not match - we need to wait so just fail through
                                    processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_MATCHING_GOAL_ID);
                                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Waiting for matching goal id: " + processBlock.getFloidGoalId());
                                }
                            } else {
                                // Floid does not yet say it has a goal:
                                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_FLOID_GOAL);
                                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Waiting for floid to have a goal");
                            }
                        } else {
                            // We must have completed, since we are default processing but have no goal and are not waiting on a command response
                            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: No goal and not waiting on a command processor -> Completed Success");
                            return true;
                        }
                    } catch (Exception e) {
                        if (floidService.mUseLogger) Log.e(FloidService.TAG, "Instruction Processor: caught exception [" + e.getStackTrace()[0].getFileName() + ":" + e.getStackTrace()[0].getLineNumber() + "]", e);
                        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Caught exception " + e.getMessage());
                        return true;

                    }
                }
            }
            if (processBlock.isTimedOut()) {
                if (floidService.mUseLogger) Log.i(FloidService.TAG, "Instruction Processor: process block timed out after " + processBlock.getProcessBlockTimeoutInterval() + " millis");
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_TIMEOUT_ERROR);
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Process block timed out after " + processBlock.getProcessBlockTimeoutInterval() + " millis");
                return true;
            }
            if (processBlock.getProcessBlockStatus() == ProcessBlock.PROCESS_BLOCK_NOT_STARTED) {
                if (floidService.mUseLogger) Log.i(FloidService.TAG, "Instruction Processor: Process Block -> Started");
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_STARTED);
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor: Process Block -> Started");
            }
            return false;  // Not done
        } else {
            return true;   // It is already done
        }
    }

    /**
     * Gets floid command response.
     *
     * @param commandNumber the command number
     * @param clearQueue whether to clear the command queue - typically not
     * @return the floid command response
     */
    private FloidCommandResponse getFloidCommandResponse(int commandNumber, boolean clearQueue) {
        FloidCommandResponse floidCommandResponse = null;
        boolean gotResponse = false;
        int responseIndex = 0;
        synchronized (floidService.mFloidCommandResponseSynchronizer) {
            for (int i = 0; i < floidService.mFloidCommandResponses.size(); ++i) {
                if (floidService.mFloidCommandResponses.get(i).getCommandNumber() == commandNumber) {
                    gotResponse = true;
                    responseIndex = i;
                    break;
                }
            }
            // Remove our response found:
            if (gotResponse) {
                floidCommandResponse = floidService.mFloidCommandResponses.get(responseIndex);
                floidService.mFloidCommandResponses.remove(responseIndex);
            }
            // Clear queue if told to: (typically not - might clear async command responses)
            if (clearQueue) {
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Clearing command response queue");
                floidService.mFloidCommandResponses.clear();
            }
        }
        return floidCommandResponse;
    }

    /**
     * Tear down instruction boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings({"UnusedReturnValue", "SameReturnValue"})
    public boolean tearDownInstruction()
	{
        if(floidService.mUseLogger) Log.d(FloidService.TAG, "Default tear down");
        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Default instruction tear down");
        // Defaults to returning true - e.g. does nothing after the setup - for many commands this is the case
        return true;
	}

    /**
     * Sets use default goal processing.
     *
     * @param useDefaultGoalProcessing the use default goal processing
     */
    @SuppressWarnings("unused")
    public void setUseDefaultGoalProcessing(boolean useDefaultGoalProcessing)
    {
        this.useDefaultGoalProcessing = useDefaultGoalProcessing;
    }

    /**
     * Is use default goal processing boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isUseDefaultGoalProcessing()
    {
        return useDefaultGoalProcessing;
    }

    /**
     * Sets good command send.
     *
     * @param goodCommandSend the good command send
     */
    @SuppressWarnings("unused")
    public void setGoodCommandSend(boolean goodCommandSend)
    {
        this.goodCommandSend = goodCommandSend;
    }

    /**
     * Is good command send boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isGoodCommandSend()
    {
        return goodCommandSend;
    }

    /**
     * Send command logic error message.
     *
     * @param message the message
     */
    void sendCommandLogicErrorMessage(String message)
    {
        // Get the tag and the message and add it to the debug message queue:
        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction Processor Logic Error: " + message);
    }

    /**
     * Is complete on command response boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("unused")
    public boolean isCompleteOnCommandResponse()
    {
        return completeOnCommandResponse;
    }

    /**
     * Sets complete on command response.
     *
     * @param completeOnCommandResponse the complete on command response
     */
    @SuppressWarnings("unused")
    public void setCompleteOnCommandResponse(boolean completeOnCommandResponse)
    {
        this.completeOnCommandResponse = completeOnCommandResponse;
    }

    /**
     * This is only called if the instruction errors to see if the mission should exit
     * Thus it is called post process of the instruction
     * There are three ways to handle this - set to either true or false depending if
     * this instruction is mission critical, adjust the stopMission value as the instruction processes
     * (default is stops mission if this instruction errors) or override this and you can
     * also check the status of the process block when determining
     * @return whether the mission should exit
     */
    public boolean isStopMission() {
        return stopMission;
    }
}
