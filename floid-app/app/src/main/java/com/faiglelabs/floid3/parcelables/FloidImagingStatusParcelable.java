package com.faiglelabs.floid3.parcelables;

import android.os.Parcel;
import android.os.Parcelable;
import com.faiglelabs.floid.servertypes.statuses.FloidImagingStatus;

import java.io.File;

/**
 * Parcelable version of floid imaging status
 */
public class FloidImagingStatusParcelable extends FloidImagingStatus implements Parcelable {

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.mStatusInitialized ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusTakingPhoto ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusPhotoCameraIdDirty ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusPhotoCameraPropertiesDirty ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusPhotoCameraOpen ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusPhotoSurfacesSetup ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusPhotoCaptureRequestSetup ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusPhotoCameraCaptureSessionConfigured ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusTakingVideo ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusTakingVideoSnapshot ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusVideoCameraIdDirty ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusVideoCameraPropertiesDirty ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusVideoCameraOpen ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusVideoSurfacesSetup ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusSupportsVideoSnapshot ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusVideoCaptureRequestSetup ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mStatusVideoCameraCaptureSessionConfigured ? (byte) 1 : (byte) 0);
        dest.writeInt(this.mNumberOfRemainingCaptureSessionsToCreate);
        dest.writeSerializable(this.mPhotoVideoStoragePath);
        dest.writeString(this.mPhotoCameraId);
        dest.writeString(this.mVideoCameraId);
        dest.writeInt(this.mPhotoState);
        dest.writeByte(this.mFlashSupported ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mHardwareIsLegacy ? (byte) 1 : (byte) 0);
    }

    /**
     * Parcelable version of FloidImagingStatus
     */
    public FloidImagingStatusParcelable() {
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected FloidImagingStatusParcelable(Parcel in) {
        this.mStatusInitialized = in.readByte() != 0;
        this.mStatusTakingPhoto = in.readByte() != 0;
        this.mStatusPhotoCameraIdDirty = in.readByte() != 0;
        this.mStatusPhotoCameraPropertiesDirty = in.readByte() != 0;
        this.mStatusPhotoCameraOpen = in.readByte() != 0;
        this.mStatusPhotoSurfacesSetup = in.readByte() != 0;
        this.mStatusPhotoCaptureRequestSetup = in.readByte() != 0;
        this.mStatusPhotoCameraCaptureSessionConfigured = in.readByte() != 0;
        this.mStatusTakingVideo = in.readByte() != 0;
        this.mStatusTakingVideoSnapshot = in.readByte() != 0;
        this.mStatusVideoCameraIdDirty = in.readByte() != 0;
        this.mStatusVideoCameraPropertiesDirty = in.readByte() != 0;
        this.mStatusVideoCameraOpen = in.readByte() != 0;
        this.mStatusVideoSurfacesSetup = in.readByte() != 0;
        this.mStatusSupportsVideoSnapshot = in.readByte() != 0;
        this.mStatusVideoCaptureRequestSetup = in.readByte() != 0;
        this.mStatusVideoCameraCaptureSessionConfigured = in.readByte() != 0;
        this.mNumberOfRemainingCaptureSessionsToCreate = in.readInt();
        this.mPhotoVideoStoragePath = (File) in.readSerializable();
        this.mPhotoCameraId = in.readString();
        this.mVideoCameraId = in.readString();
        this.mPhotoState = in.readInt();
        this.mFlashSupported = in.readByte() != 0;
        this.mHardwareIsLegacy = in.readByte() != 0;
    }

    /**
     * The creator
     */
    public static final Parcelable.Creator<FloidImagingStatusParcelable> CREATOR = new Parcelable.Creator<FloidImagingStatusParcelable>() {
        @Override
        public FloidImagingStatusParcelable createFromParcel(Parcel source) {
            return new FloidImagingStatusParcelable(source);
        }

        @Override
        public FloidImagingStatusParcelable[] newArray(int size) {
            return new FloidImagingStatusParcelable[size];
        }
    };
}
