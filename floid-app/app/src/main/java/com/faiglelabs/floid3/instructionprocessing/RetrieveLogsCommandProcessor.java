/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;
import com.faiglelabs.floid.servertypes.commands.RetrieveLogsCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.RetrieveLogsProcessBlock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

/**
 * Command processor for the retrieve logs command
 */
public class RetrieveLogsCommandProcessor extends InstructionProcessor
{
	private static final String TAG = "RetrieveLogsCommandProcessor";

	/**
     * Create a retrieve logs command processor
     * @param floidService the floid service
     * @param retrieveLogsCommand the retrieve logs command
     */
    @SuppressWarnings("WeakerAccess")
	public RetrieveLogsCommandProcessor(FloidService floidService, RetrieveLogsCommand retrieveLogsCommand)
	{
		super(floidService, retrieveLogsCommand);
		useDefaultGoalProcessing = false;
		completeOnCommandResponse = false;
		setProcessBlock(new RetrieveLogsProcessBlock(FloidService.COMMAND_NO_TIMEOUT));   // This command does not time out
	}
	@Override
	public boolean setupInstruction()
	{
        super.setupInstruction();  // Sets processBlock to started
        return false;
	}

	@Override
	public boolean processInstruction()
	{
	    if(super.processInstruction()) {
	        return true;   // we are already done...
	    }
		// Get our command and process block:
		RetrieveLogsProcessBlock retrieveLogsProcessBlock = (RetrieveLogsProcessBlock) processBlock;
		RetrieveLogsCommand retrieveLogsCommand = (RetrieveLogsCommand) droidInstruction;
		try {
			// Perform the log retrieval:
			retrieveLogs(retrieveLogsCommand.getLogLimit());
			// Set success:
			retrieveLogsProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
		} catch (Exception e) {
			Log.e(FloidService.TAG, "Exception retrieving logs.", e);
			retrieveLogsProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
		}
		// We are done:
		return true;
	}

	private void retrieveLogs(int logLimit) {
		final int floidServicePid = (floidService!=null)?floidService.getFloidServicePid():0;
		BufferedReader bufferedReader = null;
		try {
			String logcatCommand = "logcat *:V -d -t " + (logLimit>0?logLimit:200) + " -v process";
			if(floidServicePid != 0) {
				logcatCommand = String.format(Locale.getDefault(), "%s --pid=%d", logcatCommand, floidServicePid);
			}
			Process process = Runtime.getRuntime().exec(logcatCommand);
			bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			StringBuilder log = new StringBuilder();
			String line;
			String pidString = "(" + floidServicePid + ")";
			if(pidString.length()<7) {
				pidString = "( " + floidServicePid + ")";
			}
			String errorPidString = "E" + pidString;
			String warningPidString = "W" + pidString;
			String debugPidString = "D" + pidString;
			String infoPidString = "I" + pidString;
			String verbosePidString = "V" + pidString;
			String floidDroidRunnableString = "(FloidDroidRunnable)";
			String clientCommunicatorString = "(ClientCommunicator)";
			String floidServiceString = "(FloidService)";
			String networkSecurityConfigString = "(NetworkSecurityConfig)";
			String d3FloidServicString = "(d3:floidServic)";
			String textToSpeechString = "(TextToSpeech)";
			while ((line = bufferedReader.readLine()) != null) {
				line = line.replace(errorPidString, "")
						.replace(warningPidString, "")
						.replace(infoPidString, "")
						.replace(debugPidString, "")
						.replace(verbosePidString, "")
						.replace(floidDroidRunnableString, "")
						.replace(clientCommunicatorString, "")
						.replace(floidServiceString, "")
						.replace(networkSecurityConfigString, "")
						.replace(d3FloidServicString, "")
						.replace(textToSpeechString, "");
				log.append(line).append("\n");
			}
			bufferedReader.close();
			bufferedReader = null;
			// Add this log to the queue so that it gets processed:
			floidService.addSystemLogMessage(log.toString());
		} catch (IOException e) {
			Log.e(TAG, "Exception retrieving logs", e);
		}
		if(bufferedReader!=null) {
			try {
				bufferedReader.close();
			} catch (Exception e) {
				Log.e(TAG, "Exception closing buffered reader for login", e);
			}
		}
	}
	@Override
	public boolean tearDownInstruction() {
		super.tearDownInstruction();
		return true;
	}
}
