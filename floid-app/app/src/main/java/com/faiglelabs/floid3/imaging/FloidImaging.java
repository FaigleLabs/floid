/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.imaging;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.camera2.*;
import android.hardware.camera2.params.OutputConfiguration;
import android.hardware.camera2.params.SessionConfiguration;
import android.location.Location;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Surface;
import androidx.annotation.NonNull;
import com.faiglelabs.floid.servertypes.statuses.FloidImagingStatus;
import com.faiglelabs.floid3.DroidValidParameters;
import com.faiglelabs.floid3.FloidParameterModifiedHandler;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.parcelables.FloidImagingStatusParcelable;
import com.faiglelabs.floid3.utilities.ThreadPerTaskExecutor;
import com.google.android.renderscript.Toolkit;
import com.google.android.renderscript.YuvFormat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Semaphore;

/**
 * Imaging controller - handles camera - video streaming, recording, preview, photo snapshot and video snapshot
 */
public class FloidImaging {
    // Our tag for debugging:
    /**
     * Debug tag
     */
    private static final String TAG = "FloidImaging";
    // Parameters:
    /**
     * The valid imaging parameter values
     */
    private static final HashMap<String, DroidValidParameters> mImagingParametersValidValues = new HashMap<>();
    /**
     * The default imaging parameter values
     */
    private static final HashMap<String, String> mImagingDefaultParametersHashMap = new HashMap<>();
    /**
     * Get the imaging status
     * @return the imaging status
     */
    public FloidImagingStatusParcelable getFloidImagingStatus() {
        return mFloidImagingStatus;
    }
    /**
     * Get the camera info list
     * @return the camera info list
     */
    public ArrayList<FloidCameraInfo> getCameraInfoList() {
        return mCameraInfoList;
    }

    private final FloidImagingStatusParcelable mFloidImagingStatus = new FloidImagingStatusParcelable();
    // Status:
    // Photo:
    private List<Surface> mPhotoSurfaces = null;
    private List<Surface> mVideoSurfaces = null;
    // Photo & also is photo preview reader and surface:
    private ImageReader mPhotoImageReader; // For getting high-quality still images
    private Surface mPhotoImageReaderSurface;
    // Video Media Recorder:
    private MediaRecorder mVideoMediaRecorder;  // For capturing video
    private Surface mVideoMediaRecorderSurface;
    private String mVideoMediaRecorderFileName = "";
    // Video Streaming Image:
    private ImageReader mVideoStreamingImageReader; // For getting images to send back to the front-end
    private long mVideoStreamingImageReaderLastPreviewMillis = 0L;
    private long mVideoStreamFramePeriod = VIDEO_DEFAULT_STREAM_FRAME_PERIOD;
    private Surface mVideoStreamingImageReaderSurface;
    // Video Snapshot Image:
    private ImageReader mVideoSnapshotImageReader;
    private Surface mVideoSnapshotImageReaderSurface;

    private final static String FLOID_IMAGING_DIRECTORY_NAME = "floid";
    private final static String FLOID_IMAGING_DIRECTORY_PREFIX = "Floid-";
    private final static String FLOID_FILE_DATE_FORMAT = "yyyy-MM-dd_HH_mm_ssSSS";
    private String mImagingDirectoryPath;
    private static final String VIDEO_FILE_EXTENSION_3GPP = ".3gpp";
    private static final String VIDEO_FILE_EXTENSION_MP4 = ".mp4";
    private static final String VIDEO_FILE_EXTENSION_DEFAULT = VIDEO_FILE_EXTENSION_3GPP;
    /**
     * The floid service.
     */
    private final FloidService mFloidService;
    /**
     * The context.
     */
    private final Context mContext;
    /**
     * The floid id.
     */
    private int mFloidId;
    /**
     * Default video frame rate
     */
    @SuppressWarnings("WeakerAccess")
    static final int VIDEO_DEFAULT_FRAME_RATE = 30;
    /**
     * Default video preview frame rate
     */
    @SuppressWarnings("WeakerAccess")
    static final int VIDEO_DEFAULT_STREAM_FRAME_PERIOD = 5000;
    /**
     * Default video max frame rate
     */
    static final int VIDEO_DEFAULT_MAX_FRAME_RATE = 60;

    // Photo and video preview image widths:
    private final static int PHOTO_PREVIEW_WIDTH = 320;
    private final static int PHOTO_PREVIEW_HEIGHT = 240;
    private final static int VIDEO_FRAME_PREVIEW_WIDTH = 320;
    private final static int VIDEO_FRAME_PREVIEW_HEIGHT = 240;
    /**
     * The capture session for the photo camera which may be the same as video or not
     */
    private CameraCaptureSession mPhotoCameraCaptureSession;
    /**
     * The capture request builder is specific to the photo capture session
     * will be non-repeating and capture a high-res photo while not recording
     */
    private CaptureRequest.Builder mPhotoCaptureRequestBuilder;
    /**
     * The capture request builder is specific to the photo capture session during preview
     * will be repeating and used to obtain focus and exposure locks for the Photo Camera
     */
    private CaptureRequest.Builder mPhotoPreviewCaptureRequestBuilder;
    /**
     * The capture session for the video camera which may be the same as photo or not
     */
    private CameraCaptureSession mVideoCameraCaptureSession;
    /**
     * The capture request builder is for the video streaming (record and preview):
     * will be repeating and have two surfaces: 1) MediaRecord 2) Image Reader callback for 'preview' to server/Front-end
     */
    private CaptureRequest.Builder mVideoStreamingCaptureRequestBuilder;
    /**
     * The capture request builder is for the video snapshot capture session:
     * will be non-repeating and capture to an ImageReader surface while the video is recording
     */
    private CaptureRequest.Builder mVideoSnapshotCaptureRequestBuilder;
    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;
    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;
    /**
     * A reference to the opened {@link android.hardware.camera2.CameraDevice}.
     */
    private CameraDevice mVideoCameraDevice;
    /**
     * A reference to the opened {@link android.hardware.camera2.CameraDevice}.
     */
    private CameraDevice mPhotoCameraDevice;
    /**
     * A {@link Semaphore} to prevent the app from exiting before closing the camera.
     */
    private final Semaphore mCameraOpenCloseLock = new Semaphore(1);
    /**
     * The camera manager
     */
    private final CameraManager mCameraManager;
    /**
     * The camera info list
     */
    private final ArrayList<FloidCameraInfo> mCameraInfoList = new ArrayList<>();
    /**
     * The 1080 video size:
     */
    static final int IMAGE_SIZE_1080 = 1080;
    /**
     * The 640 video size:
     */
    static final int IMAGE_SIZE_640 = 640;
    /**
     * The 480 video size:
     */
    private static final int IMAGE_SIZE_480 = 480;
    /**
     * The 1088 video size:
     */
    static final int IMAGE_SIZE_1088 = 1088;
    /**
     * Highest quality video (100)
     */
    private static final int VIDEO_QUALITY_100 = 100;
    /**
     * The constant DROID_PARAMETERS_PHOTO_CAMERA_OPTION.
     */
    private static final String DROID_PARAMETERS_PHOTO_CAMERA_OPTION = "photo.camera";
    /**
     * The photo camera parameter valid strings
     */
    private static final ArrayList<String> mDroidParametersPhotoCameraValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_PHOTO_CAMERA_DEFAULT.
     */
    private static final String DROID_PARAMETERS_PHOTO_CAMERA_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_PHOTO_CAMERA_FRONT.
     */
    private static final String DROID_PARAMETERS_PHOTO_CAMERA_FRONT = "FRONT";
    /**
     * The constant DROID_PARAMETERS_PHOTO_CAMERA_BACK.
     */
    private static final String DROID_PARAMETERS_PHOTO_CAMERA_BACK = "BACK";
    /**
     * The constant DROID_PARAMETERS_PHOTO_CAMERA_EXTERNAL.
     */
    private static final String DROID_PARAMETERS_PHOTO_CAMERA_EXTERNAL = "EXTERNAL";

    @SuppressLint("SimpleDateFormat")
    private final SimpleDateFormat imagingSimpleDateFormat = new SimpleDateFormat(FLOID_FILE_DATE_FORMAT);

    /*
     * Photo camera parameters
     */
    static {
        mDroidParametersPhotoCameraValidStrings.add(DROID_PARAMETERS_PHOTO_CAMERA_DEFAULT);
        mDroidParametersPhotoCameraValidStrings.add(DROID_PARAMETERS_PHOTO_CAMERA_FRONT);
        mDroidParametersPhotoCameraValidStrings.add(DROID_PARAMETERS_PHOTO_CAMERA_BACK);
        mDroidParametersPhotoCameraValidStrings.add(DROID_PARAMETERS_PHOTO_CAMERA_EXTERNAL);
        mImagingParametersValidValues.put(DROID_PARAMETERS_PHOTO_CAMERA_OPTION,
                new DroidValidParameters(DroidValidParameters.STRING_TYPE,
                        true,
                        true,
                        0,
                        8,
                        0,
                        (float) 0.0,
                        (float) 0.0,
                        (float) 0.0,
                        false,
                        DROID_PARAMETERS_PHOTO_CAMERA_BACK,
                        mDroidParametersPhotoCameraValidStrings,
                        null,
                        null,
                        true));
    }

    /**
     * The constant DROID_PARAMETERS_PHOTO_SIZE_OPTION.
     */
    private static final String DROID_PARAMETERS_PHOTO_SIZE_OPTION = "photo.size";
    /**
     * The constant mDroidParametersPhotoSizeValidStrings.
     */
    private static final ArrayList<String> mDroidParametersPhotoSizeValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_PHOTO_SIZE_DEFAULT.
     */
    private static final String DROID_PARAMETERS_PHOTO_SIZE_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;  // 640x480
    /**
     * The constant DROID_PARAMETERS_PHOTO_SIZE_MAX.
     */
    private static final String DROID_PARAMETERS_PHOTO_SIZE_MAX = "MAX";

    /*
     * The photo size parameters
     */
    static {
        mDroidParametersPhotoSizeValidStrings.add(DROID_PARAMETERS_PHOTO_SIZE_DEFAULT);
        mDroidParametersPhotoSizeValidStrings.add(DROID_PARAMETERS_PHOTO_SIZE_MAX);
        mImagingParametersValidValues.put(DROID_PARAMETERS_PHOTO_SIZE_OPTION,
        new DroidValidParameters(DroidValidParameters.SCREEN_SIZE_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "DEFAULT",
                mDroidParametersPhotoSizeValidStrings,
                null,
                null,
                false));
    }

    /**
     * The constant DROID_PARAMETERS_PHOTO_FORMAT_OPTION.
     */
    private static final String DROID_PARAMETERS_PHOTO_FORMAT_OPTION = "photo.format";
    /**
     * The constant mDroidParametersPhotoFormatValidStrings.
     */
    private static final ArrayList<String> mDroidParametersPhotoFormatValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_PHOTO_FORMAT_DEFAULT.
     */
    private static final String DROID_PARAMETERS_PHOTO_FORMAT_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_PHOTO_FORMAT_JPEG.
     */
    private static final String DROID_PARAMETERS_PHOTO_FORMAT_JPEG = "JPEG";

    /*
     * The photo format parameters
     */
    static {
        mDroidParametersPhotoFormatValidStrings.add(DROID_PARAMETERS_PHOTO_FORMAT_DEFAULT);
        mDroidParametersPhotoFormatValidStrings.add(DROID_PARAMETERS_PHOTO_FORMAT_JPEG);
        mImagingParametersValidValues.put(DROID_PARAMETERS_PHOTO_FORMAT_OPTION, new DroidValidParameters(DroidValidParameters.STRING_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                DROID_PARAMETERS_PHOTO_FORMAT_JPEG,
                mDroidParametersPhotoFormatValidStrings,
                null,
                null,
                false));

    }

    /**
     * The constant DROID_PARAMETERS_PHOTO_QUALITY_OPTION.
     */
    private static final String DROID_PARAMETERS_PHOTO_QUALITY_OPTION = "photo.quality";
    /**
     * The constant mDroidParametersPhotoQualityStrings.
     */
    private static final ArrayList<String> mDroidParametersPhotoQualityStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_PHOTO_QUALITY_DEFAULT.
     */
    private static final String DROID_PARAMETERS_PHOTO_QUALITY_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;

    /*
     * The photo quality parameters
     */
    static {
        mImagingParametersValidValues.put(DROID_PARAMETERS_PHOTO_QUALITY_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                true,
                0,
                100,
                100,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersPhotoQualityStrings,
                null,
                null,
                false));
    }

    /**
     * The constant DROID_PARAMETERS_PHOTO_DISABLE_SHUTTER_SOUND_OPTION.
     */
    private static final String DROID_PARAMETERS_PHOTO_DISABLE_SHUTTER_SOUND_OPTION = "photo.disable_shutter_sound";
    /**
     * The constant DROID_PARAMETERS_PHOTO_DISABLE_SHUTTER_SOUND_DEFAULT.
     */
    private static final String DROID_PARAMETERS_PHOTO_DISABLE_SHUTTER_SOUND_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;  // Leaves it on

    /*
     * The photo shutter sound option
     */
    static {
        mImagingParametersValidValues.put(DROID_PARAMETERS_PHOTO_DISABLE_SHUTTER_SOUND_OPTION, new DroidValidParameters(DroidValidParameters.BOOLEAN_TYPE,
                false,
                false,
                0,
                8,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                null,
                null,
                null,
                true));
    }
    /**
     * The constant DROID_PARAMETERS_VIDEO_CAMERA_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_CAMERA_OPTION = "video.camera";
    /**
     * The constant mDroidParametersVideoCameraValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoCameraValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_CAMERA_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_CAMERA_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_CAMERA_FRONT.
     */
    private static final String DROID_PARAMETERS_VIDEO_CAMERA_FRONT = "FRONT";
    /**
     * The constant DROID_PARAMETERS_VIDEO_CAMERA_BACK.
     */
    private static final String DROID_PARAMETERS_VIDEO_CAMERA_BACK = "BACK";
    /**
     * The constant DROID_PARAMETERS_VIDEO_CAMERA_EXTERNAL.
     */
    private static final String DROID_PARAMETERS_VIDEO_CAMERA_EXTERNAL = "EXTERNAL";

    /*
     * The video camera parameters
     */
    static {
        mDroidParametersVideoCameraValidStrings.add(DROID_PARAMETERS_VIDEO_CAMERA_DEFAULT);
        mDroidParametersVideoCameraValidStrings.add(DROID_PARAMETERS_VIDEO_CAMERA_FRONT);
        mDroidParametersVideoCameraValidStrings.add(DROID_PARAMETERS_VIDEO_CAMERA_BACK);
        mDroidParametersVideoCameraValidStrings.add(DROID_PARAMETERS_VIDEO_CAMERA_EXTERNAL);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_CAMERA_OPTION, new DroidValidParameters(DroidValidParameters.STRING_TYPE,
                true,
                true,
                0,
                8,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                DROID_PARAMETERS_VIDEO_CAMERA_BACK,
                mDroidParametersVideoCameraValidStrings,
                null,
                null,
                true));
    }

    /**
     * The constant DROID_PARAMETERS_VIDEO_SIZE_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_SIZE_OPTION = "video.size";
    /**
     * The constant mDroidParametersVideoSizeValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoSizeValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_SIZE_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_SIZE_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;  // 640x480
    /**
     * The constant DROID_PARAMETERS_VIDEO_SIZE_MAX.
     */
    private static final String DROID_PARAMETERS_VIDEO_SIZE_MAX = "MAX";
    /**
     * The constant DROID_PARAMETERS_VIDEO_SIZE_1080P.
     */
    private static final String DROID_PARAMETERS_VIDEO_SIZE_1080P = "1080P";

    /*
     * The video size parameters
     */
    static {
        mDroidParametersVideoSizeValidStrings.add(DROID_PARAMETERS_VIDEO_SIZE_DEFAULT);
        mDroidParametersVideoSizeValidStrings.add(DROID_PARAMETERS_VIDEO_SIZE_MAX);
        mDroidParametersVideoSizeValidStrings.add(DROID_PARAMETERS_VIDEO_SIZE_1080P);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_SIZE_OPTION, new DroidValidParameters(DroidValidParameters.SCREEN_SIZE_TYPE,
                false,
                false,
                0,
                8,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                DROID_PARAMETERS_VIDEO_SIZE_DEFAULT,
                mDroidParametersVideoSizeValidStrings,
                null,
                null,
                false));
    }

    /**
     * The constant DROID_PARAMETERS_VIDEO_ENCODER_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_ENCODER_OPTION = "video.encoder";
    /**
     * The constant mDroidParametersVideoEncoderValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoEncoderValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_ENCODER_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_ENCODER_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_ENCODER_H264.
     */
    private static final String DROID_PARAMETERS_VIDEO_ENCODER_H264 = "H264";
    /**
     * The constant DROID_PARAMETERS_VIDEO_ENCODER_MPEG_4_SP.
     */
    private static final String DROID_PARAMETERS_VIDEO_ENCODER_MPEG_4_SP = "MPEG_4_SP";

    /*
     * The video encoder parameters
     */
    static {
        mDroidParametersVideoEncoderValidStrings.add(DROID_PARAMETERS_VIDEO_ENCODER_DEFAULT);
        mDroidParametersVideoEncoderValidStrings.add(DROID_PARAMETERS_VIDEO_ENCODER_H264);
        mDroidParametersVideoEncoderValidStrings.add(DROID_PARAMETERS_VIDEO_ENCODER_MPEG_4_SP);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_ENCODER_OPTION, new DroidValidParameters(DroidValidParameters.STRING_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                DROID_PARAMETERS_VIDEO_ENCODER_DEFAULT,
                mDroidParametersVideoEncoderValidStrings,
                null,
                null,
                true));
    }

    /**
     * The constant DROID_PARAMETERS_VIDEO_FRAME_RATE_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_FRAME_RATE_OPTION = "video.frame_rate";
    /**
     * The constant mDroidParametersVideoFrameRateValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoFrameRateValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_FRAME_RATE_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_FRAME_RATE_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_FRAME_RATE_MAX.
     */
    private static final String DROID_PARAMETERS_VIDEO_FRAME_RATE_MAX = "MAX";

    /*
     * The video frame rate default parameters
     */
    static {
        mDroidParametersVideoFrameRateValidStrings.add(DROID_PARAMETERS_VIDEO_FRAME_RATE_DEFAULT);
        mDroidParametersVideoFrameRateValidStrings.add(DROID_PARAMETERS_VIDEO_FRAME_RATE_MAX);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_FRAME_RATE_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                true,
                1,
                60,
                VIDEO_DEFAULT_FRAME_RATE,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersVideoFrameRateValidStrings,
                null,
                null,
                false));
    }
    /**
     * The constant DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_OPTION = "video.stream_frame_period";
    /**
     * The constant mDroidParametersVideoStreamFramePeriodValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoStreamFramePeriodValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;

    /*
     * The video frame rate default parameters
     */
    static {
        mDroidParametersVideoStreamFramePeriodValidStrings.add(DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_DEFAULT);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                true,
                500,
                60000,
                VIDEO_DEFAULT_STREAM_FRAME_PERIOD,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersVideoStreamFramePeriodValidStrings,
                null,
                null,
                false));
    }

    /**
     * The constant DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_OPTION = "video.output_format";
    /**
     * The constant mDroidParametersVideoOutputFormatValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoOutputFormatValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_THREE_GPP.
     */
    private static final String DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_THREE_GPP = "THREE_GPP";
    /**
     * The constant DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_MPEG_4.
     */
    private static final String DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_MPEG_4 = "MPEG_4";

    /*
     * The video output format parameters
     */
    static {
        mDroidParametersVideoOutputFormatValidStrings.add(DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_DEFAULT);
        mDroidParametersVideoOutputFormatValidStrings.add(DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_THREE_GPP);
        mDroidParametersVideoOutputFormatValidStrings.add(DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_MPEG_4);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_OPTION, new DroidValidParameters(DroidValidParameters.STRING_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_DEFAULT,
                mDroidParametersVideoOutputFormatValidStrings,
                null,
                null,
                true));
    }

    /**
     * The constant DROID_PARAMETERS_VIDEO_MAX_DURATION_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_MAX_DURATION_OPTION = "video.max_duration";
    /**
     * The constant mDroidParametersVideoMaxDurationValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoMaxDurationValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_MAX_DURATION_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_MAX_DURATION_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_MAX_DURATION_NONE.
     */
    private static final String DROID_PARAMETERS_VIDEO_MAX_DURATION_NONE = "NONE";

    /*
      The video max duration parameters
     */
    static {
        mDroidParametersVideoMaxDurationValidStrings.add(DROID_PARAMETERS_VIDEO_MAX_DURATION_DEFAULT);
        mDroidParametersVideoMaxDurationValidStrings.add(DROID_PARAMETERS_VIDEO_MAX_DURATION_NONE);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_MAX_DURATION_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersVideoMaxDurationValidStrings,
                null,
                null,
                false));
    }

    /**
     * The constant DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_OPTION = "video.max_file_size";
    /**
     * The constant mDroidParametersVideoMaxFileSizeValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoMaxFileSizeValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_NONE.
     */
    private static final String DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_NONE = "NONE";

    /*
      The video max file size parameters
     */
    static {
        mDroidParametersVideoMaxFileSizeValidStrings.add(DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_DEFAULT);
        mDroidParametersVideoMaxFileSizeValidStrings.add(DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_NONE);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersVideoMaxFileSizeValidStrings,
                null,
                null,
                false));
    }

    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_OPTION = "video.audio.source";
    /**
     * The constant mDroidParametersVideoAudioSourceValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoAudioSourceValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_CAMCORDER.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_CAMCORDER = "CAMCORDER";
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_MIC.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_MIC = "MIC";

    /*
     * The audio source for the video parameters
     */
    static {

        mDroidParametersVideoAudioSourceValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_DEFAULT);
        mDroidParametersVideoAudioSourceValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_CAMCORDER);
        mDroidParametersVideoAudioSourceValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_MIC);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_OPTION, new DroidValidParameters(DroidValidParameters.STRING_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_DEFAULT,
                mDroidParametersVideoAudioSourceValidStrings,
                null,
                null,
                true));
    }

    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_OPTION = "video.audio.encoder";
    /**
     * The constant mDroidParametersVideoAudioEncoderValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoAudioEncoderValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AAC_AAC.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AAC_AAC = "AAC_AAC";
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AAC_ELD.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AAC_ELD = "AAC_ELD";
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AMR_NB.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AMR_NB = "AMR_NB";
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AMR_WB.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AMR_WB = "AMR_WB";
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_HE_AAC.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_HE_AAC = "HE_AAC";

    /*
     * THe audio encoder parameters
     */
    static {
        mDroidParametersVideoAudioEncoderValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_DEFAULT);
        mDroidParametersVideoAudioEncoderValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AAC_AAC);
        mDroidParametersVideoAudioEncoderValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AAC_ELD);
        mDroidParametersVideoAudioEncoderValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AMR_NB);
        mDroidParametersVideoAudioEncoderValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AMR_WB);
        mDroidParametersVideoAudioEncoderValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_HE_AAC);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_OPTION, new DroidValidParameters(DroidValidParameters.STRING_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_DEFAULT,
                mDroidParametersVideoAudioEncoderValidStrings,
                null,
                null,
                true));
    }


    /**
     * The constant HARD_CODED_DEFAULT_AUDIO_SAMPLE_RATE
     */
    private static final int HARD_CODED_DEFAULT_AUDIO_SAMPLE_RATE = 44100; // 44.1 kHz
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_OPTION = "video.audio.sample_rate";
    /**
     * The constant mDroidParametersVideoAudioSampleRateValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoAudioSampleRateValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_MIN.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_MIN = "MIN";
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_MAX.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_MAX = "MAX";

    /*
     * The audio sample rate parameters
     */
    static {
        mDroidParametersVideoAudioSampleRateValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_DEFAULT);
        mDroidParametersVideoAudioSampleRateValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_MIN);
        mDroidParametersVideoAudioSampleRateValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_MAX);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersVideoAudioSampleRateValidStrings,
                null,
                null,
                false));
    }


    /**
     * The constant HARD_CODED_DEFAULT_AUDIO_BIT_RATE
     */
    private static final int HARD_CODED_DEFAULT_AUDIO_BIT_RATE = 16384;
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_OPTION = "video.audio.bit_rate";
    /**
     * The constant mDroidParametersVideoAudioBitRateValidStrings.
     */
    private static final ArrayList<String> mDroidParametersVideoAudioBitRateValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_MIN.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_MIN = "MIN";
    /**
     * The constant DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_MAX.
     */
    private static final String DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_MAX = "MAX";

    /*
     * The audio bit rate parameters
     */
    static {
        mDroidParametersVideoAudioBitRateValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_DEFAULT);
        mDroidParametersVideoAudioBitRateValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_MIN);
        mDroidParametersVideoAudioBitRateValidStrings.add(DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_MAX);
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersVideoAudioBitRateValidStrings,
                null,
                null,
                false));
    }

    /**
     * The constant DROID_PARAMETERS_VIDEO_DISABLE_SHUTTER_SOUND_OPTION.
     */
    private static final String DROID_PARAMETERS_VIDEO_DISABLE_SHUTTER_SOUND_OPTION = "video.disable_shutter_sound";
    /**
     * The constant DROID_PARAMETERS_VIDEO_DISABLE_SHUTTER_SOUND_DEFAULT.
     */
    private static final String DROID_PARAMETERS_VIDEO_DISABLE_SHUTTER_SOUND_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;  // Leaves it on

    /*
     * The video disable shutter sound parameters
     */
    static {
        mImagingParametersValidValues.put(DROID_PARAMETERS_VIDEO_DISABLE_SHUTTER_SOUND_OPTION, new DroidValidParameters(DroidValidParameters.BOOLEAN_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                true,
                "",
                null,
                null,
                null,
                true));
    }

    /*
     * Add all the parameters to the parameters list
     */
    static {
        // Photo:
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_PHOTO_CAMERA_OPTION, DROID_PARAMETERS_PHOTO_CAMERA_DEFAULT);
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_PHOTO_FORMAT_OPTION, DROID_PARAMETERS_PHOTO_FORMAT_DEFAULT);
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_PHOTO_SIZE_OPTION, DROID_PARAMETERS_PHOTO_SIZE_DEFAULT);
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_PHOTO_QUALITY_OPTION, DROID_PARAMETERS_PHOTO_QUALITY_DEFAULT); // Defaults to 1.0 quality
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_PHOTO_DISABLE_SHUTTER_SOUND_OPTION, DROID_PARAMETERS_PHOTO_DISABLE_SHUTTER_SOUND_DEFAULT); // Defaults to leave shutter sound on
        // Video:
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_CAMERA_OPTION, DROID_PARAMETERS_VIDEO_CAMERA_DEFAULT);
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_ENCODER_OPTION, DROID_PARAMETERS_VIDEO_ENCODER_H264);  // Default is H264 encoder
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_SIZE_OPTION, DROID_PARAMETERS_VIDEO_SIZE_DEFAULT);
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_FRAME_RATE_OPTION, DROID_PARAMETERS_VIDEO_FRAME_RATE_DEFAULT);
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_OPTION, DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_DEFAULT);
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_OPTION, DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_MPEG_4);  // Default is MPEG 4
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_MAX_DURATION_OPTION, DROID_PARAMETERS_VIDEO_MAX_DURATION_DEFAULT);  // Defaults to no max in the code
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_OPTION, DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_DEFAULT);  // Defaults to no max in the code
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_OPTION, DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_MIC);
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_OPTION, DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AAC_AAC);
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_OPTION, DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_DEFAULT);  // Defaults to 44.1 kHz in code
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_OPTION, DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_DEFAULT);  // Defaults to 16 kHz in code
        mImagingDefaultParametersHashMap.put(DROID_PARAMETERS_VIDEO_DISABLE_SHUTTER_SOUND_OPTION, DROID_PARAMETERS_VIDEO_DISABLE_SHUTTER_SOUND_DEFAULT); // Defaults to leave shutter sound on
        // Add these into the list of parameters:
        // Add in our valid and default values for imaging:
        FloidService.getDroidParametersValidValues().putAll(mImagingParametersValidValues);
        FloidService.getDroidDefaultParametersHashMap().putAll(mImagingDefaultParametersHashMap);
    }

    /**
     * The imaging controller constructor
     *
     * @param context the context
     * @param floidService the FloidService for this imaging controller
     * @param floidId the floid id
     */
    public FloidImaging(@NonNull Context context, FloidService floidService, int floidId) {
        if (floidService.mUseLogger) Log.d(TAG, "Setting up Imaging");
        // Store the context
        mContext = context;
        // Store the floid service
        mFloidService = floidService;
        // Store the floidId
        mFloidId = floidId;
        // Get the photo/video storage path:
        String externalStorageState = Environment.getExternalStorageState();
        File picturesDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File filesDirectory = mFloidService.getApplicationContext().getFilesDir();
        mFloidImagingStatus.setPhotoVideoStoragePath(Environment.MEDIA_MOUNTED.equals(externalStorageState) ? picturesDirectory : filesDirectory );
        setupImagingOutputDirectory();
        // Start a background thread to handle various items:
        startBackgroundThread();
        // Get the camera manager:
        mCameraManager = (CameraManager) mFloidService.getSystemService(Context.CAMERA_SERVICE);
        if (mCameraManager == null) {
            Log.e(TAG, "FloidImaging: Null camera manager");
        } else {
            try {
                String[] cameraIdList = mCameraManager.getCameraIdList();
                for (String cameraId : cameraIdList) {
                    FloidCameraInfo cameraInfo = new FloidCameraInfo();
                    cameraInfo.setCameraId(cameraId);
                    FloidImagingUtil.logCameraInfo(mCameraManager, cameraId, cameraInfo, mFloidService.mUseLogger);
                    mCameraInfoList.add(cameraInfo);
                }
            } catch(Exception e) {
                Log.e(TAG, "FloidImaging: Exception processing camera info", e);
            }
        }
        // Add our callback handlers for parameter modifications, so we know when bits are dirty:
        // Photo:
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_PHOTO_CAMERA_OPTION, dirtyPhotoIdParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_PHOTO_FORMAT_OPTION, dirtyPhotoIdParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_PHOTO_SIZE_OPTION, dirtyPhotoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_PHOTO_QUALITY_OPTION, dirtyPhotoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_PHOTO_DISABLE_SHUTTER_SOUND_OPTION, dirtyPhotoPropertyParameterModifiedHandler);
        // Video:
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_CAMERA_OPTION, dirtyVideoIdParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_ENCODER_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_SIZE_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_FRAME_RATE_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_MAX_DURATION_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_DISABLE_SHUTTER_SOUND_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        floidService.addParameterModifiedHandler(DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_OPTION, dirtyVideoPropertyParameterModifiedHandler);
        // OK, we are initialized:
        mFloidImagingStatus.setStatusInitialized(true);
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
    }

    /**
     * Handler for photo id parameter modified
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final FloidParameterModifiedHandler dirtyPhotoIdParameterModifiedHandler = new FloidParameterModifiedHandler() {
        @Override
        public void parameterModifiedPre(String parameter, String newValue, FloidService floidService) {
        }

        @Override
        public void parameterModifiedPost(String parameter, String originalValue, FloidService floidService) {
            Log.d(TAG, "Setting photo camera id dirty");
            mFloidImagingStatus.setStatusPhotoCameraIdDirty(true);
        }
    };
    /**
     * Handler for photo properties modified
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final FloidParameterModifiedHandler dirtyPhotoPropertyParameterModifiedHandler = new FloidParameterModifiedHandler() {
        @Override
        public void parameterModifiedPre(String parameter, String newValue, FloidService floidService) {
        }

        @Override
        public void parameterModifiedPost(String parameter, String originalValue, FloidService floidService) {
            Log.d(TAG, "Setting photo camera properties dirty");
            mFloidImagingStatus.setStatusPhotoCameraPropertiesDirty(true);
        }
    };
    /**
     * Handler for photo id parameter modified
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final FloidParameterModifiedHandler dirtyVideoIdParameterModifiedHandler = new FloidParameterModifiedHandler() {
        @Override
        public void parameterModifiedPre(String parameter, String newValue, FloidService floidService) {
        }

        @Override
        public void parameterModifiedPost(String parameter, String originalValue, FloidService floidService) {
            Log.d(TAG, "Setting video camera id dirty");
            mFloidImagingStatus.setStatusVideoCameraIdDirty(true);
        }
    };
    /**
     * Handler for video properties modified
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final FloidParameterModifiedHandler dirtyVideoPropertyParameterModifiedHandler = new FloidParameterModifiedHandler() {
        @Override
        public void parameterModifiedPre(String parameter, String newValue, FloidService floidService) {
        }

        @Override
        public void parameterModifiedPost(String parameter, String originalValue, FloidService floidService) {
            Log.d(TAG, "Setting video camera properties dirty");
            mFloidImagingStatus.setStatusVideoCameraPropertiesDirty(true);
        }
    };

    /**
     * release the media recorder if needed
     *
     * @return true unless error
     */
    private boolean releaseMediaRecorder() {
        logFloidImagingStatus("releaseMediaRecorder [begin]");
        if (mVideoMediaRecorder != null) {
            try {
                mVideoMediaRecorder.release();
            } catch (Exception e) {
                Log.e(TAG, "releaseMediaRecorder: Exception releasing media recorder", e);
                mFloidImagingStatus.setStatusTakingVideo(false);
                mVideoMediaRecorder = null;
                mFloidImagingStatus.setStatusVideoCameraPropertiesDirty(true);
                mFloidImagingStatus.setStatusVideoCameraIdDirty(true);
                return false;
            }
            mVideoMediaRecorder = null;
            mFloidImagingStatus.setStatusTakingVideo(false);
            FloidImagingUtil.scanMediaFile(mContext, mVideoMediaRecorderFileName);
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("releaseMediaRecorder [end]");
        return true;
    }

    /**
     * Close this imaging controller
     */
    public void close() {
        if(!releaseMediaRecorder()) {
            Log.e(TAG, "FloidImaging close: release media recorder returned false");
        }
        closeCameras();
        stopBackgroundThread();
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
    }

    /**
     * Start the video - if the video camera is open then call startVideo2 - otherwise open the camera(s) which will call startVideo2
     */
    public void startVideo() {
        // Check that we are in a good state to start video:
        if (!mFloidImagingStatus.isStatusInitialized()) {
            Log.e(TAG, "startVideo: Not initialized");
            return;
        }
        if (mFloidImagingStatus.isStatusTakingVideo()) {
            Log.e(TAG, "startVideo: Already taking video");
            return;
        }
        if (mFloidImagingStatus.isStatusTakingPhoto()) {
            Log.e(TAG, "startVideo: Already taking photo");
            return;
        }

        // Make sure we are not taking a video:
        synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
            if (mFloidService.mFloidDroidStatus.isTakingVideo()) {
                if (mFloidService.mUseLogger)
                    Log.d(TAG, "done startVideo: video already started - returning false");
                return; // Video already started
            }
            // Still here - bump the number:
            mFloidService.mFloidDroidStatus.setVideoNumber(mFloidService.mFloidDroidStatus.getVideoNumber() + 1);
        }
        // Set the status to taking video:
        mFloidImagingStatus.setStatusTakingVideo(true); // We need this flag to later determine what path we are on in the callbacks
        // checkReopenCameras will startVideo or takePhoto if it reopens the cameras - otherwise we do it:
        if (!checkReopenCameras()) {
            if (mVideoCameraDevice != null) {
                if(mFloidService.mUseLogger) {
                    Log.d(TAG, "Did not modify camera - calling startVideo2 directly");
                }
                // Yes, start the video:
                startVideo2();
            } else {
                Log.e(TAG, "ERROR AFTER checkReopenCameras - the video camera was null when if should not have been");
                mFloidImagingStatus.setStatusTakingVideo(false); // We need this flag to later determine what path we are on in the callbacks
                return; // Could not start video
            }
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("startVideo [end]");
    }

    /**
     * Check if the camera(s) are open and nothing has changed - otherwise reopen
     *
     * @return false if we did not reopen and the camera(s) are good - true if we are reopening
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean checkReopenCameras() {
        try {
            // Is it same for both?
            boolean sameCameraForBoth = mVideoCameraDevice == mPhotoCameraDevice;
            boolean closeVideoCameraFlag = false;
            boolean closePhotoCameraFlag = false;

            if (mFloidImagingStatus.isStatusPhotoCameraIdDirty() || mFloidImagingStatus.isStatusPhotoCameraPropertiesDirty()) {
                closePhotoCameraFlag = true;
                if(sameCameraForBoth) {
                    closeVideoCameraFlag = true;
                }
            }
            if (mFloidImagingStatus.isStatusVideoCameraIdDirty() || mFloidImagingStatus.isStatusVideoCameraPropertiesDirty()) {
                closeVideoCameraFlag = true;
                if(sameCameraForBoth) {
                    closePhotoCameraFlag = true;
                }
            }
            if(closePhotoCameraFlag) {
                closePhotoCamera();
                mFloidImagingStatus.setStatusPhotoCameraIdDirty(false);
                mFloidImagingStatus.setStatusPhotoCameraOpen(false);
                mFloidImagingStatus.setStatusPhotoCameraPropertiesDirty(false);
            }
            if(closeVideoCameraFlag) {
                closeVideoCamera();
                mFloidImagingStatus.setStatusVideoCameraIdDirty(false);
                mFloidImagingStatus.setStatusVideoCameraPropertiesDirty(false);
                mFloidImagingStatus.setStatusVideoCameraOpen(false);
            }

            // Do we still have a good video camera?
            if ((mVideoCameraDevice != null) && (mPhotoCameraDevice != null)) {
                // Denote that we did not modify
                return false;
            }
            // We do not still have a good video camera, so we need to open the cameras and start the video capture
            mFloidImagingStatus.setPhotoCameraId(getPhotoCameraIdFromParameter());
            // Note: As of Android 30, we no longer have access to two separate cameras so we hard-code the video camera to the photo camera:
            mFloidImagingStatus.setVideoCameraId(mFloidImagingStatus.getPhotoCameraId());
            // TODO - If the access to a single camera is permanent, then we need to remove the ability to set the video.camera parameter everywhere:
            // Here is the original call where we get the real video camera id: - Removed for Android 30
//            mFloidImagingStatus.setVideoCameraId(getVideoCameraIdFromParameter());
            // Open the video camera - it will open the photo camera if different...
            // We either open one or two cameras automatically:
            try {
                mCameraOpenCloseLock.acquire();
            } catch (InterruptedException e) {
                Log.e(TAG, "Interrupted exception acquiring camera open close lock - closing cameras", e);
                closeCameras();
                return false;
            }
            try {
                // We open the video camera and the callback opens the photo camera if it is different
                // or sets it to the video camera if it is the same:
                mCameraManager.openCamera(mFloidImagingStatus.getVideoCameraId(), mCameraStateCallback, mBackgroundHandler);
            } catch (CameraAccessException e) {
                Log.e(TAG, "Camera access exception exception opening camera - closing cameras", e);
                closeCameras();
                return false;
            }
            catch (SecurityException se) {
                Log.e(TAG, "Camera access exception security exception opening camera - closing cameras", se);
                closeCameras();
                return false;
            }
        } catch (Exception e2) {
            Log.e(TAG, "Camera access exception exception acquiring camera open close lock", e2);
            closeCameras();
            return false;
        }
        return true; // We modified the cameras
    }

    private void closePhotoCamera() {
        logFloidImagingStatus("closePhotoCamera [begin]");
        if (!mFloidImagingStatus.isStatusInitialized()) return;
        boolean alsoCloseVideoCamera = false;
        if (mPhotoCameraDevice != null) {
            try {
                mPhotoCameraDevice.close();
            } catch (Exception e) {
                Log.e(TAG, "closePhotoCamera: caught exception: ", e);
            }
            if (mPhotoCameraDevice == mVideoCameraDevice) {
                mVideoCameraDevice = null;
                alsoCloseVideoCamera = true;
            }
        }
        mFloidImagingStatus.setVideoCameraId(FloidImagingStatus.NO_CAMERA_ID_STRING);
        closePhotoSurfaces();
        mFloidImagingStatus.setStatusPhotoCameraOpen(false);
        mPhotoCameraDevice = null;
        mFloidImagingStatus.setPhotoCameraId(FloidImagingStatus.NO_CAMERA_ID_STRING);
        if (alsoCloseVideoCamera) {
            closeVideoCamera();
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("closePhotoCamera [end]");
    }

    /**
     * Close the photo surfaces
     */
    private void closePhotoSurfaces() {
        logFloidImagingStatus("closePhotoSurfaces [begin]");
        if (mPhotoCameraCaptureSession != null) {
            try {
                mPhotoCameraCaptureSession.close();
            } catch (Exception e) {
                Log.e(TAG, "closePhotoSurfaces: Exception closing photo surfaces: ", e);
            }
            mPhotoCameraCaptureSession = null;
        }
        mFloidImagingStatus.setStatusPhotoCameraCaptureSessionConfigured(false);
        if (mPhotoImageReader != null) {
            try {
                mPhotoImageReader.close();
            } catch (Exception e) {
                Log.e(TAG, "closePhotoSurfaces: Exception closing photo image reader: ", e);
            }
            mPhotoImageReaderSurface = null;
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("closePhotoSurfaces [end]");
    }

    /**
     * Close the video camera
     */
    private void closeVideoCamera() {
        logFloidImagingStatus("closeVideoCamera [begin]");
        if (!mFloidImagingStatus.isStatusInitialized()) return;
        boolean alsoClosePhotoCamera = false;
        try {
            if (mVideoCameraDevice != null) {
                mVideoCameraDevice.close();
                // If they were the same camera, we need to close both, however it is already closed
                if (mVideoCameraDevice == mPhotoCameraDevice) {
                    mPhotoCameraDevice = null;
                    alsoClosePhotoCamera = true;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception closing video camera device: ", e);
        }
        mFloidImagingStatus.setPhotoCameraId(FloidImagingStatus.NO_CAMERA_ID_STRING);
        try {
            closeVideoSurfaces();
        } catch (Exception e) {
            Log.e(TAG, "Exception closing video surfaces: ", e);
        }
        mFloidImagingStatus.setStatusVideoCameraOpen(false);
        mVideoCameraDevice = null;
        mFloidImagingStatus.setVideoCameraId(FloidImagingStatus.NO_CAMERA_ID_STRING);
        if (alsoClosePhotoCamera) {
            try {
                closePhotoCamera();
            } catch (Exception e) {
                Log.e(TAG, "Exception closing photo camera: ", e);
            }
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("closeVideoCamera [end]");
    }

    /**
     * Close the video surfaces
     */
    private void closeVideoSurfaces() {
        logFloidImagingStatus("closeVideoSurfaces [begin]");
        if (mVideoCameraCaptureSession != null) {
            try {
                mVideoCameraCaptureSession.close();
            } catch (Exception e) {
                Log.e(TAG, "closeVideoSurfaces: Exception closing video surfaces: ", e);
            }
            mVideoCameraCaptureSession = null;
        }
        mFloidImagingStatus.setStatusVideoCameraCaptureSessionConfigured(false);
        if (mVideoStreamingImageReader != null) {
            try {
                mVideoStreamingImageReader.close();
            } catch (Exception e) {
                Log.e(TAG, "closeVideoSurfaces: Exception closing video streaming image reader: ", e);
            }
            mVideoStreamingImageReader = null;
            mVideoStreamingImageReaderSurface = null;
        }
        if (mVideoSnapshotImageReader != null) {
            try {
                mVideoSnapshotImageReader.close();
            } catch (Exception e) {
                Log.e(TAG, "closeVideoSurfaces: Exception closing video snapshot image reader: ", e);
            }
            mVideoSnapshotImageReader = null;
            mVideoSnapshotImageReaderSurface = null;
        }
        mFloidImagingStatus.setStatusVideoSurfacesSetup(false);
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("closeVideoSurfaces [end]");
    }

    /**
     * Capture session created callback - this is downstream from the camera opens and surfaces created
     * calls either startVideo2 or takePhoto2 depending
     */
    private synchronized void captureSessionCreated() {
        if(mFloidService.mUseLogger)
            Log.d(TAG, "We created a capture session - " + (mFloidImagingStatus.getNumberOfRemainingCaptureSessionsToCreate() - 1) + " remaining.");
        mFloidImagingStatus.setNumberOfRemainingCaptureSessionsToCreate(mFloidImagingStatus.getNumberOfRemainingCaptureSessionsToCreate() - 1);
        if (mFloidImagingStatus.getNumberOfRemainingCaptureSessionsToCreate() == 0) {
            if (mFloidImagingStatus.isStatusTakingVideo()) {
                try {
                    startVideo2();
                } catch (Exception e) {
                    Log.e(TAG, "Exception calling startVideo2()", e);
                }
            } else {
                if (mFloidImagingStatus.isStatusTakingPhoto()) {
                    try {
                        takePhoto2();
                    } catch (Exception e) {
                        Log.e(TAG, "Exception calling takePhoto2()", e);
                    }

                }
            }
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
    }

    /**
     * Set up the surfaces and capture sessions for this camera device
     *
     * @param cameraDevice the camera device
     * @param isVideo      is this a video camera device
     * @param isPhoto      is this a photo camera device
     */
    private void setupSurfacesAndCaptureSessions(CameraDevice cameraDevice, boolean isVideo, boolean isPhoto) {
        if (!(isVideo || isPhoto)) {
            Log.e(TAG, "setupSurfacesAndCaptureSessions: Logic Error - called for neither video or photo - closing cameras");
            closeCameras();
            return;
        }
        try {
            // Set up our three surfaces and their associated capture request builders:
            List<Surface> surfaces = new ArrayList<>();

            if (isPhoto) {
                // Photo Image Reader:
                try {
                    mPhotoImageReader = getPhotoImageReaderFromParameters(cameraDevice);
                    if (mPhotoImageReader == null) {
                        Log.e(TAG, "Photo image reader is null - closing cameras");
                        closeCameras();
                        return;
                    }
                    mPhotoImageReaderSurface = mPhotoImageReader.getSurface();
                    if (mPhotoImageReaderSurface == null) {
                        Log.e(TAG, "Photo image reader surface is null - closing cameras");
                        closeCameras();
                        return;
                    }
                } catch (Exception e3) {
                    Log.e(TAG, "Caught exception creating photo capture request builder - closing cameras", e3);
                    closeCameras();
                    return;
                }
                // Check flash, create and set up photo preview image reader, create and set up photo capture request builder:
                try {
                    // Check if the flash is supported for the photo camera:
                    Boolean flashAvailable = mCameraManager.getCameraCharacteristics(cameraDevice.getId()).get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                    mFloidImagingStatus.setFlashSupported(flashAvailable != null && flashAvailable);
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "CAMERA [" + cameraDevice.getId() + "] Flash: " + (mFloidImagingStatus.isFlashSupported()?"SUPPORTED":"NOT SUPPORTED"));
                    // Create the Photo Preview request builder and add the target:
                    mPhotoPreviewCaptureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                    mPhotoPreviewCaptureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
                    mPhotoPreviewCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                    mPhotoPreviewCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
                    setAutoFlash(mPhotoPreviewCaptureRequestBuilder);
                    // The preview surface is the same photo image reader surface:
                    mPhotoPreviewCaptureRequestBuilder.addTarget(mPhotoImageReaderSurface);
                    // Create the Photo Capture request builder and add the target:
                    mPhotoCaptureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
                    mPhotoCaptureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
                    mPhotoCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                    mPhotoCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
                    mPhotoCaptureRequestBuilder.set(CaptureRequest.CONTROL_SCENE_MODE, CaptureRequest.CONTROL_SCENE_MODE_HDR);
                    setAutoFlash(mPhotoCaptureRequestBuilder);
                    mPhotoCaptureRequestBuilder.addTarget(mPhotoImageReaderSurface);
                    surfaces.add(mPhotoImageReaderSurface);
                } catch (Exception e3) {
                    Log.e(TAG, "Caught exception creating photo capture request builder - closing cameras", e3);
                    closeCameras();
                    return;
                }
                mFloidImagingStatus.setStatusPhotoCaptureRequestSetup(true); // When done...
                mPhotoSurfaces = surfaces;
                mFloidImagingStatus.setStatusPhotoSurfacesSetup(true);
            }
            if (isVideo) {
                // Media Recorder and streaming video capture:
                try {
                    setupMediaRecorder();
                } catch (Exception e3) {
                    Log.e(TAG, "Caught exception setting up media recorder - closing cameras", e3);
                    closeCameras();
                    return;
                }
                if (mVideoMediaRecorder == null) {
                    Log.e(TAG, "Video media recorder is null - closing cameras");
                    closeCameras();
                    return;
                }
                mVideoMediaRecorderSurface = mVideoMediaRecorder.getSurface();
                mVideoMediaRecorder.setPreviewDisplay(mVideoMediaRecorderSurface);
                if (mVideoMediaRecorderSurface == null) {
                    Log.e(TAG, "Video media recorder surface is null - closing cameras");
                    closeCameras();
                    return;
                }
                try {
                    mVideoStreamingCaptureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD); // Never null
                    mVideoStreamingCaptureRequestBuilder.addTarget(mVideoMediaRecorderSurface);
                    mVideoStreamingCaptureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
                    surfaces.add(mVideoMediaRecorderSurface);
                } catch (Exception e3) {
                    Log.e(TAG, "Caught exception creating video media recorder request builder / setting control mode - closing cameras", e3);
                    closeCameras();
                    return;
                }
                // Streaming image reader:
                try {
                    mVideoStreamingImageReader = getVideoStreamingImageReaderFromParameters(cameraDevice);
                    if (mVideoStreamingImageReader == null) {
                        Log.e(TAG, "Video streaming image reader is null - closing cameras");
                        closeCameras();
                        return;
                    }
                } catch (Exception e3) {
                    Log.e(TAG, "Caught exception getting video streaming image reader from parameters - closing cameras", e3);
                    closeCameras();
                    return;
                }
                try {
                    mVideoStreamingImageReaderSurface = mVideoStreamingImageReader.getSurface();
                    if (mVideoStreamingImageReaderSurface == null) {
                        Log.e(TAG, "Video streaming image reader surface is null - closing cameras");
                        closeCameras();
                        return;
                    }
                } catch (Exception e3) {
                    Log.e(TAG, "Caught exception getting video streaming surface - closing cameras", e3);
                    closeCameras();
                    return;
                }
                try {
                    mVideoStreamingCaptureRequestBuilder.addTarget(mVideoStreamingImageReaderSurface);
                    surfaces.add(mVideoStreamingImageReaderSurface);
                } catch (Exception e3) {
                    Log.e(TAG, "Caught exception adding video streaming image reader surface / setting control mode - closing cameras", e3);
                    closeCameras();
                    return;
                }
                // Video Snapshot image reader and capture request:
                try {
                    CameraCharacteristics characteristics = mCameraManager.getCameraCharacteristics(cameraDevice.getId());
                    int deviceLevel = CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY;
                    try {
                        Integer deviceLevelInteger = characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL);
                        if (deviceLevelInteger != null) {
                            deviceLevel = deviceLevelInteger;
                        }
                    } catch (Exception e2) {
                        Log.e(TAG, "Caught exception getting supported hardware level characteristics - disabling video snapshot", e2);
                        mFloidImagingStatus.setStatusSupportsVideoSnapshot(false);
                    }
                    mFloidImagingStatus.setHardwareIsLegacy(false);
                    switch (deviceLevel) {
                        case CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY:
                            if(mFloidService.mUseLogger)
                                Log.d(TAG, "CAMERA [" + cameraDevice.getId() + "] has LEGACY support ");
                            mFloidImagingStatus.setHardwareIsLegacy(true);
                            mFloidImagingStatus.setStatusSupportsVideoSnapshot(false);
                            break;
                        case CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LIMITED:
                            if(mFloidService.mUseLogger)
                                Log.d(TAG, "CAMERA [" + cameraDevice.getId() + "] has LIMITED support ");
                            mFloidImagingStatus.setStatusSupportsVideoSnapshot(true);
                            break;
                        case CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_FULL:
                            if(mFloidService.mUseLogger)
                                Log.d(TAG, "CAMERA [" + cameraDevice.getId() + "] has FULL support ");
                            mFloidImagingStatus.setStatusSupportsVideoSnapshot(true);
                            break;
                        case CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_3:
                            if(mFloidService.mUseLogger)
                                Log.d(TAG, "CAMERA [" + cameraDevice.getId() + "] has LEVEL 3 support ");
                            mFloidImagingStatus.setStatusSupportsVideoSnapshot(true);
                            break;
                        default:
                            if(mFloidService.mUseLogger)
                                Log.w(TAG, "CAMERA [" + cameraDevice.getId() + "] has UNKNOWN support: " + deviceLevel + " - assigning LEGACY");
                            mFloidImagingStatus.setHardwareIsLegacy(true);
                            mFloidImagingStatus.setStatusSupportsVideoSnapshot(false);
                            break;
                    }
                } catch (Exception e3) {
                    Log.e(TAG, "Caught exception getting video snapshot support hardware level from camera - disabling", e3);
                    mFloidImagingStatus.setStatusSupportsVideoSnapshot(false);
                }

                // Set up video snapshot if supported:
                if (mFloidImagingStatus.isStatusSupportsVideoSnapshot()) {
                    try {
                        if(isPhoto) {
                            // Reuse from photo:
                            mVideoSnapshotImageReader = mPhotoImageReader;
                        } else {
                            // Or create new:
                            mVideoSnapshotImageReader = getVideoSnapshotImageReaderFromParameters(cameraDevice);
                        }
                        if (mVideoSnapshotImageReader == null) {
                            Log.e(TAG, "Video snapshot image reader is null - closing cameras");
                            closeCameras();
                            return;
                        }
                        mVideoSnapshotImageReaderSurface = mVideoSnapshotImageReader.getSurface();
                        if (mVideoSnapshotImageReaderSurface == null) {
                            Log.e(TAG, "Video snapshot Image reader surface is null - closing cameras");
                            closeCameras();
                            return;
                        }
                    } catch (Exception e3) {
                        Log.e(TAG, "Caught exception getting video snapshot image reader from parameters - closing cameras", e3);
                        closeCameras();
                        return;
                    }
                    try {
                        mVideoSnapshotCaptureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_VIDEO_SNAPSHOT);
                        if(mPhotoImageReaderSurface != null) {
                            // Re-use the photo surface:
                            mVideoSnapshotCaptureRequestBuilder.addTarget(mPhotoImageReaderSurface);
                        } else {
                            mVideoSnapshotCaptureRequestBuilder.addTarget(mVideoSnapshotImageReaderSurface);
                            mVideoSnapshotCaptureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_CAPTURE_INTENT_VIDEO_SNAPSHOT);
                            mVideoSnapshotCaptureRequestBuilder.set(CaptureRequest.CONTROL_CAPTURE_INTENT, CameraMetadata.CONTROL_MODE_AUTO);
                            surfaces.add(mVideoSnapshotImageReaderSurface);
                        }
                    } catch (Exception e3) {
                        Log.e(TAG, "Caught exception creating video snapshot capture request builder and  video snapshot image reader from parameters - closing cameras", e3);
                        closeCameras();
                        return;
                    }
                }
                // The video capture requests are setup
                mFloidImagingStatus.setStatusVideoCaptureRequestSetup(true);
                mVideoSurfaces = surfaces;
                mFloidImagingStatus.setStatusVideoSurfacesSetup(true);
            }

            surfaces.forEach((surface -> {
                Log.d(TAG, "SURFACE: " + surface.toString());
            }));

            if (isPhoto && isVideo) {
                List<OutputConfiguration> outputConfigurations = new ArrayList<>();
                surfaces.forEach((surface)-> outputConfigurations.add(new OutputConfiguration(surface)));
                SessionConfiguration sessionConfiguration = new SessionConfiguration(SessionConfiguration.SESSION_REGULAR, outputConfigurations, new ThreadPerTaskExecutor(),
                        new CameraCaptureSession.StateCallback() {
                            @Override
                            public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                                if(mFloidService.mUseLogger)
                                    Log.d(TAG, "captureSession (video and photo) startVideo2 - onConfigured");
                                mPhotoCameraCaptureSession = cameraCaptureSession;
                                mFloidImagingStatus.setStatusPhotoCameraCaptureSessionConfigured(true);
                                mVideoCameraCaptureSession = cameraCaptureSession;
                                mFloidImagingStatus.setStatusVideoCameraCaptureSessionConfigured(true);
                                captureSessionCreated();
                            }

                            @Override
                            public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                                mFloidImagingStatus.setNumberOfRemainingCaptureSessionsToCreate(mFloidImagingStatus.getNumberOfRemainingCaptureSessionsToCreate() - 1);
                                mPhotoCameraCaptureSession = null;
                                mFloidImagingStatus.setStatusPhotoCameraCaptureSessionConfigured(false);
                                mVideoCameraCaptureSession = null;
                                mFloidImagingStatus.setStatusVideoCameraCaptureSessionConfigured(false);
                                Log.e(TAG, "Capture Session onConfigure (for both) Failed");
                            }
                        });
                if(cameraDevice.isSessionConfigurationSupported(sessionConfiguration)) {
                    Log.d(TAG, "Photo & Video Camera: Session Configuration Supported");
                } else {
                    Log.e(TAG, "Photo & Video Camera: Session Configuration NOT Supported");
                }
                cameraDevice.createCaptureSession(sessionConfiguration);
            } else if (isPhoto) {
                List<OutputConfiguration> outputConfigurations = new ArrayList<>();
                surfaces.forEach((surface)-> outputConfigurations.add(new OutputConfiguration(surface)));

                SessionConfiguration sessionConfiguration = new SessionConfiguration(SessionConfiguration.SESSION_REGULAR, outputConfigurations, new ThreadPerTaskExecutor(),
                        new CameraCaptureSession.StateCallback() {
                            @Override
                            public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                                if(mFloidService.mUseLogger)
                                    Log.d(TAG, "captureSession (photo) startVideo2 - onConfigured");
                                mPhotoCameraCaptureSession = cameraCaptureSession;
                                mFloidImagingStatus.setStatusPhotoCameraCaptureSessionConfigured(true);
                                captureSessionCreated();
                            }
                            @Override
                            public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                                if(mFloidService.mUseLogger)
                                    Log.d(TAG, "captureSession (photo) startVideo2 - onConfigured Failed");
                                mFloidImagingStatus.setNumberOfRemainingCaptureSessionsToCreate(mFloidImagingStatus.getNumberOfRemainingCaptureSessionsToCreate() - 1);
                                mPhotoCameraCaptureSession = null;
                                mFloidImagingStatus.setStatusPhotoCameraCaptureSessionConfigured(false);
                                Log.e(TAG, "Capture Session onConfigure (for photo) Failed");
                            }
                        });
                if(cameraDevice.isSessionConfigurationSupported(sessionConfiguration)) {
                    Log.d(TAG, "Photo Camera: Session Configuration Supported");
                } else {
                    Log.e(TAG, "Photo Camera: Session Configuration NOT Supported");
                }
                cameraDevice.createCaptureSession(sessionConfiguration);
            } else {
                // Must be video due to a precondition:
                List<OutputConfiguration> outputConfigurations = new ArrayList<>();
                surfaces.forEach((surface)-> outputConfigurations.add(new OutputConfiguration(surface)));

                SessionConfiguration sessionConfiguration = new SessionConfiguration(SessionConfiguration.SESSION_REGULAR, outputConfigurations, new ThreadPerTaskExecutor(),
                        new CameraCaptureSession.StateCallback() {
                            @Override
                            public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                                if(mFloidService.mUseLogger)
                                    Log.d(TAG, "captureSession (video) startVideo2 - onConfigured");
                                mVideoCameraCaptureSession = cameraCaptureSession;
                                mFloidImagingStatus.setStatusVideoCameraCaptureSessionConfigured(true);
                                captureSessionCreated();
                            }

                            @Override
                            public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                                if(mFloidService.mUseLogger)
                                    Log.d(TAG, "captureSession (video) startVideo2 - onConfigured Failed");
                                mFloidImagingStatus.setNumberOfRemainingCaptureSessionsToCreate(mFloidImagingStatus.getNumberOfRemainingCaptureSessionsToCreate() - 1);
                                mVideoCameraCaptureSession = null;
                                mFloidImagingStatus.setStatusVideoCameraCaptureSessionConfigured(false);
                                Log.e(TAG, "Capture Session onConfigure (for video) Failed");
                            }
                        });
                if(cameraDevice.isSessionConfigurationSupported(sessionConfiguration)) {
                    Log.d(TAG, "Video Camera: Session Configuration Supported");
                } else {
                    Log.e(TAG, "Video Camera: Session Configuration NOT Supported");
                }
                cameraDevice.createCaptureSession(sessionConfiguration);
            }
        } catch (Exception e) {
            Log.e(TAG, "setupSurfacesAndCaptureSessions: caught exception", e);
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
    }


    private Bitmap YUV_420_888_toRGB(Image image, int width, int height){
        // Get a toolkit:
        Toolkit toolkit  = Toolkit.INSTANCE;
        // Return the bitmap:
        return toolkit.yuvToRgbBitmap(toolkit.imageToI420(image), width, height, YuvFormat.NV21);
    }

    /**
     * Get the video streaming image reader from the parameters
     *
     * @param cameraDevice the camera device
     * @return a video image reader for the streaming video (video capture preview)
     */
    private ImageReader getVideoStreamingImageReaderFromParameters(CameraDevice cameraDevice) {
        // Get the size and format:
        int imageReaderFormat = getVideoStreamingImageReaderFormatFromVideoParameters(cameraDevice);
        int[] imageReaderSize = getSizeFromVideoParameters(cameraDevice);
        // Set up the image reader for the video steaming:
        ImageReader imageReader = ImageReader.newInstance(imageReaderSize[0], imageReaderSize[1], imageReaderFormat, 12);
        // Set up the image available listener for the video steaming:
        imageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Image Reader [Video Streaming]: Image available");
                // Get the image:
                Image image = reader.acquireNextImage();
                if(image != null) {
                    // Spin through the images that might be on the queue:
                    Image imageNext = reader.acquireNextImage();
                    while (imageNext != null) {
                        image.close();
                        image = imageNext;
                        imageNext = reader.acquireNextImage();
                    }
                }
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Image Reader [Video Streaming]: Image acquired = format: " + (image != null ? image.getFormat() : "null"));
                if (System.currentTimeMillis() >= (mVideoStreamingImageReaderLastPreviewMillis + mVideoStreamFramePeriod)) {
                    mVideoStreamingImageReaderLastPreviewMillis = System.currentTimeMillis();
                    try {
                        // Synchronize so it is added to mVideoFrames going to host:
                        if (image != null) {
                            final int width = image.getWidth();
                            final int height = image.getHeight();
                            // Convert to bitmap Using renderscript:
                            Bitmap rgbBitmap = null;
                            try {
                                rgbBitmap = YUV_420_888_toRGB(image, width, height);
                            } catch(Exception e) {
                                Log.e(TAG, "Image Reader [Video Streaming]: Exception converting bitmap", e);
                            }
                            // Close the image:
                            image.close();
                            if(rgbBitmap!=null) {
                                final Bitmap bitmap = rgbBitmap;
                                if (mFloidService.mUseLogger)
                                    Log.d(TAG, "Image Reader [Video Streaming]: Adding image to Floid Service Queue [Video Frames]");
                                // Handle the jpeg and compression in a separate thread:
                                new Thread(new Runnable() {
                                    public void run() {
                                        try {
                                            final ByteArrayOutputStream out = new ByteArrayOutputStream();
                                            // TODO - XYZZY - FIX ME!!! IS THIS IMAGE CORRECT ON TEST PHONE????
                                            // Compress the image to a jpeg:
                                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                                            byte[] videoFrameBytes = out.toByteArray();
                                            out.flush();
                                            out.close();
                                            // Create a smaller bitmap for the UX:
                                            // TODO - XYZZY - FIX ME!!! IS THIS IMAGE CORRECT ON TEST PHONE????
                                            Bitmap smallerBitmap = Bitmap.createScaledBitmap(bitmap, VIDEO_FRAME_PREVIEW_WIDTH, VIDEO_FRAME_PREVIEW_HEIGHT, true);
                                            // Get the bytes back from the smaller bitmap:
                                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                            smallerBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                            byte[] smallerVideoFrameBytes = stream.toByteArray();
                                            // Create a floid service video frame with metadata for transport:
                                            final FloidServiceVideoFrame floidServiceVideoFrame = new FloidServiceVideoFrame();
                                            Date date = new Date();
                                            setMetaData(floidServiceVideoFrame, imagingSimpleDateFormat.format(date) + "_VIDEO.jpg", "JPEG", ".jpg");
                                            floidServiceVideoFrame.setImageBytes(videoFrameBytes);
                                            // Create a floid service video frame for the ux bytes for transport:
                                            final FloidServiceVideoFrame floidServiceSmallerVideoFrame = new FloidServiceVideoFrame();
                                            setMetaData(floidServiceSmallerVideoFrame, imagingSimpleDateFormat.format(date) + "_VIDEO.jpg", "JPEG", ".jpg");
                                            floidServiceSmallerVideoFrame.setImageBytes(smallerVideoFrameBytes);
                                            synchronized (mFloidService.mFloidVideoFrameSynchronizer) {
                                                if (mFloidService.mUseLogger)
                                                    Log.d(TAG, "Image Reader [Video Stream]: Adding image to Floid Service Queue [Video Frames]");
                                                mFloidService.mVideoFrames.add(floidServiceVideoFrame);
                                                mFloidService.mVideoFramesForUx.add(floidServiceSmallerVideoFrame);
                                            }
                                            if (mFloidService.mUseLogger)
                                                Log.d(TAG, "Image Reader [Video Streaming]: Saving image: " + floidServiceVideoFrame.getImageName());
                                            writeImagingFile(mContext, floidServiceVideoFrame.getImageName(), videoFrameBytes);
                                        } catch (Exception e) {
                                            if (mFloidService.mUseLogger)
                                                Log.e(TAG, "Image Reader [Video Streaming]: Failed to save image", e);
                                        }
                                    }
                                }).start();
                            } else {
                                Log.e(TAG, "Image Reader [Video Streaming]: Bitmap was null");
                            }
                        } else {
                            if (mFloidService.mUseLogger)
                                Log.e(TAG, "Image Reader [Video Streaming]: Image was null");
                        }
                    } catch (Exception e) {
                        // Do nothing:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Image Reader [Video Streaming]: Caught exception", e);
                    }
                }
                if(image != null) {
                    try {
                        image.close();
                    } catch (Exception e2) {
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Image Reader [Video Streaming]: Caught exception closing images", e2);
                    }
                }
            }
        }, mBackgroundHandler);
        // Return our image handler:
        return imageReader;
    }

    /**
     * Get the video snapshot image reader from the parameters
     *
     * @param cameraDevice the camera device
     * @return a video image reader for the video snapshot image
     */
    private ImageReader getVideoSnapshotImageReaderFromParameters(CameraDevice cameraDevice) {
        // Get the size and format:
        int imageReaderFormat = getFormatFromPhotoParameters(cameraDevice);
        int[] imageReaderSize = getSizeFromPhotoParameters(cameraDevice, imageReaderFormat);
        // Set up the image reader for the video snapshot:
        ImageReader imageReader = ImageReader.newInstance(imageReaderSize[0], imageReaderSize[1], imageReaderFormat, 1);
        // Set up the image available listener for the video snapshot:
        imageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                Log.d(TAG, "Image Reader [Video Snapshot]: Image available");
                try {
                    // Get the next image:
                    Image image = reader.acquireNextImage();
                    Log.d(TAG, "Image Reader [Video Streaming]: Image acquired = format: " + (image != null ? image.getFormat() : "null"));
                    if (image != null) {
                        // Get the image:
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        final byte[] bytes = new byte[buffer.remaining()];
                        buffer.get(bytes);
                        // Create a floid service photo with metadata for transport:
                        final FloidServicePhoto floidServicePhoto = new FloidServicePhoto();
                        Date date = new Date();
                        setMetaData(floidServicePhoto, imagingSimpleDateFormat.format(date) + "_SNAPSHOT.jpg", "JPEG", ".jpg");
                        floidServicePhoto.setImageBytes(bytes);
                        synchronized (mFloidService.mFloidPhotoSynchronizer) {
                            if (mFloidService.mUseLogger) Log.d(TAG, "Image Reader [Video Snapshot]: Adding image to Floid Service Queue [Photos]");
                            mFloidService.mPhotos.add(floidServicePhoto);
                        }
                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    // Create a bitmap from the bytes:
                                    final Bitmap imageBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                    Bitmap smallerBitmap = Bitmap.createScaledBitmap(imageBitmap, PHOTO_PREVIEW_WIDTH, PHOTO_PREVIEW_HEIGHT, true);
                                    // Get the bytes back from the smaller bitmap:
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    smallerBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                    byte[] smallerBitmapBytes = stream.toByteArray();
                                    // Create a floid service photo with metadata for transport:
                                    final FloidServicePhoto floidServicePhoto = new FloidServicePhoto();
                                    final Date date = new Date();
                                    setMetaData(floidServicePhoto, imagingSimpleDateFormat.format(date) + ".jpg", "JPEG", ".jpg");
                                    floidServicePhoto.setImageBytes(bytes);
                                    // Create a floid service photo for the ux bytes for transport:
                                    final FloidServicePhoto floidServiceSmallerPhoto = new FloidServicePhoto();
                                    setMetaData(floidServiceSmallerPhoto, imagingSimpleDateFormat.format(date) + ".jpg", "JPEG", ".jpg");
                                    floidServiceSmallerPhoto.setImageBytes(smallerBitmapBytes);
                                    // Add the photos to the lists for processing:
                                    synchronized (mFloidService.mFloidPhotoSynchronizer) {
                                        if (mFloidService.mUseLogger)
                                            Log.d(TAG, "Image Reader [Video Snapshot]: Adding image to Floid Service Queue [Photos]");
                                        mFloidService.mPhotos.add(floidServicePhoto);
                                        mFloidService.mPhotosForUx.add(floidServiceSmallerPhoto);
                                    }
                                    if (mFloidService.mUseLogger)
                                        Log.d(TAG, "Image Reader [Video Snapshot]: Saving image: " + floidServicePhoto.getImageName());
                                    writeImagingFile(mContext, floidServicePhoto.getImageName(), bytes);
                                } catch (Exception e) {
                                    if (mFloidService.mUseLogger) Log.e(TAG, "Image Reader [Video Snapshot]: Failed to save image", e);
                                }
                            }
                        }).start();
                        image.close();
                    } else {
                        if (mFloidService.mUseLogger) Log.e(TAG, "Image Reader [Video Snapshot]: Image was null");
                    }
                } catch (Exception e) {
                    // Do nothing:
                    if (mFloidService.mUseLogger) Log.e(TAG, "Image Reader [Video Snapshot]: Caught exception", e);
                }
                mFloidImagingStatus.setStatusTakingVideoSnapshot(false);
                mFloidImagingStatus.setStatusTakingPhoto(false);
                synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
                    mFloidService.mFloidDroidStatus.setTakingPhoto(false);
                }
            }
        }, mBackgroundHandler);
        // Return our image handler:
        return imageReader;
    }

    /**
     * Set the metadata to the floid service image
     * @param floidServiceImage the floid service image
     * @param imageName the image name
     * @param imageType the image type
     * @param imageExtension the image extension
     */
    @SuppressWarnings("SameParameterValue")
    private void setMetaData(FloidServiceImage floidServiceImage, String imageName, String imageType, String imageExtension) {
        double photoGpsAltitude = mFloidService.getFloidStatus().isAi() ? mFloidService.getFloidStatus().getAa() : mFloidService.getFloidStatus().getGz();
        double photoGpsLatitude = mFloidService.getFloidStatus().getGy();
        double photoGpsLongitude = mFloidService.getFloidStatus().isAi() ? mFloidService.getFloidStatus().getAa() : mFloidService.getFloidStatus().getGz();
        floidServiceImage.setAltitude(photoGpsAltitude);
        floidServiceImage.setLatitude(photoGpsLatitude);
        floidServiceImage.setLongitude(photoGpsLongitude);
        floidServiceImage.setPan(0.0);
        floidServiceImage.setTilt(0.0);
        floidServiceImage.setImageName(imageName);
        floidServiceImage.setImageType(imageType);
        floidServiceImage.setImageExtension(imageExtension);
    }

    /**
     * Write image file
     * @param context the context
     * @param fileName the file name
     * @param bytes the image bytes
     */
    private void writeImagingFile(Context context, String fileName, byte[] bytes) {
        FileOutputStream outputStream = null;
        try {
            String filePath = createFilePath(fileName);
            outputStream = new FileOutputStream(filePath);//  getApplicationContext().openFileOutput(photoFileName, MODE_PRIVATE);
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
            outputStream = null;
            FloidImagingUtil.scanMediaFile(context, filePath);
        } catch (Exception e) {
            Log.e(TAG, "writeImagingFile: Failed to write output stream", e);
        }
        if(outputStream != null) {
            try {
                outputStream.close();
            } catch (Exception e) {
                Log.e(TAG, "writeImagingFile: Failed to close output stream", e);
            }
        }
    }

    /**
     * Get the photo image reader from the parameters
     *
     * @param cameraDevice the camera device
     * @return a photo image reader (still photo)
     */
    private ImageReader getPhotoImageReaderFromParameters(CameraDevice cameraDevice) {
        // Get the size and format:
        int imageReaderFormat = getFormatFromPhotoParameters(cameraDevice);
        int[] imageReaderSize = getSizeFromPhotoParameters(cameraDevice, imageReaderFormat);
        // Set up the image reader for the photo:
        ImageReader imageReader = ImageReader.newInstance(imageReaderSize[0], imageReaderSize[1], imageReaderFormat, 12);
        // Set up the image available listener for the photo:
        imageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Image Reader [Photo]: Image available");
                boolean doneTakingPhoto = false;
                try {
                    // Get the next image:
                    Image image = reader.acquireNextImage();
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "Image Reader [Photo]: Image acquired. Format:" + (image != null ? image.getFormat() : "null"));
                    // Do we have a good image and in state picture requested?  Or are we taking a video snapshot which comes in differently?
                    if (image != null && (mFloidImagingStatus.getPhotoState() == FloidImagingStatus.STATE_PICTURE_REQUESTED || mFloidImagingStatus.isStatusTakingVideoSnapshot())) {
                        if(mFloidService.mUseLogger)
                            Log.d(TAG, "Image Reader [Photo]: State is PICTURE REQUESTED - Saving");
                        // Get the bytes for the image:
                        Image.Plane[] planes = image.getPlanes();
                        ByteBuffer buffer = planes[0].getBuffer();
                        final byte[] bytes = new byte[buffer.remaining()];
                        buffer.get(bytes);
                        image.close();
                        // Create a thread to save this since we have the bytes already and that is all we need.
                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    // Create a bitmap from the bytes:
                                    final Bitmap imageBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                    Bitmap smallerBitmap = Bitmap.createScaledBitmap(imageBitmap, PHOTO_PREVIEW_WIDTH, PHOTO_PREVIEW_HEIGHT, true);
                                    // Get the bytes back from the smaller bitmap:
                                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                    smallerBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                    byte[] smallerBitmapBytes = stream.toByteArray();
                                    // Create a floid service photo with metadata for transport:
                                    final FloidServicePhoto floidServicePhoto = new FloidServicePhoto();
                                    final Date date = new Date();
                                    setMetaData(floidServicePhoto, imagingSimpleDateFormat.format(date) + ".jpg", "JPEG", ".jpg");
                                    floidServicePhoto.setImageBytes(bytes);
                                    // Create a floid service photo for the ux bytes for transport:
                                    final FloidServicePhoto floidServiceSmallerPhoto = new FloidServicePhoto();
                                    setMetaData(floidServiceSmallerPhoto, imagingSimpleDateFormat.format(date) + ".jpg", "JPEG", ".jpg");
                                    floidServiceSmallerPhoto.setImageBytes(smallerBitmapBytes);
                                    // Add the photos to the lists for processing:
                                    synchronized (mFloidService.mFloidPhotoSynchronizer) {
                                        if (mFloidService.mUseLogger)
                                            Log.d(TAG, "Image Reader [Photo]: Adding image to Floid Service Queue [Photos]");
                                        mFloidService.mPhotos.add(floidServicePhoto);
                                        mFloidService.mPhotosForUx.add(floidServiceSmallerPhoto);
                                    }
                                    if (mFloidService.mUseLogger)
                                        Log.d(TAG, "Image Reader [Photo]: Saving image: " + floidServicePhoto.getImageName());
                                    writeImagingFile(mContext, floidServicePhoto.getImageName(), bytes);
                                } catch (Exception e) {
                                    if (mFloidService.mUseLogger)
                                        Log.e(TAG, "Image Reader [Photo]: Failed to save image", e);
                                }
                            }
                        }).start();
                        doneTakingPhoto = true;
                        if(mFloidService.mUseLogger)
                            Log.d(TAG, "Image Reader [Photo]: Changing State: PICTURE TAKEN");
                        mFloidImagingStatus.setPhotoState(FloidImagingStatus.STATE_PICTURE_TAKEN);
                    } else {
                        if (image == null) {
                            if (mFloidService.mUseLogger)
                                Log.e(TAG, "Image Reader [Photo]: Image was null");
                        } else {
                            image.close();
                            int state = mFloidImagingStatus.getPhotoState();
                            // Log if we are in a bad state:
                            if(state != FloidImagingStatus.STATE_PREVIEW
                                    && state != FloidImagingStatus.STATE_WAITING_FOCUS_LOCK
                                    && state != FloidImagingStatus.STATE_WAITING_FOR_FOCUS_LOCK_IN_PREVIEW
                                    && state != FloidImagingStatus.STATE_WAITING_PRECAPTURE
                                    && state != FloidImagingStatus.STATE_WAITING_NON_PRECAPTURE) {
                                if (mFloidService.mUseLogger)
                                    Log.d(TAG, "Image Reader [Photo]: Warning - State is not PICTURE REQUESTED - " + mFloidImagingStatus.getPhotoState());
                            }
                        }
                    }
                } catch (Exception e) {
                    // Do nothing:
                    if (mFloidService.mUseLogger)
                        Log.e(TAG, "Image Reader [Photo]: Caught exception", e);
                    doneTakingPhoto = true;
                }
                if(doneTakingPhoto) {
                    synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
                        mFloidService.mFloidDroidStatus.setTakingPhoto(false);
                    }
                    mFloidImagingStatus.setStatusTakingPhoto(false);
                    mFloidImagingStatus.setStatusTakingVideoSnapshot(false); // Just in case
                    // Update the front end with the status:
                    mFloidService.uxSendImagingStatus();
                }
            }
        }, mBackgroundHandler);
        // Return our image handler:
        return imageReader;
    }

    /**
     * Create full file path from file name
     * @param fileName the file name
     * @return the full file path for this file
     */
    private String createFilePath(String fileName) {
        return mImagingDirectoryPath + File.separator + fileName;
    }

    /**
     * Set the floid id - also sets up appropriate imaging output directory
     * @param floidId the floid id
     */
    public void setFloidId(int floidId) {
        mFloidId = floidId;
        setupImagingOutputDirectory();
    }
    /**
     * Get the size of the photo from the parameters
     *
     * @param cameraDevice the cameraDevice
     * @param imageFormat the image format
     * @return array of two ints (w,h) for the size of the photo for this device
     */
    private int[] getSizeFromPhotoParameters(CameraDevice cameraDevice, int imageFormat) {
        int[] imageReaderSize = new int[2];
        String photoSizeString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_PHOTO_SIZE_OPTION);
        // Photo size:
        switch (photoSizeString) {
            case DROID_PARAMETERS_PHOTO_SIZE_DEFAULT: {
                int[] photoSize = FloidImagingUtil.findClosestPhotoSize(mCameraManager, cameraDevice, imageFormat, IMAGE_SIZE_1080, mFloidService.mUseLogger);
                imageReaderSize[0] = photoSize[0];
                imageReaderSize[1] = photoSize[1];
                if (mFloidService.mUseLogger)
                    Log.d(TAG, "getSizeFromPhotoParameters: picture size (DEFAULT):  " + photoSize[0] + ", " + photoSize[1]);
                break;
            }
            case DROID_PARAMETERS_PHOTO_SIZE_MAX: {
                int[] photoSize = FloidImagingUtil.findMaxPhotoSize(mCameraManager, cameraDevice, imageFormat);
                imageReaderSize[0] = photoSize[0];
                imageReaderSize[1] = photoSize[1];
                if (mFloidService.mUseLogger)
                    Log.d(TAG, "getSizeFromPhotoParameters: picture size (MAX):  " + photoSize[0] + ", " + photoSize[1]);
                break;
            }
            default:
                try {
                    imageReaderSize[0] = IMAGE_SIZE_640;    // At the end of the day we default to 640x480 for maximum support
                    imageReaderSize[1] = IMAGE_SIZE_480;
                    DroidValidParameters droidValidParameters = FloidService.getDroidParametersValidValues().get(DROID_PARAMETERS_PHOTO_SIZE_OPTION);
                    if(droidValidParameters!=null) {
                        int[] photoSize = droidValidParameters.getAsScreenSize(photoSizeString);
                        imageReaderSize[0] = photoSize[0];
                        imageReaderSize[1] = photoSize[1];
                        if (mFloidService.mUseLogger)
                            Log.d(TAG, "getSizeFromPhotoParameters: picture size (Parameter):  " + imageReaderSize[0] + ", " + imageReaderSize[1]);
                    } else {
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "getSizeFromPhotoParameters: DROID_PARAMETERS_PHOTO_SIZE_OPTION was null - using default:  " + imageReaderSize[0] + ", " + imageReaderSize[1]);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "getSizeFromPhotoParameters: caught exception ", e);
                }
                break;
        }
        // Return the image reader size:
        return imageReaderSize;
    }

    /**
     * Get the size of the photo from the parameters
     *
     * @param cameraDevice the cameraDevice
     * @return array of two ints (w,h) for the size of the photo for this device
     */
    private int[] getSizeFromVideoParameters(CameraDevice cameraDevice) {
        String videoSizeString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_SIZE_OPTION);
        return getVideoSize(mCameraManager, cameraDevice, videoSizeString);
    }

    /**
     * Get the photo format from the parameters for this camera device
     *
     * @param cameraDevice the camera device
     * @return the format
     */
    private int getFormatFromPhotoParameters(CameraDevice cameraDevice) {
        int imageReaderFormat = ImageFormat.JPEG; // Note: Only supported format
        String photoFormat = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_PHOTO_FORMAT_OPTION);
        // Photo format:
        switch (photoFormat) {
            case DROID_PARAMETERS_PHOTO_FORMAT_DEFAULT:
                if (mFloidService.mUseLogger)
                    Log.d(TAG, "getFormatFromPhotoParameters: photo format (DEFAULT): JPEG");
                //noinspection ReassignedVariable
                imageReaderFormat = ImageFormat.JPEG;
                break;
            case DROID_PARAMETERS_PHOTO_FORMAT_JPEG:
                if (mFloidService.mUseLogger)
                    Log.d(TAG, "getFormatFromPhotoParameters: photo format (JPEG): JPEG");
                //noinspection ReassignedVariable
                imageReaderFormat = ImageFormat.JPEG;
                break;
            default:
                if (mFloidService.mUseLogger)
                    Log.d(TAG, "getFormatFromPhotoParameters: Photo Error: Unknown Photo Format: " + photoFormat + " - returning false");
                break;
        }
        // Ok, now we want to verify if the output type is supported
        if (FloidImagingUtil.checkSupportedFormat(mCameraManager, cameraDevice, imageReaderFormat)) {
            // Return the image reader format:
            return imageReaderFormat;
        } else {
            Log.e(TAG, "getFormatFromPhotoParameters: unsupported image type for this camera device - returning ImageFormat.JPEG instead of " + imageReaderFormat);
            return ImageFormat.JPEG;
        }
    }

    /**
     * Get the video format from the parameters for this camera device
     *
     * @param cameraDevice the camera device
     * @return the format
     */
    @SuppressWarnings("SameReturnValue")
    private int getVideoStreamingImageReaderFormatFromVideoParameters(@SuppressWarnings({"unused", "UnusedParameters"}) CameraDevice cameraDevice) {
        return ImageFormat.YUV_420_888;
    }

    /**
     * Second phase of start video after cameras are guaranteed to be open
     */
    private void startVideo2() {
        logFloidImagingStatus("startVideo2 [begin]");
        // Check that we are in a good state to start video:
        if (!mFloidImagingStatus.isStatusInitialized()) {
            Log.e(TAG, "startVideo2: Not initialized");
            return;
        }
        if (!mFloidImagingStatus.isStatusTakingVideo()) {
            Log.e(TAG, "startVideo2: We are not set up to take video");
            return;
        }
        if (mFloidImagingStatus.isStatusTakingPhoto()) {
            Log.e(TAG, "startVideo2: Currently taking photo");
            return;
        }
        if (mVideoCameraDevice == null) {
            Log.e(TAG, "startVideo2: No video camera device");
            return;
        }
        if (mVideoMediaRecorder == null) {
            Log.e(TAG, "startVideo2: No video media recorder");
            return;
        }
        if (!mFloidImagingStatus.isStatusVideoSurfacesSetup()) {
            Log.e(TAG, "startVideo2: No video surfaces set up");
            return;
        }
        if (!mFloidImagingStatus.isStatusVideoCaptureRequestSetup()) {
            Log.e(TAG, "startVideo2: No video capture request setup");
            return;
        }
        // We should be all good to go, start the capture request:
        try {
            // Set up the repeating request for the capture session:
            if(mVideoStreamingCaptureRequestBuilder == null) {
                Log.e(TAG, "startVideo2: mVideoStreamingCaptureRequestBuilder is null");
                return;
            }
            final CaptureRequest mVideoStreamingCaptureRequest = mVideoStreamingCaptureRequestBuilder.build();
            logCameraCaptureRequest("mVideoStreamingCaptureRequest", mVideoStreamingCaptureRequest);
            mVideoCameraCaptureSession.setRepeatingRequest(mVideoStreamingCaptureRequest, null, mBackgroundHandler);
            // Start the video
            mVideoMediaRecorder.start();
            if(mFloidService.mUseLogger)
                Log.d(TAG, "startVideo2 - Starting media recorder");
            // Update the status:
            mFloidImagingStatus.setStatusTakingVideo(true);
            if (mFloidService.mUseLogger)
                Log.d(TAG, "  Taking video");
            synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
                mFloidService.mFloidDroidStatus.setTakingVideo(true);
            }
        } catch (CameraAccessException cae) {
            Log.e(TAG, "Capture Session onConfigured Exception - closing cameras", cae);
            closeCameras();
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("startVideo2 [end]");
    }

    /**
     * Stop the video
     */
    public void stopVideo() {
        logFloidImagingStatus("stopVideo [begin]");
        try {
            if(mFloidImagingStatus.isStatusTakingVideo()) {
                // Stop the repeating capture session
                if (mVideoCameraCaptureSession != null) {
                    try {
                        mVideoCameraCaptureSession.stopRepeating();
                    } catch (Exception e2) {
                        Log.e(TAG, "Exception caught during stop repeating", e2);
                    }
                    try {
                        mVideoCameraCaptureSession.abortCaptures();
                    } catch (Exception e2) {
                        Log.e(TAG, "Exception caught during abort capture", e2);
                    }
                }
                if (mVideoMediaRecorder != null) {
                    mVideoMediaRecorder.stop();
                    mVideoMediaRecorder.reset();
                    mVideoMediaRecorder.release();
                    mVideoMediaRecorder = null;
                    // Scan the media file here:
                    FloidImagingUtil.scanMediaFile(mContext, mVideoMediaRecorderFileName);
                }
                synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
                    mFloidService.mFloidDroidStatus.setTakingVideo(false);
                }
                mFloidImagingStatus.setStatusTakingVideo(false);
            }
        } catch (Exception e) {
            Log.e(TAG, "stopVideo: caught exception - closing cameras", e);
        }
        closeCameras(); // Unfortunately we have to do this to get all of the surfaces set back up
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("stopVideo [end]");
    }

    /**
     * Take a photo - if the cameras need reopening, then that callback will call takePhoto2 - otherwise we do
     *
     * @return true if we either started taking a photo or are reopening cameras to do so
     */
    public boolean takePhoto() {
        logFloidImagingStatus("takePhoto [begin]");
        // Check that we are in a good state to take a photo:
        if (!mFloidImagingStatus.isStatusInitialized()) {
            Log.e(TAG, "takePhoto: Not initialized");
            return false;
        }
        if (mFloidImagingStatus.isStatusTakingPhoto()) {
            Log.e(TAG, "takePhoto: Currently taking photo");
            return false;
        }
        // Are we taking a video and do we support video snapshot?
        if(mFloidImagingStatus.isStatusTakingVideo()) {
            if(mPhotoCameraDevice==mVideoCameraDevice) {
                if (!mFloidImagingStatus.isStatusSupportsVideoSnapshot()) {
                    if (mFloidService.mUseLogger)
                        Log.d(TAG, "takePhoto: taking video but video snapshot not supported - returning false");
                    return false;
                }
            }
        }
        // Make sure we are not taking a photo:
        synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
            if (mFloidService.mFloidDroidStatus.isTakingPhoto()) {
                if (mFloidService.mUseLogger)
                    Log.d(TAG, "takePhoto: already taking photo - returning false");
                return false; // Video already started
            }
            // Still here - bump the number:
            mFloidService.mFloidDroidStatus.setPhotoNumber(mFloidService.mFloidDroidStatus.getPhotoNumber() + 1);
        }
        if(mFloidImagingStatus.isStatusTakingVideo() && mPhotoCameraDevice==mVideoCameraDevice) {
            mFloidImagingStatus.setStatusTakingVideoSnapshot(true);
        }
        mFloidImagingStatus.setStatusTakingPhoto(true); // We need this flag to later determine what path we are on in the callbacks
        // checkReopenCameras will startVideo or takePhoto if it reopens the cameras - otherwise we do it:
        try {
            if (!checkReopenCameras()) {
                if (mPhotoCameraDevice != null) {
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "Did not modify camera - calling takePhoto2 directly");
                    // Yes, take the photo (or video snapshot...):
                    takePhoto2();
                } else {
                    Log.e(TAG, "ERROR AFTER checkReopenCameras - the photo camera was null when if should not have been");
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception checkReopenCameras or takePhoto2: ", e);
        }

        logFloidImagingStatus("takePhoto [end]");
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        return true;
    }

    /**
     * Second part of take a photo, take either a photo or video snapshot if we are also recording video
     */
    private void takePhoto2() {
        logFloidImagingStatus("takePhoto2 [begin]");
        // If we are taking a video we do a snapshot:
        if (mFloidImagingStatus.isStatusTakingVideo() && mPhotoCameraDevice==mVideoCameraDevice) {
            try {
                takeVideoSnapshotActual();
            } catch (Exception e3) {
                Log.e(TAG, "Caught exception calling takeVideoSnapshotActual from takePhoto2 - closing cameras", e3);
                closeCameras();
            }
        } else {
            if (mFloidImagingStatus.isStatusTakingPhoto()) {
                try {
                    takePhotoActual();
                } catch (Exception e3) {
                    Log.e(TAG, "Caught exception calling takeVideoSnapshotActual from takePhoto2 - closing cameras", e3);
                    closeCameras();
                }
            } else {
                Log.e(TAG, "takePhoto2 called while not taking photo or video - closing cameras");
                closeCameras();
            }
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
    }

    /**
     * Set the photo quality for this capture request builder from the parameters
     *
     * @param captureRequestBuilder the capture request builder
     */
    private void setPhotoQualityToCaptureRequestBuilderFromParameters(CaptureRequest.Builder captureRequestBuilder) {
        // Photo quality:
        byte photoQualityByte = VIDEO_QUALITY_100;
        try {
            String photoQualityString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_PHOTO_QUALITY_OPTION);
            if (photoQualityString.equals(DROID_PARAMETERS_PHOTO_QUALITY_DEFAULT)) {
                if (mFloidService.mUseLogger)
                    Log.d(TAG, "photo quality (DEFAULT): " + photoQualityByte);
            } else {
                try {
                    photoQualityByte = Byte.valueOf(photoQualityString);
                    if (mFloidService.mUseLogger)
                        Log.d(TAG, "photo quality (parameter): " + photoQualityByte);
                } catch (Exception e) {
                    if (mFloidService.mUseLogger)
                        Log.d(TAG, "Photo Error: Bad photo quality value: " + photoQualityString + " - setting to 100 (Max quality)");
                    photoQualityByte = 100;
                }
                if ((photoQualityByte > 100) || (photoQualityByte < 1)) {
                    if (mFloidService.mUseLogger)
                        Log.d(TAG, "Photo Error: Bad photo quality value: " + photoQualityString + " - must be in range 1-100 - setting to 100 (Max quality)");
                    photoQualityByte = 100;
                }
            }
        } catch (Exception e2) {
            Log.e(TAG, "Failed to get image quality (photo) - closing cameras", e2);
            closeCameras();
            return;
        }
        if (captureRequestBuilder == null) {
            Log.e(TAG, "Capture request builder was null (photo) - closing cameras");
            closeCameras();
            return;
        }
        captureRequestBuilder.set(CaptureRequest.JPEG_QUALITY, photoQualityByte);
    }

    /**
     * Set the location for this capture request builder from the parameters
     *
     * @param captureRequestBuilder the capture request builder
     */
    private void setLocationToCaptureRequestBuilder(CaptureRequest.Builder captureRequestBuilder) {
        try {
            // Set the location in altitude, latitude, longitude:
            double photoGpsAltitude = mFloidService.getFloidStatus().isAi() ? mFloidService.getFloidStatus().getAa() : mFloidService.getFloidStatus().getGz();
            double photoGpsLatitude = mFloidService.getFloidStatus().getGy();
            double photoGpsLongitude = mFloidService.getFloidStatus().isAi() ? mFloidService.getFloidStatus().getAa() : mFloidService.getFloidStatus().getGz();
            if (mFloidService.mUseLogger)
                Log.d(TAG, "Altitude: " + (mFloidService.getFloidStatus().isAi() ? mFloidService.getFloidStatus().getAa() : mFloidService.getFloidStatus().getGz()) + "  Latitude: " + mFloidService.getFloidStatus().getGy() + "  Longitude: " + mFloidService.getFloidStatus().getGx());
            Location pictureLocation = new Location("");
            pictureLocation.setAltitude(photoGpsAltitude);
            pictureLocation.setLongitude(photoGpsLongitude);
            pictureLocation.setLatitude(photoGpsLatitude);
            captureRequestBuilder.set(CaptureRequest.JPEG_GPS_LOCATION, pictureLocation);
        } catch (Exception e) {
            Log.e(TAG, "Caught exception setting location to capture request builder - closing cameras", e);
            closeCameras();
        }
    }

    /**
     * Take a photo actual (photo not video snapshot)
     */
    private void takePhotoActual() {
        logFloidImagingStatus("Take Photo Actual: [begin]");
        // Take a picture (photo) and use our callback:
        try {
            synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
                mFloidService.mFloidDroidStatus.setTakingPhoto(true);
                mFloidService.mLastPhotoTime = System.currentTimeMillis();
            }
            // Set the location and photo quality to the request builder:
            setLocationToCaptureRequestBuilder(mPhotoCaptureRequestBuilder);
            setPhotoQualityToCaptureRequestBuilderFromParameters(mPhotoCaptureRequestBuilder);
        } catch (Exception e) {
            Log.e(TAG, "Take Photo Actual: Exception starting - closing cameras", e);
            closeCameras();
            return;
        }
        // Create a captureRequest that uses the surfaces already set up to take a photo
        try {
            // 1. Tell the camera to start a preview and then in 200ms, take a picture:
            if(mFloidService.mUseLogger)
                Log.d(TAG, "Take Photo Actual: Changing State: PREVIEW");
            mFloidImagingStatus.setPhotoState(FloidImagingStatus.STATE_PREVIEW);
            final CaptureRequest photoPreviewCaptureRequest = mPhotoPreviewCaptureRequestBuilder.build();
            logCameraCaptureRequest("photoPreviewCaptureRequest", photoPreviewCaptureRequest);
            mPhotoCameraCaptureSession.setRepeatingRequest(photoPreviewCaptureRequest, mPhotoPreviewCaptureCallback, mBackgroundHandler);
            // 2. Post a delayed handler to start the auto focus, giving the camera time to open for the preview...
            if(mFloidService.mUseLogger)
                Log.d(TAG, "Take Photo Actual: Posting delayed auto focus trigger");
            new Thread(new Runnable() {
                public void run() {
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "Delayed Auto Focus Trigger: Starting");
                    // Tell the camera to execute the auto focus sequence:
                    mPhotoPreviewCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
                    // Tell #mPhotoCaptureCallback to wait for the lock.
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "Delayed Auto Focus Trigger: Changing State: WAITING LOCK");
                    mFloidImagingStatus.setPhotoState(FloidImagingStatus.STATE_WAITING_FOCUS_LOCK);
                    try {
                        final CaptureRequest photoPreviewCaptureRequest = mPhotoPreviewCaptureRequestBuilder.build();
                        logCameraCaptureRequest("photoPreviewCaptureRequest", photoPreviewCaptureRequest);
                        mPhotoCameraCaptureSession.capture(photoPreviewCaptureRequest, mPhotoAutoFocusCaptureCallback, mBackgroundHandler);
                    } catch (Exception e) {
                        Log.e(TAG, "Delayed Auto Focus Trigger: Exception: ", e);
                        closeCameras();
                    }
                    // Turn off the trigger in the builder:
                    mPhotoPreviewCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, null);
                }
            }).start();// , PHOTO_PREVIEW_REQUEST_DELAY);
        } catch (Exception e) {
            if(mFloidService.mUseLogger)
                Log.e(TAG, "Take Photo Actual: Exception requesting capture from mPhotoCameraCaptureSession - closing cameras", e);
            closeCameras();
            return;
        }
        // We are taking a photo:
        if (mFloidService.mUseLogger)
            Log.d(TAG, "Done take photo actual..wait for callback");
        logFloidImagingStatus("Take Photo Actual: [end]");
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
    }


    private void captureStillPicture() {
        try {
/*
            // Turn off the previewer:
            mPhotoCameraCaptureSession.stopRepeating();
*/
            // Change the state:
            if(mFloidService.mUseLogger)
                Log.d(TAG, "Capture Still Picture: Changing State: PICTURE REQUESTED");
            mFloidImagingStatus.setPhotoState(FloidImagingStatus.STATE_PICTURE_REQUESTED);
            // Send the capture request:
            final CaptureRequest photoCaptureRequest = mPhotoCaptureRequestBuilder.build();
            logCameraCaptureRequest("photoCaptureRequest", photoCaptureRequest);
            mPhotoCameraCaptureSession.capture(photoCaptureRequest, mPhotoTakePictureCaptureCallback, mBackgroundHandler);
        } catch (Exception e) {
          Log.e(TAG, "Caught exception in captureStillPhoto", e);
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
    }

    /**
     * Shorthand logger for photo camera capture callback logging
     * @param methodName the method name that called
     * @param completed true if was called from completed handler - false if from progressed
     * @param state the state of the floid imaging photo capture at the time this log was called (not necessarily at the time the capture was requested)
     * @param afState the auto focus state
     * @param aeState the auto exposure state
     */
    private void ccLog(String methodName, boolean completed, int state, Integer afState, Integer aeState) {
        if(!mFloidService.mUseLogger) return;
        String stateString = FloidImagingStatus.getStateName(state) + " [" + (mFloidImagingStatus.getPhotoState()+ "]");
        // Calculate the auto focus state string:
        String afString;
        if(afState == null) {
            afString = "(null)";
        }
        else {
            switch (afState) {
                case CaptureResult.CONTROL_AF_STATE_INACTIVE:
                    afString = "INACTIVE";
                    break;
                case CaptureResult.CONTROL_AF_STATE_PASSIVE_SCAN:
                    afString = "PASSIVE SCAN";
                    break;
                case CaptureResult.CONTROL_AF_STATE_PASSIVE_FOCUSED:
                    afString = "PASSIVE FOCUSED";
                    break;
                case CaptureResult.CONTROL_AF_STATE_ACTIVE_SCAN:
                    afString = "ACTIVE SCAN";
                    break;
                case CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED:
                    afString = "FOCUSED LOCKED";
                    break;
                case CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED:
                    afString = "NOT FOCUSED LOCKED";
                    break;
                case CaptureResult.CONTROL_AF_STATE_PASSIVE_UNFOCUSED:
                    afString = "PASSIVE UNFOCUSED";
                    break;
                default:
                    afString = "UNKNOWN (" + afState + ")";
                    break;
            }
        }
        // Calculate the auto exposure state string:
        String aeString;
        if(aeState == null) {
            aeString = "(null)";
        }
        else {
            switch (aeState) {
                case CaptureResult.CONTROL_AE_STATE_CONVERGED:
                    aeString = "CONVERGED";
                    break;
                case CaptureResult.CONTROL_AE_STATE_FLASH_REQUIRED:
                    aeString = "FLASH REQUIRED";
                    break;
                case CaptureResult.CONTROL_AE_STATE_INACTIVE:
                    aeString = "INACTIVE";
                    break;
                case CaptureResult.CONTROL_AE_STATE_LOCKED:
                    aeString = "LOCKED";
                    break;
                case CaptureResult.CONTROL_AE_STATE_PRECAPTURE:
                    aeString = "PRECAPTURE";
                    break;
                case CaptureResult.CONTROL_AE_STATE_SEARCHING:
                    aeString = "SEARCHING";
                    break;
                default:
                    aeString = "UNKNOWN (" + aeState + ")";
                    break;
            }
        }
        // Ok log:
        Log.d(TAG, methodName
                + ": "
                + (completed?"[COMPLETED]":"[PARTIAL]")
                + "  State: " + stateString
                + "  AF: " + afString
                + "  AE: " + aeString
        );
    }

    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private final CameraCaptureSession.CaptureCallback mPhotoTakePictureCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        private void process(CaptureResult result, boolean completed) {
            Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
            Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
            ccLog("Photo TakePicture", completed, mFloidImagingStatus.getPhotoState(), afState, aeState);
            boolean moveOn = false;

            if((mFloidImagingStatus.getPhotoState() == FloidImagingStatus.STATE_PICTURE_TAKEN) || completed) {
                moveOn = true;
            }
            if(moveOn) {
                unlockFocus();
            }
            // Update the front end with the status:
            mFloidService.uxSendImagingStatus();
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult, false);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result, true);
        }
    };

    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to photo preview capture.
     */
    private final CameraCaptureSession.CaptureCallback mPhotoPreviewCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        private void process(CaptureResult result, boolean completed) {
            Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
            Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
            ccLog("Photo Preview", completed, mFloidImagingStatus.getPhotoState(), afState, aeState);

            // For legacy hardware, we do not get all the callbacks in the autofocus callback handler:
            if(mFloidImagingStatus.isHardwareIsLegacy()
                && (mFloidImagingStatus.getPhotoState() == FloidImagingStatus.STATE_WAITING_FOR_FOCUS_LOCK_IN_PREVIEW)
                && (afState != null)
                && (   (afState == CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED)
                    || (afState == CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED)
                    || (afState == CaptureResult.CONTROL_AF_STATE_PASSIVE_FOCUSED) )) {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Photo Preview: Moving On (Legacy Hardware) [Run PreCapture Sequence]");
                runPrecaptureSequence();
            }
            // For non-legacy:
            // If waiting for focus lock and it is focus locked or ae is converged, we move on:
            if(!mFloidImagingStatus.isHardwareIsLegacy()
                    && (mFloidImagingStatus.getPhotoState() == FloidImagingStatus.STATE_WAITING_FOR_FOCUS_LOCK_IN_PREVIEW
                        || mFloidImagingStatus.getPhotoState() == FloidImagingStatus.STATE_WAITING_FOCUS_LOCK)
                    && ((afState != null && afState == CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED)
                        || (aeState != null && aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED)))
            {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Photo Preview: Moving On (Modern Hardware) [Focus is Locked]");
                runPrecaptureSequence();
            }
            // Update the front end with the status:
            mFloidService.uxSendImagingStatus();
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult, false);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result, true);
        }
    };

    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private final CameraCaptureSession.CaptureCallback mPhotoAutoFocusCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @SuppressWarnings("DuplicateBranchesInSwitch")
        private void process(CaptureResult result, boolean completed) {
            Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
            Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                ccLog("Photo AutoFocus", completed, mFloidImagingStatus.getPhotoState(), afState, aeState);
            if(mFloidImagingStatus.getPhotoState() == FloidImagingStatus.STATE_WAITING_FOCUS_LOCK) {
                boolean moveOn = false;
                if (afState == null) {
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "Photo AutoFocus: AF State is null - moving on");
                    moveOn = true;
                } else {
                    switch (afState) {
                        case CaptureResult.CONTROL_AF_STATE_INACTIVE:
                            if(aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                                if(mFloidService.mUseLogger)
                                    Log.d(TAG, "Photo AutoFocus: AF State is inactive and AE State is converged - moving on");
                                moveOn = true;
                            }
                            // Bummer - not sure what to do here
                            break;
                        case CaptureResult.CONTROL_AF_STATE_PASSIVE_SCAN:
                            // OK - it is a passive scan - if it is legacy hardware, we will let the preview catch this field
                            if (mFloidImagingStatus.isHardwareIsLegacy()) {
                                if(mFloidService.mUseLogger) {
                                    Log.d(TAG, "Photo AutoFocus: Legacy Hardware - preview should pick up the focus");
                                    Log.d(TAG, "Photo AutoFocus: Changing State: WAITING FOR FOCUS LOCK IN PREVIEW");
                                }
                                mFloidImagingStatus.setPhotoState(FloidImagingStatus.STATE_WAITING_FOR_FOCUS_LOCK_IN_PREVIEW);
                            }
                            break;
                        case CaptureResult.CONTROL_AF_STATE_PASSIVE_FOCUSED:
                            // Passive focused - good enough!
                            moveOn = true;
                            break;
                        case CaptureResult.CONTROL_AF_STATE_ACTIVE_SCAN:
                            // OK - it is an active scan - if it is legacy hardware, we will let the preview catch this field
                            if (mFloidImagingStatus.isHardwareIsLegacy()) {
                                if(mFloidService.mUseLogger) {
                                    Log.d(TAG, "Photo AutoFocus: Legacy Hardware - preview should pick up the focus");
                                    Log.d(TAG, "Photo AutoFocus: Changing State: WAITING FOR FOCUS LOCK IN PREVIEW");
                                }
                                mFloidImagingStatus.setPhotoState(FloidImagingStatus.STATE_WAITING_FOR_FOCUS_LOCK_IN_PREVIEW);
                            }
                            break;
                        case CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED:
                            // Focus focused - great!
                            moveOn = true;
                            break;
                        case CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED:
                            // Locked - ok - well anyway let's just move on...
                            moveOn = true;
                            break;
                        case CaptureResult.CONTROL_AF_STATE_PASSIVE_UNFOCUSED:
                            // Not really great - let's wait (?)
                            break;
                        default:
                            // Bummer - ok - well anyway let's move on:
                            moveOn = true;
                            break;
                    }
                }
                // Determine if we take the next step of running the precapture sequence:
                if (moveOn) {
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "Photo AutoFocus: Moving On [Run PreCapture Sequence]");
                    runPrecaptureSequence();
                } else {
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "Photo AutoFocus: Staying");
                }
                // Update the front end with the status:
                mFloidService.uxSendImagingStatus();
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult, false);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result, true);
        }
    };

    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to photo precapture.
     */
    private final CameraCaptureSession.CaptureCallback mPhotoPrecaptureCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        private void process(CaptureResult result, boolean completed) {
            Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
            Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
            ccLog("Photo PreCapture", completed, mFloidImagingStatus.getPhotoState(), afState, aeState);
            boolean moveOn = false;
            if(completed) {
                moveOn = true;
            }
            if(aeState == null) {
                moveOn = true;
            } else {
                switch (aeState) {
                    case CaptureResult.CONTROL_AE_STATE_CONVERGED:
                        // Great!!
                        moveOn = true;
                        break;
                    case CaptureResult.CONTROL_AE_STATE_FLASH_REQUIRED:
                        // No problem
                        moveOn = true;
                        break;
                    case CaptureResult.CONTROL_AE_STATE_INACTIVE:
                        // Probably should not be inactive - perhaps we should move right along (?)
                        moveOn = true;
                        break;
                    case CaptureResult.CONTROL_AE_STATE_LOCKED:
                        // Locked - guess that means we are done
                        moveOn = true;
                        break;
                    case CaptureResult.CONTROL_AE_STATE_PRECAPTURE:
                        // Ok - that is good news - let's change our state
                        if(mFloidImagingStatus.getPhotoState() == FloidImagingStatus.STATE_WAITING_PRECAPTURE) {
                            if(mFloidService.mUseLogger)
                                Log.d(TAG, "Photo PreCapture: Changing state from precapture to capture");
                            mFloidImagingStatus.setPhotoState(FloidImagingStatus.STATE_WAITING_NON_PRECAPTURE);
                            // Update the front end with the status:
                            mFloidService.uxSendImagingStatus();
                        }
                        else {
                            if(mFloidService.mUseLogger)
                                Log.d(TAG, "Photo PreCapture: Received PRECAPTURE state while waiting for a non-PRECAPTURE state");
                        }
                        break;
                    case CaptureResult.CONTROL_AE_STATE_SEARCHING:
                        // Groovy - keep going...
                        break;
                    default:
                        // Bummer - ok - well anyway let's move on:
                        moveOn = true;
                        break;
                }
            }
            // Determine if we take the next step of capturing a still picture:
            if(moveOn && (mFloidImagingStatus.getPhotoState() == FloidImagingStatus.STATE_WAITING_PRECAPTURE) || (mFloidImagingStatus.getPhotoState() == FloidImagingStatus.STATE_WAITING_NON_PRECAPTURE)) {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Photo PreCapture: Moving On: [Capture Still Picture]");
                captureStillPicture();
            } else if(moveOn) {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Photo PreCapture: Not Moving On");
            } else {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Photo PreCapture: Staying");
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult, false);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result, true);
        }
    };

    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private final CameraCaptureSession.CaptureCallback mPhotoUnlockFocusCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        private void process(CaptureResult result, boolean completed) {
            Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
            Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
            ccLog("Photo Unlock Focus", completed, mFloidImagingStatus.getPhotoState(), afState, aeState);

            // OK, we have unlocked the focus; turn off the previewer:
            try {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Photo Unlock Focus: Stopping preview repeat");
                mPhotoCameraCaptureSession.stopRepeating();
            } catch (Exception e) {
                Log.e(TAG, "Photo Unlock Focus: Exception in stop repeating: ", e);
            }
        }
        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult, false);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result, true);
        }
    };

    /**
     * Unlock the focus. This method should be called when still image capture sequence is
     * finished.
     */
    private void unlockFocus() {
        try {
            // After this, the camera will go back to the normal state of preview.
            if(mFloidService.mUseLogger)
                Log.d(TAG, "Unlocking focus");
            // Reset the auto-focus trigger
            mPhotoPreviewCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            // Make the capture request:
            final CaptureRequest photoPreviewCaptureRequest = mPhotoPreviewCaptureRequestBuilder.build();
            logCameraCaptureRequest("photoPreviewCaptureRequest", photoPreviewCaptureRequest);
            mPhotoCameraCaptureSession.capture(photoPreviewCaptureRequest, mPhotoUnlockFocusCaptureCallback, mBackgroundHandler);
            // Turn off the trigger:
            mPhotoPreviewCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, null);
        } catch (CameraAccessException e) {
            Log.e(TAG, "Exception unlocking focus", e);
            e.printStackTrace();
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
    }

    /**
     * Set the auto flash if it is supported
     * @param requestBuilder the request builder
     */
    private void setAutoFlash(CaptureRequest.Builder requestBuilder) {
        if (mFloidImagingStatus.isFlashSupported()) {
            requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                    CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
        }
    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in {@link #mPhotoPrecaptureCaptureCallback} from {@link #takePhotoActual()}.
     */
    private void runPrecaptureSequence() {
        try {
            if(mFloidImagingStatus.getPhotoState()>=FloidImagingStatus.STATE_WAITING_PRECAPTURE) {
                Log.e(TAG, "Photo state not appropriate for running precapture: " + mFloidImagingStatus.getPhotoState());
                return;
            }
            // This is how to tell the camera to trigger.
            mPhotoPreviewCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mPhotoTakePictureCaptureCallback to wait for the precapture sequence to be set.
            if(mFloidService.mUseLogger)
                Log.d(TAG, "Run Precapture Sequence: Changing State: WAITING PRECAPTURE");
            mFloidImagingStatus.setPhotoState(FloidImagingStatus.STATE_WAITING_PRECAPTURE);
            // Make the capture request:
            final CaptureRequest photoPreviewCaptureRequest = mPhotoPreviewCaptureRequestBuilder.build();
            logCameraCaptureRequest("photoPreviewCaptureRequest", photoPreviewCaptureRequest);
            mPhotoCameraCaptureSession.capture(photoPreviewCaptureRequest, mPhotoPrecaptureCaptureCallback, mBackgroundHandler);
            // Turn off the trigger in the builder:
            mPhotoPreviewCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
    }

    /**
     * Take a video snapshot actual (not photo)
     */
    private void takeVideoSnapshotActual() {
        logFloidImagingStatus("takeVideoSnapshotActual [begin]");
        if (mFloidImagingStatus.isStatusSupportsVideoSnapshot()) {
            mFloidImagingStatus.setStatusTakingVideoSnapshot(true);
            // Take a picture (photo) and use our callback:
            synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
                mFloidService.mFloidDroidStatus.setTakingPhoto(true);
                mFloidService.mLastPhotoTime = System.currentTimeMillis();
            }
            // Set the location and photo quality to the request builder:
            setLocationToCaptureRequestBuilder(mVideoSnapshotCaptureRequestBuilder);
            setPhotoQualityToCaptureRequestBuilderFromParameters(mVideoSnapshotCaptureRequestBuilder);
            try {
                final CaptureRequest mVideoSnapshotCaptureRequest = mVideoSnapshotCaptureRequestBuilder.build();
                logCameraCaptureRequest("mVideoSnapshotCaptureRequest", mVideoSnapshotCaptureRequest);
                mVideoCameraCaptureSession.capture(mVideoSnapshotCaptureRequest, null, mBackgroundHandler);
            } catch (Exception e) {
                Log.e(TAG, "Exception requesting video snapshot capture from mVideoCameraCaptureSession - closing cameras", e);
                closeCameras();
                return;
            }
            // We are taking a video snapshot:
            if (mFloidService.mUseLogger)
                Log.d(TAG, "Done take video snapshot actual..wait for callback");
        } else {
            Log.e(TAG, "Video camera does not support video snapshot...");
            closeCameras();
            return;
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("takeVideoSnapshotActual [end]");
    }

    /**
     * Print out the keys from a camera capture request
     * @param label the capture request label
     * @param captureRequest the capture request
     */
    private void logCameraCaptureRequest(String label, CaptureRequest captureRequest) {
        if(mFloidService.mUseLogger) {
            for (CaptureRequest.Key<?> captureRequestKey : captureRequest.getKeys()) {
                try {
                    final Object captureRequestValue = captureRequest.get(captureRequestKey);
                    if (captureRequestValue != null) {
                        Log.d(TAG, label + " " + captureRequestKey.toString() + " = " + captureRequestValue);
                    } else {
                        Log.d(TAG, label + " Capture Request Value was null for " + captureRequestKey.toString());
                    }
                } catch (Exception e) {
                    Log.e(TAG, label + " Error getting key " + captureRequestKey.toString());
                }
            }
        }
    }

    @SuppressWarnings("SameParameterValue")
    private static MediaRecorder createMediaRecorder(
            final @NonNull Context context,
            final int width,
            final int height,
            final int videoSource,
            final int videoEncoder,
            final int videoFrameRate,
            final int videoEncodingBitRate,
            final int maxFileSize,
            final int maxDuration,
            final int audioSource,
            final int audioEncoder,
            final int outputFormat,
            final String outputFileName,
            final int audioSampleRate,
            final int audioBitRate,
            final float latitude,
            final float longitude,
            final int orientation,
            MediaRecorder.OnErrorListener onErrorListener,
            MediaRecorder.OnInfoListener onInfoListener
    ) {
        MediaRecorder mediaRecorder = new MediaRecorder(context);
        try {
            // Set up media recorder:
            mediaRecorder.setAudioSource(audioSource);
            mediaRecorder.setVideoSource(videoSource);
            mediaRecorder.setOutputFormat(outputFormat);
            mediaRecorder.setOutputFile(outputFileName);
            mediaRecorder.setOrientationHint(orientation);
            mediaRecorder.setVideoEncodingBitRate(videoEncodingBitRate);
            mediaRecorder.setVideoFrameRate(videoFrameRate);
            mediaRecorder.setVideoSize(width, height);
            mediaRecorder.setMaxDuration(maxDuration);
            mediaRecorder.setMaxFileSize(maxFileSize);
            mediaRecorder.setAudioSamplingRate(audioSampleRate);
            mediaRecorder.setAudioEncodingBitRate(audioBitRate);
            mediaRecorder.setLocation(latitude, longitude);
            mediaRecorder.setOnErrorListener(onErrorListener);
            mediaRecorder.setOnInfoListener(onInfoListener);
            mediaRecorder.setVideoEncoder(videoEncoder);
            mediaRecorder.setAudioEncoder(audioEncoder);
            mediaRecorder.prepare();
        } catch (Exception e) {
            Log.d(TAG, "Exception in createMediaRecorder", e);
            return null;
        }
        return mediaRecorder;
    }

    private static String safeMapStringGet(Map<String, String> map, String key) {
        String value = map.get(key);
        return value!=null?value:"";
    }
    /**
     * Set up the media recorder
     *
     * @return true unless error
     */
    @SuppressWarnings("UnusedReturnValue")
    private boolean setupMediaRecorder() {
        logFloidImagingStatus("setupMediaRecorder [begin]");

        // Get parameters:
        int videoSource = MediaRecorder.VideoSource.SURFACE;  // Required by Camera 2 API
        String videoOutputFileExtension = VIDEO_FILE_EXTENSION_DEFAULT;
        String videoEncoderString = safeMapStringGet(mFloidService.mDroidParametersHashMap,DROID_PARAMETERS_VIDEO_ENCODER_OPTION);
        int videoEncoder = MediaRecorder.VideoEncoder.DEFAULT;
        String videoSizeString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_SIZE_OPTION);
        int[] videoSize = {1920, 1080}; // Defaults - will get overridden below
        String videoFrameRateString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_FRAME_RATE_OPTION);  // STRING OR INT
        int videoFrameRate = VIDEO_DEFAULT_FRAME_RATE;
        int videoEncodingBitRate = (videoSize[0] * videoSize[1] * 3 * 8 * videoFrameRate) / 200;
        String videoStreamFramePeriodString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_OPTION);  // STRING OR INT
        String videoOutputFormatString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_OPTION);
        int videoOutputFormat = MediaRecorder.OutputFormat.DEFAULT;
        String videoMaxDurationString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_MAX_DURATION_OPTION); // STRING OR INT
        int videoMaxDuration = 0;
        String videoMaxFileSizeString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_OPTION); // STRING OR INT
        int videoMaxFileSize = 0;
        String videoAudioSourceString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_OPTION);
        int videoAudioSource = MediaRecorder.AudioSource.DEFAULT;
        String videoAudioEncoderString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_OPTION);
        int videoAudioEncoder = MediaRecorder.AudioEncoder.DEFAULT;
        String videoAudioSampleRateString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_OPTION);
        int videoAudioSampleRate = HARD_CODED_DEFAULT_AUDIO_SAMPLE_RATE;
        String videoAudioBitRateString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_OPTION);
        int videoAudioBitRate = HARD_CODED_DEFAULT_AUDIO_BIT_RATE;
        String videoDisableShutterSoundString = safeMapStringGet(mFloidService.mDroidParametersHashMap, DROID_PARAMETERS_VIDEO_DISABLE_SHUTTER_SOUND_OPTION);
        float latitude = 0.0f;
        float longitude = 0.0f;
        // Log parameters:
        if (mFloidService.mUseLogger) Log.d(TAG, "Parameters: ");
        if (mFloidService.mUseLogger) Log.d(TAG, "    Video Source is Surface");
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    Video Size String:        " + videoSizeString);
        if (mFloidService.mUseLogger) Log.d(TAG, "    video Encoder:            " + videoEncoderString);
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    videoFrameRateString:       " + videoFrameRateString);
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    videoOutputFormat:          " + videoOutputFormatString);
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    videoMaxDurationString:     " + videoMaxDurationString);
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    videoMaxFileSizeString:     " + videoMaxFileSizeString);
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    videoAudioSource:           " + videoAudioSourceString);
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    videoAudioEncoder:          " + videoAudioEncoderString);
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    videoAudioSampleRateString: " + videoAudioSampleRateString);
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    videoAudioBitRateString:    " + videoAudioBitRateString);
        if (mFloidService.mUseLogger)
            Log.d(TAG, "    videoDisableShutterSound:   " + videoDisableShutterSoundString);
        // Parse parameters:
        try {
            // Video Preview Frame Rate:
            //noinspection SwitchStatementWithTooFewBranches
            switch (videoStreamFramePeriodString) {
                case DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_DEFAULT:
                    if (mFloidService.mUseLogger) Log.d(TAG, String.format("  VideoStreamFramePeriod: (Default): %d", VIDEO_DEFAULT_STREAM_FRAME_PERIOD));
                    mVideoStreamFramePeriod = VIDEO_DEFAULT_STREAM_FRAME_PERIOD;
                    break;
                default:
                    try {
                        int videoStreamFramePeriodInt = FloidService.getDroidParametersValidValues().get(DROID_PARAMETERS_VIDEO_STREAM_FRAME_PERIOD_OPTION).getAsValidInt(videoStreamFramePeriodString);
                        if (mFloidService.mUseLogger)
                            Log.d(TAG, String.format("  VideoStreamFramePeriod (Option): %d", videoStreamFramePeriodInt));
                        mVideoStreamFramePeriod = videoStreamFramePeriodInt;
                    } catch (Exception e) {
                        // Log an error and exit:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, String.format("Video camera: Bad video preview frame period: %s", videoStreamFramePeriodString));
                        return false;
                    }
                    break;
            }
            // Video Size:
            videoSize = getVideoSize(mCameraManager, mVideoCameraDevice, videoSizeString);
            // Audio Source:
            switch (videoAudioSourceString) {
                case DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_MIC:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  AudioSource (MIC)");
                    videoAudioSource = MediaRecorder.AudioSource.MIC;
                    break;
                case DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_CAMCORDER:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  AudioSource (CAMCORDER)");
                    videoAudioSource = MediaRecorder.AudioSource.CAMCORDER;
                    break;
                case DROID_PARAMETERS_VIDEO_AUDIO_SOURCE_DEFAULT:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  AudioSource (DEFAULT)");
                    videoAudioSource = MediaRecorder.AudioSource.DEFAULT;
                    break;
                default:
                    // Log an error and exit:
                    if (mFloidService.mUseLogger)
                        Log.e(TAG, "Video camera: Bad audio source parameter error: " + videoAudioSource);
                    videoAudioSource = MediaRecorder.AudioSource.DEFAULT;
                    break;
            }
            // Video source:
            if (mFloidService.mUseLogger) Log.d(TAG, "  Video Camera Source: Surface");
            // Video output format:
            switch (videoOutputFormatString) {
                case DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_DEFAULT:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  OutputFormat: DEFAULT");
                    videoOutputFormat = MediaRecorder.OutputFormat.DEFAULT;
                    videoOutputFileExtension = VIDEO_FILE_EXTENSION_DEFAULT;
                    break;
                case DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_MPEG_4:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  OutputFormat: MPEG_4");
                    videoOutputFormat = MediaRecorder.OutputFormat.MPEG_4;
                    videoOutputFileExtension = VIDEO_FILE_EXTENSION_MP4;
                    break;
                case DROID_PARAMETERS_VIDEO_OUTPUT_FORMAT_THREE_GPP:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  OutputFormat: THREE_GPP");
                    videoOutputFormat = MediaRecorder.OutputFormat.THREE_GPP;
                    videoOutputFileExtension = VIDEO_FILE_EXTENSION_3GPP;
                    break;
                default:
                    if (mFloidService.mUseLogger)
                        Log.e(TAG, "Video camera: Bad video output parameter error: " + videoOutputFormat);
                    videoOutputFileExtension = VIDEO_FILE_EXTENSION_DEFAULT;
                    break;
            }
            // Video encoder:
            switch (videoEncoderString) {
                case DROID_PARAMETERS_VIDEO_ENCODER_DEFAULT:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  VideoEncoder: DEFAULT");
                    videoEncoder = MediaRecorder.VideoEncoder.DEFAULT;
                    break;
                case DROID_PARAMETERS_VIDEO_ENCODER_MPEG_4_SP:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  VideoEncoder: MPEG_4_SP");
                    videoEncoder = MediaRecorder.VideoEncoder.MPEG_4_SP;
                    break;
                case DROID_PARAMETERS_VIDEO_ENCODER_H264:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  VideoEncoder: H264");
                    videoEncoder = MediaRecorder.VideoEncoder.H264;
                    break;
                default:
                    if (mFloidService.mUseLogger)
                        Log.e(TAG, "Video camera: Bad video encoder parameter error: " + videoEncoder);
                    videoEncoder = MediaRecorder.VideoEncoder.DEFAULT;
                    break;
            }
            // Audio Encoder:
            switch (videoAudioEncoderString) {
                case DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_DEFAULT:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  AudioEncoder (DEFAULT)");
                    videoAudioEncoder = MediaRecorder.AudioEncoder.DEFAULT;
                    break;
                case DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AAC_AAC:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  AudioEncoder (ACC)");
                    videoAudioEncoder = MediaRecorder.AudioEncoder.AAC;
                    break;
                case DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AAC_ELD:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  AudioEncoder (AAC_ELD)");
                    videoAudioEncoder = MediaRecorder.AudioEncoder.AAC_ELD;
                    break;
                case DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AMR_NB:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  AudioEncoder (AMR_NB)");
                    videoAudioEncoder = MediaRecorder.AudioEncoder.AMR_NB;
                    break;
                case DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_AMR_WB:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  AudioEncoder (AMR_WB)");
                    videoAudioEncoder = MediaRecorder.AudioEncoder.AMR_WB;
                    break;
                case DROID_PARAMETERS_VIDEO_AUDIO_ENCODER_HE_AAC:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  AudioEncoder (HE_AAC)");
                    videoAudioEncoder = MediaRecorder.AudioEncoder.HE_AAC;
                    break;
                default:
                    if (mFloidService.mUseLogger)
                        Log.e(TAG, "Video camera: Bad audio encoder parameter error: " + videoAudioEncoder);
                    videoAudioEncoder = MediaRecorder.AudioEncoder.DEFAULT;
            }
            // Orientation:
            if (mFloidService.mUseLogger) Log.d(TAG, "  setting orientation: 0 degrees");
            // Try to set the video height/width from here...
            if (mFloidService.mUseLogger) Log.d(TAG, "  setting video size: width: " + videoSize[0] + "  height: " + videoSize[1]);
            // Video and audio quality:
            // First, get the video frame rate:
            switch (videoFrameRateString) {
                case DROID_PARAMETERS_VIDEO_FRAME_RATE_DEFAULT:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  VideoFrameRate: (Default): " + VIDEO_DEFAULT_FRAME_RATE);
                    videoFrameRate = VIDEO_DEFAULT_FRAME_RATE;
                    break;
                case DROID_PARAMETERS_VIDEO_FRAME_RATE_MAX:
                    int maxVideoFrameRate = FloidImagingUtil.getMaxVideoFrameRate(mVideoCameraDevice);
                    if (mFloidService.mUseLogger)
                        Log.d(TAG, "  VideoFrameRate: (Max) " + maxVideoFrameRate);
                    videoFrameRate = maxVideoFrameRate;
                    break;
                default:
                    try {
                        int videoFrameRateInt = FloidService.getDroidParametersValidValues().get(DROID_PARAMETERS_VIDEO_FRAME_RATE_OPTION).getAsValidInt(videoFrameRateString);
                        if (mFloidService.mUseLogger)
                            Log.d(TAG, "  VideoFrameRate (Option): " + videoFrameRateInt);
                        videoFrameRate = videoFrameRateInt;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Video camera: Bad video frame rate: " + videoFrameRateString);
                    }
                    break;
            }
            // Calculate the video encoding bit rate - see here: https://support.google.com/youtube/answer/1722171?hl=en
            // The formula we derived is: video encoding bit rate = width * height * Pixels * 8(bits) * FrameRate / 200
            videoEncodingBitRate = (videoSize[0] * videoSize[1] * 3 * 8 * videoFrameRate) / 200;
            if(mFloidService.mUseLogger)
                Log.d(TAG, "Video encoding bit rate calculated to be: " + videoEncodingBitRate);
            // Video max duration:
            switch (videoMaxDurationString) {
                case DROID_PARAMETERS_VIDEO_MAX_DURATION_DEFAULT:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  MaxDuration (Default = NONE)");
                    videoMaxDuration = 0;
                    break;
                case DROID_PARAMETERS_VIDEO_MAX_DURATION_NONE:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  MaxDuration (NONE)");
                    videoMaxDuration = 0;
                    break;
                default:
                    try {
                        int videoMaxDurationInt = FloidService.getDroidParametersValidValues().get(DROID_PARAMETERS_VIDEO_MAX_DURATION_OPTION).getAsValidInt(videoMaxDurationString);
                        if (mFloidService.mUseLogger)
                            Log.d(TAG, "  MaxDuration (Option): " + videoMaxDurationInt);
                        videoMaxDuration = videoMaxDurationInt;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Video camera: Bad video max duration parameter error: " + videoMaxDurationString);
                    }
                    break;
            }
            // Video max file size:
            switch (videoMaxFileSizeString) {
                case DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_DEFAULT:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  MaxFileSize (Default = NONE = 0)");
                    videoMaxFileSize = 0;
                    break;
                case DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_NONE:
                    if (mFloidService.mUseLogger) Log.d(TAG, "  MaxFileSize (NONE = 0)");
                    videoMaxFileSize = 0;
                    break;
                default:
                    try {
                        int videoMaxFileSizeInt = FloidService.getDroidParametersValidValues().get(DROID_PARAMETERS_VIDEO_MAX_FILE_SIZE_OPTION).getAsValidInt(videoMaxFileSizeString);
                        if (mFloidService.mUseLogger)
                            Log.d(TAG, "  MaxFileSize (Option): " + videoMaxFileSizeInt);
                        videoMaxFileSize = videoMaxFileSizeInt;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Video camera: Bad video max file size error: " + videoMaxFileSizeString);
                    }
                    break;
            }
            // Audio sample rate:  --
            switch (videoAudioSampleRateString) {
                case DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_DEFAULT:
                    if (mFloidService.mUseLogger)
                        Log.d(TAG, "  AudioSampleRate (DEFAULT - setting to 16384)");
                    videoAudioSampleRate = HARD_CODED_DEFAULT_AUDIO_SAMPLE_RATE;
                    break;
                case DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_MAX:
                    if (mFloidService.mUseLogger)
                        Log.d(TAG, "  AudioSampleRate (MAX - setting to 16384)");
                    videoAudioSampleRate = HARD_CODED_DEFAULT_AUDIO_SAMPLE_RATE;
                    break;
                default:
                    try {
                        int videoAudioSampleRateInt = FloidService.getDroidParametersValidValues().get(DROID_PARAMETERS_VIDEO_AUDIO_SAMPLE_RATE_OPTION).getAsValidInt(videoAudioSampleRateString);
                        if (mFloidService.mUseLogger)
                            Log.d(TAG, "  AudioSampleRate (Option): " + videoAudioSampleRateInt);
                        videoAudioSampleRate = videoAudioSampleRateInt;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Video camera: Bad audio sample rate parameter error: " + videoAudioSampleRateString);
                    }
                    break;
            }
            // Audio bit rate:  --
            switch (videoAudioBitRateString) {
                case DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_DEFAULT:
                    if (mFloidService.mUseLogger)
                        Log.d(TAG, "  AudioBitRate (DEFAULT - setting to 16384)");
                    videoAudioBitRate = HARD_CODED_DEFAULT_AUDIO_BIT_RATE;
                    break;
                case DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_MAX:
                    if (mFloidService.mUseLogger)
                        Log.d(TAG, "  AudioBitRate (MAX - setting to 16384)");
                    videoAudioBitRate = HARD_CODED_DEFAULT_AUDIO_BIT_RATE;
                    break;
                default:
                    try {
                        int videoAudioBitRateInt = FloidService.getDroidParametersValidValues().get(DROID_PARAMETERS_VIDEO_AUDIO_BIT_RATE_OPTION).getAsValidInt(videoAudioBitRateString);
                        if (mFloidService.mUseLogger)
                            Log.d(TAG, "  AudioBitRate (Option): " + videoAudioBitRateInt);
                        videoAudioBitRate = videoAudioBitRateInt;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Video camera: Bad audio bit rate parameter error: " + videoAudioBitRateString);
                    }
                    break;
            }
        } catch (Exception e) {
            // Log an error and exit:
            if (mFloidService.mUseLogger)
                Log.e(TAG, "Video camera: Uncaught video parameter error: " + e.getMessage() + " : " + Log.getStackTraceString(e));
            return false;
        }

        // Create an error listener:
        MediaRecorder.OnErrorListener onErrorListener = new MediaRecorder.OnErrorListener() {
            public void onError(MediaRecorder mediaRecorder, int what, int extra) {
                switch (what) {
                    case MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Recorder Error: ERROR_UNKNOWN - extra: " + extra);
                        break;

                    case MediaRecorder.MEDIA_ERROR_SERVER_DIED:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Recorder Error: ERROR_SERVER_DIED - extra: " + extra);
                        break;

                    case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Recorder Error: INFO_MAX_DURATION_REACHED - extra: " + extra);
                        break;

                    case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Recorder Error: INFO_MAX_FILESIZE_REACHED - extra: " + extra);
                        break;

                    default:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Error: Default case: what: " + what + "  extra: " + extra);
                        break;
                }
                if (mFloidService.mFloidDroidStatus.isTakingVideo() && (mediaRecorder == mVideoMediaRecorder)) {
                    stopVideo();
                }
            }
        };
        // Create an info listener:
        MediaRecorder.OnInfoListener onInfoListener = new MediaRecorder.OnInfoListener() {
            public void onInfo(MediaRecorder mediaRecorder, int what, int extra) {
                boolean stopVideoRecorder = false;
                switch (what) {
                    case MediaRecorder.MEDIA_ERROR_SERVER_DIED:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Recorder Error: ERROR_SERVER_DIED - extra: " + extra);
                        stopVideoRecorder = true;
                        break;
                    case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Info: INFO_MAX_DURATION_REACHED - extra: " + extra);
                        stopVideoRecorder = true;
                        break;

                    case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Info: INFO_MAX_FILESIZE_REACHED - extra: " + extra);
                        stopVideoRecorder = true;
                        break;
                    case MediaRecorder.MEDIA_RECORDER_INFO_UNKNOWN:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Info: INFO_UNKNOWN - extra: " + extra);
                        break;
                    default:
                        if (mFloidService.mUseLogger)
                            Log.e(TAG, "Media Info: default case: what: " + what + "  extra: " + extra);
                        break;
                }
                if (stopVideoRecorder && mFloidService.mFloidDroidStatus.isTakingVideo() && (mediaRecorder == mVideoMediaRecorder)) {
                    stopVideo();
                }
            }
        };
        // Create a new date:
        Date date = new Date();
        // Create the file path from the name:
        mVideoMediaRecorderFileName = createFilePath(imagingSimpleDateFormat.format(date) + videoOutputFileExtension);
        if (mFloidService.mUseLogger) Log.d(TAG, "Setup Media Recorder: File Name: " + mVideoMediaRecorderFileName);
        // SET THE GPS LOCATION:
        latitude = (float) mFloidService.getFloidStatus().getGy();
        longitude = (float) mFloidService.getFloidStatus().getGx();
        if (mFloidService.mUseLogger) Log.d(TAG, "Lat/Lng: " + latitude + ", " + longitude);
        mVideoMediaRecorder = createMediaRecorder(mContext,
                videoSize[0],
                videoSize[1],
                videoSource,
                videoEncoder,
                videoFrameRate,
                videoEncodingBitRate,
                videoMaxFileSize,
                videoMaxDuration,
                videoAudioSource,
                videoAudioEncoder,
                videoOutputFormat,
                mVideoMediaRecorderFileName,
                videoAudioSampleRate,
                videoAudioBitRate,
                latitude,
                longitude,
                0,
                onErrorListener,
                onInfoListener);
        if(mVideoMediaRecorder == null) {
            Log.e(TAG, "Could not create the video media recorder...closing cameras");
            closeCameras();
            return false;
        }
        if (mFloidService.mUseLogger) Log.d(TAG, "done setupMediaRecorder: true");
        logFloidImagingStatus("setupMediaRecorder [end]");
        return true;
    }

    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its status.
     */
    private final CameraDevice.StateCallback mCameraStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            logFloidImagingStatus(String.format("cameraDevice onOpened [begin] device: %s", cameraDevice.getId()));
            try {
                mCameraOpenCloseLock.release();
                // Which camera is this for:
                if (mVideoCameraDevice == null) {
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "This is the video camera");
                    // This is the video camera
                    mFloidImagingStatus.setStatusVideoCameraOpen(true);
                    mVideoCameraDevice = cameraDevice;
                    mVideoMediaRecorder = new MediaRecorder();
                    // Are we also the photo camera?
                    if (mFloidImagingStatus.getVideoCameraId().equals(mFloidImagingStatus.getPhotoCameraId())) {
                        // Yes:
                        if(mFloidService.mUseLogger)
                            Log.d(TAG, "And the video camera");
                        mFloidImagingStatus.setStatusPhotoCameraOpen(true);
                        mPhotoCameraDevice = cameraDevice;
                    }
                } else {
                    // This is for the photo camera only:
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "This is the photo camera");
                    mFloidImagingStatus.setStatusPhotoCameraOpen(true);
                    mPhotoCameraDevice = cameraDevice;
                }
                // OK, where are we:
                if (mPhotoCameraDevice == null) {
                    // We need to call again and open the photo camera
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "Acquiring the photo camera");
                    try {
                        try {
                            mCameraOpenCloseLock.acquire();
                        } catch (InterruptedException e) {
                            Log.e(TAG, "Interrupted exception acquiring camera open close lock - closing cameras", e);
                            closeCameras();
                            return;
                        }
                        Log.d(TAG, "Opening photo camera: " + mFloidImagingStatus.getPhotoCameraId());
                        mCameraManager.openCamera(mFloidImagingStatus.getPhotoCameraId(), mCameraStateCallback, mBackgroundHandler);
                    } catch (CameraAccessException cae) {
                        Log.e(TAG, "Error accessing camera manager from onOpened callback", cae);
                        closeCameras();
                        return;
                    } catch (SecurityException se) {
                        Log.e(TAG, "Camera access exception security exception opening camera - closing cameras", se);
                        closeCameras();
                        return;
                    }
                } else {
                    // Do we have two cameras:
                    if(mFloidService.mUseLogger)
                        Log.d(TAG, "We now have both cameras");
                    // Was the photo camera different than the video camera?
                    if (mVideoCameraDevice == mPhotoCameraDevice) {
                        // This one camera is for both, so we have just one capture session to create:
                        mFloidImagingStatus.setNumberOfRemainingCaptureSessionsToCreate(1);
                        // Set up both video and photo surfaces for this one device:
                        setupSurfacesAndCaptureSessions(mVideoCameraDevice, true, true);
                    } else {
                        // If so we have two capture sessions to create:
                        mFloidImagingStatus.setNumberOfRemainingCaptureSessionsToCreate(2);
                        // Set up the new surfaces and capture requests:
                        setupSurfacesAndCaptureSessions(mVideoCameraDevice, true, false);
                        setupSurfacesAndCaptureSessions(mPhotoCameraDevice, false, true);
                    }
                }
            } catch(Exception e) {
                Log.e(TAG, "Exception in onOpened callback", e);
            }
            // Update the front end with the status:
            mFloidService.uxSendImagingStatus();
            logFloidImagingStatus("cameraDevice onOpened [end]");
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            logFloidImagingStatus(String.format("cameraDevice onDisconnected [begin] device: %s", cameraDevice.getId()));
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            if (cameraDevice == mVideoCameraDevice) {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "onDisconnected: video camera closed");
                mVideoCameraDevice = null;
                mFloidImagingStatus.setStatusVideoCameraOpen(false);
                mFloidImagingStatus.setStatusTakingVideo(false);
            }
            if (cameraDevice == mPhotoCameraDevice) {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "onDisconnected: photo camera closed");
                mPhotoCameraDevice = null;
                mFloidImagingStatus.setStatusPhotoCameraOpen(false);
                mFloidImagingStatus.setStatusTakingPhoto(false);
            }
            // Update the front end with the status:
            mFloidService.uxSendImagingStatus();
            logFloidImagingStatus("cameraDevice onDisconnected [end]");
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            Log.e(TAG, "Camera Device " + cameraDevice.getId() + " Failed to Open - code:  " + error);
            logFloidImagingStatus(String.format("cameraDevice onError [begin] device: %s", cameraDevice.getId()));
            mCameraOpenCloseLock.release();
            if (cameraDevice == mVideoCameraDevice) {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "onError: video camera closed");
                mVideoCameraDevice = null;
                mFloidImagingStatus.setStatusVideoCameraOpen(false);
                mFloidImagingStatus.setStatusTakingVideo(false);
            }
            if (cameraDevice == mPhotoCameraDevice) {
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "onError: photo camera closed");
                mPhotoCameraDevice = null;
                mFloidImagingStatus.setStatusPhotoCameraOpen(false);
                mFloidImagingStatus.setStatusTakingPhoto(false);
            }
            // Update the front end with the status:
            mFloidService.uxSendImagingStatus();
            logFloidImagingStatus("cameraDevice onError [end]");
        }
    };

    private void setupImagingOutputDirectory() {
        // Try to make our imaging output directory:
        mImagingDirectoryPath = String.format(Locale.getDefault(), "%s%s%s%s%s%d", mFloidImagingStatus.getPhotoVideoStoragePath(), File.separator, FLOID_IMAGING_DIRECTORY_NAME, File.separator, FLOID_IMAGING_DIRECTORY_PREFIX, mFloidId);
        try {
            File imagingDirectory = new File(mImagingDirectoryPath);
            // have the object build the directory structure, if needed.
            if (!imagingDirectory.exists()) {
                if (!imagingDirectory.mkdirs()) {
                    Log.e(TAG, "Failed to create imaging directory: " + mImagingDirectoryPath);
                } else {
                    Log.d(TAG, "Created imaging directory: " + mImagingDirectoryPath);
                }
            } else {
                Log.d(TAG, "Imaging directory already exists: " + mImagingDirectoryPath);
            }
        } catch (Exception e) {
            Log.e(TAG, "Failed to create imaging directory: " + mImagingDirectoryPath, e);
        }
    }
    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        if(mFloidService.mUseLogger)
            Log.d(TAG, "Starting background thread");
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        if(mFloidService.mUseLogger)
            Log.d(TAG, "Stopping background thread");
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close the camera(s)
     */
    private void closeCameras() {
        if(mFloidService.mUseLogger)
            Log.d(TAG, "Closing cameras");
        logFloidImagingStatus("closeCameras [begin]");
        if(mFloidService.mUseLogger)
            Log.d(TAG, "Setting photo state to none");
        mFloidImagingStatus.setPhotoState(FloidImagingStatus.STATE_NONE);
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mVideoCameraDevice) {
                mVideoCameraDevice.close();
                if(mVideoCameraDevice == mPhotoCameraDevice) {
                    // Photo camera is now also closed since they were the same
                    mPhotoCameraDevice = null;
                    mFloidImagingStatus.setPhotoCameraId(FloidImagingStatus.NO_CAMERA_ID_STRING);
                }
                mVideoCameraDevice = null;
                mFloidImagingStatus.setVideoCameraId(FloidImagingStatus.NO_CAMERA_ID_STRING);
            }
            // If photo camera was different than video camera, close it:
            if (null != mPhotoCameraDevice) {
                mPhotoCameraDevice.close();
                mPhotoCameraDevice = null;
                mFloidImagingStatus.setPhotoCameraId(FloidImagingStatus.NO_CAMERA_ID_STRING);
            }
            // Dispose of the media recorder if not null:
            if (null != mVideoMediaRecorder) {
                try {
                    mVideoMediaRecorder.stop();
                } catch (Exception e) {
                    // We do nothing...
                }
                try {
                    mVideoMediaRecorder.release();
                } catch (Exception e) {
                    // We do nothing...
                }
                mVideoMediaRecorder = null;
                // Scan the media file:
                FloidImagingUtil.scanMediaFile(mContext, mVideoMediaRecorderFileName);
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.");
        } finally {
            mCameraOpenCloseLock.release();
            mFloidImagingStatus.setStatusVideoCameraOpen(false);
            mFloidImagingStatus.setStatusPhotoCameraOpen(false);
            mFloidImagingStatus.setStatusTakingVideo(false);
            mFloidImagingStatus.setStatusTakingPhoto(false);
            mFloidImagingStatus.setStatusTakingVideoSnapshot(false);
            mFloidImagingStatus.setStatusPhotoCameraCaptureSessionConfigured(false);
            mFloidImagingStatus.setStatusVideoCameraCaptureSessionConfigured(false);
            mVideoMediaRecorder = null;
            mPhotoImageReader = null;
            mVideoSnapshotImageReader = null;
            mVideoMediaRecorderSurface = null;
            mPhotoImageReaderSurface = null;
            mVideoSnapshotImageReaderSurface = null;
            mFloidImagingStatus.setStatusVideoSurfacesSetup(false);
            mFloidImagingStatus.setStatusPhotoSurfacesSetup(false);
            synchronized (mFloidService.mFloidDroidStatusSynchronizer) {
                mFloidService.mFloidDroidStatus.setTakingVideo(false);
                mFloidService.mFloidDroidStatus.setTakingPhoto(false);
            }
        }
        // Update the front end with the status:
        mFloidService.uxSendImagingStatus();
        logFloidImagingStatus("closeCameras [end]");
    }

    /**
     * Gets photo camera id from parameter.
     *
     * @return the photo camera id from parameter
     */
    private String getPhotoCameraIdFromParameter() {
        if(mFloidService.mUseLogger)
            Log.d(TAG, "Getting photo camera id from parameters");
        String photoCameraId = FloidImagingStatus.NO_CAMERA_ID_STRING;
        try {
            String photoCameraIdString = mFloidService.mDroidParametersHashMap.get(DROID_PARAMETERS_PHOTO_CAMERA_OPTION);
            if (photoCameraIdString != null) {
                switch (photoCameraIdString) {
                    case DROID_PARAMETERS_PHOTO_CAMERA_DEFAULT:
                    case DROID_PARAMETERS_PHOTO_CAMERA_BACK:
                        photoCameraId = FloidImagingUtil.getCameraId(mCameraManager, CameraCharacteristics.LENS_FACING_BACK, mFloidService.mUseLogger);
                        break;
                    case DROID_PARAMETERS_PHOTO_CAMERA_FRONT:
                        photoCameraId = FloidImagingUtil.getCameraId(mCameraManager, CameraCharacteristics.LENS_FACING_FRONT, mFloidService.mUseLogger);
                        break;
                    case DROID_PARAMETERS_PHOTO_CAMERA_EXTERNAL:
                        photoCameraId = FloidImagingUtil.getCameraId(mCameraManager, CameraCharacteristics.LENS_FACING_EXTERNAL, mFloidService.mUseLogger);
                        break;
                    default:
                        if(mFloidService.mUseLogger)
                            Log.w(TAG, "Unrecognized photo camera id from parameters - setting to default");
                        photoCameraId = FloidImagingUtil.getCameraId(mCameraManager, CameraCharacteristics.LENS_FACING_BACK, mFloidService.mUseLogger);
                        break;
                }
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Photo Camera Id From Parameters: " + photoCameraIdString + " is " + photoCameraId);
            }
        } catch (Exception e) {
            Log.e(TAG, "Caught exception getting photo camera id from parameters - closing cameras", e);
            closeCameras();
        }
        return photoCameraId;
    }

    /**
     * Gets video camera id from parameter.
     *
     * @return the video camera id from parameter
     */
    private String getVideoCameraIdFromParameter() {
        if(mFloidService.mUseLogger)
            Log.d(TAG, "Getting video camera id from parameters");
        String videoCameraId = FloidImagingStatus.NO_CAMERA_ID_STRING;
        try {
            String videoCameraIdString = mFloidService.mDroidParametersHashMap.get(DROID_PARAMETERS_VIDEO_CAMERA_OPTION);
            if (videoCameraIdString != null) {
                switch (videoCameraIdString) {
                    case DROID_PARAMETERS_VIDEO_CAMERA_DEFAULT:
                    case DROID_PARAMETERS_VIDEO_CAMERA_BACK:
                        videoCameraId = FloidImagingUtil.getCameraId(mCameraManager, CameraCharacteristics.LENS_FACING_BACK, mFloidService.mUseLogger);
                        break;
                    case DROID_PARAMETERS_VIDEO_CAMERA_FRONT:
                        videoCameraId = FloidImagingUtil.getCameraId(mCameraManager, CameraCharacteristics.LENS_FACING_FRONT, mFloidService.mUseLogger);
                        break;
                    case DROID_PARAMETERS_VIDEO_CAMERA_EXTERNAL:
                        videoCameraId = FloidImagingUtil.getCameraId(mCameraManager, CameraCharacteristics.LENS_FACING_EXTERNAL, mFloidService.mUseLogger);
                        break;
                    default:
                        if(mFloidService.mUseLogger)
                            Log.w(TAG, "Unrecognized video camera id from parameters - setting to default");
                        videoCameraId = FloidImagingUtil.getCameraId(mCameraManager, CameraCharacteristics.LENS_FACING_BACK, mFloidService.mUseLogger);
                        break;
                }
                if(mFloidService.mUseLogger)
                    Log.d(TAG, "Video Camera Id From Parameters: " + videoCameraIdString + " is " + videoCameraId);
            }
        } catch (Exception e) {
            Log.e(TAG, "Caught exception getting video camera id from parameters - closing cameras", e);
            closeCameras();
        }
        return videoCameraId;
    }

    /**
     * Get the video size for a MediaRecorder for the camera device given the video size string
     *
     * @param cameraManager   the camera manager
     * @param cameraDevice    the camera device
     * @param videoSizeString the video size string
     * @return the video size for the MediaRecorder
     */
    private int[] getVideoSize(CameraManager cameraManager, CameraDevice cameraDevice, String videoSizeString) {
        final int SIZE_INDEX_WIDTH = 0;
        final int SIZE_INDEX_HEIGHT = 1;
        // Default video sizes:
        int[] videoSize = {IMAGE_SIZE_640, IMAGE_SIZE_480}; // Default returned on error
        try {
            // Set the camera:
            if (cameraDevice == null) {
                if (mFloidService.mUseLogger)
                    Log.e(TAG, "Video camera: Error - video camera device is null");
            } else {
                // Video Size:
                switch (videoSizeString) {
                    case DROID_PARAMETERS_VIDEO_SIZE_DEFAULT:
                        // Default:
                        videoSize = FloidImagingUtil.findClosestVideoSize(cameraManager, cameraDevice, IMAGE_SIZE_1080, mFloidService.mUseLogger);
                        if (mFloidService.mUseLogger)
                            Log.d(TAG, "  VideoSize: (Default - 1080p): " + videoSize[SIZE_INDEX_WIDTH] + ", " + videoSize[SIZE_INDEX_HEIGHT]);
                        break;
                    case DROID_PARAMETERS_VIDEO_SIZE_1080P:
                        // 1080p:
                        videoSize = FloidImagingUtil.findClosestVideoSize(cameraManager, cameraDevice, IMAGE_SIZE_1080, mFloidService.mUseLogger);
                        if (mFloidService.mUseLogger)
                            Log.d(TAG, "  VideoSize: (1080p) " + videoSize[SIZE_INDEX_WIDTH] + ", " + videoSize[SIZE_INDEX_HEIGHT]);
                        break;
                    case DROID_PARAMETERS_VIDEO_SIZE_MAX:
                        // Max:
                        videoSize = FloidImagingUtil.findMaxVideoSize(cameraManager, cameraDevice);
                        if (videoSize[SIZE_INDEX_WIDTH] == 0) {
                            // Log an error and exit:
                            if (mFloidService.mUseLogger)
                                Log.e(TAG, "Video camera: Could not compute max video size");
                        } else {
                            if (mFloidService.mUseLogger)
                                Log.d(TAG, "  VideoSize: (Max) " + videoSize[SIZE_INDEX_WIDTH] + ", " + videoSize[SIZE_INDEX_HEIGHT]);
                        }
                        break;
                    default:
                        try {
                            // Video size was height x width - parse this:
                            videoSize = FloidService.getDroidParametersValidValues().get(DROID_PARAMETERS_VIDEO_SIZE_OPTION).getAsScreenSize(videoSizeString);
                            if (mFloidService.mUseLogger)
                                Log.d(TAG, "  VideoSize: (Option) " + videoSize[SIZE_INDEX_WIDTH] + ", " + videoSize[SIZE_INDEX_HEIGHT]);
                        } catch (Exception e) {
                            // Parameter error - set to 1080p if possible:
                            if (mFloidService.mUseLogger)
                                Log.e(TAG, "Video size: Error - size not available: " + videoSizeString + " ... attempting to set to 1080p...");

                            try {
                                // 1080p:
                                videoSize = FloidImagingUtil.findClosestVideoSize(cameraManager, cameraDevice, IMAGE_SIZE_1080, mFloidService.mUseLogger);
                                if (mFloidService.mUseLogger)
                                    Log.d(TAG, "  VideoSize: (Exception: 1080p) " + videoSize[SIZE_INDEX_WIDTH] + ", " + videoSize[SIZE_INDEX_HEIGHT]);
                            } catch (Exception e2) {
                                // no 1080p - set to 640x480:
                                if (mFloidService.mUseLogger)
                                    Log.e(TAG, "Video size: Error - 1080p not available: " + videoSizeString + " ... setting to 640x480...");
                                try {
                                    if (mFloidService.mUseLogger)
                                        Log.d(TAG, "  VideoSize: (Exception2) 640, 480");
                                    videoSize[0] = IMAGE_SIZE_640;
                                    videoSize[1] = IMAGE_SIZE_480;
                                } catch (Exception e3) {
                                    // Log an error and exit:
                                    Log.e(TAG, "Video camera: Error - could not set to 640x480");
                                }
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            if (mFloidService.mUseLogger)
                Log.e(TAG, "Start Video caught exception: " + e.getMessage() + "  " + Log.getStackTraceString(e));
        }
        return videoSize;
    }

    /**
     * Log the floid imaging status using this label
     *
     * @param label the label for this set of log entries
     */
    private void logFloidImagingStatus(String label) {
        if(!mFloidService.mUseLogger) {
            return;
        }
        try {
            Log.d(TAG, "IMAGING STATUS (" + label + "):");
            Log.d(TAG, "---------------------------------------------");
            Log.d(TAG, "  GENERAL:");
            Log.d(TAG, "                       Initialized: " + mFloidImagingStatus.isStatusInitialized());
            Log.d(TAG, "  PHOTO:");
            Log.d(TAG, "                      Taking Photo: " + mFloidImagingStatus.isStatusTakingPhoto());
            Log.d(TAG, "                          Id Dirty: " + mFloidImagingStatus.isStatusPhotoCameraIdDirty());
            Log.d(TAG, "                  Properties Dirty: " + mFloidImagingStatus.isStatusPhotoCameraPropertiesDirty());
            Log.d(TAG, "                         Device Id: " + (mPhotoCameraDevice != null ? mPhotoCameraDevice.getId() : "NONE"));
            Log.d(TAG, "                              Open: " + mFloidImagingStatus.isStatusPhotoCameraOpen());
            Log.d(TAG, "                      Image Reader: " + (mPhotoImageReader != null ? "VALID" : "NULL"));
            Log.d(TAG, "                    Surfaces Setup: " + mFloidImagingStatus.isStatusPhotoSurfacesSetup());
            Log.d(TAG, "                          Surfaces: " + (mPhotoSurfaces != null ? "VALID" : "NULL"));
            Log.d(TAG, "              Image Reader Surface: " + (mPhotoImageReaderSurface != null ? "VALID" : "NULL"));
            Log.d(TAG, "             Capture Request Setup: " + mFloidImagingStatus.isStatusPhotoCaptureRequestSetup());
            Log.d(TAG, "        Capture Session Configured: " + mFloidImagingStatus.isStatusPhotoCameraCaptureSessionConfigured());
            Log.d(TAG, "  VIDEO:");
            Log.d(TAG, "                      Taking Video: " + mFloidImagingStatus.isStatusTakingVideo());
            Log.d(TAG, "                          Id Dirty: " + mFloidImagingStatus.isStatusVideoCameraIdDirty());
            Log.d(TAG, "                  Properties Dirty: " + mFloidImagingStatus.isStatusVideoCameraPropertiesDirty());
            Log.d(TAG, "                              Open: " + mFloidImagingStatus.isStatusVideoCameraOpen());
            Log.d(TAG, "                         Device Id: " + (mVideoCameraDevice != null ? mVideoCameraDevice.getId() : "NONE"));
            Log.d(TAG, "                    Media Recorder: " + (mVideoMediaRecorder != null ? "VALID" : "NULL"));
            Log.d(TAG, "                    Surfaces Setup: " + mFloidImagingStatus.isStatusVideoSurfacesSetup());
            Log.d(TAG, "                          Surfaces: " + (mVideoSurfaces != null ? "VALID" : "NULL"));
            Log.d(TAG, "             Capture Request Setup: " + mFloidImagingStatus.isStatusVideoCaptureRequestSetup());
            Log.d(TAG, "        Capture Session Configured: " + mFloidImagingStatus.isStatusVideoCameraCaptureSessionConfigured());
            Log.d(TAG, "  VIDEO STREAMING:");
            Log.d(TAG, "            Media Recorder Surface: " + (mVideoMediaRecorderSurface != null ? "VALID" : "NULL"));
            Log.d(TAG, "                    Surfaces Setup: " + mFloidImagingStatus.isStatusVideoSurfacesSetup());
            Log.d(TAG, "            Streaming Image Reader: " + (mVideoStreamingImageReader != null ? "VALID" : "NULL"));
            Log.d(TAG, "    Streaming Image Reader Surface: " + (mVideoStreamingImageReaderSurface != null ? "VALID" : "NULL"));
            Log.d(TAG, "          Media Recorder File Name: " + mVideoMediaRecorderFileName);
            Log.d(TAG, "  VIDEO SNAPSHOT:");
            Log.d(TAG, "             Taking Video Snapshot: " + mFloidImagingStatus.isStatusTakingVideoSnapshot());
            Log.d(TAG, "             Snapshot Image Reader: " + (mVideoSnapshotImageReader != null ? "VALID" : "NULL"));
            Log.d(TAG, "     Snapshot Image Reader Surface: " + (mVideoSnapshotImageReaderSurface != null ? "VALID" : "NULL"));
        } catch (Exception e) {
            Log.e(TAG, "logFloidImagingStatus: caught exception", e);
        }
    }
}
