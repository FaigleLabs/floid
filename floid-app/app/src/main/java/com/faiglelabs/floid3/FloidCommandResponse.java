/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

/**
 * A floid command response
 */
public class FloidCommandResponse
{
    private int    mCommandNumber;
    private int    mCommandStatus;
    private String mIdentifier;
    private String mCommandStatusString;

    /**
     * The command status OK
     */
    @SuppressWarnings("WeakerAccess")
    public static final String COMMAND_STATUS_STRING_OK = "OK";
    /**
     * The command status LOGIC ERROR
     */
    @SuppressWarnings("WeakerAccess")
    public static final String COMMAND_STATUS_STRING_LOGIC_ERROR = "LOGIC ERROR";
    /**
     * The command status ERROR
     */
    @SuppressWarnings("WeakerAccess")
    public static final String COMMAND_STATUS_STRING_ERROR = "ERROR";
    /**
     * The command status string UNKNOWN
     */
    @SuppressWarnings("WeakerAccess")
    public static final String COMMAND_STATUS_STRING_UNKNOWN = "UNKNOWN";
    /**
     * Get the command number
     * @return the command number
     */
    public int getCommandNumber()
    {
        return mCommandNumber;
    }

    /**
     * Set the command number
     * @param commandNumber the command number
     */
    @SuppressWarnings("WeakerAccess")
    public void setCommandNumber(int commandNumber)
    {
        mCommandNumber = commandNumber;
    }

    /**
     * Get the command status
     * @return the command status
     */
    public int getCommandStatus()
    {
        return mCommandStatus;
    }

    /**
     * Set the command status
     * @param commandStatus the command status
     */
    @SuppressWarnings("WeakerAccess")
    public void setCommandStatus(int commandStatus) {
        mCommandStatus = commandStatus;
        switch (mCommandStatus) {
            case FloidService.COMMAND_RESPONSE_OK: {
                mCommandStatusString = COMMAND_STATUS_STRING_OK;
            }
            break;
            case FloidService.COMMAND_RESPONSE_LOGIC_ERROR: {
                mCommandStatusString = COMMAND_STATUS_STRING_LOGIC_ERROR;
            }
            break;
            case FloidService.COMMAND_RESPONSE_ERROR: {
                mCommandStatusString = COMMAND_STATUS_STRING_ERROR;
            }
            break;
            default: {
                mCommandStatusString = COMMAND_STATUS_STRING_UNKNOWN + ": " + commandStatus;
            }
            break;
        }
    }

    /**
     * Get the command status string
     * @return the command status string
     */
    @SuppressWarnings("unused")
    public String getCommandStatusString()
    {
        return mCommandStatusString;
    }

    /**
     * Get the identifier
     * @return the identifier
     */
    @SuppressWarnings("unused")
    public String getIdentifier()
    {
        return mIdentifier;
    }

    /**
     * Set the identifier
     * @param identifier the identifiers
     */
    @SuppressWarnings("unused")
    public void setIdentifier(String identifier)
    {
        mIdentifier = identifier;
    }
}
