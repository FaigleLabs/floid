/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

import android.widget.EditText;

/**
 * Decimal with max and min edit text validator
 */
public class FloidDecimalWithMinAndMaxEditTextValidator extends FloidDecimalEditTextValidator
{
    private final EditText mMinValueEditText;
    private final EditText mMaxValueEditText;

    /**
     * Create a decimal with min and max edit text validator
     * @param editText the edit text
     * @param floidEditTextValidatorCallback the callback
     * @param useMinValue use min value
     * @param minValue min value
     * @param useMaxValue uee max value
     * @param maxValue max value
     * @param minValueEditText min value edit text
     * @param maxValueEditText max value edit text
     */
    public FloidDecimalWithMinAndMaxEditTextValidator(  EditText editText,
                                                        FloidEditTextValidatorCallback floidEditTextValidatorCallback,
                                                        @SuppressWarnings("SameParameterValue") boolean useMinValue,
                                                        double minValue,
                                                        @SuppressWarnings("SameParameterValue") boolean useMaxValue,
                                                        double maxValue,
                                                        EditText minValueEditText,
                                                        EditText maxValueEditText)
    {
        super(  editText,
                floidEditTextValidatorCallback,
                useMinValue,
                minValue,
                useMaxValue,
                maxValue);
        mMinValueEditText = minValueEditText;
        mMaxValueEditText = maxValueEditText;
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        double requiredMin;
        double requiredMax;
        double myValue;
        try
        {
            requiredMin = Double.valueOf(mMinValueEditText.getText().toString());
        }
        catch(Exception e)
        {
            mMinValueEditText.setError("Bad Format");
            return;
        }
        try
        {
            requiredMax = Double.valueOf(mMaxValueEditText.getText().toString());
        }
        catch(Exception e)
        {
            mMaxValueEditText.setError("Bad Format");
            return;
        }
        try
        {
            myValue = Double.valueOf(mEditText.getText().toString());
        }
        catch(Exception e)
        {
            mEditText.setError("Bad Format");
            return;
        }
        if(myValue < requiredMin)
        {
            mEditText.setError("Below Min");
            return;
        }
        if(myValue > requiredMax)
        {
            mEditText.setError("Above Max");
            return;
        }
        super.onTextChanged(s, start, before, count);
    }
}
