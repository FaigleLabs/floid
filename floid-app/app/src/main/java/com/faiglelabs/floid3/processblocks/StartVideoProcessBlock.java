/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for start video command
 */
@SuppressWarnings("SameParameterValue")
public class StartVideoProcessBlock extends ProcessBlock {
    /**
     * Create a new start video process block
     * @param timeoutInterval the timeout interval
     */
    public StartVideoProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Start Video");
    }
}
