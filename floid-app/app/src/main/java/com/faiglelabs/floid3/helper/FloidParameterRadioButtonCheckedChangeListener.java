/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.helper;

import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * Listens for checked change on a parameter radio button
 */
public class FloidParameterRadioButtonCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {
    private final int modifiedParameterValueTextColor;
    private final int unModifiedParameterValueTextColor;
    private final DroidParameterEditState droidParameterEditState;

    /**
     * Sets up a parameter ratio button check change listener
     * @param modifiedParameterValueTextColor color for modified parameter text
     * @param unModifiedParameterValueTextColor color for unmodified parameter text
     * @param droidParameterEditState the edit state for the parameter
     */
    public FloidParameterRadioButtonCheckedChangeListener(int modifiedParameterValueTextColor,
                                                          int unModifiedParameterValueTextColor,
                                                          DroidParameterEditState droidParameterEditState) {
        this.modifiedParameterValueTextColor = modifiedParameterValueTextColor;
        this.unModifiedParameterValueTextColor = unModifiedParameterValueTextColor;
        this.droidParameterEditState = droidParameterEditState;
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        RadioButton checkedRadioButton = group.findViewById(group.getCheckedRadioButtonId());
        droidParameterEditState.getEditText().setText(checkedRadioButton.getText());
        // Has the value been modified from its original value?
        if(checkedRadioButton.getText().toString().equalsIgnoreCase(droidParameterEditState.getOriginalValue())) {
            // No:
            droidParameterEditState.getEditText().setTextColor(unModifiedParameterValueTextColor);
            droidParameterEditState.setDirty(false);
        }
        else {
            // Yes:
            droidParameterEditState.getEditText().setTextColor(modifiedParameterValueTextColor);
            droidParameterEditState.setDirty(true);
        }
    }
}
