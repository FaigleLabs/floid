/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for the photo strobe command
 */
@SuppressWarnings("SameParameterValue")
public class PhotoStrobeProcessBlock extends ProcessBlock {
    /**
     * Create a new photo strobe process block
     * @param timeoutInterval the timeout interval
     */
    public PhotoStrobeProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Photo Strobe");
    }
}
