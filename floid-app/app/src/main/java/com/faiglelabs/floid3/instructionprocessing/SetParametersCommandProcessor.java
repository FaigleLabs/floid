/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import com.faiglelabs.floid.servertypes.commands.SetParametersCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.SetParametersProcessBlock;

/**
 * Command processor for set parameters command
 */
public class SetParametersCommandProcessor extends InstructionProcessor
{
	/**
	 * Create a set parameters command processor
	 * @param floidService the floid service
	 * @param setParametersCommand the set parameters command
	 */
	@SuppressWarnings("WeakerAccess")
	public SetParametersCommandProcessor(FloidService floidService, SetParametersCommand setParametersCommand)
	{
		super(floidService, setParametersCommand);
		useDefaultGoalProcessing = false;
		completeOnCommandResponse = false;

		// Set the parameters into the main:
		// Right now there are no parameters being passed in this command either to the droid or to the floid
		// This may be modified in the future so this command is stubbed out to handle it

		// At this point the parameters for the droid should be retrieved and any
		// parameters for the floid should be processed in process instruction below
		
		setProcessBlock(new SetParametersProcessBlock(FloidService.COMMAND_NO_TIMEOUT)); // This command uses the default time out
	}
	@Override
	public boolean setupInstruction()
	{
	    super.setupInstruction();
		synchronized (floidService.mFloidDroidStatusSynchronizer)
	    {
	        // Tell the droid to start:
	        floidService.getFloidDroidStatus().setDroidStarted(true);
        }
		processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
		return false; // NOT DONE...
	}
	@Override
	public boolean processInstruction()
	{
	    if(super.processInstruction())
	    {
	        return true;   // We are already done...
	    }
        SetParametersProcessBlock setParametersProcessBlock = (SetParametersProcessBlock)processBlock;
        // Go through and set the parameters from the parameters string plus the first pin:
        SetParametersCommand setParametersCommand = (SetParametersCommand)droidInstruction;
		if(setParametersCommand.isResetDroidParameters())
        {
            floidService.resetFloidDroidParameters();
        }
		//noinspection StatementWithEmptyBody
		if(setParametersCommand.isFirstPinIsHome())
        {
            // Does nothing - only affects Missions during processing on server
        }
        else
        {
            // Does nothing - only affects Missions during processing on server
        }
        if(setParametersCommand.getDroidParameters() != null)
        {
            if(!floidService.setFloidDroidParameters(setParametersCommand.getDroidParameters()))
            {
                setParametersProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return true;
            }
        }
        setParametersProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
        return true;
	}
	@Override
	public boolean tearDownInstruction()
	{
	    super.tearDownInstruction();
	    return true;
	}
}
