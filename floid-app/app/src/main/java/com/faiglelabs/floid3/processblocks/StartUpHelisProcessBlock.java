/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for start up helis command
 */
@SuppressWarnings("SameParameterValue")
public class StartUpHelisProcessBlock extends ProcessBlock {
    private boolean startupModeBegun = false;
    /**
     * Create a new start up helis process block
     * @param timeoutInterval the timeout interval
     */
    public StartUpHelisProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Start Up Helis");
    }

    /**
     * Has startup mode begun?
     * @return true if startup mode has begun
     */
    @SuppressWarnings("unused")
    public boolean isStartupModeBegun() {
        return startupModeBegun;
    }

    /**
     * Set startup mode begun
     * @param startupModeBegun true if startup mode has begun
     */
    public void setStartupModeBegun(boolean startupModeBegun) {
        this.startupModeBegun = startupModeBegun;
    }
}
