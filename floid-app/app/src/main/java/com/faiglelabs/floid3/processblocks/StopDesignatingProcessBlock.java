/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for stop designating command
 */
@SuppressWarnings("SameParameterValue")
public class StopDesignatingProcessBlock extends ProcessBlock {
    /**
     * Create a new stop designating process block
     * @param timeoutInterval the timeout interval
     */
    public StopDesignatingProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Stop Designating");
    }
}
