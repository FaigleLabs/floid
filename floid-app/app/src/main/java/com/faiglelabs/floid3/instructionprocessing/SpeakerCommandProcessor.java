/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import com.faiglelabs.floid.servertypes.commands.SpeakerCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.R;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.SpeakerProcessBlock;

import java.util.Locale;

/**
 * Command processor for the speaker command
 */
public class SpeakerCommandProcessor extends InstructionProcessor implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    private final static String TAG = "SpeakerCommandProcessor";

    private final static int REPEAT_COUNT_INFINITE = 0;
    private final static int REPEAT_COUNT_NONE = 1;
    private final static int REPEAT_COUNT_ONE = 2;
    private final static int REPEAT_COUNT_TWO = 3;
    private final static int REPEAT_COUNT_THREE = 4;
    private final static int REPEAT_COUNT_FIVE = 5;
    private final static int REPEAT_COUNT_TEN = 6;
    private final static int REPEAT_COUNT_TWENTY = 7;
    private final static int REPEAT_COUNT_ONE_HUNDRED = 8;

    private final SpeakerCommand mSpeakerCommand;
    private MediaPlayer mMediaPlayer = null;

    /**
     * Create a speaker command processor
     *
     * @param floidService   the floid service
     * @param speakerCommand the speaker command
     */
    public SpeakerCommandProcessor(FloidService floidService, SpeakerCommand speakerCommand) {
        super(floidService, speakerCommand);
        mSpeakerCommand = speakerCommand;
        setProcessBlock(new SpeakerProcessBlock(FloidService.COMMAND_NO_TIMEOUT, speakerCommand));        // No timeout
        useDefaultGoalProcessing = false;
        completeOnCommandResponse = false;
    }

    @Override
    public boolean setupInstruction() {
        super.setupInstruction();  // Sets processBlock to started
        SpeakerProcessBlock speakerProcessBlock = (SpeakerProcessBlock) processBlock;
        speakerProcessBlock.setToDefault();
        speakerProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_IN_PROGRESS);
        speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
        return false;
    }

    @Override
    public boolean processInstruction() {
        if (super.processInstruction()) {
            return true;   // Already done...
        }
        SpeakerProcessBlock speakerProcessBlock = (SpeakerProcessBlock) processBlock;
        Log.d(TAG, "Pr: " + speakerProcessBlock.getProcessBlockStatus());
        Log.d(TAG, "Sp: " + speakerProcessBlock.getSpeakerStatus());
        if (speakerProcessBlock.getProcessBlockStatus() == ProcessBlock.PROCESS_BLOCK_IN_PROGRESS) {
            speakerProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_STARTED);
            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Started");
        }
        // Process the current speaker status:
        switch (speakerProcessBlock.getSpeakerStatus()) {
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED: {
                // if we have an alert, start it, otherwise move to alert complete
                if (mSpeakerCommand.isAlert()) {
                    // Alert mode:
                    speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Alert");
                    // Start the alert play:
                    mMediaPlayer = MediaPlayer.create(floidService, R.raw.speaker_alert);
                    mMediaPlayer.setLooping(false);
                    mMediaPlayer.setOnErrorListener(this);
                    mMediaPlayer.setOnCompletionListener(this);
                    mMediaPlayer.setVolume((float) mSpeakerCommand.getVolume(), (float) mSpeakerCommand.getVolume());
                    mMediaPlayer.start();
                } else {
                    // Skip to alert complete:
                    speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_ALERT_COMPLETE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Alert Complete");
                }
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT: {
                // do nothing - we are waiting for the status to change to alert complete in the callback
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_ALERT_COMPLETE: {
                // Tear down the media player:
                if (mMediaPlayer != null) {
                    mMediaPlayer.release();
                    mMediaPlayer = null;
                }
                // Alert complete - start TTS or play File:
                if (mSpeakerCommand.getMode().equals("tts")) {
                    if (floidService.mTextToSpeech != null) {
                        // TTS Mode:
                        speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_TTS);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> In TTS");
                        // Check if the locale is already correct:
                        Locale preferredLocale = FloidService.getLanguageLocale(FloidService.safeMapStringGet(floidService.mDroidParametersHashMap, FloidService.DROID_PARAMETERS_SPEECH_LOCALE_OPTION));
                        if (!floidService.mTextToSpeech.getVoice().getLocale().equals(preferredLocale)) {
                            // Set the speech locale:
                            int result = floidService.mTextToSpeech.setLanguage(FloidService.getLanguageLocale(FloidService.safeMapStringGet(floidService.mDroidParametersHashMap, FloidService.DROID_PARAMETERS_SPEECH_LOCALE_OPTION)));
                            if (result == TextToSpeech.LANG_MISSING_DATA
                                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                                Log.e(TAG, "TTS: This Language is not supported");
                            }
                        }
                        floidService.mTextToSpeech.setPitch((float) mSpeakerCommand.getPitch());
                        floidService.mTextToSpeech.setSpeechRate((float) mSpeakerCommand.getRate());
                        // Set the volume for the TTS:
                        AudioManager audioManager = (AudioManager) floidService.getSystemService(Context.AUDIO_SERVICE);
                        if(audioManager!=null) {
                            int amStreamMusicMaxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) (amStreamMusicMaxVol * mSpeakerCommand.getVolume()), 0);
                            floidService.mTextToSpeech.speak(mSpeakerCommand.getTts(), TextToSpeech.QUEUE_FLUSH, null, "");
                        } else {
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Failed to get Audio Manager");
                        }
                    } else {
                        speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_TTS_COMPLETE);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Failed to initialize TTS");
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> TTS Complete");
                    }
                } else {
                    // File mode:
                    speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_FILE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> In File");
                    // Start the media play for the file:
                    Uri fileUri = Uri.parse("file://" + mSpeakerCommand.getFileName());
                    mMediaPlayer = MediaPlayer.create(floidService, fileUri);
                    mMediaPlayer.setLooping(false);
                    mMediaPlayer.setOnErrorListener(this);
                    mMediaPlayer.setOnCompletionListener(this);
                    mMediaPlayer.setVolume((float) mSpeakerCommand.getVolume(), (float) mSpeakerCommand.getVolume());
                    mMediaPlayer.start();
                }
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_TTS: {
                // Wait for TTS to finish:
                if (!floidService.mTextToSpeech.isSpeaking()) {
                    speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_TTS_COMPLETE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> TTS Complete");
                }
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_TTS_COMPLETE: {
                // if we have a delay, then start it, otherwise move to delay complete
                if (mSpeakerCommand.getDelay() > 0) {
                    // Start the delay:
                    speakerProcessBlock.setDelayEnd(System.currentTimeMillis() + (mSpeakerCommand.getDelay() * 1000L));
                    speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_DELAY);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> In Delay");
                } else {
                    speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_DELAY_COMPLETE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Delay Complete");
                }
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_FILE: {
                // do nothing, we are waiting for the file to complete in the call-back
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_FILE_COMPLETE: {
                // Tear down the media player:
                if (mMediaPlayer != null) {
                    mMediaPlayer.release();
                    mMediaPlayer = null;
                }
                // if we have a delay, then start it, otherwise move to delay complete
                if (mSpeakerCommand.getDelay() > 0) {
                    // Start the delay:
                    speakerProcessBlock.setDelayEnd(System.currentTimeMillis() + (mSpeakerCommand.getDelay() * 1000L));
                    speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_DELAY);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> In Delay");
                } else {
                    speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_DELAY_COMPLETE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Delay Complete");
                }
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_DELAY: {
                // check delay and move to delay complete if done
                if (System.currentTimeMillis() > speakerProcessBlock.getDelayEnd()) {
                    // Done with the delay:
                    speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_DELAY_COMPLETE);
                    floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Delay Complete");
                }
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_DELAY_COMPLETE: {
                // We are done with this loop, bump our repeat count:
                speakerProcessBlock.increaseRepeatCount();
                // See if we are to continue, otherwise move to complete:
                switch (mSpeakerCommand.getSpeakerRepeat()) {
                    case REPEAT_COUNT_INFINITE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Repeat Forever");
                        speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Not started");
                    }
                    break;
                    case REPEAT_COUNT_NONE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Repeat none");
                        speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Complete");
                    }
                    break;
                    case REPEAT_COUNT_ONE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Repeat 1 time [Remain: " + (2 - speakerProcessBlock.getRepeatCount()) + "]");
                        if (speakerProcessBlock.getRepeatCount() > 1) {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Complete");
                        } else {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_TWO:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Repeat 2 times [Remain: " + (3 - speakerProcessBlock.getRepeatCount()) + "]");
                        if (speakerProcessBlock.getRepeatCount() > 2) {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Complete");
                        } else {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_THREE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Repeat 3 times [Remain: " + (4 - speakerProcessBlock.getRepeatCount()) + "]");
                        if (speakerProcessBlock.getRepeatCount() > 3) {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Complete");
                        } else {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_FIVE:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Repeat 5 times [Remain: " + (6 - speakerProcessBlock.getRepeatCount()) + "]");
                        if (speakerProcessBlock.getRepeatCount() > 5) {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Complete");
                        } else {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_TEN:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Repeat 10 times [Remain: " + (11 - speakerProcessBlock.getRepeatCount()) + "]");
                        if (speakerProcessBlock.getRepeatCount() > 10) {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Complete");
                        } else {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_TWENTY:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Repeat 20 times [Remain: " + (21 - speakerProcessBlock.getRepeatCount()) + "]");
                        if (speakerProcessBlock.getRepeatCount() > 20) {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Complete");
                        } else {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Not started");
                        }
                    }
                    break;
                    case REPEAT_COUNT_ONE_HUNDRED:
                    {
                        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Repeat 100 times [Remain: " + (101 - speakerProcessBlock.getRepeatCount()) + "]");
                        if (speakerProcessBlock.getRepeatCount() > 100) {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Complete");
                        } else {
                            speakerProcessBlock.setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_NOT_STARTED);
                            floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Not started");
                        }
                    }
                    break;
                }
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE: {
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker Process Block -> Complete Success");
                return true; // We are done...
            }
        }
        return false; // Not done
    }

    @Override
    public boolean tearDownInstruction() {
        super.tearDownInstruction();
        floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Speaker command: instruction tear down");

        // Tear down this right:
        switch (((SpeakerProcessBlock) processBlock).getSpeakerStatus()) {
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT:
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_FILE: {
                if (mMediaPlayer != null) {
                    mMediaPlayer.stop();
                    mMediaPlayer.release();
                    mMediaPlayer = null;
                }
            }
            break;
            case SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_TTS: {
                if (floidService.mTextToSpeech != null) {
                    floidService.mTextToSpeech.stop();
                }
            }
            break;
            default: {
                // Do nothing
            }
        }
        ((SpeakerProcessBlock) processBlock).setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_COMPLETE);
        return true;
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        // Alert completion?
        if (((SpeakerProcessBlock) processBlock).getSpeakerStatus() == SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT) {
            ((SpeakerProcessBlock) processBlock).setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_ALERT_COMPLETE);
        } else {
            // Must be file completion
            ((SpeakerProcessBlock) processBlock).setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_FILE_COMPLETE);
        }
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        return true;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (((SpeakerProcessBlock) processBlock).getSpeakerStatus() == SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_IN_ALERT) {
            ((SpeakerProcessBlock) processBlock).setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_ALERT_COMPLETE);
        } else {
            // Must be file completion
            ((SpeakerProcessBlock) processBlock).setSpeakerStatus(SpeakerProcessBlock.SPEAKER_PROCESS_STATUS_FILE_COMPLETE);
        }
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }
}
