/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;
import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import com.faiglelabs.floid.servertypes.commands.ShutDownHelisCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.ShutDownHelisProcessBlock;

/**
 * Command processor for the shut down helis command
 */
public class ShutDownHelisCommandProcessor extends InstructionProcessor {
    /**
     * Create a shut down helis command processor
     *
     * @param floidService         the floid service
     * @param shutDownHelisCommand the shut down helis command
     */
    @SuppressWarnings("WeakerAccess")
    public ShutDownHelisCommandProcessor(FloidService floidService, ShutDownHelisCommand shutDownHelisCommand) {
        super(floidService, shutDownHelisCommand);
        useDefaultGoalProcessing = false;
        completeOnCommandResponse = false;
        setProcessBlock(new ShutDownHelisProcessBlock(FloidService.COMMAND_NO_TIMEOUT));  // This command uses the default time out
    }

    @Override
    public boolean setupInstruction() {
        super.setupInstruction();
        // Log it:
        if (floidService.mUseLogger) Log.d(FloidService.TAG, "===>SETUP " + this.getDroidInstruction().getType());
        if (!floidService.getFloidDroidStatus().isFloidConnected()) {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
        ShutDownHelisProcessBlock shutDownHelisProcessBlock = (ShutDownHelisProcessBlock) processBlock;
        shutDownHelisProcessBlock.setFloidGoalId(FloidService.NO_GOAL_ID);  // Does not use a goal id - done when command response is received
        try {
            FloidOutgoingCommand shutDownHelisOutgoingCommand = floidService.sendShutDownHelisCommandToFloid(shutDownHelisProcessBlock.getFloidGoalId());
            if (shutDownHelisOutgoingCommand != null) {
                shutDownHelisProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                shutDownHelisProcessBlock.setProcessBlockWaitForCommandNumber(shutDownHelisOutgoingCommand.getCommandNumber());
                return true;
            } else {
                if (floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Shut Down Helis command.");
                // We must have failed - set completion with error:
                shutDownHelisProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        } catch (Exception e) {
            if (floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;  // We are done with this whole command
    }

    @Override
    public boolean processInstruction() {
        ShutDownHelisProcessBlock shutDownHelisProcessBlock = (ShutDownHelisProcessBlock) processBlock;
        if (shutDownHelisProcessBlock.getProcessBlockStatus() == ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE) {
            // Did we time out or fail?
            boolean processInstructionResult = super.processInstruction();
            if (processInstructionResult) {
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "PI-1 shut down helis done");
            }
            return processInstructionResult;
        } else {
            // If the helis are off, we continue...
            if (!floidService.getFloidStatus().isDh()) {
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "FloidStatus: Helis Off");
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "ShutDownHelis: Complete");
                floidService.getFloidDroidStatus().setHelisStarted(false); // Helis are stopped!
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                return true; // We had success
            }
            // Let the processor determine time out etc:
            if (super.processInstruction()) {
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "PI-2 shut down helis done");
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean tearDownInstruction() {
        super.tearDownInstruction();
        return true;
    }
}
