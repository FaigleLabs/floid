/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.controller;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.faiglelabs.floid3.FloidActivity;
import com.faiglelabs.floid3.messages.FloidInterfaceEnableMessage;
import com.faiglelabs.floid3.messages.FloidInterfaceSetTextMessage;
import com.faiglelabs.floid3.validator.FloidDecimalEditTextValidator;
import com.faiglelabs.floid3.validator.FloidDecimalWithMaxEditTextValidator;
import com.faiglelabs.floid3.validator.FloidDecimalWithMinAndMaxEditTextValidator;
import com.faiglelabs.floid3.validator.FloidDecimalWithMinEditTextValidator;
import com.faiglelabs.floid3.validator.FloidEditTextValidatorCallback;

/**
 * Floid IMU emulation algorithm option controller
 */
@SuppressWarnings("SameParameterValue")
public class FloidIMUEmulationAlgorithmOptionController {
    /**
     * no emulation algorithm
     */
    public final static int FLOID_PYR_EMULATION_ALGORITHM_NONE = 0;
    /**
     * bounce emulation algorithm
     */
    @SuppressWarnings("WeakerAccess")
    public final static int FLOID_PYR_EMULATION_ALGORITHM_BOUNCE = 1;
    /**
     * up emulation algorithm
     */
    @SuppressWarnings("WeakerAccess")
    public final static int FLOID_PYR_EMULATION_ALGORITHM_UP = 2;
    /**
     * down emulation algorithm
     */
    @SuppressWarnings("WeakerAccess")
    public final static int FLOID_PYR_EMULATION_ALGORITHM_DOWN = 3;

    /*
        pyr_emulation_algorithm_rates:
            10Hz   100
            6Hz    166
            4Hz    250
            2Hz    500
            1.5    666
            1Hz    1000
            0.66Hz 1500  
            0.5Hz  2000
            0.2Hz  5000
            0.1Hz  10000
            0.2Hz  20000
    */
    /**
     * emulation algorithm rates
     */
    private static final int[] mFloidPyrEmulationAlgorithmRates =
            {
                    100, 166, 250, 500, 666, 1000, 1500, 2000, 3000, 10000, 20000
            };

    /**
     * Get floid pyr emulation algorithm rates.
     *
     * @return the pyr emulation algorithm rates
     */
    public static int[] getmFloidPyrEmulationAlgorithmRates() {
        return mFloidPyrEmulationAlgorithmRates.clone();
    }

    private final Handler mInterfaceMessageHandler;
    private final EditText mMinEditText;
    private final EditText mMaxEditText;
    private final EditText mRateEditText;
    private final EditText mStartEditText;
    private final TextView mCurrentValueTextView;
    private final Spinner mAlgorithmSpinner;
    @SuppressWarnings("FieldCanBeLocal")
    private final TextView mAbsoluteMinTextView;
    @SuppressWarnings("FieldCanBeLocal")
    private final TextView mAbsoluteMaxTextView;
    private double mCurrentValue;
    private double mMin;
    private double mMax;
    @SuppressWarnings("FieldCanBeLocal")
    private final double mAbsoluteMin;
    @SuppressWarnings("FieldCanBeLocal")
    private final double mAbsoluteMax;
    private double mRate;
    private double mStart;
    private int mAlgorithm;
    private boolean mDirectionUp;

    /**
     * Create an IMU emulation algorithm option controller
     *
     * @param interfaceMessageHandler the interface message handler
     * @param view                    the view
     * @param minEditTextViewId       the min edit text view id
     * @param maxEditTextViewId       the max edit text view id
     * @param rateEditTextViewId      the rate edit text view id
     * @param startEditTextViewId     the start edit text view id
     * @param currentValueTextViewId  the current edit text view id
     * @param algorithmSpinnerId      the algorithm spinner id
     * @param absoluteMinTextViewId   the absolute min text view id
     * @param absoluteMaxTextViewId   the absolute max text view id
     * @param currentValue            the current value
     * @param min                     the min
     * @param max                     the max
     * @param absoluteMin             the absolute min
     * @param absoluteMax             the absolute max
     * @param rate                    the rate
     * @param rateMin                 the rate min
     * @param rateMax                 the rate max
     * @param start                   the start
     * @param algorithm               the algorithm
     */
    @SuppressLint("DefaultLocale")
    public FloidIMUEmulationAlgorithmOptionController(Handler interfaceMessageHandler,
                                                      View view,
                                                      int minEditTextViewId,
                                                      int maxEditTextViewId,
                                                      int rateEditTextViewId,
                                                      int startEditTextViewId,
                                                      int currentValueTextViewId,
                                                      int algorithmSpinnerId,
                                                      int absoluteMinTextViewId,
                                                      int absoluteMaxTextViewId,
                                                      double currentValue,
                                                      double min,
                                                      double max,
                                                      double absoluteMin,
                                                      double absoluteMax,
                                                      double rate,
                                                      double rateMin,
                                                      double rateMax,
                                                      double start,
                                                      int algorithm) {
        mInterfaceMessageHandler = interfaceMessageHandler;
        mCurrentValue = currentValue;
        mMin = min;
        mMax = max;
        mRate = rate;
        mStart = start;
        mAlgorithm = algorithm;
        mAbsoluteMin = absoluteMin;
        mAbsoluteMax = absoluteMax;
        mMinEditText = view.findViewById(minEditTextViewId);
        mMaxEditText = view.findViewById(maxEditTextViewId);
        mRateEditText = view.findViewById(rateEditTextViewId);
        mStartEditText = view.findViewById(startEditTextViewId);
        mCurrentValueTextView = view.findViewById(currentValueTextViewId);
        mAlgorithmSpinner = view.findViewById(algorithmSpinnerId);
        mAbsoluteMinTextView = view.findViewById(absoluteMinTextViewId);
        mAbsoluteMaxTextView = view.findViewById(absoluteMaxTextViewId);
        mMinEditText.setText(String.format("%.2f", mMin));
        mMaxEditText.setText(String.format("%.2f", mMax));
        mRateEditText.setText(String.format("%.2f", mRate));
        mStartEditText.setText(String.format("%.2f", mStart));
        mAbsoluteMinTextView.setText(String.format("%.2f", mAbsoluteMin));
        mAbsoluteMaxTextView.setText(String.format("%.2f", mAbsoluteMax));
//        mCurrentValue = mStart;
        mCurrentValueTextView.setText(String.format("%.2f", mCurrentValue));
        mAlgorithmSpinner.setSelection(algorithm);
        // Set the direction from the algorithm convenience routine
        setDirectionFromAlgorithm();
        mMinEditText.addTextChangedListener(new FloidDecimalWithMaxEditTextValidator(mMinEditText,
                new FloidEditTextValidatorCallback() {
                    @Override
                    public void updateInterface() {
                        try {
                            mMin = Double.parseDouble(mMinEditText.getText().toString());
                        } catch (NumberFormatException nfe) {
                            // Do nothing
                        }
                    }
                },
                true,
                absoluteMin,
                true,
                absoluteMax,
                mMaxEditText));

        mMaxEditText.addTextChangedListener(new FloidDecimalWithMinEditTextValidator(mMaxEditText,
                new FloidEditTextValidatorCallback() {
                    @Override
                    public void updateInterface() {
                        try {
                            mMax = Double.parseDouble(mMaxEditText.getText().toString());
                        } catch (NumberFormatException nfe) {
                            // Do nothing
                        }
                    }
                },
                true,
                absoluteMin,
                true,
                absoluteMax,
                mMinEditText));

        mRateEditText.addTextChangedListener(new FloidDecimalEditTextValidator(mRateEditText,
                new FloidEditTextValidatorCallback() {
                    @Override
                    public void updateInterface() {
                        try {
                            mRate = Double.parseDouble(mRateEditText.getText().toString());
                        } catch (NumberFormatException nfe) {
                            // Do nothing
                        }
                    }
                },
                true,
                rateMin,
                true,
                rateMax));

        mStartEditText.addTextChangedListener(new FloidDecimalWithMinAndMaxEditTextValidator(mStartEditText,
                new FloidEditTextValidatorCallback() {
                    @Override
                    public void updateInterface() {
                        try {
                            mStart = Double.parseDouble(mStartEditText.getText().toString());
                        } catch (NumberFormatException nfe) {
                            // Do nothing
                        }
                    }
                },
                true,
                absoluteMin,
                true,
                absoluteMax,
                mMinEditText,
                mMaxEditText));

        // Add the algorithm spinner listener and make sure to change direction if needed:
        mAlgorithmSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mAlgorithm = position;
                mCurrentValue = mStart;
                //noinspection DuplicateBranchesInSwitch
                switch (mAlgorithm) {
                    case FLOID_PYR_EMULATION_ALGORITHM_NONE: {
                        // Do nothing...
                    }
                    break;
                    case FLOID_PYR_EMULATION_ALGORITHM_BOUNCE:
                    {
                        // Do nothing...
                    }
                    break;
                    case FLOID_PYR_EMULATION_ALGORITHM_UP: {
                        mDirectionUp = true;
                    }
                    break;
                    case FLOID_PYR_EMULATION_ALGORITHM_DOWN: {
                        mDirectionUp = false;
                    }
                    break;
                    default: {
                        // Do nothing
                    }
                    break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    /**
     * Execute a process step
     *
     * @return the emulation algorithm process result
     */
    public FloidIMUEmulationAlgorithmProcessResult processStep() {
        FloidIMUEmulationAlgorithmProcessResult floidPyrEmulationAlgorithmProcessResult = new FloidIMUEmulationAlgorithmProcessResult();
        floidPyrEmulationAlgorithmProcessResult.setSuccess(false);
        floidPyrEmulationAlgorithmProcessResult.setValue(mCurrentValue);
        if (mAlgorithm == FLOID_PYR_EMULATION_ALGORITHM_NONE) {
            return floidPyrEmulationAlgorithmProcessResult;
        }
        // Here we take one step...
        // Make sure we are all nicely in range:
        setDirectionFromAlgorithm();
        if (mCurrentValue > mMax) mCurrentValue = mMax;
        if (mCurrentValue < mMin) mCurrentValue = mMin;
        if (mDirectionUp) {
            mCurrentValue += mRate;
            switch (mAlgorithm) {
                case FLOID_PYR_EMULATION_ALGORITHM_BOUNCE: {
                    if (mCurrentValue >= mMax) {
                        mCurrentValue = mMax;
                        mDirectionUp = false;
                        floidPyrEmulationAlgorithmProcessResult.setSuccess(false);
                        floidPyrEmulationAlgorithmProcessResult.setValue(mCurrentValue);
                    }
                }
                break;
                case FLOID_PYR_EMULATION_ALGORITHM_UP: {
                    if (mCurrentValue > mMax) {
                        mCurrentValue = mMin;
                    }
                    floidPyrEmulationAlgorithmProcessResult.setSuccess(false);
                    floidPyrEmulationAlgorithmProcessResult.setValue(mCurrentValue);
                }
                break;
                case FLOID_PYR_EMULATION_ALGORITHM_DOWN: {
                    // LOGIC ERROR:
                }
                break;
            }
        } else {
            // mDirectionUp is false:
            mCurrentValue -= mRate;
            switch (mAlgorithm) {
                case FLOID_PYR_EMULATION_ALGORITHM_BOUNCE: {
                    if (mCurrentValue <= mMin) {
                        mCurrentValue = mMin;
                        mDirectionUp = true;
                    }
                    floidPyrEmulationAlgorithmProcessResult.setSuccess(false);
                    floidPyrEmulationAlgorithmProcessResult.setValue(mCurrentValue);
                }
                break;
                case FLOID_PYR_EMULATION_ALGORITHM_UP: {
                    // LOGIC ERROR:
                }
                break;
                case FLOID_PYR_EMULATION_ALGORITHM_DOWN: {
                    if (mCurrentValue < mMin) {
                        mCurrentValue = mMax;
                    }
                    floidPyrEmulationAlgorithmProcessResult.setSuccess(false);
                    floidPyrEmulationAlgorithmProcessResult.setValue(mCurrentValue);
                }
                break;
                default: {
                    // Do nothing
                }
            }
        }
        setCurrentValue(mCurrentValue); // Also sends message to update the interface so not redundant
        return floidPyrEmulationAlgorithmProcessResult;
    }

    private void setDirectionFromAlgorithm() {
        switch (mAlgorithm) {
            case FLOID_PYR_EMULATION_ALGORITHM_UP:
                mDirectionUp = true;
                break;
            case FLOID_PYR_EMULATION_ALGORITHM_DOWN:
                mDirectionUp = false;
                break;
            default:
                // do nothing!!!
                break;
        }
    }

    /**
     * Is enabled boolean.
     *
     * @return the mEnabled
     */
    @SuppressWarnings("unused")
    public boolean isEnabled() {
        return mAlgorithm > FLOID_PYR_EMULATION_ALGORITHM_NONE;
    }

    /**
     * Gets current value.
     *
     * @return the mCurrentValue
     */
    @SuppressWarnings("unused")
    public double getCurrentValue() {
        return mCurrentValue;
    }

    /**
     * Gets min.
     *
     * @return the mMin
     */
    @SuppressWarnings("unused")
    public double getMin() {
        return mMin;
    }

    /**
     * Gets max.
     *
     * @return the mMax
     */
    @SuppressWarnings("unused")
    public double getMax() {
        return mMax;
    }

    /**
     * Gets rate.
     *
     * @return the mRate
     */
    @SuppressWarnings("unused")
    public double getRate() {
        return mRate;
    }

    /**
     * Gets start.
     *
     * @return the mStart
     */
    @SuppressWarnings("unused")
    public double getStart() {
        return mStart;
    }

    /**
     * Gets algorithm.
     *
     * @return the mAlgorithm
     */
    @SuppressWarnings("unused")
    public int getAlgorithm() {
        return mAlgorithm;
    }

    /**
     * Is direction up boolean.
     *
     * @return the mDirectionUp
     */
    @SuppressWarnings("unused")
    public boolean isDirectionUp() {
        return mDirectionUp;
    }

    /**
     * Sets current value.
     *
     * @param currentValue the mCurrentValue to set
     */
    @SuppressWarnings("WeakerAccess")
    public void setCurrentValue(double currentValue) {
        this.mCurrentValue = currentValue;
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_SET_TEXT, new FloidInterfaceSetTextMessage(mCurrentValueTextView, Double.toString(mCurrentValue))).sendToTarget();
    }

    /**
     * Sets min.
     *
     * @param min the mMin to set
     */
    @SuppressWarnings("unused")
    public void setMin(double min) {
        this.mMin = min;
    }

    /**
     * Sets max.
     *
     * @param max the mMax to set
     */
    @SuppressWarnings("unused")
    public void setMax(double max) {
        this.mMax = max;
    }

    /**
     * Sets rate.
     *
     * @param rate the mRate to set
     */
    @SuppressWarnings("unused")
    public void setRate(double rate) {
        this.mRate = rate;
    }

    /**
     * Sets start.
     *
     * @param start the mStart to set
     */
    @SuppressWarnings("unused")
    public void setStart(double start) {
        this.mStart = start;
    }

    /**
     * Sets algorithm.
     *
     * @param algorithm the mAlgorithm to set
     */
    @SuppressWarnings("unused")
    public void setAlgorithm(int algorithm) {
        this.mAlgorithm = algorithm;
    }

    /**
     * Sets direction up.
     *
     * @param directionUp the mDirectionUp to set
     */
    @SuppressWarnings("unused")
    public void setDirectionUp(boolean directionUp) {
        this.mDirectionUp = directionUp;
    }

    /**
     * Set enable
     *
     * @param enable true if enable
     */
    public void setEnable(boolean enable) {
        // Send the messages to the interface to enable this:
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mMinEditText, enable)).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mMaxEditText, enable)).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mRateEditText, enable)).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mStartEditText, enable)).sendToTarget();
        mInterfaceMessageHandler.obtainMessage(FloidActivity.FLOID_UX_MESSAGE_INTERFACE_ENABLE, new FloidInterfaceEnableMessage(mAlgorithmSpinner, enable)).sendToTarget();
    }

    /**
     * Reset current value to start
     */
    public void resetToStart() {
        mCurrentValue = mStart;
    }
}
