/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.PanTiltCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.PanTiltProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for the pan tilt command
 */
public class PanTiltCommandProcessor extends InstructionProcessor
{
    /**
     * Create a pan tilt command processor
     * @param floidService the floid service
     * @param panTiltCommand the pan tilt command
     */
    @SuppressWarnings("WeakerAccess")
	public PanTiltCommandProcessor(FloidService floidService, PanTiltCommand panTiltCommand)
	{
		super(floidService, panTiltCommand);
		useDefaultGoalProcessing = false;
        completeOnCommandResponse = true;
		setProcessBlock(new PanTiltProcessBlock(defaultGoalTimeout));
	}
	@Override
	public boolean setupInstruction()
	{
	    // General idea:
	    // This is generally the same as flyTo and turnTo...
	    super.setupInstruction();
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
	    if(!floidService.getFloidDroidStatus().isLiftedOff())
	    {
	        // Logic error - must be lifted off to panTilt:
            sendCommandLogicErrorMessage("Not lifted off.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
	    }
	    // OK, passed all tests...
        PanTiltCommand      panTiltCommand      = (PanTiltCommand)droidInstruction;
        PanTiltProcessBlock panTiltProcessBlock = (PanTiltProcessBlock)processBlock;
        panTiltProcessBlock.setFloidGoalId(FloidService.NO_GOAL_ID);  // Does not use a goal id - done once packet response is received
        
        try
        {
            FloidOutgoingCommand panTiltOutgoingCommand;
            if(panTiltCommand.isUseAngle())
            {
                panTiltOutgoingCommand = floidService.sendPanTiltCommandToFloid(panTiltProcessBlock.getFloidGoalId(),
                                                                                 (byte)(panTiltCommand.getDevice()&0x000000ff),
                                                                                 (byte) FloidService.PAN_TILT_PACKET_TYPE_ANGLE,
                                                                                 (float)panTiltCommand.getPan(),
                                                                                 (float)panTiltCommand.getTilt(),
                                                                                 0,
                                                                                 0,
                                                                                 0);
            }
            else
            {
                panTiltOutgoingCommand = floidService.sendPanTiltCommandToFloid(panTiltProcessBlock.getFloidGoalId(),
                                                                                 (byte)(panTiltCommand.getDevice()&0x000000ff),
                                                                                 (byte) FloidService.PAN_TILT_PACKET_TYPE_TARGET,
                                                                                 0,
                                                                                 0,
                                                                                 (float)panTiltCommand.getX(),
                                                                                 (float)panTiltCommand.getY(),
                                                                                 (float)panTiltCommand.getZ());
                
            }
            if(panTiltOutgoingCommand != null)
            {
                panTiltProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                panTiltProcessBlock.setProcessBlockWaitForCommandNumber(panTiltOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Pan Tilt command.");
                // We must have failed - set completion with error:
                panTiltProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }

        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;    // We are done with this whole command
	}
	@Override
	public boolean tearDownInstruction()
	{
        super.tearDownInstruction();
        return true;
	}
}
