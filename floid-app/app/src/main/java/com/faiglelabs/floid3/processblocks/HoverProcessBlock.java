/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3.processblocks;

/**
 * Process block for hover command
 */
@SuppressWarnings("SameParameterValue")
public class HoverProcessBlock extends ProcessBlock {
    private long startTime = 0L;

    /**
     * Create a new hover process block
     * @param timeoutInterval the timeout interval
     */
    public HoverProcessBlock(long timeoutInterval) {
        super(timeoutInterval, "Hover");
    }

    /**
     * Get the start time
     * @return the start time
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * Set the start time
     * @param startTime the start times
     */
    public void setStartTime(long startTime)
    {
        this.startTime = startTime;
    }
}
