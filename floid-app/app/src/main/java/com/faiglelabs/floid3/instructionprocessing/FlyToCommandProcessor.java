/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;

import com.faiglelabs.floid.servertypes.commands.FlyToCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.FlyToProcessBlock;
import com.faiglelabs.floid3.processblocks.ProcessBlock;

/**
 * Command processor for fly to command
 */
public class FlyToCommandProcessor extends InstructionProcessor
{
    /**
     * Create a fly to command processor
     * @param floidService the floid service
     * @param flyToCommand the fly to command
     */
    @SuppressWarnings("WeakerAccess")
	public FlyToCommandProcessor(FloidService floidService, FlyToCommand flyToCommand)
	{
		super(floidService, flyToCommand);
		useDefaultGoalProcessing = true;
        completeOnCommandResponse = false;
        setProcessBlock(new FlyToProcessBlock(noGoalTimeout));    // Does not time out...
	}
    @Override
    public boolean setupInstruction()
    {
        super.setupInstruction();
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called            
        }
        if(!floidService.getFloidDroidStatus().isLiftedOff())
        {
            // Logic error - must be lifted off to FlyTo:
            sendCommandLogicErrorMessage("Not lifted off.");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
        }
        // Log it:
        if(floidService.mUseLogger) Log.d(FloidService.TAG, "===>SETUP " + this.getDroidInstruction().getType());
        FlyToCommand      flyToCommand      = (FlyToCommand)droidInstruction;
        FlyToProcessBlock flyToProcessBlock = (FlyToProcessBlock)processBlock;
        flyToProcessBlock.setFloidGoalId(floidService.getNextFloidGoalId());
        try
        {
            FloidOutgoingCommand flyToOutgoingCommand;
            if(flyToCommand.isFlyToHome())
            {
                 flyToOutgoingCommand = floidService.sendFlyToCommandToFloid(flyToProcessBlock.getFloidGoalId(), (float) floidService.getFloidDroidStatus().getHomePositionX(),(float) floidService.getFloidDroidStatus().getHomePositionY());
            }
            else
            {
                flyToOutgoingCommand = floidService.sendFlyToCommandToFloid(flyToProcessBlock.getFloidGoalId(), (float)flyToCommand.getX(),(float)flyToCommand.getY());
            }
            if(flyToOutgoingCommand != null)
            {
                flyToProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                flyToProcessBlock.setProcessBlockWaitForCommandNumber(flyToOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Fly To command.");
                // We must have failed - set completion with error:
                flyToProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;  // We are done with this whole command
    }
    @Override
    public boolean tearDownInstruction()
    {
        super.tearDownInstruction();
        return true;
    }
}
