/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.floid3;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.*;
import android.content.*;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Process;
import android.os.*;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import com.faiglelabs.floid.servertypes.commands.NullCommand;
import com.faiglelabs.floid.servertypes.instruction.DroidInstruction;
import com.faiglelabs.floid.servertypes.mission.DroidMission;
import com.faiglelabs.floid.servertypes.statuses.FloidDroidMissionInstructionStatusMessage;
import com.faiglelabs.floid.servertypes.statuses.FloidDroidStatus;
import com.faiglelabs.floid.servertypes.statuses.FloidMessage;
import com.faiglelabs.floid.servertypes.statuses.FloidSystemLog;
import com.faiglelabs.floid.utils.FloidServerMessageUtils;
import com.faiglelabs.floid3.helper.FloidDebugMessageAndTag;
import com.faiglelabs.floid3.imaging.FloidImaging;
import com.faiglelabs.floid3.imaging.FloidServiceImage;
import com.faiglelabs.floid3.imaging.FloidServicePhoto;
import com.faiglelabs.floid3.imaging.FloidServiceVideoFrame;
import com.faiglelabs.floid3.instructionprocessing.InstructionProcessor;
import com.faiglelabs.floid3.instructionprocessing.InstructionProcessorFactory;
import com.faiglelabs.floid3.instructionprocessing.SpeakerOnCommandProcessor;
import com.faiglelabs.floid3.parcelables.*;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.utilities.UploadController;
import com.faiglelabs.proto.floid.FloidServerMessages.FloidClientMessage;
import com.faiglelabs.proto.floid.FloidServerMessages.FloidServerMessage;
import com.google.protobuf.ByteString;
import org.apache.commons.collections4.QueueUtils;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * The type Floid service.
 */
@SuppressWarnings("WeakerAccess")
public class FloidService extends Service implements TextToSpeech.OnInitListener {
    // Our tag for debugging:
    /**
     * The constant TAG.
     */
    public static final String TAG = "FloidService";
    /**
     * The floid service PID:
     */
    private int mFloidServicePid;

    /**
     * Get the floid service pid
     * @return the floid service pid
     */
    public int getFloidServicePid() {
        return mFloidServicePid;
    }
    /**
     * Our constant channel id for notifications
     */
    private static final String FLOID_SERVICE_NOTIFICATION_CHANNEL_ID = "FloidServiceNotificationChannelId";
    /**
     * The Floid Database Name:
     */
    private final static String FLOID_DATABASE_NAME = "floid.db";
    /**
     * The id for the service foreground notification
     */
    private final static int FLOID_SERVICE_FOREGROUND_NOTIFICATION_ID = 1;

    // Location Listener:
    /**
     * The location manager.
     */
    private LocationManager mLocationManager;
    /**
     * The location provider.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private String mLocationProvider;
    private FloidImaging mFloidImaging;
    /**
     * The floid service handler.
     */
    private final Handler mFloidServiceHandler = new Handler(new IncomingHandler());
    /**
     * The floid service messenger.
     */
    private final Messenger mFloidServiceMessenger = new Messenger(mFloidServiceHandler);
    /**
     * The floid service running notification.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private Notification mFloidServiceRunningNotification = null;
    /**
     * The floid activity messenger.
     */
    private Messenger mFloidActivityMessenger = null;
    /**
     * The additional status items for the floid and droid
     */
    public final AdditionalStatus mAdditionalStatus = new AdditionalStatus();
    /**
     * The battery status timer.
     */
    private Timer batteryStatusTimer = null;
    // Intent actions:
    /**
     * The constant KILL_SERVICE_INTENT_ACTION.
     */
    public static final String KILL_SERVICE_INTENT_ACTION = "com.faiglelabs.floid3.action.KILL_SERVICE";
    // Floid Service Messages:
    /**
     * The constant FLOID_SERVICE_MESSAGE_PING.
     */
    public static final int FLOID_SERVICE_MESSAGE_PING = 1;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SEND_COMMAND_TO_FLOID.
     */
    public static final int FLOID_SERVICE_MESSAGE_SEND_COMMAND_TO_FLOID = 2;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SET_FLOID_ID.
     */
    public static final int FLOID_SERVICE_MESSAGE_SET_FLOID_ID = 3;
    /**
     * The constant FLOID_SERVICE_MESSAGE_GET_FLOID_IDS.
     */
    public static final int FLOID_SERVICE_MESSAGE_GET_FLOID_IDS = 4;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SET_HOST.
     */
    public static final int FLOID_SERVICE_MESSAGE_SET_HOST = 5;
    /**
     * The constant FLOID_SERVICE_MESSAGE_GET_HOST.
     */
    public static final int FLOID_SERVICE_MESSAGE_GET_HOST = 6;
    /**
     * The constant FLOID_SERVICE_MESSAGE_GET_LOG.
     */
    public static final int FLOID_SERVICE_MESSAGE_GET_LOG = 7;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SET_LOG.
     */
    public static final int FLOID_SERVICE_MESSAGE_SET_LOG = 8;
    /**
     * The constant FLOID_SERVICE_MESSAGE_GET_MODEL_PARAMETERS.
     */
    public static final int FLOID_SERVICE_MESSAGE_GET_MODEL_PARAMETERS = 9;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SET_MODEL_PARAMETERS.
     */
    public static final int FLOID_SERVICE_MESSAGE_SET_MODEL_PARAMETERS = 10;
    /**
     * The constant FLOID_SERVICE_MESSAGE_GET_FLOID_STATUS.
     */
    public static final int FLOID_SERVICE_MESSAGE_GET_FLOID_STATUS = 11;
    /**
     * The constant FLOID_SERVICE_MESSAGE_GET_FLOID_DROID_STATUS.
     */
    public static final int FLOID_SERVICE_MESSAGE_GET_FLOID_DROID_STATUS = 12;
    /**
     * The constant FLOID_SERVICE_MESSAGE_PLAY_NOTIFICATION.
     */
    public static final int FLOID_SERVICE_MESSAGE_PLAY_NOTIFICATION = 13;
    /**
     * The constant FLOID_SERVICE_MESSAGE_UPDATE_THREAD_TIMINGS.
     */
    public static final int FLOID_SERVICE_MESSAGE_UPDATE_THREAD_TIMINGS = 14;
    /**
     * The constant FLOID_SERVICE_MESSAGE_START_VIDEO.
     */
    public static final int FLOID_SERVICE_MESSAGE_START_VIDEO = 15;
    /**
     * The constant FLOID_SERVICE_MESSAGE_STOP_VIDEO.
     */
    public static final int FLOID_SERVICE_MESSAGE_STOP_VIDEO = 16;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SEND_VIDEO_FRAME.
     */
    public static final int FLOID_SERVICE_MESSAGE_SEND_VIDEO_FRAME = 17;
    /**
     * The constant FLOID_SERVICE_MESSAGE_TAKE_PHOTO.
     */
    public static final int FLOID_SERVICE_MESSAGE_TAKE_PHOTO = 18;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SPEAK_COMMAND.
     */
    public static final int FLOID_SERVICE_MESSAGE_SPEAK_COMMAND = 19;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SPEAK_STRING.
     */
    public static final int FLOID_SERVICE_MESSAGE_SPEAK_STRING = 20;
    /**
     * The constant FLOID_SERVICE_MESSAGE_GET_CLIENT_COMMUNICATOR_STATUS.
     */
    public static final int FLOID_SERVICE_MESSAGE_GET_CLIENT_COMMUNICATOR_STATUS = 21;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SET_CLIENT_COMMUNICATOR_STATUS.
     */
    public static final int FLOID_SERVICE_MESSAGE_SET_CLIENT_COMMUNICATOR_STATUS = 22;
    /**
     * The constant FLOID_SERVICE_MESSAGE_START_MISSION_TIMER.
     */
    public static final int FLOID_SERVICE_MESSAGE_START_MISSION_TIMER = 23;
    /**
     * The constant FLOID_SERVICE_MESSAGE_CANCEL_MISSION_TIMER.
     */
    public static final int FLOID_SERVICE_MESSAGE_CANCEL_MISSION_TIMER = 24;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SET_MODE.
     */
    public static final int FLOID_SERVICE_MESSAGE_SET_MODE = 25;
    /**
     * The constant FLOID_SERVICE_MESSAGE_SET_MODE.
     */
    public static final int FLOID_SERVICE_MESSAGE_OPEN_USB_ACCESSORY = 26;
    /**
     * The constant FLOID_SERVICE_MISSION_MODE.
     */
    public static final int FLOID_SERVICE_MISSION_MODE = 27;
    /**
     * The constant FLOID_SERVICE_SET_DROID_PARAMETERS.
     */
    public static final int FLOID_SERVICE_MESSAGE_SET_DROID_PARAMETERS = 28;
    /**
     * The constant FLOID_SERVICE_MESSAGE_UPLOAD_FIRMWARE.
     */
    public static final int FLOID_SERVICE_MESSAGE_UPLOAD_FIRMWARE = 29;
    // Battery status constants:
    /**
     * The constant DROID_BATTERY_CHECK_FIRST_WAIT.
     */
    public static final long DROID_BATTERY_CHECK_FIRST_WAIT = 1000L;
    /**
     * The constant DROID_BATTERY_CHECK_PERIOD.
     */
    public static final long DROID_BATTERY_CHECK_PERIOD = 10000L;
    /**
     * The constant NEW_MISSION_DEFAULT_COUNTDOWN_TIME.
     */
    public static final int NEW_MISSION_DEFAULT_COUNTDOWN_TIME = 10;
    /**
     * The droid new mission default countdown time.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final int mDroidNewMissionDefaultCountDownTime = NEW_MISSION_DEFAULT_COUNTDOWN_TIME;  // NOTE: [SetParameter] Candidate for setParameter
    // Command response timeout - timeout for arduino to respond that it received a command:  // Currently unused
    /**
     * The constant COMMAND_RESPONSE_TIMEOUT.
     */
    @SuppressWarnings("unused")
    private static final long COMMAND_RESPONSE_TIMEOUT = 2000L;
    // Some default ids:
    /**
     * The constant NO_MISSION_ID.
     */
    public final static long NO_MISSION_ID = -1L;
    /**
     * The constant NO_INSTRUCTION_ID.
     */
    public final static long NO_INSTRUCTION_ID = -1L;
    /**
     * The constant NO_INSTRUCTION_STATUS_ID.
     */
    public final static int NO_INSTRUCTION_STATUS_ID = -1;
    /**
     * The constant NO_GOAL_ID.
     */
    public final static int NO_GOAL_ID = -1;
    /**
     * The constant COMMAND_NO_TIMEOUT.
     */
    public final static long COMMAND_NO_TIMEOUT = -1L;
    // OUTGOING MESSAGES (To Arduino):
    // ===============================
    // Packet types and command number offsets:
    /**
     * The constant ACC_PACKET_TYPE_OFFSET.
     */
    public final static int ACC_PACKET_TYPE_OFFSET = 0x00;
    /**
     * The constant ACC_COMMAND_NUMBER_OFFSET.
     */
    public final static int ACC_COMMAND_NUMBER_OFFSET = 0x01;
    /**
     * The constant ACC_GOAL_ID_OFFSET.
     */
    public final static int ACC_GOAL_ID_OFFSET = 0x05;
    // Packet types:
    /**
     * The constant HELI_CONTROL_PACKET.
     */
    public final static int HELI_CONTROL_PACKET = 0x01;
    /**
     * The constant HELI_CONTROL_PACKET_SIZE.
     */
    public final static int HELI_CONTROL_PACKET_SIZE = 0x0F;
    /**
     * The constant PAN_TILT_PACKET.
     */
    public final static int PAN_TILT_PACKET = 0x02;
    /**
     * The constant PAN_TILT_PACKET_SIZE.
     */
    public final static int PAN_TILT_PACKET_SIZE = 0x17;
    /**
     * The constant RELAY_PACKET.
     */
    public final static int RELAY_PACKET = 0x03;
    /**
     * The constant RELAY_PACKET_SIZE.
     */
    public final static int RELAY_PACKET_SIZE = 0x0B;
    /**
     * The constant DECLINATION_PACKET.
     */
    public final static int DECLINATION_PACKET = 0x04;
    /**
     * The constant DECLINATION_PACKET_SIZE.
     */
    public final static int DECLINATION_PACKET_SIZE = 0x0D;
    /**
     * The constant SET_PARAMETERS_PACKET.
     */
    @SuppressWarnings("unused")
    public final static int SET_PARAMETERS_PACKET = 0x05;
    /**
     * The constant SET_PARAMETERS_PACKET_SIZE.
     */
    @SuppressWarnings("unused")
    public final static int SET_PARAMETERS_PACKET_SIZE = 0x09;
    /**
     * The constant HELIS_ON_PACKET.
     */
    public final static int HELIS_ON_PACKET = 0x06;
    /**
     * The constant HELIS_ON_PACKET_SIZE.
     */
    public final static int HELIS_ON_PACKET_SIZE = 0x09;
    /**
     * The constant HELIS_OFF_PACKET.
     */
    public final static int HELIS_OFF_PACKET = 0x07;
    /**
     * The constant HELIS_OFF_PACKET_SIZE.
     */
    public final static int HELIS_OFF_PACKET_SIZE = 0x09;
    /**
     * The constant DESIGNATE_TARGET_PACKET.
     */
    public final static int DESIGNATE_TARGET_PACKET = 0x0E;
    /**
     * The constant DESIGNATE_TARGET_PACKET_SIZE.
     */
    public final static int DESIGNATE_TARGET_PACKET_SIZE = 0x15;
    /**
     * The constant STOP_DESIGNATING_PACKET.
     */
    public final static int STOP_DESIGNATING_PACKET = 0x0F;
    /**
     * The constant STOP_DESIGNATING_PACKET_SIZE.
     */
    public final static int STOP_DESIGNATING_PACKET_SIZE = 0x05;
    /**
     * The constant SET_POSITION_GOAL_PACKET.
     */
    public final static int SET_POSITION_GOAL_PACKET = 0x10;
    /**
     * The constant SET_POSITION_GOAL_PACKET_SIZE.
     */
    public final static int SET_POSITION_GOAL_PACKET_SIZE = 0x11;
    /**
     * The constant SET_ALTITUDE_GOAL_PACKET.
     */
    public final static int SET_ALTITUDE_GOAL_PACKET = 0x24;
    /**
     * The constant SET_ALTITUDE_GOAL_PACKET_SIZE.
     */
    public final static int SET_ALTITUDE_GOAL_PACKET_SIZE = 0x0E;
    /**
     * The constant SET_ROTATION_GOAL_PACKET.
     */
    public final static int SET_ROTATION_GOAL_PACKET = 0x11;
    /**
     * The constant SET_ROTATION_GOAL_PACKET_SIZE.
     */
    public final static int SET_ROTATION_GOAL_PACKET_SIZE = 0x0E;
    /**
     * The constant DEPLOY_PARACHUTE_PACKET.
     */
    @SuppressWarnings("unused")
    public final static int DEPLOY_PARACHUTE_PACKET = 0x12;
    /**
     * The constant DEPLOY_PARACHUTE_PACKET_SIZE.
     */
    @SuppressWarnings("unused")
    public final static int DEPLOY_PARACHUTE_PACKET_SIZE = 0x05;
    /**
     * The constant LIFT_OFF_PACKET.
     */
    public final static int LIFT_OFF_PACKET = 0x15;
    /**
     * The constant LIFT_OFF_PACKET_SIZE.
     */
    public final static int LIFT_OFF_PACKET_SIZE = 0x19;
    /**
     * The constant LAND_PACKET.
     */
    public final static int LAND_PACKET = 0x16;
    /**
     * The constant LAND_PACKET_SIZE.
     */
    public final static int LAND_PACKET_SIZE = 0x0D;
    /**
     * The constant PAYLOAD_PACKET.
     */
    public final static int PAYLOAD_PACKET = 0x1A;
    /**
     * The constant PAYLOAD_PACKET_SIZE.
     */
    public final static int PAYLOAD_PACKET_SIZE = 0x0A;
    /**
     * The constant DEBUG_CONTROL_PACKET.
     */
    public final static int DEBUG_CONTROL_PACKET = 0x1B;
    /**
     * The constant DEBUG_CONTROL_PACKET_SIZE.
     */
    public final static int DEBUG_CONTROL_PACKET_SIZE = 0x0B;
    /**
     * The constant FLOID_MODE_PACKET.
     */
    public final static int FLOID_MODE_PACKET = 0x1C;
    /**
     * The constant FLOID_MODE_PACKET_SIZE.
     */
    public final static int FLOID_MODE_PACKET_SIZE = 0x0A;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET.
     */
    public final static int MODEL_PARAMETERS_1_PACKET = 0x1D;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_SIZE.
     */
    public final static int MODEL_PARAMETERS_1_PACKET_SIZE = 0x71;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET.
     */
    public final static int MODEL_PARAMETERS_2_PACKET = 0x1E;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_SIZE.
     */
    public final static int MODEL_PARAMETERS_2_PACKET_SIZE = 0x75;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET.
     */
    public final static int MODEL_PARAMETERS_3_PACKET = 0x1F;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_SIZE.
     */
    public final static int MODEL_PARAMETERS_3_PACKET_SIZE = 0x74;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET.
     */
    public final static int MODEL_PARAMETERS_4_PACKET = 0x20;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_SIZE.
     */
    public final static int MODEL_PARAMETERS_4_PACKET_SIZE = 0x79;
    /**
     * The constant GET_MODEL_PARAMETERS_PACKET.
     */
    public final static int GET_MODEL_PARAMETERS_PACKET = 0x22;
    /**
     * The constant GET_MODEL_PARAMETERS_PACKET_SIZE.
     */
    public final static int GET_MODEL_PARAMETERS_PACKET_SIZE = 0x09;
    /**
     * The constant TEST_GOALS_PACKET.
     */
    public final static int TEST_GOALS_PACKET = 0x23;
    /**
     * The constant HEARTBEAT_PACKET.
     */
    public final static int HEARTBEAT_PACKET = 0x25;
    /**
     * The constant HEARTBEAT_PACKET_SIZE.
     */
    public final static int HEARTBEAT_PACKET_SIZE = 0x09;
    /**
     * The constant DEVICE_GPS_PACKET.
     */
    public final static int DEVICE_GPS_PACKET = 0x26;
    /**
     * The constant DEVICE_PYR_PACKET.
     */
    public final static int DEVICE_PYR_PACKET = 0x27;
    // Test Packet:
    /**
     * The constant TEST_PACKET.
     */
    public final static int TEST_PACKET = 0x7F;
    /**
     * The constant TEST_PACKET_SIZE.
     */
    public final static int TEST_PACKET_SIZE = 0x09;
    // Heli Control:
    /**
     * The constant HELI_CONTROL_PACKET_HELI_OFFSET.
     */
    public final static int HELI_CONTROL_PACKET_HELI_OFFSET = 0x09;
    /**
     * The constant HELI_CONTROL_PACKET_SERVO_OFFSET.
     */
    public final static int HELI_CONTROL_PACKET_SERVO_OFFSET = 0x0A;
    /**
     * The constant HELI_CONTROL_PACKET_VALUE_OFFSET.
     */
    public final static int HELI_CONTROL_PACKET_VALUE_OFFSET = 0x0B;
    /**
     * The constant HELI_CONTROL_LEFT_SERVO.
     */
    public final static int HELI_CONTROL_LEFT_SERVO = 0x00;
    /**
     * The constant HELI_CONTROL_RIGHT_SERVO.
     */
    public final static int HELI_CONTROL_RIGHT_SERVO = 0x01;
    /**
     * The constant HELI_CONTROL_PITCH_SERVO.
     */
    public final static int HELI_CONTROL_PITCH_SERVO = 0x02;
    /**
     * The constant HELI_CONTROL_ESC_SERVO.
     */
    public final static int HELI_CONTROL_ESC_SERVO = 0x03;
    /**
     * The constant HELI_CONTROL_COLLECTIVE_SERVO.
     */
    public final static int HELI_CONTROL_COLLECTIVE_SERVO = 0x04;
    /**
     * The constant HELI_CONTROL_CYCLIC_LEFT_SERVO.
     */
    @SuppressWarnings("unused")
    public final static int HELI_CONTROL_CYCLIC_LEFT_SERVO = 0x05;
    /**
     * The constant HELI_CONTROL_CYCLIC_RIGHT_SERVO.
     */
    @SuppressWarnings("unused")
    public final static int HELI_CONTROL_CYCLIC_RIGHT_SERVO = 0x06;
    /**
     * The constant HELI_CONTROL_CYCLIC_PITCH_SERVO.
     */
    @SuppressWarnings("unused")
    public final static int HELI_CONTROL_CYCLIC_PITCH_SERVO = 0x07;
    // Pan-Tilt:
    /**
     * The constant PAN_TILT_PACKET_DEVICE_OFFSET.
     */
    public final static int PAN_TILT_PACKET_DEVICE_OFFSET = 0x09;
    /**
     * The constant PAN_TILT_PACKET_TYPE_OFFSET.
     */
    public final static int PAN_TILT_PACKET_TYPE_OFFSET = 0x0A;
    /**
     * The constant PAN_TILT_PACKET_TYPE_ANGLE.
     */
    public final static int PAN_TILT_PACKET_TYPE_ANGLE = 0x00;
    /**
     * The constant PAN_TILT_PACKET_ANGLE_PAN_OFFSET.
     */
    public final static int PAN_TILT_PACKET_ANGLE_PAN_OFFSET = 0x0B;
    /**
     * The constant PAN_TILT_PACKET_ANGLE_TILT_OFFSET.
     */
    public final static int PAN_TILT_PACKET_ANGLE_TILT_OFFSET = 0x0F;
    /**
     * The constant PAN_TILT_PACKET_TYPE_TARGET.
     */
    public final static int PAN_TILT_PACKET_TYPE_TARGET = 0x01;
    /**
     * The constant PAN_TILT_PACKET_TARGET_X_OFFSET.
     */
    public final static int PAN_TILT_PACKET_TARGET_X_OFFSET = 0x0B;
    /**
     * The constant PAN_TILT_PACKET_TARGET_Y_OFFSET.
     */
    public final static int PAN_TILT_PACKET_TARGET_Y_OFFSET = 0x0F;
    /**
     * The constant PAN_TILT_PACKET_TARGET_Z_OFFSET.
     */
    public final static int PAN_TILT_PACKET_TARGET_Z_OFFSET = 0x13;
    /**
     * The constant PAN_TILT_PACKET_TYPE_INDIVIDUAL.
     */
    public final static int PAN_TILT_PACKET_TYPE_INDIVIDUAL = 0x02;
    /**
     * The constant PAN_TILT_PACKET_INDIVIDUAL_PT_OFFSET.
     */
    public final static int PAN_TILT_PACKET_INDIVIDUAL_PT_OFFSET = 0x0B;
    /**
     * The constant PAN_TILT_PACKET_INDIVIDUAL_VALUE_OFFSET.
     */
    public final static int PAN_TILT_PACKET_INDIVIDUAL_VALUE_OFFSET = 0x0C;
    /**
     * The constant PAN_TILT_PAN_SUBCOMMAND.
     */
    public final static int PAN_TILT_PAN_SUBCOMMAND = 0x00;
    /**
     * The constant PAN_TILT_TILT_SUBCOMMAND.
     */
    public final static int PAN_TILT_TILT_SUBCOMMAND = 0x01;
    // Relay:
    /**
     * The constant RELAY_PACKET_DEVICE_OFFSET.
     */
    public final static int RELAY_PACKET_DEVICE_OFFSET = 0x09;
    /**
     * The constant RELAY_PACKET_STATE_OFFSET.
     */
    public final static int RELAY_PACKET_STATE_OFFSET = 0x0A;
    /**
     * The constant RELAY_DEVICE_STATE_OFF.
     */
    public final static int RELAY_DEVICE_STATE_OFF = 0x00;
    /**
     * The constant RELAY_DEVICE_STATE_ON.
     */
    public final static int RELAY_DEVICE_STATE_ON = 0x01;
    /**
     * The constant RELAY_DEVICE_ESC0.
     */
    public final static int RELAY_DEVICE_ESC0 = 0x00;
    /**
     * The constant RELAY_DEVICE_ESC1.
     */
    public final static int RELAY_DEVICE_ESC1 = 0x01;
    /**
     * The constant RELAY_DEVICE_ESC2.
     */
    public final static int RELAY_DEVICE_ESC2 = 0x02;
    /**
     * The constant RELAY_DEVICE_ESC3.
     */
    public final static int RELAY_DEVICE_ESC3 = 0x03;
    /**
     * The constant RELAY_DEVICE_NM0.
     */
    public final static int RELAY_DEVICE_NM0 = 0x04;
    /**
     * The constant RELAY_DEVICE_NM1.
     */
    public final static int RELAY_DEVICE_NM1 = 0x05;
    /**
     * The constant RELAY_DEVICE_NM2.
     */
    public final static int RELAY_DEVICE_NM2 = 0x06;
    /**
     * The constant RELAY_DEVICE_NM3.
     */
    public final static int RELAY_DEVICE_NM3 = 0x07;
    /**
     * The constant RELAY_DEVICE_GPS.
     */
    public final static int RELAY_DEVICE_GPS = 0x08;
    /**
     * The constant RELAY_DEVICE_PYR.
     */
    public final static int RELAY_DEVICE_PYR = 0x09;
    /**
     * The constant RELAY_DEVICE_DESIGNATOR.
     */
    public final static int RELAY_DEVICE_DESIGNATOR = 0x0A;
    /**
     * The constant RELAY_DEVICE_AUX.
     */
    public final static int RELAY_DEVICE_AUX = 0x0B;
    /**
     * The constant RELAY_DEVICE_PARACHUTE.
     */
    public final static int RELAY_DEVICE_PARACHUTE = 0x0C;
    /**
     * The constant RELAY_DEVICE_PT0.
     */
    public final static int RELAY_DEVICE_PT0 = 0x0D;
    /**
     * The constant RELAY_DEVICE_PT1.
     */
    public final static int RELAY_DEVICE_PT1 = 0x0E;
    /**
     * The constant RELAY_DEVICE_PT2.
     */
    public final static int RELAY_DEVICE_PT2 = 0x0F;
    /**
     * The constant RELAY_DEVICE_PT3.
     */
    public final static int RELAY_DEVICE_PT3 = 0x10;
    /**
     * The constant RELAY_DEVICE_LED.
     */
    public final static int RELAY_DEVICE_LED = 0x11;
    /**
     * The constant RELAY_DEVICE_S0.
     */
    public final static int RELAY_DEVICE_S0 = 0x13;
    /**
     * The constant RELAY_DEVICE_S1.
     */
    public final static int RELAY_DEVICE_S1 = 0x14;
    /**
     * The constant RELAY_DEVICE_S2.
     */
    public final static int RELAY_DEVICE_S2 = 0x15;
    /**
     * The constant RELAY_DEVICE_S3.
     */
    public final static int RELAY_DEVICE_S3 = 0x16;
    /**
     * The constant RELAY_DEVICE_ESC0_EXTRA.
     */
    public final static int RELAY_DEVICE_ESC0_EXTRA = 0x17;
    /**
     * The constant RELAY_DEVICE_ESC1_EXTRA.
     */
    public final static int RELAY_DEVICE_ESC1_EXTRA = 0x18;
    /**
     * The constant RELAY_DEVICE_ESC2_EXTRA.
     */
    public final static int RELAY_DEVICE_ESC2_EXTRA = 0x19;
    /**
     * The constant RELAY_DEVICE_ESC3_EXTRA.
     */
    public final static int RELAY_DEVICE_ESC3_EXTRA = 0x1A;
    /**
     * The constant RELAY_DEVICE_PYR_AD_SWITCH.
     */
    public final static int RELAY_DEVICE_PYR_AD_SWITCH = 0x1B;
    /**
     * The constant RELAY_DEVICE_PYR_RATE_0_SWITCH.
     */
    public final static int RELAY_DEVICE_PYR_RATE_0_SWITCH = 0x1C;
    /**
     * The constant RELAY_DEVICE_PYR_RATE_1_SWITCH.
     */
    public final static int RELAY_DEVICE_PYR_RATE_1_SWITCH = 0x1D;
    /**
     * The constant RELAY_DEVICE_PYR_RATE_2_SWITCH.
     */
    public final static int RELAY_DEVICE_PYR_RATE_2_SWITCH = 0x1E;
    /**
     * The constant RELAY_DEVICE_PYR_RATE_3_SWITCH.
     */
    public final static int RELAY_DEVICE_PYR_RATE_3_SWITCH = 0x1F;
    /**
     * The constant RELAY_DEVICE_PYR_RATE_4_SWITCH.
     */
    public final static int RELAY_DEVICE_PYR_RATE_4_SWITCH = 0x20;
    /**
     * The constant RELAY_DEVICE_PYR_RATE_5_SWITCH.
     */
    public final static int RELAY_DEVICE_PYR_RATE_5_SWITCH = 0x21;
    /**
     * The constant RELAY_DEVICE_TEST_LIFT_OFF_MODE_SWITCH.
     */
    public final static int RELAY_DEVICE_TEST_LIFT_OFF_MODE_SWITCH = 0x22;
    /**
     * The constant RELAY_DEVICE_TEST_NORMAL_MODE_SWITCH.
     */
    public final static int RELAY_DEVICE_TEST_NORMAL_MODE_SWITCH = 0x23;
    /**
     * The constant RELAY_DEVICE_TEST_LAND_MODE_SWITCH.
     */
    public final static int RELAY_DEVICE_TEST_LAND_MODE_SWITCH = 0x24;
    /**
     * The constant RELAY_DEVICE_TEST_GOAL_ON_SWITCH.
     */
    @SuppressWarnings("unused")
    public final static int RELAY_DEVICE_TEST_GOAL_ON_SWITCH = 0x25;
    /**
     * The constant RELAY_DEVICE_TEST_GOAL_OFF_SWITCH.
     */
    @SuppressWarnings("unused")
    public final static int RELAY_DEVICE_TEST_GOAL_OFF_SWITCH = 0x26;
    /**
     * The constant RELAY_DEVICE_TEST_BLADES_LOW_SWITCH.
     */
    public final static int RELAY_DEVICE_TEST_BLADES_LOW_SWITCH = 0x27;
    /**
     * The constant RELAY_DEVICE_TEST_BLADES_ZERO_SWITCH.
     */
    public final static int RELAY_DEVICE_TEST_BLADES_ZERO_SWITCH = 0x28;
    /**
     * The constant RELAY_DEVICE_TEST_BLADES_HIGH_SWITCH.
     */
    public final static int RELAY_DEVICE_TEST_BLADES_HIGH_SWITCH = 0x29;
    /**
     * The constant RELAY_DEVICE_LOAD_FROM_EEPROM.
     */
    public final static int RELAY_DEVICE_LOAD_FROM_EEPROM = 0x2A;
    /**
     * The constant RELAY_DEVICE_SAVE_TO_EEPROM.
     */
    public final static int RELAY_DEVICE_SAVE_TO_EEPROM = 0x2B;
    /**
     * The constant RELAY_DEVICE_CLEAR_EEPROM.
     */
    public final static int RELAY_DEVICE_CLEAR_EEPROM = 0x2C;
    /**
     * The constant RELAY_DEVICE_SERIAL_OUTPUT.
     */
    public final static int RELAY_DEVICE_SERIAL_OUTPUT = 0x2D;
    /**
     * The constant RELAY_DEVICE_FLOID_STATUS_RATE_0_SWITCH.
     */
    public final static int RELAY_DEVICE_FLOID_STATUS_RATE_0_SWITCH = 0x2E;
    /**
     * The constant RELAY_DEVICE_FLOID_STATUS_RATE_1_SWITCH.
     */
    public final static int RELAY_DEVICE_FLOID_STATUS_RATE_1_SWITCH = 0x2F;
    /**
     * The constant RELAY_DEVICE_FLOID_STATUS_RATE_2_SWITCH.
     */
    public final static int RELAY_DEVICE_FLOID_STATUS_RATE_2_SWITCH = 0x30;
    /**
     * The constant RELAY_DEVICE_FLOID_STATUS_RATE_3_SWITCH.
     */
    public final static int RELAY_DEVICE_FLOID_STATUS_RATE_3_SWITCH = 0x31;
    /**
     * The constant RELAY_DEVICE_FLOID_STATUS_RATE_4_SWITCH.
     */
    public final static int RELAY_DEVICE_FLOID_STATUS_RATE_4_SWITCH = 0x32;
    /**
     * The constant RELAY_DEVICE_FLOID_STATUS_RATE_5_SWITCH.
     */
    public final static int RELAY_DEVICE_FLOID_STATUS_RATE_5_SWITCH = 0x33;
    /**
     * The constant RELAY_DEVICE_FLOID_STATUS_RATE_6_SWITCH.
     */
    public final static int RELAY_DEVICE_FLOID_STATUS_RATE_6_SWITCH = 0x34;
    /**
     * The constant RELAY_DEVICE_FLOID_STATUS_RATE_7_SWITCH.
     */
    public final static int RELAY_DEVICE_FLOID_STATUS_RATE_7_SWITCH = 0x35;
    /**
     * The constant RELAY_DEVICE_GPS_RATE_0_SWITCH.
     */
    public final static int RELAY_DEVICE_GPS_RATE_0_SWITCH = 0x36;
    /**
     * The constant RELAY_DEVICE_GPS_RATE_1_SWITCH.
     */
    public final static int RELAY_DEVICE_GPS_RATE_1_SWITCH = 0x37;
    /**
     * The constant RELAY_DEVICE_GPS_RATE_2_SWITCH.
     */
    public final static int RELAY_DEVICE_GPS_RATE_2_SWITCH = 0x38;
    /**
     * The constant RELAY_DEVICE_GPS_RATE_3_SWITCH.
     */
    public final static int RELAY_DEVICE_GPS_RATE_3_SWITCH = 0x39;
    /**
     * The constant RELAY_DEVICE_GPS_RATE_4_SWITCH.
     */
    public final static int RELAY_DEVICE_GPS_RATE_4_SWITCH = 0x3A;
    /**
     * The constant RELAY_DEVICE_ESC0_SETUP.
     */
    public final static int RELAY_DEVICE_ESC0_SETUP = 0x3B;
    /**
     * The constant RELAY_DEVICE_ESC1_SETUP.
     */
    public final static int RELAY_DEVICE_ESC1_SETUP = 0x3C;
    /**
     * The constant RELAY_DEVICE_ESC2_SETUP.
     */
    public final static int RELAY_DEVICE_ESC2_SETUP = 0x3D;
    /**
     * The constant RELAY_DEVICE_ESC3_SETUP.
     */
    public final static int RELAY_DEVICE_ESC3_SETUP = 0x3E;
    /**
     * The constant RELAY_DEVICE_GPS_EMULATE.
     */
    public final static int RELAY_DEVICE_GPS_EMULATE = 0x3F;
    /**
     * The constant RELAY_DEVICE_GPS_DEVICE.
     */
    public final static int RELAY_DEVICE_GPS_DEVICE = 0x40;
    /**
     * The constant RELAY_DEVICE_PYR_DEVICE.
     */
    public final static int RELAY_DEVICE_PYR_DEVICE = 0x41;
    // Run/Test Modes:
    /**
     * The constant RELAY_DEVICE_RUN_MODE_PRODUCTION.
     */
    public final static int RELAY_DEVICE_RUN_MODE_PRODUCTION = 0x42;
    /**
     * The constant RELAY_DEVICE_TEST_MODE_PRINT_DEBUG_HEADINGS.
     */
    public final static int RELAY_DEVICE_TEST_MODE_PRINT_DEBUG_HEADINGS = 0x43;
    /**
     * The constant RELAY_DEVICE_RUN_MODE_TEST_XY.
     */
    public final static int RELAY_DEVICE_RUN_MODE_TEST_XY = 0x44;
    /**
     * The constant RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_1.
     */
    public final static int RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_1 = 0x45;
    /**
     * The constant RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_2.
     */
    public final static int RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_2 = 0x46;
    /**
     * The constant RELAY_DEVICE_RUN_MODE_TEST_HEADING_1.
     */
    public final static int RELAY_DEVICE_RUN_MODE_TEST_HEADING_1 = 0x47;
    /**
     * The constant RELAY_DEVICE_RUN_MODE_TEST_PITCH_1.
     */
    public final static int RELAY_DEVICE_RUN_MODE_TEST_PITCH_1 = 0x48;
    /**
     * The constant RELAY_DEVICE_RUN_MODE_TEST_ROLL_1.
     */
    public final static int RELAY_DEVICE_RUN_MODE_TEST_ROLL_1 = 0x49;
    /**
     * The constant RELAY_DEVICE_TEST_MODE_NO_XY.
     */
    public final static int RELAY_DEVICE_TEST_MODE_NO_XY = 0x4A;
    /**
     * The constant RELAY_DEVICE_TEST_MODE_NO_ALTITUDE.
     */
    public final static int RELAY_DEVICE_TEST_MODE_NO_ALTITUDE = 0x4B;
    /**
     * The constant RELAY_DEVICE_TEST_MODE_NO_ATTACK_ANGLE.
     */
    public final static int RELAY_DEVICE_TEST_MODE_NO_ATTACK_ANGLE = 0x4C;
    /**
     * The constant RELAY_DEVICE_TEST_MODE_NO_HEADING.
     */
    public final static int RELAY_DEVICE_TEST_MODE_NO_HEADING = 0x4D;
    /**
     * The constant RELAY_DEVICE_TEST_MODE_NO_PITCH.
     */
    public final static int RELAY_DEVICE_TEST_MODE_NO_PITCH = 0x4E;
    /**
     * The constant RELAY_DEVICE_TEST_MODE_NO_ROLL.
     */
    public final static int RELAY_DEVICE_TEST_MODE_NO_ROLL = 0x4F;
    /**
     * The constant RELAY_DEVICE_TEST_MODE_CHECK_PHYSICS_MODEL.
     */
    public final static int RELAY_DEVICE_TEST_MODE_CHECK_PHYSICS_MODEL = 0x50;
    /**
     * The constant RELAY_DEVICE_GOALS.
     */
    public final static int RELAY_DEVICE_GOALS = 0x51;
    /**
     * The constant RELAY_DEVICE_IMU_CALIBRATE.
     */
    public final static int RELAY_DEVICE_IMU_CALIBRATE = 0x52;
    /**
     * The constant RELAY_DEVICE_IMU_SAVE.
     */
    public final static int RELAY_DEVICE_IMU_SAVE = 0x53;
    // Declination:
    /**
     * The constant DECLINATION_PACKET_DECLINATION_OFFSET.
     */
    public final static int DECLINATION_PACKET_DECLINATION_OFFSET = 0x09;
    // Position:
    /**
     * The constant SET_POSITION_GOAL_PACKET_X_OFFSET.
     */
    public final static int SET_POSITION_GOAL_PACKET_X_OFFSET = 0x09;
    /**
     * The constant SET_POSITION_GOAL_PACKET_Y_OFFSET.
     */
    public final static int SET_POSITION_GOAL_PACKET_Y_OFFSET = 0x0D;
    // Altitude:
    /**
     * The constant SET_ALTITUDE_GOAL_PACKET_Z_OFFSET.
     */
    public final static int SET_ALTITUDE_GOAL_PACKET_Z_OFFSET = 0x09;
    /**
     * The constant SET_ALTITUDE_GOAL_PACKET_ABSOLUTE_HEIGHT_OFFSET.
     */
    @SuppressWarnings("unused")
    public final static int SET_ALTITUDE_GOAL_PACKET_ABSOLUTE_HEIGHT_OFFSET = 0x0D;
    // Rotation:
    /**
     * The constant SET_ROTATION_GOAL_PACKET_FOLLOW_MODE_OFFSET.
     */
    public final static int SET_ROTATION_GOAL_PACKET_FOLLOW_MODE_OFFSET = 0x09;
    /**
     * The constant SET_ROTATION_GOAL_PACKET_A_OFFSET.
     */
    public final static int SET_ROTATION_GOAL_PACKET_A_OFFSET = 0x0A;
    // Lift-Off:
    /**
     * The constant LIFT_OFF_PACKET_X_OFFSET.
     */
    public final static int LIFT_OFF_PACKET_X_OFFSET = 0x09;
    /**
     * The constant LIFT_OFF_PACKET_Y_OFFSET.
     */
    public final static int LIFT_OFF_PACKET_Y_OFFSET = 0x0D;
    /**
     * The constant LIFT_OFF_PACKET_Z_OFFSET.
     */
    public final static int LIFT_OFF_PACKET_Z_OFFSET = 0x11;
    /**
     * The constant LIFT_OFF_PACKET_A_OFFSET.
     */
    public final static int LIFT_OFF_PACKET_A_OFFSET = 0x15;
    // Land:
    /**
     * The constant LAND_PACKET_Z_OFFSET.
     */
    public final static int LAND_PACKET_Z_OFFSET = 0x09;
    // Designate:
    /**
     * The constant DESIGNATE_TARGET_PACKET_X_OFFSET.
     */
    public final static int DESIGNATE_TARGET_PACKET_X_OFFSET = 0x09;
    /**
     * The constant DESIGNATE_TARGET_PACKET_Y_OFFSET.
     */
    public final static int DESIGNATE_TARGET_PACKET_Y_OFFSET = 0x0D;
    /**
     * The constant DESIGNATE_TARGET_PACKET_Z_OFFSET.
     */
    public final static int DESIGNATE_TARGET_PACKET_Z_OFFSET = 0x11;
    // Payload:
    /**
     * The constant PAYLOAD_PACKET_BAY_OFFSET.
     */
    public final static int PAYLOAD_PACKET_BAY_OFFSET = 0x09;
    // Debug Control:
    /**
     * The constant DEBUG_PACKET_INDEX_OFFSET.
     */
    public final static int DEBUG_PACKET_INDEX_OFFSET = 0x09;
    /**
     * The constant DEBUG_PACKET_VALUE_OFFSET.
     */
    public final static int DEBUG_PACKET_VALUE_OFFSET = 0x0A;
    /**
     * The constant DEBUG_PACKET_DEBUG.
     */
    public final static int DEBUG_PACKET_DEBUG = 0x00;
    /**
     * The constant DEBUG_PACKET_MEMORY.
     */
    public final static int DEBUG_PACKET_MEMORY = 0x01;
    /**
     * The constant DEBUG_PACKET_GPS.
     */
    public final static int DEBUG_PACKET_GPS = 0x02;
    /**
     * The constant DEBUG_PACKET_PYR.
     */
    public final static int DEBUG_PACKET_PYR = 0x03;
    /**
     * The constant DEBUG_PACKET_PHYSICS.
     */
    public final static int DEBUG_PACKET_PHYSICS = 0x04;
    /**
     * The constant DEBUG_PACKET_PAN_TILT.
     */
    public final static int DEBUG_PACKET_PAN_TILT = 0x05;
    /**
     * The constant DEBUG_PACKET_PAYLOADS.
     */
    public final static int DEBUG_PACKET_PAYLOADS = 0x06;
    /**
     * The constant DEBUG_PACKET_CAMERA.
     */
    public final static int DEBUG_PACKET_CAMERA = 0x07;
    /**
     * The constant DEBUG_PACKET_DESIGNATOR.
     */
    public final static int DEBUG_PACKET_DESIGNATOR = 0x08;
    /**
     * The constant DEBUG_PACKET_AUX.
     */
    public final static int DEBUG_PACKET_AUX = 0x09;
    /**
     * The constant DEBUG_PACKET_HELIS.
     */
    public final static int DEBUG_PACKET_HELIS = 0x0A;
    /**
     * The constant DEBUG_PACKET_ALL_DEVICES.
     */
    public final static int DEBUG_PACKET_ALL_DEVICES = 0x0B;
    /**
     * The constant DEBUG_PACKET_ALTIMETER.
     */
    public final static int DEBUG_PACKET_ALTIMETER = 0x0C;
    // Mode:
    /**
     * The constant FLOID_MODE_PACKET_MODE_OFFSET.
     */
    public final static int FLOID_MODE_PACKET_MODE_OFFSET = 0x09;
    // INCOMING MESSAGES:
    // ==================
    // NOTE - SIZE HINTS BELOW ARE ARDUINO SIZES (Short = Arduino int/short = Java short) (Long = Arduino long = Java Int)
    /**
     * The constant PACKET_HEADER_FLAG_OFFSET.
     */
    public final static int PACKET_HEADER_FLAG_OFFSET = 0;    // !FLOID
    /**
     * The constant PACKET_HEADER_LENGTH_OFFSET.
     */
    public final static int PACKET_HEADER_LENGTH_OFFSET = 6;    // 2-bytes
    /**
     * The constant PACKET_HEADER_CHECKSUM_OFFSET.
     */
    public final static int PACKET_HEADER_CHECKSUM_OFFSET = 8;    // Four bytes = { 0, 0, 0, 0} XORed with bytes at checksum_calc_start and beyond
    /**
     * The constant PACKET_HEADER_NUMBER_OFFSET.
     */
    public final static int PACKET_HEADER_NUMBER_OFFSET = 12;   // Arduino long = java Int
    /**
     * The constant PACKET_HEADER_TYPE_OFFSET.
     */
    public final static int PACKET_HEADER_TYPE_OFFSET = 16;   // One byte
    /**
     * The constant PACKET_HEADER_MINIMUM_SIZE.
     */
    public final static int PACKET_HEADER_MINIMUM_SIZE = 17;   // Defines the minimum size packet...
    /**
     * The constant PACKET_HEADER_CHECKSUM_CALC_START.
     */
    public final static int PACKET_HEADER_CHECKSUM_CALC_START = 17;   // Defines a position not a data length
    // Input Message types:
    /**
     * The constant STATUS_PACKET.
     */
    public final static int STATUS_PACKET = 0x00;
    /**
     * The constant COMMAND_RESPONSE_PACKET.
     */
    public final static int COMMAND_RESPONSE_PACKET = 0x01;
    /**
     * The constant DEBUG_PACKET.
     */
    public final static int DEBUG_PACKET = 0x02;
    /**
     * The constant TEST_RESPONSE_PACKET.
     */
    public final static int TEST_RESPONSE_PACKET = 0x03;
    /**
     * The constant MODEL_PARAMETERS_PACKET.
     */
    public final static int MODEL_PARAMETERS_PACKET = 0x04;
    /**
     * The constant MODEL_STATUS_PACKET.
     */
    public final static int MODEL_STATUS_PACKET = 0x05;
    // STATUS PACKET:
    // ==============
    /**
     * The constant STATUS_PACKET_SIZE.
     */
    public final static int STATUS_PACKET_SIZE = 568;
    // Status Time and Number:
    // Longs:
    /**
     * The constant STATUS_PACKET_OFFSET_STATUS_NUMBER.
     */
    public final static int STATUS_PACKET_OFFSET_STATUS_NUMBER = 17;
    /**
     * The constant STATUS_PACKET_OFFSET_STATUS_TIME.
     */
    public final static int STATUS_PACKET_OFFSET_STATUS_TIME = 21;
    // Byte
    /**
     * The constant STATUS_PACKET_OFFSET_FLOID_MODEL.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_FLOID_MODEL = 25;  // NOTE: CURRENTLY UNUSED!!!
    // Mode:
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_FLOID_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_FLOID_MODE = 26;
    // Direction, velocity, altitude:
    // Float:
    /**
     * The constant STATUS_PACKET_OFFSET_DIRECTION_OVER_GROUND.
     */
    public final static int STATUS_PACKET_OFFSET_DIRECTION_OVER_GROUND = 27;
    /**
     * The constant STATUS_PACKET_OFFSET_VELOCITY_OVER_GROUND.
     */
    public final static int STATUS_PACKET_OFFSET_VELOCITY_OVER_GROUND = 31;
    /**
     * The constant STATUS_PACKET_OFFSET_ALTITUDE.
     */
    public final static int STATUS_PACKET_OFFSET_ALTITUDE = 35;
    /**
     * The constant STATUS_PACKET_OFFSET_ALTITUDE_VELOCITY.
     */
    public final static int STATUS_PACKET_OFFSET_ALTITUDE_VELOCITY = 39;
    // Goals:
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_HAS_GOAL.
     */
    public final static int STATUS_PACKET_OFFSET_HAS_GOAL = 43;
    // Long:
    /**
     * The constant STATUS_PACKET_OFFSET_GOAL_TICKS.
     */
    public final static int STATUS_PACKET_OFFSET_GOAL_TICKS = 44;
    // Long:
    /**
     * The constant STATUS_PACKET_OFFSET_GOAL_START_TIME.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_GOAL_START_TIME = 48;  // NOTE: CURRENTLY UNUSED!!
    // Long:
    /**
     * The constant STATUS_PACKET_OFFSET_GOAL_ID.
     */
    public final static int STATUS_PACKET_OFFSET_GOAL_ID = 52;
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_GOAL_STATE.
     */
    public final static int STATUS_PACKET_OFFSET_GOAL_STATE = 56;
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_GOAL_TARGET_STATE.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_GOAL_TARGET_STATE = 57;  // NOTE: CURRENTLY UNUSED!!
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_GOAL_X.
     */
    public final static int STATUS_PACKET_OFFSET_GOAL_X = 58;
    /**
     * The constant STATUS_PACKET_OFFSET_GOAL_Y.
     */
    public final static int STATUS_PACKET_OFFSET_GOAL_Y = 62;
    /**
     * The constant STATUS_PACKET_OFFSET_GOAL_Z.
     */
    public final static int STATUS_PACKET_OFFSET_GOAL_Z = 66;
    /**
     * The constant STATUS_PACKET_OFFSET_GOAL_A.
     */
    public final static int STATUS_PACKET_OFFSET_GOAL_A = 70;
    // Lift-off, flight, land:
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_IN_FLIGHT.
     */
    public final static int STATUS_PACKET_OFFSET_IN_FLIGHT = 74;
    /**
     * The constant STATUS_PACKET_OFFSET_LIFT_OFF_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_LIFT_OFF_MODE = 75;
    /**
     * The constant STATUS_PACKET_OFFSET_LAND_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_LAND_MODE = 76;
    // ESCs:
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_0_ON.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_0_ON = 77;
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_1_ON.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_1_ON = 78;
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_2_ON.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_2_ON = 79;
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_3_ON.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_3_ON = 80;
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_0_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_0_VALUE = 81;
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_1_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_1_VALUE = 85;
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_2_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_2_VALUE = 89;
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_3_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_3_VALUE = 93;
    // Shorts:
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_0_PULSE.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_0_PULSE = 97;
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_1_PULSE.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_1_PULSE = 99;
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_2_PULSE.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_2_PULSE = 101;
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_3_PULSE.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_3_PULSE = 103;
    // Altimeter and count/error fields:
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE.
     */
    public final static int STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE = 105;
    /**
     * The constant STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE_VELOCITY.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE_VELOCITY = 109; // NOTE: CURRENTLY UNUSED!!!
    /**
     * The constant STATUS_PACKET_OFFSET_ALTIMETER_DELTA.
     */
    public final static int STATUS_PACKET_OFFSET_ALTIMETER_DELTA = 113;
    // Booleans:
    /**
     * The constant STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE_VALID.
     */
    public final static int STATUS_PACKET_OFFSET_ALTIMETER_ALTITUDE_VALID = 117;
    /**
     * The constant STATUS_PACKET_OFFSET_ALTIMETER_INITIALIZED.
     */
    public final static int STATUS_PACKET_OFFSET_ALTIMETER_INITIALIZED = 118;
    // Longs:
    /**
     * The constant STATUS_PACKET_OFFSET_ALTIMETER_GOOD_COUNT.
     */
    public final static int STATUS_PACKET_OFFSET_ALTIMETER_GOOD_COUNT = 119;
    /**
     * The constant STATUS_PACKET_OFFSET_ALTIMETER_LAST_UPDATE_TIME.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_ALTIMETER_LAST_UPDATE_TIME = 123;  // NOTE: CURRENTLY UNUSED!!!
    // Batteries:
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_MAIN_BATTERY.
     */
    public final static int STATUS_PACKET_OFFSET_MAIN_BATTERY = 127;
    /**
     * The constant STATUS_PACKET_OFFSET_HELI0_BATTERY.
     */
    public final static int STATUS_PACKET_OFFSET_HELI0_BATTERY = 131;
    /**
     * The constant STATUS_PACKET_OFFSET_HELI1_BATTERY.
     */
    public final static int STATUS_PACKET_OFFSET_HELI1_BATTERY = 135;
    /**
     * The constant STATUS_PACKET_OFFSET_HELI2_BATTERY.
     */
    public final static int STATUS_PACKET_OFFSET_HELI2_BATTERY = 139;
    /**
     * The constant STATUS_PACKET_OFFSET_HELI3_BATTERY.
     */
    public final static int STATUS_PACKET_OFFSET_HELI3_BATTERY = 143;
    /**
     * The constant STATUS_PACKET_OFFSET_AUX_BATTERY.
     */
    public final static int STATUS_PACKET_OFFSET_AUX_BATTERY = 147;
    /**
     * The constant STATUS_PACKET_OFFSET_HELI0_CURRENT.
     */
    public final static int STATUS_PACKET_OFFSET_HELI0_CURRENT = 151;
    /**
     * The constant STATUS_PACKET_OFFSET_HELI1_CURRENT.
     */
    public final static int STATUS_PACKET_OFFSET_HELI1_CURRENT = 155;
    /**
     * The constant STATUS_PACKET_OFFSET_HELI2_CURRENT.
     */
    public final static int STATUS_PACKET_OFFSET_HELI2_CURRENT = 159;
    /**
     * The constant STATUS_PACKET_OFFSET_HELI3_CURRENT.
     */
    public final static int STATUS_PACKET_OFFSET_HELI3_CURRENT = 163;
    /**
     * The constant STATUS_PACKET_OFFSET_AUX_CURRENT.
     */
    public final static int STATUS_PACKET_OFFSET_AUX_CURRENT = 167;
    // NMs:
    // Shorts:
    /**
     * The constant STATUS_PACKET_OFFSET_NM0.
     */
    public final static int STATUS_PACKET_OFFSET_NM0 = 171;
    /**
     * The constant STATUS_PACKET_OFFSET_NM1.
     */
    public final static int STATUS_PACKET_OFFSET_NM1 = 173;
    /**
     * The constant STATUS_PACKET_OFFSET_NM2.
     */
    public final static int STATUS_PACKET_OFFSET_NM2 = 175;
    /**
     * The constant STATUS_PACKET_OFFSET_NM3.
     */
    public final static int STATUS_PACKET_OFFSET_NM3 = 177;
    // Payloads:
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_BAY0.
     */
    public final static int STATUS_PACKET_OFFSET_BAY0 = 179;
    /**
     * The constant STATUS_PACKET_OFFSET_BAY1.
     */
    public final static int STATUS_PACKET_OFFSET_BAY1 = 180;
    /**
     * The constant STATUS_PACKET_OFFSET_BAY2.
     */
    public final static int STATUS_PACKET_OFFSET_BAY2 = 181;
    /**
     * The constant STATUS_PACKET_OFFSET_BAY3.
     */
    public final static int STATUS_PACKET_OFFSET_BAY3 = 182;
    // Payload Goal States:
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_PAYLOAD_0_GOAL_STATE.
     */
    public final static int STATUS_PACKET_OFFSET_PAYLOAD_0_GOAL_STATE = 183;
    /**
     * The constant STATUS_PACKET_OFFSET_PAYLOAD_1_GOAL_STATE.
     */
    public final static int STATUS_PACKET_OFFSET_PAYLOAD_1_GOAL_STATE = 184;
    /**
     * The constant STATUS_PACKET_OFFSET_PAYLOAD_2_GOAL_STATE.
     */
    public final static int STATUS_PACKET_OFFSET_PAYLOAD_2_GOAL_STATE = 185;
    /**
     * The constant STATUS_PACKET_OFFSET_PAYLOAD_3_GOAL_STATE.
     */
    public final static int STATUS_PACKET_OFFSET_PAYLOAD_3_GOAL_STATE = 186;
    // Camera
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_CAMERA_ON.
     */
    public final static int STATUS_PACKET_OFFSET_CAMERA_ON = 187;
    // Debug:
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_ON = 188;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_MEMORY_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_MEMORY_ON = 189;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_GPS_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_GPS_ON = 190;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_PYR_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_PYR_ON = 191;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_PHYSICS_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_PHYSICS_ON = 192;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_PAN_TILT_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_PAN_TILT_ON = 193;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_PAYLOADS_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_PAYLOADS_ON = 194;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_CAMERA_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_CAMERA_ON = 195;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_DESIGNATOR_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_DESIGNATOR_ON = 196;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_AUX_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_AUX_ON = 197;
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_HELIS_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_HELIS_ON = 198;
    // Designator:
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_DESIGNATOR_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DESIGNATOR_ON = 199;
    // GPS:
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_ON.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_ON = 200;
    // Long
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_TIME.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_TIME = 201;
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL = 205;
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL = 209;
    // Short:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_FIX_QUALITY.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_FIX_QUALITY = 213;
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_Z_METERS.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_Z_METERS = 215;
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_Z_FEET.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_Z_FEET = 219;
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_HDOP.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_HDOP = 223;
    // Short:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_SATS.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_SATS = 227;
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL_FILTERED.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_X_DEGREES_DECIMAL_FILTERED = 229;
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL_FILTERED.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_Y_DEGREES_DECIMAL_FILTERED = 233;
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_Z_METERS_FILTERED.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_Z_METERS_FILTERED = 237;
    // Longs:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_GOOD_COUNT.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_GOOD_COUNT = 241;
    // Helis:
    // ======
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_HELIS_ON.
     */
    public final static int STATUS_PACKET_OFFSET_HELIS_ON = 245;
    // Servos:
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_S_0_ON.
     */
    public final static int STATUS_PACKET_OFFSET_S_0_ON = 246;
    /**
     * The constant STATUS_PACKET_OFFSET_S_1_ON.
     */
    public final static int STATUS_PACKET_OFFSET_S_1_ON = 247;
    /**
     * The constant STATUS_PACKET_OFFSET_S_2_ON.
     */
    public final static int STATUS_PACKET_OFFSET_S_2_ON = 248;
    /**
     * The constant STATUS_PACKET_OFFSET_S_3_ON.
     */
    public final static int STATUS_PACKET_OFFSET_S_3_ON = 249;
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_S_0_L_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_0_L_VALUE = 250;
    /**
     * The constant STATUS_PACKET_OFFSET_S_0_R_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_0_R_VALUE = 254;
    /**
     * The constant STATUS_PACKET_OFFSET_S_0_P_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_0_P_VALUE = 258;
    /**
     * The constant STATUS_PACKET_OFFSET_S_1_L_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_1_L_VALUE = 262;
    /**
     * The constant STATUS_PACKET_OFFSET_S_1_R_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_1_R_VALUE = 266;
    /**
     * The constant STATUS_PACKET_OFFSET_S_1_P_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_1_P_VALUE = 270;
    /**
     * The constant STATUS_PACKET_OFFSET_S_2_L_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_2_L_VALUE = 274;
    /**
     * The constant STATUS_PACKET_OFFSET_S_2_R_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_2_R_VALUE = 278;
    /**
     * The constant STATUS_PACKET_OFFSET_S_2_P_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_2_P_VALUE = 282;
    /**
     * The constant STATUS_PACKET_OFFSET_S_3_L_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_3_L_VALUE = 286;
    /**
     * The constant STATUS_PACKET_OFFSET_S_3_R_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_3_R_VALUE = 290;
    /**
     * The constant STATUS_PACKET_OFFSET_S_3_P_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_S_3_P_VALUE = 294;
    // LED:
    // ====
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_LED_ON.
     */
    public final static int STATUS_PACKET_OFFSET_LED_ON = 298;
    // Parachute:
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_PARACHUTE_DEPLOYED.
     */
    public final static int STATUS_PACKET_OFFSET_PARACHUTE_DEPLOYED = 299;
    // Pan-tilts:
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_PT_0_ON.
     */
    public final static int STATUS_PACKET_OFFSET_PT_0_ON = 300;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_1_ON.
     */
    public final static int STATUS_PACKET_OFFSET_PT_1_ON = 301;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_2_ON.
     */
    public final static int STATUS_PACKET_OFFSET_PT_2_ON = 302;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_3_ON.
     */
    public final static int STATUS_PACKET_OFFSET_PT_3_ON = 303;
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_PT_0_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_0_MODE = 304;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_1_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_1_MODE = 305;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_2_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_2_MODE = 306;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_3_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_3_MODE = 307;
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_PT_0_TARGET_X.
     */
    public final static int STATUS_PACKET_OFFSET_PT_0_TARGET_X = 308;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_1_TARGET_X.
     */
    public final static int STATUS_PACKET_OFFSET_PT_1_TARGET_X = 312;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_2_TARGET_X.
     */
    public final static int STATUS_PACKET_OFFSET_PT_2_TARGET_X = 316;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_3_TARGET_X.
     */
    public final static int STATUS_PACKET_OFFSET_PT_3_TARGET_X = 320;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_0_TARGET_Y.
     */
    public final static int STATUS_PACKET_OFFSET_PT_0_TARGET_Y = 324;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_1_TARGET_Y.
     */
    public final static int STATUS_PACKET_OFFSET_PT_1_TARGET_Y = 328;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_2_TARGET_Y.
     */
    public final static int STATUS_PACKET_OFFSET_PT_2_TARGET_Y = 332;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_3_TARGET_Y.
     */
    public final static int STATUS_PACKET_OFFSET_PT_3_TARGET_Y = 336;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_0_TARGET_Z.
     */
    public final static int STATUS_PACKET_OFFSET_PT_0_TARGET_Z = 340;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_1_TARGET_Z.
     */
    public final static int STATUS_PACKET_OFFSET_PT_1_TARGET_Z = 344;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_2_TARGET_Z.
     */
    public final static int STATUS_PACKET_OFFSET_PT_2_TARGET_Z = 348;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_3_TARGET_Z.
     */
    public final static int STATUS_PACKET_OFFSET_PT_3_TARGET_Z = 352;
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_PT_0_PAN_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_0_PAN_VALUE = 356;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_0_TILT_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_0_TILT_VALUE = 357;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_1_PAN_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_1_PAN_VALUE = 358;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_1_TILT_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_1_TILT_VALUE = 359;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_2_PAN_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_2_PAN_VALUE = 360;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_2_TILT_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_2_TILT_VALUE = 361;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_3_PAN_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_3_PAN_VALUE = 362;
    /**
     * The constant STATUS_PACKET_OFFSET_PT_3_TILT_VALUE.
     */
    public final static int STATUS_PACKET_OFFSET_PT_3_TILT_VALUE = 363;
    // AUX:
    // ====
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_AUX_ON.
     */
    public final static int STATUS_PACKET_OFFSET_AUX_ON = 364;
    // PYR:
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_ON.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_ON = 365;
    // Bytes:
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_DIGITAL_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_DIGITAL_MODE = 366;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_ANALOG_RATE.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_ANALOG_RATE = 367;
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_PITCH_VELOCITY.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_PITCH_VELOCITY = 368;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_ROLL_VELOCITY.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_ROLL_VELOCITY = 372;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_HEIGHT_VELOCITY.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_HEIGHT_VELOCITY = 376;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_HEADING_VELOCITY.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_HEADING_VELOCITY = 380;
    // Longs:
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_GOOD_COUNT.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_GOOD_COUNT = 384;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_ERROR_COUNT.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_ERROR_COUNT = 388;
    // PYR Buffer entry:
    // Long:
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_TIME.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_PYR_TIME = 392;  // NOTE: CURRENTLY UNUSED
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_ROLL.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_ROLL = 396;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_ROLL_SIN.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_PYR_ROLL_SIN = 400;  // NOTE: CURRENTLY UNUSED
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_ROLL_COS.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_PYR_ROLL_COS = 404;  // NOTE: CURRENTLY UNUSED
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_ROLL_SMOOTHED.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_ROLL_SMOOTHED = 408;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_PITCH.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_PITCH = 412;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_PITCH_SIN.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_PYR_PITCH_SIN = 416;  // NOTE: CURRENTLY UNUSED
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_PITCH_COS.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_PYR_PITCH_COS = 420;  // NOTE: CURRENTLY UNUSED
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_PITCH_SMOOTHED.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_PITCH_SMOOTHED = 424;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_HEADING.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_HEADING = 428;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_HEADING_SIN.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_PYR_HEADING_SIN = 432;  // NOTE: CURRENTLY UNUSED
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_HEADING_COS.
     */
    @SuppressWarnings("unused")
    public final static int STATUS_PACKET_OFFSET_PYR_HEADING_COS = 436;  // NOTE: CURRENTLY UNUSED
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_HEADING_SMOOTHED.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_HEADING_SMOOTHED = 440;
    // Float:
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_TEMPERATURE.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_TEMPERATURE = 444;
    // Floats:
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_HEIGHT.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_HEIGHT = 448;
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_HEIGHT_SMOOTHED.
     */
    public final static int STATUS_PACKET_OFFSET_PYR_HEIGHT_SMOOTHED = 452;
    // ESC Type:
    // Short:
    /**
     * The constant STATUS_PACKET_OFFSET_ESC_TYPE.
     */
    public final static int STATUS_PACKET_OFFSET_ESC_TYPE = 456;
    // Memory:
    // =======
    // Short:
    /**
     * The constant STATUS_PACKET_OFFSET_FREE_MEMORY.
     */
    public final static int STATUS_PACKET_OFFSET_FREE_MEMORY = 458;
    // Loop count and rate:
    // Short:
    /**
     * The constant STATUS_PACKET_OFFSET_LOOP_COUNT.
     */
    public final static int STATUS_PACKET_OFFSET_LOOP_COUNT = 460;
    // Float:
    /**
     * The constant STATUS_PACKET_OFFSET_LOOP_RATE.
     */
    public final static int STATUS_PACKET_OFFSET_LOOP_RATE = 462;
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_SERIAL_OUTPUT.
     */
    public final static int STATUS_PACKET_OFFSET_SERIAL_OUTPUT = 466;
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_STATUS_RATE.
     */
    public final static int STATUS_PACKET_OFFSET_STATUS_RATE = 467;
    // Float:
    /**
     * The constant STATUS_PACKET_OFFSET_DECLINATION.
     */
    public final static int STATUS_PACKET_OFFSET_DECLINATION = 468;
    // Byte:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_RATE.
     */
    public final static int STATUS_PACKET_OFFSET_GPS_RATE = 472;
    // Single float:
    /**
     * The constant STATUS_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE.
     */
    public static final int STATUS_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE = 473;
    // Single floats:
    /**
     * The constant STATUS_PACKET_OFFSET_DISTANCE_TO_TARGET.
     */
    public static final int STATUS_PACKET_OFFSET_DISTANCE_TO_TARGET = 477;
    /**
     * The constant STATUS_PACKET_OFFSET_ALTITUDE_TO_TARGET.
     */
    public static final int STATUS_PACKET_OFFSET_ALTITUDE_TO_TARGET = 481;
    /**
     * The constant STATUS_PACKET_OFFSET_ORIENTATION_TO_TARGET.
     */
    public static final int STATUS_PACKET_OFFSET_ORIENTATION_TO_TARGET = 485;
    /**
     * The constant STATUS_PACKET_OFFSET_ORIENTATION_TO_GOAL.
     */
    public static final int STATUS_PACKET_OFFSET_ORIENTATION_TO_GOAL = 489;
    /**
     * The constant STATUS_PACKET_OFFSET_ATTACK_ANGLE.
     */
    public static final int STATUS_PACKET_OFFSET_ATTACK_ANGLE = 493;
    // Single floats:
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_ORIENTATION_PITCH.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_ORIENTATION_PITCH = 497;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_ORIENTATION_ROLL.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_ORIENTATION_ROLL = 501;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_ORIENTATION_PITCH_DELTA.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_ORIENTATION_PITCH_DELTA = 505;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_ORIENTATION_ROLL_DELTA.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_ORIENTATION_ROLL_DELTA = 509;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_PITCH_VELOCITY.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_PITCH_VELOCITY = 513;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_ROLL_VELOCITY.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_ROLL_VELOCITY = 517;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_PITCH_VELOCITY_DELTA.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_PITCH_VELOCITY_DELTA = 521;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_ROLL_VELOCITY_DELTA.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_ROLL_VELOCITY_DELTA = 525;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY = 529;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_DELTA.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_DELTA = 533;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_HEADING_VELOCITY.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_HEADING_VELOCITY = 537;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_HEADING_VELOCITY_DELTA.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_HEADING_VELOCITY_DELTA = 541;
    /**
     * The constant STATUS_PACKET_OFFSET_TARGET_XY_VELOCITY.
     */
    public static final int STATUS_PACKET_OFFSET_TARGET_XY_VELOCITY = 545;
    // Land mode items:
    // Boolean
    /**
     * The constant STATUS_PACKET_OFFSET_LAND_MODE_MIN_ALTITUDE_CHECK_STARTED.
     */
    @SuppressWarnings("unused")
    public static final int STATUS_PACKET_OFFSET_LAND_MODE_MIN_ALTITUDE_CHECK_STARTED = 549;
    // Float:
    /**
     * The constant STATUS_PACKET_OFFSET_LAND_MODE_MIN_ALTITUDE.
     */
    @SuppressWarnings("unused")
    public static final int STATUS_PACKET_OFFSET_LAND_MODE_MIN_ALTITUDE = 550;
    // Unsigned long:
    /**
     * The constant STATUS_PACKET_OFFSET_LAND_MODE_MIN_ALTITUDE_TIME.
     */
    @SuppressWarnings("unused")
    public static final int STATUS_PACKET_OFFSET_LAND_MODE_MIN_ALTITUDE_TIME = 554;
    // Boolean
    /**
     * The constant STATUS_PACKET_OFFSET_FOLLOW_MODE.
     */
    public static final int STATUS_PACKET_OFFSET_FOLLOW_MODE = 558;
    // Boolean:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_EMULATE.
     */
    public static final int STATUS_PACKET_OFFSET_GPS_EMULATE = 559;
    // Boolean:
    /**
     * The constant STATUS_PACKET_OFFSET_HELI_STARTUP_MODE.
     */
    public static final int STATUS_PACKET_OFFSET_HELI_STARTUP_MODE = 560;
    // Boolean:
    /**
     * The constant STATUS_PACKET_OFFSET_GPS_DEVICE.
     */
    public static final int STATUS_PACKET_OFFSET_GPS_DEVICE = 561;
    // Boolean:
    /**
     * The constant STATUS_PACKET_OFFSET_PYR_DEVICE.
     */
    public static final int STATUS_PACKET_OFFSET_PYR_DEVICE = 562;
    // Boolean:
    /**
     * The constant STATUS_PACKET_OFFSET_DEBUG_ALTIMETER_ON.
     */
    public final static int STATUS_PACKET_OFFSET_DEBUG_ALTIMETER_ON = 563;
    /**
     * The constant STATUS_PACKET_OFFSET_FLOID_RUN_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_FLOID_RUN_MODE = 564;
    /**
     * The constant STATUS_PACKET_OFFSET_FLOID_RUN_MODE.
     */
    public final static int STATUS_PACKET_OFFSET_FLOID_TEST_MODE = 565;
    /**
     * The constant STATUS_PACKET_OFFSET_IMU_ALGORITHM_STATUS.
     */
    public final static int STATUS_PACKET_OFFSET_IMU_ALGORITHM_STATUS = 566;

    // COMMAND RESPONSE:
    // =================
    /**
     * The constant COMMAND_RESPONSE_PACKET_SIZE.
     */
    public final static int COMMAND_RESPONSE_PACKET_SIZE = 29;
    /**
     * The constant COMMAND_RESPONSE_COMMAND_NUMBER_OFFSET.
     */
    public final static int COMMAND_RESPONSE_COMMAND_NUMBER_OFFSET = 17;
    /**
     * The constant COMMAND_RESPONSE_STATUS_OFFSET.
     */
    public final static int COMMAND_RESPONSE_STATUS_OFFSET = 21;
    /**
     * The constant COMMAND_RESPONSE_SUB_STATUS_OFFSET.
     */
    @SuppressWarnings("unused")
    public final static int COMMAND_RESPONSE_SUB_STATUS_OFFSET = 23;  // NOTE: NOT CURRENTLY USED!!!
    /**
     * The constant COMMAND_RESPONSE_IDENTIFIER_OFFSET.
     */
    public final static int COMMAND_RESPONSE_IDENTIFIER_OFFSET = 25;
    /**
     * The constant COMMAND_RESPONSE_OK.
     */
    public final static int COMMAND_RESPONSE_OK = 0x00;
    /**
     * The constant COMMAND_RESPONSE_ERROR.
     */
    public final static int COMMAND_RESPONSE_ERROR = 0x01;
    /**
     * The constant COMMAND_RESPONSE_LOGIC_ERROR.
     */
    public final static int COMMAND_RESPONSE_LOGIC_ERROR = 0x02;
    /**
     * The constant COMMAND_RESPONSE_COMM_ERROR.
     */
    public final static int COMMAND_RESPONSE_COMM_ERROR = 0x10;
    // MODEL PACKET:
    // =============
    /**
     * The constant MODEL_PARAMETERS_PACKET_SIZE.
     */
    public final static int MODEL_PARAMETERS_PACKET_SIZE = 412;
    // Collective/cyclic:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_COLLECTIVE_MIN.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_COLLECTIVE_MIN = 17;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_COLLECTIVE_MAX.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_COLLECTIVE_MAX = 21;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_COLLECTIVE_DEFAULT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_COLLECTIVE_DEFAULT = 25;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_CYCLIC_RANGE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_CYCLIC_RANGE = 29;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_CYCLIC_DEFAULT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_CYCLIC_DEFAULT = 33;
    // Servos:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_SERVO_DEGREE_MIN.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_SERVO_DEGREE_MIN = 37;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_SERVO_DEGREE_MAX.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_SERVO_DEGREE_MAX = 41;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_SERVO_PULSE_MIN.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_SERVO_PULSE_MIN = 45;  // Short
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_SERVO_PULSE_MAX.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_SERVO_PULSE_MAX = 47;  // Short
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_0_SERVO_OFFSET_LEFT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_0_SERVO_OFFSET_LEFT = 49;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_0_SERVO_OFFSET_RIGHT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_0_SERVO_OFFSET_RIGHT = 53;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_0_SERVO_OFFSET_PITCH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_0_SERVO_OFFSET_PITCH = 57;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_1_SERVO_OFFSET_LEFT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_1_SERVO_OFFSET_LEFT = 61;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_1_SERVO_OFFSET_RIGHT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_1_SERVO_OFFSET_RIGHT = 65;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_1_SERVO_OFFSET_PITCH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_1_SERVO_OFFSET_PITCH = 69;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_2_SERVO_OFFSET_LEFT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_2_SERVO_OFFSET_LEFT = 73;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_2_SERVO_OFFSET_RIGHT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_2_SERVO_OFFSET_RIGHT = 77;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_2_SERVO_OFFSET_PITCH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_2_SERVO_OFFSET_PITCH = 81;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_3_SERVO_OFFSET_LEFT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_3_SERVO_OFFSET_LEFT = 85;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_3_SERVO_OFFSET_RIGHT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_3_SERVO_OFFSET_RIGHT = 89;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_3_SERVO_OFFSET_PITCH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_3_SERVO_OFFSET_PITCH = 93;



    // Target velocity alphas:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_TARGET_VELOCITY_KEEP_STILL.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_TARGET_VELOCITY_KEEP_STILL = 61 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_TARGET_VELOCITY_FULL_SPEED.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_TARGET_VELOCITY_FULL_SPEED = 65 + 36;
    // Blades:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_BLADES_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_BLADES_LOW = 69 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_BLADES_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_BLADES_ZERO = 73 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_BLADES_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_BLADES_HIGH = 77 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H0S0_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H0S0_LOW = 81 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H0S0_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H0S0_ZERO = 85 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H0S0_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H0S0_HIGH = 89 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H0S1_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H0S1_LOW = 93 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H0S1_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H0S1_ZERO = 97 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H0S1_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H0S1_HIGH = 101 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H0S2_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H0S2_LOW = 105 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H0S2_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H0S2_ZERO = 109 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H0S2_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H0S2_HIGH = 113 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H1S0_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H1S0_LOW = 117 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H1S0_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H1S0_ZERO = 121 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H1S0_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H1S0_HIGH = 125 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H1S1_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H1S1_LOW = 129 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H1S1_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H1S1_ZERO = 133 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H1S1_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H1S1_HIGH = 137 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H1S2_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H1S2_LOW = 141 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H1S2_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H1S2_ZERO = 145 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H1S2_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H1S2_HIGH = 149 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H2S0_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H2S0_LOW = 153 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H2S0_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H2S0_ZERO = 157 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H2S0_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H2S0_HIGH = 161 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H2S1_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H2S1_LOW = 165 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H2S1_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H2S1_ZERO = 169 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H2S1_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H2S1_HIGH = 173 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H2S2_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H2S2_LOW = 177 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H2S2_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H2S2_ZERO = 181 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H2S2_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H2S2_HIGH = 185 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H3S0_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H3S0_LOW = 189 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H3S0_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H3S0_ZERO = 193 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H3S0_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H3S0_HIGH = 197 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H3S1_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H3S1_LOW = 201 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H3S1_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H3S1_ZERO = 205 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H3S1_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H3S1_HIGH = 209 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H3S2_LOW.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H3S2_LOW = 213 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H3S2_ZERO.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H3S2_ZERO = 217 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_H3S2_HIGH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_H3S2_HIGH = 221 + 36;
    // Servo Sign:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_SERVO_SIGN_LEFT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_SERVO_SIGN_LEFT = 225 + 36;  // Byte
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_SERVO_SIGN_RIGHT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_SERVO_SIGN_RIGHT = 226 + 36;  // Byte
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_SERVO_SIGN_PITCH.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_SERVO_SIGN_PITCH = 227 + 36;  // Byte
    // ESCs:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ESC_TYPE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ESC_TYPE = 228 + 36;  // Short
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ESC_LOW_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ESC_LOW_VALUE = 230 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ESC_HIGH_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ESC_HIGH_VALUE = 234 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ESC_PULSE_MIN.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ESC_PULSE_MIN = 238 + 36;  // Short
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ESC_PULSE_MAX.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ESC_PULSE_MAX = 240 + 36;  // Short
    // Pitch, roll, heading and altitude alphas:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_TARGET_PITCH_VELOCITY_ALPHA.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_TARGET_PITCH_VELOCITY_ALPHA = 242 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_TARGET_ROLL_VELOCITY_ALPHA.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_TARGET_ROLL_VELOCITY_ALPHA = 246 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_TARGET_HEADING_VELOCITY_ALPHA.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_TARGET_HEADING_VELOCITY_ALPHA = 250 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_ALPHA.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_ALPHA = 254 + 36;
    // Attack angle:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ATTACK_ANGLE_MIN_DISTANCE_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ATTACK_ANGLE_MIN_DISTANCE_VALUE = 258 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ATTACK_ANGLE_MAX_DISTANCE_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ATTACK_ANGLE_MAX_DISTANCE_VALUE = 262 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ATTACK_ANGLE_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ATTACK_ANGLE_VALUE = 266 + 36;
    // Orientation min:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ORIENTATION_MIN_DISTANCE_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ORIENTATION_MIN_DISTANCE_VALUE = 270 + 36;
    // Pitch, roll, altitude, heading and distance to target deltas and targets
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_PITCH_DELTA_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_PITCH_DELTA_MIN_VALUE = 274 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_PITCH_DELTA_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_PITCH_DELTA_MAX_VALUE = 278 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_PITCH_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_PITCH_TARGET_VELOCITY_MAX_VALUE = 282 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ROLL_DELTA_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ROLL_DELTA_MIN_VALUE = 286 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ROLL_DELTA_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ROLL_DELTA_MAX_VALUE = 290 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ROLL_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ROLL_TARGET_VELOCITY_MAX_VALUE = 294 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ALTITUDE_TO_TARGET_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ALTITUDE_TO_TARGET_MIN_VALUE = 298 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ALTITUDE_TO_TARGET_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ALTITUDE_TO_TARGET_MAX_VALUE = 302 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ALTITUDE_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ALTITUDE_TARGET_VELOCITY_MAX_VALUE = 306 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HEADING_DELTA_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HEADING_DELTA_MIN_VALUE = 310 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HEADING_DELTA_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HEADING_DELTA_MAX_VALUE = 314 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HEADING_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HEADING_TARGET_VELOCITY_MAX_VALUE = 318 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_DISTANCE_TO_TARGET_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_DISTANCE_TO_TARGET_MIN_VALUE = 322 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_DISTANCE_TO_TARGET_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_DISTANCE_TO_TARGET_MAX_VALUE = 326 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_DISTANCE_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_DISTANCE_TARGET_VELOCITY_MAX_VALUE = 330 + 36;
    // Lift off / land:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE_DELTA_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE_DELTA_VALUE = 334 + 36;
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE = 338 + 36; // Unsigned long
    // Start mode:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_START_MODE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_START_MODE = 342 + 36; // Byte
    // Rotation mode:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ROTATION_MODE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ROTATION_MODE = 343 + 36; // Byte
    // Cyclic alpha:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_CYCLIC_HEADING_ALPHA.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_CYCLIC_HEADING_ALPHA = 344 + 36; // Float
    // Startup:
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_STARTUP_NUMBER_STEPS.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_STARTUP_NUMBER_STEPS = 348 + 36; // Short
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_HELI_STARTUP_STEP_TICK.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_HELI_STARTUP_STEP_TICK = 350 + 36; // Short
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_CALC_MIDPOINT.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_CALC_MIDPOINT = 352 + 36; // Float

    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_LOW_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_LOW_VALUE = 356 + 36; // Float
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_MID_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_MID_VALUE = 360 + 36; // Float
    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_HIGH_VALUE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ESC_COLLECTIVE_HIGH_VALUE = 364 + 36; // Float

        /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_VELOCITY_CYCLIC_ALPHA_SCALE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_VELOCITY_CYCLIC_ALPHA_SCALE = 368 + 36; // Float

    /**
     * The constant MODEL_PARAMETERS_PACKET_OFFSET_ACCELERATION_MULTIPLIER_SCALE.
     */
    public static final int MODEL_PARAMETERS_PACKET_OFFSET_ACCELERATION_MULTIPLIER_SCALE = 372 + 36; // Float

    // MODEL PARAMETERS OUTGOING PACKETS: (to Arduino - limited to 128 byte receive buffer - hence they are split)
    // ==================================
    // 1:
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_LOW.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_LOW = 0x11;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_ZERO.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_ZERO = 0x15;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_HIGH.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_HIGH = 0x19;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_LOW.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_LOW = 0x1D;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_LOW.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_LOW = 0x21;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_LOW.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_LOW = 0x25;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_LOW.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_LOW = 0x29;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_LOW.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_LOW = 0x2D;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_LOW.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_LOW = 0x31;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_ZERO.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_ZERO = 0x35;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_ZERO.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_ZERO = 0x39;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_ZERO.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_ZERO = 0x3D;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_ZERO.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_ZERO = 0x41;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_ZERO.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_ZERO = 0x45;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_ZERO.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_ZERO = 0x49;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_HIGH.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_HIGH = 0x4D;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_HIGH.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_HIGH = 0x51;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_HIGH.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_HIGH = 0x55;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_HIGH.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_HIGH = 0x59;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_HIGH.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_HIGH = 0x5D;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_HIGH.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_HIGH = 0x61;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_LEFT.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_LEFT = 0x65;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_RIGHT.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_RIGHT = 0x69;
    /**
     * The constant MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_PITCH.
     */
    public static final int MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_PITCH = 0x6D;

    // 2:
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_LOW.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_LOW = 0x11;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_LOW.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_LOW = 0x15;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_LOW.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_LOW = 0x19;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_LOW.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_LOW = 0x1D;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_LOW.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_LOW = 0x21;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_LOW.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_LOW = 0x25;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_ZERO.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_ZERO = 0x29;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_ZERO.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_ZERO = 0x2D;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_ZERO.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_ZERO = 0x31;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_ZERO.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_ZERO = 0x35;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_ZERO.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_ZERO = 0x39;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_ZERO.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_ZERO = 0x3D;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_HIGH.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_HIGH = 0x41;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_HIGH.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_HIGH = 0x45;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_HIGH.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_HIGH = 0x49;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_HIGH.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_HIGH = 0x4D;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_HIGH.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_HIGH = 0x51;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_HIGH.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_HIGH = 0x55;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_MIDPOINT.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_MIDPOINT = 0x59;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_LOW.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_LOW = 0x5D;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_MID.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_MID = 0x61;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_HIGH.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_HIGH = 0x65;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_LEFT.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_LEFT = 0x69;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_RIGHT.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_RIGHT = 0x6D;
    /**
     * The constant MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_PITCH.
     */
    public static final int MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_PITCH = 0x71;
    // 3:
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MIN.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MIN = 0x11;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MAX.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MAX = 0x15;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_DEFAULT.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_DEFAULT = 0x19;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_RANGE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_RANGE = 0x1D;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_DEFAULT.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_DEFAULT = 0x21;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_TYPE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_TYPE = 0x25;  // Short
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MIN.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MIN = 0x27;  // Short
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MAX.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MAX = 0x29;  // Short
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_LOW_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_LOW_VALUE = 0x2B;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_HIGH_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_HIGH_VALUE = 0x2F;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MIN_DISTANCE_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MIN_DISTANCE_VALUE = 0x33;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MAX_DISTANCE_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MAX_DISTANCE_VALUE = 0x37;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_VALUE = 0x3B;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MIN_VALUE = 0x3F;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MAX_VALUE = 0x43;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_TARGET_VELOCITY_MAX_VALUE = 0x47;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MIN_VALUE = 0x4B;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MAX_VALUE = 0x4F;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_TARGET_VELOCITY_MAX_VALUE = 0x53;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MIN_VALUE = 0x57;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MAX_VALUE = 0x5B;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TARGET_VELOCITY_MAX_VALUE = 0x5F;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE = 0x63;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_START_MODE.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_START_MODE = 0x67;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_LEFT.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_LEFT = 0x68;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_RIGHT.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_RIGHT = 0x6C;
    /**
     * The constant MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_PITCH.
     */
    public static final int MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_PITCH = 0x70;
    // 4:
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MIN_VALUE = 0x11;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MAX_VALUE = 0x15;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_TARGET_VELOCITY_MAX_VALUE = 0x19;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MIN_VALUE.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MIN_VALUE = 0x1D;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MAX_VALUE = 0x21;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TARGET_VELOCITY_MAX_VALUE.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TARGET_VELOCITY_MAX_VALUE = 0x25;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_ORIENTATION_MIN_DISTANCE_VALUE.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_ORIENTATION_MIN_DISTANCE_VALUE = 0x2D;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE_DELTA_VALUE.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE_DELTA_VALUE = 0x31;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MIN.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MIN = 0x35;  // Short
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MAX.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MAX = 0x37;  // Short
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MIN.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MIN = 0x39;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MAX.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MAX = 0x3D;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_LEFT.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_LEFT = 0x41;  // Byte
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_RIGHT.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_RIGHT = 0x42;  // Byte
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_PITCH.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_PITCH = 0x43;  // Byte
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_LEFT.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_LEFT = 0x44;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_RIGHT.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_RIGHT = 0x48;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_PITCH.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_PITCH = 0x4C;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_KEEP_STILL.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_KEEP_STILL = 0x50;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_FULL_SPEED.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_FULL_SPEED = 0x54;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_PITCH_VELOCITY_ALPHA.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_PITCH_VELOCITY_ALPHA = 0x58;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ROLL_VELOCITY_ALPHA.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ROLL_VELOCITY_ALPHA = 0x5C;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_HEADING_VELOCITY_ALPHA.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_HEADING_VELOCITY_ALPHA = 0x60;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_ALPHA.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_ALPHA = 0x64;
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_ROTATION_MODE.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_ROTATION_MODE = 0x68; // Byte
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_CYCLIC_HEADING_ALPHA.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_CYCLIC_HEADING_ALPHA = 0x69; // Float
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_NUMBER_STEPS.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_NUMBER_STEPS = 0x6D; // Short
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_STEP_TICK.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_STEP_TICK = 0x6F; // Short
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_VELOCITY_DELTA_CYCLIC_ALPHA.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_VELOCITY_DELTA_CYCLIC_ALPHA = 0x71; // Float
    /**
     * The constant MODEL_PARAMETERS_4_PACKET_OFFSET_ACCELERATION_SCALE_MULTIPLIER.
     */
    public static final int MODEL_PARAMETERS_4_PACKET_OFFSET_ACCELERATION_SCALE_MULTIPLIER = 0x75; // Float
    // MODEL STATUS PACKET:
    // ====================
    /**
     * The constant MODEL_STATUS_PACKET_SIZE.
     */
    public static final int MODEL_STATUS_PACKET_SIZE = 337;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ORIENTATION.
     * Floats - per helicopter = 4 x 4 = 16 bytes
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ORIENTATION = 17;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ALTITUDE.
     * Floats - per helicopter = 4 x 4 = 16 bytes
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_ALTITUDE = 33;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_HEADING.
     * Floats - per helicopter = 4 x 4 = 16 bytes
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_HEADING = 49;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_TOTAL
     * Floats - per helicopter - 16 bytes:
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_DELTA_TOTAL = 65;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE.
     * Floats - per helicopter - per servo = 4 x 4 x 3 = 48 bytes
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_CYCLIC_VALUE = 81;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_VALUE.
     * Floats - per helicopter = 4 x 4 = 16 bytes
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_COLLECTIVE_VALUE = 129;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH.
     * Floats - per helicopter - per servo = 4 x 4 x 3 = 48 bytes
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_CALCULATED_CYCLIC_BLADE_PITCH = 145;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_CALCULATED_COLLECTIVE_BLADE_PITCH.
     * Floats - per helicopter = 4 x 4 = 16 bytes
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_CALCULATED_COLLECTIVE_BLADE_PITCH = 193;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH.
     * Floats - per helicopter - per servo = 4 x 4 x 3 = 48 bytes
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_CALCULATED_BLADE_PITCH = 209;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES.
     * Floats - per helicopter - per servo = 4 x 4 x 3 = 48 bytes
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_DEGREES = 257;
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE.
     * Shorts - per helicopter - per servo = 4 x 3 x 2 = 24
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_CALCULATED_SERVO_PULSE = 305;
    // Cyclic Heading (for Normal Rotation mode)
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_CYCLIC_HEADING.
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_CYCLIC_HEADING = 329; // Float
    /**
     * The constant MODEL_STATUS_PACKET_OFFSET_VELOCITY_CYCLIC_ALPHA.
     */
    public static final int MODEL_STATUS_PACKET_OFFSET_VELOCITY_CYCLIC_ALPHA = 333; // Float
    // Test goals (Outgoing:)
    /**
     * The constant TEST_GOALS_PACKET_SIZE.
     */
    public static final int TEST_GOALS_PACKET_SIZE = 0x21;
    /**
     * The constant TEST_GOALS_OFFSET_X.
     */
    public static final int TEST_GOALS_OFFSET_X = 0x11;
    /**
     * The constant TEST_GOALS_OFFSET_Y.
     */
    public static final int TEST_GOALS_OFFSET_Y = 0x15;
    /**
     * The constant TEST_GOALS_OFFSET_Z.
     */
    public static final int TEST_GOALS_OFFSET_Z = 0x19;
    /**
     * The constant TEST_GOALS_OFFSET_A.
     */
    public static final int TEST_GOALS_OFFSET_A = 0x1D;
    // Device GPS packet (Outgoing:)
    /**
     * The constant DEVICE_GPS_PACKET_SIZE.
     */
    public static final int DEVICE_GPS_PACKET_SIZE = 0x21;
    /**
     * The constant DEVICE_GPS_OFFSET_LAT.
     */
    public static final int DEVICE_GPS_OFFSET_LAT = 0x11;
    /**
     * The constant DEVICE_GPS_OFFSET_LNG.
     */
    public static final int DEVICE_GPS_OFFSET_LNG = 0x15;
    /**
     * The constant DEVICE_GPS_OFFSET_ALT.
     */
    public static final int DEVICE_GPS_OFFSET_ALT = 0x19;
    /**
     * The constant DEVICE_GPS_OFFSET_HDOP.
     */
    public static final int DEVICE_GPS_OFFSET_HDOP = 0x1D;
    // Device PYR packet (Outgoing:)
    /**
     * The constant DEVICE_PYR_PACKET_SIZE.
     */
    public static final int DEVICE_PYR_PACKET_SIZE = 0x25;
    /**
     * The constant DEVICE_PYR_OFFSET_PITCH.
     */
    public static final int DEVICE_PYR_OFFSET_PITCH = 0x11;
    /**
     * The constant DEVICE_PYR_OFFSET_HEADING.
     */
    public static final int DEVICE_PYR_OFFSET_HEADING = 0x15;
    /**
     * The constant DEVICE_PYR_OFFSET_ROLL.
     */
    public static final int DEVICE_PYR_OFFSET_ROLL = 0x19;
    /**
     * The constant DEVICE_PYR_OFFSET_ALTITUDE.
     */
    public static final int DEVICE_PYR_OFFSET_ALTITUDE = 0x1D;
    /**
     * The constant DEVICE_PYR_OFFSET_TEMPERATURE.
     */
    public static final int DEVICE_PYR_OFFSET_TEMPERATURE = 0x21;
    // TEST RESPONSE:
    // ==============
    /**
     * The constant TEST_RESPONSE_PACKET_SIZE.
     */
    public final static int TEST_RESPONSE_PACKET_SIZE = 56;
    /**
     * The constant TEST_RESPONSE_PACKET_OFFSET_BYTES.
     */
    public final static int TEST_RESPONSE_PACKET_OFFSET_BYTES = 17;
    /**
     * The constant TEST_RESPONSE_PACKET_OFFSET_INTS.
     */
    public final static int TEST_RESPONSE_PACKET_OFFSET_INTS = 20;
    /**
     * The constant TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_INTS.
     */
    public final static int TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_INTS = 26;
    /**
     * The constant TEST_RESPONSE_PACKET_OFFSET_LONGS.
     */
    public final static int TEST_RESPONSE_PACKET_OFFSET_LONGS = 32;
    /**
     * The constant TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_LONGS.
     */
    public final static int TEST_RESPONSE_PACKET_OFFSET_UNSIGNED_LONGS = 44;
    // DEBUG MESSAGE:
    // ==============
    /**
     * The constant DEBUG_PACKET_START_OFFSET.
     */
    public final static int DEBUG_PACKET_START_OFFSET = 17;
    // GOAL STATES:
    // ============
    /**
     * The constant GOAL_STATE_NONE.
     */
    public final static int GOAL_STATE_NONE = 0x00;
    /**
     * The constant GOAL_STATE_START.
     */
    public final static int GOAL_STATE_START = 0x01;
    /**
     * The constant GOAL_STATE_IN_PROGRESS.
     */
    public final static int GOAL_STATE_IN_PROGRESS = 0x02;
    /**
     * The constant GOAL_STATE_COMPLETE_SUCCESS.
     */
    public final static int GOAL_STATE_COMPLETE_SUCCESS = 0x03;
    /**
     * The constant GOAL_STATE_COMPLETE_ERROR.
     */
    public final static int GOAL_STATE_COMPLETE_ERROR = 0x04;
    // NANO-MUSCLE:
    // ============
    /**
     * The constant NM_CONTRACTED_LEVEL.
     */
    public static final int NM_CONTRACTED_LEVEL = 800;  // BELOW THIS LEVEL AND THE NM HAS CONTRACTED
    // Pan/Tilt Mode Values:
    // =====================
    /**
     * The constant PAN_TILT_MODE_ANGLE.
     */
    public static final int PAN_TILT_MODE_ANGLE = 0;
    /**
     * The constant PAN_TILT_MODE_TARGET.
     */
    public static final int PAN_TILT_MODE_TARGET = 1;
    /**
     * The constant PAN_TILT_MODE_SCAN.
     */
    public static final int PAN_TILT_MODE_SCAN = 2;
    // Thread efficiency throttles:
    // ============================
    /**
     * The floid thread timings.
     */
    public FloidThreadTimings mFloidThreadTimings = new FloidThreadTimings();
    // Turns on and off the client communicator:
    /**
     * The client communicator on.
     */
    public boolean mClientCommunicatorOn = false;  // This is the startup default - true=connect to server on startup; false=no
    // LOGGER: (NOTE THIS IS A PUBLIC ACCESSIBLE TO ALL CLASSES TO TURN OFF LOGGING COMPLETELY)
    // ================================================================================================
    /**
     * The use logger.
     */
    @SuppressWarnings("CanBeFinal")
    public boolean mUseLogger = true;
    /**
     * The kill service broadcast receiver.
     */
    BroadcastReceiver killServiceBroadcastReceiver;
    // Alert Sounds:
    // ============================
    /**
     * The constant FLOID_ALERT_SOUND_FLOID_STATUS_RECEIVED.
     */
    public static final int FLOID_ALERT_SOUND_FLOID_STATUS_RECEIVED = 0;
    /**
     * The constant FLOID_ALERT_SOUND_DEBUG_MESSAGE_RECEIVED.
     */
    public static final int FLOID_ALERT_SOUND_DEBUG_MESSAGE_RECEIVED = 1;
    /**
     * The constant FLOID_ALERT_SOUND_FLOID_DISCONNECTED.
     */
    public static final int FLOID_ALERT_SOUND_FLOID_DISCONNECTED = 2;
    /**
     * The constant FLOID_ALERT_SOUND_FLOID_CONNECTED.
     */
    public static final int FLOID_ALERT_SOUND_FLOID_CONNECTED = 3;
    /**
     * The constant FLOID_ALERT_SOUND_COMMAND_RESPONSE_RECEIVED.
     */
    public static final int FLOID_ALERT_SOUND_COMMAND_RESPONSE_RECEIVED = 4;
    /**
     * The constant FLOID_ALERT_SOUND_MODEL_STATUS_RECEIVED.
     */
    public static final int FLOID_ALERT_SOUND_MODEL_STATUS_RECEIVED = 5;
    /**
     * The constant FLOID_ALERT_SOUND_MODEL_PARAMETERS_RECEIVED.
     */
    public static final int FLOID_ALERT_SOUND_MODEL_PARAMETERS_RECEIVED = 6;
    /**
     * The constant FLOID_ALERT_SOUND_SERVER_CONNECTED.
     */
    public static final int FLOID_ALERT_SOUND_SERVER_CONNECTED = 7;
    /**
     * The constant FLOID_ALERT_SOUND_SERVER_DISCONNECTED.
     */
    public static final int FLOID_ALERT_SOUND_SERVER_DISCONNECTED = 8;
    /**
     * The constant FLOID_ALERT_SOUND_TAKE_PHOTO.
     */
    public static final int FLOID_ALERT_SOUND_TAKE_PHOTO = 9;
    /**
     * The constant FLOID_ALERT_SOUND_START_VIDEO.
     */
    public static final int FLOID_ALERT_SOUND_START_VIDEO = 10;
    /**
     * The constant FLOID_ALERT_SOUND_STOP_VIDEO.
     */
    public static final int FLOID_ALERT_SOUND_STOP_VIDEO = 11;
    /**
     * The constant FLOID_ALERT_SOUND_SEND_VIDEO_FRAME.
     */
    public static final int FLOID_ALERT_SOUND_SEND_VIDEO_FRAME = 12;
    /**
     * The constant FLOID_ALERT_SOUND_TEST_PACKET_RESPONSE.
     */
    public static final int FLOID_ALERT_SOUND_TEST_PACKET_RESPONSE = 13;
    /**
     * The constant FLOID_ALERT_SOUND_START_INTERFACE.
     */
    public static final int FLOID_ALERT_SOUND_START_INTERFACE = 14;
    /**
     * The constant FLOID_ALERT_SOUND_STOP_INTERFACE.
     */
    public static final int FLOID_ALERT_SOUND_STOP_INTERFACE = 15;
    /**
     * The constant FLOID_ALERT_SOUND_START_MISSION_TIMER.
     */
    public static final int FLOID_ALERT_SOUND_START_MISSION_TIMER = 16;
    /**
     * The constant FLOID_ALERT_SOUND_BUZZER.
     */
    public static final int FLOID_ALERT_SOUND_BUZZER = 17;
    /**
     * The constant FLOID_ALERT_SOUND_PYR_PACKET_SENT.
     */
    public static final int FLOID_ALERT_SOUND_PYR_PACKET_SENT = 18;
    /**
     * The constant FLOID_ALERT_SOUND_SERVICE_CREATE.
     */
    public static final int FLOID_ALERT_SOUND_SERVICE_CREATE = 19;
    /**
     * The constant FLOID_ALERT_SOUND_SERVICE_DESTROY.
     */
    public static final int FLOID_ALERT_SOUND_SERVICE_DESTROY = 20;
    // Bundle Keys:
    /**
     * The constant BUNDLE_KEY_TEST_PACKET_RESPONSE.
     */
    public static final String BUNDLE_KEY_TEST_PACKET_RESPONSE = "TestPacketResponse";
    /**
     * The constant BUNDLE_KEY_DEBUG_MESSAGE.
     */
    public static final String BUNDLE_KEY_DEBUG_MESSAGE = "DebugMessage";
    /**
     * The constant BUNDLE_KEY_ACCESSORY_CONNECTED.
     */
    public static final String BUNDLE_KEY_ACCESSORY_CONNECTED = "AccessoryConnected";
    /**
     * The constant BUNDLE_KEY_HOST_PORT.
     */
    public static final String BUNDLE_KEY_HOST_PORT = "HostPort";
    /**
     * The constant BUNDLE_KEY_HOST_ADDRESS.
     */
    public static final String BUNDLE_KEY_HOST_ADDRESS = "HostAddress";
    /**
     * The constant BUNDLE_KEY_FLOID_ID.
     */
    public static final String BUNDLE_KEY_FLOID_ID = "FloidId";
    /**
     * The constant BUNDLE_KEY_FLOID_UUID.
     */
    public static final String BUNDLE_KEY_FLOID_UUID = "FloidUuid";
    /**
     * The constant BUNDLE_KEY_LOG.
     */
    public static final String BUNDLE_KEY_LOG = "Log";
    /**
     * The constant BUNDLE_KEY_FLOID_SERVICE_PID.
     */
    public static final String BUNDLE_KEY_FLOID_SERVICE_PID = "FloidServicePid";
    /**
     * The constant BUNDLE_KEY_CLIENT_COMMUNICATOR.
     */
    public static final String BUNDLE_KEY_CLIENT_COMMUNICATOR = "ClientCommunicator";
    /**
     * The constant BUNDLE_KEY_FLOID_IMAGING_STATUS.
     */
    public static final String BUNDLE_KEY_FLOID_IMAGING_STATUS = "FloidImagingStatus";
    /**
     * The constant BUNDLE_KEY_MESSAGE.
     */
    public static final String BUNDLE_KEY_MESSAGE = "Message";
    /**
     * The constant BUNDLE_KEY_FLOID_DROID_STATUS.
     */
    public static final String BUNDLE_KEY_FLOID_DROID_STATUS = "FloidDroidStatus";
    /**
     * The constant BUNDLE_KEY_FLOID_STATUS.
     */
    public static final String BUNDLE_KEY_FLOID_STATUS = "FloidStatus";
    /**
     * The constant BUNDLE_KEY_FLOID_MODEL_STATUS.
     */
    public static final String BUNDLE_KEY_FLOID_MODEL_STATUS = "FloidModelStatus";
    /**
     * The constant BUNDLE_KEY_FLOID_MODEL_PARAMETERS.
     */
    public static final String BUNDLE_KEY_FLOID_MODEL_PARAMETERS = "FloidModelParameters";
    /**
     * The constant BUNDLE_KEY_MISSION_TIMER_UPDATE.
     */
    public static final String BUNDLE_KEY_MISSION_TIMER_UPDATE = "MissionTimeUpdate";
    /**
     * The constant BUNDLE_KEY_COMMAND_TO_SPEAK.
     */
    public static final String BUNDLE_KEY_COMMAND_TO_SPEAK = "CommandToSpeak";
    /**
     * The constant BUNDLE_KEY_STRING_TO_SPEAK.
     */
    public static final String BUNDLE_KEY_STRING_TO_SPEAK = "StringToSpeak";
    /**
     * The constant BUNDLE_KEY_COMM_ITEM.
     */
    public static final String BUNDLE_KEY_COMM_ITEM = "CommItem";
    /**
     * The constant BUNDLE_KEY_COMM_STATE.
     */
    public static final String BUNDLE_KEY_COMM_STATE = "CommState";
    /**
     * The constant BUNDLE_KEY_MISSION_START_TIMER_NAME.
     */
    public static final String BUNDLE_KEY_MISSION_START_NAME = "MissionStartName";
    /**
     * The constant BUNDLE_KEY_MISSION_START_TIMER_TIME.
     */
    public static final String BUNDLE_KEY_MISSION_START_TIMER_TIME = "MissionStartTimerTime";
    /**
     * The constant BUNDLE_KEY_NOTIFICATION.
     */
    public static final String BUNDLE_KEY_NOTIFICATION = "Notification";
    /**
     * The constant BUNDLE_KEY_MISSION_MODE.
     */
    public static final String BUNDLE_KEY_MISSION_MODE = "MissionMode";
    /**
     * The constant BUNDLE_KEY_THREAD_TIMINGS.
     */
    public static final String BUNDLE_KEY_THREAD_TIMINGS = "ThreadTimings";
    /**
     * The constant BUNDLE_KEY_OUTGOING_COMMAND.
     */
    public static final String BUNDLE_KEY_OUTGOING_COMMAND = "OutgoingCommand";
    /**
     * The constant BUNDLE_KEY_MODE.
     */
    public static final String BUNDLE_KEY_MODE = "Mode";
    /**
     * The constant BUNDLE_KEY_PHOTO.
     */
    public static final String BUNDLE_KEY_PHOTO = "Photo";
    /**
     * The constant BUNDLE_KEY_VIDEO_FRAME.
     */
    public static final String BUNDLE_KEY_VIDEO_FRAME = "VideoFrame";
    /**
     * The constant BUNDLE_KEY_CAMERA_INFO_LIST.
     */
    public static final String BUNDLE_KEY_CAMERA_INFO_LIST = "CameraInfoList";
    /**
     * The constant BUNDLE_KEY_ADDITIONAL_STATUS.
     */
    public static final String BUNDLE_KEY_ADDITIONAL_STATUS = "AdditionalStatus";
    /**
     * The constant BUNDLE_KEY_DROID_DEFAULT_PARAMETERS.
     */
    public static final String BUNDLE_KEY_DROID_DEFAULT_PARAMETERS = "DroidDefaultParameters";
    /**
     * The constant BUNDLE_KEY_FLOID_DROID_PARAMETERS.
     */
    public static final String BUNDLE_KEY_FLOID_DROID_PARAMETERS = "FloidDroidParameters";
    /**
     * The constant BUNDLE_KEY_FLOID_DROID_VALID_PARAMETERS.
     */
    public static final String BUNDLE_KEY_FLOID_DROID_VALID_PARAMETERS = "FloidDroidValidParameters";
    /**
     * The constant BUNDLE_KEY_COMMAND_RESPONSE.
     */
    public static final String BUNDLE_KEY_COMMAND_RESPONSE = "CommandResponse";
    /**
     * The constant BUNDLE_KEY_MISSION_READY_TIMER.
     */
    public static final String BUNDLE_KEY_MISSION_READY_TIMER = "MissionReadyTimer";
    /**
     * The constant BUNDLE_KEY_USB_ACCESSORY.
     */
    public static final String BUNDLE_KEY_USB_ACCESSORY = "UsbAccessory";
    // Media:
    /**
     * The floid status received media player.
     */
    private MediaPlayer mFloidStatusReceivedMediaPlayer;
    /**
     * The debug message received media player.
     */
    private MediaPlayer mDebugMessageReceivedMediaPlayer;
    /**
     * The floid connected media player.
     */
    private MediaPlayer mFloidConnectedMediaPlayer;
    /**
     * The floid disconnected media player.
     */
    private MediaPlayer mFloidDisconnectedMediaPlayer;
    /**
     * The command response received media player.
     */
    private MediaPlayer mCommandResponseReceivedMediaPlayer;
    /**
     * The model status received media player.
     */
    private MediaPlayer mModelStatusReceivedMediaPlayer;
    /**
     * The model parameters received media player.
     */
    private MediaPlayer mModelParametersReceivedMediaPlayer;
    /**
     * The server connected media player.
     */
    private MediaPlayer mServerConnectedMediaPlayer;
    /**
     * The server disconnected media player.
     */
    private MediaPlayer mServerDisconnectedMediaPlayer;
    /**
     * The take photo media player.
     */
    private MediaPlayer mTakePhotoMediaPlayer;
    /**
     * The start video player.
     */
    private MediaPlayer mStartVideoPlayer;
    /**
     * The stop video media player.
     */
    private MediaPlayer mStopVideoMediaPlayer;
    /**
     * The send video frame media player.
     */
    private MediaPlayer mSendVideoFrameMediaPlayer;
    /**
     * The test packet response media player.
     */
    private MediaPlayer mTestPacketResponseMediaPlayer;
    /**
     * The start interface media player.
     */
    private MediaPlayer mStartInterfaceMediaPlayer;
    /**
     * The stop interface media player.
     */
    private MediaPlayer mStopInterfaceMediaPlayer;
    /**
     * The start mission timer media player.
     */
    private MediaPlayer mStartMissionTimerMediaPlayer;
    /**
     * The buzzer media player.
     */
    private MediaPlayer mBuzzerMediaPlayer;
    /**
     * The sent pyr packet media player.
     */
    private MediaPlayer mSentPyrPacketMediaPlayer;
    /**
     * The service create media player.
     */
    private MediaPlayer mServiceCreateMediaPlayer;
    /**
     * The service destroy media player.
     */
    private MediaPlayer mServiceDestroyMediaPlayer;
    // License:
    /**
     * The floid license:
     */
    private boolean floidLicense = false;
    /**
     * Communications monitor thread execution period in millis
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final long COMMUNICATIONS_MONITOR_EXECUTION_PERIOD = 10;
    /**
     * Accessory Communicator thread execution period in millis
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final long ACCESSORY_COMMUNICATOR_EXECUTION_PERIOD = 10;
    /**
     * Floid Droid thread execution period in millis
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final long FLOID_DROID_EXECUTION_PERIOD = 10;

    /**
     * The default host address.
     */
    public static final String sDefaultHostAddress = "floid.faiglelabs.com";
    /**
     * The default host port.
     */
    public static final int sDefaultHostPort = 6002;
    /**
     * The host address.
     */
    private String mHostAddress = sDefaultHostAddress;
    /**
     * The host port.
     */
    private int mHostPort = sDefaultHostPort;
    /**
     * The server address synchronizer.
     */
    private final Object mServerAddressSynchronizer = new Object();
    /**
     * The has new server address.
     */
    private boolean mHasNewServerAddress = false;
    /**
     * The new host address.
     */
    private String mNewHostAddress = sDefaultHostAddress;
    /**
     * The new host port.
     */
    private int mNewHostPort = sDefaultHostPort;
    /**
     * The communication monitor runnable.
     */
    protected Runnable mCommunicationMonitorRunnable = null;

    /**
     * The floid droid runnable.
     */
    protected FloidDroidRunnable mFloidDroidRunnable = null;
    /**
     * The floid droid thread.
     */
    protected Thread mFloidDroidThread = null;
    // Accessory communicator thread:
    /**
     * The floid accessory communicator.
     */
    protected FloidAccessoryCommunicator mFloidAccessoryCommunicator = null;
    /**
     * The accessory status synchronizer.
     */
    public final Object mAccessoryStatusSynchronizer = new Object();
    /**
     * The new accessory available flag.
     */
    protected boolean mNewAccessoryAvailableFlag = false;
    /**
     * The new accessory.
     */
    protected UsbAccessory mNewAccessory = null;
    /**
     * The close accessory flag.
     */
    protected boolean mCloseAccessoryFlag = false;
    /**
     * Communication thread monitor warning time
     */
    private final long communicationMonitorThreadWarningTime = 5000L;
    /**
     * Communication thread monitor error time
     */
    private final long communicationMonitorThreadErrorTime = 10000L;
    /**
     * Communication thread monitor sleep time
     */
    private final long communicationMonitorSleepTime = 500L;
    /**
     * Floid Service Wi-Fi Lock
     */
    private WifiManager.WifiLock floidServiceWifiLock;
    /**
     * Floid Service Wake Lock
     */
    private PowerManager.WakeLock floidServiceWakeLock;

    // Droid parameters:
    // =================
    /**
     * The droid parameters hash map.
     */
    public final HashMap<String, String> mDroidParametersHashMap = new HashMap<>();
    // Valid Droid parameters:
    /**
     * The constant mDroidDefaultParametersHashMap.
     */
    private static final HashMap<String, String> mDroidDefaultParametersHashMap = new HashMap<>();

    /**
     * Get the Droid Default Parameter Hash Map
     * @return the Droid Default Parameter Hash Map
     */
    public static HashMap<String, String> getDroidDefaultParametersHashMap() { return mDroidDefaultParametersHashMap;}
    /**
     * The constant mDroidParametersValidValues.
     */
    private static final HashMap<String, DroidValidParameters> mDroidParametersValidValues = new HashMap<>();

    /**
     * Get the droid parameters valid values
     *
     * @return the droid parameters valid values
     */
    public static HashMap<String, DroidValidParameters> getDroidParametersValidValues() { return mDroidParametersValidValues;}
    /**
     * The constant DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_OPTION.
     */
    public static final String DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_OPTION = "updaterate.droid_status";
    /**
     * The constant mDroidParametersUpdateRateDroidStatusValidStrings.
     */
    private static final ArrayList<String> mDroidParametersUpdateRateDroidStatusValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_DEFAULT.
     */
    public static final String DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;

    static {
        mDroidParametersUpdateRateDroidStatusValidStrings.add(DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_OPTION);
        mDroidParametersValidValues.put(DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                true,
                100,
                300000,
                1000,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersUpdateRateDroidStatusValidStrings,
                null,
                null,
                false));
    }
    //      -- Floid Status:
    /**
     * The constant DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_OPTION.
     */
    public static final String DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_OPTION = "updaterate.floid_status";
    /**
     * The constant mDroidParametersUpdateRateFloidStatusValidStrings.
     */
    private static final ArrayList<String> mDroidParametersUpdateRateFloidStatusValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_DEFAULT.
     */
    public static final String DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;

    static {
        mDroidParametersUpdateRateFloidStatusValidStrings.add(DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_OPTION);
        mDroidParametersValidValues.put(DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                true,
                100,
                300000,
                1000,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersUpdateRateFloidStatusValidStrings,
                null,
                null,
                false));
    }
    // Model Status:
    /**
     * The constant DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_OPTION.
     */
    public static final String DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_OPTION = "updaterate.model_status";
    /**
     * The constant mDroidParametersUpdateRateModelStatusValidStrings.
     */
    private static final ArrayList<String> mDroidParametersUpdateRateModelStatusValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_DEFAULT.
     */
    public static final String DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;

    static {
        mDroidParametersUpdateRateModelStatusValidStrings.add(DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_DEFAULT);
        mDroidParametersValidValues.put(DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                true,
                100,
                300000,
                60000,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersUpdateRateModelStatusValidStrings,
                null,
                null,
                false));
    }
    // Model Parameters:
    /**
     * The constant DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_OPTION.
     */
    public static final String DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_OPTION = "updaterate.model_parameters";
    /**
     * The constant mDroidParametersUpdateRateModelParametersValidStrings.
     */
    private static final ArrayList<String> mDroidParametersUpdateRateModelParametersValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_DEFAULT.
     */
    public static final String DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;

    static {
        mDroidParametersUpdateRateModelParametersValidStrings.add(DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_DEFAULT);
        mDroidParametersValidValues.put(DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_OPTION, new DroidValidParameters(DroidValidParameters.INTEGER_TYPE,
                true,
                true,
                100,
                300000,
                60000,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                mDroidParametersUpdateRateModelParametersValidStrings,
                null,
                null,
                false));
    }
    // Speak Commands
    /**
     * The constant DROID_PARAMETERS_SPEAK_COMMANDS_OPTION.
     */
    public static final String DROID_PARAMETERS_SPEAK_COMMANDS_OPTION = "speech.speak_commands";
    /**
     * The constant DROID_PARAMETERS_SPEAK_COMMANDS_DEFAULT.
     */
    public static final String DROID_PARAMETERS_SPEAK_COMMANDS_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;  // Leaves it on

    static {
        mDroidParametersValidValues.put(DROID_PARAMETERS_SPEAK_COMMANDS_OPTION, new DroidValidParameters(DroidValidParameters.BOOLEAN_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                true,
                "",
                null,
                null,
                null,
                true));
    }
    // Speak Commands
    /**
     * The constant DROID_PARAMETERS_MISSION_SKIP_CHECK_OPTION.
     */
    public static final String DROID_PARAMETERS_MISSION_SKIP_CHECK_OPTION = "mission.skip_check";
    /**
     * The constant DROID_PARAMETERS_MISSION_SKIP_CHECK_DEFAULT.
     */
    public static final String DROID_PARAMETERS_MISSION_SKIP_CHECK_DEFAULT = DroidValidParameters.BOOLEAN_FALSE;  // Leaves it on

    static {
        mDroidParametersValidValues.put(DROID_PARAMETERS_MISSION_SKIP_CHECK_OPTION, new DroidValidParameters(DroidValidParameters.BOOLEAN_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                "",
                null,
                null,
                null,
                true));
    }
    // Sounds On/Off:
    /**
     * The constant DROID_PARAMETERS_SOUNDS_ON_OPTION.
     */
    public static final String DROID_PARAMETERS_SOUNDS_ON_OPTION = "speech.sounds_on";
    /**
     * The constant DROID_PARAMETERS_SOUNDS_ON_DEFAULT.
     */
    public static final String DROID_PARAMETERS_SOUNDS_ON_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;  // Leaves it on

    static {
        mDroidParametersValidValues.put(DROID_PARAMETERS_SOUNDS_ON_OPTION, new DroidValidParameters(DroidValidParameters.BOOLEAN_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                true,
                "",
                null,
                null,
                null,
                true));
    }
    // Speech locale:
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_OPTION.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_OPTION = "speech.locale";
    /**
     * The constant mDroidParametersSpeechLocaleValidStrings.
     */
    private static final ArrayList<String> mDroidParametersSpeechLocaleValidStrings = new ArrayList<>();
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_DEFAULT.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_DEFAULT = DroidValidParameters.BOOLEAN_DEFAULT;
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_CANADA.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_CANADA = "CANADA";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_CANADA_FRENCH.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_CANADA_FRENCH = "CANADA_FRENCH";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_CHINA.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_CHINA = "CHINA";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_CHINESE.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_CHINESE = "CHINESE";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_ENGLISH.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_ENGLISH = "ENGLISH";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_FRANCE.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_FRANCE = "FRANCE";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_FRENCH.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_FRENCH = "FRENCH";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_GERMAN.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_GERMAN = "GERMAN";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_GERMANY.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_GERMANY = "GERMANY";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_ITALIAN.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_ITALIAN = "ITALIAN";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_ITALY.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_ITALY = "ITALY";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_JAPAN.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_JAPAN = "JAPAN";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_JAPANESE.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_JAPANESE = "JAPANESE";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_KOREA.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_KOREA = "KOREA";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_KOREAN.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_KOREAN = "KOREAN";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_PRC.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_PRC = "PRC";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_ROOT.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_ROOT = "ROOT";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_SIMPLIFIED_CHINESE.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_SIMPLIFIED_CHINESE = "SIMPLIFIED_CHINESE";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_TAIWAN.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_TAIWAN = "TAIWAN";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_TRADITIONAL_CHINESE.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_TRADITIONAL_CHINESE = "TRADITIONAL_CHINESE";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_UK.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_UK = "UK";
    /**
     * The constant DROID_PARAMETERS_SPEECH_LOCALE_US.
     */
    public static final String DROID_PARAMETERS_SPEECH_LOCALE_US = "US";

    static {
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_DEFAULT);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_CANADA);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_CANADA_FRENCH);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_CHINA);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_CHINESE);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_ENGLISH);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_FRANCE);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_FRENCH);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_GERMAN);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_GERMANY);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_ITALIAN);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_ITALY);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_JAPAN);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_JAPANESE);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_KOREA);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_KOREAN);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_PRC);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_ROOT);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_SIMPLIFIED_CHINESE);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_TAIWAN);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_TRADITIONAL_CHINESE);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_UK);
        mDroidParametersSpeechLocaleValidStrings.add(DROID_PARAMETERS_SPEECH_LOCALE_US);
        mDroidParametersValidValues.put(DROID_PARAMETERS_SPEECH_LOCALE_OPTION, new DroidValidParameters(DroidValidParameters.STRING_TYPE,
                false,
                false,
                0,
                0,
                0,
                (float) 0.0,
                (float) 0.0,
                (float) 0.0,
                false,
                DROID_PARAMETERS_SPEECH_LOCALE_DEFAULT,
                mDroidParametersSpeechLocaleValidStrings,
                null,
                null,
                true));
    }

    // Finally, combine all options:
    static {
        // Update rates:
        mDroidDefaultParametersHashMap.put(DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_OPTION, DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_DEFAULT);
        mDroidDefaultParametersHashMap.put(DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_OPTION, DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_DEFAULT);
        mDroidDefaultParametersHashMap.put(DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_OPTION, DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_DEFAULT);
        mDroidDefaultParametersHashMap.put(DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_OPTION, DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_DEFAULT);
        // Speech:
        mDroidDefaultParametersHashMap.put(DROID_PARAMETERS_SPEECH_LOCALE_OPTION, DROID_PARAMETERS_SPEECH_LOCALE_DEFAULT);
        mDroidDefaultParametersHashMap.put(DROID_PARAMETERS_SPEAK_COMMANDS_OPTION, DROID_PARAMETERS_SPEAK_COMMANDS_DEFAULT); // Defaults to true
        mDroidDefaultParametersHashMap.put(DROID_PARAMETERS_MISSION_SKIP_CHECK_OPTION, DROID_PARAMETERS_MISSION_SKIP_CHECK_DEFAULT); // Defaults to false
        mDroidDefaultParametersHashMap.put(DROID_PARAMETERS_SOUNDS_ON_OPTION, DROID_PARAMETERS_SOUNDS_ON_DEFAULT);      // Defaults to true
        /*
            // TODO [DROID PARAMETERS] HAVE A REASONABLE SET OF SIMPLE PARAMETERS FOR FLIGHT ERRORS:
            mDroidDefaultParametersHashMap.put("flight.error.lost_comms",      "LAND,SHUTDOWN");    // TODO [DROID PARAMETERS - SHUTDOWN] - what does this mean in this context?  Lost comms to home? or lost comms to floid - first doesn't matter, second won't be able to do anything about it...
            mDroidDefaultParametersHashMap.put("flight.error.battery",         "HOME,LAND,SHUTDOWN");
            mDroidDefaultParametersHashMap.put("flight.error.gps",             "LAND,SHUTDOWN");
            mDroidDefaultParametersHashMap.put("flight.error.hardware",        "SHUTDOWN");
        */
    }
    /**
     * Floid parameter modified callback map
     */
    private final Map<String, FloidParameterModifiedHandler> floidParameterModifiedHandlerMap = new HashMap<>();

    /**
     * Add a parameter modified handler
     *
     * @param floidParameterName            the parameter name
     * @param floidParameterModifiedHandler the parameter modified handler
     */
    public void addParameterModifiedHandler(String floidParameterName, FloidParameterModifiedHandler floidParameterModifiedHandler) {
        floidParameterModifiedHandlerMap.put(floidParameterName, floidParameterModifiedHandler);
    }

    // Outgoing Floid commands:
    /**
     * The floid outgoing commands synchronizer.
     */
    public final Object mFloidOutgoingCommandsSynchronizer = new Object();
    /**
     * The floid outgoing commands.
     */
    public final ArrayList<FloidOutgoingCommand> mFloidOutgoingCommands = new ArrayList<>();
    // Command Responses:
    /**
     * The floid command response synchronizer.
     */
    public final Object mFloidCommandResponseSynchronizer = new Object();
    /**
     * The floid command responses.
     */
    public final ArrayList<FloidCommandResponse> mFloidCommandResponses = new ArrayList<>();
    // Floid database:
    /**
     * The floid database.
     */
    public SQLiteDatabase mFloidDatabase = null;
    /**
     * Use floid database?
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final boolean mUseFloidDatabase = false; // [PROD] Set to false for prod
    // USB/ADK Stuff:
    /**
     * The constant ACTION_USB_PERMISSION.
     */
    private static final String ACTION_USB_PERMISSION = "com.faiglelabs.floid3.action.USB_PERMISSION";
    /**
     * The usb manager.
     */
    public UsbManager mUsbManager = null;
    /**
     * The permission intent.
     */
    private PendingIntent mPermissionIntent = null;
    /**
     * The permission request pending.
     */
    private boolean mPermissionRequestPending = false;
    /**
     * The accessory.
     */
    public UsbAccessory mAccessory = null;
    /**
     * The file descriptor.
     */
    protected ParcelFileDescriptor mFileDescriptor = null;
    /**
     * The input stream.
     */
    protected FileInputStream mInputStream = null;
    /**
     * The output stream.
     */
    protected FileOutputStream mOutputStream = null;
    // Command numbers and goal ids for Floid:
    /**
     * The floid command number.
     */
    private int mFloidCommandNumber = 0;
    /**
     * The floid goal id.
     */
    private int mFloidGoalId = 0;
    /**
     * The received model parameters.
     */
    public boolean mReceivedModelParameters = false;
    // FloidDroidStatus items:
    /**
     * The floid droid status synchronizer.
     */
    public final Object mFloidDroidStatusSynchronizer = new Object();
    /**
     * The floid droid status.
     */
    public final FloidDroidStatusParcelable mFloidDroidStatus = new FloidDroidStatusParcelable();
    /**
     * The new floid droid status.
     */
    private boolean mNewFloidDroidStatus = false;
    /**
     * The last floid droid status update time.
     */
    private long mLastFloidDroidStatusUpdateTime = 0;
    /**
     * The floid droid goal id.
     */
    private int mFloidDroidGoalID = 0;
    // Debug Message Items:
    /**
     * The floid debug message queue.
     */
    public final Queue<FloidDebugMessageAndTag> mFloidDebugMessageQueue = QueueUtils.synchronizedQueue(new CircularFifoQueue<>());
    // System Log Items:
    /**
     * The floid system log queue.
     */
    public final Queue<FloidSystemLog> mFloidSystemLogQueue = QueueUtils.synchronizedQueue(new CircularFifoQueue<>());
    // FloidStatus items:
    /**
     * The floid status queue.
     */
    public final Queue<FloidStatusParcelable> mFloidStatusQueue = QueueUtils.synchronizedQueue(new CircularFifoQueue<>());
    /**
     * The floid status.
     */
    private FloidStatusParcelable mFloidStatus = new FloidStatusParcelable();
    /**
     * The last floid status update time.
     */
    private long mLastFloidStatusUpdateTime = -1;
    /**
     * The last floid status number.
     */
    public int mLastFloidStatusNumber = -1;
    /**
     * The floid model parameters queue.
     */
    public final Queue<FloidModelParametersParcelable> mFloidModelParametersQueue = QueueUtils.synchronizedQueue(new CircularFifoQueue<>());
    /**
     * The floid model parameters.
     */
    private FloidModelParametersParcelable mFloidModelParameters = new FloidModelParametersParcelable();
    /**
     * The last floid model parameters update time.
     */
    private long mLastFloidModelParametersUpdateTime = -1;
    // FloidModelStatus items:
    /**
     * The floid model status queue.
     */
    public final Queue<FloidModelStatusParcelable> mFloidModelStatusQueue = QueueUtils.synchronizedQueue(new CircularFifoQueue<>());
    /**
     * The floid model status.
     */
    private FloidModelStatusParcelable mFloidModelStatus = new FloidModelStatusParcelable();
    /**
     * The last floid model status update time.
     */
    private long mLastFloidModelStatusUpdateTime = -1;
    // Photo / Video
    /**
     * The photos.
     */
    public final ArrayList<FloidServicePhoto> mPhotos = new ArrayList<>();
    /**
     * Smaller photos to send to UX.
     */
    public final ArrayList<FloidServicePhoto> mPhotosForUx = new ArrayList<>();
    /**
     * The floid photo synchronizer.
     */
    public final Object mFloidPhotoSynchronizer = new Object();
    /**
     * The last photo time.
     */
    public long mLastPhotoTime = 0;
    /**
     * The video frames.
     */
    public final ArrayList<FloidServiceVideoFrame> mVideoFrames = new ArrayList<>();
    /**
     * The video frames.
     */
    public final ArrayList<FloidServiceVideoFrame> mVideoFramesForUx = new ArrayList<>();
    /**
     * The floid video frame synchronizer.
     */
    public final Object mFloidVideoFrameSynchronizer = new Object();
    // Parameters: - These are set in the parameter defaults:
    /**
     * The floid droid status update rate.
     */
    public long mFloidDroidStatusUpdateRate = 1000L;  // 1000l;  // 1000 millisecond update rate // Was 100000
    /**
     * The floid status update rate.
     */
    public long mFloidStatusUpdateRate = 1000L;  // 1000l;  // 1000-millisecond update rate // Was 5000
    /**
     * The floid model status update rate.
     */
    public long mFloidModelStatusUpdateRate = 60000L;  // 60000-millisecond update rate (if no new status received)
    /**
     * The floid model parameters update rate.
     */
    public long mFloidModelParametersUpdateRate = 60000L;  // 60000-millisecond update rate (if no new status received)
    // Container for missions:
    /**
     * The mission stack.
     */
    private final ArrayList<DroidMission> mMissionStack = new ArrayList<>();
    // Current Mission:
    /**
     * The current mission.
     */
    private DroidMission mCurrentMission = null;
    // Instruction Processor:
    /**
     * The current instruction processor.
     */
    private InstructionProcessor mCurrentInstructionProcessor = null;    // An instruction in action
    // Speaker:
    // Text to speech:
    /**
     * The text to speech.
     */
    public TextToSpeech mTextToSpeech = null;
    // Async SpeakerOnCommandProcessor:
    /**
     * The speaker on command processor.
     */
    public SpeakerOnCommandProcessor mSpeakerOnCommandProcessor = null;
    // Mission timer and mission ready fields:
    /**
     * The floid mission timer synchronizer.
     */
    private final Object mFloidMissionTimerSynchronizer = new Object();
    /**
     * The in mission timer mode.
     */
    private boolean mInMissionTimerMode = false;
    /**
     * The new mission is ready.
     */
    private boolean mNewMissionIsReady = false;
    /**
     * The new mission.
     */
    private DroidMission mNewMission = null;
    /**
     * The mission has been cancelled.
     */
    private boolean mMissionCancelled = false;
    /**
     * The cancelled mission.
     */
    private DroidMission mCancelledMission = null;
    /**
     * The mission has been queued.
     */
    private boolean mMissionQueued = false;
    /**
     * The queued mission.
     */
    private DroidMission mQueuedMission = null;
    /**
     * The floid mission ready timer object.
     */
    private FloidMissionReadyTimerObject mFloidMissionReadyTimerObject = null;
    /**
     * Current message number for ux messages from service
     */
    private int currentUxMessageNumber = 1;
    /**
     * Accumulates unsent floid messages.
     */
    String accumulatedUxFloidMessages = "";
    /**
     * Are we quitting - set in onDestroy
     */
    public boolean quitting = false;
    /**
     * Create uuid.
     */
    protected void createUuid() {
        // Set a UUID into the Floid Droid Status:
        mFloidDroidStatus.setFloidUuid(UUID.randomUUID().toString());
    }
    /**
     * The type Floid droid runnable.
     */
    private class FloidDroidRunnable implements Runnable {
        /**
         * The context.
         */
        private final Context mContext;
        /**
         * The floid service.
         */
        private final FloidService mFloidService;
        /**
         * The floid droid start time.
         */
        private final long mFloidDroidStartTime;
        /**
         * The server communicator.
         */
        private ClientCommunicator mServerCommunicator;
        /**
         * The floid database.
         */
        @SuppressWarnings("FieldCanBeLocal")
        private final SQLiteDatabase mFloidDatabase;
        /**
         * The droid runnable loop count.
         */
        private long mDroidRunnableLoopCount = 0L;
        /**
         * The droid status number.
         */
        private long mDroidStatusNumber = 0L;
        /**
         * The running indicator time.
         */
        @SuppressWarnings("FieldCanBeLocal")
        private long mRunningIndicatorTime;
        /**
         * The running indicator period.
         */
        private static final long mRunningIndicatorPeriod = 30000L;   // Pop a log message every 5 seconds to show thread is running...
        /**
         * The droid status indicator time.
         */
        @SuppressWarnings("FieldCanBeLocal")
        private long mDroidStatusIndicatorTime;
        /**
         * The droid status indicator period.
         */
        private static final long mDroidStatusIndicatorPeriod = 3000L;   // Update the ux with the droid status this often at minimum
        /**
         * Ignore delay processing if packet sent
         */
        final boolean IGNORE_DELAY_PROCESSING_ON_SENT_PACKET = true; // [PROD] Set to true for prod
        /**
         * Maximum processing loops per each type
         */
        final int MAX_PROCESSING_ITERATIONS_PER_TYPE = 5;
        /**
         * The constant TAG.
         */
        public static final String TAG = "FloidDroidRunnable";

        private final boolean floidLicense;

        /**
         * Get loop count for droid runnable
         * @return the loop count for the droid runnable
         */
        public long getDroidRunnableLoopCount() {
            return mDroidRunnableLoopCount;
        }
        /**
         * Get droid status number
         * @return the current droid status number
         */
        @SuppressWarnings("unused")
        public long getDroidStatusNumber() {
            return mDroidStatusNumber;
        }

        /**
         * @return true if floid licensed
         */
        @SuppressWarnings("unused")
        public boolean isFloidLicense() {
            return floidLicense;
        }

        private long getPacketsToServerCount() {
            return mAdditionalStatus.get(AdditionalStatus.PACKETS_TO_SERVER) + mAdditionalStatus.get(AdditionalStatus.BAD_PACKETS_TO_SERVER);
        }

        /**
         * Instantiates a new Floid droid runnable.
         *
         * @param context       the context
         * @param floidService  the floid service
         * @param floidDatabase the floid database
         */
        public FloidDroidRunnable(Context context, FloidService floidService, SQLiteDatabase floidDatabase, boolean floidLicense) {
            super();
            mContext = context;
            this.floidLicense = floidLicense;
            mFloidService = floidService;
            mFloidDatabase = floidDatabase;
            mFloidDroidStartTime = (new Date()).getTime();
            // Create a new uuid:
            createUuid();
            // Indicator messages:
            mRunningIndicatorTime = SystemClock.uptimeMillis();
            mDroidStatusIndicatorTime = SystemClock.uptimeMillis();
            if (mFloidService.mUseLogger) Log.i(TAG, "Floid Droid Runnable Starting");
            // Send a message to the interface also...
            uxSendFloidMessageToDisplay("Starting FloidDroidRunnable\n");
            // First time through we perform a reset:
            resetFloidDroidState();
            mServerCommunicator = null;
        }

        @Override
        @SuppressWarnings("StatementWithEmptyBody")
        public void run() {
            if (mServerCommunicator == null) {
                // Open up a communication to the host:
                mServerCommunicator = new ClientCommunicator(mContext, mFloidService, mHostAddress, mHostPort);
            }
            // Increase the loop count:
            mDroidRunnableLoopCount++;
            long droidRunnableStartingPacketsToServerCount = getPacketsToServerCount();
            // Get the time:
            long currentTime = SystemClock.uptimeMillis();
            // Pop running indicator message if time:
            if (currentTime - mRunningIndicatorTime > mRunningIndicatorPeriod) {
                mRunningIndicatorTime = currentTime;
                if (mFloidService.mUseLogger) Log.i(TAG, "Floid Droid Runnable: Alive");
            }
            try {
                // Set the floid connected status in the floid droid status:
                mFloidDroidStatus.setFloidConnected(mInputStream != null);
                // 0. If we have a new server address, use it:
                if (mHasNewServerAddress) {
                    synchronized (mServerAddressSynchronizer) {
                        mHostAddress = mNewHostAddress;
                        mHostPort = mNewHostPort;
                        mHasNewServerAddress = false;  // We have absorbed this address...
                    }
                    mServerCommunicator.clear();
                    mServerCommunicator = new ClientCommunicator(mContext, mFloidService, mHostAddress, mHostPort);
                }
                int currentProcessingIterations;
                // 1. Photo:
                try {
                    currentProcessingIterations = 0;
                    while (processPhoto() && (++currentProcessingIterations < MAX_PROCESSING_ITERATIONS_PER_TYPE)) ;
                } catch (Exception e) {
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processPhoto", e);
                }
                // 2. Video
                if (getPacketsToServerCount() == droidRunnableStartingPacketsToServerCount || IGNORE_DELAY_PROCESSING_ON_SENT_PACKET) {
                    try {
                        currentProcessingIterations = 0;
                        while (processVideoFrames() && (++currentProcessingIterations < MAX_PROCESSING_ITERATIONS_PER_TYPE)) ;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processVideo", e);
                    }
                }
                // 3. FloidStatus:
                if (getPacketsToServerCount() == droidRunnableStartingPacketsToServerCount || IGNORE_DELAY_PROCESSING_ON_SENT_PACKET) {
                    try {
                        currentProcessingIterations = 0;
                        while (processFloidStatus(currentTime) && (++currentProcessingIterations < MAX_PROCESSING_ITERATIONS_PER_TYPE)) ;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processFloidStatus", e);
                    }
                }
                // 4. FloidModelStatus:
                if (getPacketsToServerCount() == droidRunnableStartingPacketsToServerCount || IGNORE_DELAY_PROCESSING_ON_SENT_PACKET) {
                    try {
                        currentProcessingIterations = 0;
                        while (processFloidModelStatus(currentTime) && (++currentProcessingIterations < MAX_PROCESSING_ITERATIONS_PER_TYPE)) ;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processModelStatus", e);
                    }
                }
                // 5. FloidModelParameters:
                if (getPacketsToServerCount() == droidRunnableStartingPacketsToServerCount || IGNORE_DELAY_PROCESSING_ON_SENT_PACKET) {
                    try {
                        currentProcessingIterations = 0;
                        while (processFloidModelParameters(currentTime) && (++currentProcessingIterations < MAX_PROCESSING_ITERATIONS_PER_TYPE)) ;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processModelParameters", e);
                    }
                }
                // 6. Debug Messages:
                if (getPacketsToServerCount() == droidRunnableStartingPacketsToServerCount || IGNORE_DELAY_PROCESSING_ON_SENT_PACKET) {
                    try {
                        currentProcessingIterations = 0;
                        while (processDebugMessages(currentTime) && (++currentProcessingIterations < MAX_PROCESSING_ITERATIONS_PER_TYPE)) ;
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processDebugMessages", e);
                    }
                }
                // 6a. Process System Logs:
                try {
                    processSystemLogs(currentTime);
                } catch (Exception e) {
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processDebugMessages", e);
                }
                // 7. FloidDroidStatus:
                // We always process the status:
                try {
                    processFloidDroidStatus(currentTime);
                } catch (Exception e) {
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processFloidDroidStatus", e);
                }
                // 8. See if we have a new mission from the interface and if so clean up the stack and current instruction processor etc.
                try {
                    processCheckIfMissionReady();  // Checks to see if we have a proper mission from the interface that is ready to go...
                } catch (Exception e) {
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processCheckIfMissionReady", e);
                }
                // 9. process:
                try {
                    process();
                } catch (Exception e) {
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception in process", e);
                }
                // 10. Update our status display if time:
                try {
                    // Re-Get the time:
                    currentTime = SystemClock.uptimeMillis();
                    // Update ux if time:
                    if (currentTime - mDroidStatusIndicatorTime > mDroidStatusIndicatorPeriod) {
                        mDroidStatusIndicatorTime = currentTime;
                        mDroidStatusNumber++;
                        uxSendFloidDroidStatus();
                    }
                } catch (Exception e) {
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception in sendFloidDroidStatus", e);
                }
                // 11. Update additional status if it is dirty:
                if (mAdditionalStatus.isDirty()) {
                    mAdditionalStatus.clearDirty();
                    uxSendAdditionalStatus();
                }
            } catch (Exception e) {
                if (mFloidService.mUseLogger) Log.e(TAG, "Exception in main thread: " + e.getMessage(), e);
            }
            try {
                if (mFloidThreadTimings.getMainThreadSleepTime() != 0L) {
                    Thread.sleep(mFloidThreadTimings.getMainThreadSleepTime());
                }
            } catch (InterruptedException interruptedException) {
                // Do nothing
            }
        }
        /**
         * Process check if mission ready.
         */
        private void processCheckIfMissionReady() {
            boolean ready = false;
            boolean cancelled = false;
            boolean queued = false;
            synchronized (mFloidMissionTimerSynchronizer) {
                // 1. Check our missionReady flag
                if (mNewMissionIsReady) {
                    ready = true;
                    mNewMissionIsReady = false;
                } else if(mMissionCancelled) {
                    cancelled = true;
                    mMissionCancelled = false;
                } else if(mMissionQueued) {
                    queued = true;
                    mMissionQueued = false;
                }
            }
            if(ready) {
                // Tear down any current missions, instructions and clear mission stack
                // NOTE: For some current situations we may want to act differently in the future, such as explicit GOTO Labels, ABORTs, etc, however this is a mission and came from the timer interface so we may not need to make changes here
                explicitCurrentMissionAndInstructionTearDown();
                // Add or mission (will get picked up in process next...)
                mMissionStack.add(mNewMission);
                // Turn off our mission ready flags:
                mNewMission = null;
            }
            if(queued) {
                // Send a message that we are queueing the mission:
                try {
                    FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = new FloidDroidMissionInstructionStatusMessage();
                    floidDroidMissionInstructionStatusMessage.setMessageType(FloidCommands.FLOID_MESSAGE_PACKET_MISSION_QUEUED);
                    if (mQueuedMission != null) {
                        if (mQueuedMission.getId() != null) {
                            floidDroidMissionInstructionStatusMessage.setMissionId(mQueuedMission.getId());
                        } else {
                            floidDroidMissionInstructionStatusMessage.setMissionId(NO_MISSION_ID);
                        }
                        if (mQueuedMission.getMissionName() != null) {
                            floidDroidMissionInstructionStatusMessage.setMissionName(mQueuedMission.getMissionName());
                        } else {
                            floidDroidMissionInstructionStatusMessage.setMissionName("");
                        }
                        floidDroidMissionInstructionStatusMessage.setInstructionId(mQueuedMission.getId());
                        floidDroidMissionInstructionStatusMessage.setInstructionType(mQueuedMission.getType());
                    } else {
                        floidDroidMissionInstructionStatusMessage.setMissionId(NO_MISSION_ID);
                        floidDroidMissionInstructionStatusMessage.setMissionName("");
                        floidDroidMissionInstructionStatusMessage.setInstructionId(NO_INSTRUCTION_ID);
                        floidDroidMissionInstructionStatusMessage.setInstructionType("");
                    }
                    floidDroidMissionInstructionStatusMessage.setInstructionStatus(ProcessBlock.PROCESS_BLOCK_NOT_STARTED);
                    if (mQueuedMission == null) {
                        floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Mission<-Queued=NoQueuedMission");
                    } else {
                        floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Mission<-Queued=" + mQueuedMission.getMissionName());
                    }
                    mFloidDroidRunnable.sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage);
                } catch(Exception e) {
                    Log.e(TAG, "Caught Exception queueing mission", e);
                }
                mQueuedMission = null;
            }
            if(cancelled) {
                // Send a message that we are cancelling the mission:
                try {
                    FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = new FloidDroidMissionInstructionStatusMessage();
                    floidDroidMissionInstructionStatusMessage.setMessageType(FloidCommands.FLOID_MESSAGE_PACKET_MISSION_CANCELLED);
                    if (mCancelledMission != null) {
                        if (mCancelledMission.getId() != null) {
                            floidDroidMissionInstructionStatusMessage.setMissionId(mCancelledMission.getId());
                            floidDroidMissionInstructionStatusMessage.setInstructionId(mCancelledMission.getId());
                            floidDroidMissionInstructionStatusMessage.setInstructionType(mCancelledMission.getType());
                        } else {
                            floidDroidMissionInstructionStatusMessage.setMissionId(NO_MISSION_ID);
                            floidDroidMissionInstructionStatusMessage.setInstructionId(NO_INSTRUCTION_ID);
                            floidDroidMissionInstructionStatusMessage.setInstructionType("");                        }
                        if (mCancelledMission.getMissionName() != null) {
                            floidDroidMissionInstructionStatusMessage.setMissionName(mCancelledMission.getMissionName());
                        } else {
                            floidDroidMissionInstructionStatusMessage.setMissionName("");
                        }
                    } else {
                        floidDroidMissionInstructionStatusMessage.setMissionId(NO_MISSION_ID);
                        floidDroidMissionInstructionStatusMessage.setMissionName("");
                        floidDroidMissionInstructionStatusMessage.setInstructionType("");
                    }
                    floidDroidMissionInstructionStatusMessage.setInstructionStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_CANCELLED);
                    if (mCancelledMission == null) {
                        floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Mission<-Cancelled=NoQueuedMission");
                    } else {
                        floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Mission<-Cancelled=" + mCancelledMission.getMissionName());
                    }
                    mFloidDroidRunnable.sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage);
                } catch(Exception e) {
                    Log.e(TAG, "Caught Exception cancelling mission", e);
                }
                mCancelledMission = null;
            }
        }


        /**
         * Process floid client message from photo send.
         *
         * @param floidClientMessage the floid client message
         */
        @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
        private void processFloidClientMessageFromPhotoSend(FloidClientMessage floidClientMessage) {
            // Does nothing...
        }
        /**
         * Process floid client message from video send.
         *
         * @param floidClientMessage the floid client message
         */
        @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
        private void processFloidClientMessageFromVideoSend(FloidClientMessage floidClientMessage) {
            // Does nothing...
        }

        /**
         * Process video preview frames.
         */
        private boolean processVideoFrames() {
            boolean processedVideoFrame = false;
            // Send any videos we have:
            byte[] videoFrameBytes;
            byte[] videoFrameBytesForUx;
            try {
                do {
                    FloidServiceVideoFrame floidServiceVideoFrame = null;
                    FloidServiceVideoFrame floidServiceVideoFrameForUx = null;
                    videoFrameBytes = null;  // Make sure we empty the video bytes
                    videoFrameBytesForUx = null;

                    // Sends a video if one is available and gets a response...
                    synchronized (mFloidVideoFrameSynchronizer) {
                        if (!mVideoFrames.isEmpty()) {
                            // We have a video, get it and send it:
                            floidServiceVideoFrame = mVideoFrames.get(0);
                            mVideoFrames.remove(0);
                            floidServiceVideoFrameForUx = mVideoFramesForUx.get(0);
                            mVideoFramesForUx.remove(0);
                            videoFrameBytes = floidServiceVideoFrame.getImageBytes();
                            videoFrameBytesForUx = floidServiceVideoFrameForUx.getImageBytes();
                            processedVideoFrame = true;
                        }
                    }
                    // Done part that needed synchronization:
                    if ((floidServiceVideoFrame != null) && (videoFrameBytes != null) && (videoFrameBytesForUx != null)) {
                        // Format and send the video preview frame bytes:
                        uxSendVideoFrame(videoFrameBytesForUx);
                        // Create an image message for the video frame:
                        // Alternately choose the full frame or the frame for the UX (UX is smaller one):
                        // Full Frame:
//                        FloidServerMessage.FloidServerImageMessage photoFloidServerImageMessage = createFloidServerImageMessage(floidServiceVideoFrame);
                        // UX Frame
                        FloidServerMessage.FloidServerImageMessage photoFloidServerImageMessage = createFloidServerImageMessage(floidServiceVideoFrameForUx);
                        // And then create a video frame message:
                        FloidServerMessage floidServerMessage = FloidServerMessageUtils.createImageFloidServerMessage(
                                mFloidDroidStatus,
                                FloidCommands.FLOID_MESSAGE_PACKET_VIDEO_FRAME,
                                FloidCommands.FLOID_MESSAGE_PACKET_VIDEO_FRAME,
                                photoFloidServerImageMessage);
                        try {
                            // Send the Floid Status in a Floid Server Message, get a Floid Client Message back:
                            if (mFloidService.mUseLogger) Log.i(TAG, "Service -> Server: Floid Video Frame");
                            if(floidServerMessage != null) {
                                byte[] floidClientMessageBytes = mServerCommunicator.sendFloidServerMessage(floidServerMessage);
                                if(floidClientMessageBytes != null) {
                                    try {
                                        FloidClientMessage floidClientMessage = FloidClientMessage.parseFrom(floidClientMessageBytes);
                                        processFloidClientMessageFromVideoSend(floidClientMessage);
                                    } catch (Exception e) {
                                        if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Floid Client Message from Video Frame", e);
                                    }
                                } else {
                                    mAdditionalStatus.increment(AdditionalStatus.NULL_RETURN_FROM_SERVER);
                                }
                            } else {
                                Log.e(TAG, "Empty Floid Server Message in processVideoFrames");
                            }
                        } catch (Exception e) {
                            if (mFloidService.mUseLogger) Log.w(TAG, "Failed to send Video Frame: " + e.getMessage());
                        }
                    }
                } while (videoFrameBytes != null);
            } catch (Exception e) {
                Log.e(TAG, "processVideoFrames exception: ", e);
            }
            return processedVideoFrame;
        }

        /**
         * Process floid status.
         *
         * @param currentTime the current time
         */
        private boolean processFloidStatus(long currentTime) {
            // Pull a new floid model status if one in queue:
            boolean newFloidStatus = false;
            synchronized (mFloidStatusQueue) {
                if (!mFloidStatusQueue.isEmpty()) {
                    if(mFloidStatusQueue.size()>1) {
                        Log.i(TAG, "FloidStatusQueue-- " + mFloidStatusQueue.size() + "->" + (mFloidStatusQueue.size() - 1));
                    }
                    mFloidStatus = mFloidStatusQueue.remove();
                    mFloidDroidGoalID = mFloidStatus.getGid();
                    // Update the floid droid mode also where needed:
                    if ((mFloidStatus != null) && (mFloidDroidStatus != null)) {
                        // Set the mode:
                        if (mFloidStatus.getFm() != mFloidDroidStatus.getCurrentMode()) {
                            setFloidDroidMode(mFloidStatus.getFm());
                        }
                        // Set the helis on/off state from the hardware:
                        // Note: in the cpp, 'helisOn' is dh and only set once the entire start up helis sequence is complete:
                        mFloidDroidStatus.setHelisStarted(mFloidStatus.isDh());
                    }
                    newFloidStatus = true;
                }
            }
            if (newFloidStatus || ((currentTime - mLastFloidStatusUpdateTime) >= mFloidStatusUpdateRate)) {
                try {
                    if(mFloidStatus!=null) {
                        FloidServerMessage floidServerMessage = FloidServerMessageUtils.createStandardFloidServerMessage(mFloidDroidStatus, mFloidStatus.getType(), FloidCommands.FLOID_MESSAGE_PACKET_FLOID_STATUS, mFloidStatus.toJSON().toString());
                        // Send the Floid Status in a Floid Server Message, get a Floid Client Message back:
                        if (mFloidService.mUseLogger) Log.i(TAG, "Service -> Server: Floid Status");
                        if (floidServerMessage != null) {
                            byte[] floidClientMessageBytes = mServerCommunicator.sendFloidServerMessage(floidServerMessage);
                            if (floidClientMessageBytes != null) {
                                try {
                                    FloidClientMessage floidClientMessage = FloidClientMessage.parseFrom(floidClientMessageBytes);
                                    processFloidClientMessageFromFloidStatus(floidClientMessage);
                                } catch (Exception e) {
                                    if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Floid Client Message from Floid Status", e);
                                }
                            } else {
                                mAdditionalStatus.increment(AdditionalStatus.NULL_RETURN_FROM_SERVER);
                            }
                        } else {
                            Log.e(TAG, "Empty Floid Server Message in processFloidStatus");
                        }
                    } else {
                        Log.e(TAG, "Empty FloidStatus in processFloidStatus");
                    }
                } catch (Exception e2) {
                    Log.e(TAG, "Caught Exception", e2);
                }
                mLastFloidStatusUpdateTime = currentTime;
                mLastFloidStatusNumber++;
                return true;
            }
            return false;
        }

        /**
         * Process floid model status.
         *
         * @param currentTime the current time
         */
        private boolean processFloidModelStatus(long currentTime) {
            // Pull a new floid model status if one in queue:
            boolean newFloidModelStatus = false;
            synchronized (mFloidModelStatusQueue) {
                if (!mFloidModelStatusQueue.isEmpty()) {
                    if(mFloidModelStatusQueue.size()>1) {
                        Log.i(TAG, "FloidModelStatusQueue-- " + mFloidModelStatusQueue.size() + "->" + (mFloidModelStatusQueue.size() - 1));
                    }
                    mFloidModelStatus = mFloidModelStatusQueue.remove();
                    newFloidModelStatus = true;
                }
            }
            // Process the model status
            if (newFloidModelStatus || ((currentTime - mLastFloidModelStatusUpdateTime) >= mFloidModelStatusUpdateRate)) {
                try {
                    FloidServerMessage floidServerMessage = FloidServerMessageUtils.createStandardFloidServerMessage(mFloidDroidStatus, mFloidModelStatus.getType(), FloidCommands.FLOID_MESSAGE_PACKET_MODEL_STATUS, mFloidModelStatus.toJSON().toString());
                    if (floidServerMessage != null) {
                        byte[] floidClientMessageBytes = mServerCommunicator.sendFloidServerMessage(floidServerMessage);
                        if (floidClientMessageBytes != null) {
                            try {
                                FloidClientMessage floidClientMessage = FloidClientMessage.parseFrom(floidClientMessageBytes);
                                processFloidClientMessageFromFloidModelStatus(floidClientMessage);
                            } catch (Exception e) {
                                if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Floid Client Message from Floid Model Status", e);
                            }
                        } else {
                            mAdditionalStatus.increment(AdditionalStatus.NULL_RETURN_FROM_SERVER);
                        }
                    } else {
                        Log.e(TAG, "Empty Floid Server Message in processFloidModelStatus");
                    }
                } catch (Exception e2) {
                    Log.e(TAG, "Caught exception processing Floid Model Status", e2);
                }
                mLastFloidModelStatusUpdateTime = currentTime;
                newFloidModelStatus = true;
            }
            return newFloidModelStatus;
        }

        /**
         * Process floid model parameters.
         *
         * @param currentTime the current time
         */
        private boolean processFloidModelParameters(long currentTime) {
            // Pull a new floid model status if one in queue:
            boolean newFloidModelParameters = false;
            synchronized (mFloidModelParametersQueue) {
                if (!mFloidModelParametersQueue.isEmpty()) {
                    if(mFloidModelParametersQueue.size()>1) {
                        Log.i(TAG, "FloidModelParametersQueue-- " + mFloidModelParametersQueue.size() + "->" + (mFloidModelParametersQueue.size() - 1));
                    }
                    mFloidModelParameters = mFloidModelParametersQueue.remove();
                    newFloidModelParameters = true;
                }
            }
            if (newFloidModelParameters || ((currentTime - mLastFloidModelParametersUpdateTime) >= mFloidModelParametersUpdateRate)) {
                try {
                    if(!newFloidModelParameters) {
                        // We are resending the same object, so it needs a new status number:
                        mFloidModelParameters.setFloidMessageNumber(FloidMessage.getNextFloidMessageNumber());
                    }
                    FloidServerMessage floidServerMessage = FloidServerMessageUtils.createStandardFloidServerMessage(mFloidDroidStatus, mFloidModelParameters.getType(), FloidCommands.FLOID_MESSAGE_PACKET_MODEL_PARAMETERS, mFloidModelParameters.toJSON().toString());
                    if (floidServerMessage != null) {
                        byte[] floidClientMessageBytes = mServerCommunicator.sendFloidServerMessage(floidServerMessage);
                        if (floidClientMessageBytes != null) {
                            try {
                                FloidClientMessage floidClientMessage = FloidClientMessage.parseFrom(floidClientMessageBytes);
                                processFloidClientMessageFromFloidModelParameters(floidClientMessage);
                            } catch (Exception e) {
                                if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Floid Client Message from Floid Model Parameters", e);
                            }
                        } else {
                            mAdditionalStatus.increment(AdditionalStatus.NULL_RETURN_FROM_SERVER);
                        }
                    } else {
                        Log.e(TAG, "Empty Floid Server Message in processFloidModelParameters");
                    }
                } catch (Exception e2) {
                    Log.e(TAG, "Caught exception processing Floid Model Parameters", e2);
                }
                mLastFloidModelParametersUpdateTime = currentTime;
                newFloidModelParameters = true;
            }
            return newFloidModelParameters;
        }

        /**
         * Process floid client message received from sending floid status.
         *
         * @param floidClientMessage the floid client message
         */
        @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
        private void processFloidClientMessageFromFloidStatus(FloidClientMessage floidClientMessage) {
            // Does nothing
        }

        /**
         * Process floid client message received from sending floid model status.
         *
         * @param floidClientMessage the floid client message
         */
        @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
        private void processFloidClientMessageFromFloidModelStatus(FloidClientMessage floidClientMessage) {
            // Does nothing
        }

        /**
         * Process floid client message received from sending floid model parameters.
         *
         * @param floidClientMessage the floid client message
         */
        @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
        private void processFloidClientMessageFromFloidModelParameters(FloidClientMessage floidClientMessage) {
            // Does nothing
        }

        /**
         * Process debug messages.
         *
         * @param currentTime the current time
         */
        @SuppressWarnings("UnusedParameters")
        private boolean processDebugMessages(long currentTime) {
            boolean processedDebugMessage = false;
            // Pull a new debug message if one in queue:
            ArrayList<FloidDebugMessageAndTag> floidDebugMessageAndTags = new ArrayList<>();
            synchronized (mFloidDebugMessageQueue) {
                while (!mFloidDebugMessageQueue.isEmpty()) {
                    // Log.i(TAG, "FloidDebugMessageQueue-- " + mFloidDebugMessageQueue.size() + "->" + (mFloidDebugMessageQueue.size()-1) );
                    floidDebugMessageAndTags.add(mFloidDebugMessageQueue.remove());
                    processedDebugMessage = true;
                }
            }
            // Put all the debug messages together and send:
            if (floidDebugMessageAndTags.size() > 0) {
                StringBuilder debugMessageStringBuilder = new StringBuilder();
                for (FloidDebugMessageAndTag floidDebugMessageAndTag : floidDebugMessageAndTags) {
                    if (floidDebugMessageAndTag != null) {
                        debugMessageStringBuilder.append(floidDebugMessageAndTag.getMessage());
                        if (floidDebugMessageAndTag.getMessage().charAt(floidDebugMessageAndTag.getMessage().length() - 1) != '\n') {
                            debugMessageStringBuilder.append("\n");
                        }
                    }
                }
                try {
                    FloidServerMessage floidServerMessage = FloidServerMessageUtils.createStandardFloidServerMessage(mFloidDroidStatus, FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, debugMessageStringBuilder.toString());
                    // Send the Debug Message JSON, get a return string...
                    if (mFloidService.mUseLogger) Log.i(TAG, "Service -> Server: Debug Message");
                    if (floidServerMessage != null) {
                        byte[] floidClientMessageBytes = mServerCommunicator.sendFloidServerMessage(floidServerMessage);
                        if (floidClientMessageBytes != null) {
                            try {
                                FloidClientMessage floidClientMessage = FloidClientMessage.parseFrom(floidClientMessageBytes);
                                processFloidClientMessageFromDebugMessage(floidClientMessage);
                            } catch (Exception e) {
                                if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Floid Client Message from Process Debug Messages", e);
                            }
                        } else {
                            mAdditionalStatus.increment(AdditionalStatus.NULL_RETURN_FROM_SERVER);
                        }
                    } else {
                        Log.e(TAG, "Empty Floid Server Message in processDebugMessages");
                    }
                } catch (Exception e) {
                    if (mFloidService.mUseLogger)
                        Log.w(TAG, "Failed to send Debug Message - clearing: " + e.getMessage());
                }
            }
            return processedDebugMessage;
        }

        /**
         * Process system logs.
         *
         * @param currentTime the current time
         */
        @SuppressWarnings("UnusedParameters")
        private boolean processSystemLogs(long currentTime) {
            boolean processedSystemLogs = false;
            // Pull a new system log if one in queue:
            ArrayList<FloidSystemLog> floidSystemLogs = new ArrayList<>();
            synchronized (mFloidSystemLogQueue) {
                while (!mFloidSystemLogQueue.isEmpty()) {
                    // Log.i(TAG, "mFloidSystemLogQueue-- " + mFloidSystemLogQueue.size() + "->" + (mFloidSystemLogQueue.size()-1) );
                    floidSystemLogs.add(mFloidSystemLogQueue.remove());
                    processedSystemLogs = true;
                }
            }
            // For each log we have, send it:
            floidSystemLogs.forEach(floidSystemLog -> {
                try {
                    FloidServerMessage floidServerMessage = FloidServerMessageUtils.createStandardFloidServerMessage(mFloidDroidStatus, FloidCommands.FLOID_MESSAGE_PACKET_SYSTEM_LOG, FloidCommands.FLOID_MESSAGE_PACKET_SYSTEM_LOG, floidSystemLog.getSystemLog());
                    // Send the Debug Message JSON, get a return string...
                    if (mFloidService.mUseLogger) Log.i(TAG, "Service -> Server: System Log");
                    if (floidServerMessage != null) {
                        byte[] floidClientMessageBytes = mServerCommunicator.sendFloidServerMessage(floidServerMessage);
                        if (floidClientMessageBytes != null) {
                            try {
                                FloidClientMessage floidClientMessage = FloidClientMessage.parseFrom(floidClientMessageBytes);
                                processFloidClientMessageFromSystemLog(floidClientMessage);
                            } catch (Exception e) {
                                if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Floid Client Message from Process System Log", e);
                            }
                        } else {
                            mAdditionalStatus.increment(AdditionalStatus.NULL_RETURN_FROM_SERVER);
                        }
                    } else {
                        Log.e(TAG, "Empty Floid Server Message in processSystemLogs");
                    }
                } catch (Exception e) {
                    if (mFloidService.mUseLogger)
                        Log.w(TAG, "Failed to send System Log Message - clearing: " + e.getMessage());
                }
            });
            return processedSystemLogs;
        }

        /**
         * Process floid client message from debug message.
         *
         * @param floidClientMessage the floid client message
         */
        @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
        private void processFloidClientMessageFromDebugMessage(FloidClientMessage floidClientMessage) {
            // Does nothing for now!
        }

        /**
         * Process floid client message from system log.
         *
         * @param floidClientMessage the floid client message
         */
        @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
        private void processFloidClientMessageFromSystemLog(FloidClientMessage floidClientMessage) {
            // Does nothing for now!
        }

        /**
         * Process floid droid status.
         *
         * @param currentTime the current time
         */
        @SuppressWarnings("UnusedReturnValue")
        public boolean processFloidDroidStatus(long currentTime) {
            boolean processedFloidDroidStatus = false;
            if (mNewFloidDroidStatus || ((currentTime - mLastFloidDroidStatusUpdateTime) >= mFloidDroidStatusUpdateRate)) {
                processedFloidDroidStatus = true;
                try {
                    FloidServerMessage floidServerMessage;
                    String floidDroidStatusJSONString;
                    // Synchronize this to read the value but then free up:
                    synchronized (mFloidDroidStatusSynchronizer) {
                        // Set status number and time:
                        mFloidDroidStatus.setStatusNumber(mFloidDroidStatus.getStatusNumber() + 1);
                        mFloidDroidStatus.setStatusTime((new Date()).getTime() - mFloidDroidStartTime);
                        mNewFloidDroidStatus = false;
                        mLastFloidDroidStatusUpdateTime = currentTime;
                        try {
                            floidDroidStatusJSONString = mFloidDroidStatus.toJSON().toString();
                            floidServerMessage = FloidServerMessageUtils.createStandardFloidServerMessage(mFloidDroidStatus, mFloidDroidStatus.getType(), FloidCommands.FLOID_MESSAGE_PACKET_FLOID_DROID_STATUS, floidDroidStatusJSONString);
                        } catch (Exception e2) {
                            if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Floid Droid Status", e2);
                            floidDroidStatusJSONString = "";
                            floidServerMessage = null;
                        }
                    }
                    if(mFloidService.mFloidDatabase!=null) {
                        try {
                            ContentValues insertContentValues = new ContentValues();
                            // Write the message out to the database:
                            insertContentValues.put(FloidCommands.PARAMETER_FLOID_ID, Integer.toString(mFloidDroidStatus.getFloidId()));
                            insertContentValues.put(FloidCommands.PARAMETER_FLOID_UUID, mFloidDroidStatus.getFloidUuid());
                            insertContentValues.put(FloidCommands.PARAMETER_STATUS_NUMBER, mFloidDroidStatus.getStatusNumber());
                            insertContentValues.put(FloidCommands.FLOID_DROID_STATUS_TABLE_KEY_NAME, floidDroidStatusJSONString);
                            mFloidService.mFloidDatabase.insert(FloidCommands.FLOID_DROID_STATUS_TABLE_NAME, null, insertContentValues);
                        } catch (Exception e) {
                            // Failed to insert into database:
                            if (mFloidService.mUseLogger) Log.w(TAG, "Failed to insert Droid Status into database: " + e.getMessage());
                        }
                    }
                    // Send the FloidDroidStatus JSON, get a return string...
                    if (mFloidService.mUseLogger) Log.v(TAG, "Service -> Server: Floid Droid Status");
                    if(floidServerMessage != null) {
                        byte[] floidClientMessageBytes = mServerCommunicator.sendFloidServerMessage(floidServerMessage);
                        if(floidClientMessageBytes != null) {
                            try {
                                FloidClientMessage floidClientMessage = FloidClientMessage.parseFrom(floidClientMessageBytes);
                                processFloidClientMessageFromFloidDroidStatus(floidClientMessage);
                            } catch (Exception e) {
                                if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Floid Client Message from Floid Droid Status", e);
                            }
                        } else {
                            mAdditionalStatus.increment(AdditionalStatus.NULL_RETURN_FROM_SERVER);
                        }
                    } else {
                        Log.e(TAG, "Empty Floid Server Message in processFloidDroidStatus");
                    }
                } catch (Exception e) {
                    // Failed to send
                    if (mFloidService.mUseLogger) Log.w(TAG, "Failed to send Droid Status: " + e.getMessage());
                }
            }
            return processedFloidDroidStatus;
        }

        /**
         * Process photo.
         */
        private boolean processPhoto() {
            boolean processedPhoto = false;
            // Check the see if we need to take an automatic photo:
            if (mFloidDroidStatus.isPhotoStrobe()) {
                // Is our delay up?
                if ((System.currentTimeMillis() - mLastPhotoTime) >= (mFloidDroidStatus.getPhotoStrobeDelay() * 1000L)) {
                    mLastPhotoTime = System.currentTimeMillis(); // Note: Take photo actual will set this to a closer time when it runs - this is to stop re-entry of this code while that gets set up
                    serviceSendTakePhoto();
                }
            }
            // Send any photos we have:
            byte[] photoBytes;
            byte[] photoBytesForUx;
            do {
                FloidServicePhoto floidServicePhoto = null;
                FloidServicePhoto floidServicePhotoForUx = null;
                photoBytes = null;  // Make sure we empty the photo bytes
                photoBytesForUx = null;  // Make sure we empty the photo bytes for ux
                // Sends a photo if one is available and gets a response...
                synchronized (mFloidPhotoSynchronizer) {
                    if (mPhotos.size() > 0) {
                        processedPhoto = true;
                        try {
                            // We have a photo, get it and send it:
                            floidServicePhoto = mPhotos.get(0);
                            photoBytes = floidServicePhoto.getImageBytes();
                            mPhotos.remove(0);
                            // Also get the smaller bytes:
                            floidServicePhotoForUx = mPhotosForUx.get(0);
                            photoBytesForUx = floidServicePhotoForUx.getImageBytes();
                            mPhotosForUx.remove(0);
                        } catch (Exception e) {
                            Log.e(TAG, "Exception getting photos/photos for ux", e);
                        }
                    }
                }
                // Done part that needed synchronization:
                if((floidServicePhoto != null) && (photoBytes != null) && (floidServicePhotoForUx != null) && (photoBytesForUx != null)) {
                    // Format and send the picture bytes:
                    uxSendPhoto(photoBytesForUx);
                    // Create an image message for the photo:
                    FloidServerMessage.FloidServerImageMessage photoFloidServerImageMessage = createFloidServerImageMessage(floidServicePhotoForUx);
                    // And then create a photo message:
                    FloidServerMessage floidServerMessage = FloidServerMessageUtils.createImageFloidServerMessage(
                            mFloidDroidStatus,
                            FloidCommands.FLOID_MESSAGE_PACKET_PHOTO,
                            FloidCommands.FLOID_MESSAGE_PACKET_PHOTO,
                            photoFloidServerImageMessage);
                    try {
                        // Send the Floid Status in a Floid Server Message, get a Floid Client Message back:
                        if (mFloidService.mUseLogger) Log.i(TAG, "Service -> Server: Floid Status");
                        if(floidServerMessage != null) {
                            byte[] floidClientMessageBytes = mServerCommunicator.sendFloidServerMessage(floidServerMessage);
                            if(floidClientMessageBytes != null) {
                                try {
                                    FloidClientMessage floidClientMessage = FloidClientMessage.parseFrom(floidClientMessageBytes);
                                    processFloidClientMessageFromPhotoSend(floidClientMessage);
                                } catch (Exception e) {
                                    if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Floid Client Message from Photo", e);
                                }
                            } else {
                                mAdditionalStatus.increment(AdditionalStatus.NULL_RETURN_FROM_SERVER);
                            }
                        } else {
                            Log.e(TAG, "Empty Floid Server Message in processPhoto");
                        }
                    } catch (Exception e) {
                        if (mFloidService.mUseLogger) Log.w(TAG, "Failed to send Photo: " + e.getMessage());
                    }
                }
            } while (photoBytes != null);
            return processedPhoto;
        }

        /**
         * Create a new floid server image message from a floid service image
         * @param floidServiceImage the floid service image
         * @return the floid server image message
         */
        private FloidServerMessage.FloidServerImageMessage createFloidServerImageMessage(FloidServiceImage floidServiceImage) {
            return FloidServerMessage.FloidServerImageMessage
                        .newBuilder()
                        .setAltitude(floidServiceImage.getAltitude())
                        .setDataBytes(ByteString.copyFrom(floidServiceImage.getImageBytes()))
                        .setImageName(floidServiceImage.getImageName())
                        .setImageType(floidServiceImage.getImageType())
                        .setLatitude(floidServiceImage.getLatitude())
                        .setLongitude(floidServiceImage.getLongitude())
                        .setPan(floidServiceImage.getPan())
                        .setTilt(floidServiceImage.getTilt())
                        .setImageExtension(floidServiceImage.getImageExtension())
                        .build();
        }

        /**
         * Process floid client message from floid droid status.
         *
         * @param floidClientMessage the floid client message
         */
        @SuppressWarnings({"rawtypes", "ConstantConditions"})
        private void processFloidClientMessageFromFloidDroidStatus(FloidClientMessage floidClientMessage) {
            if(floidClientMessage != null) {
                try {
                    JSONObject returnedJSON = new JSONObject(floidClientMessage.getStringMessage());
                    Class[] classParam = null;
                    Object[] objectParam = null;
                    try {
                        // Get the name of the command:
                        Class<?> cl = Class.forName(floidClientMessage.getMessageClass());
                        // Construct it:
                        java.lang.reflect.Constructor co = cl.getConstructor(classParam);
                        Object o = co.newInstance(objectParam);
                        // OK, now fill it in from the JSON:
                        Method fromJSONMethod = o.getClass().getMethod("fromJSON", JSONObject.class);
                        fromJSONMethod.invoke(o, returnedJSON);
                        DroidInstruction droidInstruction = (DroidInstruction) o;
                        //noinspection StatementWithEmptyBody
                        if (droidInstruction.getClass() != NullCommand.class) {
                            // Tear down any current mission:
                            // NOTE: FOR CERTAIN COMMANDS WE MAY NOT WANT TO DO THIS, FOR EXAMPLE A CANCEL CURRENT COMMAND OR "GOTO LABEL" COMMAND WITHIN MISSION WOULD BE HANDLED DIFFERENTLY IF IMPLEMENTED
                            explicitCurrentMissionAndInstructionTearDown();
                            // Setup the new instruction:
                            setupNewInstruction(mFloidService, (DroidInstruction) o, true);
                        } else {
                            // Null Commands do nothing!
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Caught exception processing return string from Floid Droid Status", e);
                    }
                } catch (Exception e) {
                    // [STATS] Increase bad packets from server:
                    mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_FROM_SERVER);
                    // [STATS] Increase bad json from server:
                    mAdditionalStatus.increment(AdditionalStatus.BAD_MESSAGES_FROM_SERVER);
                    // [STATS] Add the bad bytes from server:
                    mAdditionalStatus.add(AdditionalStatus.BAD_BYTES_FROM_SERVER, floidClientMessage.getStringMessage()!=null?floidClientMessage.getStringMessage().length():0);
                    // Bad string:
                    if (mFloidService.mUseLogger)
                        Log.e(TAG, "Service <-- Server: Bad return message from server: " + floidClientMessage.getStringMessage() + " " + e.getMessage() + " " + Log.getStackTraceString(e));
                }
            } else {
                // [STATS] Increase bad packets from server:
                mAdditionalStatus.increment(AdditionalStatus.BAD_PACKETS_FROM_SERVER);
                // [STATS] Increase bad json from server:
                mAdditionalStatus.increment(AdditionalStatus.BAD_MESSAGES_FROM_SERVER);
            }
        }

        /**
         * Explicit current mission and instruction tear down.
         */
        private void explicitCurrentMissionAndInstructionTearDown() {
            if((mCurrentInstructionProcessor==null) && (mCurrentMission==null) && (mMissionStack.isEmpty())) {
                return; // Nothing to do:
            }
            String tearDownItems = String.format("%s%s%s", (mCurrentInstructionProcessor != null) ? "-INSTRUCTION-" : " ", (mCurrentMission != null) ? "-MISSION-" : " ", (!mMissionStack.isEmpty()) ? "-STACK-" : " ");
            addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, String.format("Explicit mission/instruction tear down: %s", tearDownItems));
            // Clear the mission, stack and currentInstruction
            mCurrentInstructionProcessor = null;      //  NOTE:   processReturnStringFromFloidDroidStatus: THIS PERFORMS A BAD TEAR-DOWN...
            mCurrentMission = null;      //  NOTE:  processReturnStringFromFloidDroidStatus: THIS PERFORMS A BAD TEAR-DOWN...
            mMissionStack.clear();
        }

        /**
         * Sets new instruction.
         *
         * @param floidService     the floid service
         * @param droidInstruction the droid instruction
         * @param isNewFromServer  the is new from server
         * @return the new instruction
         */
        // Returns true if command sets up correctly:
        @SuppressWarnings("UnusedReturnValue")
        private boolean setupNewInstruction(FloidService floidService, DroidInstruction droidInstruction, boolean isNewFromServer) {
            if(droidInstruction==null) {
                return false;
            }
            addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, droidInstruction.getShortType() + (isNewFromServer?"<-Server":"<-CurrentMission"));
            try {
                // If we were idle, we are not processing:
                if (mFloidDroidStatus.getCurrentStatus() == FloidDroidStatus.DROID_STATUS_IDLE) {
                    addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Start->Processing");
                    // We don't have a mission and nothing on the mission stack - we are idle...
                    synchronized (mFloidDroidStatusSynchronizer) {
                        mFloidDroidStatus.setCurrentStatus(FloidDroidStatus.DROID_STATUS_PROCESSING);
                    }
                    // Tell the interface to display the changed droid status:
                    uxSendFloidDroidStatus();
                }
                if (droidInstruction.getClass() == DroidMission.class) {
                    // This is a mission
                    if (isNewFromServer) {
                        addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Mission<-Server");
                        // Kill off any current mission and instructions...
                        explicitCurrentMissionAndInstructionTearDown();
                        addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Start->Timer");
                        // Create a new mission ready timer object
                        FloidMissionReadyTimerObject floidMissionReadyTimerObject = new FloidMissionReadyTimerObject((DroidMission) droidInstruction, mDroidNewMissionDefaultCountDownTime);
                        serviceSendStartMissionTimer(floidMissionReadyTimerObject);
                    } else {
                        // Are we in a mission:
                        if (mCurrentMission != null) {
                            // Put it on the stack so we will return to it later...
                            addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Stack<-Mission");
                            mMissionStack.add(mCurrentMission);
                        }
                        mCurrentMission = (DroidMission) droidInstruction;
                        FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = new FloidDroidMissionInstructionStatusMessage();
                        floidDroidMissionInstructionStatusMessage.setMessageType(FloidCommands.FLOID_MESSAGE_PACKET_MISSION_STARTING);
                        if (mCurrentMission != null) {
                            if (mCurrentMission.getId() != null) {
                                floidDroidMissionInstructionStatusMessage.setMissionId(mCurrentMission.getId());
                            } else {
                                floidDroidMissionInstructionStatusMessage.setMissionId(NO_MISSION_ID);
                            }
                            if (mCurrentMission.getMissionName() != null) {
                                floidDroidMissionInstructionStatusMessage.setMissionName(mCurrentMission.getMissionName());
                            } else {
                                floidDroidMissionInstructionStatusMessage.setMissionName("");
                            }

                            floidDroidMissionInstructionStatusMessage.setInstructionId(mCurrentMission.getId());
                            floidDroidMissionInstructionStatusMessage.setInstructionType(mCurrentMission.getType());
                        } else {
                            floidDroidMissionInstructionStatusMessage.setMissionId(NO_MISSION_ID);
                            floidDroidMissionInstructionStatusMessage.setMissionName("");
                            floidDroidMissionInstructionStatusMessage.setInstructionId(NO_INSTRUCTION_ID);
                            floidDroidMissionInstructionStatusMessage.setInstructionType("");
                        }
                        floidDroidMissionInstructionStatusMessage.setInstructionStatus(ProcessBlock.PROCESS_BLOCK_STARTED);
                        if(mCurrentMission==null) {
                            floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Mission<-Start=NoCurrentMission");
                        } else {
                            floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Mission<-Start=" + mCurrentMission.getMissionName());
                        }
                        sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage);
//                        addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Processor<-Clear");
                        mCurrentInstructionProcessor = null;    // Will force us to get a new command next time!
                    }
                } else {
                    // This is a command:
//                    addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction is a command");
                    // Create a new instruction processor:
                    addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Command<-Processor");
                    mCurrentInstructionProcessor = InstructionProcessorFactory.getNewInstructionProcessor(floidService, droidInstruction);
                    // And set it up:
                    if (mCurrentInstructionProcessor != null) {
//                        addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Setting up instruction");
                        mCurrentInstructionProcessor.setupInstruction();
                        // And send a message:
                        FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = new FloidDroidMissionInstructionStatusMessage();
                        floidDroidMissionInstructionStatusMessage.setMessageType(FloidCommands.FLOID_MESSAGE_PACKET_INSTRUCTION_STARTING);
                        setMissionIdAndNameToStatusMessage(mCurrentMission, floidDroidMissionInstructionStatusMessage);
                        if (mCurrentInstructionProcessor.getDroidInstruction().getId() != null) {
                            floidDroidMissionInstructionStatusMessage.setInstructionId(mCurrentInstructionProcessor.getDroidInstruction().getId());
                        } else {
                            floidDroidMissionInstructionStatusMessage.setInstructionId(NO_INSTRUCTION_ID);
                        }
                        floidDroidMissionInstructionStatusMessage.setInstructionType(mCurrentInstructionProcessor.getDroidInstruction().getType());
                        floidDroidMissionInstructionStatusMessage.setInstructionStatus(mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus());
                        floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Start<-" + mCurrentInstructionProcessor.getDroidInstruction().getSimplifiedType() + "=" + mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus());
                        sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage);

                        // Speaker On is async - move it over:
                        if (mCurrentInstructionProcessor.getClass() == SpeakerOnCommandProcessor.class) {
                            mSpeakerOnCommandProcessor = (SpeakerOnCommandProcessor) mCurrentInstructionProcessor;
                            addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Processor<-Clear(AsyncCommand)");
                            mCurrentInstructionProcessor = null;  // And null out the current processor
                        }
                    } else {
                        addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Processor<-Fail");
                        return false;
                    }
                }
                return true;
            } catch (Exception e) {
                addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Instruction<-Fail:" + e.getMessage());
                if (mFloidService.mUseLogger) Log.e(TAG, "Service Failed to set up new instruction", e);
                mCurrentInstructionProcessor = null;
                return false;
            }
        }

        /**
         * Process.
         */
        // Main process routine:
        // =====================
        private void process() {
            // If we have an instruction processor, then process:
            if (mCurrentInstructionProcessor != null) {
                try {
                    // Get current status so if it changes we can send a message if it changed:
                    int savedProcessBlockStatus = mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus();
                    // Process:
                    if (mCurrentInstructionProcessor.processInstruction()) {
                        // Complete - send a message:
                        FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = new FloidDroidMissionInstructionStatusMessage();
                        floidDroidMissionInstructionStatusMessage.setMessageType(FloidCommands.FLOID_MESSAGE_PACKET_INSTRUCTION_COMPLETE);
                        int processBlockFinalStatus = mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus();
                        setMissionIdAndNameToStatusMessage(mCurrentMission, floidDroidMissionInstructionStatusMessage);
                        if (mCurrentInstructionProcessor.getDroidInstruction().getId() != null) {
                            floidDroidMissionInstructionStatusMessage.setInstructionId(mCurrentInstructionProcessor.getDroidInstruction().getId());
                        } else {
                            floidDroidMissionInstructionStatusMessage.setInstructionId(NO_INSTRUCTION_ID);
                        }
                        floidDroidMissionInstructionStatusMessage.setInstructionType(mCurrentInstructionProcessor.getDroidInstruction().getType());
                        floidDroidMissionInstructionStatusMessage.setInstructionStatus(mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus());
                        floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Complete<-" + mCurrentInstructionProcessor.getDroidInstruction().getType() + "=" + mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus());
                        sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage);
                        // Here we will error out the mission if the instruction erred:
                        boolean instructionErred = false;
                        int instructionStatus;

                        switch(processBlockFinalStatus) {
                            case ProcessBlock.PROCESS_BLOCK_COMPLETED_COMMUNICATION_ERROR:
                            case ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR:
                            case ProcessBlock.PROCESS_BLOCK_COMPLETED_LOGIC_ERROR:
                            case ProcessBlock.PROCESS_BLOCK_COMPLETED_NO_FLOID_ERROR:
                            case ProcessBlock.PROCESS_BLOCK_COMPLETED_TIMEOUT_ERROR:
                                instructionErred = true;
                                instructionStatus = processBlockFinalStatus;
                                break;
                            case ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS:
                                // Do nothing - this is the only success criteria!!!
                                instructionStatus = processBlockFinalStatus;
                                break;
                            case ProcessBlock.PROCESS_BLOCK_COMMAND_RESPONSE_RECEIVED:
                            case ProcessBlock.PROCESS_BLOCK_IN_PROGRESS:
                            case ProcessBlock.PROCESS_BLOCK_NOT_STARTED:
                            case ProcessBlock.PROCESS_BLOCK_STARTED:
                            case ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE:
                                // These are not valid states but are logic errors - if it is complete, it should have a completed status or it is not implemented correctly
                            default:
                                // This is a logic error somewhere!
                                instructionErred = true;
                                instructionStatus = ProcessBlock.PROCESS_BLOCK_COMPLETED_LOGIC_ERROR;
                                break;
                        }
                        boolean stopMission = (mCurrentMission!=null) && instructionErred && mCurrentInstructionProcessor.isStopMission();
                        // This command is done - tear it down:
                        mCurrentInstructionProcessor.tearDownInstruction();
                        mCurrentInstructionProcessor = null;        // We'll skip a beat and come back here and come back below:
                        // Did we have a fault and need to error out the mission?
                        if (stopMission) {
                            // OK, send a message and tear it down
                            // TODO [PHYSICS ERROR HANDLING] Here we should be smarter about e.g. turning off helicopters automatically if error
                            FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage2 = new FloidDroidMissionInstructionStatusMessage();
                            floidDroidMissionInstructionStatusMessage2.setMessageType(FloidCommands.FLOID_MESSAGE_PACKET_MISSION_COMPLETE_ERROR);
                            setMissionIdAndNameToStatusMessage(mCurrentMission, floidDroidMissionInstructionStatusMessage2);
                            floidDroidMissionInstructionStatusMessage2.setInstructionId(mCurrentMission!=null?mCurrentMission.getId():NO_MISSION_ID);
                            floidDroidMissionInstructionStatusMessage2.setInstructionType(mCurrentMission.getType());
                            floidDroidMissionInstructionStatusMessage2.setInstructionStatus(instructionStatus);  // We copy in the instruction error to the mission
                            if (mCurrentMission != null) {
                                if (mCurrentMission.getId() != null) {
                                    floidDroidMissionInstructionStatusMessage2.setInstructionStatusDisplayString("Mission<-Complete[Error]=" + mCurrentMission.getMissionName());
                                }
                            }
                            sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage2);
                            explicitCurrentMissionAndInstructionTearDown();
                        }
                    } else {
                        // Instruction did not finish but did its status change?
                        if (savedProcessBlockStatus != mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus()) {
                            // Send a message:
                            sendInstructionStatusChangeMessage();
                        }
                    }
                } catch (Exception e) {
                    mCurrentInstructionProcessor = null;    // Skip a beat and come back below
                    addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Process<-Exception(@1)=" + e.getMessage() + "[" + e.getStackTrace()[0].getFileName() + ":" + e.getStackTrace()[0].getLineNumber() + "]");
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception in process - @1 [" + e.getStackTrace()[0].getFileName() + ":" + e.getStackTrace()[0].getLineNumber() + "]", e);
                }
            } else {
                try {
                    // Get the next instruction from the current mission:
                    if (mCurrentMission != null) {
                        // Call into the mission and get the next command and then set it up:
                        DroidInstruction droidInstruction = mCurrentMission.getNextInstruction();
                        if (droidInstruction == null) {
                            // This mission is done - send a message:
                            FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = new FloidDroidMissionInstructionStatusMessage();
                            floidDroidMissionInstructionStatusMessage.setMessageType(FloidCommands.FLOID_MESSAGE_PACKET_MISSION_COMPLETE);
                            setMissionIdAndNameToStatusMessage(mCurrentMission, floidDroidMissionInstructionStatusMessage);
                            floidDroidMissionInstructionStatusMessage.setInstructionId(mCurrentMission.getId());
                            floidDroidMissionInstructionStatusMessage.setInstructionType(mCurrentMission.getType());
                            floidDroidMissionInstructionStatusMessage.setInstructionStatus(NO_INSTRUCTION_STATUS_ID);
                            floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Mission<-Complete=" + mCurrentMission.getMissionName());
                            sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage);
                            mCurrentMission = null;        // Skip a beat and next time we'll fall down and see if any missions are on the stack:
                        } else {
                            // Set up the new instruction!
                            setupNewInstruction(mFloidService, droidInstruction, false);
                        }
                    } else {
                        // No current mission:
                        // See if we have anything on the mission stack...
                        if (mMissionStack.size() > 0) {
                            // If so, set it into the current mission - we will hit the code above and get the next instruction, etc.
                            DroidInstruction nextMission = mMissionStack.get(mMissionStack.size() - 1);
                            mMissionStack.remove(mMissionStack.size() - 1);
                            if(nextMission!=null) {
                                setupNewInstruction(mFloidService, nextMission, false);
                            }
                        } else {
                            // We don't have a mission and nothing on the mission stack - we are idle...
                            if (mFloidDroidStatus.getCurrentStatus() != FloidDroidStatus.DROID_STATUS_IDLE) {
                                synchronized (mFloidDroidStatusSynchronizer) {
                                    mFloidDroidStatus.setCurrentStatus(FloidDroidStatus.DROID_STATUS_IDLE);
                                }
                                // Tell the interface to display the changed droid status:
                                uxSendFloidDroidStatus();
                            }
                        }
                    }
                } catch (Exception e) {
                    addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Process<-Exception(@2)=" + e.getMessage());
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception in process - 2 ", e);
                }
            }
            // Process asynchronous commands:
            // Speaker On:
            if (mSpeakerOnCommandProcessor != null) {
                try {
                    if (mSpeakerOnCommandProcessor.processInstruction()) {
                        // Complete -  send a message:
                        FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = new FloidDroidMissionInstructionStatusMessage();
                        floidDroidMissionInstructionStatusMessage.setMessageType(FloidCommands.FLOID_MESSAGE_PACKET_INSTRUCTION_COMPLETE);
                        setMissionIdAndNameToStatusMessage(mCurrentMission, floidDroidMissionInstructionStatusMessage);
                        if (mSpeakerOnCommandProcessor.getDroidInstruction().getId() != null) {
                            floidDroidMissionInstructionStatusMessage.setInstructionId(mSpeakerOnCommandProcessor.getDroidInstruction().getId());
                        } else {
                            floidDroidMissionInstructionStatusMessage.setInstructionId(NO_INSTRUCTION_ID);
                        }
                        floidDroidMissionInstructionStatusMessage.setInstructionType(mSpeakerOnCommandProcessor.getDroidInstruction().getType());
                        floidDroidMissionInstructionStatusMessage.setInstructionStatus(mSpeakerOnCommandProcessor.getProcessBlock().getProcessBlockStatus());
                        floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Instruction<-Complete=" + mCurrentInstructionProcessor.getDroidInstruction().getType() + "-" + mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus());
                        sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage);
                        // This command is done - tear it down:
                        mSpeakerOnCommandProcessor.tearDownInstruction();
                        mSpeakerOnCommandProcessor = null;
                    }
                } catch (Exception e) {
                    addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "Process<-Exception(@speakeron)=" + e.getMessage());
                    if (mFloidService.mUseLogger) Log.e(TAG, "Exception in processing speaker on", e);
                }
            }
        }

        private void setMissionIdAndNameToStatusMessage(DroidMission droidMission, FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage) {
            if (droidMission != null) {
                if (droidMission.getId() != null) {
                    floidDroidMissionInstructionStatusMessage.setMissionId(droidMission.getId());
                } else {
                    floidDroidMissionInstructionStatusMessage.setMissionId(NO_MISSION_ID);
                }
                floidDroidMissionInstructionStatusMessage.setMissionName(droidMission.getMissionName());
            } else {
                floidDroidMissionInstructionStatusMessage.setMissionId(NO_MISSION_ID);
                floidDroidMissionInstructionStatusMessage.setMissionName("");
            }
        }
        /**
         * Send an instruction status changed message for the current instruction:
         */
        private void sendInstructionStatusChangeMessage() {
            // Send a message:
            FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage = new FloidDroidMissionInstructionStatusMessage();
            // Process block changed status but did not complete - send message:
            floidDroidMissionInstructionStatusMessage.setMessageType(FloidCommands.FLOID_MESSAGE_PACKET_INSTRUCTION_STATUS_CHANGE);
            setMissionIdAndNameToStatusMessage(mCurrentMission, floidDroidMissionInstructionStatusMessage);
            if (mCurrentInstructionProcessor.getDroidInstruction().getId() != null) {
                floidDroidMissionInstructionStatusMessage.setInstructionId(mCurrentInstructionProcessor.getDroidInstruction().getId());
            } else {
                floidDroidMissionInstructionStatusMessage.setInstructionId(NO_MISSION_ID);
            }
            floidDroidMissionInstructionStatusMessage.setInstructionType(mCurrentInstructionProcessor.getDroidInstruction().getType());
            floidDroidMissionInstructionStatusMessage.setInstructionStatus(mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus());
            floidDroidMissionInstructionStatusMessage.setInstructionStatusDisplayString("Instruction<-Status-" + mCurrentInstructionProcessor.getDroidInstruction().getSimplifiedType() + "=" + mCurrentInstructionProcessor.getProcessBlock().getProcessBlockStatus());
            sendFloidDroidMissionInstructionStatusMessage(floidDroidMissionInstructionStatusMessage);
        }
        /**
         * Send floid droid mission instruction status message boolean.
         *
         * @param floidDroidMissionInstructionStatusMessage the floid droid mission instruction status message
         * @return the boolean
         */
        @SuppressWarnings("UnusedReturnValue")
        public boolean sendFloidDroidMissionInstructionStatusMessage(FloidDroidMissionInstructionStatusMessage floidDroidMissionInstructionStatusMessage) {
            try {
                // Send the Floid Status in a Floid Server Message, get a Floid Client Message back:
                if (mFloidService.mUseLogger) Log.v(TAG, "Service -> Server: Mission Instruction Status");
                // Set the floid id and floid uuid:
                if(mFloidService.getFloidDroidStatus()!=null) {
                    floidDroidMissionInstructionStatusMessage.setFloidId(mFloidService.getFloidDroidStatus().getFloidId());
                    floidDroidMissionInstructionStatusMessage.setFloidUuid(mFloidService.getFloidDroidStatus().getFloidUuid());
                }
                FloidServerMessage floidServerMessage = FloidServerMessageUtils.createStandardFloidServerMessage(mFloidDroidStatus, FloidDroidMissionInstructionStatusMessage.class.getName(), FloidCommands.FLOID_MESSAGE_PACKET_INSTRUCTION_STATUS, floidDroidMissionInstructionStatusMessage.toJSON().toString());
                if(floidServerMessage != null) {
                    byte[] floidClientMessageBytes = mServerCommunicator.sendFloidServerMessage(floidServerMessage);
                    if(floidClientMessageBytes != null) {
                        try {
                            FloidClientMessage floidClientMessage = FloidClientMessage.parseFrom(floidClientMessageBytes);
                            return processFloidClientMessageFromFloidDroidMissionInstructionStatusMessage(floidClientMessage);
                        } catch (Exception e) {
                            if (mFloidService.mUseLogger) Log.e(TAG, "Caught exception processing Mission Instruction Status", e);
                            return false;
                        }
                    } else {
                        mAdditionalStatus.increment(AdditionalStatus.NULL_RETURN_FROM_SERVER);
                        return false;
                    }
                } else {
                    Log.e(TAG, "Empty Floid Server Message in sendFloidDroidMissionInstructionStatusMessage");
                    return false;
                }
            } catch (Exception e) {
                if (mFloidService.mUseLogger) Log.w(TAG, "Failed to send Floid Droid Mission Instruction Status: " + e.getMessage());
                return false;
            }
        }

        /**
         * Process floid client message from floid droid mission instruction status message boolean.
         *
         * @param floidClientMessage the floid client message
         * @return the boolean
         */
        // Process the return string:
        @SuppressWarnings({"SameReturnValue", "UnusedParameters"})
        public boolean processFloidClientMessageFromFloidDroidMissionInstructionStatusMessage(FloidClientMessage floidClientMessage) {
            // Does nothing for now:
            return true;
        }
    } // FloidDroidRunnable

    /**
     * Gets next floid command number.
     *
     * @return the next floid command number
     */
    public int getNextFloidCommandNumber() {
        return ++mFloidCommandNumber;
    }

    /**
     * Send command to floid.
     *
     * @param floidOutgoingCommand the floid outgoing command
     * @return the floid outgoing command
     */
    // Communication routines:
    // =======================
    public FloidOutgoingCommand sendCommandToFloid(FloidOutgoingCommand floidOutgoingCommand) {
        // [STATS] Bump the commands to floid count:
        mAdditionalStatus.increment(AdditionalStatus.COMMANDS_TO_FLOID);
        // Add a command number if it did not have one:
        if (floidOutgoingCommand.getCommandNumber() == -1) {
            floidOutgoingCommand.setCommandNumber(getNextFloidCommandNumber());
        }
        // We only communicate in full variant mode:
        if(FloidVariant.getVariant() == FloidVariant.VARIANT_FULL) {
            synchronized (mFloidOutgoingCommandsSynchronizer) {
                mFloidOutgoingCommands.add(floidOutgoingCommand);
            }
        }
        floidOutgoingCommand.logCommandBuffer();
        return floidOutgoingCommand;
    }

    /**
     * Send relay command to floid.
     *
     * @param relayDevice      the relay device
     * @param relayDeviceState the relay device state
     * @return the floid outgoing command
     */
    @SuppressWarnings("UnusedReturnValue")
    public FloidOutgoingCommand sendRelayCommandToFloid(byte relayDevice, byte relayDeviceState) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Relay Command");
            FloidOutgoingCommand relayOutgoingCommand = new FloidOutgoingCommand(RELAY_PACKET, RELAY_PACKET_SIZE, getNextFloidCommandNumber(), NO_GOAL_ID);
            relayOutgoingCommand.setByteToBuffer(RELAY_PACKET_DEVICE_OFFSET, relayDevice);
            relayOutgoingCommand.setByteToBuffer(RELAY_PACKET_STATE_OFFSET, relayDeviceState);
            return sendCommandToFloid(relayOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Relay command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send heartbeat command to floid.
     *
     * @return the floid outgoing command
     */
    @SuppressWarnings("UnusedReturnValue")
    public FloidOutgoingCommand sendHeartbeatCommandToFloid() {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Heartbeat Command");
            FloidOutgoingCommand heartbeatOutgoingCommand = new FloidOutgoingCommand(HEARTBEAT_PACKET, HEARTBEAT_PACKET_SIZE, getNextFloidCommandNumber(), NO_GOAL_ID);
            return sendCommandToFloid(heartbeatOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Heartbeat command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send designate command to floid.
     *
     * @param x the x
     * @param y the y
     * @param z the z
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendDesignateCommandToFloid(float x, float y, float z) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Designate Command");
            FloidOutgoingCommand designateOutgoingCommand = new FloidOutgoingCommand(DESIGNATE_TARGET_PACKET, DESIGNATE_TARGET_PACKET_SIZE, getNextFloidCommandNumber(), NO_GOAL_ID);
            designateOutgoingCommand.setFloatToBuffer(DESIGNATE_TARGET_PACKET_X_OFFSET, x);
            designateOutgoingCommand.setFloatToBuffer(DESIGNATE_TARGET_PACKET_Y_OFFSET, y);
            designateOutgoingCommand.setFloatToBuffer(DESIGNATE_TARGET_PACKET_Z_OFFSET, z);
            return sendCommandToFloid(designateOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Designate command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send debug control to floid.
     *
     * @param debugIndex the debug index
     * @param debugValue the debug value
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendDebugControlToFloid(byte debugIndex, byte debugValue) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Debug Control");
            FloidOutgoingCommand debugControlOutgoingCommand = new FloidOutgoingCommand(DEBUG_CONTROL_PACKET, DEBUG_CONTROL_PACKET_SIZE, getNextFloidCommandNumber(), NO_GOAL_ID);
            debugControlOutgoingCommand.setByteToBuffer(DEBUG_PACKET_INDEX_OFFSET, debugIndex);
            debugControlOutgoingCommand.setByteToBuffer(DEBUG_PACKET_VALUE_OFFSET, debugValue);
            return sendCommandToFloid(debugControlOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Debug Control command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send fly to command to floid.
     *
     * @param goalId the goal id
     * @param x      the x
     * @param y      the y
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendFlyToCommandToFloid(int goalId, float x, float y) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Fly To Command");
            FloidOutgoingCommand flyToOutgoingCommand = new FloidOutgoingCommand(SET_POSITION_GOAL_PACKET, SET_POSITION_GOAL_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            flyToOutgoingCommand.setFloatToBuffer(SET_POSITION_GOAL_PACKET_X_OFFSET, x);
            flyToOutgoingCommand.setFloatToBuffer(SET_POSITION_GOAL_PACKET_Y_OFFSET, y);
            return sendCommandToFloid(flyToOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Fly To command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send height to command to floid.
     *
     * @param goalId the goal id
     * @param z      the z
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendHeightToCommandToFloid(int goalId, float z) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Height To Command");
            FloidOutgoingCommand heightToOutgoingCommand = new FloidOutgoingCommand(SET_ALTITUDE_GOAL_PACKET, FloidService.SET_ALTITUDE_GOAL_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            heightToOutgoingCommand.setFloatToBuffer(SET_ALTITUDE_GOAL_PACKET_Z_OFFSET, z);
            return sendCommandToFloid(heightToOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Height To command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send device gps command to floid.
     *
     * @param latitude  the latitude
     * @param longitude the longitude
     * @param altitude  the altitude
     * @param hdop      the hdop
     * @return the floid outgoing command
     */
    @SuppressWarnings("UnusedReturnValue")
    public FloidOutgoingCommand sendDeviceGpsCommandToFloid(float latitude, float longitude, float altitude, float hdop) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Device GPS Command");
            FloidOutgoingCommand deviceGpsOutgoingCommand = new FloidOutgoingCommand(DEVICE_GPS_PACKET, DEVICE_GPS_PACKET_SIZE, getNextFloidCommandNumber(), NO_GOAL_ID);
            deviceGpsOutgoingCommand.setFloatToBuffer(DEVICE_GPS_OFFSET_LAT, latitude);
            deviceGpsOutgoingCommand.setFloatToBuffer(DEVICE_GPS_OFFSET_LNG, longitude);
            deviceGpsOutgoingCommand.setFloatToBuffer(DEVICE_GPS_OFFSET_ALT, altitude);
            deviceGpsOutgoingCommand.setFloatToBuffer(DEVICE_GPS_OFFSET_HDOP, hdop);
            return sendCommandToFloid(deviceGpsOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Device GPS command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send land command to floid.
     *
     * @param goalId the goal id
     * @param z      the z
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendLandCommandToFloid(int goalId, float z) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Land Command");
            FloidOutgoingCommand landOutgoingCommand = new FloidOutgoingCommand(FloidService.LAND_PACKET, FloidService.LAND_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            landOutgoingCommand.setFloatToBuffer(FloidService.LAND_PACKET_Z_OFFSET, z);
            return sendCommandToFloid(landOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Land command: " + Log.getStackTraceString(e) + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send lift off command to floid.
     *
     * @param goalId the goal id
     * @param x      the x
     * @param y      the y
     * @param z      the z
     * @param a      the a
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendLiftOffCommandToFloid(int goalId, float x, float y, float z, float a) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Lift Off Command");
            FloidOutgoingCommand liftOffOutgoingCommand = new FloidOutgoingCommand(LIFT_OFF_PACKET, LIFT_OFF_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            liftOffOutgoingCommand.setFloatToBuffer(LIFT_OFF_PACKET_X_OFFSET, x);
            liftOffOutgoingCommand.setFloatToBuffer(LIFT_OFF_PACKET_Y_OFFSET, y);
            liftOffOutgoingCommand.setFloatToBuffer(LIFT_OFF_PACKET_Z_OFFSET, z);
            liftOffOutgoingCommand.setFloatToBuffer(LIFT_OFF_PACKET_A_OFFSET, a);
            return sendCommandToFloid(liftOffOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Lift Off command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send pan tilt command to floid.
     *
     * @param goalId the goal id
     * @param device the device
     * @param type   the type
     * @param pan    the pan
     * @param tilt   the tilt
     * @param x      the x
     * @param y      the y
     * @param z      the z
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendPanTiltCommandToFloid(int goalId, byte device, byte type, float pan, float tilt, float x, float y, float z) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Pan TIlt Command");
            FloidOutgoingCommand panTiltOutgoingCommand = new FloidOutgoingCommand(PAN_TILT_PACKET, PAN_TILT_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            panTiltOutgoingCommand.setByteToBuffer(PAN_TILT_PACKET_DEVICE_OFFSET, device);
            panTiltOutgoingCommand.setByteToBuffer(PAN_TILT_PACKET_TYPE_OFFSET, type);
            panTiltOutgoingCommand.setFloatToBuffer(PAN_TILT_PACKET_ANGLE_PAN_OFFSET, pan);
            panTiltOutgoingCommand.setFloatToBuffer(PAN_TILT_PACKET_ANGLE_TILT_OFFSET, tilt);
            panTiltOutgoingCommand.setFloatToBuffer(PAN_TILT_PACKET_TARGET_X_OFFSET, x);
            panTiltOutgoingCommand.setFloatToBuffer(PAN_TILT_PACKET_TARGET_Y_OFFSET, y);
            panTiltOutgoingCommand.setFloatToBuffer(PAN_TILT_PACKET_TARGET_Z_OFFSET, z);
            return sendCommandToFloid(panTiltOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Pan Tilt command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send payload command to floid.
     *
     * @param goalId the goal id
     * @param bay    the bay
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendPayloadCommandToFloid(int goalId, byte bay) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Payload Command");
            FloidOutgoingCommand payloadOutgoingCommand = new FloidOutgoingCommand(PAYLOAD_PACKET, PAYLOAD_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            payloadOutgoingCommand.setByteToBuffer(PAYLOAD_PACKET_BAY_OFFSET, bay);
            return sendCommandToFloid(payloadOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Payload command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send shut down helis command to floid.
     *
     * @param goalId the goal id
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendShutDownHelisCommandToFloid(int goalId) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Fly To Command");
            FloidOutgoingCommand shutDownHelisOutgoingCommand = new FloidOutgoingCommand(HELIS_OFF_PACKET, HELIS_OFF_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            return sendCommandToFloid(shutDownHelisOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Shut Down Helis command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send start up helis command to floid.
     *
     * @param goalId the goal id
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendStartUpHelisCommandToFloid(int goalId) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Start Up Helis Command");
            FloidOutgoingCommand startUpHelisOutgoingCommand = new FloidOutgoingCommand(HELIS_ON_PACKET, HELIS_ON_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            return sendCommandToFloid(startUpHelisOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Start Up Helis command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send stop designating command to floid.
     *
     * @param goalId the goal id
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendStopDesignatingCommandToFloid(int goalId) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Stop Designating Command");
            FloidOutgoingCommand stopDesignatingOutgoingCommand = new FloidOutgoingCommand(STOP_DESIGNATING_PACKET, STOP_DESIGNATING_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            return sendCommandToFloid(stopDesignatingOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Stop Designating command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send turn to command to floid.
     *
     * @param goalId the goal id
     * @param mode   the mode
     * @param angle  the angle
     * @return the floid outgoing command
     */
    public FloidOutgoingCommand sendTurnToCommandToFloid(int goalId, byte mode, float angle) {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Turn To Command");
            FloidOutgoingCommand turnToControlOutgoingCommand = new FloidOutgoingCommand(SET_ROTATION_GOAL_PACKET, SET_ROTATION_GOAL_PACKET_SIZE, getNextFloidCommandNumber(), goalId);
            turnToControlOutgoingCommand.setByteToBuffer(SET_ROTATION_GOAL_PACKET_FOLLOW_MODE_OFFSET, mode);
            turnToControlOutgoingCommand.setFloatToBuffer(SET_ROTATION_GOAL_PACKET_A_OFFSET, angle);
            return sendCommandToFloid(turnToControlOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Turn To command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * Send get model parameters command to.
     * Note: This is not 'unused' in the 'full' version, just the 'demo version'
     *
     * @return the floid outgoing command
     */
    @SuppressWarnings("UnusedReturnValue, unused")
    public FloidOutgoingCommand sendGetModelParametersCommandToFloid() {
        try {
            if(mUseLogger)
                Log.d(TAG, "Service -> Floid: Get Model Parameters Command");
            FloidOutgoingCommand getModelParametersOutgoingCommand = new FloidOutgoingCommand(GET_MODEL_PARAMETERS_PACKET, GET_MODEL_PARAMETERS_PACKET_SIZE, getNextFloidCommandNumber(), NO_GOAL_ID);
            return sendCommandToFloid(getModelParametersOutgoingCommand);
        } catch (Exception e) {
            if (mUseLogger)
                Log.e(TAG, "Failed to make/send Get Model Parameters command: " + e.getMessage() + " " + Log.getStackTraceString(e));
        }
        return null;
    }

// Reset routines:
    /**
     * Reset floid droid state.
     */
    public void resetFloidDroidState() {
        // Reset the FloidDroidState:
        if(mUseLogger)
            Log.d(TAG, "Resetting Floid Droid State");
        synchronized (mFloidDroidStatusSynchronizer) {
            // Clear the InstructionProcessor:
            mCurrentInstructionProcessor = null;
            // Clear the Mission stack:
            mMissionStack.clear();
            // Clear the mFloidActivityFloidDroidStatus:
            mFloidDroidStatus.setDroidStarted(false);
            mFloidDroidStatus.setCurrentStatus(FloidDroidStatus.DROID_STATUS_IDLE);
            mFloidDroidStatus.setHelisStarted(false);
            mFloidDroidStatus.setLiftedOff(false);
            mFloidDroidStatus.setParachuteDeployed(false);
            mFloidDroidStatus.setPhotoNumber(0);
            mFloidDroidStatus.setVideoNumber(0);
            mFloidDroidStatus.setTakingPhoto(false);
            mFloidDroidStatus.setTakingVideo(false);
            // Reset battery information:
            mFloidDroidStatus.setBatteryLevel(-1.0);
            mFloidDroidStatus.setBatteryRawLevel(0);
            mFloidDroidStatus.setBatteryRawScale(1);
            // Set the mode to the startup mode:
            setFloidDroidMode(mFloidModelParameters.getStartMode());
            // Set our update time:
            mLastFloidDroidStatusUpdateTime = SystemClock.uptimeMillis();
        }
        if (mInMissionTimerMode) {
            // Make sure we cancel the mission timer if it exists:
            uxSendCancelMissionTimer();
            cancelMissionTimer();
        }
    }

    /**
     * Sets floid droid mode.
     *
     * @param newMode the new mode
     */
    public void setFloidDroidMode(int newMode) {
        if(mUseLogger)
            Log.d(TAG, "Setting Floid Droid Mode");
        synchronized (mFloidDroidStatusSynchronizer) {
            mFloidDroidStatus.setCurrentMode(newMode);
            if (newMode == FloidDroidStatus.DROID_MODE_MISSION) {
                uxSendTurnOffInterfaceMessage();
            } else {
                uxSendTurnOnInterfaceMessage();
            }
        }
    }

    /**
     * Start mission timer.
     *
     * @param floidMissionReadyTimerObject the floid mission ready timer object
     */
    private void startMissionTimer(FloidMissionReadyTimerObject floidMissionReadyTimerObject) {
        if(mUseLogger)
            Log.d(TAG, "Starting Mission Timer");
        if(floidMissionReadyTimerObject.getPendingMission() == null) {
            Log.e(TAG, "Mission null starting mission timer - cancelling");
            return;
        }
        serviceSendSpeakCommand("Starting mission "
                + floidMissionReadyTimerObject.getPendingMission().getMissionName()
                + " in "
                + floidMissionReadyTimerObject.getTimeoutInSeconds()
                + " seconds."
                + "  Please move to safety");
        // Cancel any mission timer we might have:
        if(floidMissionReadyTimerObject != null) {
            cancelMissionTimer();
            // Copy this to the class timer object:
            mFloidMissionReadyTimerObject = floidMissionReadyTimerObject;
            // Tell the ux to start a mission timer cancel screen:
            uxSendStartMissionTimer(floidMissionReadyTimerObject.getTimeoutInSeconds(), mFloidMissionReadyTimerObject.getPendingMission().getMissionName() + " " + mFloidMissionReadyTimerObject.getPendingMission().getId());
            mFloidMissionReadyTimerObject = floidMissionReadyTimerObject;
            // Set up the timer
            mFloidMissionReadyTimerObject.setLastSecondsDisplayed(mFloidMissionReadyTimerObject.getTimeoutInSeconds());
            mInMissionTimerMode = true;
            // Fire up the timer here
            mFloidServiceHandler.removeCallbacks(mMissionStartTimerUpdateTask);   // Remove old callbacks if any
            mFloidServiceHandler.postDelayed(mMissionStartTimerUpdateTask, 5000);  // Fire it off in 100ms...
            synchronized (mFloidMissionTimerSynchronizer) {
                mQueuedMission = mFloidMissionReadyTimerObject.getPendingMission();
                mMissionQueued = true;
            }
        } else {
            Log.e(TAG, "Mission Read Timer is Null");
        }
    }

    /**
     * The mission start timer update task.
     */
    private final Runnable mMissionStartTimerUpdateTask = new Runnable() {

        public void run() {
            // 1. Calculate the current time and see if the number of seconds has changed
            long currentTime = SystemClock.uptimeMillis();
            if(mFloidMissionReadyTimerObject.getStartTime() == 0) {
                mFloidMissionReadyTimerObject.setStartTime(SystemClock.uptimeMillis());
            }
            int currentRemainingSeconds = (mFloidMissionReadyTimerObject.getTimeoutInSeconds() - (int) ((currentTime - mFloidMissionReadyTimerObject.getStartTime()) / 1000));
            // 2. Are we done???
            if (currentRemainingSeconds == 0) {
                // Yup - cancel the timer (changes the views) and set the new mission
                cancelMissionTimer();
                synchronized (mFloidMissionTimerSynchronizer) {
                    mNewMission = mFloidMissionReadyTimerObject.getPendingMission();
                    mNewMissionIsReady = true;
                    mFloidMissionReadyTimerObject = null;
                }
                // We do not re-fire the timer and we are done
            } else {
                // 3. Do we have a new value?
                if (currentRemainingSeconds != mFloidMissionReadyTimerObject.getLastSecondsDisplayed()) {
                    // Tell the ux to update the mission timer:
                    uxSendUpdateMissionTimer(currentRemainingSeconds);
                    // Save the new time
                    mFloidMissionReadyTimerObject.setLastSecondsDisplayed(currentRemainingSeconds);
                    // Speak the timer's value:
                    serviceSendSpeakCommand(Integer.toString(currentRemainingSeconds));
                }
                mFloidServiceHandler.postDelayed(mMissionStartTimerUpdateTask, 100);        // Start the timer again...
            }
        }
    };

    private void cancelMissionTimerFromUx() {
        cancelMissionTimer();
        synchronized (mFloidMissionTimerSynchronizer) {
            mCancelledMission = mFloidMissionReadyTimerObject.getPendingMission();
            mMissionCancelled = true;
        }
    }

    /**
     * Cancel mission timer.
     */
    private void cancelMissionTimer() {
        if(mUseLogger)
            Log.d(TAG, "Service -> Cancelling Mission Timer");
        if (mInMissionTimerMode) {
            // Cancel any current timer:
            mFloidServiceHandler.removeCallbacks(mMissionStartTimerUpdateTask);
            mInMissionTimerMode = false;
            uxSendCancelMissionTimer();
        }
    }
    /**
     * Add floid debug message.
     *
     * @param tag     the tag
     * @param message the message
     */
    @SuppressWarnings("SameParameterValue")
    public void addFloidDebugMessage(String tag, String message) {
        synchronized (mFloidDebugMessageQueue) {
            mFloidDebugMessageQueue.add(new FloidDebugMessageAndTag(tag, message));
        }
    }

    /**
     * Add system log message.
     *
     * @param systemLog the system log
     */
    public void addSystemLogMessage(String systemLog) {
        synchronized (mFloidSystemLogQueue) {
            mFloidSystemLogQueue.add(new FloidSystemLog(systemLog));
        }
    }
    /**
     * Speak string.
     *
     * @param stringToSpeak the string to speak
     */
    private void speakString(String stringToSpeak) {
        mTextToSpeech.speak(stringToSpeak, TextToSpeech.QUEUE_FLUSH, null, "");
    }
    /**
     * Set the floid status
     * @param floidStatus the floid status
     */
    public void setFloidStatus(FloidStatusParcelable floidStatus) {
        synchronized (mFloidStatusQueue) {
            if(mFloidStatusQueue.size() > 0) {
                Log.i(TAG, "FloidStatusQueue++ " + mFloidStatusQueue.size() + "->" + (mFloidStatusQueue.size() + 1));
            }
            mFloidStatusQueue.add(floidStatus);
        }
    }

    /**
     * Sets floid model parameters.
     * @param floidModelParameters the floid model parameters
     */
    public void setFloidModelParameters(FloidModelParametersParcelable floidModelParameters) {
        synchronized (mFloidModelParametersQueue) {
            if(mFloidModelParametersQueue.size() > 0) {
                Log.i(TAG, "FloidModelParametersQueue++ " + mFloidModelParametersQueue.size() + "->" + (mFloidModelParametersQueue.size() + 1));
            }
            mFloidModelParametersQueue.add(floidModelParameters);
        }
    }

    /**
     * Sets floid model status.
     *
     * @param floidModelStatus the floid model status
     */
    public void setFloidModelStatus(FloidModelStatusParcelable floidModelStatus) {
        synchronized (mFloidModelStatusQueue) {
            if(mFloidModelStatusQueue.size() > 0) {
                Log.i(TAG, "FloidModelStatusQueue++ " + mFloidModelStatusQueue.size() + "->" + (mFloidModelStatusQueue.size() + 1));
            }
            mFloidModelStatusQueue.add(floidModelStatus);
        }
    }

    /**
     * Gets floid status.
     *
     * @return the floid status
     */
    public FloidStatusParcelable getFloidStatus() {
        return mFloidStatus;
    }

    /**
     * Sets last floid status update time.
     *
     * @param lastFloidStatusUpdateTime the last floid status update time
     */
    @SuppressWarnings("unused")
    public void setLastFloidStatusUpdateTime(long lastFloidStatusUpdateTime) {
        mLastFloidStatusUpdateTime = lastFloidStatusUpdateTime;
    }

    /**
     * Gets last floid status update time.
     *
     * @return the last floid status update time
     */
    public long getLastFloidStatusUpdateTime() {
        return mLastFloidStatusUpdateTime;
    }

    /**
     * Gets floid droid goal id.
     *
     * @return the floid droid goal id
     */
    @SuppressWarnings("unused")
    public int getFloidDroidGoalID() {
        return mFloidDroidGoalID;
    }

    /**
     * Gets floid status update time.
     *
     * @return the floid status update time
     */
    @SuppressWarnings("unused")
    public long getFloidStatusUpdateTime() {
        return mLastFloidStatusUpdateTime;
    }

    /**
     * Gets last floid status number.
     *
     * @return the last floid status number
     */
    @SuppressWarnings("unused")
    public int getLastFloidStatusNumber() {
        return mLastFloidStatusNumber;
    }

    /**
     * Gets next floid goal id.
     *
     * @return the next floid goal id
     */
    public int getNextFloidGoalId() {
        return mFloidGoalId++;
    }

    /**
     * Gets floid droid status.
     *
     * @return the floid droid status
     */
    public FloidDroidStatus getFloidDroidStatus() {
        return mFloidDroidStatus;
    }

    /**
     * Arduino byte array offset to float.
     *
     * @param bytes the bytes
     * @param start the start
     * @return the float
     */
    public static float arduinoByteArrayOffsetToFloat(byte[] bytes, int start) {
        //noinspection PointlessBitwiseExpression
        return Float.intBitsToFloat((((int) bytes[start + 3]) & 0x000000ff) << 24
                | (((int) bytes[start + 2]) & 0x000000ff) << 16
                | (((int) bytes[start + 1]) & 0x000000ff) << 8
                | (((int) bytes[start]) & 0x000000ff) << 0);
    }

    /**
     * Arduino byte array offset to boolean.
     *
     * @param bytes the bytes
     * @param start the start
     * @return the boolean
     */
    public static boolean arduinoByteArrayOffsetToBoolean(byte[] bytes, int start) {
        return bytes[start] > 0;
    }

    /**
     * Arduino byte array offset to byte int.
     *
     * @param bytes the bytes
     * @param start the start
     * @return the int
     */
    @SuppressWarnings("SameParameterValue")
    public static int arduinoByteArrayOffsetToByteInt(byte[] bytes, int start) {
        return (((int) bytes[start]) & 0x000000ff);
    }

    /**
     * Arduino byte array offset to signed byte int.
     *
     * @param bytes the bytes
     * @param start the start
     * @return the int
     */
    public static int arduinoByteArrayOffsetToSignedByteInt(byte[] bytes, int start) {
        return bytes[start];
    }

    /**
     * Arduino byte array offset for unsigned short to produce int
     * @param bytes the bytes
     * @param start the start
     * @return the int
     */
    // Takes an unsigned short and produces an int:
    @SuppressWarnings("SameParameterValue")
    public static int arduinoByteArrayOffsetToShortInt(byte[] bytes, int start) {
        //noinspection PointlessArithmeticExpression
        return (((int) bytes[start + 1]) & 0x000000ff) << 8 | (((int) bytes[start + 0]) & 0x000000ff);
    }

    /**
     * Arduino byte array offset to long int
     * @param bytes the bytes
     * @param start the start
     * @return the int
     */
    @SuppressWarnings("SameParameterValue")
    public static int arduinoByteArrayOffsetToLongInt(byte[] bytes, int start) {
        //noinspection PointlessBitwiseExpression,PointlessArithmeticExpression
        return (((int) bytes[start + 3]) & 0x000000ff) << 24
                | (((int) bytes[start + 2]) & 0x000000ff) << 16
                | (((int) bytes[start + 1]) & 0x000000ff) << 8
                | (((int) bytes[start + 0]) & 0x000000ff) << 0;
    }

    /**
     * Open accessory.
     *
     * @param accessory the accessory
     */
    private void openAccessory(UsbAccessory accessory) {
        if (mUseLogger) Log.i(TAG, "Open Accessory");
        synchronized (mAccessoryStatusSynchronizer) {
            mNewAccessoryAvailableFlag = true;
            mNewAccessory = accessory;
            mCloseAccessoryFlag = true;
        }
    }

    /**
     * Close accessory.
     */
    private void closeAccessory() {
        if (mUseLogger) Log.i(TAG, "Closing Accessory");
        synchronized (mAccessoryStatusSynchronizer) {
            mCloseAccessoryFlag = true;
        }
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mUseLogger) Log.i(TAG, "Configuration changed.");
    }

    /**
     * Sets floid droid gps from location.
     *
     * @param location the location
     */
    private void setFloidDroidGpsFromLocation(Location location) {
        // GPS:
        mFloidDroidStatus.setGpsStatusTime(location.getTime());
        mFloidDroidStatus.setGpsHasAccuracy(location.hasAccuracy());
        mFloidDroidStatus.setGpsAccuracy(location.getAccuracy());
        mFloidDroidStatus.setGpsLatitude(location.getLatitude());
        mFloidDroidStatus.setGpsLongitude(location.getLongitude());
        mFloidDroidStatus.setGpsHasAltitude(location.hasAltitude());
        mFloidDroidStatus.setGpsAltitude(location.getAltitude());
        mFloidDroidStatus.setGpsHasBearing(location.hasBearing());
        mFloidDroidStatus.setGpsBearing(location.getBearing());
        mFloidDroidStatus.setGpsSpeed(location.getSpeed());
        mFloidDroidStatus.setGpsProvider(location.getProvider());
        if (mUseLogger)
            Log.v(TAG, "Droid: Lat: " + mFloidDroidStatus.getGpsLatitude() + " Lng: " + mFloidDroidStatus.getGpsLongitude());
    }

    // TODO - [DROID GPS]  - IF YOU DO NOT HAVE HIGH ACCURACY (OR MAYBE GPS ONLY) TURNED ON, YOU GET A MESSAGE - IS THIS IMPORTANT???
    /**
     * Droid location listener - used to get the droid's location periodically (default 400ms)
     */
    final LocationListener droidLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            setFloidDroidGpsFromLocation(location);
        }
        public void onProviderEnabled(String provider) {
            Toast.makeText(getApplicationContext(), "Enabled new provider " + provider,
                    Toast.LENGTH_SHORT).show();
        }
        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), "Disabled provider " + provider,
                    Toast.LENGTH_SHORT).show();
        }
    };

    /**
     * Network location listener - used to get the droid's location periodically from the network (most accurate) - for use when emulating gps?
     */
    final LocationListener networkLocationListener = new LocationListener() {
        // Called when a new location is found by the network location provider.
        public void onLocationChanged(Location location) {
            // Location From device?
            if (mFloidStatus.isDv()) {
                try {
                    sendDeviceGpsCommandToFloid((float) location.getLatitude(), (float) location.getLongitude(), (float) location.getAltitude(), (float) 0.9);
                } catch (Exception e) {
                    if (mUseLogger)
                        Log.e(TAG, "Failed to make/send Test location from network: " + e.getMessage() + " " + Log.getStackTraceString(e));
                }
            }
        }
        public void onProviderEnabled(String provider) {
        }
        public void onProviderDisabled(String provider) {
        }
    };

    /**
     * Droid location listener - used to get the droid's location periodically from the gos - it is not used but is left here in case you want to use this instead of network for gos emulation
     */
    final LocationListener gpsLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // Location From device?
            if (mFloidStatus.isDv()) {
                //noinspection EmptyTryBlock
                try {
                    // Note: This is commented out on purpose:
                    //                        sendDeviceGpsCommandToFloid((float)location.getLatitude(), (float)location.getLongitude(), (float)location.getAltitude(), (float)0.9);
                } catch (Exception e) {
                    if (mUseLogger)
                        Log.e(TAG, "Failed to make/send Test location from gps: " + e.getMessage() + " " + Log.getStackTraceString(e));
                }
            }
        }
        public void onProviderEnabled(String provider) {
        }
        public void onProviderDisabled(String provider) {
        }
    };
    // Utility routine to reset the floid droid parameters:

    /**
     * Reset floid droid parameters.
     */
    public void resetFloidDroidParameters() {
        // Clear the parameters map:
        mDroidParametersHashMap.clear();
        // Set the parameters from the default ones and call the callbacks
        for (Map.Entry<String, String> droidDefaultParameterEntry : mDroidDefaultParametersHashMap.entrySet()) {
            setParameter(droidDefaultParameterEntry.getKey(), droidDefaultParameterEntry.getValue());
        }
    }

    /**
     * Return a nill safe String as a map entry given a key - default is ""
     * @param map the map of Strings
     * @param key the key
     * @return the String from the map or "" if non-existent
     */
    public static String safeMapStringGet(Map<String, String> map, String key) {
        String value = map.get(key);
        return value!=null?value:"";
    }

    /**
     * Sets floid droid instance members from parameters.
     */
    public void setFloidDroidInstanceMembersFromParameters() {
        // This method sets the Floid Droid instance members from the parameters:
        try {
            // mFloidDroidStatusUpdateRate:
            String updateRateFloidDroidStatusString = safeMapStringGet(mDroidParametersHashMap, DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_OPTION);

            if (updateRateFloidDroidStatusString.equals(DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_DEFAULT)) {
                mFloidDroidStatusUpdateRate = mDroidParametersValidValues.get(DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_OPTION).getIntDefault();
            } else {
                try {
                    mFloidDroidStatusUpdateRate = mDroidParametersValidValues.get(DROID_PARAMETERS_UPDATE_RATE_DROID_STATUS_OPTION).getAsValidInt(updateRateFloidDroidStatusString);
                } catch (Exception e) {
                    Log.e(TAG, "Bad Floid Status Update Rate Parameter: " + updateRateFloidDroidStatusString);
                }
            }
            // mFloidStatusUpdateRate:
            String updateRateFloidStatusString = safeMapStringGet(mDroidParametersHashMap, DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_OPTION);
            if (updateRateFloidStatusString.equals(DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_DEFAULT)) {
                mFloidStatusUpdateRate = mDroidParametersValidValues.get(DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_OPTION).getIntDefault();
            } else {
                try {
                    mFloidStatusUpdateRate = mDroidParametersValidValues.get(DROID_PARAMETERS_UPDATE_RATE_FLOID_STATUS_OPTION).getAsValidInt(updateRateFloidStatusString);
                } catch (Exception e) {
                    Log.e(TAG, "Bad Floid Status Update Rate Parameter: " + updateRateFloidStatusString);
                }
            }
            // mFloidModelStatusUpdateRate:
            String updateRateFloidModelStatusString = safeMapStringGet(mDroidParametersHashMap, DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_OPTION);
            if (updateRateFloidModelStatusString.equals(DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_DEFAULT)) {
                mFloidStatusUpdateRate = mDroidParametersValidValues.get(DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_OPTION).getIntDefault();
            } else {
                try {
                    mFloidModelStatusUpdateRate = mDroidParametersValidValues.get(DROID_PARAMETERS_UPDATE_RATE_MODEL_STATUS_OPTION).getAsValidInt(updateRateFloidModelStatusString);
                } catch (Exception e) {
                    Log.e(TAG, "Bad Floid Model Status Update Rate Parameter: " + updateRateFloidModelStatusString);
                }
            }
            // mFloidModelParametersUpdateRate:
            String updateRateFloidModelParametersString = safeMapStringGet(mDroidParametersHashMap, DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_OPTION);
            if (updateRateFloidModelParametersString.equals(DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_DEFAULT)) {
                mFloidStatusUpdateRate = mDroidParametersValidValues.get(DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_OPTION).getIntDefault();
            } else {
                try {
                    mFloidModelParametersUpdateRate = mDroidParametersValidValues.get(DROID_PARAMETERS_UPDATE_RATE_MODEL_PARAMETERS_OPTION).getAsValidInt(updateRateFloidModelParametersString);
                } catch (Exception e) {
                    Log.e(TAG, "Bad Floid Model Parameters Update Rate Parameter: " + updateRateFloidModelParametersString);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "setFloidDroidInstanceMembersFromParameters", e);
        }
    }

    /**
     * Sets floid droid parameters.
     *
     * @param floidDroidParameters the floid droid parameters - space (or \n) separated property=value entries.
     *                             each property has format: ([a-z._A-Z]+=VALUE)
     *                             e.g. "droid.parameter.a=DEFAULT droid.parameter.b=100"
     * @return the floid droid parameters
     */
    public boolean setFloidDroidParameters(String floidDroidParameters) {
        // Parse the parameters and set into the model:
        try {
            Log.d(TAG, "Parameters: " + floidDroidParameters);
            if (floidDroidParameters == null || floidDroidParameters.length() == 0) {
                return true; // All good
            }
            String floidDroidParametersCleaned = floidDroidParameters.replace('\n', ' ').replace('\r', ' ');
            String[] floidDroidParametersArray = floidDroidParametersCleaned.split(" ");
            for (String parameter : floidDroidParametersArray) {
                // OK, split the parameter on "="
                String[] parameterArray = parameter.split("=", 2);
                if (parameterArray.length == 2) {
                    // Key not empty?
                    if (parameterArray[0].length() > 0) {
                        // OK check the first element (the key) for correct RegExp: (a-z and . only):\
                        String parameterRegExp = "[a-z._A-Z]+";
                        if (parameterArray[0].matches(parameterRegExp)) {
                            // OK, is the second one non-empty?
                            if (parameterArray[1].length() > 0) {
                                // Set or modify the parameter value:
                                String parameterKey = parameterArray[0];
                                String parameterValue = parameterArray[1];
                                setParameter(parameterKey, parameterValue);
                            } else {
                                if (mUseLogger)
                                    Log.i(TAG, "Error parsing droid parameter: (empty value): " + parameter);
                            }
                        } else {
                            // Log the error and return false:
                            if (mUseLogger)
                                Log.i(TAG, "Error parsing droid parameter: (key is a-z, A-z, _ or . only): " + parameter);
                            return false;
                        }
                    } else {
                        if (mUseLogger)
                            Log.i(TAG, "Error parsing droid parameter: (empty key): " + parameter);
                        return false;
                    }
                } else {
                    if (mUseLogger)
                        Log.i(TAG, "Error parsing droid parameter: (no = sign): " + parameter);
                    return false;
                }
            }
            // Parameters are set, put them into the instances where appropriate:
            setFloidDroidInstanceMembersFromParameters();
            return true;
        } catch (Exception e) {
//            if (mUseLogger)
                Log.e(TAG, "setFloidDroidParameters; exception: " + e.toString());
            return false;
        }
    }

    private void setParameter(String parameterKey, String parameterValue) {
        String originalParameterValue = mDroidParametersHashMap.get(parameterKey);
        // Call pre-modification handler:
        if (floidParameterModifiedHandlerMap.containsKey(parameterKey)) {
            FloidParameterModifiedHandler floidParameterModifiedHandler = floidParameterModifiedHandlerMap.get(parameterKey);
            if(floidParameterModifiedHandler!=null) {
                floidParameterModifiedHandler.parameterModifiedPre(parameterKey, parameterValue, this);
            }
        }
        // Modify:
        mDroidParametersHashMap.put(parameterKey, parameterValue);
        if (mUseLogger) Log.i(TAG, "[SetParameter] " + parameterKey + " : " + (originalParameterValue!=null?originalParameterValue:"NULL") + " -> " + (parameterValue!=null?parameterValue:"NULL"));
        // Call post-modification handler:
        if (floidParameterModifiedHandlerMap.containsKey(parameterKey)) {
            FloidParameterModifiedHandler floidParameterModifiedHandler = floidParameterModifiedHandlerMap.get(parameterKey);
            if(floidParameterModifiedHandler!=null) {
                floidParameterModifiedHandler.parameterModifiedPost(parameterKey, originalParameterValue, this);
            }
        }
    }

    private boolean getLicense()
    {
        //noinspection RedundantIfStatement, ConstantConditions, SimplifiableIfStatement
        if(BuildConfig.FLAVOR.equals("demo")) {
            return false;
        }
        // TODO - [LICENSE] implement license restriction
        return true;
    }

    /**
     * Get the app time stamp
     * @param context the context
     * @return the formatted app timestamp
     */
    @SuppressWarnings("unused")
    private String getAppTimeStamp(Context context) {
        String timeStamp = "";
        try {
            ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            String appFile = appInfo.sourceDir;
            long time = new File(appFile).lastModified();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.US);
            timeStamp = formatter.format(time);
        } catch (Exception e) {
            Log.e(TAG, "Exception getting app timestamp", e);
        }
        return timeStamp;
    }

    private void logPermissions() {
        int cameraPermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        Log.d(TAG, "Floid Service Permission: " + Manifest.permission.CAMERA + (cameraPermissionGranted==PackageManager.PERMISSION_GRANTED?" Granted":" Denied") );

        int fineLocationPermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        Log.d(TAG, "Floid Service Permission: " + Manifest.permission.ACCESS_FINE_LOCATION + (fineLocationPermissionGranted==PackageManager.PERMISSION_GRANTED?" Granted":" Denied") );

        int coarseLocationPermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        Log.d(TAG, "Floid Service Permission: " + Manifest.permission.ACCESS_COARSE_LOCATION + (coarseLocationPermissionGranted==PackageManager.PERMISSION_GRANTED?" Granted":" Denied") );

        int writeExternalStoragePermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        Log.d(TAG, "Floid Service Permission: " + Manifest.permission.WRITE_EXTERNAL_STORAGE + (writeExternalStoragePermissionGranted==PackageManager.PERMISSION_GRANTED?" Granted":" Denied") );

        int readExternalStoragePermissionGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        Log.d(TAG, "Floid Service Permission: " + Manifest.permission.READ_EXTERNAL_STORAGE + (readExternalStoragePermissionGranted==PackageManager.PERMISSION_GRANTED?" Granted":" Denied") );
    }

    /* (non-Javadoc)
     * @see android.app.Service#onCreate()
     */
    @Override
    @SuppressLint("WakelockTimeout")
    public void onCreate() {
        super.onCreate();
        // Set strict mode behavior - detects leaking objects:
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder(StrictMode.getVmPolicy())
                .detectLeakedClosableObjects()
                .build());
        // Log build flavor:
        Log.d(TAG, "Floid Build Flavor: " + BuildConfig.FLAVOR);
        mFloidServicePid = Process.myPid();
        Log.d(TAG, "Service PID: " + mFloidServicePid);
        // Log permissions for service here:
        logPermissions();
        // Set up the notification:
        setupNotification();
        // Acquire Wifi Lock:
        Log.i(TAG, "Acquiring WiFi Lock...");
        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            if(wifiManager != null) {
                floidServiceWifiLock = wifiManager.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "com.faiglelabs.floidservice");
                if (floidServiceWifiLock != null) {
                    floidServiceWifiLock.acquire();
                    Log.i(TAG, "WiFi Lock Acquired...");
                } else {
                    Log.e(TAG, "Exception acquiring WiFi Lock - disabling");
                    floidServiceWifiLock = null;
                }
            } else {
                Log.e(TAG, "Wifi Manager is null - disabling WiFi Lock");
                floidServiceWifiLock = null;
            }
        } catch(Exception e) {
            Log.e(TAG, "Exception acquiring Wifi Lock - disabling");
            floidServiceWifiLock = null;
        }
        // Acquire Wake Lock:
        Log.i(TAG, "Acquiring Wake Lock...");
        try {
            PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
            if(powerManager != null) {
                floidServiceWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "com.faiglelabs.floidservice:wakelock");
                if(floidServiceWakeLock != null) {
                    floidServiceWakeLock.acquire();
                    Log.i(TAG, "Wake Lock Acquired...");
                }
            } else {
                Log.e(TAG, "Power Manager is null - disabling Wake Lock");
                floidServiceWakeLock = null;
            }
        } catch(Exception e) {
            Log.e(TAG, "Exception acquiring Wake Lock - disabling");
            floidServiceWakeLock = null;
        }
        floidLicense = getLicense();
        // Retrieve out of storage:
        retrieveFloidId();
        retrieveHostIpAndPort();
        // Explicitly load the imaging class:
        try {
            Class.forName(FloidImaging.class.getName());
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "Error loading Floid Imaging class - camera will be unavailable", e);
        }
        // Reset the parameters:
        resetFloidDroidParameters();
        // And set the instances from the parameters:
        setFloidDroidInstanceMembersFromParameters();
        // Set up the sounds:
        setupSounds();
        playNotification(FLOID_ALERT_SOUND_SERVICE_CREATE);

        // Set up the broadcast receiver and functionality to kill the service from an intent:
        IntentFilter killServiceIntentFilter = new IntentFilter();
        killServiceIntentFilter.addAction(KILL_SERVICE_INTENT_ACTION);
        killServiceBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // See if it was our intent action:
                if (KILL_SERVICE_INTENT_ACTION.equals(intent.getAction())) {
                    Log.i(TAG, "Service <-- Received Kill Service Intent");
                    // Cancel the floid droid task:
                    if(mFloidDroidTask!=null) {
                        mFloidDroidTask.cancel(false);
                        mFloidDroidTask = null;
                    }
                    // Cancel the accessory communicator task:
                    if(mCommunicationMonitorTask !=null) {
                        mCommunicationMonitorTask.cancel(false);
                        mCommunicationMonitorTask = null;
                    }
                    quitting = true;
                    stopSelf();
                    // If we are bound, send an intent to the app to quit and unbind...
                    Intent quitFloidActivityIntent = new Intent(FloidActivity.QUIT_ACTIVITY_INTENT_ACTION);
                    sendBroadcast(quitFloidActivityIntent);
                } else {
                    Log.e(TAG, "Service <-- Received Non-Kill Service Intent");
                }
            }
        };
        registerReceiver(killServiceBroadcastReceiver, killServiceIntentFilter);
        // Text to speech:
        mTextToSpeech = new TextToSpeech(this, this);
        // Database and id:
        setupDatabase();
        // Battery status:
        setupBatteryStatus();
        // Location:
        setupLocation();
        // Camera:
        mFloidImaging = new FloidImaging(FloidService.this, this, mFloidDroidStatus.getFloidId());
        // Set up accessory:
        setupAccessory();
        // Connect to accessory:
        connectAccessory();
        // Create a new runnable for our logic and initialize it, and then fire it off
        startFloidDroidThread();
        // Create an accessory monitor thread:
        mCommunicationMonitorRunnable = new Runnable() {

            FloidRunnableStatus floidRunnableStatus = new FloidRunnableStatus();
            @Override
            public void run() {
                long currentTime = System.currentTimeMillis();
                // 1. Check the accessory communicator:
                if (mAccessoryCommunicatorTask != null) {
                    // If this was not already null, then this is a state change:
                    if (floidRunnableStatus.accessoryCommunicatorRunnableNull) {
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_NULL, 0);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_ALIVE, 1);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_RUNNING, 0);
                        uxSendCommStatus(FloidActivity.COMM_FLOID, FloidActivity.COMM_ERROR);
                        uxSendFloidMessageToDisplay("Accessory communicator has thread\n");
                        floidRunnableStatus.accessoryCommunicatorRunnableNull = false;
                    }
                    if (mAccessoryCommunicatorTask.isDone()) {
                        uxSendFloidMessageToDisplay("Accessory communicator thread is dead\n");
                        mAccessoryCommunicatorTask = null;
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_NULL, 1);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_ALIVE, 0);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_RUNNING, 0);
                        uxSendCommStatus(FloidActivity.COMM_FLOID, FloidActivity.COMM_ERROR);
                        if (floidRunnableStatus.accessoryCommunicatorRunnableAlive) {
                            // State change:
                            mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_ALIVE, 0);
                            floidRunnableStatus.accessoryCommunicatorRunnableAlive = false;
                        }
                        floidRunnableStatus.accessoryCommunicatorRunnableNull = true;
                    } else {
                        if (!floidRunnableStatus.accessoryCommunicatorRunnableAlive) {
                            // State change:
                            uxSendFloidMessageToDisplay("Accessory communicator thread is alive\n");
                            mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_ALIVE, 1);
                            floidRunnableStatus.accessoryCommunicatorRunnableAlive = true;
                        }
                        // Has the loop count changed?
                        if (mFloidAccessoryCommunicator.getFloidAccessoryCommunicatorRunnableLoopCount() == floidRunnableStatus.lastAccessoryCommunicatorRunnableLoopCount) {
                            long timeSinceAccessoryUpdate = currentTime - floidRunnableStatus.lastAccessoryCommunicatorRunnableUpdate;
                            // Has it been warned yet?
                            if (floidRunnableStatus.accessoryCommunicatorThreadWarned) {
                                if (!floidRunnableStatus.accessoryCommunicatorThreadErrored) {
                                    if (timeSinceAccessoryUpdate > communicationMonitorThreadErrorTime) {
                                        // Error
                                        uxSendFloidMessageToDisplay("Accessory communicator thread error\n");
                                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_RUNNING, 0);
                                        uxSendCommStatus(FloidActivity.COMM_FLOID, FloidActivity.COMM_ERROR);
                                        floidRunnableStatus.accessoryCommunicatorThreadErrored = true;
                                   } // Otherwise we are between warning and error time
                                }
                            } else {
                                if (timeSinceAccessoryUpdate > communicationMonitorThreadWarningTime) {
                                    // Warning
                                    uxSendFloidMessageToDisplay("Accessory communicator thread warning\n");
                                    uxSendCommStatus(FloidActivity.COMM_FLOID, FloidActivity.COMM_WARNING);
                                    floidRunnableStatus.accessoryCommunicatorThreadWarned = true;
                                } // Otherwise we are between no issue and warning time
                            }
                        } else {
                            floidRunnableStatus.lastAccessoryCommunicatorRunnableLoopCount = mFloidAccessoryCommunicator.getFloidAccessoryCommunicatorRunnableLoopCount();
                            if (floidRunnableStatus.accessoryCommunicatorThreadWarned) {
                                // This is a state change
                                uxSendFloidMessageToDisplay("Accessory communicator thread back to good\n");
                                uxSendCommStatus(FloidActivity.COMM_FLOID, FloidActivity.COMM_GOOD);
                                mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_RUNNING, 1);
                                floidRunnableStatus.accessoryCommunicatorThreadWarned = false;
                                floidRunnableStatus.accessoryCommunicatorThreadErrored = false;
                            } else {
                                if (mAdditionalStatus.get(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_RUNNING) == 0) {
                                    // This is a state change
                                    uxSendFloidMessageToDisplay("Accessory communicator thread is running\n");
                                    mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_RUNNING, 1);
                                    uxSendCommStatus(FloidActivity.COMM_FLOID, FloidActivity.COMM_GOOD);
                                }
                            }
                            floidRunnableStatus.lastAccessoryCommunicatorRunnableUpdate = currentTime;
                        }
                    }
                } else {
                    // If this was not already null, then this is a state change:
                    if (!floidRunnableStatus.accessoryCommunicatorRunnableNull) {
                        uxSendFloidMessageToDisplay("Accessory communicator thread is now null\n");
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_NULL, 0);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_ALIVE, 0);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_COMMUNICATOR_IS_RUNNING, 0);
                        uxSendCommStatus(FloidActivity.COMM_FLOID, FloidActivity.COMM_ERROR);
                        floidRunnableStatus.accessoryCommunicatorRunnableNull = true;
                    }
                    floidRunnableStatus.lastAccessoryCommunicatorRunnableUpdate = currentTime;
                }
                // 2. Check the Floid Droid Runnable:
                if (mFloidDroidThread != null) {
                    // If this was not already null, then this is a state change:
                    if (floidRunnableStatus.floidDroidRunnableNull) {
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_NULL, 0);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_ALIVE, 1);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_RUNNING, 0);
                        uxSendCommStatus(FloidActivity.COMM_DROID, FloidActivity.COMM_ERROR);
                        uxSendFloidMessageToDisplay("Floid droid has thread\n");
                        floidRunnableStatus.floidDroidRunnableNull = false;
                    }
                    if (!mFloidDroidThread.isAlive()) {
                        uxSendFloidMessageToDisplay("Floid droid thread is dead\n");
                        mFloidDroidThread = null;
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_NULL, 1);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_ALIVE, 0);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_RUNNING, 0);
                        uxSendCommStatus(FloidActivity.COMM_DROID, FloidActivity.COMM_ERROR);
                        if (floidRunnableStatus.floidDroidRunnableAlive) {
                            // State change:
                            mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_ALIVE, 0);
                            floidRunnableStatus.floidDroidRunnableAlive = false;
                        }
                        floidRunnableStatus.floidDroidRunnableNull = true;
                    } else {
                        if (!floidRunnableStatus.floidDroidRunnableAlive) {
                            // State change:
                            uxSendFloidMessageToDisplay("Floid droid thread is alive\n");
                            mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_ALIVE, 1);
                            floidRunnableStatus.floidDroidRunnableAlive = true;
                        }
                        // Has the loop count changed?
                        if (mFloidDroidRunnable.getDroidRunnableLoopCount() == floidRunnableStatus.lastFloidDroidRunnableLoopCount) {
                            long timeSinceFloidDroidUpdate = currentTime - floidRunnableStatus.lastFloidDroidRunnableUpdate;
                            // Has it been warned yet?
                            if (floidRunnableStatus.floidDroidThreadWarned) {
                                if (!floidRunnableStatus.floidDroidThreadErrored) {
                                    if (timeSinceFloidDroidUpdate > communicationMonitorThreadErrorTime) {
                                        // Error
                                        uxSendFloidMessageToDisplay("Floid droid thread error\n");
                                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_RUNNING, 0);
                                        uxSendCommStatus(FloidActivity.COMM_DROID, FloidActivity.COMM_ERROR);
                                        floidRunnableStatus.floidDroidThreadErrored = true;
                                        String stackTrace = getStackTrace(mFloidDroidThread);
                                        uxSendFloidMessageToDisplay("Stack:\n" + stackTrace);
                                    }  // Otherwise we are between warning and error time
                                }
                            } else {
                                if (timeSinceFloidDroidUpdate > communicationMonitorThreadWarningTime) {
                                    // Warning
                                    uxSendFloidMessageToDisplay("Floid droid thread warning\n");
                                    uxSendCommStatus(FloidActivity.COMM_DROID, FloidActivity.COMM_WARNING);
                                    floidRunnableStatus.floidDroidThreadWarned = true;
                                    String stackTrace = getStackTrace(mFloidDroidThread);
                                    uxSendFloidMessageToDisplay("Stack:\n" + stackTrace);
                                } // Otherwise we are between no issue and warning time
                            }
                        } else {
                            floidRunnableStatus.lastFloidDroidRunnableLoopCount = mFloidDroidRunnable.getDroidRunnableLoopCount();
                            if (floidRunnableStatus.floidDroidThreadWarned) {
                                // This is a state change
                                uxSendFloidMessageToDisplay("Floid droid thread back to good\n");
                                mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_RUNNING, 1);
                                uxSendCommStatus(FloidActivity.COMM_DROID, FloidActivity.COMM_GOOD);
                                floidRunnableStatus.floidDroidThreadWarned = false;
                                floidRunnableStatus.floidDroidThreadErrored = false;
                            } else {
                                if (mAdditionalStatus.get(AdditionalStatus.THREAD_FLOID_DROID_IS_RUNNING) == 0) {
                                    // This is a state change
                                    uxSendFloidMessageToDisplay("Floid droid thread is running\n");
                                    mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_RUNNING, 1);
                                    uxSendCommStatus(FloidActivity.COMM_DROID, FloidActivity.COMM_GOOD);
                                }
                            }
                            floidRunnableStatus.lastFloidDroidRunnableUpdate = currentTime;
                        }
                    }
                } else {
                    // If this was not already null, then this is a state change:
                    if (!floidRunnableStatus.floidDroidRunnableNull) {
                        uxSendFloidMessageToDisplay("Floid droid thread is now null\n");
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_NULL, 0);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_ALIVE, 0);
                        mAdditionalStatus.set(AdditionalStatus.THREAD_FLOID_DROID_IS_RUNNING, 0);
                        floidRunnableStatus.floidDroidRunnableNull = true;
                    }
                    floidRunnableStatus.lastFloidDroidRunnableUpdate = currentTime;
                }
            }
        };
        // Schedule the communication monitor task
        mCommunicationMonitorTask = mCommunicationMonitorTaskExecutor.scheduleAtFixedRate(() -> {
            try {
                mCommunicationMonitorRunnable.run();
            } catch (Exception e) {
                Log.e(TAG, "Exception: Communication Monitor ", e);
            }
        }, 0, COMMUNICATIONS_MONITOR_EXECUTION_PERIOD, TimeUnit.MILLISECONDS);
        if (mUseLogger) {
            Log.d(TAG, "Done onCreate");
        }
    }

    private String getStackTrace(Thread thread) {
        StringBuilder stackTraceStringBuilder = new StringBuilder();
        final StackTraceElement[] stackTrace = thread.getStackTrace();
        for (StackTraceElement stackTraceElement : stackTrace) {
            stackTraceStringBuilder.append(stackTraceElement.getClassName())
                    .append(" ")
                    .append(stackTraceElement.getMethodName())
                    .append(":")
                    .append(stackTraceElement.getLineNumber())
                    .append("\n");
        }
        return stackTraceStringBuilder.toString();
    }

    /**
     * Sets up sounds.
     */
    private void setupSounds() {
        // Set up sounds:
        mFloidStatusReceivedMediaPlayer = MediaPlayer.create(this, R.raw.floid_status_received);
        mDebugMessageReceivedMediaPlayer = MediaPlayer.create(this, R.raw.debug_message_received);
        mFloidConnectedMediaPlayer = MediaPlayer.create(this, R.raw.floid_connected);
        mFloidDisconnectedMediaPlayer = MediaPlayer.create(this, R.raw.floid_disconnected);
        mCommandResponseReceivedMediaPlayer = MediaPlayer.create(this, R.raw.command_response_received);
        mModelStatusReceivedMediaPlayer = MediaPlayer.create(this, R.raw.model_status_received);
        mModelParametersReceivedMediaPlayer = MediaPlayer.create(this, R.raw.model_parameters_received);
        mServerConnectedMediaPlayer = MediaPlayer.create(this, R.raw.server_connected);
        mServerDisconnectedMediaPlayer = MediaPlayer.create(this, R.raw.server_disconnected);
        mTakePhotoMediaPlayer = MediaPlayer.create(this, R.raw.floid_take_photo);
        mStartVideoPlayer = MediaPlayer.create(this, R.raw.floid_start_video);
        mStopVideoMediaPlayer = MediaPlayer.create(this, R.raw.floid_stop_video);
        mSendVideoFrameMediaPlayer = MediaPlayer.create(this, R.raw.floid_take_photo);  // Note: This is same as take photo
        mTestPacketResponseMediaPlayer = MediaPlayer.create(this, R.raw.floid_test_packet_response);
        mStartInterfaceMediaPlayer = MediaPlayer.create(this, R.raw.floid_start_interface);
        mStopInterfaceMediaPlayer = MediaPlayer.create(this, R.raw.floid_stop_interface);
        mStartMissionTimerMediaPlayer = MediaPlayer.create(this, R.raw.floid_mission_starting);
        mBuzzerMediaPlayer = MediaPlayer.create(this, R.raw.floid_buzzer);
        mSentPyrPacketMediaPlayer = MediaPlayer.create(this, R.raw.floid_pyr_packet_sent);
        mServiceCreateMediaPlayer = MediaPlayer.create(this, R.raw.service_created);
        mServiceDestroyMediaPlayer = MediaPlayer.create(this, R.raw.service_destroyed);
    }

    /* (non-Javadoc)
     * @see android.app.Service#onDestroy()
     */
    @Override
    public void onDestroy() {
        Log.i(TAG, "[onDestroy] Floid Service is being destroyed!");
        playNotification(FLOID_ALERT_SOUND_SERVICE_DESTROY);
        // Remove our foreground notification
        stopForeground(true);
        stopBackground();
        super.onDestroy();
    }

    private void stopBackground() {
        // Cancel the floid droid task:
        if(mFloidDroidTask!=null) {
            mFloidDroidTask.cancel(false);
            mFloidDroidTask = null;
        }
        // Cancel the accessory communicator task:
        if(mCommunicationMonitorTask !=null) {
            mCommunicationMonitorTask.cancel(false);
            mCommunicationMonitorTask = null;
        }
        quitting = true;
        if(batteryStatusTimer!=null) {
            try {
                batteryStatusTimer.cancel();
            } catch(Exception e) {
                // Do nothing
            }
        }
        unregisterReceiver(mUsbReceiver);
        unregisterReceiver(killServiceBroadcastReceiver);
        mLocationManager.removeUpdates(droidLocationListener);
        mLocationManager.removeUpdates(networkLocationListener);
        mLocationManager.removeUpdates(gpsLocationListener);
        // Kill the accessory communicator:
        killAccessoryCommunicator();
        // Stop the floid droid thread:
        try {
            if(mFloidDroidThread != null) {
                mFloidDroidThread.interrupt();
                mFloidDroidThread = null;
            }
        } catch (Exception e) {
            Log.e(TAG, "Error stopping floid droid thread: ", e);
        }
        // Stop the imaging:
        try {
            if (mFloidImaging != null) {
                mFloidImaging.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "onDestroy: Caught exception closing floid imaging", e);
            mFloidImaging = null;
        }
        mFloidImaging = null;
        if (mTextToSpeech != null) {
            try {
                mTextToSpeech.shutdown();
            } catch (Exception e) {
                if (mUseLogger) Log.e(TAG, "onDestroy: failed to shut down the text to speech: " + e.getMessage());
            }
        }
        // Cancel notifications:
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(notificationManager!=null) {
            try {
                notificationManager.cancel(FLOID_SERVICE_FOREGROUND_NOTIFICATION_ID);
            } catch (Exception e) {
                if (mUseLogger) Log.e(TAG, "onDestroy: caught exception cancelling notifications: " + e.getMessage());
            }
        }
        // Kill Our WiFi and Wake locks:
        if(floidServiceWifiLock != null) {
            if(floidServiceWifiLock.isHeld()) {
                floidServiceWifiLock.release();
                floidServiceWifiLock= null;
            }
        }
        if(floidServiceWakeLock != null) {
            if(floidServiceWakeLock.isHeld()) {
                floidServiceWakeLock.release();
                floidServiceWakeLock= null;
            }
        }
    }

    /**
     * Kill the accessory communicator
     */
    @SuppressWarnings("BusyWait")
    public void killAccessoryCommunicator() {
        // Signal the accessory to close:
        synchronized (mAccessoryStatusSynchronizer) {
            mCloseAccessoryFlag = true;
        }
        try {
            Thread.sleep(40L); // Wait for the accessory to have a chance to close
            // Do nothing
        } catch (InterruptedException ie) {
        }
        if (mAccessoryCommunicatorTask != null) {
            if (!mAccessoryCommunicatorTask.isDone()) {
                if (!mAccessoryCommunicatorTask.isCancelled()) {
                    mAccessoryCommunicatorTask.cancel(false);
                }
            }
        }
        // Wait for finish:
        while (mAccessoryCommunicatorTask != null && mAccessoryCommunicatorTask.isDone()) {
            try {
                Thread.sleep(40L);
            } catch (InterruptedException ie) {
                // Do nothing
            }
        }
    }

    /**
     * The usb receiver.
     */
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        // Essentially the same:
        @Override
        public void onReceive(Context context, Intent intent) {
            uxSendFloidMessageToDisplay("Usb broadcast received\n");
            if (mUseLogger) Log.e(TAG, "Floid USB Receiver: Usb broadcast received");
            String action = intent.getAction();
            if(action != null) {
                switch (action) {
                    case ACTION_USB_PERMISSION:
                        if (mUseLogger) Log.e(TAG, "Floid USB Receiver: Usb broadcast received: Usb Permission");
                        synchronized (this) {
                            // FUTURE LIBRARY USB CODE:
                            //                    UsbAccessory accessory = UsbManager.getAccessory(intent);
                            // HARDWARE LIBRARY USB CODE:
                            UsbAccessory accessory = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
                            if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                                uxSendFloidMessageToDisplay("Permission granted for accessory. Opening...\n");
                                openAccessory(accessory);
                            } else {
                                uxSendFloidMessageToDisplay("Permission denied for accessory.\n");
                                if (mUseLogger)
                                    Log.i(TAG, "Floid USB Receiver: Permission denied for accessory: " + accessory.getManufacturer() + " " + accessory.getModel() + " " + accessory.getDescription());
                            }
                            mPermissionRequestPending = false;
                        }
                        break;
                    case UsbManager.ACTION_USB_ACCESSORY_ATTACHED:
                        if (mUseLogger) Log.e(TAG, "Floid USB Receiver: USB Broadcast Received: Accessory Attached");
                        try {
                            synchronized (this) {
                                // FUTURE LIBRARY USB CODE:
                                //                    UsbAccessory accessory = UsbManager.getAccessory(intent);
                                // HARDWARE LIBRARY USB CODE:
                                uxSendFloidMessageToDisplay("Usb accessory attached - Opening...\n");
                                UsbAccessory accessory = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
                                openAccessory(accessory);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "ACTION_USB_ACCESSORY_DETACHED - exception", e);
                        }

                break;
                    case UsbManager.ACTION_USB_ACCESSORY_DETACHED:
                        try {
                            if (mUseLogger) Log.e(TAG, "Floid USB Receiver: USB Broadcast Received: Accessory Detached");
                            uxSendFloidMessageToDisplay("Usb accessory detached - Closing...\n");
                            // FUTURE LIBRARY USB CODE:
                            //                UsbAccessory accessory = UsbManager.getAccessory(intent);
                            // HARDWARE LIBRARY USB CODE:
                            UsbAccessory accessory = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
                            if (accessory != null) {
                                closeAccessory();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "ACTION_USB_ACCESSORY_DETACHED - exception", e);
                        }
                        break;
                    default:
                        if (mUseLogger) Log.e(TAG, "Floid USB Receiver: Usb unknown action: " + action);
                        uxSendFloidMessageToDisplay("Usb unknown action: " + action + "\n");
                        break;
                }
            } else {
                Log.e(TAG, "Floid USB Receiver: Broadcast action was null");
            }
        }
    };

    // Text to speech init:
    @Override
    public void onInit(int status) {
        Log.i(TAG, "TTS: Initializing...");
        try {
            if (status == TextToSpeech.SUCCESS) {
                // Check if the locale is already correct:
                Locale preferredLocale = FloidService.getLanguageLocale(FloidService.safeMapStringGet(mDroidParametersHashMap, FloidService.DROID_PARAMETERS_SPEECH_LOCALE_OPTION));
                if (!mTextToSpeech.getVoice().getLocale().equals(preferredLocale)) {
                    // Set the speech locale:
                    int result = mTextToSpeech.setLanguage(FloidService.getLanguageLocale(FloidService.safeMapStringGet(mDroidParametersHashMap, FloidService.DROID_PARAMETERS_SPEECH_LOCALE_OPTION)));
                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e(TAG, "TTS: This Language is not supported");
                    }
                }
                mTextToSpeech.setPitch((float) 0.6);
                mTextToSpeech.setSpeechRate((float) 0.7);
                if (BuildConfig.FLAVOR.equals("demo")) {
                    mTextToSpeech.speak("The Floid Demo Version", TextToSpeech.QUEUE_FLUSH, null, "");
                } else {
                    mTextToSpeech.speak("The Floid", TextToSpeech.QUEUE_FLUSH, null, "");
                }
                Log.i(TAG, "TTS: Initialized");
            } else {
                Log.e(TAG, "TTS: Initialization Failed!");
                mTextToSpeech = null;   // No good init...
            }
        } catch (Exception e) {
            Log.e(TAG, "TTS: Initialization Execption", e);
        }
    }

    /**
     * Gets language locale.
     *
     * @param speechLocaleString the speech locale string
     * @return the language locale
     */
// TTS LOCALE:
    public static Locale getLanguageLocale(String speechLocaleString) {
        Locale speechLocale;
        switch(speechLocaleString) {
            case DROID_PARAMETERS_SPEECH_LOCALE_DEFAULT:
            case DROID_PARAMETERS_SPEECH_LOCALE_US:
                speechLocale = Locale.US;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_CANADA:
                speechLocale = Locale.CANADA;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_CANADA_FRENCH:
                speechLocale = Locale.CANADA_FRENCH;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_CHINA:
                speechLocale = Locale.CHINA;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_CHINESE:
                speechLocale = Locale.CHINESE;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_ENGLISH:
                speechLocale = Locale.ENGLISH;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_FRANCE:
                speechLocale = Locale.FRANCE;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_FRENCH:
                speechLocale = Locale.FRENCH;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_GERMAN:
                speechLocale = Locale.GERMAN;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_GERMANY:
                speechLocale = Locale.GERMANY;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_ITALIAN:
                speechLocale = Locale.ITALIAN;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_ITALY:
                speechLocale = Locale.ITALY;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_JAPAN:
                speechLocale = Locale.JAPAN;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_JAPANESE:
                speechLocale = Locale.JAPANESE;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_KOREA:
                speechLocale = Locale.KOREA;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_KOREAN:
                speechLocale = Locale.KOREAN;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_PRC:
                speechLocale = Locale.PRC;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_ROOT:
                speechLocale = Locale.ROOT;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_SIMPLIFIED_CHINESE:
                speechLocale = Locale.SIMPLIFIED_CHINESE;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_TAIWAN:
                speechLocale = Locale.TAIWAN;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_TRADITIONAL_CHINESE:
                speechLocale = Locale.TRADITIONAL_CHINESE;
                break;
            case DROID_PARAMETERS_SPEECH_LOCALE_UK:
                speechLocale = Locale.UK;
                break;
            default:
                    Log.e(TAG, "getLanguageLocale: Bad Locale String (Reverting to US): " + speechLocaleString);
                speechLocale = Locale.US;
                break;
        }
        return speechLocale;
    }

    /**
     * Sets accessory.
     */
    private void setupAccessory() {
        Log.i(TAG, "Setting Up Accessory");
        // Make sure all are empty:
        mAccessory = null;
        mInputStream = null;
        mOutputStream = null;
        // Register an intent receiver for accessory connections:
        // FUTURE LIBRARY USB CODE:
//        mUsbManager         = UsbManager.getInstance(this);
        // HARDWARE LIBRARY USB CODE:
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        // Log any current accessories:
        UsbAccessory[] accessories = mUsbManager.getAccessoryList();
        if(accessories==null) {
            if (mUseLogger) Log.i(TAG, "accessories is null");
        } else if (accessories.length == 0) {
            if (mUseLogger) Log.i(TAG, "accessories length is zero");
        } else {
            for(int i=0; i<accessories.length; ++i) {
                if (mUseLogger)
                    Log.i(TAG, "Accessory: " + i + " = "
                            + accessories[i].getManufacturer() + " " + accessories[i].getModel()
                            + accessories[i].getVersion() + " " + accessories[i].getDescription()
                    );
            }
        }
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), PendingIntent.FLAG_IMMUTABLE);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED); // Added 1/29/2012 - ctf
        registerReceiver(mUsbReceiver, filter);
        // Open up a thread for accessory communication:
        if (mUseLogger) Log.d(TAG, "Setting Up Floid Accessory Communicator Thread");
        mFloidAccessoryCommunicator = new FloidAccessoryCommunicator(this, floidLicense);
        // Schedule the accessory communicator task
        mAccessoryCommunicatorTask = mAccessoryCommunicatorTaskExecutor.scheduleAtFixedRate(() -> {
            try {
                mFloidAccessoryCommunicator.run();
            } catch (Exception e) {
                Log.e(TAG, "Exception: Accessory Communicator", e);
            }
        }, 0, ACCESSORY_COMMUNICATOR_EXECUTION_PERIOD, TimeUnit.MILLISECONDS);
    }

    /**
     * Connect accessory.
     */
    private void connectAccessory() {
        Log.i(TAG, "Connecting Accessory");
        // We do not want to try to open the accessory if we are already attached to one...
        if (mInputStream != null && mOutputStream != null && mFileDescriptor != null) {
            if (mUseLogger) Log.i(TAG, "Connect accessory called when we are already connected");
            return;
        }
        // Try to connect to The accessory:
        UsbAccessory[] accessories = mUsbManager.getAccessoryList();
        UsbAccessory accessory = (accessories == null ? null : accessories[0]);
        if (accessory != null) {
            if (mUsbManager.hasPermission(accessory)) {
                openAccessory(accessory);
            } else {
                synchronized (mUsbReceiver) {
                    if (!mPermissionRequestPending) {
                        mUsbManager.requestPermission(accessory, mPermissionIntent);
                        mPermissionRequestPending = true;
                    }
                }
            }
        } else {
            if (mUseLogger) Log.i(TAG, "Accessory is null");
        }
    }
    // Tasks:
    private volatile ScheduledFuture<?> mFloidDroidTask = null;
    private final ScheduledExecutorService mFloidDroidTaskExecutor = Executors.newSingleThreadScheduledExecutor();
    private volatile ScheduledFuture<?> mCommunicationMonitorTask = null;
    private final ScheduledExecutorService mCommunicationMonitorTaskExecutor = Executors.newSingleThreadScheduledExecutor();
    private volatile ScheduledFuture<?> mAccessoryCommunicatorTask = null;
    private final ScheduledExecutorService mAccessoryCommunicatorTaskExecutor = Executors.newSingleThreadScheduledExecutor();

    /**
     * Start floid droid thread.
     */
    public void startFloidDroidThread() {
        Log.i(TAG, "Starting Floid Droid Thread");
        mFloidDroidRunnable = new FloidDroidRunnable(FloidService.this, this, mFloidDatabase, floidLicense);
        mFloidDroidTask = mFloidDroidTaskExecutor.scheduleAtFixedRate(() -> {
            try {
                mFloidDroidRunnable.run();
            } catch (Exception e) {
                Log.e(TAG, "Exception: FloidDroidRunnable ", e);
            }
        }, 0, FLOID_DROID_EXECUTION_PERIOD, TimeUnit.MILLISECONDS);
    }

    /**
     * Sets location.
     */
    @SuppressLint("MissingPermission")
    private void setupLocation() {
        Log.i(TAG, "Setting Up Location");
        try {
            // Get the location manager
            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (mLocationManager != null) {
                // Define the criteria how to select the location provider -> use
                // default
                Criteria criteria = new Criteria(); // Never null
                mLocationProvider = mLocationManager.getBestProvider(criteria, false);
                if(mLocationProvider != null) {
                    Location location = mLocationManager.getLastKnownLocation(mLocationProvider);
                    // Initialize the location fields:
                    if (location != null) {
                        if (mUseLogger) {
                            Log.i(TAG, "Provider " + mLocationProvider + " has been selected.");
                        }
                        droidLocationListener.onLocationChanged(location);
                    } else {
                        Log.w(TAG, "Provider " + mLocationProvider + " could not be selected.");
                    }
                    // Request location updates for droid::
                    mLocationManager.requestLocationUpdates(mLocationProvider, 400, 1, droidLocationListener);
                    // Request location updates for network:
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, networkLocationListener);
                    // Request location updates for gps:
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, gpsLocationListener);
                } else {
                    Log.w(TAG, "Location Provider was null");
                }
            } else {
                Log.w(TAG, "Location Manager was null");
            }
        } catch(Exception e) {
            Log.e(TAG, "Caught exception", e);
        }
    }

    /**
     * Retrieve floid id boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("UnusedReturnValue")
    private boolean retrieveFloidId() {
        Log.i(TAG, "Retrieving Floid Id");
        try {
            // Get the preferences
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            // Retrieve the key:
            int floidId = sharedPreferences.getInt(getString(R.string.preference_floid_id),-1);
            if(floidId<0) {
                floidId = new Random().nextInt(100000000);
                saveFloidId(floidId);
            }
            // Set the floid id:
            mFloidDroidStatus.setFloidId(floidId);
            // Send the floid id to the UX:
            uxSendFloidIds();
        } catch (Exception e) {
            if (mUseLogger) Log.e(TAG, "Could not retrieve floid id");
            return false;
        }
        return true;
    }

    /**
     * Save floid id boolean.
     *
     * @param floidId the floid id
     * @return the boolean
     */
    @SuppressWarnings("UnusedReturnValue")
    private boolean saveFloidId(int floidId) {
        Log.i(TAG, "Saving Floid Id");
        try {
            // Get the preferences
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            // Save the key:
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(getString(R.string.preference_floid_id), floidId);
            editor.apply();
            if (mUseLogger) Log.d(TAG, "Saved Floid id.");
        } catch (Exception e) {
            if (mUseLogger) Log.e(TAG, "Failed to save Floid id");
            return false;
        }
        return true;
    }

    /**
     * Retrieve host ip and port boolean.
     *
     * @return the boolean
     */
    @SuppressWarnings("UnusedReturnValue")
    private boolean retrieveHostIpAndPort() {
        Log.i(TAG, "Retrieving Host IP and Port");
        try {
            // Get the preferences
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            mHostAddress = sharedPreferences.getString(getString(R.string.preference_host_address), sDefaultHostAddress);
            mHostPort = sharedPreferences.getInt(getString(R.string.preference_host_port), sDefaultHostPort);
            uxSendHost();
            String hostIpDebugMessage = String.format(Locale.getDefault(), "Floid Server: %s:%d", mHostAddress, mHostPort);
            if (mUseLogger) Log.d(TAG, hostIpDebugMessage);
            uxSendFloidDebugMessage(hostIpDebugMessage);
        } catch (Exception e) {
            if (mUseLogger) Log.e(TAG, "Failed to load Floid Server address");
            uxSendFloidDebugMessage("Failed to load Floid Server address");
            return false;
        }
        return true;
    }

    /**
     * Save host ip and port boolean.
     *
     * @param hostAddress the host address
     * @param hostPort the host port
     * @return the boolean
     */
    @SuppressWarnings("UnusedReturnValue")
    private boolean saveHostIpAndPort(String hostAddress, int hostPort) {
        Log.i(TAG, "Saving floid server address...");
        try {
            // Get the preferences
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            // Save the key:
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(getString(R.string.preference_host_address), hostAddress);
            editor.putInt(getString(R.string.preference_host_port), hostPort);
            editor.apply();
            if (mUseLogger) Log.d(TAG, "Saved floid server address.");
            uxSendFloidDebugMessage("Saved floid server address.");
        } catch (Exception e) {
            if (mUseLogger) Log.e(TAG, "Failed to save floid server address.");
            uxSendFloidDebugMessage("Failed to save floid server address.");
            return false;
        }
        return true;
    }

    /**
     * Sets database.
     */
    @SuppressLint("WrongConstant")
    @SuppressWarnings("ConstantConditions")
    private void setupDatabase() {
        if(mUseFloidDatabase) {
            Log.i(TAG, "Setting Up Database");
            boolean dropTables = false;
            // Open a connection to the database:
            mFloidDatabase = openOrCreateDatabase(FLOID_DATABASE_NAME, SQLiteDatabase.CREATE_IF_NECESSARY, null);
            mFloidDatabase.setLocale(Locale.getDefault());
            mFloidDatabase.setVersion(1);
            // Create the database tables:
            String createFloidMessageTableString = "CREATE TABLE IF NOT EXISTS floidMessage          (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp DATE DEFAULT (datetime('now','localtime')), floidId INTEGER, floidUuid TEXT, messageType TEXT, responseNumber INTEGER, tag TEXT, message TEXT);";
            String createFloidStatusTableString = "CREATE TABLE IF NOT EXISTS floidStatus           (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp DATE DEFAULT (datetime('now','localtime')), floidId INTEGER, floidUuid TEXT, statusNumber INTEGER, floidStatus TEXT);";
            String createFloidDroidStatusTableString = "CREATE TABLE IF NOT EXISTS floidDroidStatus      (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp DATE DEFAULT (datetime('now','localtime')), floidId INTEGER, floidUuid TEXT, statusNumber INTEGER, floidDroidStatus TEXT);";
            String createFloidCommandTableString = "CREATE TABLE IF NOT EXISTS floidCommand          (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp DATE DEFAULT (datetime('now','localtime')), floidId INTEGER, floidUuid TEXT, missionNumber INTEGER, commandNumber Integer, command TEXT);";
            String createFloidModelStatusTableString = "CREATE TABLE IF NOT EXISTS floidModelStatus      (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp DATE DEFAULT (datetime('now','localtime')), floidId INTEGER, floidUuid TEXT, floidModelStatus TEXT);";
            String createFloidModelParametersTableString = "CREATE TABLE IF NOT EXISTS floidModelParameters  (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp DATE DEFAULT (datetime('now','localtime')), floidId INTEGER, floidUuid TEXT, floidModelParameters TEXT);";
            try {
                if (dropTables) {
                    mFloidDatabase.execSQL("DROP TABLE IF EXISTS " + "floidMessage");
                    mFloidDatabase.execSQL("DROP TABLE IF EXISTS " + "floidStatus");
                    mFloidDatabase.execSQL("DROP TABLE IF EXISTS " + "floidDroidStatus");
                    mFloidDatabase.execSQL("DROP TABLE IF EXISTS " + "floidCommand");
                    mFloidDatabase.execSQL("DROP TABLE IF EXISTS " + "floidModelStatus");
                    mFloidDatabase.execSQL("DROP TABLE IF EXISTS " + "floidModelParameters");
                }
            } catch (Exception e) {
                Log.e(TAG, "Error dropping tables: " + Log.getStackTraceString(e));
            }
            try {
                // Create floidMessage table:
                mFloidDatabase.execSQL(createFloidMessageTableString);
            } catch (Exception e) {
                if (mUseLogger) Log.e(TAG, Log.getStackTraceString(e));
            }
            try {
                // Create mFloidStatus table:
                mFloidDatabase.execSQL(createFloidStatusTableString);
            } catch (Exception e) {
                if (mUseLogger) Log.e(TAG, Log.getStackTraceString(e));
            }
            try {
                // Create mFloidActivityFloidDroidStatus table:
                mFloidDatabase.execSQL(createFloidDroidStatusTableString);
            } catch (Exception e) {
                if (mUseLogger) Log.e(TAG, Log.getStackTraceString(e));
            }
            try {
                // Create floidCommand table:
                mFloidDatabase.execSQL(createFloidCommandTableString);
            } catch (Exception e) {
                if (mUseLogger) Log.e(TAG, Log.getStackTraceString(e));
            }
            try {
                // Create floidModelStatus table:
                mFloidDatabase.execSQL(createFloidModelStatusTableString);
            } catch (Exception e) {
                if (mUseLogger) Log.e(TAG, Log.getStackTraceString(e));
            }
            try {
                // Create floidModelParameters table:
                mFloidDatabase.execSQL(createFloidModelParametersTableString);
            } catch (Exception e) {
                if (mUseLogger) Log.e(TAG, Log.getStackTraceString(e));
            }
        } else {
            mFloidDatabase = null;
        }
    }

    /**
     * Sets battery status.
     */
    public void setupBatteryStatus() {
        Log.i(TAG, "Setting Up Battery Status");
        // Droid Battery Status:  Set up a timer callback to get the battery on a regular basis.
        final TimerTask droidBatteryTimerTask = new TimerTask() {
            @SuppressWarnings("UnusedAssignment")
            @Override
            public void run() {
                IntentFilter droidBatteryChangedFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
                Intent droidBatteryStatusIntent = registerReceiver(null, droidBatteryChangedFilter);
                int droidBatteryRawLevel = 0;
                if (droidBatteryStatusIntent != null) {
                    droidBatteryRawLevel = droidBatteryStatusIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                    int droidBatteryRawScale = droidBatteryStatusIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                    droidBatteryStatusIntent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
                    droidBatteryStatusIntent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
                    int droidBatteryLevel = -1;
                    if (droidBatteryRawLevel >= 0 && droidBatteryRawScale > 0) {
                        droidBatteryLevel = (droidBatteryRawLevel * 100) / droidBatteryRawScale;
                    }
                    synchronized (mFloidDroidStatusSynchronizer) {
                        mFloidDroidStatus.setBatteryLevel(droidBatteryLevel);
                        mFloidDroidStatus.setBatteryRawLevel(droidBatteryRawLevel);
                        mFloidDroidStatus.setBatteryRawScale(droidBatteryRawScale);
                    }
                }
            }
        };
        batteryStatusTimer = new Timer();
        batteryStatusTimer.scheduleAtFixedRate(droidBatteryTimerTask, DROID_BATTERY_CHECK_FIRST_WAIT, DROID_BATTERY_CHECK_PERIOD);
    }

    /**
     * Play notification.
     *
     * @param notificationType the notification type
     */
    private void playNotification(int notificationType) {
        // See if we have sounds on:
        String soundsOnString = mDroidParametersHashMap.get(DROID_PARAMETERS_SOUNDS_ON_OPTION);
        boolean soundsOn = true;
        try {
            DroidValidParameters soundsOnValidParameters = mDroidParametersValidValues.get(DROID_PARAMETERS_SOUNDS_ON_OPTION);
            if(soundsOnValidParameters!=null) {
                soundsOn = soundsOnValidParameters.getAsValidBoolean(soundsOnString);
            }
        } catch (Exception e) {
            Log.e(TAG, "playNotification() threw exception retrieving DROID_PARAMETERS_SOUNDS_ON_OPTION: ", e);
        }
        if (soundsOn) {
            switch(notificationType) {
                case FLOID_ALERT_SOUND_FLOID_STATUS_RECEIVED: {
                    mFloidStatusReceivedMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_DEBUG_MESSAGE_RECEIVED: {
                    mDebugMessageReceivedMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_FLOID_CONNECTED: {
                    mFloidConnectedMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_FLOID_DISCONNECTED: {
                    mFloidDisconnectedMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_COMMAND_RESPONSE_RECEIVED: {
                    mCommandResponseReceivedMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_MODEL_STATUS_RECEIVED: {
                    mModelStatusReceivedMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_MODEL_PARAMETERS_RECEIVED: {
                    mModelParametersReceivedMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_SERVER_CONNECTED: {
                    mServerConnectedMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_SERVER_DISCONNECTED: {
                    mServerDisconnectedMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_TAKE_PHOTO: {
                    mTakePhotoMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_START_VIDEO: {
                    mStartVideoPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_STOP_VIDEO: {
                    mStopVideoMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_SEND_VIDEO_FRAME: {
                    mSendVideoFrameMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_TEST_PACKET_RESPONSE: {
                    mTestPacketResponseMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_START_INTERFACE: {
                    mStartInterfaceMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_STOP_INTERFACE: {
                    mStopInterfaceMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_START_MISSION_TIMER: {
                    mStartMissionTimerMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_BUZZER: {
                    mBuzzerMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_PYR_PACKET_SENT: {
                    mSentPyrPacketMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_SERVICE_CREATE: {
                    mServiceCreateMediaPlayer.start();
                }
                break;
                case FLOID_ALERT_SOUND_SERVICE_DESTROY: {
                    mServiceDestroyMediaPlayer.start();
                }
                break;
                default: {
                    // Unknown sound:
                    Log.e("Floid: playNotification", "Unknown sound: " + notificationType);
                }
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (mUseLogger) Log.i(TAG, "Service BOUND");
        UsbAccessory usbAccessory = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY, UsbAccessory.class);
        if (usbAccessory != null) {
            String fullAccessoryName = usbAccessory.getManufacturer() + " " + usbAccessory.getModel() + " " + usbAccessory.getVersion();
            Log.d(TAG, "USB Attached: " + fullAccessoryName);
            uxSendFloidMessageToDisplay("onBind: Usb accessory: " + fullAccessoryName + "\n");
            openAccessory(usbAccessory);
        }
        // Return the binder:
        return mFloidServiceMessenger.getBinder();
    }

    private void setupNotification() {
        try {
            // Create a pending intent to open the activity:
            Intent floidActivityIntent = new Intent(this, FloidActivity.class);
            PendingIntent pendingFloidActivityIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), floidActivityIntent, PendingIntent.FLAG_IMMUTABLE);
            // Create a pending intent to kill service intent:
            Intent floidServiceKillIntent = new Intent(KILL_SERVICE_INTENT_ACTION);
            PendingIntent pendingFloidServiceKillIntent = PendingIntent.getBroadcast(this, (int) System.currentTimeMillis(), floidServiceKillIntent, PendingIntent.FLAG_IMMUTABLE);
            Bitmap notificationIconBitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.faigle_labs_icon);
            // Create the notification Manager:
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (notificationManager!=null) {
                NotificationChannel notificationChannel = new NotificationChannel(FLOID_SERVICE_NOTIFICATION_CHANNEL_ID, "Floid Service Notifications", NotificationManager.IMPORTANCE_DEFAULT);
                // Configure the notification channel.
                notificationChannel.setDescription("Floid Service Notification Channel");
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableVibration(true);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            // Create the notification:
            mFloidServiceRunningNotification = new NotificationCompat.Builder(this, FLOID_SERVICE_NOTIFICATION_CHANNEL_ID)
                    .setContentTitle("Floid Accessory Service")
                    .setContentText("Floid Accessory Service Bound")
                    .setSmallIcon(R.drawable.ic_action_fl)
                    .setLargeIcon(notificationIconBitmap)
                    .setContentIntent(pendingFloidActivityIntent)
                    .addAction(R.drawable.ic_stat_content_clear, "Kill", pendingFloidServiceKillIntent)
                    .addAction(R.drawable.ic_stat_action_launch, "Open", pendingFloidActivityIntent)
                    .build();
            startForeground(FLOID_SERVICE_FOREGROUND_NOTIFICATION_ID, mFloidServiceRunningNotification);
        } catch (Exception e) {
            Log.e(TAG, "Failed to set up notification", e);
        }
    }
    /* (non-Javadoc)
     * @see android.app.Service#onUnbind(android.content.Intent)
     */
    @Override
    public boolean onUnbind(Intent intent) {
        if (mUseLogger) Log.i(TAG, "Service UNBOUND");
        mFloidActivityMessenger = null;
        return true;
    }

    /* (non-Javadoc)
     * @see android.app.Service#onRebind(android.content.Intent)
     */
    @Override
    public void onRebind(Intent intent) {
        if (mUseLogger) Log.i(TAG, "Service REBIND");
    }

    /**
     * The type Incoming handler.
     */
    private class IncomingHandler implements Handler.Callback {
        // Handler of incoming messages from clients.
        @Override
        public boolean handleMessage(Message incomingMessage) {
            if (incomingMessage == null) {
                // Should not happen:
                Log.e(TAG, "UX Message was null...");
                return true;
            }
            Bundle data = incomingMessage.getData();
            if(data!=null) {
                data.setClassLoader(FloidService.class.getClassLoader());
            }
            try {
                switch (incomingMessage.what) {
                    case FLOID_SERVICE_MESSAGE_PING: {
                        // This retrieves our outgoing handler:
                        if (mUseLogger) Log.d(TAG, "Service <-- Ping");
                        mFloidActivityMessenger = incomingMessage.replyTo;
                        if (mFloidActivityMessenger == null) {
                            if (mUseLogger)
                                Log.d(TAG, "DAMN - mFloidActivityMessenger is null...");
                        } else {
                            if (mUseLogger)
                                Log.d(TAG, "GREAT - mFloidActivityMessenger is OK...");
                        }
                        // Send accumulated ux messages if any:
                        uxSendAccumulatedUxFloidMessages();

                        // Send the various parameters:
                        uxSendLog();
                        uxSendFloidIds();
                        uxSendHost();
                        uxSendFloidModelStatus();
                        uxSendFloidModelParameters();
                        uxSendFloidDroidStatus();
                        uxSendFloidStatus();
                        uxSendClientCommunicator();
                        uxSendSetAccessoryConnectedStatus();
                        uxSendCameraInfo();
                        uxSendImagingStatus();
                        // Set the parameters to the UX in this order
                        uxSendFloidDroidParameterValidValues(); // UX does not render
                        uxSendFloidDroidDefaultParameters();  // UX does not render
                        uxSendFloidDroidParameters();  // UX renders the parameters
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_SEND_COMMAND_TO_FLOID: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Send Command To Floid");
                            if(data!=null) {
                                FloidOutgoingCommand floidOutgoingCommand = data.getParcelable(BUNDLE_KEY_OUTGOING_COMMAND, FloidOutgoingCommand.class);
                                if(floidOutgoingCommand!=null) {
                                    sendCommandToFloid(floidOutgoingCommand);
                                } else {
                                    Log.e(TAG, "Service <-- ERROR: Floid Outgoing Command was null=");
                                }
                            } else {
                                Log.e(TAG, "Service <-- ERROR: Empty Data");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Send Command To Floid: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_SET_FLOID_ID: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Set Floid Id");
                        if(data!=null) {
                            // Retrieve and set the new floid id:
                            int newFloidId = data.getInt(BUNDLE_KEY_FLOID_ID);
                            mFloidDroidStatus.setFloidId(newFloidId);
                            saveFloidId(newFloidId);
                            // Also pass it to the imaging to set up the new imaging directory:
                            mFloidImaging.setFloidId(newFloidId);
                            // Create a new uuid:
                            createUuid();
                            // And send the ids back:
                            uxSendFloidIds();
                        } else {
                            Log.e(TAG, "Service <-- ERROR: Empty Data");
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_GET_FLOID_IDS: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Get Floid Ids");
                            uxSendFloidIds();
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Get Floid Ids: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_SET_HOST: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Set Host");
                            if(data!=null) {
                                String newHostAddress = data.getString(FloidService.BUNDLE_KEY_HOST_ADDRESS);
                                int newHostPort = data.getInt(FloidService.BUNDLE_KEY_HOST_PORT);
                                synchronized (mServerAddressSynchronizer) {
                                    mHasNewServerAddress = true;
                                    mNewHostAddress = newHostAddress;
                                    mNewHostPort = newHostPort;
                                }
                                if(mNewHostAddress != null) {
                                    saveHostIpAndPort(mNewHostAddress, newHostPort);
                                } else {
                                    Log.e(TAG, "Set Host - Host IP is null");
                                }
                            } else {
                                Log.e(TAG, "Service <-- ERROR: Empty Data");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Set Host: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_GET_HOST: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Get Host");
                            uxSendHost();
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Get Host: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_GET_LOG: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Get Log");
                            uxSendLog();
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Get Log: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_SET_LOG: {
                        try {
                            if(data!=null) {
                                final boolean useLogger = data.getBoolean(BUNDLE_KEY_LOG);
                                Log.i(TAG, String.format(Locale.getDefault(), "Service <-- Set Log: %b -> %b", mUseLogger, useLogger));  // Note that this is the one log message that always appears
                                mUseLogger = useLogger;
                            } else {
                                Log.e(TAG, "Service <-- ERROR: Empty Data");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Get Log: " + e.getMessage());
                        }
                    }
                    break;
                    // This is the UX asking for the model parameters from the service:
                    case FLOID_SERVICE_MESSAGE_GET_MODEL_PARAMETERS: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Get Model Parameters");
                            uxSendFloidModelParameters();
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Get Model Parameters: " + e.getMessage());
                        }
                    }
                    break;
                    // This is the UX telling the service to send the model parameters:
                    case FLOID_SERVICE_MESSAGE_SET_MODEL_PARAMETERS: {
                        if(data!=null) {
                            FloidModelParametersParcelable floidModelParametersParcelable = data.getParcelable(BUNDLE_KEY_FLOID_MODEL_PARAMETERS);
                            setFloidModelParameters(floidModelParametersParcelable);
                        } else {
                            Log.e(TAG, "Service <-- ERROR: Empty Data");
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_GET_FLOID_STATUS: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Get Floid Status");
                            uxSendFloidStatus();
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Get Floid Status: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_GET_FLOID_DROID_STATUS: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Get Floid Droid Status");
                            uxSendFloidDroidStatus();
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Get Floid Droid Status: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_PLAY_NOTIFICATION: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Play Notification");
                            if(data!=null) {
                                playNotification(data.getInt(FloidService.BUNDLE_KEY_NOTIFICATION));
                            } else {
                                Log.e(TAG, "Service <-- ERROR: Empty Data");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Play Notification: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MISSION_MODE: {
                        // Update the mission mode, but only if no floid is connected:
                        synchronized (mFloidDroidStatusSynchronizer) {
                            if(data!=null) {
                                if (!mFloidDroidStatus.isFloidConnected()) {
                                    mFloidDroidStatus.setCurrentMode(data.getInt(FloidService.BUNDLE_KEY_MISSION_MODE));
                                }
                            } else {
                                Log.e(TAG, "Service <-- ERROR: Empty Data");
                            }
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_UPDATE_THREAD_TIMINGS: {
                        // Update our thread timings:
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Update Thread Timings");
                            if(data!=null) {
                                mFloidThreadTimings = data.getParcelable(FloidService.BUNDLE_KEY_THREAD_TIMINGS, FloidThreadTimings.class);
                            } else {
                                Log.e(TAG, "Service <-- ERROR: Empty Data");
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception in message Update Thread Timings: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_START_VIDEO: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Start Video");
                        playNotification(FLOID_ALERT_SOUND_START_VIDEO);
                        mFloidImaging.startVideo();
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_STOP_VIDEO: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Stop Video");
                        playNotification(FLOID_ALERT_SOUND_STOP_VIDEO);
                        mFloidImaging.stopVideo();
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_SEND_VIDEO_FRAME: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Send Video Frame");
                        playNotification(FLOID_ALERT_SOUND_SEND_VIDEO_FRAME);
                        // [VIDEO-PREVIEW] - THIS IS THE CODE THAT CALLS TO SEND THE PREVIEW VIDEO FRAME:
                        if (mUseLogger)
                            Log.i(TAG, "NOT Sending video  frame - THIS CODE DOES NOT FUNCTION");
                    /*
                        if(mUseLogger) Log.i(TAG, "Sending video frame [Not Implemented]");
                        sendVideoFrame();
                    */
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_TAKE_PHOTO: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Take Photo");
                        playNotification(FLOID_ALERT_SOUND_TAKE_PHOTO);
                        if(mFloidImaging.takePhoto()) {
                            if (mUseLogger) Log.d(TAG,"Service: Take Photo returned true (success)");
                        } else {
                            Log.e(TAG,"Service: Take Photo returned false (failure)");
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_SPEAK_COMMAND: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Speak Command");
                        String speakCommandsString = mDroidParametersHashMap.get(DROID_PARAMETERS_SPEAK_COMMANDS_OPTION);
                        boolean speakCommands = true;
                        try {
                            speakCommands = mDroidParametersValidValues.get(DROID_PARAMETERS_SPEAK_COMMANDS_OPTION).getAsValidBoolean(speakCommandsString);
                            if (mUseLogger) Log.d(TAG, "Speak Commands: " + speakCommands);
                        } catch (Exception e) {
                            if (mUseLogger) Log.e(TAG, "Speak Commands: Bad speak commands value: " + speakCommandsString + " - setting to default (true)");
                        }
                        if (speakCommands) {
                            if(data!=null) {
                                speakString(data.getString(BUNDLE_KEY_COMMAND_TO_SPEAK));
                            } else {
                                Log.e(TAG, "Service <-- ERROR: Empty Data");
                            }
                        }
                    }
                    break;
                    // Note: String is a Parcelable:
                    case FLOID_SERVICE_MESSAGE_SPEAK_STRING: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Speak String");
                        if(data!=null) {
                            speakString(data.getString(BUNDLE_KEY_STRING_TO_SPEAK));
                        } else {
                            Log.e(TAG, "Service <-- ERROR: Empty Data");
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_GET_CLIENT_COMMUNICATOR_STATUS: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Get Client Communicator Status");
                        uxSendClientCommunicator();
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_SET_CLIENT_COMMUNICATOR_STATUS: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Set Client Communicator Status");
                        if(data!=null) {
                            mClientCommunicatorOn = data.getBoolean(BUNDLE_KEY_CLIENT_COMMUNICATOR);
                        } else {
                            Log.e(TAG, "Service <-- ERROR: Empty Data");
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_START_MISSION_TIMER: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Start Mission Timer");
                        if(data!=null) {
                            // Note: Only available from same process:
                            FloidMissionReadyTimerObject floidMissionReadyTimerObject = data.getParcelable(BUNDLE_KEY_MISSION_READY_TIMER, FloidMissionReadyTimerObject.class);
                            startMissionTimer(floidMissionReadyTimerObject);
                        } else {
                            Log.e(TAG, "Service <-- ERROR: Empty Data");
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_CANCEL_MISSION_TIMER: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Cancel Mission Timer");
                        cancelMissionTimerFromUx();
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_SET_MODE: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Set Mode");
                        if(data!=null) {
                            int newFloidMode = data.getInt(BUNDLE_KEY_MODE);
                            setFloidDroidMode(newFloidMode);
                        } else {
                            Log.e(TAG, "Service <-- ERROR: Empty Data");
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_OPEN_USB_ACCESSORY: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Open usb accessory");
                            if(data!=null) {
                                uxSendFloidMessageToDisplay("Open usb accessory" + "\n");
                                UsbAccessory usbAccessory = data.getParcelable(BUNDLE_KEY_USB_ACCESSORY, UsbAccessory.class);
                                openAccessory(usbAccessory);
                            } else {
                                Log.e(TAG, "Service <-- ERROR: Empty Data");
                            }
                        } catch (Exception e) {
                            uxSendFloidMessageToDisplay("Failed to open usb accessory: " + e.getMessage() + "\n");
                            Log.e(TAG, "Exception in message -> Open usb accessory: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_SET_DROID_PARAMETERS: {
                        try {
                            if (mUseLogger) Log.d(TAG, "Service <-- Set Droid Parameters");
                            if(data!=null) {
                                HashMapStringToStringParcelable droidParametersParcelable = data.getParcelable(BUNDLE_KEY_FLOID_DROID_PARAMETERS, HashMapStringToStringParcelable.class);
                                if (droidParametersParcelable != null) {
                                    for (Map.Entry<String, String> droidParameterEntry : droidParametersParcelable.getStringToStringHashMap().entrySet()) {
                                        setParameter(droidParameterEntry.getKey(), droidParameterEntry.getValue());
                                    }
                                    // Parameters are set, put them into the instances where appropriate:
                                    setFloidDroidInstanceMembersFromParameters();
                                } else {
                                    Log.e(TAG, "Set Droid Parameters - droidParametersParcelable was null");
                                }
                            } else {
                                Log.e(TAG, "Service <-- ERROR: Empty Data");
                            }
                        } catch (Exception e) {
                            uxSendFloidMessageToDisplay("Failed to set droid parameters: " + e.getMessage() + "\n");
                            Log.e(TAG, "Exception in message -> Set droid parameters: " + e.getMessage());
                        }
                    }
                    break;
                    case FLOID_SERVICE_MESSAGE_UPLOAD_FIRMWARE: {
                        if (mUseLogger) Log.d(TAG, "Service <-- Upload Firmware");
                        try {
                            (new UploadController()).uploadController(FloidService.this, FloidService.this);
                        } catch (Exception e) {
                            uxSendFloidMessageToDisplay("Failed to set droid parameters: " + e.getMessage() + "\n");
                            Log.e(TAG, "Exception in message -> Set droid parameters: " + e.getMessage());
                        }
                    }
                    break;
                    default: {
                        uxSendFloidMessageToDisplay("Service <-- Unknown message: " + incomingMessage.what + "\n");
                        Log.e(TAG, "Unknown message: " + incomingMessage.what);
                    }
                    break;
                }
            } catch (Exception e) {
                Log.e(TAG, "Floid Service Incoming Message: Exception", e);
            }
            return true;
        }
    }

    /**
     * Ux send floid status.
     */
    public void uxSendFloidStatus() {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Status");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Status]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_FLOID_STATUS);
            outgoingUXMessage.getData().putParcelable(BUNDLE_KEY_FLOID_STATUS, mFloidStatus);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in message Send To UX Floid Status: " + e.getMessage());
        }
    }
    /**
     * Ux send additional status.
     */
    public void uxSendAdditionalStatus() {
        // Too chatty:
        // if (mUseLogger) Log.v(TAG, "Service -> UX: Additional Status");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [AdditionalStatus]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_ADDITIONAL_STATUS);
            outgoingUXMessage.getData().putParcelable(BUNDLE_KEY_ADDITIONAL_STATUS, mAdditionalStatus);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in message Send To UX Additional Status: " + e.getMessage());
        }
    }
    /**
     * Ux send update to comm status.
     * @param item which comm channel (floid or droid)
     * @param state the new state (good, warning or error)
     */
    public void uxSendCommStatus(int item, int state) {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Comm Status");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Comm Status]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_UPDATE_COMM_STATUS);
            outgoingUXMessage.getData().putInt(BUNDLE_KEY_COMM_ITEM, item);
            outgoingUXMessage.getData().putInt(BUNDLE_KEY_COMM_STATE, state);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in message Send To UX Comm Status: " + e.getMessage());
        }
    }

    /**
     * Ux send floid droid status.
     */
    public void uxSendFloidDroidStatus() {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Droid Status");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Droid Status]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_DROID_STATUS);
            outgoingUXMessage.getData().putParcelable(BUNDLE_KEY_FLOID_DROID_STATUS, mFloidDroidStatus);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in message Send To UX Floid Droid Status: " + e.getMessage());
        }
    }

    /**
     * Ux send floid model parameters.
     */
    public void uxSendFloidModelParameters() {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Model Parameters");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Model Parameters]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_MODEL_PARAMETERS);
            outgoingUXMessage.getData().putParcelable(BUNDLE_KEY_FLOID_MODEL_PARAMETERS, mFloidModelParameters);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Model Parameters: " + e.getMessage());
        }
    }
    /**
     * Ux send floid droid default parameters.
     */
    private void uxSendFloidDroidDefaultParameters() {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Droid Default Parameters");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Droid Default Parameters]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_DROID_DEFAULT_PARAMETERS);
            HashMapStringToStringParcelable hashMapStringToStringParcelable = new HashMapStringToStringParcelable();
            hashMapStringToStringParcelable.setStringToStringHashMap(mDroidDefaultParametersHashMap);
            outgoingUXMessage.getData().putParcelable(BUNDLE_KEY_DROID_DEFAULT_PARAMETERS ,hashMapStringToStringParcelable);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Droid Default Parameters: " + e.getMessage());
        }
    }

    /**
     * Ux send floid droid parameters.
     */
    private void uxSendFloidDroidParameters() {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Droid Parameters");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Droid Parameters]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_DROID_PARAMETERS);
            HashMapStringToStringParcelable hashMapStringToStringParcelable = new HashMapStringToStringParcelable();
            hashMapStringToStringParcelable.setStringToStringHashMap(mDroidParametersHashMap);
            outgoingUXMessage.getData().putParcelable(BUNDLE_KEY_FLOID_DROID_PARAMETERS ,hashMapStringToStringParcelable);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Droid Parameters: " + e.getMessage());
        }
    }
    /**
     * Ux send floid droid valid values.
     */
    private void uxSendFloidDroidParameterValidValues() {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Droid Parameter Valid Values");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Droid Parameter Valid Values]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_DROID_PARAMETER_VALID_VALUES);
            HashMapStringToDroidValidParametersParcelable hashMapStringToDroidValidParametersParcelable = new HashMapStringToDroidValidParametersParcelable();
            hashMapStringToDroidValidParametersParcelable.setStringToDroidValidParametersHashMap(mDroidParametersValidValues);
            outgoingUXMessage.getData().putParcelable(BUNDLE_KEY_FLOID_DROID_VALID_PARAMETERS, hashMapStringToDroidValidParametersParcelable);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Droid Parameter Valid Values: " + e.getMessage());
        }
    }
    /**
     * Ux send floid model status.
     */
    public void uxSendFloidModelStatus() {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Model Status");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Model Status]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_MODEL_STATUS);
            outgoingUXMessage.getData().putParcelable(BUNDLE_KEY_FLOID_MODEL_STATUS, mFloidModelStatus);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Model Parameters: " + e.getMessage());
        }
    }

    /**
     * Ux send floid test response message.
     *
     * @param testPacketResponse the test packet response
     */
    public void uxSendFloidTestResponseMessage(String testPacketResponse) {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Test Response Message");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Test Response Message]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_TEST_PACKET_RESPONSE);
            outgoingUXMessage.getData().putString(BUNDLE_KEY_TEST_PACKET_RESPONSE, testPacketResponse);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Test Response Message: " + e.getMessage());
        }
    }

    /**
     * Ux send host.
     */
    private void uxSendHost() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Host");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Host]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_HOST);
            outgoingUXMessage.getData().putString(BUNDLE_KEY_HOST_ADDRESS, mHostAddress);
            outgoingUXMessage.getData().putInt(BUNDLE_KEY_HOST_PORT, mHostPort);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception UX Send Floid Host: " + e.getMessage());
        }
    }

    /**
     * Ux send ding comm status.
     */
    public void uxSendDingCommStatus() {
        // if (mUseLogger) Log.v(TAG, "Service -> UX: Ding Comm Status");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [DingCommStatus]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_DING_COMM_STATUS);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Ding Comm Status: " + e.getMessage());
        }
    }

    /**
     * Ux send ding server status.
     */
    // This is unused as the server status ding message is sent from the client communicator thread:
    @SuppressWarnings("unused")
    private void uxSendDingServerStatus() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Ding Server Status");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Ding Server Status]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_DING_SERVER_STATUS);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Ding Server Status: " + e.getMessage());
        }
    }
    /**
     * Ux send imaging status.
     */
    public void uxSendImagingStatus() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Imaging Status");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Imaging Status]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_IMAGING_STATUS);
            outgoingUXMessage.getData().putParcelable(BUNDLE_KEY_FLOID_IMAGING_STATUS, mFloidImaging.getFloidImagingStatus());
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Imaging Status: " + e.getMessage());
        }
    }
    /**
     * Ux send camera info.
     */
    public void uxSendCameraInfo() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Camera Info");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Camera Info]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_CAMERA_INFO);
            outgoingUXMessage.getData().putParcelableArrayList(BUNDLE_KEY_CAMERA_INFO_LIST, mFloidImaging.getCameraInfoList());
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Camera Info: " + e.getMessage());
        }
    }
    /**
     * Ux send photo.
     * @param photoBytes the bytes for the photo
     */
    public void uxSendPhoto(byte[] photoBytes) {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Photo");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Photo]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_PHOTO);
            outgoingUXMessage.getData().putByteArray(FloidService.BUNDLE_KEY_PHOTO, photoBytes);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Photo: " + e.getMessage());
        }
    }
    /**
     * Ux send video frame.
     * @param videoFrameBytes the bytes for the video frame
     */
    public void uxSendVideoFrame(byte[] videoFrameBytes) {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Video Frame");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Video Frame]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_VIDEO_FRAME);
            outgoingUXMessage.getData().putByteArray(FloidService.BUNDLE_KEY_VIDEO_FRAME, videoFrameBytes);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Video Frame: " + e.getMessage());
        }
    }
    /**
     * Ux send floid ids.
     */
    private void uxSendFloidIds() {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Ids");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Ids]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_IDS);
            outgoingUXMessage.getData().putInt(BUNDLE_KEY_FLOID_ID, mFloidDroidStatus.getFloidId());
            outgoingUXMessage.getData().putString(BUNDLE_KEY_FLOID_UUID, mFloidDroidStatus.getFloidUuid());
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Ids: " + e.getMessage());
        }
    }

    /**
     * Ux send log.
     */
    private void uxSendLog() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Log");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Log]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_LOG);
            outgoingUXMessage.getData().putBoolean(BUNDLE_KEY_LOG, mUseLogger);
            outgoingUXMessage.getData().putInt(BUNDLE_KEY_FLOID_SERVICE_PID, mFloidServicePid);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Log: " + e.getMessage());
        }
    }

    /**
     * Ux send client communicator.
     */
    private void uxSendClientCommunicator() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Client Communicator");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Client Communicator]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_CLIENT_COMMUNICATOR);
            outgoingUXMessage.getData().putBoolean(BUNDLE_KEY_CLIENT_COMMUNICATOR, mClientCommunicatorOn);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Client Communicator: " + e.getMessage());
        }
    }

    /**
     * Ux send floid debug message.
     *
     * @param debugMessage the debug message
     */
    public void uxSendFloidDebugMessage(String debugMessage) {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Debug Message");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Debug Message]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_DEBUG);
            outgoingUXMessage.getData().putString(BUNDLE_KEY_DEBUG_MESSAGE, debugMessage);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Debug Message", e);
        }
    }

    /**
     * Ux send floid command response message.
     *
     * @param formattedCommandResponseString the formatted command response string
     */
    public void uxSendFloidCommandResponseMessage(String formattedCommandResponseString) {
        if (mUseLogger) Log.v(TAG, "Service -> UX: Floid Command Response");
        if (mUseLogger) Log.d(TAG, "Sending Floid Command Response");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Floid Command Response]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_COMMAND_RESPONSE);
            outgoingUXMessage.getData().putString(BUNDLE_KEY_COMMAND_RESPONSE, formattedCommandResponseString);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Command Response Message: " + e.getMessage());
        }
    }

    /**
     * Accumulate unsent ux messages
     *
     * @param messageText the message text to accumulate
     */
    private void accumulateUxFloidMessage(String messageText) {
        accumulatedUxFloidMessages = accumulatedUxFloidMessages + messageText;
    }

    /**
     * Send accumulated unsent ux messages
     */
    private void uxSendAccumulatedUxFloidMessages() {
        if (accumulatedUxFloidMessages.length() > 0) {
            uxSendFloidMessages(accumulatedUxFloidMessages);
            accumulatedUxFloidMessages = "";
        }
    }

    /**
     * Ux send floid message to display.
     *
     * @param messageText the message text
     */
    public void uxSendFloidMessageToDisplay(String messageText) {
        if (mUseLogger)
            Log.i(TAG, "Service -> UX: Floid Message To Display");
        // Add a message number in front of it:
        try {
            messageText = String.format(Locale.getDefault(), "%d: %s", currentUxMessageNumber++, messageText == null ? "null" : messageText);
            if (mFloidActivityMessenger == null) {
                if(mUseLogger)
                    Log.w(TAG, "Accumulating message: " + messageText);
                accumulateUxFloidMessage(messageText);
            } else {
                if(mUseLogger)
                    Log.d(TAG, "Sending message: " + messageText);
                uxSendFloidMessages(messageText);
            }
        } catch(Exception e) {
            Log.e(TAG, "uxSendFloidMessageToDisplay: caught exception", e);
        }
    }

    /**
     * Ux send floid messages to display without adding prefix number
     *
     * @param messageText the message text
     */
    private void uxSendFloidMessages(String messageText) {
        if (mUseLogger)
            Log.i(TAG, "Service -> UX: Floid Message [From Outgoing Accumulator]");
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_MESSAGE_TO_DISPLAY);
            outgoingUXMessage.getData().putString(BUNDLE_KEY_MESSAGE, messageText);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Floid Display Message: " + e.getMessage());
        }
    }

    /**
     * Ux send turn on interface message.
     */
    private void uxSendTurnOnInterfaceMessage() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Turn On Interface");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Turn On Interface]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_START_INTERFACE);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Turn On Interface Message: " + e.getMessage());
        }
    }

    /**
     * Ux send set accessory connected status.
     */
    public void uxSendSetAccessoryConnectedStatus() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Set Accessory Connected Status");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Set Accessory Connected Status]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_SET_ACCESSORY_CONNECTED);
            outgoingUXMessage.getData().putBoolean(BUNDLE_KEY_ACCESSORY_CONNECTED, mFloidDroidStatus.isFloidConnected());
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Accessory Connected Message: " + Boolean.valueOf(mFloidDroidStatus.isFloidConnected()).toString() + " " + e.getMessage());
        }
    }

    /**
     * Ux send turn off interface message.
     */
    private void uxSendTurnOffInterfaceMessage() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Turn Off Interface");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Turn Off Interface]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_STOP_INTERFACE);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in UX Send Turn Off Interface Message: " + e.getMessage());
        }
    }

    /**
     * Ux send start mission timer.
     *
     * @param time        the time
     * @param missionName the mission name
     */
    private void uxSendStartMissionTimer(int time, String missionName) {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Start Mission Timer");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Start Mission Timer]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_START_MISSION_TIMER);
            outgoingUXMessage.getData().putString(BUNDLE_KEY_MISSION_START_NAME, missionName);
            outgoingUXMessage.getData().putInt(BUNDLE_KEY_MISSION_START_TIMER_TIME, time);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in message UX Send Start Mission Timer: " + e.getMessage());
        }
    }

    /**
     * Ux send update mission timer.
     *
     * @param time the time
     */
    private void uxSendUpdateMissionTimer(int time) {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Update Mission Timer");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Update Mission Timer]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_UPDATE_MISSION_TIMER);
            outgoingUXMessage.getData().putInt(BUNDLE_KEY_MISSION_TIMER_UPDATE, time);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in message UX Send Update Mission Timer: " + e.getMessage());
        }
    }

    /**
     * Ux send cancel mission timer.
     */
    private void uxSendCancelMissionTimer() {
        if (mUseLogger) Log.d(TAG, "Service -> UX: Cancel Mission Timer");
        if (mFloidActivityMessenger == null) {
            if (mUseLogger) Log.v(TAG, "Waiting for Messenger [Cancel Mission Timer]");
            return;
        }
        try {
            Message outgoingUXMessage = Message.obtain(null, FloidActivity.FLOID_UX_MESSAGE_CANCEL_MISSION_TIMER);
            mFloidActivityMessenger.send(outgoingUXMessage);
        } catch (Exception e) {
            Log.e(TAG, "Exception in message UX Send Cancel Mission Timer: " + e.getMessage());
        }
    }

    /**
     * Service send speak command.
     *
     * @param commandToSpeak the command to speak
     */
    public void serviceSendSpeakCommand(String commandToSpeak) {
        if (mUseLogger) Log.d(TAG, "Service -> Svc: Sending Command To Speak");
        try {
            Message floidServiceMessage = Message.obtain(null, FLOID_SERVICE_MESSAGE_SPEAK_COMMAND);
            floidServiceMessage.getData().putString(BUNDLE_KEY_COMMAND_TO_SPEAK, commandToSpeak);
            mFloidServiceMessenger.send(floidServiceMessage);
        } catch (Exception e) {
            Log.e(TAG, "serviceSendSpeakCommand: Exception:  " + e.getMessage());
        }
    }

    /**
     * Service send play notification.
     *
     * @param notification the notification
     */
    public void serviceSendPlayNotification(int notification) {
        if (mUseLogger) Log.d(TAG, "Service -> Svc: Sending Play Notification");
        try {
            Message floidServiceMessage = Message.obtain(null, FLOID_SERVICE_MESSAGE_PLAY_NOTIFICATION);
            floidServiceMessage.getData().putInt(BUNDLE_KEY_NOTIFICATION, notification);
            mFloidServiceMessenger.send(floidServiceMessage);
        } catch (Exception e) {
            Log.e(TAG, "serviceSendPlayNotification: Exception:  " + e.getMessage());
        }
    }

    /**
     * Service send take photo.
     */
    public void serviceSendTakePhoto() {
        if (mUseLogger) Log.d(TAG, "Service -> Svc: Sending Take Photo");
        try {
            Message floidServiceMessage = Message.obtain(null, FLOID_SERVICE_MESSAGE_TAKE_PHOTO);
            mFloidServiceMessenger.send(floidServiceMessage);
        } catch (Exception e) {
            Log.e(TAG, "serviceSendTakePhoto: Exception:  " + e.getMessage());
        }
    }

    /**
     * Service send start video.
     */
    public void serviceSendStartVideo() {
        if (mUseLogger) Log.d(TAG, "Service -> Svc: Sending Start Video");
        try {
            Message floidServiceMessage = Message.obtain(null, FLOID_SERVICE_MESSAGE_START_VIDEO);
            mFloidServiceMessenger.send(floidServiceMessage);
            if (mUseLogger) {
                Log.d(TAG, "Service WAS SENT: Start Video");
            }
        } catch (Exception e) {
            Log.e(TAG, "serviceSendStartVideo: Exception:  " + e.getMessage());
        }
    }

    /**
     * Service send stop video.
     */
    public void serviceSendStopVideo() {
        if (mUseLogger) Log.i(TAG, "Service -> Svc: Sending Stop Video");
        try {
            Message floidServiceMessage = Message.obtain(null, FLOID_SERVICE_MESSAGE_STOP_VIDEO);
            mFloidServiceMessenger.send(floidServiceMessage);
        } catch (Exception e) {
            Log.e(TAG, "serviceSendStopVideo: Exception:  " + e.getMessage());
        }
    }

    /**
     * Service send video frame.
     */
    public void serviceSendVideoFrame() {
        if (mUseLogger) Log.i(TAG, "Service -> Svc: Sending Video Frame");
        try {
            Message floidServiceMessage = Message.obtain(null, FLOID_SERVICE_MESSAGE_SEND_VIDEO_FRAME);
            mFloidServiceMessenger.send(floidServiceMessage);
            if (mUseLogger) {
                Log.d(TAG, "Service WAS SENT: Send Video Frame");
            }
        } catch (Exception e) {
            Log.e(TAG, "serviceSendVideoFrame: Exception:  " + e.getMessage());
        }
    }

    /**
     * Service send start mission timer.
     *
     * @param floidMissionReadyTimerObject the floid mission ready timer object
     */
    public void serviceSendStartMissionTimer(FloidMissionReadyTimerObject floidMissionReadyTimerObject) {
        if (mUseLogger) Log.i(TAG, "Service -> Svc: Sending Start Mission Timer");
        try {
            Message floidServiceMessage = Message.obtain(null, FLOID_SERVICE_MESSAGE_START_MISSION_TIMER);
            floidServiceMessage.getData().putParcelable(BUNDLE_KEY_MISSION_READY_TIMER, floidMissionReadyTimerObject);
            mFloidServiceMessenger.send(floidServiceMessage);
        } catch (Exception e) {
            Log.e(TAG, "serviceSendStartMissionTimer: Exception:  " + e.getMessage());
        }
    }

    /**
     * Gets floid activity messenger.
     *
     * @return the floid activity messenger
     */
    public Messenger getFloidActivityMessenger() {
        return mFloidActivityMessenger;
    }

    /**
     * Gets floid service messenger.
     *
     * @return the floid service messenger
     */
    public Messenger getFloidServiceMessenger() {
        return mFloidServiceMessenger;
    }
}
