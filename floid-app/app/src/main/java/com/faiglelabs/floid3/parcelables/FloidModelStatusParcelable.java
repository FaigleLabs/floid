package com.faiglelabs.floid3.parcelables;

import android.os.Parcel;
import android.os.Parcelable;
import com.faiglelabs.floid.servertypes.statuses.FloidModelStatus;

/**
 * Parcelable version of FloidModelStatus
 */
public class FloidModelStatusParcelable extends FloidModelStatus implements Parcelable {

    @Override
    public FloidModelStatusParcelable clone() throws CloneNotSupportedException {
        return (FloidModelStatusParcelable) super.clone();
    }
    /**
     * Parcelable version of FloidModelStatus
     */
    public FloidModelStatusParcelable() {
        super();
        this.getClass().getSuperclass().getName();
        type=this.getClass().getSuperclass().getName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.collectiveDeltaOrientationH0);
        dest.writeDouble(this.collectiveDeltaOrientationH1);
        dest.writeDouble(this.collectiveDeltaOrientationH2);
        dest.writeDouble(this.collectiveDeltaOrientationH3);
        dest.writeDouble(this.collectiveDeltaAltitudeH0);
        dest.writeDouble(this.collectiveDeltaAltitudeH1);
        dest.writeDouble(this.collectiveDeltaAltitudeH2);
        dest.writeDouble(this.collectiveDeltaAltitudeH3);
        dest.writeDouble(this.collectiveDeltaHeadingH0);
        dest.writeDouble(this.collectiveDeltaHeadingH1);
        dest.writeDouble(this.collectiveDeltaHeadingH2);
        dest.writeDouble(this.collectiveDeltaHeadingH3);
        dest.writeDouble(this.collectiveDeltaTotalH0);
        dest.writeDouble(this.collectiveDeltaTotalH1);
        dest.writeDouble(this.collectiveDeltaTotalH2);
        dest.writeDouble(this.collectiveDeltaTotalH3);
        dest.writeDouble(this.cyclicValueH0S0);
        dest.writeDouble(this.cyclicValueH0S1);
        dest.writeDouble(this.cyclicValueH0S2);
        dest.writeDouble(this.cyclicValueH1S0);
        dest.writeDouble(this.cyclicValueH1S1);
        dest.writeDouble(this.cyclicValueH1S2);
        dest.writeDouble(this.cyclicValueH2S0);
        dest.writeDouble(this.cyclicValueH2S1);
        dest.writeDouble(this.cyclicValueH2S2);
        dest.writeDouble(this.cyclicValueH3S0);
        dest.writeDouble(this.cyclicValueH3S1);
        dest.writeDouble(this.cyclicValueH3S2);
        dest.writeDouble(this.collectiveValueH0);
        dest.writeDouble(this.collectiveValueH1);
        dest.writeDouble(this.collectiveValueH2);
        dest.writeDouble(this.collectiveValueH3);
        dest.writeDouble(this.calculatedCyclicBladePitchH0S0);
        dest.writeDouble(this.calculatedCyclicBladePitchH0S1);
        dest.writeDouble(this.calculatedCyclicBladePitchH0S2);
        dest.writeDouble(this.calculatedCyclicBladePitchH1S0);
        dest.writeDouble(this.calculatedCyclicBladePitchH1S1);
        dest.writeDouble(this.calculatedCyclicBladePitchH1S2);
        dest.writeDouble(this.calculatedCyclicBladePitchH2S0);
        dest.writeDouble(this.calculatedCyclicBladePitchH2S1);
        dest.writeDouble(this.calculatedCyclicBladePitchH2S2);
        dest.writeDouble(this.calculatedCyclicBladePitchH3S0);
        dest.writeDouble(this.calculatedCyclicBladePitchH3S1);
        dest.writeDouble(this.calculatedCyclicBladePitchH3S2);
        dest.writeDouble(this.calculatedCollectiveBladePitchH0);
        dest.writeDouble(this.calculatedCollectiveBladePitchH1);
        dest.writeDouble(this.calculatedCollectiveBladePitchH2);
        dest.writeDouble(this.calculatedCollectiveBladePitchH3);
        dest.writeDouble(this.calculatedBladePitchH0S0);
        dest.writeDouble(this.calculatedBladePitchH0S1);
        dest.writeDouble(this.calculatedBladePitchH0S2);
        dest.writeDouble(this.calculatedBladePitchH1S0);
        dest.writeDouble(this.calculatedBladePitchH1S1);
        dest.writeDouble(this.calculatedBladePitchH1S2);
        dest.writeDouble(this.calculatedBladePitchH2S0);
        dest.writeDouble(this.calculatedBladePitchH2S1);
        dest.writeDouble(this.calculatedBladePitchH2S2);
        dest.writeDouble(this.calculatedBladePitchH3S0);
        dest.writeDouble(this.calculatedBladePitchH3S1);
        dest.writeDouble(this.calculatedBladePitchH3S2);
        dest.writeDouble(this.calculatedServoDegreesH0S0);
        dest.writeDouble(this.calculatedServoDegreesH0S1);
        dest.writeDouble(this.calculatedServoDegreesH0S2);
        dest.writeDouble(this.calculatedServoDegreesH1S0);
        dest.writeDouble(this.calculatedServoDegreesH1S1);
        dest.writeDouble(this.calculatedServoDegreesH1S2);
        dest.writeDouble(this.calculatedServoDegreesH2S0);
        dest.writeDouble(this.calculatedServoDegreesH2S1);
        dest.writeDouble(this.calculatedServoDegreesH2S2);
        dest.writeDouble(this.calculatedServoDegreesH3S0);
        dest.writeDouble(this.calculatedServoDegreesH3S1);
        dest.writeDouble(this.calculatedServoDegreesH3S2);
        dest.writeInt(this.calculatedServoPulseH0S0);
        dest.writeInt(this.calculatedServoPulseH0S1);
        dest.writeInt(this.calculatedServoPulseH0S2);
        dest.writeInt(this.calculatedServoPulseH1S0);
        dest.writeInt(this.calculatedServoPulseH1S1);
        dest.writeInt(this.calculatedServoPulseH1S2);
        dest.writeInt(this.calculatedServoPulseH2S0);
        dest.writeInt(this.calculatedServoPulseH2S1);
        dest.writeInt(this.calculatedServoPulseH2S2);
        dest.writeInt(this.calculatedServoPulseH3S0);
        dest.writeInt(this.calculatedServoPulseH3S1);
        dest.writeInt(this.calculatedServoPulseH3S2);
        dest.writeDouble(this.cyclicHeading);
        dest.writeValue(this.id);
        dest.writeString(this.floidUuid);
        dest.writeString(this.type);
        dest.writeInt(this.floidId);
        dest.writeValue(this.floidMessageNumber);
        dest.writeDouble(this.velocityCyclicAlpha);
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    @SuppressWarnings("WeakerAccess")
    protected FloidModelStatusParcelable(Parcel in) {
        this.collectiveDeltaOrientationH0 = in.readDouble();
        this.collectiveDeltaOrientationH1 = in.readDouble();
        this.collectiveDeltaOrientationH2 = in.readDouble();
        this.collectiveDeltaOrientationH3 = in.readDouble();
        this.collectiveDeltaAltitudeH0 = in.readDouble();
        this.collectiveDeltaAltitudeH1 = in.readDouble();
        this.collectiveDeltaAltitudeH2 = in.readDouble();
        this.collectiveDeltaAltitudeH3 = in.readDouble();
        this.collectiveDeltaHeadingH0 = in.readDouble();
        this.collectiveDeltaHeadingH1 = in.readDouble();
        this.collectiveDeltaHeadingH2 = in.readDouble();
        this.collectiveDeltaHeadingH3 = in.readDouble();
        this.collectiveDeltaTotalH0 = in.readDouble();
        this.collectiveDeltaTotalH1 = in.readDouble();
        this.collectiveDeltaTotalH2 = in.readDouble();
        this.collectiveDeltaTotalH3 = in.readDouble();
        this.cyclicValueH0S0 = in.readDouble();
        this.cyclicValueH0S1 = in.readDouble();
        this.cyclicValueH0S2 = in.readDouble();
        this.cyclicValueH1S0 = in.readDouble();
        this.cyclicValueH1S1 = in.readDouble();
        this.cyclicValueH1S2 = in.readDouble();
        this.cyclicValueH2S0 = in.readDouble();
        this.cyclicValueH2S1 = in.readDouble();
        this.cyclicValueH2S2 = in.readDouble();
        this.cyclicValueH3S0 = in.readDouble();
        this.cyclicValueH3S1 = in.readDouble();
        this.cyclicValueH3S2 = in.readDouble();
        this.collectiveValueH0 = in.readDouble();
        this.collectiveValueH1 = in.readDouble();
        this.collectiveValueH2 = in.readDouble();
        this.collectiveValueH3 = in.readDouble();
        this.calculatedCyclicBladePitchH0S0 = in.readDouble();
        this.calculatedCyclicBladePitchH0S1 = in.readDouble();
        this.calculatedCyclicBladePitchH0S2 = in.readDouble();
        this.calculatedCyclicBladePitchH1S0 = in.readDouble();
        this.calculatedCyclicBladePitchH1S1 = in.readDouble();
        this.calculatedCyclicBladePitchH1S2 = in.readDouble();
        this.calculatedCyclicBladePitchH2S0 = in.readDouble();
        this.calculatedCyclicBladePitchH2S1 = in.readDouble();
        this.calculatedCyclicBladePitchH2S2 = in.readDouble();
        this.calculatedCyclicBladePitchH3S0 = in.readDouble();
        this.calculatedCyclicBladePitchH3S1 = in.readDouble();
        this.calculatedCyclicBladePitchH3S2 = in.readDouble();
        this.calculatedCollectiveBladePitchH0 = in.readDouble();
        this.calculatedCollectiveBladePitchH1 = in.readDouble();
        this.calculatedCollectiveBladePitchH2 = in.readDouble();
        this.calculatedCollectiveBladePitchH3 = in.readDouble();
        this.calculatedBladePitchH0S0 = in.readDouble();
        this.calculatedBladePitchH0S1 = in.readDouble();
        this.calculatedBladePitchH0S2 = in.readDouble();
        this.calculatedBladePitchH1S0 = in.readDouble();
        this.calculatedBladePitchH1S1 = in.readDouble();
        this.calculatedBladePitchH1S2 = in.readDouble();
        this.calculatedBladePitchH2S0 = in.readDouble();
        this.calculatedBladePitchH2S1 = in.readDouble();
        this.calculatedBladePitchH2S2 = in.readDouble();
        this.calculatedBladePitchH3S0 = in.readDouble();
        this.calculatedBladePitchH3S1 = in.readDouble();
        this.calculatedBladePitchH3S2 = in.readDouble();
        this.calculatedServoDegreesH0S0 = in.readDouble();
        this.calculatedServoDegreesH0S1 = in.readDouble();
        this.calculatedServoDegreesH0S2 = in.readDouble();
        this.calculatedServoDegreesH1S0 = in.readDouble();
        this.calculatedServoDegreesH1S1 = in.readDouble();
        this.calculatedServoDegreesH1S2 = in.readDouble();
        this.calculatedServoDegreesH2S0 = in.readDouble();
        this.calculatedServoDegreesH2S1 = in.readDouble();
        this.calculatedServoDegreesH2S2 = in.readDouble();
        this.calculatedServoDegreesH3S0 = in.readDouble();
        this.calculatedServoDegreesH3S1 = in.readDouble();
        this.calculatedServoDegreesH3S2 = in.readDouble();
        this.calculatedServoPulseH0S0 = in.readInt();
        this.calculatedServoPulseH0S1 = in.readInt();
        this.calculatedServoPulseH0S2 = in.readInt();
        this.calculatedServoPulseH1S0 = in.readInt();
        this.calculatedServoPulseH1S1 = in.readInt();
        this.calculatedServoPulseH1S2 = in.readInt();
        this.calculatedServoPulseH2S0 = in.readInt();
        this.calculatedServoPulseH2S1 = in.readInt();
        this.calculatedServoPulseH2S2 = in.readInt();
        this.calculatedServoPulseH3S0 = in.readInt();
        this.calculatedServoPulseH3S1 = in.readInt();
        this.calculatedServoPulseH3S2 = in.readInt();
        this.cyclicHeading = in.readDouble();
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.floidUuid = in.readString();
        this.type = in.readString();
        this.floidId = in.readInt();
        this.floidMessageNumber = (Long) in.readValue(Long.class.getClassLoader());
        this.velocityCyclicAlpha = in.readDouble();
    }

    /**
     * The creator
     */
    public static final Creator<FloidModelStatusParcelable> CREATOR = new Creator<FloidModelStatusParcelable>() {
        @Override
        public FloidModelStatusParcelable createFromParcel(Parcel source) {
            return new FloidModelStatusParcelable(source);
        }

        @Override
        public FloidModelStatusParcelable[] newArray(int size) {
            return new FloidModelStatusParcelable[size];
        }
    };
}
