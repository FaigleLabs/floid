/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.validator;

import android.widget.EditText;

/**
 * IP address edit text validator
 */
public class FloidIPAddressEditTextValidator extends FloidEditTextValidator {
    private static final String IP_ADDRESS_FORMAT_ERROR_MESSAGE = "[0-255].[0-255].[0-255].[0-255]";

    /**
     * Create an ip address edit text validator
     *
     * @param editText                       the edit text
     * @param floidEditTextValidatorCallback the validator callback
     */
    public FloidIPAddressEditTextValidator(EditText editText, FloidEditTextValidatorCallback floidEditTextValidatorCallback) {
        super(editText, floidEditTextValidatorCallback);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        checkText();
        mFloidEditTextValidatorCallback.updateInterface();
    }

    private void checkText() {
        mEditText.setError(null);
        // Get the address:
        String ipAddressString = mEditText.getText().toString();
        String[] ipByteStrings = ipAddressString.split("\\.");
        if (ipByteStrings.length != 4)  // NOTE: ipv4 only!!!
        {
            mEditText.setError(IP_ADDRESS_FORMAT_ERROR_MESSAGE);
        } else {
            for (int i = 0; i < 4; ++i)  // NOTE: ipv4 only!!!
            {
                try {
                    int tupleInteger = Integer.parseInt(ipByteStrings[i]);
                    if ((tupleInteger < 0) || (tupleInteger > 255)) {
                        mEditText.setError(IP_ADDRESS_FORMAT_ERROR_MESSAGE);
                    }
                } catch (NumberFormatException nfe) {
                    mEditText.setError(IP_ADDRESS_FORMAT_ERROR_MESSAGE);
                }
            }
        }
    }
}
