/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a valid parameter entry and its options
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class DroidValidParameters implements Parcelable, Serializable {
    // Our tag for debugging:
    /**
     * The constant TAG.
     */
    private static final String TAG = "DroidValidParameters";
    /**
     * The string type constant
     */
    public static final int STRING_TYPE = 0;
    /**
     * The integer type constant
     */
    public static final int INTEGER_TYPE = 1;
    /**
     * The float type constant
     */
    public static final int FLOAT_TYPE = 2;
    /**
     * The boolean type constant
     */
    public static final int BOOLEAN_TYPE = 3;
    /**
     * The screen size type constant
     */
    public static final int SCREEN_SIZE_TYPE = 4;
    /**
     * The boolean true constant
     */
    public static final String BOOLEAN_TRUE = "TRUE";
    /**
     * The boolean false constant
     */
    public static final String BOOLEAN_FALSE = "FALSE";
    /**
     * The boolean default constant
     */
    public static final String BOOLEAN_DEFAULT = "DEFAULT";

    /**
     * The constant DROID_PARAMETER_TYPE_NAME_BOOLEAN.
     */
    public static final String DROID_PARAMETER_TYPE_NAME_BOOLEAN = "Boolean";
    /**
     * The constant DROID_PARAMETER_TYPE_NAME_FLOAT.
     */
    public static final String DROID_PARAMETER_TYPE_NAME_FLOAT = "Float";
    /**
     * The constant DROID_PARAMETER_TYPE_NAME_INTEGER.
     */
    public static final String DROID_PARAMETER_TYPE_NAME_INTEGER = "Integer";
    /**
     * The constant DROID_PARAMETER_TYPE_NAME_SCREEN_SIZE.
     */
    public static final String DROID_PARAMETER_TYPE_NAME_SCREEN_SIZE = "Screen (#x#)";
    /**
     * The constant DROID_PARAMETER_TYPE_NAME_STRING.
     */
    public static final String DROID_PARAMETER_TYPE_NAME_STRING = "String";

    private final int mType;
    private final boolean mUseMin;
    private final boolean mUseMax;
    private final int mIntRangeMin;
    private final int mIntRangeMax;
    private final float mFloatRangeMin;
    private final float mFloatRangeMax;
    private final List<String> mValidStrings;
    private final List<Integer> mValidInts;
    private final List<Float> mValidFloats;
    private int mIntDefault;
    private float mFloatDefault;
    private boolean mBooleanDefault;
    private final String mStringDefault;
    private final boolean mUseOptionsOnly;

    /**
     * Create a valid droid parameter entry
     *
     * @param type           the type
     * @param useMin         this has a lower bound
     * @param useMax         this has an upper bound
     * @param intRangeMin    integer min
     * @param intRangeMax    integer max
     * @param intDefault     integer default
     * @param floatRangeMin  float min
     * @param floatRangeMax  float max
     * @param floatDefault   float default
     * @param booleanDefault boolean default
     * @param stringDefault  string default
     * @param validStrings   list of valid strings
     * @param validInts      list of valid ints
     * @param validFloats    list of valid floats
     * @param useOptionsOnly use only the valid parameters as options
     */
    public DroidValidParameters(int type, boolean useMin, boolean useMax,
                                int intRangeMin, int intRangeMax, int intDefault,
                                float floatRangeMin, float floatRangeMax, float floatDefault,
                                boolean booleanDefault, String stringDefault,
                                List<String> validStrings,
                                @SuppressWarnings("SameParameterValue") List<Integer> validInts,
                                @SuppressWarnings("SameParameterValue") List<Float> validFloats,
                                boolean useOptionsOnly) {
        super();
        mType = type;
        mUseMin = useMin;
        mUseMax = useMax;
        mIntRangeMin = intRangeMin;
        mIntRangeMax = intRangeMax;
        mIntDefault = intDefault;
        mFloatRangeMin = floatRangeMin;
        mFloatRangeMax = floatRangeMax;
        mFloatDefault = floatDefault;
        mValidStrings = validStrings;
        mBooleanDefault = booleanDefault;
        mStringDefault = stringDefault;
        mValidInts = validInts;
        mValidFloats = validFloats;
        mUseOptionsOnly = useOptionsOnly;
    }

    /**
     * Is this a valid parameters
     *
     * @param parameter the parameter
     * @return true if valid
     */
    public boolean isValid(String parameter) {
        if(BOOLEAN_DEFAULT.equals(parameter)) {
            return true;
        }
        if (mType == BOOLEAN_TYPE) {
            if ((parameter.equals(BOOLEAN_TRUE)) || (parameter.equals(BOOLEAN_FALSE))) {
                return true;
            }
        }
        if (mValidStrings.contains(parameter)) {
            return true;  // Valid by default
        }
        if (mType == INTEGER_TYPE) {
            try {
                int i = Integer.parseInt(parameter);
                if (mUseOptionsOnly) {
                    return mValidInts.contains(i);
                }
                return !(mUseMin && i < mIntRangeMin) && !(mUseMax && i > mIntRangeMax);
            } catch (Exception e) {
                return false;
            }
        }
        if (mType == FLOAT_TYPE) {
            try {
                float f = Float.parseFloat(parameter);
                if (mUseOptionsOnly) {
                    return mValidFloats.contains(f);
                }
                return !(mUseMin && f < mFloatRangeMin) && !(mUseMax && f > mFloatRangeMax);
            } catch (Exception e) {
                return false;
            }
        }
        if (mType == SCREEN_SIZE_TYPE) {
            try {
                // Split on "x"
                String[] sizes = parameter.split("x");
                if (sizes.length != 2) {
                    return false;
                }
                if(mUseOptionsOnly) {
                    return mValidStrings.contains(parameter);
                }
                // And try to get two integers:
                try {
                    // The following are simply an attempt to throw an error
                    Integer.valueOf(sizes[0]);  // Width
                    Integer.valueOf(sizes[1]);  // Height (??)
                    return true;
                } catch (Exception e) {
                    Log.e(TAG, "Failed to convert SCREEN_SIZE_TYPE strings - exception: ", e);
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
        }
        if (mType == STRING_TYPE) {
            return true;
        }
        return false; // This is a logic error - should never get here
    }

    /**
     * Get this parameter as a valid string
     *
     * @param parameter the parameter
     * @return a valid string for that parameter
     */
    public String getAsValidString(String parameter) {
        if(BOOLEAN_DEFAULT.equals(parameter)) {
            return mStringDefault;
        }
        if(mUseOptionsOnly) {
            if (mValidStrings.contains(parameter)) {
                return parameter;  // Valid by default
            } else {
                return null; // Not valid
            }
        } else {
            // All are valid:
            return parameter;
        }
    }

    /**
     * Get this parameter as a valid int
     *
     * @param parameter the parameter
     * @return a valid int for that parameter
     */
    public int getAsValidInt(String parameter) throws Exception {
        if (mType == INTEGER_TYPE) {
            if(BOOLEAN_DEFAULT.equals(parameter)) {
                return mIntDefault;
            }
            try {
                int i = Integer.parseInt(parameter);
                if(mUseOptionsOnly) {
                    if(mValidInts.contains(i)) {
                        return i;
                    }
                    throw new Exception();
                }
                if (mUseMin) {
                    if (i < mIntRangeMin) {
                        throw new Exception();
                    }
                }
                if (mUseMax) {
                    if (i > mIntRangeMax) {
                        throw new Exception();
                    }
                }
                if (mValidInts != null) {
                    if (mValidInts.contains(i)) {
                        return i;
                    }
                    throw new Exception();
                }
                return i;
            } catch (Exception e) {
                throw new Exception();
            }
        }
        throw new Exception();
    }

    /**
     * Get this parameter as a valid float
     *
     * @param parameter the parameter
     * @return a valid float for that parameter
     */
    public float getAsValidFloat(String parameter) throws Exception {
        if (mType == FLOAT_TYPE) {
            if(BOOLEAN_DEFAULT.equals(parameter)) {
                return mFloatDefault;
            }
            try {
                float f = Float.parseFloat(parameter);
                if(mUseOptionsOnly) {
                    if(mValidFloats.contains(f)) {
                        return f;
                    }
                    throw new Exception();
                }
                if (mUseMin) {
                    if (f < mFloatRangeMin) {
                        throw new Exception();
                    }
                }
                if (mUseMax) {
                    if (f > mFloatRangeMax) {
                        throw new Exception();
                    }
                }
                if (mValidFloats != null) {
                    if (mValidFloats.contains((f))) {
                        return f;
                    }
                    throw new Exception();
                }
                return f;
            } catch (Exception e) {
                throw new Exception();
            }
        }
        throw new Exception();
    }

    /**
     * Get this parameter as a valid boolean
     *
     * @param parameter the parameter
     * @return a valid boolean for that parameter
     */
    public boolean getAsValidBoolean(String parameter) throws Exception {
        if (mType == BOOLEAN_TYPE) {
            if (BOOLEAN_TRUE.equals(parameter)) {
                return true;
            }
            if (BOOLEAN_FALSE.equals(parameter)) {
                return false;
            }
            if (BOOLEAN_DEFAULT.equals(parameter)) {
                return mBooleanDefault;
            }
        }
        throw new Exception();
    }

    /**
     * Get this parameter as a valid screen size
     *
     * @param parameter the parameter
     * @return a valid int[2] (screen size - w x h) for that parameter
     */
    public int[] getAsScreenSize(String parameter) throws Exception {
        if(parameter != null) {
            parameter = parameter.replace("X", "x");
            // Split on "X"
            String[] sizes = parameter.split("x");
            if (sizes.length != 2) {
                throw new Exception();
            }
            // Verify that it is in the options if needed:
            if (mUseOptionsOnly) {
                if (!mValidStrings.contains(parameter)) {
                    throw new Exception();
                }
            }
            // And try to get two integers:
            try {
                int[] screenSize = new int[2];
                screenSize[0] = Integer.parseInt(sizes[0]);
                screenSize[1] = Integer.parseInt(sizes[1]);
                return screenSize;
            } catch (Exception e) {
                throw new Exception();
            }
        }
        throw new Exception();
    }

    /**
     * Get the integer default value
     *
     * @return the integer default value
     */
    public int getIntDefault() {
        return mIntDefault;
    }

    /**
     * Set the integer default value
     *
     * @param intDefault the integer default value
     */
    public void setIntDefault(int intDefault) {
        this.mIntDefault = intDefault;
    }

    /**
     * Get the float default value
     *
     * @return the float default value
     */
    public float getFloatDefault() {
        return mFloatDefault;
    }

    /**
     * Set the float default value
     *
     * @param floatDefault the float default value
     */
    public void setFloatDefault(float floatDefault) {
        this.mFloatDefault = floatDefault;
    }

    /**
     * Get the boolean default value
     *
     * @return the boolean default value
     */
    public boolean getBooleanDefault() {
        return mBooleanDefault;
    }

    /**
     * Set the boolean default value
     *
     * @param booleanDefault the new boolean default value
     */
    public void setBooleanDefault(Boolean booleanDefault) {
        this.mBooleanDefault = booleanDefault;
    }

    /**
     * GEt the type
     *
     * @return the type
     */
    public int getType() {
        return mType;
    }

    /**
     * Is use min boolean.
     *
     * @return the boolean
     */
    public boolean isUseMin() {
        return mUseMin;
    }

    /**
     * Is use max boolean.
     *
     * @return the boolean
     */
    public boolean isUseMax() {
        return mUseMax;
    }

    /**
     * Gets int range min.
     *
     * @return the int range min
     */
    public int getIntRangeMin() {
        return mIntRangeMin;
    }

    /**
     * Gets int range max.
     *
     * @return the int range max
     */
    public int getIntRangeMax() {
        return mIntRangeMax;
    }

    /**
     * Gets float range min.
     *
     * @return the float range min
     */
    public float getFloatRangeMin() {
        return mFloatRangeMin;
    }

    /**
     * Gets float range max.
     *
     * @return the float range max
     */
    public float getFloatRangeMax() {
        return mFloatRangeMax;
    }

    /**
     * Gets valid strings.
     *
     * @return the valid strings
     */
    public List<String> getValidStrings() {
        return mValidStrings;
    }

    /**
     * Gets valid ints.
     *
     * @return the valid ints
     */
    public List<Integer> getValidInts() {
        return mValidInts;
    }

    /**
     * Gets valid floats.
     *
     * @return the valid floats
     */
    public  List<Float> getValidFloats() {
        return mValidFloats;
    }

    /**
     * Is use options only boolean.
     *
     * @return the boolean
     */
    public boolean isUseOptionsOnly() {
        return mUseOptionsOnly;
    }

    /**
     * Gets string default.
     *
     * @return the string default
     */
    public String getStringDefault() {
        return mStringDefault;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mType);
        dest.writeByte(this.mUseMin ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mUseMax ? (byte) 1 : (byte) 0);
        dest.writeInt(this.mIntRangeMin);
        dest.writeInt(this.mIntRangeMax);
        dest.writeFloat(this.mFloatRangeMin);
        dest.writeFloat(this.mFloatRangeMax);
        dest.writeStringList(this.mValidStrings);
        dest.writeList(this.mValidInts);
        dest.writeList(this.mValidFloats);
        dest.writeInt(this.mIntDefault);
        dest.writeFloat(this.mFloatDefault);
        dest.writeByte(this.mBooleanDefault ? (byte) 1 : (byte) 0);
        dest.writeString(this.mStringDefault);
        dest.writeByte(this.mUseOptionsOnly ? (byte) 1 : (byte) 0);
    }

    /**
     * Create from parcel
     * @param in the parcel
     */
    protected DroidValidParameters(Parcel in) {
        this.mType = in.readInt();
        this.mUseMin = in.readByte() != 0;
        this.mUseMax = in.readByte() != 0;
        this.mIntRangeMin = in.readInt();
        this.mIntRangeMax = in.readInt();
        this.mFloatRangeMin = in.readFloat();
        this.mFloatRangeMax = in.readFloat();
        this.mValidStrings = in.createStringArrayList();
        this.mValidInts = new ArrayList<>();
        in.readList(this.mValidInts, Integer.class.getClassLoader());
        this.mValidFloats = new ArrayList<>();
        in.readList(this.mValidFloats, Float.class.getClassLoader());
        this.mIntDefault = in.readInt();
        this.mFloatDefault = in.readFloat();
        this.mBooleanDefault = in.readByte() != 0;
        this.mStringDefault = in.readString();
        this.mUseOptionsOnly = in.readByte() != 0;
    }

    /**
     * Static CREATOR
     */
    public static final Creator<DroidValidParameters> CREATOR = new Creator<DroidValidParameters>() {
        @Override
        public DroidValidParameters createFromParcel(Parcel source) {
            return new DroidValidParameters(source);
        }

        @Override
        public DroidValidParameters[] newArray(int size) {
            return new DroidValidParameters[size];
        }
    };
}
