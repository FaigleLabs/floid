/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.imaging;

/**
 * Image and metadata representation in the service
 */
public class FloidServiceImage {
    private String imageName;
    private String imageType;
    private String imageExtension;
    private double latitude;
    private double longitude;
    private double altitude;
    private double pan; // TODO [FUTURE]
    private double tilt; // TODO [FUTURE]
    private byte[] imageBytes;
    /**
     * Get images bytes byte [ ].
     *
     * @return the byte [ ]
     */
    public byte[] getImageBytes() {
        return imageBytes;
    }

    /**
     * Sets image bytes.
     *
     * @param imageBytes the image bytes
     */
    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    /**
     * Gets image name.
     *
     * @return the image name
     */
    public String getImageName() {
        return imageName;
    }

    /**
     * Sets image name.
     *
     * @param imageName the image name
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**
     * Gets image type.
     *
     * @return the image type
     */
    public String getImageType() {
        return imageType;
    }

    /**
     * Sets image type.
     *
     * @param imageType the image type
     */
    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    /**
     * Gets image extension.
     *
     * @return the image extension
     */
    public String getImageExtension() {
        return imageExtension;
    }

    /**
     * Sets image extension.
     *
     * @param imageExtension the image extension
     */
    public void setImageExtension(String imageExtension) {
        this.imageExtension = imageExtension;
    }

    /**
     * Gets latitude.
     *
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Sets latitude.
     *
     * @param latitude the latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * Gets longitude.
     *
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Sets longitude.
     *
     * @param longitude the longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * Gets altitude.
     *
     * @return the altitude
     */
    public double getAltitude() {
        return altitude;
    }

    /**
     * Sets altitude.
     *
     * @param altitude the altitude
     */
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    /**
     * Gets pan.
     *
     * @return the pan
     */
    public double getPan() {
        return pan;
    }

    /**
     * Sets pan.
     *
     * @param pan the pan
     */
    public void setPan(double pan) {
        this.pan = pan;
    }

    /**
     * Gets tilt.
     *
     * @return the tilt
     */
    public double getTilt() {
        return tilt;
    }

    /**
     * Sets tilt.
     *
     * @param tilt the tilt
     */
    public void setTilt(double tilt) {
        this.tilt = tilt;
    }
}
