/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.instructionprocessing;

import android.util.Log;
import com.faiglelabs.floid.servertypes.commands.FloidCommands;
import com.faiglelabs.floid.servertypes.commands.StartUpHelisCommand;
import com.faiglelabs.floid3.FloidOutgoingCommand;
import com.faiglelabs.floid3.FloidService;
import com.faiglelabs.floid3.processblocks.ProcessBlock;
import com.faiglelabs.floid3.processblocks.StartUpHelisProcessBlock;

/**
 * Command processor for start up helis command
 */
public class StartUpHelisCommandProcessor extends InstructionProcessor
{
    /**
     * Create a start up helis command processor
     * @param floidService the floid service
     * @param startUpHelisCommand the start up helis command
     */
    @SuppressWarnings("WeakerAccess")
	public StartUpHelisCommandProcessor(FloidService floidService, StartUpHelisCommand startUpHelisCommand)
	{
		super(floidService, startUpHelisCommand);
        useDefaultGoalProcessing  = false;
        completeOnCommandResponse = false;
		setProcessBlock(new StartUpHelisProcessBlock(60000L));        // This command times out in 1 minute
	}
	@Override
	public boolean setupInstruction()
	{
        StartUpHelisProcessBlock startUpHelisProcessBlock = (StartUpHelisProcessBlock)processBlock;
	    // 1. Need to check if floid is connected - if not return error
        if(!floidService.getFloidDroidStatus().isFloidConnected())
        {
            sendCommandLogicErrorMessage("Floid Not Connected");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
        }
        // Are the helis already started?
        if(floidService.getFloidStatus().isDh())
        {
            sendCommandLogicErrorMessage("Helis already started...");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
        }
        // Are we already in Heli Startup mode?
        if(floidService.getFloidStatus().isHsm())
        {
            sendCommandLogicErrorMessage("Heli Startup Mode already in progress...");
            processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
            return true;       // we are done but this is ignored above anyway and processInstruction is still called
        }
        ((StartUpHelisProcessBlock)processBlock).setStartupModeBegun(false);
        super.setupInstruction();
        // Log it:
        if(floidService.mUseLogger) Log.d(FloidService.TAG, "===>SETUP " + this.getDroidInstruction().getType());
        startUpHelisProcessBlock.setFloidGoalId(FloidService.NO_GOAL_ID);  // Does not use a goal id - done once command response is received
        try
        {
            FloidOutgoingCommand startUpHelisOutgoingCommand = floidService.sendStartUpHelisCommandToFloid(startUpHelisProcessBlock.getFloidGoalId());
            if(startUpHelisOutgoingCommand != null)
            {
                startUpHelisProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE);
                startUpHelisProcessBlock.setProcessBlockWaitForCommandNumber(startUpHelisOutgoingCommand.getCommandNumber());
                return true;
            }
            else
            {
                if(floidService.mUseLogger) Log.d(FloidService.TAG, "Failed to create Start Up Helis command.");
                // We must have failed - set completion with error:
                startUpHelisProcessBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
                return false;
            }
        }
        catch(Exception e)
        {
            if(floidService.mUseLogger) Log.d(FloidService.TAG, e.toString());
        }
        // We must have failed - set completion with error:
        if(floidService.mUseLogger) Log.d(FloidService.TAG, "Start helis - we must have failed " + this.getDroidInstruction().getType());

        processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_ERROR);
        return true;  // We are done with this whole command
	}
    @Override
    public boolean processInstruction()
    {
        StartUpHelisProcessBlock startUpHelisProcessBlock = (StartUpHelisProcessBlock)processBlock;
        if(startUpHelisProcessBlock.getProcessBlockStatus() == ProcessBlock.PROCESS_BLOCK_WAITING_FOR_COMMAND_RESPONSE) {
            // Did we time out or fail?
            boolean processInstructionResult = super.processInstruction();
            if(processInstructionResult) {
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "PI-1: Start Up Helis done");
            }
            return processInstructionResult;
        } else {
            // If the helis are on, we continue...
            if(floidService.getFloidStatus().isDh()) {
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "StartUpHelis: FloidStatus: Helis On - StartUpHelis complete");
                floidService.getFloidDroidStatus().setHelisStarted(true); // Helis are started!
                processBlock.setProcessBlockStatus(ProcessBlock.PROCESS_BLOCK_COMPLETED_SUCCESS);
                return true; // We had success
            }
            // Let the processor determine time out etc:
            if(super.processInstruction()) {
                floidService.addFloidDebugMessage(FloidCommands.FLOID_MESSAGE_PACKET_DEBUG, "PI-2: Start Up Helis done");
                return true;
            }
        }
        return false;
    }
    @Override
    public boolean tearDownInstruction()
    {
        super.tearDownInstruction();
        return true;
    }
}
