/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.floid3.imaging;

/**
 * The representation of a video frame in the floid service
 */
public class FloidServiceVideoFrame extends FloidServiceImage {
}
