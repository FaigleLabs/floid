package com.helloandroid.android.horizontalslider;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;

/**
 * A horizontal slider
 */
public class HorizontalSlider extends ProgressBar
{

    private OnProgressChangeListener listener;
    private static final int padding = 2;

    /**
     * The progress change listener interface
     */
    public interface OnProgressChangeListener
    {
        /**
         * The progress changed callback
         * @param v the view
         * @param progress the progress
         */
        @SuppressWarnings("UnusedParameters")
        void onProgressChanged(View v, int progress);

        /**
         * Perform click
         * @param v the view
         */
        @SuppressWarnings("UnusedParameters")
        void performClick(View v);
    }

    /**
     * Create a horizontal slider with this context, these attributes and this style
     * @param context the context
     * @param attrs the attributes
     * @param defStyle the style
     */
    public HorizontalSlider(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }
    /**
     * Create a horizontal slider with this context and these attributes
     * @param context the context
     * @param attrs the attributes
     */
    public HorizontalSlider(Context context, AttributeSet attrs)
    {
        super(context, attrs, android.R.attr.progressBarStyleHorizontal);
    }
    /**
     * Create a horizontal slider with this context
     * @param context the context
     */
    public HorizontalSlider(Context context)
    {
        super(context);
    }

    /**
     * Set the progress changed listener
     * @param l the progress changed listener
     */
    public void setOnProgressChangeListener(OnProgressChangeListener l)
    {
        listener = l;
    }
    @SuppressWarnings("EmptyMethod")
    @Override
    public boolean performClick()
    {
       return super.performClick();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if(isEnabled()) {
            int action = event.getAction();
            if (action == MotionEvent.ACTION_DOWN
                    || action == MotionEvent.ACTION_MOVE) {
                float x_mouse = event.getX() - padding;
                float width = getWidth() - 2 * padding;
                int progress = Math.round((float) getMax() * (x_mouse / width));
                if (progress < 0)
                    progress = 0;
                this.setProgress(progress);
                if (listener != null)
                    listener.onProgressChanged(this, progress);
            }
            if (action == MotionEvent.ACTION_UP) {
                performClick();
                if (listener != null)
                    listener.performClick(this);
            }
        }
        return true;
    }

}
