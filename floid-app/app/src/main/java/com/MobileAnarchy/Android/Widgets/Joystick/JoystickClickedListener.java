package com.MobileAnarchy.Android.Widgets.Joystick;

/**
 * Joystick clicked listener
 */
@SuppressWarnings("unused")
public interface JoystickClickedListener {
    /**
     * On close
     */
    void OnClicked();

    /**
     * On click
     */
    void OnReleased();
}

