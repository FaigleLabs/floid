package com.MobileAnarchy.Android.Widgets.Joystick;

/**
 * Joystick moved listener
 */
public interface JoystickMovedListener {
    /**
     * On moved
     *
     * @param pan  the pan value
     * @param tilt the tilt value
     */
    void OnMoved(int pan, int tilt);

    /**
     * On released
     */
    @SuppressWarnings("EmptyMethod")
    void OnReleased();

    /**
     * On returned to center
     */
    @SuppressWarnings("EmptyMethod")
    void OnReturnedToCenter();
}

