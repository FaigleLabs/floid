package com.faiglelabs.floid3;

/**
 * The type Floid variant.
 */
public class FloidVariant {
    /**
     * The constant VARIANT_DEMO.
     */
    public static int VARIANT_DEMO = 0;
    /**
     * The constant VARIANT_FULL.
     */
    @SuppressWarnings("unused")
    public static int VARIANT_FULL = 1;
    /**
     * The variant
     */
    private static final int variant = VARIANT_DEMO;
    /**
     * Gets variant.
     *
     * @return the variant
     */
    public static int getVariant() {
        return variant;
    }
}
