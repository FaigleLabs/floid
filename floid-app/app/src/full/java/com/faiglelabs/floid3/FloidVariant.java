package com.faiglelabs.floid3;

/**
 * The type Floid variant.
 */
class FloidVariant {
    /**
     * The constant VARIANT_DEMO.
     */
    @SuppressWarnings("unused")
    public static final int VARIANT_DEMO = 0;
    /**
     * The constant VARIANT_FULL.
     */
    public static final int VARIANT_FULL = 1;
    /**
     * The variant
     */
    private static final int variant = VARIANT_FULL;
    /**
     * Gets variant.
     *
     * @return the variant
     */
    public static int getVariant() {
        return variant;
    }
}
