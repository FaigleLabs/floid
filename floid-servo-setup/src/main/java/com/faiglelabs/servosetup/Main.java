/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/
package com.faiglelabs.servosetup;

/**
 * Servo setup main routine.
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Running");

        ServoSetupFrame servoSetupFrame = new ServoSetupFrame();

        servoSetupFrame.setSize(1100, 120);

        servoSetupFrame.setVisible(true);
    }

}



