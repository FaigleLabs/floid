/*
    (c) 2009-2020 Chris Faigle/faiglelabs
    All rights reserved.
    This code is strictly licensed.
*/

package com.faiglelabs.servosetup;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Servo setup frame.  JFrame for servo setup controls.
 */
class ServoSetupFrame extends JFrame implements SerialPortEventListener, ActionListener {
    private InputStream inputStream;
    private OutputStream outputStream;
    @SuppressWarnings("FieldCanBeLocal")
    private JPanel mainPanel;
    @SuppressWarnings("FieldCanBeLocal")
    private JPanel servoControlPanel;
    private JTextField fileNameTextField;
    private JButton connectButton;
    private JButton disconnectButton;
    @SuppressWarnings("FieldCanBeLocal")
    private JComboBox<String> heliComboBox;
    @SuppressWarnings("FieldCanBeLocal")
    private JButton heliOnButton;
    @SuppressWarnings("FieldCanBeLocal")
    private JButton heliOffButton;
    @SuppressWarnings("FieldCanBeLocal")
    private int servoNumber = 0;
    @SuppressWarnings("FieldCanBeLocal")
    private CommPortIdentifier portId = null;
    private SerialPort serialPort = null;
    private JTextField servoLeftTextField;
    @SuppressWarnings("FieldCanBeLocal")
    private JButton sendServoLeftButton;
    private JTextField servoRightTextField;
    @SuppressWarnings("FieldCanBeLocal")
    private JButton sendServoRightButton;
    private JTextField servoPitchTextField;
    @SuppressWarnings("FieldCanBeLocal")
    private JButton sendServoPitchButton;
    private JTextField servoESCTextField;
    @SuppressWarnings("FieldCanBeLocal")
    private JButton sendServoESCButton;

    /**
     * Instantiates a new Servo setup frame.
     *
     * @throws HeadlessException the headless exception
     */
    ServoSetupFrame() throws HeadlessException {
        super();


        // Create the main panel and set a layout:
        mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));


        JPanel filePanel = new JPanel();
        FlowLayout filePanelFlowLayout = new FlowLayout();
        filePanel.setLayout(filePanelFlowLayout);

        fileNameTextField = new JTextField(30);
        fileNameTextField.setText("/dev/tty.usbserial-A600emPV");
        filePanel.add(fileNameTextField);

        JButton browseButton = new JButton("Browse..");
        browseButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ae) {
                        JFileChooser fileChooser = new JFileChooser();
                        if (fileChooser.showOpenDialog(rootPane) == JFileChooser.APPROVE_OPTION) {
                            fileNameTextField.setText(fileChooser.getSelectedFile().getAbsolutePath());
                        }
                    }
                }
        );
        filePanel.add(browseButton);

        connectButton = new JButton("Connect");
        connectButton.addActionListener(this);
        filePanel.add(connectButton);

        disconnectButton = new JButton("Disconnect");
        disconnectButton.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (outputStream != null) {
                            try {
                                outputStream.close();
                            } catch (Exception e) {
                                // do nothing...
                            }
                            outputStream = null;
                        }
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (Exception e) {
                                // do nothing...
                            }
                            inputStream = null;
                        }

                        if (serialPort != null) {
                            try {
                                serialPort.removeEventListener();
                            } catch (Exception e) {
                                // do nothing...
                            }
                            try {
                                serialPort.close();
                            } catch (Exception e) {
                                // do nothing...
                            }
                            serialPort = null;
                        }
                        connectButton.setEnabled(true);
                        disconnectButton.setEnabled(false);
                    }
                }
        );
        disconnectButton.setEnabled(false);
        filePanel.add(disconnectButton);
        mainPanel.add(filePanel);
        servoControlPanel = new JPanel();
        servoControlPanel.setLayout(new FlowLayout());

        // Heli:
        JPanel heliPanel = new JPanel();
        heliPanel.setLayout(new FlowLayout());
        JLabel heliLabel = new JLabel("Heli:");
        heliPanel.add(heliLabel);

        String[] helis = new String[4];
        helis[0] = "0";
        helis[1] = "1";
        helis[2] = "2";
        helis[3] = "3";
        heliComboBox = new JComboBox<>(helis);
        heliComboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                int selectedIndex = cb.getSelectedIndex();
                setHeli(selectedIndex);
            }
        });
        heliPanel.add(heliComboBox);
        heliOnButton = new JButton("On");
        heliOnButton.addActionListener(new ActionListener() {
                                           public void actionPerformed(ActionEvent e) {
                                               try {
                                                   outputStream.write((byte) 'f');
                                               } catch (Exception e2) {
                                                   e2.printStackTrace();
                                               }
                                           }
                                       }
        );
        heliPanel.add(heliOnButton);
        heliOffButton = new JButton("Off");
        heliOffButton.addActionListener(new ActionListener() {
                                            public void actionPerformed(ActionEvent e) {
                                                try {
                                                    outputStream.write((byte) 'g');
                                                } catch (Exception e2) {
                                                    e2.printStackTrace();
                                                }
                                            }
                                        }
        );
        heliPanel.add(heliOffButton);
        servoControlPanel.add(heliPanel);

        // Left:
        JPanel servoLeftPanel = new JPanel();
        servoLeftPanel.setLayout(new FlowLayout());


        JLabel servoLeftLabel = new JLabel("Left:");
        servoLeftPanel.add(servoLeftLabel);

        servoLeftTextField = new JTextField("1500", 5);
        servoLeftPanel.add(servoLeftTextField);

        sendServoLeftButton = new JButton("Set");
        sendServoLeftButton.addActionListener(new ActionListener() {
                                                  public void actionPerformed(ActionEvent e) {
                                                      // Get the value:
                                                      int servoNum = Integer.valueOf(servoLeftTextField.getText());
                                                      sendServoLeftValue(servoNum);
                                                  }
                                              }
        );

        servoLeftPanel.add(sendServoLeftButton);
        servoControlPanel.add(servoLeftPanel);

        // Right:
        JPanel servoRightPanel = new JPanel();
        servoRightPanel.setLayout(new FlowLayout());

        JLabel servoRightLabel = new JLabel("Right:");
        servoRightPanel.add(servoRightLabel);

        servoRightTextField = new JTextField("1500", 5);
        servoRightPanel.add(servoRightTextField);

        sendServoRightButton = new JButton("Set");
        sendServoRightButton.addActionListener(new ActionListener() {
                                                   public void actionPerformed(ActionEvent e) {
                                                       // Get the value:
                                                       int servoNum = Integer.valueOf(servoRightTextField.getText());
                                                       sendServoRightValue(servoNum);
                                                   }
                                               }
        );

        servoRightPanel.add(sendServoRightButton);
        servoControlPanel.add(servoRightPanel);

        // Pitch:
        JPanel servoPitchPanel = new JPanel();
        servoPitchPanel.setLayout(new FlowLayout());

        JLabel servoPitchLabel = new JLabel("Pitch:");
        servoPitchPanel.add(servoPitchLabel);

        servoPitchTextField = new JTextField("1500", 5);
        servoPitchPanel.add(servoPitchTextField);

        sendServoPitchButton = new JButton("Set");
        sendServoPitchButton.addActionListener(new ActionListener() {
                                                   public void actionPerformed(ActionEvent e) {
                                                       // Get the value:
                                                       int servoNum = Integer.valueOf(servoPitchTextField.getText());
                                                       sendServoPitchValue(servoNum);
                                                   }
                                               }
        );

        servoPitchPanel.add(sendServoPitchButton);
        servoControlPanel.add(servoPitchPanel);

        // ESC:
        JPanel servoESCPanel = new JPanel();
        servoESCPanel.setLayout(new FlowLayout());

        JLabel servoESCLabel = new JLabel("ESC:");
        servoESCPanel.add(servoESCLabel);

        servoESCTextField = new JTextField("1000", 5);
        servoESCPanel.add(servoESCTextField);

        sendServoESCButton = new JButton("Set");
        sendServoESCButton.addActionListener(new ActionListener() {
                                                 public void actionPerformed(ActionEvent e) {
                                                     // Get the value:
                                                     int servoNum = Integer.valueOf(servoESCTextField.getText());
                                                     sendServoESCValue(servoNum);
                                                 }
                                             }
        );

        servoESCPanel.add(sendServoESCButton);
        servoControlPanel.add(servoESCPanel);


        mainPanel.add(servoControlPanel);

        this.getContentPane().add(mainPanel);
    }

    private void setHeli(int heli) {
        try {
            outputStream.write('0' + heli);
            outputStream.write('h');
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendServoLeftValue(int value) {
        // Convert back to a string
        String s = String.valueOf(value);

        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            byte b = (byte) c;
            assert ((char) b == c);      // Double check the logic
            try {
                outputStream.write(b);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            outputStream.write((byte) 'l');
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void sendServoRightValue(int value) {
        // Convert back to a string
        String s = String.valueOf(value);

        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            byte b = (byte) c;
            assert ((char) b == c);      // Double check the logic
            try {
                outputStream.write(b);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            outputStream.write((byte) 'r');
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendServoPitchValue(int value) {
        // Convert back to a string
        String s = String.valueOf(value);

        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            byte b = (byte) c;
            assert ((char) b == c);      // Double check the logic
            try {
                outputStream.write(b);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            outputStream.write((byte) 'p');
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendServoESCValue(int value) {
        // Convert back to a string
        String s = String.valueOf(value);

        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            byte b = (byte) c;
            assert ((char) b == c);      // Double check the logic
            try {
                outputStream.write(b);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            outputStream.write((byte) 'e');
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets output stream.
     *
     * @return the output stream
     */
    @SuppressWarnings("unused")
    public OutputStream getOutputStream() {
        return outputStream;
    }

    /**
     * Gets servo number.
     *
     * @return the servo number
     */
    @SuppressWarnings("unused")
    public int getServoNumber() {
        return servoNumber;
    }

    public void serialEvent(SerialPortEvent event) {
        switch (event.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY: {
            }
            break;
            case SerialPortEvent.DATA_AVAILABLE: {
                byte[] readBuffer = new byte[20000];
                try {
                    while (inputStream.available() > 0) {
                        int numBytes = inputStream.read(readBuffer);
                        for (int i = 0; i < numBytes; ++i) {
                            System.out.print("O: 0x" + Integer.toHexString(readBuffer[i]));
                        }
                    }
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
            break;
        }
    }

    public void actionPerformed(ActionEvent e) {
        try {
            portId = CommPortIdentifier.getPortIdentifier(fileNameTextField.getText());
            serialPort = (SerialPort) portId.open("ServoSetup", 2000);
            serialPort.setSerialPortParams(19200,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            inputStream = serialPort.getInputStream();
            outputStream = serialPort.getOutputStream();
            connectButton.setEnabled(false);
            disconnectButton.setEnabled(true);
            serialPort.addEventListener(this);

            Thread.sleep(100L); // quick snooze

            outputStream.write('c'); // Connect - we should see output on the serial port...
        } catch (Exception e2) {
            e2.printStackTrace();
            portId = null;
            serialPort = null;
        }
    }
}
