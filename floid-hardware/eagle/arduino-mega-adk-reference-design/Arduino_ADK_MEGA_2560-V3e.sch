<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="12" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="9" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="14" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="11" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="2" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="5" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="13" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="12" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="no" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="no" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="no"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="16" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="17" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic>
<libraries>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SmartPrj">
<packages>
<package name="FIDUCIA-MOUNT">
<circle x="0" y="0" radius="0.5" width="2.1844" layer="29"/>
<circle x="0" y="0" radius="1.5" width="0.127" layer="41"/>
<smd name="P$1" x="0" y="0" dx="1.016" dy="1.016" layer="1" roundness="100"/>
</package>
</packages>
<symbols>
<symbol name="FIDUCIAL">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIALMOUNT">
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIA-MOUNT">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jumper">
<packages>
<package name="SJ">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
<package name="SJW">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.524" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.524" x2="2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="1.27" x2="-1.905" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-1.27" x2="-1.905" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="1.905" y1="-1.524" x2="2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-1.27" x2="-2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.524" x2="1.905" y2="1.524" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.524" y1="0" x2="2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0" x2="-2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51" curve="-180"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51" curve="180"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="-2.159" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1" y="0" size="0.02" layer="27">&gt;VALUE</text>
<rectangle x1="0.762" y1="-0.762" x2="1.016" y2="0.762" layer="51"/>
<rectangle x1="1.016" y1="-0.635" x2="1.27" y2="0.635" layer="51"/>
<rectangle x1="1.27" y1="-0.508" x2="1.397" y2="0.508" layer="51"/>
<rectangle x1="1.397" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.016" y1="-0.762" x2="-0.762" y2="0.762" layer="51"/>
<rectangle x1="-1.27" y1="-0.635" x2="-1.016" y2="0.635" layer="51"/>
<rectangle x1="-1.397" y1="-0.508" x2="-1.27" y2="0.508" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.397" y2="0.254" layer="51"/>
<rectangle x1="0.9652" y1="-0.7112" x2="1.0922" y2="-0.5842" layer="51"/>
<rectangle x1="1.3462" y1="-0.3556" x2="1.4732" y2="-0.2286" layer="51"/>
<rectangle x1="1.3462" y1="0.2032" x2="1.4732" y2="0.3302" layer="51"/>
<rectangle x1="0.9652" y1="0.5842" x2="1.0922" y2="0.7112" layer="51"/>
<rectangle x1="-1.0922" y1="-0.7112" x2="-0.9652" y2="-0.5842" layer="51"/>
<rectangle x1="-1.4478" y1="-0.3302" x2="-1.3208" y2="-0.2032" layer="51"/>
<rectangle x1="-1.4732" y1="0.2032" x2="-1.3462" y2="0.3302" layer="51"/>
<rectangle x1="-1.1176" y1="0.5842" x2="-0.9906" y2="0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="SJ">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SJ" prefix="SJ" uservalue="yes">
<description>SMD solder &lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="SJ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJ">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="W" package="SJW">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="bibaja">
<packages>
<package name="J0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="1.025" y1="0.224" x2="1.515" y2="0.224" width="0.1524" layer="51"/>
<wire x1="1.515" y1="-0.224" x2="1.025" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="2.773" y2="0.483" width="0.0508" layer="39"/>
<wire x1="2.773" y1="0.483" x2="2.773" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="2.773" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="3" x="1.95" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-1.143" y="0.635" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="2.413" y="-0.635" size="1.016" layer="27" font="vector" ratio="15" rot="R180">&gt;VALUE</text>
<rectangle x1="0.716" y1="-0.3048" x2="1.016" y2="0.2951" layer="51"/>
<rectangle x1="1.5288" y1="-0.3048" x2="1.8288" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<rectangle x1="1.1001" y1="-0.4001" x2="1.4999" y2="0.4001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="J-US">
<wire x1="-2.54" y1="2.54" x2="-2.159" y2="3.556" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="3.556" x2="-1.524" y2="1.524" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="1.524" x2="-0.889" y2="3.556" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="3.556" x2="-0.254" y2="1.524" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="1.524" x2="0.381" y2="3.556" width="0.2032" layer="94"/>
<wire x1="0.381" y1="3.556" x2="1.016" y2="1.524" width="0.2032" layer="94"/>
<wire x1="1.016" y1="1.524" x2="1.651" y2="3.556" width="0.2032" layer="94"/>
<wire x1="1.651" y1="3.556" x2="2.286" y2="1.524" width="0.2032" layer="94"/>
<wire x1="2.286" y1="1.524" x2="2.54" y2="2.54" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-3.556" x2="2.54" y2="-2.54" width="0.2032" layer="94"/>
<wire x1="1.651" y1="-1.524" x2="2.286" y2="-3.556" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-3.556" x2="1.651" y2="-1.524" width="0.2032" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="1.016" y2="-3.556" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-3.556" x2="0.381" y2="-1.524" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="-1.524" x2="-0.254" y2="-3.556" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-3.556" x2="-0.889" y2="-1.524" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="-1.524" x2="-1.524" y2="-3.556" width="0.2032" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.159" y2="-1.524" width="0.2032" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="3.81" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-5.08" y="-5.715" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<pin name="C" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="1" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="J" prefix="R">
<description>Three terminal jumper made from two 0402, 0603, 0805, etc.</description>
<gates>
<gate name="G$1" symbol="J-US" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="J0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Resistenze">
<packages>
<package name="R0603-ROUND">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.683" x2="1.473" y2="0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.683" x2="1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.683" x2="-1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.683" x2="-1.473" y2="0.683" width="0.0508" layer="39"/>
<smd name="1" x="-0.889" y="0" dx="0.8984" dy="1.1" layer="1" roundness="80"/>
<smd name="2" x="0.889" y="0" dx="0.8984" dy="1.1" layer="1" roundness="80"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CAY16">
<description>&lt;b&gt;BOURNS&lt;/b&gt; Chip Resistor Array&lt;p&gt;
Source: RS Component / BUORNS</description>
<wire x1="-1.55" y1="0.75" x2="-1" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="0.75" x2="-0.2" y2="0.75" width="0.1016" layer="51"/>
<wire x1="0.2" y1="0.75" x2="0.6" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1" y1="0.75" x2="-0.6" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-0.2" y1="0.75" x2="0.2" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="0.6" y1="0.75" x2="1" y2="0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="1.55" y1="-0.75" x2="1" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.75" x2="0.2" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="-0.75" x2="-0.6" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="1" y1="-0.75" x2="0.6" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="0.2" y1="-0.75" x2="-0.2" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<wire x1="-0.6" y1="-0.75" x2="-1" y2="-0.75" width="0.1016" layer="51" curve="180"/>
<smd name="1" x="-1.2" y="-0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="2" x="-0.4" y="-0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="3" x="0.4" y="-0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="4" x="1.2" y="-0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="5" x="1.2" y="0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="6" x="0.4" y="0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="7" x="-0.4" y="0.675" dx="0.5" dy="0.65" layer="1"/>
<smd name="8" x="-1.2" y="0.675" dx="0.5" dy="0.65" layer="1"/>
<text x="-1.905" y="-2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="-1.63" y1="1.13" x2="-1.63" y2="-1.13" width="0.0508" layer="39"/>
<wire x1="-1.63" y1="-1.13" x2="1.63" y2="-1.13" width="0.0508" layer="39"/>
<wire x1="1.63" y1="-1.13" x2="1.63" y2="1.13" width="0.0508" layer="39"/>
<wire x1="1.63" y1="1.13" x2="-1.63" y2="1.13" width="0.0508" layer="39"/>
</package>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.118" y1="0.224" x2="0.118" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.118" y1="-0.224" x2="-0.118" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.828" y1="0.513" x2="0.828" y2="0.513" width="0.0508" layer="39"/>
<wire x1="0.828" y1="0.513" x2="0.828" y2="-0.513" width="0.0508" layer="39"/>
<wire x1="0.828" y1="-0.513" x2="-0.828" y2="-0.513" width="0.0508" layer="39"/>
<wire x1="-0.828" y1="-0.513" x2="-0.828" y2="0.513" width="0.0508" layer="39"/>
<smd name="1" x="-0.4445" y="0" dx="0.508" dy="0.762" layer="1" roundness="90"/>
<smd name="2" x="0.4445" y="0" dx="0.508" dy="0.762" layer="1" roundness="90"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-0.254" x2="-0.254" y2="0.254" layer="51"/>
<rectangle x1="0.254" y1="-0.254" x2="0.508" y2="0.254" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="R1NV">
<wire x1="-2.54" y1="-0.762" x2="2.54" y2="-0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.762" x2="-2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0.762" x2="-2.54" y2="-0.762" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="2.54" y="-3.048" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="-3.048" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="0026_100K_0603" prefix="R" uservalue="yes">
<description>100K 0603</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603-ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="0603"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="100K_0603"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Resistenze"/>
<attribute name="VALUE" value="100k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2022_CR0603FW104L" prefix="R" uservalue="yes">
<description>0603 Resistor 100K  1%</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603-ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PART_NUMBER" value="CR0603FW104L" constant="no"/>
<attribute name="VALUE" value="100K_1%" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2339_" prefix="R" uservalue="yes">
<description>19K1 0,1% 0603</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603-ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="0603"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value=""/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Resistenze"/>
<attribute name="VALUE" value="19K1_0,1%"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0411_56K_0603" prefix="R" uservalue="yes">
<description>56K 0603</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603-ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="0603"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="56K_0603"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Resistenze"/>
<attribute name="VALUE" value="56k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0003_064R_22R_/_CAY16-220J4LF" prefix="RN" uservalue="yes">
<description>22R 064R</description>
<gates>
<gate name="A" symbol="R1NV" x="0" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="B" symbol="R1NV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="C" symbol="R1NV" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="D" symbol="R1NV" x="0" y="-7.62" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="CAY16">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="SMD_3,2x1,6"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="064R_22R_/_CAY16-220J4LF"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Resistenze"/>
<attribute name="VALUE" value="22R"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0016_064R_10K_/_CAY16-103J4LF" prefix="RN" uservalue="yes">
<description>10K 064 R</description>
<gates>
<gate name="A" symbol="R1NV" x="0" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="B" symbol="R1NV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="C" symbol="R1NV" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="D" symbol="R1NV" x="0" y="-7.62" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="CAY16">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="SMD_3,2x1,6"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="064R_10K_/_CAY16-103J4LF"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Resistenze"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0039_JUMP_0402" prefix="R" uservalue="yes">
<description>0R 0402</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="0402"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="JUMP_0402"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Resistenze"/>
<attribute name="VALUE" value="0R"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0005_064R_1K_/_CAY16-102J4LF" prefix="R" uservalue="yes">
<description>1K 064R</description>
<gates>
<gate name="A" symbol="R1NV" x="0" y="7.62" addlevel="always" swaplevel="1"/>
<gate name="B" symbol="R1NV" x="0" y="2.54" addlevel="always" swaplevel="1"/>
<gate name="C" symbol="R1NV" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="D" symbol="R1NV" x="0" y="-7.62" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="CAY16">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="8"/>
<connect gate="B" pin="1" pad="2"/>
<connect gate="B" pin="2" pad="7"/>
<connect gate="C" pin="1" pad="3"/>
<connect gate="C" pin="2" pad="6"/>
<connect gate="D" pin="1" pad="4"/>
<connect gate="D" pin="2" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="SMD_3,2x1,6"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="064R_1K_/_CAY16-102J4LF"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Resistenze"/>
<attribute name="VALUE" value="1K"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0028_CR0603JW103L" prefix="R" uservalue="yes">
<description>10K 0603</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603-ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="0603"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="CR0603JW103L"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Resistenze"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IntegratedCircuits">
<packages>
<package name="QFN-20">
<circle x="-0.9" y="1.55" radius="0.065" width="0.127" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2.5" x2="1.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-2.5" x2="1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="2.5" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="-1.35" y1="2.75" x2="-1.25" y2="2.75" width="0.127" layer="29"/>
<wire x1="-1.25" y1="2.75" x2="-1.15" y2="2.75" width="0.127" layer="29"/>
<wire x1="-1.15" y1="2.75" x2="-1.15" y2="2.15" width="0.127" layer="29"/>
<wire x1="-1.15" y1="2.15" x2="-1.25" y2="2.15" width="0.127" layer="29"/>
<wire x1="-1.25" y1="2.15" x2="-1.35" y2="2.15" width="0.127" layer="29"/>
<wire x1="-1.35" y1="2.15" x2="-1.35" y2="2.75" width="0.127" layer="29"/>
<wire x1="-1.25" y1="2.75" x2="-1.25" y2="2.15" width="0.127" layer="29"/>
<wire x1="-0.85" y1="2.75" x2="-0.75" y2="2.75" width="0.127" layer="29"/>
<wire x1="-0.75" y1="2.75" x2="-0.65" y2="2.75" width="0.127" layer="29"/>
<wire x1="-0.65" y1="2.75" x2="-0.65" y2="2.15" width="0.127" layer="29"/>
<wire x1="-0.65" y1="2.15" x2="-0.75" y2="2.15" width="0.127" layer="29"/>
<wire x1="-0.75" y1="2.15" x2="-0.85" y2="2.15" width="0.127" layer="29"/>
<wire x1="-0.85" y1="2.15" x2="-0.85" y2="2.75" width="0.127" layer="29"/>
<wire x1="-0.75" y1="2.75" x2="-0.75" y2="2.15" width="0.127" layer="29"/>
<wire x1="-0.35" y1="2.75" x2="-0.25" y2="2.75" width="0.127" layer="29"/>
<wire x1="-0.25" y1="2.75" x2="-0.15" y2="2.75" width="0.127" layer="29"/>
<wire x1="-0.15" y1="2.75" x2="-0.15" y2="1.75" width="0.127" layer="29"/>
<wire x1="-0.15" y1="1.75" x2="-0.25" y2="1.75" width="0.127" layer="29"/>
<wire x1="-0.25" y1="1.75" x2="-0.35" y2="1.75" width="0.127" layer="29"/>
<wire x1="-0.35" y1="1.75" x2="-0.35" y2="2.75" width="0.127" layer="29"/>
<wire x1="-0.25" y1="2.75" x2="-0.25" y2="1.75" width="0.127" layer="29"/>
<wire x1="0.15" y1="2.75" x2="0.25" y2="2.75" width="0.127" layer="29"/>
<wire x1="0.25" y1="2.75" x2="0.35" y2="2.75" width="0.127" layer="29"/>
<wire x1="0.35" y1="2.75" x2="0.35" y2="1.75" width="0.127" layer="29"/>
<wire x1="0.35" y1="1.75" x2="0.25" y2="1.75" width="0.127" layer="29"/>
<wire x1="0.25" y1="1.75" x2="0.15" y2="1.75" width="0.127" layer="29"/>
<wire x1="0.15" y1="1.75" x2="0.15" y2="2.75" width="0.127" layer="29"/>
<wire x1="0.25" y1="2.75" x2="0.25" y2="1.75" width="0.127" layer="29"/>
<wire x1="0.65" y1="2.75" x2="0.75" y2="2.75" width="0.127" layer="29"/>
<wire x1="0.75" y1="2.75" x2="0.85" y2="2.75" width="0.127" layer="29"/>
<wire x1="0.85" y1="2.75" x2="0.85" y2="2.15" width="0.127" layer="29"/>
<wire x1="0.85" y1="2.15" x2="0.75" y2="2.15" width="0.127" layer="29"/>
<wire x1="0.75" y1="2.15" x2="0.65" y2="2.15" width="0.127" layer="29"/>
<wire x1="0.65" y1="2.15" x2="0.65" y2="2.75" width="0.127" layer="29"/>
<wire x1="0.75" y1="2.75" x2="0.75" y2="2.15" width="0.127" layer="29"/>
<wire x1="1.15" y1="2.75" x2="1.25" y2="2.75" width="0.127" layer="29"/>
<wire x1="1.25" y1="2.75" x2="1.35" y2="2.75" width="0.127" layer="29"/>
<wire x1="1.35" y1="2.75" x2="1.35" y2="2.15" width="0.127" layer="29"/>
<wire x1="1.35" y1="2.15" x2="1.25" y2="2.15" width="0.127" layer="29"/>
<wire x1="1.25" y1="2.15" x2="1.15" y2="2.15" width="0.127" layer="29"/>
<wire x1="1.15" y1="2.15" x2="1.15" y2="2.75" width="0.127" layer="29"/>
<wire x1="1.25" y1="2.75" x2="1.25" y2="2.15" width="0.127" layer="29"/>
<wire x1="1.15" y1="1.65" x2="1.75" y2="1.65" width="0.127" layer="29"/>
<wire x1="1.75" y1="1.65" x2="1.75" y2="1.55" width="0.127" layer="29"/>
<wire x1="1.75" y1="1.55" x2="1.75" y2="1.45" width="0.127" layer="29"/>
<wire x1="1.75" y1="1.45" x2="1.15" y2="1.45" width="0.127" layer="29"/>
<wire x1="1.15" y1="1.45" x2="1.15" y2="1.55" width="0.127" layer="29"/>
<wire x1="1.15" y1="1.55" x2="1.15" y2="1.65" width="0.127" layer="29"/>
<wire x1="1.15" y1="1.55" x2="1.75" y2="1.55" width="0.127" layer="29"/>
<wire x1="-1.75" y1="1.65" x2="-1.15" y2="1.65" width="0.127" layer="29"/>
<wire x1="-1.15" y1="1.65" x2="-1.15" y2="1.55" width="0.127" layer="29"/>
<wire x1="-1.15" y1="1.55" x2="-1.15" y2="1.45" width="0.127" layer="29"/>
<wire x1="-1.15" y1="1.45" x2="-1.75" y2="1.45" width="0.127" layer="29"/>
<wire x1="-1.75" y1="1.45" x2="-1.75" y2="1.55" width="0.127" layer="29"/>
<wire x1="-1.75" y1="1.55" x2="-1.75" y2="1.65" width="0.127" layer="29"/>
<wire x1="-1.75" y1="1.55" x2="-1.15" y2="1.55" width="0.127" layer="29"/>
<wire x1="-1.75" y1="1" x2="-0.75" y2="1" width="0.127" layer="29"/>
<wire x1="-0.75" y1="1" x2="-0.75" y2="0.9" width="0.127" layer="29"/>
<wire x1="-0.75" y1="0.9" x2="-0.75" y2="0.8" width="0.127" layer="29"/>
<wire x1="-0.75" y1="0.8" x2="-1.75" y2="0.8" width="0.127" layer="29"/>
<wire x1="-1.75" y1="0.8" x2="-1.75" y2="0.9" width="0.127" layer="29"/>
<wire x1="-1.75" y1="0.9" x2="-1.75" y2="1" width="0.127" layer="29"/>
<wire x1="-1.75" y1="0.9" x2="-0.75" y2="0.9" width="0.127" layer="29"/>
<wire x1="-1.75" y1="0.35" x2="-0.75" y2="0.35" width="0.127" layer="29"/>
<wire x1="-0.75" y1="0.35" x2="-0.75" y2="0.25" width="0.127" layer="29"/>
<wire x1="-0.75" y1="0.25" x2="-0.75" y2="0.15" width="0.127" layer="29"/>
<wire x1="-0.75" y1="0.15" x2="-1.75" y2="0.15" width="0.127" layer="29"/>
<wire x1="-1.75" y1="0.15" x2="-1.75" y2="0.25" width="0.127" layer="29"/>
<wire x1="-1.75" y1="0.25" x2="-1.75" y2="0.35" width="0.127" layer="29"/>
<wire x1="-1.75" y1="0.25" x2="-0.75" y2="0.25" width="0.127" layer="29"/>
<wire x1="0.75" y1="0.35" x2="1.75" y2="0.35" width="0.127" layer="29"/>
<wire x1="1.75" y1="0.35" x2="1.75" y2="0.25" width="0.127" layer="29"/>
<wire x1="1.75" y1="0.25" x2="1.75" y2="0.15" width="0.127" layer="29"/>
<wire x1="1.75" y1="0.15" x2="0.75" y2="0.15" width="0.127" layer="29"/>
<wire x1="0.75" y1="0.15" x2="0.75" y2="0.25" width="0.127" layer="29"/>
<wire x1="0.75" y1="0.25" x2="0.75" y2="0.35" width="0.127" layer="29"/>
<wire x1="0.75" y1="0.25" x2="1.75" y2="0.25" width="0.127" layer="29"/>
<wire x1="0.75" y1="1" x2="1.75" y2="1" width="0.127" layer="29"/>
<wire x1="1.75" y1="1" x2="1.75" y2="0.9" width="0.127" layer="29"/>
<wire x1="1.75" y1="0.9" x2="1.75" y2="0.8" width="0.127" layer="29"/>
<wire x1="1.75" y1="0.8" x2="0.75" y2="0.8" width="0.127" layer="29"/>
<wire x1="0.75" y1="0.8" x2="0.75" y2="0.9" width="0.127" layer="29"/>
<wire x1="0.75" y1="0.9" x2="0.75" y2="1" width="0.127" layer="29"/>
<wire x1="0.75" y1="0.9" x2="1.75" y2="0.9" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-0.3" x2="-1" y2="-0.3" width="0.127" layer="29"/>
<wire x1="-1" y1="-0.3" x2="-1" y2="-0.4" width="0.127" layer="29"/>
<wire x1="-1" y1="-0.4" x2="-1" y2="-0.5" width="0.127" layer="29"/>
<wire x1="-1" y1="-0.5" x2="-1.75" y2="-0.5" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-0.5" x2="-1.75" y2="-0.4" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-0.4" x2="-1.75" y2="-0.3" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-1.15" x2="-1" y2="-1.15" width="0.127" layer="29"/>
<wire x1="-1" y1="-1.15" x2="-1" y2="-1.25" width="0.127" layer="29"/>
<wire x1="-1" y1="-1.25" x2="-1" y2="-1.35" width="0.127" layer="29"/>
<wire x1="-1" y1="-1.35" x2="-1.75" y2="-1.35" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-1.35" x2="-1.75" y2="-1.25" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-1.25" x2="-1.75" y2="-1.15" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-0.4" x2="-1" y2="-0.4" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-1.25" x2="-1" y2="-1.25" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-2" x2="-1" y2="-2" width="0.127" layer="29"/>
<wire x1="-1" y1="-2" x2="-1" y2="-2.1" width="0.127" layer="29"/>
<wire x1="-1" y1="-2.1" x2="-1" y2="-2.2" width="0.127" layer="29"/>
<wire x1="-1" y1="-2.2" x2="-1.75" y2="-2.2" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-2.2" x2="-1.75" y2="-2.1" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-2.1" x2="-1.75" y2="-2" width="0.127" layer="29"/>
<wire x1="-1.75" y1="-2.1" x2="-1" y2="-2.1" width="0.127" layer="29"/>
<wire x1="1.75" y1="-0.3" x2="1" y2="-0.3" width="0.127" layer="29"/>
<wire x1="1" y1="-0.3" x2="1" y2="-0.4" width="0.127" layer="29"/>
<wire x1="1" y1="-0.4" x2="1" y2="-0.5" width="0.127" layer="29"/>
<wire x1="1" y1="-0.5" x2="1.75" y2="-0.5" width="0.127" layer="29"/>
<wire x1="1.75" y1="-0.5" x2="1.75" y2="-0.4" width="0.127" layer="29"/>
<wire x1="1.75" y1="-0.4" x2="1.75" y2="-0.3" width="0.127" layer="29"/>
<wire x1="1.75" y1="-1.15" x2="1" y2="-1.15" width="0.127" layer="29"/>
<wire x1="1" y1="-1.15" x2="1" y2="-1.25" width="0.127" layer="29"/>
<wire x1="1" y1="-1.25" x2="1" y2="-1.35" width="0.127" layer="29"/>
<wire x1="1" y1="-1.35" x2="1.75" y2="-1.35" width="0.127" layer="29"/>
<wire x1="1.75" y1="-1.35" x2="1.75" y2="-1.25" width="0.127" layer="29"/>
<wire x1="1.75" y1="-1.25" x2="1.75" y2="-1.15" width="0.127" layer="29"/>
<wire x1="1" y1="-1.25" x2="1.75" y2="-1.25" width="0.127" layer="29"/>
<wire x1="1" y1="-0.4" x2="1.75" y2="-0.4" width="0.127" layer="29"/>
<wire x1="1.75" y1="-2" x2="1" y2="-2" width="0.127" layer="29"/>
<wire x1="1" y1="-2" x2="1" y2="-2.1" width="0.127" layer="29"/>
<wire x1="1" y1="-2.1" x2="1" y2="-2.2" width="0.127" layer="29"/>
<wire x1="1" y1="-2.2" x2="1.75" y2="-2.2" width="0.127" layer="29"/>
<wire x1="1.75" y1="-2.2" x2="1.75" y2="-2.1" width="0.127" layer="29"/>
<wire x1="1.75" y1="-2.1" x2="1.75" y2="-2" width="0.127" layer="29"/>
<wire x1="1" y1="-2.1" x2="1.75" y2="-2.1" width="0.127" layer="29"/>
<rectangle x1="-0.575" y1="0.18" x2="0.575" y2="0.97" layer="41"/>
<smd name="1" x="-1.45" y="1.55" dx="0.25" dy="0.7" layer="1" roundness="60" rot="R90" stop="no"/>
<smd name="2" x="-1.25" y="0.9" dx="0.25" dy="1.1" layer="1" roundness="60" rot="R90" stop="no"/>
<smd name="3" x="-1.25" y="0.25" dx="0.25" dy="1.1" layer="1" roundness="60" rot="R90" stop="no"/>
<smd name="4" x="-1.45" y="-0.4" dx="0.25" dy="0.7" layer="1" roundness="60" rot="R270" stop="no"/>
<smd name="5" x="-1.45" y="-1.25" dx="0.25" dy="0.7" layer="1" roundness="60" rot="R270" stop="no"/>
<smd name="6" x="-1.45" y="-2.1" dx="0.25" dy="0.7" layer="1" roundness="60" rot="R270" stop="no"/>
<smd name="7" x="1.45" y="-2.1" dx="0.25" dy="0.7" layer="1" roundness="60" rot="R270" stop="no"/>
<smd name="8" x="1.45" y="-1.25" dx="0.25" dy="0.7" layer="1" roundness="60" rot="R270" stop="no"/>
<smd name="9" x="1.45" y="-0.4" dx="0.25" dy="0.7" layer="1" roundness="60" rot="R270" stop="no"/>
<smd name="10" x="1.25" y="0.25" dx="0.25" dy="1.1" layer="1" roundness="60" rot="R90" stop="no"/>
<smd name="11" x="1.25" y="0.9" dx="0.25" dy="1.1" layer="1" roundness="60" rot="R90" stop="no"/>
<smd name="12" x="1.45" y="1.55" dx="0.25" dy="0.7" layer="1" roundness="60" rot="R90" stop="no"/>
<smd name="13" x="1.25" y="2.45" dx="0.25" dy="0.7" layer="1" roundness="60" stop="no"/>
<smd name="14" x="0.75" y="2.45" dx="0.25" dy="0.7" layer="1" roundness="60" stop="no"/>
<smd name="15" x="0.25" y="2.25" dx="0.25" dy="1.1" layer="1" roundness="60" stop="no"/>
<smd name="16" x="-0.25" y="2.25" dx="0.25" dy="1.1" layer="1" roundness="60" stop="no"/>
<smd name="17" x="-0.75" y="2.45" dx="0.25" dy="0.7" layer="1" roundness="60" stop="no"/>
<smd name="18" x="-1.25" y="2.45" dx="0.25" dy="0.7" layer="1" roundness="60" stop="no"/>
<smd name="19" x="-1" y="-1.25" dx="1.95" dy="0.6" layer="1" roundness="10" rot="R90"/>
<smd name="20" x="1" y="-1.25" dx="1.95" dy="0.6" layer="1" roundness="10" rot="R90"/>
<text x="-1.35" y="-3.25" size="0.25" layer="27">&gt;VALUE</text>
<text x="-1.35" y="-2.9" size="0.25" layer="25">&gt;NAME</text>
</package>
<package name="TQFP32-05">
<description>0.5mm Spaced TQFP-32 Package</description>
<circle x="-2.6" y="-2.6" radius="0.2" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="2.3" x2="-2.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2.1" y1="2.3" x2="2.3" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2.3" y1="2.3" x2="2.3" y2="2.1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-2.1" x2="2.3" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-2.3" x2="2.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="-2.2" x2="-2.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="2.1" x2="-2.3" y2="2.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.75" y="-3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="2" x="-1.25" y="-3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="3" x="-0.75" y="-3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="4" x="-0.25" y="-3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="5" x="0.25" y="-3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="6" x="0.75" y="-3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="7" x="1.25" y="-3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="8" x="1.75" y="-3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="9" x="3.16" y="-1.75" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="10" x="3.16" y="-1.25" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="11" x="3.16" y="-0.75" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="12" x="3.16" y="-0.25" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="13" x="3.16" y="0.25" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="14" x="3.16" y="0.75" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="15" x="3.16" y="1.25" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="16" x="3.16" y="1.75" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="17" x="1.75" y="3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="18" x="1.25" y="3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="19" x="0.75" y="3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="20" x="0.25" y="3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="21" x="-0.25" y="3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="22" x="-0.75" y="3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="23" x="-1.25" y="3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="24" x="-1.75" y="3.16" dx="0.27" dy="1.45" layer="1"/>
<smd name="25" x="-3.16" y="1.75" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="26" x="-3.16" y="1.25" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="27" x="-3.16" y="0.75" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="28" x="-3.16" y="0.25" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="29" x="-3.16" y="-0.25" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="30" x="-3.16" y="-0.75" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="31" x="-3.16" y="-1.25" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<smd name="32" x="-3.16" y="-1.75" dx="0.27" dy="1.45" layer="1" rot="R90"/>
<text x="-4.064" y="-2.794" size="1.016" layer="25" font="vector" ratio="15" rot="R90">&gt;NAME</text>
<text x="4.064" y="2.54" size="1.016" layer="27" font="vector" ratio="15" rot="R270">&gt;VALUE</text>
</package>
<package name="SC70-5">
<description>&lt;b&gt;SMT SC70-5&lt;/b&gt;&lt;p&gt;
SOT353 - Philips Semiconductors&lt;br&gt;
Source: http://www.semiconductors.philips.com/acrobat_download/datasheets/74HC_HCT1G66_3.pdf</description>
<wire x1="1" y1="0.55" x2="-1" y2="0.55" width="0.127" layer="51"/>
<wire x1="-1" y1="0.55" x2="-1" y2="-0.55" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.55" x2="1" y2="-0.55" width="0.127" layer="51"/>
<wire x1="1" y1="-0.55" x2="1" y2="0.55" width="0.127" layer="21"/>
<rectangle x1="-0.125" y1="-1.05" x2="0.125" y2="-0.6" layer="51"/>
<rectangle x1="-0.775" y1="-1.05" x2="-0.525" y2="-0.6" layer="51"/>
<rectangle x1="0.525" y1="-1.05" x2="0.775" y2="-0.6" layer="51"/>
<rectangle x1="-0.775" y1="0.6" x2="-0.525" y2="1.05" layer="51"/>
<rectangle x1="0.525" y1="0.6" x2="0.775" y2="1.05" layer="51"/>
<smd name="1" x="-0.65" y="-0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="2" x="0" y="-0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="3" x="0.65" y="-0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="4" x="0.65" y="0.85" dx="0.4" dy="0.7" layer="1"/>
<smd name="5" x="-0.65" y="0.85" dx="0.4" dy="0.7" layer="1"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MLF32">
<description>mlf-32</description>
<circle x="-1.6" y="1.6" radius="0.3162" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="-2.1825" y2="2.5" width="0.127" layer="21"/>
<wire x1="-2.1825" y1="2.5" x2="-2.5" y2="2.1825" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.1825" x2="-2.5" y2="-2.5" width="0.127" layer="21"/>
<rectangle x1="-0.635" y1="-0.635" x2="0.635" y2="0.635" layer="31"/>
<smd name="1" x="-2.45" y="1.75" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="2" x="-2.45" y="1.25" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="3" x="-2.45" y="0.75" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="4" x="-2.45" y="0.25" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="5" x="-2.45" y="-0.25" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="6" x="-2.45" y="-0.75" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="7" x="-2.45" y="-1.25" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="8" x="-2.45" y="-1.75" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="9" x="-1.75" y="-2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="10" x="-1.25" y="-2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="11" x="-0.75" y="-2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="12" x="-0.25" y="-2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="13" x="0.25" y="-2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="14" x="0.75" y="-2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="15" x="1.25" y="-2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="16" x="1.75" y="-2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="17" x="2.45" y="-1.75" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="18" x="2.45" y="-1.25" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="19" x="2.45" y="-0.75" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="20" x="2.45" y="-0.25" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="21" x="2.45" y="0.25" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="22" x="2.45" y="0.75" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="23" x="2.45" y="1.25" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="24" x="2.45" y="1.75" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R90"/>
<smd name="25" x="1.75" y="2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="26" x="1.25" y="2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="27" x="0.75" y="2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="28" x="0.25" y="2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="29" x="-0.25" y="2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="30" x="-0.75" y="2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="31" x="-1.25" y="2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="32" x="-1.75" y="2.45" dx="0.28" dy="0.7" layer="1" roundness="80" rot="R180"/>
<smd name="33" x="0" y="0" dx="3.254" dy="3.254" layer="1" roundness="15" rot="R180" stop="no" cream="no"/>
<text x="-3.1" y="2" size="0.8128" layer="21">1</text>
<text x="-2.7" y="3" size="1.016" layer="25">&gt;NAME</text>
<polygon width="0.127" layer="29">
<vertex x="0" y="1.27" curve="-90"/>
<vertex x="1.27" y="0" curve="-90"/>
<vertex x="0" y="-1.27" curve="-90"/>
<vertex x="-1.27" y="0" curve="-90"/>
</polygon>
<text x="-2.6" y="-3.95" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="SOT23-DBV">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt; DBV (R-PDSO-G5)&lt;p&gt;
Source: http://focus.ti.com/lit/ds/symlink/tps77001.pdf</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="1.328" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="-1.328" y2="0.81" width="0.1524" layer="21"/>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MSOP08">
<description>&lt;b&gt;Mini Small Outline Package&lt;/b&gt;</description>
<circle x="-0.9456" y="-0.7906" radius="0.5" width="0.0508" layer="21"/>
<wire x1="1.624" y1="1.299" x2="1.624" y2="-1.301" width="0.1524" layer="21"/>
<wire x1="-1.626" y1="-1.301" x2="-1.626" y2="1.299" width="0.1524" layer="21"/>
<wire x1="1.299" y1="1.624" x2="1.624" y2="1.299" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.626" y1="1.299" x2="-1.301" y2="1.624" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.626" y1="-1.301" x2="-1.301" y2="-1.626" width="0.1524" layer="21" curve="90"/>
<wire x1="1.299" y1="-1.626" x2="1.624" y2="-1.301" width="0.1524" layer="21" curve="90"/>
<wire x1="-1.341" y1="-1.626" x2="-1.204" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="-0.747" y1="-1.626" x2="-0.554" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="-0.097" y1="-1.626" x2="0.096" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="0.553" y1="-1.626" x2="0.746" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="1.203" y1="-1.626" x2="1.299" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="-1.301" y1="1.624" x2="-1.204" y2="1.624" width="0.1524" layer="51"/>
<wire x1="-0.747" y1="1.624" x2="-0.554" y2="1.624" width="0.1524" layer="51"/>
<wire x1="-0.097" y1="1.624" x2="0.096" y2="1.624" width="0.1524" layer="51"/>
<wire x1="0.553" y1="1.624" x2="0.746" y2="1.624" width="0.1524" layer="51"/>
<wire x1="1.203" y1="1.624" x2="1.299" y2="1.624" width="0.1524" layer="51"/>
<rectangle x1="-1.0975" y1="1.6244" x2="-0.8537" y2="2.3557" layer="51"/>
<rectangle x1="-0.4475" y1="1.6244" x2="-0.2037" y2="2.3557" layer="51"/>
<rectangle x1="0.2025" y1="1.6244" x2="0.4463" y2="2.3557" layer="51"/>
<rectangle x1="0.8525" y1="1.6244" x2="1.0963" y2="2.3557" layer="51"/>
<rectangle x1="-1.0975" y1="-2.3569" x2="-0.8537" y2="-1.6256" layer="51"/>
<rectangle x1="-0.4475" y1="-2.3569" x2="-0.2037" y2="-1.6256" layer="51"/>
<rectangle x1="0.2025" y1="-2.3569" x2="0.4463" y2="-1.6256" layer="51"/>
<rectangle x1="0.8525" y1="-2.3569" x2="1.0963" y2="-1.6256" layer="51"/>
<smd name="1" x="-0.976" y="-2.113" dx="0.3" dy="1.2" layer="1"/>
<smd name="2" x="-0.326" y="-2.113" dx="0.3" dy="1.2" layer="1"/>
<smd name="3" x="0.324" y="-2.113" dx="0.3" dy="1.2" layer="1"/>
<smd name="4" x="0.974" y="-2.113" dx="0.3" dy="1.2" layer="1"/>
<smd name="5" x="0.974" y="2.112" dx="0.3" dy="1.2" layer="1"/>
<smd name="6" x="0.324" y="2.112" dx="0.3" dy="1.2" layer="1"/>
<smd name="7" x="-0.326" y="2.112" dx="0.3" dy="1.2" layer="1"/>
<smd name="8" x="-0.976" y="2.112" dx="0.3" dy="1.2" layer="1"/>
<text x="-2.032" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.302" y="-2.54" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
<package name="TQFP100">
<description>&lt;b&gt;100-lead Thin Quad Flat Pack Package Outline&lt;/b&gt;</description>
<circle x="-6" y="6" radius="0.2499" width="0.254" layer="21"/>
<circle x="3.59" y="-0.7699" radius="0.4999" width="0.1016" layer="51"/>
<wire x1="-7" y1="6.25" x2="-6.25" y2="7" width="0.254" layer="21"/>
<wire x1="-6.25" y1="7" x2="6.75" y2="7" width="0.254" layer="21"/>
<wire x1="6.75" y1="7" x2="7" y2="6.75" width="0.254" layer="21"/>
<wire x1="7" y1="6.75" x2="7" y2="-6.75" width="0.254" layer="21"/>
<wire x1="7" y1="-6.75" x2="6.75" y2="-7" width="0.254" layer="21"/>
<wire x1="6.75" y1="-7" x2="-6.75" y2="-7" width="0.254" layer="21"/>
<wire x1="-6.75" y1="-7" x2="-7" y2="-6.75" width="0.254" layer="21"/>
<wire x1="-7" y1="-6.75" x2="-7" y2="6.25" width="0.254" layer="21"/>
<wire x1="0.39" y1="0.9299" x2="-0.0099" y2="1.3298" width="0.1016" layer="51" curve="90"/>
<rectangle x1="-8.1999" y1="5.8499" x2="-7.15" y2="6.1502" layer="51"/>
<rectangle x1="-8.1999" y1="5.35" x2="-7.15" y2="5.6501" layer="51"/>
<rectangle x1="-8.1999" y1="4.8499" x2="-7.15" y2="5.1502" layer="51"/>
<rectangle x1="-8.1999" y1="4.35" x2="-7.15" y2="4.6501" layer="51"/>
<rectangle x1="-8.1999" y1="3.8499" x2="-7.15" y2="4.1502" layer="51"/>
<rectangle x1="-8.1999" y1="3.35" x2="-7.15" y2="3.6501" layer="51"/>
<rectangle x1="-8.1999" y1="2.8499" x2="-7.15" y2="3.1502" layer="51"/>
<rectangle x1="-8.1999" y1="2.35" x2="-7.15" y2="2.6501" layer="51"/>
<rectangle x1="-8.1999" y1="1.8499" x2="-7.15" y2="2.1502" layer="51"/>
<rectangle x1="-8.1999" y1="1.35" x2="-7.15" y2="1.6501" layer="51"/>
<rectangle x1="-8.1999" y1="0.8499" x2="-7.15" y2="1.1502" layer="51"/>
<rectangle x1="-8.1999" y1="0.35" x2="-7.15" y2="0.6501" layer="51"/>
<rectangle x1="-8.1999" y1="-0.1501" x2="-7.15" y2="0.1502" layer="51"/>
<rectangle x1="-8.1999" y1="-0.65" x2="-7.15" y2="-0.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-1.1501" x2="-7.15" y2="-0.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-1.65" x2="-7.15" y2="-1.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-2.1501" x2="-7.15" y2="-1.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-2.65" x2="-7.15" y2="-2.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-3.1501" x2="-7.15" y2="-2.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-3.65" x2="-7.15" y2="-3.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-4.1501" x2="-7.15" y2="-3.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-4.65" x2="-7.15" y2="-4.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-5.1501" x2="-7.15" y2="-4.8498" layer="51"/>
<rectangle x1="-8.1999" y1="-5.65" x2="-7.15" y2="-5.3499" layer="51"/>
<rectangle x1="-8.1999" y1="-6.1501" x2="-7.15" y2="-5.8498" layer="51"/>
<rectangle x1="-6.1501" y1="-8.1999" x2="-5.8498" y2="-7.15" layer="51"/>
<rectangle x1="-5.65" y1="-8.1999" x2="-5.3499" y2="-7.15" layer="51"/>
<rectangle x1="-5.1501" y1="-8.1999" x2="-4.8498" y2="-7.15" layer="51"/>
<rectangle x1="-4.65" y1="-8.1999" x2="-4.3499" y2="-7.15" layer="51"/>
<rectangle x1="-4.1501" y1="-8.1999" x2="-3.8498" y2="-7.15" layer="51"/>
<rectangle x1="-3.65" y1="-8.1999" x2="-3.3499" y2="-7.15" layer="51"/>
<rectangle x1="-3.1501" y1="-8.1999" x2="-2.8498" y2="-7.15" layer="51"/>
<rectangle x1="-2.65" y1="-8.1999" x2="-2.3499" y2="-7.15" layer="51"/>
<rectangle x1="-2.1501" y1="-8.1999" x2="-1.8498" y2="-7.15" layer="51"/>
<rectangle x1="-1.65" y1="-8.1999" x2="-1.3499" y2="-7.15" layer="51"/>
<rectangle x1="-1.1501" y1="-8.1999" x2="-0.8498" y2="-7.15" layer="51"/>
<rectangle x1="-0.65" y1="-8.1999" x2="-0.3499" y2="-7.15" layer="51"/>
<rectangle x1="-0.1501" y1="-8.1999" x2="0.1502" y2="-7.15" layer="51"/>
<rectangle x1="0.35" y1="-8.1999" x2="0.6501" y2="-7.15" layer="51"/>
<rectangle x1="0.8499" y1="-8.1999" x2="1.1502" y2="-7.15" layer="51"/>
<rectangle x1="1.35" y1="-8.1999" x2="1.6501" y2="-7.15" layer="51"/>
<rectangle x1="1.8499" y1="-8.1999" x2="2.1502" y2="-7.15" layer="51"/>
<rectangle x1="2.35" y1="-8.1999" x2="2.6501" y2="-7.15" layer="51"/>
<rectangle x1="2.8499" y1="-8.1999" x2="3.1502" y2="-7.15" layer="51"/>
<rectangle x1="3.35" y1="-8.1999" x2="3.6501" y2="-7.15" layer="51"/>
<rectangle x1="3.8499" y1="-8.1999" x2="4.1502" y2="-7.15" layer="51"/>
<rectangle x1="4.35" y1="-8.1999" x2="4.6501" y2="-7.15" layer="51"/>
<rectangle x1="4.8499" y1="-8.1999" x2="5.1502" y2="-7.15" layer="51"/>
<rectangle x1="5.35" y1="-8.1999" x2="5.6501" y2="-7.15" layer="51"/>
<rectangle x1="5.8499" y1="-8.1999" x2="6.1502" y2="-7.15" layer="51"/>
<rectangle x1="7.1501" y1="-6.1501" x2="8.2" y2="-5.8498" layer="51"/>
<rectangle x1="7.1501" y1="-5.65" x2="8.2" y2="-5.3499" layer="51"/>
<rectangle x1="7.1501" y1="-5.1501" x2="8.2" y2="-4.8498" layer="51"/>
<rectangle x1="7.1501" y1="-4.65" x2="8.2" y2="-4.3499" layer="51"/>
<rectangle x1="7.1501" y1="-4.1501" x2="8.2" y2="-3.8498" layer="51"/>
<rectangle x1="7.1501" y1="-3.65" x2="8.2" y2="-3.3499" layer="51"/>
<rectangle x1="7.1501" y1="-3.1501" x2="8.2" y2="-2.8498" layer="51"/>
<rectangle x1="7.1501" y1="-2.65" x2="8.2" y2="-2.3499" layer="51"/>
<rectangle x1="7.1501" y1="-2.1501" x2="8.2" y2="-1.8498" layer="51"/>
<rectangle x1="7.1501" y1="-1.65" x2="8.2" y2="-1.3499" layer="51"/>
<rectangle x1="7.1501" y1="-1.1501" x2="8.2" y2="-0.8498" layer="51"/>
<rectangle x1="7.1501" y1="-0.65" x2="8.2" y2="-0.3499" layer="51"/>
<rectangle x1="7.1501" y1="-0.1501" x2="8.2" y2="0.1502" layer="51"/>
<rectangle x1="7.1501" y1="0.35" x2="8.2" y2="0.6501" layer="51"/>
<rectangle x1="7.1501" y1="0.8499" x2="8.2" y2="1.1502" layer="51"/>
<rectangle x1="7.1501" y1="1.35" x2="8.2" y2="1.6501" layer="51"/>
<rectangle x1="7.1501" y1="1.8499" x2="8.2" y2="2.1502" layer="51"/>
<rectangle x1="7.1501" y1="2.35" x2="8.2" y2="2.6501" layer="51"/>
<rectangle x1="7.1501" y1="2.8499" x2="8.2" y2="3.1502" layer="51"/>
<rectangle x1="7.1501" y1="3.35" x2="8.2" y2="3.6501" layer="51"/>
<rectangle x1="7.1501" y1="3.8499" x2="8.2" y2="4.1502" layer="51"/>
<rectangle x1="7.1501" y1="4.35" x2="8.2" y2="4.6501" layer="51"/>
<rectangle x1="7.1501" y1="4.8499" x2="8.2" y2="5.1502" layer="51"/>
<rectangle x1="7.1501" y1="5.35" x2="8.2" y2="5.6501" layer="51"/>
<rectangle x1="7.1501" y1="5.8499" x2="8.2" y2="6.1502" layer="51"/>
<rectangle x1="5.8499" y1="7.1501" x2="6.1502" y2="8.2" layer="51"/>
<rectangle x1="5.35" y1="7.1501" x2="5.6501" y2="8.2" layer="51"/>
<rectangle x1="4.8499" y1="7.1501" x2="5.1502" y2="8.2" layer="51"/>
<rectangle x1="4.35" y1="7.1501" x2="4.6501" y2="8.2" layer="51"/>
<rectangle x1="3.8499" y1="7.1501" x2="4.1502" y2="8.2" layer="51"/>
<rectangle x1="3.35" y1="7.1501" x2="3.6501" y2="8.2" layer="51"/>
<rectangle x1="2.8499" y1="7.1501" x2="3.1502" y2="8.2" layer="51"/>
<rectangle x1="2.35" y1="7.1501" x2="2.6501" y2="8.2" layer="51"/>
<rectangle x1="1.8499" y1="7.1501" x2="2.1502" y2="8.2" layer="51"/>
<rectangle x1="1.35" y1="7.1501" x2="1.6501" y2="8.2" layer="51"/>
<rectangle x1="0.8499" y1="7.1501" x2="1.1502" y2="8.2" layer="51"/>
<rectangle x1="0.35" y1="7.1501" x2="0.6501" y2="8.2" layer="51"/>
<rectangle x1="-0.1501" y1="7.1501" x2="0.1502" y2="8.2" layer="51"/>
<rectangle x1="-0.65" y1="7.1501" x2="-0.3499" y2="8.2" layer="51"/>
<rectangle x1="-1.1501" y1="7.1501" x2="-0.8498" y2="8.2" layer="51"/>
<rectangle x1="-1.65" y1="7.1501" x2="-1.3499" y2="8.2" layer="51"/>
<rectangle x1="-2.1501" y1="7.1501" x2="-1.8498" y2="8.2" layer="51"/>
<rectangle x1="-2.65" y1="7.1501" x2="-2.3499" y2="8.2" layer="51"/>
<rectangle x1="-3.1501" y1="7.1501" x2="-2.8498" y2="8.2" layer="51"/>
<rectangle x1="-3.65" y1="7.1501" x2="-3.3499" y2="8.2" layer="51"/>
<rectangle x1="-4.1501" y1="7.1501" x2="-3.8498" y2="8.2" layer="51"/>
<rectangle x1="-4.65" y1="7.1501" x2="-4.3499" y2="8.2" layer="51"/>
<rectangle x1="-5.1501" y1="7.1501" x2="-4.8498" y2="8.2" layer="51"/>
<rectangle x1="-5.65" y1="7.1501" x2="-5.3499" y2="8.2" layer="51"/>
<rectangle x1="-6.1501" y1="7.1501" x2="-5.8498" y2="8.2" layer="51"/>
<smd name="1" x="-8" y="6" dx="1.5" dy="0.3" layer="1"/>
<smd name="2" x="-8" y="5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="3" x="-8" y="5" dx="1.5" dy="0.3" layer="1"/>
<smd name="4" x="-8" y="4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="5" x="-8" y="4" dx="1.5" dy="0.3" layer="1"/>
<smd name="6" x="-8" y="3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="7" x="-8" y="3" dx="1.5" dy="0.3" layer="1"/>
<smd name="8" x="-8" y="2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="9" x="-8" y="2" dx="1.5" dy="0.3" layer="1"/>
<smd name="10" x="-8" y="1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="11" x="-8" y="1" dx="1.5" dy="0.3" layer="1"/>
<smd name="12" x="-8" y="0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="13" x="-8" y="0" dx="1.5" dy="0.3" layer="1"/>
<smd name="14" x="-8" y="-0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="15" x="-8" y="-1" dx="1.5" dy="0.3" layer="1"/>
<smd name="16" x="-8" y="-1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="17" x="-8" y="-2" dx="1.5" dy="0.3" layer="1"/>
<smd name="18" x="-8" y="-2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="19" x="-8" y="-3" dx="1.5" dy="0.3" layer="1"/>
<smd name="20" x="-8" y="-3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="21" x="-8" y="-4" dx="1.5" dy="0.3" layer="1"/>
<smd name="22" x="-8" y="-4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="23" x="-8" y="-5" dx="1.5" dy="0.3" layer="1"/>
<smd name="24" x="-8" y="-5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="25" x="-8" y="-6" dx="1.5" dy="0.3" layer="1"/>
<smd name="26" x="-6" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="27" x="-5.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="28" x="-5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="29" x="-4.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="30" x="-4" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="31" x="-3.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="32" x="-3" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="33" x="-2.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="34" x="-2" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="35" x="-1.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="36" x="-1" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="37" x="-0.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="38" x="0" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="39" x="0.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="40" x="1" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="41" x="1.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="42" x="2" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="43" x="2.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="44" x="3" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="45" x="3.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="46" x="4" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="47" x="4.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="48" x="5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="49" x="5.5" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="50" x="6" y="-8" dx="0.3" dy="1.5" layer="1"/>
<smd name="51" x="8" y="-6" dx="1.5" dy="0.3" layer="1"/>
<smd name="52" x="8" y="-5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="53" x="8" y="-5" dx="1.5" dy="0.3" layer="1"/>
<smd name="54" x="8" y="-4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="55" x="8" y="-4" dx="1.5" dy="0.3" layer="1"/>
<smd name="56" x="8" y="-3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="57" x="8" y="-3" dx="1.5" dy="0.3" layer="1"/>
<smd name="58" x="8" y="-2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="59" x="8" y="-2" dx="1.5" dy="0.3" layer="1"/>
<smd name="60" x="8" y="-1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="61" x="8" y="-1" dx="1.5" dy="0.3" layer="1"/>
<smd name="62" x="8" y="-0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="63" x="8" y="0" dx="1.5" dy="0.3" layer="1"/>
<smd name="64" x="8" y="0.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="65" x="8" y="1" dx="1.5" dy="0.3" layer="1"/>
<smd name="66" x="8" y="1.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="67" x="8" y="2" dx="1.5" dy="0.3" layer="1"/>
<smd name="68" x="8" y="2.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="69" x="8" y="3" dx="1.5" dy="0.3" layer="1"/>
<smd name="70" x="8" y="3.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="71" x="8" y="4" dx="1.5" dy="0.3" layer="1"/>
<smd name="72" x="8" y="4.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="73" x="8" y="5" dx="1.5" dy="0.3" layer="1"/>
<smd name="74" x="8" y="5.5" dx="1.5" dy="0.3" layer="1"/>
<smd name="75" x="8" y="6" dx="1.5" dy="0.3" layer="1"/>
<smd name="76" x="6" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="77" x="5.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="78" x="5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="79" x="4.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="80" x="4" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="81" x="3.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="82" x="3" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="83" x="2.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="84" x="2" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="85" x="1.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="86" x="1" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="87" x="0.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="88" x="0" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="89" x="-0.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="90" x="-1" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="91" x="-1.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="92" x="-2" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="93" x="-2.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="94" x="-3" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="95" x="-3.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="96" x="-4" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="97" x="-4.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="98" x="-5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="99" x="-5.5" y="8" dx="0.3" dy="1.5" layer="1"/>
<smd name="100" x="-6" y="8" dx="0.3" dy="1.5" layer="1"/>
<text x="-6.395" y="9.2451" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.1801" y="2.8001" size="1.27" layer="27">&gt;VALUE</text>
<text x="-2.81" y="-2.4801" size="0.8128" layer="51">TQFP 100</text>
<text x="3.3899" y="-1.0701" size="0.6096" layer="51" ratio="15">R</text>
<polygon width="0.1" layer="51">
<vertex x="-3.81" y="-0.6701"/>
<vertex x="-2.81" y="1.3299"/>
<vertex x="-2.2101" y="1.3299"/>
<vertex x="-2.2101" y="-0.6701"/>
<vertex x="-2.6101" y="-0.6701"/>
<vertex x="-2.6101" y="0.73"/>
<vertex x="-3.2101" y="-0.4699"/>
<vertex x="-3.0099" y="-0.4699"/>
<vertex x="-3.0099" y="-0.6701"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="-2.7099" y="1.6299"/>
<vertex x="-2.51" y="2.03"/>
<vertex x="3.0899" y="2.03"/>
<vertex x="3.0899" y="1.6299"/>
<vertex x="-1.51" y="1.6299"/>
<vertex x="-1.51" y="-0.6701"/>
<vertex x="-1.9101" y="-0.6701"/>
<vertex x="-1.9101" y="1.6299"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="-1.2101" y="1.3299"/>
<vertex x="-1.2101" y="-0.6701"/>
<vertex x="-0.81" y="-0.6701"/>
<vertex x="-0.81" y="1.13"/>
<vertex x="-0.6101" y="1.13"/>
<vertex x="-0.6101" y="-0.6701"/>
<vertex x="-0.2101" y="-0.6701"/>
<vertex x="-0.2101" y="1.13"/>
<vertex x="-0.0099" y="1.13"/>
<vertex x="-0.0099" y="-0.6701"/>
<vertex x="0.3899" y="-0.6701"/>
<vertex x="0.3899" y="0.9299"/>
<vertex x="0.2901" y="1.13"/>
<vertex x="0.19" y="1.2301"/>
<vertex x="-0.0099" y="1.3299"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="0.6901" y="1.3299"/>
<vertex x="0.6901" y="-0.6701"/>
<vertex x="1.89" y="-0.6701"/>
<vertex x="1.89" y="-0.0701"/>
<vertex x="0.89" y="-0.0701"/>
<vertex x="0.89" y="0.13"/>
<vertex x="1.89" y="0.13"/>
<vertex x="1.89" y="0.5301"/>
<vertex x="0.89" y="0.5301"/>
<vertex x="0.89" y="0.73"/>
<vertex x="1.89" y="0.73"/>
<vertex x="1.89" y="1.3299"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="2.19" y="1.3299"/>
<vertex x="2.19" y="-0.6701"/>
<vertex x="2.7899" y="-0.6701"/>
<vertex x="2.9901" y="-0.27"/>
<vertex x="2.59" y="-0.27"/>
<vertex x="2.59" y="1.3299"/>
</polygon>
<polygon width="0.1" layer="51">
<vertex x="-3.81" y="-0.8699"/>
<vertex x="-3.81" y="-1.27"/>
<vertex x="2.49" y="-1.27"/>
<vertex x="2.6901" y="-0.8699"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="MPM3610">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<pin name="AAM" x="-15.24" y="-10.16" length="middle"/>
<pin name="AGND" x="2.54" y="-17.78" length="middle" rot="R90"/>
<pin name="BST" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="EN" x="-15.24" y="-2.54" length="middle"/>
<pin name="FB" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="GND" x="0" y="-17.78" length="middle" rot="R90"/>
<pin name="OUT" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="SW" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="VCC" x="-15.24" y="-7.62" length="middle"/>
<pin name="VIN" x="-15.24" y="5.08" length="middle"/>
<text x="-10.16" y="12.7" size="1.905" layer="96">&gt;VALUE</text>
<text x="-10.16" y="15.24" size="1.905" layer="95">&gt;NAME</text>
</symbol>
<symbol name="MAX3421">
<wire x1="-15.24" y1="25.4" x2="15.24" y2="25.4" width="0.254" layer="94"/>
<wire x1="15.24" y1="25.4" x2="15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="-27.94" x2="-15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-27.94" x2="-15.24" y2="25.4" width="0.254" layer="94"/>
<pin name="D+" x="-20.32" y="12.7" length="middle"/>
<pin name="D-" x="-20.32" y="15.24" length="middle"/>
<pin name="GND@3" x="-2.54" y="-33.02" length="middle" rot="R90"/>
<pin name="GND@19" x="2.54" y="-33.02" length="middle" rot="R90"/>
<pin name="GPIN0" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="GPIN1" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="GPIN2" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="GPIN3" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="GPIN4" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="GPIN5" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="GPIN6" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="GPIN7" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="GPOUT0" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="GPOUT1" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="GPOUT2" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="GPOUT3" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="GPOUT4" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="GPOUT5" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="GPOUT6" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="GPOUT7" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="GPX" x="-20.32" y="-15.24" length="middle"/>
<pin name="INT" x="-20.32" y="20.32" length="middle"/>
<pin name="MISO" x="-20.32" y="-10.16" length="middle"/>
<pin name="MOSI" x="-20.32" y="-12.7" length="middle"/>
<pin name="RES" x="-20.32" y="-2.54" length="middle"/>
<pin name="SCK" x="-20.32" y="-5.08" length="middle"/>
<pin name="SS" x="-20.32" y="-7.62" length="middle"/>
<pin name="VBCOMP" x="-20.32" y="-20.32" length="middle"/>
<pin name="VCC" x="-2.54" y="30.48" length="middle" rot="R270"/>
<pin name="VL" x="2.54" y="30.48" length="middle" rot="R270"/>
<pin name="XI" x="-20.32" y="2.54" length="middle"/>
<pin name="XO" x="-20.32" y="7.62" length="middle"/>
<text x="-15.24" y="27.94" size="1.778" layer="95" rot="MR180">&gt;PART</text>
<text x="15.24" y="-30.48" size="1.778" layer="96" rot="MR0">&gt;VALUE</text>
</symbol>
<symbol name="74125">
<circle x="0" y="3.81" radius="1.016" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="0" y1="4.826" x2="0" y2="5.588" width="0.1524" layer="94"/>
<pin name="I" x="-10.16" y="0" visible="pad" length="middle" direction="in"/>
<pin name="O" x="10.16" y="0" visible="pad" length="middle" direction="hiz" rot="R180"/>
<pin name="OE" x="0" y="10.16" visible="pad" length="middle" direction="in" rot="R270"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PWRN">
<pin name="VDD" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="VSS" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<text x="-1.27" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VDD</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">VSS</text>
</symbol>
<symbol name="ATMEL_MEGA8U2">
<wire x1="-22.86" y1="33.02" x2="22.86" y2="33.02" width="0.254" layer="94"/>
<wire x1="22.86" y1="33.02" x2="22.86" y2="-30.48" width="0.254" layer="94"/>
<wire x1="22.86" y1="-30.48" x2="-22.86" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-30.48" x2="-22.86" y2="33.02" width="0.254" layer="94"/>
<pin name="(AIN0/INT1)PD1" x="27.94" y="-25.4" length="middle" rot="R180"/>
<pin name="(AIN2/PCINT11)PC2" x="27.94" y="-5.08" length="middle" rot="R180"/>
<pin name="(CTS/HWB/AIN6/TO/INT7)PD7" x="27.94" y="-10.16" length="middle" rot="R180"/>
<pin name="(INT4/ICP1/CLK0)PC7" x="27.94" y="5.08" length="middle" rot="R180"/>
<pin name="(INT5/AIN3)PD4" x="27.94" y="-17.78" length="middle" rot="R180"/>
<pin name="(OC0B/INT0)PD0" x="27.94" y="-27.94" length="middle" rot="R180"/>
<pin name="(OC1A/PCINT8)PC6" x="27.94" y="2.54" length="middle" rot="R180"/>
<pin name="(PCINT5)PB5" x="27.94" y="25.4" length="middle" rot="R180"/>
<pin name="(PCINT6)PB6" x="27.94" y="27.94" length="middle" rot="R180"/>
<pin name="(PCINT7/OC0A/OC1C)PB7" x="27.94" y="30.48" length="middle" rot="R180"/>
<pin name="(PCINT9/OC1B)PC5" x="27.94" y="0" length="middle" rot="R180"/>
<pin name="(PCINT10)PC4" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="(PD0/MISO/PCINT3)PB3" x="27.94" y="20.32" length="middle" rot="R180"/>
<pin name="(PDI/MOSI/PCINT2)PB2" x="27.94" y="17.78" length="middle" rot="R180"/>
<pin name="(RTS/AIN5/INT6)PD6" x="27.94" y="-12.7" length="middle" rot="R180"/>
<pin name="(RXD1/AIN1/INT2)PD2" x="27.94" y="-22.86" length="middle" rot="R180"/>
<pin name="(SCLK/PCINT1)PB1" x="27.94" y="15.24" length="middle" rot="R180"/>
<pin name="(SS/PCINT0)PB0" x="27.94" y="12.7" length="middle" rot="R180"/>
<pin name="(T1/PCINT4)PB4" x="27.94" y="22.86" length="middle" rot="R180"/>
<pin name="(TXD1/INT3)PD3" x="27.94" y="-20.32" length="middle" rot="R180"/>
<pin name="(XCK/AIN4/PCINT12)PD5" x="27.94" y="-15.24" length="middle" rot="R180"/>
<pin name="AVCC" x="-27.94" y="5.08" length="middle" direction="pwr"/>
<pin name="D+" x="-27.94" y="-20.32" length="middle"/>
<pin name="D-" x="-27.94" y="-17.78" length="middle"/>
<pin name="GND" x="-27.94" y="-5.08" length="middle" direction="pwr"/>
<pin name="PAD" x="-27.94" y="-27.94" length="middle" direction="pwr"/>
<pin name="RESET(PC1/DW)" x="-27.94" y="25.4" length="middle" direction="in" function="dot"/>
<pin name="UCAP" x="-27.94" y="-12.7" length="middle" direction="pas"/>
<pin name="UGND" x="-27.94" y="-22.86" length="middle" direction="pwr"/>
<pin name="UVCC" x="-27.94" y="-15.24" length="middle" direction="pwr"/>
<pin name="VCC" x="-27.94" y="-2.54" length="middle" direction="pwr"/>
<pin name="XTAL1" x="-27.94" y="12.7" length="middle"/>
<pin name="XTAL2(PC0)" x="-27.94" y="17.78" length="middle"/>
<text x="-22.86" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
<text x="-22.86" y="33.782" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="LP2985-XXDBVR">
<wire x1="-7.62" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="GND" x="-10.16" y="-5.08" length="short" direction="pwr"/>
<pin name="IN" x="-10.16" y="5.08" length="short" direction="in"/>
<pin name="NC/FB" x="12.7" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="ON/!OFF" x="-10.16" y="0" length="short" direction="in"/>
<pin name="OUT" x="12.7" y="5.08" length="short" direction="pas" rot="R180"/>
<text x="-7.62" y="8.89" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="OPAMP">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<pin name="+IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="-IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PWR+-">
<pin name="V+" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="V-" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<text x="1.27" y="3.175" size="0.8128" layer="93" rot="R90">V+</text>
<text x="1.27" y="-4.445" size="0.8128" layer="93" rot="R90">V-</text>
</symbol>
<symbol name="86-I/O-1">
<wire x1="-30.48" y1="78.74" x2="30.48" y2="78.74" width="0.254" layer="94"/>
<wire x1="30.48" y1="78.74" x2="30.48" y2="-76.2" width="0.254" layer="94"/>
<wire x1="30.48" y1="-76.2" x2="-30.48" y2="-76.2" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-76.2" x2="-30.48" y2="78.74" width="0.254" layer="94"/>
<wire x1="10.16" y1="36.6713" x2="13.0175" y2="36.6713" width="0.127" layer="95"/>
<wire x1="19.685" y1="-72.5488" x2="22.7013" y2="-72.5488" width="0.127" layer="95"/>
<wire x1="19.8437" y1="-70.0088" x2="22.86" y2="-70.0088" width="0.127" layer="95"/>
<wire x1="-28.2575" y1="77.3113" x2="-21.2725" y2="77.3113" width="0.127" layer="95"/>
<pin name="(A8)PC0" x="35.56" y="12.7" length="middle" rot="R180"/>
<pin name="(A9)PC1" x="35.56" y="15.24" length="middle" rot="R180"/>
<pin name="(A10)PC2" x="35.56" y="17.78" length="middle" rot="R180"/>
<pin name="(A11)PC3" x="35.56" y="20.32" length="middle" rot="R180"/>
<pin name="(A12)PC4" x="35.56" y="22.86" length="middle" rot="R180"/>
<pin name="(A13)PC5" x="35.56" y="25.4" length="middle" rot="R180"/>
<pin name="(A14)PC6" x="35.56" y="27.94" length="middle" rot="R180"/>
<pin name="(A15)PC7" x="35.56" y="30.48" length="middle" rot="R180"/>
<pin name="(AD0)PA0" x="35.56" y="58.42" length="middle" rot="R180"/>
<pin name="(AD1)PA1" x="35.56" y="60.96" length="middle" rot="R180"/>
<pin name="(AD2)PA2" x="35.56" y="63.5" length="middle" rot="R180"/>
<pin name="(AD3)PA3" x="35.56" y="66.04" length="middle" rot="R180"/>
<pin name="(AD4)PA4" x="35.56" y="68.58" length="middle" rot="R180"/>
<pin name="(AD5)PA5" x="35.56" y="71.12" length="middle" rot="R180"/>
<pin name="(AD6)PA6" x="35.56" y="73.66" length="middle" rot="R180"/>
<pin name="(AD7)PA7" x="35.56" y="76.2" length="middle" rot="R180"/>
<pin name="(ADC0)PF0" x="35.56" y="-55.88" length="middle" rot="R180"/>
<pin name="(ADC1)PF1" x="35.56" y="-53.34" length="middle" rot="R180"/>
<pin name="(ADC2)PF2" x="35.56" y="-50.8" length="middle" rot="R180"/>
<pin name="(ADC3)PF3" x="35.56" y="-48.26" length="middle" rot="R180"/>
<pin name="(ADC4/TCK)PF4" x="35.56" y="-45.72" length="middle" rot="R180"/>
<pin name="(ADC5/TMS)PF5" x="35.56" y="-43.18" length="middle" rot="R180"/>
<pin name="(ADC6/TDO)PF6" x="35.56" y="-40.64" length="middle" rot="R180"/>
<pin name="(ADC7/TDI)PF7" x="35.56" y="-38.1" length="middle" rot="R180"/>
<pin name="(ALE)PG2" x="35.56" y="-68.58" length="middle" rot="R180"/>
<pin name="(CLKO/ICP3/INT7)PE7" x="35.56" y="-15.24" length="middle" rot="R180"/>
<pin name="(ICP1)PD4" x="35.56" y="0" length="middle" rot="R180"/>
<pin name="(MISO/PCINT3)PB3" x="35.56" y="43.18" length="middle" rot="R180"/>
<pin name="(MOSI/PCINT2)PB2" x="35.56" y="40.64" length="middle" rot="R180"/>
<pin name="(OC0A/OC1C/PCINT7)PB7" x="35.56" y="53.34" length="middle" rot="R180"/>
<pin name="(OC0B)PG5" x="35.56" y="-60.96" length="middle" rot="R180"/>
<pin name="(OC1A/PCINT5)PB5" x="35.56" y="48.26" length="middle" rot="R180"/>
<pin name="(OC1B/PCINT6)PB6" x="35.56" y="50.8" length="middle" rot="R180"/>
<pin name="(OC2A/PCINT4)PB4" x="35.56" y="45.72" length="middle" rot="R180"/>
<pin name="(OC3A/AIN1)PE3" x="35.56" y="-25.4" length="middle" rot="R180"/>
<pin name="(OC3B/INT4)PE4" x="35.56" y="-22.86" length="middle" rot="R180"/>
<pin name="(OC3C/INT5)PE5" x="35.56" y="-20.32" length="middle" rot="R180"/>
<pin name="(RD)PG1" x="35.56" y="-71.12" length="middle" rot="R180"/>
<pin name="(RXD0/PCIN8)PE0" x="35.56" y="-33.02" length="middle" rot="R180"/>
<pin name="(RXD1/INT2)PD2" x="35.56" y="-5.08" length="middle" rot="R180"/>
<pin name="(SCK/PCINT1)PB1" x="35.56" y="38.1" length="middle" rot="R180"/>
<pin name="(SCL/INT0)PD0" x="35.56" y="-10.16" length="middle" rot="R180"/>
<pin name="(SDA/INT1)PD1" x="35.56" y="-7.62" length="middle" rot="R180"/>
<pin name="(SS/PCINT0)PB0" x="35.56" y="35.56" length="middle" rot="R180"/>
<pin name="(T0)PD7" x="35.56" y="7.62" length="middle" rot="R180"/>
<pin name="(T1)PD6" x="35.56" y="5.08" length="middle" rot="R180"/>
<pin name="(T3/INT6)PE6" x="35.56" y="-17.78" length="middle" rot="R180"/>
<pin name="(TOSC1)PG4" x="35.56" y="-63.5" length="middle" rot="R180"/>
<pin name="(TOSC2)PG3" x="35.56" y="-66.04" length="middle" rot="R180"/>
<pin name="(TXD0)PE1" x="35.56" y="-30.48" length="middle" rot="R180"/>
<pin name="(TXD1/INT3)PD3" x="35.56" y="-2.54" length="middle" rot="R180"/>
<pin name="(WR)PG0" x="35.56" y="-73.66" length="middle" rot="R180"/>
<pin name="(XCK0/AIN0)PE2" x="35.56" y="-27.94" length="middle" rot="R180"/>
<pin name="(XCK1)PD5" x="35.56" y="2.54" length="middle" rot="R180"/>
<pin name="AGND" x="-35.56" y="55.88" length="middle" direction="pwr"/>
<pin name="AREF" x="-35.56" y="60.96" length="middle"/>
<pin name="AVCC" x="-35.56" y="58.42" length="middle" direction="pwr"/>
<pin name="GND" x="-35.56" y="38.1" length="middle" direction="pwr"/>
<pin name="GND1" x="-35.56" y="35.56" visible="pad" length="middle" direction="pwr"/>
<pin name="GND2" x="-35.56" y="33.02" visible="pad" length="middle" direction="pwr"/>
<pin name="GND3" x="-35.56" y="30.48" visible="pad" length="middle" direction="pwr"/>
<pin name="PH0(RXD2)" x="-35.56" y="-73.66" length="middle"/>
<pin name="PH1(TXD2)" x="-35.56" y="-71.12" length="middle"/>
<pin name="PH2(XCK2)" x="-35.56" y="-68.58" length="middle"/>
<pin name="PH3(OC4A)" x="-35.56" y="-66.04" length="middle"/>
<pin name="PH4(OC4B)" x="-35.56" y="-63.5" length="middle"/>
<pin name="PH5(OC4C)" x="-35.56" y="-60.96" length="middle"/>
<pin name="PH6(OC2B)" x="-35.56" y="-58.42" length="middle"/>
<pin name="PH7(T4)" x="-35.56" y="-55.88" length="middle"/>
<pin name="PJ0(RXD3/PCINT9)" x="-35.56" y="-50.8" length="middle"/>
<pin name="PJ1(TXD3/PCINT10)" x="-35.56" y="-48.26" length="middle"/>
<pin name="PJ2(XCK3/PCINT11)" x="-35.56" y="-45.72" length="middle"/>
<pin name="PJ3(PCINT12)" x="-35.56" y="-43.18" length="middle"/>
<pin name="PJ4(PCINT13)" x="-35.56" y="-40.64" length="middle"/>
<pin name="PJ5(PCINT14)" x="-35.56" y="-38.1" length="middle"/>
<pin name="PJ6(PCINT15)" x="-35.56" y="-35.56" length="middle"/>
<pin name="PJ7" x="-35.56" y="-33.02" length="middle"/>
<pin name="PK0(ADC8/PCINT16)" x="-35.56" y="-27.94" length="middle"/>
<pin name="PK1(ADC9/PCINT17)" x="-35.56" y="-25.4" length="middle"/>
<pin name="PK2(ADC10/PCINT18)" x="-35.56" y="-22.86" length="middle"/>
<pin name="PK3(ADC11/PCINT19)" x="-35.56" y="-20.32" length="middle"/>
<pin name="PK4(ADC12/PCINT20)" x="-35.56" y="-17.78" length="middle"/>
<pin name="PK5(ADC13/PCINT21)" x="-35.56" y="-15.24" length="middle"/>
<pin name="PK6(ADC14/PCINT22)" x="-35.56" y="-12.7" length="middle"/>
<pin name="PK7(ADC15/PCINT23)" x="-35.56" y="-10.16" length="middle"/>
<pin name="PL0(ICP4)" x="-35.56" y="-5.08" length="middle"/>
<pin name="PL1(ICP5)" x="-35.56" y="-2.54" length="middle"/>
<pin name="PL2(T5)" x="-35.56" y="0" length="middle"/>
<pin name="PL3(OC5A)" x="-35.56" y="2.54" length="middle"/>
<pin name="PL4(OC5B)" x="-35.56" y="5.08" length="middle"/>
<pin name="PL5(OC5C)" x="-35.56" y="7.62" length="middle"/>
<pin name="PL6" x="-35.56" y="10.16" length="middle"/>
<pin name="PL7" x="-35.56" y="12.7" length="middle"/>
<pin name="RESET" x="-35.56" y="76.2" length="middle" direction="in" function="dot"/>
<pin name="VCC" x="-35.56" y="48.26" length="middle" direction="pwr"/>
<pin name="VCC1" x="-35.56" y="45.72" visible="pad" length="middle" direction="pwr"/>
<pin name="VCC2" x="-35.56" y="43.18" visible="pad" length="middle" direction="pwr"/>
<pin name="VCC3" x="-35.56" y="40.64" visible="pad" length="middle" direction="pwr"/>
<pin name="XTAL1" x="-35.56" y="66.04" length="middle" direction="in"/>
<pin name="XTAL2" x="-35.56" y="71.12" length="middle" direction="out"/>
<text x="-30.48" y="-78.74" size="1.778" layer="96">&gt;VALUE</text>
<text x="-30.48" y="80.01" size="1.778" layer="95">&gt;NAME</text>
<text x="-28.0987" y="35.0838" size="1.524" layer="95">GND</text>
<text x="-28.0987" y="32.5438" size="1.524" layer="95">GND</text>
<text x="-28.0987" y="30.0038" size="1.524" layer="95">GND</text>
<text x="-28.0987" y="42.3863" size="1.524" layer="95">VCC</text>
<text x="-28.0987" y="44.9263" size="1.524" layer="95">VCC</text>
<text x="-28.0987" y="39.8463" size="1.524" layer="95">VCC</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="2093_MPM3610GQV-P" prefix="IC" uservalue="yes">
<description>MPM3610 Step Down Regulator</description>
<gates>
<gate name="G$1" symbol="MPM3610" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN-20">
<connects>
<connect gate="G$1" pin="AAM" pad="18"/>
<connect gate="G$1" pin="AGND" pad="3"/>
<connect gate="G$1" pin="BST" pad="11"/>
<connect gate="G$1" pin="EN" pad="17"/>
<connect gate="G$1" pin="FB" pad="1"/>
<connect gate="G$1" pin="GND" pad="12 13 14"/>
<connect gate="G$1" pin="OUT" pad="7 8 9 20"/>
<connect gate="G$1" pin="SW" pad="4 5 6 19"/>
<connect gate="G$1" pin="VCC" pad="2"/>
<connect gate="G$1" pin="VIN" pad="16"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="QFN-20" constant="no"/>
<attribute name="PART_NUMBER" value="MPM3610GQV-P" constant="no"/>
<attribute name="TYPE" value="Circuito_Integrato" constant="no"/>
<attribute name="VALUE" value="MPM3610" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0052_MAX3421EEHJ" prefix="IC" uservalue="yes">
<description>USB Peripheral/Host Controller with SPI Interface-MAX3421EEHJ</description>
<gates>
<gate name="G$1" symbol="MAX3421" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP32-05">
<connects>
<connect gate="G$1" pin="D+" pad="21"/>
<connect gate="G$1" pin="D-" pad="20"/>
<connect gate="G$1" pin="GND@19" pad="19"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GPIN0" pad="26"/>
<connect gate="G$1" pin="GPIN1" pad="27"/>
<connect gate="G$1" pin="GPIN2" pad="28"/>
<connect gate="G$1" pin="GPIN3" pad="29"/>
<connect gate="G$1" pin="GPIN4" pad="30"/>
<connect gate="G$1" pin="GPIN5" pad="31"/>
<connect gate="G$1" pin="GPIN6" pad="32"/>
<connect gate="G$1" pin="GPIN7" pad="1"/>
<connect gate="G$1" pin="GPOUT0" pad="4"/>
<connect gate="G$1" pin="GPOUT1" pad="5"/>
<connect gate="G$1" pin="GPOUT2" pad="6"/>
<connect gate="G$1" pin="GPOUT3" pad="7"/>
<connect gate="G$1" pin="GPOUT4" pad="8"/>
<connect gate="G$1" pin="GPOUT5" pad="9"/>
<connect gate="G$1" pin="GPOUT6" pad="10"/>
<connect gate="G$1" pin="GPOUT7" pad="11"/>
<connect gate="G$1" pin="GPX" pad="17"/>
<connect gate="G$1" pin="INT" pad="18"/>
<connect gate="G$1" pin="MISO" pad="15"/>
<connect gate="G$1" pin="MOSI" pad="16"/>
<connect gate="G$1" pin="RES" pad="12"/>
<connect gate="G$1" pin="SCK" pad="13"/>
<connect gate="G$1" pin="SS" pad="14"/>
<connect gate="G$1" pin="VBCOMP" pad="22"/>
<connect gate="G$1" pin="VCC" pad="23"/>
<connect gate="G$1" pin="VL" pad="2"/>
<connect gate="G$1" pin="XI" pad="24"/>
<connect gate="G$1" pin="XO" pad="25"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="TQFP-32L-5x5"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="MAX3421EEHJ"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Circuito_Integrato"/>
<attribute name="VALUE" value="MAX3421EEHJ"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0040_SN74LVC1G125DCKR" prefix="IC" uservalue="yes">
<description>Single bus buffer with 3-state output</description>
<gates>
<gate name="A" symbol="74125" x="17.78" y="0"/>
<gate name="P" symbol="PWRN" x="-5.08" y="-7.62" addlevel="request"/>
</gates>
<devices>
<device name="" package="SC70-5">
<connects>
<connect gate="A" pin="I" pad="2"/>
<connect gate="A" pin="O" pad="4"/>
<connect gate="A" pin="OE" pad="1"/>
<connect gate="P" pin="VDD" pad="5"/>
<connect gate="P" pin="VSS" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="SC70"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="SN74LVC1G125DCKR"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Circuito_Integrato"/>
<attribute name="VALUE" value="74LVC1G125DCKR"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0007_ATMEGA16U2-MUR" prefix="IC" uservalue="yes">
<description>8-bit Microcontroller with 16K Bytes of ISP Flash and USB Controller   ATMEGA16U2-MUR</description>
<gates>
<gate name="G$1" symbol="ATMEL_MEGA8U2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MLF32">
<connects>
<connect gate="G$1" pin="(AIN0/INT1)PD1" pad="7"/>
<connect gate="G$1" pin="(AIN2/PCINT11)PC2" pad="5"/>
<connect gate="G$1" pin="(CTS/HWB/AIN6/TO/INT7)PD7" pad="13"/>
<connect gate="G$1" pin="(INT4/ICP1/CLK0)PC7" pad="22"/>
<connect gate="G$1" pin="(INT5/AIN3)PD4" pad="10"/>
<connect gate="G$1" pin="(OC0B/INT0)PD0" pad="6"/>
<connect gate="G$1" pin="(OC1A/PCINT8)PC6" pad="23"/>
<connect gate="G$1" pin="(PCINT10)PC4" pad="26"/>
<connect gate="G$1" pin="(PCINT5)PB5" pad="19"/>
<connect gate="G$1" pin="(PCINT6)PB6" pad="20"/>
<connect gate="G$1" pin="(PCINT7/OC0A/OC1C)PB7" pad="21"/>
<connect gate="G$1" pin="(PCINT9/OC1B)PC5" pad="25"/>
<connect gate="G$1" pin="(PD0/MISO/PCINT3)PB3" pad="17"/>
<connect gate="G$1" pin="(PDI/MOSI/PCINT2)PB2" pad="16"/>
<connect gate="G$1" pin="(RTS/AIN5/INT6)PD6" pad="12"/>
<connect gate="G$1" pin="(RXD1/AIN1/INT2)PD2" pad="8"/>
<connect gate="G$1" pin="(SCLK/PCINT1)PB1" pad="15"/>
<connect gate="G$1" pin="(SS/PCINT0)PB0" pad="14"/>
<connect gate="G$1" pin="(T1/PCINT4)PB4" pad="18"/>
<connect gate="G$1" pin="(TXD1/INT3)PD3" pad="9"/>
<connect gate="G$1" pin="(XCK/AIN4/PCINT12)PD5" pad="11"/>
<connect gate="G$1" pin="AVCC" pad="32"/>
<connect gate="G$1" pin="D+" pad="29"/>
<connect gate="G$1" pin="D-" pad="30"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="PAD" pad="33"/>
<connect gate="G$1" pin="RESET(PC1/DW)" pad="24"/>
<connect gate="G$1" pin="UCAP" pad="27"/>
<connect gate="G$1" pin="UGND" pad="28"/>
<connect gate="G$1" pin="UVCC" pad="31"/>
<connect gate="G$1" pin="VCC" pad="4"/>
<connect gate="G$1" pin="XTAL1" pad="1"/>
<connect gate="G$1" pin="XTAL2(PC0)" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="QFN32_5x5"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="ATMEGA16U2-MUR"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Circuito_Integrato"/>
<attribute name="VALUE" value="ATMEGA16U2-MU"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0008_TEXLP2985-33DBVR" prefix="IC" uservalue="yes">
<description>150-mA LOW-NOISE LOW-DROPOUT REGULATOR WITH SHUTDOWN   LP2985-33DBVR</description>
<gates>
<gate name="G$1" symbol="LP2985-XXDBVR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-DBV">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="NC/FB" pad="4"/>
<connect gate="G$1" pin="ON/!OFF" pad="3"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="SOT23-5"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="TEXLP2985-33DBVR"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Circuito_Integrato"/>
<attribute name="VALUE" value="LP2985-33DBVR"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0021_LMV358IDGKR" prefix="IC" uservalue="yes">
<description>Dual low-voltage (2.7 V to 5.5 V), operational amplifiers with rail-to-rail output swing   LMV358IDGKR</description>
<gates>
<gate name="A" symbol="OPAMP" x="0" y="10.16"/>
<gate name="B" symbol="OPAMP" x="0" y="-10.16"/>
<gate name="P" symbol="PWR+-" x="0" y="10.16" addlevel="always"/>
</gates>
<devices>
<device name="" package="MSOP08">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="P" pin="V+" pad="8"/>
<connect gate="P" pin="V-" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="MSOP8"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="LMV358IDGKR"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Circuito_Integrato"/>
<attribute name="VALUE" value="LMV358IDGKR"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0023_ATMEGA2560-16AU" prefix="IC" uservalue="yes">
<description>8-bit Microcontroller with 256K Bytes In-System Programmable Flash - ATMEGA2560-16AU</description>
<gates>
<gate name="1" symbol="86-I/O-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP100">
<connects>
<connect gate="1" pin="(A10)PC2" pad="55"/>
<connect gate="1" pin="(A11)PC3" pad="56"/>
<connect gate="1" pin="(A12)PC4" pad="57"/>
<connect gate="1" pin="(A13)PC5" pad="58"/>
<connect gate="1" pin="(A14)PC6" pad="59"/>
<connect gate="1" pin="(A15)PC7" pad="60"/>
<connect gate="1" pin="(A8)PC0" pad="53"/>
<connect gate="1" pin="(A9)PC1" pad="54"/>
<connect gate="1" pin="(AD0)PA0" pad="78"/>
<connect gate="1" pin="(AD1)PA1" pad="77"/>
<connect gate="1" pin="(AD2)PA2" pad="76"/>
<connect gate="1" pin="(AD3)PA3" pad="75"/>
<connect gate="1" pin="(AD4)PA4" pad="74"/>
<connect gate="1" pin="(AD5)PA5" pad="73"/>
<connect gate="1" pin="(AD6)PA6" pad="72"/>
<connect gate="1" pin="(AD7)PA7" pad="71"/>
<connect gate="1" pin="(ADC0)PF0" pad="97"/>
<connect gate="1" pin="(ADC1)PF1" pad="96"/>
<connect gate="1" pin="(ADC2)PF2" pad="95"/>
<connect gate="1" pin="(ADC3)PF3" pad="94"/>
<connect gate="1" pin="(ADC4/TCK)PF4" pad="93"/>
<connect gate="1" pin="(ADC5/TMS)PF5" pad="92"/>
<connect gate="1" pin="(ADC6/TDO)PF6" pad="91"/>
<connect gate="1" pin="(ADC7/TDI)PF7" pad="90"/>
<connect gate="1" pin="(ALE)PG2" pad="70"/>
<connect gate="1" pin="(CLKO/ICP3/INT7)PE7" pad="9"/>
<connect gate="1" pin="(ICP1)PD4" pad="47"/>
<connect gate="1" pin="(MISO/PCINT3)PB3" pad="22"/>
<connect gate="1" pin="(MOSI/PCINT2)PB2" pad="21"/>
<connect gate="1" pin="(OC0A/OC1C/PCINT7)PB7" pad="26"/>
<connect gate="1" pin="(OC0B)PG5" pad="1"/>
<connect gate="1" pin="(OC1A/PCINT5)PB5" pad="24"/>
<connect gate="1" pin="(OC1B/PCINT6)PB6" pad="25"/>
<connect gate="1" pin="(OC2A/PCINT4)PB4" pad="23"/>
<connect gate="1" pin="(OC3A/AIN1)PE3" pad="5"/>
<connect gate="1" pin="(OC3B/INT4)PE4" pad="6"/>
<connect gate="1" pin="(OC3C/INT5)PE5" pad="7"/>
<connect gate="1" pin="(RD)PG1" pad="52"/>
<connect gate="1" pin="(RXD0/PCIN8)PE0" pad="2"/>
<connect gate="1" pin="(RXD1/INT2)PD2" pad="45"/>
<connect gate="1" pin="(SCK/PCINT1)PB1" pad="20"/>
<connect gate="1" pin="(SCL/INT0)PD0" pad="43"/>
<connect gate="1" pin="(SDA/INT1)PD1" pad="44"/>
<connect gate="1" pin="(SS/PCINT0)PB0" pad="19"/>
<connect gate="1" pin="(T0)PD7" pad="50"/>
<connect gate="1" pin="(T1)PD6" pad="49"/>
<connect gate="1" pin="(T3/INT6)PE6" pad="8"/>
<connect gate="1" pin="(TOSC1)PG4" pad="29"/>
<connect gate="1" pin="(TOSC2)PG3" pad="28"/>
<connect gate="1" pin="(TXD0)PE1" pad="3"/>
<connect gate="1" pin="(TXD1/INT3)PD3" pad="46"/>
<connect gate="1" pin="(WR)PG0" pad="51"/>
<connect gate="1" pin="(XCK0/AIN0)PE2" pad="4"/>
<connect gate="1" pin="(XCK1)PD5" pad="48"/>
<connect gate="1" pin="AGND" pad="99"/>
<connect gate="1" pin="AREF" pad="98"/>
<connect gate="1" pin="AVCC" pad="100"/>
<connect gate="1" pin="GND" pad="11"/>
<connect gate="1" pin="GND1" pad="32"/>
<connect gate="1" pin="GND2" pad="62"/>
<connect gate="1" pin="GND3" pad="81"/>
<connect gate="1" pin="PH0(RXD2)" pad="12"/>
<connect gate="1" pin="PH1(TXD2)" pad="13"/>
<connect gate="1" pin="PH2(XCK2)" pad="14"/>
<connect gate="1" pin="PH3(OC4A)" pad="15"/>
<connect gate="1" pin="PH4(OC4B)" pad="16"/>
<connect gate="1" pin="PH5(OC4C)" pad="17"/>
<connect gate="1" pin="PH6(OC2B)" pad="18"/>
<connect gate="1" pin="PH7(T4)" pad="27"/>
<connect gate="1" pin="PJ0(RXD3/PCINT9)" pad="63"/>
<connect gate="1" pin="PJ1(TXD3/PCINT10)" pad="64"/>
<connect gate="1" pin="PJ2(XCK3/PCINT11)" pad="65"/>
<connect gate="1" pin="PJ3(PCINT12)" pad="66"/>
<connect gate="1" pin="PJ4(PCINT13)" pad="67"/>
<connect gate="1" pin="PJ5(PCINT14)" pad="68"/>
<connect gate="1" pin="PJ6(PCINT15)" pad="69"/>
<connect gate="1" pin="PJ7" pad="79"/>
<connect gate="1" pin="PK0(ADC8/PCINT16)" pad="89"/>
<connect gate="1" pin="PK1(ADC9/PCINT17)" pad="88"/>
<connect gate="1" pin="PK2(ADC10/PCINT18)" pad="87"/>
<connect gate="1" pin="PK3(ADC11/PCINT19)" pad="86"/>
<connect gate="1" pin="PK4(ADC12/PCINT20)" pad="85"/>
<connect gate="1" pin="PK5(ADC13/PCINT21)" pad="84"/>
<connect gate="1" pin="PK6(ADC14/PCINT22)" pad="83"/>
<connect gate="1" pin="PK7(ADC15/PCINT23)" pad="82"/>
<connect gate="1" pin="PL0(ICP4)" pad="35"/>
<connect gate="1" pin="PL1(ICP5)" pad="36"/>
<connect gate="1" pin="PL2(T5)" pad="37"/>
<connect gate="1" pin="PL3(OC5A)" pad="38"/>
<connect gate="1" pin="PL4(OC5B)" pad="39"/>
<connect gate="1" pin="PL5(OC5C)" pad="40"/>
<connect gate="1" pin="PL6" pad="41"/>
<connect gate="1" pin="PL7" pad="42"/>
<connect gate="1" pin="RESET" pad="30"/>
<connect gate="1" pin="VCC" pad="10"/>
<connect gate="1" pin="VCC1" pad="31"/>
<connect gate="1" pin="VCC2" pad="61"/>
<connect gate="1" pin="VCC3" pad="80"/>
<connect gate="1" pin="XTAL1" pad="34"/>
<connect gate="1" pin="XTAL2" pad="33"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="TQFP100_14x14"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="ATMEGA2560-16AU"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Circuito_Integrato"/>
<attribute name="VALUE" value="ATMEGA2560-15AU"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Condensatori">
<packages>
<package name="C0603-ROUND">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.683" x2="1.473" y2="0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.683" x2="1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.683" x2="-1.473" y2="-0.683" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.683" x2="-1.473" y2="0.683" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.889" y="0" dx="1.1" dy="0.8984" layer="1" roundness="80" rot="R90"/>
<smd name="2" x="0.889" y="0" dx="1.1" dy="0.8984" layer="1" roundness="80" rot="R90"/>
<text x="-0.889" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SMC_B">
<description>&lt;b&gt;Chip Capacitor &lt;/b&gt; Polar tantalum capacitors with solid electrolyte&lt;p&gt;
Siemens Matsushita Components B 45 194, B 45 197, B 45 198&lt;br&gt;
Source: www.farnell.com/datasheets/247.pdf</description>
<wire x1="-1.6" y1="1.35" x2="1.6" y2="1.35" width="0.1016" layer="51"/>
<wire x1="1.6" y1="1.35" x2="1.6" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.6" y1="-1.35" x2="-1.6" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="-1.35" x2="-1.6" y2="1.35" width="0.1016" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.55" y2="1.1" layer="51"/>
<rectangle x1="1.55" y1="-1.1" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<rectangle x1="-1.6" y1="-1.35" x2="-0.95" y2="1.35" layer="51"/>
<smd name="+" x="-1.5" y="0" dx="1.6" dy="2.4" layer="1"/>
<smd name="-" x="1.5" y="0" dx="1.6" dy="2.4" layer="1" rot="R180"/>
<text x="-1.905" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-2.43" y1="1.43" x2="2.43" y2="1.43" width="0.0508" layer="39"/>
<wire x1="2.43" y1="1.43" x2="2.43" y2="-1.42" width="0.0508" layer="39"/>
<wire x1="2.43" y1="-1.42" x2="-2.43" y2="-1.42" width="0.0508" layer="39"/>
<wire x1="-2.43" y1="-1.42" x2="-2.43" y2="1.43" width="0.0508" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="C-EU">
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="CPOL">
<rectangle x1="-1.651" y1="-2.54" x2="1.651" y2="-1.651" layer="94"/>
<wire x1="-1.524" y1="-0.889" x2="1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.889" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="1.524" y2="0" width="0.254" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="1.143" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="0.4064" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-4.5974" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="0015_100NF_0603" prefix="C" uservalue="yes">
<description>100nF 0603 50V</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0603-ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="0603"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="cl10b104kbnc"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Condensatori"/>
<attribute name="VALUE" value="100nF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0167_" prefix="C" uservalue="yes">
<description>22uF 25V</description>
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMC_B">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value=""/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Condensatori"/>
<attribute name="VALUE" value="22uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0014_22PF_0603" prefix="C" uservalue="yes">
<description>22pF 0603</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0603-ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="0603"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="22pF_0603"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Condensatori"/>
<attribute name="VALUE" value="22p"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0006_1UF_0603/GRM188R61E105KA12D" prefix="C" uservalue="yes">
<description>1uF 0603</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0603-ROUND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="0603"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="1uF_0603/GRM188R61E105KA12D"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Condensatori"/>
<attribute name="VALUE" value="1uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connettori">
<packages>
<package name="PN61729">
<description>&lt;b&gt;BERG&lt;/b&gt;</description>
<wire x1="-5.9" y1="5.6" x2="-5.9" y2="-10.15" width="0.254" layer="21"/>
<wire x1="-5.9" y1="-10.15" x2="5.9" y2="-10.15" width="0.254" layer="21"/>
<wire x1="5.9" y1="-10.15" x2="5.9" y2="5.6" width="0.254" layer="21"/>
<wire x1="5.9" y1="5.6" x2="-5.9" y2="5.6" width="0.254" layer="21"/>
<wire x1="5.9" y1="5.6" x2="-5.9" y2="5.6" width="0.254" layer="51"/>
<wire x1="-5.9" y1="5.6" x2="-5.9" y2="-10.15" width="0.254" layer="51"/>
<wire x1="5.9" y1="-10.15" x2="5.9" y2="5.6" width="0.254" layer="51"/>
<wire x1="-5.9" y1="-10.15" x2="5.9" y2="-10.15" width="0.254" layer="51"/>
<pad name="1" x="1.25" y="4.71" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="2" x="-1.25" y="4.71" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-1.25" y="2.71" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="4" x="1.25" y="2.71" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="P$1" x="-6" y="0" drill="2.2"/>
<pad name="P$2" x="6" y="0" drill="2.2"/>
<text x="-6.35" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="7.62" y="-8.89" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<wire x1="-6.1" y1="5.8" x2="-6.1" y2="1.76" width="0.05" layer="39"/>
<wire x1="-6.1" y1="-1.8" x2="-6.1" y2="-10.4" width="0.05" layer="39"/>
<wire x1="-6.1" y1="-10.4" x2="6.1" y2="-10.4" width="0.05" layer="39"/>
<wire x1="6.1" y1="-10.4" x2="6.1" y2="-1.8" width="0.05" layer="39"/>
<wire x1="6.1" y1="1.8" x2="6.1" y2="5.8" width="0.05" layer="39"/>
<wire x1="6.1" y1="5.8" x2="-6.1" y2="5.8" width="0.05" layer="39"/>
<wire x1="-6.1" y1="1.76" x2="-6.1" y2="-1.8" width="0.05" layer="39" curve="180"/>
<wire x1="6.1" y1="1.8" x2="6.1" y2="-1.8" width="0.05" layer="39" curve="-176.817719"/>
</package>
<package name="PN87520-S">
<description>&lt;b&gt;USB connector&lt;/b&gt; with shield&lt;p&gt;</description>
<wire x1="-7.4" y1="-10.19" x2="7.4" y2="-10.19" width="0.254" layer="21"/>
<wire x1="7.4" y1="-10.19" x2="7.4" y2="4.11" width="0.254" layer="21"/>
<wire x1="7.4" y1="4.11" x2="-7.4" y2="4.11" width="0.254" layer="21"/>
<wire x1="-7.4" y1="4.11" x2="-7.4" y2="-10.19" width="0.254" layer="21"/>
<wire x1="-5.08" y1="-2.87" x2="-3.81" y2="-8.72" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-8.72" x2="-2.54" y2="-8.72" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-8.72" x2="-1.27" y2="-2.87" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-2.87" x2="2.54" y2="-8.72" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-8.72" x2="3.81" y2="-8.72" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-8.72" x2="5.08" y2="-2.87" width="0.1524" layer="21"/>
<wire x1="-2.46" y1="-0.1" x2="-2.46" y2="0.9" width="0.0508" layer="21" curve="180"/>
<wire x1="-2.46" y1="-1.1" x2="-2.46" y2="-0.1" width="0.0508" layer="21" curve="180"/>
<wire x1="3.665" y1="0.4" x2="3.665" y2="-0.6" width="0.0508" layer="21" curve="180"/>
<wire x1="3.415" y1="0.9" x2="3.415" y2="-1.1" width="0.0508" layer="21" curve="180"/>
<wire x1="3.665" y1="0.4" x2="4.165" y2="0.4" width="0.0508" layer="21" curve="-15.189287"/>
<wire x1="3.415" y1="0.9" x2="4.175" y2="0.845" width="0.0508" layer="21" curve="-12.057134"/>
<wire x1="3.415" y1="-1.1" x2="4.165" y2="-0.975" width="0.0508" layer="21" curve="18.422836"/>
<wire x1="1.665" y1="-0.35" x2="1.665" y2="0.9" width="0.0508" layer="21" curve="180"/>
<wire x1="1.29" y1="0.025" x2="1.29" y2="0.4" width="0.0508" layer="21" curve="180"/>
<wire x1="-3.835" y1="0.9" x2="-3.835" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="-3.835" y1="-1.1" x2="-3.21" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="-1.1" x2="-3.21" y2="-0.1" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="-0.1" x2="-3.21" y2="0.9" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="0.9" x2="-3.835" y2="0.9" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="0.9" x2="-2.46" y2="0.4" width="0.0508" layer="21"/>
<wire x1="-2.46" y1="0.4" x2="-3.21" y2="-0.1" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="-0.1" x2="-2.46" y2="-0.6" width="0.0508" layer="21"/>
<wire x1="-2.46" y1="-0.6" x2="-3.21" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="-2.46" y1="-1.1" x2="-3.21" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="-2.46" y1="0.9" x2="-3.21" y2="0.9" width="0.0508" layer="21"/>
<wire x1="-1.71" y1="0.9" x2="-1.71" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="-1.71" y1="-1.1" x2="0.04" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="0.04" y1="-1.1" x2="0.04" y2="-0.6" width="0.0508" layer="21"/>
<wire x1="0.04" y1="-0.6" x2="-1.085" y2="-0.6" width="0.0508" layer="21"/>
<wire x1="-1.085" y1="-0.6" x2="-1.085" y2="-0.35" width="0.0508" layer="21"/>
<wire x1="-1.085" y1="-0.35" x2="0.04" y2="-0.35" width="0.0508" layer="21"/>
<wire x1="0.04" y1="-0.35" x2="0.04" y2="0.15" width="0.0508" layer="21"/>
<wire x1="0.04" y1="0.15" x2="-1.085" y2="0.15" width="0.0508" layer="21"/>
<wire x1="-1.085" y1="0.15" x2="-1.085" y2="0.4" width="0.0508" layer="21"/>
<wire x1="-1.085" y1="0.4" x2="0.04" y2="0.4" width="0.0508" layer="21"/>
<wire x1="0.04" y1="0.4" x2="0.04" y2="0.9" width="0.0508" layer="21"/>
<wire x1="0.04" y1="0.9" x2="-1.71" y2="0.9" width="0.0508" layer="21"/>
<wire x1="0.29" y1="0.9" x2="0.29" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="0.29" y1="-1.1" x2="0.915" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="0.915" y1="-1.1" x2="0.915" y2="-0.35" width="0.0508" layer="21"/>
<wire x1="0.915" y1="-0.35" x2="1.415" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="1.415" y1="-1.1" x2="2.165" y2="-1.1" width="0.0508" layer="21"/>
<wire x1="2.165" y1="-1.1" x2="1.665" y2="-0.35" width="0.0508" layer="21"/>
<wire x1="0.915" y1="0.4" x2="0.915" y2="0.025" width="0.0508" layer="21"/>
<wire x1="0.29" y1="0.9" x2="1.665" y2="0.9" width="0.0508" layer="21"/>
<wire x1="0.915" y1="0.4" x2="1.29" y2="0.4" width="0.0508" layer="21"/>
<wire x1="0.915" y1="0.025" x2="1.29" y2="0.025" width="0.0508" layer="21"/>
<wire x1="3.665" y1="-0.1" x2="4.165" y2="-0.1" width="0.0508" layer="21"/>
<wire x1="3.665" y1="-0.1" x2="3.665" y2="-0.6" width="0.0508" layer="21"/>
<wire x1="4.16" y1="0.4" x2="4.16" y2="0.845" width="0.0508" layer="21"/>
<wire x1="4.165" y1="-0.1" x2="4.165" y2="-0.975" width="0.0508" layer="21"/>
<pad name="1" x="-3.5" y="2.71" drill="0.95" shape="long" rot="R90"/>
<pad name="2" x="-1" y="2.71" drill="0.95" shape="long" rot="R90"/>
<pad name="3" x="1" y="2.71" drill="0.95" shape="long" rot="R90"/>
<pad name="4" x="3.5" y="2.71" drill="0.95" shape="long" rot="R90"/>
<pad name="S1" x="-6.57" y="0" drill="2.2" diameter="2.9"/>
<pad name="S2" x="6.57" y="0" drill="2.2" diameter="2.9" rot="R180"/>
<text x="-7.62" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="9.144" y="-10.16" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-3.81" y="-2.04" size="0.4064" layer="21">E L E C T R O N I C S</text>
<wire x1="-7.6" y1="4.3" x2="7.6" y2="4.3" width="0.05" layer="39"/>
<wire x1="7.6" y1="4.3" x2="7.6" y2="1.3" width="0.05" layer="39"/>
<wire x1="7.6" y1="1.3" x2="7.6" y2="-1.3" width="0.05" layer="39" curve="-102.144913"/>
<wire x1="7.6" y1="-1.3" x2="7.6" y2="-10.4" width="0.05" layer="39"/>
<wire x1="7.6" y1="-10.4" x2="-7.6" y2="-10.4" width="0.05" layer="39"/>
<wire x1="-7.6" y1="-10.4" x2="-7.6" y2="-1.3" width="0.05" layer="39"/>
<wire x1="-7.6" y1="-1.3" x2="-7.6" y2="1.3" width="0.05" layer="39" curve="-104.862816"/>
<wire x1="-7.6" y1="1.3" x2="-7.6" y2="4.3" width="0.05" layer="39"/>
</package>
<package name="2X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" diameter="1.55" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" diameter="1.55" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" diameter="1.55" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="1.55" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" diameter="1.55" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" diameter="1.55" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.93" y1="2.66" x2="3.93" y2="2.66" width="0.05" layer="39"/>
<wire x1="3.93" y1="2.66" x2="3.93" y2="-2.65" width="0.05" layer="39"/>
<wire x1="3.93" y1="-2.65" x2="-3.93" y2="-2.65" width="0.05" layer="39"/>
<wire x1="-3.93" y1="-2.65" x2="-3.93" y2="2.66" width="0.05" layer="39"/>
</package>
<package name="1X10">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="7.62" y1="0.635" x2="8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="1.27" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-1.27" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="1.27" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-0.635" x2="-12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-1.27" x2="-12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="12.065" y2="1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="1.27" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-0.635" x2="12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-11.43" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="2" x="-8.89" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="3" x="-6.35" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="4" x="-3.81" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="5" x="-1.27" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="7" x="3.81" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="8" x="6.35" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="10" x="11.43" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<text x="-12.7762" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-12.7" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="-11.684" y1="-0.254" x2="-11.176" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<wire x1="-12.8" y1="1.6" x2="12.8" y2="1.6" width="0.05" layer="39"/>
<wire x1="12.8" y1="1.6" x2="12.8" y2="-1.6" width="0.05" layer="39"/>
<wire x1="12.8" y1="-1.6" x2="-12.8" y2="-1.6" width="0.05" layer="39"/>
<wire x1="-12.8" y1="-1.6" x2="-12.8" y2="1.6" width="0.05" layer="39"/>
</package>
<package name="1X08">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="1.27" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-1.27" x2="-9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<pad name="1" x="-8.89" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="0" drill="0.85" diameter="1.4224" shape="long" rot="R90"/>
<text x="-10.2362" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.16" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-10.27" y1="1.63" x2="10.27" y2="1.6" width="0.05" layer="39"/>
<wire x1="10.27" y1="1.6" x2="10.27" y2="-1.6" width="0.05" layer="39"/>
<wire x1="10.27" y1="-1.6" x2="-10.27" y2="-1.57" width="0.05" layer="39"/>
<wire x1="-10.27" y1="-1.57" x2="-10.27" y2="1.63" width="0.05" layer="39"/>
</package>
<package name="2X18">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-22.86" y1="-1.905" x2="-22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="-2.54" x2="-20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="-2.54" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-2.54" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-2.54" x2="-15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-2.54" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="2.54" x2="-20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="2.54" x2="-17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="2.54" x2="-15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="-2.54" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.875" y1="-2.54" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-2.54" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="51"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="51"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="51"/>
<pad name="1" x="-21.59" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="2" x="-21.59" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-19.05" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="4" x="-19.05" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="5" x="-16.51" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="6" x="-16.51" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="7" x="-13.97" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="8" x="-13.97" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="9" x="-11.43" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="10" x="-11.43" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="11" x="-8.89" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="12" x="-8.89" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="13" x="-6.35" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="14" x="-6.35" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="15" x="-3.81" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="16" x="-3.81" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="17" x="-1.27" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="18" x="-1.27" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="19" x="1.27" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="20" x="1.27" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="21" x="3.81" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="22" x="3.81" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="23" x="6.35" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="24" x="6.35" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="25" x="8.89" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="26" x="8.89" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="27" x="11.43" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="28" x="11.43" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="29" x="13.97" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="30" x="13.97" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="31" x="16.51" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="32" x="16.51" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="33" x="19.05" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="34" x="19.05" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="35" x="21.59" y="-1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<pad name="36" x="21.59" y="1.27" drill="0.95" diameter="1.6764" shape="octagon"/>
<text x="-22.86" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-22.86" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-22.99" y1="2.65" x2="22.99" y2="2.65" width="0.05" layer="39"/>
<wire x1="22.99" y1="2.65" x2="22.99" y2="-2.65" width="0.05" layer="39"/>
<wire x1="22.99" y1="-2.65" x2="-22.99" y2="-2.64" width="0.05" layer="39"/>
<wire x1="-22.99" y1="-2.64" x2="-22.99" y2="2.65" width="0.05" layer="39"/>
</package>
<package name="POWERSUPPLY_DC-21MM">
<description>DC 2.1mm Jack</description>
<wire x1="-2.8575" y1="-6.35" x2="-5.08" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-6.35" x2="-5.08" y2="-6.0325" width="0.127" layer="21"/>
<wire x1="2.54" y1="-6.35" x2="3.9116" y2="-6.35" width="0.127" layer="21"/>
<wire x1="3.9116" y1="-6.35" x2="3.9116" y2="3.6576" width="0.127" layer="21"/>
<wire x1="3.9116" y1="3.6576" x2="3.9116" y2="7.1374" width="0.127" layer="21"/>
<wire x1="3.9116" y1="7.1374" x2="-5.08" y2="7.1374" width="0.127" layer="21"/>
<wire x1="-5.08" y1="7.1374" x2="-5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-5.0546" y1="3.6576" x2="3.9116" y2="3.6576" width="0.127" layer="21"/>
<pad name="1" x="-5.08" y="-3.3528" drill="1.3" diameter="4" shape="octagon" rot="R90"/>
<pad name="2" x="-0.0762" y="-6.35" drill="1.3" diameter="4" shape="octagon"/>
<pad name="3" x="-0.0762" y="-0.3556" drill="1.3" diameter="4" shape="octagon" first="yes"/>
<pad name="4" x="-5.08" y="-4.1148" drill="1.3"/>
<pad name="5" x="-5.08" y="-2.5908" drill="1.3"/>
<pad name="6" x="-0.8382" y="-0.3556" drill="1.3"/>
<pad name="7" x="0.6858" y="-0.3556" drill="1.3"/>
<pad name="8" x="-0.8382" y="-6.35" drill="1.3"/>
<pad name="9" x="0.6858" y="-6.35" drill="1.3"/>
<text x="6.35" y="-6.35" size="1.778" layer="25" ratio="5" rot="R90">&gt;NAME</text>
<text x="8.89" y="-6.35" size="1.778" layer="27" ratio="5" rot="R90">&gt;VALUE</text>
<wire x1="-5.23" y1="7.28" x2="4" y2="7.28" width="0.05" layer="39"/>
<wire x1="4" y1="7.28" x2="4" y2="-6.5" width="0.05" layer="39"/>
<wire x1="4" y1="-6.5" x2="2.1" y2="-6.5" width="0.05" layer="39"/>
<wire x1="2.1" y1="-6.5" x2="2.1" y2="-7.24" width="0.05" layer="39"/>
<wire x1="2.1" y1="-7.24" x2="0.82" y2="-8.52" width="0.05" layer="39"/>
<wire x1="0.82" y1="-8.52" x2="-0.99" y2="-8.52" width="0.05" layer="39"/>
<wire x1="-0.99" y1="-8.52" x2="-2.25" y2="-7.26" width="0.05" layer="39"/>
<wire x1="-2.25" y1="-7.26" x2="-2.25" y2="-6.5" width="0.05" layer="39"/>
<wire x1="-2.25" y1="-6.5" x2="-5.2" y2="-6.5" width="0.05" layer="39"/>
<wire x1="-5.2" y1="-6.5" x2="-5.2" y2="-5.52" width="0.05" layer="39"/>
<wire x1="-5.2" y1="-5.52" x2="-6" y2="-5.52" width="0.05" layer="39"/>
<wire x1="-6" y1="-5.52" x2="-7.27" y2="-4.25" width="0.05" layer="39"/>
<wire x1="-7.27" y1="-4.25" x2="-7.27" y2="-2.44" width="0.05" layer="39"/>
<wire x1="-7.27" y1="-2.44" x2="-5.99" y2="-1.17" width="0.05" layer="39"/>
<wire x1="-5.99" y1="-1.17" x2="-5.23" y2="-1.17" width="0.05" layer="39"/>
<wire x1="-5.23" y1="-1.17" x2="-5.23" y2="7.28" width="0.05" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="J11">
<wire x1="0" y1="7.62" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="P$1" x="2.54" y="-10.16" length="middle" rot="R90"/>
<pin name="P$2" x="5.08" y="-10.16" length="middle" rot="R90"/>
<text x="0" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.08" y="-2.54" size="2.54" layer="94" rot="R90">USB</text>
</symbol>
<symbol name="USB-SHIELD">
<wire x1="0" y1="7.62" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-9.398" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-8.89" x2="2.54" y2="-8.128" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-5.334" x2="6.35" y2="-4.572" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-4.064" x2="6.35" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-2.794" x2="6.35" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-1.524" x2="6.35" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-0.254" x2="6.35" y2="0.508" width="0.1524" layer="94"/>
<wire x1="6.35" y1="1.016" x2="6.35" y2="1.778" width="0.1524" layer="94"/>
<wire x1="6.35" y1="2.286" x2="6.35" y2="3.048" width="0.1524" layer="94"/>
<wire x1="6.35" y1="3.556" x2="6.35" y2="4.318" width="0.1524" layer="94"/>
<wire x1="6.35" y1="4.826" x2="6.35" y2="5.588" width="0.1524" layer="94"/>
<wire x1="6.35" y1="6.096" x2="6.35" y2="6.858" width="0.1524" layer="94"/>
<wire x1="6.35" y1="7.366" x2="6.35" y2="8.128" width="0.1524" layer="94"/>
<wire x1="6.35" y1="8.382" x2="5.588" y2="8.382" width="0.1524" layer="94"/>
<wire x1="5.08" y1="8.382" x2="4.318" y2="8.382" width="0.1524" layer="94"/>
<wire x1="3.81" y1="8.382" x2="3.048" y2="8.382" width="0.1524" layer="94"/>
<wire x1="2.54" y1="8.382" x2="1.778" y2="8.382" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-7.112" x2="2.54" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-5.842" x2="2.794" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="4.826" y1="-5.842" x2="4.064" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="6.096" y1="-5.842" x2="5.334" y2="-5.842" width="0.1524" layer="94"/>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="S1" x="0" y="-7.62" visible="off" length="short" direction="pas"/>
<pin name="S2" x="0" y="-10.16" visible="off" length="short" direction="pas"/>
<text x="0" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.08" y="-2.54" size="2.54" layer="94" rot="R90">USB</text>
</symbol>
<symbol name="PINH2X3">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PINHD10">
<wire x1="-6.35" y1="-15.24" x2="1.27" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-15.24" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-15.24" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="9" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD8">
<wire x1="-6.35" y1="-10.16" x2="1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PINH2X18">
<wire x1="-6.35" y1="-25.4" x2="8.89" y2="-25.4" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-25.4" x2="8.89" y2="22.86" width="0.4064" layer="94"/>
<wire x1="8.89" y1="22.86" x2="-6.35" y2="22.86" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="22.86" x2="-6.35" y2="-25.4" width="0.4064" layer="94"/>
<pin name="1" x="-2.54" y="20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="27" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="28" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="29" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="30" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="31" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="32" x="5.08" y="-17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="33" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="34" x="5.08" y="-20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="35" x="-2.54" y="-22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="36" x="5.08" y="-22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<text x="-6.35" y="23.495" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-27.94" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="POWERSUPPLY_DC21PWR">
<wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="-10.16" length="middle" direction="pwr" rot="R90"/>
<pin name="2" x="5.08" y="-10.16" length="middle" direction="pwr" rot="R90"/>
<pin name="3" x="15.24" y="0" length="middle" direction="pwr" rot="R180"/>
<text x="-10.16" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="0056_USB-B-S-RA-WT-SPCC" prefix="USB" uservalue="yes">
<description>USB - B</description>
<gates>
<gate name="G$1" symbol="J11" x="0" y="5.08"/>
</gates>
<devices>
<device name="" package="PN61729">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="USB-B-S-RA-WT-SPCC"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Connettori"/>
<attribute name="VALUE" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0054_USB-A-S-RA" prefix="X" uservalue="yes">
<description>USB   A </description>
<gates>
<gate name="G$1" symbol="USB-SHIELD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PN87520-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="S1" pad="S1"/>
<connect gate="G$1" pin="S2" pad="S2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="USB-A-S-RA"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Connettori"/>
<attribute name="VALUE" value="USB-A-THT"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0114_PH254-203DF118A00V" prefix="JP" uservalue="yes">
<description>3x2 M V h 8,5 mm p2,54</description>
<gates>
<gate name="A" symbol="PINH2X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="PH254-203DF118A00V"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Connettori"/>
<attribute name="VALUE" value="ICSP"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1216_FH254-110DF08500T30" prefix="JP" uservalue="yes">
<description>Female Connector strip for Arduino bases - 10x1 F V h 8,5 p2,54 serigrafato STRISCIA VUOTA</description>
<gates>
<gate name="A" symbol="PINHD10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X10">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="FH254-110DF08500T30"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Connettori"/>
<attribute name="VALUE" value="FH254-110DF08500T30"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1218_FH254-108DF08500T20" prefix="JP" uservalue="yes">
<description>Female Connector strip for Arduino bases - 8x1 F V h 8,5 p2,54 serigrafato DIGITAL</description>
<gates>
<gate name="A" symbol="PINHD8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X08">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="FH254-108DF08500T20"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Connettori"/>
<attribute name="VALUE" value="FH254-108DF08500T20"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1217_FH254-108DF08500T21" prefix="JP" uservalue="yes">
<description>Female Connector strip for Arduino bases - 8x1 F V h 8,5 p2,54 serigrafato POWER</description>
<gates>
<gate name="A" symbol="PINHD8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X08">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="FH254-108DF08500T21"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Connettori"/>
<attribute name="VALUE" value="FH254-108DF08500T21"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0058_FH254-108DF08500V" prefix="JP" uservalue="yes">
<description>Female Connector strip for Arduino bases - 8x1 F V h 8,5 p2,54mm</description>
<gates>
<gate name="A" symbol="PINHD8" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X08">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="FH254-108DF08500V"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Connettori"/>
<attribute name="VALUE" value="FH254-108DF08500V"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0060_FH254-218DF08500V" prefix="JP" uservalue="yes">
<description>18x2 F V h 8,5 p2,54</description>
<gates>
<gate name="A" symbol="PINH2X18" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X18">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="FH254-218DF08500V"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Connettori"/>
<attribute name="VALUE" value="18x2F-H8.5"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0018_19746-POWER-SUPPLY" prefix="JK" uservalue="yes">
<description>JACK</description>
<gates>
<gate name="G$1" symbol="POWERSUPPLY_DC21PWR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="POWERSUPPLY_DC-21MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="19746"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Connettori"/>
<attribute name="VALUE" value="POWER-SUPPLY"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Cristalli">
<packages>
<package name="CRYSTAL-3.2-2.5">
<wire x1="1.6" y1="1.25" x2="1.6" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.6" y1="-1.25" x2="-1.6" y2="-1.25" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.25" x2="-1.6" y2="1.25" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1.25" x2="1.6" y2="1.25" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1.2" x2="1.6" y2="1.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.2" x2="1.6" y2="-1.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.2" x2="-1.6" y2="-1.2" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.2" x2="-1.6" y2="1.2" width="0.127" layer="51"/>
<smd name="1" x="-1.15" y="-0.925" dx="1.05" dy="1.3" layer="1" rot="R90"/>
<smd name="2" x="1.15" y="-0.925" dx="1.05" dy="1.3" layer="1" rot="R90"/>
<smd name="3" x="1.15" y="0.925" dx="1.05" dy="1.3" layer="1" rot="R90"/>
<smd name="4" x="-1.15" y="0.925" dx="1.05" dy="1.3" layer="1" rot="R90"/>
<text x="-1.65" y="1.6" size="1.27" layer="25">&gt;name</text>
<text x="-1.6" y="-2.25" size="1.27" layer="27">&gt;value</text>
<wire x1="-1.94" y1="1.58" x2="1.94" y2="1.58" width="0.0508" layer="39"/>
<wire x1="1.94" y1="1.58" x2="1.94" y2="-1.58" width="0.0508" layer="39"/>
<wire x1="1.94" y1="-1.58" x2="-1.94" y2="-1.58" width="0.0508" layer="39"/>
<wire x1="-1.94" y1="-1.58" x2="-1.94" y2="1.58" width="0.0508" layer="39"/>
</package>
<package name="RESONATOR">
<wire x1="-1.4" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.8" x2="-1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="-1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0.8" x2="-1.6" y2="-0.8" width="0.22" layer="21"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.22" layer="21"/>
<smd name="1" x="-1" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="0" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="1" y="0" dx="2" dy="0.7" layer="1" rot="R90"/>
<text x="-1.45" y="1.35" size="0.3556" layer="25">&gt;NAME</text>
<text x="-1.45" y="-1.688" size="0.3556" layer="27">&gt;VALUE</text>
<wire x1="-1.74" y1="1.14" x2="1.74" y2="1.14" width="0.0508" layer="39"/>
<wire x1="1.74" y1="1.14" x2="1.74" y2="-1.14" width="0.0508" layer="39"/>
<wire x1="1.74" y1="-1.14" x2="-1.74" y2="-1.14" width="0.0508" layer="39"/>
<wire x1="-1.74" y1="-1.14" x2="-1.74" y2="1.14" width="0.0508" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="CRYSTAL-KX7">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.905" x2="-1.778" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.778" y1="2.54" x2="1.778" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.778" y1="2.54" x2="1.778" y2="1.905" width="0.4064" layer="94"/>
<wire x1="1.778" y1="-1.905" x2="1.778" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="1.778" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.778" y1="-2.54" x2="-1.778" y2="-1.905" width="0.4064" layer="94"/>
<pin name="C1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="C2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="GND@1" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="GND@2" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<text x="1.143" y="2.794" size="0.8636" layer="93" rot="R90">3</text>
<text x="-1.27" y="-4.318" size="1.27" layer="94">S</text>
<text x="-1.27" y="3.048" size="1.27" layer="94">S</text>
</symbol>
<symbol name="RESONATOR">
<wire x1="-1.524" y1="-0.508" x2="1.524" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-0.508" x2="1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.524" y1="0.508" x2="-1.524" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0.508" x2="-1.524" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-1.016" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.016" x2="1.778" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="1.016" x2="0" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="1.016" x2="1.778" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="1.016" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-2.032" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.032" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="3.302" x2="-2.032" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="3.302" x2="-3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-1.778" x2="-2.032" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-1.778" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-3.048" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R180"/>
<pin name="2" x="-7.62" y="0" visible="off" length="point" direction="pas"/>
<pin name="3" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R180"/>
<text x="-5.08" y="4.572" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.588" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="0396_12MHZ-12PF-K7" prefix="X" uservalue="yes">
<description>12Mhz - K7    12pF - SMD</description>
<gates>
<gate name="G$1" symbol="CRYSTAL-KX7" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRYSTAL-3.2-2.5">
<connects>
<connect gate="G$1" pin="C1" pad="1"/>
<connect gate="G$1" pin="C2" pad="3"/>
<connect gate="G$1" pin="GND@1" pad="2"/>
<connect gate="G$1" pin="GND@2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="VALUE" value="12mHz-12pF-K7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0395_16.000MHZ__12PF" prefix="X" uservalue="yes">
<description>16Mhz - K7   12pF - SMD</description>
<gates>
<gate name="G$1" symbol="CRYSTAL-KX7" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRYSTAL-3.2-2.5">
<connects>
<connect gate="G$1" pin="C1" pad="1"/>
<connect gate="G$1" pin="C2" pad="3"/>
<connect gate="G$1" pin="GND@1" pad="2"/>
<connect gate="G$1" pin="GND@2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="16.000Mhz__12pF"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Cristalli"/>
<attribute name="VALUE" value="16Mhz-KX-7"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0011_CSTCE16M0V53-R0" prefix="X" uservalue="yes">
<description>16Mhz - SMD</description>
<gates>
<gate name="G$1" symbol="RESONATOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RESONATOR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="CSTCE16M0V53-R0"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Cristalli"/>
<attribute name="VALUE" value="CSTCE16M0V53-R0"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Protezione">
<packages>
<package name="L1812">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.048" y1="1.8415" x2="3.0353" y2="1.8415" width="0.0508" layer="39"/>
<wire x1="3.0353" y1="1.8415" x2="3.0353" y2="-1.8415" width="0.0508" layer="39"/>
<wire x1="3.0353" y1="-1.8415" x2="-3.048" y2="-1.8415" width="0.0508" layer="39"/>
<wire x1="-3.048" y1="-1.8415" x2="-3.048" y2="1.8415" width="0.0508" layer="39"/>
</package>
<package name="0805">
<description>Multilayer SMD</description>
<wire x1="-1.3" y1="0" x2="1.2" y2="0" width="0.127" layer="21"/>
<wire x1="-1.1" y1="0" x2="1.1" y2="0" width="0.127" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.07" layer="21"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.07" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.07" layer="21"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.07" layer="51"/>
<rectangle x1="-0.35" y1="-0.15" x2="0.35" y2="0.15" layer="21"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1" layer="1"/>
<text x="-1.905" y="0.9525" size="0.6096" layer="27">&gt;VALUE</text>
<text x="-1.905" y="-1.5875" size="0.6096" layer="25">&gt;NAME</text>
<wire x1="-1.8288" y1="0.67945" x2="1.83515" y2="0.67945" width="0.0508" layer="39"/>
<wire x1="1.83515" y1="0.67945" x2="1.83515" y2="-0.67945" width="0.0508" layer="39"/>
<wire x1="1.83515" y1="-0.67945" x2="-1.8288" y2="-0.67945" width="0.0508" layer="39"/>
<wire x1="-1.8288" y1="-0.67945" x2="-1.8288" y2="0.67945" width="0.0508" layer="39"/>
</package>
<package name="CT/CN0603">
<description>&lt;b&gt;EPCOS SMD Varistors, MLV; Standard Series&lt;/b&gt;&lt;p&gt;
Source: www.farnell.com/datasheets/49238.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<rectangle x1="-0.75" y1="-0.35" x2="-0.4" y2="0.35" layer="51"/>
<rectangle x1="0.4" y1="-0.35" x2="0.75" y2="0.35" layer="51" rot="R180"/>
<smd name="1" x="-1" y="0" dx="1" dy="1" layer="1"/>
<smd name="2" x="1" y="0" dx="1" dy="1" layer="1"/>
<text x="-1.5" y="1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-1.6383" y1="0.635" x2="1.6383" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.6383" y1="0.635" x2="1.6383" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.6383" y1="-0.635" x2="-1.6383" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.6383" y1="-0.635" x2="-1.6383" y2="0.635" width="0.0508" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="L-EU">
<rectangle x1="-1.016" y1="-3.556" x2="1.016" y2="3.556" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="-1.4986" y="-3.81" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.302" y="-3.81" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="L">
<rectangle x1="-2.54" y1="1.27" x2="2.54" y2="3.81" layer="94"/>
<pin name="1" x="-5.08" y="2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="-1.27" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.08" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="VARISTOR">
<wire x1="2.54" y1="1.016" x2="2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.016" x2="-2.54" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.016" x2="-2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="2.032" y2="-2.032" width="0.254" layer="94"/>
<wire x1="-2.032" y1="2.032" x2="-2.54" y2="2.032" width="0.254" layer="94"/>
<wire x1="2.032" y1="-2.032" x2="-2.032" y2="2.032" width="0.254" layer="94"/>
<pin name="1" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-2.54" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="0002_MF-MSMF050-2" prefix="F" uservalue="yes">
<description>500 mA Multifuse � Polyfuse</description>
<gates>
<gate name="G$1" symbol="L-EU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="L1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="MF-MSMF050-2"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Protezione"/>
<attribute name="VALUE" value="MF-MSMF050-2 500mA"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0001_BLM21PG300SN1D_/_MH2029-300Y" prefix="L" uservalue="yes">
<description>BLM21</description>
<gates>
<gate name="G$1" symbol="L" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="BLM21PG300SN1D_/_MH2029-300Y"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Protezione"/>
<attribute name="VALUE" value="MH2029-300Y"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0010_BRNCG0603MLC-05E" prefix="Z" uservalue="yes">
<description>CG0603MLC-05E</description>
<gates>
<gate name="G$1" symbol="VARISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CT/CN0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="BRNCG0603MLC-05E"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Protezione"/>
<attribute name="VALUE" value="BRNCG0603MLC-05E"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Misc">
<packages>
<package name="J0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="1.025" y1="0.224" x2="1.515" y2="0.224" width="0.1524" layer="51"/>
<wire x1="1.515" y1="-0.224" x2="1.025" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="2.773" y2="0.483" width="0.0508" layer="39"/>
<wire x1="2.773" y1="0.483" x2="2.773" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="2.773" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<rectangle x1="0.716" y1="-0.3048" x2="1.016" y2="0.2951" layer="51"/>
<rectangle x1="1.5288" y1="-0.3048" x2="1.8288" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<rectangle x1="1.1001" y1="-0.4001" x2="1.4999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="3" x="1.95" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-1.143" y="0.635" size="1.016" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="2.413" y="-0.635" size="1.016" layer="27" font="vector" ratio="15" rot="R180">&gt;VALUE</text>
</package>
<package name="SJ">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
</package>
<package name="SJW">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.524" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.524" x2="2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="1.27" x2="-1.905" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-1.27" x2="-1.905" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="1.905" y1="-1.524" x2="2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.159" y1="-1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="-1.27" x2="-2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.524" x2="1.905" y2="1.524" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.524" y1="0" x2="2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0" x2="-2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.1524" layer="51" curve="-180"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="51" curve="180"/>
<rectangle x1="0.762" y1="-0.762" x2="1.016" y2="0.762" layer="51"/>
<rectangle x1="1.016" y1="-0.635" x2="1.27" y2="0.635" layer="51"/>
<rectangle x1="1.27" y1="-0.508" x2="1.397" y2="0.508" layer="51"/>
<rectangle x1="1.397" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.016" y1="-0.762" x2="-0.762" y2="0.762" layer="51"/>
<rectangle x1="-1.27" y1="-0.635" x2="-1.016" y2="0.635" layer="51"/>
<rectangle x1="-1.397" y1="-0.508" x2="-1.27" y2="0.508" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.397" y2="0.254" layer="51"/>
<rectangle x1="0.9652" y1="-0.7112" x2="1.0922" y2="-0.5842" layer="51"/>
<rectangle x1="1.3462" y1="-0.3556" x2="1.4732" y2="-0.2286" layer="51"/>
<rectangle x1="1.3462" y1="0.2032" x2="1.4732" y2="0.3302" layer="51"/>
<rectangle x1="0.9652" y1="0.5842" x2="1.0922" y2="0.7112" layer="51"/>
<rectangle x1="-1.0922" y1="-0.7112" x2="-0.9652" y2="-0.5842" layer="51"/>
<rectangle x1="-1.4478" y1="-0.3302" x2="-1.3208" y2="-0.2032" layer="51"/>
<rectangle x1="-1.4732" y1="0.2032" x2="-1.3462" y2="0.3302" layer="51"/>
<rectangle x1="-1.1176" y1="0.5842" x2="-0.9906" y2="0.7112" layer="51"/>
<smd name="1" x="-1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="1.27" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="-2.159" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1" y="0" size="0.02" layer="27">&gt;VALUE</text>
</package>
<package name="DISCLAIMER">
<text x="0" y="18.161" size="1.27" layer="1" ratio="10">Reference Designs ARE PROVIDED "AS IS" AND "WITH ALL FAULTS. Arduino DISCLAIMS ALL OTHER WARRANTIES, EXPRESS OR IMPLIED,</text>
<text x="0" y="16.129" size="1.27" layer="1" ratio="10">REGARDING PRODUCTS, INCLUDING BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE</text>
<text x="0" y="13.081" size="1.27" layer="1" ratio="10">Arduino may make changes to specifications and product descriptions at any time, without notice. The Customer must not</text>
<text x="0" y="11.049" size="1.27" layer="1" ratio="10">rely on the absence or characteristics of any features or instructions marked "reserved" or "undefined." Arduino reserves</text>
<text x="0" y="9.017" size="1.27" layer="1" ratio="10">these for future definition and shall have no responsibility whatsoever for conflicts or incompatibilities arising from future changes to them.</text>
<text x="0" y="5.969" size="1.27" layer="1" ratio="10">The product information on the Web Site or Materials is subject to change without notice. Do not finalize a design with this information. </text>
<text x="0" y="2.921" size="1.4224" layer="1">“Arduino” name and logo are trademarks registered by Arduino S.r.l. in Italy, in the European Union and in other countries of the world.</text>
</package>
</packages>
<symbols>
<symbol name="J-US">
<wire x1="-2.54" y1="2.54" x2="-2.159" y2="3.556" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="3.556" x2="-1.524" y2="1.524" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="1.524" x2="-0.889" y2="3.556" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="3.556" x2="-0.254" y2="1.524" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="1.524" x2="0.381" y2="3.556" width="0.2032" layer="94"/>
<wire x1="0.381" y1="3.556" x2="1.016" y2="1.524" width="0.2032" layer="94"/>
<wire x1="1.016" y1="1.524" x2="1.651" y2="3.556" width="0.2032" layer="94"/>
<wire x1="1.651" y1="3.556" x2="2.286" y2="1.524" width="0.2032" layer="94"/>
<wire x1="2.286" y1="1.524" x2="2.54" y2="2.54" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-3.556" x2="2.54" y2="-2.54" width="0.2032" layer="94"/>
<wire x1="1.651" y1="-1.524" x2="2.286" y2="-3.556" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-3.556" x2="1.651" y2="-1.524" width="0.2032" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="1.016" y2="-3.556" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-3.556" x2="0.381" y2="-1.524" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="-1.524" x2="-0.254" y2="-3.556" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-3.556" x2="-0.889" y2="-1.524" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="-1.524" x2="-1.524" y2="-3.556" width="0.2032" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.159" y2="-1.524" width="0.2032" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<pin name="1" x="5.08" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="C" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<text x="-5.08" y="3.81" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-5.08" y="-5.715" size="1.778" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="SJ">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="FRAME_C_L">
<frame x1="0" y1="0" x2="558.8" y2="431.8" columns="11" rows="9" layer="94" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
<symbol name="DISCLAIMER">
<text x="0" y="33.02" size="3.048" layer="91">Reference Designs ARE PROVIDED "AS IS" AND "WITH ALL FAULTS". Arduino DISCLAIMS ALL OTHER WARRANTIES, EXPRESS OR IMPLIED,</text>
<text x="0" y="22.86" size="3.048" layer="91">Arduino may make changes to specifications and product descriptions at any time, without notice. The Customer must not</text>
<text x="0" y="27.94" size="3.048" layer="91">REGARDING PRODUCTS, INCLUDING BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE</text>
<text x="0" y="17.78" size="3.048" layer="91">rely on the absence or characteristics of any features or instructions marked "reserved" or "undefined." Arduino reserves</text>
<text x="0" y="12.7" size="3.048" layer="91">these for future definition and shall have no responsibility whatsoever for conflicts or incompatibilities arising from future changes to them.</text>
<text x="0" y="7.62" size="3.048" layer="91">The product information on the Web Site or Materials is subject to change without notice. Do not finalize a design with this information. </text>
<text x="0" y="0" size="3.81" layer="91">"Arduino" name and logo are trademarks registered by Arduino S.r.l. in Italy, in the European Union and in other countries of the world.</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="J" prefix="R">
<description>Three terminal jumper made from two 0402, 0603, 0805, etc.</description>
<gates>
<gate name="G$1" symbol="J-US" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="J0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SJ" prefix="SJ" uservalue="yes">
<description>SMD solder &lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="SJ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJ">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="W" package="SJW">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FRAME_C_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt; C Size , 17 x 22 INCH, Landscape&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="FRAME_C_L" x="0" y="0" addlevel="always"/>
<gate name="G$2" symbol="DOCFIELD" x="452.12" y="0" addlevel="always"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DISCLAIMER">
<gates>
<gate name="G$1" symbol="DISCLAIMER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DISCLAIMER">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diodi">
<packages>
<package name="MINIMELF">
<description>&lt;b&gt;Mini Melf Diode&lt;/b&gt;</description>
<wire x1="1.3208" y1="0.7874" x2="-1.3208" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.7874" x2="-1.3208" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="0.5" y1="0.5" x2="-0.5" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="-0.5" width="0.2032" layer="21"/>
<wire x1="0.5" y1="-0.5" x2="0.5" y2="0.5" width="0.2032" layer="21"/>
<rectangle x1="-1.8542" y1="-0.8636" x2="-1.2954" y2="0.8636" layer="51"/>
<rectangle x1="1.2954" y1="-0.8636" x2="1.8542" y2="0.8636" layer="51"/>
<rectangle x1="-0.8636" y1="-0.7874" x2="-0.254" y2="0.7874" layer="21"/>
<smd name="A" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="C" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.54" y1="1.0414" x2="2.54" y2="1.0414" width="0.0508" layer="39"/>
<wire x1="2.54" y1="1.0414" x2="2.54" y2="-1.0414" width="0.0508" layer="39"/>
<wire x1="2.54" y1="-1.0414" x2="-2.54" y2="-1.0414" width="0.0508" layer="39"/>
<wire x1="-2.54" y1="-1.0414" x2="-2.54" y2="1.0414" width="0.0508" layer="39"/>
</package>
<package name="SMB">
<description>&lt;B&gt;DIODE&lt;/B&gt;</description>
<wire x1="-2.2606" y1="1.905" x2="2.2606" y2="1.905" width="0.1016" layer="21"/>
<wire x1="-2.2606" y1="-1.905" x2="2.2606" y2="-1.905" width="0.1016" layer="21"/>
<wire x1="-2.2606" y1="-1.905" x2="-2.2606" y2="1.905" width="0.1016" layer="51"/>
<wire x1="2.2606" y1="-1.905" x2="2.2606" y2="1.905" width="0.1016" layer="51"/>
<wire x1="0.193" y1="1" x2="-0.83" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.83" y1="0" x2="0.193" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.193" y1="-1" x2="0.193" y2="1" width="0.2032" layer="21"/>
<rectangle x1="-2.794" y1="-1.0922" x2="-2.2606" y2="1.0922" layer="51"/>
<rectangle x1="2.2606" y1="-1.0922" x2="2.794" y2="1.0922" layer="51"/>
<rectangle x1="-1.35" y1="-1.9" x2="-0.8" y2="1.9" layer="51"/>
<smd name="A" x="2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<smd name="C" x="-2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<text x="-2.159" y="2.159" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.159" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-3.6195" y1="2.032" x2="-3.6195" y2="-2.032" width="0.0508" layer="39"/>
<wire x1="-3.6195" y1="-2.032" x2="3.6195" y2="-2.032" width="0.0508" layer="39"/>
<wire x1="3.6195" y1="-2.032" x2="3.6195" y2="2.032" width="0.0508" layer="39"/>
<wire x1="3.6195" y1="2.032" x2="-3.6195" y2="2.032" width="0.0508" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0012_CD1206-S01575" prefix="D" uservalue="yes">
<description>small-signal high-speed Switching Diodes   CD1206</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MINIMELF">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="1206"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="CD1206-S01575"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Diodi"/>
<attribute name="VALUE" value="CD1206-S01575"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0020_M7" prefix="D" uservalue="yes">
<description>1.0A 40V SURFACE MOUNT GLASS PASSIVATED RECTIFIER   M7</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMB">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="SMA"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="M7"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Diodi"/>
<attribute name="VALUE" value="M7"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Opto">
<packages>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<wire x1="-0.74295" y1="1.78435" x2="0.7366" y2="1.78435" width="0.0508" layer="39"/>
<wire x1="0.7366" y1="1.78435" x2="0.7366" y2="-1.78435" width="0.0508" layer="39"/>
<wire x1="0.7366" y1="-1.78435" x2="-0.74295" y2="-1.78435" width="0.0508" layer="39"/>
<wire x1="-0.74295" y1="-1.78435" x2="-0.74295" y2="1.78435" width="0.0508" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="0044_KPT-2012YC" prefix="LED" uservalue="yes">
<description>LED YELLOW - SMD - 0805</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="KPT-2012YC"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Opto"/>
<attribute name="VALUE" value="Yellow"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0043_KPT-2012SGC" prefix="LED" uservalue="yes">
<description>LED GREEN - SMD-0805</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="KPT-2012SGC"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Opto"/>
<attribute name="VALUE" value="Green"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Transistors">
<packages>
<package name="SOT-23">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="-1.4224" y1="0.381" x2="1.4732" y2="0.381" width="0.1524" layer="21"/>
<wire x1="1.4732" y1="0.381" x2="1.4732" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="1.4732" y1="-0.381" x2="-1.4224" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="-0.381" x2="-1.4224" y2="0.381" width="0.1524" layer="21"/>
<rectangle x1="0.7874" y1="0.4318" x2="1.1684" y2="0.9398" layer="51"/>
<rectangle x1="-1.143" y1="0.4318" x2="-0.762" y2="0.9398" layer="51"/>
<rectangle x1="-0.1778" y1="-0.9398" x2="0.2032" y2="-0.4318" layer="51"/>
<smd name="1" x="0.0254" y="-1.016" dx="0.7874" dy="0.889" layer="1"/>
<smd name="2" x="-0.9398" y="1.016" dx="0.7874" dy="0.889" layer="1"/>
<smd name="3" x="0.9906" y="1.016" dx="0.7874" dy="0.889" layer="1"/>
<text x="-1.397" y="1.778" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.397" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-1.524" y1="1.6002" x2="1.58115" y2="1.6002" width="0.0508" layer="39"/>
<wire x1="1.58115" y1="1.6002" x2="1.58115" y2="-0.4826" width="0.0508" layer="39"/>
<wire x1="1.58115" y1="-0.4826" x2="0.55245" y2="-0.4826" width="0.0508" layer="39"/>
<wire x1="0.55245" y1="-0.4826" x2="0.55245" y2="-1.59385" width="0.0508" layer="39"/>
<wire x1="0.55245" y1="-1.59385" x2="-0.50165" y2="-1.59385" width="0.0508" layer="39"/>
<wire x1="-0.50165" y1="-1.59385" x2="-0.50165" y2="-0.4826" width="0.0508" layer="39"/>
<wire x1="-0.50165" y1="-0.4826" x2="-1.524" y2="-0.4826" width="0.0508" layer="39"/>
<wire x1="-1.524" y1="-0.4826" x2="-1.524" y2="1.6002" width="0.0508" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="P_MOSFET">
<rectangle x1="-2.032" y1="1.397" x2="-1.524" y2="2.921" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<rectangle x1="-2.032" y1="-2.921" x2="-1.524" y2="-1.397" layer="94"/>
<rectangle x1="0.762" y1="-0.635" x2="2.032" y2="-0.508" layer="94"/>
<wire x1="-1.651" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.159" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-0.508" x2="1.397" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="-3.048" x2="0" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.159" x2="0" y2="2.159" width="0.1524" layer="94"/>
<wire x1="0" y1="2.159" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="3.048" x2="1.397" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.397" y1="3.048" x2="1.397" y2="0.381" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.159" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<polygon width="0.1016" layer="94">
<vertex x="-0.127" y="0"/>
<vertex x="-1.143" y="-0.635"/>
<vertex x="-1.143" y="0.635"/>
</polygon>
<polygon width="0.1016" layer="94">
<vertex x="1.397" y="-0.508"/>
<vertex x="0.762" y="0.508"/>
<vertex x="2.032" y="0.508"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="0297_PMV48XP" prefix="TR" uservalue="yes">
<description>20 V, 3.5 A P-channel Trench MOSFET - PMV48XP</description>
<gates>
<gate name="A" symbol="P_MOSFET" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="SOT-23">
<connects>
<connect gate="A" pin="D" pad="1"/>
<connect gate="A" pin="G" pad="3"/>
<connect gate="A" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value="SOT23"/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="PMV48XP"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Transistor"/>
<attribute name="VALUE" value="PMV48XP"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Switches">
<packages>
<package name="TS42">
<circle x="0" y="0" radius="1.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.2" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="-2.85" y1="3" x2="-2.85" y2="0.5" width="0.127" layer="21"/>
<wire x1="-2.85" y1="-0.5" x2="-2.85" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.85" y1="0.5" x2="-3.1" y2="0.5" width="0.127" layer="21"/>
<wire x1="-3.1" y1="0.5" x2="-3.1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-0.5" x2="-2.85" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-1" y1="2.6" x2="1" y2="2.6" width="0.127" layer="21"/>
<wire x1="1" y1="2.6" x2="1" y2="3" width="0.127" layer="21"/>
<wire x1="1" y1="3" x2="-1" y2="3" width="0.127" layer="21"/>
<wire x1="-1" y1="3" x2="-1" y2="2.6" width="0.127" layer="21"/>
<wire x1="-1" y1="-3" x2="1" y2="-3" width="0.127" layer="21"/>
<wire x1="1" y1="-3" x2="1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="1" y1="-2.6" x2="-1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-1" y1="-2.6" x2="-1" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.85" y1="3" x2="-1" y2="3" width="0.127" layer="21"/>
<wire x1="1" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="1" y1="-3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="-1" y1="-3" x2="-2.85" y2="-3" width="0.127" layer="21"/>
<rectangle x1="-4.699" y1="-0.381" x2="-3.81" y2="0.381" layer="31"/>
<smd name="1" x="-4.2" y="2.25" dx="1.6" dy="1.4" layer="1" roundness="15"/>
<smd name="2" x="4.2" y="2.25" dx="1.6" dy="1.4" layer="1" roundness="15"/>
<smd name="3" x="-4.2" y="-2.25" dx="1.6" dy="1.4" layer="1" roundness="15"/>
<smd name="4" x="4.2" y="-2.25" dx="1.6" dy="1.4" layer="1" roundness="15"/>
<smd name="5" x="-4.2" y="0" dx="1.6" dy="1.4" layer="1" roundness="15" cream="no"/>
<text x="-3" y="4" size="1.27" layer="21">&gt;VALUE</text>
<text x="-3" y="-4.5" size="1.27" layer="21">&gt;NAME</text>
<wire x1="-5.2" y1="3.15" x2="-5.2" y2="-3.14" width="0.127" layer="39"/>
<wire x1="-5.2" y1="-3.14" x2="5.18" y2="-3.14" width="0.127" layer="39"/>
<wire x1="5.18" y1="-3.14" x2="5.18" y2="-1.36" width="0.127" layer="39"/>
<wire x1="5.18" y1="-1.36" x2="3.15" y2="-1.36" width="0.127" layer="39"/>
<wire x1="3.15" y1="-1.36" x2="3.15" y2="1.38" width="0.127" layer="39"/>
<wire x1="3.15" y1="1.38" x2="5.19" y2="1.38" width="0.127" layer="39"/>
<wire x1="5.19" y1="1.38" x2="5.19" y2="3.15" width="0.127" layer="39"/>
<wire x1="5.19" y1="3.15" x2="-5.2" y2="3.15" width="0.127" layer="39"/>
</package>
</packages>
<symbols>
<symbol name="ST-MOD_V1.4_TS42">
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.1524" layer="94"/>
<pin name="1" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="2" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="3" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="4" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="5-GND" x="5.08" y="-5.08" visible="pad" length="short" direction="pwr" rot="R90"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="0946_TS42031-160W-TR-7260" prefix="PB" uservalue="yes">
<description>Pushbutton 6x6 Smd - White</description>
<gates>
<gate name="G$1" symbol="ST-MOD_V1.4_TS42" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TS42">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5-GND" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="CASE" value=""/>
<attribute name="MONTHLY_USAGE" value="_____"/>
<attribute name="PART_NUMBER" value="IT-1157AHNP-160G-G_GTR"/>
<attribute name="PRICE" value="_____"/>
<attribute name="SUPPLIER" value="_____"/>
<attribute name="TYPE" value="Switches"/>
<attribute name="VALUE" value="TS42"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.2032" drill="0.4064">
<clearance class="0" value="0.1778"/>
</class>
<class number="1" name="power" width="0.3048" drill="0.4064">
<clearance class="1" value="0.1778"/>
</class>
<class number="2" name="gnd" width="0.3048" drill="0.4064">
<clearance class="2" value="0.1778"/>
</class>
<class number="3" name="power usb" width="0.508" drill="0.4064">
<clearance class="3" value="0.1778"/>
</class>
</classes>
<parts>
<part name="ICSP" library="Connettori" deviceset="0114_PH254-203DF118A00V" device="" value="ICSP"/>
<part name="P+1" library="supply1" deviceset="+5V" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="PWML" library="Connettori" deviceset="1218_FH254-108DF08500T20" device="" value="FH254-108DF08500T20"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="P+4" library="supply1" deviceset="+5V" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="PC1" library="Condensatori" deviceset="0167_" device="" value="22uF"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="ON" library="Opto" deviceset="0043_KPT-2012SGC" device="" value="Green"/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="P+5" library="supply1" deviceset="+5V" device=""/>
<part name="D2" library="Diodi" deviceset="0020_M7" device="" value="M7"/>
<part name="X1" library="Connettori" deviceset="0018_19746-POWER-SUPPLY" device="" value="POWER-SUPPLY"/>
<part name="FID2" library="SmartPrj" deviceset="FIDUCIALMOUNT" device=""/>
<part name="FID1" library="SmartPrj" deviceset="FIDUCIALMOUNT" device=""/>
<part name="FID4" library="SmartPrj" deviceset="FIDUCIALMOUNT" device=""/>
<part name="C6" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="C9" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="C3" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="ADCL" library="Connettori" deviceset="0058_FH254-108DF08500V" device="" value="FH254-108DF08500V"/>
<part name="COMMUNICATION" library="Connettori" deviceset="0058_FH254-108DF08500V" device="" value="FH254-108DF08500V"/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="P+8" library="supply1" deviceset="+5V" device=""/>
<part name="IC3" library="IntegratedCircuits" deviceset="0023_ATMEGA2560-16AU" device="" value="ATMEGA2560-15AU"/>
<part name="C8" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="C7" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="ADCH" library="Connettori" deviceset="0058_FH254-108DF08500V" device="" value="FH254-108DF08500V"/>
<part name="C1" library="Condensatori" deviceset="0014_22PF_0603" device="" value="22p"/>
<part name="P+10" library="supply1" deviceset="+5V" device=""/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="C11" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="C23" library="Condensatori" deviceset="0006_1UF_0603/GRM188R61E105KA12D" device="" value="1uF"/>
<part name="RX" library="Opto" deviceset="0044_KPT-2012YC" device="" value="Yellow"/>
<part name="TX" library="Opto" deviceset="0044_KPT-2012YC" device="" value="Yellow"/>
<part name="X2" library="Connettori" deviceset="0056_USB-B-S-RA-WT-SPCC" device=""/>
<part name="F1" library="Protezione" deviceset="0002_MF-MSMF050-2" device="" value="MF-MSMF050-2 500mA"/>
<part name="P+13" library="supply1" deviceset="+5V" device=""/>
<part name="C15" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="L" library="Opto" deviceset="0044_KPT-2012YC" device="" value="Yellow"/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="C10" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="T1" library="Transistors" deviceset="0297_PMV48XP" device="" value="PMV48XP"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="C22" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="C21" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="P+11" library="supply1" deviceset="+5V" device=""/>
<part name="RESET-EN" library="jumper" deviceset="SJ" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="C16" library="Condensatori" deviceset="0006_1UF_0603/GRM188R61E105KA12D" device="" value="1uF"/>
<part name="P+9" library="supply1" deviceset="+5V" device=""/>
<part name="IC4" library="IntegratedCircuits" deviceset="0007_ATMEGA16U2-MUR" device="" value="ATMEGA16U2-MU"/>
<part name="ICSP1" library="Connettori" deviceset="0114_PH254-203DF118A00V" device="" value="ICSP"/>
<part name="P+14" library="supply1" deviceset="+5V" device=""/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="L2" library="Protezione" deviceset="0001_BLM21PG300SN1D_/_MH2029-300Y" device="" value="MH2029-300Y"/>
<part name="Z1" library="Protezione" deviceset="0010_BRNCG0603MLC-05E" device="" value="BRNCG0603MLC-05E"/>
<part name="Z2" library="Protezione" deviceset="0010_BRNCG0603MLC-05E" device="" value="BRNCG0603MLC-05E"/>
<part name="GROUND" library="Misc" deviceset="SJ" device=""/>
<part name="Y1" library="Cristalli" deviceset="0011_CSTCE16M0V53-R0" device="" value="CSTCE16M0V53-R0"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="RN5" library="Resistenze" deviceset="0005_064R_1K_/_CAY16-102J4LF" device="" value="1K"/>
<part name="RN3" library="Resistenze" deviceset="0016_064R_10K_/_CAY16-103J4LF" device="" value="10k"/>
<part name="RN1" library="Resistenze" deviceset="0016_064R_10K_/_CAY16-103J4LF" device="" value="10k"/>
<part name="RN2" library="Resistenze" deviceset="0005_064R_1K_/_CAY16-102J4LF" device="" value="1K"/>
<part name="FID3" library="SmartPrj" deviceset="FIDUCIALMOUNT" device=""/>
<part name="RN4" library="Resistenze" deviceset="0003_064R_22R_/_CAY16-220J4LF" device="" value="22R"/>
<part name="RESET" library="Switches" deviceset="0946_TS42031-160W-TR-7260" device="" value="TS42"/>
<part name="IC7" library="IntegratedCircuits" deviceset="0008_TEXLP2985-33DBVR" device="" value="LP2985-33DBVR"/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="C13" library="Condensatori" deviceset="0014_22PF_0603" device="" value="22p"/>
<part name="C12" library="Condensatori" deviceset="0014_22PF_0603" device="" value="22p"/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="IC1" library="IntegratedCircuits" deviceset="0021_LMV358IDGKR" device="" value="LMV358IDGKR"/>
<part name="U7A" library="IntegratedCircuits" deviceset="0052_MAX3421EEHJ" device="" value="MAX3421EEHJ"/>
<part name="P+12" library="supply1" deviceset="+5V" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="RN7" library="Resistenze" deviceset="0003_064R_22R_/_CAY16-220J4LF" device="" value="22R"/>
<part name="F2" library="Protezione" deviceset="0002_MF-MSMF050-2" device="" value="MF-MSMF050-2 500mA"/>
<part name="L3" library="Protezione" deviceset="0001_BLM21PG300SN1D_/_MH2029-300Y" device="" value="MH2029-300Y"/>
<part name="C17" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="C24" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="C18" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="C19" library="Condensatori" deviceset="0014_22PF_0603" device="" value="22p"/>
<part name="C20" library="Condensatori" deviceset="0014_22PF_0603" device="" value="22p"/>
<part name="Z3" library="Protezione" deviceset="0010_BRNCG0603MLC-05E" device="" value="BRNCG0603MLC-05E"/>
<part name="Z4" library="Protezione" deviceset="0010_BRNCG0603MLC-05E" device="" value="BRNCG0603MLC-05E"/>
<part name="SJ3" library="Misc" deviceset="J" device="0402" value="NM"/>
<part name="SJ1" library="bibaja" deviceset="J" device="0402" value="NM"/>
<part name="IC5" library="IntegratedCircuits" deviceset="0040_SN74LVC1G125DCKR" device="" value="74LVC1G125DCKR"/>
<part name="IC6" library="IntegratedCircuits" deviceset="0040_SN74LVC1G125DCKR" device="" value="74LVC1G125DCKR"/>
<part name="IC8" library="IntegratedCircuits" deviceset="0040_SN74LVC1G125DCKR" device="" value="74LVC1G125DCKR"/>
<part name="IC9" library="IntegratedCircuits" deviceset="0040_SN74LVC1G125DCKR" device="" value="74LVC1G125DCKR"/>
<part name="RN6" library="Resistenze" deviceset="0016_064R_10K_/_CAY16-103J4LF" device="" value="10k"/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="P+15" library="supply1" deviceset="+5V" device=""/>
<part name="SJ2" library="Misc" deviceset="J" device="0402" value="NM"/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="C14" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="C25" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="+3V4" library="supply1" deviceset="+3V3" device=""/>
<part name="C26" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="P+16" library="supply1" deviceset="+5V" device=""/>
<part name="Y2" library="Cristalli" deviceset="0395_16.000MHZ__12PF" device="" value="16Mhz-KX-7"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="Y3" library="Cristalli" deviceset="0396_12MHZ-12PF-K7" device="" value="12mHz-12pF-K7"/>
<part name="R5" library="Resistenze" deviceset="0039_JUMP_0402" device="" value="0R"/>
<part name="R6" library="Resistenze" deviceset="0039_JUMP_0402" device="" value="0R"/>
<part name="R9" library="Resistenze" deviceset="0039_JUMP_0402" device="" value="0R"/>
<part name="XIO" library="Connettori" deviceset="0060_FH254-218DF08500V" device="" value="18x2F-H8.5"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="P+7" library="supply1" deviceset="+5V" device=""/>
<part name="D4" library="Diodi" deviceset="0012_CD1206-S01575" device="" value="CD1206-S01575"/>
<part name="D5" library="Diodi" deviceset="0012_CD1206-S01575" device="" value="CD1206-S01575"/>
<part name="JP1" library="Connettori" deviceset="1216_FH254-110DF08500T30" device="" value="FH254-110DF08500T30"/>
<part name="POWER" library="Connettori" deviceset="1217_FH254-108DF08500T21" device="" value="FH254-108DF08500T21"/>
<part name="X3" library="Connettori" deviceset="0054_USB-A-S-RA" device="" value="USB-A-THT"/>
<part name="L4" library="Protezione" deviceset="0001_BLM21PG300SN1D_/_MH2029-300Y" device="" value="MH2029-300Y"/>
<part name="C27" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="C49" library="Condensatori" deviceset="0167_" device="" value="22uF"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="PC2" library="Condensatori" deviceset="0167_" device="" value="22uF"/>
<part name="C50" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="GND48" library="supply1" deviceset="GND" device=""/>
<part name="IC2" library="IntegratedCircuits" deviceset="2093_MPM3610GQV-P" device="" value="MPM3610"/>
<part name="R28" library="Resistenze" deviceset="2022_CR0603FW104L" device="" value="100K_1%"/>
<part name="R29" library="Resistenze" deviceset="2339_" device="" value="19K1_0,1%"/>
<part name="GND44" library="supply1" deviceset="GND" device=""/>
<part name="R30" library="Resistenze" deviceset="0026_100K_0603" device="" value="100k"/>
<part name="R31" library="Resistenze" deviceset="0411_56K_0603" device="" value="56k"/>
<part name="GND50" library="supply1" deviceset="GND" device=""/>
<part name="C41" library="Condensatori" deviceset="0015_100NF_0603" device="" value="100nF"/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="R1" library="Resistenze" deviceset="0028_CR0603JW103L" device="" value="10k"/>
<part name="FRAME1" library="Misc" deviceset="FRAME_C_L" device=""/>
<part name="U$1" library="Misc" deviceset="DISCLAIMER" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="236.22" y="27.94" size="1.778" layer="91">0</text>
<text x="236.22" y="30.48" size="1.778" layer="91">1</text>
<text x="236.22" y="33.02" size="1.778" layer="91">2</text>
<text x="236.22" y="35.56" size="1.778" layer="91">3</text>
<text x="236.22" y="38.1" size="1.778" layer="91">4</text>
<text x="236.22" y="40.64" size="1.778" layer="91">5</text>
<text x="236.22" y="43.18" size="1.778" layer="91">6</text>
<text x="236.22" y="45.72" size="1.778" layer="91">7</text>
<text x="238.125" y="95.885" size="1.778" layer="91">8</text>
<text x="238.125" y="98.425" size="1.778" layer="91">9</text>
<text x="238.125" y="100.965" size="1.778" layer="91">10</text>
<text x="238.125" y="103.505" size="1.778" layer="91">11</text>
<text x="238.125" y="106.045" size="1.778" layer="91">12</text>
<text x="238.125" y="108.585" size="1.778" layer="91">13</text>
<text x="234.95" y="66.675" size="1.778" layer="91">15</text>
<text x="234.95" y="64.135" size="1.778" layer="91">16</text>
<text x="234.95" y="61.595" size="1.778" layer="91">17</text>
<text x="234.95" y="59.055" size="1.778" layer="91">18</text>
<text x="234.95" y="56.515" size="1.778" layer="91">19</text>
<text x="234.95" y="53.975" size="1.778" layer="91">20</text>
<text x="234.95" y="51.435" size="1.778" layer="91">21</text>
<text x="234.95" y="69.215" size="1.778" layer="91">14</text>
<text x="254" y="152.4" size="1.6764" layer="91">(SCK)</text>
<text x="254" y="154.94" size="1.6764" layer="91">(MISO)</text>
<text x="299.72" y="152.4" size="1.6764" layer="91">(MOSI)</text>
<text x="193.04" y="114.3" size="1.6764" layer="91">pwm</text>
<text x="193.04" y="111.76" size="1.6764" layer="91">pwm</text>
<text x="193.04" y="109.22" size="1.6764" layer="91">pwm</text>
<text x="193.04" y="106.68" size="1.6764" layer="91">pwm</text>
<text x="185.42" y="40.64" size="1.6764" layer="91">pwm</text>
<text x="185.42" y="38.1" size="1.6764" layer="91">pwm</text>
<text x="185.42" y="35.56" size="1.6764" layer="91">pwm</text>
<text x="184.785" y="1.27" size="1.6764" layer="91">pwm</text>
<text x="85.09" y="3.81" size="1.6764" layer="91">pwm</text>
<text x="85.09" y="1.27" size="1.6764" layer="91">pwm</text>
<text x="85.09" y="-1.27" size="1.6764" layer="91">pwm</text>
<text x="85.09" y="-3.81" size="1.6764" layer="91">pwm</text>
<text x="93.98" y="68.58" size="1.6764" layer="91">pwm</text>
<text x="93.98" y="66.04" size="1.6764" layer="91">pwm</text>
<text x="93.98" y="63.5" size="1.6764" layer="91">pwm</text>
<text x="185.42" y="30.48" size="1.6764" layer="91">(TX0)</text>
<text x="185.42" y="27.94" size="1.6764" layer="91">(RX0)</text>
<text x="219.71" y="46.99" size="1.6764" layer="91">pwm</text>
<text x="219.71" y="44.45" size="1.6764" layer="91">pwm</text>
<text x="219.71" y="34.29" size="1.6764" layer="91">pwm</text>
<text x="220.98" y="99.06" size="1.6764" layer="91">pwm</text>
<text x="220.98" y="96.52" size="1.6764" layer="91">pwm</text>
<text x="189.23" y="104.775" size="1.6764" layer="91">(MISO)</text>
<text x="189.23" y="99.695" size="1.6764" layer="91">(SCK)</text>
<text x="189.23" y="102.235" size="1.6764" layer="91">(MOSI)</text>
<text x="219.71" y="41.91" size="1.6764" layer="91">pwm</text>
<text x="219.71" y="39.37" size="1.6764" layer="91">pwm</text>
<text x="219.71" y="36.83" size="1.6764" layer="91">pwm</text>
<text x="219.71" y="31.75" size="1.6764" layer="91">pwm</text>
<text x="219.71" y="29.21" size="1.6764" layer="91">pwm</text>
<text x="500.38" y="48.26" size="1.778" layer="91" rot="R90">USB boot En</text>
<text x="134.62" y="185.42" size="2.54" layer="91" ratio="20">TM</text>
<text x="99.06" y="25.4" size="1.778" layer="91">int</text>
<text x="99.06" y="5.08" size="1.778" layer="91">csa</text>
<text x="530.86" y="-7.62" size="1.016" layer="94">STUFF FOR PJ2</text>
<text x="515.62" y="-127" size="1.016" layer="94">STUFF FOR PJ3</text>
<text x="500.38" y="-142.24" size="1.016" layer="94">STUFF FOR PE6</text>
<text x="189.23" y="97.155" size="1.6764" layer="91">(SS)</text>
<text x="515.62" y="-124.46" size="1.778" layer="91">era PH5</text>
<text x="152.4" y="-129.54" size="7.62" layer="91">ARDUINO MEGA2560 ADK (for Android)</text>
<text x="39.37" y="-55.245" size="1.778" layer="91">51</text>
<text x="92.71" y="-52.705" size="1.778" layer="91">52</text>
<text x="39.37" y="-52.705" size="1.778" layer="91">53</text>
<text x="50.8" y="-53.34" size="1.6764" layer="91">(SS)</text>
<text x="50.8" y="-55.88" size="1.6764" layer="91">(MOSI)</text>
<text x="76.2" y="-53.34" size="1.6764" layer="91">(SCK)</text>
<text x="76.2" y="-55.88" size="1.6764" layer="91">(MISO)</text>
<text x="39.37" y="-57.785" size="1.778" layer="91">49</text>
<text x="39.37" y="-60.325" size="1.778" layer="91">47</text>
<text x="39.37" y="-62.865" size="1.778" layer="91">45</text>
<text x="39.37" y="-65.405" size="1.778" layer="91">43</text>
<text x="39.37" y="-67.945" size="1.778" layer="91">41</text>
<text x="39.37" y="-70.485" size="1.778" layer="91">39</text>
<text x="92.71" y="-55.245" size="1.778" layer="91">50</text>
<text x="92.71" y="-57.785" size="1.778" layer="91">48</text>
<text x="92.71" y="-60.325" size="1.778" layer="91">46</text>
<text x="92.71" y="-62.865" size="1.778" layer="91">44</text>
<text x="92.71" y="-65.405" size="1.778" layer="91">42</text>
<text x="92.71" y="-67.945" size="1.778" layer="91">40</text>
<text x="92.71" y="-70.485" size="1.778" layer="91">38</text>
<text x="92.075" y="-90.805" size="1.778" layer="91">22</text>
<text x="45.085" y="-90.805" size="1.778" layer="91">23</text>
<text x="92.075" y="-88.265" size="1.778" layer="91">24</text>
<text x="45.085" y="-88.265" size="1.778" layer="91">25</text>
<text x="92.075" y="-85.725" size="1.778" layer="91">26</text>
<text x="45.085" y="-85.725" size="1.778" layer="91">27</text>
<text x="92.075" y="-83.185" size="1.778" layer="91">28</text>
<text x="45.085" y="-83.185" size="1.778" layer="91">29</text>
<text x="92.075" y="-80.645" size="1.778" layer="91">30</text>
<text x="92.075" y="-78.105" size="1.778" layer="91">32</text>
<text x="92.075" y="-75.565" size="1.778" layer="91">34</text>
<text x="92.075" y="-73.025" size="1.778" layer="91">36</text>
<text x="45.085" y="-80.645" size="1.778" layer="91">31</text>
<text x="45.085" y="-78.105" size="1.778" layer="91">33</text>
<text x="45.085" y="-75.565" size="1.778" layer="91">35</text>
<text x="45.085" y="-73.025" size="1.778" layer="91">37</text>
<text x="60.96" y="12.7" size="1.778" layer="91">IOREF</text>
<text x="459.74" y="-190.5" size="3.81" layer="94" font="vector">Author: G.Martino</text>
</plain>
<instances>
<instance part="ICSP" gate="A" x="281.94" y="152.4"/>
<instance part="P+1" gate="1" x="299.72" y="160.02" smashed="yes">
<attribute name="VALUE" x="301.625" y="162.56" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="299.72" y="147.32"/>
<instance part="P+3" gate="1" x="86.36" y="116.84" smashed="yes">
<attribute name="VALUE" x="88.265" y="119.38" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND2" gate="1" x="86.36" y="88.9" smashed="yes">
<attribute name="VALUE" x="83.82" y="86.36" size="1.778" layer="96"/>
</instance>
<instance part="PWML" gate="A" x="233.68" y="38.1" smashed="yes" rot="MR180">
<attribute name="NAME" x="243.205" y="37.465" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="227.33" y="50.8" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND5" gate="1" x="35.56" y="-5.08"/>
<instance part="P+4" gate="1" x="35.56" y="17.78" smashed="yes">
<attribute name="VALUE" x="37.465" y="20.32" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND7" gate="1" x="170.18" y="-99.06"/>
<instance part="PC1" gate="G$1" x="160.02" y="-91.44" smashed="yes">
<attribute name="NAME" x="154.559" y="-91.9734" size="1.778" layer="95"/>
<attribute name="VALUE" x="152.146" y="-94.5134" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="160.02" y="-99.06"/>
<instance part="GND12" gate="1" x="60.96" y="-27.94" rot="R90"/>
<instance part="GND19" gate="1" x="119.38" y="-101.6"/>
<instance part="ON" gate="G$1" x="210.82" y="160.02" rot="R90"/>
<instance part="GND14" gate="1" x="220.98" y="160.02" rot="R90"/>
<instance part="P+5" gate="1" x="175.26" y="170.18" smashed="yes">
<attribute name="VALUE" x="177.165" y="172.72" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D2" gate="G$1" x="149.86" y="-99.06" rot="R90"/>
<instance part="X1" gate="G$1" x="124.46" y="-81.28"/>
<instance part="FID2" gate="G$1" x="121.92" y="157.48"/>
<instance part="FID1" gate="G$1" x="127" y="157.48"/>
<instance part="FID4" gate="G$1" x="127" y="152.4"/>
<instance part="C6" gate="G$1" x="48.26" y="78.74" smashed="yes" rot="MR0">
<attribute name="NAME" x="46.736" y="79.121" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="46.736" y="74.041" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND8" gate="1" x="48.26" y="68.58" smashed="yes">
<attribute name="VALUE" x="45.72" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="86.36" y="106.68" smashed="yes" rot="MR0">
<attribute name="NAME" x="84.836" y="107.061" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="84.836" y="101.981" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C3" gate="G$1" x="170.18" y="-91.44" rot="MR0"/>
<instance part="ADCL" gate="A" x="233.68" y="15.24" smashed="yes" rot="MR180">
<attribute name="NAME" x="243.205" y="17.145" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="227.33" y="27.94" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="COMMUNICATION" gate="A" x="233.68" y="60.96" smashed="yes" rot="MR180">
<attribute name="NAME" x="243.205" y="60.96" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="227.33" y="73.66" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="+3V3" gate="G$1" x="40.64" y="17.78" smashed="yes">
<attribute name="VALUE" x="43.18" y="20.32" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+8" gate="1" x="337.82" y="180.34" smashed="yes">
<attribute name="VALUE" x="339.725" y="182.88" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="IC3" gate="1" x="139.7" y="60.96" smashed="yes">
<attribute name="NAME" x="109.22" y="140.97" size="1.778" layer="95"/>
<attribute name="VALUE" x="109.22" y="-17.78" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="G$1" x="76.2" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="74.676" y="109.601" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="74.676" y="104.521" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C7" gate="G$1" x="66.04" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="64.516" y="109.601" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="64.516" y="104.521" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="ADCH" gate="A" x="68.58" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="64.77" y="42.545" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="74.93" y="55.88" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="238.76" y="165.1" rot="MR0"/>
<instance part="P+10" gate="1" x="238.76" y="185.42" smashed="yes">
<attribute name="VALUE" x="240.665" y="187.96" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND23" gate="1" x="238.76" y="154.94"/>
<instance part="C11" gate="G$1" x="358.14" y="78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="360.68" y="81.28" size="1.778" layer="95"/>
<attribute name="VALUE" x="360.68" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="GND25" gate="1" x="358.14" y="88.9" rot="R180"/>
<instance part="C23" gate="G$1" x="276.86" y="-60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="275.463" y="-57.785" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="275.463" y="-64.516" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="RX" gate="G$1" x="520.7" y="17.78" smashed="yes" rot="R270">
<attribute name="NAME" x="516.128" y="14.859" size="1.778" layer="95"/>
<attribute name="VALUE" x="516.128" y="12.065" size="1.778" layer="96"/>
</instance>
<instance part="TX" gate="G$1" x="520.7" y="33.02" rot="R270"/>
<instance part="X2" gate="G$1" x="317.5" y="66.04" rot="MR0"/>
<instance part="F1" gate="G$1" x="347.98" y="73.66" rot="R270"/>
<instance part="P+13" gate="1" x="548.64" y="40.64" smashed="yes">
<attribute name="VALUE" x="550.545" y="43.18" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C15" gate="G$1" x="375.92" y="30.48" smashed="yes">
<attribute name="NAME" x="378.46" y="30.48" size="1.778" layer="95"/>
<attribute name="VALUE" x="378.46" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="GND28" gate="1" x="375.92" y="20.32"/>
<instance part="L" gate="G$1" x="210.82" y="149.86" rot="R90"/>
<instance part="GND13" gate="1" x="220.98" y="149.86" rot="R90"/>
<instance part="C10" gate="G$1" x="525.78" y="81.28" rot="MR270"/>
<instance part="T1" gate="A" x="200.66" y="-55.88" smashed="yes" rot="MR270">
<attribute name="NAME" x="198.755" y="-59.055" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="193.675" y="-61.595" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND15" gate="1" x="139.7" y="-63.5"/>
<instance part="C22" gate="G$1" x="147.32" y="-55.88" smashed="yes" rot="MR180">
<attribute name="NAME" x="153.543" y="-53.975" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="155.194" y="-56.515" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND16" gate="1" x="114.3" y="-63.5"/>
<instance part="P+2" gate="1" x="114.3" y="-35.56" smashed="yes">
<attribute name="VALUE" x="116.205" y="-33.02" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C21" gate="G$1" x="119.38" y="-53.34" smashed="yes" rot="MR180">
<attribute name="NAME" x="125.603" y="-51.435" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="127.254" y="-53.975" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="P+11" gate="1" x="220.98" y="-45.72" smashed="yes">
<attribute name="VALUE" x="222.885" y="-43.18" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="RESET-EN" gate="1" x="538.48" y="96.52" smashed="yes">
<attribute name="NAME" x="533.4" y="92.71" size="1.778" layer="95"/>
<attribute name="VALUE" x="535.94" y="92.71" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="1" x="510.54" y="58.42"/>
<instance part="C16" gate="G$1" x="386.08" y="30.48" smashed="yes" rot="MR0">
<attribute name="NAME" x="388.62" y="27.94" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="388.62" y="25.4" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="P+9" gate="1" x="414.02" y="58.42" smashed="yes">
<attribute name="VALUE" x="415.925" y="60.96" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="IC4" gate="G$1" x="457.2" y="55.88"/>
<instance part="ICSP1" gate="A" x="398.78" y="101.6"/>
<instance part="P+14" gate="1" x="408.94" y="114.3" smashed="yes">
<attribute name="VALUE" x="410.845" y="116.84" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND24" gate="1" x="408.94" y="91.44"/>
<instance part="GND26" gate="1" x="424.18" y="15.24"/>
<instance part="GND22" gate="1" x="276.86" y="-68.58"/>
<instance part="L2" gate="G$1" x="345.44" y="40.64" rot="R180"/>
<instance part="Z1" gate="G$1" x="325.12" y="53.34" smashed="yes" rot="MR270">
<attribute name="NAME" x="321.31" y="50.8" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="326.39" y="45.72" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="Z2" gate="G$1" x="337.82" y="53.34" smashed="yes" rot="MR270">
<attribute name="NAME" x="334.01" y="50.8" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="339.09" y="43.18" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="GROUND" gate="1" x="416.56" y="22.86" smashed="yes">
<attribute name="NAME" x="411.48" y="19.05" size="1.778" layer="95"/>
<attribute name="VALUE" x="414.02" y="19.05" size="1.778" layer="96"/>
</instance>
<instance part="Y1" gate="G$1" x="55.88" y="129.54" smashed="yes">
<attribute name="NAME" x="50.8" y="134.112" size="1.778" layer="95"/>
<attribute name="VALUE" x="27.94" y="123.952" size="1.778" layer="96"/>
</instance>
<instance part="GND20" gate="1" x="40.64" y="129.54" smashed="yes" rot="R270">
<attribute name="VALUE" x="38.1" y="132.08" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="RN5" gate="A" x="482.6" y="2.54"/>
<instance part="RN5" gate="B" x="482.6" y="15.24"/>
<instance part="RN5" gate="C" x="533.4" y="40.64"/>
<instance part="RN5" gate="D" x="533.4" y="17.78"/>
<instance part="RN3" gate="A" x="431.8" y="111.76" rot="R270"/>
<instance part="RN3" gate="D" x="132.08" y="-55.88" rot="R270"/>
<instance part="RN3" gate="C" x="132.08" y="-43.18" rot="R90"/>
<instance part="RN3" gate="B" x="279.4" y="17.78"/>
<instance part="RN1" gate="C" x="330.2" y="167.64" rot="R270"/>
<instance part="RN1" gate="D" x="246.38" y="175.26" smashed="yes" rot="R270">
<attribute name="VALUE" x="248.412" y="172.72" size="1.778" layer="96" rot="R270"/>
<attribute name="NAME" x="248.412" y="180.34" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="RN1" gate="B" x="337.82" y="167.64" rot="R270"/>
<instance part="RN1" gate="A" x="279.4" y="10.16"/>
<instance part="RN2" gate="A" x="200.66" y="149.86"/>
<instance part="RN2" gate="B" x="200.66" y="160.02"/>
<instance part="RN2" gate="C" x="231.14" y="-53.34" smashed="yes">
<attribute name="NAME" x="226.06" y="-50.8" size="1.778" layer="95"/>
<attribute name="VALUE" x="233.68" y="-50.8" size="1.778" layer="96"/>
</instance>
<instance part="RN2" gate="D" x="505.46" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="501.142" y="56.642" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="506.73" y="54.356" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="FID3" gate="G$1" x="121.92" y="152.4"/>
<instance part="RN4" gate="A" x="378.46" y="71.12" rot="R180"/>
<instance part="RN4" gate="B" x="279.4" y="38.1"/>
<instance part="RN4" gate="C" x="279.4" y="33.02"/>
<instance part="RN4" gate="D" x="378.46" y="63.5" rot="R180"/>
<instance part="RESET" gate="G$1" x="45.72" y="-30.48" rot="R90"/>
<instance part="IC7" gate="G$1" x="254" y="-55.88"/>
<instance part="GND30" gate="1" x="243.84" y="-68.58"/>
<instance part="C13" gate="G$1" x="408.94" y="66.04" smashed="yes" rot="MR90">
<attribute name="NAME" x="411.48" y="68.58" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="411.48" y="66.04" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="C12" gate="G$1" x="408.94" y="76.2" smashed="yes" rot="MR90">
<attribute name="NAME" x="411.48" y="78.74" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="411.48" y="76.2" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="GND3" gate="1" x="398.78" y="71.12" rot="R270"/>
<instance part="IC1" gate="A" x="157.48" y="154.94"/>
<instance part="IC1" gate="B" x="172.72" y="-43.18"/>
<instance part="IC1" gate="P" x="109.22" y="-50.8"/>
<instance part="U7A" gate="G$1" x="429.26" y="-45.72" smashed="yes">
<attribute name="VALUE" x="434.34" y="-76.2" size="1.778" layer="96"/>
<attribute name="PART" x="419.1" y="-20.32" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="P+12" gate="1" x="381" y="0" smashed="yes">
<attribute name="VALUE" x="377.952" y="0.762" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="342.9" y="-35.56" smashed="yes">
<attribute name="VALUE" x="340.36" y="-38.1" size="1.778" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="429.26" y="-7.62" smashed="yes">
<attribute name="VALUE" x="425.45" y="-6.858" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="429.26" y="-86.36"/>
<instance part="GND21" gate="1" x="373.38" y="-58.42" smashed="yes">
<attribute name="VALUE" x="370.84" y="-60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND29" gate="1" x="363.22" y="-17.78"/>
<instance part="GND27" gate="1" x="439.42" y="-17.78"/>
<instance part="RN7" gate="A" x="381" y="-20.32"/>
<instance part="RN7" gate="B" x="279.4" y="-2.54"/>
<instance part="RN7" gate="C" x="279.4" y="-7.62"/>
<instance part="RN7" gate="D" x="381" y="-27.94"/>
<instance part="F2" gate="G$1" x="353.06" y="-5.08" rot="R270"/>
<instance part="L3" gate="G$1" x="373.38" y="-2.54" rot="R180"/>
<instance part="C17" gate="G$1" x="363.22" y="-10.16" rot="MR0"/>
<instance part="C24" gate="G$1" x="406.4" y="-71.12" rot="MR0"/>
<instance part="C18" gate="G$1" x="439.42" y="-10.16"/>
<instance part="C19" gate="G$1" x="365.76" y="-50.8" smashed="yes" rot="MR180">
<attribute name="NAME" x="368.3" y="-48.26" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="368.3" y="-50.8" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="C20" gate="G$1" x="381" y="-50.8" smashed="yes" rot="MR180">
<attribute name="NAME" x="383.54" y="-48.26" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="383.54" y="-50.8" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="Z3" gate="G$1" x="350.52" y="-33.02" smashed="yes" rot="MR270">
<attribute name="NAME" x="346.71" y="-35.56" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="351.79" y="-43.18" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="Z4" gate="G$1" x="358.14" y="-33.02" smashed="yes" rot="MR270">
<attribute name="NAME" x="354.33" y="-35.56" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="359.41" y="-43.18" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="SJ3" gate="G$1" x="490.22" y="-137.16" smashed="yes">
<attribute name="NAME" x="490.22" y="-140.97" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="492.76" y="-142.875" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="SJ1" gate="G$1" x="538.48" y="-12.7" smashed="yes">
<attribute name="NAME" x="538.48" y="-16.51" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="541.02" y="-18.415" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="IC5" gate="A" x="482.6" y="-20.32" rot="MR0"/>
<instance part="IC5" gate="P" x="391.16" y="-127"/>
<instance part="IC6" gate="A" x="482.6" y="-40.64" rot="MR0"/>
<instance part="IC6" gate="P" x="398.78" y="-127"/>
<instance part="IC8" gate="A" x="482.6" y="-60.96" rot="MR0"/>
<instance part="IC8" gate="P" x="406.4" y="-127"/>
<instance part="IC9" gate="A" x="482.6" y="-81.28" rot="MR0"/>
<instance part="IC9" gate="P" x="414.02" y="-127"/>
<instance part="RN6" gate="A" x="406.4" y="-17.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="400.812" y="-20.32" size="1.778" layer="96" rot="R270"/>
<attribute name="NAME" x="400.812" y="-12.7" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="RN6" gate="B" x="518.16" y="-53.34" rot="R90"/>
<instance part="RN6" gate="C" x="279.4" y="-20.32" rot="R180"/>
<instance part="RN6" gate="D" x="523.24" y="-22.86" rot="R270"/>
<instance part="GND31" gate="1" x="510.54" y="-76.2"/>
<instance part="GND32" gate="1" x="523.24" y="-30.48"/>
<instance part="P+15" gate="1" x="518.16" y="-45.72" smashed="yes">
<attribute name="VALUE" x="515.112" y="-44.958" size="1.778" layer="96"/>
</instance>
<instance part="SJ2" gate="G$1" x="530.86" y="-60.96" smashed="yes" rot="MR180">
<attribute name="NAME" x="530.86" y="-57.15" size="1.778" layer="95" font="vector" rot="MR0"/>
<attribute name="VALUE" x="533.4" y="-55.245" size="1.778" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="+3V2" gate="G$1" x="289.56" y="-45.72" smashed="yes">
<attribute name="VALUE" x="285.75" y="-44.958" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="365.76" y="30.48" smashed="yes">
<attribute name="NAME" x="368.3" y="30.48" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.3" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="C25" gate="G$1" x="424.18" y="-124.46"/>
<instance part="+3V4" gate="G$1" x="424.18" y="-111.76" smashed="yes">
<attribute name="VALUE" x="420.37" y="-110.998" size="1.778" layer="96"/>
</instance>
<instance part="C26" gate="G$1" x="431.8" y="-124.46"/>
<instance part="GND33" gate="1" x="424.18" y="-139.7"/>
<instance part="GND34" gate="1" x="149.86" y="-104.14"/>
<instance part="P+16" gate="1" x="467.36" y="172.72" smashed="yes">
<attribute name="VALUE" x="469.265" y="175.26" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="Y2" gate="G$1" x="416.56" y="71.12" smashed="yes" rot="R90">
<attribute name="NAME" x="419.1" y="72.136" size="1.778" layer="95"/>
<attribute name="VALUE" x="419.1" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="424.18" y="71.12" rot="R90"/>
<instance part="Y3" gate="G$1" x="373.38" y="-43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="368.3" y="-40.64" size="1.778" layer="95"/>
<attribute name="VALUE" x="375.92" y="-40.64" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="541.02" y="-22.86"/>
<instance part="R6" gate="G$1" x="533.4" y="-50.8"/>
<instance part="R9" gate="G$1" x="490.22" y="-147.32" rot="R180"/>
<instance part="XIO" gate="A" x="68.58" y="-73.66" rot="R180"/>
<instance part="GND11" gate="1" x="86.36" y="-45.72" rot="R90"/>
<instance part="P+7" gate="1" x="86.36" y="-96.52" smashed="yes">
<attribute name="VALUE" x="88.265" y="-93.98" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D4" gate="G$1" x="424.18" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="422.1226" y="99.06" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="429.0314" y="83.82" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D5" gate="G$1" x="238.76" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="234.1626" y="177.8" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="233.4514" y="170.18" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="JP1" gate="A" x="233.68" y="109.22"/>
<instance part="POWER" gate="A" x="58.42" y="5.08" smashed="yes">
<attribute name="NAME" x="53.34" y="20.32" size="1.778" layer="95"/>
<attribute name="VALUE" x="52.07" y="-7.62" size="1.778" layer="96"/>
</instance>
<instance part="X3" gate="G$1" x="332.74" y="-25.4" rot="MR0"/>
<instance part="L4" gate="G$1" x="48.26" y="109.22" smashed="yes">
<attribute name="NAME" x="49.276" y="116.586" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="42.164" y="108.204" size="1.778" layer="96"/>
</instance>
<instance part="C27" gate="G$1" x="35.56" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="34.036" y="109.601" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="34.036" y="104.521" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C49" gate="G$1" x="388.62" y="154.94" smashed="yes">
<attribute name="NAME" x="391.16" y="154.4066" size="1.778" layer="95"/>
<attribute name="VALUE" x="390.906" y="151.8666" size="1.778" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="388.62" y="144.78"/>
<instance part="GND36" gate="1" x="477.52" y="144.78"/>
<instance part="PC2" gate="G$1" x="477.52" y="157.48"/>
<instance part="C50" gate="G$1" x="487.68" y="157.48"/>
<instance part="GND48" gate="1" x="439.42" y="132.08"/>
<instance part="IC2" gate="G$1" x="439.42" y="154.94"/>
<instance part="R28" gate="G$1" x="467.36" y="154.94" rot="R90"/>
<instance part="R29" gate="G$1" x="467.36" y="139.7" rot="R90"/>
<instance part="GND44" gate="1" x="467.36" y="127"/>
<instance part="R30" gate="G$1" x="416.56" y="152.4"/>
<instance part="R31" gate="G$1" x="416.56" y="139.7" rot="R90"/>
<instance part="GND50" gate="1" x="416.56" y="127"/>
<instance part="C41" gate="G$1" x="398.78" y="154.94"/>
<instance part="GND37" gate="1" x="398.78" y="144.78"/>
<instance part="R1" gate="G$1" x="419.1" y="93.98" rot="R90"/>
<instance part="FRAME1" gate="G$1" x="5.08" y="-218.44"/>
<instance part="FRAME1" gate="G$2" x="457.2" y="-218.44"/>
<instance part="U$1" gate="G$1" x="27.94" y="-195.58"/>
</instances>
<busses>
</busses>
<nets>
<net name="+5V" class="3">
<segment>
<wire x1="299.72" y1="157.48" x2="299.72" y2="154.94" width="0.1524" layer="91"/>
<wire x1="299.72" y1="154.94" x2="287.02" y2="154.94" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<pinref part="ICSP" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="175.26" y1="160.02" x2="195.58" y2="160.02" width="0.1524" layer="91"/>
<wire x1="175.26" y1="167.64" x2="175.26" y2="160.02" width="0.1524" layer="91"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<pinref part="RN2" gate="B" pin="1"/>
</segment>
<segment>
<wire x1="86.36" y1="114.3" x2="86.36" y2="111.76" width="0.1524" layer="91"/>
<wire x1="86.36" y1="111.76" x2="86.36" y2="109.22" width="0.1524" layer="91"/>
<wire x1="76.2" y1="111.76" x2="66.04" y2="111.76" width="0.1524" layer="91"/>
<wire x1="104.14" y1="101.6" x2="96.52" y2="101.6" width="0.1524" layer="91"/>
<wire x1="96.52" y1="101.6" x2="96.52" y2="104.14" width="0.1524" layer="91"/>
<wire x1="96.52" y1="104.14" x2="96.52" y2="106.68" width="0.1524" layer="91"/>
<wire x1="96.52" y1="106.68" x2="96.52" y2="109.22" width="0.1524" layer="91"/>
<wire x1="96.52" y1="109.22" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="101.6" y1="109.22" x2="104.14" y2="109.22" width="0.1524" layer="91"/>
<wire x1="104.14" y1="106.68" x2="96.52" y2="106.68" width="0.1524" layer="91"/>
<wire x1="104.14" y1="104.14" x2="96.52" y2="104.14" width="0.1524" layer="91"/>
<wire x1="76.2" y1="111.76" x2="86.36" y2="111.76" width="0.1524" layer="91"/>
<junction x="76.2" y="111.76"/>
<junction x="96.52" y="106.68"/>
<junction x="96.52" y="104.14"/>
<junction x="86.36" y="111.76"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="IC3" gate="1" pin="VCC3"/>
<pinref part="IC3" gate="1" pin="VCC"/>
<pinref part="IC3" gate="1" pin="VCC1"/>
<pinref part="IC3" gate="1" pin="VCC2"/>
<wire x1="86.36" y1="111.76" x2="101.6" y2="111.76" width="0.1524" layer="91"/>
<wire x1="101.6" y1="111.76" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<junction x="101.6" y="109.22"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="53.34" y1="111.76" x2="66.04" y2="111.76" width="0.1524" layer="91"/>
<junction x="66.04" y="111.76"/>
</segment>
<segment>
<wire x1="35.56" y1="5.08" x2="35.56" y2="15.24" width="0.1524" layer="91"/>
<wire x1="55.88" y1="5.08" x2="48.26" y2="5.08" width="0.1524" layer="91"/>
<wire x1="48.26" y1="5.08" x2="35.56" y2="5.08" width="0.1524" layer="91"/>
<wire x1="55.88" y1="12.7" x2="48.26" y2="12.7" width="0.1524" layer="91"/>
<wire x1="48.26" y1="12.7" x2="48.26" y2="5.08" width="0.1524" layer="91"/>
<junction x="48.26" y="5.08"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<pinref part="POWER" gate="A" pin="5"/>
<pinref part="POWER" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="330.2" y1="175.26" x2="337.82" y2="175.26" width="0.1524" layer="91"/>
<wire x1="337.82" y1="175.26" x2="337.82" y2="177.8" width="0.1524" layer="91"/>
<wire x1="330.2" y1="172.72" x2="330.2" y2="175.26" width="0.1524" layer="91"/>
<pinref part="P+8" gate="1" pin="+5V"/>
<pinref part="RN1" gate="C" pin="1"/>
<pinref part="RN1" gate="B" pin="1"/>
<wire x1="337.82" y1="172.72" x2="337.82" y2="175.26" width="0.1524" layer="91"/>
<junction x="337.82" y="175.26"/>
</segment>
<segment>
<wire x1="238.76" y1="180.34" x2="238.76" y2="182.88" width="0.1524" layer="91"/>
<wire x1="246.38" y1="180.34" x2="238.76" y2="180.34" width="0.1524" layer="91"/>
<wire x1="238.76" y1="177.8" x2="238.76" y2="180.34" width="0.1524" layer="91"/>
<junction x="238.76" y="180.34"/>
<pinref part="P+10" gate="1" pin="+5V"/>
<pinref part="RN1" gate="D" pin="1"/>
<pinref part="D5" gate="G$1" pin="C"/>
</segment>
<segment>
<wire x1="548.64" y1="25.4" x2="543.56" y2="25.4" width="0.1524" layer="91"/>
<wire x1="548.64" y1="25.4" x2="548.64" y2="33.02" width="0.1524" layer="91"/>
<wire x1="548.64" y1="33.02" x2="548.64" y2="38.1" width="0.1524" layer="91"/>
<wire x1="543.56" y1="33.02" x2="548.64" y2="33.02" width="0.1524" layer="91"/>
<wire x1="538.48" y1="17.78" x2="543.56" y2="17.78" width="0.1524" layer="91"/>
<wire x1="543.56" y1="17.78" x2="543.56" y2="25.4" width="0.1524" layer="91"/>
<wire x1="538.48" y1="40.64" x2="543.56" y2="40.64" width="0.1524" layer="91"/>
<wire x1="543.56" y1="40.64" x2="543.56" y2="33.02" width="0.1524" layer="91"/>
<junction x="548.64" y="33.02"/>
<pinref part="P+13" gate="1" pin="+5V"/>
<pinref part="RN5" gate="D" pin="2"/>
<pinref part="RN5" gate="C" pin="2"/>
</segment>
<segment>
<wire x1="114.3" y1="-38.1" x2="114.3" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-40.64" x2="119.38" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-40.64" x2="119.38" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-40.64" x2="109.22" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-40.64" x2="109.22" y2="-43.18" width="0.1524" layer="91"/>
<junction x="114.3" y="-40.64"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="IC1" gate="P" pin="V+"/>
</segment>
<segment>
<wire x1="403.86" y1="104.14" x2="408.94" y2="104.14" width="0.1524" layer="91"/>
<wire x1="408.94" y1="104.14" x2="408.94" y2="111.76" width="0.1524" layer="91"/>
<wire x1="408.94" y1="104.14" x2="419.1" y2="104.14" width="0.1524" layer="91"/>
<wire x1="419.1" y1="104.14" x2="424.18" y2="104.14" width="0.1524" layer="91"/>
<wire x1="424.18" y1="96.52" x2="424.18" y2="104.14" width="0.1524" layer="91"/>
<junction x="408.94" y="104.14"/>
<pinref part="ICSP1" gate="A" pin="2"/>
<pinref part="P+14" gate="1" pin="+5V"/>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="419.1" y1="99.06" x2="419.1" y2="104.14" width="0.1524" layer="91"/>
<junction x="419.1" y="104.14"/>
</segment>
<segment>
<wire x1="429.26" y1="53.34" x2="414.02" y2="53.34" width="0.1524" layer="91"/>
<wire x1="424.18" y1="60.96" x2="429.26" y2="60.96" width="0.1524" layer="91"/>
<wire x1="424.18" y1="55.88" x2="424.18" y2="60.96" width="0.1524" layer="91"/>
<wire x1="414.02" y1="55.88" x2="424.18" y2="55.88" width="0.1524" layer="91"/>
<wire x1="414.02" y1="55.88" x2="414.02" y2="53.34" width="0.1524" layer="91"/>
<wire x1="414.02" y1="53.34" x2="375.92" y2="53.34" width="0.1524" layer="91"/>
<wire x1="375.92" y1="53.34" x2="375.92" y2="35.56" width="0.1524" layer="91"/>
<wire x1="375.92" y1="35.56" x2="375.92" y2="33.02" width="0.1524" layer="91"/>
<wire x1="365.76" y1="33.02" x2="365.76" y2="35.56" width="0.1524" layer="91"/>
<wire x1="365.76" y1="35.56" x2="375.92" y2="35.56" width="0.1524" layer="91"/>
<junction x="414.02" y="55.88"/>
<junction x="414.02" y="53.34"/>
<junction x="375.92" y="35.56"/>
<pinref part="IC4" gate="G$1" pin="VCC"/>
<pinref part="IC4" gate="G$1" pin="AVCC"/>
<pinref part="P+9" gate="1" pin="+5V"/>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="C14" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="378.46" y1="-5.08" x2="381" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="381" y1="-5.08" x2="381" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="P+12" gate="1" pin="+5V"/>
<pinref part="L3" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="236.22" y1="-55.88" x2="236.22" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="243.84" y1="-50.8" x2="241.3" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="241.3" y1="-50.8" x2="241.3" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="241.3" y1="-55.88" x2="243.84" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="241.3" y1="-55.88" x2="236.22" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="220.98" y1="-55.88" x2="220.98" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-55.88" x2="220.98" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="220.98" y1="-55.88" x2="226.06" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-55.88" x2="236.22" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="226.06" y1="-53.34" x2="226.06" y2="-55.88" width="0.1524" layer="91"/>
<junction x="241.3" y="-55.88"/>
<junction x="236.22" y="-55.88"/>
<junction x="220.98" y="-55.88"/>
<junction x="226.06" y="-55.88"/>
<label x="220.98" y="-53.34" size="1.778" layer="95" rot="R90"/>
<pinref part="RN2" gate="C" pin="1"/>
<pinref part="RN2" gate="C" pin="2"/>
<pinref part="IC7" gate="G$1" pin="ON/!OFF"/>
<pinref part="IC7" gate="G$1" pin="IN"/>
<pinref part="P+11" gate="1" pin="+5V"/>
<pinref part="T1" gate="A" pin="S"/>
</segment>
<segment>
<pinref part="RN6" gate="B" pin="2"/>
<pinref part="P+15" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="63.5" y1="-93.98" x2="55.88" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-93.98" x2="55.88" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-101.6" x2="78.74" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-101.6" x2="78.74" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-93.98" x2="71.12" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-101.6" x2="86.36" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-101.6" x2="86.36" y2="-99.06" width="0.1524" layer="91"/>
<junction x="78.74" y="-101.6"/>
<pinref part="XIO" gate="A" pin="2"/>
<pinref part="XIO" gate="A" pin="1"/>
<pinref part="P+7" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="477.52" y1="160.02" x2="477.52" y2="165.1" width="0.1524" layer="91"/>
<pinref part="PC2" gate="G$1" pin="+"/>
<junction x="477.52" y="165.1"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="487.68" y1="160.02" x2="487.68" y2="165.1" width="0.1524" layer="91"/>
<wire x1="487.68" y1="165.1" x2="477.52" y2="165.1" width="0.1524" layer="91"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="467.36" y1="160.02" x2="467.36" y2="165.1" width="0.1524" layer="91"/>
<wire x1="467.36" y1="165.1" x2="459.74" y2="165.1" width="0.1524" layer="91"/>
<wire x1="459.74" y1="165.1" x2="459.74" y2="154.94" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="OUT"/>
<wire x1="459.74" y1="154.94" x2="454.66" y2="154.94" width="0.1524" layer="91"/>
<wire x1="467.36" y1="165.1" x2="477.52" y2="165.1" width="0.1524" layer="91"/>
<junction x="467.36" y="165.1"/>
<pinref part="P+16" gate="1" pin="+5V"/>
<wire x1="467.36" y1="170.18" x2="467.36" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="2">
<segment>
<wire x1="299.72" y1="149.86" x2="287.02" y2="149.86" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="ICSP" gate="A" pin="6"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="PC1" gate="G$1" pin="-"/>
</segment>
<segment>
<wire x1="231.14" y1="111.76" x2="215.9" y2="111.76" width="0.1524" layer="91"/>
<label x="215.9" y="111.76" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="4"/>
</segment>
<segment>
<wire x1="218.44" y1="160.02" x2="215.9" y2="160.02" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="ON" gate="G$1" pin="C"/>
</segment>
<segment>
<wire x1="119.38" y1="-91.44" x2="119.38" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-93.98" x2="119.38" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-93.98" x2="139.7" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-93.98" x2="139.7" y2="-81.28" width="0.1524" layer="91"/>
<junction x="119.38" y="-93.98"/>
<pinref part="X1" gate="G$1" pin="1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="X1" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="50.8" y1="-30.48" x2="53.34" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-30.48" x2="53.34" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-27.94" x2="50.8" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-27.94" x2="58.42" y2="-27.94" width="0.1524" layer="91"/>
<junction x="53.34" y="-27.94"/>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="RESET" gate="G$1" pin="1"/>
<pinref part="RESET" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="48.26" y1="71.12" x2="48.26" y2="73.66" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="55.88" y1="2.54" x2="35.56" y2="2.54" width="0.1524" layer="91"/>
<wire x1="35.56" y1="2.54" x2="35.56" y2="0" width="0.1524" layer="91"/>
<wire x1="55.88" y1="0" x2="35.56" y2="0" width="0.1524" layer="91"/>
<wire x1="35.56" y1="0" x2="35.56" y2="-2.54" width="0.1524" layer="91"/>
<junction x="35.56" y="0"/>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="POWER" gate="A" pin="6"/>
<pinref part="POWER" gate="A" pin="7"/>
</segment>
<segment>
<wire x1="86.36" y1="91.44" x2="86.36" y2="101.6" width="0.1524" layer="91"/>
<wire x1="76.2" y1="104.14" x2="76.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="76.2" y1="101.6" x2="86.36" y2="101.6" width="0.1524" layer="91"/>
<wire x1="76.2" y1="101.6" x2="66.04" y2="101.6" width="0.1524" layer="91"/>
<wire x1="66.04" y1="101.6" x2="66.04" y2="104.14" width="0.1524" layer="91"/>
<wire x1="104.14" y1="116.84" x2="91.44" y2="116.84" width="0.1524" layer="91"/>
<wire x1="91.44" y1="116.84" x2="91.44" y2="101.6" width="0.1524" layer="91"/>
<wire x1="91.44" y1="101.6" x2="86.36" y2="101.6" width="0.1524" layer="91"/>
<wire x1="104.14" y1="91.44" x2="96.52" y2="91.44" width="0.1524" layer="91"/>
<wire x1="96.52" y1="91.44" x2="86.36" y2="91.44" width="0.1524" layer="91"/>
<wire x1="104.14" y1="99.06" x2="96.52" y2="99.06" width="0.1524" layer="91"/>
<wire x1="96.52" y1="99.06" x2="96.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="96.52" y1="96.52" x2="96.52" y2="93.98" width="0.1524" layer="91"/>
<wire x1="96.52" y1="93.98" x2="96.52" y2="91.44" width="0.1524" layer="91"/>
<wire x1="104.14" y1="96.52" x2="96.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="104.14" y1="93.98" x2="96.52" y2="93.98" width="0.1524" layer="91"/>
<junction x="86.36" y="101.6"/>
<junction x="76.2" y="101.6"/>
<junction x="86.36" y="91.44"/>
<junction x="96.52" y="91.44"/>
<junction x="96.52" y="96.52"/>
<junction x="96.52" y="93.98"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="IC3" gate="1" pin="AGND"/>
<pinref part="IC3" gate="1" pin="GND3"/>
<pinref part="IC3" gate="1" pin="GND"/>
<pinref part="IC3" gate="1" pin="GND1"/>
<pinref part="IC3" gate="1" pin="GND2"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="66.04" y1="101.6" x2="35.56" y2="101.6" width="0.1524" layer="91"/>
<wire x1="35.56" y1="101.6" x2="35.56" y2="104.14" width="0.1524" layer="91"/>
<junction x="66.04" y="101.6"/>
</segment>
<segment>
<wire x1="238.76" y1="160.02" x2="238.76" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="375.92" y1="22.86" x2="375.92" y2="25.4" width="0.1524" layer="91"/>
<wire x1="375.92" y1="22.86" x2="365.76" y2="22.86" width="0.1524" layer="91"/>
<wire x1="365.76" y1="22.86" x2="365.76" y2="25.4" width="0.1524" layer="91"/>
<junction x="375.92" y="22.86"/>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="C14" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="218.44" y1="149.86" x2="215.9" y2="149.86" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<pinref part="L" gate="G$1" pin="C"/>
</segment>
<segment>
<wire x1="147.32" y1="-58.42" x2="147.32" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-60.96" x2="139.7" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-60.96" x2="139.7" y2="-60.96" width="0.1524" layer="91"/>
<junction x="139.7" y="-60.96"/>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="C22" gate="G$1" pin="1"/>
<pinref part="RN3" gate="D" pin="2"/>
</segment>
<segment>
<wire x1="119.38" y1="-55.88" x2="119.38" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="119.38" y1="-60.96" x2="114.3" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="114.3" y1="-60.96" x2="109.22" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-60.96" x2="109.22" y2="-58.42" width="0.1524" layer="91"/>
<junction x="114.3" y="-60.96"/>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="IC1" gate="P" pin="V-"/>
</segment>
<segment>
<wire x1="403.86" y1="99.06" x2="408.94" y2="99.06" width="0.1524" layer="91"/>
<wire x1="408.94" y1="99.06" x2="408.94" y2="93.98" width="0.1524" layer="91"/>
<pinref part="ICSP1" gate="A" pin="6"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="429.26" y1="50.8" x2="424.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="429.26" y1="27.94" x2="424.18" y2="27.94" width="0.1524" layer="91"/>
<wire x1="424.18" y1="50.8" x2="424.18" y2="27.94" width="0.1524" layer="91"/>
<wire x1="424.18" y1="17.78" x2="424.18" y2="22.86" width="0.1524" layer="91"/>
<wire x1="424.18" y1="22.86" x2="424.18" y2="27.94" width="0.1524" layer="91"/>
<wire x1="421.64" y1="22.86" x2="424.18" y2="22.86" width="0.1524" layer="91"/>
<junction x="424.18" y="27.94"/>
<junction x="424.18" y="22.86"/>
<pinref part="IC4" gate="G$1" pin="GND"/>
<pinref part="IC4" gate="G$1" pin="PAD"/>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="GROUND" gate="1" pin="2"/>
</segment>
<segment>
<wire x1="510.54" y1="60.96" x2="510.54" y2="63.5" width="0.1524" layer="91"/>
<wire x1="505.46" y1="58.42" x2="505.46" y2="63.5" width="0.1524" layer="91"/>
<wire x1="505.46" y1="63.5" x2="510.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="RN2" gate="D" pin="2"/>
</segment>
<segment>
<wire x1="358.14" y1="83.82" x2="358.14" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND25" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="276.86" y1="-63.5" x2="276.86" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="43.18" y1="129.54" x2="48.26" y2="129.54" width="0.1524" layer="91"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="243.84" y1="-60.96" x2="243.84" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="IC7" gate="G$1" pin="GND"/>
<pinref part="GND30" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="403.86" y1="66.04" x2="403.86" y2="71.12" width="0.1524" layer="91"/>
<wire x1="403.86" y1="71.12" x2="403.86" y2="76.2" width="0.1524" layer="91"/>
<wire x1="401.32" y1="71.12" x2="403.86" y2="71.12" width="0.1524" layer="91"/>
<wire x1="411.48" y1="71.12" x2="403.86" y2="71.12" width="0.1524" layer="91"/>
<junction x="403.86" y="71.12"/>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="Y2" gate="G$1" pin="GND@2"/>
</segment>
<segment>
<wire x1="342.9" y1="-27.94" x2="342.9" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-27.94" x2="342.9" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="X3" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="429.26" y1="-83.82" x2="429.26" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="429.26" y1="-81.28" x2="426.72" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="426.72" y1="-81.28" x2="426.72" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="429.26" y1="-81.28" x2="431.8" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-81.28" x2="431.8" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-76.2" x2="406.4" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-81.28" x2="426.72" y2="-81.28" width="0.1524" layer="91"/>
<junction x="429.26" y="-81.28"/>
<junction x="426.72" y="-81.28"/>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="U7A" gate="G$1" pin="GND@3"/>
<pinref part="U7A" gate="G$1" pin="GND@19"/>
<pinref part="C24" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="373.38" y1="-55.88" x2="365.76" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="365.76" y1="-55.88" x2="365.76" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-55.88" x2="381" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="381" y1="-55.88" x2="381" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-48.26" x2="373.38" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-38.1" x2="373.38" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-35.56" x2="378.46" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="378.46" y1="-35.56" x2="378.46" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="378.46" y1="-48.26" x2="373.38" y2="-48.26" width="0.1524" layer="91"/>
<junction x="373.38" y="-55.88"/>
<junction x="373.38" y="-48.26"/>
<pinref part="GND21" gate="1" pin="GND"/>
<pinref part="C19" gate="G$1" pin="1"/>
<pinref part="C20" gate="G$1" pin="1"/>
<pinref part="Y3" gate="G$1" pin="GND@2"/>
<pinref part="Y3" gate="G$1" pin="GND@1"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="C17" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="C18" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="482.6" y1="-30.48" x2="510.54" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="482.6" y1="-50.8" x2="510.54" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="482.6" y1="-71.12" x2="510.54" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="510.54" y1="-71.12" x2="510.54" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="510.54" y1="-50.8" x2="510.54" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="510.54" y1="-30.48" x2="510.54" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="510.54" y1="-10.16" x2="482.6" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="510.54" y1="-73.66" x2="510.54" y2="-71.12" width="0.1524" layer="91"/>
<junction x="510.54" y="-50.8"/>
<junction x="510.54" y="-30.48"/>
<junction x="510.54" y="-71.12"/>
<pinref part="IC5" gate="A" pin="OE"/>
<pinref part="IC6" gate="A" pin="OE"/>
<pinref part="IC8" gate="A" pin="OE"/>
<pinref part="IC9" gate="A" pin="OE"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="RN6" gate="D" pin="2"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="406.4" y1="-137.16" x2="414.02" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-137.16" x2="424.18" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="424.18" y1="-137.16" x2="431.8" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-137.16" x2="431.8" y2="-129.54" width="0.1524" layer="91"/>
<wire x1="424.18" y1="-129.54" x2="424.18" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="391.16" y1="-134.62" x2="391.16" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-134.62" x2="406.4" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-134.62" x2="414.02" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="391.16" y1="-137.16" x2="398.78" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="398.78" y1="-137.16" x2="406.4" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="398.78" y1="-137.16" x2="398.78" y2="-134.62" width="0.1524" layer="91"/>
<junction x="424.18" y="-137.16"/>
<junction x="406.4" y="-137.16"/>
<junction x="414.02" y="-137.16"/>
<junction x="398.78" y="-137.16"/>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="IC5" gate="P" pin="VSS"/>
<pinref part="IC6" gate="P" pin="VSS"/>
<pinref part="IC8" gate="P" pin="VSS"/>
<pinref part="IC9" gate="P" pin="VSS"/>
<pinref part="GND33" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="A"/>
<pinref part="GND34" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Y2" gate="G$1" pin="GND@1"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="63.5" y1="-50.8" x2="55.88" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-50.8" x2="55.88" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-45.72" x2="78.74" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-45.72" x2="78.74" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-50.8" x2="71.12" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-45.72" x2="83.82" y2="-45.72" width="0.1524" layer="91"/>
<junction x="78.74" y="-45.72"/>
<pinref part="XIO" gate="A" pin="36"/>
<pinref part="XIO" gate="A" pin="35"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="388.62" y1="147.32" x2="388.62" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C49" gate="G$1" pin="-"/>
<pinref part="GND17" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="477.52" y1="152.4" x2="477.52" y2="149.86" width="0.1524" layer="91"/>
<pinref part="GND36" gate="1" pin="GND"/>
<pinref part="PC2" gate="G$1" pin="-"/>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="477.52" y1="149.86" x2="477.52" y2="147.32" width="0.1524" layer="91"/>
<wire x1="487.68" y1="152.4" x2="487.68" y2="149.86" width="0.1524" layer="91"/>
<wire x1="487.68" y1="149.86" x2="477.52" y2="149.86" width="0.1524" layer="91"/>
<junction x="477.52" y="149.86"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="GND"/>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="439.42" y1="137.16" x2="439.42" y2="134.62" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="AGND"/>
<wire x1="441.96" y1="137.16" x2="441.96" y2="134.62" width="0.1524" layer="91"/>
<wire x1="441.96" y1="134.62" x2="439.42" y2="134.62" width="0.1524" layer="91"/>
<junction x="439.42" y="134.62"/>
</segment>
<segment>
<pinref part="GND44" gate="1" pin="GND"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="467.36" y1="129.54" x2="467.36" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="GND50" gate="1" pin="GND"/>
<wire x1="416.56" y1="134.62" x2="416.56" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C41" gate="G$1" pin="2"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="398.78" y1="149.86" x2="398.78" y2="147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AREF" class="0">
<segment>
<wire x1="231.14" y1="114.3" x2="215.9" y2="114.3" width="0.1524" layer="91"/>
<label x="215.9" y="114.3" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="3"/>
</segment>
<segment>
<wire x1="104.14" y1="121.92" x2="88.9" y2="121.92" width="0.1524" layer="91"/>
<label x="91.44" y="121.92" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="AREF"/>
</segment>
<segment>
<wire x1="48.26" y1="81.28" x2="40.64" y2="81.28" width="0.1524" layer="91"/>
<label x="40.64" y="81.28" size="1.778" layer="95"/>
<pinref part="C6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<wire x1="22.86" y1="10.16" x2="55.88" y2="10.16" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-27.94" x2="22.86" y2="10.16" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-27.94" x2="38.1" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-27.94" x2="38.1" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-30.48" x2="40.64" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-27.94" x2="22.86" y2="-27.94" width="0.1524" layer="91"/>
<junction x="38.1" y="-27.94"/>
<label x="22.86" y="-27.305" size="1.778" layer="95"/>
<pinref part="RESET" gate="G$1" pin="4"/>
<pinref part="RESET" gate="G$1" pin="3"/>
<pinref part="POWER" gate="A" pin="3"/>
</segment>
<segment>
<wire x1="83.82" y1="137.16" x2="104.14" y2="137.16" width="0.1524" layer="91"/>
<label x="86.995" y="137.795" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="RESET"/>
</segment>
<segment>
<wire x1="279.4" y1="149.86" x2="261.62" y2="149.86" width="0.1524" layer="91"/>
<label x="266.7" y="149.86" size="1.778" layer="95"/>
<pinref part="ICSP" gate="A" pin="5"/>
</segment>
<segment>
<wire x1="238.76" y1="167.64" x2="238.76" y2="170.18" width="0.1524" layer="91"/>
<wire x1="238.76" y1="170.18" x2="218.44" y2="170.18" width="0.1524" layer="91"/>
<wire x1="246.38" y1="170.18" x2="238.76" y2="170.18" width="0.1524" layer="91"/>
<wire x1="238.76" y1="172.72" x2="238.76" y2="170.18" width="0.1524" layer="91"/>
<junction x="238.76" y="170.18"/>
<label x="218.44" y="170.18" size="1.6764" layer="95"/>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="RN1" gate="D" pin="2"/>
<pinref part="D5" gate="G$1" pin="A"/>
</segment>
<segment>
<wire x1="530.86" y1="81.28" x2="533.4" y2="81.28" width="0.1524" layer="91"/>
<wire x1="533.4" y1="81.28" x2="546.1" y2="81.28" width="0.1524" layer="91"/>
<wire x1="546.1" y1="81.28" x2="548.64" y2="81.28" width="0.1524" layer="91"/>
<wire x1="533.4" y1="96.52" x2="533.4" y2="81.28" width="0.1524" layer="91"/>
<wire x1="543.56" y1="96.52" x2="546.1" y2="96.52" width="0.1524" layer="91"/>
<wire x1="546.1" y1="96.52" x2="546.1" y2="81.28" width="0.1524" layer="91"/>
<junction x="533.4" y="81.28"/>
<junction x="546.1" y="81.28"/>
<label x="535.94" y="81.28" size="1.778" layer="95"/>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="RESET-EN" gate="1" pin="1"/>
<pinref part="RESET-EN" gate="1" pin="2"/>
</segment>
</net>
<net name="VIN" class="1">
<segment>
<wire x1="40.64" y1="-2.54" x2="55.88" y2="-2.54" width="0.1524" layer="91"/>
<label x="43.18" y="-2.54" size="1.778" layer="95"/>
<pinref part="POWER" gate="A" pin="8"/>
</segment>
<segment>
<wire x1="124.46" y1="-35.56" x2="132.08" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-38.1" x2="132.08" y2="-35.56" width="0.1524" layer="91"/>
<label x="123.825" y="-35.56" size="1.778" layer="95"/>
<pinref part="RN3" gate="C" pin="2"/>
</segment>
<segment>
<wire x1="129.54" y1="-91.44" x2="149.86" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-91.44" x2="152.4" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="152.4" y1="-91.44" x2="152.4" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-86.36" x2="160.02" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="170.18" y1="-86.36" x2="170.18" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-88.9" x2="160.02" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-86.36" x2="160.02" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-81.28" x2="167.64" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="152.4" y1="-86.36" x2="160.02" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-96.52" x2="149.86" y2="-91.44" width="0.1524" layer="91"/>
<junction x="160.02" y="-86.36"/>
<junction x="149.86" y="-91.44"/>
<label x="130.81" y="-91.44" size="1.778" layer="95"/>
<label x="164.084" y="-80.645" size="1.778" layer="95"/>
<pinref part="X1" gate="G$1" pin="2"/>
<pinref part="PC1" gate="G$1" pin="+"/>
<pinref part="D2" gate="G$1" pin="C"/>
<pinref part="C3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VIN"/>
<wire x1="424.18" y1="160.02" x2="408.94" y2="160.02" width="0.1524" layer="91"/>
<wire x1="408.94" y1="160.02" x2="408.94" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="411.48" y1="152.4" x2="408.94" y2="152.4" width="0.1524" layer="91"/>
<label x="381" y="160.02" size="1.778" layer="95"/>
<wire x1="381" y1="160.02" x2="388.62" y2="160.02" width="0.1524" layer="91"/>
<junction x="408.94" y="160.02"/>
<pinref part="C49" gate="G$1" pin="+"/>
<wire x1="388.62" y1="160.02" x2="398.78" y2="160.02" width="0.1524" layer="91"/>
<wire x1="398.78" y1="160.02" x2="408.94" y2="160.02" width="0.1524" layer="91"/>
<wire x1="388.62" y1="157.48" x2="388.62" y2="160.02" width="0.1524" layer="91"/>
<junction x="388.62" y="160.02"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="398.78" y1="157.48" x2="398.78" y2="160.02" width="0.1524" layer="91"/>
<junction x="398.78" y="160.02"/>
</segment>
</net>
<net name="M8RXD" class="0">
<segment>
<wire x1="485.14" y1="35.56" x2="500.38" y2="35.56" width="0.1524" layer="91"/>
<wire x1="500.38" y1="35.56" x2="500.38" y2="12.7" width="0.1524" layer="91"/>
<wire x1="492.76" y1="12.7" x2="500.38" y2="12.7" width="0.1524" layer="91"/>
<wire x1="487.68" y1="15.24" x2="492.76" y2="15.24" width="0.1524" layer="91"/>
<wire x1="492.76" y1="15.24" x2="492.76" y2="12.7" width="0.1524" layer="91"/>
<label x="486.41" y="35.56" size="1.778" layer="95"/>
<label x="490.22" y="15.24" size="1.778" layer="95" rot="MR180"/>
<pinref part="IC4" gate="G$1" pin="(TXD1/INT3)PD3"/>
<pinref part="RN5" gate="B" pin="2"/>
</segment>
</net>
<net name="M8TXD" class="0">
<segment>
<wire x1="502.92" y1="33.02" x2="485.14" y2="33.02" width="0.1524" layer="91"/>
<wire x1="492.76" y1="5.08" x2="502.92" y2="5.08" width="0.1524" layer="91"/>
<wire x1="502.92" y1="5.08" x2="502.92" y2="33.02" width="0.1524" layer="91"/>
<wire x1="487.68" y1="2.54" x2="492.76" y2="2.54" width="0.1524" layer="91"/>
<wire x1="492.76" y1="2.54" x2="492.76" y2="5.08" width="0.1524" layer="91"/>
<label x="490.22" y="7.62" size="1.778" layer="95" rot="MR180"/>
<label x="486.41" y="33.02" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="(RXD1/AIN1/INT2)PD2"/>
<pinref part="RN5" gate="A" pin="2"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="208.28" y1="160.02" x2="205.74" y2="160.02" width="0.1524" layer="91"/>
<pinref part="ON" gate="G$1" pin="A"/>
<pinref part="RN2" gate="B" pin="2"/>
</segment>
</net>
<net name="ADC0" class="0">
<segment>
<wire x1="231.14" y1="5.08" x2="175.26" y2="5.08" width="0.1524" layer="91"/>
<label x="214.63" y="7.62" size="1.778" layer="95" rot="MR180"/>
<pinref part="ADCL" gate="A" pin="1"/>
<pinref part="IC3" gate="1" pin="(ADC0)PF0"/>
</segment>
</net>
<net name="ADC2" class="0">
<segment>
<wire x1="231.14" y1="10.16" x2="175.26" y2="10.16" width="0.1524" layer="91"/>
<label x="214.63" y="12.7" size="1.778" layer="95" rot="MR180"/>
<pinref part="ADCL" gate="A" pin="3"/>
<pinref part="IC3" gate="1" pin="(ADC2)PF2"/>
</segment>
</net>
<net name="ADC1" class="0">
<segment>
<wire x1="175.26" y1="7.62" x2="231.14" y2="7.62" width="0.1524" layer="91"/>
<label x="214.63" y="10.16" size="1.778" layer="95" rot="MR180"/>
<pinref part="ADCL" gate="A" pin="2"/>
<pinref part="IC3" gate="1" pin="(ADC1)PF1"/>
</segment>
</net>
<net name="ADC3" class="0">
<segment>
<wire x1="175.26" y1="12.7" x2="231.14" y2="12.7" width="0.1524" layer="91"/>
<label x="214.63" y="15.24" size="1.778" layer="95" rot="MR180"/>
<pinref part="ADCL" gate="A" pin="4"/>
<pinref part="IC3" gate="1" pin="(ADC3)PF3"/>
</segment>
</net>
<net name="ADC4" class="0">
<segment>
<wire x1="231.14" y1="15.24" x2="175.26" y2="15.24" width="0.1524" layer="91"/>
<label x="214.63" y="17.78" size="1.778" layer="95" rot="MR180"/>
<pinref part="ADCL" gate="A" pin="5"/>
<pinref part="IC3" gate="1" pin="(ADC4/TCK)PF4"/>
</segment>
</net>
<net name="ADC5" class="0">
<segment>
<wire x1="175.26" y1="17.78" x2="231.14" y2="17.78" width="0.1524" layer="91"/>
<label x="214.63" y="20.32" size="1.778" layer="95" rot="MR180"/>
<pinref part="ADCL" gate="A" pin="6"/>
<pinref part="IC3" gate="1" pin="(ADC5/TMS)PF5"/>
</segment>
</net>
<net name="ADC6" class="0">
<segment>
<wire x1="175.26" y1="20.32" x2="231.14" y2="20.32" width="0.1524" layer="91"/>
<label x="214.63" y="22.86" size="1.778" layer="95" rot="MR180"/>
<pinref part="ADCL" gate="A" pin="7"/>
<pinref part="IC3" gate="1" pin="(ADC6/TDO)PF6"/>
</segment>
</net>
<net name="ADC7" class="0">
<segment>
<wire x1="231.14" y1="22.86" x2="175.26" y2="22.86" width="0.1524" layer="91"/>
<label x="214.63" y="25.4" size="1.778" layer="95" rot="MR180"/>
<pinref part="ADCL" gate="A" pin="8"/>
<pinref part="IC3" gate="1" pin="(ADC7/TDI)PF7"/>
</segment>
</net>
<net name="+3V3" class="1">
<segment>
<wire x1="55.88" y1="7.62" x2="40.64" y2="7.62" width="0.1524" layer="91"/>
<wire x1="40.64" y1="7.62" x2="40.64" y2="15.24" width="0.1524" layer="91"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="POWER" gate="A" pin="4"/>
</segment>
<segment>
<wire x1="165.1" y1="-45.72" x2="152.4" y2="-45.72" width="0.1524" layer="91"/>
<label x="154.94" y="-45.72" size="1.778" layer="95"/>
<pinref part="IC1" gate="B" pin="-IN"/>
</segment>
<segment>
<wire x1="276.86" y1="-50.8" x2="276.86" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="276.86" y1="-50.8" x2="289.56" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-50.8" x2="276.86" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="289.56" y1="-50.8" x2="289.56" y2="-48.26" width="0.1524" layer="91"/>
<junction x="276.86" y="-50.8"/>
<label x="269.24" y="-50.8" size="1.778" layer="95"/>
<label x="279.4" y="-50.8" size="1.778" layer="95"/>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="IC7" gate="G$1" pin="OUT"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="426.72" y1="-15.24" x2="426.72" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="426.72" y1="-12.7" x2="429.26" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="429.26" y1="-12.7" x2="431.8" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-12.7" x2="431.8" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="429.26" y1="-12.7" x2="429.26" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-12.7" x2="426.72" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-12.7" x2="434.34" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="434.34" y1="-12.7" x2="434.34" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="434.34" y1="-7.62" x2="439.42" y2="-7.62" width="0.1524" layer="91"/>
<junction x="429.26" y="-12.7"/>
<junction x="426.72" y="-12.7"/>
<junction x="431.8" y="-12.7"/>
<pinref part="U7A" gate="G$1" pin="VCC"/>
<pinref part="U7A" gate="G$1" pin="VL"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="RN6" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="424.18" y1="-114.3" x2="424.18" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="424.18" y1="-116.84" x2="424.18" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="424.18" y1="-116.84" x2="414.02" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-116.84" x2="406.4" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-116.84" x2="398.78" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="398.78" y1="-116.84" x2="391.16" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="391.16" y1="-116.84" x2="391.16" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="398.78" y1="-119.38" x2="398.78" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-119.38" x2="406.4" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="414.02" y1="-119.38" x2="414.02" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-121.92" x2="431.8" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-116.84" x2="424.18" y2="-116.84" width="0.1524" layer="91"/>
<junction x="424.18" y="-116.84"/>
<junction x="398.78" y="-116.84"/>
<junction x="406.4" y="-116.84"/>
<junction x="414.02" y="-116.84"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<pinref part="C25" gate="G$1" pin="1"/>
<pinref part="IC5" gate="P" pin="VDD"/>
<pinref part="IC6" gate="P" pin="VDD"/>
<pinref part="IC8" gate="P" pin="VDD"/>
<pinref part="IC9" gate="P" pin="VDD"/>
<pinref part="C26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<wire x1="314.96" y1="154.94" x2="337.82" y2="154.94" width="0.1524" layer="91"/>
<label x="317.5" y="155.575" size="1.778" layer="95"/>
<pinref part="RN1" gate="B" pin="2"/>
<wire x1="337.82" y1="162.56" x2="337.82" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="231.14" y1="53.34" x2="175.26" y2="53.34" width="0.1524" layer="91"/>
<label x="179.705" y="53.975" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(SDA/INT1)PD1"/>
<pinref part="COMMUNICATION" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="213.36" y1="116.84" x2="231.14" y2="116.84" width="0.1524" layer="91"/>
<label x="213.36" y="116.84" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="2"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<wire x1="330.2" y1="157.48" x2="330.2" y2="162.56" width="0.1524" layer="91"/>
<wire x1="330.2" y1="157.48" x2="314.96" y2="157.48" width="0.1524" layer="91"/>
<label x="317.5" y="158.115" size="1.778" layer="95"/>
<pinref part="RN1" gate="C" pin="2"/>
</segment>
<segment>
<wire x1="231.14" y1="50.8" x2="175.26" y2="50.8" width="0.1524" layer="91"/>
<label x="179.705" y="51.435" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(SCL/INT0)PD0"/>
<pinref part="COMMUNICATION" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="213.36" y1="119.38" x2="231.14" y2="119.38" width="0.1524" layer="91"/>
<label x="213.36" y="119.38" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="1"/>
</segment>
</net>
<net name="ADC9" class="0">
<segment>
<wire x1="104.14" y1="35.56" x2="71.12" y2="35.56" width="0.1524" layer="91"/>
<label x="84.455" y="38.1" size="1.778" layer="95" rot="R180"/>
<pinref part="ADCH" gate="A" pin="2"/>
<pinref part="IC3" gate="1" pin="PK1(ADC9/PCINT17)"/>
</segment>
</net>
<net name="ADC8" class="0">
<segment>
<wire x1="71.12" y1="33.02" x2="104.14" y2="33.02" width="0.1524" layer="91"/>
<label x="84.455" y="35.56" size="1.778" layer="95" rot="R180"/>
<pinref part="ADCH" gate="A" pin="1"/>
<pinref part="IC3" gate="1" pin="PK0(ADC8/PCINT16)"/>
</segment>
</net>
<net name="ADC10" class="0">
<segment>
<wire x1="71.12" y1="38.1" x2="104.14" y2="38.1" width="0.1524" layer="91"/>
<label x="84.455" y="40.64" size="1.778" layer="95" rot="R180"/>
<pinref part="ADCH" gate="A" pin="3"/>
<pinref part="IC3" gate="1" pin="PK2(ADC10/PCINT18)"/>
</segment>
</net>
<net name="ADC11" class="0">
<segment>
<wire x1="104.14" y1="40.64" x2="71.12" y2="40.64" width="0.1524" layer="91"/>
<label x="84.455" y="43.18" size="1.778" layer="95" rot="R180"/>
<pinref part="ADCH" gate="A" pin="4"/>
<pinref part="IC3" gate="1" pin="PK3(ADC11/PCINT19)"/>
</segment>
</net>
<net name="ADC12" class="0">
<segment>
<wire x1="71.12" y1="43.18" x2="104.14" y2="43.18" width="0.1524" layer="91"/>
<label x="84.455" y="45.72" size="1.778" layer="95" rot="R180"/>
<pinref part="ADCH" gate="A" pin="5"/>
<pinref part="IC3" gate="1" pin="PK4(ADC12/PCINT20)"/>
</segment>
</net>
<net name="ADC13" class="0">
<segment>
<wire x1="104.14" y1="45.72" x2="71.12" y2="45.72" width="0.1524" layer="91"/>
<label x="84.455" y="48.26" size="1.778" layer="95" rot="R180"/>
<pinref part="ADCH" gate="A" pin="6"/>
<pinref part="IC3" gate="1" pin="PK5(ADC13/PCINT21)"/>
</segment>
</net>
<net name="ADC14" class="0">
<segment>
<wire x1="104.14" y1="48.26" x2="71.12" y2="48.26" width="0.1524" layer="91"/>
<label x="76.2" y="48.895" size="1.778" layer="95"/>
<pinref part="ADCH" gate="A" pin="7"/>
<pinref part="IC3" gate="1" pin="PK6(ADC14/PCINT22)"/>
</segment>
</net>
<net name="ADC15" class="0">
<segment>
<wire x1="71.12" y1="50.8" x2="104.14" y2="50.8" width="0.1524" layer="91"/>
<label x="76.2" y="51.435" size="1.778" layer="95"/>
<pinref part="ADCH" gate="A" pin="8"/>
<pinref part="IC3" gate="1" pin="PK7(ADC15/PCINT23)"/>
</segment>
</net>
<net name="PB3_MISO" class="0">
<segment>
<wire x1="261.62" y1="154.94" x2="279.4" y2="154.94" width="0.1524" layer="91"/>
<label x="266.7" y="154.94" size="1.778" layer="95"/>
<pinref part="ICSP" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="193.04" y1="104.14" x2="175.26" y2="104.14" width="0.1524" layer="91"/>
<label x="176.53" y="104.775" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(MISO/PCINT3)PB3"/>
</segment>
<segment>
<wire x1="393.7" y1="-55.88" x2="408.94" y2="-55.88" width="0.1524" layer="91"/>
<label x="393.7" y="-55.626" size="1.778" layer="95"/>
<pinref part="U7A" gate="G$1" pin="MISO"/>
</segment>
<segment>
<wire x1="91.44" y1="-55.88" x2="71.12" y2="-55.88" width="0.1524" layer="91"/>
<label x="85.09" y="-55.245" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="31"/>
</segment>
</net>
<net name="PB2_MOSI" class="0">
<segment>
<wire x1="287.02" y1="152.4" x2="299.72" y2="152.4" width="0.1524" layer="91"/>
<label x="292.1" y="152.4" size="1.778" layer="95"/>
<pinref part="ICSP" gate="A" pin="4"/>
</segment>
<segment>
<wire x1="193.04" y1="101.6" x2="175.26" y2="101.6" width="0.1524" layer="91"/>
<label x="176.53" y="102.235" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(MOSI/PCINT2)PB2"/>
</segment>
<segment>
<wire x1="492.76" y1="-81.28" x2="502.92" y2="-81.28" width="0.1524" layer="91"/>
<label x="495.3" y="-81.28" size="1.778" layer="95"/>
<pinref part="IC9" gate="A" pin="I"/>
</segment>
<segment>
<wire x1="63.5" y1="-55.88" x2="43.18" y2="-55.88" width="0.1524" layer="91"/>
<label x="43.815" y="-55.245" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="32"/>
</segment>
</net>
<net name="PB1_SCK" class="0">
<segment>
<wire x1="279.4" y1="152.4" x2="261.62" y2="152.4" width="0.1524" layer="91"/>
<label x="266.7" y="152.4" size="1.778" layer="95"/>
<pinref part="ICSP" gate="A" pin="3"/>
</segment>
<segment>
<wire x1="193.04" y1="99.06" x2="175.26" y2="99.06" width="0.1524" layer="91"/>
<label x="176.53" y="99.695" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(SCK/PCINT1)PB1"/>
</segment>
<segment>
<wire x1="492.76" y1="-40.64" x2="502.92" y2="-40.64" width="0.1524" layer="91"/>
<label x="495.3" y="-40.64" size="1.778" layer="95"/>
<pinref part="IC6" gate="A" pin="I"/>
</segment>
<segment>
<wire x1="91.44" y1="-53.34" x2="71.12" y2="-53.34" width="0.1524" layer="91"/>
<label x="85.09" y="-52.705" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="33"/>
</segment>
</net>
<net name="PB5" class="0">
<segment>
<wire x1="175.26" y1="109.22" x2="200.66" y2="109.22" width="0.1524" layer="91"/>
<wire x1="231.14" y1="104.14" x2="200.66" y2="104.14" width="0.1524" layer="91"/>
<wire x1="200.66" y1="104.14" x2="200.66" y2="109.22" width="0.1524" layer="91"/>
<label x="177.8" y="109.855" size="1.6764" layer="95"/>
<pinref part="IC3" gate="1" pin="(OC1A/PCINT5)PB5"/>
<pinref part="JP1" gate="A" pin="7"/>
</segment>
</net>
<net name="PB4" class="0">
<segment>
<wire x1="175.26" y1="106.68" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<wire x1="198.12" y1="106.68" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
<wire x1="198.12" y1="101.6" x2="231.14" y2="101.6" width="0.1524" layer="91"/>
<label x="177.8" y="107.315" size="1.6764" layer="95"/>
<pinref part="IC3" gate="1" pin="(OC2A/PCINT4)PB4"/>
<pinref part="JP1" gate="A" pin="8"/>
</segment>
</net>
<net name="PE5" class="0">
<segment>
<wire x1="195.58" y1="40.64" x2="175.26" y2="40.64" width="0.1524" layer="91"/>
<wire x1="231.14" y1="35.56" x2="200.66" y2="35.56" width="0.1524" layer="91"/>
<wire x1="200.66" y1="35.56" x2="195.58" y2="40.64" width="0.1524" layer="91"/>
<label x="176.53" y="41.275" size="1.6764" layer="95"/>
<label x="210.82" y="36.195" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(OC3C/INT5)PE5"/>
<pinref part="PWML" gate="A" pin="4"/>
</segment>
</net>
<net name="PE4" class="0">
<segment>
<wire x1="195.58" y1="38.1" x2="175.26" y2="38.1" width="0.1524" layer="91"/>
<wire x1="195.58" y1="38.1" x2="200.66" y2="33.02" width="0.1524" layer="91"/>
<wire x1="200.66" y1="33.02" x2="231.14" y2="33.02" width="0.1524" layer="91"/>
<label x="176.53" y="38.735" size="1.6764" layer="95"/>
<label x="210.82" y="33.655" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(OC3B/INT4)PE4"/>
<pinref part="PWML" gate="A" pin="3"/>
</segment>
</net>
<net name="PE3" class="0">
<segment>
<wire x1="200.66" y1="40.64" x2="195.58" y2="35.56" width="0.1524" layer="91"/>
<wire x1="195.58" y1="35.56" x2="175.26" y2="35.56" width="0.1524" layer="91"/>
<wire x1="231.14" y1="40.64" x2="200.66" y2="40.64" width="0.1524" layer="91"/>
<label x="176.53" y="36.195" size="1.6764" layer="95"/>
<label x="210.82" y="41.275" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(OC3A/AIN1)PE3"/>
<pinref part="PWML" gate="A" pin="6"/>
</segment>
</net>
<net name="PE1" class="0">
<segment>
<wire x1="231.14" y1="30.48" x2="175.26" y2="30.48" width="0.1524" layer="91"/>
<label x="176.53" y="31.115" size="1.6764" layer="95"/>
<label x="210.82" y="31.115" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(TXD0)PE1"/>
<pinref part="PWML" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="472.44" y1="5.08" x2="464.82" y2="5.08" width="0.1524" layer="91"/>
<wire x1="477.52" y1="2.54" x2="472.44" y2="2.54" width="0.1524" layer="91"/>
<wire x1="472.44" y1="2.54" x2="472.44" y2="5.08" width="0.1524" layer="91"/>
<label x="472.44" y="5.08" size="1.6764" layer="95" rot="MR0"/>
<pinref part="RN5" gate="A" pin="1"/>
</segment>
</net>
<net name="PE0" class="0">
<segment>
<wire x1="231.14" y1="27.94" x2="175.26" y2="27.94" width="0.1524" layer="91"/>
<label x="176.53" y="28.575" size="1.6764" layer="95"/>
<label x="210.82" y="28.575" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(RXD0/PCIN8)PE0"/>
<pinref part="PWML" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="464.82" y1="12.7" x2="472.44" y2="12.7" width="0.1524" layer="91"/>
<wire x1="477.52" y1="15.24" x2="472.44" y2="15.24" width="0.1524" layer="91"/>
<wire x1="472.44" y1="15.24" x2="472.44" y2="12.7" width="0.1524" layer="91"/>
<label x="472.44" y="12.7" size="1.6764" layer="95" rot="MR0"/>
<pinref part="RN5" gate="B" pin="1"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<wire x1="528.32" y1="17.78" x2="523.24" y2="17.78" width="0.1524" layer="91"/>
<pinref part="RX" gate="G$1" pin="A"/>
<pinref part="RN5" gate="D" pin="1"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="528.32" y1="40.64" x2="523.24" y2="40.64" width="0.1524" layer="91"/>
<wire x1="523.24" y1="40.64" x2="523.24" y2="33.02" width="0.1524" layer="91"/>
<pinref part="TX" gate="G$1" pin="A"/>
<pinref part="RN5" gate="C" pin="1"/>
</segment>
</net>
<net name="DTR" class="0">
<segment>
<wire x1="515.62" y1="81.28" x2="523.24" y2="81.28" width="0.1524" layer="91"/>
<wire x1="515.62" y1="45.72" x2="515.62" y2="81.28" width="0.1524" layer="91"/>
<wire x1="505.46" y1="48.26" x2="505.46" y2="45.72" width="0.1524" layer="91"/>
<wire x1="505.46" y1="45.72" x2="515.62" y2="45.72" width="0.1524" layer="91"/>
<wire x1="485.14" y1="45.72" x2="505.46" y2="45.72" width="0.1524" layer="91"/>
<junction x="505.46" y="45.72"/>
<label x="518.16" y="81.28" size="1.778" layer="95"/>
<pinref part="C10" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="(CTS/HWB/AIN6/TO/INT7)PD7"/>
<pinref part="RN2" gate="D" pin="1"/>
</segment>
</net>
<net name="USBVCC" class="3">
<segment>
<wire x1="411.48" y1="40.64" x2="429.26" y2="40.64" width="0.1524" layer="91"/>
<label x="412.115" y="40.64" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="UVCC"/>
</segment>
<segment>
<wire x1="370.84" y1="73.66" x2="358.14" y2="73.66" width="0.1524" layer="91"/>
<wire x1="358.14" y1="76.2" x2="358.14" y2="73.66" width="0.1524" layer="91"/>
<wire x1="358.14" y1="73.66" x2="353.06" y2="73.66" width="0.1524" layer="91"/>
<junction x="358.14" y="73.66"/>
<label x="363.22" y="73.66" size="1.778" layer="95"/>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="F1" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="182.88" y1="-55.88" x2="195.58" y2="-55.88" width="0.1524" layer="91"/>
<label x="185.42" y="-55.88" size="1.778" layer="95"/>
<pinref part="T1" gate="A" pin="D"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="208.28" y1="149.86" x2="205.74" y2="149.86" width="0.1524" layer="91"/>
<pinref part="L" gate="G$1" pin="A"/>
<pinref part="RN2" gate="A" pin="2"/>
</segment>
</net>
<net name="GATE_CMD" class="0">
<segment>
<wire x1="203.2" y1="-50.8" x2="203.2" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-43.18" x2="203.2" y2="-43.18" width="0.1524" layer="91"/>
<label x="185.42" y="-43.18" size="1.778" layer="95"/>
<pinref part="T1" gate="A" pin="G"/>
<pinref part="IC1" gate="B" pin="OUT"/>
</segment>
</net>
<net name="CMP" class="0">
<segment>
<wire x1="147.32" y1="-48.26" x2="147.32" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-48.26" x2="147.32" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-48.26" x2="132.08" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-48.26" x2="132.08" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-40.64" x2="165.1" y2="-40.64" width="0.1524" layer="91"/>
<junction x="147.32" y="-48.26"/>
<junction x="132.08" y="-48.26"/>
<label x="154.94" y="-40.64" size="1.778" layer="95"/>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="RN3" gate="C" pin="1"/>
<pinref part="RN3" gate="D" pin="1"/>
<pinref part="IC1" gate="B" pin="+IN"/>
</segment>
</net>
<net name="PB6" class="0">
<segment>
<wire x1="175.26" y1="111.76" x2="203.2" y2="111.76" width="0.1524" layer="91"/>
<wire x1="231.14" y1="106.68" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="203.2" y1="106.68" x2="203.2" y2="111.76" width="0.1524" layer="91"/>
<label x="177.8" y="112.395" size="1.6764" layer="95"/>
<pinref part="IC3" gate="1" pin="(OC1B/PCINT6)PB6"/>
<pinref part="JP1" gate="A" pin="6"/>
</segment>
</net>
<net name="PH3" class="0">
<segment>
<wire x1="81.28" y1="-5.08" x2="104.14" y2="-5.08" width="0.1524" layer="91"/>
<label x="93.98" y="-4.445" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PH3(OC4A)"/>
</segment>
<segment>
<wire x1="231.14" y1="43.18" x2="210.82" y2="43.18" width="0.1524" layer="91"/>
<label x="211.455" y="43.815" size="1.778" layer="95"/>
<pinref part="PWML" gate="A" pin="7"/>
</segment>
</net>
<net name="PH4" class="0">
<segment>
<wire x1="81.28" y1="-2.54" x2="104.14" y2="-2.54" width="0.1524" layer="91"/>
<label x="93.98" y="-1.905" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PH4(OC4B)"/>
</segment>
<segment>
<wire x1="231.14" y1="45.72" x2="210.82" y2="45.72" width="0.1524" layer="91"/>
<label x="211.455" y="46.355" size="1.778" layer="95"/>
<pinref part="PWML" gate="A" pin="8"/>
</segment>
<segment>
<wire x1="543.56" y1="-10.16" x2="548.64" y2="-10.16" width="0.1524" layer="91"/>
<label x="543.56" y="-7.62" size="1.778" layer="95" rot="MR180"/>
<pinref part="SJ1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PH5" class="0">
<segment>
<wire x1="81.28" y1="0" x2="104.14" y2="0" width="0.1524" layer="91"/>
<label x="93.98" y="0.635" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PH5(OC4C)"/>
</segment>
<segment>
<wire x1="231.14" y1="96.52" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<label x="212.09" y="97.155" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="10"/>
</segment>
</net>
<net name="PH6" class="0">
<segment>
<wire x1="81.28" y1="2.54" x2="104.14" y2="2.54" width="0.1524" layer="91"/>
<label x="93.98" y="3.175" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PH6(OC2B)"/>
</segment>
<segment>
<wire x1="210.82" y1="99.06" x2="231.14" y2="99.06" width="0.1524" layer="91"/>
<label x="212.09" y="99.695" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="9"/>
</segment>
</net>
<net name="PG5" class="0">
<segment>
<wire x1="205.74" y1="0" x2="175.26" y2="0" width="0.1524" layer="91"/>
<wire x1="205.74" y1="0" x2="205.74" y2="38.1" width="0.1524" layer="91"/>
<wire x1="231.14" y1="38.1" x2="205.74" y2="38.1" width="0.1524" layer="91"/>
<label x="177.8" y="0.635" size="1.778" layer="95"/>
<label x="211.455" y="38.735" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(OC0B)PG5"/>
<pinref part="PWML" gate="A" pin="5"/>
</segment>
</net>
<net name="RXD1" class="0">
<segment>
<wire x1="231.14" y1="55.88" x2="175.26" y2="55.88" width="0.1524" layer="91"/>
<label x="215.265" y="56.515" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(RXD1/INT2)PD2"/>
<pinref part="COMMUNICATION" gate="A" pin="3"/>
</segment>
</net>
<net name="TXD1" class="0">
<segment>
<wire x1="231.14" y1="58.42" x2="175.26" y2="58.42" width="0.1524" layer="91"/>
<label x="215.265" y="59.055" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(TXD1/INT3)PD3"/>
<pinref part="COMMUNICATION" gate="A" pin="4"/>
</segment>
</net>
<net name="RXD2" class="0">
<segment>
<wire x1="213.36" y1="60.96" x2="231.14" y2="60.96" width="0.1524" layer="91"/>
<label x="215.265" y="61.595" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="5"/>
</segment>
<segment>
<wire x1="91.44" y1="-12.7" x2="104.14" y2="-12.7" width="0.1524" layer="91"/>
<label x="93.98" y="-12.065" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PH0(RXD2)"/>
</segment>
</net>
<net name="RXD3" class="0">
<segment>
<wire x1="213.36" y1="66.04" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
<label x="215.265" y="66.675" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="7"/>
</segment>
<segment>
<wire x1="88.9" y1="10.16" x2="104.14" y2="10.16" width="0.1524" layer="91"/>
<label x="93.98" y="10.795" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PJ0(RXD3/PCINT9)"/>
</segment>
</net>
<net name="TXD2" class="0">
<segment>
<wire x1="213.36" y1="63.5" x2="231.14" y2="63.5" width="0.1524" layer="91"/>
<label x="215.265" y="64.135" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="6"/>
</segment>
<segment>
<wire x1="91.44" y1="-10.16" x2="104.14" y2="-10.16" width="0.1524" layer="91"/>
<label x="93.98" y="-9.525" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PH1(TXD2)"/>
</segment>
</net>
<net name="TXD3" class="0">
<segment>
<wire x1="231.14" y1="68.58" x2="213.36" y2="68.58" width="0.1524" layer="91"/>
<label x="215.265" y="69.215" size="1.778" layer="95"/>
<pinref part="COMMUNICATION" gate="A" pin="8"/>
</segment>
<segment>
<wire x1="88.9" y1="12.7" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<label x="93.98" y="13.335" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PJ1(TXD3/PCINT10)"/>
</segment>
</net>
<net name="PC0" class="0">
<segment>
<wire x1="190.5" y1="73.66" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
<label x="177.165" y="74.295" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(A8)PC0"/>
</segment>
<segment>
<wire x1="50.8" y1="-73.66" x2="63.5" y2="-73.66" width="0.1524" layer="91"/>
<label x="52.705" y="-73.025" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="18"/>
</segment>
</net>
<net name="PC1" class="0">
<segment>
<wire x1="190.5" y1="76.2" x2="175.26" y2="76.2" width="0.1524" layer="91"/>
<label x="177.165" y="76.835" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(A9)PC1"/>
</segment>
<segment>
<wire x1="88.9" y1="-73.66" x2="71.12" y2="-73.66" width="0.1524" layer="91"/>
<label x="78.105" y="-73.025" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="17"/>
</segment>
</net>
<net name="PC2" class="0">
<segment>
<wire x1="190.5" y1="78.74" x2="175.26" y2="78.74" width="0.1524" layer="91"/>
<label x="177.165" y="79.375" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(A10)PC2"/>
</segment>
<segment>
<wire x1="63.5" y1="-76.2" x2="50.8" y2="-76.2" width="0.1524" layer="91"/>
<label x="52.705" y="-75.565" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="16"/>
</segment>
</net>
<net name="PC3" class="0">
<segment>
<wire x1="190.5" y1="81.28" x2="175.26" y2="81.28" width="0.1524" layer="91"/>
<label x="177.165" y="81.915" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(A11)PC3"/>
</segment>
<segment>
<wire x1="71.12" y1="-76.2" x2="88.9" y2="-76.2" width="0.1524" layer="91"/>
<label x="78.105" y="-75.565" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="15"/>
</segment>
</net>
<net name="PC4" class="0">
<segment>
<wire x1="190.5" y1="83.82" x2="175.26" y2="83.82" width="0.1524" layer="91"/>
<label x="177.165" y="84.455" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(A12)PC4"/>
</segment>
<segment>
<wire x1="63.5" y1="-78.74" x2="50.8" y2="-78.74" width="0.1524" layer="91"/>
<label x="52.705" y="-78.105" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="14"/>
</segment>
</net>
<net name="PC5" class="0">
<segment>
<wire x1="190.5" y1="86.36" x2="175.26" y2="86.36" width="0.1524" layer="91"/>
<label x="177.165" y="86.995" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(A13)PC5"/>
</segment>
<segment>
<wire x1="71.12" y1="-78.74" x2="88.9" y2="-78.74" width="0.1524" layer="91"/>
<label x="78.105" y="-78.105" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="13"/>
</segment>
</net>
<net name="PC6" class="0">
<segment>
<wire x1="190.5" y1="88.9" x2="175.26" y2="88.9" width="0.1524" layer="91"/>
<label x="177.165" y="89.535" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(A14)PC6"/>
</segment>
<segment>
<wire x1="63.5" y1="-81.28" x2="50.8" y2="-81.28" width="0.1524" layer="91"/>
<label x="52.705" y="-80.645" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="12"/>
</segment>
</net>
<net name="PC7" class="0">
<segment>
<wire x1="190.5" y1="91.44" x2="175.26" y2="91.44" width="0.1524" layer="91"/>
<label x="177.165" y="92.075" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(A15)PC7"/>
</segment>
<segment>
<wire x1="71.12" y1="-81.28" x2="88.9" y2="-81.28" width="0.1524" layer="91"/>
<label x="78.105" y="-80.645" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="11"/>
</segment>
</net>
<net name="PB0_SS" class="0">
<segment>
<wire x1="193.04" y1="96.52" x2="175.26" y2="96.52" width="0.1524" layer="91"/>
<label x="176.53" y="97.155" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(SS/PCINT0)PB0"/>
</segment>
<segment>
<wire x1="535.94" y1="-58.42" x2="548.64" y2="-58.42" width="0.1524" layer="91"/>
<label x="538.48" y="-58.42" size="1.778" layer="95"/>
<pinref part="SJ2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="63.5" y1="-53.34" x2="43.18" y2="-53.34" width="0.1524" layer="91"/>
<label x="43.815" y="-52.705" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="34"/>
</segment>
</net>
<net name="PG0" class="0">
<segment>
<wire x1="190.5" y1="-12.7" x2="175.26" y2="-12.7" width="0.1524" layer="91"/>
<label x="179.07" y="-12.065" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(WR)PG0"/>
</segment>
<segment>
<wire x1="63.5" y1="-68.58" x2="43.18" y2="-68.58" width="0.1524" layer="91"/>
<label x="43.815" y="-67.945" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="22"/>
</segment>
</net>
<net name="PG1" class="0">
<segment>
<wire x1="190.5" y1="-10.16" x2="175.26" y2="-10.16" width="0.1524" layer="91"/>
<label x="179.07" y="-9.525" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(RD)PG1"/>
</segment>
<segment>
<wire x1="71.12" y1="-68.58" x2="91.44" y2="-68.58" width="0.1524" layer="91"/>
<label x="85.09" y="-67.945" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="21"/>
</segment>
</net>
<net name="PG2" class="0">
<segment>
<wire x1="190.5" y1="-7.62" x2="175.26" y2="-7.62" width="0.1524" layer="91"/>
<label x="179.07" y="-6.985" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(ALE)PG2"/>
</segment>
<segment>
<wire x1="63.5" y1="-71.12" x2="43.18" y2="-71.12" width="0.1524" layer="91"/>
<label x="43.815" y="-70.485" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="20"/>
</segment>
</net>
<net name="PD7" class="0">
<segment>
<wire x1="190.5" y1="68.58" x2="175.26" y2="68.58" width="0.1524" layer="91"/>
<label x="177.165" y="69.215" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(T0)PD7"/>
</segment>
<segment>
<wire x1="71.12" y1="-71.12" x2="91.44" y2="-71.12" width="0.1524" layer="91"/>
<label x="85.09" y="-70.485" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="19"/>
</segment>
</net>
<net name="PA0" class="0">
<segment>
<wire x1="175.26" y1="119.38" x2="187.96" y2="119.38" width="0.1524" layer="91"/>
<label x="177.165" y="120.015" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(AD0)PA0"/>
</segment>
<segment>
<wire x1="71.12" y1="-91.44" x2="88.9" y2="-91.44" width="0.1524" layer="91"/>
<label x="78.105" y="-90.805" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="3"/>
</segment>
</net>
<net name="PA1" class="0">
<segment>
<wire x1="187.96" y1="121.92" x2="175.26" y2="121.92" width="0.1524" layer="91"/>
<label x="177.165" y="122.555" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(AD1)PA1"/>
</segment>
<segment>
<wire x1="63.5" y1="-91.44" x2="50.8" y2="-91.44" width="0.1524" layer="91"/>
<label x="53.34" y="-90.805" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="4"/>
</segment>
</net>
<net name="PA2" class="0">
<segment>
<wire x1="187.96" y1="124.46" x2="175.26" y2="124.46" width="0.1524" layer="91"/>
<label x="177.165" y="125.095" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(AD2)PA2"/>
</segment>
<segment>
<wire x1="71.12" y1="-88.9" x2="88.9" y2="-88.9" width="0.1524" layer="91"/>
<label x="78.105" y="-88.265" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="5"/>
</segment>
</net>
<net name="PA3" class="0">
<segment>
<wire x1="187.96" y1="127" x2="175.26" y2="127" width="0.1524" layer="91"/>
<label x="177.165" y="127.635" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(AD3)PA3"/>
</segment>
<segment>
<wire x1="63.5" y1="-88.9" x2="50.8" y2="-88.9" width="0.1524" layer="91"/>
<label x="53.34" y="-88.265" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="6"/>
</segment>
</net>
<net name="PA4" class="0">
<segment>
<wire x1="187.96" y1="129.54" x2="175.26" y2="129.54" width="0.1524" layer="91"/>
<label x="177.165" y="130.175" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(AD4)PA4"/>
</segment>
<segment>
<wire x1="71.12" y1="-86.36" x2="88.9" y2="-86.36" width="0.1524" layer="91"/>
<label x="78.105" y="-85.725" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="7"/>
</segment>
</net>
<net name="PA5" class="0">
<segment>
<wire x1="187.96" y1="132.08" x2="175.26" y2="132.08" width="0.1524" layer="91"/>
<label x="177.165" y="132.715" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(AD5)PA5"/>
</segment>
<segment>
<wire x1="63.5" y1="-86.36" x2="50.8" y2="-86.36" width="0.1524" layer="91"/>
<label x="53.34" y="-85.725" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="8"/>
</segment>
</net>
<net name="PA6" class="0">
<segment>
<wire x1="187.96" y1="134.62" x2="175.26" y2="134.62" width="0.1524" layer="91"/>
<label x="177.165" y="135.255" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(AD6)PA6"/>
</segment>
<segment>
<wire x1="88.9" y1="-83.82" x2="71.12" y2="-83.82" width="0.1524" layer="91"/>
<label x="78.105" y="-83.185" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="9"/>
</segment>
</net>
<net name="PA7" class="0">
<segment>
<wire x1="187.96" y1="137.16" x2="175.26" y2="137.16" width="0.1524" layer="91"/>
<label x="177.165" y="137.795" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(AD7)PA7"/>
</segment>
<segment>
<wire x1="50.8" y1="-83.82" x2="63.5" y2="-83.82" width="0.1524" layer="91"/>
<label x="53.34" y="-83.185" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="10"/>
</segment>
</net>
<net name="PL0" class="0">
<segment>
<wire x1="104.14" y1="55.88" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<label x="85.09" y="56.515" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PL0(ICP4)"/>
</segment>
<segment>
<wire x1="63.5" y1="-58.42" x2="43.18" y2="-58.42" width="0.1524" layer="91"/>
<label x="43.815" y="-57.785" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="30"/>
</segment>
</net>
<net name="PL1" class="0">
<segment>
<wire x1="83.82" y1="58.42" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
<label x="85.09" y="59.055" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PL1(ICP5)"/>
</segment>
<segment>
<wire x1="71.12" y1="-58.42" x2="91.44" y2="-58.42" width="0.1524" layer="91"/>
<label x="85.09" y="-57.785" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="29"/>
</segment>
</net>
<net name="PL2" class="0">
<segment>
<wire x1="104.14" y1="60.96" x2="83.82" y2="60.96" width="0.1524" layer="91"/>
<label x="85.09" y="61.595" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PL2(T5)"/>
</segment>
<segment>
<wire x1="43.18" y1="-60.96" x2="63.5" y2="-60.96" width="0.1524" layer="91"/>
<label x="43.815" y="-60.325" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="28"/>
</segment>
</net>
<net name="PL3" class="0">
<segment>
<wire x1="83.82" y1="63.5" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
<label x="85.09" y="64.135" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PL3(OC5A)"/>
</segment>
<segment>
<wire x1="71.12" y1="-60.96" x2="91.44" y2="-60.96" width="0.1524" layer="91"/>
<label x="85.09" y="-60.325" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="27"/>
</segment>
</net>
<net name="PL4" class="0">
<segment>
<wire x1="104.14" y1="66.04" x2="83.82" y2="66.04" width="0.1524" layer="91"/>
<label x="85.09" y="66.675" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PL4(OC5B)"/>
</segment>
<segment>
<wire x1="63.5" y1="-63.5" x2="43.18" y2="-63.5" width="0.1524" layer="91"/>
<label x="43.815" y="-62.865" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="26"/>
</segment>
</net>
<net name="PL5" class="0">
<segment>
<wire x1="83.82" y1="68.58" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
<label x="85.09" y="69.215" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PL5(OC5C)"/>
</segment>
<segment>
<wire x1="71.12" y1="-63.5" x2="91.44" y2="-63.5" width="0.1524" layer="91"/>
<label x="85.09" y="-62.865" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="25"/>
</segment>
</net>
<net name="PL6" class="0">
<segment>
<wire x1="104.14" y1="71.12" x2="83.82" y2="71.12" width="0.1524" layer="91"/>
<label x="85.09" y="71.755" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PL6"/>
</segment>
<segment>
<wire x1="63.5" y1="-66.04" x2="43.18" y2="-66.04" width="0.1524" layer="91"/>
<label x="43.815" y="-65.405" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="24"/>
</segment>
</net>
<net name="PL7" class="0">
<segment>
<wire x1="104.14" y1="73.66" x2="83.82" y2="73.66" width="0.1524" layer="91"/>
<label x="85.09" y="74.295" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PL7"/>
</segment>
<segment>
<wire x1="91.44" y1="-66.04" x2="71.12" y2="-66.04" width="0.1524" layer="91"/>
<label x="85.09" y="-65.405" size="1.778" layer="95"/>
<pinref part="XIO" gate="A" pin="23"/>
</segment>
</net>
<net name="PB7" class="0">
<segment>
<wire x1="137.16" y1="157.48" x2="149.86" y2="157.48" width="0.1524" layer="91"/>
<wire x1="205.74" y1="109.22" x2="231.14" y2="109.22" width="0.1524" layer="91"/>
<wire x1="205.74" y1="114.3" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<wire x1="175.26" y1="114.3" x2="200.66" y2="114.3" width="0.1524" layer="91"/>
<wire x1="200.66" y1="114.3" x2="205.74" y2="114.3" width="0.1524" layer="91"/>
<wire x1="200.66" y1="144.78" x2="200.66" y2="114.3" width="0.1524" layer="91"/>
<wire x1="200.66" y1="144.78" x2="137.16" y2="144.78" width="0.1524" layer="91"/>
<wire x1="137.16" y1="144.78" x2="137.16" y2="157.48" width="0.1524" layer="91"/>
<junction x="200.66" y="114.3"/>
<label x="177.8" y="114.935" size="1.6764" layer="95"/>
<pinref part="IC1" gate="A" pin="+IN"/>
<pinref part="IC3" gate="1" pin="(OC0A/OC1C/PCINT7)PB7"/>
<pinref part="JP1" gate="A" pin="5"/>
</segment>
</net>
<net name="VUCAP" class="0">
<segment>
<wire x1="429.26" y1="43.18" x2="386.08" y2="43.18" width="0.1524" layer="91"/>
<wire x1="386.08" y1="43.18" x2="386.08" y2="33.02" width="0.1524" layer="91"/>
<label x="412.115" y="43.18" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="UCAP"/>
<pinref part="C16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RD-" class="0">
<segment>
<wire x1="429.26" y1="38.1" x2="393.7" y2="38.1" width="0.1524" layer="91"/>
<wire x1="383.54" y1="68.58" x2="393.7" y2="68.58" width="0.1524" layer="91"/>
<wire x1="393.7" y1="68.58" x2="393.7" y2="38.1" width="0.1524" layer="91"/>
<wire x1="383.54" y1="71.12" x2="383.54" y2="68.58" width="0.1524" layer="91"/>
<label x="386.08" y="68.58" size="1.778" layer="95"/>
<label x="412.115" y="38.1" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="D-"/>
<pinref part="RN4" gate="A" pin="1"/>
</segment>
</net>
<net name="RD+" class="0">
<segment>
<wire x1="391.16" y1="35.56" x2="429.26" y2="35.56" width="0.1524" layer="91"/>
<wire x1="383.54" y1="66.04" x2="391.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="391.16" y1="66.04" x2="391.16" y2="35.56" width="0.1524" layer="91"/>
<wire x1="383.54" y1="63.5" x2="383.54" y2="66.04" width="0.1524" layer="91"/>
<label x="386.08" y="66.04" size="1.778" layer="95"/>
<label x="412.115" y="35.56" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="D+"/>
<pinref part="RN4" gate="D" pin="1"/>
</segment>
</net>
<net name="RESET2" class="0">
<segment>
<wire x1="429.26" y1="81.28" x2="424.18" y2="81.28" width="0.1524" layer="91"/>
<wire x1="424.18" y1="81.28" x2="419.1" y2="81.28" width="0.1524" layer="91"/>
<wire x1="419.1" y1="81.28" x2="386.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="386.08" y1="81.28" x2="386.08" y2="99.06" width="0.1524" layer="91"/>
<wire x1="386.08" y1="99.06" x2="396.24" y2="99.06" width="0.1524" layer="91"/>
<wire x1="424.18" y1="91.44" x2="424.18" y2="81.28" width="0.1524" layer="91"/>
<junction x="424.18" y="81.28"/>
<label x="406.4" y="81.28" size="1.778" layer="95"/>
<label x="383.54" y="99.06" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="RESET(PC1/DW)"/>
<pinref part="ICSP1" gate="A" pin="5"/>
<pinref part="D4" gate="G$1" pin="A"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="419.1" y1="88.9" x2="419.1" y2="81.28" width="0.1524" layer="91"/>
<junction x="419.1" y="81.28"/>
</segment>
</net>
<net name="MISO2" class="0">
<segment>
<wire x1="485.14" y1="76.2" x2="502.92" y2="76.2" width="0.1524" layer="91"/>
<wire x1="502.92" y1="76.2" x2="502.92" y2="99.06" width="0.1524" layer="91"/>
<wire x1="502.92" y1="99.06" x2="426.72" y2="99.06" width="0.1524" layer="91"/>
<wire x1="426.72" y1="99.06" x2="426.72" y2="86.36" width="0.1524" layer="91"/>
<wire x1="426.72" y1="86.36" x2="381" y2="86.36" width="0.1524" layer="91"/>
<wire x1="381" y1="86.36" x2="381" y2="104.14" width="0.1524" layer="91"/>
<wire x1="381" y1="104.14" x2="396.24" y2="104.14" width="0.1524" layer="91"/>
<label x="486.41" y="76.2" size="1.778" layer="95"/>
<label x="383.54" y="104.14" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="(PD0/MISO/PCINT3)PB3"/>
<pinref part="ICSP1" gate="A" pin="1"/>
</segment>
</net>
<net name="MOSI2" class="0">
<segment>
<wire x1="485.14" y1="73.66" x2="505.46" y2="73.66" width="0.1524" layer="91"/>
<wire x1="505.46" y1="73.66" x2="505.46" y2="101.6" width="0.1524" layer="91"/>
<wire x1="505.46" y1="101.6" x2="403.86" y2="101.6" width="0.1524" layer="91"/>
<label x="486.41" y="73.66" size="1.778" layer="95"/>
<label x="411.48" y="101.6" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="(PDI/MOSI/PCINT2)PB2"/>
<pinref part="ICSP1" gate="A" pin="4"/>
</segment>
</net>
<net name="SCK2" class="0">
<segment>
<wire x1="485.14" y1="71.12" x2="500.38" y2="71.12" width="0.1524" layer="91"/>
<wire x1="500.38" y1="71.12" x2="500.38" y2="96.52" width="0.1524" layer="91"/>
<wire x1="500.38" y1="96.52" x2="429.26" y2="96.52" width="0.1524" layer="91"/>
<wire x1="429.26" y1="96.52" x2="429.26" y2="83.82" width="0.1524" layer="91"/>
<wire x1="429.26" y1="83.82" x2="383.54" y2="83.82" width="0.1524" layer="91"/>
<wire x1="383.54" y1="83.82" x2="383.54" y2="101.6" width="0.1524" layer="91"/>
<wire x1="383.54" y1="101.6" x2="396.24" y2="101.6" width="0.1524" layer="91"/>
<label x="486.41" y="71.12" size="1.778" layer="95"/>
<label x="383.54" y="101.6" size="1.778" layer="95"/>
<pinref part="IC4" gate="G$1" pin="(SCLK/PCINT1)PB1"/>
<pinref part="ICSP1" gate="A" pin="3"/>
</segment>
</net>
<net name="XVCC" class="3">
<segment>
<wire x1="320.04" y1="71.12" x2="320.04" y2="73.66" width="0.1524" layer="91"/>
<wire x1="320.04" y1="73.66" x2="342.9" y2="73.66" width="0.1524" layer="91"/>
<label x="330.2" y="73.66" size="1.778" layer="95"/>
<pinref part="X2" gate="G$1" pin="1"/>
<pinref part="F1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="RXL" class="0">
<segment>
<wire x1="508" y1="17.78" x2="515.62" y2="17.78" width="0.1524" layer="91"/>
<wire x1="508" y1="17.78" x2="508" y2="38.1" width="0.1524" layer="91"/>
<wire x1="508" y1="38.1" x2="485.14" y2="38.1" width="0.1524" layer="91"/>
<label x="490.22" y="38.1" size="1.778" layer="95"/>
<pinref part="RX" gate="G$1" pin="C"/>
<pinref part="IC4" gate="G$1" pin="(INT5/AIN3)PD4"/>
</segment>
</net>
<net name="TXL" class="0">
<segment>
<wire x1="513.08" y1="33.02" x2="515.62" y2="33.02" width="0.1524" layer="91"/>
<wire x1="485.14" y1="40.64" x2="513.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="513.08" y1="40.64" x2="513.08" y2="33.02" width="0.1524" layer="91"/>
<label x="490.22" y="40.64" size="1.778" layer="95"/>
<pinref part="TX" gate="G$1" pin="C"/>
<pinref part="IC4" gate="G$1" pin="(XCK/AIN4/PCINT12)PD5"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<wire x1="320.04" y1="68.58" x2="325.12" y2="68.58" width="0.1524" layer="91"/>
<wire x1="325.12" y1="68.58" x2="373.38" y2="68.58" width="0.1524" layer="91"/>
<wire x1="325.12" y1="58.42" x2="325.12" y2="68.58" width="0.1524" layer="91"/>
<wire x1="373.38" y1="71.12" x2="373.38" y2="68.58" width="0.1524" layer="91"/>
<junction x="325.12" y="68.58"/>
<label x="363.22" y="68.58" size="1.778" layer="95"/>
<pinref part="X2" gate="G$1" pin="2"/>
<pinref part="Z1" gate="G$1" pin="2"/>
<pinref part="RN4" gate="A" pin="2"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<wire x1="373.38" y1="66.04" x2="337.82" y2="66.04" width="0.1524" layer="91"/>
<wire x1="337.82" y1="66.04" x2="320.04" y2="66.04" width="0.1524" layer="91"/>
<wire x1="337.82" y1="58.42" x2="337.82" y2="66.04" width="0.1524" layer="91"/>
<wire x1="373.38" y1="63.5" x2="373.38" y2="66.04" width="0.1524" layer="91"/>
<junction x="337.82" y="66.04"/>
<label x="363.22" y="66.04" size="1.778" layer="95"/>
<pinref part="X2" gate="G$1" pin="3"/>
<pinref part="Z2" gate="G$1" pin="2"/>
<pinref part="RN4" gate="D" pin="2"/>
</segment>
</net>
<net name="UGND" class="2">
<segment>
<wire x1="350.52" y1="38.1" x2="358.14" y2="38.1" width="0.1524" layer="91"/>
<wire x1="358.14" y1="63.5" x2="358.14" y2="38.1" width="0.1524" layer="91"/>
<wire x1="320.04" y1="63.5" x2="358.14" y2="63.5" width="0.1524" layer="91"/>
<wire x1="358.14" y1="38.1" x2="358.14" y2="15.24" width="0.1524" layer="91"/>
<wire x1="358.14" y1="15.24" x2="386.08" y2="15.24" width="0.1524" layer="91"/>
<wire x1="386.08" y1="15.24" x2="408.94" y2="15.24" width="0.1524" layer="91"/>
<wire x1="408.94" y1="15.24" x2="408.94" y2="22.86" width="0.1524" layer="91"/>
<wire x1="408.94" y1="22.86" x2="408.94" y2="33.02" width="0.1524" layer="91"/>
<wire x1="408.94" y1="33.02" x2="429.26" y2="33.02" width="0.1524" layer="91"/>
<wire x1="386.08" y1="25.4" x2="386.08" y2="15.24" width="0.1524" layer="91"/>
<wire x1="411.48" y1="22.86" x2="408.94" y2="22.86" width="0.1524" layer="91"/>
<junction x="358.14" y="38.1"/>
<junction x="386.08" y="15.24"/>
<junction x="408.94" y="22.86"/>
<label x="368.3" y="53.34" size="1.778" layer="95" rot="R90"/>
<label x="411.48" y="33.02" size="1.778" layer="95"/>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="X2" gate="G$1" pin="4"/>
<pinref part="IC4" gate="G$1" pin="UGND"/>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GROUND" gate="1" pin="1"/>
</segment>
</net>
<net name="USHIELD" class="2">
<segment>
<wire x1="312.42" y1="43.18" x2="312.42" y2="55.88" width="0.1524" layer="91"/>
<wire x1="337.82" y1="48.26" x2="337.82" y2="43.18" width="0.1524" layer="91"/>
<wire x1="325.12" y1="43.18" x2="337.82" y2="43.18" width="0.1524" layer="91"/>
<wire x1="325.12" y1="48.26" x2="325.12" y2="43.18" width="0.1524" layer="91"/>
<wire x1="314.96" y1="55.88" x2="314.96" y2="43.18" width="0.1524" layer="91"/>
<wire x1="314.96" y1="43.18" x2="325.12" y2="43.18" width="0.1524" layer="91"/>
<wire x1="314.96" y1="43.18" x2="312.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="340.36" y1="38.1" x2="314.96" y2="38.1" width="0.1524" layer="91"/>
<wire x1="314.96" y1="38.1" x2="314.96" y2="43.18" width="0.1524" layer="91"/>
<wire x1="350.52" y1="-38.1" x2="350.52" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="350.52" y1="-43.18" x2="358.14" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-43.18" x2="358.14" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="350.52" y1="-43.18" x2="335.28" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-43.18" x2="314.96" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="314.96" y1="-43.18" x2="314.96" y2="38.1" width="0.1524" layer="91"/>
<wire x1="332.74" y1="-33.02" x2="335.28" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-33.02" x2="335.28" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-35.56" x2="335.28" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="332.74" y1="-35.56" x2="335.28" y2="-35.56" width="0.1524" layer="91"/>
<junction x="325.12" y="43.18"/>
<junction x="314.96" y="43.18"/>
<junction x="314.96" y="38.1"/>
<junction x="350.52" y="-43.18"/>
<junction x="335.28" y="-43.18"/>
<junction x="335.28" y="-35.56"/>
<label x="312.42" y="43.18" size="1.778" layer="95" rot="R90"/>
<label x="320.04" y="38.1" size="1.778" layer="95"/>
<pinref part="X2" gate="G$1" pin="P$2"/>
<pinref part="Z2" gate="G$1" pin="1"/>
<pinref part="Z1" gate="G$1" pin="1"/>
<pinref part="X2" gate="G$1" pin="P$1"/>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="Z3" gate="G$1" pin="1"/>
<pinref part="Z4" gate="G$1" pin="1"/>
<pinref part="X3" gate="G$1" pin="S1"/>
<pinref part="X3" gate="G$1" pin="S2"/>
</segment>
</net>
<net name="XTAL2" class="0">
<segment>
<wire x1="81.28" y1="134.62" x2="88.9" y2="134.62" width="0.1524" layer="91"/>
<wire x1="88.9" y1="134.62" x2="88.9" y2="132.08" width="0.1524" layer="91"/>
<wire x1="88.9" y1="132.08" x2="104.14" y2="132.08" width="0.1524" layer="91"/>
<wire x1="55.88" y1="132.08" x2="55.88" y2="137.16" width="0.1524" layer="91"/>
<wire x1="55.88" y1="137.16" x2="81.28" y2="137.16" width="0.1524" layer="91"/>
<wire x1="81.28" y1="137.16" x2="81.28" y2="134.62" width="0.1524" layer="91"/>
<label x="91.44" y="132.08" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="XTAL2"/>
<pinref part="Y1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="XTAL1" class="0">
<segment>
<wire x1="55.88" y1="116.84" x2="55.88" y2="127" width="0.1524" layer="91"/>
<wire x1="104.14" y1="127" x2="88.9" y2="127" width="0.1524" layer="91"/>
<wire x1="88.9" y1="127" x2="88.9" y2="124.46" width="0.1524" layer="91"/>
<wire x1="88.9" y1="124.46" x2="81.28" y2="124.46" width="0.1524" layer="91"/>
<wire x1="81.28" y1="124.46" x2="81.28" y2="116.84" width="0.1524" layer="91"/>
<wire x1="55.88" y1="116.84" x2="81.28" y2="116.84" width="0.1524" layer="91"/>
<label x="63.5" y="116.84" size="1.778" layer="95"/>
<label x="91.44" y="127" size="1.778" layer="95"/>
<pinref part="Y1" gate="G$1" pin="3"/>
<pinref part="IC3" gate="1" pin="XTAL1"/>
</segment>
</net>
<net name="XT2" class="0">
<segment>
<wire x1="416.56" y1="76.2" x2="411.48" y2="76.2" width="0.1524" layer="91"/>
<wire x1="429.26" y1="76.2" x2="416.56" y2="76.2" width="0.1524" layer="91"/>
<wire x1="429.26" y1="76.2" x2="429.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="416.56" y1="73.66" x2="416.56" y2="76.2" width="0.1524" layer="91"/>
<junction x="416.56" y="76.2"/>
<label x="419.1" y="76.2" size="1.778" layer="95"/>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="XTAL2(PC0)"/>
<pinref part="Y2" gate="G$1" pin="C2"/>
</segment>
</net>
<net name="XT1" class="0">
<segment>
<wire x1="416.56" y1="66.04" x2="411.48" y2="66.04" width="0.1524" layer="91"/>
<wire x1="429.26" y1="66.04" x2="416.56" y2="66.04" width="0.1524" layer="91"/>
<wire x1="429.26" y1="66.04" x2="429.26" y2="68.58" width="0.1524" layer="91"/>
<wire x1="416.56" y1="68.58" x2="416.56" y2="66.04" width="0.1524" layer="91"/>
<junction x="416.56" y="66.04"/>
<label x="419.1" y="63.5" size="1.778" layer="95"/>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="XTAL1"/>
<pinref part="Y2" gate="G$1" pin="C1"/>
</segment>
</net>
<net name="L13" class="0">
<segment>
<wire x1="149.86" y1="152.4" x2="147.32" y2="152.4" width="0.1524" layer="91"/>
<wire x1="147.32" y1="152.4" x2="147.32" y2="147.32" width="0.1524" layer="91"/>
<wire x1="147.32" y1="147.32" x2="167.64" y2="147.32" width="0.1524" layer="91"/>
<wire x1="167.64" y1="147.32" x2="167.64" y2="154.94" width="0.1524" layer="91"/>
<wire x1="167.64" y1="154.94" x2="165.1" y2="154.94" width="0.1524" layer="91"/>
<wire x1="180.34" y1="149.86" x2="195.58" y2="149.86" width="0.1524" layer="91"/>
<wire x1="167.64" y1="154.94" x2="180.34" y2="154.94" width="0.1524" layer="91"/>
<wire x1="180.34" y1="154.94" x2="180.34" y2="149.86" width="0.1524" layer="91"/>
<junction x="167.64" y="154.94"/>
<label x="182.88" y="149.86" size="1.778" layer="95"/>
<pinref part="IC1" gate="A" pin="-IN"/>
<pinref part="IC1" gate="A" pin="OUT"/>
<pinref part="RN2" gate="A" pin="1"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="358.14" y1="-22.86" x2="373.38" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-22.86" x2="373.38" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-20.32" x2="375.92" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-27.94" x2="358.14" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="335.28" y1="-22.86" x2="358.14" y2="-22.86" width="0.1524" layer="91"/>
<junction x="358.14" y="-22.86"/>
<pinref part="RN7" gate="A" pin="1"/>
<pinref part="Z4" gate="G$1" pin="2"/>
<pinref part="X3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="350.52" y1="-25.4" x2="373.38" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-25.4" x2="373.38" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="373.38" y1="-27.94" x2="375.92" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="350.52" y1="-27.94" x2="350.52" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="350.52" y1="-25.4" x2="335.28" y2="-25.4" width="0.1524" layer="91"/>
<junction x="350.52" y="-25.4"/>
<pinref part="RN7" gate="D" pin="1"/>
<pinref part="Z3" gate="G$1" pin="2"/>
<pinref part="X3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="386.08" y1="-20.32" x2="391.16" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="391.16" y1="-20.32" x2="391.16" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="391.16" y1="-30.48" x2="408.94" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U7A" gate="G$1" pin="D-"/>
<pinref part="RN7" gate="A" pin="2"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="386.08" y1="-27.94" x2="388.62" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-27.94" x2="388.62" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="388.62" y1="-33.02" x2="408.94" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="U7A" gate="G$1" pin="D+"/>
<pinref part="RN7" gate="D" pin="2"/>
</segment>
</net>
<net name="INT_MAX" class="0">
<segment>
<wire x1="406.4" y1="-22.86" x2="406.4" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-25.4" x2="408.94" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-25.4" x2="396.24" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="396.24" y1="-25.4" x2="396.24" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="396.24" y1="-15.24" x2="388.62" y2="-15.24" width="0.1524" layer="91"/>
<junction x="406.4" y="-25.4"/>
<label x="398.78" y="-12.7" size="1.778" layer="95" rot="R180"/>
<pinref part="U7A" gate="G$1" pin="INT"/>
<pinref part="RN6" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="469.9" y1="-137.16" x2="485.14" y2="-137.16" width="0.1524" layer="91"/>
<label x="469.9" y="-137.16" size="1.778" layer="95"/>
<pinref part="SJ3" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="381" y1="-43.18" x2="408.94" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="381" y1="-45.72" x2="381" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="375.92" y1="-43.18" x2="381" y2="-43.18" width="0.1524" layer="91"/>
<junction x="381" y="-43.18"/>
<pinref part="U7A" gate="G$1" pin="XI"/>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="Y3" gate="G$1" pin="C1"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="408.94" y1="-38.1" x2="386.08" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="386.08" y1="-38.1" x2="386.08" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="386.08" y1="-33.02" x2="365.76" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="365.76" y1="-33.02" x2="365.76" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="365.76" y1="-43.18" x2="365.76" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="370.84" y1="-43.18" x2="365.76" y2="-43.18" width="0.1524" layer="91"/>
<junction x="365.76" y="-43.18"/>
<pinref part="U7A" gate="G$1" pin="XO"/>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="Y3" gate="G$1" pin="C2"/>
</segment>
</net>
<net name="RST_3V3" class="0">
<segment>
<wire x1="393.7" y1="-48.26" x2="408.94" y2="-48.26" width="0.1524" layer="91"/>
<label x="393.7" y="-48.006" size="1.778" layer="95"/>
<pinref part="U7A" gate="G$1" pin="RES"/>
</segment>
<segment>
<wire x1="462.28" y1="-20.32" x2="472.44" y2="-20.32" width="0.1524" layer="91"/>
<label x="462.28" y="-20.32" size="1.778" layer="95"/>
<pinref part="IC5" gate="A" pin="O"/>
</segment>
</net>
<net name="SCK_3V3" class="0">
<segment>
<wire x1="393.7" y1="-50.8" x2="408.94" y2="-50.8" width="0.1524" layer="91"/>
<label x="393.7" y="-50.546" size="1.778" layer="95"/>
<pinref part="U7A" gate="G$1" pin="SCK"/>
</segment>
<segment>
<wire x1="462.28" y1="-40.64" x2="472.44" y2="-40.64" width="0.1524" layer="91"/>
<label x="462.28" y="-40.64" size="1.778" layer="95"/>
<pinref part="IC6" gate="A" pin="O"/>
</segment>
</net>
<net name="SS_3V3" class="0">
<segment>
<wire x1="393.7" y1="-53.34" x2="408.94" y2="-53.34" width="0.1524" layer="91"/>
<label x="393.7" y="-53.086" size="1.778" layer="95"/>
<pinref part="U7A" gate="G$1" pin="SS"/>
</segment>
<segment>
<wire x1="462.28" y1="-60.96" x2="472.44" y2="-60.96" width="0.1524" layer="91"/>
<label x="462.28" y="-60.96" size="1.778" layer="95"/>
<pinref part="IC8" gate="A" pin="O"/>
</segment>
</net>
<net name="MOSI_3V3" class="0">
<segment>
<wire x1="393.7" y1="-58.42" x2="408.94" y2="-58.42" width="0.1524" layer="91"/>
<label x="393.7" y="-58.166" size="1.778" layer="95"/>
<pinref part="U7A" gate="G$1" pin="MOSI"/>
</segment>
<segment>
<wire x1="462.28" y1="-81.28" x2="472.44" y2="-81.28" width="0.1524" layer="91"/>
<label x="462.28" y="-81.28" size="1.778" layer="95"/>
<pinref part="IC9" gate="A" pin="O"/>
</segment>
</net>
<net name="GPX_MAX" class="0">
<segment>
<wire x1="393.7" y1="-60.96" x2="408.94" y2="-60.96" width="0.1524" layer="91"/>
<label x="393.446" y="-60.706" size="1.778" layer="95"/>
<pinref part="U7A" gate="G$1" pin="GPX"/>
</segment>
<segment>
<wire x1="469.9" y1="-124.46" x2="482.6" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="482.6" y1="-124.46" x2="482.6" y2="-127" width="0.1524" layer="91"/>
<wire x1="482.6" y1="-127" x2="513.08" y2="-127" width="0.1524" layer="91"/>
<label x="469.9" y="-124.46" size="1.778" layer="95"/>
<label x="500.38" y="-127" size="1.6764" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="104.14" y1="17.78" x2="88.9" y2="17.78" width="0.1524" layer="91"/>
<label x="88.9" y="17.78" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PJ3(PCINT12)"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="408.94" y1="-66.04" x2="406.4" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="406.4" y1="-66.04" x2="406.4" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="U7A" gate="G$1" pin="VBCOMP"/>
<pinref part="C24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="347.98" y1="-5.08" x2="342.9" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="342.9" y1="-5.08" x2="342.9" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="342.9" y1="-20.32" x2="335.28" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="F2" gate="G$1" pin="2"/>
<pinref part="X3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="358.14" y1="-5.08" x2="363.22" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="363.22" y1="-5.08" x2="368.3" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="363.22" y1="-7.62" x2="363.22" y2="-5.08" width="0.1524" layer="91"/>
<junction x="363.22" y="-5.08"/>
<pinref part="F2" gate="G$1" pin="1"/>
<pinref part="L3" gate="G$1" pin="2"/>
<pinref part="C17" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PE6" class="0">
<segment>
<wire x1="513.08" y1="-139.7" x2="495.3" y2="-139.7" width="0.1524" layer="91"/>
<label x="500.38" y="-139.7" size="1.6764" layer="95" rot="MR0"/>
<pinref part="SJ3" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="175.26" y1="43.18" x2="190.5" y2="43.18" width="0.1524" layer="91"/>
<label x="177.8" y="43.18" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="(T3/INT6)PE6"/>
</segment>
</net>
<net name="PJ2" class="0">
<segment>
<wire x1="548.64" y1="-15.24" x2="543.56" y2="-15.24" width="0.1524" layer="91"/>
<label x="543.56" y="-12.7" size="1.6764" layer="95" rot="MR180"/>
<pinref part="SJ1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="104.14" y1="15.24" x2="88.9" y2="15.24" width="0.1524" layer="91"/>
<label x="88.9" y="15.24" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PJ2(XCK3/PCINT11)"/>
</segment>
</net>
<net name="RST_5V" class="0">
<segment>
<wire x1="515.62" y1="-12.7" x2="523.24" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="523.24" y1="-12.7" x2="533.4" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="492.76" y1="-20.32" x2="515.62" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="515.62" y1="-20.32" x2="515.62" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="523.24" y1="-12.7" x2="523.24" y2="-17.78" width="0.1524" layer="91"/>
<junction x="523.24" y="-12.7"/>
<label x="518.16" y="-10.16" size="1.6764" layer="95" rot="MR180"/>
<label x="495.3" y="-20.32" size="1.778" layer="95"/>
<pinref part="SJ1" gate="G$1" pin="C"/>
<pinref part="IC5" gate="A" pin="I"/>
<pinref part="RN6" gate="D" pin="1"/>
</segment>
</net>
<net name="PJ6" class="0">
<segment>
<wire x1="104.14" y1="25.4" x2="88.9" y2="25.4" width="0.1524" layer="91"/>
<label x="88.9" y="25.4" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PJ6(PCINT15)"/>
</segment>
<segment>
<wire x1="513.08" y1="-134.62" x2="495.3" y2="-134.62" width="0.1524" layer="91"/>
<label x="495.3" y="-134.62" size="1.778" layer="95"/>
<pinref part="SJ3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PH7" class="0">
<segment>
<wire x1="104.14" y1="5.08" x2="88.9" y2="5.08" width="0.1524" layer="91"/>
<label x="88.9" y="5.08" size="1.778" layer="95"/>
<pinref part="IC3" gate="1" pin="PH7(T4)"/>
</segment>
<segment>
<wire x1="548.64" y1="-63.5" x2="535.94" y2="-63.5" width="0.1524" layer="91"/>
<label x="543.56" y="-66.04" size="1.778" layer="95"/>
<pinref part="SJ2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SS_MAX5" class="0">
<segment>
<wire x1="492.76" y1="-60.96" x2="518.16" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="518.16" y1="-60.96" x2="518.16" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="525.78" y1="-60.96" x2="518.16" y2="-60.96" width="0.1524" layer="91"/>
<junction x="518.16" y="-60.96"/>
<label x="495.3" y="-60.96" size="1.778" layer="95"/>
<pinref part="IC8" gate="A" pin="I"/>
<pinref part="RN6" gate="B" pin="1"/>
<pinref part="SJ2" gate="G$1" pin="C"/>
</segment>
</net>
<net name="AVCC" class="3">
<segment>
<pinref part="C27" gate="G$1" pin="1"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="43.18" y1="111.76" x2="35.56" y2="111.76" width="0.1524" layer="91"/>
<wire x1="35.56" y1="111.76" x2="35.56" y2="114.3" width="0.1524" layer="91"/>
<junction x="35.56" y="111.76"/>
<wire x1="35.56" y1="114.3" x2="83.82" y2="114.3" width="0.1524" layer="91"/>
<wire x1="83.82" y1="114.3" x2="83.82" y2="119.38" width="0.1524" layer="91"/>
<pinref part="IC3" gate="1" pin="AVCC"/>
<wire x1="104.14" y1="119.38" x2="83.82" y2="119.38" width="0.1524" layer="91"/>
<label x="91.44" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="FB"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="454.66" y1="147.32" x2="467.36" y2="147.32" width="0.1524" layer="91"/>
<wire x1="467.36" y1="147.32" x2="467.36" y2="149.86" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="467.36" y1="144.78" x2="467.36" y2="147.32" width="0.1524" layer="91"/>
<junction x="467.36" y="147.32"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="EN"/>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="424.18" y1="152.4" x2="421.64" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="AAM"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="424.18" y1="144.78" x2="416.56" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
