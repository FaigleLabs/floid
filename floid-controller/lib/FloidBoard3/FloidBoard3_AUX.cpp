//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_AUX.cpp:
#include <FloidBoard3.h>
// Aux logic:
boolean       auxDeployed                                    = false;
unsigned long auxFireTime                                    = 0ul;
// AUX:
boolean setupAux()
{
  // Setup Aux:
  pinMode(auxOnPin, OUTPUT);
  digitalWrite(auxOnPin, LOW);
  AUX_SERIAL.begin(AUX_BAUD_RATE);
  floidStatus.auxOn = false;
  auxDeployed       = false;
  return true;
}
void startAux()
{
  if(floidStatus.debug && floidStatus.debugAUX)
  {
    debugPrintlnToken(FLOID_DEBUG_AUX_ON);
  }
  digitalWrite(auxOnPin, HIGH);
  auxDeployed  = true;
  auxFireTime  = millis();
  floidStatus.auxOn        = true;
}
void loopAux()
{
  loopAuxSerial();
}
void loopAuxSerial()
{
  if(floidStatus.auxOn)
    {
      while(AUX_SERIAL.available())
	{
	  byte b = AUX_SERIAL.read();
	  debugPrint(b);
	}
    }
}
void stopAux()
{
  if(floidStatus.debug && floidStatus.debugAUX)
  {
    debugPrintlnToken(FLOID_DEBUG_AUX_ON);
  }
  digitalWrite(auxOnPin, LOW);
  floidStatus.auxOn = false;
}
