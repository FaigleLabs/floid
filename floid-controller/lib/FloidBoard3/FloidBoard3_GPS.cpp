//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_GPS.cpp:
#include <FloidBoard3.h>

#define            GPS_ASYNC_DELAY_0                (1000ul)
#define            GPS_ASYNC_DELAY_1                (500ul)
#define            GPS_ASYNC_DELAY_2                (200ul)
extern boolean     hasNewDeviceGPS;
const int          baudRatesToTry[]                  = {PMTK_BAUD_RATE_9600, PMTK_BAUD_RATE_19200, PMTK_BAUD_RATE_38400, PMTK_BAUD_RATE_57600, PMTK_BAUD_RATE_115200};

boolean            gpsDeployed                       = false;
unsigned long      gpsFireTime                       = 0ul;
NMEA               gps(ALL);                                   // GPS data connection to all sentence types for GPS
float              gpsX[NUMBER_GPS_READINGS];
float              gpsY[NUMBER_GPS_READINGS];
float              gpsZ[NUMBER_GPS_READINGS];
float              gpsZVelocity;
float              gpsDOG[NUMBER_GPS_READINGS]; // Direction over ground - in std trig degrees NOT compass degrees
float              gpsVOG[NUMBER_GPS_READINGS]; // Velocity currently moving the calculated Direction over Ground
unsigned long      gpsTimes[NUMBER_GPS_READINGS];
boolean            gpsNewReading                     = false;
unsigned long      gpsLastGoodReadTime               = 0ul;
// GPS Emulation and interval:
unsigned long      lastGPSEmulateTime                = 0ul;
unsigned int       gpsRateInMilliSeconds             = 1000u;  // Actual set up is in initial setGPSRate
// GPS Device time and good rate:
//unsigned long      lastGPSDeviceTime                 = 0ul;
//unsigned int       goodDeviceReadRateInMilliSeconds  = 3000u;
// GPS Kalman filters:
aKalman aKalmanGPS_X(GPS_X_KALMAN_DP, GPS_X_KALMAN_MP, GPS_X_KALMAN_CP, GPS_X_KALMAN_PROCESS_NOISE_COV_INIT, GPS_X_KALMAN_MEASUREMENT_NOISE_COV_INIT, GPS_X_KALMAN_PRINT_MATRICES);
aKalman aKalmanGPS_Y(GPS_Y_KALMAN_DP, GPS_Y_KALMAN_MP, GPS_Y_KALMAN_CP, GPS_Y_KALMAN_PROCESS_NOISE_COV_INIT, GPS_Y_KALMAN_MEASUREMENT_NOISE_COV_INIT, GPS_Y_KALMAN_PRINT_MATRICES);
// Note: Can Kalman filter Z or not to save memory - defined in FloidBoard3_GPS.h
#ifdef  GPS_USE_KALMAN_FILTER_FOR_Z
aKalman aKalmanGPS_Z(GPS_Z_KALMAN_DP, GPS_Z_KALMAN_MP, GPS_Z_KALMAN_CP, GPS_Z_KALMAN_PROCESS_NOISE_COV_INIT, GPS_Z_KALMAN_MEASUREMENT_NOISE_COV_INIT, GPS_Z_KALMAN_PRINT_MATRICES);
#endif  /* GPS_USE_KALMAN_FILTER_FOR_Z */
// GPS Setup:
boolean setupGPS()
{
  if(floidStatus.debug && floidStatus.debugGPS)
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_GPS_SETUP);
  }
  // Default is not emulation:
  floidStatus.emulateGPS = false;
  // Default is not device gps:
  floidStatus.deviceGPS = false;
  // Set up GPS filters:
  if(!aKalmanGPS_X.init())
  {
    if(floidStatus.debug && floidStatus.debugGPS)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_GPS_KALMAN_X_INIT_FAIL);
    }
    return false;
  }
  if(!aKalmanGPS_Y.init())
  {
    if(floidStatus.debug && floidStatus.debugGPS)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_GPS_KALMAN_Y_INIT_FAIL);
    }
    return false;
  }
#ifdef  GPS_USE_KALMAN_FILTER_FOR_Z
  if(floidStatus.debug && floidStatus.debugGPS)
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_GPS_USING_Z_KALMAN);
  }
  if(!aKalmanGPS_Z.init())
  {
    if(floidStatus.debug && floidStatus.debugGPS)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_GPS_KALMAN_Z_INIT_FAIL);
    }
    return false;
  }
#endif  /* GPS_USE_KALMAN_FILTER_FOR_Z */
  if(floidStatus.debug && floidStatus.debugGPS)
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_GPS_KALMAN_INITIALIZED_SETTING_RATE);
  }
  // Setup GPS:
  pinMode(gpsOnPin, OUTPUT);
  // Start the PMTK handler:
  pmtkHandler.pmtkResetStatus();
  // Physically turn on the gps:
  floidStatus.gpsOn                = true;
  digitalWrite(gpsOnPin, HIGH);
  // and wait:
  asyncDelay(GPS_ASYNC_DELAY_0);

  // Now set up the gps by connecting at different baud rates and instructing it to switch to our desired rate:
  pmtkHandler.pmtkSetupGpsSerial(5, baudRatesToTry, PMTK_BAUD_RATE_38400);
  pmtkHandler.processPmtkCommandResponse(true);
  // And then connect at that rate:
  pmtkHandler.pmtkConnectToGps(PMTK_BAUD_RATE_38400);
  pmtkHandler.processPmtkCommandResponse(true);
  // We add two additional processes to catch any junk:
  pmtkHandler.processPmtkCommandResponse(false);
  pmtkHandler.processPmtkCommandResponse(false);
  // Turn all the sentences all off - they default to a rate of zero
  pmtkHandler.pmtkSendNmeaSentences();
  pmtkHandler.processPmtkCommandResponse(true);
  // Enable DPGS to WAAS mode and Query:
  pmtkHandler.pmtkSendSetDgpsMode(PMTK_DGPS_MODE_WAAS);
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryDgpsMode();
  pmtkHandler.processPmtkCommandResponse(true);
  // Enable SBAS and Query:
  pmtkHandler.pmtkSendSetSbasEnabled(true);
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQuerySbasEnabled();
  pmtkHandler.processPmtkCommandResponse(true);
  // Set SBAS not in test and Query::
  pmtkHandler.pmtkSendSetSbasInTest(false);
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQuerySbasInTest();
  pmtkHandler.processPmtkCommandResponse(true);
  // Query NMEA sentences:
  pmtkHandler.pmtkSendQueryNmeaOutput();
  pmtkHandler.processPmtkCommandResponse(true);
  // Send and query fix rate:
  pmtkHandler.pmtkSendSetFixRate(100);// THIS IS HOW LONG WE DELAY BETWEEN FIXES
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryFixRate();
  pmtkHandler.processPmtkCommandResponse(true);
  // Set it to have our sentence GGA at the top rate of once per output:
  pmtkHandler.pmtkSetNmeaSentenceRate(PMTK_NMEA_MASK_SEN_GGA, 1);
  pmtkHandler.pmtkSendNmeaSentences();
  pmtkHandler.processPmtkCommandResponse(true);
  // Set the gps rate (This is tokenized: e.g. 0=100 ms = 10Hz)
  setGPSRate();
  /*
  // For completeness, we will query every thing else:
  pmtkHandler.pmtkSendQueryDatum();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryDatumAdvanced();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryAic();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryUserOption();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryFirmwareInfo();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryGpsMode();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryEpoStatus();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryLocusData(true);
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryAvailableEph(2400);
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryAvailableAlm(7);
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryEnableEasy();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryBtMacAddress();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryFirmwareVersion();
  pmtkHandler.processPmtkCommandResponse(true);
  pmtkHandler.pmtkSendQueryPwrSaveMode();
  pmtkHandler.processPmtkCommandResponse(true);
  */
  // OK, started:
  floidStatus.gpsOn                = false;
  gpsDeployed                      = false;
  gpsLastGoodReadTime              = 0ul;
  floidStatus.gpsGoodCount         = 0ul;
  gpsNewReading                    = false;
  gpsZVelocity                     = 0.0;
  digitalWrite(gpsOnPin, LOW);
  // Note: Leaves GPS_SERIAL on and set-up but the unit off and its status as off and undeployed
  if(floidStatus.debug && floidStatus.debugGPS)
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_GPS_SETUP_DONE);
  }
  return true;
}
void startGPS()
{
  if(floidStatus.debug && floidStatus.debugGPS)
  {
    debugPrintlnToken(FLOID_DEBUG_GPS_START);
  }
  // Power on GPS:
  digitalWrite(gpsOnPin, HIGH);
  gpsDeployed                      = true;
  gpsFireTime                      = millis();
  floidStatus.gpsOn                = true;
  gpsLastGoodReadTime              = 0ul;
  floidStatus.gpsGoodCount         = 0ul;
  gpsNewReading                    = false;
  gpsZVelocity                     = 0.0;
  for(int i=0; i<NUMBER_GPS_READINGS; ++i)
  {
    gpsVOG[i] = 0.0;
  }
}
void loopGPS2()
{
  loopGPSSerial();
}
// GPS Serial input:
void loopGPSSerial()
{
  if(floidStatus.gpsOn)
  {
    // Are we getting gps from the Android?
    if(floidStatus.deviceGPS)
    {
      // If so, do we have a new one?
        if(hasNewDeviceGPS)
        {
          // They are already copied in...we just need to clear, count and process:
          hasNewDeviceGPS = false;
          ++floidStatus.gpsGoodCount;
	  processGPS();
       }
    }
    else
    {
      if(floidStatus.emulateGPS)
      {
	// GPS emulation is fixed location at a fixed rate:
        if((millis() - lastGPSEmulateTime) >= gpsRateInMilliSeconds)
        {
          floidStatus.gpsXDegreesDecimal = 34.5;
          floidStatus.gpsYDegreesDecimal = 77.0;
          floidStatus.gpsZMeters         = 55.0;
          floidStatus.gpsZMetersFiltered = floidStatus.gpsZMeters;
          floidStatus.gpsZFeet           = floidStatus.gpsZMeters * METERS_TO_FEET;
          floidStatus.gpsFixQuality      = 1.0;
          floidStatus.gpsHDOP            = 0.9;
          floidStatus.gpsSats            = 12;
          ++floidStatus.gpsGoodCount;
          gpsNewReading = true;
          lastGPSEmulateTime = millis();
        }
      }
      else
      {
        while(GPS_SERIAL.available())
        {
          byte    b             = GPS_SERIAL.read();
          boolean handledByPmtk = false;
	  // Handle pmtk:
          if(pmtkHandler.pmtkDecode(b))
          {
            handledByPmtk = true; // All packets are the same for now
          }
          if(gps.decode(b))
          {
            if(isNMEASentenceType(gps, "GGA"))
            {
              // Get and the reading for this GPS:
              unsigned long gpsTimeTemp                = (unsigned long)(gps.term_decimal(GPS_NMEA_GGA_INDEX_TIME)*1000);
              float         tmpFloat;
              floidStatus.gpsTime                      = gpsTimeTemp % 1000;         // Milliseconds
              gpsTimeTemp                              /= 1000;
              floidStatus.gpsTime                      += (gpsTimeTemp%100) * 1000;   // Seconds
              gpsTimeTemp                              /= 100;
              floidStatus.gpsTime                      += (gpsTimeTemp%100) * 60000;  // Minutes
              gpsTimeTemp                              /= 100;
              floidStatus.gpsTime                      += (gpsTimeTemp) * 3600000;    // Hours
              tmpFloat                                 = longitudeToDegreesDecimal(gps.term(GPS_NMEA_GGA_INDEX_LONG_0), gps.term(GPS_NMEA_GGA_INDEX_LONG_1));
              if(!isnan(tmpFloat))
              {
                floidStatus.gpsXDegreesDecimal         = tmpFloat;
              }
              tmpFloat                                 = latitudeToDegreesDecimal(gps.term(GPS_NMEA_GGA_INDEX_LAT_0), gps.term(GPS_NMEA_GGA_INDEX_LAT_1));
              if(!isnan(tmpFloat))
              {
                floidStatus.gpsYDegreesDecimal         = tmpFloat;
              }
              tmpFloat                                 = gps.term_decimal(GPS_NMEA_GGA_INDEX_ALTITUDE);
              if(!isnan(tmpFloat))
              {
                floidStatus.gpsZMeters                 = tmpFloat;
              }
              floidStatus.gpsZFeet                     = floidStatus.gpsZMeters * METERS_TO_FEET;
              floidStatus.gpsFixQuality                = atoi(gps.term(GPS_NMEA_GGA_INDEX_FIX_QUALITY));
              floidStatus.gpsHDOP                      = gps.term_decimal(GPS_NMEA_GGA_INDEX_HDOP);
              floidStatus.gpsSats                      = atoi(gps.term(GPS_NMEA_GGA_INDEX_NUMBER_SATS));
              if(floidStatus.debug && floidStatus.debugGPS)
              {
                debugPrintToken(FLOID_DEBUG_LABEL_GPS_RAW_TERMS);
                for(int i=1; i<10; ++i) // Note starts at 1 - ends at 9
                {
                  debugPrint(gps.term(i));
                  debugPrint(FLOID_DEBUG_SPACE);
                }
                debugPrintln();
                debugPrintToken(FLOID_DEBUG_GPS_DECIMAL_READING);
                debugPrint(floidStatus.gpsYDegreesDecimal);
                debugPrint(FLOID_DEBUG_SPACE);
                debugPrintln(floidStatus.gpsXDegreesDecimal);
              }
	      // Good enough?
              if((floidStatus.gpsFixQuality > 0) && (floidStatus.gpsHDOP < GPS_GOOD_READING_HDOP_VALUE) && (floidStatus.gpsSats >= GPS_GOOD_READING_NUMBER_SATS))
              {
                gpsLastGoodReadTime = millis();
                ++floidStatus.gpsGoodCount;
                // Process the GPS:
                processGPS();
              }
              else
              {
                floidStatus.gpsGoodCount = 0ul;
              }
            }
            else
            {
              // Unsupported sentence - make sure it was not already handled by PMTK - otherwise we send a note:
              if(!handledByPmtk)
              {
                if(floidStatus.debug && floidStatus.debugGPS)
                {
                  debugPrintToken(FLOID_DEBUG_GPS_UNSUPPORTED_SENTENCE);
                  debugPrintln(gps.term(GPS_NMEA_GGA_INDEX_TYPE));
                }
              }
            }
          }
        }
      }
    }
    // Last thing: See if we timed out our gps reading:
    if(!floidStatus.emulateGPS && !floidStatus.deviceGPS && (millis() > gpsLastGoodReadTime + GPS_GOOD_READING_TIMEOUT))
    {
      floidStatus.gpsGoodCount = 0ul;
    }
  }
}
void processGPS()
{
  float         tmpFloat;
  // Run this reading through our Kalman filter - predict, update, correct, retrieve:
  aKalmanGPS_X.predict();
  aKalmanGPS_X.setM(0, 0, floidStatus.gpsXDegreesDecimal);
  aKalmanGPS_X.correct();
  aKalmanGPS_Y.predict();
  aKalmanGPS_Y.setM(0, 0, floidStatus.gpsYDegreesDecimal);
  aKalmanGPS_Y.correct();
#ifdef  GPS_USE_KALMAN_FILTER_FOR_Z
  aKalmanGPS_Z.predict();
  aKalmanGPS_Z.setM(0, 0, floidStatus.gpsZMeters);
  aKalmanGPS_Z.correct();
#endif  /* GPS_USE_KALMAN_FILTER_FOR_Z */
  // We retrieve the predicted results as our current state:
  tmpFloat = aKalmanGPS_X.getStatePre(0, 0);
  if(!isnan(tmpFloat))
  {
    floidStatus.gpsXDegreesDecimalFiltered = tmpFloat;
  }
  else
  {
    aKalmanGPS_X.init();
  }
  tmpFloat = aKalmanGPS_Y.getStatePre(0, 0);
  if(!isnan(tmpFloat))
  {
    floidStatus.gpsYDegreesDecimalFiltered = tmpFloat;
  }
  else
  {
    aKalmanGPS_Y.init();
  }
#ifdef  GPS_USE_KALMAN_FILTER_FOR_Z
  tmpFloat = aKalmanGPS_Z.getStatePre(0, 0);
  if(!isnan(tmpFloat))
  {
    floidStatus.gpsZMetersFiltered = tmpFloat;
  }
  else
  {
    aKalmanGPS_Z.init();
  }
#else  /* GPS_USE_KALMAN_FILTER_FOR_Z */
  floidStatus.gpsZMetersFiltered           = floidStatus.gpsZMeters;
#endif  /* GPS_USE_KALMAN_FILTER_FOR_Z */
  // Copy down the readings so we can calculate our velocities:
  for(int i=0; i<NUMBER_GPS_READINGS-1; ++i)
  {
    gpsX[i]         = gpsX[i+1];
    gpsY[i]         = gpsY[i+1];
    gpsZ[i]         = gpsZ[i+1];
    gpsTimes[i]     = gpsTimes[i+1];
    gpsDOG[i]       = gpsDOG[i+1];
    gpsVOG[i]       = gpsVOG[i+1];
  }
  // Put in the new readings:
  gpsX[NUMBER_GPS_READINGS-1]      = floidStatus.gpsXDegreesDecimalFiltered;
  gpsY[NUMBER_GPS_READINGS-1]      = floidStatus.gpsYDegreesDecimalFiltered;
  gpsZ[NUMBER_GPS_READINGS-1]      = floidStatus.gpsZMetersFiltered;
  gpsTimes[NUMBER_GPS_READINGS-1]  = floidStatus.gpsTime;
  gpsDOG[NUMBER_GPS_READINGS-1]    = compassToDegrees(initial_course(gpsY[0], gpsX[0], gpsY[NUMBER_GPS_READINGS-1], gpsX[NUMBER_GPS_READINGS-1]));
  unsigned long gpsTimeDelta       = gpsTimes[NUMBER_GPS_READINGS-1] - gpsTimes[0];  // In milliseconds
  // Make sure time did not flip over, if so we need to add a day so to speak:
  if(gpsTimeDelta<0)
  {
    gpsTimeDelta       += TICKS_PER_DAY;
  }
  // OK, we need to make sure the gpsTimeDelta is in some sort of range in case of error on the time - say initial v. when it has good fix...
  if(gpsTimeDelta > GPS_MAX_GPS_TIME_DELTA)
  {
    gpsTimeDelta = GPS_MAX_GPS_TIME_DELTA;
  }
  if(gpsTimeDelta == 0)
  {
    // DO NOTHING: the last VOG and ZVelocity will still be in their slots so do nothing...
  }
  else
  {
    // 1/2 the current Z Velocity & 1/2 the new (in m/s):
    gpsZVelocity                  = ((1.0-GPS_NEW_READING_BLEND_RATIO) * gpsZVelocity) + (GPS_NEW_READING_BLEND_RATIO * TICKS_PER_SECOND * ((gpsZ[NUMBER_GPS_READINGS-1] - gpsZ[0])/((float)gpsTimeDelta))); // In ft/s
    gpsVOG[NUMBER_GPS_READINGS-1] = TICKS_PER_SECOND * (distance_between(gpsY[0], gpsX[0], gpsY[NUMBER_GPS_READINGS-1], gpsX[NUMBER_GPS_READINGS-1], MTR)/((float)gpsTimeDelta));
  }
  // Calculate DOG and VOG:
  floidStatus.directionOverGround = gpsDOG[NUMBER_GPS_READINGS-1];
  floidStatus.velocityOverGround  = smoothVOG();
  gpsNewReading = true;
}

float smoothVOG()
{
  // Linearly average these:
  float sumVOG = 0.0;
  for(int i=0; i<NUMBER_GPS_READINGS; ++i)
  {
    sumVOG += gpsVOG[i];
  }
  return sumVOG / (float)NUMBER_GPS_READINGS;
}
boolean  loopGPS()
{
  loopGPS2();
  if(gpsNewReading)
  {
    gpsNewReading = false;
    return true;
  }
  else
  {
    return false;
  }
}
boolean isNMEASentenceType(NMEA gps, const char type[])
{
  if(    (gps.term(0)[2] == type[0])
      && (gps.term(0)[3] == type[1])
      && (gps.term(0)[4] == type[2]) )
  {
    return true;
  }
  return false;
}
float longitudeToDegreesDecimal(char *longitude, char* eastWest)
{
  // degrees in [0..2]
  // minutes in [3..4]
  // seconds in [6..9]
  float longitudeDecimal = 0;
  switch(strlen(longitude))
  {
    case 11:
    case 10:
    {
	longitudeDecimal = convertDegreesMinutesSecondsToDecimal(longitude[0],
								 longitude[1],
								 longitude[2],
								 longitude[3],
								 longitude[4],
								 longitude[6],
								 longitude[7],
								 longitude[8],
								 longitude[9]);
    }
    break;
    case 9:
    {
        longitudeDecimal = convertDegreesMinutesSecondsToDecimal(longitude[0],
								 longitude[1],
								 longitude[2],
								 longitude[3],
								 longitude[4],
								 longitude[6],
								 longitude[7],
								 longitude[8],
								 '0');
    }
    break;
    case 8:
    {
	longitudeDecimal = convertDegreesMinutesSecondsToDecimal(longitude[0],
								 longitude[1],
								 longitude[2],
								 longitude[3],
								 longitude[4],
								 longitude[6],
								 longitude[7],
								 '0',
								 '0');
    }
    break;
    case 7:
    {
	longitudeDecimal = convertDegreesMinutesSecondsToDecimal(longitude[0],
								 longitude[1],
								 longitude[2],
								 longitude[3],
								 longitude[4],
								 longitude[6],
								 '0',
								 '0',
								 '0');
    }
    break;
    }
  if(*eastWest == 'W')
  {
    longitudeDecimal = 0.0 - longitudeDecimal;
  }
  return longitudeDecimal;
}
float latitudeToDegreesDecimal(char* latitude, char* northSouth)
{
  // degrees in [0..1]
  // minutes in [2..3]
  // seconds in [5..8]
  float latitudeDecimal=0;
  switch(strlen(latitude))
  {
    case 9:
    case 10:
    {
	latitudeDecimal = convertDegreesMinutesSecondsToDecimal('0',
								latitude[0],
								latitude[1],
								latitude[2],
								latitude[3],
								latitude[5],
								latitude[6],
								latitude[7],
                                latitude[8]
								);
    }
    break;
    case 8:
    {
	latitudeDecimal = convertDegreesMinutesSecondsToDecimal('0',
								latitude[0],
								latitude[1],
								latitude[2],
								latitude[3],
								latitude[5],
								latitude[6],
								latitude[7],
                                '0'
								);
    }
    break;    
    case 7:
    {
	latitudeDecimal = convertDegreesMinutesSecondsToDecimal('0',
								latitude[0],
								latitude[1],
								latitude[2],
								latitude[3],
								latitude[5],
								latitude[6],
								'0',
                                '0'
								);
    }
    break;                                               
    case 6:
    {
	latitudeDecimal = convertDegreesMinutesSecondsToDecimal('0',
								latitude[0],
								latitude[1],
								latitude[2],
								latitude[3],
								latitude[5],
								'0',
								'0',
                                '0'
								);
    }
    break;                                               
  }
  if(*northSouth == 'S')
  {
    latitudeDecimal = 0.0 - latitudeDecimal;
  }
  return latitudeDecimal;
}
float convertDegreesMinutesSecondsToDecimal(char deg0, char deg1, char deg2, char min0, char min1, char sec0, char sec1, char sec2, char sec3)
{
  float degrees = (float)(charToValue(deg0)*100 + charToValue(deg1)*10 + charToValue(deg2));
  
  float minutesSeconds = charToValue(min0)*10.0 + (float)charToValue(min1) + charToValue(sec0)/10.0 + charToValue(sec1)/100.0 + charToValue(sec2)/1000.0 + charToValue(sec3)/10000.0;
  return(degrees + minutesSeconds/60.0);
}
int charToValue(const char c)
{
  if((c < '0') || (c > '9'))
  {
    return 0;
  }
  else
  {
    return ((int) (c-'0')) & 0xff;
  }
}
void stopGPS()
{
  if(floidStatus.debug && floidStatus.debugGPS)
  {
      debugPrintlnToken(FLOID_DEBUG_GPS_STOP);
  }
  digitalWrite(gpsOnPin, LOW);
  floidStatus.gpsOn          = false;
  gpsLastGoodReadTime        = 0ul;
  floidStatus.gpsGoodCount   = 0ul;
}
void setGPSRate(byte newGPSRate)
{
  if(floidStatus.debug && floidStatus.debugGPS)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_GPS_NEW_RATE);
    debugPrintln(newGPSRate);
  }
  floidStatus.gpsRate = newGPSRate;
  if(floidStatus.gpsOn)
  { 
    setGPSRate();
  }
}
void setGPSRate()
{
  switch(floidStatus.gpsRate)
  {
    case 0:
    {
      gpsRateInMilliSeconds = 100;
    }
    break;
    case 1:
    {
      gpsRateInMilliSeconds = 200;
    }
    break;
    case 2:
    {
      gpsRateInMilliSeconds = 250;
    }
    break;
    case 3:
    {
      gpsRateInMilliSeconds = 500;
    }
    break;
    case 4:
    {
      gpsRateInMilliSeconds = 1000;
    }
    break;
    default:
    {
      gpsRateInMilliSeconds = 100; // Defaults to 100
    }
    break;
  }
  // Set the output rate to the GPS:
  pmtkHandler.pmtkSendSetOutputRate(gpsRateInMilliSeconds);
  pmtkHandler.processPmtkCommandResponse(true);
}
