//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_GPS.h:
#ifndef	__FLOIDBOARD3_GPS_H__
#define	__FLOIDBOARD3_GPS_H__
#define GPS_USE_KALMAN_FILTER_FOR_Z
#define METERS_TO_FEET                                ((float)3.2808399)
#define TICKS_PER_SECOND                              (1000)
#define TICKS_PER_DAY                                 (86400000)
#define GPS_MAX_GPS_TIME_DELTA                        (2000ul)
#define GPS_NEW_READING_BLEND_RATIO                   (0.5)
// GPS logic:
#define              NUMBER_GPS_READINGS              (5)
extern boolean       gpsDeployed;
extern unsigned long gpsFireTime;
extern NMEA          gps;                     // GPS data connection to all sentence types for GPS
extern PMTKHandler   pmtkHandler;
extern unsigned long gpsLastGoodReadTime;
extern float         gpsX[NUMBER_GPS_READINGS];
extern float         gpsY[NUMBER_GPS_READINGS];
extern float         gpsZ[NUMBER_GPS_READINGS];
extern float         gpsZVelocity;
extern float         gpsDOG[NUMBER_GPS_READINGS]; // This is in std trig degrees, NOT compass degrees
extern float         gpsVOG[NUMBER_GPS_READINGS];
extern unsigned long gpsTimes[NUMBER_GPS_READINGS];
// TODO - [ACQUIRE HOME POSITION PROCESSING] - MOVE THESE TO MODEL PARAMETERS AND MAKE INTERFACE AND ALLOW ADJUSTMENT FROM ACQUIRE HOME POSITION
#define  GPS_GOOD_READING_HDOP_VALUE    (1.6)
#define  GPS_GOOD_READING_NUMBER_SATS   (8)
// GPS Readings are good for three seconds:
#define  GPS_GOOD_READING_TIMEOUT       (3000ul)
#define  GPS_NMEA_GGA_INDEX_TYPE        (0)
#define  GPS_NMEA_GGA_INDEX_TIME        (1)
#define  GPS_NMEA_GGA_INDEX_LAT_0       (2)
#define  GPS_NMEA_GGA_INDEX_LAT_1       (3)
#define  GPS_NMEA_GGA_INDEX_LONG_0      (4)
#define  GPS_NMEA_GGA_INDEX_LONG_1      (5)
#define  GPS_NMEA_GGA_INDEX_FIX_QUALITY (6)
#define  GPS_NMEA_GGA_INDEX_NUMBER_SATS (7)
#define  GPS_NMEA_GGA_INDEX_HDOP        (8)
#define  GPS_NMEA_GGA_INDEX_ALTITUDE    (9)
/********************************************************************************
 GPS LS23060 NOTES:
 ------------------
  NMEA SENTENCES:    http://www.gpsinformation.org/dale/nmea.htm
                     http://aprs.gids.nl/nmea/
  CHECKSUM CREATOR:  http://www.hhhh.org/wiml/proj/nmeaxor.html
	See the EB-230-data-sheet file for more PMTK commands
	Defaults:	57600/WS-180/SBAS On/WAAS On
	Data to send to units:
	 - Set only the output I want (GGA=3 [Every Time]): $PMTK314,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0*29\r\n
	 - Set output rate (5Hz):                           $PMTK220,200*2C\r\n
	 - Set baud rate (115200):                          $PMTK251,115200*1F\r\n
*********************************************************************************/
//#define  GPS_LS23060_DEFAULT_BAUD_RATE                (57600)
//#define  GPS_LS23060_FINAL_BAUD_RATE                  (115200)
// Readings:
#define  GPS_CURRENT_READING                          (1)
#define  GPS_PREVIOUS_READING                         (0)
#define  GPS_FLAGS_NO_READING                         (0b00000000)
#define  GPS_FLAGS_ERROR                              (0b10000000)
#define  GPS_FLAGS_GPS                                (0b00000001)
// Kalman stuff:
#define  GPS_X_KALMAN_PRINT_MATRICES                   ((bool)false)
#define  GPS_X_KALMAN_DP                               (2)
#define  GPS_X_KALMAN_MP                               (1)
#define  GPS_X_KALMAN_CP                               (0)
#define  GPS_X_KALMAN_PROCESS_NOISE_COV_INIT           ((float)1.0)
#define  GPS_X_KALMAN_MEASUREMENT_NOISE_COV_INIT       ((float)1.0)
#define  GPS_Y_KALMAN_PRINT_MATRICES                   ((bool)false)
#define  GPS_Y_KALMAN_DP                               (2)
#define  GPS_Y_KALMAN_MP                               (1)
#define  GPS_Y_KALMAN_CP                               (0)
#define  GPS_Y_KALMAN_PROCESS_NOISE_COV_INIT           ((float)1.0)
#define  GPS_Y_KALMAN_MEASUREMENT_NOISE_COV_INIT       ((float)1.0)
#ifdef  GPS_USE_KALMAN_FILTER_FOR_Z
#define  GPS_Z_KALMAN_PRINT_MATRICES                   ((bool)false)
#define  GPS_Z_KALMAN_DP                               (2)
#define  GPS_Z_KALMAN_MP                               (1)
#define  GPS_Z_KALMAN_CP                               (0)
#define  GPS_Z_KALMAN_PROCESS_NOISE_COV_INIT           ((float)1.0)
#define  GPS_Z_KALMAN_MEASUREMENT_NOISE_COV_INIT       ((float)1.0)
#endif /*  GPS_USE_KALMAN_FILTER_FOR_Z */
extern aKalman          aKalmanGPS_X;
extern aKalman          aKalmanGPS_Y;
#ifdef  GPS_USE_KALMAN_FILTER_FOR_Z
extern aKalman          aKalmanGPS_Z;
#endif  /* GPS_USE_KALMAN_FILTER_FOR_Z */
boolean  setupGPS();
void     loopGPS2();
float    smoothDOG();
float    smoothVOG();
void     loopGPSSerial();
void     processGPS();
void     startGPS();
void     stopGPS();
boolean  isNMEASentenceType(NMEA gps, const char type[]);
float    longitudeToDegreesDecimal(char *longitude, char* eastWest);
float    latitudeToDegreesDecimal(char* latitude, char* northSouth);
float    convertDegreesMinutesSecondsToDecimal(char deg0, char deg1, char deg2, char min0, char min1, char sec0, char sec1, char sec2, char sec3);
int      charToValue(const char c);
boolean  loopGPS();
void     setGPSRate(byte newGPSRate);
void     setGPSRate();
#endif /* __FLOIDBOARD3_GPS_H__ */
