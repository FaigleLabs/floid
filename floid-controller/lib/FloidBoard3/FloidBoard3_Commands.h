//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Commands.h:
#ifndef	__FLOIDBOARD3_COMMANDS_H__
#define	__FLOIDBOARD3_COMMANDS_H__

// INPUT MESSAGES:
// ===============
// Packet types and command number offsets:
#define       ACC_PACKET_TYPE_OFFSET                  (0x00)
#define       ACC_COMMAND_NUMBER_OFFSET               (0x01)
#define       ACC_GOAL_ID_OFFSET                      (0x05)
// Packet types:
#define       HELI_CONTROL_PACKET                     (0x01)
#define       PAN_TILT_PACKET                         (0x02)
#define       RELAY_PACKET                            (0x03)
#define       DECLINATION_PACKET                      (0x04)
#define       SET_PARAMETERS_PACKET                   (0x05)
#define       HELIS_ON_PACKET                         (0x06)
#define       HELIS_OFF_PACKET                        (0x07)
#define       DESIGNATE_TARGET_PACKET                 (0x0E)
#define       STOP_DESIGNATING_PACKET                 (0x0F)
#define       SET_POSITION_GOAL_PACKET                (0x10)
#define       SET_ROTATION_GOAL_PACKET                (0x11)
#define       DEPLOY_PARACHUTE_PACKET                 (0x12)
#define       LIFT_OFF_PACKET                         (0x15)
#define       LAND_PACKET    	                      (0x16)
#define       TEST_HELIS_ON_PACKET                    (0x17)
#define       TEST_HELIS_OFF_PACKET                   (0x18)
#define       PAYLOAD_PACKET                          (0x1A)
#define       DEBUG_CONTROL_PACKET                    (0x1B)
#define       FLOID_MODE_PACKET                       (0x1C)
#define       MODEL_PARAMETERS_1_PACKET               (0x1D)
#define       MODEL_PARAMETERS_2_PACKET               (0x1E)
#define       MODEL_PARAMETERS_3_PACKET               (0x1F)
#define       MODEL_PARAMETERS_4_PACKET               (0x20)
#define       GET_MODEL_PARAMETERS_PACKET             (0x22)
#define       TEST_GOALS_PACKET                       (0x23)
#define       SET_ALTITUDE_GOAL_PACKET                (0x24)
#define       HEARTBEAT_PACKET                        (0x25)
#define       DEVICE_GPS_PACKET                       (0x26)
#define       DEVICE_PYR_PACKET                       (0x27)
#define       TEST_PACKET                             (0x7F)
// Heli Control:
#define       HELI_CONTROL_PACKET_HELI_OFFSET         (0x09)
#define       HELI_CONTROL_PACKET_SERVO_OFFSET        (0x0A)
#define       HELI_CONTROL_PACKET_VALUE_OFFSET        (0x0B)
#define       HELI_CONTROL_LEFT_SERVO                 (0x00)
#define       HELI_CONTROL_RIGHT_SERVO                (0x01)
#define       HELI_CONTROL_PITCH_SERVO                (0x02)
#define       HELI_CONTROL_ESC_SERVO                  (0x03)
#define       HELI_CONTROL_COLLECTIVE_SERVO           (0x04)
#define       HELI_CONTROL_CYCLIC_LEFT_SERVO          (0x05)
#define       HELI_CONTROL_CYCLIC_RIGHT_SERVO         (0x06)
#define       HELI_CONTROL_CYCLIC_PITCH_SERVO         (0x07)
// Pan-Tilt:
#define       PAN_TILT_PACKET_DEVICE_OFFSET           (0x09)
#define       PAN_TILT_PACKET_TYPE_OFFSET             (0x0A)
#define       PAN_TILT_PACKET_TYPE_ANGLE              (0x00)
#define       PAN_TILT_PACKET_ANGLE_PAN_OFFSET        (0x0B)
#define       PAN_TILT_PACKET_ANGLE_TILT_OFFSET       (0x0F)
#define       PAN_TILT_PACKET_TYPE_TARGET             (0x01)
#define       PAN_TILT_PACKET_TARGET_X_OFFSET         (0x0B)
#define       PAN_TILT_PACKET_TARGET_Y_OFFSET         (0x0F)
#define       PAN_TILT_PACKET_TARGET_Z_OFFSET         (0x13)
#define       PAN_TILT_PACKET_TYPE_INDIVIDUAL         (0x02)
#define       PAN_TILT_PACKET_INDIVIDUAL_PT_OFFSET    (0x0B)
#define       PAN_TILT_PACKET_INDIVIDUAL_VALUE_OFFSET (0x0C)
#define       PAN_TILT_PAN_SUBCOMMAND                 (0x00)
#define       PAN_TILT_TILT_SUBCOMMAND                (0x01)
#define       PAN_TILT_PACKET_TYPE_SCAN               (0x03)
// TODO [PAN_TILT] IMPLEMENT THE PARAMETERS FOR PAN_TILT_SCAN_MODE:
// Relay:
#define       RELAY_PACKET_DEVICE_OFFSET              (0x09)
#define       RELAY_PACKET_STATE_OFFSET               (0x0A)
#define       RELAY_DEVICE_ESC0                       (0x00)
#define       RELAY_DEVICE_ESC1                       (0x01)
#define       RELAY_DEVICE_ESC2                       (0x02)
#define       RELAY_DEVICE_ESC3                       (0x03)
#define       RELAY_DEVICE_NM0                        (0x04)
#define       RELAY_DEVICE_NM1                        (0x05)
#define       RELAY_DEVICE_NM2                        (0x06)
#define       RELAY_DEVICE_NM3                        (0x07)
#define       RELAY_DEVICE_GPS                        (0x08)
#define       RELAY_DEVICE_PYR                        (0x09)
#define       RELAY_DEVICE_DESIGNATOR                 (0x0A)
#define       RELAY_DEVICE_AUX                        (0x0B)
#define       RELAY_DEVICE_PARACHUTE                  (0x0C)
#define       RELAY_DEVICE_PT0                        (0x0D)
#define       RELAY_DEVICE_PT1                        (0x0E)
#define       RELAY_DEVICE_PT2                        (0x0F)
#define       RELAY_DEVICE_PT3                        (0x10)
#define       RELAY_DEVICE_LED0                       (0x11)
#define       RELAY_DEVICE_S0                         (0x13)
#define       RELAY_DEVICE_S1                         (0x14)
#define       RELAY_DEVICE_S2                         (0x15)
#define       RELAY_DEVICE_S3                         (0x16)
#define       RELAY_DEVICE_ESC0_EXTRA                 (0x17)
#define       RELAY_DEVICE_ESC1_EXTRA                 (0x18)
#define       RELAY_DEVICE_ESC2_EXTRA                 (0x19)
#define       RELAY_DEVICE_ESC3_EXTRA                 (0x1A)
#define       RELAY_DEVICE_PYR_AD_SWITCH              (0x1B)
#define       RELAY_DEVICE_PYR_RATE_0_SWITCH          (0x1C)
#define       RELAY_DEVICE_PYR_RATE_1_SWITCH          (0x1D)
#define       RELAY_DEVICE_PYR_RATE_2_SWITCH          (0x1E)
#define       RELAY_DEVICE_PYR_RATE_3_SWITCH          (0x1F)
#define       RELAY_DEVICE_PYR_RATE_4_SWITCH          (0x20)
#define       RELAY_DEVICE_PYR_RATE_5_SWITCH          (0x21)
#define       RELAY_DEVICE_TEST_LIFT_OFF_MODE_SWITCH  (0x22)
#define       RELAY_DEVICE_TEST_NORMAL_MODE_SWITCH    (0x23)
#define       RELAY_DEVICE_TEST_LAND_MODE_SWITCH      (0x24)
#define       RELAY_DEVICE_TEST_GOAL_ON_SWITCH        (0x25)
#define       RELAY_DEVICE_TEST_GOAL_OFF_SWITCH       (0x26)
#define       RELAY_DEVICE_TEST_BLADES_LOW_SWITCH     (0x27)
#define       RELAY_DEVICE_TEST_BLADES_ZERO_SWITCH    (0x28)
#define       RELAY_DEVICE_TEST_BLADES_HIGH_SWITCH    (0x29)
#define       RELAY_DEVICE_LOAD_FROM_EEPROM           (0x2A)
#define       RELAY_DEVICE_SAVE_TO_EEPROM             (0x2B)
#define       RELAY_DEVICE_CLEAR_EEPROM               (0x2C)
#define       RELAY_DEVICE_SERIAL_OUTPUT              (0x2D)
#define       RELAY_DEVICE_FLOID_STATUS_RATE_0_SWITCH (0x2E)
#define       RELAY_DEVICE_FLOID_STATUS_RATE_1_SWITCH (0x2F)
#define       RELAY_DEVICE_FLOID_STATUS_RATE_2_SWITCH (0x30)
#define       RELAY_DEVICE_FLOID_STATUS_RATE_3_SWITCH (0x31)
#define       RELAY_DEVICE_FLOID_STATUS_RATE_4_SWITCH (0x32)
#define       RELAY_DEVICE_FLOID_STATUS_RATE_5_SWITCH (0x33)
#define       RELAY_DEVICE_FLOID_STATUS_RATE_6_SWITCH (0x34)
#define       RELAY_DEVICE_FLOID_STATUS_RATE_7_SWITCH (0x35)
#define       RELAY_DEVICE_GPS_RATE_0_SWITCH          (0x36)
#define       RELAY_DEVICE_GPS_RATE_1_SWITCH          (0x37)
#define       RELAY_DEVICE_GPS_RATE_2_SWITCH          (0x38)
#define       RELAY_DEVICE_GPS_RATE_3_SWITCH          (0x39)
#define       RELAY_DEVICE_GPS_RATE_4_SWITCH          (0x3A)
#define       RELAY_DEVICE_ESC0_SETUP                 (0x3B)
#define       RELAY_DEVICE_ESC1_SETUP                 (0x3C)
#define       RELAY_DEVICE_ESC2_SETUP                 (0x3D)
#define       RELAY_DEVICE_ESC3_SETUP                 (0x3E)
#define       RELAY_DEVICE_GPS_EMULATE                (0x3F)
#define       RELAY_DEVICE_GPS_DEVICE                 (0x40)
#define       RELAY_DEVICE_PYR_DEVICE                 (0x41)
#define       RELAY_DEVICE_RUN_MODE_PRODUCTION        (0x42)
#define       RELAY_DEVICE_TEST_MODE_PRINT_DEBUG_HEADINGS (0x43)
#define       RELAY_DEVICE_RUN_MODE_TEST_XY           (0x44)
#define       RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_1   (0x45)
#define       RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_2   (0x46)
#define       RELAY_DEVICE_RUN_MODE_TEST_HEADING_1    (0x47)
#define       RELAY_DEVICE_RUN_MODE_TEST_PITCH_1      (0x48)
#define       RELAY_DEVICE_RUN_MODE_TEST_ROLL_1       (0x49)
#define       RELAY_DEVICE_TEST_MODE_NO_XY            (0x4A)
#define       RELAY_DEVICE_TEST_MODE_NO_ALTITUDE      (0x4B)
#define       RELAY_DEVICE_TEST_MODE_NO_ATTACK_ANGLE  (0x4C)
#define       RELAY_DEVICE_TEST_MODE_NO_HEADING       (0x4D)
#define       RELAY_DEVICE_TEST_MODE_NO_PITCH         (0x4E)
#define       RELAY_DEVICE_TEST_MODE_NO_ROLL          (0x4F)
#define       RELAY_DEVICE_TEST_MODE_CHECK_PHYSICS_MODEL (0x50)
#define       RELAY_DEVICE_GOALS                      (0x51)
#define       RELAY_DEVICE_IMU_CALIBRATE              (0x52)
#define       RELAY_DEVICE_IMU_SAVE                   (0x53)
// Declination:
#define       DECLINATION_PACKET_DECLINATION_OFFSET   (0x09)
// Position:
#define       SET_POSITION_GOAL_PACKET_X_OFFSET       (0x09)
#define       SET_POSITION_GOAL_PACKET_Y_OFFSET       (0x0D)
// Position:
#define       SET_ALTITUDE_GOAL_PACKET_Z_OFFSET       (0x09)
#define       SET_ALTITUDE_GOAL_PACKET_ABSOLUTE_HEIGHT_OFFSET  (0x0D)
// Rotation:
#define       SET_ROTATION_GOAL_FOLLOW_MODE_OFFSET    (0x09)
#define       SET_ROTATION_GOAL_PACKET_A_OFFSET       (0x0A)
// Lift-Off/Land:
#define       LIFT_OFF_PACKET_X_OFFSET                (0x09)
#define       LIFT_OFF_PACKET_Y_OFFSET                (0x0D)
#define       LIFT_OFF_PACKET_Z_OFFSET                (0x11)
#define       LIFT_OFF_PACKET_A_OFFSET                (0x15)
#define       LAND_PACKET_Z_OFFSET                    (0x09)
// Designate:
#define       DESIGNATE_TARGET_PACKET_X_OFFSET        (0x09)
#define       DESIGNATE_TARGET_PACKET_Y_OFFSET        (0x0D)
#define       DESIGNATE_TARGET_PACKET_Z_OFFSET        (0x11)
// Payload:
#define       PAYLOAD_PACKET_BAY_OFFSET               (0x09)
// Debug Control:
#define       DEBUG_PACKET_INDEX_OFFSET               (0x09)
#define       DEBUG_PACKET_VALUE_OFFSET               (0x0A)
#define       DEBUG_PACKET_DEBUG                      (0x00)
#define       DEBUG_PACKET_MEMORY                     (0x01)
#define       DEBUG_PACKET_GPS                        (0x02)
#define       DEBUG_PACKET_PYR                        (0x03)
#define       DEBUG_PACKET_PHYSICS                    (0x04)
#define       DEBUG_PACKET_PAN_TILT                   (0x05)
#define       DEBUG_PACKET_PAYLOADS                   (0x06)
#define       DEBUG_PACKET_CAMERA                     (0x07)
#define       DEBUG_PACKET_DESIGNATOR                 (0x08)
#define       DEBUG_PACKET_AUX                        (0x09)
#define       DEBUG_PACKET_HELIS                      (0x0A)
#define       DEBUG_PACKET_ALL_DEVICES                (0x0B)
#define       DEBUG_PACKET_ALTIMETER                  (0x0C)
// Mode:
#define       FLOID_MODE_PACKET_MODE_OFFSET           (0x09)
// Test goals:
#define       TEST_GOALS_PACKET_SIZE                  (0x21)
#define       TEST_GOALS_OFFSET_X                     (0x11)
#define       TEST_GOALS_OFFSET_Y                     (0x15)
#define       TEST_GOALS_OFFSET_Z                     (0x19)
#define       TEST_GOALS_OFFSET_A                     (0x1D)
// Device gps:
#define       DEVICE_GPS_PACKET_SIZE                  (0x21)
#define       DEVICE_GPS_OFFSET_LAT                   (0x11)
#define       DEVICE_GPS_OFFSET_LNG                   (0x15)
#define       DEVICE_GPS_OFFSET_ALT                   (0x19)
#define       DEVICE_GPS_OFFSET_HDOP                  (0x1D)
// Device PYR:
#define       DEVICE_PYR_PACKET_SIZE                  (0x25)
#define       DEVICE_PYR_OFFSET_PITCH                 (0x11)
#define       DEVICE_PYR_OFFSET_HEADING               (0x15)
#define       DEVICE_PYR_OFFSET_ROLL                  (0x19)
#define       DEVICE_PYR_OFFSET_ALTITUDE              (0x1D)
#define       DEVICE_PYR_OFFSET_TEMPERATURE           (0x21)
// Command Response:
#define       COMMAND_RESPONSE_OK                     (0x00)
#define       COMMAND_RESPONSE_ERROR                  (0x01)
#define       COMMAND_RESPONSE_LOGIC_ERROR            (0x02)
// Command SubStatus:
#define       SUB_STATUS_NONE                         (0x00)
#define       SUB_STATUS_WRONG_MODE                   (0x01)
#define       SUB_STATUS_PERFORMED                    (0x02)
#define       SUB_STATUS_FAILED                       (0x03)
#define       SUB_STATUS_UNKNOWN_COMMAND              (0x04)
#define       SUB_STATUS_UNKNOWN_RELAY                (0x05)
#define       SUB_STATUS_NOT_IMPLEMENTED              (0x06)
#define       SUB_STATUS_BAD_INDEX                    (0x07)
#define       SUB_STATUS_ONLY_IN_FLIGHT               (0x08)
#define       SUB_STATUS_ONLY_ON_GROUND               (0x09)
#define       SUB_STATUS_ALREADY_IN_MODE              (0x0A)
// Command Response:
struct CommandResponse
{
  public:
    PacketHeader  packetHeader;
    long          number;
    int           status;
    int           subStatus;
    byte          identifier[4];
};
extern CommandResponse commandResponse;
// Input Buffer:
#define       FLOID_COMMAND_ACC_BUFFER_SIZE         (128)
extern byte   accBuffer[FLOID_COMMAND_ACC_BUFFER_SIZE];
// Period Output Time:
extern unsigned long lastStatusOutputTime;
boolean setupCommands();
int     loopCommands();
void    setBooleanToByteOffset(byte* bytes, boolean b, int offset);
void    getBooleanFromByteOffset(byte* bytes, boolean *b, int offset);
void    setIntToByteOffset(byte* bytes, int i, int offset);
void    getIntFromByteOffset(byte* bytes, int *i, int offset);
void    setUnsignedLongToByteOffset(byte* bytes, unsigned long l, int offset);
void    getUnsignedLongFromByteOffset(byte* bytes, unsigned long *l, int offset);
void    setLongToByteOffset(byte* bytes, long l, int offset);
void    getLongFromByteOffset(byte* bytes, long *l, int offset);
void    setFloatToByteOffset(byte* bytes, float f, int offset);
void    getFloatFromByteOffset(byte* bytes, float *f, int offset);
void    getByteFromByteOffset(byte* bytes, byte *b, int offset);
void    setByteToByteOffset(byte* bytes, byte b, int offset);
void    sendGoodCommandResponse(long commandNumber, int subStatus, byte* threeByteIdentifier, boolean* sentResponse);
void    sendErrorCommandResponse(long commandNumber, int subStatus, byte* threeByteIdentifier, boolean* sentResponse);
void    sendLogicErrorCommandResponse(long commandNumber, int subStatus, byte* threeByteIdentifier, boolean* sentResponse);
void    sendCommandResponse(long commandNumber, int commandStatus, int subStatus, byte* threeByteIdentifier);
void    printCommandResponseOffsets();
boolean sendCommandResponse();
#endif /* __FLOIDBOARD3_COMMANDS_H__ */
