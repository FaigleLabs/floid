//--------------------------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs                  //
//  http://www.faiglelabs.com                             //
//  A free artifact from the licensed Floid Drone project //
//  All rights reserved.                                  //
//  This code is free to use or modify:                   //
//   No warranty, license or restriction                  //
//--------------------------------------------------------//
// ConvertFloat.h:
#ifndef	__CONVERTFLOAT_H__
#define	__CONTERTFLOAT_H__
bool convertFloat(float val, char* buffer, int bufferSize, int decimalPoints);
#endif /* __FLOIDBOARD3_H__ */
