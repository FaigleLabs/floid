//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// Floidboard3_Heli.cpp:
#include <FloidBoard3.h>
// Heli Servos:
boolean setupHeliServos()
{
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    floidStatus.heliServoOn[heliIndex]       = false;
    setDefaultHeliCyclicCollectiveValues(heliIndex);
    floidModelCalculateHeliServosFromCollectivesAndCyclics(heliIndex, true);
    // OK, now attach the servos after these values are set:
    for(int servoIndex=0; servoIndex<NUMBER_HELI_SERVOS; ++servoIndex)
    {
      servoAttach(SERVO_TYPE_HELI, heliIndex, servoIndex, servoPins[heliIndex][servoIndex], floidModelParameters.floidModelHeliServoMinPulseWidthValue, floidModelParameters.floidModelHeliServoMaxPulseWidthValue);
    }
  }
  floidStatus.helisOn = false;
  return true;
}
void setDefaultHeliCyclicCollectiveValues(int heliIndex)
{
  floidModelStatus.floidModelCollectiveValue[heliIndex] = floidModelParameters.floidModelCollectiveDefaultValue;
  for(int servoIndex=0; servoIndex<NUMBER_HELI_SERVOS; ++servoIndex)
  {
    floidModelStatus.floidModelCyclicValue[heliIndex][servoIndex] = floidModelParameters.floidModelCyclicDefaultValue;
  }
}
void startHeliServos(int heliIndex)
{
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_HELI_SERVO_START);
    debugPrintln(heliIndex);
  }
  setDefaultHeliCyclicCollectiveValues(heliIndex);
  // Use the collectives and cyclics to set the servos:
  floidModelCalculateHeliServosFromCollectivesAndCyclics(heliIndex, true);
  // Mark them as deployed and on:
  floidStatus.heliServoOn[heliIndex]  = true;
}
void stopHeliServos(int heliIndex)
{
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_HELI_SERVO_STOP);
    debugPrintln(heliIndex);
  }
  floidStatus.heliServoOn[heliIndex] = false;
}
void startHeliServos()
{
  for(int i=0; i<NUMBER_HELICOPTERS; ++i)
  {
    startHeliServos(i);
  }
}
void stopHeliServos()
{
  for(int i=0; i<NUMBER_HELICOPTERS; ++i)
  {
    stopHeliServos(i);
  }
  floidStatus.helisOn = false;
}
