//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_ESC.h:
#ifndef	__FLOIDBOARD3_ESC_H__
#define	__FLOIDBOARD3_ESC_H__
// ESC logic:
#define              ESC_TYPE_EXCEED_RC_MODE_DELAY                             (1000ul)
#define              ESC_TYPE_EXCEED_RC_OPTION_DELAY                           (1000ul)
#define              ESC_TYPE_EXCEED_RC_BATTERY_TYPE_ITEM                      (1)
#define              ESC_TYPE_EXCEED_RC_CUTOFF_MODE_ITEM                       (2)
#define              ESC_TYPE_EXCEED_RC_CUTOFF_THRESHOLD_ITEM                  (3)
#define              ESC_TYPE_EXCEED_RC_START_MODE_ITEM                        (4)
#define              ESC_TYPE_EXCEED_RC_TIMING_ITEM                            (5)
#define              ESC_TYPE_EXCEED_RC_BRAKE_MODE_OFF                         (0)
#define              ESC_TYPE_EXCEED_RC_BRAKE_MODE_ON                          (1)
#define              ESC_TYPE_EXCEED_RC_BATTERY_TYPE_LITHIUM                   (0)
#define              ESC_TYPE_EXCEED_RC_BATTERY_TYPE_NICKEL                    (1)
#define              ESC_TYPE_EXCEED_RC_CUTOFF_MODE_SOFT_CUT                   (0)
#define              ESC_TYPE_EXCEED_RC_CUTOFF_MODE_CUT_OFF                    (1)
#define              ESC_TYPE_EXCEED_RC_CUTOFF_THRESHOLD_LOW                   (0)
#define              ESC_TYPE_EXCEED_RC_CUTOFF_THRESHOLD_MED                   (1)
#define              ESC_TYPE_EXCEED_RC_CUTOFF_THRESHOLD_HIGH                  (2)
#define              ESC_TYPE_EXCEED_RC_START_MODE_NORMAL                      (0)
#define              ESC_TYPE_EXCEED_RC_START_MODE_SOFT                        (1)
#define              ESC_TYPE_EXCEED_RC_START_MODE_SUPER_SOFT                  (2)
#define              ESC_TYPE_EXCEED_RC_TIMING_LOW                             (0)
#define              ESC_TYPE_EXCEED_RC_TIMING_MEDIUM                          (1)
#define              ESC_TYPE_EXCEED_RC_TIMING_HIGH                            (2)
#define              ESC_TYPE_EXCEED_RC_NUMBER_OPTIONS                         (6)
#define              ESC_TYPE_EXCEED_RC_ON_DELAY                               (2000ul)
#define              ESC_TYPE_EXCEED_RC_PROGRAM_MODE_DELAY                     (5000ul)
#define              ESC_TYPE_EXCEED_RC_EXTRA_DELAY                            (100ul)
#define              ESC_TYPE_EXCEED_RC_PROGRAM_MODE_OPTION_DELAY              (3000ul)
#define              ESC_TYPE_EXCEED_RC_PROGRAM_MODE_VALUE_DELAY               (2000ul)
#define              ESC_TYPE_DYNAM_BRAKE_MODE_DELAY                           (5000ul)
#define              ESC_TYPE_DYNAM_BRAKE_MODE_EXTRA_DELAY                     (100ul)
// Storage:
extern const int     esc_type_exceed_rc_options_array[ESC_TYPE_EXCEED_RC_NUMBER_OPTIONS];
// ESC:
boolean setupESCs(boolean fullSetup);
boolean setupESCSetup(int escIndex);
void    setupESCExtra(int escIndex);
void    setESCValue(int escIndex, float escValue);
void    setESCHigh(int escIndex);
void    setESCLow(int escIndex);
void    startESC(int escIndex);
void    stopESC(int escIndex);
// Specialty ESC routines:
void    esc_type_exceed_rc_setup_ranges(int escIndex);
void    esc_type_exceed_rc_set_options(int escIndex, int optionIndex, int optionValue);
void    esc_dynam_switch_brake_mode(int escIndex);
#endif /* __FLOIDBOARD3_ESC_H__ */
