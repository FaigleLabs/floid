//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Camera.h
// faiglelabs.com
#ifndef	__FLOIDBOARD3_CAMERA_H__
#define	__FLOIDBOARD3_CAMERA_H__
extern byte          cameraPanTiltIndex;
extern boolean       cameraDeployed;
extern unsigned long cameraFireTime;
boolean setupCamera();
void    loopCamera();
void    startCamera();
void    stopCamera();
void    setCameraTarget(float x, float y, float z);
void    setCameraAngle(byte pan, byte tilt);
void    setCameraScan(byte panMin, byte panMax, unsigned long panPeriod, byte tiltMin, byte tiltMax, unsigned long tiltPeriod);
void    setCameraPanTiltIndex(byte index);
#endif /* __FLOIDBOARD3_CAMERA_H__ */
