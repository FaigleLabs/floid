//--------------------------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs                  //
//  http://www.faiglelabs.com                             //
//  A free artifact from the licensed Floid Drone project //
//  All rights reserved.                                  //
//  This code is free to use or modify:                   //
//   No warranty, license or restriction                  //
//--------------------------------------------------------//
// PMTKHandler.cpp
#include <FloidBoard3.h>

// For utility routines:
#define PMTK_BAUD_CHANGE_DELAY       (200)
#define PMTK_COMMAND_PROCESS_TIMEOUT (200ul)


// PMTKNmeaSentences:
// ------------------
// Constructor:
PMTKNmeaSentences::PMTKNmeaSentences()
{
  pmtkResetNmeaSentences();
}
// Routines:
void PMTKNmeaSentences::pmtkResetNmeaSentences()
{
  for(byte i=0; i<PMTK_NMEA_NUMBER_SENTENCES; ++i)
  {
    pmtkNmeaSentences[i] = 0;
  }
}
byte PMTKNmeaSentences::pmtkSetNmeaSentenceRate(byte nmeaSentence, byte rate)
{
  if(nmeaSentence > PMTK_NMEA_NUMBER_SENTENCES)
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  if(rate > PMTK_NMEA_SENTENCE_MAX_RATE_VALUE)
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  pmtkNmeaSentences[nmeaSentence] = rate;
  return PMTK_ROUTINE_SUCCESS;
}
// PMTKHandler:
// ------------
// Constructor:
PMTKHandler::PMTKHandler()
{
  init();
}
PMTKHandler::PMTKHandler(HardwareSerial* gpsSerial, HardwareSerial* debugSerial)
{
  init();
  this->gpsSerial           = gpsSerial;
  this->debugSerial         = debugSerial;
}
void PMTKHandler::init()
{
  gpsSerial                                = NULL;
  debugSerial                              = NULL;
  pmtkLastCommandStatus                    = PMTK_LAST_COMMAND_NO_STATUS;
  pmtkCurrentSendBufferPos                 = 0;
  pmtkSendBuffer[pmtkCurrentSendBufferPos] = '\0';
  pmtkProcessingState                      = PMTK_STATE_WAITING_FOR_DOLLAR;
  pmtkInputBufferPosition                  = 0;
  pmtkBtMacAddress[0]                      = '\0';

  pmtkResetStatus();
  pmtkResetSendBuffer();
}
// Utilities:
void PMTKHandler::pmtkResetSendBuffer()
{
  pmtkSendBuffer[0]        = '$';
  pmtkSendBuffer[1]        = 'P';
  pmtkSendBuffer[2]        = 'M';
  pmtkSendBuffer[3]        = 'T';
  pmtkSendBuffer[4]        = 'K';
  pmtkSendBuffer[5]        = '\0';
  pmtkCurrentSendBufferPos = PMTK_SEND_BUFFER_HEADER_LENGTH;
}
void PMTKHandler::pmtkResetStatus()
{
  // Reset:  Various flags and make sure the buffers are terminated, etc
  pmtkValidityFlags               = 0;
  pmtkLastCommandStatus           = PMTK_LAST_COMMAND_NO_STATUS;
  pmtkLastResponseReceived        = PMTK_LAST_RESPONSE_RECEIVED_NONE;
  pmtkLastResponseReceivedValue   = 0;
  pmtkLastResponseReceivedName[0] = '\0';
  pmtkNmeaSentences.pmtkResetNmeaSentences();
  pmtkLastAckCommandResponse      = 0;
  pmtkFixRate                     = 0;
  pmtkBtMacAddress[0]             = '\0';
  pmtkDgpsMode                    = PMTK_DGPS_MODE_NONE;
  pmtkDatum                       = 0;
  pmtkEasyEnabled                 = false;
  pmtkSbasEnabled                 = false;
  pmtkSbasEnabledInTest           = false;
}
// Attach to gps on various bauds and attempt to change to our desired rate:
void PMTKHandler::pmtkSetupGpsSerial(int numberBaudRatesToTry, const int* baudRatesToTry, int pmtkBaudRateToSet)
{
  for(int i=0; i< numberBaudRatesToTry; ++i)
  {
    if(debugSerial != NULL)
    {
      debugSerial->print("@");
      debugSerial->println(pmtkBauds[baudRatesToTry[i]]);
    }
    gpsSerial->begin(pmtkBauds[baudRatesToTry[i]]);
    delay(PMTK_BAUD_CHANGE_DELAY);
    printPmtkRoutineResult(pmtkSendSetNmeaBaudRate(pmtkBaudRateToSet));
    processPmtkCommandResponse(false);
    delay(PMTK_BAUD_CHANGE_DELAY);
    // We disconnect from the GPS unless it is the last baud rate and we are told to:
    gpsSerial->end();
    // Delay:
    delay(PMTK_BAUD_CHANGE_DELAY);
  }
}
// Open the gps serial at the baud:
void PMTKHandler::pmtkConnectToGps(int baudRate)
{
  gpsSerial->begin(pmtkBauds[baudRate]);
}
// PMTK Packet send and checksum routines:
byte PMTKHandler::pmtkSendSimplePacket(const char* command)
{
  return pmtkSendPacket(command, "");
}
byte PMTKHandler::pmtkSendPacket(const char* command, const char* data)
{
  pmtkResetSendBuffer();
  if(debugSerial != NULL)
  {
    (*debugSerial).print(">");
    (*debugSerial).print(" [");
    (*debugSerial).print(command);
    (*debugSerial).print("] ");
  }
  pmtkLastCommandStatus           = PMTK_LAST_COMMAND_NO_STATUS;
  pmtkLastResponseReceived        = PMTK_LAST_RESPONSE_RECEIVED_NONE;
  pmtkLastResponseReceivedValue   = 0;
  pmtkLastResponseReceivedName[0] = '\0';
  pmtkSetValidity(PMTK_VALIDITY_MASK_LAST_RESPONSE_RECEIVED, false);

  byte pmtkRoutineStatus   = pmtkInsertCommand(command);
  // Error?
  if(pmtkRoutineStatus != PMTK_ROUTINE_SUCCESS)
  {
    return pmtkRoutineStatus;
  }
  int dataLength = strlen(data);
  if(dataLength < PMTK_SEND_BUFFER_LENGTH - PMTK_SEND_BUFFER_HEADER_LENGTH - PMTK_SEND_BUFFER_FOOTER_LENGTH)
  {
    strcpy(&pmtkSendBuffer[pmtkCurrentSendBufferPos], data);
    pmtkCurrentSendBufferPos += dataLength;
    pmtkRoutineStatus         = pmtkChecksumPacket();
    // Error?
    if(pmtkRoutineStatus != PMTK_ROUTINE_SUCCESS)
    {
      return pmtkRoutineStatus;
    }
    return pmtkSendPacket();
  }
  else
  {
    return PMTK_ROUTINE_FAIL_BUFFER_OVERRUN;
  }
}
byte PMTKHandler::pmtkChecksumPacket()
{
  // 0. Verify this will fit into buffer:
  if(pmtkCurrentSendBufferPos < PMTK_SEND_BUFFER_LENGTH - PMTK_SEND_BUFFER_FOOTER_LENGTH)
  {
    unsigned char checkSum = 0;
    // 1. Calculate the checksum
    // Note: First byte is "$":
    for(int i=1; i<pmtkCurrentSendBufferPos; ++i)
    {
      checkSum ^= pmtkSendBuffer[i];
    }
    // 2. Put in: *CC\r\n
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = '*';
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = pmtkGetAsciiDigit(checkSum>>4);
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = pmtkGetAsciiDigit(checkSum&0x0f);
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = '\r';
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = '\n';
  }
  else
  {
    return PMTK_ROUTINE_FAIL_BUFFER_OVERRUN;
  }
  return PMTK_ROUTINE_SUCCESS;
}
char PMTKHandler::pmtkGetAsciiDigit(unsigned char d)
{
    if(d<=9)
    {
        return '0' + d;
    }
    return 'A' + (d-10);
}
byte PMTKHandler::pmtkSendPacket()
{
  if(pmtkInputBufferPosition >= (PMTK_SEND_BUFFER_LENGTH-1))
  {
    // Return buffer overrun - there should be one byte at the end to terminate for printing at pmtkInputBufferPosition
    return PMTK_ROUTINE_FAIL_BUFFER_OVERRUN;
  }
  // Terminate if forgotten:
  pmtkSendBuffer[pmtkCurrentSendBufferPos] = '\0';
  if(gpsSerial != NULL)
  {
    for(int i=0; i<pmtkCurrentSendBufferPos; ++i)
    {
      (*gpsSerial).write(pmtkSendBuffer[i]);
    }
    // Terminate with the tail for potential debug print next:
    pmtkSendBuffer[pmtkCurrentSendBufferPos - 2] = '\0';
    // Debug
    if(debugSerial != NULL)
    {
      (*debugSerial).print(pmtkSendBuffer);
      (*debugSerial).print(' ');
    }
    return PMTK_ROUTINE_SUCCESS;
  }
  else
  {
    return PMTK_ROUTINE_FAIL_NO_SERIAL_PORT;
  }
}
//////////////////
// Commands:    //
//////////////////
// Send test packet:
byte PMTKHandler::pmtkSendTest()
{
  pmtkResetStatus();
  return pmtkSendSimplePacket(PMTK_TEST);
}
// Startups:
byte PMTKHandler::pmtkSendHotStart()
{
  pmtkResetStatus();
  return pmtkSendSimplePacket(PMTK_CMD_HOT_START);
}
byte PMTKHandler::pmtkSendWarmStart()
{
  pmtkResetStatus();
  return pmtkSendSimplePacket(PMTK_CMD_WARM_START);
}
byte PMTKHandler::pmtkSendColdStart()
{
  pmtkResetStatus();
  return pmtkSendSimplePacket(PMTK_CMD_COLD_START);
}
byte PMTKHandler::pmtkSendFullColdStart()
{
  pmtkResetStatus();
  return pmtkSendSimplePacket(PMTK_CMD_FULL_COLD_START);
}
// Clear Flashes and Standby:
byte PMTKHandler::pmtkSendClearFlash()
{
  return pmtkSendSimplePacket(PMTK_CMD_CLEAR_FLASH);
}
byte PMTKHandler::pmtkSendClearEpo()
{
  return pmtkSendSimplePacket(PMTK_CMD_CLEAR_EPO);
}
byte PMTKHandler::pmtkSendStandbyMode(bool sleepMode)
{
  if(sleepMode)
  {
    return pmtkSendPacket(PMTK_CMD_STANDBY_MODE,",1"); // Sleep mode
  }
  else
  {
    return pmtkSendPacket(PMTK_CMD_STANDBY_MODE,",0"); // Stop mode
  }
}
//  Logging:
byte PMTKHandler::pmtkSendCommandLog()
{
  // Not implemented:
  return PMTK_ROUTINE_FAIL_NOT_IMPLEMENTED;
}
byte PMTKHandler::pmtkSendQueryLocusLoggerStatus()
{
    return pmtkSendSimplePacket(PMTK_CMD_QUERY_LOCUS_LOGGER_STATUS);
}
byte PMTKHandler::pmtkSendEraseLocusLoggerFlash()
{
    return pmtkSendSimplePacket(PMTK_CMD_ERASE_LOCUS_LOGGER_FLASH);
}
byte PMTKHandler::pmtkSendStartStopLocusLogger(bool start)
{
  if(start)
  {
    return pmtkSendPacket(PMTK_CMD_STANDBY_MODE,",1"); // Start
  }
  else
  {
    return pmtkSendPacket(PMTK_CMD_STANDBY_MODE,",0"); // Stop
  }
}
byte PMTKHandler::pmtkSendLocusLogNow()
{
  return pmtkSendSimplePacket(PMTK_CMD_LOCUS_LOG_NOW);
}
// Parameters:
byte PMTKHandler::pmtkSendSetOutputRate(int interval)
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_OUTPUT_RATE, false);
  pmtkOutputRate = interval;
  if((interval < PMTK_POSITION_RATE_MIN) || (interval > PMTK_POSITION_RATE_MAX))
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  char tempBuffer[PMTK_TEMP_BUFFER_SIZE_1];
  if(snprintf(tempBuffer, PMTK_TEMP_BUFFER_SIZE_1, ",%d", interval)>=PMTK_TEMP_BUFFER_SIZE_1)
  {
    return PMTK_ROUTINE_FAIL_TEMP_BUFFER_OVERRUN;
  }
  return pmtkSendPacket(PMTK_SET_OUTPUT_RATE,tempBuffer);
}
byte PMTKHandler::pmtkSendSetFixRate(int interval)
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_FIX_RATE, false);
  if((interval < PMTK_FIX_RATE_MIN) || (interval > PMTK_FIX_RATE_MAX))
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  char tempBuffer[PMTK_TEMP_BUFFER_SIZE_1];
  if(snprintf(tempBuffer, PMTK_TEMP_BUFFER_SIZE_1, ",%d,0,0,0,0", interval)>=PMTK_TEMP_BUFFER_SIZE_1)
  {
    return PMTK_ROUTINE_FAIL_TEMP_BUFFER_OVERRUN;
  }
  return pmtkSendPacket(PMTK_SET_FIX_RATE,tempBuffer);
}
byte PMTKHandler::pmtkSendSetDgpsMode(byte dgpsMode)
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_DGPS_MODE, false);
  switch(dgpsMode)
  {
    case PMTK_DGPS_MODE_NONE:
         return pmtkSendPacket(PMTK_SET_DGPS_MODE,",0");
    case PMTK_DGPS_MODE_RTCM:
         return pmtkSendPacket(PMTK_SET_DGPS_MODE,",1");
    case PMTK_DGPS_MODE_WAAS:
         return pmtkSendPacket(PMTK_SET_DGPS_MODE,",2");
    default:
         return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
}
byte PMTKHandler::pmtkSendSetAlwaysLocateDefaultConfig()
{
  // Not implemented:
  return PMTK_ROUTINE_FAIL_NOT_IMPLEMENTED;
}
byte PMTKHandler::pmtkSendSetPeriodMode(byte mode, unsigned long runTime, unsigned long sleepTime, unsigned long secondRunTime, unsigned long secondSleepTime)
{
  // Verify mode:
  switch(mode)
  {
    case PMTK_PERIOD_BACK_TO_NORMAL:
    case PMTK_PERIOD_PERIODIC_BACKUP_MODE:
    case PMTK_PERIOD_PERIODIC_STANDBY_MODE:
    case PMTK_PERIOD_PERPETUAL_BACKUP_MODE:
    case PMTK_PERIOD_ALWAYS_LOCATE_STANDBY_MODE:
    case PMTK_PERIOD_ALWAYS_LOCATE_BACKUP_MODE:
       break;
    default:
    {
       return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
     }
  }
  // Verify times are in range:
  if((runTime > PMTK_RUN_AND_SLEEP_TIME_MAX) || (sleepTime > PMTK_RUN_AND_SLEEP_TIME_MAX)
      || (secondRunTime > PMTK_RUN_AND_SLEEP_TIME_MAX) || (secondSleepTime > PMTK_RUN_AND_SLEEP_TIME_MAX))
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  if((runTime < PMTK_RUN_AND_SLEEP_TIME_MIN && runTime>0) || (sleepTime < PMTK_RUN_AND_SLEEP_TIME_MIN && sleepTime>0)
      || (secondRunTime < PMTK_RUN_AND_SLEEP_TIME_MIN && secondRunTime > 0) || (secondSleepTime < PMTK_RUN_AND_SLEEP_TIME_MIN && secondSleepTime > 0))
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  char tempBuffer[PMTK_TEMP_BUFFER_SIZE_1];
  if(snprintf(tempBuffer, PMTK_TEMP_BUFFER_SIZE_1, ",%d,%lu,%lu,%lu,%lu", (int)mode, runTime, sleepTime, secondRunTime, secondSleepTime)>=PMTK_TEMP_BUFFER_SIZE_1)
  {
    return PMTK_ROUTINE_FAIL_TEMP_BUFFER_OVERRUN;
  }
  return pmtkSendPacket(PMTK_SET_PERIOD_MODE,tempBuffer);
}
byte PMTKHandler::pmtkSendSetNmeaBaudRate(byte baudRate)
{
  // Valid baud rate index?
  if(baudRate<PMTK_BAUD_RATE_NUMBER_BAUDS)
  {
    char tempBuffer[PMTK_TEMP_BUFFER_SIZE_1];
    if(snprintf(tempBuffer, PMTK_TEMP_BUFFER_SIZE_1, ",%ld", pmtkBauds[baudRate])>=PMTK_TEMP_BUFFER_SIZE_1)
    {
      return PMTK_ROUTINE_FAIL_TEMP_BUFFER_OVERRUN;
    }
    else
    {
      return pmtkSendPacket(PMTK_SET_NMEA_BAUDRATE, tempBuffer);
    }
  }
  else
  {
    return PMTK_ROUTINE_FAIL_LOGIC_ERROR;
  }
}
byte PMTKHandler::pmtkSendSetAic(bool enable)
{
  if(enable)
  {
    return pmtkSendPacket(PMTK_SET_ENABLE_AIC, ",1");  // Enable
  }
  else
  {
    return pmtkSendPacket(PMTK_SET_ENABLE_AIC, ",0");  // Disable
  }
}
byte PMTKHandler::pmtkSendSetDatum(byte datum)
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_DATUM, false);
  char tempBuffer[PMTK_TEMP_BUFFER_SIZE_1];
  if(snprintf(tempBuffer, PMTK_TEMP_BUFFER_SIZE_1, ",%d", (int)datum)>=PMTK_TEMP_BUFFER_SIZE_1)
  {
    return PMTK_ROUTINE_FAIL_TEMP_BUFFER_OVERRUN;
  }
  return pmtkSendPacket(PMTK_SET_DATUM,tempBuffer);
}
byte PMTKHandler::pmtkSendSetDatumAdvanced()
{
  // Not implemented:
  return PMTK_ROUTINE_FAIL_NOT_IMPLEMENTED;
}
byte PMTKHandler::pmtkSendSetRtcTime()
{
  // Not implemented:
  return PMTK_ROUTINE_FAIL_NOT_IMPLEMENTED;
}
byte PMTKHandler::pmtkSendSetQzssNmeaOutput(bool enable)
{
  if(enable)
  {
    return pmtkSendPacket(PMTK_SET_QZSS_NMEA_OUTPUT, ",1");  // Enable
  }
  else
  {
    return pmtkSendPacket(PMTK_SET_QZSS_NMEA_OUTPUT, ",0");  // Disable
  }
}
// Not sure why this is different than above:
byte PMTKHandler::pmtkSendSetStopQzss(bool stop)
{
  if(stop)
  {
    return pmtkSendPacket(PMTK_SET_STOP_QZSS, ",1");  // Stop
  }
  else
  {
    return pmtkSendPacket(PMTK_SET_STOP_QZSS, ",0");  // Do not stop
  }
}
byte PMTKHandler::pmtkSendSetGnssSearchMode(bool gps, bool glonass)
{

  if(gps)
  {
    if(glonass)
    {
      return pmtkSendPacket(PMTK_SET_GNSS_SEARCH_MODE, ",1,1");
    }
    else
    {
      return pmtkSendPacket(PMTK_SET_GNSS_SEARCH_MODE, ",1,0");
    }
  }
  else
  {
    if(glonass)
    {
      return pmtkSendPacket(PMTK_SET_GNSS_SEARCH_MODE, ",0,1");
    }
    else
    {
      return pmtkSendPacket(PMTK_SET_GNSS_SEARCH_MODE, ",0,0");
    }
  }
}
byte PMTKHandler::pmtkSendSetStaticNavThreshold333(float threshold)
{
  // Verify range:
  if((threshold<0) || (threshold>0 && threshold < PMTK_STATIC_NAV_THRESHOLD_MIN) || (threshold > PMTK_STATIC_NAV_THRESHOLD_MAX))
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  char tempBuffer[PMTK_TEMP_BUFFER_SIZE_1];
  tempBuffer[0] =',';
  convertFloat(threshold, &tempBuffer[1], PMTK_TEMP_BUFFER_SIZE_1-1, 1);
  return pmtkSendPacket(PMTK_SET_STATIC_NAV_THRESHOLD_333, tempBuffer);
}
byte PMTKHandler::pmtkSendSetUserOption()
{
  // Not implemented:
  return PMTK_ROUTINE_FAIL_NOT_IMPLEMENTED;
}
byte PMTKHandler::pmtkSendSetStaticNavThreshold332(float threshold)
{
  // Verify range:
  if((threshold<0) || (threshold>0 && threshold < PMTK_STATIC_NAV_THRESHOLD_MIN) || (threshold > PMTK_STATIC_NAV_THRESHOLD_MAX))
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  char tempBuffer[PMTK_TEMP_BUFFER_SIZE_1];
  tempBuffer[0] =',';
  convertFloat(threshold, &tempBuffer[1], PMTK_TEMP_BUFFER_SIZE_1-1, 1);
  return pmtkSendPacket(PMTK_SET_STATIC_NAV_THRESHOLD_332, tempBuffer);
}
byte PMTKHandler::pmtkSendSetEnableEasy(bool enable)
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_EASY_ENABLED, false);
  if(enable)
  {
    return pmtkSendPacket(PMTK_SET_QUERY_ENABLE_EASY, ",1,1");  // First parameter is to set, second is enable
  }
  else
  {
    return pmtkSendPacket(PMTK_SET_QUERY_ENABLE_EASY, ",1,0");
  }
}
byte PMTKHandler::pmtkSendSetSbasEnabled(bool enable)
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_ENABLED, false);
  if(enable)
  {
    return pmtkSendPacket(PMTK_SET_SBAS_ENABLED, ",1");
  }
  else
  {
    return pmtkSendPacket(PMTK_SET_SBAS_ENABLED, ",0");
  }
}
byte PMTKHandler::pmtkSendSetPwrSaveMode(bool pwrSave)
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_PWR_SAVE_MODE, false);
  if(pwrSave)
  {
    return pmtkSendPacket(PMTK_SET_PWR_SAVE_MODE, ",1");
  }
  else
  {
    return pmtkSendPacket(PMTK_SET_PWR_SAVE_MODE, ",0");
  }
}
byte PMTKHandler::pmtkSendSetSbasInTest(bool enable)
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_IN_TEST_ENABLED, false);
  if(enable)
  {
    return pmtkSendPacket(PMTK_SET_SBAS_IN_TEST, ",0");
  }
  else
  {
    return pmtkSendPacket(PMTK_SET_SBAS_IN_TEST, ",1");
  }
}
byte PMTKHandler::pmtkSendSetUtcTime()
{
  // Not implemented:
  return PMTK_ROUTINE_FAIL_NOT_IMPLEMENTED;
}
byte PMTKHandler::pmtkSendSetInitialPositionAndTime()
{
  // Not implemented:
  return PMTK_ROUTINE_FAIL_NOT_IMPLEMENTED;
}
// Queries:
byte PMTKHandler::pmtkSendQueryFixRate()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_FIX_RATE, false);
  return pmtkSendSimplePacket(PMTK_QUERY_FIX_RATE);
}
byte PMTKHandler::pmtkSendQueryDgpsMode()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_DGPS_MODE, false);
  return pmtkSendSimplePacket(PMTK_QUERY_DGPS_MODE);
}
byte PMTKHandler::pmtkSendQuerySbasEnabled()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_ENABLED, false);
  return pmtkSendSimplePacket(PMTK_QUERY_SBAS_ENABLED);
}
byte PMTKHandler::pmtkSendQueryNmeaOutput()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_NMEA_SENTENCES, false);
  return pmtkSendSimplePacket(PMTK_QUERY_NMEA_OUTPUT);
}
byte PMTKHandler::pmtkSendQuerySbasInTest()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_IN_TEST_ENABLED, false);
  return pmtkSendSimplePacket(PMTK_QUERY_SBAS_IN_TEST);
}
byte PMTKHandler::pmtkSendQueryDatum()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_DATUM, false);
  return pmtkSendSimplePacket(PMTK_QUERY_DATUM);
}
byte PMTKHandler::pmtkSendQueryDatumAdvanced()
{
  return pmtkSendSimplePacket(PMTK_QUERY_DATUM_ADVANCED);
}
byte PMTKHandler::pmtkSendQueryAic()
{
  return pmtkSendSimplePacket(PMTK_QUERY_AIC);
}
byte PMTKHandler::pmtkSendQueryGpsMode()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_GPS_ENABLED, false);
  pmtkSetValidity(PMTK_VALIDITY_MASK_GLONASS_ENABLED, false);
  return pmtkSendSimplePacket(PMTK_QUERY_GPS_MODE);
}
byte PMTKHandler::pmtkSendQueryUserOption()
{
  return pmtkSendSimplePacket(PMTK_QUERY_USER_OPTION);
}
byte PMTKHandler::pmtkSendQueryBtMacAddress()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_BT_MAC_ADDRESS, false);
  return pmtkSendSimplePacket(PMTK_QUERY_BT_MAC_ADDRESS);
}
byte PMTKHandler::pmtkSendQueryFirmwareVersion()
{
  return pmtkSendSimplePacket(PMTK_QUERY_FIRMWARE_VERSION);
}
byte PMTKHandler::pmtkSendQueryFirmwareInfo()
{
  return pmtkSendSimplePacket(PMTK_QUERY_FIRMWARE_INFO);
}
byte PMTKHandler::pmtkSendQueryPwrSaveMode()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_PWR_SAVE_MODE, false);
  return pmtkSendSimplePacket(PMTK_QUERY_PWR_SAVE_MODE);
}
byte PMTKHandler::pmtkSendQueryEpoStatus()
{
  return pmtkSendPacket(PMTK_QUERY_EPO_STATUS,",0"); // 0 is request status
}
byte PMTKHandler::pmtkSendQueryLocusData(bool fullDump)
{
  if(fullDump)
  {
    return pmtkSendPacket(PMTK_QUERY_LOCUS_DATA, ",0"); // Full
  }
  else
  {
    return pmtkSendPacket(PMTK_QUERY_LOCUS_DATA, ",1"); // In Use
  }
}
byte PMTKHandler::pmtkSendQueryAvailableEph(int seconds)
{
  // Verify date:
  if((seconds < PMTK_EPH_QUERY_MIN_SECONDS) || (seconds > PMTK_EPH_QUERY_MAX_SECONDS))
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  char tempBuffer[PMTK_TEMP_BUFFER_SIZE_1];
  if(snprintf(tempBuffer, PMTK_TEMP_BUFFER_SIZE_1, ",%d", seconds)>=PMTK_TEMP_BUFFER_SIZE_1)
  {
    return PMTK_ROUTINE_FAIL_TEMP_BUFFER_OVERRUN;
  }
  return pmtkSendPacket(PMTK_QUERY_AVAILABLE_EPH,tempBuffer);
}
byte PMTKHandler::pmtkSendQueryAvailableAlm(int days)
{
  // Verify day range:
  if((days < PMTK_ALMANAC_AVAILABILITY_DAYS_MIN) || (days > PMTK_ALMANAC_AVAILABILITY_DAYS_MAX))
  {
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  char tempBuffer[PMTK_TEMP_BUFFER_SIZE_1];
  if(snprintf(tempBuffer, PMTK_TEMP_BUFFER_SIZE_1, ",%d", days)>=PMTK_TEMP_BUFFER_SIZE_1)
  {
    return PMTK_ROUTINE_FAIL_TEMP_BUFFER_OVERRUN;
  }
  return pmtkSendPacket(PMTK_QUERY_AVAILABLE_ALM,tempBuffer);
}
byte PMTKHandler::pmtkSendQueryEnableEasy()
{
  pmtkSetValidity(PMTK_VALIDITY_MASK_EASY_ENABLED, false);
  return pmtkSendPacket(PMTK_SET_QUERY_ENABLE_EASY,",0"); // 0 is query status
}
// Main read routine:
bool PMTKHandler::pmtkRead()
{
  // Read until we have no bytes or we have a good decode:
  while((*gpsSerial).available())
  {
    int c=(*gpsSerial).read();
    if(pmtkDecode((byte)c))
    {
      return true; // We pass this on - we received a good packet
    }
  }
  return false;
}
// Main decode routine:
bool PMTKHandler::pmtkDecode(byte b)
{
  switch(pmtkProcessingState)
  {
    case PMTK_STATE_WAITING_FOR_DOLLAR:
    {
      pmtkInputBufferPosition = 0;
      if(b=='$')
      {
         pmtkProcessingState = PMTK_STATE_WAITING_FOR_NEWLINE;
      }
    }
    break;
    case PMTK_STATE_WAITING_FOR_NEWLINE:
    {
      switch(b)
      {
        case '$':
        {
          // Reset to start:
          pmtkInputBufferPosition = 0;
        }
        break;
        case '\n':
        {
          byte processPacketStatus = pmtkProcessPacket();
          pmtkProcessingState      = PMTK_STATE_WAITING_FOR_DOLLAR;
          pmtkInputBufferPosition  = 0;
          return processPacketStatus;
        }
        break;
        default:
        {
         // Store it if room:
         if(pmtkInputBufferPosition < PMTK_INPUT_BUFFER_SIZE)
         {
           pmtkInputBuffer[pmtkInputBufferPosition++] = b;
         }
         else
         {
           // Overflow - reset:
           pmtkProcessingState     = PMTK_STATE_WAITING_FOR_DOLLAR;
           pmtkInputBufferPosition = 0;
         }
       }
       break;
     }
   }
   break;
 }
 return false;  // We did not process a valid sentence:
}
bool PMTKHandler::pmtkProcessPacket()
{
  // Process packet if large enough:
  if(pmtkInputBufferPosition < 10) // Min sentence is 10: PMTK###*CC
  {
    return false;
  }
  // PMTK Sentence?:
  if((pmtkInputBuffer[0] != 'P') || (pmtkInputBuffer[1] != 'M') || (pmtkInputBuffer[2] != 'T') || (pmtkInputBuffer[3] != 'K'))
  {
    return false;
  }
  // * before last two chars?
  if(pmtkInputBuffer[pmtkInputBufferPosition - PMTK_INPUT_PACKET_ASTERISK_BACKSTEP] != '*')
  {
    if(debugSerial != NULL)
    {
      (*debugSerial).println("-NA");
    }

    return false;
  }
  // Checksum the input buffer and verify:
  if(!pmtkChecksumInputBuffer())
  {
    if(debugSerial != NULL)
    {
      (*debugSerial).println("-CK");
    }
    return false;
  }
  pmtkLastResponseReceivedName[0] = pmtkInputBuffer[PMTK_INPUT_PACKET_COMMAND_START];
  pmtkLastResponseReceivedName[1] = pmtkInputBuffer[PMTK_INPUT_PACKET_COMMAND_START+1];
  pmtkLastResponseReceivedName[2] = pmtkInputBuffer[PMTK_INPUT_PACKET_COMMAND_START+2];
  pmtkLastResponseReceivedName[3] = '\0';
  pmtkLastResponseReceivedValue = atoi(pmtkLastResponseReceivedName);
  pmtkSetValidity(PMTK_VALIDITY_MASK_LAST_RESPONSE_RECEIVED, true);
  // Terminate the packet with a \0 at the '*':
  pmtkInputBuffer[pmtkInputBufferPosition - PMTK_INPUT_PACKET_ASTERISK_BACKSTEP] = '\0';
  int dataBufferSize = pmtkInputBufferPosition - PMTK_INPUT_PACKET_DATA_START - PMTK_INPUT_PACKET_ASTERISK_BACKSTEP;
  // If debug print it starting with the #'s:
  if(debugSerial != NULL)
  {
    (*debugSerial).print("\n< ");
    (*debugSerial).print(&(pmtkInputBuffer[PMTK_INPUT_PACKET_COMMAND_START]));
    (*debugSerial).print(" ");
  }
  // Process the packet responses:
  //  Note: Order these in the order of the frequency for best performance
  if(pmtkCompareResponse(PMTK_ACK))
  {
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_ACK;
    // Two cases:
    //   - 1 is data size is: CMD,V  V=Value (or backwards - should have data-length of 5
    //   - 2 in one peculiar case the data for command 661 is returns as ,######## - 32-bit hex number, sot data-length of 5+1+8 = 14 - - We do not process this
    if((dataBufferSize == 5) || (dataBufferSize == 14))
    {
      // OK, lets get the command and the status:
      // Find the first comma:
      for(int i=0; i<dataBufferSize; ++i)
      {
        if(pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START + i] == ',')
        {
          // Terminate:
          pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START + i] = '\0';
          // get the integer:
          pmtkLastAckCommandResponse = atoi(&pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START]);
          // Set validity true:
          pmtkSetValidity(PMTK_VALIDITY_MASK_LAST_ACK_COMMAND_RESPONSE, true);
          // Get the value:
          switch(pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START + PMTK_ACK_RESPONSE_STATUS_OFFSET])
          {
            case PMTK_ACK_FLAG_INVALID:
            {
              pmtkLastCommandStatus = PMTK_LAST_COMMAND_INVALID;
              return true;
            }
            break;
            case PMTK_ACK_FLAG_SUCCESS:
            {
              pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
              switch(pmtkLastAckCommandResponse)
              {
                // Case output rate:
                case PMTK_SET_OUTPUT_RATE_RESPONSE:  // Set outputRate came back - mark it as valid
                {
                  pmtkSetValidity(PMTK_VALIDITY_MASK_OUTPUT_RATE, true);
                }
                break;
              }
              return true;
            }
            break;
            case PMTK_ACK_FLAG_UNSUPPORTED:
            {
              pmtkLastCommandStatus = PMTK_LAST_COMMAND_UNSUPPORTED;
              return true;
            }
            break;
            case PMTK_ACK_FLAG_VALID_BUT_FAIL:
            {
              pmtkLastCommandStatus = PMTK_LAST_COMMAND_VALID_BUT_FAIL;
              return true;
            }
            break;
            default:
            {
              pmtkLastCommandStatus = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
              return true;
            }
            break;
          }
        }
      }
    }
    pmtkSetValidity(PMTK_VALIDITY_MASK_LAST_ACK_COMMAND_RESPONSE, false);
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
    // Had a command - all should return true if matched command:
    return true;
  }
  else if(pmtkCompareResponse(PMTK_SYS_MESSAGE))
  {
    // Process System Message:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_SYS_MESSAGE;
    // We do not process this further...
    // Default success:
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_SUCCESS;
    return true;
  }
  else if(pmtkCompareResponse(PMTK_SYS_TEXT))
  {
    // Process System Text:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_SYS_TEXT;
    // We do not process this further...
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_SUCCESS;
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_FIX_CONTROL))
  {
    // Process Fix Control:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_FIX_RATE;
    // Check to see that this data has exactly four commas, and then capture the fix interval
    // Find the first comma:
    for(int i=0; i<dataBufferSize; ++i)
    {
      if(pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START + i] == ',')
      {
        // Terminate:
        pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START + i] = '\0';
        // get the integer:
        pmtkFixRate = atoi(&pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START]);
        // Set validity true:
        pmtkSetValidity(PMTK_VALIDITY_MASK_FIX_RATE, true);
        // Good success:
        pmtkLastCommandStatus    = PMTK_LAST_COMMAND_SUCCESS;
        return true;
      }
    }
    // We did not have a comma in the data:
    pmtkSetValidity(PMTK_VALIDITY_MASK_FIX_RATE, false);
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_DGPS_MODE))
  {
    // Process DPGS Mode:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_DGPS_MODE;
    if(dataBufferSize != 1)
    {
      pmtkLastCommandStatus = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
      return true;
    }
    switch(pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START])
    {
      case PMTK_DGPS_MODE_NONE_CHAR:
      {
        pmtkDgpsMode = PMTK_DGPS_MODE_NONE;
      }
      break;
      case PMTK_DGPS_MODE_RTCM_CHAR:
      {
        pmtkDgpsMode = PMTK_DGPS_MODE_RTCM;
      }
      break;
      case PMTK_DGPS_MODE_WAAS_CHAR:
      {
        pmtkDgpsMode = PMTK_DGPS_MODE_WAAS;
      }
      break;
      default:
      {
        pmtkSetValidity(PMTK_VALIDITY_MASK_DGPS_MODE, false);
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
        return true;
      }
      break;
    }
    // Falls through to here if successful:
    pmtkSetValidity(PMTK_VALIDITY_MASK_DGPS_MODE, true);
    // Good dgps mode:
    pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_SBAS_ENABLED))
  {
    // Process SBAS Enabled:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_SBAS_ENABLED;
    switch(pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START])
    {
      case PMTK_ENABLED_CHAR:
      {
        // Set enabled:
        pmtkSbasEnabled = true;
        // Set validity true:
        pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_ENABLED, true);
        // Set success:
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
      }
      break;
      case PMTK_DISABLED_CHAR:
      {
        // Set disabled:
        pmtkSbasEnabled = false;
        // Set validity true:
        pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_ENABLED, true);
        // Set success:
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
      }
      break;
      default:
      {
        // Set validity false:
        pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_ENABLED, false);
        // Bad data
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
      }
      break;
    }
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_PWR_SAVE_MODE))
  {
    // Process SBAS Enabled:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_PWR_SAVE_MODE;
    switch(pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START])
    {
      case PMTK_ENABLED_CHAR:
      {
        // Set enabled:
        pmtkPwrSaveMode = true;
        // Set validity true:
        pmtkSetValidity(PMTK_VALIDITY_MASK_PWR_SAVE_MODE, true);
        // Set success:
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
      }
      break;
      case PMTK_DISABLED_CHAR:
      {
        // Set disabled:
        pmtkPwrSaveMode = false;
        // Set validity true:
        pmtkSetValidity(PMTK_VALIDITY_MASK_PWR_SAVE_MODE, true);
        // Set success:
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
      }
      break;
      default:
      {
        // Set validity false:
        pmtkSetValidity(PMTK_VALIDITY_MASK_PWR_SAVE_MODE, false);
        // Bad data
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
      }
      break;
    }
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_NMEA_OUTPUT))
  {
    // Process NMEA Output:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_NMEA_OUTPUT;

    // 1. Make sure the data is the right length - should always be: (19*2)-1 = 37  - e.g. 19 zeros with commas between: (0,0,0,...,0) 
    if(dataBufferSize != PMTK_NMEA_DATA_BUFFER_REQUIRED_SIZE)
    {
      pmtkLastCommandStatus    = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
      pmtkSetValidity(PMTK_VALIDITY_MASK_NMEA_SENTENCES, false);
    }
    else
    {
      // Retrieve the data:
      pmtkLastCommandStatus    = PMTK_LAST_COMMAND_SUCCESS;
      // First batch:
      for(int i=0; i < PMTK_NMEA_SENTENCES_FIRST_BATCH_SIZE; ++i)
      {
        pmtkNmeaSentences.pmtkNmeaSentences[i] = pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START+i*2] - '0';
      }
      // Third batch:
      for(int i=0; i < PMTK_NMEA_SENTENCES_THIRD_BATCH_SIZE; ++i)
      {
        pmtkNmeaSentences.pmtkNmeaSentences[i+PMTK_NMEA_SENTENCES_FIRST_BATCH_SIZE] = pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START
                                                                                                      + (i
                                                                                                      +  PMTK_NMEA_SENTENCES_FIRST_BATCH_SIZE
                                                                                                      +  PMTK_NMEA_SENTENCES_SECOND_BATCH_SIZE)*2] - '0';
      }
      pmtkSetValidity(PMTK_VALIDITY_MASK_NMEA_SENTENCES, true);
    }
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_DATUM))
  {
    // Process Datum:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_DATUM;
    // If has data, get it and set the validity:
    if(dataBufferSize>=1)
    {
      // get the integer:
      pmtkDatum              = atoi(&pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START]);
      pmtkSetValidity(PMTK_VALIDITY_MASK_DATUM, true);
      pmtkLastCommandStatus  = PMTK_LAST_COMMAND_SUCCESS;
      return true;
    }
    pmtkSetValidity(PMTK_VALIDITY_MASK_DATUM, false);
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_DATUM_ADVANCED))
  {
    // Process Datum Advanced:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_DATUM_ADVANCED;
    // Already printed if debug - we take no state action
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_AIC))
  {
    // Process AIC:Datum:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_AIC;
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_SUCCESS;
    // We do not process this further:
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_USER_OPTION))
  {
    // Process AIC:Datum:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_USER_OPTION;
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_SUCCESS;
    // We do not process this further:
    return true;
  }
  // #define  PMTK_DATA_USER_OPTION

  else if(pmtkCompareResponse(PMTK_DATA_BT_MAC_ADDRESS))
  {
    // Process BT Mac Address:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_BT_MAC_ADDRESS;
    if(dataBufferSize != PMTK_BT_MAC_ADDRESS_LENGTH)
    {
      // Status is bad data:
      pmtkLastCommandStatus = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
      // Mark as invalid
      pmtkSetValidity(PMTK_VALIDITY_MASK_BT_MAC_ADDRESS, false);
      return true;
    }
    for(int i=0; i<PMTK_BT_MAC_ADDRESS_LENGTH; ++i)
    {
      pmtkBtMacAddress[i]    = pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START + i];
    }
    // Status is good:
    pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
    // Mark as valid:
    pmtkSetValidity(PMTK_VALIDITY_MASK_BT_MAC_ADDRESS, true);
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_FIRMWARE_INFO))
  {
    // Process Firmware Info :
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_FIRMWARE_INFO;
    if(dataBufferSize > 0)
    {
      // Fine got some data - we do not process further:
      pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
      return true;
    }
    else
    {
      // Status is bad data:
      pmtkLastCommandStatus = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
      return true;
    }
  }
  else if(pmtkCompareResponse(PMTK_DATA_EPO_STATUS))
  {
    // Process EPO Status:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_EPO_STATUS;
    // Default success:
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_SUCCESS;
    // We do not process further
    return true;
  }
  else if(pmtkCompareResponse(PMTK_SET_QUERY_ENABLE_EASY))
  {
    // Process Query Enable Easy Response:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_ENABLE_EASY;
    switch(pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START])
    {
      case PMTK_ENABLED_CHAR:
      {
        // Set enabled:
        pmtkEasyEnabled = true;
        // Set validity true:
        pmtkSetValidity(PMTK_VALIDITY_MASK_EASY_ENABLED, true);
        // Set success:
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
      }
      break;
      case PMTK_DISABLED_CHAR:
      {
        // Set disabled:
        pmtkEasyEnabled = false;
        // Set validity true:
        pmtkSetValidity(PMTK_VALIDITY_MASK_EASY_ENABLED, true);
        // Set success:
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
      }
      break;
      default:
      {
        // Set validity false:
        pmtkSetValidity(PMTK_VALIDITY_MASK_EASY_ENABLED, false);
        // Bad data
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
      }
      break;
    }
    return true;
  }
  else if(pmtkCompareResponse(PMTK_DATA_SBAS_ENABLED_IN_TEST))
  {
    // Process Query SBAS Enabled In Test Response:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_SBAS_IN_TEST;
    switch(pmtkInputBuffer[PMTK_INPUT_PACKET_DATA_START])
    {
      // NOTE THESE ARE BACKWARDS - RETURN IS 'isDisabled'
      case PMTK_DISABLED_CHAR:
      {
        // Set enabled:
        pmtkSbasEnabledInTest = true;
        // Set validity true:
        pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_IN_TEST_ENABLED, true);
        // Set success:
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
      }
      break;
      case PMTK_ENABLED_CHAR:
      {
        // Set disabled:
        pmtkSbasEnabledInTest = false;
        // Set validity true:
        pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_IN_TEST_ENABLED, true);
        // Set success:
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_SUCCESS;
      }
      break;
      default:
      {
        // Set validity false:
        pmtkSetValidity(PMTK_VALIDITY_MASK_SBAS_IN_TEST_ENABLED, false);
        // Bad data
        pmtkLastCommandStatus = PMTK_LAST_COMMAND_RETURNED_BAD_DATA;
      }
      break;
    }
    return true;
  }
  else if(pmtkCompareResponse(PMTK_LOX))
  {
    // Process LOX:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_RECEIVED_LOX;
    // We do not process this further...
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_SUCCESS;
    return true;
  }
  else
  {
    // We did not recognize:
    pmtkLastResponseReceived = PMTK_LAST_RESPONSE_DID_NOT_RECOGNIZE;
    // Status is Did recognized:
    pmtkLastCommandStatus    = PMTK_LAST_COMMAND_DID_NOT_RECOGNIZE;
    return true;
  }
  // We should never get here logically but anyway:
  pmtkLastResponseReceived = PMTK_LAST_RESPONSE_DID_NOT_RECOGNIZE;
  pmtkLastCommandStatus    = PMTK_LAST_COMMAND_DID_NOT_RECOGNIZE;
  return true;
}
bool PMTKHandler::pmtkCompareResponse(const char* command)
{
  if((pmtkInputBuffer[4]!=command[0]) || (pmtkInputBuffer[5]!=command[1]) || (pmtkInputBuffer[6]!=command[2]))
  {
    return false;
  }
  return true;
}
bool PMTKHandler::pmtkChecksumInputBuffer()
{
  unsigned char checkSum = 0;
  for(int i=0; i<pmtkInputBufferPosition-4; ++i)
  {
    checkSum ^= pmtkInputBuffer[i];
  }
  if(pmtkInputBuffer[pmtkInputBufferPosition-3] != pmtkGetAsciiDigit(checkSum>>4))
  {
    return false;
  }
  if(pmtkInputBuffer[pmtkInputBufferPosition-2] != pmtkGetAsciiDigit(checkSum&0x0f))
  {
    return false;
  }
  return true;
}
void PMTKHandler::pmtkResetNmeaSentences()
{
  pmtkNmeaSentences.pmtkResetNmeaSentences();
}
byte PMTKHandler::pmtkSetNmeaSentenceRate(byte nmeaSentence, byte rate)
{
  return pmtkNmeaSentences.pmtkSetNmeaSentenceRate(nmeaSentence, rate);
}
byte PMTKHandler::pmtkSetNmeaSentenceRateAll(byte rate)
{
  for(byte i=0; i<PMTK_NMEA_NUMBER_SENTENCES; ++i)
  {
    byte status = pmtkNmeaSentences.pmtkSetNmeaSentenceRate(i, rate);
    if(status != PMTK_ROUTINE_SUCCESS)
    {
      return status;
    }
  }
  return PMTK_ROUTINE_SUCCESS;
}
byte PMTKHandler::pmtkSendNmeaSentences()
{
  // Reset the buffer:
  pmtkResetSendBuffer();
  // Set these to no longer valid:
  pmtkSetValidity(PMTK_VALIDITY_MASK_NMEA_SENTENCES, false);
  // Put in the command:
  byte pmtkRoutineStatus = pmtkInsertCommand(PMTK_SET_NMEA_OUTPUT);
  // Error?
  if(pmtkRoutineStatus != PMTK_ROUTINE_SUCCESS)
  {
    return pmtkRoutineStatus;
  }
  // Check that it will fit:
  if(pmtkCurrentSendBufferPos + (PMTK_NMEA_SENTENCES_FIRST_BATCH_SIZE + PMTK_NMEA_SENTENCES_SECOND_BATCH_SIZE + PMTK_NMEA_SENTENCES_THIRD_BATCH_SIZE) * 2 +1 >= PMTK_SEND_BUFFER_LENGTH)
  {
    return PMTK_ROUTINE_FAIL_BUFFER_OVERRUN;
  }
  // Logic:
  // Build the NMEA sentence structure from the pmtkNmeaSentences structure:
  //  pmtkSendPacket(PMTK_SET_NMEA_OUTPUT, nmeaSentenceBuffer);
  // Do the first set:
  for(int i=0; i<PMTK_NMEA_SENTENCES_FIRST_BATCH_SIZE; ++i)
  {
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = ',';
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = '0' + pmtkNmeaSentences.pmtkNmeaSentences[i];
  }
  // Do the reserved 5:
  for(int i=0; i<PMTK_NMEA_SENTENCES_SECOND_BATCH_SIZE; ++i)
  {
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = ',';
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = '0';
  }
  // Do the final sets:
  for(int i=0; i<PMTK_NMEA_SENTENCES_THIRD_BATCH_SIZE; ++i)
  {
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = ',';
    pmtkSendBuffer[pmtkCurrentSendBufferPos++] = '0' + pmtkNmeaSentences.pmtkNmeaSentences[PMTK_NMEA_SENTENCES_FIRST_BATCH_SIZE + i];
  }
  pmtkSendBuffer[pmtkCurrentSendBufferPos] = '\0';
  pmtkRoutineStatus                        = pmtkChecksumPacket();
  // Error?
  if(pmtkRoutineStatus > 0)
  {
    return pmtkRoutineStatus;
  }
  return pmtkSendPacket();
}
byte PMTKHandler::pmtkInsertCommand(const char* command)
{
  // 1. Is the command the right length?
  if(strlen(command) != 3)
  {
    if(debugSerial != NULL)
    {
      (*debugSerial).println("-TS");
    }
    return PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR;
  }
  // 2. Does the command fit?
  if(pmtkCurrentSendBufferPos+4 >= PMTK_SEND_BUFFER_LENGTH)
  {
    return PMTK_ROUTINE_FAIL_BUFFER_OVERRUN;
  }
  // 3. Put in the command and update the buffer position:
  pmtkSendBuffer[pmtkCurrentSendBufferPos++] = command[0];
  pmtkSendBuffer[pmtkCurrentSendBufferPos++] = command[1];
  pmtkSendBuffer[pmtkCurrentSendBufferPos++] = command[2];
  pmtkSendBuffer[pmtkCurrentSendBufferPos]   = '\0';  // Terminate
  return PMTK_ROUTINE_SUCCESS;
}
bool PMTKHandler::pmtkCheckValidity(byte validityMask)
{
  return( (pmtkValidityFlags & (1u << validityMask)) >0 );
}
void PMTKHandler::pmtkSetValidity(byte validityMask, bool valid)
{
  if(valid)
  {
    // Turn on with or:
    pmtkValidityFlags |= (1u << validityMask);
  }
  else
  {
    // If on, turn off with xor:
    if( (pmtkValidityFlags & (1u << validityMask)) >0 )
    {
      pmtkValidityFlags ^= (1u << validityMask);
    }
  }
}
byte PMTKHandler::pmtkGetLastCommandStatus()
{
  return pmtkLastCommandStatus;
}
byte PMTKHandler::pmtkGetLastResponseReceived()
{
  return pmtkLastResponseReceived;
}
int PMTKHandler::pmtkGetLastResponseReceivedValue()
{
  return pmtkLastResponseReceivedValue;
}
char* PMTKHandler::pmtkGetLastResponseReceivedName()
{
  return pmtkLastResponseReceivedName;
}
int  PMTKHandler::pmtkGetLastAckCommandResponse()
{
  return pmtkLastAckCommandResponse;
}
int   PMTKHandler::pmtkGetFixRate()
{
  return pmtkFixRate;
}
byte  PMTKHandler::pmtkGetDgpsMode()
{
  return pmtkDgpsMode;
}
byte  PMTKHandler::pmtkGetDatum()
{
  return pmtkDatum;
}
bool  PMTKHandler::pmtkGetEasyEnabled()
{
  return pmtkEasyEnabled;
}
bool  PMTKHandler::pmtkGetSbasEnabled()
{
  return pmtkSbasEnabled;
}
bool  PMTKHandler::pmtkGetSbasEnabledInTest()
{
  return pmtkSbasEnabledInTest;
}
char* PMTKHandler::pmtkGetBtMacAddress()
{
  return pmtkBtMacAddress;
}
PMTKNmeaSentences* PMTKHandler::pmtkGetNmeaSentences()
{
  return &pmtkNmeaSentences;
}
int PMTKHandler::pmtkGetOutputRate()
{
  return pmtkOutputRate;
}
float PMTKHandler::pmtkGetNavThreshold()
{
  return pmtkNavThreshold;
}
bool PMTKHandler::pmtkGetPwrSaveMode()
{
  return pmtkPwrSaveMode;
}
bool PMTKHandler::pmtkGetGpsEnabled()
{
  return pmtkGpsEnabled;
}
bool PMTKHandler::pmtkGetGlonassEnabled()
{
  return pmtkGlonassEnabled;
}

void PMTKHandler::processPmtkCommandResponse(bool printState)
{
    // 1. Save the time
    // 2. Continually looping and reading from the gpsSerial and feeding into the handler
    // 3. Until either we get a true on the command response or we time out...
    unsigned long startTime=millis();
    while(millis() < startTime + PMTK_COMMAND_PROCESS_TIMEOUT)
    {
        if(gpsSerial->available())
        {
            int c = gpsSerial->read();
            if(pmtkDecode((byte)c))
            {
                printCommandResponseStatus(pmtkGetLastCommandStatus(), pmtkGetLastResponseReceived());
                if(printState)
                {
                    // Print the state:
                    printPmtkValidityState();
                }
                return;
            }
        }
    }
    // We timed out:
    if(debugSerial != NULL)
    {
      debugSerial->println("+TO");
    }
    return;
}

void PMTKHandler::printPmtkValidityState()
{
    if(debugSerial != NULL)
    {
      debugSerial->print("  ");
      debugSerial->print("NM ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_NMEA_SENTENCES)?"* ":"  ");
      for(int i=0; i<PMTK_NMEA_NUMBER_SENTENCES; ++i)
      {
        debugSerial->print((int)pmtkGetNmeaSentences()->pmtkNmeaSentences[i]);
      }
      debugSerial->println();
      debugSerial->print("  ");
      debugSerial->print("BT ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_BT_MAC_ADDRESS)?"* ":"  ");
      debugSerial->println(pmtkGetBtMacAddress());
      debugSerial->print("  ");
      debugSerial->print("EE ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_EASY_ENABLED)?"* ":"  ");
      debugSerial->println(pmtkGetEasyEnabled());
      debugSerial->print("  ");
      debugSerial->print("SB ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_SBAS_ENABLED)?"* ":"  ");
      debugSerial->println(pmtkGetSbasEnabled());
      debugSerial->print("  ");
      debugSerial->print("ST ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_SBAS_IN_TEST_ENABLED)?"* ":"  ");
      debugSerial->println(pmtkGetSbasEnabledInTest());
      debugSerial->print("  ");
      debugSerial->print("DT ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_DATUM)?"* ":"  ");
      debugSerial->println((int)pmtkGetDatum());
      debugSerial->print("  ");
      debugSerial->print("FR ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_FIX_RATE)?"* ":"  ");
      debugSerial->println(pmtkGetFixRate());
      debugSerial->print("  ");
      debugSerial->print("DG ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_DGPS_MODE)?"* ":"  ");
      debugSerial->println((int)pmtkGetDgpsMode());
      debugSerial->print("  ");
      debugSerial->print("OR ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_OUTPUT_RATE)?"* ":"  ");
      debugSerial->println((int)pmtkGetOutputRate());
      debugSerial->print("  ");
      debugSerial->print("NT ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_NAV_THRESHOLD)?"* ":"  ");
      debugSerial->println(pmtkGetNavThreshold());
      debugSerial->print("  ");
      debugSerial->print("PS ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_PWR_SAVE_MODE)?"* ":"  ");
      debugSerial->println(pmtkGetPwrSaveMode());
      debugSerial->print("  ");
      debugSerial->print("GP ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_GPS_ENABLED)?"* ":"  ");
      debugSerial->println(pmtkGetGpsEnabled());
      debugSerial->print("  ");
      debugSerial->print("GL ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_GLONASS_ENABLED)?"* ":"  ");
      debugSerial->println(pmtkGetGlonassEnabled());
      debugSerial->print("  ");
      debugSerial->print("LA ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_LAST_ACK_COMMAND_RESPONSE)?"* ":"  ");
      debugSerial->println(pmtkGetLastAckCommandResponse());
      debugSerial->print("  ");
      debugSerial->print("RR ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_LAST_RESPONSE_RECEIVED)?"* ":"  ");
      debugSerial->println(pmtkGetLastResponseReceived());
      debugSerial->print("  ");
      debugSerial->print("RV ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_LAST_RESPONSE_RECEIVED)?"* ":"  ");
      debugSerial->println(pmtkGetLastResponseReceivedValue());
      debugSerial->print("  ");
      debugSerial->print("RN ");
      debugSerial->print(pmtkCheckValidity(PMTK_VALIDITY_MASK_LAST_RESPONSE_RECEIVED)?"* ":"  ");
      debugSerial->println(pmtkGetLastResponseReceivedName());
    }
}
void PMTKHandler::printCommandResponseStatus(byte lastCommandStatus, byte lastResponseReceived)
{
  if(debugSerial!=NULL)
  {
    switch(lastCommandStatus)
    {
        case PMTK_LAST_COMMAND_NO_STATUS:
        {
            debugSerial->print("NS ");
        }
        break;
        case PMTK_LAST_COMMAND_SUCCESS:
        {
            debugSerial->print("OK ");
        }
        break;
        case PMTK_LAST_COMMAND_INVALID:
        {
            debugSerial->print("IN ");
        }
        break;
        case PMTK_LAST_COMMAND_UNSUPPORTED:
        {
            debugSerial->print("US ");
        }
        break;
        case PMTK_LAST_COMMAND_VALID_BUT_FAIL:
        {
            debugSerial->print("VF ");
        }
        break;
        case PMTK_LAST_COMMAND_RETURNED_BAD_DATA:
        {
            debugSerial->print("RB ");
        }
        break;
        case PMTK_LAST_COMMAND_DID_NOT_RECOGNIZE:
        {
            debugSerial->print("DR ");
        }
        break;
        default:
        {
            debugSerial->print("?? ");
        }
        break;
    }
    switch(lastResponseReceived)
    {
        case PMTK_LAST_RESPONSE_RECEIVED_NONE:
        {
            debugSerial->println("NN");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_ACK:
        {
            debugSerial->println("AK");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_SYS_MESSAGE:
        {
            debugSerial->println("SM");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_SYS_TEXT:
        {
            debugSerial->println("ST");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_FIX_RATE:
        {
            debugSerial->println("FC");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_DGPS_MODE:
        {
            debugSerial->println("DM");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_SBAS_ENABLED:
        {
            debugSerial->println("SE");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_NMEA_OUTPUT:
        {
            debugSerial->println("NM");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_DATUM:
        {
            debugSerial->println("DM");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_DATUM_ADVANCED:
        {
            debugSerial->println("DA");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_AIC:
        {
            debugSerial->println("AI");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_BT_MAC_ADDRESS:
        {
            debugSerial->println("BT");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_FIRMWARE_INFO:
        {
            debugSerial->println("FW");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_EPO_STATUS:
        {
            debugSerial->println("EP");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_ENABLE_EASY:
        {
            debugSerial->println("EZ");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_SBAS_IN_TEST:
        {
            debugSerial->println("ST");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_LOX:
        {
            debugSerial->println("LX");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_PWR_SAVE_MODE:
        {
            debugSerial->println("PW");
        }
        break;
        case PMTK_LAST_RESPONSE_RECEIVED_USER_OPTION:
        {
            debugSerial->println("UO");
        }
        break;
        case PMTK_LAST_RESPONSE_DID_NOT_RECOGNIZE:
        {
            debugSerial->println("DR");
        }
        break;
        default:
        {
            debugSerial->println("?1");
        }
        break;
    }
  }
}
void PMTKHandler::printPmtkRoutineResult(byte result)
{
  if(debugSerial!=NULL)
  {
    switch(result)
    {
        case PMTK_ROUTINE_SUCCESS:
        {
//            debugSerial->println("OK");  // DO NOTHING IN THIS CASE:
        }
        break;
        case PMTK_ROUTINE_FAIL_BUFFER_OVERRUN:
        {
            debugSerial->println("BF");
        }
        break;
        case PMTK_ROUTINE_FAIL_TEMP_BUFFER_OVERRUN:
        {
            debugSerial->println("TB");
        }
        break;
        case PMTK_ROUTINE_FAIL_LOGIC_ERROR:
        {
            debugSerial->println("LO");
        }
        break;
        case PMTK_ROUTINE_FAIL_NOT_IMPLEMENTED:
        {
            debugSerial->println("NI");
        }
        break;
        case PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR:
        {
            debugSerial->println("DR");
        }
        break;
        case PMTK_ROUTINE_FAIL_NO_SERIAL_PORT:
        {
            debugSerial->println("SE");
        }
        break;
        default:
        {
            debugSerial->println("?2");
        }
        break;
    }
  }
}
