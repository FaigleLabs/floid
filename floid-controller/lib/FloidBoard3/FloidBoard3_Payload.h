//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Payload.h:
#ifndef	__FLOIDBOARD3_PAYLOAD_H__
#define	__FLOIDBOARD3_PAYLOAD_H__
// NM logic:
extern boolean       nmDeployed[NUMBER_PAYLOADS];
extern unsigned long nmFireTimes[NUMBER_PAYLOADS];
extern boolean       nmFirstTime[NUMBER_PAYLOADS];
extern long          payloadGoalId[NUMBER_PAYLOADS];
#define              NM_CONTRACTED_PIN_LEVEL                        (128)
#define              BAY_RELEASED_PIN_STATE                         (HIGH)
// NanoMuscle:
boolean setupPayloads();
void    loopPayloads();
void    startPayload(int nmIndex, long goalId);
void    stopPayload(int nmIndex);
void    dropPayload(int bay, long goalId);
void    engageNM(int payloadIndex);
void    disengageNM(int payloadIndex);
#endif /* __FLOIDBOARD3_PAYLOAD_H__ */
