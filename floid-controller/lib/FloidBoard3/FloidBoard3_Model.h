//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Model.h:
#ifndef	__FLOIDBOARD3_MODEL_H__
#define	__FLOIDBOARD3_MODEL_H__

// Floid Models:
// -------------
#define FLOID_MODEL_THREE                                          (3)
#define FLOID_MODEL_FOUR                                           (4)
// Start Modes:
// ------------
#define MODEL_1_DEFAULT_START_MODE                                 (FLOID_MODE_TEST)
// Target Velocities to determine amount of target direction projection for heli servos:
// -------------------------------------------------------------------------------------
// Below this target velocity we keep still:
#define MODEL_1_TARGET_VELOCITY_KEEP_STILL                         (2.5)
// Above this target velocity we move full speed:
#define MODEL_1_TARGET_VELOCITY_FULL_SPEED                         (10)
// Attack angle:
// Below this distance from target in meters, we are at flat attack angle:
#define MODEL_1_ATTACK_ANGLE_MIN_DISTANCE                          (5)
// Above this distance from target in meters, we are at full attack angle:
#define MODEL_1_ATTACK_ANGLE_MAX_DISTANCE                          (10)
// Degrees:
#define MODEL_1_ATTACK_ANGLE_MAX_VALUE                             (-5.0)
// Below this distance in meters, we give a zero delta heading angle to target;
#define MODEL_1_ORIENTATION_MIN_DISTANCE_TO_TARGET_FOR_ORIENTATION (5)
// Roll/Pitch:
// Below this angle we are on target:
#define MODEL_1_PITCH_DELTA_MIN                                    (1.0)
#define MODEL_1_PITCH_DELTA_MAX                                    (5)
// Degrees / sec:
#define MODEL_1_PITCH_TARGET_VELOCITY_MAX_VALUE                    (2)
// Below this angle we are on target:
#define MODEL_1_ROLL_DELTA_MIN                                     (1.0)
#define MODEL_1_ROLL_DELTA_MAX                                     (5)
// Degrees / sec:
#define MODEL_1_ROLL_TARGET_VELOCITY_MAX_VALUE                     (2)
// Below this distance (meters) we are at right altitude:
#define MODEL_1_ALTITUDE_TO_TARGET_MIN                             (1)
#define MODEL_1_ALTITUDE_TO_TARGET_MAX                             (3)
// Meters/sec:
#define MODEL_1_ALTITUDE_TARGET_VELOCITY_MAX_VALUE                 (3)
// Below this distance we are at right heading:
#define MODEL_1_HEADING_DELTA_MIN                                  (2)
#define MODEL_1_HEADING_DELTA_MAX                                  (10)
// degrees/sec:
#define MODEL_1_HEADING_TARGET_VELOCITY_MAX_VALUE                  (3)
// Below this distance we are at right location so stop orienting the cyclic:
#define MODEL_1_DISTANCE_TO_TARGET_MIN                             (2)
#define MODEL_1_DISTANCE_TO_TARGET_MAX                             (20)
// Meters/sec:
// Note: This is the actual target velocity in m/s when the bird is MODEL_1_DISTANCE_TO_TARGET_MAX away from the target (10m/s = ~22.37 mph)
// Note: This is the value to control (floidModelDistanceTargetVelocityMaxValue) in real-time to adjust target velocity
#define MODEL_1_DISTANCE_TARGET_VELOCITY_MAX_VALUE                 (3)
// Lift-off / Land modes:
#define MODEL_1_LIFT_OFF_MODE_TARGET_ALTITUDE_DELTA                (5)
#define MODEL_1_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE            (5000)
// Pitch indices and their indexes:
// --------------------------------
#define FLOID_MODEL_NUMBER_PITCH_INDICES            (3)
#define FLOID_MODEL_LOW_PITCH_INDEX                 (0)
#define FLOID_MODEL_ZERO_PITCH_INDEX                (1)
#define FLOID_MODEL_HIGH_PITCH_INDEX                (2)
// =====================================
// MODEL 1: Five Blades - Exceed ESC's:
// =====================================
// Collective and cyclic Blade pitch values:
// -----------------------------------------
// Collective: These are the values per each heli: 16 degrees travel total, from 4 to 20
#define MODEL_1_COLLECTIVE_MIN_DEGREE_VALUE         (4)
#define MODEL_1_COLLECTIVE_MAX_DEGREE_VALUE         (20)
// In range of [0-1] e.g. COLLECTIVE_MIN_DEGREE to COLLECTIVE_MAX_DEGREE_VALUE, so at 0.0 we will start with a collective of 0 for each heli
#define MODEL_1_COLLECTIVE_DEFAULT_VALUE            (0.0)
// Cyclic: 2 x range: This allows the cyclic ranges to go from -4 to 4  // NOTE: COLLECTIVE_MIN/MAX -/+ CYCLIC_RANGE MUST BE IN RANGE OF BLADE PITCH LOW AND HIGH
#define MODEL_1_CYCLIC_RANGE_VALUE                  (4)
// Default value: [-1,1] gives -CYCLIC_RANGE to CYCLIC_RANGE, so 0.0 is ZERO
#define MODEL_1_CYCLIC_DEFAULT_VALUE                (0.0)

// The alpha value for applying heading velocity deltas to the cyclic for the normal rotation mode
#define MODEL_1_CYCLIC_HEADING_ALPHA_DEFAULT_VALUE  (0.3)
// Rotation modes:  Counter: 1 & 3 rotate opposite of 0 & 2  All same: all rotate same direction
#define FLOID_ROTATION_MODE_COUNTER                 (0)
#define FLOID_ROTATION_MODE_ALL_SAME                (1)
#define MODEL_1_ROTATION_MODE_DEFAULT_VALUE  (FLOID_ROTATION_MODE_COUNTER)
// Heli Servos:
// ------------
//   Servo degrees and equivalent pulse widths:
//   These values translate servo degrees to servo values:
#define MODEL_1_HELI_SERVO_MIN_DEGREE_VALUE         (0)
#define MODEL_1_HELI_SERVO_MAX_DEGREE_VALUE         (90)
#define MODEL_1_HELI_SERVO_MIN_PULSE_WIDTH_VALUE    (1000)
#define MODEL_1_HELI_SERVO_MAX_PULSE_WIDTH_VALUE    (2000)
//   Blade pitch Index values for the mapping below:
#define MODEL_1_BLADE_PITCH_LOW_VALUE               (-6)
#define MODEL_1_BLADE_PITCH_ZERO_VALUE              (8)
#define MODEL_1_BLADE_PITCH_HIGH_VALUE              (24)
//   Pitch values to servo degrees for each heli and servo and Low/Zero/High value (These form low and high linear interpolation ranges - in future could use five points and fit with Bezier but we are too tight on memory for now)
#define MODEL_1_H0S0_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H1S0_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H2S0_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H3S0_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H0S1_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H1S1_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H2S1_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H3S1_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H0S2_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H1S2_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H2S2_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H3S2_PITCH_LOW_VALUE                (5.0)
#define MODEL_1_H0S0_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H1S0_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H2S0_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H3S0_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H0S1_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H1S1_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H2S1_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H3S1_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H0S2_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H1S2_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H2S2_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H3S2_PITCH_ZERO_VALUE               (28.0)
#define MODEL_1_H0S0_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H1S0_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H2S0_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H3S0_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H0S1_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H1S1_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H2S1_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H3S1_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H0S2_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H1S2_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H2S2_PITCH_HIGH_VALUE               (55.0)
#define MODEL_1_H3S2_PITCH_HIGH_VALUE               (55.0)
//  Servo locations and direction of servo travel to head travel:
// Note: angles in heading/compass units:
#define MODEL_1_HELI_SERVO_LEFT_OFFSET              (60)
#define MODEL_1_HELI_SERVO_RIGHT_OFFSET             (-60)
#define MODEL_1_HELI_SERVO_PITCH_OFFSET             (180)
// Servo signs:
#define MODEL_1_HELI_SERVO_LEFT_SIGN                (1)
#define MODEL_1_HELI_SERVO_RIGHT_SIGN               (0)
#define MODEL_1_HELI_SERVO_PITCH_SIGN               (1)
// ESCs: 
// -----
#define ESC_TYPE_EXCEED_RC                          (0)
#define ESC_TYPE_DYNAM                              (1)
#define MODEL_1_ESC_TYPE                            ESC_TYPE_EXCEED_RC
#define MODEL_1_ESC_SERVO_DEFAULT_LOW_VALUE         (0.00)
#define MODEL_1_ESC_SERVO_DEFAULT_HIGH_VALUE        (1.00)
#define MODEL_1_ESC_SERVO_MIN_PULSE_WIDTH_VALUE     (1000)
#define MODEL_1_ESC_SERVO_MAX_PULSE_WIDTH_VALUE     (2000)
// Target Velocity Alphas:
// -----------------------
#define MODEL_1_TARGET_PITCH_VELOCITY_ALPHA         (0.1)
#define MODEL_1_TARGET_ROLL_VELOCITY_ALPHA          (0.1)
#define MODEL_1_TARGET_HEADING_VELOCITY_ALPHA       (0.1)
#define MODEL_1_TARGET_ALTITUDE_VELOCITY_ALPHA      (0.1)
// Heli Startup Modes:
// -------------------
#define MODEL_1_HELI_STARTUP_NUMBER_STEPS           (20)
#define MODEL_1_HELI_STARTUP_STEP_TICK              (250)
// ESC Model:
// -------------------
#define MODEL_1_ESC_COLLECTIVE_CALC_MIDPOINT       (0.5)
#define MODEL_1_ESC_COLLECTIVE_LOW_VALUE           (0.0)
#define MODEL_1_ESC_COLLECTIVE_MID_VALUE           (0.2)
#define MODEL_1_ESC_COLLECTIVE_HIGH_VALUE          (1.0)
// Velocity delta cyclic alpha:
#define MODEL_1_VELOCITY_DELTA_CYCLIC_ALPHA        (0.1)
// Acceleration multiplier:
#define MODEL_1_ACCELERATION_MULTIPLIER            (10.0)

// MODEL PARAMETER INPUT PACKETS:
// =============================
// Packet 1:
#define MODEL_PARAMETERS_1_PACKET_SIZE                             (0x71)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_LOW                (0x11)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_ZERO               (0x15)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_HIGH               (0x19)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_LOW                  (0x1D)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_LOW                  (0x21)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_LOW                  (0x25)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_LOW                  (0x29)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_LOW                  (0x2D)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_LOW                  (0x31)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_ZERO                 (0x35)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_ZERO                 (0x39)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_ZERO                 (0x3D)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_ZERO                 (0x41)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_ZERO                 (0x45)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_ZERO                 (0x49)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_HIGH                 (0x4D)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_HIGH                 (0x51)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_HIGH                 (0x55)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_HIGH                 (0x59)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_HIGH                 (0x5D)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_HIGH                 (0x61)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_LEFT  (0x65)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_RIGHT (0x69)
#define MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_PITCH (0x6D)
// Packet 2:
#define MODEL_PARAMETERS_2_PACKET_SIZE                             (0x75)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_LOW                  (0x11)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_LOW                  (0x15)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_LOW                  (0x19)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_LOW                  (0x1D)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_LOW                  (0x21)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_LOW                  (0x25)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_ZERO                 (0x29)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_ZERO                 (0x2D)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_ZERO                 (0x31)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_ZERO                 (0x35)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_ZERO                 (0x39)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_ZERO                 (0x3D)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_HIGH                 (0x41)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_HIGH                 (0x45)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_HIGH                 (0x49)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_HIGH                 (0x4D)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_HIGH                 (0x51)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_HIGH                 (0x55)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_CALC_MIDPOINT     (0x59)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_LOW_VALUE         (0x5D)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_MID_VALUE         (0x61)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_HIGH_VALUE        (0x65)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_LEFT  (0x69)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_RIGHT (0x6D)
#define MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_PITCH (0x71)
// Packet 3:
#define MODEL_PARAMETERS_3_PACKET_SIZE                                        (0x74)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MIN                       (0x11)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MAX                       (0x15)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_DEFAULT                   (0x19)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_RANGE                         (0x1D)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_DEFAULT                       (0x21)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_TYPE                             (0x25)  // Short
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MIN                        (0x27)  // Short
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MAX                        (0x29)  // Short
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_LOW_VALUE                        (0x2B)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_HIGH_VALUE                       (0x2F)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MIN_DISTANCE_VALUE      (0x33)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MAX_DISTANCE_VALUE      (0x37)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_VALUE                   (0x3B)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MIN_VALUE                (0x3F)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MAX_VALUE                (0x43)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_TARGET_VELOCITY_MAX_VALUE      (0x47)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MIN_VALUE                 (0x4B)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MAX_VALUE                 (0x4F)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_TARGET_VELOCITY_MAX_VALUE       (0x53)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MIN_VALUE         (0x57)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MAX_VALUE         (0x5B)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TARGET_VELOCITY_MAX_VALUE   (0x5F)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE (0x63)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_START_MODE                           (0x67) // Byte
#define MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_LEFT             (0x68)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_RIGHT            (0x6C)
#define MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_PITCH            (0x70)
// Packet 4:
#define MODEL_PARAMETERS_4_PACKET_SIZE                                        (0x79)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MIN_VALUE              (0x11)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MAX_VALUE              (0x15)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_TARGET_VELOCITY_MAX_VALUE    (0x19)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MIN_VALUE         (0x1D)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MAX_VALUE         (0x21)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TARGET_VELOCITY_MAX_VALUE   (0x25)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_ORIENTATION_MIN_DISTANCE_VALUE       (0x2D)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE_DELTA_VALUE (0x31)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MIN                      (0x35)  // Short
#define MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MAX                      (0x37)  // Short
#define MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MIN                     (0x39)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MAX                     (0x3D)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_LEFT                      (0x41)  // Byte
#define MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_RIGHT                     (0x42)  // Byte
#define MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_PITCH                     (0x43)  // Byte
#define MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_LEFT             (0x44)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_RIGHT            (0x48)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_PITCH            (0x4C)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_KEEP_STILL           (0x50)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_FULL_SPEED           (0x54)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_PITCH_VELOCITY_ALPHA          (0x58)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ROLL_VELOCITY_ALPHA           (0x5C)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_HEADING_VELOCITY_ALPHA        (0x60)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_ALPHA       (0x64)
#define MODEL_PARAMETERS_4_PACKET_OFFSET_ROTATION_MODE                        (0x68) // Byte
#define MODEL_PARAMETERS_4_PACKET_OFFSET_CYCLIC_HEADING_ALPHA                 (0x69) // Float
#define MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_MODE_NUMBER_STEPS       (0x6D) // Short
#define MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_MODE_STEP_TICK          (0x6F) // Short
#define MODEL_PARAMETERS_4_PACKET_OFFSET_VELOCITY_DELTA_CYCLIC_ALPHA          (0x71) // Float
#define MODEL_PARAMETERS_4_PACKET_OFFSET_ACCELERATION_MULTIPLIER              (0x75) // Float

// Model parameters object:
struct FloidModelParameters
{
 public:
  PacketHeader packetHeader;
  // Members:
  // ========
  // Collective / Cyclic values and defaults:
  // ----------------------------------------
  float floidModelCollectiveMinValue;
  float floidModelCollectiveMaxValue;
  float floidModelCollectiveDefaultValue;
  float floidModelCyclicRangeValue;
  float floidModelCyclicDefaultValue;
  // Heli-Servos:
  // ------------
  //   Servo degrees and equivalent pulse widths:
  float floidModelHeliServoMinDegreeValue;
  float floidModelHeliServoMaxDegreeValue;
  int   floidModelHeliServoMinPulseWidthValue;
  int   floidModelHeliServoMaxPulseWidthValue;
  float floidModelHeliServoOffsets[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];
  // Both of these are unused: (Correct ones are: floidModelDistanceToTargetMin and floidModelDistanceToTargetMax)
  float floidModelTargetVelocityKeepStill;
  float floidModelTargetVelocityFullSpeed;
  //   Blade pitch Index values for the mapping below:
  float floidModelBladePitchIndices[FLOID_MODEL_NUMBER_PITCH_INDICES];
  //   Pitch values to servo degrees for each heli and servo and LOW/ZERO/HIGH value (These form low and high linear interpolation ranges - in future could use five points and fit with Bezier but we are too tight on memory for now)
  float floidModelBladePitchServoDegrees[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS][FLOID_MODEL_NUMBER_PITCH_INDICES];
  //   Direction of servo travel to head travel:
  byte  floidModelServoSigns[NUMBER_HELI_SERVOS];
  // ESCs: 
  // -----
  int   floidModelESCType;
  float floidModelESCServoDefaultLowValue;
  float floidModelESCServoDefaultHighValue;
  int   floidModelESCServoMinPulseWidthValue;
  int   floidModelESCServoMaxPulseWidthValue;
  // Target Velocity Alphas:
  // -----------------------
  float floidModelTargetPitchVelocityAlpha;
  float floidModelTargetRollVelocityAlpha;
  float floidModelTargetHeadingVelocityAlpha;
  float floidModelTargetAltitudeVelocityAlpha;
  // Model Physics:
  // --------------
  float floidModelAttackAngleMinDistance;
  float floidModelAttackAngleMaxDistance;
  float floidModelAttackAngleMaxValue;
  float floidModelOrientationMinDistanceToTargetForOrientation;
  float floidModelPitchDeltaMin;
  float floidModelPitchDeltaMax;
  float floidModelPitchTargetVelocityMaxValue;
  float floidModelRollDeltaMin;
  float floidModelRollDeltaMax;
  float floidModelRollTargetVelocityMaxValue;
  float floidModelAltitudeToTargetMin;
  float floidModelAltitudeToTargetMax;
  float floidModelAltitudeTargetVelocityMaxValue;
  float floidModelHeadingDeltaMin;
  float floidModelHeadingDeltaMax;
  float floidModelHeadingTargetVelocityMaxValue;
  float floidModelDistanceToTargetMin;
  float floidModelDistanceToTargetMax;
  float floidModelDistanceTargetVelocityMaxValue;
  float floidModelLiftOffModeTargetAltitudeDelta;
  unsigned long floidModelLandModeRequiredTimeAtMinAltitude;
  // Start Mode:
  // -----------
  byte  floidModelStartMode;
  // Helicopter Normal/Counter Rotation mode:
  // ----------------------------------------
  byte  floidModelRotationMode;
  // Cyclic Heading Alpha value:
  float floidModelCyclicHeadingAlpha;
  // Heli Startup:
  int  heliStartupModeNumberSteps;
  int  heliStartupModeStepTick;

  float floidModelEscCollectiveCalcMidpoint;
  float floidModelEscCollectiveLowValue;
  float floidModelEscCollectiveMidValue;
  float floidModelEscCollectiveHighValue;
  //  The scaling factor for cyclic alpha values:
  float velocityDeltaCyclicAlphaScale;
  // Acceleration multiplier scale:
  float accelerationMultiplierScale;
};
// Model status:
struct FloidModelStatus
{
 // FloidModel status items:
 // ------------------------
 public:
  PacketHeader packetHeader;
  float floidModelCollectiveDeltaOrientation[NUMBER_HELICOPTERS];
  float floidModelCollectiveDeltaAltitude[NUMBER_HELICOPTERS];
  float floidModelCollectiveDeltaHeading[NUMBER_HELICOPTERS];
  float floidModelCollectiveDeltaTotal[NUMBER_HELICOPTERS];
  float floidModelCyclicValue[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];
  float floidModelCollectiveValue[NUMBER_HELICOPTERS];
  // Calculated pitch, servo and pulse values:
  float floidModelCalculatedCyclicBladePitch[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];    // This value is the actual calculated cyclic blade pitch setting
  float floidModelCalculatedCollectiveBladePitch[NUMBER_HELICOPTERS];                    // This value is the actual calculated collective blade pitch setting
  float floidModelCalculatedBladePitch[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];          // This value is the actual calculated blade pitch setting (e.g. collective for he heli + cyclic for the servo)
  float floidModelCalculatedServoDegrees[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];        // This value is the actual calculated servo degree setting
  int   floidModelCalculatedServoPulse[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];          // This value is the actual calculated servo pulse width
  float floidModelCyclicHeadingValue;
  float floidModelVelocityCyclicAlpha;                                                   // The amount of the direction projection that we are going to use for directing the servos
};
extern FloidModelParameters floidModelParameters;
extern FloidModelStatus     floidModelStatus;
// Methods:
// ========
boolean setupFloidModel();
boolean setupFloidModelStatus();
void    printModelStatusOffsets();
boolean sendModelStatus();
boolean sendModelParameters();
void    printModelParametersOffsets();
// Heli blade pitch and servo degree and pulse mapping functions:
// --------------------------------------------------------------
void    floidModelCalculateHeliServosFromCollectivesAndCyclics(int heliIndex, boolean writeToServo);
float   floidModelCalculateCyclicBladePitch(float cyclicValue);
float   floidModelCalculateCollectiveBladePitch(float collectiveValue);
float   floidModelBladePitchToServoDegrees(int heliIndex, int servoIndex, float bladePitch);
float   floidModelServoDegreeToServoPulse(int heliIndex, int servoIndex, float servoDegree);
void    floidModelSetHeliServoFromValue(int heliIndex, int servoIndex, float heliServoValue, boolean writeToServo);
void    floidModelSetHeliServoFromBladePitch(int heliIndex, int servoIndex, float bladePitch, boolean writeToServo);
void    floidModelSetHeliServoFromDegreeValue(int heliIndex, int servoIndex, float heliServoDegreeValue, boolean writeToServo);
float   floidModelHeliValueFromServoPulse(int heliIndex, int servoIndex);
// ESC mapping function:
// --------------------------------------------------------------
void    floidModelSetEscFromCollective(int heliIndex);
// Standard clamp/linear interpolation function:
// ---------------------------------------
float   clampFloat(float alpha, float t0, float t1);
float   floidModelLinearlyInterpolate(float alpha, float t0, float t1, float v0, float v1, bool clamp);
// ESC Servo Value Mappings:
// -------------------------
int     escValueToESCServoPulse(float escValue);
#endif /* __FLOIDBOARD3_MODEL_H__ */
