//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Packet.h
// faiglelabs.com
#ifndef	__FLOIDBOARD3_PACKET_H__
#define	__FLOIDBOARD3_PACKET_H__
#include <Arduino.h>
struct PacketHeader
{
  public:
    byte          flag[6];
    unsigned int  length;
    byte          checksum[4];
    unsigned long number;
    byte          type;
};
void printPacketHeaderOffsets();
extern unsigned long packetNumber;

#endif /* __FLOIDBOARD3_PACKET_H__ */
