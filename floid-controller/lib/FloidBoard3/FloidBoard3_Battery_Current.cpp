//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Battery_Current.cpp:
#include <FloidBoard3.h>
// Main, Heli and Aux: Batteries and Current (Main x 1, Heli x 4, AUX x1)
const float   arduinoPinValueToFiveVolts                             = 5.0 / 1023.0;            // 0=0V    1023=5V
const float   mainBatteryTopResistor                                 = 27000.0;                 // Resistor calculations for scaling pin voltage here
const float   mainBatteryBottomResistor                              = 56000.0;
const float   mainBatteryPinVoltageToBatteryVoltage                  = (mainBatteryTopResistor + mainBatteryBottomResistor)/mainBatteryBottomResistor;
const int     mainBatteryNumberPinSamples                            = 8;                       // How many samples we will take and average
const int     heliBatteryNumberPinSamples                            = 8;                       // How many samples we will take and average
const float   heliBatteryTopResistor                                 = 27000.0;                 // Resistor calculations for scaling pin voltage here
const float   heliBatteryBottomResistor                              = 56000.0;
const float   heliBatteryPinVoltageToBatteryVoltage                  = (heliBatteryTopResistor + heliBatteryBottomResistor)/heliBatteryBottomResistor;
// Current info:
const float   currentTopResistor                                     = 56000.0;                 // Resistor calculations for scaling pin voltage here
const float   currentBottomResistor                                  = 27000.0;
const float   currentPinVoltageToCurrent                             = (currentTopResistor + currentBottomResistor)/currentBottomResistor;
const int     currentNumberPinSamples                                = 8;                       // How many samples we will take and average
// Aux battery and aux current info:
const int     auxBatteryNumberPinSamples                            = 8;                        // How many samples we will take and average
const float   auxBatteryPinVoltageToBatteryVoltage                  = 1.0;                      // TODO [AUX] This is the scale of the voltage coming from the aux battery - calculate this!!!
const float   auxPinVoltageToCurrent                                = 1.0;                      // TODO [AUX] What is the scale of the voltage to current?
const int     auxNumberPinSamples                                   = 8;                        // How many samples we will take and average
// Batteries:
boolean setupBatteries()
{
  setupMainBattery();
  setupHeliBatteries();
  setupAuxBattery();
  return true;
}
void loopBatteries()
{
  loopMainBattery();
  loopHeliBatteries();
  loopAuxBattery();
}
void setupMainBattery()
{
  pinMode(mainBatteryPin, INPUT);
  digitalWrite(mainBatteryPin, LOW);    // Turn off pull-up resistor
}
void setupAuxBattery()
{
  pinMode(auxBatteryPin, INPUT);
  digitalWrite(auxBatteryPin, LOW);    // Turn off pull-up resistor
}
void setupHeliBatteries()
{
  for(int h=0; h<NUMBER_HELICOPTERS; ++h)
  {
    pinMode(batteryPins[h], INPUT);
    digitalWrite(batteryPins[h], LOW);    // Turn off pull-up resistor
  }
}
void loopMainBattery()
{
  int mainBatteryAccumulatedPinVoltage = 0;
  // Clear voltage, accumulate, calculate:
  for(int j=0; j<mainBatteryNumberPinSamples; ++j)
  {
    mainBatteryAccumulatedPinVoltage += analogRead(mainBatteryPin);
  }
  floidStatus.mainBatteryVoltage = (((float)mainBatteryAccumulatedPinVoltage) * arduinoPinValueToFiveVolts * mainBatteryPinVoltageToBatteryVoltage) / mainBatteryNumberPinSamples;
}
void loopHeliBatteries()
{
  // Clear voltages, accumulate, calculate:
  for(int h=0; h<NUMBER_HELICOPTERS; ++h)
  {
    int heliBatteryAccumulatedPinVoltage = 0;
    for(int j=0; j<heliBatteryNumberPinSamples; ++j)
    {
      heliBatteryAccumulatedPinVoltage += analogRead(batteryPins[h]);
    }
    floidStatus.heliBatteryVoltages[h] = (((float)heliBatteryAccumulatedPinVoltage)  * arduinoPinValueToFiveVolts * heliBatteryPinVoltageToBatteryVoltage) / heliBatteryNumberPinSamples;
  }
}
void loopAuxBattery()
{
  int auxBatteryAccumulatedPinVoltage = 0;
  // Clear voltage, accumulate, calculate:
  for(int j=0; j<auxBatteryNumberPinSamples; ++j)
  {
    auxBatteryAccumulatedPinVoltage += analogRead(auxBatteryPin);
  }
  floidStatus.auxBatteryVoltage = (((float)auxBatteryAccumulatedPinVoltage) * arduinoPinValueToFiveVolts * auxBatteryPinVoltageToBatteryVoltage) / auxBatteryNumberPinSamples;
}
// Current:
boolean setupCurrents()
{
  setupHeliCurrents();
  setupAuxCurrents();
  return true;
}
void setupHeliCurrents()
{
  for(int h=0; h<NUMBER_HELICOPTERS; ++h)
  {
     pinMode(currentPins[h], INPUT);
     digitalWrite(currentPins[h], LOW);    // Turn off pull-up resistor
  }
}
void setupAuxCurrents()
{
  pinMode(auxCurrentPin, INPUT);
  digitalWrite(auxCurrentPin, LOW);    // Turn off pull-up resistor
}
void loopCurrents()
{
  loopHeliCurrents();
  loopAuxCurrent();
}
void loopHeliCurrents()
{
  // Clear voltages, accumulate, calculate:
  for(int h=0; h<NUMBER_HELICOPTERS; ++h)
  {
    int currentAccumulatedPinVoltage = 0;
    for(int j=0; j<currentNumberPinSamples; ++j)
    {
      currentAccumulatedPinVoltage += analogRead(currentPins[h]);
    }  
    floidStatus.currents[h] = (((float)currentAccumulatedPinVoltage) * arduinoPinValueToFiveVolts * currentPinVoltageToCurrent) / currentNumberPinSamples;
  }
}
void loopAuxCurrent()
{
  int auxAccumulatedPinVoltage = 0;
  for(int j=0; j<auxNumberPinSamples; ++j)
  {
    auxAccumulatedPinVoltage += analogRead(auxCurrentPin);
  }
  floidStatus.auxCurrent = (((float)auxAccumulatedPinVoltage) * arduinoPinValueToFiveVolts * auxPinVoltageToCurrent) / auxNumberPinSamples;
}
