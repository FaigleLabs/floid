//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Battery_Current.h:
#ifndef	__FLOIDBOARD3_BATTERY_CURRENT_H__
#define	__FLOIDBOARD3_BATTERY_CURRENT_H__

// Main, Heli and Aux: Batteries and Current (Main x 1, Heli x 4, AUX x1)
extern const float   arduinoPinValueToFiveVolts;
extern const float   mainBatteryTopResistor;
extern const float   mainBatteryBottomResistor;
extern const float   mainBatteryPinVoltageToBatteryVoltage;
extern const int     mainBatteryNumberPinSamples;
extern const int     heliBatteryNumberPinSamples;
extern const float   heliBatteryTopResistor;
extern const float   heliBatteryBottomResistor;
extern const float   heliBatteryPinVoltageToBatteryVoltage;
// Current info:
extern const float   currentTopResistor;
extern const float   currentBottomResistor;
extern const float   currentPinVoltageToCurrent;
extern const int     currentNumberPinSamples;
// Aux battery and aux current info:
extern const int     auxBatteryNumberPinSamples;
extern const float   auxBatteryPinVoltageToBatteryVoltage;
extern const float   auxPinVoltageToCurrent;
extern const int     auxNumberPinSamples;
// Batteries:
boolean setupBatteries();
void    loopBatteries();
void    setupMainBattery();
void    setupAuxBattery();
void    setupHeliBatteries();
void    loopMainBattery();
void    loopHeliBatteries();
void    loopAuxBattery();
// Current:
boolean setupCurrents();
void    setupHeliCurrents();
void    setupAuxCurrents();
void    loopCurrents();
void    loopHeliCurrents();
void    loopAuxCurrent();
#endif /* __FLOIDBOARD3_BATTERY_CURRENT_H__ */
