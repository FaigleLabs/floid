//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_EEPROM.cpp:
#include <FloidBoard3.h>
// Model Parameters EEPROM:
// ========================
boolean clearEEPROM()
{
  // Overwrite the token so model parameters cannot be loaded
  for(int i=0; i<FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_SIZE; ++i)
  {
    eepromWrite(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + i, FLOID_EEPROM_CLEAR_TOKEN);
  }
  return true;
}
boolean loadFromEEPROM()
{
  if(!checkEEPROMModelParameterToken())
  {
    return false;
  }
  if(!checkEEPROMModelParametersChecksum())
  {
    return false;
  }
  readEEPROMModelParameters();
  return true;
}
boolean saveToEEPROM()
{
  writeEEPROMModelParametersToken();
  writeEEPROMModelParameters();
  writeEEPROMModelParametersChecksum();
  return true;
}
void writeEEPROMModelParametersToken()
{
  eepromWrite(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 0, FLOID_EEPROM_MODEL_PARAMETERS_HEADER_0);
  eepromWrite(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 1, FLOID_EEPROM_MODEL_PARAMETERS_HEADER_1);
  eepromWrite(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 2, FLOID_EEPROM_MODEL_PARAMETERS_HEADER_2);
  eepromWrite(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 3, FLOID_EEPROM_MODEL_PARAMETERS_HEADER_3);
  eepromWrite(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 4, FLOID_EEPROM_MODEL_PARAMETERS_HEADER_4);
}
void writeEEPROMModelParameters()
{
  byte *modelParametersByteBufferPtr = (byte *)&floidModelParameters;
  for(unsigned int i=0; i< sizeof(FloidModelParameters); ++i)
  {
    eepromWrite(FLOID_EEPROM_MODEL_PARAMETERS_OFFSET + i, modelParametersByteBufferPtr[i]);
  }
}
void writeEEPROMModelParametersChecksum()
{
  int   checksum           = calculateModelParametersChecksum();
  byte *checksumBytePtr    = (byte *)&checksum;
  eepromWrite(FLOID_EEPROM_MODEL_PARAMETERS_CHECKSUM_OFFSET + 0, checksumBytePtr[0]);
  eepromWrite(FLOID_EEPROM_MODEL_PARAMETERS_CHECKSUM_OFFSET + 1, checksumBytePtr[1]);
}
int readEEPROMModelParametersChecksum()
{
  int checksum           = 0;
  byte *checksumBytePtr  = (byte *)&checksum;
  checksumBytePtr[0]     = eepromRead(FLOID_EEPROM_MODEL_PARAMETERS_CHECKSUM_OFFSET + 0);
  checksumBytePtr[1]     = eepromRead(FLOID_EEPROM_MODEL_PARAMETERS_CHECKSUM_OFFSET + 1);
  return checksum;
}
boolean checkEEPROMModelParameterToken()
{
  if(eepromRead(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 0) != FLOID_EEPROM_MODEL_PARAMETERS_HEADER_0) return false;
  if(eepromRead(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 1) != FLOID_EEPROM_MODEL_PARAMETERS_HEADER_1) return false;
  if(eepromRead(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 2) != FLOID_EEPROM_MODEL_PARAMETERS_HEADER_2) return false;
  if(eepromRead(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 3) != FLOID_EEPROM_MODEL_PARAMETERS_HEADER_3) return false;
  if(eepromRead(FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET + 4) != FLOID_EEPROM_MODEL_PARAMETERS_HEADER_4) return false;
  return true;
}
boolean checkEEPROMModelParametersChecksum()
{
  int  calculatedChecksum            = 0;
  byte *calculatedChecksumBytePtr    = (byte *)&calculatedChecksum;
  for(unsigned int i=0; i< sizeof(FloidModelParameters); ++i)
  {
    calculatedChecksumBytePtr[i%2] ^= eepromRead(FLOID_EEPROM_MODEL_PARAMETERS_OFFSET + i);
  }
  if(calculatedChecksum == readEEPROMModelParametersChecksum())
  {
    return true;
  }
  return false;  // Do not match
}
void readEEPROMModelParameters()
{
  byte *modelParametersByteBufferPtr = (byte *)&floidModelParameters;
  for(unsigned int i=0; i< sizeof(FloidModelParameters); ++i)
  {
    modelParametersByteBufferPtr[i] = eepromRead(FLOID_EEPROM_MODEL_PARAMETERS_OFFSET + i);
  }
}
int calculateModelParametersChecksum()
{
  int checksum                       = 0;
  byte *checksumBytePtr              = (byte *)&checksum;
  byte *modelParametersByteBufferPtr = (byte *)&floidModelParameters;
  for(unsigned int i=0; i< sizeof(FloidModelParameters); ++i)
  {
    checksumBytePtr[i%2] ^= modelParametersByteBufferPtr[i];
  }
  return checksum;
}
