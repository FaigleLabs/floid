//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_PYR.cpp
#include <FloidBoard3.h>
boolean         newPYR                                          = false;
unsigned long   pyrFireTime                                     = 0ul;
byte            pyrDecodeBuffer[PYR_DECODE_BUFFER_SIZE+1];
unsigned int    pyrAnalogBufferPos                              = 0;
char            pyrInputBuffer[PYR_INPUT_BUFFER_SIZE];
float           pyr[PYR_NUMBER_FLOATS];                                      // Conversion buffer for output floats + checksum float;
PYRBufferEntry  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH];
unsigned long   pyrTimeDelta                                    = 0ul;
uint32_t        lastGoodPacketNumber                            = -1l;       // No packet yet
void clearPYR()
{
  pyrTimeDelta                      = 0ul;
  floidStatus.pitchVelocity         = 0;
  floidStatus.rollVelocity          = 0;
  floidStatus.heightVelocity        = 0;
  floidStatus.headingVelocity       = 0;
  // Clear the pyr buffers:
  clearPYRBuffers();
  newPYR                            = false;
  lastGoodPacketNumber              = -1l;       // No packet yet
  floidStatus.pyrGoodCount          = 0ul;
  floidStatus.pyrErrorCount         = 0ul;
}
// PYR:
boolean setupPYR()
{
  // Setup PYR:
  pinMode(pyrOnPin,      OUTPUT);
  digitalWrite(pyrOnPin, LOW);
  delay(100);
  PYR_SERIAL.begin(PYR_BAUD_RATE);
  delay(100);
  floidStatus.pyrOn                = false;
  floidStatus.pyrMode              = PYR_DIGITAL;
  floidStatus.pyrAnalogRate        = PYR_ANALOG_DEFAULT_RATE;
  clearPYR();
  return true;
}
void startPYR()
{
  if(floidStatus.debug && floidStatus.debugPYR)
  {
    debugPrintlnToken(FLOID_DEBUG_PYR_START);
  }
  // Power on the PYR:
  clearPYR();
  pinMode(pyrOnPin, OUTPUT);
  digitalWrite(pyrOnPin, HIGH);
  pyrFireTime                 = millis();
  floidStatus.pyrMode         = PYR_DIGITAL;      // Default start mode for our mongoose code is DIGITAL
  floidStatus.pyrOn           = true;
}
void loopPYR()
{
  // Are we in PYR from (Android) device mode?
  if(floidStatus.devicePYR)
  {
    // Were we sent a new pyr from the (Android) device?
    if(hasNewDevicePYR)
    {
      hasNewDevicePYR = false;
      newPYR          = true;  // Let the system know we have a new pyr even though from Device
      // Process it:
      processPyrPacket();
    }
  }
  else
  {
    loopPYRSerial();
  }
}
void setPYRAnalog() {
  PYR_SERIAL.println("$A*");
  clearPYRBuffers();
  floidStatus.pyrMode  = PYR_ANALOG;
  pyrDecodeBuffer[0]   = '\0';
  pyrAnalogBufferPos   = 0;
}
void setPYRDigital() {
  clearPYRBuffers();
  PYR_SERIAL.println("$D*");
  floidStatus.pyrMode = PYR_DIGITAL;
}
void setImuCalibrate() {
  PYR_SERIAL.println("$C*");
}
void setImuSave() {
   PYR_SERIAL.println("$S*");
 }
void setPYRRate(int rateIndex)
{
  floidStatus.pyrAnalogRate = rateIndex;
  PYR_SERIAL.print('$');
  switch(rateIndex)
  {
    case 0:
    {
      PYR_SERIAL.print('0');
    }
    break;
    case 1:
    {
      PYR_SERIAL.print('1');
    }
    break;
    case 2:
    {
      PYR_SERIAL.print('2');
    }
    break;
    case 3:
    {
      PYR_SERIAL.print('3');
    }
    break;
    case 4:
    {
      PYR_SERIAL.print('4');
    }
    break;
    case 5:
    {
      PYR_SERIAL.print('5');
    }
    break;
    default:
    {
      debugPrintlnToken(FLOID_DEBUG_PYR_BAD_RATE);
    }
    break;
  }
  PYR_SERIAL.println('*');
}
void loopPYRSerial()
{
  if(floidStatus.pyrOn)
  {
    // Check to see if we have any bytes available:
    int  bytesAvailable = PYR_SERIAL.available();
    if(bytesAvailable > 0)
    {
      int bytesWanted = min(bytesAvailable, PYR_INPUT_BUFFER_SIZE);
      int bytesRead   = PYR_SERIAL.readBytes(pyrInputBuffer, bytesWanted);
/*
      Serial.print("BA:");
      Serial.print(bytesAvailable);
      Serial.print(" BW:");
      Serial.print(bytesWanted);
      Serial.print(" BR:");
      Serial.println(bytesRead);
*/
      if(bytesRead > 0)
      {
        if(floidStatus.pyrMode == PYR_DIGITAL)
        {
    	  // Digital
          for(int i=0; i<bytesRead; ++i)
          {
            boolean newPYRFromDecode = pyrDecode((byte)pyrInputBuffer[i]);
            if(newPYRFromDecode)
            {
              // Potentially do something here but we are already outputting below
              newPYR = true;
              if(floidStatus.debug && floidStatus.debugPYR)
              {
                debugPrintlnToken(FLOID_DEBUG_PYR_PACKET_RECEIVED);
              }
            }
          }
        }
	    else
        {
	      // Analog
          for(int i=0; i<bytesRead; ++i)
    	  {
            if(pyrInputBuffer[i] == (byte)'\n')
            {
              pyrDecodeBuffer[pyrAnalogBufferPos]   = '\0';
              if(floidStatus.debug && floidStatus.debugPYR)
              {
	        debugPrintln((char *)pyrDecodeBuffer);
	        pyrAnalogBufferPos = 0;
              }
            }
            else
            {
              if(pyrAnalogBufferPos == PYR_DECODE_BUFFER_SIZE)
              {
                pyrDecodeBuffer[pyrAnalogBufferPos] = '\0';  // Note: We made buffer one larger so this null would fit
                if(floidStatus.debug && floidStatus.debugPYR)
                {
                  debugPrintln((char *)pyrDecodeBuffer);
                }
                pyrAnalogBufferPos = 0;
              }
              pyrDecodeBuffer[pyrAnalogBufferPos++] = pyrInputBuffer[i];
              pyrDecodeBuffer[pyrAnalogBufferPos]   = '\0';
            }
	  }
        }
      }
      else
      {
/*
        Serial.print("NBA:");
        Serial.println(bytesAvailable);
*/
      }
    }
  }
}
boolean pyrDecode(byte b)
{
  addPYRBufferByte(b);
  if(isValidPYRSentence())
  {
    return true;
  }
  return false;
}
void addPYRBufferByte(byte b)
{
  // Shift down:
  for(int i=0; i< PYR_DIGITAL_SENTENCE_LENGTH-1; ++i)
  {
    pyrDecodeBuffer[i] = pyrDecodeBuffer[i+1];
  }
  pyrDecodeBuffer[PYR_DIGITAL_SENTENCE_LENGTH-1] = b;
}
boolean isValidPYRSentence()
{
  // Do we have start sequence?
  if(pyrDecodeBuffer[0] != (byte)'!')
  {
    return false;
  }
  if(pyrDecodeBuffer[1] != (byte)'@')
  {
    return false;
  }
  if(pyrDecodeBuffer[2] != (byte)'#')
  {
    return false;
  }
  if(floidStatus.debug && floidStatus.debugPYR)
  {
    debugPrintlnToken(FLOID_DEBUG_PYR_VALID_START_CHAR);
  }
  // Get the packet number:
  boolean        goodPacketNumber = false;
  uint32_t       pyrPacketNumber  = 0l;
  uint32_t*      lPtr             = &pyrPacketNumber;
  for(int j = 0; j < 4; ++j)
  {
    ((byte*)lPtr)[j]=(*(byte*)&pyrDecodeBuffer[PYR_DIGITAL_PACKET_NUMBER_OFFSET + j]);
  }
  // NOTE: we reset the lasGoodPacketNumber at the bottom:
  if(lastGoodPacketNumber+1 != pyrPacketNumber)
  {
    if(floidStatus.debug && floidStatus.debugPYR)
    {
      debugPrintToken(FLOID_DEBUG_PYR_BAD_PACKET_NUMBER);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrint(lastGoodPacketNumber+1);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrintln(pyrPacketNumber);
      floidStatus.pyrGoodCount   = 0ul;
    }
    ++floidStatus.pyrErrorCount;
  }
  else
  {
    goodPacketNumber = true;
  }
  // OK, we have a packet number now...
  // Terminating chars:
  if(pyrDecodeBuffer[PYR_DIGITAL_SENTENCE_LENGTH - 3] != (byte)'$')
  {
    floidStatus.pyrGoodCount   = 0ul;
    if(floidStatus.debug && floidStatus.debugPYR)
    {
      debugPrintToken(FLOID_DEBUG_PYR_BAD_TERMINATING_CHAR);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrint(1);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrintln(pyrDecodeBuffer[PYR_DIGITAL_SENTENCE_LENGTH -3]);
    }
    ++floidStatus.pyrErrorCount;
    return false;
  }
  if(pyrDecodeBuffer[PYR_DIGITAL_SENTENCE_LENGTH - 2] != (byte)'%')
  {
    floidStatus.pyrGoodCount   = 0ul;
    if(floidStatus.debug && floidStatus.debugPYR)
    {
      debugPrintToken(FLOID_DEBUG_PYR_BAD_TERMINATING_CHAR);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrint(2);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrintln(pyrDecodeBuffer[PYR_DIGITAL_SENTENCE_LENGTH -2]);
    }
    ++floidStatus.pyrErrorCount;
    return false;
  }
  if(pyrDecodeBuffer[PYR_DIGITAL_SENTENCE_LENGTH - 1] != (byte)'^')
  {
    floidStatus.pyrGoodCount   = 0ul;
    if(floidStatus.debug && floidStatus.debugPYR)
    {
      debugPrintToken(FLOID_DEBUG_PYR_BAD_TERMINATING_CHAR);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrint(3);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrintln(pyrDecodeBuffer[PYR_DIGITAL_SENTENCE_LENGTH - 1]);
    }
    ++floidStatus.pyrErrorCount;
    return false;
  }
  // Try to convert the floats:
  for(int i = 0; i < PYR_NUMBER_FLOATS; ++i) {
    float* fPtr = &pyr[i];
    for(int j = 0; j < 4; ++j) {
      ((byte*)fPtr)[j]=(*(byte*)&pyrDecodeBuffer[i*4+PYR_DIGITAL_FLOAT_OFFSET + j]);
    }
  }
  for(int i=0; i<PYR_NUMBER_FLOATS-1; ++i) {
    // Make sure it is not a NaN:
    if(isnan(pyr[i])) {
      if(floidStatus.debug && floidStatus.debugPYR) {
        debugPrintToken(FLOID_DEBUG_PYR_NAN);
        debugPrint(FLOID_DEBUG_SPACE);
        debugPrintln(i);
      }
    }
  }
  // Check the checksum:
  byte *packetChecksum  = (byte *)(&pyr[PYR_NUMBER_FLOATS-1]);
  byte testChecksum[4];
  testChecksum[0]     = pyrDecodeBuffer[PYR_DIGITAL_PACKET_NUMBER_OFFSET+0];
  testChecksum[1]     = pyrDecodeBuffer[PYR_DIGITAL_PACKET_NUMBER_OFFSET+1];
  testChecksum[2]     = pyrDecodeBuffer[PYR_DIGITAL_PACKET_NUMBER_OFFSET+2];
  testChecksum[3]     = pyrDecodeBuffer[PYR_DIGITAL_PACKET_NUMBER_OFFSET+3];
  checksumData((byte*)&pyrDecodeBuffer[PYR_DIGITAL_PACKET_STATUS_OFFSET], testChecksum, (PYR_NUMBER_FLOATS-1)*sizeof(float)+1);
  for(int checksumIndex = 0; checksumIndex<4; ++checksumIndex)
  {
    if(packetChecksum[checksumIndex] != testChecksum[checksumIndex])
    {
      floidStatus.pyrGoodCount   = 0ul;
      // Invalid checksum:
      if(floidStatus.debug && floidStatus.debugPYR)
      {
	    debugPrintlnToken(FLOID_DEBUG_PYR_BAD_CHECKSUM);
      }
      ++floidStatus.pyrErrorCount;
      return false;
    }
  }
  // Get the algorithm status:
  memcpy((void *)&floidStatus.imuAlgorithmStatus, (void *)&pyrDecodeBuffer[PYR_DIGITAL_PACKET_STATUS_OFFSET], sizeof(uint16_t));
  // Process the packet:
  if(processPyrPacket())
  {
    lastGoodPacketNumber = pyrPacketNumber;
    return true;
  }
  // No joy:
  return false;
}
boolean processPyrPacket()
{
  float         tmpFloat = 0;  // For decode
  unsigned long time     = millis();
  // 2. Copy all the buffer values down:
  for(int i=0; i<PYR_BUFFER_ENTRY_LENGTH-1; ++i)
  {
    memcpy((void *)&pyrBufferEntry[i], (void *)&pyrBufferEntry[i+1], sizeof(PYRBufferEntry));
  }
  // 3. Copy in the new pyr values at the last entry:
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].time             = time;
  // Roll:
  tmpFloat = pyr[PYR_BOARD_OUTPUT_ROLL];
  if(!isnan(tmpFloat))
  {
    pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].roll           = tmpFloat;
  }
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].rollSin          = sin(ToRad(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].roll));
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].rollCos          = cos(ToRad(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].roll));
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].rollSmoothed     = smoothRoll();
  // Pitch:
  tmpFloat = pyr[PYR_BOARD_OUTPUT_PITCH];
  if(!isnan(tmpFloat))
  {
    pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitch          = tmpFloat;
  }
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitchSin         = sin(ToRad(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitch));
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitchCos         = cos(ToRad(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitch));
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitchSmoothed    = smoothPitch();
  // Heading: - This is in std trig angle - NOT COMPASS DEGREES:
  tmpFloat = keepInDegreeRange(compassToDegrees(pyr[PYR_BOARD_OUTPUT_HEADING] + floidStatus.declination));
  if(!isnan(tmpFloat))
  {
    pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].heading        = tmpFloat;
  }
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingSin       = sin(ToRad(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].heading));
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingCos       = cos(ToRad(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].heading));
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingSmoothed  = smoothHeading(); // In compass degrees, not std trig angle
  // Temperature:
  tmpFloat = pyr[PYR_BOARD_OUTPUT_TEMPERATURE];
  if(!isnan(tmpFloat))
  {
    pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].temperature = tmpFloat;
  }
  // Height:
  tmpFloat = pyr[PYR_BOARD_OUTPUT_HEIGHT];
  if(!isnan(tmpFloat))
  {
    pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].height = tmpFloat;
  }
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].heightSmoothed = smoothHeight();
  // 4. Calculate the new pyr velocities:
  // OK, I know the time is a bit fake since we smoothed these without regard to time but it should have been the average at that time given that condition so go with it!
  pyrTimeDelta = (pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].time - pyrBufferEntry[0].time);
  // Ensure our time is not too short:
  if(pyrTimeDelta <= 3ul)
  {
    pyrTimeDelta = 3ul;  // Manually advance time!
  }
  floidStatus.pitchVelocity       = ((pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitchSmoothed     - pyrBufferEntry[0].pitchSmoothed)     * 1000.0) / pyrTimeDelta;
  floidStatus.rollVelocity        = ((pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].rollSmoothed      - pyrBufferEntry[0].rollSmoothed)      * 1000.0) / pyrTimeDelta;
  floidStatus.headingVelocity     = ((pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingSmoothed   - pyrBufferEntry[0].headingSmoothed)   * 1000.0) / pyrTimeDelta;
  floidStatus.heightVelocity      = ((pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].heightSmoothed    - pyrBufferEntry[0].heightSmoothed)    * 1000.0) / pyrTimeDelta;
  // Debug print the buffer:
  if(floidStatus.debug && floidStatus.debugPYR)
  {
    printPYRBuffer(PYR_BUFFER_ENTRY_LENGTH-1);
    debugPrint(pyrTimeDelta);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(floidStatus.pitchVelocity);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(floidStatus.rollVelocity);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(floidStatus.headingVelocity);
  }
  ++floidStatus.pyrGoodCount;
  return true;
}
void checksumData(byte* data, byte* checksum, unsigned int length)
{
  for(unsigned int byteIndex=0; byteIndex<length; ++byteIndex)
  {
    checksum[byteIndex%4] ^= data[byteIndex];  // Wha-bam
  }
}
void  clearPYRBuffers()
{
  for(int i=0; i<PYR_BUFFER_ENTRY_LENGTH; ++i)
  {
    clearPYRBufferEntry(i);
  }
  for(int i=0; i<PYR_DECODE_BUFFER_SIZE+1; ++i)
  {
    pyrDecodeBuffer[i] = ' ';
  }
  pyrDecodeBuffer[0] = '\0';
  pyrAnalogBufferPos = 0;
}
void clearPYRBufferEntry(int i)
{
  pyrBufferEntry[i].time                = 0;
  pyrBufferEntry[i].roll                = 0;
  pyrBufferEntry[i].rollSin             = 0;
  pyrBufferEntry[i].rollCos             = 0;
  pyrBufferEntry[i].rollSmoothed        = 0;
  pyrBufferEntry[i].pitch               = 0;
  pyrBufferEntry[i].pitchSin            = 0;
  pyrBufferEntry[i].pitchCos            = 0;
  pyrBufferEntry[i].pitchSmoothed       = 0;
  pyrBufferEntry[i].heading             = 0;
  pyrBufferEntry[i].headingSin          = 0;
  pyrBufferEntry[i].headingCos          = 0;
  pyrBufferEntry[i].headingSmoothed     = 0;
  pyrBufferEntry[i].temperature         = 0;
  pyrBufferEntry[i].height              = 0;
  pyrBufferEntry[i].heightSmoothed      = 0;
}
void printPYRBuffer(int i)
{
  if(floidStatus.debug && floidStatus.debugPYR)
  {
    debugPrintToken(FLOID_DEBUG_PYR_TIME);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(pyrBufferEntry[i].time);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintToken(FLOID_DEBUG_PYR_PITCH);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(pyrBufferEntry[i].pitch);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintToken(FLOID_DEBUG_PYR_ROLL);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(pyrBufferEntry[i].roll);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintToken(FLOID_DEBUG_PYR_HEADING);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(pyrBufferEntry[i].heading);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintToken(FLOID_DEBUG_PYR_TEMPERATURE);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(pyrBufferEntry[i].temperature);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintToken(FLOID_DEBUG_PYR_ALTITUDE);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(pyrBufferEntry[i].height);
  }
}
void stopPYR()
{
  if(floidStatus.debug && floidStatus.debugPYR)
  {
    debugPrintlnToken(FLOID_DEBUG_PYR_STOP);
  }
  digitalWrite(pyrOnPin, LOW);
  floidStatus.pyrOn = false;
  clearPYR();
}
// Keep in range: -180 < x <= 180
float keepInDegreeRange(float degrees)
{
  while (degrees <= -180)
  {
    degrees += 360;
  }
  while (degrees > 180)
  {
    degrees -= 360;
  }
  return degrees;
}
// Compass heading to std trig angle
float compassToDegrees(float heading)
{
  return(keepInDegreeRange(90-heading));
}
 // Std trig angle to compass degrees
float degreesToCompass(float degrees)
{
  return(keepInDegreeRange(90-degrees));
}
float smoothPitch()
{
  // Idea: circularly average the angles, however if it is singular, we drop the oldest and repeat:
  for(int i=0; i<PYR_BUFFER_ENTRY_LENGTH; ++i)
  {
    float sumPitchSin = 0.0;
    float sumPitchCos = 0.0;
    for(int j=i; j<PYR_BUFFER_ENTRY_LENGTH; ++j)
    {
      sumPitchSin += pyrBufferEntry[j].pitchSin;
      sumPitchCos += pyrBufferEntry[j].pitchCos;
    }
    // Check for singularity:
    if((sumPitchSin != 0.0) || (sumPitchCos != 0.0))
    {
      float tmpFloat = ToDeg(atan2(sumPitchSin, sumPitchCos));
      if(!isnan(tmpFloat))
      {
        return tmpFloat;
      }
    }
  }
  // Error condition: just return the final value and do not smooth:
  return pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitch;
}
float smoothHeading()
{
  // Idea: circularly average the angles, however if it is singular, we drop the oldest and repeat:
  for(int i=0; i<PYR_BUFFER_ENTRY_LENGTH; ++i)
  {
    float sumHeadingSin = 0.0;
    float sumHeadingCos = 0.0;
    for(int j=i; j<PYR_BUFFER_ENTRY_LENGTH; ++j)
    {
      sumHeadingSin += pyrBufferEntry[j].headingSin;
      sumHeadingCos += pyrBufferEntry[j].headingCos;
    }
    // Check for singularity:
    if((sumHeadingSin != 0.0) || (sumHeadingCos != 0.0))
    {
      float tmpFloat = ToDeg(atan2(sumHeadingSin, sumHeadingCos));
      if(!isnan(tmpFloat))
      {
        return tmpFloat;
      }
    }
  }
  // Error condition: just return the final value and do not smooth:
  return pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].heading;
}
float smoothRoll()
{
  // Idea: circularly average the angles, however if it is singular, we drop the oldest and repeat:
  for(int i=0; i<PYR_BUFFER_ENTRY_LENGTH; ++i)
  {
    float sumRollSin = 0.0;
    float sumRollCos = 0.0;
    for(int j=i; j<PYR_BUFFER_ENTRY_LENGTH; ++j)
    {
      sumRollSin += pyrBufferEntry[j].rollSin;
      sumRollCos += pyrBufferEntry[j].rollCos;
    }
    // Check for singularity:
    if((sumRollSin != 0.0) || (sumRollCos != 0.0))
    {
      float tmpFloat = ToDeg(atan2(sumRollSin, sumRollCos));
      if(!isnan(tmpFloat))
      {
        return tmpFloat;
      }
    }
  }
  // Error condition: just return the final value and do not smooth:
  return pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].roll;
}
float smoothHeight()
{
  // Linear average:
  float sumHeight = 0.0;
  for(int i=0; i<PYR_BUFFER_ENTRY_LENGTH; ++i)
  {
    sumHeight += pyrBufferEntry[i].height;
  }
  return sumHeight / PYR_BUFFER_ENTRY_LENGTH;
}
