//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_NM.cpp
#include <FloidBoard3.h>
// Payload logic:
boolean       nmDeployed[NUMBER_PAYLOADS]                = { false,  false,  false,  false};
unsigned long nmFireTimes[NUMBER_PAYLOADS]               = {   0ul,    0ul,    0ul,    0ul};
long          payloadGoalId[NUMBER_PAYLOADS]             = { -1l, -1l, -1l, -1l};
unsigned long nmPayloadDropTimeout                       = 8000ul;
int           nmContractedLevel                          = 800;  // Analog read less than this amount means the nm has contracted since the pin which has the pull-up resistor turned on will be driven to ground
//int           nmContractedLevel                          = 1000;  // Analog read less than this amount means the nm has contracted since the pin which has the pull-up resistor turned on will be driven to ground
// Payload:
boolean setupPayloads()
{
  for(int payloadIndex=0; payloadIndex < NUMBER_PAYLOADS; ++payloadIndex)
  {
    // Set up but do not power on the NanoMuscle:
    // Power pin - contracts the NM:
    floidStatus.payloadGoalState[payloadIndex]             = GOAL_STATE_NONE;
    pinMode(bayReleasePins[payloadIndex], OUTPUT);
    digitalWrite(bayReleasePins[payloadIndex], LOW);                   // Do not contract yer
    // Analog NM Contracted input pin - goes to ground when the NM contracts:
    pinMode(bayReleaseAnalogNMStatusPins[payloadIndex], INPUT);
    digitalWrite(bayReleaseAnalogNMStatusPins[payloadIndex], HIGH);    // Turn on pull-up resistor - new hardware - this pin gets driven to ground (low analog value) when the nm is contracted - otherwise at high analog value since pull-up resistor is on
    // Bay Released detect pin:
    pinMode(bayReleaseDigitalBayStatusPins[payloadIndex], INPUT);
    digitalWrite(bayReleaseDigitalBayStatusPins[payloadIndex], HIGH);  // Turn on pull-up resistor - new hardware - this pin stays at ground (digital LOW) when the bay is shut, and goes to digital HIGH when the connection from the bay is broken...
    nmDeployed[payloadIndex] = false;
  }
  return true;
}
void loopPayloads()
{
  for(int payloadIndex=0; payloadIndex < NUMBER_PAYLOADS; ++payloadIndex)
  {
    floidStatus.nmState[payloadIndex]     = analogRead(bayReleaseAnalogNMStatusPins[payloadIndex]);           // New hardware: low analog value means that the nm has contracted since it has pull-up resistor turned on
    floidStatus.bayState[payloadIndex]    = (byte)digitalRead(bayReleaseDigitalBayStatusPins[payloadIndex]);  // New hardware: LOW digital value means that the bay is 
    if(nmDeployed[payloadIndex])
    {
      // Read our current states:
      // First, check to see if we have a goal and the bay is now open:
      if(((floidStatus.payloadGoalState[payloadIndex] == GOAL_STATE_START) || (floidStatus.payloadGoalState[payloadIndex] == GOAL_STATE_IN_PROGRESS)) && (floidStatus.bayState[payloadIndex] == HIGH))
      {
        if(floidStatus.debug && floidStatus.debugNM)
        {
          debugPrintToken(FLOID_DEBUG_PAYLOAD_RELEASED);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrintln(payloadIndex);
        }
        // Bay is opened!
        floidStatus.payloadGoalState[payloadIndex] = GOAL_STATE_COMPLETE_SUCCESS;
        stopPayload(payloadIndex);
      }
      // Bay is closed - are we working on a goal?
      if(floidStatus.payloadGoalState[payloadIndex] == GOAL_STATE_START)
      {
        // NMs are on - have we reached a contraction:(pin gets driven to ground and should now have low analog value)
        if(floidStatus.nmState[payloadIndex] <= nmContractedLevel)
        {
          if(floidStatus.debug && floidStatus.debugNM)
          {
            debugPrintToken(FLOID_DEBUG_PAYLOAD_NM_FULLY_CONTRACTED);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrintln(payloadIndex);
          }
          floidStatus.payloadGoalState[payloadIndex] = GOAL_STATE_IN_PROGRESS;
          // We disengage the nm but leave the state as deployed (e.g. do not stop payload)
          disengageNM(payloadIndex);
        }
      }
      // OK, finally check once again to see if we have a goal and now to see if it has timed out
      if((floidStatus.payloadGoalState[payloadIndex] == GOAL_STATE_START) || (floidStatus.payloadGoalState[payloadIndex] == GOAL_STATE_IN_PROGRESS))
      {
        if(millis() - nmFireTimes[payloadIndex] > nmPayloadDropTimeout)
        {
          if(floidStatus.debug && floidStatus.debugNM)
          {
            debugPrintToken(FLOID_DEBUG_PAYLOAD_TIMED_OUT);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrintln(payloadIndex);
          }
          floidStatus.payloadGoalState[payloadIndex] = GOAL_STATE_COMPLETE_ERROR;
          stopPayload(payloadIndex);
        }
      }
    }
  }
  return;
}
void startPayload(int payloadIndex, long goalId)
{
  if(floidStatus.debug && floidStatus.debugNM)
  {
    debugPrintToken(FLOID_DEBUG_PAYLOAD_NM_START);
    debugPrint(payloadIndex);
  }
  engageNM(payloadIndex);
  nmDeployed[payloadIndex]                    = true;
  nmFireTimes[payloadIndex]                   = millis();
  payloadGoalId[payloadIndex]                 = goalId;
  floidStatus.payloadGoalState[payloadIndex]  = GOAL_STATE_START;
}
void stopPayload(int payloadIndex)
{
  if(floidStatus.debug && floidStatus.debugNM)
  {
    debugPrintToken(FLOID_DEBUG_PAYLOAD_NM_STOP);
    debugPrint(payloadIndex);
  }
  disengageNM(payloadIndex);
  nmDeployed[payloadIndex] = false;
}
void dropPayload(int bay, long goalId)
{
  startPayload(bay, goalId);
}
void engageNM(int payloadIndex)
{
  digitalWrite(bayReleasePins[payloadIndex], HIGH);    // Fire in the hole
}
void disengageNM(int payloadIndex)
{
  digitalWrite(bayReleasePins[payloadIndex], LOW);    // Turn it off
}
