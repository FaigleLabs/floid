//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Status.h
// faiglelabs.com
#ifndef	__FLOIDBOARD3_STATUS_H__
#define	__FLOIDBOARD3_STATUS_H__

// Extra ACC Time:
// Define this to use all the extra acc give times:
#define PERFORM_EXTRA_ACC
// Modes:
#define        FLOID_MODE_TEST                     (0)
#define        FLOID_MODE_MISSION                  (1)
// Periodic actions and status sends:
// TODO [SET_PARAMETER] MAKE THIS INTO A PARAMETER:
// Physics is called 4Hz:
#define        FLOID_PERIOD                        (250ul)
#define        LOOP_RATE_PERIOD                    (2000ul)
// Servo indices:
#define        SERVO_LEFT                          (0)
#define        SERVO_RIGHT                         (1)
#define        SERVO_PITCH                         (2)
#define        SERVO_ESC                           (3)
// Goal states:
#define        GOAL_STATE_NONE                     (0)
#define        GOAL_STATE_START                    (1)
#define        GOAL_STATE_IN_PROGRESS              (2)
#define        GOAL_STATE_COMPLETE_SUCCESS         (3)
#define        GOAL_STATE_COMPLETE_ERROR           (4)
// Goal target states:
#define        GOAL_TARGET_STATE_UNKNOWN           (0)
#define        GOAL_TARGET_STATE_ON_TARGET         (1)
#define        GOAL_TARGET_STATE_OFF_TARGET        (2)
// IMU Algorithm Status:
#define        IMU_ALGORITHM_STATUS_CALIBRATED      (0x08)
struct PYRBufferEntry
{
 public:
  unsigned long time;
  float         roll;
  float         rollSin;
  float         rollCos;
  float         rollSmoothed;
  float         pitch;
  float         pitchSin;
  float         pitchCos;
  float         pitchSmoothed;
  float         heading; // In compass degrees
  float         headingSin;
  float         headingCos;
  float         headingSmoothed; // In compass degrees
  float         temperature;
  float         height;
  float         heightSmoothed;
};
struct FloidStatus
{
  public:
    // Header fields:
    PacketHeader  packetHeader;
    // Status time, mode:
    unsigned long floidStatusNumber;
    long          statusTime;
    byte          floidModel;
    byte          mode;
    // Direction, velocity, altitude:
    float         directionOverGround;
    float         velocityOverGround;
    float         altitude;           // Calculated altitude
    float         altitudeVelocity;   // Calculated altitude velocity
    // Goals:
    bool          hasGoal;            // Has goal
    unsigned long goalTicks;          // Ticks on this goal
    unsigned long goalStartTime;      // Goal start time;
    unsigned long goalId;             // goal id
    byte          goalState;          // goal state
    byte          goalTargetState;    // goal target state
    float         goalX;
    float         goalY;
    float         goalZ;
    float         goalAngle;          // Angle
    // Systems:
    bool          inFlight;            // In flight
    bool          liftOffMode;         // Lift Off Mode
    bool          landMode;            // Land Mode
    // ESCs:
    boolean       escServoOn[NUMBER_HELICOPTERS];
    float         escValues[NUMBER_HELICOPTERS];
    int           escServoPulses[NUMBER_HELICOPTERS];
    // Altimeter:
    float         altimeterAltitude;
    float         altimeterAltitudeVelocity;
    float         altimeterAltitudeDelta;
    boolean       altimeterAltitudeValid;
    boolean       altimeterInitialized;
    unsigned long altimeterGoodCount;
    unsigned long altimeterLastUpdateTime;
    // Batteries and currents:
    float         mainBatteryVoltage;
    float         heliBatteryVoltages[NUMBER_HELICOPTERS];
    float         auxBatteryVoltage;
    float         currents[NUMBER_HELICOPTERS];
    float         auxCurrent;
    // Bays, Payloads, Nano-muscles:
    int           nmState[NUMBER_PAYLOADS];
    byte          bayState[NUMBER_PAYLOADS];
    byte          payloadGoalState[NUMBER_PAYLOADS];
    // Camera:
    boolean       cameraOn;
    // Debug: 
    boolean       debug;
    boolean       debugMem;
    boolean       debugGPS;
    boolean       debugPYR;
    boolean       debugPhysics;
    boolean       debugPanTilt;
    boolean       debugNM;
    boolean       debugCamera;
    boolean       debugDesignator;
    boolean       debugAUX;
    boolean       debugHelis;
    // Designator:
    boolean       designatorOn;
    // GPS:
    boolean            gpsOn;
    unsigned long      gpsTime;
    float              gpsXDegreesDecimal;
    float              gpsYDegreesDecimal;
    int                gpsFixQuality;
    float              gpsZMeters;
    float              gpsZFeet;
    float              gpsHDOP;
    int                gpsSats;
    float              gpsXDegreesDecimalFiltered;
    float              gpsYDegreesDecimalFiltered;
    float              gpsZMetersFiltered;
    unsigned long      gpsGoodCount;
    // Heli Servos:
    boolean            helisOn;
    boolean            heliServoOn[NUMBER_HELICOPTERS];
    float              heliServoValues[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];  // Calculated only on output
    // LED:
    boolean            ledOn[NUMBER_LED_PINS];
    // Parachute:
    boolean            parachuteDeployed;
    // Pan Tilts:
    boolean            panTiltOn[NUMBER_PANTILTS];
    byte               panTiltMode[NUMBER_PANTILTS];
    float              panTiltTargetX[NUMBER_PANTILTS];
    float              panTiltTargetY[NUMBER_PANTILTS];
    float              panTiltTargetZ[NUMBER_PANTILTS];
    byte               panTiltServoValues[NUMBER_PANTILTS][2];
    // Aux:
    boolean            auxOn;
    // PYR:
    boolean            pyrOn;
    byte               pyrMode;
    byte               pyrAnalogRate;
    float              pitchVelocity;
    float              rollVelocity;
    float              heightVelocity;
    float              headingVelocity;
    unsigned long      pyrGoodCount;
    unsigned long      pyrErrorCount;
    PYRBufferEntry     outputPYRBufferEntry; // This is copied from the buffer entry array just before output
    int                escType;
    int                freeMemory;
    int                loopCount;
    float              loopRate;
    boolean            serialOutput; // Serial output on
    byte               statusRate;
    float              declination;
    byte               gpsRate;
    float              liftOffTargetAltitude;
    float              distanceToTarget;
    float              altitudeToTarget;
    float              deltaHeadingAngleToTarget;
    float              orientationToGoal; 
    float              attackAngle;
    float              targetOrientationPitch;
    float              targetOrientationRoll;
    float              targetOrientationPitchDelta;
    float              targetOrientationRollDelta;
    float              targetPitchVelocity;
    float              targetRollVelocity;
    float              targetPitchVelocityDelta;
    float              targetRollVelocityDelta;
    float              targetAltitudeVelocity;
    float              targetAltitudeVelocityDelta;
    float              targetHeadingVelocity;
    float              targetHeadingVelocityDelta;
    float              targetXYVelocity;
    // For land mode:
    boolean            landModeMinAltitudeCheckStarted;
    float              landModeMinAltitude;
    unsigned long      landModeMinAltitudeTime;
    // Follow mode:
    boolean            followMode;
    // Emulate GPS:
    boolean            emulateGPS;
    // Heli Startup Mode:
    boolean            heliStartupMode;
    // GPS from Device:
    boolean            deviceGPS;
    // PYR from Device:
    boolean            devicePYR;
    // PYR from Device:
    boolean            debugAltimeter;
    /* ==============================
     * Run/Test Modes:
     * ============================== */
    byte floidRunMode;
    byte floidTestMode;
    // IMU Algorithm Status:
    uint16_t imuAlgorithmStatus;
};
// Test Response:
struct TestResponse
{
  public:
    PacketHeader  packetHeader;
    byte          bytes[3];
    int           ints[3];
    unsigned int  unsignedInts[3];
    long          longs[3];
    unsigned long unsignedLongs[3];
};
void     setupTestResponse();
boolean  sendTestResponse();
// OUTPUT MESSAGES:
// ================
// Message Types:
#define       STATUS_PACKET                                 (0x00)
#define       COMMAND_RESPONSE_PACKET                       (0x01)
#define       DEBUG_MESSAGE_PACKET                          (0x02)
#define       TEST_RESPONSE_PACKET                          (0x03)
#define       MODEL_PARAMETERS_PACKET                       (0x04)
#define       MODEL_STATUS_PACKET                           (0x05)
// Output Period:
#define       STATUS_OUTPUT_PERIOD                          (1000ul)
// Floid Status:
extern boolean        setupSuccess;
extern FloidStatus    floidStatus;
extern TestResponse   testResponse;
extern unsigned long  lastTime;                  // Used for storing the time for our periodic actions...
extern unsigned long  currentTime;               // Used for storing the time for our periodic actions...
extern unsigned long  lastStatusSendTime;        // Used for storing the time for our periodic actions...
extern int            currentStatusRateMillis;
// Methods:
void    setupFloidStatus();
boolean sendFloidStatus();
void    setupPacketHeader(PacketHeader* packetHeaderPtr, byte packetType, int packetSize);
boolean sendPacket(byte* packet, int packetSize);
boolean sendTestResponsePacket();
void    printFloidStatusOffsets();
void    printTestResponseOffsets();

// Production mode:
void setFloidProductionMode();
bool isFloidProductionMode();
// Physics tests:
bool isTestModePhysicsNoXY();
bool isTestModePhysicsNoAltitude();
bool isTestModePhysicsNoAttackAngle();
bool isTestModePhysicsNoHeading();
bool isTestModePhysicsNoPitch();
bool isTestModePhysicsNoRoll();
bool isTestModePrintDebugPhysicsHeadings();
bool isTestModeCheckPhysicsModel();
bool isRunModePhysicsTestXY();
bool isRunModePhysicsTestAltitude1();
bool isRunModePhysicsTestAltitude2();
bool isRunModePhysicsTestHeading1();
bool isRunModePhysicsTestPitch1();
bool isRunModePhysicsTestRoll1();

// Set modes:
void setTestModePhysicsNoXY(bool on);
void setTestModePhysicsNoAltitude(bool on);
void setTestModePhysicsNoAttackAngle(bool on);
void setTestModePhysicsNoHeading(bool on);
void setTestModePhysicsNoPitch(bool on);
void setTestModePhysicsNoRoll(bool on);
void setTestModePhysicsPrintDebugPhysicsHeadings(bool on);
void setTestModeCheckPhysicsModel(bool on);
void setRunModePhysicsTestAltitude1(bool on);
void setRunModePhysicsTestAltitude2(bool on);
void setRunModePhysicsTestHeading1(bool on);
void setRunModePhysicsTestPitch1(bool on);
void setRunModePhysicsTestRoll1(bool on);
void setRunModePhysicsTestXY(bool on);

// Set/clear bit utilities:
bool checkBit(byte b, byte bit);
void setBit(byte *b, byte bit);
void clearBit(byte *b, byte bit);
void toggleBit(byte *b, byte bit);

#endif /* __FLOIDBOARD3_STATUS_H__ */



