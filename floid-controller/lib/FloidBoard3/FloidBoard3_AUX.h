//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_AUX.h:
#ifndef	__FLOIDBOARD3_AUX_H__
#define	__FLOIDBOARD3_AUX_H__
// Aux logic:
extern boolean       auxDeployed;
extern unsigned long auxFireTime;
// AUX:
boolean setupAux();
void    startAux();
void    loopAux();
void    loopAuxSerial();
void    stopAux();
#endif /* __FLOIDBOARD3_AUX_H__ */
