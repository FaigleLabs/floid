//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Camera.cpp
// faiglelabs.com
#include <FloidBoard3.h>
byte          cameraPanTiltIndex = PAN_TILT_CAMERA;
boolean       cameraDeployed     = false;
unsigned long cameraFireTime     = 0ul;
boolean setupCamera()
{
  // Camera pan tilt:
  floidStatus.cameraOn           = false;
  cameraPanTiltIndex = PAN_TILT_CAMERA;
  cameraDeployed     = false;
  cameraFireTime     = 0ul;
  return true;
}
void loopCamera()
{
  // Does nothing
}
void startCamera()
{
  floidStatus.cameraOn = true;
  cameraDeployed       = true;
  cameraFireTime       = millis();
  startPanTilt(cameraPanTiltIndex);
}
void stopCamera()
{
  floidStatus.cameraOn = false;
  stopPanTilt(cameraPanTiltIndex);
}
void setCameraTarget(float x, float y, float z)
{
  setPanTiltTargetMode(cameraPanTiltIndex, x, y, z);
}
void setCameraAngle(byte pan, byte tilt)
{
  setPanTiltAngleMode(cameraPanTiltIndex, pan, tilt);
}
void setCameraScan(byte panMin, byte panMax, unsigned long panPeriod, byte tiltMin, byte tiltMax, unsigned long tiltPeriod)
{
  setPanTiltScanMode(cameraPanTiltIndex, panMin, panMax, panPeriod, tiltMin, tiltMax, tiltPeriod);
}
void setCameraPanTiltIndex(byte index)
{
  cameraPanTiltIndex = index;
}
