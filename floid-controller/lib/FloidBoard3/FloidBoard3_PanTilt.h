//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_PanTilt.h:
#ifndef	__FLOIDBOARD3_PAN_TILT_H__
#define	__FLOIDBOARD3_PAN_TILT_H__
// PanTilt Logic:
#define              PAN_INDEX                            (0)
#define              TILT_INDEX                           (1)
#define              PAN_TILT_MODE_ANGLE                  (0x00)
#define              PAN_TILT_MODE_TARGET                 (0x01)
#define              PAN_TILT_MODE_SCAN                   (0x02)
extern byte          panTiltScanPanMin[NUMBER_PANTILTS];
extern byte          panTiltScanPanMax[NUMBER_PANTILTS];
extern byte          panTiltScanTiltMin[NUMBER_PANTILTS];
extern byte          panTiltScanTiltMax[NUMBER_PANTILTS];
extern unsigned long panTiltScanPanPeriod[NUMBER_PANTILTS];
extern unsigned long panTiltScanTiltPeriod[NUMBER_PANTILTS];
extern byte          panTiltScanPanCurrent[NUMBER_PANTILTS];
extern byte          panTiltScanTiltCurrent[NUMBER_PANTILTS];
extern byte          panTiltScanPanStep[NUMBER_PANTILTS];
extern byte          panTiltScanTiltStep[NUMBER_PANTILTS];
extern boolean       panTiltServoDeployed[NUMBER_PANTILTS];
extern byte          panTiltServoMinValues[NUMBER_PANTILTS][2];
extern byte          panTiltServoDefaultValues[NUMBER_PANTILTS][2];
extern byte          panTiltServoMaxValues[NUMBER_PANTILTS][2];
boolean setupPanTilts();
void    loopPanTilts();
void    startPanTilt(int panTiltIndex);
void    stopPanTilt(int panTiltIndex);
void    setPanTiltAngleMode(int panTiltIndex, byte pan, byte tilt);
void    startAngleMode(int panTiltIndex);
void    setPanTiltIndividual(int panTiltIndex, int panOrTilt, byte panTiltValue);
void    attachPanTiltServos(int panTiltIndex);
void    setPanTiltServoValues(int panTiltIndex);
void    setPanTiltTargetMode(int panTiltIndex, float x, float y, float z);
void    startTargetMode(int panTiltIndex);
void    setPanTiltScanMode(int  panTiltIndex, byte panMin, byte panMax, unsigned long panPeriod, byte tiltMin, byte tiltMax, unsigned long tiltPeriod);
void    startScanMode(int panTiltIndex);
void    calculateCurrentTargetServoValues(int panTiltIndex);
void    calculateCurrentScanServoValues(int panTiltIndex);
#endif /* __FLOIDBOARD3_PAN_TILT_H__ */
