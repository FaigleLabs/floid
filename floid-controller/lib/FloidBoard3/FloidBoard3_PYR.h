//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_PYR.h:
#ifndef	__FLOIDBOARD3_PYR_H__
#define	__FLOIDBOARD3_PYR_H__
// *pi/180
#define       ToRad(x)                          ((x)*0.01745329252)
// *180/pi
#define       ToDeg(x)                          ((x)*57.2957795131)
// PYR logic:
#define       PYR_ANALOG                        (0)
#define       PYR_DIGITAL                       (1)
#define       PYR_ANALOG_DEFAULT_RATE           (3)
#define       PYR_INPUT_BUFFER_SIZE             (128)
#define       PYR_DECODE_BUFFER_SIZE            (128)
// Digital sentence From PYR - note: PDSL must be <= PDBS:
#define       PYR_DIGITAL_SENTENCE_LENGTH       (36)
#define       PYR_DIGITAL_PACKET_NUMBER_OFFSET  (3)
#define       PYR_DIGITAL_PACKET_STATUS_OFFSET  (7)
#define       PYR_DIGITAL_FLOAT_OFFSET          (9)
#define       PYR_NUMBER_FLOATS                 (6)
#define       PYR_BOARD_OUTPUT_PITCH            (0x00)
#define       PYR_BOARD_OUTPUT_ROLL             (0x01)
#define       PYR_BOARD_OUTPUT_HEADING          (0x02)
#define       PYR_BOARD_OUTPUT_TEMPERATURE      (0x03)
#define       PYR_BOARD_OUTPUT_HEIGHT           (0x04)
#define       PYR_BOARD_OUTPUT_CHECKSUM         (0x05)
// Queue of PYR Entries from board:
#define       PYR_BUFFER_ENTRY_LENGTH           (5)
// PYR:
boolean      setupPYR();
void         startPYR();
void         loopPYR();
void         setPYRAnalog();
void         setPYRDigital();
void         setPYRRate(int rateIndex);
void         setImuCalibrate();
void         setImuSave();
void         loopPYRSerial();
boolean      pyrDecode(byte b);
void         addPYRBufferByte(byte b);
boolean      isValidPYRSentence();
boolean      processPyrPacket();
void         checksumData(byte* data, byte* checksum, unsigned int length);
void         clearPYRBuffers();
void         clearPYRBufferEntry(int i);
void         printPYRBuffer(int i);
void         stopPYR();
float        keepInDegreeRange(float degrees); // -180 < x <= 180
float        compassToDegrees(float heading); // Compass heading to std trig angle
float        degreesToHeading(float degrees); // Std trig angle to compass degrees
float        smoothPitch();
float        smoothHeading();
float        smoothRoll();
float        smoothHeight();
extern boolean         hasNewDevicePYR;
extern boolean         newPYR;
extern unsigned long   pyrFireTime;
extern byte            pyrDecodeBuffer[PYR_DECODE_BUFFER_SIZE+1];
extern unsigned int    pyrAnalogBufferPos;
extern float           pyr[PYR_NUMBER_FLOATS];      // Conversion buffer for output floats + checksum float;
extern PYRBufferEntry  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH];
extern unsigned long   pyrTimeDelta;
#endif /* __FLOIDBOARD3_PYR_H__ */
