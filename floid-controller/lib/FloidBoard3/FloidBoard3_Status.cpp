//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Status.cpp
// faiglelabs.com
#include <FloidBoard3.h>
#include <FloidBoard3_ACC.h>
// Floid Status:
boolean        setupSuccess                        = false;
FloidStatus    floidStatus;
TestResponse   testResponse;
unsigned long  lastTime                            = 0ul;    // Used for storing the time for our periodic actions...
unsigned long  currentTime                         = 0ul;    // Used for storing the time for our periodic actions...
unsigned long  lastStatusSendTime                  = 0ul;    // Used for storing the time for our periodic actions...
int            currentStatusRateMillis             = 1000;   // Matches the statusRate '4' below:
void setupFloidStatus()
{
  floidStatus.statusRate           = 4;  // Defaults to once per second (1Hz)
  currentStatusRateMillis          = 1000;  // Matches the statusRate '4' above
  // Loop count and rate (per status update)
  floidStatus.loopCount            = 0;
  floidStatus.loopRate             = 0.0;
  // Model, status time, mode:
  floidStatus.floidModel           = 3;  // 3 and 4 are same..
  floidStatus.statusTime           = 0l;
  floidStatus.mode                 = FLOID_MODE_TEST;
  floidStatus.followMode           = true;
  // Direction, velocity, altitude, declination:
  floidStatus.directionOverGround  = 0;            // Std trig degrees NOT compass degrees
  floidStatus.velocityOverGround   = 0;            // Velocity moving the Direction Over Ground
  floidStatus.altitude             = 0.0;          // Calculated altitude - from GPS or altimeter - in meters
  floidStatus.altitudeVelocity     = 0.0;          // Altitude velocity - in m/s
  floidStatus.declination          = 0.0;          // Current magnetic declination
  // Goals:
  floidStatus.hasGoal              = false;        // Has goal
  floidStatus.goalTicks            = 0l;           // Ticks on this goal
  floidStatus.goalStartTime        = 0l;           // Goal start time;
  floidStatus.goalId               = 0l;           // goal id
  floidStatus.goalX                = 0.0;
  floidStatus.goalY                = 0.0;
  floidStatus.goalZ                = 0.0;
  floidStatus.goalAngle            = 0.0;          // Goal Angle - in std trig degrees
  floidStatus.goalState            = 0;            // Goal State
  floidStatus.goalTargetState      = GOAL_STATE_NONE; // Goal Target State
  // Systems:
  floidStatus.inFlight             = false;        // In flight
  floidStatus.liftOffMode          = false;        // Lift Off Mode
  floidStatus.landMode             = false;        // Land Mode
  // ESCs:
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    floidStatus.escServoOn[heliIndex]     = false;
    floidStatus.escValues[heliIndex]      = 0.0;
    floidStatus.escServoPulses[heliIndex] = 0;
  }
  // Altimeter:
  floidStatus.altimeterAltitude          = 0.0;
  floidStatus.altimeterAltitudeVelocity  = 0.0;
  floidStatus.altimeterAltitudeDelta     = 0.0;
  floidStatus.altimeterAltitudeValid     = false;
  floidStatus.altimeterInitialized       = false;
  floidStatus.altimeterGoodCount         = 0ul;
  floidStatus.altimeterLastUpdateTime    = 0ul;
  // Batteries and currents:
  floidStatus.mainBatteryVoltage         = 0.0;
  floidStatus.auxBatteryVoltage          = 0.0;
  floidStatus.auxCurrent                 = 0.0;
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    floidStatus.heliBatteryVoltages[heliIndex] = 0.0;
    floidStatus.currents[heliIndex]            = 0.0;
  }
  // Payloads, bays and nano-muscles:
  for(int payloadIndex=0; payloadIndex<NUMBER_PAYLOADS; ++payloadIndex)
  {
    floidStatus.nmState[payloadIndex]          = 0;
    floidStatus.bayState[payloadIndex]         = 0;
    floidStatus.payloadGoalState[payloadIndex] = GOAL_STATE_NONE;
  }
  // Camera:
  floidStatus.cameraOn                   = false;
  // Debug:
  floidStatus.debug                      = false; // [PROD] - SET THIS TO FALSE FOR PROD
  floidStatus.debugMem                   = false;
  floidStatus.debugGPS                   = false;
  floidStatus.debugPYR                   = false;
  floidStatus.debugPhysics               = false;
  floidStatus.debugPanTilt               = false;
  floidStatus.debugNM                    = false;
  floidStatus.debugCamera                = false;
  floidStatus.debugDesignator            = false;
  floidStatus.debugAUX                   = false;
  floidStatus.debugHelis                 = false;
  floidStatus.debugAltimeter             = false;
  floidStatus.serialOutput               = true;
  // Designator:
  floidStatus.designatorOn               = false;
  // GPS:
  floidStatus.gpsOn                      = false;
  floidStatus.gpsTime                    = 0l;
  floidStatus.gpsXDegreesDecimal         = 0.0;
  floidStatus.gpsYDegreesDecimal         = 0.0;
  floidStatus.gpsFixQuality              = 0;
  floidStatus.gpsZMeters                 = 0.0;
  floidStatus.gpsZFeet                   = 0.0;
  floidStatus.gpsHDOP                    = 0.0;
  floidStatus.gpsSats                    = 0;
  floidStatus.gpsXDegreesDecimalFiltered = 0.0;
  floidStatus.gpsYDegreesDecimalFiltered = 0.0;
  floidStatus.gpsZMetersFiltered         = 0.0;
  floidStatus.gpsGoodCount               = 0ul;
  // Heli Servos:
  floidStatus.helisOn                    = false;
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    floidStatus.heliServoOn[heliIndex] = false;
    for(int servoIndex=0; servoIndex<NUMBER_HELI_SERVOS; ++servoIndex)
    {
      floidStatus.heliServoValues[heliIndex][servoIndex] = 0.0;
    }
  }
  // LED:
  for(int ledIndex=0; ledIndex<NUMBER_LED_PINS; ++ledIndex)
  {
    floidStatus.ledOn[ledIndex] = false;
  }
  // Parachute:
  floidStatus.parachuteDeployed         = false;
  // Pan Tilts:
  for(int panTiltIndex=0; panTiltIndex < NUMBER_PANTILTS; ++panTiltIndex)
  {
    floidStatus.panTiltOn[panTiltIndex]                      = false;
    floidStatus.panTiltMode[panTiltIndex]                    = PAN_TILT_MODE_ANGLE;
    floidStatus.panTiltTargetX[panTiltIndex]                 = 0.0;
    floidStatus.panTiltTargetY[panTiltIndex]                 = 0.0;
    floidStatus.panTiltTargetZ[panTiltIndex]                 = 0.0;
    floidStatus.panTiltServoValues[panTiltIndex][PAN_INDEX]  = 0;
    floidStatus.panTiltServoValues[panTiltIndex][TILT_INDEX] = 0;
  }
  // GPS:
  floidStatus.gpsRate                                         = 1; // Defaults to 5Hz
  // Aux:
  floidStatus.auxOn                                          = false;
  // PYR:
  floidStatus.pyrOn                                          = false;
  floidStatus.pyrAnalogRate                                  = PYR_ANALOG_DEFAULT_RATE;
  floidStatus.pyrMode                                        = PYR_DIGITAL;
  floidStatus.pitchVelocity                                  = 0.0;
  //  floidStatus.yawVelocity                                    = 0.0;
  floidStatus.rollVelocity                                   = 0.0;
  floidStatus.heightVelocity                                 = 0.0;
  floidStatus.headingVelocity                                = 0.0;
  floidStatus.pyrGoodCount                                   = 0ul;
  floidStatus.pyrErrorCount                                  = 0ul;
  // Model Goals:
  floidStatus.liftOffTargetAltitude                          = 0.0;
  floidStatus.distanceToTarget                               = 0.0;
  floidStatus.altitudeToTarget                               = 0.0;
  floidStatus.deltaHeadingAngleToTarget                            = 0.0;
  floidStatus.orientationToGoal                              = 0.0;
  floidStatus.attackAngle                                    = 0.0;
  // Targets:
  floidStatus.targetOrientationPitch                         = 0.0;
  floidStatus.targetOrientationRoll                          = 0.0;
  floidStatus.targetOrientationPitchDelta                    = 0.0;
  floidStatus.targetOrientationRollDelta                     = 0.0;
  floidStatus.targetPitchVelocity                            = 0.0;
  floidStatus.targetRollVelocity                             = 0.0;
  floidStatus.targetPitchVelocityDelta                       = 0.0;
  floidStatus.targetRollVelocityDelta                        = 0.0;
  floidStatus.targetAltitudeVelocity                         = 0.0;
  floidStatus.targetAltitudeVelocityDelta                    = 0.0;
  floidStatus.targetHeadingVelocity                          = 0.0;
  floidStatus.targetHeadingVelocityDelta                     = 0.0;
  floidStatus.targetXYVelocity                               = 0.0;
  // Land Mode:
  floidStatus.landModeMinAltitudeCheckStarted                = false;
  floidStatus.landModeMinAltitude                            = 0;
  floidStatus.landModeMinAltitudeTime                        = 0l;
  // Heli Startup Mode:
  floidStatus.heliStartupMode                                = false;
  // GPS from device:
  floidStatus.deviceGPS                                      = false;
  // PYR from device:
  floidStatus.devicePYR                                      = false;
  // Never sent:
  lastStatusSendTime                                         = 0ul;
  // imuAlgorithmStatus
  floidStatus.imuAlgorithmStatus                             = 0;
  /* ==============================
   * Run/Test Modes:
   * ============================== */
  floidStatus.floidRunMode = FLOID_RUN_MODE_PRODUCTION
//                 | FLOID_RUN_MODE_PHYSICS_TEST_XY
//                 | FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1
//                 | FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2
//                 | FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1
//                 | FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1
//                 | FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1
                 ;
  floidStatus.floidTestMode = FLOID_TEST_MODE_OFF
//                  | FLOID_TEST_MODE_PHYSICS_NO_XY
//                  | FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE
//                  | FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE
//                  | FLOID_TEST_MODE_PHYSICS_NO_HEADING
//                  | FLOID_TEST_MODE_PHYSICS_NO_PITCH
//                  | FLOID_TEST_MODE_PHYSICS_NO_ROLL
                  ;

}
boolean sendFloidStatus()
{
  // 1. Copy in the current status time, the pyr buffer entry and make any other adjustments to the status packet before sending
  // Increase the status number:
  floidStatus.floidStatusNumber++;
  floidStatus.floidModel = FLOID_MODEL_THREE;
  floidStatus.statusTime = millis();
  floidStatus.escType    = floidModelParameters.floidModelESCType; // Note this is a copy from the model parameters to the status...
  floidStatus.freeMemory = get_free_memory();
  memcpy(&floidStatus.outputPYRBufferEntry, &pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1], sizeof(PYRBufferEntry));
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    for(int servoIndex=0; servoIndex<NUMBER_HELI_SERVOS; ++servoIndex)
    {
      floidStatus.heliServoValues[heliIndex][servoIndex] = floidModelHeliValueFromServoPulse(heliIndex, servoIndex);
    }
  }
  // 2. Set up the header
  setupPacketHeader(&floidStatus.packetHeader, (byte)STATUS_PACKET, sizeof(FloidStatus));
  // 3. Send the packet:
  return sendPacket((byte*)&floidStatus, sizeof(FloidStatus));
}
void printFloidStatusOffsets()
{
  DEBUG_SERIAL.println("XXX");
  DEBUG_SERIAL.println("FS");
  DEBUG_SERIAL.println(sizeof(FloidStatus));
  DEBUG_SERIAL.println("fsn");
  DEBUG_SERIAL.println(offsetof(FloidStatus, floidStatusNumber));
  DEBUG_SERIAL.println(offsetof(FloidStatus, statusTime));
  DEBUG_SERIAL.println(offsetof(FloidStatus, floidModel));
  DEBUG_SERIAL.println(offsetof(FloidStatus, mode));
  //Direction,velocity,altitude:
  DEBUG_SERIAL.println("dva");
  DEBUG_SERIAL.println(offsetof(FloidStatus, directionOverGround));
  DEBUG_SERIAL.println(offsetof(FloidStatus, velocityOverGround));
  DEBUG_SERIAL.println(offsetof(FloidStatus, altitude));
  DEBUG_SERIAL.println(offsetof(FloidStatus, altitudeVelocity));
  //Goals:
  DEBUG_SERIAL.println("gol");
  DEBUG_SERIAL.println(offsetof(FloidStatus, hasGoal));
  DEBUG_SERIAL.println(offsetof(FloidStatus, goalTicks));
  DEBUG_SERIAL.println(offsetof(FloidStatus, goalStartTime));
  DEBUG_SERIAL.println(offsetof(FloidStatus, goalId));
  DEBUG_SERIAL.println(offsetof(FloidStatus, goalState));
  DEBUG_SERIAL.println(offsetof(FloidStatus, goalTargetState));
  DEBUG_SERIAL.println(offsetof(FloidStatus, goalX));
  DEBUG_SERIAL.println(offsetof(FloidStatus, goalY));
  DEBUG_SERIAL.println(offsetof(FloidStatus, goalZ));
  DEBUG_SERIAL.println(offsetof(FloidStatus, goalAngle));
  //Systems:
  DEBUG_SERIAL.println("sts");
  DEBUG_SERIAL.println(offsetof(FloidStatus, inFlight));
  DEBUG_SERIAL.println(offsetof(FloidStatus, liftOffMode));
  DEBUG_SERIAL.println(offsetof(FloidStatus, landMode));
  //ESCs:
  DEBUG_SERIAL.println("esc");
  DEBUG_SERIAL.println(offsetof(FloidStatus, escServoOn));
  DEBUG_SERIAL.println(offsetof(FloidStatus, escValues));
  DEBUG_SERIAL.println(offsetof(FloidStatus, escServoPulses));
  //Altimeter:
  DEBUG_SERIAL.println("alt");
  DEBUG_SERIAL.println(offsetof(FloidStatus, altimeterAltitude));
  DEBUG_SERIAL.println(offsetof(FloidStatus, altimeterAltitudeVelocity));
  DEBUG_SERIAL.println(offsetof(FloidStatus, altimeterAltitudeDelta));
  DEBUG_SERIAL.println(offsetof(FloidStatus, altimeterAltitudeValid));
  DEBUG_SERIAL.println(offsetof(FloidStatus, altimeterInitialized));
  DEBUG_SERIAL.println(offsetof(FloidStatus, altimeterGoodCount));
  DEBUG_SERIAL.println(offsetof(FloidStatus, altimeterLastUpdateTime));
  //Batteries and currents:
  DEBUG_SERIAL.println("bat");
  DEBUG_SERIAL.println(offsetof(FloidStatus, mainBatteryVoltage));
  DEBUG_SERIAL.println(offsetof(FloidStatus, heliBatteryVoltages));
  DEBUG_SERIAL.println(offsetof(FloidStatus, auxBatteryVoltage));
  DEBUG_SERIAL.println(offsetof(FloidStatus, currents));
  DEBUG_SERIAL.println(offsetof(FloidStatus, auxCurrent));
  //Bays, Payloads and Nano-muscles:
  DEBUG_SERIAL.println("bay");
  DEBUG_SERIAL.println(offsetof(FloidStatus, nmState));
  DEBUG_SERIAL.println(offsetof(FloidStatus, bayState));
  DEBUG_SERIAL.println(offsetof(FloidStatus, payloadGoalState));
  //Camera:
  DEBUG_SERIAL.println("cam");
  DEBUG_SERIAL.println(offsetof(FloidStatus, cameraOn));
  //Debug:
  DEBUG_SERIAL.println("dbg");
  DEBUG_SERIAL.println(offsetof(FloidStatus, debug));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugMem));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugGPS));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugPYR));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugPhysics));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugPanTilt));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugNM));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugCamera));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugDesignator));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugAUX));
  DEBUG_SERIAL.println(offsetof(FloidStatus, debugHelis));
  DEBUG_SERIAL.println("des");
  //Designator:
  DEBUG_SERIAL.println(offsetof(FloidStatus, designatorOn));
  //GPS:
  DEBUG_SERIAL.println("gps");
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsOn));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsTime));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsXDegreesDecimal));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsYDegreesDecimal));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsFixQuality));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsZMeters));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsZFeet));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsHDOP));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsSats));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsXDegreesDecimalFiltered));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsYDegreesDecimalFiltered));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsZMetersFiltered));
  DEBUG_SERIAL.println(offsetof(FloidStatus, gpsGoodCount));
  //HeliServos:
  DEBUG_SERIAL.println("hs");
  DEBUG_SERIAL.println(offsetof(FloidStatus, helisOn));
  DEBUG_SERIAL.println(offsetof(FloidStatus, heliServoOn));
  DEBUG_SERIAL.println(offsetof(FloidStatus, heliServoValues));
  //LED:
  DEBUG_SERIAL.println("led");
  DEBUG_SERIAL.println(offsetof(FloidStatus, ledOn));
  //Parachute:
  DEBUG_SERIAL.println("par");
  DEBUG_SERIAL.println(offsetof(FloidStatus, parachuteDeployed));
  //PanTilts:
  DEBUG_SERIAL.println("pt");
  DEBUG_SERIAL.println(offsetof(FloidStatus, panTiltOn));
  DEBUG_SERIAL.println(offsetof(FloidStatus, panTiltMode));
  DEBUG_SERIAL.println(offsetof(FloidStatus, panTiltTargetX));
  DEBUG_SERIAL.println(offsetof(FloidStatus, panTiltTargetY));
  DEBUG_SERIAL.println(offsetof(FloidStatus, panTiltTargetZ));
  DEBUG_SERIAL.println(offsetof(FloidStatus, panTiltServoValues));
  //Aux:
  DEBUG_SERIAL.println("aux");
  DEBUG_SERIAL.println(offsetof(FloidStatus, auxOn));
  //PYR:
  DEBUG_SERIAL.println("pyr");
  DEBUG_SERIAL.println(offsetof(FloidStatus, pyrOn));
  DEBUG_SERIAL.println(offsetof(FloidStatus, pyrMode));
  DEBUG_SERIAL.println(offsetof(FloidStatus, pyrAnalogRate));
  DEBUG_SERIAL.println(offsetof(FloidStatus, pitchVelocity));
  //  DEBUG_SERIAL.println(offsetof(FloidStatus, yawVelocity));
  DEBUG_SERIAL.println(offsetof(FloidStatus, rollVelocity));
  DEBUG_SERIAL.println(offsetof(FloidStatus, heightVelocity));
  DEBUG_SERIAL.println(offsetof(FloidStatus, headingVelocity));
  DEBUG_SERIAL.println(offsetof(FloidStatus, pyrGoodCount));
  DEBUG_SERIAL.println(offsetof(FloidStatus, pyrErrorCount));
  DEBUG_SERIAL.println("PBE");
//  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.time));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.roll));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.rollSin));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.rollCos));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.rollSmoothed));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.pitch));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.pitchSin));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.pitchCos));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.pitchSmoothed));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.heading));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.headingSin));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.headingCos));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.headingSmoothed));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.temperature));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.height));
  DEBUG_SERIAL.println(offsetof(FloidStatus, outputPYRBufferEntry.heightSmoothed));
  DEBUG_SERIAL.println("et");
  DEBUG_SERIAL.println(offsetof(FloidStatus, escType));
  DEBUG_SERIAL.println("fm");
  DEBUG_SERIAL.println(offsetof(FloidStatus, freeMemory));
  DEBUG_SERIAL.println("lta");
  DEBUG_SERIAL.println(offsetof(FloidStatus, liftOffTargetAltitude));
  DEBUG_SERIAL.println(offsetof(FloidStatus, distanceToTarget));
  DEBUG_SERIAL.println(offsetof(FloidStatus, altitudeToTarget));
  DEBUG_SERIAL.println(offsetof(FloidStatus, deltaHeadingAngleToTarget));
  DEBUG_SERIAL.println(offsetof(FloidStatus, orientationToGoal));
  DEBUG_SERIAL.println("aa");
  DEBUG_SERIAL.println(offsetof(FloidStatus, attackAngle));
  DEBUG_SERIAL.println("tg");
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetOrientationPitch));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetOrientationRoll));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetOrientationPitchDelta));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetOrientationRollDelta));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetPitchVelocity));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetRollVelocity));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetPitchVelocityDelta));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetRollVelocityDelta));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetAltitudeVelocity));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetAltitudeVelocityDelta));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetHeadingVelocity));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetHeadingVelocityDelta));
  DEBUG_SERIAL.println(offsetof(FloidStatus, targetXYVelocity));
  DEBUG_SERIAL.println("lm");
  // For land mode:
  DEBUG_SERIAL.println(offsetof(FloidStatus, landModeMinAltitudeCheckStarted));
  DEBUG_SERIAL.println(offsetof(FloidStatus, landModeMinAltitude));
  DEBUG_SERIAL.println(offsetof(FloidStatus, landModeMinAltitudeTime));
  // Follow mode:
  DEBUG_SERIAL.println(offsetof(FloidStatus, followMode));
  DEBUG_SERIAL.println("XXX");
}
void setupPacketHeader(PacketHeader* packetHeaderPtr, byte packetType, int packetSize)
{
  packetHeaderPtr->flag[0]    = '!';
  packetHeaderPtr->flag[1]    = 'F';
  packetHeaderPtr->flag[2]    = 'L';
  packetHeaderPtr->flag[3]    = 'O';
  packetHeaderPtr->flag[4]    = 'I';
  packetHeaderPtr->flag[5]    = 'D';
  packetHeaderPtr->number     = packetNumber++;
  packetHeaderPtr->type       = packetType;
  packetHeaderPtr->length     = packetSize;
  byte* packetChecksumStart   = (byte *)packetHeaderPtr + sizeof(PacketHeader);
  int   checksumLength        = packetSize - sizeof(PacketHeader);
  byte  packetCheckSum[4]     = {0, 0, 0, 0};
  for(int packetIndex = 0; packetIndex<checksumLength; ++packetIndex)
  {
    packetCheckSum[packetIndex % 4] ^= *((byte*) packetChecksumStart + packetIndex);
  }
  // Put the checksum in:
  packetHeaderPtr->checksum[0] = packetCheckSum[0];
  packetHeaderPtr->checksum[1] = packetCheckSum[1];
  packetHeaderPtr->checksum[2] = packetCheckSum[2];
  packetHeaderPtr->checksum[3] = packetCheckSum[3];
}
boolean sendPacket(byte* packet, int packetSize)
{
  acc.giveTime();  // 7/8/2015 - changed ctf - this is after the change above to add the led-flip
  acc.setNoUsbOutput(true);
  if (acc.isConnected())
  {
#ifdef PERFORM_EXTRA_ACC
      acc.giveTime();
#endif /* PERFORM_EXTRA_ACC */
    if(acc.write(packet, packetSize) == packetSize)
    {
#ifdef PERFORM_EXTRA_ACC
        acc.giveTime();
#endif /* PERFORM_EXTRA_ACC */
      acc.setNoUsbOutput(false);
      return true;
    }
    else
    {
#ifdef PERFORM_EXTRA_ACC
        acc.giveTime();
#endif /* PERFORM_EXTRA_ACC */
      acc.setNoUsbOutput(false);
      return false;
    }
  }
  else
  {
    acc.setNoUsbOutput(false);
    return false;
  }
}
void setupTestResponse()
{
  testResponse.bytes[0]          = 0;
  testResponse.bytes[1]          = -30;
  testResponse.bytes[2]          = 30;
  testResponse.ints[0]           = 0;
  testResponse.ints[1]           = -12000;
  testResponse.ints[2]           = 12000;
  testResponse.unsignedInts[0]   = 0;
  testResponse.unsignedInts[0]   = 100;
  testResponse.unsignedInts[0]   = 56000;
  testResponse.longs[0]          = 0;
  testResponse.longs[1]          = -100000;
  testResponse.longs[2]          = 100000;
  testResponse.unsignedLongs[0]  = 0;
  testResponse.unsignedLongs[1]  = 100000;
  testResponse.unsignedLongs[2]  = 400000;
}
boolean sendTestResponse()
{
  // 1. Set up the header
  setupPacketHeader(&testResponse.packetHeader, (byte)TEST_RESPONSE_PACKET, sizeof(TestResponse));
  // 2. Send the packet:
  return sendPacket((byte*)&testResponse, sizeof(TestResponse));
}
void printTestResponseOffsets()
{
  DEBUG_SERIAL.println("XXX");
  DEBUG_SERIAL.println("TR");
  DEBUG_SERIAL.println(sizeof(TestResponse));
  DEBUG_SERIAL.println("b");
  DEBUG_SERIAL.println(offsetof(TestResponse, bytes));
  DEBUG_SERIAL.println("i");
  DEBUG_SERIAL.println(offsetof(TestResponse, ints));
  DEBUG_SERIAL.println("ui");
  DEBUG_SERIAL.println(offsetof(TestResponse, unsignedInts));
  DEBUG_SERIAL.println("l");
  DEBUG_SERIAL.println(offsetof(TestResponse, longs));
  DEBUG_SERIAL.println("ul");
  DEBUG_SERIAL.println(offsetof(TestResponse, unsignedLongs));
  DEBUG_SERIAL.println("XXX");
}

void setFloidProductionMode() {floidStatus.floidRunMode = FLOID_RUN_MODE_PRODUCTION; floidStatus.floidTestMode = FLOID_TEST_MODE_OFF; }
bool isFloidProductionMode() { return (floidStatus.floidRunMode == FLOID_RUN_MODE_PRODUCTION) && (floidStatus.floidTestMode == FLOID_TEST_MODE_OFF); }

bool isTestModePhysicsNoXY() { return checkBit(floidStatus.floidTestMode, FLOID_TEST_MODE_PHYSICS_NO_XY_BIT); }
bool isTestModePhysicsNoAltitude() { return checkBit(floidStatus.floidTestMode, FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE_BIT); }
bool isTestModePhysicsNoAttackAngle() { return checkBit(floidStatus.floidTestMode, FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE_BIT); }
bool isTestModePhysicsNoHeading() { return checkBit(floidStatus.floidTestMode, FLOID_TEST_MODE_PHYSICS_NO_HEADING_BIT); }
bool isTestModePhysicsNoPitch() { return checkBit(floidStatus.floidTestMode, FLOID_TEST_MODE_PHYSICS_NO_PITCH_BIT); }
bool isTestModePhysicsNoRoll() { return checkBit(floidStatus.floidTestMode, FLOID_TEST_MODE_PHYSICS_NO_ROLL_BIT); }
bool isTestModePrintDebugPhysicsHeadings() { return checkBit(floidStatus.floidTestMode, FLOID_TEST_MODE_PRINT_DEBUG_PHYSICS_HEADINGS_BIT); }
bool isTestModeCheckPhysicsModel() { return checkBit(floidStatus.floidTestMode, FLOID_TEST_MODE_CHECK_PHYSICS_MODEL_BIT); }
bool isRunModePhysicsTestXY() { return checkBit(floidStatus.floidRunMode, FLOID_RUN_MODE_PHYSICS_TEST_XY_BIT); }
bool isRunModePhysicsTestAltitude1() { return checkBit(floidStatus.floidRunMode, FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1_BIT); }
bool isRunModePhysicsTestAltitude2() { return checkBit(floidStatus.floidRunMode, FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2_BIT); }
bool isRunModePhysicsTestHeading1() { return checkBit(floidStatus.floidRunMode, FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1_BIT); }
bool isRunModePhysicsTestPitch1() { return checkBit(floidStatus.floidRunMode, FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1_BIT); }
bool isRunModePhysicsTestRoll1() { return checkBit(floidStatus.floidRunMode, FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1_BIT); }

void setTestModePhysicsNoXY(bool on) {
  if(on) {
    setBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_XY_BIT);
  } else {
    clearBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_XY_BIT);
  }
}

void setTestModePhysicsNoAltitude(bool on) {
  if(on) {
    setBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE_BIT);
  } else {
    clearBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE_BIT);
  }
}

void setTestModePhysicsNoAttackAngle(bool on) {
  if(on) {
    setBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE_BIT);
  } else {
    clearBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE_BIT);
  }
}

void setTestModePhysicsNoHeading(bool on) {
  if(on) {
    setBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_HEADING_BIT);
  } else {
    clearBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_HEADING_BIT);
  }
}

void setTestModePhysicsNoPitch(bool on) {
  if(on) {
    setBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_PITCH_BIT);
  } else {
    clearBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_PITCH_BIT);
  }
}

void setTestModePhysicsNoRoll(bool on) {
  if(on) {
    setBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_ROLL_BIT);
  } else {
    clearBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PHYSICS_NO_ROLL_BIT);
  }
}

void setTestModePhysicsPrintDebugPhysicsHeadings(bool on) {
  if(on) {
    setBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PRINT_DEBUG_PHYSICS_HEADINGS_BIT);
  } else {
    clearBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_PRINT_DEBUG_PHYSICS_HEADINGS_BIT);
  }
}

void setTestModeCheckPhysicsModel(bool on) {
  if(on) {
    setBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_CHECK_PHYSICS_MODEL_BIT);
  } else {
    clearBit(&floidStatus.floidTestMode, (byte)FLOID_TEST_MODE_CHECK_PHYSICS_MODEL_BIT);
  }
}

void setRunModePhysicsTestXY(bool on) {
  if(on) {
    setBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_XY_BIT);
  } else {
    clearBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_XY_BIT);
  }
}

void setRunModePhysicsTestAltitude1(bool on) {
  if(on) {
    setBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1_BIT);
  } else {
    clearBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1_BIT);
  }
}

void setRunModePhysicsTestAltitude2(bool on) {
  if(on) {
    setBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2_BIT);
  } else {
    clearBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2_BIT);
  }
}

void setRunModePhysicsTestHeading1(bool on) {
  if(on) {
    setBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1_BIT);
  } else {
    clearBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1_BIT);
  }
}

void setRunModePhysicsTestPitch1(bool on) {
  if(on) {
    setBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1_BIT);
  } else {
    clearBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1_BIT);
  }
}

void setRunModePhysicsTestRoll1(bool on) {
  if(on) {
    setBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1_BIT);
  }
  else {
    clearBit(&floidStatus.floidRunMode, (byte)FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1_BIT);
  }
}

// Bit utility routines:
void setBit(byte *b, byte bit) {
  (*b) |= (byte)1 << bit;
}

void clearBit(byte *b, byte bit) {
  (*b) &= ~((byte)1 << bit);
}

bool checkBit(byte b, byte bit) {
   return (bool)((b >> bit) & (byte)1);
}

void toggleBit(byte *b, byte bit) {
  (*b) ^= (byte)1 << bit;
}
