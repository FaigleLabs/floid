//--------------------------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs                  //
//  http://www.faiglelabs.com                             //
//  A free artifact from the licensed Floid Drone project //
//  All rights reserved.                                  //
//  This code is free to use or modify:                   //
//   No warranty, license or restriction                  //
//--------------------------------------------------------//
// ConvertFloat.cpp
#include <FloidBoard3.h>
// Convert float to string utility routine:
bool convertFloat(float val, char* buffer, int bufferSize, int decimalPoints)
{
//  // Formatter does not work well for small values below the decimal point number
//  if(fabs(val) < 0.001)
//  {
//    val = 0.0;
//  }
  if(isnan(val))
  {
    if(bufferSize>=4)
    {
      buffer[0] = 'N';
      buffer[1] = 'A';
      buffer[2] = 'N';
      buffer[3] = '\0';
      return true;
    }
    return false;
  }
  if(isinf(val))
  {
    if(bufferSize>=4)
    {
      buffer[0] = 'I';
      buffer[1] = 'N';
      buffer[2] = 'F';
      buffer[3] = '\0';
      return true;
    }
    return false;
  }
  int     bufferPtr  = 0;
  buffer[bufferPtr] = '\0';
  bool    sign       = (val<0);
  if(sign)
  {
    val *= (-1);
  }
  long  upper  = (long)val;
  float lower  = val - upper;
#ifdef TEST
    Serial.print("U: ");
    Serial.println(upper);
    Serial.print("L: ");
    Serial.println(lower);
#endif /* TEST */
  // Check for correction:
  if(lower >= 1.0)
  {
    lower = 0.999999999999;
  }
  if(lower < 0)
  {
    lower = 0;
  }
  // Print out sign
  if(sign)
  {
    if(bufferPtr < bufferSize -1)
    {
      buffer[bufferPtr++] = '-';
      buffer[bufferPtr]   = '\0';
    }
    else
    {
      return false;
    }
  }
  long    divisor      = 1000000000;
  bool    startedUpper = false;
  while (divisor > 0)
  {
#ifdef TEST
      Serial.print("D: ");
      Serial.println(divisor);
      Serial.print("U: ");
      Serial.println(upper);
#endif /* TEST */
    int digit = upper/divisor;
    if(digit<0)
    {
      digit = 0;
    }
    if(digit>9)
    {
      digit = 9;
    }
#ifdef TEST
      Serial.print("d: ");
      Serial.println(digit);
      Serial.print("sU: ");
      Serial.println(startedUpper);
#endif /* TEST */
    if((startedUpper || (digit !=0)) || (!startedUpper && (digit==0) && (upper==0) && (divisor == 1)))
    {
      startedUpper        = true;
      if(bufferPtr < bufferSize -1)
      {
        buffer[bufferPtr++] = (char)(digit+48);
        buffer[bufferPtr]   = '\0';
      }
      else
      {
        return false;
      }
    }
    upper    = upper - (digit*divisor);
    divisor /= 10;
  }
  if(bufferPtr < bufferSize -1)
  {
    buffer[bufferPtr++] = '.';
    buffer[bufferPtr]   = '\0';
  }
  else
  {
    return false;
  }
  long remainderTest = 1;
  for(int i=0; i<decimalPoints; ++i)
  {
    remainderTest *= 10;
  }
  for(int i=0; i<decimalPoints; ++i)
  {
#ifdef TEST
      Serial.print("i: ");
      Serial.println(i);
      Serial.print("l: ");
      Serial.println(lower);
#endif /* TEST */
    lower      = lower * 10;
    long digit = (long)lower;
    if(digit>9)
    {
      digit = 9;
    }
    if(digit<0)
    {
      digit = 0;
    }
    lower -= digit;
    if(lower > 1.0)
    {
      lower = 1.0;
    }
    if(lower < 0)
    {
      lower = 0.0;
    }
#ifdef TEST
      Serial.print("d: ");
      Serial.println(digit);
#endif /* TEST */
    if(bufferPtr < bufferSize -1)
    {
      buffer[bufferPtr++] = (char)(digit+48);
      buffer[bufferPtr]   = '\0';
    }
    else
    {
      return false;
    }
    if(((long)(lower * remainderTest)) < 1.0)
    {
      return true;  // we are done...
    }
  }
  return true;
}
