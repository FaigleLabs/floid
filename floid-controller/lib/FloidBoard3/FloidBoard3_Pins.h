//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
#ifndef	__FLOIDBOARD3_PINS_H__
#define	__FLOIDBOARD3_PINS_H__


#define             USB_RESET_PIN                       (7)
#define             NUMBER_HELICOPTERS                  (4)
#define             NUMBER_HELI_SERVOS                  (3)
#define             NUMBER_PAYLOADS                     (4)
#define             NUMBER_PANTILTS                     (4)
#define             NUMBER_PANTILT_SERVOS               (2)
// NM/Bay:
const extern int    bayReleasePins[NUMBER_PAYLOADS];
extern const int    bayReleaseAnalogNMStatusPins[NUMBER_PAYLOADS];
extern const int    bayReleaseDigitalBayStatusPins[NUMBER_PAYLOADS];
// Pan-tilt:
#define             PAN_TILT_CAMERA                      (0)
#define	            PAN_TILT_DESIGNATOR                  (1)
#define	            PAN_TILT_AUX0                        (2)
#define	            PAN_TILT_AUX1                        (3)
extern const int    panTiltPins[NUMBER_PANTILTS][NUMBER_PANTILT_SERVOS];
// Parachute:      // TODO [PARACHUTE] Parachute unimplemented
// Digital pin:
extern const int    parachuteServoPin;
// Analog pin:
extern const int    parachuteDetectPin;
// Batteries:
// Analog pins:
extern const int    mainBatteryPin;
extern const int    currentPins[NUMBER_HELICOPTERS];
extern const int    batteryPins[NUMBER_HELICOPTERS];
// Servos and ESCs:
// Digital pins:                                         L   R   P
extern const int    servoPins[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];
extern const int    escPins[NUMBER_HELICOPTERS];
extern const int    heliOnPins[NUMBER_HELICOPTERS];
extern const int    gpsOnPin;
extern const int    pyrOnPin;
extern const int    designatorOnPin;
extern const int    designatorStatusPin;
extern const int    auxOnPin;
extern const int    auxCurrentPin;
extern const int    auxBatteryPin;
// LEDs:
#define             NUMBER_LED_PINS                    (1)
#define             FLOID_LED_PIN_INDEX                (0)
// LED ORDER: Floid Droid
extern const int    ledPins[NUMBER_LED_PINS];
// I/O Ports, rates and debug  info:
#define             DEBUG_SERIAL                        Serial
#define             DEBUG_BAUD_RATE                     (115200)
#define             GPS_SERIAL                          Serial1
#define             PYR_SERIAL                          Serial2
#define             PYR_BAUD_RATE                       (38400)
#define             AUX_SERIAL                          Serial3
#define             AUX_BAUD_RATE                       (38400)
#endif /* __FLOIDBOARD3_PINS_H__*/






