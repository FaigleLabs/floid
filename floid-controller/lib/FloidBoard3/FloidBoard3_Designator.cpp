//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Designator.cpp:
// faiglelabs.com
#include <FloidBoard3.h>
// Designator logic:
boolean       designatorOn           = false;
byte          designatorPanTiltIndex = PAN_TILT_DESIGNATOR;
unsigned long designatorFireTime     = 0ul;
boolean setupDesignator()
{
  // Setup designator:
  floidStatus.designatorOn      = false;
  designatorPanTiltIndex        = PAN_TILT_DESIGNATOR;
  pinMode(designatorOnPin, OUTPUT);
  digitalWrite(designatorOnPin, LOW);
  setDesignatorTarget(0,0,0);
  return true;
}
void startDesignating()
{
  if(floidStatus.debug && floidStatus.debugDesignator)
  {
    debugPrintlnToken(FLOID_DEBUG_DESIGNATOR_ON);
  }
  digitalWrite(designatorOnPin, HIGH);
  designatorFireTime         = millis();
  floidStatus.designatorOn   = true;
  startPanTilt(designatorPanTiltIndex);
}
void loopDesignator()
{
  if(designatorOn)
  {
    // Does nothing
  }
}
void stopDesignating()
{
  if(floidStatus.debug && floidStatus.debugDesignator)
  {
    debugPrintlnToken(FLOID_DEBUG_DESIGNATOR_OFF);
  }
  digitalWrite(designatorOnPin, LOW);
  floidStatus.designatorOn  = false;
  stopPanTilt(designatorPanTiltIndex);
}
void setDesignatorTarget(float x, float y, float z)
{
  setPanTiltTargetMode(designatorPanTiltIndex, x, y, z);
}
void setDesignatorAngle(byte  pan, byte tilt)
{
  setPanTiltAngleMode(designatorPanTiltIndex, pan, tilt);
}
void setDesignatorScan(byte panMin, byte panMax, unsigned long panPeriod, byte tiltMin, byte tiltMax, unsigned long tiltPeriod)
{
  setPanTiltScanMode(designatorPanTiltIndex, panMin, panMax, panPeriod, tiltMin, tiltMax, tiltPeriod);
}
void setDesignatorPanTiltIndex(byte index)
{
  designatorPanTiltIndex = index;
}
