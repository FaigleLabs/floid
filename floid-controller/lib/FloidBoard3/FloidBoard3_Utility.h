//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Utility.h:
#ifndef	__FLOIDBOARD3_UTILITY_H__
#define	__FLOIDBOARD3_UTILITY_H__

boolean setupFloidBoard3();
void    asyncDelay(unsigned long delay);
void    loopSerialInputs();
void    resetUSB();

#endif /* __FLOIDBOARD3_UTILITY_H__ */
