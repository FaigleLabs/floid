//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_ACC.cpp
#include <FloidBoard3_ACC.h>
// ADK USB:
AndroidAccessory acc("faiglelabs",
                     "Floid",
                     "Floid Droid",
                     "7.0",
                     "faiglelabs.com",
                     "0000000012345678");
