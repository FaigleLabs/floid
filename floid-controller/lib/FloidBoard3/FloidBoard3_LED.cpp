//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_LED.cpp:
#include <FloidBoard3.h>
// LEDs:
boolean setupLEDs()
{
  for(int ledIndex=0; ledIndex<NUMBER_LED_PINS; ++ledIndex)
  {
    pinMode(ledPins[ledIndex], OUTPUT);
    digitalWrite(ledPins[ledIndex], LOW);
    floidStatus.ledOn[ledIndex]  = false;
  }
  return true;
}
void startLED(int ledIndex)
{
  digitalWrite(ledPins[ledIndex], HIGH);
  floidStatus.ledOn[ledIndex]    = true;
}
void stopLED(int ledIndex)
{
  digitalWrite(ledPins[ledIndex], LOW);
  floidStatus.ledOn[ledIndex]   = false;
}
void flipLED(int ledIndex)
{
  if(floidStatus.ledOn[ledIndex])
  {
    stopLED(ledIndex);
  }
  else
  {
    startLED(ledIndex);
  }
}
