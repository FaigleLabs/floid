//----------------------------------------//
// (c); 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Debug.h:
#ifndef	__FLOIDBOARD3_DEBUG_H__
#define	__FLOIDBOARD3_DEBUG_H__
#include "FloidBoard3_Packet.h"

#define                DEBUG_BUFFER_SIZE                   (128)
#define                DEBUG_CONVERSION_BUFFER_SIZE        (24)
class  DebugMessage
{
  public:
    PacketHeader packetHeader;
    byte         debugBuffer[DEBUG_BUFFER_SIZE];
};
extern  DebugMessage   debugMessage;
extern  unsigned int   currentDebugBufferPosition;
extern  char           debugConversionBuffer[DEBUG_CONVERSION_BUFFER_SIZE];
///////////////////////////
// DEBUG PRINT ROUTINES: //
///////////////////////////
boolean setupDebug();
boolean sendDebugMessage(int currentDebugBufferPosition);
void    displayFreeMemory(const char* tag);
void    printDebugMessageOffsets();
void    sendDebugBuffer();
void    debugAddChar(char c);
void    debugSend(const char *message);
void    debugSendln(const char *message);
void    debugPrint(const byte b);
void    debugPrintln(const byte b);
void    debugPrint(const char c);
void    debugPrintln(const char c);
void    debugPrint(const char *c);
void    debugPrintln(const char *c);
void    debugPrint(float f);
void    debugPrintln(float f);
void    debugPrint(int i);
void    debugPrintln(int i);
void    debugPrint(unsigned int ui);
void    debugPrintln(unsigned int ui);
void    debugPrint(long l);
void    debugPrintln(long l);
void    debugPrint(unsigned long ul);
void    debugPrintln(unsigned long ul);
void    debugPrintln();
void    debugPrintToken(unsigned int token);
void    debugPrintlnToken(unsigned int token);
void    debugPrintHexByte(byte hb);
void    debugPrintHexChar(byte pb);
#endif /* __FLOIDBOARD3_DEBUG_H__ */
