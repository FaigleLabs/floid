/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <AndroidAccessory.h>
#include <FloidBoard3_Debug.h>
//#define DEBUG_ANDROID_ACCESSORY
#define USB_ACCESSORY_VENDOR_ID         0x18D1
#define USB_ACCESSORY_PRODUCT_ID        0x2D00
#define USB_ACCESSORY_ADB_PRODUCT_ID    0x2D01
#define ACCESSORY_STRING_MANUFACTURER   0
#define ACCESSORY_STRING_MODEL          1
#define ACCESSORY_STRING_DESCRIPTION    2
#define ACCESSORY_STRING_VERSION        3
#define ACCESSORY_STRING_URI            4
#define ACCESSORY_STRING_SERIAL         5
#define ACCESSORY_GET_PROTOCOL          51
#define ACCESSORY_SEND_STRING           52
#define ACCESSORY_START                 53
AndroidAccessory::AndroidAccessory(const char *manufacturer,
                                   const char *model,
                                   const char *description,
                                   const char *version,
                                   const char *uri,
                                   const char *serial) : manufacturer(manufacturer),
                                                         model(model),
                                                         description(description),
                                                         version(version),
                                                         uri(uri),
                                                         serial(serial),
                                                         connected(false),
							 noUsbOutput(false)
{
}
void AndroidAccessory::powerOn(void)
{
  //  DEBUG_SERIAL.println("APO");
  max.powerOn();
  delay(200);
}
int AndroidAccessory::getProtocol(byte addr)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("AGP");
  if(!noUsbOutput)
  {
    noUsbOutput = true;
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  uint16_t protocol = -1;
  usb.ctrlReq(addr,
              0,
              USB_SETUP_DEVICE_TO_HOST | USB_SETUP_TYPE_VENDOR | USB_SETUP_RECIPIENT_DEVICE,
              ACCESSORY_GET_PROTOCOL,
              0,
              0,
              0,
              2,
             (char *)&protocol);
#ifdef DEBUG_ANDROID_ACCESSORY
  if(floidStatus.debug && floidStatus.serialOutput)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_GET_PROTOCOL);
    debugPrintln(protocol);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  return protocol;
}
void AndroidAccessory::sendString(byte addr, int index, const char *str)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("ASS");
  if(!noUsbOutput)
  {
    noUsbOutput = true;
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  usb.ctrlReq(addr, 0,
              USB_SETUP_HOST_TO_DEVICE | USB_SETUP_TYPE_VENDOR | USB_SETUP_RECIPIENT_DEVICE,
              ACCESSORY_SEND_STRING,
              0,
              0,
              index,
              strlen(str) + 1,
              (char *)str);
}
bool AndroidAccessory::switchDevice(byte addr)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("ASD");
  if(!noUsbOutput)
  {
    noUsbOutput = true;
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF);
  }
  if(floidStatus.debug)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_SWITCHING_DEVICE);
    debugPrintHexByte(addr);
    debugPrintln();
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  int protocol = getProtocol(addr);
  if (protocol >= 1) 
  {
#ifdef DEBUG_ANDROID_ACCESSORY
    if(floidStatus.debug)
    {
      debugPrintToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_PROTOCOL);
      debugPrintln(protocol);
    }
#endif /* DEBUG_ANDROID_ACCESSORY */
  }
  else
  {
#ifdef DEBUG_ANDROID_ACCESSORY
    if(floidStatus.debug)
    {
      debugPrintToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_BAD_PROTOCOL);
      debugPrintln(protocol);
    }
#endif /* DEBUG_ANDROID_ACCESSORY */
    return false;
  }
  sendString(addr, ACCESSORY_STRING_MANUFACTURER, manufacturer);
  sendString(addr, ACCESSORY_STRING_MODEL,        model);
  sendString(addr, ACCESSORY_STRING_DESCRIPTION,  description);
  sendString(addr, ACCESSORY_STRING_VERSION,      version);
  sendString(addr, ACCESSORY_STRING_URI,          uri);
  sendString(addr, ACCESSORY_STRING_SERIAL,       serial);
  usb.ctrlReq(addr,
              0,
              USB_SETUP_HOST_TO_DEVICE |
              USB_SETUP_TYPE_VENDOR |
              USB_SETUP_RECIPIENT_DEVICE,
              ACCESSORY_START,
              0,
              0,
              0,
              0,
              NULL);
#ifdef DEBUG_ANDROID_ACCESSORY
  if(floidStatus.debug)
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_START);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  while (usb.getUsbTaskState() != USB_DETACHED_SUBSTATE_WAIT_FOR_DEVICE)
  {
    max.Task();
    usb.Task();
  }
#ifdef DEBUG_ANDROID_ACCESSORY
  if(floidStatus.debug)
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_USB_WAITING_FOR_DEVICE);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  return true;
}
// Finds the first bulk IN and bulk OUT endpoints
bool AndroidAccessory::findEndpoints(byte addr, EP_RECORD *inEp, EP_RECORD *outEp)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("AFE");
  if(!noUsbOutput)
  {
    noUsbOutput = true;
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  int len;
  byte err;
  uint8_t *p;
  err = usb.getConfDescr(addr, 0, 4, 0, (char *)descBuff);
  if (err)
  {
#ifdef DEBUG_ANDROID_ACCESSORY
    if(floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_CONFIG_DESCRIPTOR_ERROR);
    }
#endif /* DEBUG_ANDROID_ACCESSORY */
    return false;
  }
  len = descBuff[2] | ((int)descBuff[3] << 8);
  if (len > (int)sizeof(descBuff))
  {
#ifdef DEBUG_ANDROID_ACCESSORY
    if(floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_DESCRIPTOR_TOO_LARGE_ERROR);
    }
#endif /* DEBUG_ANDROID_ACCESSORY */
    return false;
  }
  err = usb.getConfDescr(addr, 0, len, 0, (char *)descBuff);
  if (err)
  {
#ifdef DEBUG_ANDROID_ACCESSORY
    if(floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_CONFIG_DESCRIPTOR_2_ERROR);
    }
#endif /* DEBUG_ANDROID_ACCESSORY */
    return false;
  }
  p = descBuff;
  inEp->epAddr = 0;
  outEp->epAddr = 0;
  while (p < (descBuff + len))
  {
    uint8_t descLen = p[0];
    uint8_t descType = p[1];
    USB_ENDPOINT_DESCRIPTOR *epDesc;
    EP_RECORD *ep;
    switch (descType)
    {
      case USB_DESCRIPTOR_CONFIGURATION:
      {
#ifdef DEBUG_ANDROID_ACCESSORY
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_DESCRIPTOR_CONFIGURATION);
        }
#endif /* DEBUG_ANDROID_ACCESSORY */
      }
      break;
      case USB_DESCRIPTOR_INTERFACE:
      {
#ifdef DEBUG_ANDROID_ACCESSORY
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_DESCRIPTOR_INTERFACE);
        }
#endif /* DEBUG_ANDROID_ACCESSORY */
      }
      break;
      case USB_DESCRIPTOR_ENDPOINT:
      {
         epDesc = (USB_ENDPOINT_DESCRIPTOR *)p;
         if (!inEp->epAddr && (epDesc->bEndpointAddress & 0x80))
         {
           ep = inEp;
         }
         else if (!outEp->epAddr)
         {
            ep = outEp;
         }
         else
         {
            ep = NULL;
         }
         if (ep)
         {
           ep->epAddr = epDesc->bEndpointAddress & 0x7f;
           ep->Attr = epDesc->bmAttributes;
           ep->MaxPktSize = epDesc->wMaxPacketSize;
           ep->sndToggle = bmSNDTOG0;
           ep->rcvToggle = bmRCVTOG0;
         }
      }
      break;
      default:
      {
#ifdef DEBUG_ANDROID_ACCESSORY
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_DESCRIPTOR_UNKNOWN);
          debugPrintHexByte(descType);
        }
#endif /* DEBUG_ANDROID_ACCESSORY */
      }
      break;
    }
    p += descLen;
  }
  if (!(inEp->epAddr && outEp->epAddr))
  {
#ifdef DEBUG_ANDROID_ACCESSORY
    if(floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_FIND_ENDPOINTS_NO_ENDPOINTS_ERROR);
    }
#endif /* DEBUG_ANDROID_ACCESSORY */
  }
  return inEp->epAddr && outEp->epAddr;
}
bool AndroidAccessory::configureAndroid(void)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("ACA");
  if(!noUsbOutput)
  {
    noUsbOutput = true;
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  byte err;
  EP_RECORD inEp, outEp;
  if (!findEndpoints(1, &inEp, &outEp))
  {
      return false;
  }
  memset(&epRecord, 0x0, sizeof(epRecord));
  epRecord[inEp.epAddr] = inEp;
  if (outEp.epAddr != inEp.epAddr)
      epRecord[outEp.epAddr] = outEp;
  in = inEp.epAddr;
  out = outEp.epAddr;
#ifdef DEBUG_ANDROID_ACCESSORY
  if(floidStatus.debug)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_CONFIGURE_ANDROID_IN_EP);
    debugPrintHexByte(inEp.epAddr);
    debugPrintln();
    debugPrintToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_CONFIGURE_ANDROID_OUT_EP);
    debugPrintHexByte(outEp.epAddr);
    debugPrintln();
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  epRecord[0] = *(usb.getDevTableEntry(0,0));
  usb.setDevTableEntry(1, epRecord);
  err = usb.setConf( 1, 0, 1 );
  if (err)
  {
#ifdef DEBUG_ANDROID_ACCESSORY
    if(floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_CONFIGURE_ANDROID_CANT_CHOOSE_CONFIG_ONE_ERROR);
    }
#endif /* DEBUG_ANDROID_ACCESSORY */
    return false;
  }
  usb.setUsbTaskState( USB_STATE_RUNNING );
  return true;
}
bool AndroidAccessory::isConnected(void)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("AIC");
  if(!noUsbOutput)
  {
    noUsbOutput = true;
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  USB_DEVICE_DESCRIPTOR *devDesc = (USB_DEVICE_DESCRIPTOR *) descBuff;
  byte err;
  max.Task();
  usb.Task();
  if (!connected &&
      usb.getUsbTaskState() >= USB_STATE_CONFIGURING &&
      usb.getUsbTaskState() != USB_STATE_RUNNING)
  {
#ifdef DEBUG_ANDROID_ACCESSORY
    if(floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_READING_DEVICE_DESCRIPTOR);
    }
#endif /* DEBUG_ANDROID_ACCESSORY */
    err = usb.getDevDescr(1, 0, 0x12, (char *) devDesc);
    if (err)
    {
#ifdef DEBUG_ANDROID_ACCESSORY
      if(floidStatus.debug)
      {
        debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_READING_DEVICE_DESCRIPTOR_ERROR);
      }
#endif /* DEBUG_ANDROID_ACCESSORY */
      return false;
    }
    if (isAccessoryDevice(devDesc))
    {
#ifdef DEBUG_ANDROID_ACCESSORY
      if(floidStatus.debug)
      {
        debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_FOUND_ACCESSORY_DEVICE);
      }
#endif /* DEBUG_ANDROID_ACCESSORY */
      connected = configureAndroid();
    }
    else
    {
#ifdef DEBUG_ANDROID_ACCESSORY
      if(floidStatus.debug)
      {
        debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_FOUND_POSSIBLE_DEVICE_SWITCHING_TO_SERIAL_MODE);
      }
#endif /* DEBUG_ANDROID_ACCESSORY */
      switchDevice(1);
    }
  }
  else if (usb.getUsbTaskState() == USB_DETACHED_SUBSTATE_WAIT_FOR_DEVICE)
  {
    if (connected)
    {
#ifdef DEBUG_ANDROID_ACCESSORY
      if(floidStatus.debug)
      {
        debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_IS_CONNECTED_WAITING_FOR_DEVICE);
      }
#endif /* DEBUG_ANDROID_ACCESSORY */
    }
    connected = false;
  }
  return connected;
}
void AndroidAccessory::giveTime(void)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("AGT");
  if(!noUsbOutput)
  {
    noUsbOutput = true;
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  max.Task();
  usb.Task();
}
int AndroidAccessory::read(void *buff, int len, unsigned int nakLimit)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("ARD");
  if(!noUsbOutput)
  {
    noUsbOutput = true;
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  return usb.newInTransfer(1, in, len, (char *)buff, nakLimit);
}
int AndroidAccessory::write(void *buff, int len)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("AWR");
  if(!noUsbOutput)
  {
    noUsbOutput = true;
    debugPrintlnToken(FLOID_DEBUG_LABEL_ANDROID_ACCESSORY_LOGIC_ERROR_USB_OUTPUT_SHOULD_BE_OFF);
  }
#endif /* DEBUG_ANDROID_ACCESSORY */
  usb.outTransfer(1, out, len, (char *)buff);
  return len;
}
void AndroidAccessory::setNoUsbOutput(boolean iNoUsbOutput)
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.print("ANO ");
  //  DEBUG_SERIAL.println(iNoUsbOutput);
#endif /* DEBUG_ANDROID_ACCESSORY */
  noUsbOutput = iNoUsbOutput;
}
bool AndroidAccessory::getNoUsbOutput()
{
#ifdef DEBUG_ANDROID_ACCESSORY
  //  DEBUG_SERIAL.println("AGN");
#endif /* DEBUG_ANDROID_ACCESSORY */
  return noUsbOutput;
}
