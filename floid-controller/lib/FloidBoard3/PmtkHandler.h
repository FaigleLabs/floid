//--------------------------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs                  //
//  http://www.faiglelabs.com                             //
//  A free artifact from the licensed Floid Drone project //
//  All rights reserved.                                  //
//  This code is free to use or modify except this header //
//    Otherwise no warranty, license or restriction       //
//--------------------------------------------------------//
// PMTKHandler.h
/*
  PMTKHandler - Arduino library for the MediaTex MTK protocol from GPS units.
  Provides routines for sending and decoding "PMTK" packets. Call pmtkSend commands
  for control and query. Call pmtkReset to reset the state machine and then feed
  bytes from the GPS into pmtkDecode and it will decode and alter the state machine
  which is then queried to retrieve results.
*/

#ifndef __PMTK_HANDLER__H
#define __PMTK_HANDLER__H
/*
 *************************************************************************************************************************************************************************
===========================
 PMTK PROTOCOL DESCRIPTION
===========================
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Resources:
// 1. ONLINE PACKET CHECKSUM CALCULATOR: http://www.hhhh.org/wiml/proj/nmeaxor.html
// 2. PMTK - Best resource: http://www.u-blox.com/images/downloads/Product_Docs/NMEA-CommandManual_%28FTX-HW-13002%29.pdf
// 3. PMTK - https://www.olimex.com/Products/Modules/GPS/MOD-GPS/resources/MOD-GPS-MTK-NMEA.pdf
// 4. PMTK - https://www.adafruit.com/datasheets/PMTK_A08.pdf
// 5. PMTK - https://www.sparkfun.com/datasheets/GPS/Modules/PMTK_Protocol.pdf
// 6. PMTK - http://www.robotshop.com/media/files/PDF/pmtk-protocol-reference-gps-08975.pdf
// 7. PMTK-182: Logging: http://www.rigacci.org/wiki/lib/exe/fetch.php/doc/appunti/hardware/gps/mtk_logger_library_user_manual_1.2_tsi.pdf
// 8. PMTK More: http://www.orcam.eu/res/default/orcamnmeamessagesummaryv2.pdf
// 9. PMTK More: http://websvn.arcao.com/mobilemtk/trunk/src/com/arcao/mobilemtk/pages/HardwarePage.java
// 10. PMTK More: http://api.ning.com/files/6rjQHm9GnYpkElsL5lMBRzmE-5PAnoow*Xxfj4a5pEfaoAgsmZEYUgB3-Ve*b2O9Wve-xbJn3DC*h6nh0l9apEUlYz8HcP9I/MTKNMEAPROPRIETARYCOMMAND.pdf
// 11. This one is pretty complete: www.adaptivemodules.com/assets/telit/Telit-Jupiter-SL871-SL869-V2-Families-Software-User-Guide.pdfc
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Minimum Sentence:
// $PMTK###*CC<\r><\n>
// Note: We will not process the
CMD NAME:                               DIRECTION:      FORMAT:                     DATA/DESCRIPTION:
--- ----------------------------------- --------------- -----------------           -----------------------------------------------------------------------------
000 TEST                                TO GPS          000
001 ACK                                 FROM GPS        001,Cmd,Flag[,Data]         Flag:   0-invalid
                                                                                            1-unsupported
                                                                                            2-valid but fail
                                                                                            3-valid and success
                                                                                    Data:   Normally empty, however e.g. Command 661 returns a 32-bit hex number in Data
010 SYS_MESSAGE                         FROM GPS        010,Msg                     Msg:    0-Unknown
                                                                                            1-Startup
                                                                                            2-Host Aiding EPO
                                                                                            3-Normal Mode Transition Done
011 SYS_TEXT                            FROM GPS        011,System                  e.g. "MTKGPS"
101 CMD_HOT_START                       TO GPS          101                         Use all data in NV store
102 CMD_WARM_START                      TO GPS          102                         Don't use ephemeris at start
103 CMD_COLD_START                      TO GPS          103                         Don't use time, position, almanacs, ephemeris data at start
104 CMD_FULL_COLD_START                 TO GPS          104                         Cold Start + Clear system + user configs
120 CMD_CLEAR_FLASH                     TO GPS          120
127 CMD_CLEAR_EPO                       TO GPS          127,Data                    Data:   0-Clear
161 CMD_STANDBY_MODE                    TO GPS          161,Ctrl                    Ctrl:   0-Go to stop mode
                                                                                            1-Go to sleep mode
182 CMD_LOG                             TO GPS          182,[LOTS OF OPTIONS - See #7]
183 CMD_QUERY_LOCUS_LOGGER_STATUS       TO GPS          183                         Returns: $PMTKLOG,Serial#,Type,Mode,Content,Interval,
                                                                                                      Distance, Speed,Status,Number,Percent
184 CMD_ERASE_LOCUS_LOGGER_FLASH        TO GPS          184
185 CMD_START_STOP_LOCUS_LOGGER         TO GPS          185,Data                    Data:   0-Stop Logging
                                                                                            1-Start Logging
186 CMD_LOCUS_LOG_NOW                   TO GPS          186                         Note: In test received 'Unsupported' response for LS23060
====================
220 SET_OUTPUT_RATE                     TO GPS          220,Interval                Interval: in millis (>=200?? - I think 100 might be OK?)
223 SET_ALWAYS_LOCATE_DEFAULT_CONFIG    TO GPS          223,SV,SNR,                 SV:     1        (1 - 4)
                                                            ExtensionThreshold,     SNR:    30       (25 - 30)
                                                            ExtensionGap            ET:     180000   (40000 - 180000)
                                                                                    EG:     60000    (0 - 3600000)
225 SET_PERIOD_MODE                     TO GPS          225,Type,RunTime,           Type:   0-Back to Normal
                                                            SleepTime,SecondRT,             1-Periodic Backup Mode
                                                            SecondST                        2-Periodic Standby Mode
                                                                                            3-Perpetual Backup Mode
                                                                                            8-AlwaysLocate Standby Mode
                                                                                            9-AlwaysLocate Backup Mode
                                                                                    RT:     0 - Disable  Enabled: (1000-518400000)
                                                                                    ST:     (1000-518400000)
                                                                                    SRT:    0 - Disable  Enabled: (1000-518400000) (NOTE: SRT must be > RT if >0)
                                                                                    SST:    0 - Disable  Enabled: (1000-518400000)
251 SET_NMEA_BAUDRATE                   TO GPS          251,Baud                    Baud:   0,4800,9600,14400,19200,38400,57600,115200
286 SET_ENABLE_AIC                      TO GPS          286,Enable                  Enable: 0-Disable
                                                                                            1-Enable
291 SET_BACKUP_MODE                     TO GPS          291,Type,0,RTCWakup,Mode    ** Not implemented at all here - this is just a doc note
====================
300 SET_FIX_INTERVAL                    TO GPS          300,Interval,0,0,0,0        Interval: 100-10000
301 SET_DGPS_MODE                       TO GPS          301,Source                  Source: 0-None
                                                                                            1-RTCM
                                                                                            2-WAAS
313 SET_SBAS_ENABLED                    TO GPS          313,Enable                  Enable: 0-Disable
                                                                                            1-Enable
314 SET_NMEA_OUTPUT                     TO GPS          314,[19 Flags]              Each:   0-Disable  
                                                                                            1-Once per fix, [etc: 2,3,4], up to 5-Once per five fixes
                                                                                            Note: Special format restores defaults - use a single '-1' eg: $PMTK314,-1*04
                                                                                    Flags:  GLL, RMC, VTG, GGA, GSA, GSV, GRS, GST,[5 undocumented sentences],
                                                                                            MALM, MEPH, MDGP, MDBG, ZDA, MCHN
                                                                                           0 NMEA_SEN_GLL,  // GPGLL interval - Geographic Position - Latitude longitude 
                                                                                           1 NMEA_SEN_RMC,  // GPRMC interval - Recomended Minimum Specific GNSS Sentence 
                                                                                           2 NMEA_SEN_VTG,  // GPVTG interval - Course Over Ground and Ground Speed 
                                                                                           3 NMEA_SEN_GGA,  // GPGGA interval - GPS Fix Data 
                                                                                           4 NMEA_SEN_GSA,  // GPGSA interval - GNSS DOPS and Active Satellites 
                                                                                           5 NMEA_SEN_GSV,  // GPGSV interval - GNSS Satellites in View 
                                                                                           6 NMEA_SEN_GRS,  // GPGRS interval - GNSS Range Residuals 
                                                                                           7 NMEA_SEN_GST,  // GPGST interval - GNSS Pseudorange Erros Statistics 
                                                                                          13 NMEA_SEN_MALM, // PMTKALM interval - GPS almanac information 
                                                                                          14 NMEA_SEN_MEPH, // PMTKEPH interval - GPS ephmeris information 
                                                                                          15 NMEA_SEN_MDGP, // PMTKDGP interval - GPS differential correction information 
                                                                                          16 NMEA_SEN_MDBG, // PMTKDBG interval – MTK debug information 
                                                                                          17 NMEA_SEN_ZDA,  // GPZDA interval – Time & Date 
                                                                                          18 NMEA_SEN_MCHN, // PMTKCHN interval – GPS channel status 
319 SET_SBAS_IN_TEST                    TO GPS          319,Disable                 Disable: 0-Enable
                                                                                             1-Disable
320 SET_PWR_SAVE_MODE                   TO GPS          320,Mode                    Mode:   0-PwrSave Disbled 1-PwrSave Enabled
330 SET_DATUM                           TO GPS          330,Datum                   Datum:  0-WGS84
                                                                                            1-Tokyo-M (etc - 219 Datums total)
331 SET_DATUM_ADVANCED                  TO GPS          331,MajorAxis,Eccentric,    See Docs #2
                                                            dX,dY,dZ
335 SET_RTC_TIME                        TO GPS          335,Year,Month,Day,         Note: gps time will resume after 60 sec  See Docs
                                                            Hour,Min,Sec
351 SET_QZSS_NMEA_OUTPUT                TO GPS          351,Enable                  Enable: 0-Disable
                                                                                            1-Enable
352 SET_STOP_QZSS                       TO GPS          352,Stop                    Enable: 0-Do not stop
                                                                                            1-Stop
353 SET_GNSS_SEARCH_MODE                TO GPS          353,EnableGPS,              Each:   0-Disable
                                                            EnableGLONASS                   1-Enable
355 QUERY_GPS_MODE                      TO GPS          355                         Data in ack packet as (doc #11) as:  I get unsupported LS23060  - Not Implemented
                                                                                            $PMTK001,355,3,GLON_Enable,BEIDOU_Enabled,GALILEO_Enabled
356 SET_HDOP_THRESHOLD                  TO GPS          356,Threshold               Threshold: 0-disable, else hdop threshold - Unsupported on LS23060 - Not Implemented
                                                                                            Data returned as: $PMTK356,0.8,Set OK!*5F<CR><LF>
386 SET_STATIC_NAV_THRESHOLD_333        TO GPS          386,SpeedThreshold          ST:     0-Disable  (0.1-2.0) in meters/sec
397 SET_STATIC_NAV_THRESHOLD_332        TO GPS          397,SpeedThreshold          SAME AS ABOVE
389 SET_TXCO_DEBUG                      TO GPS          389,Enable                  Enable: 0-Disable 1-Enable  I get unsupported LS23060  - Not implemented
390 SET_USER_OPTION                     TO GPS          390,Lock,Update_Rate,       See Docs #2
                                                            Baud_Rate,GLL_Period,
                                                            RMC_Period,VTG_Period,
                                                            GSA_Period,GSV_Period,
                                                            GGA_Period,ZDA_Period,
                                                            MCHN_Period,Datum,
                                                            DGPS_Mode,RTCM_Baud_Rate
399 SET_FLASH_DATE                      TO GPS          399,See doc #11             Not implemented or tested here            
====================
400 QUERY_FIX_CONTROL                   TO GPS          400
401 QUERY_DGPS_MODE                     TO GPS          401
413 QUERY_SBAS_ENABLED                  TO GPS          413
414 QUERY_NMEA_OUTPUT                   TO GPS          414
419 QUERY_SBAS_IN_TEST                  TO GPS          419
420 QUERY_PWR_SAVE_MODE                 TO GPS          420
430 QUERY_DATUM                         TO GPS          430
431 QUERY_DATUM_ADVANCED                TO GPS          431                         Note: In test recieved 'Supported But Failed' response for LS23060
447 QUERY_NAV_THRESHOLD                 TO GPS          447
486 QUERY_AIC                           TO GPS          486                         Note: In test received 'Unsupported' response for LS23060
490 QUERY_USER_OPTION                   TO GPS          490
492 QUERY_BT_MAC_ADDRESS                TO GPS          492
499 QUERY_FLASH_DATE                    TO GPS          499                         Note: I get valid but fail in testing   - Not implemented
====================
500 DATA_FIX_CONTROL                    FROM GPS        500,Interval,0,0,0.0,0.0    Interval: in Milliseconds 100-1000
501 DATA_DGPS_MODE                      FROM GPS        501,Mode                    Mode:     0-Disabled 1-RTCM 2-WAAS
513 DATA_SBAS_ENABLED                   FROM GPS        513,Enabled                 Enabled:  0-Disabled 1-Enabled
514 DATA_NMEA_OUTPUT                    FROM GPS        514,[SAME SENTENCE AS SENT TO NMEA]
519 DATA_SBAS_IN_TEST                   FROM GPS        519,Disabled                Disabled: 0-Enabled  1-Disabled
520 DATA_PWR_SAVE_MODE                  FROM GPS        520,Enabled                 Enabled: 0-PwrSave Disabled 1-PwrSave Enabled
527 DATA_NAV_THRESHOLD                  FROM GPS        527,Threshold
530 DATA_DATUM                          FROM GPS        530,Datum
531 DATA_DATUM_ADVANCED (?)             FROM GPS        531,AdvancedDatum...        See note for 431 - I receive 'Supported But Failed' so this is a guess
                                                                                    Also note that one of the docs above had the 431 return a 530 as:
                                                                                    $PMTK530,6377397.155,299.152812800,-148.0,507.0,685.0*11
586 DATA_AIC                            FROM GPS        586,Enabled                 Enabled: 0-Disabled 1-Enabled
                                                                                    - Pretty sure -see note for 486
                                                                                    - I received 'Unsupported' response for LS23060 so this is a guess if supported
589 DATA_TXCO_DRIFT_VALUE               FROM GPS        589,See details in doc #11                                            - Not Implemented
590 DATA_USER_OPTION                    FROM GPS        590,[same options as 390, except first is lock count(?)] - See doc #2
592 DATA_BT_MAC_ADDRESS                 FROM GPS        592,MacAddress              MacAddress: 12 char string - not separated
599 DATA_FLASH_DATA                     FROM GPS        599,See format in doc #11   Not implemented in this code              
====================
604 QUERY_FIRMWARE_VERSION              TO GPS          604  Note: In test received 'Unsupported' response for LS23060
605 QUERY_FIRMWARE_INFO                 TO GPS          605
607 QUERY_EPO_STATUS                    TO GPS          607,0                       0=Status
622 QUERY_LOCUS_DATA                    TO GPS          622,Type                    Type:   0-Dump full flash data
                                                                                            1-Dump in-use flash data
                                                                                    Output data:  Some number of PMTKLOX packets with start and end packets:
                                                                                        PMTKLOX packet type:
                                                                                            Type1: LOCUS start (n is the number PMTKLOX packets will be sent)
                                                                                                PMTKLOX,0,n
                                                                                            Type2: LOCUS data (data will be sent by 8-byte HEX sting, at most 24 events)
                                                                                                PMTKLOX,1,[Fields]   (“FFFFFFFF” if field empty)
                                                                                            Type3: LOCUS end
                                                                                                PMTKLOX,2
                                                                                        LOCUS Data:
                                                                                            UTC:4 bytes
                                                                                            Fix:1 byte
                                                                                            Lat:4 bytes
                                                                                            Lon:4 bytes
                                                                                            Alt:2 bytes
                                                                                            Spd:2 bytes
                                                                                            Sat:2 bytes
                                                                                            Cks:1 byte
660 QUERY_AVAILABLE_EPH                 TO GPS          660,TimeInterval                TimeInterval>0 and <=7200 (2 hours) - in seconds
661 QUERY_AVAILABLE_ALM                 TO GPS          661,TimeInterval                TimeInterval in days (Note: Data comes back as Data field in ACK packet)
667 SET_QUERY_UTC_CORRECTION            TO/FROM GPS     667 See doc #11                 Result is returned in 667  - I get unsupported LS23060 - Not implemented
====================
705 DATA_FIRMWARE_INFO                  FROM GPS        705,[VARYING NUMBER OF FIRMWARE INFO FIELDS]
707 DATA_EPO_STATUS                     FROM GPS        707,NumberEpoch,                 See Docs
                                                            FirstEpochWeek,FirstEpochTOW,
                                                            FinalEpochWeek,FinalEpochTOW,
                                                            CurrentMinEpochWeek,CurrentMinEpochTOW,
                                                            CurrentMaxEpochWeek,CurrentMaxEpochTOW
740 SET_UTC_TIME                        TO GPS          740,YYYY,MM,DD,hh,mm,ss     Year:   YYYY
                                                                                    Month:  1-12
                                                                                    Day:    1-31
                                                                                    Hour:   0-23
741 SET_INITIAL_POSITION_AND_TIME       TO GPS          741,Lat,Long,Alt,YYYY,      Lat:    (-90 - 90) in decimal degrees, six digits precision
                                                            MM,DD,hh,mm,ss          Long:   (-180, 180) in decimal degrees, six digits precision
                                                                                    All others same as 740
====================
810 SEND_TEST_ALL                       TO GPS          810,Mask,Sat                Mask (XXXX) is 2-byte hex digit (e.g. 'FFFF' or '0000')   - Not implemented
                                                                                    Bits: 0-Test Info  1-Test Acq  2-Test BitSync  3-Test Signal
                                                                                    so 0003 -> Test Info + Test Acq (note bits 4-15 reserved and also 3 requires 1 and 2)
                                                                                    Sat: Satellite id 1-20 (hex format)
                                                                                       Note - here is the result (set) I received testing the LS23060:
                                                                                           In this order: (ACK, Firmware, NMEA, Fix Rate, Text, Message)
                                                                                           $PMTK001,810,3*39
                                                                                           $PMTK705,AXN_1.30,3080,MC-1513,*0A
                                                                                           $PMTK514,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*2E
                                                                                           $PMTK500,200,0,0,0.0,0.0*29
                                                                                           $PMTK011,MTKGPS*08
                                                                                           $PMTK010,001*2E

811 SEND_TEST_STOP                      TO GPS          811                         Stop current test                                         - Not implemented
812 DATA_TEST_FINISHED                  FROM GPS        812                         The test has finished                                     - Not implemented
813 DATA_TEST_ACQ                       FROM GPS        813,Sat,Acq                 Sat:  The satellite number                                - Not implemented
                                                                                    Acq:  Acquisition time in seconds
814 DATA_TEST_BIT_SYNC                  FROM GPS        814,Sat,Time                Sat:  The satellite number                                - Not implemented
                                                                                    Time: Time to bit sync in seconds
815 DATA_TEST_SIGNAL                    FROM GPS        815,See format in doc #11                                                             - Not implemented
837 SEND_TEST_JAMMING                   TO GPS          837,1,Count                 Run jamming test for Count times  Unsupported LS23060     - Not implemented
869 SET_QUERY_ENABLE_EASY               TO/FROM GPS     869,Cmd[,Enable]            Cmd:    0-Query         (TO GPS)
                                                                                            1-Set           (TO GPS)
                                                                                            2-Query Result  (FROM GPS)
                                                                                    Enable: 0-Disable
                                                                                            1-Enable
875 SEND_ENABLE_LEAP_SECOND_CHANGE      TO/FROM GPS     875,See doc #11 for details                                   Unsupported LS23060     - Not implemented
 *************************************************************************************************************************************************************************
*/

//////////////////////////////////////
// PMTK PROTOCOL PACKET IDS:        //
//////////////////////////////////////
#define  PMTK_TEST                             ("000")
#define  PMTK_ACK                              ("001")
#define  PMTK_SYS_MESSAGE                      ("010")
#define  PMTK_SYS_TEXT                         ("011")
#define  PMTK_CMD_HOT_START                    ("101")
#define  PMTK_CMD_WARM_START                   ("102")
#define  PMTK_CMD_COLD_START                   ("103")
#define  PMTK_CMD_FULL_COLD_START              ("104")
#define  PMTK_CMD_CLEAR_FLASH                  ("120")
#define  PMTK_CMD_CLEAR_EPO                    ("127")
#define  PMTK_CMD_STANDBY_MODE                 ("161")
#define  PMTK_CMD_LOG                          ("182")
#define  PMTK_CMD_QUERY_LOCUS_LOGGER_STATUS    ("183")
#define  PMTK_CMD_ERASE_LOCUS_LOGGER_FLASH     ("184")
#define  PMTK_CMD_START_STOP_LOCUS_LOGGER      ("185")
#define  PMTK_CMD_LOCUS_LOG_NOW                ("186")
#define  PMTK_SET_OUTPUT_RATE                  ("220")
#define  PMTK_SET_OUTPUT_RATE_RESPONSE         (220)
#define  PMTK_SET_ALWAYS_LOCATE_DEFAULT_CONFIG ("223")
#define  PMTK_SET_PERIOD_MODE                  ("225")
#define  PMTK_SET_NMEA_BAUDRATE                ("251")
#define  PMTK_SET_ENABLE_AIC                   ("286")
#define  PMTK_SET_FIX_RATE                     ("300")
#define  PMTK_SET_DGPS_MODE                    ("301")
#define  PMTK_SET_SBAS_ENABLED                 ("313")
#define  PMTK_SET_NMEA_OUTPUT                  ("314")
#define  PMTK_SET_SBAS_IN_TEST                 ("319")
#define  PMTK_SET_PWR_SAVE_MODE                ("320")
#define  PMTK_SET_DATUM                        ("330")
#define  PMTK_SET_DATUM_ADVANCED               ("331")
#define  PMTK_SET_RTC_TIME                     ("335")
#define  PMTK_SET_QZSS_NMEA_OUTPUT             ("351")
#define  PMTK_SET_STOP_QZSS                    ("352")
#define  PMTK_SET_GNSS_SEARCH_MODE             ("353")
#define  PMTK_QUERY_GPS_MODE                   ("355")
// Not implemented at all:
#define  PMTK_SET_HDOP_THRESHOLD               ("356")
#define  PMTK_SET_STATIC_NAV_THRESHOLD_333     ("386")
// Not implemented at all:
#define  PMTK_SET_TXCO_DEBUG                   ("389")
#define  PMTK_SET_USER_OPTION                  ("390")
#define  PMTK_SET_STATIC_NAV_THRESHOLD_332     ("397")
// Not implemented at all:
#define  PMTK_SET_FLASH_DATE                   ("399")
#define  PMTK_QUERY_FIX_RATE                   ("400")
#define  PMTK_QUERY_DGPS_MODE                  ("401")
#define  PMTK_QUERY_SBAS_ENABLED               ("413")
#define  PMTK_QUERY_NMEA_OUTPUT                ("414")
#define  PMTK_QUERY_SBAS_IN_TEST               ("419")
#define  PMTK_QUERY_PWR_SAVE_MODE              ("420")
#define  PMTK_QUERY_DATUM                      ("430")
#define  PMTK_QUERY_DATUM_ADVANCED             ("431")
#define  PMTK_QUERY_AIC                        ("486")
#define  PMTK_QUERY_USER_OPTION                ("490")
#define  PMTK_QUERY_BT_MAC_ADDRESS             ("492")
// Not implemented at all:
#define  PMTK_QUERY_FLASH_DATE                 ("499")
#define  PMTK_DATA_FIX_CONTROL                 ("500")
#define  PMTK_DATA_DGPS_MODE                   ("501")
#define  PMTK_DATA_SBAS_ENABLED                ("513")
#define  PMTK_DATA_NMEA_OUTPUT                 ("514")
#define  PMTK_DATA_SBAS_ENABLED_IN_TEST        ("519")
#define  PMTK_DATA_PWR_SAVE_MODE               ("520")
#define  PMTK_DATA_DATUM                       ("530")
#define  PMTK_DATA_DATUM_ADVANCED              ("531")
#define  PMTK_DATA_AIC                         ("586")
// Not implemented at all:
#define  PMTK_DATA_TXCO_DRIFT_VALUE            ("589")
#define  PMTK_DATA_USER_OPTION                 ("590")
#define  PMTK_DATA_BT_MAC_ADDRESS              ("592")
// Not implemented at all:
#define  PMTK_DATA_FLASH_DATA                  ("599")
#define  PMTK_QUERY_FIRMWARE_VERSION           ("604")
#define  PMTK_QUERY_FIRMWARE_INFO              ("605")
#define  PMTK_QUERY_EPO_STATUS                 ("607")
#define  PMTK_QUERY_LOCUS_DATA                 ("622")
#define  PMTK_QUERY_AVAILABLE_EPH              ("660")
#define  PMTK_QUERY_AVAILABLE_ALM              ("661")
// Not implemented at all:
#define  PMTK_SET_QUERY_UTC_CORRECTION         ("667")
#define  PMTK_DATA_FIRMWARE_INFO               ("705")
#define  PMTK_DATA_EPO_STATUS                  ("707")
#define  PMTK_DATA_UTC_TIME                    ("740")
#define  PMTK_DATA_INITIAL_POSITION_AND_TIME   ("741")
// Not implemented at all:
#define  PMTK_SEND_TEST_ALL                    ("810")
// Not implemented at all:
#define  PMTK_SEND_TEST_STOP                   ("811")
// Not implemented at all:
#define  PMTK_DATA_TEST_FINISHED               ("812")
// Not implemented at all:
#define  PMTK_DATA_TEST_ACQ                    ("813")
// Not implemented at all:
#define  PMTK_DATA_TEST_BIT_SYNC               ("814")
// Not implemented at all:
#define  PMTK_DATA_TEST_SIGNAL                 ("815")
// Not implemented at all:
#define  PMTK_SEND_TEST_JAMMING                ("837")
#define  PMTK_SET_QUERY_ENABLE_EASY            ("869")
// Not implemented at all:
#define  PMTK_SEND_ENABLE_LEAP_SECOND_CHANGE   ("875")
#define  PMTK_LOX                              ("LOX")
//////////////////////////////////////
// PMTK ROUTINE RETURN VALUES:      //
//////////////////////////////////////
static const byte PMTK_ROUTINE_SUCCESS                  = 0;
static const byte PMTK_ROUTINE_FAIL_BUFFER_OVERRUN      = 1;
static const byte PMTK_ROUTINE_FAIL_TEMP_BUFFER_OVERRUN = 2;
static const byte PMTK_ROUTINE_FAIL_LOGIC_ERROR         = 3;
static const byte PMTK_ROUTINE_FAIL_NOT_IMPLEMENTED     = 4;
static const byte PMTK_ROUTINE_FAIL_DATA_RANGE_ERROR    = 5;
static const byte PMTK_ROUTINE_FAIL_NO_SERIAL_PORT      = 6;
//////////////////////////////////////
// PMTK INPUT PACKET OFFSETS:       //
//////////////////////////////////////
#define PMTK_INPUT_PACKET_COMMAND_START             (4)
#define PMTK_INPUT_PACKET_DATA_START                (8)
#define PMTK_INPUT_PACKET_ASTERISK_BACKSTEP         (4)
#define PMTK_ACK_RESPONSE_STATUS_OFFSET             (4)
//////////////////////////////////////
// PMTK BAUD RATES:                 //
//////////////////////////////////////
#define PMTK_BAUD_RATE_0                            (0)
#define PMTK_BAUD_RATE_4800                         (1)
#define PMTK_BAUD_RATE_9600                         (2)
#define PMTK_BAUD_RATE_14400                        (3)
#define PMTK_BAUD_RATE_19200                        (4)
#define PMTK_BAUD_RATE_38400                        (5)
#define PMTK_BAUD_RATE_57600                        (6)
#define PMTK_BAUD_RATE_115200                       (7)
#define PMTK_BAUD_RATE_NUMBER_BAUDS                 (8)
static const long pmtkBauds[PMTK_BAUD_RATE_NUMBER_BAUDS]  = {0, 4800, 9600, 14400, 19200, 38400, 57600, 115200};
//////////////////////////////////////
// PMTK DPGS MODES:                 //
//////////////////////////////////////
#define PMTK_DGPS_MODE_NONE                         (0)
#define PMTK_DGPS_MODE_RTCM                         (1)
#define PMTK_DGPS_MODE_WAAS                         (2)
//////////////////////////////////////
// PMTK PERIOD MODES:               //
//////////////////////////////////////
#define PMTK_PERIOD_BACK_TO_NORMAL                  (0)
#define PMTK_PERIOD_PERIODIC_BACKUP_MODE            (1)
#define PMTK_PERIOD_PERIODIC_STANDBY_MODE           (2)
#define PMTK_PERIOD_PERPETUAL_BACKUP_MODE           (3)
#define PMTK_PERIOD_ALWAYS_LOCATE_STANDBY_MODE      (8)
#define PMTK_PERIOD_ALWAYS_LOCATE_BACKUP_MODE       (9)
//////////////////////////////////////
// PMTK RANGE: EPH QUERY SECONDS    //
//////////////////////////////////////
#define PMTK_EPH_QUERY_MIN_SECONDS                  (1)
#define PMTK_EPH_QUERY_MAX_SECONDS                  (7200)
//////////////////////////////////////
// PMTK RANGE: STATIC NAV THRESHOLD //
//////////////////////////////////////
#define PMTK_STATIC_NAV_THRESHOLD_MIN               (0.1)
#define PMTK_STATIC_NAV_THRESHOLD_MAX               (2.0)
//////////////////////////////////////
// PMTK BUFFER SIZE AND LENGTHS:    //
//////////////////////////////////////
#define PMTK_SEND_BUFFER_LENGTH                     (200)
// $PMTK:
#define PMTK_SEND_BUFFER_HEADER_LENGTH              (5)
// *CC\r\n:
#define PMTK_SEND_BUFFER_FOOTER_LENGTH              (5)
// Size of temp buffer for printing parameters for shorter commands:
#define PMTK_TEMP_BUFFER_SIZE_1                     (15)
// Size of temp buffer for printing parameters for longer commands:
#define PMTK_TEMP_BUFFER_SIZE_2                     (100)
//////////////////////////////////////
// PMTK INPUT BUFFER AND DECODE:    //
//////////////////////////////////////
#define PMTK_INPUT_BUFFER_SIZE                      (200)
#define PMTK_STATE_WAITING_FOR_DOLLAR               (0)
#define PMTK_STATE_WAITING_FOR_NEWLINE              (1)
//////////////////////////////////////
// PMTK NMEA INDEX MASK FLAGS:      //
//////////////////////////////////////
#define PMTK_NMEA_MASK_SEN_GLL                      (0)
#define PMTK_NMEA_MASK_SEN_RMC                      (1)
#define PMTK_NMEA_MASK_SEN_VTG                      (2)
#define PMTK_NMEA_MASK_SEN_GGA                      (3)
#define PMTK_NMEA_MASK_SEN_GSA                      (4)
#define PMTK_NMEA_MASK_SEN_GSV                      (5)
#define PMTK_NMEA_MASK_SEN_GRS                      (6)
#define PMTK_NMEA_MASK_SEN_GST                      (7)
#define PMTK_NMEA_MASK_SEN_MALM                     (8)
#define PMTK_NMEA_MASK_SEN_MEPH                     (9)
#define PMTK_NMEA_MASK_SEN_MDGP                     (10)
#define PMTK_NMEA_MASK_SEN_MDBG                     (11)
#define PMTK_NMEA_MASK_SEN_ZDA                      (12)
#define PMTK_NMEA_MASK_SEN_MCHN                     (13)
#define PMTK_NMEA_NUMBER_SENTENCES                  (14)
#define PMTK_NMEA_SENTENCE_MAX_RATE_VALUE           (5)
// NMEA Sentences are split into three groups - 8,5,6 - total = 19
#define PMTK_NMEA_SENTENCES_FIRST_BATCH_SIZE        (8)
#define PMTK_NMEA_SENTENCES_SECOND_BATCH_SIZE       (5)
#define PMTK_NMEA_SENTENCES_THIRD_BATCH_SIZE        (6)
#define PMTK_NMAE_NUMBER_SENTENCES                  (PMTK_NMEA_SENTENCES_FIRST_BATCH_SIZE + PMTK_NMEA_SENTENCES_SECOND_BATCH_SIZE + PMTK_NMEA_SENTENCES_THIRD_BATCH_SIZE)
#define PMTK_NMEA_DATA_BUFFER_REQUIRED_SIZE         (PMTK_NMAE_NUMBER_SENTENCES*2-1)
//////////////////////////////////////////
// PMTK POSITION FIX & INTERVAL RANGES: //
//////////////////////////////////////////
#define PMTK_POSITION_RATE_MIN                       (100)
#define PMTK_POSITION_RATE_MAX                       (1000)
#define PMTK_FIX_RATE_MIN                            (100)
#define PMTK_FIX_RATE_MAX                            (1000)
//////////////////////////////////////
// PMTK RUN AND SLEEP TIME RANGE:   //
//////////////////////////////////////
#define PMTK_RUN_AND_SLEEP_TIME_MAX                 (518400000ul)
#define PMTK_RUN_AND_SLEEP_TIME_MIN                 (1000)
////////////////////////////////////////
// PMTK ALMANAC AVAILABITY DAY RANGE: //
////////////////////////////////////////
#define PMTK_ALMANAC_AVAILABILITY_DAYS_MIN          (1)
#define PMTK_ALMANAC_AVAILABILITY_DAYS_MAX          (7200)
////////////////////////////////////////
// PMTK BT MAC ADDRESS:               //
////////////////////////////////////////
#define PMTK_BT_MAC_ADDRESS_LENGTH                  (12)
//////////////////////////////////////
// PMTK VALIDITY FLAGS AND MASKS:   //
//////////////////////////////////////
#define PMTK_VALIDITY_MASK_NMEA_SENTENCES            (0)
#define PMTK_VALIDITY_MASK_BT_MAC_ADDRESS            (1)
#define PMTK_VALIDITY_MASK_EASY_ENABLED              (2)
#define PMTK_VALIDITY_MASK_SBAS_ENABLED              (3)
#define PMTK_VALIDITY_MASK_SBAS_IN_TEST_ENABLED      (4)
#define PMTK_VALIDITY_MASK_DATUM                     (5)
#define PMTK_VALIDITY_MASK_FIX_RATE                  (6)
#define PMTK_VALIDITY_MASK_DGPS_MODE                 (7)
#define PMTK_VALIDITY_MASK_LAST_ACK_COMMAND_RESPONSE (8)
#define PMTK_VALIDITY_MASK_OUTPUT_RATE               (9)
#define PMTK_VALIDITY_MASK_NAV_THRESHOLD             (10)
#define PMTK_VALIDITY_MASK_PWR_SAVE_MODE             (11)
#define PMTK_VALIDITY_MASK_GPS_ENABLED               (12)
#define PMTK_VALIDITY_MASK_GLONASS_ENABLED           (13)
#define PMTK_VALIDITY_MASK_LAST_RESPONSE_RECEIVED    (14)
//////////////////////////////////////
// PMTK LAST COMMAND STATUS:        //
//////////////////////////////////////
#define PMTK_LAST_COMMAND_NO_STATUS            (0)
#define PMTK_LAST_COMMAND_SUCCESS              (1)
#define PMTK_LAST_COMMAND_INVALID              (2)
#define PMTK_LAST_COMMAND_UNSUPPORTED          (3)
#define PMTK_LAST_COMMAND_VALID_BUT_FAIL       (4)
#define PMTK_LAST_COMMAND_RETURNED_BAD_DATA    (5)
#define PMTK_LAST_COMMAND_DID_NOT_RECOGNIZE    (6)
//////////////////////////////////////
// PMTK LAST RESPONSE RECEIVED:     //
//////////////////////////////////////
#define PMTK_LAST_RESPONSE_RECEIVED_NONE               (0)
#define PMTK_LAST_RESPONSE_RECEIVED_ACK                (1)
#define PMTK_LAST_RESPONSE_RECEIVED_SYS_MESSAGE        (2)
#define PMTK_LAST_RESPONSE_RECEIVED_SYS_TEXT           (3)
#define PMTK_LAST_RESPONSE_RECEIVED_FIX_RATE           (4)
#define PMTK_LAST_RESPONSE_RECEIVED_DGPS_MODE          (5)
#define PMTK_LAST_RESPONSE_RECEIVED_SBAS_ENABLED       (6)
#define PMTK_LAST_RESPONSE_RECEIVED_NMEA_OUTPUT        (7)
#define PMTK_LAST_RESPONSE_RECEIVED_DATUM              (8)
#define PMTK_LAST_RESPONSE_RECEIVED_DATUM_ADVANCED     (9)
#define PMTK_LAST_RESPONSE_RECEIVED_AIC                (10)
#define PMTK_LAST_RESPONSE_RECEIVED_BT_MAC_ADDRESS     (11)
#define PMTK_LAST_RESPONSE_RECEIVED_FIRMWARE_INFO      (12)
#define PMTK_LAST_RESPONSE_RECEIVED_EPO_STATUS         (13)
#define PMTK_LAST_RESPONSE_RECEIVED_ENABLE_EASY        (14)
#define PMTK_LAST_RESPONSE_RECEIVED_SBAS_IN_TEST       (15)
#define PMTK_LAST_RESPONSE_RECEIVED_LOX                (16)
#define PMTK_LAST_RESPONSE_RECEIVED_USER_OPTION        (17)
#define PMTK_LAST_RESPONSE_RECEIVED_PWR_SAVE_MODE      (18)
#define PMTK_LAST_RESPONSE_DID_NOT_RECOGNIZE           (19)
//////////////////////////////////////
// PMTK LAST RESPONSE RECEIVED:     //
//////////////////////////////////////
#define PMTK_DISABLED_CHAR                             ('0')
#define PMTK_ENABLED_CHAR                              ('1')
#define PMTK_DGPS_MODE_NONE_CHAR                       ('0')
#define PMTK_DGPS_MODE_RTCM_CHAR                       ('1')
#define PMTK_DGPS_MODE_WAAS_CHAR                       ('2')
#define PMTK_ACK_FLAG_INVALID                          ('0')
#define PMTK_ACK_FLAG_UNSUPPORTED                      ('1')
#define PMTK_ACK_FLAG_VALID_BUT_FAIL                   ('2')
#define PMTK_ACK_FLAG_SUCCESS                          ('3')
// PMTKNmeaSentences:
class PMTKNmeaSentences
{
  public:
    PMTKNmeaSentences();
    byte              pmtkNmeaSentences[PMTK_NMEA_NUMBER_SENTENCES];
    byte              pmtkSetNmeaSentenceRate(byte nmeaSentence, byte rate);
    void              pmtkResetNmeaSentences();
};
// PMTKHandler:
class PMTKHandler
{
  private:
    // Internal Storage:
    // Serial:
    HardwareSerial*   gpsSerial;
    HardwareSerial*   debugSerial;
    // Output:
    char              pmtkSendBuffer[PMTK_SEND_BUFFER_LENGTH];
    int               pmtkCurrentSendBufferPos;
    //  Input:
    char              pmtkInputBuffer[PMTK_INPUT_BUFFER_SIZE];
    byte              pmtkProcessingState;
    int               pmtkInputBufferPosition;
    // Internal Methods:
    // Output:
    byte              pmtkSendPacket();
    void              pmtkResetSendBuffer();
    byte              pmtkChecksumPacket();
    char              pmtkGetAsciiDigit(unsigned char d);
    byte              pmtkInsertCommand(const char *command);
    // Input:
    bool              pmtkProcessPacket();
    bool              pmtkChecksumInputBuffer();
    bool              pmtkCompareResponse(const char* command);
    // Status:
    byte              pmtkLastCommandStatus;
    int               pmtkValidityFlags;
    byte              pmtkLastResponseReceived;
    int               pmtkLastResponseReceivedValue;
    char              pmtkLastResponseReceivedName[4];
    int               pmtkLastAckCommandResponse;
    int               pmtkOutputRate;
    int               pmtkFixRate;
    float             pmtkNavThreshold;
    char              pmtkBtMacAddress[PMTK_BT_MAC_ADDRESS_LENGTH + 1];
    byte              pmtkDgpsMode;
    byte              pmtkDatum;
    bool              pmtkEasyEnabled;
    bool              pmtkSbasEnabled;
    bool              pmtkSbasEnabledInTest;
    bool              pmtkPwrSaveMode;
    bool              pmtkGpsEnabled;
    bool              pmtkGlonassEnabled;
    PMTKNmeaSentences pmtkNmeaSentences;
    void              pmtkSetValidity(byte validityMask, bool valid);
  public:
    PMTKHandler();
    PMTKHandler(HardwareSerial* gpsSerial, HardwareSerial* debugSerial);
    void              init();
    // Read, Decode and state machine:
    bool              pmtkRead();
    bool              pmtkDecode(byte b);
    void              pmtkResetStatus();
    // Utility routine to connect to GPS on various baud rates, stops at last:
    void              pmtkSetupGpsSerial(int numberBaudRatesToTry, const int* baudRatesToTry, int pmtkBaudRateToSet);
    void              pmtkConnectToGps(int baudRate);
    // Packet response and print routines:
    void              printPmtkRoutineResult(byte result);
    void              printCommandResponseStatus(byte lastCommandStatus, byte lastResponseReceived);
    void              printPmtkValidityState();
    void              processPmtkCommandResponse(bool printState);
    // Direct packet send routines:
    byte              pmtkSendSimplePacket(const char *command);
    byte              pmtkSendPacket(const char *command, const char *data);
    // Status:
    bool              pmtkCheckValidity(byte validityMask);
    byte              pmtkGetLastCommandStatus();
    byte              pmtkGetLastResponseReceived();
    int               pmtkGetLastResponseReceivedValue();
    char*             pmtkGetLastResponseReceivedName();
    int               pmtkGetLastAckCommandResponse();
    int               pmtkGetFixRate();
    int               pmtkGetOutputRate();
    float             pmtkGetNavThreshold();
    byte              pmtkGetDgpsMode();
    byte              pmtkGetDatum();
    bool              pmtkGetEasyEnabled();
    bool              pmtkGetSbasEnabled();
    bool              pmtkGetSbasEnabledInTest();
    bool              pmtkGetPwrSaveMode();
    bool              pmtkGetGpsEnabled();
    bool              pmtkGetGlonassEnabled();
    char*             pmtkGetBtMacAddress();
    PMTKNmeaSentences* pmtkGetNmeaSentences();
    // Nmea Sentences:
    void              pmtkResetNmeaSentences();
    byte              pmtkSetNmeaSentenceRateAll(byte rate);
    byte              pmtkSetNmeaSentenceRate(byte nmeaSentence, byte rate);
    // Commands:
    //    Test and Start:
    byte              pmtkSendTest();
    byte              pmtkSendHotStart();
    byte              pmtkSendWarmStart();
    byte              pmtkSendColdStart();
    byte              pmtkSendFullColdStart();
    //    Clear Flashes and Standby:
    byte              pmtkSendClearFlash();
    byte              pmtkSendClearEpo();
    byte              pmtkSendStandbyMode(bool enable);
    //    Logging:
    byte              pmtkSendQueryLocusLoggerStatus();
    byte              pmtkSendCommandLog();
    byte              pmtkSendEraseLocusLoggerFlash();
    byte              pmtkSendStartStopLocusLogger(bool start);
    byte              pmtkSendLocusLogNow();
    // Set Parameters:
    byte              pmtkSendSetOutputRate(int interval);
    byte              pmtkSendSetFixRate(int interval);
    byte              pmtkSendSetDgpsMode(byte mode);
    byte              pmtkSendSetAlwaysLocateDefaultConfig();
    byte              pmtkSendSetPeriodMode(byte mode, unsigned long runTime, unsigned long sleepTime, unsigned long secondRunTime, unsigned long secondSleepTime);
    byte              pmtkSendSetNmeaBaudRate(byte baudRate);
    byte              pmtkSendSetAic(bool enable);
    byte              pmtkSendSetDatum(byte datum);
    byte              pmtkSendSetDatumAdvanced();
    byte              pmtkSendSetRtcTime();
    byte              pmtkSendSetQzssNmeaOutput(bool enable);
    byte              pmtkSendSetStopQzss(bool stop);
    byte              pmtkSendSetGnssSearchMode(bool gps, bool glonass);
    byte              pmtkSendSetStaticNavThreshold333(float threshold);
    byte              pmtkSendSetUserOption();
    byte              pmtkSendSetStaticNavThreshold332(float threshold);
    byte              pmtkSendSetEnableEasy(bool enable);
    byte              pmtkSendSetSbasEnabled(bool enable);
    byte              pmtkSendSetSbasInTest(bool enable);
    byte              pmtkSendSetUtcTime();
    byte              pmtkSendSetInitialPositionAndTime();
    byte              pmtkSendNmeaSentences();
    byte              pmtkSendSetPwrSaveMode(bool pwrSave);
    // Queries:
    byte              pmtkSendQueryFixRate();
    byte              pmtkSendQueryDgpsMode();
    byte              pmtkSendQuerySbasEnabled();
    byte              pmtkSendQuerySbasInTest();
    byte              pmtkSendQueryNmeaOutput();
    byte              pmtkSendQueryDatum();
    byte              pmtkSendQueryDatumAdvanced();
    byte              pmtkSendQueryAic();
    byte              pmtkSendQueryUserOption();
    byte              pmtkSendQueryFirmwareInfo();
    byte              pmtkSendQueryGpsMode();
    byte              pmtkSendQueryEpoStatus();
    byte              pmtkSendQueryLocusData(bool fullDebug);
    byte              pmtkSendQueryAvailableEph(int seconds);
    byte              pmtkSendQueryAvailableAlm(int days);
    byte              pmtkSendQueryEnableEasy();
    byte              pmtkSendQueryBtMacAddress();
    byte              pmtkSendQueryFirmwareVersion();
    byte              pmtkSendQueryPwrSaveMode();
};
#endif /* __PMTK_HANDLER__H */
