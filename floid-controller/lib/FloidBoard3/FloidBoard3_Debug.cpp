//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Debug.cpp
#include <FloidBoard3.h>
#include <FloidBoard3_ACC.h>
DebugMessage   debugMessage;
char           debugConversionBuffer[DEBUG_CONVERSION_BUFFER_SIZE];
unsigned int   currentDebugBufferPosition  = 0;

// Turns off snprintf outputs:
// #define NO_SNPRINTF

// Setup:
boolean setupDebug()
{
  currentDebugBufferPosition               = 0;
  return true;
}
boolean sendDebugMessage(int currentDebugBufferPosition)
{
  if(!acc.getNoUsbOutput())
  {
    // 1. Set up the header:
    long dataSizeToSend = sizeof(DebugMessage)- (DEBUG_BUFFER_SIZE - currentDebugBufferPosition);  // We only checksum and send the data we need...
    setupPacketHeader(&debugMessage.packetHeader, (byte)DEBUG_MESSAGE_PACKET, dataSizeToSend);
    // 2. Send the packet:
    boolean sendPacketStatus = sendPacket((byte*)&debugMessage, dataSizeToSend);
    return sendPacketStatus;
  }
  return false;
}
void displayFreeMemory(const char* tag)
{
  if(floidStatus.debug && floidStatus.debugMem)
  {
    int freeMemory = get_free_memory();
    debugPrintToken(FLOID_DEBUG_MEMORY_DISPLAY);
    debugPrint(tag);
    debugPrint(FLOID_DEBUG_COMMA_SPACE);
    debugPrintln(freeMemory);
  }
}
void printDebugMessageOffsets()
{
  DEBUG_SERIAL.println("XXX");
  DEBUG_SERIAL.println("DM");
  DEBUG_SERIAL.println(sizeof(DebugMessage));
  DEBUG_SERIAL.println("db");
  DEBUG_SERIAL.println(offsetof(DebugMessage, debugBuffer));
  DEBUG_SERIAL.println("XXX");
}
void sendDebugBuffer()
{
  if(floidStatus.debug && !acc.getNoUsbOutput())
  {
    sendDebugMessage(currentDebugBufferPosition);
    currentDebugBufferPosition = 0;
  }
}
void debugAddChar(char c)
{
  // We do not absorb messages while trying to send one:
  if(floidStatus.debug && !acc.getNoUsbOutput())
  {
    debugMessage.debugBuffer[currentDebugBufferPosition++] = (byte)c;
    if((currentDebugBufferPosition == DEBUG_BUFFER_SIZE) || (c=='\n'))
    {
      sendDebugBuffer();
    }
  }
}
void debugSend(const char *message)
{
  if(floidStatus.debug && !acc.getNoUsbOutput())
  {
    //  Goal of this routine:
    //  Absorb messages until the buffer is full or until a newline is reached
    for(unsigned int i=0; i<strlen(message); ++i)
    {
      debugAddChar(message[i]);
    }
  }
}
void debugSendln(const char *message)
{
  if(floidStatus.debug && !acc.getNoUsbOutput())
  {
    debugSend(message);
    debugAddChar('\n');
  }
}
void debugPrint(const byte b)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.print(b);
    }
    if(!acc.getNoUsbOutput())
    {
      debugConversionBuffer[0] = (char) b;
      debugConversionBuffer[1] ='\0';
      debugSend(debugConversionBuffer);
    }
  }
}
void debugPrintln(const byte b)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println(b);
    }
    if(!acc.getNoUsbOutput())
    {
      debugConversionBuffer[0] = (char) b;
      debugConversionBuffer[1] = '\0';
      debugSendln(debugConversionBuffer);
    }
  }
}
void debugPrint(const char c)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.print(c);
    }
    if(!acc.getNoUsbOutput())
    {
      debugConversionBuffer[0] = (byte)c;
      debugConversionBuffer[1] = '\0';
      debugSend(debugConversionBuffer);
    }
  }
}
void debugPrintln(const char c)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println(c);
    }
    if(!acc.getNoUsbOutput())
    {
      debugConversionBuffer[0] = (byte)c;
      debugConversionBuffer[1] = '\0';
      debugSendln(debugConversionBuffer);
    }
  }
}
void debugPrint(const char *c)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.print(c);
    }
    if(!acc.getNoUsbOutput())
    {
      debugSend(c);
    }
  }
}
void debugPrintln(const char *c)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println(c);
    }
    if(!acc.getNoUsbOutput())
    {
      debugSendln(c);
    }
  }
}
void debugPrint(float f)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.print(f);
    }
    if(!acc.getNoUsbOutput())
    {
      if(convertFloat(f, debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE, 3))
      {
        debugSend(debugConversionBuffer);
      }
      else
      {
          debugSend("__");
      }
    }
  }
}
void debugPrintln(float f)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println(f);
    }
    if(!acc.getNoUsbOutput())
    {
      if(convertFloat(f, debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE, 3))
      {
        debugSendln(debugConversionBuffer);
      }
      else
      {
          debugSendln("__");
      }
    }
  }
}
void debugPrint(int i)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.print(i);
    }
    if(!acc.getNoUsbOutput())
    {
#ifndef NO_SNPRINTF
      snprintf(debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE, "%d", i);
      debugSend(debugConversionBuffer);
#endif /* NO_SNPRINTF */
    }
  }
}
void debugPrintln(int i)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println(i);
    }
    if(!acc.getNoUsbOutput())
    {
#ifndef NO_SNPRINTF
      snprintf(debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE, "%d", i);
      debugSendln(debugConversionBuffer);
#endif /* NO_SNPRINTF */
    }
  }
}
void debugPrint(unsigned int ui)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.print(ui);
    }
    if(!acc.getNoUsbOutput())
    {
#ifndef NO_SNPRINTF
      snprintf(debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE,"%u", ui);
      debugSend(debugConversionBuffer);
#endif /* NO_SNPRINTF */
    }
  }
}
void debugPrintln(unsigned int ui)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println(ui);
    }
    if(!acc.getNoUsbOutput())
    {
#ifndef NO_SNPRINTF
      snprintf(debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE, "%u", ui);
      debugSendln(debugConversionBuffer);
#endif /* NO_SNPRINTF */
    }
  }
}
void debugPrint(long l)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.print(l);
    }
    if(!acc.getNoUsbOutput())
    {
#ifndef NO_SNPRINTF
      snprintf(debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE, "%ld", l);
      debugSend(debugConversionBuffer);
#endif /* NO_SNPRINTF */
    }
  }
}
void debugPrintln(long l)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println(l);
    }
    if(!acc.getNoUsbOutput())
    {
#ifndef NO_SNPRINTF
      snprintf(debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE, "%ld", l);
      debugSendln(debugConversionBuffer);
#endif /* NO_SNPRINTF */
    }
  }
}
void debugPrint(unsigned long ul)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.print(ul);
    }
    if(!acc.getNoUsbOutput())
    {
#ifndef NO_SNPRINTF
      snprintf(debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE, "%lu", ul);
      debugSend(debugConversionBuffer);
#endif /* NO_SNPRINTF */
    }
  }
}
void debugPrintln(unsigned long ul)
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println(ul);
    }
    if(!acc.getNoUsbOutput())
    {
#ifndef NO_SNPRINTF
      snprintf(debugConversionBuffer, DEBUG_CONVERSION_BUFFER_SIZE, "%lu", ul);
      debugSendln(debugConversionBuffer);
#endif /* NO_SNPRINTF */
    }
  }
}
void debugPrintln()
{
  if(floidStatus.debug)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println();
    }
    if(!acc.getNoUsbOutput())
    {
      debugSend("\n");
    }
  }
}
void debugPrintToken(unsigned int token)
{
  if(floidStatus.debug)
  {
  debugPrint('#');
  debugPrint('#');
  byte *b = (byte*)&token;
  debugPrintHexByte(b[1]);
  debugPrintHexByte(b[0]);
  }
}
void debugPrintlnToken(unsigned int token)
{
  if(floidStatus.debug)
  {
  debugPrintToken(token);
  debugPrintln();
  }
}
void debugPrintHexByte(byte hb)
{
  if(floidStatus.debug)
  {
  debugPrintHexChar((hb&0xf0)>>4);
  debugPrintHexChar((hb&0x0f));
  }
}
void debugPrintHexChar(byte pb)
{
  if(floidStatus.debug)
  {
  switch(pb)
  {
    case 0:
      debugPrint('0');
      break;
    case 1:
      debugPrint('1');
      break;
    case 2:
      debugPrint('2');
      break;
    case 3:
      debugPrint('3');
      break;
    case 4:
      debugPrint('4');
      break;
    case 5:
      debugPrint('5');
      break;
    case 6:
      debugPrint('6');
      break;
    case 7:
      debugPrint('7');
      break;
    case 8:
      debugPrint('8');
      break;
    case 9:
      debugPrint('9');
      break;
    case 10:
      debugPrint('A');
      break;
    case 11:
      debugPrint('B');
      break;
    case 12:
      debugPrint('C');
      break;
    case 13:
      debugPrint('D');
      break;
    case 14:
      debugPrint('E');
      break;
    case 15:
      debugPrint('F');
      break;
  }
  }
}
