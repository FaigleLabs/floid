//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_PanTilt.cpp:
#include <FloidBoard3.h>
// PanTilt Logic:
// TODO [PAN_TILT] CHANGE THESE ALL TO FLOATS AND ANGLES INSTEAD OF BYTES AND ANGLES:
// TODO [PAN_TILT] ALSO ABSTRACT AWAY THE SERVO AND MAKE A POSITION / ANGLE CALCULATOR:
// TODO [PAN_TILE] WILL NEED TO ADD ANGLE RANGES AND DIRECTIONAL/ORIENTATION OFFSETS FOR EACH SERVO OF EACH PAN-TILT
byte          panTiltScanPanMin[NUMBER_PANTILTS]                                = {90, 90, 90, 90};
byte          panTiltScanPanMax[NUMBER_PANTILTS]                                = {90, 90, 90, 90};
byte          panTiltScanTiltMin[NUMBER_PANTILTS]                               = {90, 90, 90, 90};
byte          panTiltScanTiltMax[NUMBER_PANTILTS]                               = {90, 90, 90, 90};
unsigned long panTiltScanPanPeriod[NUMBER_PANTILTS]                             = {1000ul, 1000ul, 1000ul, 1000ul};
unsigned long panTiltScanTiltPeriod[NUMBER_PANTILTS]                            = {1000ul, 1000ul, 1000ul, 1000ul};
byte          panTiltScanPanCurrent[NUMBER_PANTILTS]                            = {90, 90, 90, 90};
byte          panTiltScanTiltCurrent[NUMBER_PANTILTS]                           = {90, 90, 90, 90};
byte          panTiltScanPanStep[NUMBER_PANTILTS]                               = {1, 1, 1, 1};
byte          panTiltScanTiltStep[NUMBER_PANTILTS]                              = {1, 1, 1, 1};
boolean       panTiltServoDeployed[NUMBER_PANTILTS]                             = {false, false, false, false};

// TODO [PAN_TILT] IMPLEMENT THESE CORRECTLY ABSTRACTING THE SERVO...
byte          panTiltServoMinValues[NUMBER_PANTILTS][NUMBER_PANTILT_SERVOS]     = {
                                                                                  {  0,   0},
                                                                                    {  0,   0},
                                                                                    {  0,   0},
                                                                                    {  0,   0}
                                                                                  };
byte          panTiltServoDefaultValues[NUMBER_PANTILTS][NUMBER_PANTILT_SERVOS] = {
                                                                                    { 90,  90},
                                                                                    { 90,  90},
                                                                                    { 90,  90},
                                                                                    { 90,  90}
                                                                                  };
byte          panTiltServoMaxValues[NUMBER_PANTILTS][NUMBER_PANTILT_SERVOS]     = {
                                                                                    {180, 180},
                                                                                    {180, 180},
                                                                                    {180, 180},
                                                                                    {180, 180}
                                                                                  };
boolean setupPanTilts()
{
  for(int panTiltIndex=0; panTiltIndex<NUMBER_PANTILTS; ++panTiltIndex)
  {
    floidStatus.panTiltOn[panTiltIndex]                       = false;
    floidStatus.panTiltMode[panTiltIndex]                     = PAN_TILT_MODE_ANGLE;
    floidStatus.panTiltServoValues[panTiltIndex][PAN_INDEX]   = panTiltServoDefaultValues[panTiltIndex][PAN_INDEX];
    floidStatus.panTiltServoValues[panTiltIndex][TILT_INDEX]  = panTiltServoDefaultValues[panTiltIndex][TILT_INDEX];
    panTiltServoDeployed[panTiltIndex]                        = false;
    attachPanTiltServos(panTiltIndex);
  }
  return true;
}
void loopPanTilts()
{
  for(int panTiltIndex = 0; panTiltIndex < NUMBER_PANTILTS; ++panTiltIndex)
  {
    if(floidStatus.panTiltOn[panTiltIndex])
    {
      switch(floidStatus.panTiltMode[panTiltIndex])
      {
        case  PAN_TILT_MODE_ANGLE:
        {
	  // Does nothing - servos are already set
        }
        break;
        case  PAN_TILT_MODE_TARGET:
        {
	  // Calculate and set our updated values:
	  calculateCurrentTargetServoValues(panTiltIndex);
	  setPanTiltServoValues(panTiltIndex);
        }
        break;
        case  PAN_TILT_MODE_SCAN:
        {
	  // Calculate and set our updated values:
	  calculateCurrentScanServoValues(panTiltIndex);
	  setPanTiltServoValues(panTiltIndex);
        }
        break;
      }
    }
  }
}
void startPanTilt(int panTiltIndex)
{
  if(floidStatus.debug && floidStatus.debugPanTilt)
  {
    debugPrintToken(FLOID_DEBUG_PAN_TILT_START);
    debugPrintln(panTiltIndex);
  }
  floidStatus.panTiltOn[panTiltIndex] = true;
  switch(floidStatus.panTiltMode[panTiltIndex])
  {
    case  PAN_TILT_MODE_ANGLE:
    {
      // Does nothing - servos are already set
      startAngleMode(panTiltIndex);
    }
    break;
    case  PAN_TILT_MODE_TARGET:
    {
      startTargetMode(panTiltIndex);
    }
    break;
    case  PAN_TILT_MODE_SCAN:
    {
      startScanMode(panTiltIndex);
    }
    break;
  }
  panTiltServoDeployed[panTiltIndex]  = true;
  floidStatus.panTiltOn[panTiltIndex] = true;  // Note: This is not spare, since it could possible get turned off in one of the mode changes...
}
void stopPanTilt(int panTiltIndex)
{
  if(floidStatus.debug && floidStatus.debugPanTilt)
  {
    debugPrintToken(FLOID_DEBUG_PAN_TILT_START);
    debugPrintln(panTiltIndex);
  }
  floidStatus.panTiltOn[panTiltIndex] = false;
}
void setPanTiltAngleMode(int panTiltIndex, byte pan, byte tilt)
{
  floidStatus.panTiltMode[panTiltIndex]                    = PAN_TILT_MODE_ANGLE;
  floidStatus.panTiltServoValues[panTiltIndex][PAN_INDEX]  = pan;
  floidStatus.panTiltServoValues[panTiltIndex][TILT_INDEX] = tilt;
  if(floidStatus.panTiltOn[panTiltIndex])
  {
    startAngleMode(panTiltIndex);
  }
}
void setPanTiltIndividual(int panTiltIndex, int panOrTilt, byte panTiltValue)
{
  floidStatus.panTiltServoValues[panTiltIndex][panOrTilt] = panTiltValue;
  startAngleMode(panTiltIndex);
}
void startAngleMode(int panTiltIndex)
{
  floidStatus.panTiltMode[panTiltIndex] = PAN_TILT_MODE_ANGLE;
  // Values are already set - all we need to do is attach if not on
  setPanTiltServoValues(panTiltIndex);
}
void attachPanTiltServos(int panTiltIndex)
{
  for(int servoIndex=0; servoIndex<NUMBER_PANTILT_SERVOS; ++servoIndex)
  {
    servoWrite(SERVO_TYPE_PAN_TILT, panTiltIndex, servoIndex, floidStatus.panTiltServoValues[panTiltIndex][servoIndex]);
    servoAttach(SERVO_TYPE_PAN_TILT, panTiltIndex, servoIndex, panTiltPins[panTiltIndex][servoIndex], 1000, 2000);  // TODO [PAN_TILT] WHAT ARE THE MAX AND MIN PULSES HERE???
  }
}
void setPanTiltServoValues(int panTiltIndex)
{
  for(int servoIndex=0; servoIndex<NUMBER_PANTILT_SERVOS; ++servoIndex)
  {
    servoWrite(SERVO_TYPE_PAN_TILT, panTiltIndex, servoIndex, floidStatus.panTiltServoValues[panTiltIndex][servoIndex]);
  }
}
void setPanTiltTargetMode(int panTiltIndex, float x, float y, float z)
{
  floidStatus.panTiltMode[panTiltIndex]    = PAN_TILT_MODE_TARGET;
  floidStatus.panTiltTargetX[panTiltIndex] = x;
  floidStatus.panTiltTargetY[panTiltIndex] = y;
  floidStatus.panTiltTargetZ[panTiltIndex] = z;
  if(floidStatus.panTiltOn[panTiltIndex])
  {
    startTargetMode(panTiltIndex);
  }
}
void startTargetMode(int panTiltIndex)
{
  floidStatus.panTiltMode[panTiltIndex] = PAN_TILT_MODE_TARGET;
  calculateCurrentTargetServoValues(panTiltIndex);
  // Values are already set - all we need to do is attach if not on
  setPanTiltServoValues(panTiltIndex);
}
void setPanTiltScanMode(int panTiltIndex, byte panMin, byte panMax, unsigned long panPeriod, byte tiltMin, byte tiltMax, unsigned long tiltPeriod)
{
  panTiltScanPanMin[panTiltIndex]         = panMin;
  panTiltScanPanMax[panTiltIndex]         = panMax;
  panTiltScanTiltMin[panTiltIndex]        = tiltMin;
  panTiltScanTiltMax[panTiltIndex]        = tiltMax;
  panTiltScanPanPeriod[panTiltIndex]      = panPeriod;
  panTiltScanTiltPeriod[panTiltIndex]     = tiltPeriod;
  startScanMode(panTiltIndex);
}
void startScanMode(int panTiltIndex)
{
  floidStatus.panTiltMode[panTiltIndex] = PAN_TILT_MODE_SCAN;
  // TODO [PAN_TILT] Calculate the starting values for the scan:
  //  float         panTiltScanPanCurrent[panTiltIndex]     
  //  float         panTiltScanTiltCurrent[panTiltIndex]    
  //  float         panTiltScanPanStep[panTiltIndex]        
  //  float         panTiltScanTiltStep[panTiltIndex]      
  setPanTiltServoValues(panTiltIndex);
}
void calculateCurrentTargetServoValues(int panTiltIndex)
{
  // TODO [PAN_TILT] FILL THIS IN - Calculate the current servo values for this pan/tilt:
  // Notes:
  //   This should be a normalization followed by project on the x-y plane, then assuming it is a 90/90 centered servo, that is just the inverse sin/cosing
  //   Make sure I convert all x and y degree value differences to meters first (What does this comment mean?)
}
void calculateCurrentScanServoValues(int panTiltIndex)
{
  // TODO [PAN_TILT] FILL THIS IN - Calculate the current servo values for this pan/tilt:
}
