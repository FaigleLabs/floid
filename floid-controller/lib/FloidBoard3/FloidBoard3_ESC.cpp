//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_ESC.cpp:
#include <FloidBoard3.h>
// ESC logic:
const int     esc_type_exceed_rc_options_array[ESC_TYPE_EXCEED_RC_NUMBER_OPTIONS] = {
                                                                                       ESC_TYPE_EXCEED_RC_BRAKE_MODE_OFF,
                                                                                       ESC_TYPE_EXCEED_RC_BATTERY_TYPE_LITHIUM,
                                                                                       ESC_TYPE_EXCEED_RC_CUTOFF_MODE_SOFT_CUT,
                                                                                       ESC_TYPE_EXCEED_RC_CUTOFF_THRESHOLD_MED,
                                                                                       ESC_TYPE_EXCEED_RC_START_MODE_SUPER_SOFT,
                                                                                       ESC_TYPE_EXCEED_RC_TIMING_MEDIUM
                                                                                    };
// ESC:
boolean setupESCs(boolean fullSetup)
{
  // Set the esc output pins:
  for(int h=0; h<NUMBER_HELICOPTERS; ++h)
  {
    pinMode(heliOnPins[h], OUTPUT);
    digitalWrite(heliOnPins[h], LOW);
  }
  for(int escIndex=0; escIndex<NUMBER_HELICOPTERS; ++escIndex)
  {
    setESCLow(escIndex);
    servoAttach(SERVO_TYPE_ESC, escIndex, 0, escPins[escIndex], floidModelParameters.floidModelESCServoMinPulseWidthValue, floidModelParameters.floidModelESCServoMaxPulseWidthValue);
    if(fullSetup)
    {
      setupESCSetup(escIndex);
    }
  }
  return true;
}
boolean setupESCSetup(int escIndex)
{
  switch(floidModelParameters.floidModelESCType)
  {
    case ESC_TYPE_EXCEED_RC:
    {
      // Setup ESC:
      floidStatus.escServoOn[escIndex]       = false;
      esc_type_exceed_rc_setup_ranges(escIndex);  // Set up the range
      for(int i=0; i<ESC_TYPE_EXCEED_RC_NUMBER_OPTIONS; ++i)
      {
        esc_type_exceed_rc_set_options(escIndex, i, esc_type_exceed_rc_options_array[i]);
      }
    }
    break;
    case ESC_TYPE_DYNAM:
    {
      // Setup ESC:
      pinMode(heliOnPins[escIndex], OUTPUT);
      digitalWrite(heliOnPins[escIndex], LOW);
      floidStatus.escServoOn[escIndex]       = false;
    }
    break;
    default:
    {
      // ERROR!!
      if(floidStatus.debugHelis)
      {
	debugPrintToken(FLOID_DEBUG_ESC_BAD_ESC_TYPE);
	debugPrint(FLOID_DEBUG_SPACE);
	debugPrintln(floidModelParameters.floidModelESCType);
      }
      return false;
    }
    break;
  }
  return true;
}
void setupESCExtra(int escIndex)
{
  switch(floidModelParameters.floidModelESCType)
  {
    case ESC_TYPE_EXCEED_RC:
    {
      // Does nothing
    }
    break;
    case ESC_TYPE_DYNAM:
    {
      // Sets the brake mode:
      esc_dynam_switch_brake_mode(escIndex);
    }
    break;
    default:
    {
      // ERROR!!
      if(floidStatus.debug && floidStatus.debugHelis)
      {
	debugPrintToken(FLOID_DEBUG_ESC_BAD_ESC_TYPE);
	debugPrint(FLOID_DEBUG_SPACE);
	debugPrintln(floidModelParameters.floidModelESCType);
      }
    }
    break;
  }
}
void startESC(int escIndex)
{
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_START);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(escIndex);
  }
  // First, we the heli-servos which will put them in a default low position for safety:
  startHeliServos(escIndex);
  // OK, now we turn on the escs:
  floidStatus.escServoOn[escIndex]        = true;
  setESCLow(escIndex);
  // Power on ESC:
  digitalWrite(heliOnPins[escIndex], HIGH);
}
void stopESC(int escIndex)
{
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_STOP);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(escIndex);
  }
  setESCLow(escIndex);
  // Power off the ESC:
  digitalWrite(heliOnPins[escIndex], LOW);
  floidStatus.escServoOn[escIndex] = false;
}
void setESCValue(int escIndex, float newESCValue)
{
  floidStatus.escValues[escIndex]      = newESCValue;
  floidStatus.escServoPulses[escIndex] = escValueToESCServoPulse(floidStatus.escValues[escIndex]);
  servoWrite(SERVO_TYPE_ESC, escIndex, 0, floidStatus.escServoPulses[escIndex]);

  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SET_VALUE);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(escIndex);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(newESCValue);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(floidStatus.escServoPulses[escIndex]);
  }
}
void setESCHigh(int escIndex)
{
  setESCValue(escIndex, floidModelParameters.floidModelESCServoDefaultHighValue);
}
void setESCLow(int escIndex)
{
  setESCValue(escIndex, floidModelParameters.floidModelESCServoDefaultLowValue);
}
//////////////////////////////
// Specialty ESC routines:  //
//////////////////////////////
// Very short additional delay between initial stick setting and turn on:
#define FLOID_ESC_ASYNC_DELAY        (200ul)
void esc_type_exceed_rc_setup_ranges(int escIndex)
{
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SET_RANGE);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(escIndex);
  }
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_STICK_HIGH);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(escIndex);
  }
  setESCHigh(escIndex);

  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_DELAY);
  }
  asyncDelay(FLOID_ESC_ASYNC_DELAY);
  // Power on ESC:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_ESC_ON);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(escIndex);
  }
  digitalWrite(heliOnPins[escIndex], HIGH);

  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_LONG_DELAY);
  }
  asyncDelay(ESC_TYPE_EXCEED_RC_ON_DELAY);
  asyncDelay(ESC_TYPE_EXCEED_RC_EXTRA_DELAY);
  // Set stick low:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_STICK_LOW);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(escIndex);
  }
  setESCLow(escIndex);

  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_LONG_DELAY);
  }
  asyncDelay(ESC_TYPE_EXCEED_RC_ON_DELAY);
  asyncDelay(ESC_TYPE_EXCEED_RC_EXTRA_DELAY);
  // Power off ESC:
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_ESC_OFF);
    debugPrintln(escIndex);
  }
  digitalWrite(heliOnPins[escIndex], LOW);
}
void esc_type_exceed_rc_set_options(int escIndex, int optionIndex, int optionValue)
{
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_OPTION_VALUE);
    debugPrint(optionIndex);
    debugPrint(FLOID_DEBUG_SPACE_EQUAL_SPACE);
    debugPrintln(optionValue);
  }
  // Attach servo and set stick high:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_STICK_HIGH);
    debugPrintln(escIndex);
  }
  setESCHigh(escIndex);

  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_DELAY);
  }
  asyncDelay(FLOID_ESC_ASYNC_DELAY);
  // Power on ESC:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_START);
    debugPrintln(escIndex);
  }
  digitalWrite(heliOnPins[escIndex], HIGH);

  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_LONG_DELAY);
  }
  asyncDelay(ESC_TYPE_EXCEED_RC_ON_DELAY);
  asyncDelay(ESC_TYPE_EXCEED_RC_EXTRA_DELAY);
  // Now wait for program mode:
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_DELAY);
  }
  asyncDelay(ESC_TYPE_EXCEED_RC_PROGRAM_MODE_DELAY);
  // Now wait for the appropriate option:
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_OPTION_DELAY);
  }
  for(int i=0; i<optionIndex; ++i)
  {
    asyncDelay(ESC_TYPE_EXCEED_RC_PROGRAM_MODE_OPTION_DELAY);
  }
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_DELAY);
  }
  // Now wait just a bit longer:
  asyncDelay(ESC_TYPE_EXCEED_RC_EXTRA_DELAY);
  // We are at our option - set stick low:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_STICK_LOW);
    debugPrintln(escIndex);
  }
  setESCLow(escIndex);

  // Wait until it enters this mode:
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_DELAY);
  }
  asyncDelay(ESC_TYPE_EXCEED_RC_MODE_DELAY);
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_VALUE_DELAY);
  }
  // Now wait for the right value:
  for(int i=0; i<optionValue; ++i)
  {
    asyncDelay(ESC_TYPE_EXCEED_RC_PROGRAM_MODE_VALUE_DELAY);
  }
  // Now wait just a little longer:
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_VALUE_DELAY);
  }
  asyncDelay(ESC_TYPE_EXCEED_RC_EXTRA_DELAY);
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_OPTION_VALUE);
    debugPrintln(optionValue);
  }
  // Now set the stick to the top:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_STICK_HIGH);
  }
  setESCHigh(escIndex);
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_DELAY);
  }
  // Wait until it enters this option is set
  asyncDelay(ESC_TYPE_EXCEED_RC_OPTION_DELAY);
  // Power off ESC:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_ESC_OFF);
    debugPrintln(escIndex);
  }
  digitalWrite(heliOnPins[escIndex], LOW);
}
void esc_dynam_switch_brake_mode(int escIndex)
{
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SWITCH_BRAKE_MODE);
    debugPrintln(escIndex);
  }
  // Attach servo and set stick high:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_SETUP_STICK_HIGH);
  }
  setESCHigh(escIndex);
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_DELAY);
  }
  asyncDelay(FLOID_ESC_ASYNC_DELAY);
  // Power on ESC:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_START);
    debugPrintln(escIndex);
  }
  digitalWrite(heliOnPins[escIndex], HIGH);
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_DELAY);
  }
  asyncDelay(ESC_TYPE_DYNAM_BRAKE_MODE_DELAY);
  asyncDelay(ESC_TYPE_DYNAM_BRAKE_MODE_EXTRA_DELAY);
  // Set stick low:
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_STICK_LOW);
  }
  setESCLow(escIndex);
  if(floidStatus.debugHelis)
  {
    debugPrintlnToken(FLOID_DEBUG_ESC_SETUP_DELAY);
  }
  asyncDelay(ESC_TYPE_DYNAM_BRAKE_MODE_EXTRA_DELAY);
  // Power off ESC:
  if(floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_ESC_STOP);
    debugPrintln(escIndex);
  }
  digitalWrite(heliOnPins[escIndex], LOW);
}
