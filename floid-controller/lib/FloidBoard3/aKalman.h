// aKalman is a straight port of JKalman (http://sourceforge.net/projects/jkalman/) - see there for licensing and docs
//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  Free                                  //
//----------------------------------------//

#ifndef aKalman__h
#define aKalman__h

/******************************************************************************
 * Includes
 ******************************************************************************/
class aKalman
{
  public:
      int     dp;
      int     mp;
      int     cp;
      float   processNoiseCovInit;
      float   measurementNoiseCovInit;
      bool    bPrintMatrices;
      bool    matricesCreated;
      

      float*  m;                                                         //              [MP,  1]                - Measurement vector
              /** predicted state (x'(k)): x(k)=A*x(k-1)Serial.print(B*u(k) */
      float*  statePre;                                                  //              [DP,  1]                - Initialized in predict
              /** corrected state (x(k)): x(k)=x'(k)Serial.print(K(k)*(z(k)-H*x'(k)) */
      float*  statePost;                                                 //              [DP,  1]                - Initialized in first measurement
              /** state transition matrix (A) */
      float*  transitionMatrix;                                          //              [DP, DP]                - Set to identity plus dx,dy,dz set to 1 from x, y, z
              /** state transition transpose matrix T(A) */
      float*  transitionMatrixTranspose;                                 //              [DP, DP]                - Used temporarily in predict
              /** control matrix (B) (it is not used if there is no control) */
      float*  controlMatrix;                                             //              [CP, DP] or null        - WE USE NULL
              /** measurement matrix (H) */
      float*  measurementMatrix;                                         //              [MP, DP]                - Set to identity in the init
              /** measurement matrix transpose T(H) */
      float*  measurementMatrixTranspose;                                //              [DP, MP]                - Used temporarily in correct
              /** process noise covariance matrix (Q) */
      float*  processNoiseCov;                                           //              [DP, DP]                - Set to processNoiseCovInit identity
              /** measurement noise covariance matrix (R) */
      float*  measurementNoiseCov;                                       //              [MP, MP]                - Set to measurementNoiseCovInit identity
              /** priori error estimate covariance matrix (P'(k)): P'(k)=A*P(k-1)*At Serial.print( Q) */
      float*  errorCovPre;                                               //              [DP, DP]                - Initialized in predict
              /** Kalman gain matrix (K(k)): K(k)=P'(k)*Ht*inv(H*P'(k)*HtSerial.print(R) */
      float*  gain;                                                      //              [DP, MP]                - Not initialized
              /** posteriori error estimate covariance matrix (P(k)): P(k)=(I-K(k)*H)*P'(k) */
      float*  errorCovPost;                                              //              [DP, DP]                - Set to identity
              /** temporary matrices */
      float*  temp1;                                                     //              [DP, DP]
      float*  temp2;                                                     //              [MP, DP]
      float*  temp3;                                                     //              [MP, MP]
      float*  temp3Inverted;                                             //              [MP, MP]                - Used temporarily in correct
      float*  temp4;                                                     //              [MP, DP]
      float*  temp5;                                                     //              [MP,  1]
      
      // Constructor:
      aKalman(int dp, int mp, int cp, float processNoiseCovInit, float measurementNoiseCovInit, bool bPrintMatrices);
      // Destructor:
      ~aKalman();
      
      // Methods:
      boolean createMatrices();
      void freeMatrices();
      boolean init();
      void predict();
      void correct();
      
      float         getM(int row, int column);
      void          setM(int row, int column, float value);
      float  getStatePre(int row, int column);
      void   setStatePre(int row, int column, float value);
      float getStatePost(int row, int column);
      void  setStatePost(int row, int column, float value);
      
      void printMatrices();        
      
};

void  zeroMatrix(                   float* matrix, int rows, int columns);
void  identityMatrix(               float* matrix, int rows, int columns);
void  identityMatrix(               float* matrix, int rows, int columns, float start);
void  identityPlusDerivativesMatrix(float* matrix, int rows, int columns);
void  gemm(float* A, float* B, float* C, float *X, int m, int n, int p, double alpha, double beta);

#endif /* aKalman__h */



