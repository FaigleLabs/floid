//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Commands.cpp:
#include <FloidBoard3.h>
#include <FloidBoard3_ACC.h>
#define         ACC_TIMEOUT_ERROR_TIME                    (100ul)
#define         ACC_TIMEOUT_RESET_COUNT                   (10)
#define         ACC_TIMEOUT_CLEAR_COUNT                   (10)
CommandResponse commandResponse;
unsigned long   lastStatusOutputTime                      = 0ul;
byte            accBuffer[FLOID_COMMAND_ACC_BUFFER_SIZE];
byte            accGoodReads                              = 0;
byte            accErrors                                 = 0;
boolean sendCommandResponse()
{
  // 1. Set up the header
  setupPacketHeader(&commandResponse.packetHeader, (byte)COMMAND_RESPONSE_PACKET, sizeof(CommandResponse));
  // 2. Send the packet:
  return sendPacket((byte*)&commandResponse, sizeof(CommandResponse));
}
boolean setupCommands()
{
  debugPrintToken(FLOID_DEBUG_LABEL_RESET_USB);
  accGoodReads = 0;
  accErrors    = 0;
  resetUSB();
  return true;
}
// Command main loop - looks for commands - sends responses..
int loopCommands()
{
  unsigned long startAccReadMillis = millis();
  int           lengthRead         = 0;
  acc.setNoUsbOutput(true);
  if(acc.isConnected())
  {
    lengthRead = acc.read(accBuffer, sizeof(accBuffer), 1);
  }
  acc.setNoUsbOutput(false);
  if((millis() - startAccReadMillis) >= ACC_TIMEOUT_ERROR_TIME)
  {
    accGoodReads = 0;
    ++accErrors;
    if(accErrors >= ACC_TIMEOUT_RESET_COUNT)
    {
      // Send a note out the serial that we are resetting the USN
      if(floidStatus.debug)
      {
        debugPrintlnToken(FLOID_DEBUG_LABEL_USB_TIMEOUT);
      }
      resetUSB();
      accGoodReads = 0;
      accErrors    = 0;
    }
  }
  else
  {
    ++accGoodReads = 0;
    if(accGoodReads >= ACC_TIMEOUT_CLEAR_COUNT)
    {
      accGoodReads = 0;
      accErrors    = 0;
    }
  }
  return lengthRead;
}
void setBooleanToByteOffset(byte* bytes, boolean b, int offset)
{
  if(b)
  {
    bytes[offset+0] = 1;
  }
  else
  {
    bytes[offset+0] = 0;
  }
}
void getBooleanFromByteOffset(byte* bytes, boolean *b, int offset)
{
  *b = (boolean)bytes[offset+0];
}
void getByteFromByteOffset(byte* bytes, byte *b, int offset)
{
  *b = bytes[offset+0];
}
void setByteToByteOffset(byte* bytes, byte b, int offset)
{
  bytes[offset+0]   = b;
}
void setIntToByteOffset(byte* bytes, int i, int offset)
{
  byte  *b        = (byte*)&i;
  bytes[offset+0] = b[1];
  bytes[offset+1] = b[0];
}
void getIntFromByteOffset(byte* bytes, int *i, int offset)
{
  byte  *b        = (byte*)i;
  b[1] = bytes[offset+0];
  b[0] = bytes[offset+1];
}
void setUnsignedLongToByteOffset(byte* bytes, unsigned long l, int offset)
{
  byte  *b         = (byte*)&l;
  bytes[offset+0]  = b[3];
  bytes[offset+1]  = b[2];
  bytes[offset+2]  = b[1];
  bytes[offset+3]  = b[0];
}
void getUnsignedLongFromByteOffset(byte* bytes, unsigned long *l, int offset)
{
  byte  *b         = (byte*)l;
  b[3] = bytes[offset+0];
  b[2] = bytes[offset+1];
  b[1] = bytes[offset+2];
  b[0] = bytes[offset+3];
}
void setLongToByteOffset(byte* bytes, long l, int offset)
{
  byte  *b         = (byte*)&l;
  bytes[offset+0]  = b[3];
  bytes[offset+1]  = b[2];
  bytes[offset+2]  = b[1];
  bytes[offset+3]  = b[0];
}
void getLongFromByteOffset(byte* bytes, long *l, int offset)
{
  byte  *b         = (byte*)l;
  b[3] = bytes[offset+0];
  b[2] = bytes[offset+1];
  b[1] = bytes[offset+2];
  b[0] = bytes[offset+3];
}
void setFloatToByteOffset(byte* bytes, float f, int offset)
{
  byte  *b        = (byte*)&f;
  bytes[offset+0] = b[0];
  bytes[offset+1] = b[1];
  bytes[offset+2] = b[2];
  bytes[offset+3] = b[3];
}
void getFloatFromByteOffset(byte* bytes, float *f, int offset)
{
  byte* b = (byte*)f;
  b[3] = bytes[offset + 0];
  b[2] = bytes[offset + 1];
  b[1] = bytes[offset + 2];
  b[0] = bytes[offset + 3];
}
void sendGoodCommandResponse(long commandNumber, int subStatus, byte* threeByteIdentifier, boolean* sentResponse)
{
  sendCommandResponse(commandNumber, COMMAND_RESPONSE_OK, subStatus, threeByteIdentifier);
  *sentResponse = true;
}
void sendErrorCommandResponse(long commandNumber, int subStatus, byte* threeByteIdentifier, boolean* sentResponse)
{
  sendCommandResponse(commandNumber, COMMAND_RESPONSE_ERROR, subStatus, threeByteIdentifier);
  *sentResponse = true;
}
void sendLogicErrorCommandResponse(long commandNumber, int subStatus, byte* threeByteIdentifier, boolean* sentResponse)
{
  sendCommandResponse(commandNumber, COMMAND_RESPONSE_LOGIC_ERROR, subStatus, threeByteIdentifier);
  *sentResponse = true;
}
void sendCommandResponse(long commandNumber, int commandStatus, int subStatus, byte* threeByteIdentifier)
{
  commandResponse.number           = commandNumber;
  commandResponse.status           = commandStatus;
  commandResponse.subStatus        = subStatus;
  commandResponse.identifier[0]    = threeByteIdentifier[0];
  commandResponse.identifier[1]    = threeByteIdentifier[1];
  commandResponse.identifier[2]    = threeByteIdentifier[2];
  commandResponse.identifier[3]    = (byte)' ';
  sendCommandResponse();
}
void printCommandResponseOffsets()
{
  DEBUG_SERIAL.println("XXX");
  DEBUG_SERIAL.println("CR");
  DEBUG_SERIAL.println(sizeof(CommandResponse));
  DEBUG_SERIAL.println("cn");
  DEBUG_SERIAL.println(offsetof(CommandResponse, number));
  DEBUG_SERIAL.println(offsetof(CommandResponse, status));
  DEBUG_SERIAL.println(offsetof(CommandResponse, subStatus));
  DEBUG_SERIAL.println(offsetof(CommandResponse, identifier));
  DEBUG_SERIAL.println("XXX");
}
