//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard_ACC.h
#ifndef	__FLOIDBOARD3_ACC_H__
#define	__FLOIDBOARD3_ACC_H__
#include <AndroidAccessory.h>

// ADK USB:
extern AndroidAccessory acc;

#endif /* __FLOIDBOARD3_ACC_H__ */
