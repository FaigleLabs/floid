//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
#include <FloidBoard3.h>
static int freeMemoryLowWaterMark = 8192;
int get_free_memory()
{
  int free_memory;
  if((int)__brkval == 0)
  {
     free_memory = ((int)&free_memory) - ((int)&__bss_end);
  }
  else
  {
    free_memory = ((int)&free_memory) - ((int)__brkval);
  }
  return free_memory;
}
void mem(void)                
{
  int currentFreeMemory = get_free_memory();
  if(currentFreeMemory < freeMemoryLowWaterMark)
  {
    freeMemoryLowWaterMark = currentFreeMemory;
    if(floidStatus.debug)
    {
      debugPrintToken(FLOID_DEBUG_MEMORY_DISPLAY);
      debugPrintln(currentFreeMemory);
    }
  }
}


