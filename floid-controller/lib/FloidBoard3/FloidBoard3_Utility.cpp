//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Utility.cpp
#include <FloidBoard3.h>
#include <FloidBoard3_ACC.h>
void asyncDelay(unsigned long delay)
{
  unsigned long targetTime  = millis() + delay;
  while(millis() < targetTime)
  {
    loopSerialInputs();
  }
}
void loopSerialInputs()
{
  // This routine is called when routines with delays want to give some time to the serial outputs only
  // This routine is not called during normal processing - instead the outputs are processed by the loop routines
  loopGPSSerial();
  loopPYRSerial();
  loopAuxSerial();
}
void resetUSB()
{
  acc.setNoUsbOutput(true);
  // Below is not defined for production use:
#ifdef FLOID_USB_POWER_OFF_AND_ON_DURING_RESET
    pinMode( MAX_RESET, OUTPUT );
    digitalWrite( MAX_RESET, HIGH );  //release MAX3421E from reset
    delay(500l);
    digitalWrite( MAX_RESET, LOW );  // reacquire MAX3421E from reset
    delay(500l);
#endif /* TEMPORARY_REMOVE_OF_USB_RESET_TO_SEE_IF_CONNECTION_MAINTAINS */
  // Does nothing:
  acc.powerOn();
  acc.setNoUsbOutput(false);
}
boolean setupFloidBoard3()
{
  boolean success = true;
  analogReference(DEFAULT);
  // OK, we are up!
  DEBUG_SERIAL.begin(DEBUG_BAUD_RATE);
  DEBUG_SERIAL.println("FB3");
  success &= setupDebug();
  if(floidStatus.debug)
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_COPYRIGHT_NOTICE);
    mem();
  }
  if(floidStatus.debug)
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_COMMANDS);
  }
  success &= setupCommands();  // Note: Also powers on ACC
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_LEDS);
  }
  success &= setupLEDs();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_BATTERIES);
  }
  success &= setupBatteries();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_CURRENTS);
  }
  success &= setupCurrents();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_GPS);
  }
  success &= setupGPS();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_PYR);
  }
  success &= setupPYR();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_ALTIMETER);
  }
  success &= setupAltimeter();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_MODEL);
  }
  success &= setupFloidModel();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_MODEL_STATUS);
  }
  success &= setupFloidModelStatus();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_HELI_SERVOS);
  }
  success &= setupHeliServos();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_ESCS);
  }
  success &= setupESCs(false);    // THIS TURNS OFF THE FULL SETUP - DECIDE IF I WANT TO PROGRAM THE CONTROLLERS ON OR OFF-LINE - CURRENTLY SET FOR OFF-LINE
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_PAN_TILTS);
  }
  success &= setupPanTilts();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_DESIGNATOR);
  }
  success &= setupDesignator();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_CAMERA);
  }
  success &= setupCamera();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_PAYLOADS);
  }
  success &= setupPayloads();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_AUX);
  }
  success &= setupAux();
  if(floidStatus.debug)
  {
    mem();
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_PARACHUTE);
  }
  success &= setupParachute();
  if(success)
  {
    if(floidStatus.debug)
    {
      mem();
      debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_SUCCESS);
    }
  }
  else
  {
    if(floidStatus.debug)
    {
      mem();
      debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_FAILURE);
    }
    // Note: WE PRINT OUT ON FAIL REGARDLESS!
    DEBUG_SERIAL.println("FB!");
  }
  return success;
}
