//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_LED.h:
#ifndef	__FLOIDBOARD3_LED_H__
#define	__FLOIDBOARD3_LED_H__
// LEDs:
boolean setupLEDs();
void    startLED(int ledIndex);
void    stopLED(int ledIndex);
void    flipLED(int ledIndex);
#endif /* __FLOIDBOARD3_LED_H__ */
