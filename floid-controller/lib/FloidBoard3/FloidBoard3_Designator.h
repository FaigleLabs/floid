//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Designator.h:
#ifndef	__FLOIDBOARD3_DESIGNATOR_H__
#define	__FLOIDBOARD3_DESIGNATOR_H__
// Designator logic:
extern unsigned long designatorFireTime;
boolean setupDesignator();
void    loopDesignator();
void    startDesignating();
void    stopDesignating();
void    setDesignatorTarget(float x, float y, float z);
void    setDesignatorAngle(byte pan, byte tilt);
void    setDesignatorScan(byte panMin, byte panMax, unsigned long panPeriod, byte tiltMin, byte tiltMax, unsigned long tiltPeriod);
void    setDesignatorPanTiltIndex(byte index);
#endif /* __FLOIDBOARD3_DESIGNATOR_H__ */
