//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_EEPROM.h:
#ifndef	__FLOIDBOARD3_EEPROM_H__
#define	__FLOIDBOARD3_EEPROM_H__
#define  FLOID_EEPROM_CLEAR_TOKEN                      (' ')
// Model Parameters:
#define  FLOID_EEPROM_MODEL_PARAMETERS_HEADER_0        ('F')
#define  FLOID_EEPROM_MODEL_PARAMETERS_HEADER_1        ('L')
#define  FLOID_EEPROM_MODEL_PARAMETERS_HEADER_2        ('M')
#define  FLOID_EEPROM_MODEL_PARAMETERS_HEADER_3        ('D')
#define  FLOID_EEPROM_MODEL_PARAMETERS_HEADER_4        ('P')
#define  FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_SIZE      (5)
#define  FLOID_EEPROM_MODEL_PARAMETERS_TOKEN_OFFSET    (0)
#define  FLOID_EEPROM_MODEL_PARAMETERS_CHECKSUM_OFFSET (5)
#define  FLOID_EEPROM_MODEL_PARAMETERS_OFFSET          (7)
// EEPROM proxy routines in main pde to avoid compiling against EEPROM library here:
extern void eepromWrite(int eepromLocation, byte eepromValue);
extern byte eepromRead(int eepromLocation);
// Local methods:
boolean clearEEPROM();
boolean loadFromEEPROM();
boolean saveToEEPROM();
void    writeEEPROMModelParametersToken();
void    writeEEPROMModelParameters();
void    writeEEPROMModelParametersChecksum();
int     readEEPROMModelParametersChecksum();
boolean checkEEPROMModelParameterToken();
boolean checkEEPROMModelParametersChecksum();
void    readEEPROMModelParameters();
int     calculateModelParametersChecksum();
#endif /* __FLOIDBOARD3_EEPROM_H__ */


