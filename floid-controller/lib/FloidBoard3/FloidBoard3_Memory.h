//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//

#ifndef	__FLOIDBOARD3_MEMORY_H__
#define	__FLOIDBOARD3_MEMORY_H__
extern unsigned int __bss_end;
extern void *__brkval;
int get_free_memory();
void mem(void);
#endif /* __FLOIDBOARD3_MEMORY_H__ */
