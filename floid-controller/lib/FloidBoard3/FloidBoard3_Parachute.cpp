//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Parachute.cpp:
#include <FloidBoard3.h>

// TODO [PARACHUTE] Parachute Unimplemented
boolean setupParachute()
{
  // TODO [PARACHUTE] setupParachute: UNIMPLEMENTED
  floidStatus.parachuteDeployed = false;
  return true;
}
void startParachute()
{
  deployParachute();
}
void loopParachute()
{
}
void stopParachute()
{
}
void deployParachute()
{
  // TODO [PARACHUTE] deployParachute: UNIMPLEMENTED
  floidStatus.parachuteDeployed = true;
}
