//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_HeliServo.h:
#ifndef	__FLOIDBOARD3_HELI_SERVO_H__
#define	__FLOIDBOARD3_HELI_SERVO_H__
// Heli logic:
// Heli Servos:
boolean setupHeliServos();
void    startHeliServos(int heliIndex);
void    stopHeliServos(int heliIndex);
void    startHeliServos();
void    stopHeliServos();
void    setDefaultHeliCyclicCollectiveValues(int heliIndex);
#endif /* __FLOIDBOARD3_HELI_SERVO_H__ */
