//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Servo.h:
#ifndef	__FLOIDBOARD3_SERVO_H__
#define	__FLOIDBOARD3_SERVO_H__
#define SERVO_TYPE_ESC  (1)
#define SERVO_TYPE_HELI (2)
#define SERVO_TYPE_PAN_TILT (3)
extern void servoAttach(byte servoType, byte servoIndex, byte servoSubIndex, byte servoPin, int servoMinPulseWidth, int servoMaxPulseWidth);
extern void servoWrite(byte servoType, byte servoIndex, byte servoSubIndex, int servoPulse);
#endif /* __FLOIDBOARD3_SERVO_H__ */
