//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
#ifndef	__FLOIDBOARD3_PRODUCTION_H__
#define	__FLOIDBOARD3_PRODUCTION_H__

// RUN MODES:
#define FLOID_RUN_MODE_PRODUCTION                          (0x00)
#define FLOID_RUN_MODE_PHYSICS_TEST_XY_BIT                 (0)
#define FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_1_BIT         (1)
#define FLOID_RUN_MODE_PHYSICS_TEST_ALTITUDE_2_BIT         (2)
#define FLOID_RUN_MODE_PHYSICS_TEST_HEADING_1_BIT          (3)
#define FLOID_RUN_MODE_PHYSICS_TEST_PITCH_1_BIT            (4)
#define FLOID_RUN_MODE_PHYSICS_TEST_ROLL_1_BIT             (5)


// TEST MODES:
#define FLOID_TEST_MODE_OFF                                (0x00)
#define FLOID_TEST_MODE_PHYSICS_NO_XY_BIT                  (0)
#define FLOID_TEST_MODE_PHYSICS_NO_ALTITUDE_BIT            (1)
#define FLOID_TEST_MODE_PHYSICS_NO_ATTACK_ANGLE_BIT        (2)
#define FLOID_TEST_MODE_PHYSICS_NO_HEADING_BIT             (3)
#define FLOID_TEST_MODE_PHYSICS_NO_PITCH_BIT               (4)
#define FLOID_TEST_MODE_PHYSICS_NO_ROLL_BIT                (5)
#define FLOID_TEST_MODE_PRINT_DEBUG_PHYSICS_HEADINGS_BIT   (6)
#define FLOID_TEST_MODE_CHECK_PHYSICS_MODEL_BIT            (7)

#endif /* __FLOIDBOARD3_PRODUCTION_H__ */
