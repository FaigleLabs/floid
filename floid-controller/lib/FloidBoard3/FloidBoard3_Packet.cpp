//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
#include <FloidBoard3.h>
unsigned long packetNumber = 0;
void printPacketHeaderOffsets()
{
  DEBUG_SERIAL.println("XXX");
  DEBUG_SERIAL.println("PH");
  DEBUG_SERIAL.println("fl");
  DEBUG_SERIAL.println(offsetof(PacketHeader, flag));
  DEBUG_SERIAL.println(offsetof(PacketHeader, length));
  DEBUG_SERIAL.println(offsetof(PacketHeader, checksum));
  DEBUG_SERIAL.println(offsetof(PacketHeader, number));
  DEBUG_SERIAL.println(offsetof(PacketHeader, type));
  DEBUG_SERIAL.println("XXX");
}
