//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Pins.cpp
#include <FloidBoard3.h>
// NM/Bay:
const int bayReleasePins[NUMBER_PAYLOADS]                   = { 24,  6,  38,  36};
const int bayReleaseAnalogNMStatusPins[NUMBER_PAYLOADS]     = { A1, A3, A15,  A7};
const int bayReleaseDigitalBayStatusPins[NUMBER_PAYLOADS]   = { 10,  9,   8,   5};
// Pan-tilt:
const int panTiltPins[NUMBER_PANTILTS][NUMBER_PANTILT_SERVOS] = {
                                                                  {12, 11},
                                                                  {37, 25},
                                                                  {39, 35},
                                                                  { 3,  4}
                                                                };
// Batteries:
// Analog pins:
const int mainBatteryPin                                    = A0;
const int currentPins[NUMBER_HELICOPTERS]                   = { A2,  A8, A14,  A6};
const int batteryPins[NUMBER_HELICOPTERS]                   = { A4,  A5, A11, A10};
// Servos and ESCs:
// Digital pins:                                                 L   R   P
const int servoPins[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS] = {
                                                                {26, 28, 30},
                                                                {33, 31, 29},
                                                                {47, 45, 43},
                                                                {40, 42, 44}
                                                              };
// Digital pins:
const int escPins[NUMBER_HELICOPTERS]                       = {32, 27, 41, 46};
const int heliOnPins[NUMBER_HELICOPTERS]                    = {22, 23, 49, 48};
const int gpsOnPin                                          = 34;
const int pyrOnPin                                          =  2;
const int designatorOnPin                                   = 21;
const int designatorStatusPin                               = A9;
const int auxOnPin                                          = 20;
const int auxCurrentPin                                     = A13;
const int auxBatteryPin                                     = A12;
// LEDs:
const int ledPins[NUMBER_LED_PINS]                          = {13};   // Only one LED for FloidBoard4 - FloidBoard3 had pin 24:  //  , 24};
// Parachute:      // TODO [PARACHUTE] DO ALL THE PARACHUTE WORK AND FIX THESE UP BELOW:
// Digital pin:
const int parachuteServoPin                                  = -1;  // Not implemented
// Analog pin:
const int parachuteDetectPin                                 = -1;  // Not implemented
