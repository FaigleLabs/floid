//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3.h:
#ifndef	__FLOIDBOARD3_H__
#define	__FLOIDBOARD3_H__
#include <Arduino.h>
#include <Servo.h>
#ifndef offsetof
  #define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif
#include <FloidBoard3_Production.h>
#include <MatrixMath.h>
#include <ConvertFloat.h>
#include <FloidBoard3_Debug_Tokens.h>
#include <nmea.h>
#include <aKalman.h>
#include <FloidBoard3_Pins.h>
#include <FloidBoard3_Memory.h>
#include <FloidBoard3_Packet.h>
#include <FloidBoard3_Status.h>
#include <FloidBoard3_Model.h>
#include <FloidBoard3_Servo.h>
#include <FloidBoard3_HeliServo.h>
#include <FloidBoard3_Commands.h>
#include <PmtkHandler.h>
#include <FloidBoard3_GPS.h>
#include <FloidBoard3_PYR.h>
#include <FloidBoard3_Altimeter.h>
#include <FloidBoard3_Esc.h>
#include <FloidBoard3_PanTilt.h>
#include <FloidBoard3_Camera.h>
#include <FloidBoard3_Payload.h>
#include <FloidBoard3_Designator.h>
#include <FloidBoard3_AUX.h>
#include <FloidBoard3_LED.h>
#include <FloidBoard3_Battery_Current.h>
#include <FloidBoard3_Parachute.h>
#include <FloidBoard3_Utility.h>
#include <FloidBoard3_Debug.h>
#include <FloidBoard3_EEPROM.h>
extern void asyncDelay(unsigned long delay);
#endif /* __FLOIDBOARD3_H__ */
