//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Altimeter.h
// faiglelabs.com
#ifndef	__FLOIDBOARD3_ALTIMETER_H__
#define	__FLOIDBOARD3_ALTIMETER_H__
#define              ALTIMETER_NUMBER_READINGS                              (5)
#define              ALTIMETER_BAROMETER_ALTITUDE_OUT_OF_RANGE_MIN          (-4000)
#define              ALTIMETER_BAROMETER_ALTITUDE_OUT_OF_RANGE_MAX          (25000)
extern float         altimeterAltitudeReadings[ALTIMETER_NUMBER_READINGS];
extern unsigned long altimeterAltitudeReadingTimes[ALTIMETER_NUMBER_READINGS];
// Altimeter readings are good for four seconds max:
#define              ALTIMETER_VALIDITY_PERIOD                              (4000ul)
#define              ALTIMETER_REQUIRED_GPS_COUNT_FOR_INITIALIZATION        (20)
#define              ALTIMETER_REQUIRED_GPS_COUNT_FOR_UPDATE                (10)
#define              ALTIMETER_REQUIRED_PYR_COUNT_FOR_INITIALIZATION        (20)
#define              ALTIMETER_REQUIRED_ALTIMETER_COUNT_FOR_INITIALIZATION  (20)
#define              ALTIMETER_UPDATE_ALPHA                                 (0.2)
boolean              setupAltimeter();
void                 loopAltimeter();
void                 initializeAltimeter(float barometerAltitude , float gpsAltitude);
void                 updateAltimeter(float barometerAltitude, float gpsAltitude, float alpha);
float                getAltimeterAltitude(float barometerAltitude);
#endif  /* __FLOIDBOARD3_ALTIMETER_H__ */
