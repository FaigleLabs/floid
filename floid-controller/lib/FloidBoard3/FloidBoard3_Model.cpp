//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Model.cpp:
#include <FloidBoard3.h>
// Model parameters and status:
FloidModelParameters floidModelParameters;
FloidModelStatus     floidModelStatus;
// Constructor for model status:
boolean setupFloidModelStatus()
{
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    floidModelStatus.floidModelCollectiveDeltaOrientation[heliIndex]       = 0.0;
    floidModelStatus.floidModelCollectiveDeltaAltitude[heliIndex]          = 0.0;
    floidModelStatus.floidModelCollectiveDeltaHeading[heliIndex]           = 0.0;
    floidModelStatus.floidModelCollectiveDeltaTotal[heliIndex]             = 0.0;
  }
  floidModelStatus.floidModelCyclicHeadingValue                            = 0.0;
  floidModelStatus.floidModelVelocityCyclicAlpha                           = 0.0;
  return true;
}
boolean setupFloidModel()
{
  floidModelParameters.floidModelCollectiveMinValue                                         = MODEL_1_COLLECTIVE_MIN_DEGREE_VALUE;
  floidModelParameters.floidModelCollectiveMaxValue                                         = MODEL_1_COLLECTIVE_MAX_DEGREE_VALUE;
  floidModelParameters.floidModelCollectiveDefaultValue                                     = MODEL_1_COLLECTIVE_DEFAULT_VALUE;
  floidModelParameters.floidModelCyclicRangeValue                                           = MODEL_1_CYCLIC_RANGE_VALUE;
  floidModelParameters.floidModelCyclicDefaultValue                                         = MODEL_1_CYCLIC_DEFAULT_VALUE;
  floidModelParameters.floidModelCyclicHeadingAlpha                                         = MODEL_1_CYCLIC_HEADING_ALPHA_DEFAULT_VALUE;
  for(int heliIndex = 0; heliIndex < NUMBER_HELICOPTERS; ++heliIndex)
  {
    floidModelStatus.floidModelCollectiveValue[heliIndex]                                   = floidModelParameters.floidModelCollectiveDefaultValue;
    for(int servoIndex = 0; servoIndex < NUMBER_HELI_SERVOS; ++servoIndex)
    {
      floidModelStatus.floidModelCyclicValue[heliIndex][servoIndex]                         = floidModelParameters.floidModelCyclicDefaultValue;
    }
  }
  // Heli-Servos:
  // ------------
  //   Servo degrees and equivalent pulse widths:
  floidModelParameters.floidModelHeliServoMinDegreeValue                                    = MODEL_1_HELI_SERVO_MIN_DEGREE_VALUE;
  floidModelParameters.floidModelHeliServoMaxDegreeValue                                    = MODEL_1_HELI_SERVO_MAX_DEGREE_VALUE;
  floidModelParameters.floidModelHeliServoMinPulseWidthValue                                = MODEL_1_HELI_SERVO_MIN_PULSE_WIDTH_VALUE;
  floidModelParameters.floidModelHeliServoMaxPulseWidthValue                                = MODEL_1_HELI_SERVO_MAX_PULSE_WIDTH_VALUE;
  //   Blade pitch Index values for the mapping below:
  floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_LOW_PITCH_INDEX]             = MODEL_1_BLADE_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_ZERO_PITCH_INDEX]            = MODEL_1_BLADE_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_HIGH_PITCH_INDEX]            = MODEL_1_BLADE_PITCH_HIGH_VALUE;
  //   Pitch values to servo degrees for each heli and servo and LOW/ZERO/HIGH value (These form low and high linear interpolation ranges - in future could use five points and fit with Bezier but we are too tight on memory for now) 
  floidModelParameters.floidModelBladePitchServoDegrees[0][0][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H0S0_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[0][0][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H0S0_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[0][0][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H0S0_PITCH_HIGH_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[0][1][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H0S1_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[0][1][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H0S1_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[0][1][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H0S1_PITCH_HIGH_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[0][2][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H0S2_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[0][2][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H0S2_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[0][2][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H0S2_PITCH_HIGH_VALUE;

  floidModelParameters.floidModelBladePitchServoDegrees[1][0][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H1S0_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[1][0][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H1S0_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[1][0][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H1S0_PITCH_HIGH_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[1][1][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H1S1_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[1][1][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H1S1_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[1][1][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H1S1_PITCH_HIGH_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[1][2][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H1S2_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[1][2][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H1S2_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[1][2][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H1S2_PITCH_HIGH_VALUE;

  floidModelParameters.floidModelBladePitchServoDegrees[2][0][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H2S0_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[2][0][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H2S0_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[2][0][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H2S0_PITCH_HIGH_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[2][1][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H2S1_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[2][1][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H2S1_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[2][1][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H2S1_PITCH_HIGH_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[2][2][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H2S2_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[2][2][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H2S2_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[2][2][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H2S2_PITCH_HIGH_VALUE;

  floidModelParameters.floidModelBladePitchServoDegrees[3][0][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H3S0_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[3][0][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H3S0_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[3][0][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H3S0_PITCH_HIGH_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[3][1][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H3S1_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[3][1][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H3S1_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[3][1][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H3S1_PITCH_HIGH_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[3][2][FLOID_MODEL_LOW_PITCH_INDEX]  = MODEL_1_H3S2_PITCH_LOW_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[3][2][FLOID_MODEL_ZERO_PITCH_INDEX] = MODEL_1_H3S2_PITCH_ZERO_VALUE;
  floidModelParameters.floidModelBladePitchServoDegrees[3][2][FLOID_MODEL_HIGH_PITCH_INDEX] = MODEL_1_H3S2_PITCH_HIGH_VALUE;
  //   Direction of servo travel to head travel:
  floidModelParameters.floidModelServoSigns[SERVO_LEFT]                                     = MODEL_1_HELI_SERVO_LEFT_SIGN;
  floidModelParameters.floidModelServoSigns[SERVO_RIGHT]                                    = MODEL_1_HELI_SERVO_RIGHT_SIGN;
  floidModelParameters.floidModelServoSigns[SERVO_PITCH]                                    = MODEL_1_HELI_SERVO_PITCH_SIGN;
  // Location of servo influence points:
  floidModelParameters.floidModelHeliServoOffsets[0][SERVO_LEFT]                            = MODEL_1_HELI_SERVO_LEFT_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[0][SERVO_RIGHT]                           = MODEL_1_HELI_SERVO_RIGHT_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[0][SERVO_PITCH]                           = MODEL_1_HELI_SERVO_PITCH_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[1][SERVO_LEFT]                            = MODEL_1_HELI_SERVO_LEFT_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[1][SERVO_RIGHT]                           = MODEL_1_HELI_SERVO_RIGHT_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[1][SERVO_PITCH]                           = MODEL_1_HELI_SERVO_PITCH_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[2][SERVO_LEFT]                            = MODEL_1_HELI_SERVO_LEFT_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[2][SERVO_RIGHT]                           = MODEL_1_HELI_SERVO_RIGHT_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[2][SERVO_PITCH]                           = MODEL_1_HELI_SERVO_PITCH_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[3][SERVO_LEFT]                            = MODEL_1_HELI_SERVO_LEFT_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[3][SERVO_RIGHT]                           = MODEL_1_HELI_SERVO_RIGHT_OFFSET;
  floidModelParameters.floidModelHeliServoOffsets[3][SERVO_PITCH]                           = MODEL_1_HELI_SERVO_PITCH_OFFSET;
  // Both of these are unused: (Correct ones are: floidModelDistanceToTargetMin and floidModelDistanceToTargetMax)
  floidModelParameters.floidModelTargetVelocityKeepStill                                    = MODEL_1_TARGET_VELOCITY_KEEP_STILL;
  floidModelParameters.floidModelTargetVelocityFullSpeed                                    = MODEL_1_TARGET_VELOCITY_FULL_SPEED;
  // Calculated pitch, servo and pulse values:
  // -----------------------------------------
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    floidModelCalculateHeliServosFromCollectivesAndCyclics(heliIndex, false);
  }
  // ESCs:
  // -----
  floidModelParameters.floidModelESCType                                                    = MODEL_1_ESC_TYPE;
  floidModelParameters.floidModelESCServoDefaultLowValue                                    = MODEL_1_ESC_SERVO_DEFAULT_LOW_VALUE;
  floidModelParameters.floidModelESCServoDefaultHighValue                                   = MODEL_1_ESC_SERVO_DEFAULT_HIGH_VALUE;
  floidModelParameters.floidModelESCServoMinPulseWidthValue                                 = MODEL_1_ESC_SERVO_MIN_PULSE_WIDTH_VALUE;
  floidModelParameters.floidModelESCServoMaxPulseWidthValue                                 = MODEL_1_ESC_SERVO_MAX_PULSE_WIDTH_VALUE;
  // Target Velocity Alphas:
  // -----------------------
  floidModelParameters.floidModelTargetPitchVelocityAlpha                                   = MODEL_1_TARGET_PITCH_VELOCITY_ALPHA;
  floidModelParameters.floidModelTargetRollVelocityAlpha                                    = MODEL_1_TARGET_ROLL_VELOCITY_ALPHA;
  floidModelParameters.floidModelTargetHeadingVelocityAlpha                                 = MODEL_1_TARGET_HEADING_VELOCITY_ALPHA;
  floidModelParameters.floidModelTargetAltitudeVelocityAlpha                                = MODEL_1_TARGET_ALTITUDE_VELOCITY_ALPHA;
  floidModelParameters.floidModelAttackAngleMinDistance                                     = MODEL_1_ATTACK_ANGLE_MIN_DISTANCE;
  floidModelParameters.floidModelAttackAngleMaxDistance                                     = MODEL_1_ATTACK_ANGLE_MAX_DISTANCE;
  floidModelParameters.floidModelAttackAngleMaxValue                                        = MODEL_1_ATTACK_ANGLE_MAX_VALUE;
  floidModelParameters.floidModelOrientationMinDistanceToTargetForOrientation               = MODEL_1_ORIENTATION_MIN_DISTANCE_TO_TARGET_FOR_ORIENTATION;
  floidModelParameters.floidModelPitchDeltaMin                                              = MODEL_1_PITCH_DELTA_MIN;
  floidModelParameters.floidModelPitchDeltaMax                                              = MODEL_1_PITCH_DELTA_MAX;
  floidModelParameters.floidModelPitchTargetVelocityMaxValue                                = MODEL_1_PITCH_TARGET_VELOCITY_MAX_VALUE;
  floidModelParameters.floidModelRollDeltaMin                                               = MODEL_1_ROLL_DELTA_MIN;
  floidModelParameters.floidModelRollDeltaMax                                               = MODEL_1_ROLL_DELTA_MAX;
  floidModelParameters.floidModelRollTargetVelocityMaxValue                                 = MODEL_1_ROLL_TARGET_VELOCITY_MAX_VALUE;
  floidModelParameters.floidModelAltitudeToTargetMin                                        = MODEL_1_ALTITUDE_TO_TARGET_MIN;
  floidModelParameters.floidModelAltitudeToTargetMax                                        = MODEL_1_ALTITUDE_TO_TARGET_MAX;
  floidModelParameters.floidModelAltitudeTargetVelocityMaxValue                             = MODEL_1_ALTITUDE_TARGET_VELOCITY_MAX_VALUE;
  floidModelParameters.floidModelHeadingDeltaMin                                            = MODEL_1_HEADING_DELTA_MIN;
  floidModelParameters.floidModelHeadingDeltaMax                                            = MODEL_1_HEADING_DELTA_MAX;
  floidModelParameters.floidModelHeadingTargetVelocityMaxValue                              = MODEL_1_HEADING_TARGET_VELOCITY_MAX_VALUE;
  floidModelParameters.floidModelDistanceToTargetMin                                        = MODEL_1_DISTANCE_TO_TARGET_MIN;
  floidModelParameters.floidModelDistanceToTargetMax                                        = MODEL_1_DISTANCE_TO_TARGET_MAX;
  floidModelParameters.floidModelDistanceTargetVelocityMaxValue                             = MODEL_1_DISTANCE_TARGET_VELOCITY_MAX_VALUE;
  floidModelParameters.floidModelLiftOffModeTargetAltitudeDelta                             = MODEL_1_LIFT_OFF_MODE_TARGET_ALTITUDE_DELTA;
  floidModelParameters.floidModelLandModeRequiredTimeAtMinAltitude                          = MODEL_1_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE;
  // Start Mode:
  floidModelParameters.floidModelStartMode                                                  = MODEL_1_DEFAULT_START_MODE;
  // Normal Rotation:
  floidModelParameters.floidModelRotationMode                                               = MODEL_1_ROTATION_MODE_DEFAULT_VALUE;
  // Heli Startup:
  floidModelParameters.heliStartupModeNumberSteps                                           = MODEL_1_HELI_STARTUP_NUMBER_STEPS;
  floidModelParameters.heliStartupModeStepTick                                              = MODEL_1_HELI_STARTUP_STEP_TICK;
  // ESC Collective Calcs:
  floidModelParameters.floidModelEscCollectiveCalcMidpoint                                  = MODEL_1_ESC_COLLECTIVE_CALC_MIDPOINT;
  floidModelParameters.floidModelEscCollectiveLowValue                                      = MODEL_1_ESC_COLLECTIVE_LOW_VALUE;
  floidModelParameters.floidModelEscCollectiveMidValue                                      = MODEL_1_ESC_COLLECTIVE_MID_VALUE;
  floidModelParameters.floidModelEscCollectiveHighValue                                     = MODEL_1_ESC_COLLECTIVE_HIGH_VALUE;
  // Velocity delta cyclic alpha:
  floidModelParameters.velocityDeltaCyclicAlphaScale                                        = MODEL_1_VELOCITY_DELTA_CYCLIC_ALPHA;
  // Acceleration multiplier:
  floidModelParameters.accelerationMultiplierScale                                          = MODEL_1_ACCELERATION_MULTIPLIER;
  // Load the model from the eeprom if possible:
  debugPrintlnToken(FLOID_DEBUG_LABEL_EEPROM_LOADING);
  if(loadFromEEPROM())
  {
    // We successfully loaded from EEProm
    debugPrintlnToken(FLOID_DEBUG_LABEL_EEPROM_LOAD_FAILURE);
  }
  else
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_EEPROM_LOAD_SUCCESS);
  }
  // Finally set the start mode:
  floidStatus.mode                                                                          = floidModelParameters.floidModelStartMode;
  return true;
}
void floidModelCalculateHeliServosFromCollectivesAndCyclics(int heliIndex, boolean writeToServo)
{
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_HELI_SERVOS_FROM_COLLECTIVES_AND_CYCLICS);
    debugPrintln(heliIndex);
  }

  floidModelStatus.floidModelCalculatedCollectiveBladePitch[heliIndex] = floidModelCalculateCollectiveBladePitch(floidModelStatus.floidModelCollectiveValue[heliIndex]);
  for(int servoIndex = 0; servoIndex < NUMBER_HELI_SERVOS; ++servoIndex)
  {
    floidModelStatus.floidModelCalculatedCyclicBladePitch[heliIndex][servoIndex] = floidModelCalculateCyclicBladePitch(floidModelStatus.floidModelCyclicValue[heliIndex][servoIndex]);
    if(floidStatus.debug && floidStatus.debugHelis)
    {
      debugPrintToken(FLOID_DEBUG_LABEL_MODEL_SERVO);
      debugPrint(servoIndex);
      debugPrintToken(FLOID_DEBUG_LABEL_MODEL_CYCLIC);
      debugPrint(floidModelStatus.floidModelCalculatedCyclicBladePitch[heliIndex][servoIndex]);
      debugPrintToken(FLOID_DEBUG_LABEL_MODEL_COLLECTIVE);
      debugPrintln(floidModelStatus.floidModelCalculatedCollectiveBladePitch[heliIndex]);
    }
    floidModelSetHeliServoFromBladePitch(heliIndex, servoIndex, floidModelStatus.floidModelCalculatedCollectiveBladePitch[heliIndex] + floidModelStatus.floidModelCalculatedCyclicBladePitch[heliIndex][servoIndex], writeToServo);
  }
}
void floidModelSetHeliServoFromValue(int heliIndex, int servoIndex, float heliServoValue, boolean writeToServo)
{
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_HELI_SERVO_FROM_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_HELI);
    debugPrint(heliIndex);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_SERVO);
    debugPrintln(servoIndex);
  }
  // heliServoValue in 0-1 - convert to range of min/max degrees:
  floidModelSetHeliServoFromDegreeValue(heliIndex, servoIndex, heliServoValue * (floidModelParameters.floidModelHeliServoMaxDegreeValue - floidModelParameters.floidModelHeliServoMinDegreeValue) + floidModelParameters.floidModelHeliServoMinDegreeValue, writeToServo);

}
void floidModelSetHeliServoFromBladePitch(int heliIndex, int servoIndex, float bladePitch, boolean writeToServo)
{
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_HELI_SERVO_FROM_BLADE_PITCH);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_HELI);
    debugPrint(heliIndex);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_SERVO);
    debugPrint(servoIndex);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_INPUT);
    debugPrintln(bladePitch);
  }
  // bladePitch is in degrees based around zero, normally about -20<0<20:
  floidModelStatus.floidModelCalculatedBladePitch[heliIndex][servoIndex]                = bladePitch;
  floidModelSetHeliServoFromDegreeValue(heliIndex, servoIndex, floidModelBladePitchToServoDegrees(heliIndex, servoIndex, floidModelStatus.floidModelCalculatedBladePitch[heliIndex][servoIndex]), writeToServo);
}
void floidModelSetHeliServoFromDegreeValue(int heliIndex, int servoIndex, float heliServoDegreeValue, boolean writeToServo)
{
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_HELI_SERVO_FROM_DEGREE_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_HELI);
    debugPrint(heliIndex);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_SERVO);
    debugPrint(servoIndex);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_INPUT);
    debugPrintln(heliServoDegreeValue);
  }
  // heliServoDegreeValue in degree value:
  floidModelStatus.floidModelCalculatedServoDegrees[heliIndex][servoIndex]              = heliServoDegreeValue;
  floidModelStatus.floidModelCalculatedServoPulse[heliIndex][servoIndex]                = floidModelServoDegreeToServoPulse(heliIndex, servoIndex, floidModelStatus.floidModelCalculatedServoDegrees[heliIndex][servoIndex]);
  if(writeToServo)
  {
    // Write this to the servo:
    servoWrite(SERVO_TYPE_HELI, heliIndex, servoIndex, floidModelStatus.floidModelCalculatedServoPulse[heliIndex][servoIndex]);
  }
}
float floidModelCalculateCyclicBladePitch(float cyclicValue)
{
  float cyclicBladePitch = cyclicValue * floidModelParameters.floidModelCyclicRangeValue;  // cyclic value is [-1,1]
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_CALC_CYCLIC_BLADE_PITCH);
    debugPrintln();
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_INPUT);
    debugPrint(cyclicValue);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(floidModelParameters.floidModelCyclicRangeValue);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_OUTPUT);
    debugPrintln(cyclicBladePitch);
  }
  return cyclicBladePitch;
}
float floidModelCalculateCollectiveBladePitch(float collectiveValue)
{
  // The collective value is linearly interpolated between the high and low collective blade pitches
  float collectiveBladePitch = collectiveValue * (floidModelParameters.floidModelCollectiveMaxValue - floidModelParameters.floidModelCollectiveMinValue) + floidModelParameters.floidModelCollectiveMinValue;
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_COLLECTIVE_BLADE_PITCH);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_INPUT);
    debugPrint(collectiveValue);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_OUTPUT);
    debugPrintln(collectiveBladePitch);
  }
  return collectiveBladePitch;
}
float floidModelBladePitchToServoDegrees(int heliIndex, int servoIndex, float bladePitch)
{
  float servoDegrees = 0.0;
  // TODO [POTENTIAL_CODE_ENHANCEMENT] Consider re-writing this so it produces a cyclic value (?)
  if(bladePitch < floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_ZERO_PITCH_INDEX])
  {
    // Linearly interpolate between these LOW and ZERO values:
    servoDegrees = floidModelLinearlyInterpolate(bladePitch,
                                                 floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_LOW_PITCH_INDEX],
                                                 floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_ZERO_PITCH_INDEX],
                                                 floidModelParameters.floidModelBladePitchServoDegrees[heliIndex][servoIndex][FLOID_MODEL_LOW_PITCH_INDEX],
                                                 floidModelParameters.floidModelBladePitchServoDegrees[heliIndex][servoIndex][FLOID_MODEL_ZERO_PITCH_INDEX],
                                                 true);
  }
  else
  {
    // Linearly interpolate between ZERO and HIGH values:
    servoDegrees = floidModelLinearlyInterpolate(bladePitch,
                                                 floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_ZERO_PITCH_INDEX],
                                                 floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_HIGH_PITCH_INDEX],
                                                 floidModelParameters.floidModelBladePitchServoDegrees[heliIndex][servoIndex][FLOID_MODEL_ZERO_PITCH_INDEX],
                                                 floidModelParameters.floidModelBladePitchServoDegrees[heliIndex][servoIndex][FLOID_MODEL_HIGH_PITCH_INDEX],
                                                 true);
  }
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_BLADE_PITCH_TO_SERVO_DEGREES);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_HELI);
    debugPrint(heliIndex);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_SERVO);
    debugPrint(servoIndex);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_INPUT);
    debugPrint(bladePitch);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_OUTPUT);
    debugPrintln(servoDegrees);
  }
  return servoDegrees;
}
float floidModelServoDegreeToServoPulse(int heliIndex, int servoIndex, float servoDegree)
{
  float servoPulse = 0.0;
  // Yes:
  servoPulse = floidModelLinearlyInterpolate(servoDegree,
               floidModelParameters.floidModelHeliServoMinDegreeValue,
               floidModelParameters.floidModelHeliServoMaxDegreeValue,
               floidModelParameters.floidModelHeliServoMinPulseWidthValue,
               floidModelParameters.floidModelHeliServoMaxPulseWidthValue,
               true);
  // Negative servo?
  if(floidModelParameters.floidModelServoSigns[servoIndex] == 0)
  {
    // Invert the pulse:
    if(floidStatus.debug && floidStatus.debugHelis)
    {
      debugPrint(FLOID_DEBUG_SPACE_MINUS_SPACE);
    }
    servoPulse = floidModelParameters.floidModelHeliServoMinPulseWidthValue + (floidModelParameters.floidModelHeliServoMaxPulseWidthValue - servoPulse);
  }
  else
  {
    // Do not invert the pulse:
    if(floidStatus.debug && floidStatus.debugHelis)
    {
      debugPrint(FLOID_DEBUG_SPACE_PLUS_SPACE);
    }
  }
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_SERVO_DEGREES_TO_SERVO_PULSE);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_HELI);
    debugPrint(heliIndex);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_SERVO);
    debugPrint(servoIndex);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_INPUT);
    debugPrint((int)floidModelParameters.floidModelServoSigns[servoIndex]);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_OUTPUT);
    debugPrintln(servoPulse);
  }
  return servoPulse;
}
// ESC mapping function:
// ---------------------
void floidModelSetEscFromCollective(int heliIndex)
{
  // Note there is an additional linear interpolation step (2) below.
  // This could be removed but provides a final ranging of the ESC.
  // The defaults are linearly mapping [0,1]->[0,1] so no effect by default.
  // By temporarily modifying e.g ESCServoDefaultHighValue, we can decrease the actual ESC output linearly for testing.
  //
  // 1. Calculate ESC Collective Value from Collective Value:  (two-part linear interpolation)
  //       [0,1]->[EscCollectiveLowValue>=0, EscCollectiveHighValue<=1]
  // 2. Calculate ESC Value from ESC Collective Value:
  //       [0,1]->[ESCServoDefaultLowValue>=0, ESCServoDefaultHighValue<=1]
  //
  float escCollectiveValue = 0.0;
  if (floidModelStatus.floidModelCollectiveValue[heliIndex] < floidModelParameters.floidModelEscCollectiveCalcMidpoint)
  {
    // From 0 to midpoint:
    escCollectiveValue = floidModelLinearlyInterpolate(floidModelStatus.floidModelCollectiveValue[heliIndex],
                                                       0,
                                                       floidModelParameters.floidModelEscCollectiveCalcMidpoint,
                                                       floidModelParameters.floidModelEscCollectiveLowValue,
                                                       floidModelParameters.floidModelEscCollectiveMidValue,
                                                       true);
  }
  else
  {
    // From midpoint to 1:
    escCollectiveValue = floidModelLinearlyInterpolate(floidModelStatus.floidModelCollectiveValue[heliIndex],
                                                       floidModelParameters.floidModelEscCollectiveCalcMidpoint,
                                                       1,
                                                       floidModelParameters.floidModelEscCollectiveMidValue,
                                                       floidModelParameters.floidModelEscCollectiveHighValue,
                                                       true);
  }
  // Calculate the ESC value:
  float escValue = floidModelLinearlyInterpolate(escCollectiveValue,
                                                 0,
                                                 1,
                                                 floidModelParameters.floidModelESCServoDefaultLowValue,
                                                 floidModelParameters.floidModelESCServoDefaultHighValue,
                                                 true);
  // Set the ESC Value (if in PRODUCTION mode - in TEST mode don't set the ESC's):
  if(isFloidProductionMode()) {
    setESCValue(heliIndex, escValue);
  }
}

float clampFloat(float alpha, float t0, float t1) {
  // Clamp
  if(t0 < t1) {
    if(alpha < t0) {
    alpha = t0;
    }
    if(alpha > t1) {
      alpha = t1;
    }
  } else {
    if(alpha < t1) {
    alpha = t1;
    }
    if(alpha > t0) {
      alpha = t0;
    }
  }
  return alpha;
}

float floidModelLinearlyInterpolate(float alpha, float t0, float t1, float v0, float v1, bool clamp)
{
  if(clamp) {
    alpha = clampFloat(alpha, t0, t1);
  }
  return v0 + (alpha - t0)/(t1-t0) * (v1-v0);
}

// ESC Servo Value Mappings:
// ==========================
// Map esc values in 0-1 to servo values: 
int escValueToESCServoPulse(float escValue)
{
  // Clamp the escValue to 0 to 1:
  escValue = fmin(fmax(escValue, 0), 1);
  int escPulse = (int)(escValue * (floidModelParameters.floidModelESCServoMaxPulseWidthValue - floidModelParameters.floidModelESCServoMinPulseWidthValue) + floidModelParameters.floidModelESCServoMinPulseWidthValue);
  if(floidStatus.debug && floidStatus.debugHelis)
  {
    debugPrintToken(FLOID_DEBUG_LABEL_ESC_VALUE_TO_ESC_PULSE);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_INPUT);
    debugPrint(escValue);
    debugPrintToken(FLOID_DEBUG_LABEL_MODEL_OUTPUT);
    debugPrint(escPulse);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(floidModelParameters.floidModelESCServoMinPulseWidthValue);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(floidModelParameters.floidModelESCServoMaxPulseWidthValue);
  }
  return escPulse;
}
boolean sendModelParameters()
{
  // 1. Set up the header
  setupPacketHeader(&floidModelParameters.packetHeader, (byte)MODEL_PARAMETERS_PACKET, sizeof(FloidModelParameters));
  // 2. Send the packet:
  return sendPacket((byte*)&floidModelParameters, sizeof(FloidModelParameters));
}
boolean sendModelStatus()
{
  // 1. Set up the header
  setupPacketHeader(&floidModelStatus.packetHeader, (byte)MODEL_STATUS_PACKET, sizeof(FloidModelStatus));
  // 2. Send the packet:
  return sendPacket((byte*)&floidModelStatus, sizeof(FloidModelStatus));
}
float floidModelHeliValueFromServoPulse(int heliIndex, int servoIndex)
{
  // This routine inverts a servo pulse to a range value between the high and low degrees:
  return fmin(fmax((floidModelStatus.floidModelCalculatedServoPulse[heliIndex][servoIndex] - floidModelParameters.floidModelHeliServoMinPulseWidthValue)/(float)(floidModelParameters.floidModelHeliServoMaxPulseWidthValue - floidModelParameters.floidModelHeliServoMinPulseWidthValue),0),1);
}
void printModelStatusOffsets()
{
  DEBUG_SERIAL.println("XXX");
  DEBUG_SERIAL.println("MS");
  DEBUG_SERIAL.println(sizeof(FloidModelStatus));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCollectiveDeltaOrientation));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCollectiveDeltaAltitude));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCollectiveDeltaHeading));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCollectiveDeltaTotal));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCyclicValue));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCollectiveValue));
  DEBUG_SERIAL.println("clc");
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCalculatedCyclicBladePitch));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCalculatedCollectiveBladePitch));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCalculatedBladePitch));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCalculatedServoDegrees));
  DEBUG_SERIAL.println(offsetof(FloidModelStatus, floidModelCalculatedServoPulse));
  DEBUG_SERIAL.println("XXX");
}
void    printModelParametersOffsets()
{
  DEBUG_SERIAL.println("XXX");
  DEBUG_SERIAL.println("MP");
  DEBUG_SERIAL.println(sizeof(FloidModelParameters));
  DEBUG_SERIAL.println("cc");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelCollectiveMinValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelCollectiveMaxValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelCollectiveDefaultValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelCyclicRangeValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelCyclicDefaultValue));
  DEBUG_SERIAL.println("hs");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelHeliServoMinDegreeValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelHeliServoMaxDegreeValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelHeliServoMinPulseWidthValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelHeliServoMaxPulseWidthValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelHeliServoOffsets));
  // Both of these are unused: (Correct ones are: floidModelDistanceToTargetMin and floidModelDistanceToTargetMax)
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelTargetVelocityKeepStill));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelTargetVelocityFullSpeed));
  DEBUG_SERIAL.println("bp");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelBladePitchIndices));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelBladePitchServoDegrees));
  DEBUG_SERIAL.println("ss");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelServoSigns));
  DEBUG_SERIAL.println("esc");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelESCType));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelESCServoDefaultLowValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelESCServoDefaultHighValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelESCServoMinPulseWidthValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelESCServoMaxPulseWidthValue));
  DEBUG_SERIAL.println("alp");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelTargetPitchVelocityAlpha));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelTargetRollVelocityAlpha));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelTargetHeadingVelocityAlpha));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelTargetAltitudeVelocityAlpha));
  DEBUG_SERIAL.println("phy");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelAttackAngleMinDistance));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelAttackAngleMaxDistance));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelAttackAngleMaxValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelOrientationMinDistanceToTargetForOrientation));
  DEBUG_SERIAL.println("ptc");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelPitchDeltaMin));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelPitchDeltaMax));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelPitchTargetVelocityMaxValue));
  DEBUG_SERIAL.println("rol");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelRollDeltaMin));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelRollDeltaMax));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelRollTargetVelocityMaxValue));
  DEBUG_SERIAL.println("alt");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelAltitudeToTargetMin));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelAltitudeToTargetMax));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelAltitudeTargetVelocityMaxValue));
  DEBUG_SERIAL.println("hed");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelHeadingDeltaMin));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelHeadingDeltaMax));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelHeadingTargetVelocityMaxValue));
  DEBUG_SERIAL.println("dst");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelDistanceToTargetMin));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelDistanceToTargetMax));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelDistanceTargetVelocityMaxValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelLiftOffModeTargetAltitudeDelta));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelLandModeRequiredTimeAtMinAltitude));
  DEBUG_SERIAL.println("smd");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelStartMode));

  DEBUG_SERIAL.println("rmd");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelRotationMode));

  DEBUG_SERIAL.println("cha");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelCyclicHeadingAlpha));
  DEBUG_SERIAL.println("sup");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, heliStartupModeNumberSteps));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, heliStartupModeStepTick));

  DEBUG_SERIAL.println("eco");
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelEscCollectiveCalcMidpoint));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelEscCollectiveLowValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelEscCollectiveMidValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, floidModelEscCollectiveHighValue));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, velocityDeltaCyclicAlphaScale));
  DEBUG_SERIAL.println(offsetof(FloidModelParameters, accelerationMultiplierScale));
  DEBUG_SERIAL.println("XXX");
}
