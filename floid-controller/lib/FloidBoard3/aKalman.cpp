// aKalman is a straight port of JKalman (http://sourceforge.net/projects/jkalman/) - see there for licensing and docs
//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
// Free                                   //
//----------------------------------------//
#include <FloidBoard3.h>
MatrixMath matrixMath;
aKalman::aKalman(int dp, int mp, int cp, float processNoiseCovInit, float measurementNoiseCovInit, bool bPrintMatrices)
{
  this->dp                         = dp;
  this->mp                         = mp;
  this->cp                         = cp;
  this->processNoiseCovInit        = processNoiseCovInit;
  this->measurementNoiseCovInit    = measurementNoiseCovInit;
  this->bPrintMatrices             = bPrintMatrices;
  this->matricesCreated            = false;
  /* Create all matrices: */
  this->m                          = (float*)0l;
  this->statePre                   = (float*)0l;
  this->statePost                  = (float*)0l;
  this->transitionMatrix           = (float*)0l;
  this->transitionMatrixTranspose  = (float*)0l;
  this->controlMatrix              = (float*)0l;
  this->measurementMatrix          = (float*)0l;
  this->measurementMatrixTranspose = (float*)0l;
  this->processNoiseCov            = (float*)0l;
  this->measurementNoiseCov        = (float*)0l;
  this->errorCovPre                = (float*)0l;
  this->gain                       = (float*)0l;
  this->errorCovPost               = (float*)0l;
  this->temp1                      = (float*)0l;
  this->temp2                      = (float*)0l;
  this->temp3                      = (float*)0l;
  this->temp3Inverted              = (float*)0l;
  this->temp4                      = (float*)0l;
  this->temp5                      = (float*)0l;
}
#define null (0l)
boolean aKalman::createMatrices()
{
  /* Create all matrices: */
  m                            = (float*) malloc(sizeof(float) * mp *  1);    // [MP, 1]
  if(m == null) return false;
  statePre                     = (float*) malloc(sizeof(float) * dp *  1);    // [DP, 1]
  if(statePre == null) return false;
  statePost                    = (float*) malloc(sizeof(float) * dp *  1);    // [DP, 1]        
  if(statePost == null) return false;
  transitionMatrix             = (float*) malloc(sizeof(float) * dp * dp);    // [DP, DP]
  if(transitionMatrix == null) return false;
  transitionMatrixTranspose    = (float*) malloc(sizeof(float) * dp * dp);    // [DP, DP]
  if(transitionMatrixTranspose == null) return false;
  if(cp!=0)
  {
    controlMatrix              = (float*) malloc(sizeof(float) * cp * dp);    // [CP, DP]
    if(controlMatrix == null) return false;
  }
  else
  {
    controlMatrix              = 0l;                                          // or NULL
  }
  measurementMatrix            = (float*) malloc(sizeof(float) * mp * dp);    // [MP, DP]  
  if(measurementMatrix == null) return false;
  measurementMatrixTranspose   = (float*) malloc(sizeof(float) * dp * mp);    // [DP, MP]  
  if(measurementMatrixTranspose == null) return false;
  processNoiseCov              = (float*) malloc(sizeof(float) * dp * dp);    // [DP, DP]
  if(processNoiseCov == null) return false;
  measurementNoiseCov          = (float*) malloc(sizeof(float) * mp * mp);    // [MP, MP]
  if(measurementNoiseCov == null) return false;
  errorCovPre                  = (float*) malloc(sizeof(float) * dp * dp);    // [DP, DP]
  if(errorCovPre == null) return false;
  gain                         = (float*) malloc(sizeof(float) * dp * mp);    // [DP, MP]
  if(gain == null) return false;
  errorCovPost                 = (float*) malloc(sizeof(float) * dp * dp);    // [DP, DP]
  if(errorCovPost == null) return false;
  temp1                        = (float*) malloc(sizeof(float) * dp * dp);    // [DP, DP]
  if(temp1 == null) return false;
  temp2                        = (float*) malloc(sizeof(float) * mp * dp);    // [MP, DP]
  if(temp2 == null) return false;
  temp3                        = (float*) malloc(sizeof(float) * mp * mp);    // [MP, MP]
  if(temp3 == null) return false;
  temp3Inverted                = (float*) malloc(sizeof(float) * mp * mp);    // [MP, MP]
  if(temp3Inverted == null) return false;
  temp4                        = (float*) malloc(sizeof(float) * mp * dp);    // [MP, DP]
  if(temp4 == null) return false;
  temp5                        = (float*) malloc(sizeof(float) * mp *  1);    // [MP,  1]
  if(temp5 == null) return false;
  matricesCreated              = true;
  return true;
}
void aKalman::freeMatrices()
{
  if(matricesCreated)
  {
    if(m)
    {
      free(m);
      m = (float*)0l;
    }
    if(statePre)
    {
      free(statePre);
      statePre = (float*)0l;
    }
    if(statePost)
    {
      free(statePost);
      statePost = (float*)0l;
    }
    if(transitionMatrix)
    {
      free(transitionMatrix);
      transitionMatrix = (float*)0l;
    }
    if(transitionMatrixTranspose)
    {
      free(transitionMatrixTranspose);
      transitionMatrixTranspose = (float*)0l;
    }
    if(controlMatrix)
    {
      free(controlMatrix);
      controlMatrix = (float*)0l;
    }
    if(measurementMatrix)
    {
      free(measurementMatrix);
      measurementMatrix = (float*)0l;
    }
    if(measurementMatrixTranspose)
    {
      free(measurementMatrixTranspose);
      measurementMatrixTranspose = (float*)0l;
    }
    if(processNoiseCov)
    {
      free(processNoiseCov);
      processNoiseCov = (float*)0l;
    }
    if(measurementNoiseCov)
    {
      free(measurementNoiseCov);
      measurementNoiseCov = (float*)0l;
    }
    if(errorCovPre)
    {
      free(errorCovPre);
      errorCovPre = (float*)0l;
    }
    if(gain)
    {
      free(gain);
      gain = (float*)0l;
    }
    if(errorCovPost)
    {
      free(errorCovPost);
      errorCovPost = (float*)0l;
    }
    if(temp1)
    {
      free(temp1);
      temp1 = (float*)0l;
    }
    if(temp2)
    {
      free(temp2);
      temp2 = (float*)0l;
    }
    if(temp3)
    {
      free(temp3);
      temp3 = (float*)0l;
    }
    if(temp3Inverted)
    {
      free(temp3Inverted);
      temp3Inverted = (float*)0l;
    }
    if(temp4)
    {
      free(temp4);
      temp4 = (float*)0l;
    }
    if(temp5)
    {
      free(temp5);
      temp5 = (float*)0l;
    }
    matricesCreated = false;
  }
}
aKalman::~aKalman()
{
  freeMatrices();
}
boolean aKalman::init()
{
  if(!matricesCreated)
  {
    if(!createMatrices()) return false;
  }
  /* Initialize all the matrices to defaults */
  // Clear necessary matrices to start:
  // Important matrices to init:
  zeroMatrix(                              statePre,  dp,  1);
  identityMatrix(                 measurementMatrix,  mp, dp);
  identityPlusDerivativesMatrix(   transitionMatrix,  dp, dp);
  identityMatrix(                   processNoiseCov,  dp, dp, processNoiseCovInit);
  identityMatrix(               measurementNoiseCov,  mp, mp, measurementNoiseCovInit);
  identityMatrix(                      errorCovPost,  dp, dp);
  // Other matrices to init for consistency:
  zeroMatrix(                                     m,  mp,  1);
  zeroMatrix(                             statePost,  dp,  1);
  identityMatrix(                       errorCovPre,  dp, dp);
  identityMatrix(         transitionMatrixTranspose,  dp, dp);  // Note: Not the transpose of the transition matrix at this point but will get set later
  if(cp!=0)
  {
    zeroMatrix(                       controlMatrix,  cp, dp);    // [CP, DP]
  }
  identityMatrix(        measurementMatrixTranspose,  dp, mp);  // Note: Not the transpose of the measurement matrix at this point but will get set later
  zeroMatrix(                                  gain,  dp, mp);    // [DP, MP]
  zeroMatrix(                                 temp1,  dp, dp);    // [DP, DP]
  zeroMatrix(                                 temp2,  mp, dp);    // [MP, DP]
  identityMatrix(                             temp3,  mp, mp);    // [MP, MP]
  identityMatrix(                     temp3Inverted,  mp, mp);    // [MP, MP]
  zeroMatrix(                                 temp4,  mp, dp);    // [MP, DP]
  zeroMatrix(                                 temp5,  mp,  1);    // [MP,  1]
  if(bPrintMatrices)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println("In");
      printMatrices();
    }
  }
  return true;
}

void aKalman::predict()
{
  // (1) Project the state ahead
  // update the state: x'(k) = A*x(k)
  // state_pre = transition_matrix.times(state_post);
  matrixMath.MatrixMult(transitionMatrix, statePost, dp, dp, 1, statePre);
  if( controlMatrix != 0l && cp > 0 )
  {
    // x'(k) = x'(k) + B*u(k)
    //state_pre = control_matrix.gemm(control, state_pre, 1, 1);
    matrixMath.MatrixMult(controlMatrix, statePre, dp, dp, 1, statePre);
  }
  // (2) Project the error covariance ahead
  // update error covariance matrices: temp1 = A*P(k)
  // temp1 = transition_matrix.times(error_cov_post);
  matrixMath.MatrixMult(transitionMatrix, errorCovPost, dp, dp, dp, temp1);
  // Then perform a general matrix multiply:
  // error_cov_pre = temp1.gemm(transition_matrix.transpose(), process_noise_cov, 1, 1);
  // First make a transpose copy of the transition matrix:          
  matrixMath.MatrixTranspose(transitionMatrix, dp, dp, transitionMatrixTranspose);
  gemm(temp1, transitionMatrixTranspose, processNoiseCov, errorCovPre, dp, dp, dp, 1, 1);
  if(bPrintMatrices)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println("Pre");
      printMatrices();
    }
  }
}
void aKalman::correct()
{
  // (1) Compute the Kalman gain
  // temp2 = H*P'(k)
  //        temp2 = measurement_matrix.times(error_cov_pre);
  matrixMath.MatrixMult(measurementMatrix, errorCovPre, mp, dp, dp, temp2);
  // temp3 = temp2*Ht + R 
  // temp3 = temp2.gemm(measurement_matrix.transpose(), measurement_noise_cov, 1, 1);
  // First make a transpose copy of the measurement matrix:
  matrixMath.MatrixTranspose(measurementMatrix, mp, dp, measurementMatrixTranspose);
  gemm(temp2, measurementMatrixTranspose, measurementNoiseCov, temp3, mp, dp, mp, 1, 1);
  // temp4 = inv(temp3)*temp2 = Kt(k) 
  matrixMath.MatrixCopy(temp3, mp, mp, temp3Inverted);
  matrixMath.MatrixInvert(temp3Inverted, mp);                    // NOTE: MATRIX INVERSION DONE IN PLACE - THAT IS WHY WE MADE A TEMP COPY FIRST
  matrixMath.MatrixMult(temp3Inverted, temp2, mp, mp, dp, temp4);
  // temp4 = temp3.solve(temp2);                                 // [NOT IMPLEMENTED IN EITHER JKalman or aKalman]
  // hokus pokus...
  // temp4 = temp3.svd().getU().times(temp2);                    // [NOT IMPLEMENTED IN EITHER JKalman or aKalman]
  // K(k) 
  // gain = temp4.transpose();
  matrixMath.MatrixTranspose(temp4, mp, dp, gain);
  // (2) Update estimate with measurement z(k):
  // temp5 = z(k) - H*x'(k) 
  // temp5 = measurement_matrix.gemm(state_pre, measurement, -1, 1);
  gemm(measurementMatrix, statePre, m, temp5, mp, dp, 1, -1, 1);
   // x(k) = x'(k) + K(k)*temp5 
   // state_post = gain.gemm(temp5, state_pre, 1, 1);
   gemm(gain, temp5, statePre, statePost, dp, mp, 1, 1, 1);
  // (3) Update the error covariance:
  // P(k) = P'(k) - K(k)*temp2 
  // error_cov_post = gain.gemm(temp2, error_cov_pre, -1, 1);
  gemm(gain, temp2, errorCovPre, errorCovPost, dp, mp, dp, -1, 1);
  if(bPrintMatrices)
  {
    if(floidStatus.serialOutput)
    {
      DEBUG_SERIAL.println("Cor");
      printMatrices();
    }
  }
}
// Getters/Setters:
float aKalman::getM(int row, int column)
{
  return m[row * 1 + column];
}
void aKalman::setM(int row, int column, float value)
{
  m[row * 1 + column] = value;
}
float aKalman::getStatePre(int row, int column)
{
  return statePre[row * 1 + column];
}
void aKalman::setStatePre(int row, int column, float value)
{
  statePre[row * 1 + column] = value;
}
float aKalman::getStatePost(int row, int column)
{
  return statePost[row * 1 + column];
}
void aKalman::setStatePost(int row, int column, float value)
{
  statePost[row * 1 + column] = value;
}
// Print:
void aKalman::printMatrices()
{
  if(floidStatus.serialOutput)
  {
    MatrixMath matrixMathZ;
    matrixMathZ.MatrixPrint(m,                            mp,  1, "m");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(statePre,                     dp,  1, "r");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(statePost,                    dp,  1, "o");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(transitionMatrix,             dp, dp, "t");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(transitionMatrixTranspose,    dp, dp, "T");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(measurementMatrix,            mp, dp, "m");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(measurementMatrixTranspose,   dp, mp, "M");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(processNoiseCov,              dp, dp, "N");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(measurementNoiseCov,          mp, mp, "C");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(errorCovPre,                  dp, dp, "R");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(errorCovPost,                 dp, dp, "O");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(gain,                         dp, mp, "G");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(temp1,                        dp, dp, "1");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(temp2,                        mp, dp, "2");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(temp3,                        mp, mp, "3");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(temp4,                        mp, dp, "4");
    DEBUG_SERIAL.println();
    matrixMathZ.MatrixPrint(temp5,                        mp,  1, "5");
    DEBUG_SERIAL.println();
  }
}
void  identityMatrix(float* matrix, int rows, int columns)
{
  identityMatrix(matrix, rows, columns, 1.0);  
}
void  identityMatrix(float* matrix, int rows, int columns, float start)
{
  for(int i=0; i<rows; ++i)
  {
    for(int j=0; j<columns; ++j)
    {
      if(i==j)
      {
        matrix[i*columns + j] = start;
      }
      else
      {
        matrix[i*columns + j] = 0.0;
      }
    }
  }
}
void  zeroMatrix(float* matrix, int rows, int columns)
{
  for(int i=0; i<rows; ++i)
  {
    for(int j=0; j<columns; ++j)
    {
        matrix[i*columns + j] = 0.0;
    }
  }
}
void  identityPlusDerivativesMatrix(float* matrix, int rows, int columns)
{
  // Start with an identity matrix:
  identityMatrix(matrix, rows, columns);
  // And add terms for the differences - these go in the first half rows - note that rows must be even - really this should be a square matrix:
  // e.g.
  //  { 1 0 1 0 }  - these note the difference change if for example this matrix is x, y, dx, dy
  //  { 0 1 0 1 } 
  //  { 0 0 1 0 }
  //  { 0 0 0 1 }
  for(int i=0; i < rows/2; ++i)
  {
    matrix[i*columns + (i + rows/2)] = 1.0;
  }
}
// Notes:
//  A = m x n
//  B = n x p  (B.m = n) (B.n = p)
//  C = m x p
//  X = m x p - result matrix
void gemm(float* A, float* B, float* C, float *X, int m, int n, int p, double alpha, double beta)
{
  if (C == 0l)    // multiplication - rare, but necessary :(
  { 
    for (int j = p; --j >= 0; )
    {
      for (int i = m; --i >= 0; )
      {
        float s = 0;
        for (int k = n; --k >= 0; )
        {
          s += A[i*n+k] * B[k*p+j];
        }
        X[i*p+j] = alpha * s;
      }
    }
  }
  else
  {
    for (int j = p; --j >= 0; )
    {
      for (int i = m; --i >= 0; )
      {
        float s = 0;
        for (int k = n; --k >= 0; )
        {
          s += A[i*n+k] * B[k*p+j];
        }
        X[i*p+j] = alpha * s + beta * C[i*p+j];
      }
    }
  }
}
