//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Altimeter.cpp
// faiglelabs.com
#include <FloidBoard3.h>
float          altimeterAltitudeReadings[ALTIMETER_NUMBER_READINGS]     = {0.0,0.0,0.0,0.0,0.0};
unsigned long  altimeterAltitudeReadingTimes[ALTIMETER_NUMBER_READINGS] = {0ul, 0ul, 0ul, 0ul, 0ul};
boolean setupAltimeter()
{
  floidStatus.altimeterAltitude         = 0.0;
  floidStatus.altimeterAltitudeVelocity = 0.0;
  floidStatus.altimeterAltitudeDelta    = 0;
  floidStatus.altimeterInitialized      = false;
  floidStatus.altimeterAltitudeValid    = false;
  floidStatus.altimeterGoodCount        = 0ul;
  floidStatus.altimeterLastUpdateTime   = 0ul;
  for(int i=0; i<ALTIMETER_NUMBER_READINGS; ++i)
  {
    altimeterAltitudeReadings[i] = 0.0;
  }
  return true;
}
// Make sure this is called before newPYR is cleared, but be sure to have newPYR
//  cleared later so that we don't continue to process the same PYR reading
void loopAltimeter()
{
  if(newPYR)
  {
    // Get our current readings for the altimeter:
    float barometerAltitude = pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].heightSmoothed;
    float gpsAltitude       = floidStatus.gpsZMetersFiltered;
    // Check if Altimeter should be initialized…
    if(floidStatus.debug && floidStatus.debugAltimeter)
    {
      if(floidStatus.altimeterInitialized)
      {
        debugPrintlnToken(FLOID_DEBUG_ALTIMETER_INITIALIZED);
      }
      else
      {
        debugPrintlnToken(FLOID_DEBUG_ALTIMETER_NOT_INITIALIZED);
      }
      if (floidStatus.gpsGoodCount >= ALTIMETER_REQUIRED_GPS_COUNT_FOR_INITIALIZATION)
      {
        debugPrintlnToken(FLOID_DEBUG_ALTIMETER_GPS_COUNT_GOOD);
      }
      else
      {
        debugPrintlnToken(FLOID_DEBUG_ALTIMETER_GPS_COUNT_LOW);
      }
      if(floidStatus.pyrGoodCount >= ALTIMETER_REQUIRED_PYR_COUNT_FOR_INITIALIZATION)
      {
        debugPrintlnToken(FLOID_DEBUG_ALTIMETER_PYR_COUNT_GOOD);
      }
      else
      {
        debugPrintlnToken(FLOID_DEBUG_ALTIMETER_PYR_COUNT_LOW);
      }
    }
    // Uninitialized and finally have enough good counts?
    if(!floidStatus.altimeterInitialized && (floidStatus.gpsGoodCount >= ALTIMETER_REQUIRED_GPS_COUNT_FOR_INITIALIZATION) && (floidStatus.pyrGoodCount >= ALTIMETER_REQUIRED_PYR_COUNT_FOR_INITIALIZATION)) {
      // Initialize one time:
      initializeAltimeter(barometerAltitude, gpsAltitude);
    } else {
        // Initialized?
        if(floidStatus.altimeterInitialized)
        {
          // We can update the GPS only if HDOP is OK...(even though we wish we had VDOP!)
          if(floidStatus.gpsGoodCount >= ALTIMETER_REQUIRED_GPS_COUNT_FOR_UPDATE)
          {
            updateAltimeter(barometerAltitude, gpsAltitude, ALTIMETER_UPDATE_ALPHA);
          }
          // Regardless of update we use the barometer pressure to get an estimate of the current altitude and voila - altimeter!
          floidStatus.altimeterAltitude       = getAltimeterAltitude(barometerAltitude);
          if(floidStatus.altimeterGoodCount >= ALTIMETER_NUMBER_READINGS)  // Min readings to fill out smoothing table...
          {
            floidStatus.altimeterAltitudeValid  = true;
            floidStatus.altimeterLastUpdateTime = millis();
          } else {
             floidStatus.altimeterAltitudeValid    = false;
           }
        }
        else
        {
          floidStatus.altimeterAltitudeValid    = false;
        }
    }
  }
  else
  {
    //  Time out this reading if needed:
    if(millis() > floidStatus.altimeterLastUpdateTime + ALTIMETER_VALIDITY_PERIOD)
    {
      floidStatus.altimeterAltitudeValid    = false;
    }
  }
}
void initializeAltimeter(float barometerAltitude, float gpsAltitude)
{
  if(floidStatus.debug && floidStatus.debugAltimeter)
  {
      debugPrintToken(FLOID_DEBUG_ALTIMETER_INITIALIZING);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrint(barometerAltitude);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrintln(gpsAltitude);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrintln(barometerAltitude - gpsAltitude);
  }
  // Assume this altitude:
  floidStatus.altimeterAltitude      = gpsAltitude;
  // And this barometric difference:
  floidStatus.altimeterAltitudeDelta = barometerAltitude - gpsAltitude;
  // Initialized:
  floidStatus.altimeterInitialized   = true;
  // But not yet valid:
  floidStatus.altimeterAltitudeValid = false;
  // Good count starts at zero:
  floidStatus.altimeterGoodCount     = 0ul;
}
void updateAltimeter(float barometerAltitude, float gpsAltitude, float alpha)
{
  // Calculate the current delta:
  float altitudeUpdateDelta  = barometerAltitude - gpsAltitude;
  // Update the overall delta:
  floidStatus.altimeterAltitudeDelta     = (1-alpha) * floidStatus.altimeterAltitudeDelta + alpha * altitudeUpdateDelta;
  if(floidStatus.debug && floidStatus.debugAltimeter)
  {
    debugPrintToken(FLOID_DEBUG_ALTIMETER_UPDATE);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(barometerAltitude);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(gpsAltitude);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(alpha);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrint(altitudeUpdateDelta);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(floidStatus.altimeterAltitudeDelta);
  }
}
float getAltimeterAltitude(float barometerAltitude)
{
  float altitudeSum = 0.0;
  float altitude    = 0.0;
  if(floidStatus.debug && floidStatus.debugAltimeter)
  {
    debugPrintToken(FLOID_DEBUG_ALTIMETER_BAROMETER_ALTITUDE);
    debugPrint(FLOID_DEBUG_SPACE);
    debugPrintln(barometerAltitude);
  }

  // Are we initialized?
  if(!floidStatus.altimeterInitialized)
  {
    return 0.0;
  }
  // Barometer reading even possibly good?
  if((barometerAltitude < ALTIMETER_BAROMETER_ALTITUDE_OUT_OF_RANGE_MIN) || ( barometerAltitude > ALTIMETER_BAROMETER_ALTITUDE_OUT_OF_RANGE_MAX))
  {
    if(floidStatus.debug && floidStatus.debugAltimeter)
    {
      debugPrintlnToken(FLOID_DEBUG_ALTIMETER_BAROMETER_OUT_OF_RANGE);
    }
    return floidStatus.altimeterAltitude;  // Do not modify - this reading is out of range and is likely in error - better to stick with what we currently have...
  }
  // OK, this one is good enough for government work - denote we are increasing the altimeter good count by one:
  ++floidStatus.altimeterGoodCount;
  if(floidStatus.debug && floidStatus.debugAltimeter)
  {
    debugPrintlnToken(FLOID_DEBUG_ALTIMETER_PRE);
    for(int i=0; i<ALTIMETER_NUMBER_READINGS; ++i)
    {
      debugPrint(i);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrintln(altimeterAltitudeReadings[i]);
    }
  }
  for(int i=0; i<ALTIMETER_NUMBER_READINGS-1; ++i)
  {
    if(floidStatus.debug && floidStatus.debugAltimeter)
    {
      debugPrint(i);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrint(i+1);
    }
    altimeterAltitudeReadings[i]     = altimeterAltitudeReadings[i+1];
    altimeterAltitudeReadingTimes[i] = altimeterAltitudeReadingTimes[i+1];
  }
  if(floidStatus.debug && floidStatus.debugAltimeter)
  {
    debugPrintlnToken(FLOID_DEBUG_ALTIMETER_PRE);
    for(int j=0; j<ALTIMETER_NUMBER_READINGS; ++j)
    {
      debugPrint(j);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrintln(altimeterAltitudeReadings[j]);
    }
  }
  // Put our new reading at the end:
  altimeterAltitudeReadings[ALTIMETER_NUMBER_READINGS-1]     = barometerAltitude - floidStatus.altimeterAltitudeDelta;
  altimeterAltitudeReadingTimes[ALTIMETER_NUMBER_READINGS-1] = millis();
  // Altitude is average of these readings:
  for(int k=0; k<ALTIMETER_NUMBER_READINGS; ++k)
  {
    altitudeSum += (double)altimeterAltitudeReadings[k];
  }
  if(floidStatus.debug && floidStatus.debugAltimeter)
  {
    debugPrintlnToken(FLOID_DEBUG_ALTIMETER_FINAL);
    for(int l=0; l<ALTIMETER_NUMBER_READINGS; ++l)
    {
      debugPrint(l);
      debugPrint(FLOID_DEBUG_SPACE);
      debugPrintln(altimeterAltitudeReadings[l]);
    }
  }
  altitude = altitudeSum / (float)ALTIMETER_NUMBER_READINGS;
  unsigned long altimeterAltitudeReadingTimeDelta = altimeterAltitudeReadingTimes[ALTIMETER_NUMBER_READINGS-1] - altimeterAltitudeReadingTimes[0];
  if(altimeterAltitudeReadingTimeDelta == 0)
  {
    floidStatus.altimeterAltitudeVelocity = 0.0;
  }
  else
  {
    floidStatus.altimeterAltitudeVelocity  = (altimeterAltitudeReadings[ALTIMETER_NUMBER_READINGS-1] - altimeterAltitudeReadings[0])/(float)altimeterAltitudeReadingTimeDelta;
  }
  if(floidStatus.debug && floidStatus.debugAltimeter)
  {
    debugPrintlnToken(FLOID_DEBUG_ALTIMETER_ALTITUDE_AND_VELOCITY);
    debugPrintln(altitude);
    debugPrintln(FLOID_DEBUG_SPACE);
    debugPrintln(altitude);
  }
  return altitude;
}
