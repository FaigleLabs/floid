//----------------------------------------//
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
//----------------------------------------//
// FloidBoard3_Parachute.h:
#ifndef	__FLOIDBOARD3_PARACHUTE_H__
#define	__FLOIDBOARD3_PARACHUTE_H__
// Parachute:
boolean setupParachute();
void    startParachute();
void    loopParachute();
void    stopParachute();
void    deployParachute();
#endif /* __FLOIDBOARD3_PARACHUTE_H__ */
