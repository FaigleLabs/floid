//////////////////////////////////
// FloidBoard3                  //
// faiglelabs.com               //
//////////////////////////////////
////////////////////////////////////////////
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
////////////////////////////////////////////
////////////////////////////////////////////////////
// Target device -> Arduino Mega 2560 or Mega ADK //
////////////////////////////////////////////////////
// ==============================
// FloidBoard 9R1 Hardware notes:
// ==============================
//   Android ADK
//   1 x IMU+BARO
//   1 x GPS
//   4 x Collective Pitch Helis: 3 Servos + 1 ESC each
//   1+ x Batteries:  1 or more in parallel 3-cell LiPo
//   1 x LED status light
//   4 x Bays: with 1 open/closed detector each
//   4 x Pan/Tilt: with first one devoted to camera movement
//   1 x Parachute: (Not implemented / can be implemented from one of the bay releases)
//   1 x Aux Port
// ============================


// Note: This include order is important:
#include "Arduino.h"
#include "FloidBoard.h"
#include <Servo.h>
#include <EEPROM.h>
#include <FloidBoard3.h>

////////////////////////////////////////////////////////////
// Heartbeat, safety and emergency modes                  //
////////////////////////////////////////////////////////////
// The heartbeat timeout period - currently the app is sending a heartbeat every five seconds:
static const int requiredHeartbeatPeriod    = 10000;
// Safety mode flags:
static byte safetyModes           = 0;
// Emergency mode flags:
static byte emergencyModes        = 0;
/* ==========================================================================================
 * Servos are defined here and extern-ed everywhere else to avoid Arduino compilation issues:
 * ========================================================================================== */
Servo            escServo[NUMBER_HELICOPTERS];
Servo            heliServo[NUMBER_HELICOPTERS][NUMBER_HELI_SERVOS];
Servo            panTiltServo[NUMBER_PANTILTS][NUMBER_PANTILT_SERVOS];
/* ====================================
 * Some local interest only globals:
 * ==================================== */
// Force status output:
boolean          forceStatusOutput          = false;
// Heli-Startup:
// Time the ESC's started:
unsigned long    heliStartupModeStartTime   = 0ul;
// Current startup mode step and phase:
int              heliStartupModeCurrentStep = 0;
byte             helisStartupModePhase      = HELI_STARTUP_PHASE_NONE;
// If GPS from Device:
boolean          hasNewDeviceGPS            = false;
// If PYR from Device:
boolean          hasNewDevicePYR            = false;
// For total shutdown:
boolean          totalShutdownMode          = false;

///////////////////////////////////
// Heartbeat and Loop Rate:      //
///////////////////////////////////
boolean          gotHeartbeat               = false;  // Set when we get heartbeat - after this lost communication testing is active
unsigned long    lastHeartbeatTime          = 0ul;
unsigned long    loopRateTestStart          = 0ul;
int              loopRateCount              = 0;

///////////////////////////////////
// Total Shutdown mode:          //
///////////////////////////////////

// Debug physics print heading (set true only at start of goal):
bool  printPhysicsDebugHeaders  = false;

/* =============
 * PMTK Handler:
 * ============= */
PMTKHandler pmtkHandler(&GPS_SERIAL, &DEBUG_SERIAL);
//////////////////////////////////
// NOTE: To finally fix the hardware serial etc warnings, see here and declare i as an unsigned int in HardwareSerial.cpp etc: http://www.arduinoos.com/2014/06/compiler-warnings-part-1/
//////////////////////////////////
#define TEST_GOAL_ID (0)
/* ======
 * SETUP:
 * ====== */
void setup()
{
  // Set our safety modes:
  setSafetyMode(SAFETY_MODE_1_CHECK_PHYSICAL_FLIGHT, true);
  setSafetyMode(SAFETY_MODE_2_CHECK_CONTROLLER_COMMUNICATION, true);
  // Get the current time:
  currentTime            = millis();
  lastStatusSendTime     = currentTime;
  // Set up the floid status:
  setupFloidStatus();
  // We default to serial debug printing even in production:
  floidStatus.serialOutput = true;
#ifdef PRINT_OFFSETS_ONLY
  // Print the offset addresses of the various structures if set and then stop:
  {
    // This enables us to print out the structures that we are sending - we must do this whenever we make
    // changes to these structures - be sure to fix up the print routines on changes also - note that
    // the offsetof code requires that these structures be Plain Old Data Objects so that is why they
    // are structs with external functions instead of classes with constructors and methods
    DEBUG_SERIAL.begin(DEBUG_BAUD_RATE);
    printPacketHeaderOffsets();
    printFloidStatusOffsets();
    printModelStatusOffsets();
    printModelParametersOffsets();
    printCommandResponseOffsets();
    printTestResponseOffsets();
    printDebugMessageOffsets();
    return;
  }
#endif /* PRINT_OFFSETS_ONLY */
  // Set up the board:
  setupSuccess = setupFloidBoard3();
  // Turn on GPS if not on:
  if(!floidStatus.gpsOn) {
    startGPS();
  }
  // Turn on IMU if not on:
  if(!floidStatus.pyrOn) {
    startPYR();
  }
  // Start the camera pan/tilts:
  startCamera();
  // Print the memory:
  if(floidStatus.serialOutput)
  {
    mem();
  }
  // If we start in any non production mode,
  // (see FloidBoard3_Status.cpp where FloidStatus is initialized)
  // then we can also start up with our choice of debug flags here:
  if(!isFloidProductionMode()) {
    if(floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_NON_PRODUCTION);
    }
    /* ============================================================================================================
     * This area is for setting any flags for debug / test mode - these should all be commented out for production:
     * ============================================================================================================ */
    // floidStatus.debug           = true;
    // floidStatus.debugMem        = true;
    // floidStatus.debugGPS        = true;
    // floidStatus.debugPYR        = true;
    // floidStatus.debugPhysics    = true;
    // floidStatus.debugPanTilt    = true;
    // floidStatus.debugNM         = true;
    // floidStatus.debugCamera     = true;
    // floidStatus.debugDesignator = true;
    // floidStatus.debugAUX        = true;
    // floidStatus.debugHelis      = true;
    /* ============================================================================================================ */
  }
  // Loop rate count starts now:
  loopRateTestStart = millis();
  loopRateCount = 0;
}
//////////////////////////////////
// LOOP:
//////////////////////////////////
void loop()
{
  // Get the current time:
  currentTime = millis();
  // Are we in totalShutdownMode?
  if(totalShutdownMode)
  {
    // Yes - we do nothing but blink led:
    debugPrintlnToken(FLOID_DEBUG_TOKEN_TOTAL_SHUTDOWN_MODE);
    flipLED(FLOID_LED_PIN_INDEX);
    return;
  }
  ++floidStatus.loopCount;
  forceStatusOutput = false;  // Reset this
  // If we are only printing the offsets, then that is already done in setup() so we just return...
#ifdef PRINT_OFFSETS_ONLY
  {
    return;
  }
#endif /* PRINT_OFFSETS_ONLY */
  // If we did not setup right, we just print error message and return:
  if(!setupSuccess)
  {
    debugPrintlnToken(FLOID_DEBUG_LABEL_FLOID_SETUP_FAILURE);
    return;
  }
  // If debugging memory display for this loop:
  if(floidStatus.debugMem)
  {
    displayFreeMemory("");
  }
  // Process - order is as follows:
  // 1. Process command from the serial (aux) input if exists - UNIMPLEMENTED - intended use for specific hardware aux platforms as needed
  // 2. Process command from the controller (Android) if exists
  // 3. Batteries
  // 4. Currents
  // 5. GPS
  // 6. PYR
  // 7. Altimeter
  // 8. Altitude selection and velocity calculation from altimeter or gps depending on validities
  // 9. Payloads
  // 10. AUX
  // 11. Parachute
  // 12. Camera (Pan/Tilt)
  // 13. Designator (Pan/Tilt)
  // 14. Pan/Tilts
  // 15. if Heli-Startup mode -> set Low ESC & leave Heli-Startup mode
  // 16. Set to not skip-physics
  // 16. Check Safety Modes
  // 17. if Handle Emergency Mode -> set to skip-physics
  // 18. if Has Goal and Floid Period has expired 
  //        update goal ticks
  //        if not skip-physics -> calculate physics
  //        reset Floid Period
  // 19. if status send time expired or we had received a new command or some part set force status output ->
  //          blink (flip) the led
  //          calculate loop rates and free mem
  //          send floid status
  //          reset those flags and times
  // 20. if debug mem is on then output the free memory to serial
  // ----------------------------------

  // Process serial commands: (Implement this for serial command processing from aux)
  /* boolean newSerialCommand = */ loopGetSerialCommand();
  // Process commands from Android:
  /* boolean newCommand = */ loopGetControllerCommand();
  // Batteries:
  loopBatteries();
  // Currents:
  loopCurrents();
  // XYZ:
  boolean newGPS = loopGPS();
  if(floidStatus.debugMem)
  {
    displayFreeMemory("l");
  }
  // PYR:
  loopPYR();
#ifdef USE_ALTIMETER
  // Altimeter:
  loopAltimeter();  // Updates if valid, initialized and newPYR
  // Update from GPS if no altimeter:
  if(newGPS && (!floidStatus.altimeterInitialized || !floidStatus.altimeterAltitudeValid))
  {
    floidStatus.altitude         = floidStatus.gpsZMetersFiltered;  // Altimeter not working - use GPS altitude filtered and velocity
    floidStatus.altitudeVelocity = gpsZVelocity;
    DEBUG_SERIAL.print("A1 ");
    DEBUG_SERIAL.println(floidStatus.altitude);
  }
  // Update from altimeter if working and new PYR:
  if(newPYR && floidStatus.altimeterInitialized && floidStatus.altimeterAltitudeValid)
  {
    floidStatus.altitude         = floidStatus.altimeterAltitude;     // Altimeter working - use altimeter altitude
    floidStatus.altitudeVelocity = floidStatus.altimeterAltitudeVelocity;
    DEBUG_SERIAL.print("A2 ");
    DEBUG_SERIAL.println(floidStatus.altitude);
  }
#else /* USE_ALTIMETER */
  // New code replace the altimeter with Kalman filtered GPS:
  if(newGPS) {
      floidStatus.altimeterInitialized = true;
      floidStatus.altimeterAltitudeValid = true;
      floidStatus.altitude = floidStatus.gpsZMetersFiltered;
      floidStatus.altitudeVelocity = gpsZVelocity;
      floidStatus.altimeterGoodCount = floidStatus.gpsGoodCount;
  }
#endif /* USE_ALTIMETER */
  // Reset PYR:
  newPYR = false;
  // Payloads:
  loopPayloads();
  // Aux:
  loopAux();
  // Parachute:
  loopParachute();
  // Camera:
  loopCamera();
  // Designator:
  loopDesignator();
  // PanTilts:
  loopPanTilts();
  // Update our time:
  currentTime = millis();
  // Check the safety modes:
  checkSafetyModes();
  boolean skipPhysics = false;
  if(handleEmergencyMode())  // This returns a boolean if we are not to process any further...
  {
  //      debugPrint('E');
  //      debugPrintln('M');
        skipPhysics = true;
  }
  if(!skipPhysics) {
    // If we are in heli startup mode, process it:
    if(floidStatus.heliStartupMode) {
      processStartupMode();
    } else {
      // We are not in heli startup mode:
      // Have goal?
      if(floidStatus.hasGoal)
      {
        // Has our period expired for calculating the physics again?
        if((currentTime - lastTime) >= FLOID_PERIOD)
        {
          // OK, enough time has now passed..do something :)
          // Update the tick count for this goal:
          floidStatus.goalTicks = currentTime - floidStatus.goalStartTime;
          if(!skipPhysics)
          {
//          debugPrint('C');
//          debugPrintln('P');
            calculatePhysics();
          }
          lastTime = currentTime;    // Store the time
        }
      } else {
//      debugPrint('N');
//      debugPrintln('G');
      }
    }
  }
  // Update our time:
  currentTime = millis();
  // Update our loop count:
  ++loopRateCount;
  // Do we need to send a status?
  if(((currentTime - lastStatusSendTime) > (unsigned long)currentStatusRateMillis) || (forceStatusOutput) /* || (newCommand) || (newSerialCommand)*/)    // Floid status send period expired or we have new command
  {
    if(currentTime > loopRateTestStart + LOOP_RATE_PERIOD) {
      floidStatus.loopRate = (((float)loopRateCount) / (float)(currentTime - loopRateTestStart)) * 1000.0;
      loopRateTestStart = currentTime;
      loopRateCount = 0;
    }
#ifdef BLINK_LED_ON_STATUS_COMM
    // Flip the LED:
    flipLED(FLOID_LED_PIN_INDEX);
#endif /* BLINK_LED_ON_STATUS_COMM */
    // Send a Floid status message:
    DEBUG_SERIAL.println("FS");
    sendFloidStatus();
    // Send a Floid model status message:
    DEBUG_SERIAL.println("MS");
    sendModelStatus();
    // Reset the loop count since last status
    floidStatus.loopCount  = 0;
    // Reset our counters:
    lastStatusSendTime    = currentTime;
  }
  if(floidStatus.debugMem)
  {
    displayFreeMemory("");
  }
}
// Physics is calculated if:
//  hasGoal is set, skipPhysics is not set and after a FLOID_PERIOD interval
void calculatePhysics()
{
  float timeDelta = currentTime - lastTime;
  // Make sure we have a good period:
  // Too short? - Should not happen
  if(timeDelta<CALCULATE_PHYSICS_TOO_SHORT_PERIOD)
  {
    return; 
  }
  // Too long? - Could happen
  if(timeDelta > CALCULATE_PHYSICS_TOO_LONG_PERIOD)
  {
    return;
  }
  // Still too long?
  if(timeDelta > CALCULATE_PHYSICS_MAX_PERIOD)
  {
    timeDelta = CALCULATE_PHYSICS_MAX_PERIOD;
  }
 // Acceleration multiplier - opposite of the time period calculated once per convenience:
  float accelerationMultiplier = (floidModelParameters.accelerationMultiplierScale)/timeDelta;  // Was 1000 - way too sensitive!!

//  debugPrint('C');
//  debugPrint('P');
//  debugPrintln('2');
  bool physicsTestMode = false;
  // Run various tests or not:
  if(isRunModePhysicsTestXY()) {
    physicsTestMode = true;
    physicsTestXY1();
  }
  if(isRunModePhysicsTestAltitude1()) {
    physicsTestMode = true;
    physicsTestAltitude1();
  }
  if(isRunModePhysicsTestAltitude2()) {
    physicsTestMode = true;
    physicsTestAltitude2();
  }
  if(isRunModePhysicsTestHeading1()) {
    physicsTestMode = true;
    physicsTestHeading1();
  }
  if(isRunModePhysicsTestPitch1()) {
    physicsTestMode = true;
    physicsTestPitch1();
  }
  if(isRunModePhysicsTestRoll1()) {
    physicsTestMode = true;
    physicsTestRoll1();
  }
  // Calculate all distances and angles:
  floidStatus.distanceToTarget                  = calculateDistanceToTarget();    // In meters, non-negative
  floidStatus.altitudeToTarget                  = calculateAltitudeToTarget();    // In meters
  floidStatus.deltaHeadingAngleToTarget         = calculateDeltaHeadingAngleToTarget(calculateAngleOfTarget()); // delta heading angle to target In degrees
  floidStatus.orientationToGoal                 = calculateOrientationToGoal();   // orientation from rotational goal In degrees
  floidStatus.attackAngle                       = calculateAttackAngle();         // In degrees

  if(isTestModePhysicsNoAttackAngle()) {
    floidStatus.attackAngle = 0;  //  no attack angle
  }
  // Check to see if we have completed our goal if we have one:
  if(!physicsTestMode && floidStatus.hasGoal)
  {
    switch(floidStatus.goalState)
    {
      case GOAL_STATE_NONE:
      case GOAL_STATE_COMPLETE_SUCCESS:
      case GOAL_STATE_COMPLETE_ERROR:
      {
        // Do nothing
      }
      break;
      case GOAL_STATE_START: // Falls through to next on purpose
      {
        floidStatus.goalState = GOAL_STATE_IN_PROGRESS;
        forceStatusOutput     = true;
      }
      case GOAL_STATE_IN_PROGRESS:
      {
        if(floidFlightGoalAchieved())
        {
          if(!floidStatus.landMode) {
            floidStatus.goalState = GOAL_STATE_COMPLETE_SUCCESS;
            forceStatusOutput     = true;
          } else {
            // Land Mode - decrease our target altitude if we arrived there...Land mode completes differently:
            floidStatus.goalZ -= 5;
          }
        }
        else
        {
          // TODO [GOAL_TIME_OUT] - IMPLEMENT THIS BELOW...
          // Check if goal timed out, set to error and force a status send
          // TODO [GOAL_TIME_OUT] Potentially have a different time out for flight goal, height goal and angle goal but for now they do not time out
          // forceStatusOutput = true;
        }
      }
      break;
      default:
      {
        // Unknown goal state - logic error - shutdown:
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_TOKEN_UNKNOWN_GOAL_STATE);
          debugPrintln(floidStatus.goalState);
        }
        floidStatus.goalState = GOAL_STATE_NONE;
      }
      break;
    }
  }
  if(!physicsTestMode) {
    if(floidStatus.inFlight)
    {
      // In air:
      if(floidStatus.landMode)
      {
        // Landing - determine if we have landed...
        if(checkIfWeJustLanded())
        {
          floidStatus.landMode = false;
          floidStatus.inFlight = false;
          for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex) {
            // Change the cyclics and collectives to the default values since we are on the ground now.
            setDefaultHeliCyclicCollectiveValues(heliIndex);
          }
          setGoalState(GOAL_STATE_COMPLETE_SUCCESS);
          floidStatus.hasGoal = false;
          forceStatusOutput   = true;
          return; // We do not calculate the physics...
        }
      }
    }
    else
    {
      // On ground:
      if(floidStatus.liftOffMode)
      {
        if(checkIfWeJustLiftedOff())
        {
          floidStatus.inFlight    = true;
          floidStatus.liftOffMode = false;
          forceStatusOutput       = true;
        }
      }
      else
      {
        switch(floidStatus.mode)
        {
          case FLOID_MODE_TEST:
          {
            // Do nothing - all ok in test mode...
          }
          break;
          case FLOID_MODE_MISSION:
          {
            // In mission mode the state must be lift-off, land or in-air and it is not, so error:
            setGoalState(GOAL_STATE_COMPLETE_ERROR);
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_TOKEN_GOAL_COMPLETED_ERROR);
            }
            floidStatus.hasGoal      = false;
            forceStatusOutput        = true;
            return; // Skip the remaining physics calculation
          }
          break;
          default:
          {
            // Bad mode:
            setGoalState(GOAL_STATE_COMPLETE_ERROR);
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_TOKEN_GOAL_COMPLETED_MODE_ERROR);
            }
            floidStatus.hasGoal      = false;
            forceStatusOutput        = true;
            return; // Skip the remaining physics calculation
          }
          break;
        }
      }
    }
  }
  // OK, we only call calculatePhysics if we have a goal and we are still here so time to make the donuts:
  // Reset our desired deltas to zero:
  for(int i=0; i<NUMBER_HELICOPTERS; ++i)
  {
    floidModelStatus.floidModelCollectiveDeltaOrientation[i]     = 0.0;
    floidModelStatus.floidModelCollectiveDeltaAltitude[i]        = 0.0;
    floidModelStatus.floidModelCollectiveDeltaHeading[i]         = 0.0;
    floidModelStatus.floidModelCollectiveDeltaTotal[i]           = 0.0;
  }
  // Calculate physics pitch deltas:
  // ===============================
  floidStatus.targetOrientationPitch      = keepInDegreeRange(floidStatus.attackAngle * cos(ToRad(-floidStatus.deltaHeadingAngleToTarget)));  // In degrees
  floidStatus.targetOrientationPitchDelta = keepInDegreeRange(floidStatus.targetOrientationPitch - pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitchSmoothed);  // In degrees
  floidStatus.targetPitchVelocity         = calculateTargetPitchVelocity(floidStatus.targetOrientationPitchDelta);  // In degrees/sec
  if(isTestModePhysicsNoPitch()) {
    floidStatus.targetPitchVelocity         = 0;  // In degrees/sec
  }
  floidStatus.targetPitchVelocityDelta    = clampVelocityDelta((floidStatus.targetPitchVelocity - floidStatus.pitchVelocity)*accelerationMultiplier, -PITCH_VELOCITY_DELTA_CLAMP, PITCH_VELOCITY_DELTA_CLAMP);  // In degrees/sec^2
  // Calculate physics roll deltas:
  // ==============================
  floidStatus.targetOrientationRoll       = keepInDegreeRange(floidStatus.attackAngle * sin(ToRad(-floidStatus.deltaHeadingAngleToTarget))); // In degrees
  floidStatus.targetOrientationRollDelta  = keepInDegreeRange(floidStatus.targetOrientationRoll  - pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].rollSmoothed);  // In degrees
  floidStatus.targetRollVelocity          = calculateTargetRollVelocity(floidStatus.targetOrientationRollDelta);  // In degrees/sec
  if(isTestModePhysicsNoRoll()) {
    floidStatus.targetRollVelocity          = 0;  // In degrees/sec
  }
  floidStatus.targetRollVelocityDelta     = clampVelocityDelta((floidStatus.targetRollVelocity  - floidStatus.rollVelocity)*accelerationMultiplier, -ROLL_VELOCITY_DELTA_CLAMP, ROLL_VELOCITY_DELTA_CLAMP);   // In degrees/sec^2
  // Set collective deltas from pitch and roll as Orientation:
  setCollectiveDeltasOrientation(floidStatus.targetPitchVelocityDelta, floidStatus.targetRollVelocityDelta); // In degrees
  // Calculate physics altitude deltas:
  // ==================================
  floidStatus.targetAltitudeVelocity      = calculateTargetAltitudeVelocity(floidStatus.altitudeToTarget);  // in: m, return: m/s
  if(isTestModePhysicsNoAltitude()) {
    floidStatus.targetAltitudeVelocity      = 0;  // In m/sec
  }
  floidStatus.targetAltitudeVelocityDelta = clampVelocityDelta((floidStatus.targetAltitudeVelocity - floidStatus.altitudeVelocity)*accelerationMultiplier, -ALTITUDE_VELOCITY_DELTA_CLAMP, ALTITUDE_VELOCITY_DELTA_CLAMP); // In m/s^2
  setCollectiveDeltasAltitude(floidStatus.targetAltitudeVelocityDelta); // In m/s^2
  // Calculate physics heading deltas:
  // =================================
  floidStatus.targetHeadingVelocity       = calculateTargetHeadingVelocity(floidStatus.orientationToGoal); // In degrees/sec
  if(isTestModePhysicsNoHeading()) {
    floidStatus.targetHeadingVelocity       = 0;  // In degrees/sec
  }
  floidStatus.targetHeadingVelocityDelta  = clampVelocityDelta((floidStatus.targetHeadingVelocity - floidStatus.headingVelocity)*accelerationMultiplier, -HEADING_VELOCITY_DELTA_CLAMP, HEADING_VELOCITY_DELTA_CLAMP); // In degrees/sec^2
  // If we are in COUNTER rotation mode, we use the collective to handle the rotation - otherwise not
  if(floidModelParameters.floidModelRotationMode == FLOID_ROTATION_MODE_COUNTER)
  {
    if(floidStatus.debugPhysics && floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_TOKEN_COUNTER_ROTATION_MODE);
    }
    setCollectiveDeltasHeading(floidStatus.targetHeadingVelocityDelta);
  }
  else
  {
    // All same mode, we use the cyclics below:
    if(floidStatus.debugPhysics && floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_TOKEN_ALL_SAME_ROTATION_MODE);
    }
    for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
    {
      floidModelStatus.floidModelCollectiveDeltaHeading[heliIndex] = 0.0;
    }
  }
  // Accumulate collective deltas:
  // =============================
  accumulateCollectiveDeltas();
  // Calculate physics xy:
  // ===========================
  floidStatus.targetXYVelocity            = calculateTargetXYVelocity(floidStatus.distanceToTarget);
  if(isTestModePhysicsNoXY()) {
    floidStatus.targetXYVelocity         = 0;
  }
  if(floidStatus.debug && floidStatus.debugPhysics)  {
    // Are we supposed to print the debug physics headings:
    if(isTestModePrintDebugPhysicsHeadings())      {
        printPhysicsDebugHeadersOut();
    }
    debugPrint(currentTime - lastTime);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.gpsXDegreesDecimalFiltered);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.gpsYDegreesDecimalFiltered);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.altitude);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.goalX);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.goalY);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.goalZ);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(accelerationMultiplier);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.distanceToTarget);              // DT ok
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.altitudeToTarget);              // AT ok
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingSmoothed);     // Heading in compass
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.deltaHeadingAngleToTarget);           // OT
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.goalAngle);                     // Goal Heading
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.orientationToGoal);             // OG
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetOrientationPitch);        // TP
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetOrientationRoll);         // TR
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitchSmoothed);  // ps
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetOrientationPitchDelta);   // pd
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].rollSmoothed);  // rs
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetOrientationRollDelta);    // rd
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetPitchVelocity);           // pv
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetRollVelocity);            // rv
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetPitchVelocityDelta);      // pd
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetRollVelocityDelta);       // rd
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetAltitudeVelocity);        // av
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetAltitudeVelocityDelta);   // ad
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.headingVelocity);               // hv
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetHeadingVelocity);         // thv
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetHeadingVelocityDelta);    // thvd
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.velocityOverGround);            // vog
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.targetXYVelocity);              // txyv
    debugPrint(FLOID_DEBUG_COMMA);
  }
  // Calculate per heli per servo weighted projection to target and apply:
  calculateCyclics(floidStatus.targetXYVelocity, floidStatus.targetHeadingVelocityDelta);
  // Calculate servos from collectives and cyclics and also set ESC from collective:
  // ===============================================================================
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    floidModelCalculateHeliServosFromCollectivesAndCyclics(heliIndex, true);  // And write to servos
    floidModelSetEscFromCollective(heliIndex);
  }
  if(floidStatus.debug && floidStatus.debugPhysics)
  {
    for(int i=0; i<NUMBER_HELICOPTERS; ++i)
    {
      debugPrint(floidModelStatus.floidModelCollectiveDeltaOrientation[i]);
      debugPrint(FLOID_DEBUG_COMMA);
      debugPrint(floidModelStatus.floidModelCollectiveDeltaAltitude[i]);
      debugPrint(FLOID_DEBUG_COMMA);
      debugPrint(floidModelStatus.floidModelCollectiveDeltaHeading[i]);
      debugPrint(FLOID_DEBUG_COMMA);
      debugPrint(floidModelStatus.floidModelCollectiveDeltaTotal[i]);
      if(i<(NUMBER_HELICOPTERS-1))
      {
        debugPrint(FLOID_DEBUG_COMMA);
      }
      else
      {
        debugPrintln();
      }
    }
  }
  if(floidStatus.debug && floidStatus.debugMem)
  {
    displayFreeMemory(FLOID_DEBUG_P2);
  }
  // Verify the physics model somewhat:
  if(isTestModeCheckPhysicsModel()) {
    checkPhysicsModel();
  }
  return;
}

// Process the heli startup mode through the phases:
void processStartupMode() {
  if(helisStartupModePhase == HELI_STARTUP_PHASE_START_ESCS) {
    // Delay until the ESCs have time to start before setting the default low:
    if(millis() > (heliStartupModeStartTime + HELI_STARTUP_MODE_ESC_ON_DELAY)) {
      for(int i=0;i<NUMBER_HELICOPTERS; ++i) {
        setESCValue(i, floidModelParameters.floidModelESCServoDefaultLowValue);
      }
      helisStartupModePhase = HELI_STARTUP_PHASE_POWER_HELIS; // Move to next phase
      heliStartupModeStartTime = millis();
      heliStartupModeCurrentStep = 0;  // Ensure we start at zero for next phase...
    }
  } else {
    // This will start on the next cycle after the phase change above:
    if(helisStartupModePhase == HELI_STARTUP_PHASE_POWER_HELIS) {
      // Slowly increase the collective from the default value to HELI_STARTUP_COLLECTIVE_TARGET in heliStartupModeNumberSteps of heliStartupModeStepTick each:
      if(heliStartupModeCurrentStep < floidModelParameters.heliStartupModeNumberSteps) {
        // Is it time for the next step?
        if((int)(millis()-heliStartupModeStartTime) >= floidModelParameters.heliStartupModeStepTick) {
          flipLED(FLOID_LED_PIN_INDEX);
          // Calculate the startup collective value as above:
          float startupCollectiveValue = (HELI_STARTUP_COLLECTIVE_TARGET * (heliStartupModeCurrentStep+1)) / floidModelParameters.heliStartupModeNumberSteps;
          // Set the collective to the value and calculate and set it:
          for(int heli=0;heli<NUMBER_HELICOPTERS; ++heli) {
            floidModelStatus.floidModelCollectiveValue[heli] = startupCollectiveValue;
            // Set the collective servos:
            floidModelCalculateHeliServosFromCollectivesAndCyclics(heli, true);
            // Set the collective ESCs:
            floidModelSetEscFromCollective(heli);
          }
          // Increase the current step
          heliStartupModeCurrentStep++;
          // And increase the timer:
          heliStartupModeStartTime = millis();
        }
      } else {
        // We are done:
        floidStatus.heliStartupMode = false;
        helisStartupModePhase = HELI_STARTUP_PHASE_NONE; // Reset this
        heliStartupModeCurrentStep = 0; // Reset this
        // Only here do we set the helis to on finally:
        floidStatus.helisOn = true;
      }
    }
  }
}
void printPhysicsDebugHeadersOut() {
    printPhysicsDebugHeaders = false;
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TICKS);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_X);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_Y);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_Z);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_X_GOAL);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_Y_GOAL);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_Z_GOAL);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ACCELERATION_MULTIPLIER);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_DISTANCE_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ALTITUDE_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_HEADING);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ORIENTATION_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_HEADING_GOAL);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ORIENTATION_TO_GOAL);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_ORIENTATION_PITCH);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_ORIENTATION_ROLL);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_PITCH_SMOOTHED);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_ORIENTATION_PITCH_DELTA);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ROLL_SMOOTHED);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_ORIENTATION_ROLL_DELTA);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_PITCH_VELOCITY);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_ROLL_VELOCITY);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_PITCH_VELOCITY_DELTA);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_ROLL_VELOCITY_DELTA);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_ALTITUDE_VELOCITY);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_ALTITUDE_VELOCITY_DELTA);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_HEADING_VELOCITY);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_HEADING_VELOCITY);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_HEADING_VELOCITY_DELTA);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_VELOCITY_OVER_GROUND);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_TARGET_XY_VELOCITY);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_HELI);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ANGULAR_OFFSET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_HELI);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ANGULAR_OFFSET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_HELI);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ANGULAR_OFFSET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_HELI);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ANGULAR_OFFSET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_OFFSET_TO_TARGET);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_FOR_HEADING_ROTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_TARGET_PROJECTION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_SERVO_CYCLIC_VALUE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_ORIENTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_ALTITUDE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_HEADING);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_TOTAL);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_ORIENTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_ALTITUDE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_HEADING);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_TOTAL);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_ORIENTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_ALTITUDE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_HEADING);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_TOTAL);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_ORIENTATION);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_ALTITUDE);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_HEADING);
    debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_DELTA_TOTAL);
    debugPrintln();
}

float clampVelocityDelta(float value, float clampMin, float clampMax)
{
  // Swap?
  if(clampMin>clampMax)
  {
    float  a = clampMin;
    clampMin = clampMax;
    clampMax = a;
  }
  if(value < clampMin) return clampMin;
  if(value > clampMax) return clampMax;
  return value;
}

boolean checkPhysicsModel()
{
  boolean isOk = true;
  // Check model calculations:
  // Check smoothed roll and pitches:
  isOk |= checkModelRange(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitchSmoothed, DEGREE_ANGLE_MIN,              DEGREE_ANGLE_MAX,                                   FLOID_DEBUG_LABEL_PITCH_SMOOTHED,                 -1, -1);
  isOk |= checkModelRange(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].rollSmoothed,  DEGREE_ANGLE_MIN,              DEGREE_ANGLE_MAX,                                   FLOID_DEBUG_LABEL_ROLL_SMOOTHED,                  -1, -1);
  // Attack angle, pitch, roll:
  isOk |= checkModelRange(floidStatus.attackAngle,                                 -floidModelParameters.floidModelAttackAngleMaxValue,  floidModelParameters.floidModelAttackAngleMaxValue, FLOID_DEBUG_LABEL_ATTACK_ANGLE, -1, -1);
  isOk |= checkModelRange(floidStatus.targetOrientationPitch,                      -floidStatus.attackAngle,      floidStatus.attackAngle,                            FLOID_DEBUG_LABEL_TARGET_ORIENTATION_PITCH,       -1, -1);
  isOk |= checkModelRange(floidStatus.targetOrientationRoll,                       -floidStatus.attackAngle,      floidStatus.attackAngle,                            FLOID_DEBUG_LABEL_TARGET_ORIENTATION_ROLL,        -1, -1);
  isOk |= checkModelRange(floidStatus.targetOrientationPitchDelta,                 DEGREE_ANGLE_MIN,              DEGREE_ANGLE_MAX,                                   FLOID_DEBUG_LABEL_TARGET_ORIENTATION_PITCH_DELTA, -1, -1);
  isOk |= checkModelRange(floidStatus.targetOrientationRollDelta,                  DEGREE_ANGLE_MIN,              DEGREE_ANGLE_MAX,                                   FLOID_DEBUG_LABEL_TARGET_ORIENTATION_ROLL_DELTA,  -1, -1);
  isOk |= checkModelRange(floidStatus.targetPitchVelocity,                         -floidModelParameters.floidModelPitchTargetVelocityMaxValue, floidModelParameters.floidModelPitchTargetVelocityMaxValue, FLOID_DEBUG_LABEL_TARGET_PITCH_VELOCITY, -1, -1);
  isOk |= checkModelRange(floidStatus.targetRollVelocity,                          -floidModelParameters.floidModelRollTargetVelocityMaxValue,  floidModelParameters.floidModelRollTargetVelocityMaxValue,  FLOID_DEBUG_LABEL_TARGET_ROLL_VELOCITY, -1, -1);
  isOk |= checkModelRange(floidStatus.targetPitchVelocityDelta,                    -PITCH_VELOCITY_DELTA_CLAMP,   PITCH_VELOCITY_DELTA_CLAMP,                         FLOID_DEBUG_LABEL_TARGET_PITCH_VELOCITY_DELTA,    -1, -1);
  isOk |= checkModelRange(floidStatus.targetRollVelocityDelta,                     -ROLL_VELOCITY_DELTA_CLAMP,    ROLL_VELOCITY_DELTA_CLAMP,                          FLOID_DEBUG_LABEL_TARGET_ROLL_VELOCITY_DELTA,     -1, -1);
  // Altitude:
  isOk |= checkModelRange(floidStatus.altitudeToTarget,                            -32000.0,                      32000.0,                                            FLOID_DEBUG_LABEL_ALTITUDE_TO_TARGET,             -1, -1);   // Arbitrarily large distances
  isOk |= checkModelRange(floidStatus.targetAltitudeVelocity,                      -floidModelParameters.floidModelAltitudeTargetVelocityMaxValue, floidModelParameters.floidModelAltitudeTargetVelocityMaxValue, FLOID_DEBUG_LABEL_TARGET_ALTITUDE_VELOCITY, -1, -1);
  isOk |= checkModelRange(floidStatus.targetAltitudeVelocityDelta,                 -ALTITUDE_VELOCITY_DELTA_CLAMP, ALTITUDE_VELOCITY_DELTA_CLAMP,                     FLOID_DEBUG_LABEL_TARGET_ALTITUDE_VELOCITY_DELTA, -1, -1);
  // Orientations:
  isOk |= checkModelRange(floidStatus.deltaHeadingAngleToTarget,                         DEGREE_ANGLE_MIN,              DEGREE_ANGLE_MAX,                                   FLOID_DEBUG_LABEL_ORIENTATION_TO_TARGET,          -1, -1);
  isOk |= checkModelRange(floidStatus.orientationToGoal,                           DEGREE_ANGLE_MIN,              DEGREE_ANGLE_MAX,                                   FLOID_DEBUG_LABEL_ORIENTATION_TO_GOAL,            -1, -1);
  // Heading:
  isOk |= checkModelRange(floidStatus.targetHeadingVelocity,                       -floidModelParameters.floidModelHeadingTargetVelocityMaxValue, floidModelParameters.floidModelHeadingTargetVelocityMaxValue, FLOID_DEBUG_LABEL_TARGET_HEADING_VELOCITY, -1, -1);
  isOk |= checkModelRange(floidStatus.targetHeadingVelocityDelta,                  -HEADING_VELOCITY_DELTA_CLAMP,  HEADING_VELOCITY_DELTA_CLAMP,                      FLOID_DEBUG_LABEL_TARGET_HEADING_VELOCITY_DELTA,  -1, -1);
  // Distance / XY:
  isOk |= checkModelRange(floidStatus.distanceToTarget,                            0.0,                           HALF_CIRCUMFERENCE_OF_EARTH_IN_METERS,              FLOID_DEBUG_LABEL_DISTANCE_TO_TARGET,             -1, -1);   // Should not be further than half the circumference of the earth...
  isOk |= checkModelRange(floidStatus.targetXYVelocity,                            -floidModelParameters.floidModelDistanceTargetVelocityMaxValue, floidModelParameters.floidModelDistanceTargetVelocityMaxValue, FLOID_DEBUG_LABEL_TARGET_XY_VELOCITY,             -1, -1);
  // Check per heli items:
  for(int heliIndex = 0; heliIndex < NUMBER_HELICOPTERS; ++heliIndex)
  {
    // Check deltas etc:
    float testRange1 = -PITCH_VELOCITY_DELTA_CLAMP * floidModelParameters.floidModelTargetPitchVelocityAlpha - ROLL_VELOCITY_DELTA_CLAMP * floidModelParameters.floidModelTargetRollVelocityAlpha;
    float testRange2 = PITCH_VELOCITY_DELTA_CLAMP * floidModelParameters.floidModelTargetPitchVelocityAlpha + ROLL_VELOCITY_DELTA_CLAMP * floidModelParameters.floidModelTargetRollVelocityAlpha;
    isOk |= checkModelRange(floidModelStatus.floidModelCollectiveDeltaOrientation[heliIndex], testRange1, testRange2, FLOID_DEBUG_LABEL_COLLECTIVE_DELTA_ORIENTATION, heliIndex, -1);
    isOk |= checkModelRange(floidModelStatus.floidModelCollectiveDeltaAltitude[heliIndex], -ALTITUDE_VELOCITY_DELTA_CLAMP * floidModelParameters.floidModelTargetAltitudeVelocityAlpha,
                                                                                               ALTITUDE_VELOCITY_DELTA_CLAMP * floidModelParameters.floidModelTargetAltitudeVelocityAlpha, FLOID_DEBUG_LABEL_COLLECTIVE_DELTA_ALTITUDE, heliIndex, -1);
    isOk |= checkModelRange(floidModelStatus.floidModelCollectiveDeltaHeading[heliIndex], -HEADING_VELOCITY_DELTA_CLAMP * floidModelParameters.floidModelTargetHeadingVelocityAlpha,
                                                                                               HEADING_VELOCITY_DELTA_CLAMP * floidModelParameters.floidModelTargetHeadingVelocityAlpha, FLOID_DEBUG_LABEL_COLLECTIVE_DELTA_HEADING, heliIndex, -1);
    isOk |= checkModelRange(floidModelStatus.floidModelCollectiveDeltaTotal[heliIndex], -1.0, 1.0, FLOID_DEBUG_LABEL_COLLECTIVE_DELTA_TOTAL, heliIndex, -1);
    isOk |= checkModelRange(floidModelStatus.floidModelCollectiveValue[heliIndex], 0.0, 1.0, FLOID_DEBUG_LABEL_COLLECTIVE_VALUE, heliIndex, -1);
    isOk |= checkModelRange(floidModelStatus.floidModelCalculatedCollectiveBladePitch[heliIndex], floidModelParameters.floidModelCollectiveMinValue, floidModelParameters.floidModelCollectiveMaxValue, FLOID_DEBUG_LABEL_CALCULATED_COLLECTIVE_BLADE_PITCH, heliIndex, -1);

    // Check per servo items:
    for(int servoIndex = 0; servoIndex < NUMBER_HELI_SERVOS; ++servoIndex)
    {
      isOk |= checkModelRange(floidModelStatus.floidModelCyclicValue[heliIndex][servoIndex],                -1.0,                                             1.0,                                               FLOID_DEBUG_LABEL_CYCLIC_VALUE,                  heliIndex, servoIndex);
      isOk |= checkModelRange(floidModelStatus.floidModelCalculatedCyclicBladePitch[heliIndex][servoIndex], -floidModelParameters.floidModelCyclicRangeValue, floidModelParameters.floidModelCyclicRangeValue,   FLOID_DEBUG_LABEL_CALCULATED_CYCLIC_BLADE_PITCH, heliIndex, servoIndex);
      isOk |= checkModelRange(floidModelStatus.floidModelCalculatedBladePitch[heliIndex][servoIndex],       floidModelParameters.floidModelCollectiveMinValue - floidModelParameters.floidModelCyclicRangeValue,
                                                                                                            floidModelParameters.floidModelCollectiveMaxValue + floidModelParameters.floidModelCyclicRangeValue, FLOID_DEBUG_LABEL_CALCULATED_BLADE_PITCH, heliIndex, servoIndex);
      isOk |= checkModelRange(floidModelStatus.floidModelCalculatedServoDegrees[heliIndex][servoIndex],     floidModelParameters.floidModelBladePitchServoDegrees[heliIndex][servoIndex][FLOID_MODEL_LOW_PITCH_INDEX],
                                                                                                            floidModelParameters.floidModelBladePitchServoDegrees[heliIndex][servoIndex][FLOID_MODEL_HIGH_PITCH_INDEX], FLOID_DEBUG_LABEL_CALCULATED_SERVO_DEGREES, heliIndex, servoIndex);
      isOk |= checkModelRange(floidModelStatus.floidModelCalculatedServoPulse[heliIndex][servoIndex],       floidModelParameters.floidModelHeliServoMinPulseWidthValue, floidModelParameters.floidModelHeliServoMaxPulseWidthValue, FLOID_DEBUG_LABEL_CALCULATED_SERVO_PULSE, heliIndex, servoIndex);
    }
    // Check esc:
    isOk |= checkModelRange(floidStatus.escValues[heliIndex], 0.0, 1.0, FLOID_DEBUG_LABEL_ESC_VALUE, heliIndex, -1);
    isOk |= checkModelRange(floidStatus.escServoPulses[heliIndex], floidModelParameters.floidModelESCServoMinPulseWidthValue, floidModelParameters.floidModelESCServoMaxPulseWidthValue, FLOID_DEBUG_LABEL_ESC_PULSE, heliIndex, -1);
  }
  return isOk;
}
boolean checkModelRange(float value, float minRange, float maxRange, int labelToken, int heliIndex, int servoIndex)
{
  // Swap if range inverted:
  if(minRange > maxRange)
  {
    float a  = maxRange;
    maxRange = minRange;
    minRange = a;
  }
  if(value>=minRange)
  {
    if(value<=maxRange)
    {
      return true;
    }
    else
    {
      debugPrintToken(labelToken);
      debugPrint(FLOID_DEBUG_SPACE);
      if(heliIndex >=0)
      {
        debugPrintToken(FLOID_DEBUG_LABEL_HELICOPTER);
        debugPrint(heliIndex);
        debugPrint(FLOID_DEBUG_SPACE);
        if(servoIndex >=0)
        {
          debugPrintToken(FLOID_DEBUG_LABEL_SERVO);
          debugPrint(servoIndex);
          debugPrint(FLOID_DEBUG_SPACE);
        }
      }
      debugPrint(value);
      debugPrintToken(FLOID_DEBUG_LABEL_ABOVE_MAX_OF);
      debugPrintln(maxRange);
      return false;
    }
  }
  else
  {
    debugPrintToken(labelToken);
    debugPrint(FLOID_DEBUG_SPACE);
    if(heliIndex >=0)
    {
      debugPrintToken(FLOID_DEBUG_LABEL_HELICOPTER);
      debugPrint(heliIndex);
      debugPrint(FLOID_DEBUG_SPACE);
      if(servoIndex >=0)
      {
        debugPrintToken(FLOID_DEBUG_LABEL_SERVO);
        debugPrint(servoIndex);
        debugPrint(FLOID_DEBUG_SPACE);
      }
    }
    debugPrint(value);
    debugPrintToken(FLOID_DEBUG_LABEL_BELOW_MIN_OF);
    debugPrintln(minRange);
    return false;
  }
}

boolean floidFlightGoalAchieved()
{
  // Check if we have achieved our flight goal using our mins:
  if((floidStatus.distanceToTarget < floidModelParameters.floidModelDistanceToTargetMin) && (fabs(floidStatus.altitudeToTarget)<=floidModelParameters.floidModelAltitudeToTargetMin) && (fabs(floidStatus.orientationToGoal) < floidModelParameters.floidModelHeadingDeltaMin))
  {
    if(floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_GOAL_ACHIEVED);
    }
    return true;
  }
  return false;
}
// Lift-off and Land Tests:
// ========================
boolean checkIfWeJustLiftedOff()
{
  // We just lifted off if we passed the lift off target altitude, which was our altitude at the time of start of lift-off + lift off target altitude
  return floidStatus.altitude >= floidStatus.liftOffTargetAltitude;
}
// To see if we have landed, we see if the altitude has stabilized for a period of time, since
// in land mode we keep lowering the target altitude so eventually we hit the ground - that is
// as we keep moving our descent target down eventually we no longer achieve a lower altitude and
// after a period of time this is considered landed
boolean checkIfWeJustLanded()
{
  // Save min altitude value and then make sure it has no new minimum for some time period
  if(!floidStatus.landModeMinAltitudeCheckStarted) {
    floidStatus.landModeMinAltitudeCheckStarted = true;
    floidStatus.landModeMinAltitude             = floidStatus.altitude;
    floidStatus.landModeMinAltitudeTime         = millis();
  }
  // Has altitude decreased?
  if(floidStatus.altitude < floidStatus.landModeMinAltitude) {
    // Set a new altitude and reset time:
    floidStatus.landModeMinAltitude = floidStatus.altitude;
    floidStatus.landModeMinAltitudeTime = millis();
  } else {
    // Has altitude increased?
    if(floidStatus.altitude > floidStatus.landModeMinAltitude) {
      // Leave the landModeMinAltitude alone but reset the time:
      floidStatus.landModeMinAltitudeTime = millis();
    } else {
      // Altitude is the same - has this been long enough that we conclude we are landed?
      if((millis() - floidStatus.landModeMinAltitudeTime) > floidModelParameters.floidModelLandModeRequiredTimeAtMinAltitude) {
        if(floidStatus.debug) {
          debugPrintlnToken(FLOID_DEBUG_JUST_LANDED);
        }
        return true;
      }
    }
  }
  return false;
}
// Unused - this was prev used by checkIfWeJustLanded but this might be too restrictive in
// terms of making the collectives achieve the full min value - for example maybe not min if
// it is still attempting to keep the pitch/roll at zero - perhaps best used with a tolerance?
boolean collectivesAreAtLow() {
  // Checks whether all the collectives are at their low values: (for use in landing determination)
  for(int i=0;i<NUMBER_HELICOPTERS; ++i)
  {
    if(floidModelStatus.floidModelCalculatedCollectiveBladePitch[i] != floidModelParameters.floidModelCollectiveMinValue)
    {
      return false;
    }
  }
  return true;
}
// Target States:
// ==============
float calculateDistanceToTarget()
{
  // Spherical distance to target in xy:
  float dt = distance_between(floidStatus.goalY, floidStatus.goalX, floidStatus.gpsYDegreesDecimalFiltered, floidStatus.gpsXDegreesDecimalFiltered, MTR);
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
    if(isTestModePrintDebugPhysicsHeadings()) {
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_GOAL_LATITUDE);
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_GOAL_LONGITUDE);
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_LATITUDE);
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_LONGITUDE);
      debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_DISTANCE_TO_TARGET);
    }
    debugPrint(floidStatus.goalY);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.goalX);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.gpsYDegreesDecimalFiltered);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.gpsXDegreesDecimalFiltered);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(dt);
    debugPrintln();
  }
  return dt;
}
float calculateAltitudeToTarget()
{
  // Distance in Z - in meters:
  float att = floidStatus.goalZ - floidStatus.altitude; 
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
    if(isTestModePrintDebugPhysicsHeadings()) {
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_GOAL_ALTITUDE);
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ALTITUDE);
      debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ALTITUDE_TO_TARGET);
    }
    debugPrint(floidStatus.goalZ);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(floidStatus.altitude);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(att);
    debugPrintln();
  }
  return att;
}
// Calculate the angle of the target location (goalY & goalY (lat/lng)) from our current position:
// In std. degrees:
float calculateAngleOfTarget()
{
  if(floidStatus.distanceToTarget < floidModelParameters.floidModelOrientationMinDistanceToTargetForOrientation)
  {
    // SINCE THIS IS ORIENTATION TO TARGET, WHEN WE ARE CLOSE, we just return a zero saying we are oriented to the target:
    if(floidStatus.debugPhysics && floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_ANGLE_OF_TARGET_NEAR);
    }
    return 0;
  }
  // NOTE: This is the orientation to the target in std degrees
  // Logic:
  // Calculate the vector from here to there and return the angle in degrees:
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
      debugPrintlnToken(FLOID_DEBUG_ANGLE_OF_TARGET_FAR);
  }
  return keepInDegreeRange(ToDeg(atan2(floidStatus.goalY - floidStatus.gpsYDegreesDecimalFiltered, floidStatus.goalX - floidStatus.gpsXDegreesDecimalFiltered)));  // In degrees
}
// Calculate the angle to the target from our current heading: (what is std degrees between our current heading and the target?)
// In std degrees:
float calculateDeltaHeadingAngleToTarget(float angleOfTarget)
{
  // Take the angle of the target and subtract the heading (both are in std trig angles not compass degrees)
  return keepInDegreeRange(angleOfTarget - pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingSmoothed);
}

// Calculate the angular distance from our orientation to our angular goal - this is in std. degrees:
float calculateOrientationToGoal()
{
  // Angular orientation to goal - in std trig angle degrees:
  float otg = keepInDegreeRange(floidStatus.goalAngle - pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingSmoothed);  // NOTE: both goal angle and heading are in std trig degrees
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
    if(isTestModePrintDebugPhysicsHeadings()) {
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_GOAL_ANGLE);
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_HEADING);
      debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_ORIENTATION_TO_GOAL);
    }
    debugPrint(floidStatus.goalAngle);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingSmoothed);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrint(otg);
    debugPrintln();
  }
  return otg;
}
// Angle of Attack:
// ================
// In degrees:
float calculateAttackAngle()
{
  if(floidStatus.landMode || floidStatus.liftOffMode)
  {
    if(floidStatus.debugPhysics && floidStatus.debug)
    {
      // debugPrintlnToken(FLOID_DEBUG_ATTACK_ANGLE_LIFT_OFF_OR_LAND_MODE);
    }
    return 0.0;  // We are level when landing or lifting off
  }
  else
  {
    // Attack angle depends on distance to target:
    return linearlyScale(floidStatus.distanceToTarget, floidModelParameters.floidModelAttackAngleMinDistance, floidModelParameters.floidModelAttackAngleMaxDistance, 0.0, floidModelParameters.floidModelAttackAngleMaxValue, false);
  }
}
// Target Velocities:
// ==================
float calculateTargetPitchVelocity(float targetOrientationPitchDelta)
{
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
    if(isTestModePrintDebugPhysicsHeadings()) {
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_MODEL_PITCH_DELTA_MIN);
      debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_MODEL_PITCH_DELTA_MAX);
    }
    debugPrint(floidModelParameters.floidModelPitchDeltaMin);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrintln(floidModelParameters.floidModelPitchDeltaMax);
  }
  // Idea:
  // Depending on how far we are, we return the ideal velocity to return to that point
  return linearlyScale(targetOrientationPitchDelta, floidModelParameters.floidModelPitchDeltaMin, floidModelParameters.floidModelPitchDeltaMax, 0.0, floidModelParameters.floidModelPitchTargetVelocityMaxValue, true);
}
// In degrees:
float calculateTargetRollVelocity(float targetOrientationRollDelta)
{
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
    if(isTestModePrintDebugPhysicsHeadings()) {
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_MODEL_ROLL_DELTA_MIN);
      debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_MODEL_ROLL_DELTA_MAX);
    }
    debugPrint(floidModelParameters.floidModelRollDeltaMin);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrintln(floidModelParameters.floidModelRollDeltaMax);
  }
  // Idea:
  // Depending on how far we are, we return the ideal velocity to return to that point
  return linearlyScale(targetOrientationRollDelta, floidModelParameters.floidModelRollDeltaMin, floidModelParameters.floidModelRollDeltaMax, 0.0, floidModelParameters.floidModelRollTargetVelocityMaxValue, true);
}
float calculateTargetAltitudeVelocity(float altitudeToTarget)
{
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
    if(isTestModePrintDebugPhysicsHeadings()) {
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_MODEL_ALTITUDE_TO_TARGET_MIN);
      debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_MODEL_ALTITUDE_TO_TARGET_MAX);
    }
    debugPrint(floidModelParameters.floidModelAltitudeToTargetMin);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrintln(floidModelParameters.floidModelAltitudeToTargetMax);
  }
  // Idea:
  // Depending on how far we are, we return the ideal velocity to return to that point
  return linearlyScale(altitudeToTarget, floidModelParameters.floidModelAltitudeToTargetMin, floidModelParameters.floidModelAltitudeToTargetMax, 0.0, floidModelParameters.floidModelAltitudeTargetVelocityMaxValue, true);
}

float linearlyScale(float value, float min, float max, float low, float high, bool preserveSign) {
    float fabsValue = fabs(value);
    if(fabsValue < min) return low * (preserveSign?SIGN(value):1);
    if(fabsValue > max) return high * (preserveSign?SIGN(value):1);
    return ((fabsValue - min)/(max-min)) * (preserveSign?SIGN(value):1);
}

// In degrees:
float calculateTargetHeadingVelocity(float orientationToGoal)
{
  // Idea:
  // Depending on how far we are, we return the ideal velocity to return to that point
  return linearlyScale(orientationToGoal, floidModelParameters.floidModelHeadingDeltaMin, floidModelParameters.floidModelHeadingDeltaMax, 0.0, floidModelParameters.floidModelHeadingTargetVelocityMaxValue, true);
}
// In meters, always non-negative:
float calculateTargetXYVelocity(float distanceToTarget)
{
  // Idea:
  // Depending on how far we are, we return the ideal velocity to return to that point
  if(fabs(distanceToTarget) < floidModelParameters.floidModelDistanceToTargetMin)
  {
    if(floidStatus.debugPhysics && floidStatus.debug)
    {
      debugPrintlnToken(FLOID_DEBUG_XY_STILL);
    }
    return 0.0;  // Don't move!
  }
  else
  {
    if(fabs(distanceToTarget) > floidModelParameters.floidModelDistanceToTargetMax)
    {
      if(floidStatus.debugPhysics && floidStatus.debug)
      {
        debugPrintlnToken(FLOID_DEBUG_XY_FAR);
      }
      return floidModelParameters.floidModelDistanceTargetVelocityMaxValue;
    }
    else
    {
      // Linear combination:
      if(floidStatus.debugPhysics && floidStatus.debug)
      {
        debugPrintlnToken(FLOID_DEBUG_XY_SCALE);
      }
      return ((fabs(distanceToTarget) - floidModelParameters.floidModelDistanceToTargetMin) / (float)(floidModelParameters.floidModelDistanceToTargetMax - floidModelParameters.floidModelDistanceToTargetMin)) * floidModelParameters.floidModelDistanceTargetVelocityMaxValue; // Otherwise scales linearly between 0 and max
    }
  }
}
// Calculate Collective Deltas:
// ============================
// In degrees/sec:
void setCollectiveDeltasOrientation(float targetPitchVelocityDelta, float targetRollVelocityDelta)
{
  float collectivePitchDelta    = targetPitchVelocityDelta * floidModelParameters.floidModelTargetPitchVelocityAlpha;
  float collectiveRollDelta     = targetRollVelocityDelta  * floidModelParameters.floidModelTargetRollVelocityAlpha;
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
    if(isTestModePrintDebugPhysicsHeadings()) {
      debugPrintToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_PITCH_DELTA);
      debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_ROLL_DELTA);
    }
    debugPrint(collectivePitchDelta);
    debugPrint(FLOID_DEBUG_COMMA);
    debugPrintln(collectiveRollDelta);
  }
  // For pitch: 0 v 2 - for roll: 1 v 3
  floidModelStatus.floidModelCollectiveDeltaOrientation[0] =  collectivePitchDelta; // In degrees
  floidModelStatus.floidModelCollectiveDeltaOrientation[2] = -collectivePitchDelta; // In degrees
  floidModelStatus.floidModelCollectiveDeltaOrientation[1] =  collectiveRollDelta;  // In degrees
  floidModelStatus.floidModelCollectiveDeltaOrientation[3] = -collectiveRollDelta;  // In degrees
}
void setCollectiveDeltasAltitude(float targetAltitudeVelocityDelta)
{
  float collectiveAltitudeDelta = targetAltitudeVelocityDelta * floidModelParameters.floidModelTargetAltitudeVelocityAlpha;
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
    if(isTestModePrintDebugPhysicsHeadings()) {
      debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_ALTITUDE_DELTA);
    }
    debugPrintln(collectiveAltitudeDelta);
  }
  for(int heliIndex = 0; heliIndex < NUMBER_HELICOPTERS; ++heliIndex)
  {
    floidModelStatus.floidModelCollectiveDeltaAltitude[heliIndex] = collectiveAltitudeDelta;
  }
}
// This is only called in COUNTER_ROTATION mode:
void setCollectiveDeltasHeading(float targetHeadingVelocityDelta)
{
  float collectiveHeadingDelta = targetHeadingVelocityDelta * floidModelParameters.floidModelTargetHeadingVelocityAlpha;
  if(floidStatus.debugPhysics && floidStatus.debug)
  {
    if(isTestModePrintDebugPhysicsHeadings()) {
      debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_COLLECTIVE_HEADING_DELTA);
    }
    debugPrintln(collectiveHeadingDelta);
  }
  // For orientation: 0 & 2 are counter to 1 & 3:
  floidModelStatus.floidModelCollectiveDeltaHeading[0]    =  collectiveHeadingDelta; // In degrees
  floidModelStatus.floidModelCollectiveDeltaHeading[1]    = -collectiveHeadingDelta; // In degrees
  floidModelStatus.floidModelCollectiveDeltaHeading[2]    =  collectiveHeadingDelta; // In degrees
  floidModelStatus.floidModelCollectiveDeltaHeading[3]    = -collectiveHeadingDelta; // In degrees
}


float keepInRange(float value, float min, float max) {
  if(value<min) return min;
  if(value>max) return max;
  return value;
}

void calculateCyclics(float targetXYVelocity, float targetHeadingVelocityDelta)
{
  // Notes:
  // Two modes: "Counter" and "All Same":
  //
  //  - Counter   - two helis (0 & 2) rotate standard / clockwise
  //              - two helis (1 & 3) rotate opposite / counter-clockwise
  //              - this is implemented by increasing/decreasing the collectives opposite on each
  //
  //  - All Same  - all four helis rotate the same way
  //              - this is implemented by having the cyclics push against the rotation
  //              - this is then one factor of the cyclics - governed by the target heading velocity alpha
  //

  // If we are in all same rotation mode, we use the cyclics to handle heading:
  if(floidModelParameters.floidModelRotationMode == FLOID_ROTATION_MODE_ALL_SAME)
  {
    float cyclicHeadingDelta = targetHeadingVelocityDelta * floidModelParameters.floidModelTargetHeadingVelocityAlpha;
    if(floidStatus.debugPhysics && floidStatus.debug)
    {
      if(isTestModePrintDebugPhysicsHeadings()) {
        debugPrintlnToken(FLOID_DEBUG_LABEL_PHYSICS_DEBUG_CYCLIC_HEADING_DELTA);
      }
      debugPrintln(cyclicHeadingDelta);
    }
    floidModelStatus.floidModelCyclicHeadingValue = fmax(fmin(1.0, floidModelStatus.floidModelCyclicHeadingValue + cyclicHeadingDelta), -1.0);
    // OK, we will convert the cyclicHeadingDelta into a vector for each heli below and then add it to the xy cyclic vector:
  }
  // 1. Calculate our current VOG in the direction of the target:
  float vogToTarget = cos(floidStatus.directionOverGround - floidStatus.deltaHeadingAngleToTarget) * floidStatus.velocityOverGround;
  // 2. Compare current VOG to targetXYVelocity and get the delta:
  float deltaVOG = targetXYVelocity - vogToTarget;
  // 3. Clamp the deltaVOG to [-floidModelDistanceTargetVelocityMaxValue, floidModelDistanceTargetVelocityMaxValue]
  if(deltaVOG > floidModelParameters.floidModelDistanceTargetVelocityMaxValue) {
    deltaVOG = floidModelParameters.floidModelDistanceTargetVelocityMaxValue;
  }
  if(deltaVOG < -floidModelParameters.floidModelDistanceTargetVelocityMaxValue) {
    deltaVOG = -floidModelParameters.floidModelDistanceTargetVelocityMaxValue;
  }
  // 4. Normalize to -1, 1:
  deltaVOG /= floidModelParameters.floidModelDistanceTargetVelocityMaxValue;
  // 5. Calculate the change to cyclic alpha:
  float deltaVelocityCyclicAlpha = deltaVOG * floidModelParameters.velocityDeltaCyclicAlphaScale;
  // 6. Adjust velocityCyclicAlpha:
  floidModelStatus.floidModelVelocityCyclicAlpha = keepInRange(floidModelStatus.floidModelVelocityCyclicAlpha + deltaVelocityCyclicAlpha, 0, 1);
  // 7. Calculate the cyclic values:
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    float heliAngularOffset  = keepInDegreeRange(heliIndex * (-90));                                   // in degrees
    float heliOffsetToTarget = keepInDegreeRange(floidStatus.deltaHeadingAngleToTarget - heliAngularOffset); // In degrees, this is the orientation of the heli to the target (e.g. the way you would have to move in standard degrees to be pointing at the target)
    if(floidStatus.debugPhysics)
    {
      debugPrint(heliIndex);
      debugPrint(FLOID_DEBUG_COMMA);
      debugPrint(heliAngularOffset);
      debugPrint(FLOID_DEBUG_COMMA);
      debugPrint(heliOffsetToTarget);
      debugPrint(FLOID_DEBUG_COMMA);
    }
    // Each servo for each heli:
    for(int servoIndex=0; servoIndex<NUMBER_HELI_SERVOS; ++servoIndex)
    {
      float servoOffsetToTarget           = keepInDegreeRange(heliOffsetToTarget - floidModelParameters.floidModelHeliServoOffsets[heliIndex][servoIndex]);  // should be -105 for heli-0 servo-0 pointing north (heading = 0d) with target at 5,5 = 45d
      float servoCyclicForHeadingRotation = sin(ToRad(floidModelParameters.floidModelHeliServoOffsets[heliIndex][servoIndex])) * floidModelStatus.floidModelCyclicHeadingValue;
      float servoTargetProjection         = -cos(ToRad(servoOffsetToTarget));
      float cyclicValue                   = 0;
      // OK, now we have the projection value for this servo:
      if(floidModelParameters.floidModelRotationMode == FLOID_ROTATION_MODE_ALL_SAME)
      {
        if(floidStatus.debugPhysics && floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_TOKEN_ALL_SAME_ROTATION_MODE);
        }
        // All Same Rotation Mode: Uses cyclic also for rotation not just xy, so the heading (in -1,1) scales by floidModelParameters.floidModelCyclicHeadingAlpha and the regular xy cyclic (in -1,-1) scales by (1-floidModelParameters.floidModelCyclicHeadingAlpha)
        cyclicValue = (servoTargetProjection * floidModelStatus.floidModelVelocityCyclicAlpha) * (1.0-floidModelParameters.floidModelCyclicHeadingAlpha) + servoCyclicForHeadingRotation * floidModelParameters.floidModelCyclicHeadingAlpha;
      }
      else
      {
        if(floidStatus.debugPhysics && floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_TOKEN_COUNTER_ROTATION_MODE);
        }
        // Counter Rotation Mode: Does not use cyclic for rotation:
        cyclicValue = servoTargetProjection * floidModelStatus.floidModelVelocityCyclicAlpha;
      }
      floidModelStatus.floidModelCyclicValue[heliIndex][servoIndex] = cyclicValue;
      if(floidStatus.debugPhysics)
      {
        debugPrint(servoIndex);
        debugPrint(FLOID_DEBUG_COMMA);
        debugPrint(servoOffsetToTarget);
        debugPrint(FLOID_DEBUG_COMMA);
        debugPrint(servoCyclicForHeadingRotation);
        debugPrint(FLOID_DEBUG_COMMA);
        debugPrint(servoTargetProjection);
        debugPrint(FLOID_DEBUG_COMMA);
        debugPrint(cyclicValue);
        debugPrint(FLOID_DEBUG_COMMA);
      }
    }
  }
}
// Collective accumulation:
// ========================
void accumulateCollectiveDeltas()
{
  for(int heliIndex=0; heliIndex<NUMBER_HELICOPTERS; ++heliIndex)
  {
    // Calculate the collective delta totals:
    floidModelStatus.floidModelCollectiveDeltaTotal[heliIndex] =   floidModelStatus.floidModelCollectiveDeltaOrientation[heliIndex] 
                                                                 + floidModelStatus.floidModelCollectiveDeltaAltitude[heliIndex]
                                                                 + floidModelStatus.floidModelCollectiveDeltaHeading[heliIndex];
    // And accumulate them into the current collective values:
    floidModelStatus.floidModelCollectiveValue[heliIndex] = fmin(fmax(floidModelStatus.floidModelCollectiveValue[heliIndex] + floidModelStatus.floidModelCollectiveDeltaTotal[heliIndex], 0.0),1.0);
  }
}

// Commands:
// =========
// Commands From Serial:
boolean loopGetSerialCommand()
{
  // Implement this stub to process commands from the serial port - target is AUX port serial for specific aux hardware
  return false;
}
// Commands from Controller:
boolean loopGetControllerCommand()
{
  boolean          gotCommand       = false;
  boolean          sentResponse     = false;
  unsigned long    commandNumber    = 0lu;
  long             goalId           = 0lu;

  int messageLength = loopCommands();
  if(messageLength > 0)
  {
    getUnsignedLongFromByteOffset(accBuffer, &commandNumber, ACC_COMMAND_NUMBER_OFFSET);
    getLongFromByteOffset(        accBuffer, &goalId,        ACC_GOAL_ID_OFFSET);
    if(commandNumber > 0l)
    {
      gotCommand = true;  // Denotes we need to send a response - not everything necessarily requires a response...
    }
    byte packetType = accBuffer[ACC_PACKET_TYPE_OFFSET];

    // Print the packet out if debug:
    // We do not print heartbeat packet in production mode:
    if(isFloidProductionMode() || packetType != HEARTBEAT_PACKET)
    {
      if(floidStatus.debug)
      {
        debugPrintToken(FLOID_DEBUG_LABEL_COMMAND_RECEIVED);
        debugPrint(commandNumber);
        printPacketTypeLabel(packetType);
        debugPrint(FLOID_DEBUG_SPACE);
//        debugPrint((int)packetType);
        if(goalId<0)
        {
          debugPrintln();
        }
        else
        {
          debugPrintToken(FLOID_DEBUG_LABEL_GOAL_ID);
          debugPrintln(goalId);
        }
      }
    }
    switch(packetType)
    {
      case HEARTBEAT_PACKET:
      {
        // We got a heartbeat - set the global flag - this lets us start the lost communication test protocol:
        gotHeartbeat = true;
        // Update heartbeat time...
        lastHeartbeatTime = millis();
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_HEARTBEAT, &sentResponse);
      }
      break;
      case TEST_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_TEST_PACKET_RECEIVED);
        }
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST, &sentResponse);
        sendTestResponse();
      }
      break;
      case HELI_CONTROL_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_HELI_CONTROL);
        }
        byte  heli        = accBuffer[HELI_CONTROL_PACKET_HELI_OFFSET];
        byte  servo       = accBuffer[HELI_CONTROL_PACKET_SERVO_OFFSET];
        float servoValue;
        getFloatFromByteOffset(accBuffer, &servoValue, HELI_CONTROL_PACKET_VALUE_OFFSET);
        // Servos:
        switch(servo)
        {
          case HELI_CONTROL_LEFT_SERVO:
          case HELI_CONTROL_RIGHT_SERVO:
          case HELI_CONTROL_PITCH_SERVO:
          {
            floidModelSetHeliServoFromValue(heli, servo, servoValue, true);  // And write to servo
            if(floidStatus.debug && floidStatus.debugHelis)
            {
              debugPrintToken(FLOID_DEBUG_HELI_SERVO_VALUE_RECEIVED);
              debugPrint(heli);
              debugPrint(FLOID_DEBUG_COMMA);
              debugPrint((servo==HELI_CONTROL_LEFT_SERVO?"L ":(servo==HELI_CONTROL_RIGHT_SERVO?"R ":"P ")));
              debugPrint(FLOID_DEBUG_COMMA);
              debugPrintln(floidModelStatus.floidModelCalculatedServoPulse[heli][servo]);
            }
            switch(servo)
            {
              case HELI_CONTROL_LEFT_SERVO:
              {
               sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_HELI_SERVO_LEFT, &sentResponse);
              }
              break;
              case HELI_CONTROL_RIGHT_SERVO:
              {
               sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_HELI_SERVO_RIGHT, &sentResponse);
              }
              break;
              case HELI_CONTROL_PITCH_SERVO:
              {
               sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_HELI_SERVO_PITCH, &sentResponse);
              }
              break;
           }
          }
          break;
          case HELI_CONTROL_ESC_SERVO:  // ESC
          {
            floidStatus.escValues[heli] = servoValue;
            if(floidStatus.debug && floidStatus.debugHelis)
            {
              debugPrintToken(FLOID_DEBUG_HELI_ESC_VALUE_RECEIVED);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrint(heli);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrintln(floidStatus.escValues[heli]);
            }
            setESCValue(heli, servoValue);
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_CONTROL, &sentResponse);
          }
          break;
          case HELI_CONTROL_COLLECTIVE_SERVO: // Collective for all three servos:
          {
            if(floidStatus.debug && floidStatus.debugHelis)
            {
              debugPrintToken(FLOID_DEBUG_HELI_COLLECTIVE_SERVO_VALUE_RECEIVED);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrint(heli);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrintln(servoValue);
            }
            // Set the collective to the value and calculate and set it:
            floidModelStatus.floidModelCollectiveValue[heli] = servoValue;
            floidModelCalculateHeliServosFromCollectivesAndCyclics(heli, true);
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_COLLECTIVE_CONTROL, &sentResponse);
          }
          break;
          case HELI_CONTROL_CYCLIC_LEFT_SERVO: // Cyclic for left servo:
          {
            if(floidStatus.debug && floidStatus.debugHelis)
            {
              debugPrintToken(FLOID_DEBUG_HELI_LEFT_SERVO_VALUE_RECEIVED);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrint(heli);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrintln(servoValue);
            }
            // Set the cyclic to the value and calculate and set it:
            floidModelStatus.floidModelCyclicValue[heli][HELI_CONTROL_LEFT_SERVO] = servoValue;
            floidModelCalculateHeliServosFromCollectivesAndCyclics(heli, true);
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_CYCLIC_LEFT, &sentResponse);
          }
          break;
          case HELI_CONTROL_CYCLIC_RIGHT_SERVO: // Cyclic for right servo:
          {
            if(floidStatus.debug && floidStatus.debugHelis)
            {
              debugPrintToken(FLOID_DEBUG_HELI_RIGHT_SERVO_VALUE_RECEIVED);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrint(heli);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrintln(servoValue);
            }
            // Set the cyclic to the value and calculate and set it:
            floidModelStatus.floidModelCyclicValue[heli][HELI_CONTROL_RIGHT_SERVO] = servoValue;
            floidModelCalculateHeliServosFromCollectivesAndCyclics(heli, true);
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_CYCLIC_RIGHT, &sentResponse);
          }
          break;
          case HELI_CONTROL_CYCLIC_PITCH_SERVO: // Cyclic for pitch servo:
          {
            if(floidStatus.debug && floidStatus.debugHelis)
            {
              debugPrintToken(FLOID_DEBUG_HELI_PITCH_SERVO_VALUE_RECEIVED);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrint(heli);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrintln(servoValue);
            }
            // Set the cyclic to the value and calculate and set it:
            floidModelStatus.floidModelCyclicValue[heli][HELI_CONTROL_PITCH_SERVO] = servoValue;
            floidModelCalculateHeliServosFromCollectivesAndCyclics(heli, true);
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_CYCLIC_PITCH, &sentResponse);
          }
          break;
          default:
          {
            if(floidStatus.debug && floidStatus.debugHelis)
            {
              debugPrintToken(FLOID_DEBUG_HELI_SERVO_VALUE_BAD_SERVO);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrint(heli);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrintln(servoValue);
            }
            sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_HELI_UNKNOWN, &sentResponse);
          }
        }
        if(!sentResponse)
        {
          if(floidStatus.debug && floidStatus.debugHelis)
          {
            debugPrintToken(FLOID_DEBUG_HELI_SERVO_VALUE_BAD_HELI);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrint(heli);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrintln(servoValue);
          }
          sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_HELI_UNKNOWN, &sentResponse);
        }
      }
      break;
      case PAN_TILT_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_PAN_TILT_VALUES);
        }
        byte panTiltIndex  = accBuffer[PAN_TILT_PACKET_DEVICE_OFFSET];
        byte panTiltType   = accBuffer[PAN_TILT_PACKET_TYPE_OFFSET];
        switch(panTiltType)
        {
          case PAN_TILT_PACKET_TYPE_ANGLE:
          {
            byte pan  = map(accBuffer[PAN_TILT_PACKET_ANGLE_PAN_OFFSET], 0, 255, 0, 180);
            byte tilt = map(accBuffer[PAN_TILT_PACKET_ANGLE_PAN_OFFSET], 0, 255, 0, 180);
            setPanTiltAngleMode(panTiltIndex, pan, tilt);
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAN_TILT_ANGLE, &sentResponse);
          }
          break;
          case PAN_TILT_PACKET_TYPE_TARGET:
          {
            float targetX;
            float targetY;
            float targetZ;
            getFloatFromByteOffset(accBuffer, &targetX, PAN_TILT_PACKET_TARGET_X_OFFSET);
            getFloatFromByteOffset(accBuffer, &targetY, PAN_TILT_PACKET_TARGET_Y_OFFSET);
            getFloatFromByteOffset(accBuffer, &targetZ, PAN_TILT_PACKET_TARGET_Z_OFFSET);
            setPanTiltTargetMode(panTiltIndex, targetX, targetY, targetZ);
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAN_TILT_TARGET, &sentResponse);
          }
          break;
          case PAN_TILT_PACKET_TYPE_INDIVIDUAL:
          {
            // Individual settings set only one value - side effect is that pan-tilt is set to angle mode and the other value comes from the current value
            byte panOrTilt    = accBuffer[PAN_TILT_PACKET_INDIVIDUAL_PT_OFFSET];
            byte panTiltValue = (byte)map(accBuffer[PAN_TILT_PACKET_INDIVIDUAL_VALUE_OFFSET], 0, 255, 0, 180); 
            setPanTiltIndividual(panTiltIndex, panOrTilt, panTiltValue);
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAN_TILT_INDIVIDUAL, &sentResponse);
          }
          break;
          case PAN_TILT_PACKET_TYPE_SCAN:
          {
            // TODO [PAN_TILT] PAN_TILT_PACKET_TYPE_SCAN MODE NOT YET IMPLEMENTED!!!
            sendErrorCommandResponse(commandNumber, SUB_STATUS_NOT_IMPLEMENTED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAN_TILT_SCAN, &sentResponse);
          }
          break;
          default:
          {
            sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAN_TILT_UNKNOWN, &sentResponse);
          }
          break;
        }
        if(!sentResponse)
        {
          sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAN_TILT_UNKNOWN, &sentResponse);
        }
      }
      break;
      case RELAY_PACKET:
      {
        byte device      = accBuffer[RELAY_PACKET_DEVICE_OFFSET];
        byte deviceState = accBuffer[RELAY_PACKET_STATE_OFFSET];
        switch(device)
        {
        case RELAY_DEVICE_ESC0:
        case RELAY_DEVICE_ESC1:
        case RELAY_DEVICE_ESC2:
        case RELAY_DEVICE_ESC3:
          {
            byte escIndex = device - RELAY_DEVICE_ESC0;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_ESC_RELAY);
              debugPrint(escIndex);
              debugPrint(FLOID_DEBUG_SPACE);
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_OFF);
              }
              stopESC(escIndex);
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_ON);
              }
              startESC(escIndex);
            }
            switch(device)
            {
              case RELAY_DEVICE_ESC0:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_CONTROL_0, &sentResponse);
              }
              break;
              case RELAY_DEVICE_ESC1:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_CONTROL_1, &sentResponse);
              }
              break;
              case RELAY_DEVICE_ESC2:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_CONTROL_2, &sentResponse);
              }
              break;
              case RELAY_DEVICE_ESC3:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_CONTROL_3, &sentResponse);
              }
              break;
            }
          }
          break;
          case RELAY_DEVICE_ESC0_EXTRA:
          case RELAY_DEVICE_ESC1_EXTRA:
          case RELAY_DEVICE_ESC2_EXTRA:
          case RELAY_DEVICE_ESC3_EXTRA:
          {
            byte escIndex = device - RELAY_DEVICE_ESC0_EXTRA;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_ESC_EXTRA_RELAY);
              debugPrint(escIndex);
              debugPrint(FLOID_DEBUG_SPACE);
            }
            setupESCExtra(escIndex);
            switch(device)
            {
              case RELAY_DEVICE_ESC0_EXTRA:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_EXTRA_0, &sentResponse);
              }
              break;
              case RELAY_DEVICE_ESC1_EXTRA:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_EXTRA_1, &sentResponse);
              }
              break;
              case RELAY_DEVICE_ESC2_EXTRA:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_EXTRA_2, &sentResponse);
              }
              break;
              case RELAY_DEVICE_ESC3_EXTRA:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_EXTRA_3, &sentResponse);
              }
              break;
            }
          }
          break;
          case RELAY_DEVICE_ESC0_SETUP:
          case RELAY_DEVICE_ESC1_SETUP:
          case RELAY_DEVICE_ESC2_SETUP:
          case RELAY_DEVICE_ESC3_SETUP:
          {
            byte escIndex = device - RELAY_DEVICE_ESC0_SETUP;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_ESC_SETUP_RELAY);
              debugPrint(escIndex);
              debugPrint(FLOID_DEBUG_COMMA);
            }
            setupESCSetup(escIndex);
            switch(device)
            {
              case RELAY_DEVICE_ESC0_SETUP:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_SETUP_0, &sentResponse);
              }
              break;
              case RELAY_DEVICE_ESC1_SETUP:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_SETUP_1, &sentResponse);
              }
              break;
              case RELAY_DEVICE_ESC2_SETUP:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_SETUP_2, &sentResponse);
              }
              break;
              case RELAY_DEVICE_ESC3_SETUP:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ESC_SETUP_3, &sentResponse);
              }
              break;
            }
          }
          break;
          case RELAY_DEVICE_NM0:
          case RELAY_DEVICE_NM1:
          case RELAY_DEVICE_NM2:
          case RELAY_DEVICE_NM3:
          {
            byte nmIndex = device - RELAY_DEVICE_NM0;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_NM_RELAY);
              debugPrint(nmIndex);
              debugPrint(' ');
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_OFF);
              }
              startPayload(nmIndex, goalId);
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_ON);
              }
              stopPayload(nmIndex);
            }
            switch(device)
            {
              case RELAY_DEVICE_NM0:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_NM_0, &sentResponse);
              }
              break;
              case RELAY_DEVICE_NM1:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_NM_1, &sentResponse);
              }
              break;
              case RELAY_DEVICE_NM2:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_NM_2, &sentResponse);
              }
              break;
              case RELAY_DEVICE_NM3:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_NM_3, &sentResponse);
              }
              break;
            }
          }
          break;
          case RELAY_DEVICE_GPS:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_GPS_RELAY);
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_OFF);
              }
              stopGPS();
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_ON);
              }
              startGPS();
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS, &sentResponse);
          }
          break;
          case RELAY_DEVICE_GPS_EMULATE:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_GPS_EMULATE_RELAY);
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_OFF);
              }
              floidStatus.emulateGPS = false;
              floidStatus.deviceGPS  = false;
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_ON);
              }
              floidStatus.emulateGPS = true;
              floidStatus.deviceGPS  = false;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS_EMULATE, &sentResponse);
          }
          break;
          // GPS To come from Android device or not:
          case RELAY_DEVICE_GPS_DEVICE:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_GPS_DEVICE_RELAY);
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_OFF);
              }
              floidStatus.deviceGPS = false;
              floidStatus.emulateGPS = false;
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_ON);
              }
              floidStatus.deviceGPS = true;
              floidStatus.emulateGPS = false;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS_DEVICE, &sentResponse);
          }
          break;
          // PYR To come from Android device or not:
          case RELAY_DEVICE_PYR_DEVICE:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_PYR_DEVICE_RELAY);
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_OFF);
              }
              floidStatus.devicePYR = false;
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_ON);
              }
              floidStatus.devicePYR = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_DEVICE, &sentResponse);
          }
          break;
          case RELAY_DEVICE_GPS_RATE_0_SWITCH:
          case RELAY_DEVICE_GPS_RATE_1_SWITCH:
          case RELAY_DEVICE_GPS_RATE_2_SWITCH:
          case RELAY_DEVICE_GPS_RATE_3_SWITCH:
          case RELAY_DEVICE_GPS_RATE_4_SWITCH:
          {
            byte rateIndex = device - RELAY_DEVICE_GPS_RATE_0_SWITCH;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_GPS_RATE_SWITCH);
              debugPrintln('0' + rateIndex);
            }
            setGPSRate(rateIndex);
            switch(device)
            {
              case RELAY_DEVICE_GPS_RATE_0_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS_RATE_0, &sentResponse);
              }
              break;
              case RELAY_DEVICE_GPS_RATE_1_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS_RATE_1, &sentResponse);
              }
              break;
              case RELAY_DEVICE_GPS_RATE_2_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS_RATE_2, &sentResponse);
              }
              break;
              case RELAY_DEVICE_GPS_RATE_3_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS_RATE_3, &sentResponse);
              }
              break;
              case RELAY_DEVICE_GPS_RATE_4_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS_RATE_4, &sentResponse);
              }
              break;
              default:
              {
                if(floidStatus.debug)
                {
                  debugPrintToken(FLOID_DEBUG_GPS_BAD_RATE_VALUE);
                  debugPrintln('0' + rateIndex);
                }
                sendGoodCommandResponse(commandNumber, SUB_STATUS_BAD_INDEX, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS_RATE, &sentResponse);
              }
              break;
            }
          }
          break;
          case RELAY_DEVICE_PYR:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_PYR_RELAY);
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_OFF);
              }
              stopPYR();
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_ON);
              }
              startPYR();
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR, &sentResponse);
          }
          break;
          case RELAY_DEVICE_PYR_AD_SWITCH:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_PYR_AD_RELAY);
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_A);
              }
              setPYRAnalog();
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_D);
              }
              setPYRDigital();
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_ANALOG_DIGITAL, &sentResponse);
          }
          break;
          case RELAY_DEVICE_PYR_RATE_0_SWITCH:
          case RELAY_DEVICE_PYR_RATE_1_SWITCH:
          case RELAY_DEVICE_PYR_RATE_2_SWITCH:
          case RELAY_DEVICE_PYR_RATE_3_SWITCH:
          case RELAY_DEVICE_PYR_RATE_4_SWITCH:
          case RELAY_DEVICE_PYR_RATE_5_SWITCH:
          {
            byte rateIndex = device - RELAY_DEVICE_PYR_RATE_0_SWITCH;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_PYR_RATE_SWITCH);
              debugPrintln('0' + rateIndex);
            }
            setPYRRate(rateIndex);
            switch(device)
            {
              case RELAY_DEVICE_PYR_RATE_0_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_RATE_0, &sentResponse);
              }
              break;
              case RELAY_DEVICE_PYR_RATE_1_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_RATE_1, &sentResponse);
              }
              break;
              case RELAY_DEVICE_PYR_RATE_2_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_RATE_2, &sentResponse);
              }
              break;
              case RELAY_DEVICE_PYR_RATE_3_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_RATE_3, &sentResponse);
              }
              break;
              case RELAY_DEVICE_PYR_RATE_4_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_RATE_4, &sentResponse);
              }
              break;
              case RELAY_DEVICE_PYR_RATE_5_SWITCH:
              {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_RATE_5, &sentResponse);
              }
              break;
              default:
              {
                if(floidStatus.debug)
                {
                  debugPrintToken(FLOID_DEBUG_PYR_BAD_RATE_VALUE);
                  debugPrintln('0' + rateIndex);
                }
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_RATE, &sentResponse);
              }
              break;
            }
          }
          break;
          case RELAY_DEVICE_FLOID_STATUS_RATE_0_SWITCH:
          case RELAY_DEVICE_FLOID_STATUS_RATE_1_SWITCH:
          case RELAY_DEVICE_FLOID_STATUS_RATE_2_SWITCH:
          case RELAY_DEVICE_FLOID_STATUS_RATE_3_SWITCH:
          case RELAY_DEVICE_FLOID_STATUS_RATE_4_SWITCH:
          case RELAY_DEVICE_FLOID_STATUS_RATE_5_SWITCH:
          case RELAY_DEVICE_FLOID_STATUS_RATE_6_SWITCH:
          case RELAY_DEVICE_FLOID_STATUS_RATE_7_SWITCH:
          {
            floidStatus.statusRate = device - RELAY_DEVICE_FLOID_STATUS_RATE_0_SWITCH;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_STATUS_RATE_SWITCH);
              debugPrintln('0' + floidStatus.statusRate);
            }
            switch(floidStatus.statusRate)
            {
              case 0:
                currentStatusRateMillis = 0;
                sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STATUS_RATE_0, &sentResponse);
              break;
              case 1:
                currentStatusRateMillis = 250;
                sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STATUS_RATE_1, &sentResponse);
              break;
              case 2:
                currentStatusRateMillis = 500;
                sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STATUS_RATE_2, &sentResponse);
              break;
              case 3:
                currentStatusRateMillis = 750;
                sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STATUS_RATE_3, &sentResponse);
              break;
              case 4: 
                currentStatusRateMillis = 1000;
                sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STATUS_RATE_4, &sentResponse);
              break;
              case 5: 
                currentStatusRateMillis = 2000;
                sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STATUS_RATE_5, &sentResponse);
              break;
              case 6: 
                currentStatusRateMillis = 5000;
                sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STATUS_RATE_6, &sentResponse);
              break;
              case 7: 
                currentStatusRateMillis = 10000;
                sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STATUS_RATE_7, &sentResponse);
              break;
            }
          }
          break;
          case RELAY_DEVICE_DESIGNATOR:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_DESIGNATOR_RELAY);
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_OFF);
              }
              stopDesignating();
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_ON);
              }
              startDesignating();
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_DESIGNATOR, &sentResponse);
          }
          break;
          case RELAY_DEVICE_AUX:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_AUX_RELAY);
            }
            if(deviceState == 0)
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_OFF);
              }
              stopAux();
            }
            else
            {
              if(floidStatus.debug)
              {
                debugPrintln(FLOID_DEBUG_ON);
              }
              startAux();
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_AUX, &sentResponse);
          }
          break;
          case RELAY_DEVICE_PARACHUTE:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_PARACHUTE_RELAY);
            }
            if(deviceState == 0)
            {
              stopParachute();
            }
            else
            {
              startParachute();
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PARACHUTE, &sentResponse);
          }
          break;
          case RELAY_DEVICE_PT0:
          case RELAY_DEVICE_PT1:
          case RELAY_DEVICE_PT2:
          case RELAY_DEVICE_PT3:
          {
            byte ptIndex = device - RELAY_DEVICE_PT0;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_PAN_TILT_RELAY);
              debugPrint(ptIndex);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrintln(deviceState);
            }
            if(deviceState == 0)
            {
              stopPanTilt(ptIndex);
            }
            else
            {
              startPanTilt(ptIndex);
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAN_TILT, &sentResponse);
          }
          break;
          case RELAY_DEVICE_LED0:
          {
            byte ledIndex = device - RELAY_DEVICE_LED0;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_LED_RELAY);
              debugPrint(ledIndex);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            if(deviceState == 0)
            {
              stopLED(ledIndex);
            }
            else
            {
              startLED(ledIndex);
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_LED, &sentResponse);
          }
          break;
          case RELAY_DEVICE_S0:
          case RELAY_DEVICE_S1:
          case RELAY_DEVICE_S2:
          case RELAY_DEVICE_S3:
          {
            byte heliIndex = device - RELAY_DEVICE_S0;
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_HELI_SERVOS_RELAY);
              debugPrint(heliIndex);
              debugPrint(FLOID_DEBUG_SPACE);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            if(deviceState == 0)
            {
              stopHeliServos(heliIndex);
            }
            else
            {
              startHeliServos(heliIndex);
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_HELI_SERVOS, &sentResponse);
          }
          break;
          case RELAY_DEVICE_TEST_LIFT_OFF_MODE_SWITCH:
          {
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_LIFT_OFF_MODE_SWITCH);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_LIFT_OFF_MODE_SWITCH_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_LIFT_OFF_MODE, &sentResponse);
            }
            else
            {
              floidStatus.liftOffMode = true;
              floidStatus.landMode    = false;
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_LIFT_OFF_MODE, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_NORMAL_MODE_SWITCH:
          {
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_NORMAL_MODE_SWITCH);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_NORMAL_MODE_SWITCH_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_NORMAL_MODE, &sentResponse);
            }
            else
            {
              floidStatus.liftOffMode = false;
              floidStatus.landMode    = false;
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_NORMAL_MODE, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_LAND_MODE_SWITCH:
          {
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_LAND_MODE_SWITCH);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_LAND_MODE_SWITCH_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_LAND_MODE, &sentResponse);
            }
            else
            {
              floidStatus.liftOffMode = false;
              floidStatus.landMode    = true;
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_LAND_MODE, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_GOALS:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_GOALS_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            if(deviceState == 0)
            {
                // Valid in test mode only:
                if(floidStatus.mode != FLOID_MODE_TEST)
                {
                  if(floidStatus.debug)
                  {
                    debugPrintlnToken(FLOID_DEBUG_TEST_GOALS_OFF_SWITCH_WRONG_MODE);
                  }
                  sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_GOALS_ON, &sentResponse);
                }
                else
                {
                  floidStatus.goalStartTime = millis();
                  floidStatus.goalTicks     = 0l;
                  floidStatus.hasGoal       = true;
                  if(isTestModePrintDebugPhysicsHeadings())
                  {
                    printPhysicsDebugHeaders = true;
                  }
                  sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_GOALS_ON, &sentResponse);
                }
            }
            else
            {
               // Valid in test mode only:
                if(floidStatus.mode != FLOID_MODE_TEST)
                {
                  if(floidStatus.debug)
                  {
                    debugPrintlnToken(FLOID_DEBUG_TEST_GOALS_OFF_SWITCH_WRONG_MODE);
                  }
                  floidStatus.hasGoal       = false;
                  sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_GOALS_OFF, &sentResponse);
                }
                else
                {
                  floidStatus.hasGoal       = false;
                  sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_GOALS_OFF, &sentResponse);
                }
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_SERIAL_OUTPUT, &sentResponse);
          }
          break;
          case RELAY_DEVICE_TEST_GOAL_ON_SWITCH:
          {
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_TEST_GOALS_ON_SWITCH);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_TEST_GOALS_OFF_SWITCH_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_GOALS_ON, &sentResponse);
            }
            else
            {
              floidStatus.goalStartTime = millis();
              floidStatus.goalTicks     = 0l;
              floidStatus.hasGoal       = true;
              if(isTestModePrintDebugPhysicsHeadings())
              {
                printPhysicsDebugHeaders = true;
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_GOALS_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_GOAL_OFF_SWITCH:
          {
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_TEST_GOALS_OFF_SWITCH);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_TEST_GOALS_OFF_SWITCH_WRONG_MODE);
              }
              floidStatus.hasGoal       = false;
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_GOALS_OFF, &sentResponse);
            }
            else
            {
              floidStatus.hasGoal       = false;
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_GOALS_OFF, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_BLADES_LOW_SWITCH:
          {
            if(floidStatus.debugHelis)
            {
              debugPrintlnToken(FLOID_DEBUG_TEST_BLADES_LOW_SWITCH);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_TEST_BLADES_LOW_SWITCH_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_BLADES_LOW, &sentResponse);
            }
            else
            {
              // Set each servo to the high value:
              for(int heliIndex = 0; heliIndex < NUMBER_HELICOPTERS; ++heliIndex)
              {
                for(int servoIndex =0; servoIndex < NUMBER_HELI_SERVOS; ++servoIndex)
                {
                  floidModelSetHeliServoFromBladePitch(heliIndex, servoIndex, floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_LOW_PITCH_INDEX], true);
                }
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_BLADES_LOW, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_BLADES_ZERO_SWITCH:
          {
            if(floidStatus.debugHelis)
            {
              debugPrintlnToken(FLOID_DEBUG_TEST_BLADES_ZERO_SWITCH);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_TEST_BLADES_ZERO_SWITCH_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_BLADES_ZERO, &sentResponse);
            }
            else
            {
              // Set each servo to the zero value:
              for(int heliIndex = 0; heliIndex < NUMBER_HELICOPTERS; ++heliIndex)
              {
                for(int servoIndex =0; servoIndex < NUMBER_HELI_SERVOS; ++servoIndex)
                {
                  floidModelSetHeliServoFromBladePitch(heliIndex, servoIndex, floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_ZERO_PITCH_INDEX], true);
                }
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_BLADES_ZERO, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_BLADES_HIGH_SWITCH:
          {
            if(floidStatus.debugHelis)
            {
              debugPrintlnToken(FLOID_DEBUG_TEST_BLADES_HIGH_SWITCH);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_TEST_BLADES_HIGH_SWITCH_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_BLADES_HIGH, &sentResponse);
            }
            else
            {
              // Set each servo to the high value:
              for(int heliIndex = 0; heliIndex < NUMBER_HELICOPTERS; ++heliIndex)
              {
                for(int servoIndex =0; servoIndex < NUMBER_HELI_SERVOS; ++servoIndex)
                {
                  floidModelSetHeliServoFromBladePitch(heliIndex, servoIndex, floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_HIGH_PITCH_INDEX], true);
                }
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_BLADES_HIGH, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_LOAD_FROM_EEPROM:
          {
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_EEPROM_LOAD_RELAY);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_EEPROM_LOAD_RELAY_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_EEPROM_LOAD, &sentResponse);
            }
            else
            {
              if(loadFromEEPROM())
              {
                // loaded - send message
                if(floidStatus.debug)
                {
                  debugPrintlnToken(FLOID_DEBUG_SUCCESS);
                }
                resetPhysicsModel();
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_EEPROM_LOAD, &sentResponse);
              }
              else
              {
                // Unable to load - send appropriate response
                if(floidStatus.debug)
                {
                  debugPrintlnToken(FLOID_DEBUG_FAILURE);
                }
                sendErrorCommandResponse(commandNumber, SUB_STATUS_FAILED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_EEPROM_LOAD, &sentResponse);
              }
            }
          }
          break;
          case RELAY_DEVICE_SAVE_TO_EEPROM:
          {
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_EEPROM_SAVE_RELAY);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_EEPROM_SAVE_RELAY_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_EEPROM_SAVE, &sentResponse);
            }
            else
            {
              if(saveToEEPROM())
              {
                // Saved - send message
                debugPrintlnToken(FLOID_DEBUG_SUCCESS);
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_EEPROM_SAVE, &sentResponse);
              }
              else
              {
                if(floidStatus.debug)
                {
                  debugPrintlnToken(FLOID_DEBUG_FAILURE);
                }
                sendErrorCommandResponse(commandNumber, SUB_STATUS_FAILED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_EEPROM_SAVE, &sentResponse);
              }
            }
          }
          break;
          case RELAY_DEVICE_CLEAR_EEPROM:
          {
            if(floidStatus.debug)
            {
              debugPrintlnToken(FLOID_DEBUG_EEPROM_CLEAR_RELAY);
            }
            // Valid in test mode only:
            if(floidStatus.mode != FLOID_MODE_TEST)
            {
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_EEPROM_CLEAR_RELAY_WRONG_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_WRONG_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_EEPROM_CLEAR, &sentResponse);
            }
            else
            {
              if(clearEEPROM())
              {
                // cleared - send message
                if(floidStatus.debug)
                {
                  debugPrintlnToken(FLOID_DEBUG_SUCCESS);
                }
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_EEPROM_CLEAR, &sentResponse);
              }
              else
              {
                if(floidStatus.debug)
                {
                  debugPrintlnToken(FLOID_DEBUG_FAILURE);
                }
                sendErrorCommandResponse(commandNumber, SUB_STATUS_FAILED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_EEPROM_CLEAR, &sentResponse);
              }
            }
          }
          break;
          case RELAY_DEVICE_SERIAL_OUTPUT:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_SERIAL_OUTPUT_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            if(deviceState == 0)
            {
              floidStatus.serialOutput = false;
            }
            else
            {
              floidStatus.serialOutput = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_SERIAL_OUTPUT, &sentResponse);
          }
          break;
          case RELAY_DEVICE_RUN_MODE_PRODUCTION:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_RUN_MODE_PRODUCTION_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            // Note: This should only set the production mode to on, never to off:
            if(deviceState == 1) {
                floidStatus.floidRunMode = FLOID_RUN_MODE_PRODUCTION;
                floidStatus.floidTestMode = FLOID_TEST_MODE_OFF;
                resetFloidStatus(floidStatus.mode);
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PROD_MODE_ON, &sentResponse);
            } else {
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PROD_MODE_OFF, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_MODE_PRINT_DEBUG_HEADINGS:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_TEST_MODE_PRINT_DEBUG_HEADINGS_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setTestModePhysicsPrintDebugPhysicsHeadings(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_DEBUG_PHYSICS_HEADING_OFF, &sentResponse);
            } else {
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_DEBUG_PHYSICS_HEADING_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_MODE_CHECK_PHYSICS_MODEL:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_TEST_MODE_CHECK_PHYSICS_MODEL_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setTestModeCheckPhysicsModel(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_CHECK_PHYSICS_MODEL_OFF, &sentResponse);
            } else {
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_CHECK_PHYSICS_MODEL_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_RUN_MODE_TEST_XY:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_RUN_MODE_TEST_XY_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setRunModePhysicsTestXY(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_X_OFF, &sentResponse);
            } else {
              setUpRunModePhysicsTestXY1();
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_X_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_1:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_RUN_MODE_TEST_ALTITUDE_1_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setRunModePhysicsTestAltitude1(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_A_OFF, &sentResponse);
            } else {
              setUpRunModePhysicsTestAltitude1();
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_A_ON, &sentResponse);
            }
          }
          break;
         case RELAY_DEVICE_RUN_MODE_TEST_ALTITUDE_2:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_RUN_MODE_TEST_ALTITUDE_2_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setRunModePhysicsTestAltitude2(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_B_OFF, &sentResponse);
            } else {
              setUpRunModePhysicsTestAltitude2();
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_B_ON, &sentResponse);
            }
          }
          break;
         case RELAY_DEVICE_RUN_MODE_TEST_HEADING_1:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_RUN_MODE_TEST_HEADING_1_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setRunModePhysicsTestHeading1(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_H_OFF, &sentResponse);
            } else {
              setUpRunModePhysicsTestHeading1();
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_H_ON, &sentResponse);
            }
          }
          break;
         case RELAY_DEVICE_RUN_MODE_TEST_PITCH_1:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_RUN_MODE_TEST_PITCH_1_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setRunModePhysicsTestPitch1(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_P_OFF, &sentResponse);
            } else {
              setUpRunModePhysicsTestPitch1();
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_P_ON, &sentResponse);
            }
          }
          break;
         case RELAY_DEVICE_RUN_MODE_TEST_ROLL_1:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_RUN_MODE_TEST_ROLL_1_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setRunModePhysicsTestRoll1(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_R_OFF, &sentResponse);
            } else {
              setUpRunModePhysicsTestRoll1();
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RUN_MODE_R_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_MODE_NO_XY:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_TEST_MODE_NO_XY_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setTestModePhysicsNoXY(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_X_OFF, &sentResponse);
            } else {
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_X_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_MODE_NO_ALTITUDE:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_TEST_MODE_NO_ALTITUDE_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setTestModePhysicsNoAltitude(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_A_OFF, &sentResponse);
            } else {
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_A_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_MODE_NO_ATTACK_ANGLE:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_TEST_MODE_NO_ATTACK_ANGLE_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setTestModePhysicsNoAttackAngle(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_K_OFF, &sentResponse);
            } else {
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_K_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_MODE_NO_HEADING:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_TEST_MODE_NO_HEADING_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setTestModePhysicsNoHeading(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_H_OFF, &sentResponse);
            } else {
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_H_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_MODE_NO_PITCH:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_TEST_MODE_NO_PITCH_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setTestModePhysicsNoPitch(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_P_OFF, &sentResponse);
            } else {
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_P_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_TEST_MODE_NO_ROLL:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_TEST_MODE_NO_ROLL_RELAY);
              debugPrintln(deviceState==0?FLOID_DEBUG_OFF:FLOID_DEBUG_ON);
            }
            setTestModePhysicsNoRoll(deviceState);
            if(deviceState==0) {
              if(isFloidProductionMode()) {
                  resetFloidStatus(floidStatus.mode);
              }
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_R_OFF, &sentResponse);
            } else {
              sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_MODE_NO_R_ON, &sentResponse);
            }
          }
          break;
          case RELAY_DEVICE_IMU_CALIBRATE: {
            setImuCalibrate();
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_IMU_CALIBRATE, &sentResponse);
          }
          break;
          case RELAY_DEVICE_IMU_SAVE: {
            setImuSave();
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_IMU_SAVE, &sentResponse);
          }
          break;
          default:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_RELAY_UNKNOWN);
              debugPrintln(device);
            }
            sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_UNKNOWN_RELAY, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RELAY, &sentResponse);
          }
          break;
        }
        // If we haven't sent a response, then we have a logic error:
        if(!sentResponse)
        {
          debugPrintToken(FLOID_DEBUG_NO_RESPONSE_SENT);
          debugPrintln(device);
          sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_RELAY, &sentResponse);
        }
      }
      break;
      case DECLINATION_PACKET:
      {
        getFloatFromByteOffset(accBuffer, &floidStatus.declination, DECLINATION_PACKET_DECLINATION_OFFSET);
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_DECLINATION_PACKET);
          debugPrintln(floidStatus.declination);
        }
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_SET_DECLINATION, &sentResponse);
      }
      break;
      case  SET_PARAMETERS_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_SET_PARAMETERS_PACKET);
        }
        // Get the parameters from the object:
        // Note: Implement this if needed to pass parameters to the floid...
        // Send good command response...
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_SET_PARAMETERS, &sentResponse);
      }
      break;
      case  HELIS_ON_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_HELIS_ON_PACKET);
        }
        turnHelisOn();
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_START_HELIS, &sentResponse);
        forceStatusOutput     = true;
      }
      break;
      case  HELIS_OFF_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_HELIS_OFF_PACKET);
        }
        turnHelisOff();
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STOP_HELIS, &sentResponse);
      }
      break;
      case  DESIGNATE_TARGET_PACKET:
      {
        float x;
        float y;
        float z;
        getFloatFromByteOffset(accBuffer, &x, DESIGNATE_TARGET_PACKET_X_OFFSET);
        getFloatFromByteOffset(accBuffer, &y, DESIGNATE_TARGET_PACKET_Y_OFFSET);
        getFloatFromByteOffset(accBuffer, &z, DESIGNATE_TARGET_PACKET_Z_OFFSET);
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_DESIGNATE_TARGET_PACKET);
          // Note: lat long alt:
          debugPrint(y);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrint(x);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrintln(z);
        }
        setDesignatorTarget(x, y, z);
        startDesignating();
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_START_DESIGNATING, &sentResponse);
      }
      break;
      case  DEPLOY_PARACHUTE_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_DEPLOY_PARACHUTE_PACKET);
        }
        deployParachute();
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_DEPLOY_PARACHUTE, &sentResponse);
      }
      break;
      case  STOP_DESIGNATING_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_STOP_DESIGNATING_PACKET);
        }
        stopDesignating();
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_STOP_DESIGNATING, &sentResponse);
      }
      break;
      case  SET_POSITION_GOAL_PACKET:
      {
        getFloatFromByteOffset(accBuffer, &floidStatus.goalX,  SET_POSITION_GOAL_PACKET_X_OFFSET);
        getFloatFromByteOffset(accBuffer, &floidStatus.goalY,  SET_POSITION_GOAL_PACKET_Y_OFFSET);
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_POSITION_GOAL_PACKET);
          // Note: lat lng inFollowMode
          debugPrint(floidStatus.goalY);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrint(floidStatus.goalX);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrintln(floidStatus.followMode?FLOID_DEBUG_ON:FLOID_DEBUG_OFF);
        }
        if(floidStatus.followMode)
        {
          floidStatus.goalAngle = floidStatus.deltaHeadingAngleToTarget;  // NOTE: GoalAngle is in std degrees as is delta heading angle to target
        }
        startGoal(goalId);
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_POSITION_GOAL, &sentResponse);
      }
      break;
      case  SET_ALTITUDE_GOAL_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_ALTITUDE_GOAL_PACKET);
        }
        if(accBuffer[SET_ALTITUDE_GOAL_PACKET_ABSOLUTE_HEIGHT_OFFSET])  // Is it an absolute height or relative
        {
          getFloatFromByteOffset(accBuffer, &floidStatus.goalZ,  SET_ALTITUDE_GOAL_PACKET_Z_OFFSET);
          if(floidStatus.debug)
          {
            debugPrint(FLOID_DEBUG_ABSOLUTE);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrintln(floidStatus.goalZ);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrintln(floidStatus.followMode?FLOID_DEBUG_ON:FLOID_DEBUG_OFF);
          }
        }
        else
        {
          float tmpRelativeAltitude;
          getFloatFromByteOffset(accBuffer, &tmpRelativeAltitude,  SET_ALTITUDE_GOAL_PACKET_Z_OFFSET);
          floidStatus.goalZ += tmpRelativeAltitude; // Add in relative altitude
          if(floidStatus.debug)
          {
            debugPrint(FLOID_DEBUG_RELATIVE);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrint(tmpRelativeAltitude);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrintln(floidStatus.goalZ);
          }
        }
        startGoal(goalId);
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ALTITUDE_GOAL, &sentResponse);
      }
      break;
      case  SET_ROTATION_GOAL_PACKET:
      {
        floidStatus.followMode = accBuffer[SET_ROTATION_GOAL_FOLLOW_MODE_OFFSET];  // Get the follow mode
        // NOTE: For now we do not have a goal pos to follow to - this will get changed in the next set position packet - for mow the goal will always be the current one...
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_ROTATION_GOAL_PACKET);
        }
        if(!floidStatus.followMode)
        {
          getFloatFromByteOffset(accBuffer, &floidStatus.goalAngle, SET_ROTATION_GOAL_PACKET_A_OFFSET);
          if(floidStatus.debug)
          {
            debugPrint(FLOID_DEBUG_FOLLOW);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrintln(floidStatus.goalAngle);
          }
        }
        else
        {
          debugPrintln(FLOID_DEBUG_ABSOLUTE);
          floidStatus.goalAngle = floidStatus.deltaHeadingAngleToTarget;
        }
        startGoal(goalId);
        sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_ROTATIONAL_GOAL, &sentResponse);
      }
      break;
      case  LIFT_OFF_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_LIFT_OFF_PACKET);
        }
        // Make sure the altimeter and everything and the helis are started, etc. etc.
        if(floidStatus.inFlight)
        {
          if(floidStatus.debug)
          {
            debugPrintln();
            debugPrintlnToken(FLOID_DEBUG_LIFT_OFF_PACKET_WRONG_MODE);
          }
          // LOGIC ERROR - ALREADY IN FLIGHT
          sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_ONLY_ON_GROUND, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_LIFT_OFF, &sentResponse);
        }
        else
        {
          // Set the goals to the current position:
          setGoalsToCurrentPosition(); // Sets x, y, z and a goals:
          // Now set z goal from packet:
          getFloatFromByteOffset(accBuffer, &floidStatus.goalZ,     LIFT_OFF_PACKET_Z_OFFSET);
          // Debug:
          if(floidStatus.debug)
          {
            // Note: lat lng alt angle:
            debugPrint(floidStatus.goalY);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrint(floidStatus.goalX);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrint(floidStatus.goalZ);
            debugPrint(FLOID_DEBUG_SPACE);
            debugPrintln(floidStatus.goalAngle);
          }
          // Make sure our lift-off goal height is >= than the current altitude + the lift-off-mode-target-altitude-delta (i.e. min lift-off altitude)
          if(floidStatus.goalZ >= (floidStatus.altitude + floidModelParameters.floidModelLiftOffModeTargetAltitudeDelta))
          {
            setUpForLiftOff();
            startGoal(goalId);
            sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_LIFT_OFF, &sentResponse);
          }
          else
          {
            debugPrintln();
            debugPrintlnToken(FLOID_DEBUG_LIFT_OFF_ALTITUDE_FAILURE);
            // We failed to perform this command...
            sendErrorCommandResponse(commandNumber, SUB_STATUS_FAILED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_LIFT_OFF, &sentResponse);
          }
        }
      }
      break;
      case  LAND_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_LAND_PACKET);
        }
        if(!floidStatus.inFlight)
        {
          if(floidStatus.debug)
          {
            debugPrintln();
            debugPrintlnToken(FLOID_DEBUG_LAND_PACKET_WRONG_MODE);
          }
          floidStatus.landMode      = false;
          // LOGIC ERROR - NOT IN FLIGHT
          sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_ONLY_IN_FLIGHT, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_LAND, &sentResponse);
        }
        else
        {
          // Set the land mode goal (Z in packet is ignored)
          // getFloatFromByteOffset(accBuffer, &floidStatus.goalZ, LAND_PACKET_Z_OFFSET); // We do not do this mny more...
          setGoalsToCurrentPosition();
          floidStatus.goalZ -= 10;  // Land goal is 10m down and then decreases by 5 when we achieve that
          if(floidStatus.debug)
          {
            // Note: lat lng alt angle:
            debugPrintln(floidStatus.goalZ);
          }
          setUpForLanding();
          startGoal(goalId);
          sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_LAND, &sentResponse);
        }
      }
      break;
      case  PAYLOAD_PACKET:
      {
        byte bay = accBuffer[PAYLOAD_PACKET_BAY_OFFSET];
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_PAYLOAD_PACKET);
          debugPrintln(bay);
        }
        if((bay>=0) && (bay < NUMBER_PAYLOADS))
        {
          dropPayload(bay, goalId);
        }
        switch(bay)
        {
          case 0:
          {
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAYLOAD_0, &sentResponse);
          }
          break;
          case 1:
          {
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAYLOAD_1, &sentResponse);
          }
          break;
          case 2:
          {
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAYLOAD_2, &sentResponse);
          }
          break;
          case 3:
          {
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAYLOAD_3, &sentResponse);
          }
          break;
          default:
          {
            if(floidStatus.debug)
            {
              debugPrintToken(FLOID_DEBUG_PAYLOAD_PACKET_BAD_BAY);
            }
            sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PAYLOAD, &sentResponse);
          }
          break;
        }
      }
      break;
      case  DEBUG_CONTROL_PACKET:
      {
        // Debug changes go to serial output:
        if(floidStatus.serialOutput)
        {
          DEBUG_SERIAL.println(FLOID_DEBUG_DBG);
        }
        byte    debugDevice = accBuffer[DEBUG_PACKET_INDEX_OFFSET];
        boolean debugState  = accBuffer[DEBUG_PACKET_VALUE_OFFSET] > 0;
        switch(debugDevice)
        {
          case DEBUG_PACKET_DEBUG:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debug = debugState;
            // Was this an off - we turn off all debugs:
            if(!floidStatus.debug)
            {
              floidStatus.debugMem        = false;
              floidStatus.debugGPS        = false;
              floidStatus.debugPYR        = false;
              floidStatus.debugPhysics    = false;
              floidStatus.debugPanTilt    = false;
              floidStatus.debugNM         = false;
              floidStatus.debugCamera     = false;
              floidStatus.debugDesignator = false;
              floidStatus.debugAUX        = false;
              floidStatus.debugHelis      = false;
              floidStatus.debugAltimeter  = false;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG, &sentResponse);
          }
          break;
          case DEBUG_PACKET_ALL_DEVICES:
          {
            if(floidStatus.serialOutput)
            {

              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_ALL);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
//            floidStatus.debugMem        = debugState;  // We do not turn on debug mem by default when we choose all as it is too noisy!
            floidStatus.debugGPS        = debugState;
            floidStatus.debugPYR        = debugState;
            floidStatus.debugPhysics    = debugState;
            floidStatus.debugPanTilt    = debugState;
            floidStatus.debugNM         = debugState;
            floidStatus.debugCamera     = debugState;
            floidStatus.debugDesignator = debugState;
            floidStatus.debugAUX        = debugState;
            floidStatus.debugHelis      = debugState;
            floidStatus.debugAltimeter  = debugState;
            // Must also turn on debug if we haven't yet:
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_ALL_DEVICES, &sentResponse);
          }
          break;
          case DEBUG_PACKET_GPS:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_GPS);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugGPS        = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_GPS, &sentResponse);
          }
          break;
          case DEBUG_PACKET_PYR:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_PYR);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugPYR        = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_PYR, &sentResponse);
          }
          break;
          case DEBUG_PACKET_PHYSICS:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_PHYSICS);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugPhysics    = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_PHYSICS, &sentResponse);
          }
          break;
          case DEBUG_PACKET_PAN_TILT:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_PAN_TILT);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugPanTilt    = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_PAN_TILT, &sentResponse);
          }
          break;
          case DEBUG_PACKET_MEMORY:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_MEMORY);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugMem        = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_MEMORY, &sentResponse);
          }
          break;
          case DEBUG_PACKET_PAYLOADS:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_PAYLOAD);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugNM         = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_PAYLOAD, &sentResponse);
          }
          break;
          case DEBUG_PACKET_DESIGNATOR:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_DESIGNATOR);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugDesignator = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_DESIGNATOR, &sentResponse);
          }
          break;
          case DEBUG_PACKET_CAMERA:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_CAMERA);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugCamera     = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_CAMERA, &sentResponse);
          }
          break;
          case DEBUG_PACKET_HELIS:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_HELIS);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugHelis      = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_HELIS, &sentResponse);
          }
          break;
          case DEBUG_PACKET_AUX:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_AUX);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugAUX        = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_AUX, &sentResponse);
          }
          break;
          case DEBUG_PACKET_ALTIMETER:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_ALTIMETER);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            floidStatus.debugAltimeter        = debugState;
            if(debugState)
            {
              floidStatus.debug = true;
            }
            sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_ALTIMETER, &sentResponse);
          }
          break;
          default:
          {
            if(floidStatus.serialOutput)
            {
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG);
              DEBUG_SERIAL.print(FLOID_DEBUG_DBG_UNKNOWN);
              if(debugState)
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_ON);
              }
              else
              {
                DEBUG_SERIAL.println(FLOID_DEBUG_OFF);
              }
            }
            sendErrorCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_DEBUG_UNKNOWN, &sentResponse);
          }
          break;
        }
      }
      break;
      case  FLOID_MODE_PACKET:
      {
        byte    newFloidMode = accBuffer[FLOID_MODE_PACKET_MODE_OFFSET];
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_MODE_PACKET);
          debugPrintln(newFloidMode);
        }
        if(newFloidMode != floidStatus.mode)
        {
          switch(newFloidMode)
          {
            case FLOID_MODE_TEST:
            {
              if(floidStatus.inFlight)
              {
                debugPrintlnToken(FLOID_DEBUG_MODE_ERROR_IN_FLIGHT);
                sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_ONLY_ON_GROUND, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODE, &sentResponse);
              }
              else
              {
                switchToTestMode();
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODE, &sentResponse);
              }
            }
            break;
            case FLOID_MODE_MISSION:
            {
              if(floidStatus.inFlight)
              {
                debugPrintlnToken(FLOID_DEBUG_MODE_ERROR_IN_FLIGHT);
                sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_ONLY_ON_GROUND, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODE, &sentResponse);
              }
              else
              {
                switchToMissionMode();
                sendGoodCommandResponse(commandNumber, SUB_STATUS_PERFORMED, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODE, &sentResponse);
              }
            }
            break;
            default:
            {
              // Bad Command:
              if(floidStatus.debug)
              {
                debugPrintlnToken(FLOID_DEBUG_MODE_PACKET_BAD_MODE);
              }
              sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_BAD_INDEX, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODE, &sentResponse);
            }
            break;
          }
        }
        if(!sentResponse)
        {
          sendGoodCommandResponse(commandNumber, SUB_STATUS_ALREADY_IN_MODE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODE, &sentResponse);
        }
      }
      break;
      case  GET_MODEL_PARAMETERS_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_GET_MODEL_PARAMETERS);
        }
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GET_MODEL_PARAMETERS, &sentResponse);
        sendModelParameters();
      }
      break;
      case MODEL_PARAMETERS_1_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_SET_MODEL_PARAMETERS_1);
        }
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchIndices[FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_BLADES_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[0][0][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[0][1][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[0][2][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[1][0][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[1][1][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[1][2][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[0][0][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[0][1][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[0][2][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[1][0][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[1][1][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[1][2][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[0][0][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H0S0_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[0][1][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H0S1_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[0][2][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H0S2_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[1][0][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H1S0_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[1][1][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H1S1_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[1][2][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_1_PACKET_OFFSET_H1S2_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[0][SERVO_LEFT],                 MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_LEFT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[0][SERVO_RIGHT],                MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_RIGHT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[0][SERVO_PITCH],                MODEL_PARAMETERS_1_PACKET_OFFSET_HELI_0_SERVO_OFFSET_PITCH);
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODEL_PARAMETERS_1, &sentResponse);
      }
      break;
      case MODEL_PARAMETERS_2_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_SET_MODEL_PARAMETERS_2);
        }
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[2][0][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[2][1][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[2][2][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[3][0][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[3][1][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[3][2][FLOID_MODEL_LOW_PITCH_INDEX],  MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_LOW);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[2][0][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[2][1][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[2][2][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[3][0][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[3][1][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[3][2][FLOID_MODEL_ZERO_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_ZERO);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[2][0][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H2S0_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[2][1][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H2S1_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[2][2][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H2S2_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[3][0][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H3S0_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[3][1][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H3S1_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelBladePitchServoDegrees[3][2][FLOID_MODEL_HIGH_PITCH_INDEX], MODEL_PARAMETERS_2_PACKET_OFFSET_H3S2_HIGH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelEscCollectiveCalcMidpoint,                    MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_CALC_MIDPOINT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelEscCollectiveLowValue,                        MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_LOW_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelEscCollectiveMidValue,                        MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_MID_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelEscCollectiveHighValue,                       MODEL_PARAMETERS_2_PACKET_OFFSET_ESC_COL_HIGH_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[1][SERVO_LEFT],                 MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_LEFT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[1][SERVO_RIGHT],                MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_RIGHT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[1][SERVO_PITCH],                MODEL_PARAMETERS_2_PACKET_OFFSET_HELI_1_SERVO_OFFSET_PITCH);
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODEL_PARAMETERS_2, &sentResponse);
      }
      break;
      case MODEL_PARAMETERS_3_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_SET_MODEL_PARAMETERS_3);
        }
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelCollectiveMinValue,                     MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MIN);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelCollectiveMaxValue,                     MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_MAX);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelCollectiveDefaultValue ,                MODEL_PARAMETERS_3_PACKET_OFFSET_COLLECTIVE_DEFAULT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelCyclicRangeValue,                       MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_RANGE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelCyclicDefaultValue,                     MODEL_PARAMETERS_3_PACKET_OFFSET_CYCLIC_DEFAULT);
        getIntFromByteOffset(accBuffer,   &floidModelParameters.floidModelESCType,                                MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_TYPE);       // Short
        getIntFromByteOffset(accBuffer,   &floidModelParameters.floidModelESCServoMinPulseWidthValue,             MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MIN);  // Short
        getIntFromByteOffset(accBuffer,   &floidModelParameters.floidModelESCServoMaxPulseWidthValue,             MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_PULSE_MAX);  // Short
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelESCServoDefaultLowValue,                MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_LOW_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelESCServoDefaultHighValue,               MODEL_PARAMETERS_3_PACKET_OFFSET_ESC_HIGH_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelAttackAngleMinDistance,                 MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MIN_DISTANCE_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelAttackAngleMaxDistance,                 MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_MAX_DISTANCE_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelAttackAngleMaxValue,                    MODEL_PARAMETERS_3_PACKET_OFFSET_ATTACK_ANGLE_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelPitchDeltaMin,                          MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MIN_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelPitchDeltaMax,                          MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_DELTA_MAX_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelPitchTargetVelocityMaxValue,            MODEL_PARAMETERS_3_PACKET_OFFSET_PITCH_TARGET_VELOCITY_MAX_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelRollDeltaMin,                           MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MIN_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelRollDeltaMax,                           MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_DELTA_MAX_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelRollTargetVelocityMaxValue,             MODEL_PARAMETERS_3_PACKET_OFFSET_ROLL_TARGET_VELOCITY_MAX_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelAltitudeToTargetMin,                    MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MIN_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelAltitudeToTargetMax,                    MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TO_TARGET_MAX_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelAltitudeTargetVelocityMaxValue,         MODEL_PARAMETERS_3_PACKET_OFFSET_ALTITUDE_TARGET_VELOCITY_MAX_VALUE);
        getUnsignedLongFromByteOffset(accBuffer, &floidModelParameters.floidModelLandModeRequiredTimeAtMinAltitude, MODEL_PARAMETERS_3_PACKET_OFFSET_LAND_MODE_REQUIRED_TIME_AT_MIN_ALTITUDE);  // Unsigned long
        getByteFromByteOffset(accBuffer,  &floidModelParameters.floidModelStartMode,                              MODEL_PARAMETERS_3_PACKET_OFFSET_START_MODE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[2][SERVO_LEFT],                 MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_LEFT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[2][SERVO_RIGHT],                MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_RIGHT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[2][SERVO_PITCH],                MODEL_PARAMETERS_3_PACKET_OFFSET_HELI_2_SERVO_OFFSET_PITCH);
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODEL_PARAMETERS_3, &sentResponse);
      }
      break;
      case MODEL_PARAMETERS_4_PACKET:
      {
        if(floidStatus.debug)
        {
          debugPrintlnToken(FLOID_DEBUG_SET_MODEL_PARAMETERS_4);
        }
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeadingDeltaMin,                              MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MIN_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeadingDeltaMax,                              MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_DELTA_MAX_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeadingTargetVelocityMaxValue,                MODEL_PARAMETERS_4_PACKET_OFFSET_HEADING_TARGET_VELOCITY_MAX_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelDistanceToTargetMin,                          MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MIN_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelDistanceToTargetMax,                          MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TO_TARGET_MAX_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelDistanceTargetVelocityMaxValue,               MODEL_PARAMETERS_4_PACKET_OFFSET_DISTANCE_TARGET_VELOCITY_MAX_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelOrientationMinDistanceToTargetForOrientation, MODEL_PARAMETERS_4_PACKET_OFFSET_ORIENTATION_MIN_DISTANCE_VALUE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelLiftOffModeTargetAltitudeDelta,               MODEL_PARAMETERS_4_PACKET_OFFSET_LIFT_OFF_TARGET_ALTITUDE_DELTA_VALUE);
        getIntFromByteOffset(accBuffer,   &floidModelParameters.floidModelHeliServoMinPulseWidthValue,                  MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MIN); // Short
        getIntFromByteOffset(accBuffer,   &floidModelParameters.floidModelHeliServoMaxPulseWidthValue,                  MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_PULSE_MAX); // Short
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoMinDegreeValue,                      MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MIN);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoMaxDegreeValue,                      MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_DEGREE_MAX);
        getByteFromByteOffset(accBuffer,  &floidModelParameters.floidModelServoSigns[SERVO_LEFT],                       MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_LEFT);
        getByteFromByteOffset(accBuffer,  &floidModelParameters.floidModelServoSigns[SERVO_RIGHT],                      MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_RIGHT);
        getByteFromByteOffset(accBuffer,  &floidModelParameters.floidModelServoSigns[SERVO_PITCH],                      MODEL_PARAMETERS_4_PACKET_OFFSET_SERVO_SIGN_PITCH);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[3][SERVO_LEFT],              MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_LEFT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[3][SERVO_RIGHT],             MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_RIGHT);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelHeliServoOffsets[3][SERVO_PITCH],             MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_3_SERVO_OFFSET_PITCH);
        // Both of these are unused: (Correct ones are: floidModelDistanceToTargetMin and floidModelDistanceToTargetMax)
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelTargetVelocityKeepStill,                      MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_KEEP_STILL);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelTargetVelocityFullSpeed,                      MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_VELOCITY_FULL_SPEED);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelTargetPitchVelocityAlpha,                     MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_PITCH_VELOCITY_ALPHA);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelTargetRollVelocityAlpha,                      MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ROLL_VELOCITY_ALPHA);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelTargetHeadingVelocityAlpha,                   MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_HEADING_VELOCITY_ALPHA);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelTargetAltitudeVelocityAlpha,                  MODEL_PARAMETERS_4_PACKET_OFFSET_TARGET_ALTITUDE_VELOCITY_ALPHA);
        getByteFromByteOffset(accBuffer,  &floidModelParameters.floidModelRotationMode,                                 MODEL_PARAMETERS_4_PACKET_OFFSET_ROTATION_MODE);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.floidModelCyclicHeadingAlpha,                           MODEL_PARAMETERS_4_PACKET_OFFSET_CYCLIC_HEADING_ALPHA);
        getIntFromByteOffset(accBuffer,   &floidModelParameters.heliStartupModeNumberSteps,                             MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_MODE_NUMBER_STEPS); // Short
        getIntFromByteOffset(accBuffer,   &floidModelParameters.heliStartupModeStepTick,                                MODEL_PARAMETERS_4_PACKET_OFFSET_HELI_STARTUP_MODE_STEP_TICK);    // Short
        getFloatFromByteOffset(accBuffer, &floidModelParameters.velocityDeltaCyclicAlphaScale,                          MODEL_PARAMETERS_4_PACKET_OFFSET_VELOCITY_DELTA_CYCLIC_ALPHA);
        getFloatFromByteOffset(accBuffer, &floidModelParameters.accelerationMultiplierScale,                            MODEL_PARAMETERS_4_PACKET_OFFSET_ACCELERATION_MULTIPLIER);

        resetPhysicsModel();
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_MODEL_PARAMETERS_4, &sentResponse);
      }
      break;
      case  TEST_GOALS_PACKET:
      {
        getFloatFromByteOffset(accBuffer, &floidStatus.goalX,     TEST_GOALS_OFFSET_X);
        getFloatFromByteOffset(accBuffer, &floidStatus.goalY,     TEST_GOALS_OFFSET_Y);
        getFloatFromByteOffset(accBuffer, &floidStatus.goalZ,     TEST_GOALS_OFFSET_Z);
        getFloatFromByteOffset(accBuffer, &floidStatus.goalAngle, TEST_GOALS_OFFSET_A);
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_TEST_GOALS_PACKET);
          // Note: lat lng alt angle
          debugPrint(floidStatus.goalY);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrint(floidStatus.goalX);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrint(floidStatus.goalZ);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrintln(floidStatus.goalAngle);
        }
        startGoal(goalId);
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_TEST_GOALS, &sentResponse);
      }
      break;
      case  DEVICE_GPS_PACKET:
      {
        // Get the new gps and set that we have it so it gets processed:
        hasNewDeviceGPS = true;
        getFloatFromByteOffset(accBuffer, &floidStatus.gpsXDegreesDecimal,     DEVICE_GPS_OFFSET_LNG);
        getFloatFromByteOffset(accBuffer, &floidStatus.gpsYDegreesDecimal,     DEVICE_GPS_OFFSET_LAT);
        getFloatFromByteOffset(accBuffer, &floidStatus.gpsZMeters,             DEVICE_GPS_OFFSET_ALT);
        getFloatFromByteOffset(accBuffer, &floidStatus.gpsHDOP,                DEVICE_GPS_OFFSET_HDOP);
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_DEVICE_GPS_PACKET);
          debugPrint(floidStatus.gpsYDegreesDecimal);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrint(floidStatus.gpsXDegreesDecimal);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrint(floidStatus.gpsZMeters);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrintln(floidStatus.goalAngle);
        }
        // Convert / simulate the rest:
        floidStatus.gpsZFeet           = floidStatus.gpsZMeters * METERS_TO_FEET;
        floidStatus.gpsFixQuality      = 1.0;
        floidStatus.gpsSats            = 12;
        floidStatus.gpsTime            = millis();
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_GPS_DEVICE_PACKET, &sentResponse);
      }
      break;
      case  DEVICE_PYR_PACKET:
      {
        hasNewDevicePYR = true;
        // Get the new pyr and set that we have it so it gets processed:
        floidStatus.imuAlgorithmStatus = IMU_ALGORITHM_STATUS_CALIBRATED;
        getFloatFromByteOffset(accBuffer, &pyr[PYR_BOARD_OUTPUT_PITCH],       DEVICE_PYR_OFFSET_PITCH);
        getFloatFromByteOffset(accBuffer, &pyr[PYR_BOARD_OUTPUT_HEADING],     DEVICE_PYR_OFFSET_HEADING);
        getFloatFromByteOffset(accBuffer, &pyr[PYR_BOARD_OUTPUT_ROLL],        DEVICE_PYR_OFFSET_ROLL);
        getFloatFromByteOffset(accBuffer, &pyr[PYR_BOARD_OUTPUT_HEIGHT],      DEVICE_PYR_OFFSET_ALTITUDE);
        getFloatFromByteOffset(accBuffer, &pyr[PYR_BOARD_OUTPUT_TEMPERATURE], DEVICE_PYR_OFFSET_TEMPERATURE);
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_DEVICE_PYR_PACKET);
          debugPrint(pyr[PYR_BOARD_OUTPUT_PITCH]);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrint(pyr[PYR_BOARD_OUTPUT_HEADING]);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrint(pyr[PYR_BOARD_OUTPUT_ROLL]);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrint(pyr[PYR_BOARD_OUTPUT_HEIGHT]);
          debugPrint(FLOID_DEBUG_SPACE);
          debugPrintln(pyr[PYR_BOARD_OUTPUT_TEMPERATURE]);
        }
        sendGoodCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_PYR_DEVICE_PACKET, &sentResponse);
      }
      break;
      default:
      {
        if(floidStatus.debug)
        {
          debugPrintToken(FLOID_DEBUG_UNKNOWN_COMMAND);
          debugPrintln(packetType);
        }
        sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_UNKNOWN_COMMAND, &sentResponse);
      }
      break;
    }
    if(gotCommand && !sentResponse)
    {
      if(floidStatus.debug)
      {
        debugPrintToken(FLOID_DEBUG_NO_RESPONSE_SENT);
        debugPrintln(packetType);
      }
      sendLogicErrorCommandResponse(commandNumber, SUB_STATUS_NONE, FLOID_COMMAND_PACKET_RESPONSE_IDENTIFIER_NO_RESPONSE, &sentResponse);
    }
  }
  return gotCommand;
}
void printPacketTypeLabel(byte packetType)
{
  switch(packetType)
  {
    case TEST_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_TEST_PACKET);
    }
    break;
    case HELI_CONTROL_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_HELI_CONTROL_PACKET);
    }
    break;
    case PAN_TILT_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_PAN_TILT_PACKET);
    }
    break;
    case RELAY_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_RELAY_PACKET);
    }
    break;
    case DECLINATION_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_DECLINATION_PACKET);
    }
    break;
    case  SET_PARAMETERS_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_SET_PARAMETERS_PACKET);
    }
    break;
    case  HELIS_ON_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_HELIS_ON_PACKET);
    }
    break;
    case  HELIS_OFF_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_HELIS_OFF_PACKET);
    }
    break;
    case  DESIGNATE_TARGET_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_DESIGNATE_TARGET_PACKET);
    }
    break;
    case  DEPLOY_PARACHUTE_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_DEPLOY_PARACHUTE_PACKET);
    }
    break;
    case  STOP_DESIGNATING_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_STOP_DESIGNATING_PACKET);
    }
    break;
    case  SET_POSITION_GOAL_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_SET_POSITION_GOAL_PACKET);
    }
    break;
    case  SET_ALTITUDE_GOAL_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_SET_ALTITUDE_GOAL_PACKET);
    }
    break;
    case  SET_ROTATION_GOAL_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_SET_ROTATION_GOAL_PACKET);
    }
    break;
    case  LIFT_OFF_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_LIFT_OFF_PACKET);
    }
    break;
    case  LAND_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_LAND_PACKET);
    }
    break;
    case  PAYLOAD_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_PAYLOAD_PACKET);
    }
    break;
    case  DEBUG_CONTROL_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_DEBUG_CONTROL_PACKET);
    }
    break;
    case  FLOID_MODE_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_FLOID_MODE_PACKET);
    }
    break;
    case  GET_MODEL_PARAMETERS_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_GET_MODEL_PARAMETERS_PACKET);
    }
    break;
    case MODEL_PARAMETERS_1_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_MODEL_PARAMETERS_1_PACKET);
    }
    break;
    case MODEL_PARAMETERS_2_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_MODEL_PARAMETERS_2_PACKET);
    }
    break;
    case MODEL_PARAMETERS_3_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_MODEL_PARAMETERS_3_PACKET);
    }
    break;
    case MODEL_PARAMETERS_4_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_MODEL_PARAMETERS_4_PACKET);
    }
    break;
    case  TEST_GOALS_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_TEST_GOALS_PACKET);
    }
    break;
    case  DEVICE_GPS_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_DEVICE_GPS_PACKET);
    }
    break;
    case  DEVICE_PYR_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_DEVICE_PYR_PACKET);
    }
    break;
    case  HEARTBEAT_PACKET:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_HEARTBEAT_PACKET);
    }
    break;
    default:
    {
      debugPrintToken(FLOID_DEBUG_LABEL_UNKNOWN_PACKET);
    }
    break;
  }
}
void resetPhysicsModel()
{
  // TODO [RESET_PHYSICS_MODEL] SEPARATE OUT THE MODEL STATUS INITIALIZER AND CALL IT FROM HERE
  // TODO [RESET_PHYSICS_MODEL] PERFORM ANY OTHER PHYSICS RESET REQUIRED
}

void turnHelisOn() {
    startHeliServos();  // First we start the servos
    startHeliESCs();    // Then we start the esc's
    floidStatus.heliStartupMode = true;
    heliStartupModeStartTime    = millis();
    heliStartupModeCurrentStep  = 0;
    helisStartupModePhase = HELI_STARTUP_PHASE_START_ESCS;
}

void turnHelisOff() {
    stopHeliESCs();
    stopHeliServos();
    // Also reset the goals and :
    floidStatus.hasGoal         = false;
    floidStatus.goalState       = GOAL_STATE_NONE;
    floidStatus.landMode        = false;
    forceStatusOutput           = true;
    floidStatus.inFlight        = false;
    floidStatus.heliStartupMode = false;
    floidStatus.helisOn         = false;
}

void startHeliESCs()
{
  for(int i=0; i<NUMBER_HELICOPTERS; ++i)
  {
    startESC(i);
  }
}

void stopHeliESCs() {
  for(int i=0; i<NUMBER_HELICOPTERS; ++i) {
    stopESC(i);
  }
}

void setGoalState(int newState) {
  if(newState != floidStatus.goalState) {
    floidStatus.goalState = newState;
    forceStatusOutput     = true;
  }
}
void finishGoal(int completion)
{
}
void setUpForLiftOff()
{
  // Set up for lift off!
  floidStatus.liftOffMode            = true;
  // Lift off mode shuts off after we achieve a delta altitude, then continues as a regular goal - nominally 5 meters in original model:
  floidStatus.liftOffTargetAltitude  = floidStatus.altitude + floidModelParameters.floidModelLiftOffModeTargetAltitudeDelta;
  floidStatus.followMode             = true;
  forceStatusOutput                  = true;
}
void setUpForLanding()
{
  // Set up for land!
  floidStatus.landMode                        = true;
  forceStatusOutput                           = true;
  floidStatus.landModeMinAltitudeCheckStarted = false;
}
void switchToMissionMode()
{
  // Set to production mode and then perform reset and set to mission mode:
  setFloidProductionMode();
  resetFloidStatus(FLOID_MODE_MISSION);
}
void switchToTestMode()
{
  // Perform reset and set to test mode:
  resetFloidStatus(FLOID_MODE_TEST);
}
void resetFloidStatus(int mode)
{
  // Stop the helis:
  stopHeliServos();
  // Reset the modes and states:
  floidStatus.mode        = mode;
  floidStatus.hasGoal     = false;
  floidStatus.goalState   = GOAL_STATE_NONE;
  floidStatus.followMode  = true;
  floidStatus.inFlight    = false;
  floidStatus.landMode    = false;
  floidStatus.liftOffMode = false;
  // If mission mode revert to production mode:
  floidStatus.floidRunMode = mode==FLOID_MODE_MISSION?FLOID_RUN_MODE_PRODUCTION:floidStatus.floidRunMode;
  // Implement anything else needed here:
}
void startGoal(unsigned long goalId)
{
  floidStatus.goalId        = goalId;
  floidStatus.goalStartTime = millis();
  floidStatus.goalTicks     = 0l;
  floidStatus.hasGoal       = true;
  floidStatus.goalState     = GOAL_STATE_START;
  printPhysicsDebugHeaders  = true;
}
// EEProm Proxy routines to avoid compiling against EEProm library in other files:
void eepromWrite(int eepromLocation, byte eepromValue)
{
  EEPROM.write(eepromLocation, eepromValue);
}
byte eepromRead(int eepromLocation)
{
  return EEPROM.read(eepromLocation);
}
// Servo Proxy routines to avoid compiling against Servo library in other files:
void servoAttach(byte servoType, byte servoIndex, byte servoSubIndex, byte servoPin, int servoMinPulseWidth, int servoMaxPulseWidth)
{
  switch(servoType)
  {
    case SERVO_TYPE_ESC:
    {
      escServo[servoIndex].attach(servoPin, servoMinPulseWidth, servoMaxPulseWidth);
    }
    break;
    case SERVO_TYPE_HELI:
    {
      heliServo[servoIndex][servoSubIndex].attach(servoPin, servoMinPulseWidth, servoMaxPulseWidth);
    }
    break;
    case SERVO_TYPE_PAN_TILT:
    {
      panTiltServo[servoIndex][servoSubIndex].attach(servoPin, servoMinPulseWidth, servoMaxPulseWidth);
    }
    break;
    default:
    {
          debugPrintToken(FLOID_DEBUG_SERVO_BAD_TYPE);
          debugPrintln(servoType);
    }
    break;
  }
}
void servoWrite(byte servoType, byte servoIndex, byte servoSubIndex, int servoPulse)
{
  switch(servoType)
  {
    case SERVO_TYPE_ESC:
    {
      escServo[servoIndex].write(servoPulse);
    }
    break;
    case SERVO_TYPE_HELI:
    {
      heliServo[servoIndex][servoSubIndex].write(servoPulse);
    }
    break;
    case SERVO_TYPE_PAN_TILT:
    {
      panTiltServo[servoIndex][servoSubIndex].write(servoPulse);
    }
    break;
    default:
    {
          debugPrintToken(FLOID_DEBUG_SERVO_BAD_TYPE);
          debugPrintln(servoType);
    }
    break;
  }
}

boolean isSafetyMode(int mode)
{
  return (safetyModes & mode) > 0;
}
void setSafetyMode(int mode, boolean on)
{
  if(on)
  {
    safetyModes |= mode;
  }
  else
  {
    safetyModes &= mode;
  }
}
boolean isEmergencyMode(int mode)
{
  return (emergencyModes & mode) > 0;
}
void setEmergencyMode(int mode, boolean on)
{
  if(on)
  {
    emergencyModes |= mode;
  }
  else
  {
    emergencyModes &= mode;
  }
}
void checkSafetyModes()
{
  if(isSafetyMode(SAFETY_MODE_1_CHECK_PHYSICAL_FLIGHT))
  {
    checkSafetyMode1();
  }
  if(isSafetyMode(SAFETY_MODE_2_CHECK_CONTROLLER_COMMUNICATION))
  {
    checkSafetyMode2();
  }
}
void checkSafetyMode1() {
  if(!isEmergencyMode(EMERGENCY_MODE_1_PHYSICAL_FLIGHT_ISSUE)) {
      setEmergencyMode(EMERGENCY_MODE_1_PHYSICAL_FLIGHT_ISSUE, physicalFlightIssue());
  }
}
void checkSafetyMode2() {
  if(!isEmergencyMode(EMERGENCY_MODE_1_PHYSICAL_FLIGHT_ISSUE)) {
      setEmergencyMode(EMERGENCY_MODE_2_LOST_COMMUNICATION_TO_CONTROLLER, lostCommunication());
  }
}
boolean handleEmergencyMode()
{
  // Emergency mode 1 is physical flight issue
  if(isEmergencyMode(EMERGENCY_MODE_1_PHYSICAL_FLIGHT_ISSUE))
  {
    // shut down helis immediately...
    if(floidStatus.helisOn) {
      turnHelisOff();
    }
    // If helis were not on, then maybe the escs are on manually:
    for(int heli=0;heli<NUMBER_HELICOPTERS;++heli) {
      if(floidStatus.escServoOn[heli]) {
        stopESC(heli);
      }
      if(floidStatus.heliServoOn[heli]) {
        stopHeliServos(heli);
      }
    }
    floidStatus.heliStartupMode = false;
    return true; // Do not process any more physics
  }
  // Emergency mode 2 means we lost communication
  if(isEmergencyMode(EMERGENCY_MODE_2_LOST_COMMUNICATION_TO_CONTROLLER))
  {
    // shut down helis immediately...
    if(floidStatus.helisOn) {
      turnHelisOff();
    }
    // If helis were not on, then maybe the escs are on manually:
    for(int heli=0;heli<NUMBER_HELICOPTERS;++heli) {
      if(floidStatus.escServoOn[heli]) {
        stopESC(heli);
      }
      if(floidStatus.heliServoOn[heli]) {
        stopHeliServos(heli);
      }
    }
    floidStatus.heliStartupMode = false;
    return true; // Do not process any more physics
  }
  return false; // No emergency mode
}

boolean lostCommunication()
{
  // 1. We need to wait for the first ever heartbeat
  if(gotHeartbeat)
  {
    if(millis() > lastHeartbeatTime + requiredHeartbeatPeriod)
    {
      return true;
    }
  }
  return false;
}

// Perform whatever is necessary to determine if we have a physical flight issue:
boolean physicalFlightIssue()
{
  if(floidStatus.helisOn) {
    if(fabs(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitchSmoothed) > FLIGHT_ISSUE_TEST_MAX_PITCH) {
      debugPrintlnToken(FLOID_DEBUG_LABEL_MAX_PITCH_EXCEEDED);
      return true;
    }
    if(fabs(pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].rollSmoothed) > FLIGHT_ISSUE_TEST_MAX_ROLL) {
      debugPrintlnToken(FLOID_DEBUG_LABEL_MAX_ROLL_EXCEEDED);
      return true;
    }
  }
  return false;
}

// Test variables:
// For Tests that use a target:
float physicsTestTarget = 0.0;
// For tests that cycle:
boolean physicsTestIncrease = true;
// For tests that use a count:
int physicsTestCount = 0;

void setGoalsToCurrentPosition() {
 // Start target at current altitude, position, pitch, etc
 floidStatus.goalZ = floidStatus.altitude;
 floidStatus.goalX = floidStatus.gpsXDegreesDecimalFiltered;
 floidStatus.goalY = floidStatus.gpsYDegreesDecimalFiltered;
 floidStatus.goalAngle = pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingSmoothed; // Both are in std trig angles - not compass angles!
 floidStatus.attackAngle = 0.0;
}

// Set Lat/lng: 0,0  Altitude: 0  Altitude goal: 0, Angle goal: 0 Heading: 0 etc.
void setPositionAndGoalsToZero() {
  floidStatus.gpsXDegreesDecimalFiltered = 0;
  floidStatus.gpsYDegreesDecimalFiltered = 0;
  floidStatus.altitude = 0;
  floidStatus.goalX = 0;
  floidStatus.goalY = 0;
  floidStatus.goalZ = 0;
  // Set the heading to the target angle:
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].headingSmoothed = 0;  // Keep us always pointing north
  floidStatus.goalAngle = 0;
}

void setVelocitiesToZero() {
  floidStatus.rollVelocity = 0;
  floidStatus.pitchVelocity = 0;
  floidStatus.headingVelocity = 0;
  floidStatus.heightVelocity = 0;
  floidStatus.targetOrientationPitch = 0;
  floidStatus.targetOrientationRoll = 0;
  floidStatus.targetOrientationPitchDelta = 0;
  floidStatus.targetOrientationRollDelta = 0;
  floidStatus.targetPitchVelocity = 0;
  floidStatus.targetRollVelocity = 0;
  floidStatus.targetPitchVelocityDelta = 0;
  floidStatus.targetRollVelocityDelta = 0;
  floidStatus.targetAltitudeVelocity = 0;
  floidStatus.targetAltitudeVelocityDelta = 0;
  floidStatus.targetHeadingVelocity = 0;
  floidStatus.targetHeadingVelocityDelta = 0;
  floidStatus.targetXYVelocity = 0;
}

void turnOffAllPhysicsModes() {
    setTestModePhysicsNoXY(true);
    setTestModePhysicsNoAltitude(true);
    setTestModePhysicsNoAttackAngle(true);
    setTestModePhysicsNoHeading(true);
    setTestModePhysicsNoPitch(true);
    setTestModePhysicsNoRoll(true);
}

void setUpRunModePhysicsTestXY1() {
  // Output our setup:
  debugPrint('S');
  debugPrint('U');
  debugPrint('T');
  debugPrint('X');
  debugPrintln('Y');
  // Set goals to current position:
  turnOffAllPhysicsModes();
  setGoalsToCurrentPosition();
  setVelocitiesToZero();
  floidStatus.altitudeVelocity = 0;
  // Set up:
  physicsTestIncrease = true;
  physicsTestCount = 0;
  physicsTestTarget = 0.0;
  // Turn on the XY:
  setTestModePhysicsNoXY(false);
  // Start Goal:
  startGoal(TEST_GOAL_ID);
}

// Changes goal x and goal y to move around
void physicsTestXY1()
{
//  debugPrint('T');
//  debugPrint('X');
//  debugPrintln('Y');
  // Set to 0's and zero the goals:
  setPositionAndGoalsToZero();
  // Update the target - here target is angle of goal rotation:
  physicsTestTarget = keepInDegreeRange(physicsTestTarget + PHYSICS_TEST_XY_1_ANGLE_DELTA*FLOID_PERIOD/1000);  // Move by the delta ~every second
  // Rotate the x,y goal:
  floidStatus.goalX = sin(ToRad(physicsTestTarget)) * PHYSICS_TEST_XY_1_DISTANCE;
  floidStatus.goalY = cos(ToRad(physicsTestTarget)) * PHYSICS_TEST_XY_1_DISTANCE;
}

/*
  Test Altitude 1
  ---------------
  This test:
    => cyclicly increases and decreases the target altitude from
         0 to +10m and back to -10m centered around the current altitude
    => should make the altitude collectives go to max and min accordingly
*/

void setUpRunModePhysicsTestAltitude1() {
  // Output our setup:
  debugPrint('S');
  debugPrint('U');
  debugPrint('T');
  debugPrint('A');
  debugPrintln('1');
  // Turn off all physics modes:
  turnOffAllPhysicsModes();
  // Set goals to current position:
  setGoalsToCurrentPosition();
  setVelocitiesToZero();
  // Start up:
  physicsTestIncrease = true;
  physicsTestCount = 0;
  physicsTestTarget = 0.0;
  // Turn on the Altitude:
  setTestModePhysicsNoAltitude(false);
  // Start Goal:
  startGoal(TEST_GOAL_ID);
}

void physicsTestAltitude1()
{
//  debugPrint('T');
//  debugPrint('A');
//  debugPrintln('1');
  // If we are going up - have we reached the limit:
  if(physicsTestIncrease && ((physicsTestTarget - floidStatus.altitude) >= PHYSICS_TEST_ALTITUDE_1_RANGE)) {
    physicsTestIncrease = false;
  }
  // If we are going don - have we reached the limit:
  if(!physicsTestIncrease && ((physicsTestTarget - floidStatus.altitude) <= -(PHYSICS_TEST_ALTITUDE_1_RANGE))) {
    physicsTestIncrease = true;
  }
  // Adjust:
  if(physicsTestIncrease) {
    physicsTestTarget += PHYSICS_TEST_ALTITUDE_1_DELTA*FLOID_PERIOD/1000.0;
  } else {
    physicsTestTarget -= PHYSICS_TEST_ALTITUDE_1_DELTA*FLOID_PERIOD/1000.0;
  }
  floidStatus.goalZ = physicsTestTarget;
}

/*
  Test Altitude 2
  ---------------
  This test:
    sets the altitude and goal altitude to 50
    starts cyclicly increasing and decreasing the altitude velocity over 200 steps [-100 to 100], starting at 0
    this should have the effect of having the cyclic head the opposite (ie. starting down and then going back up etc)
*/
void setUpRunModePhysicsTestAltitude2() {
  // Output our setup:
  debugPrint('S');
  debugPrint('U');
  debugPrint('T');
  debugPrint('A');
  debugPrintln('2');
  // Turn off all physics modes:
  turnOffAllPhysicsModes();
  // Set goals to current position:
  setGoalsToCurrentPosition();
  setVelocitiesToZero();
  // Start up:
  physicsTestIncrease = false; // Start by decreasing altitude velocity to make target alt velocity increase
  physicsTestCount = 0;
  physicsTestTarget = 0.0;
  // Turn on the Altitude:
  setTestModePhysicsNoAltitude(false);
  // Start Goal:
  startGoal(TEST_GOAL_ID);
}

// Test the altitude physics via altitude velocity:
void physicsTestAltitude2()
{
//  debugPrint('T');
//  debugPrint('A');
//  debugPrintln('2');
  ++physicsTestCount;
  if(physicsTestCount > PHYSICS_TEST_ALTITUDE_2_MAX_COUNT)
  {
    floidStatus.altitudeVelocity = 0; // Reset altitude velocity to zero
    physicsTestIncrease = !physicsTestIncrease; // Go the other direction
    physicsTestCount = 0; // And reset the count
  }
  // Modify the altitude velocity:
  if(physicsTestIncrease)
  {
    floidStatus.altitudeVelocity += PHYSICS_TEST_ALTITUDE_2_VELOCITY_DELTA*FLOID_PERIOD/1000.0;
  }
  else
  {
    floidStatus.altitudeVelocity -= PHYSICS_TEST_ALTITUDE_2_VELOCITY_DELTA*FLOID_PERIOD/1000.0;
  }
}

/*
  This test:
    continually increases the goal angle
    should make the heading collectives go to max and min accordingly
*/
void setUpRunModePhysicsTestHeading1() {
  // Output our setup:
  debugPrint('S');
  debugPrint('U');
  debugPrint('T');
  debugPrint('P');
  debugPrintln('H');
  // Turn off all physics modes:
  turnOffAllPhysicsModes();
  // Set goals to current position:
  setGoalsToCurrentPosition();
  setVelocitiesToZero();
  // Start up:
  physicsTestIncrease = true;
  physicsTestCount = 0;
  physicsTestTarget = 0.0; // Test target is heading in compass degrees
  // Turn on the Heading:
  setTestModePhysicsNoHeading(false);
  // Start Goal:
  startGoal(TEST_GOAL_ID);
}

// Test heading physics:
void physicsTestHeading1()
{
//  debugPrint('T');
//  debugPrint('P');
//  debugPrintln('H');
  physicsTestTarget = keepInDegreeRange(physicsTestTarget + PHYSICS_TEST_HEADING_1_DELTA*FLOID_PERIOD/1000.0);
  // Set goal angle:
  floidStatus.goalAngle = physicsTestTarget;
}

/*
  This test:
    cyclicly increases and decreases the target pitch velocity
    changes the pitch velocity to a static positive or negative value
    should make the pitch collectives go to max and min accordingly
*/
void setUpRunModePhysicsTestPitch1() {
  // Output our setup:
  debugPrint('S');
  debugPrint('U');
  debugPrint('T');
  debugPrint('P');
  debugPrintln('1');
  // Turn off all physics modes:
  turnOffAllPhysicsModes();
  // Set goals to current position:
  setGoalsToCurrentPosition();
  setVelocitiesToZero();
  // Start up:
  physicsTestIncrease = true;
  physicsTestCount = 0;
  physicsTestTarget = 0.0;
  // Turn on the Pitch:
  setTestModePhysicsNoPitch(false);
  // Start Goal:
  startGoal(TEST_GOAL_ID);
}

// Test the pitch physics:
void physicsTestPitch1()
{
//  debugPrint('T');
//  debugPrint('P');
//  debugPrintln('1');
  ++physicsTestCount;
  if(physicsTestCount > PHYSICS_TEST_PITCH_1_MAX_COUNT)
  {
    physicsTestIncrease = !physicsTestIncrease;
    physicsTestCount = -(PHYSICS_TEST_PITCH_1_MAX_COUNT);
  }
  if(physicsTestIncrease)
  {
    // Change target pitch velocity:
    physicsTestTarget += PHYSICS_TEST_PITCH_1_DELTA*FLOID_PERIOD/1000.0;
    // Set pitch velocity:
    floidStatus.pitchVelocity = PHYSICS_TEST_PITCH_1_DELTA;  // Note: This is not 2 m/s
  }
  else
  {
    // Change target pitch velocity:
    physicsTestTarget -= PHYSICS_TEST_PITCH_1_DELTA*FLOID_PERIOD/1000.0;
    // Set pitch velocity:
    floidStatus.pitchVelocity = -PHYSICS_TEST_PITCH_1_DELTA;  // Note: This is not 2 m/s
  }
  // Set current pitch:
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].pitchSmoothed = physicsTestTarget;
}
/*
  This test:
    cyclicly increases and decreases the target roll velocity
    changes the roll velocity to a static positive or negative value
    should make the roll collectives go to max and min accordingly
*/
void setUpRunModePhysicsTestRoll1() {
  // Output our setup:
  debugPrint('S');
  debugPrint('U');
  debugPrint('T');
  debugPrint('R');
  debugPrintln('1');
  // Turn off all physics modes:
  turnOffAllPhysicsModes();
  // Set goals to current position:
  setGoalsToCurrentPosition();
  setVelocitiesToZero();
  // Start up:
  physicsTestIncrease = true;
  physicsTestCount = 0;
  physicsTestTarget = 0.0;
  // Turn on the Altitude:
  setTestModePhysicsNoRoll(false);
  // Start Goal:
  startGoal(TEST_GOAL_ID);
}
// Test the roll physics:
void physicsTestRoll1()
{
//  debugPrint('T');
//  debugPrint('R');
//  debugPrintln('1');
  ++physicsTestCount;
  if(physicsTestCount > PHYSICS_TEST_ROLL_1_MAX_COUNT)
  {
    physicsTestIncrease = !physicsTestIncrease;
    physicsTestCount = -(PHYSICS_TEST_ROLL_1_MAX_COUNT);
  }
  if(physicsTestIncrease)
  {
    // Change target roll velocity:
    physicsTestTarget += PHYSICS_TEST_ROLL_1_DELTA*FLOID_PERIOD/1000.0;
    // Set roll velocity:
    floidStatus.rollVelocity    = PHYSICS_TEST_ROLL_1_DELTA;
  }
  else
  {
    // Change target roll velocity:
    physicsTestTarget -= PHYSICS_TEST_ROLL_1_DELTA*FLOID_PERIOD/1000.0;
    // Set roll velocity:
    floidStatus.rollVelocity    = -PHYSICS_TEST_ROLL_1_DELTA;
  }
  // Set current roll:
  pyrBufferEntry[PYR_BUFFER_ENTRY_LENGTH-1].rollSmoothed = physicsTestTarget;
}
