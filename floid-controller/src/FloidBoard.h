////////////////////////////////////////////
// (c) 2009-2020 Chris Faigle/faiglelabs  //
//  All rights reserved.                  //
//  This code is strictly licensed.       //
////////////////////////////////////////////
#ifndef	__FLOIDBOARD_H__
#define	__FLOIDBOARD__H__

/* ==============================
 * ACCELERATION CONSTANT: (m/s^s)
 * ============================== */
#define ACCELERATION_ONE_G                      (9.80665)
////////////////////////////////////////////////////////////
// Safety and emergency modes                             //
////////////////////////////////////////////////////////////
// These are turned off by parameters only perhaps        //
////////////////////////////////////////////////////////////
#define SAFETY_MODE_1_CHECK_PHYSICAL_FLIGHT                 (1<<0)
#define SAFETY_MODE_2_CHECK_CONTROLLER_COMMUNICATION        (1<<1)
#define EMERGENCY_MODE_1_PHYSICAL_FLIGHT_ISSUE              (1<<0)
#define EMERGENCY_MODE_2_LOST_COMMUNICATION_TO_CONTROLLER   (1<<1)
// ==============================
// PHYSICS MODEL VELOCITY CLAMPS:
// ==============================
#define PITCH_VELOCITY_DELTA_CLAMP      (4*ACCELERATION_ONE_G)
#define ROLL_VELOCITY_DELTA_CLAMP       (4*ACCELERATION_ONE_G)
#define ALTITUDE_VELOCITY_DELTA_CLAMP   (4*ACCELERATION_ONE_G)
#define HEADING_VELOCITY_DELTA_CLAMP    (4*ACCELERATION_ONE_G)
/* ================================
 * CALCULATE PHYSICS PERIOD LIMITS:
 * ================================ */
#define CALCULATE_PHYSICS_TOO_SHORT_PERIOD      (10)
#define CALCULATE_PHYSICS_TOO_LONG_PERIOD       (2000)
#define CALCULATE_PHYSICS_MAX_PERIOD            (1000)
/* ==========
 * ALTIMETER:
 * ========== */
// Define this to use altimeter (GPS+baro) instead of GPS only:
// NOTE: DO NOT USE ALTIMETER
// NOTE: Testing (3/8/23): altimeter (GPS+baro) less accurate than GPS only
//#define USE_ALTIMETER
/* ===========================
 * CHECK PHYSICS MODEL LIMITS:
 * =========================== */
// Angle limits:
#define  DEGREE_ANGLE_MIN                       (-180)
#define  DEGREE_ANGLE_MAX                        (180)
// Circumference of Earth: 40,075  - we will need half of this plus some slop
#define  HALF_CIRCUMFERENCE_OF_EARTH_IN_METERS   (21000000)
// Heli-Startup:
// Delay until ESCs have time to start:
#define HELI_STARTUP_MODE_ESC_ON_DELAY      (8000ul)
#define HELI_STARTUP_COLLECTIVE_TARGET      (0.40)
// Heli Startup:
#define HELI_STARTUP_PHASE_NONE             (0)
#define HELI_STARTUP_PHASE_START_ESCS       (1)
#define HELI_STARTUP_PHASE_POWER_HELIS      (2)
// Physical error checks:
#define FLIGHT_ISSUE_TEST_MAX_PITCH         (30)
#define FLIGHT_ISSUE_TEST_MAX_ROLL          (30)
///////////////////////////////////
// Heartbeat and Loop Rate:      //
///////////////////////////////////
// Defaults to #defined - set to blink on led every status communication:
#define BLINK_LED_ON_STATUS_COMM
// Defaults to #undefined - set this one time to print offsets during dev.
//#define          PRINT_OFFSETS_ONLY
////////////////////
//                //
// PHYSICS TESTS: //
//                //
////////////////////
#define PHYSICS_TEST_XY_1_ANGLE_DELTA (6.0)
#define PHYSICS_TEST_XY_1_DISTANCE (3)
#define PHYSICS_TEST_ALTITUDE_1_RANGE (10)
#define PHYSICS_TEST_ALTITUDE_1_DELTA (2)
#define PHYSICS_TEST_ALTITUDE_2_MAX_COUNT (100)
#define PHYSICS_TEST_ALTITUDE_2_VELOCITY_DELTA (0.1)
#define PHYSICS_TEST_PITCH_1_MAX_COUNT (40)
#define PHYSICS_TEST_ROLL_1_MAX_COUNT (40)
#define PHYSICS_TEST_HEADING_1_DELTA (3)
#define PHYSICS_TEST_PITCH_1_DELTA (2)
#define PHYSICS_TEST_ROLL_1_DELTA (2)

// Math:
#define SIGN(x)             ((x)<0?(-1):(1))

// Prototypes:
void calculatePhysics();
void processStartupMode();
float clampVelocityDelta(float value, float clampMin, float clampMax);
float keepInRange(float value, float min, float max);
boolean checkPhysicsModel();
boolean checkModelRange(float value, float minRange, float maxRange, int labelToken, int heliIndex, int servoIndex);
boolean floidFlightGoalAchieved();
boolean checkIfWeJustLiftedOff();
boolean checkIfWeJustLanded();
boolean collectivesAreAtLow();
float linearlyScale(float value, float min, float max, float low, float high, bool preserveSign);
float calculateDistanceToTarget();
float calculateAltitudeToTarget();
float calculateAngleOfTarget();
float calculateDeltaHeadingAngleToTarget(float angleOfTarget);
float calculateOrientationToGoal();
float calculateAttackAngle();
float calculateTargetPitchVelocity(float targetOrientationPitchDelta);
float calculateTargetRollVelocity(float targetOrientationRollDelta);
float calculateTargetAltitudeVelocity(float altitudeToTarget);
float calculateTargetHeadingVelocity(float orientationToGoal);
float calculateTargetXYVelocity(float distanceToTarget);
void setCollectiveDeltasOrientation(float targetPitchVelocityDelta, float targetRollVelocityDelta);
void setCollectiveDeltasAltitude(float targetAltitudeVelocityDelta);
void setCollectiveDeltasHeading(float targetHeadingVelocityDelta);
void calculateCyclics(float targetXYVelocity, float targetHeadingVelocityDelta);
void accumulateCollectiveDeltas();
boolean loopGetSerialCommand();
boolean loopGetControllerCommand();
void printPacketTypeLabel(byte packetType);
void resetPhysicsModel();
void turnHelisOn();
void turnHelisOff();
void startHeliESCs();
void stopHeliESCs();
void setGoalState(int newState);
void finishGoal(int completion);
void setUpForLiftOff();
void setUpForLanding();
void switchToMissionMode();
void switchToTestMode();
void resetFloidStatus(int mode);
void startGoal(unsigned long goalId);
void eepromWrite(int eepromLocation, byte eepromValue);
byte eepromRead(int eepromLocation);
void servoAttach(byte servoType, byte servoIndex, byte servoSubIndex, byte servoPin, int servoMinPulseWidth, int servoMaxPulseWidth);
void servoWrite(byte servoType, byte servoIndex, byte servoSubIndex, int servoPulse);
boolean isSafetyMode(int mode);
void setSafetyMode(int mode, boolean on);
boolean isEmergencyMode(int mode);
void setEmergencyMode(int mode, boolean on);
void checkSafetyModes();
void checkSafetyMode1();
void checkSafetyMode2();
boolean handleEmergencyMode();
boolean lostCommunication();
boolean physicalFlightIssue();
void setGoalsToCurrentPosition();
void setPositionAndGoalsToZero();
void setVelocitiesToZero();
void setUpRunModePhysicsTestXY1();
void physicsTestXY1();
void setUpRunModePhysicsTestAltitude1();
void physicsTestAltitude1();
void setUpRunModePhysicsTestAltitude2();
void physicsTestAltitude2();
void setUpRunModePhysicsTestHeading1();
void physicsTestHeading1();
void setUpRunModePhysicsTestPitch1();
void physicsTestPitch1();
void setUpRunModePhysicsTestRoll1();
void physicsTestRoll1();
void printPhysicsDebugHeadersOut();

#endif /* __FLOIDBOARD_H__ */
